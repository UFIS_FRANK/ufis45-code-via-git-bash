using System;
using System.Data;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using Ufis.BL.Common;
using Ufis.Utils;

namespace Ufis.Snapshot.Win
{
	/// <summary>
	/// Summary description for CompareFunctionViewer.
	/// </summary>
	public class CompareFunctionViewer : Microsoft.ApplicationBlocks.UIProcess.WindowsFormView
	{
		private AxTABLib.AxTAB tabImport;
		private AxTABLib.AxTAB tabAODB;
		private System.Windows.Forms.Button buttonDetails;
		private System.Windows.Forms.Button buttonClose;
		private System.Windows.Forms.StatusBar statusBar1;
		private DataTable dataTableImport;
		private DataTable dataTableAODB;
		private DataSet dataSetImport;
		private DataSet dataSetAODB;
		private System.Windows.Forms.Label label23;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.GroupBox groupBoxTimeFrame;
		private System.Windows.Forms.Label label22;
		private System.Windows.Forms.Label label24;
		private int selectedRow;
		private System.Windows.Forms.TextBox dateTimeToDate;
		private System.Windows.Forms.TextBox dateTimeFromDate;
		private Ufis.Snapshot.Win.UfisDataGrid dataGridInput;
		private Ufis.Snapshot.Win.UfisDataGrid dataGridAODB;
		private System.Windows.Forms.Button buttonCharts;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public CompareFunctionViewer()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(CompareFunctionViewer));
			this.tabImport = new AxTABLib.AxTAB();
			this.tabAODB = new AxTABLib.AxTAB();
			this.buttonDetails = new System.Windows.Forms.Button();
			this.buttonClose = new System.Windows.Forms.Button();
			this.statusBar1 = new System.Windows.Forms.StatusBar();
			this.label23 = new System.Windows.Forms.Label();
			this.label1 = new System.Windows.Forms.Label();
			this.groupBoxTimeFrame = new System.Windows.Forms.GroupBox();
			this.dateTimeToDate = new System.Windows.Forms.TextBox();
			this.dateTimeFromDate = new System.Windows.Forms.TextBox();
			this.label22 = new System.Windows.Forms.Label();
			this.label24 = new System.Windows.Forms.Label();
			this.dataGridInput = new Ufis.Snapshot.Win.UfisDataGrid();
			this.dataGridAODB = new Ufis.Snapshot.Win.UfisDataGrid();
			this.buttonCharts = new System.Windows.Forms.Button();
			((System.ComponentModel.ISupportInitialize)(this.tabImport)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.tabAODB)).BeginInit();
			this.groupBoxTimeFrame.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.dataGridInput)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.dataGridAODB)).BeginInit();
			this.SuspendLayout();
			// 
			// tabImport
			// 
			this.tabImport.Location = new System.Drawing.Point(2, 90);
			this.tabImport.Name = "tabImport";
			this.tabImport.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("tabImport.OcxState")));
			this.tabImport.Size = new System.Drawing.Size(564, 312);
			this.tabImport.TabIndex = 1;
			this.tabImport.OnVScroll += new AxTABLib._DTABEvents_OnVScrollEventHandler(this.tabImport_OnVScroll);
			this.tabImport.OnHScroll += new AxTABLib._DTABEvents_OnHScrollEventHandler(this.tabImport_OnHScroll);
			// 
			// tabAODB
			// 
			this.tabAODB.Location = new System.Drawing.Point(572, 90);
			this.tabAODB.Name = "tabAODB";
			this.tabAODB.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("tabAODB.OcxState")));
			this.tabAODB.Size = new System.Drawing.Size(568, 312);
			this.tabAODB.TabIndex = 1;
			this.tabAODB.OnVScroll += new AxTABLib._DTABEvents_OnVScrollEventHandler(this.tabAODB_OnVScroll);
			this.tabAODB.OnHScroll += new AxTABLib._DTABEvents_OnHScrollEventHandler(this.tabAODB_OnHScroll);
			// 
			// buttonDetails
			// 
			this.buttonDetails.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.buttonDetails.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.buttonDetails.Location = new System.Drawing.Point(2, 2);
			this.buttonDetails.Name = "buttonDetails";
			this.buttonDetails.Size = new System.Drawing.Size(65, 23);
			this.buttonDetails.TabIndex = 5;
			this.buttonDetails.Text = "Details";
			this.buttonDetails.Click += new System.EventHandler(this.buttonDetails_Click);
			// 
			// buttonClose
			// 
			this.buttonClose.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.buttonClose.Location = new System.Drawing.Point(138, 2);
			this.buttonClose.Name = "buttonClose";
			this.buttonClose.Size = new System.Drawing.Size(65, 23);
			this.buttonClose.TabIndex = 9;
			this.buttonClose.Text = "Close";
			this.buttonClose.Click += new System.EventHandler(this.buttonClose_Click);
			// 
			// statusBar1
			// 
			this.statusBar1.Location = new System.Drawing.Point(0, 815);
			this.statusBar1.Name = "statusBar1";
			this.statusBar1.Size = new System.Drawing.Size(1144, 22);
			this.statusBar1.TabIndex = 30;
			// 
			// label23
			// 
			this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label23.Location = new System.Drawing.Point(8, 72);
			this.label23.Name = "label23";
			this.label23.Size = new System.Drawing.Size(156, 16);
			this.label23.TabIndex = 112;
			this.label23.Text = "Import Flights";
			// 
			// label1
			// 
			this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label1.Location = new System.Drawing.Point(576, 72);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(156, 16);
			this.label1.TabIndex = 113;
			this.label1.Text = "AODB Flights";
			// 
			// groupBoxTimeFrame
			// 
			this.groupBoxTimeFrame.Controls.Add(this.dateTimeToDate);
			this.groupBoxTimeFrame.Controls.Add(this.dateTimeFromDate);
			this.groupBoxTimeFrame.Controls.Add(this.label22);
			this.groupBoxTimeFrame.Controls.Add(this.label24);
			this.groupBoxTimeFrame.Location = new System.Drawing.Point(962, 8);
			this.groupBoxTimeFrame.Name = "groupBoxTimeFrame";
			this.groupBoxTimeFrame.Size = new System.Drawing.Size(178, 68);
			this.groupBoxTimeFrame.TabIndex = 114;
			this.groupBoxTimeFrame.TabStop = false;
			this.groupBoxTimeFrame.Text = "Loaded Timeframe";
			// 
			// dateTimeToDate
			// 
			this.dateTimeToDate.Location = new System.Drawing.Point(52, 40);
			this.dateTimeToDate.Name = "dateTimeToDate";
			this.dateTimeToDate.ReadOnly = true;
			this.dateTimeToDate.Size = new System.Drawing.Size(116, 20);
			this.dateTimeToDate.TabIndex = 107;
			this.dateTimeToDate.Text = "";
			// 
			// dateTimeFromDate
			// 
			this.dateTimeFromDate.Location = new System.Drawing.Point(52, 16);
			this.dateTimeFromDate.Name = "dateTimeFromDate";
			this.dateTimeFromDate.ReadOnly = true;
			this.dateTimeFromDate.Size = new System.Drawing.Size(116, 20);
			this.dateTimeFromDate.TabIndex = 106;
			this.dateTimeFromDate.Text = "";
			// 
			// label22
			// 
			this.label22.Location = new System.Drawing.Point(16, 36);
			this.label22.Name = "label22";
			this.label22.Size = new System.Drawing.Size(30, 23);
			this.label22.TabIndex = 6;
			this.label22.Text = "End:";
			this.label22.TextAlign = System.Drawing.ContentAlignment.BottomRight;
			// 
			// label24
			// 
			this.label24.Location = new System.Drawing.Point(12, 12);
			this.label24.Name = "label24";
			this.label24.Size = new System.Drawing.Size(36, 23);
			this.label24.TabIndex = 5;
			this.label24.Text = "Begin:";
			this.label24.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
			// 
			// dataGridInput
			// 
			this.dataGridInput.DataMember = "";
			this.dataGridInput.HeaderForeColor = System.Drawing.SystemColors.ControlText;
			this.dataGridInput.Location = new System.Drawing.Point(2, 90);
			this.dataGridInput.Name = "dataGridInput";
			this.dataGridInput.Size = new System.Drawing.Size(566, 724);
			this.dataGridInput.TabIndex = 115;
			this.dataGridInput.Click += new System.EventHandler(this.dataGridInput_Click);
			this.dataGridInput.Scroll += new System.EventHandler(this.dataGridInput_Scroll);
			this.dataGridInput.CurrentCellChanged +=new EventHandler(dataGridInput_CurrentCellChanged);
			// 
			// dataGridAODB
			// 
			this.dataGridAODB.DataMember = "";
			this.dataGridAODB.HeaderForeColor = System.Drawing.SystemColors.ControlText;
			this.dataGridAODB.Location = new System.Drawing.Point(572, 90);
			this.dataGridAODB.Name = "dataGridAODB";
			this.dataGridAODB.Size = new System.Drawing.Size(570, 724);
			this.dataGridAODB.TabIndex = 116;
			this.dataGridAODB.Click += new System.EventHandler(this.dataGridAODB_Click);
			this.dataGridAODB.Scroll += new System.EventHandler(this.dataGridAODB_Scroll);
			this.dataGridAODB.CurrentCellChanged +=new EventHandler(dataGridAODB_CurrentCellChanged);
			// 
			// buttonCharts
			// 
			this.buttonCharts.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.buttonCharts.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.buttonCharts.Location = new System.Drawing.Point(70, 2);
			this.buttonCharts.Name = "buttonCharts";
			this.buttonCharts.Size = new System.Drawing.Size(65, 23);
			this.buttonCharts.TabIndex = 117;
			this.buttonCharts.Text = "Charts";
			this.buttonCharts.Click += new System.EventHandler(this.buttonCharts_Click);
			// 
			// CompareFunctionViewer
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(1144, 837);
			this.Controls.Add(this.buttonCharts);
			this.Controls.Add(this.dataGridAODB);
			this.Controls.Add(this.dataGridInput);
			this.Controls.Add(this.groupBoxTimeFrame);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.label23);
			this.Controls.Add(this.statusBar1);
			this.Controls.Add(this.buttonClose);
			this.Controls.Add(this.buttonDetails);
			this.Controls.Add(this.tabImport);
			this.Controls.Add(this.tabAODB);
			this.Name = "CompareFunctionViewer";
			this.Text = "CompareFunctionViewer";
			this.Load += new System.EventHandler(this.CompareFunctionViewer_Load);
			((System.ComponentModel.ISupportInitialize)(this.tabImport)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.tabAODB)).EndInit();
			this.groupBoxTimeFrame.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.dataGridInput)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.dataGridAODB)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion

		private void CompareFunctionViewer_Load(object sender, System.EventArgs e)
		{

			this.tabImport.ResetContent();
			this.tabAODB.ResetContent();

			ProgressWindow progress = new ProgressWindow();
			progress.Text = "Work";
			progress.Show(); 
			MyController.LoadAODB(progress);
			MyController.Compare("Flight", out this.dataTableImport, out this.dataTableAODB);
			progress.Dispose();
		
			FillImportTab();
			FillAODBTab();

			PrepareImport();
			PrepareAODB();

			this.dateTimeFromDate.Text = MyController.LoadFrom.ToString();
			this.dateTimeToDate.Text = MyController.LoadTo.ToString();

			this.statusBar1.Text = "Left: " + this.dataTableImport.Rows.Count.ToString() + " Right: " + this.dataTableAODB.Rows.Count.ToString();
		
			this.dataSetAODB = MyController.AODBDataSet;
			this.dataSetImport = MyController.InputDataSet;
		}

		internal void PrepareImport()
		{
			//ImportFlights
			DataTable dt = new DataTable();
			MyController.ConvertDataTablesDateTimes(this.dataTableImport, out dt);
			DataView dataViewImportFlights = new DataView(dt,"","",DataViewRowState.CurrentRows);

			
			DataGridTableStyle tableStyle = new DataGridTableStyle();
			tableStyle.MappingName = "Flight";

			int numCols = dataViewImportFlights.Table.Columns.Count;
			DataGridEnableTextBoxColumn aColumnTextColumn ;
			for(int i = 0; i < numCols; ++i)
			{
				aColumnTextColumn = new DataGridEnableTextBoxColumn(i);
				aColumnTextColumn.HeaderText = this.dataTableImport.Columns[i].ColumnName;
				aColumnTextColumn.MappingName = this.dataTableImport.Columns[i].ColumnName;
				aColumnTextColumn.CheckCellEnabled += new EnableCellEventHandler(SetEnableValuesImport);
				tableStyle.GridColumnStyles.Add(aColumnTextColumn);
			}

			this.dataGridInput.TableStyles.Clear();
			this.dataGridInput.TableStyles.Add(tableStyle);
			dataViewImportFlights.AllowNew = false;
			this.dataGridInput.DataSource = dataViewImportFlights;

		}



		internal void FillImportTab()
		{
			string header = "";
			string lenstring = "";

			DataTable dt = new DataTable();
			MyController.ConvertDataTablesDateTimes(this.dataTableImport, out dt);

			for(int i = 0; i < dt.Columns.Count; i++)
			{
				if( i+1 < dt.Columns.Count)
				{
					header += dt.Columns[i].Caption + ",";
					lenstring += "120,";
				}
				else
				{
					header += dt.Columns[i].Caption;
					lenstring += "120";
				}
			}
			tabImport.HeaderString = header;
			tabImport.LogicalFieldList = header;
			tabImport.HeaderLengthString = lenstring;
			tabImport.LineHeight = 18;

			

			for(int i = 0; i < dt.Rows.Count; i++)
			{
				string str="";
				for(int j = 0; j < dt.Columns.Count; j++)
				{
					switch (dt.Columns[j].DataType.FullName)
					{
						case "System.Int32":
							if( j < dt.Columns.Count)
							{
								if (str.Length > 0)
									str += ',';
								str += ((int)dt.Rows[i][dt.Columns[j]]).ToString();
							}
							break;
						case "System.String":
							if( j < dt.Columns.Count)
							{
								if (str.Length > 0)
									str += ',';
								str += dt.Rows[i][dt.Columns[j]];
							}
							break;
						case "System.DateTime":
							if( j < dt.Columns.Count)
							{
								if (str.Length > 0)
									str += ',';
								str += dt.Rows[i][dt.Columns[j]];
							}
							break;
						default:
							if (dt.Rows[i][dt.Columns[j]] != System.DBNull.Value ) 
							{
								int valu = (int)dt.Rows[i][dt.Columns[j]];
								if (str.Length > 0)
									str += ',';
								str += Enum.GetName(dt.Columns[j].DataType,valu);
							}
							else
							{
								if (str.Length > 0)
									str += ',';

								str += " ";
							}
							break;
					}
				}
				
				tabImport.InsertTextLine(str, false);
				int backColor = UT.colLightGreen;
				if(dt.Rows[i].HasErrors)
				{
					switch(dt.Rows[i].RowError.Substring(0,1))
					{
						case "M":	
							backColor = UT.colRed;
							break;
						case "E":
							backColor = UT.colGray;
							break;
						case "S":	
							backColor = UT.colLightYellow;
							break;
						default:
							
							break;
					}
					this.tabImport.SetLineColor(i,UT.colBlack,backColor);
				}
			}
			this.tabImport.ShowHorzScroller(true);
			this.tabImport.ShowVertScroller(true);
			tabImport.Refresh();
		}


		internal void FillAODBTab()
		{
			string header = "";
			string lenstring = "";

			DataTable dt = new DataTable();
			MyController.ConvertDataTablesDateTimes(this.dataTableAODB, out dt);

			for(int i = 0; i < dt.Columns.Count; i++)
			{
				if( i+1 < dt.Columns.Count)
				{
					header += dt.Columns[i].Caption + ",";
					lenstring += "120,";
				}
				else
				{
					header += dt.Columns[i].Caption;
					lenstring += "120";
				}
			}
			tabAODB.HeaderString = header;
			tabAODB.LogicalFieldList = header;
			tabAODB.HeaderLengthString = lenstring;
			tabAODB.LineHeight = 18;
			

			for(int i = 0; i < dt.Rows.Count; i++)
			{
				string str="";
				for(int j = 0; j < dt.Columns.Count; j++)
				{
					switch (dt.Columns[j].DataType.FullName)
					{
						case "System.Int32":
							if( j < dt.Columns.Count)
							{
								if (str.Length > 0)
									str += ',';
								str += ((int)dt.Rows[i][dt.Columns[j]]).ToString();
							}
							break;
						case "System.String":
							if( j < dt.Columns.Count)
							{
								if (str.Length > 0)
									str += ',';
								str += dt.Rows[i][dt.Columns[j]];
							}
							break;
						case "System.DateTime":
							if( j < dt.Columns.Count)
							{
								if (str.Length > 0)
									str += ',';
								str += dt.Rows[i][dt.Columns[j]];
							}
							break;
						default:
							if (dt.Rows[i][dt.Columns[j]] != System.DBNull.Value ) 
							{
								int valu = (int)dt.Rows[i][dt.Columns[j]];
								if (str.Length > 0)
									str += ',';
								str += Enum.GetName(dt.Columns[j].DataType,valu);
							}
							else
							{
								if (str.Length > 0)
									str += ',';
							}
							break;
					}
				}
				tabAODB.InsertTextLine(str, false);
				int backColor = UT.colGray;
				if(dt.Rows[i].HasErrors)
				{
					
					switch(dt.Rows[i].RowError.Substring(0,1))
					{
						case "M":	
							backColor = UT.colRed;
							break;
						case "E":
							backColor = UT.colGray;
							break;
						case "S":	
							backColor = UT.colLightYellow;
							break;
						default:
							break;
					}
					this.tabAODB.SetLineColor(i,UT.colBlack,backColor);
				}
			}
			this.tabAODB.ShowHorzScroller(true);
			this.tabAODB.ShowVertScroller(true);
			tabAODB.Refresh();
		}

		internal void PrepareAODB()
		{
			//AODB Flights
			DataTable dt = new DataTable();
			MyController.ConvertDataTablesDateTimes(this.dataTableAODB, out dt);
			DataView dataViewAODBFlights = new DataView(dt,"","",DataViewRowState.CurrentRows);
	
		
	

			DataGridTableStyle tableStyle = new DataGridTableStyle();
			tableStyle.MappingName = "Flight";

			int numCols = dataViewAODBFlights.Table.Columns.Count;
			DataGridEnableTextBoxColumn aColumnTextColumn ;
			for(int i = 0; i < numCols; ++i)
			{
				aColumnTextColumn = new DataGridEnableTextBoxColumn(i);
				aColumnTextColumn.HeaderText = this.dataTableAODB.Columns[i].ColumnName;
				aColumnTextColumn.MappingName = this.dataTableAODB.Columns[i].ColumnName;
				aColumnTextColumn.CheckCellEnabled += new EnableCellEventHandler(SetEnableValuesAODB);
				tableStyle.GridColumnStyles.Add(aColumnTextColumn);
			}

			this.dataGridAODB.TableStyles.Clear();
			this.dataGridAODB.TableStyles.Add(tableStyle);
			dataViewAODBFlights.AllowNew = false;
			this.dataGridAODB.DataSource = dataViewAODBFlights;
			

		}
		
		public void SetEnableValuesImport(object sender, DataGridEnableEventArgs e)
		{
			e.EnableValue = false;

			if(this.dataTableImport.Rows[e.Row].HasErrors)
			{
				switch(this.dataTableImport.Rows[e.Row].RowError.Substring(0,1))
				{
					case "M":	
						e.Color = "Red";
						break;
					case "E":
						e.Color = "Grey";
						break;
					case "S":	
						e.Color = "Yellow";
						break;
					default:
						break;
				}
			}
			else
				e.Color = "Green";
		}

		public void SetEnableValuesAODB(object sender, DataGridEnableEventArgs e)
		{
			e.EnableValue = false;

			if(this.dataTableAODB.Rows[e.Row].HasErrors)
			{
				switch(this.dataTableAODB.Rows[e.Row].RowError.Substring(0,1))
				{
					case "M":	
						e.Color = "Red";
						break;
					case "E":
						e.Color = "Grey";
						break;
					case "S":	
						e.Color = "Yellow";
						break;
					default:
						break;
				}
			}
			else
				e.Color = "Grey";
		}



		private void buttonClose_Click(object sender, System.EventArgs e)
		{
			this.Dispose();
		}

		private Controller MyController
		{
			get
			{
				return (Controller)base.Controller;
			}
		}



		private void buttonDetails_Click(object sender, System.EventArgs e)
		{
			if(MyController.CurrentRotation != null)
			{
				MyController.SelectService("RecordDetail");	
				if(this.dataSetImport.HasChanges())
				{
					ProgressWindow progress = new ProgressWindow();
					progress.Text = "Work";
					progress.Show(); 
					MyController.LoadAODB(progress);
					MyController.Compare("Flight", out this.dataTableImport, out this.dataTableAODB);
					progress.ShowDialog();
					progress.Dispose();

					PrepareImport();
					PrepareAODB();

					this.dateTimeFromDate.Text = MyController.LoadFrom.ToString();
					this.dateTimeToDate.Text = MyController.LoadTo.ToString();

					this.statusBar1.Text = "Left: " + this.dataTableImport.Rows.Count.ToString() + " Right: " + this.dataTableAODB.Rows.Count.ToString();
		
					this.dataSetAODB = MyController.AODBDataSet;
					this.dataSetImport = MyController.InputDataSet;
				}
			} else
				MessageBox.Show("No row selected");
		}

		private void buttonShrink_Click(object sender, System.EventArgs e)
		{
		
		}

		private void buttonActivate_Click(object sender, System.EventArgs e)
		{
		
		}

		private void dataGridAODB_CurrentCellChanged(object sender, EventArgs e)
		{
			this.dataGridAODB.Select(this.dataGridInput.CurrentCell.RowNumber); 	

			SelectedRow = this.dataGridAODB.CurrentCell.RowNumber;

			//determine selected row in datatable "rotation"
			int rowId = this.dataTableAODB.Columns["FlightId"].Ordinal;
			DataGridCell dgc = new DataGridCell(this.dataGridAODB.CurrentCell.RowNumber,rowId);
			string selectedId = this.dataGridInput[dgc].ToString();
			if(selectedId.Length > 0)
				selectedId = "FlightId = " + selectedId;
			else
				selectedId = "";
			DataView dv = new DataView(this.dataSetAODB.Tables["Flight"], selectedId, "", DataViewRowState.CurrentRows);
			
			if(dv.Count == 1)
				MyController.CurrentRotation = dv[0].Row;	
			else
				MyController.CurrentRotation = null;
		}

		private void dataGridInput_CurrentCellChanged(object sender, EventArgs e)
		{

			this.dataGridInput.Select(this.dataGridInput.CurrentCell.RowNumber); 	

			SelectedRow = this.dataGridInput.CurrentCell.RowNumber;

			//determine selected row in datatable "rotation"
			int rowId = this.dataTableImport.Columns["FlightId"].Ordinal;
			DataGridCell dgc = new DataGridCell(this.dataGridInput.CurrentCell.RowNumber,rowId);
			string selectedId = this.dataGridInput[dgc].ToString();
			if(selectedId.Length > 0)
				selectedId = "FlightId = " + selectedId;
			else
				selectedId = "";
			DataView dv = new DataView(this.dataSetImport.Tables["Flight"], selectedId, "", DataViewRowState.CurrentRows);
			
			if(dv.Count == 1)
				MyController.CurrentRotation = dv[0].Row;	
			else
				MyController.CurrentRotation = null;
		}

		public int SelectedRow
		{
			get
			{
				return this.selectedRow;
			}
			set
			{
				this.selectedRow = value;
			}
		}

		private void dataGridInput_Click(object sender, EventArgs e)
		{
			System.Drawing.Point pt = dataGridInput.PointToClient(Cursor.Position);

			DataGrid.HitTestInfo hti = dataGridInput.HitTest(pt);

			if(hti.Type == DataGrid.HitTestType.ColumnHeader)
			{
				this.dataGridAODB.DataBindings.Clear();
				DataView dv = new DataView(this.dataTableAODB, "", this.dataTableAODB.Columns[hti.Column].ColumnName.ToString() + " ASC",DataViewRowState.CurrentRows);
				this.dataGridAODB.DataSource = dv;
			}	
		}

		private void dataGridAODB_Click(object sender, EventArgs e)
		{
			System.Drawing.Point pt = dataGridAODB.PointToClient(Cursor.Position);

			DataGrid.HitTestInfo hti = dataGridInput.HitTest(pt);

			if(hti.Type == DataGrid.HitTestType.ColumnHeader)
			{
				this.dataGridInput.DataBindings.Clear();
				DataView dv = new DataView(this.dataTableImport, "", this.dataTableImport.Columns[hti.Column].ColumnName.ToString() + " ASC",DataViewRowState.CurrentRows);
				this.dataGridInput.DataSource = dv;
			}	
		}

		private void dataGridInput_Scroll(object sender, EventArgs e)
		{
			if(this.dataGridInput.HScrollBar.Value <= this.dataGridAODB.HScrollBar.Maximum && this.dataGridInput.HScrollBar.Value >= this.dataGridAODB.HScrollBar.Minimum)
			{
				this.dataGridAODB.HScrollBar.Value = this.dataGridInput.HScrollBar.Value;
				this.dataGridAODB.ScrollToColumn(this.dataGridInput.HScrollBar.Value);
			}
			if(this.dataGridInput.VScrollBar.Value <= this.dataGridAODB.VScrollBar.Maximum && this.dataGridInput.VScrollBar.Value >= this.dataGridAODB.VScrollBar.Minimum)
			{
				this.dataGridAODB.VScrollBar.Value = this.dataGridInput.VScrollBar.Value;
				this.dataGridAODB.ScrollToRow(this.dataGridInput.VScrollBar.Value);
			}
			
		}

		private void dataGridAODB_Scroll(object sender, EventArgs e)
		{
			if(this.dataGridAODB.HScrollBar.Value <= this.dataGridInput.HScrollBar.Maximum && this.dataGridAODB.HScrollBar.Value >= this.dataGridInput.HScrollBar.Minimum)
			{
				this.dataGridInput.HScrollBar.Value = this.dataGridAODB.HScrollBar.Value;
				this.dataGridInput.ScrollToColumn(this.dataGridAODB.HScrollBar.Value);
			}
			if(this.dataGridAODB.VScrollBar.Value <= this.dataGridInput.VScrollBar.Maximum && this.dataGridAODB.VScrollBar.Value >= this.dataGridInput.VScrollBar.Minimum)
			{
				this.dataGridInput.VScrollBar.Value = this.dataGridAODB.VScrollBar.Value;
				this.dataGridInput.ScrollToRow(this.dataGridAODB.VScrollBar.Value);
			}
		}

		private void tabAODB_OnHScroll(object sender, AxTABLib._DTABEvents_OnHScrollEvent e)
		{
			if(this.tabAODB.Focused)
				this.tabImport.OnHScrollTo(e.colNo);
		}

		private void tabAODB_OnVScroll(object sender, AxTABLib._DTABEvents_OnVScrollEvent e)
		{
			if(this.tabAODB.Focused)
				this.tabImport.OnVScrollTo(e.lineNo);
		}

		private void tabImport_OnHScroll(object sender, AxTABLib._DTABEvents_OnHScrollEvent e)
		{
			if(this.tabImport.Focused)
				this.tabAODB.OnHScrollTo(e.colNo);
		}

		private void tabImport_OnVScroll(object sender, AxTABLib._DTABEvents_OnVScrollEvent e)
		{
			if(this.tabImport.Focused)
				this.tabAODB.OnVScrollTo(e.lineNo);
		}

		private void buttonCharts_Click(object sender, System.EventArgs e)
		{
			MyController.SelectService("GanttChartLayout");
		}
	}
}
