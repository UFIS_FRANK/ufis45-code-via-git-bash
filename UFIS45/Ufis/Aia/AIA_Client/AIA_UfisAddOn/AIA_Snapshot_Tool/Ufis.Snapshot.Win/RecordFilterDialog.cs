using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using System.Text;
using Ufis.BL.Common;


namespace Ufis.Snapshot.Win
{
	/// <summary>
	/// Summary description for RecordFilterDialog.
	/// </summary>
	public class RecordFilterDialog : Microsoft.ApplicationBlocks.UIProcess.WindowsFormView
	{
		private System.Windows.Forms.Button buttonLoad;
		private System.Windows.Forms.Button buttonClose;
		private System.Windows.Forms.Button buttonAll;
		private System.Windows.Forms.Button buttonNone;
		private System.Windows.Forms.ComboBox comboBox1;
		private System.Windows.Forms.ListBox selection1;
		private System.Windows.Forms.ListBox result1;
		private System.Windows.Forms.TextBox cntSel1;
		private System.Windows.Forms.TextBox cntRsl1;
		private System.Windows.Forms.StatusBar statusBar1;
		private System.Windows.Forms.TextBox cntRsl2;
		private System.Windows.Forms.TextBox cntSel2;
		private System.Windows.Forms.ListBox result2;
		private System.Windows.Forms.ComboBox comboBox2;
		private System.Windows.Forms.ListBox selection2;
		private System.Windows.Forms.TextBox cntRsl3;
		private System.Windows.Forms.TextBox cntSel3;
		private System.Windows.Forms.ListBox result3;
		private System.Windows.Forms.ComboBox comboBox3;
		private System.Windows.Forms.ListBox selection3;
		private System.Windows.Forms.ComboBox comboBox4;
		private System.Windows.Forms.TextBox cntRsl4;
		private System.Windows.Forms.TextBox cntSel4;
		private System.Windows.Forms.ListBox result4;
		private System.Windows.Forms.ListBox selection4;
		private System.Windows.Forms.ComboBox comboBox5;
		private System.Windows.Forms.TextBox cntRsl5;
		private System.Windows.Forms.TextBox cntSel5;
		private System.Windows.Forms.ListBox result5;
		private System.Windows.Forms.ListBox selection5;
		private System.Data.DataSet dataSetFlightRecords;
		private System.Collections.ArrayList resultList1;
		private System.Collections.ArrayList resultList2;
		private System.Collections.ArrayList resultList3;
		private System.Collections.ArrayList resultList4;
		private System.Collections.ArrayList resultList5;
		private System.Windows.Forms.GroupBox groupBoxTimeFrame;
		private System.Windows.Forms.Label label22;
		private System.Windows.Forms.Label label24;
		private System.Windows.Forms.DateTimePicker dateTimePickerFromDate;
		private System.Windows.Forms.DateTimePicker dateTimePickerToDate;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.CheckBox checkBoxMonday;
		private System.Windows.Forms.CheckBox checkBoxTuesday;
		private System.Windows.Forms.CheckBox checkBoxWednesday;
		private System.Windows.Forms.CheckBox checkBoxSunday;
		private System.Windows.Forms.CheckBox checkBoxThursday;
		private System.Windows.Forms.CheckBox checkBoxFriday;
		private System.Windows.Forms.CheckBox checkBoxSaturday;
		private ArrayList arrayListDays;
		private String where;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public RecordFilterDialog()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		private Controller MyController
		{
			get
			{
				return (Controller)base.Controller;
			}
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.buttonLoad = new System.Windows.Forms.Button();
			this.buttonClose = new System.Windows.Forms.Button();
			this.buttonAll = new System.Windows.Forms.Button();
			this.buttonNone = new System.Windows.Forms.Button();
			this.selection1 = new System.Windows.Forms.ListBox();
			this.comboBox1 = new System.Windows.Forms.ComboBox();
			this.result1 = new System.Windows.Forms.ListBox();
			this.cntSel1 = new System.Windows.Forms.TextBox();
			this.cntRsl1 = new System.Windows.Forms.TextBox();
			this.statusBar1 = new System.Windows.Forms.StatusBar();
			this.cntRsl2 = new System.Windows.Forms.TextBox();
			this.cntSel2 = new System.Windows.Forms.TextBox();
			this.result2 = new System.Windows.Forms.ListBox();
			this.comboBox2 = new System.Windows.Forms.ComboBox();
			this.selection2 = new System.Windows.Forms.ListBox();
			this.cntRsl3 = new System.Windows.Forms.TextBox();
			this.cntSel3 = new System.Windows.Forms.TextBox();
			this.result3 = new System.Windows.Forms.ListBox();
			this.comboBox3 = new System.Windows.Forms.ComboBox();
			this.selection3 = new System.Windows.Forms.ListBox();
			this.cntRsl4 = new System.Windows.Forms.TextBox();
			this.cntSel4 = new System.Windows.Forms.TextBox();
			this.result4 = new System.Windows.Forms.ListBox();
			this.comboBox4 = new System.Windows.Forms.ComboBox();
			this.selection4 = new System.Windows.Forms.ListBox();
			this.cntRsl5 = new System.Windows.Forms.TextBox();
			this.cntSel5 = new System.Windows.Forms.TextBox();
			this.result5 = new System.Windows.Forms.ListBox();
			this.comboBox5 = new System.Windows.Forms.ComboBox();
			this.selection5 = new System.Windows.Forms.ListBox();
			this.checkBoxMonday = new System.Windows.Forms.CheckBox();
			this.checkBoxTuesday = new System.Windows.Forms.CheckBox();
			this.checkBoxWednesday = new System.Windows.Forms.CheckBox();
			this.checkBoxSunday = new System.Windows.Forms.CheckBox();
			this.checkBoxThursday = new System.Windows.Forms.CheckBox();
			this.checkBoxFriday = new System.Windows.Forms.CheckBox();
			this.checkBoxSaturday = new System.Windows.Forms.CheckBox();
			this.groupBoxTimeFrame = new System.Windows.Forms.GroupBox();
			this.label22 = new System.Windows.Forms.Label();
			this.label24 = new System.Windows.Forms.Label();
			this.dateTimePickerFromDate = new System.Windows.Forms.DateTimePicker();
			this.dateTimePickerToDate = new System.Windows.Forms.DateTimePicker();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.label1 = new System.Windows.Forms.Label();
			this.groupBoxTimeFrame.SuspendLayout();
			this.groupBox1.SuspendLayout();
			this.SuspendLayout();
			// 
			// buttonLoad
			// 
			this.buttonLoad.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.buttonLoad.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.buttonLoad.Location = new System.Drawing.Point(2, 2);
			this.buttonLoad.Name = "buttonLoad";
			this.buttonLoad.Size = new System.Drawing.Size(65, 23);
			this.buttonLoad.TabIndex = 1;
			this.buttonLoad.Text = "Load";
			this.buttonLoad.Click += new System.EventHandler(this.buttonLoad_Click);
			// 
			// buttonClose
			// 
			this.buttonClose.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.buttonClose.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.buttonClose.Location = new System.Drawing.Point(138, 2);
			this.buttonClose.Name = "buttonClose";
			this.buttonClose.Size = new System.Drawing.Size(65, 23);
			this.buttonClose.TabIndex = 5;
			this.buttonClose.Text = "Close";
			this.buttonClose.Click += new System.EventHandler(this.buttonClose_Click);
			// 
			// buttonAll
			// 
			this.buttonAll.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.buttonAll.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.buttonAll.Location = new System.Drawing.Point(184, 16);
			this.buttonAll.Name = "buttonAll";
			this.buttonAll.Size = new System.Drawing.Size(65, 23);
			this.buttonAll.TabIndex = 6;
			this.buttonAll.Text = "All";
			this.buttonAll.Click += new System.EventHandler(this.buttonAll_Click);
			// 
			// buttonNone
			// 
			this.buttonNone.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.buttonNone.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.buttonNone.Location = new System.Drawing.Point(184, 40);
			this.buttonNone.Name = "buttonNone";
			this.buttonNone.Size = new System.Drawing.Size(65, 23);
			this.buttonNone.TabIndex = 7;
			this.buttonNone.Text = "None";
			this.buttonNone.Click += new System.EventHandler(this.buttonNone_Click);
			// 
			// selection1
			// 
			this.selection1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.selection1.Location = new System.Drawing.Point(2, 112);
			this.selection1.Name = "selection1";
			this.selection1.Size = new System.Drawing.Size(80, 212);
			this.selection1.TabIndex = 21;
			this.selection1.DoubleClick += new System.EventHandler(this.selection1_DoubleClick);
			// 
			// comboBox1
			// 
			this.comboBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.comboBox1.Location = new System.Drawing.Point(2, 88);
			this.comboBox1.Name = "comboBox1";
			this.comboBox1.Size = new System.Drawing.Size(162, 21);
			this.comboBox1.TabIndex = 22;
			this.comboBox1.SelectedValueChanged += new System.EventHandler(this.comboBox1_SelectedValueChanged);
			// 
			// result1
			// 
			this.result1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.result1.Location = new System.Drawing.Point(82, 112);
			this.result1.Name = "result1";
			this.result1.Size = new System.Drawing.Size(80, 212);
			this.result1.TabIndex = 23;
			// 
			// cntSel1
			// 
			this.cntSel1.Enabled = false;
			this.cntSel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.cntSel1.Location = new System.Drawing.Point(2, 328);
			this.cntSel1.Name = "cntSel1";
			this.cntSel1.Size = new System.Drawing.Size(80, 20);
			this.cntSel1.TabIndex = 24;
			this.cntSel1.Text = "";
			this.cntSel1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			// 
			// cntRsl1
			// 
			this.cntRsl1.Enabled = false;
			this.cntRsl1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.cntRsl1.Location = new System.Drawing.Point(82, 328);
			this.cntRsl1.Name = "cntRsl1";
			this.cntRsl1.Size = new System.Drawing.Size(80, 20);
			this.cntRsl1.TabIndex = 25;
			this.cntRsl1.Text = "";
			this.cntRsl1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			// 
			// statusBar1
			// 
			this.statusBar1.Location = new System.Drawing.Point(0, 351);
			this.statusBar1.Name = "statusBar1";
			this.statusBar1.Size = new System.Drawing.Size(860, 22);
			this.statusBar1.TabIndex = 26;
			this.statusBar1.Text = "statusBar1";
			// 
			// cntRsl2
			// 
			this.cntRsl2.Enabled = false;
			this.cntRsl2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.cntRsl2.Location = new System.Drawing.Point(256, 328);
			this.cntRsl2.Name = "cntRsl2";
			this.cntRsl2.Size = new System.Drawing.Size(80, 20);
			this.cntRsl2.TabIndex = 31;
			this.cntRsl2.Text = "";
			this.cntRsl2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			// 
			// cntSel2
			// 
			this.cntSel2.Enabled = false;
			this.cntSel2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.cntSel2.Location = new System.Drawing.Point(176, 328);
			this.cntSel2.Name = "cntSel2";
			this.cntSel2.Size = new System.Drawing.Size(80, 20);
			this.cntSel2.TabIndex = 30;
			this.cntSel2.Text = "";
			this.cntSel2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			// 
			// result2
			// 
			this.result2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.result2.Location = new System.Drawing.Point(256, 112);
			this.result2.Name = "result2";
			this.result2.Size = new System.Drawing.Size(80, 212);
			this.result2.TabIndex = 29;
			// 
			// comboBox2
			// 
			this.comboBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.comboBox2.Location = new System.Drawing.Point(176, 88);
			this.comboBox2.Name = "comboBox2";
			this.comboBox2.Size = new System.Drawing.Size(162, 21);
			this.comboBox2.TabIndex = 28;
			this.comboBox2.SelectedValueChanged += new System.EventHandler(this.comboBox2_SelectedValueChanged);
			// 
			// selection2
			// 
			this.selection2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.selection2.Location = new System.Drawing.Point(176, 112);
			this.selection2.Name = "selection2";
			this.selection2.Size = new System.Drawing.Size(80, 212);
			this.selection2.TabIndex = 27;
			this.selection2.DoubleClick += new System.EventHandler(this.selection2_DoubleClick);
			// 
			// cntRsl3
			// 
			this.cntRsl3.Enabled = false;
			this.cntRsl3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.cntRsl3.Location = new System.Drawing.Point(430, 328);
			this.cntRsl3.Name = "cntRsl3";
			this.cntRsl3.Size = new System.Drawing.Size(80, 20);
			this.cntRsl3.TabIndex = 36;
			this.cntRsl3.Text = "";
			this.cntRsl3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			// 
			// cntSel3
			// 
			this.cntSel3.Enabled = false;
			this.cntSel3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.cntSel3.Location = new System.Drawing.Point(350, 328);
			this.cntSel3.Name = "cntSel3";
			this.cntSel3.Size = new System.Drawing.Size(80, 20);
			this.cntSel3.TabIndex = 35;
			this.cntSel3.Text = "";
			this.cntSel3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			// 
			// result3
			// 
			this.result3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.result3.Location = new System.Drawing.Point(430, 112);
			this.result3.Name = "result3";
			this.result3.Size = new System.Drawing.Size(80, 212);
			this.result3.TabIndex = 34;
			// 
			// comboBox3
			// 
			this.comboBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.comboBox3.Location = new System.Drawing.Point(350, 88);
			this.comboBox3.Name = "comboBox3";
			this.comboBox3.Size = new System.Drawing.Size(162, 21);
			this.comboBox3.TabIndex = 33;
			this.comboBox3.SelectedValueChanged += new System.EventHandler(this.comboBox3_SelectedValueChanged);
			// 
			// selection3
			// 
			this.selection3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.selection3.Location = new System.Drawing.Point(350, 112);
			this.selection3.Name = "selection3";
			this.selection3.Size = new System.Drawing.Size(80, 212);
			this.selection3.TabIndex = 32;
			this.selection3.DoubleClick += new System.EventHandler(this.selection3_DoubleClick);
			// 
			// cntRsl4
			// 
			this.cntRsl4.Enabled = false;
			this.cntRsl4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.cntRsl4.Location = new System.Drawing.Point(604, 328);
			this.cntRsl4.Name = "cntRsl4";
			this.cntRsl4.Size = new System.Drawing.Size(80, 20);
			this.cntRsl4.TabIndex = 41;
			this.cntRsl4.Text = "";
			this.cntRsl4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			// 
			// cntSel4
			// 
			this.cntSel4.Enabled = false;
			this.cntSel4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.cntSel4.Location = new System.Drawing.Point(524, 328);
			this.cntSel4.Name = "cntSel4";
			this.cntSel4.Size = new System.Drawing.Size(80, 20);
			this.cntSel4.TabIndex = 40;
			this.cntSel4.Text = "";
			this.cntSel4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			// 
			// result4
			// 
			this.result4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.result4.Location = new System.Drawing.Point(604, 112);
			this.result4.Name = "result4";
			this.result4.Size = new System.Drawing.Size(80, 212);
			this.result4.TabIndex = 39;
			// 
			// comboBox4
			// 
			this.comboBox4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.comboBox4.Location = new System.Drawing.Point(524, 88);
			this.comboBox4.Name = "comboBox4";
			this.comboBox4.Size = new System.Drawing.Size(162, 21);
			this.comboBox4.TabIndex = 38;
			this.comboBox4.SelectedValueChanged += new System.EventHandler(this.comboBox4_SelectedValueChanged);
			// 
			// selection4
			// 
			this.selection4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.selection4.Location = new System.Drawing.Point(524, 112);
			this.selection4.Name = "selection4";
			this.selection4.Size = new System.Drawing.Size(80, 212);
			this.selection4.TabIndex = 37;
			this.selection4.DoubleClick += new System.EventHandler(this.selection4_DoubleClick);
			// 
			// cntRsl5
			// 
			this.cntRsl5.Enabled = false;
			this.cntRsl5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.cntRsl5.Location = new System.Drawing.Point(778, 328);
			this.cntRsl5.Name = "cntRsl5";
			this.cntRsl5.Size = new System.Drawing.Size(80, 20);
			this.cntRsl5.TabIndex = 46;
			this.cntRsl5.Text = "";
			this.cntRsl5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			// 
			// cntSel5
			// 
			this.cntSel5.Enabled = false;
			this.cntSel5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.cntSel5.Location = new System.Drawing.Point(698, 328);
			this.cntSel5.Name = "cntSel5";
			this.cntSel5.Size = new System.Drawing.Size(80, 20);
			this.cntSel5.TabIndex = 45;
			this.cntSel5.Text = "";
			this.cntSel5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			// 
			// result5
			// 
			this.result5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.result5.Location = new System.Drawing.Point(778, 112);
			this.result5.Name = "result5";
			this.result5.Size = new System.Drawing.Size(80, 212);
			this.result5.TabIndex = 44;
			// 
			// comboBox5
			// 
			this.comboBox5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.comboBox5.Location = new System.Drawing.Point(698, 88);
			this.comboBox5.Name = "comboBox5";
			this.comboBox5.Size = new System.Drawing.Size(162, 21);
			this.comboBox5.TabIndex = 43;
			this.comboBox5.SelectedValueChanged += new System.EventHandler(this.comboBox5_SelectedValueChanged);
			// 
			// selection5
			// 
			this.selection5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.selection5.Location = new System.Drawing.Point(698, 112);
			this.selection5.Name = "selection5";
			this.selection5.Size = new System.Drawing.Size(80, 212);
			this.selection5.TabIndex = 42;
			this.selection5.DoubleClick += new System.EventHandler(this.selection5_DoubleClick);
			// 
			// checkBoxMonday
			// 
			this.checkBoxMonday.Checked = true;
			this.checkBoxMonday.CheckState = System.Windows.Forms.CheckState.Checked;
			this.checkBoxMonday.Location = new System.Drawing.Point(16, 40);
			this.checkBoxMonday.Name = "checkBoxMonday";
			this.checkBoxMonday.Size = new System.Drawing.Size(16, 16);
			this.checkBoxMonday.TabIndex = 49;
			this.checkBoxMonday.CheckedChanged += new System.EventHandler(this.checkBoxMonday_CheckedChanged);
			// 
			// checkBoxTuesday
			// 
			this.checkBoxTuesday.Checked = true;
			this.checkBoxTuesday.CheckState = System.Windows.Forms.CheckState.Checked;
			this.checkBoxTuesday.Location = new System.Drawing.Point(40, 40);
			this.checkBoxTuesday.Name = "checkBoxTuesday";
			this.checkBoxTuesday.Size = new System.Drawing.Size(16, 16);
			this.checkBoxTuesday.TabIndex = 50;
			this.checkBoxTuesday.CheckedChanged += new System.EventHandler(this.checkBoxTuesday_CheckedChanged);
			// 
			// checkBoxWednesday
			// 
			this.checkBoxWednesday.Checked = true;
			this.checkBoxWednesday.CheckState = System.Windows.Forms.CheckState.Checked;
			this.checkBoxWednesday.Location = new System.Drawing.Point(64, 40);
			this.checkBoxWednesday.Name = "checkBoxWednesday";
			this.checkBoxWednesday.Size = new System.Drawing.Size(16, 16);
			this.checkBoxWednesday.TabIndex = 51;
			this.checkBoxWednesday.CheckedChanged += new System.EventHandler(this.checkBoxWednesday_CheckedChanged);
			// 
			// checkBoxSunday
			// 
			this.checkBoxSunday.Checked = true;
			this.checkBoxSunday.CheckState = System.Windows.Forms.CheckState.Checked;
			this.checkBoxSunday.Location = new System.Drawing.Point(160, 40);
			this.checkBoxSunday.Name = "checkBoxSunday";
			this.checkBoxSunday.Size = new System.Drawing.Size(16, 16);
			this.checkBoxSunday.TabIndex = 52;
			this.checkBoxSunday.CheckedChanged += new System.EventHandler(this.checkBoxSunday_CheckedChanged);
			// 
			// checkBoxThursday
			// 
			this.checkBoxThursday.Checked = true;
			this.checkBoxThursday.CheckState = System.Windows.Forms.CheckState.Checked;
			this.checkBoxThursday.Location = new System.Drawing.Point(88, 40);
			this.checkBoxThursday.Name = "checkBoxThursday";
			this.checkBoxThursday.Size = new System.Drawing.Size(16, 16);
			this.checkBoxThursday.TabIndex = 53;
			this.checkBoxThursday.CheckedChanged += new System.EventHandler(this.checkBoxThursday_CheckedChanged);
			// 
			// checkBoxFriday
			// 
			this.checkBoxFriday.Checked = true;
			this.checkBoxFriday.CheckState = System.Windows.Forms.CheckState.Checked;
			this.checkBoxFriday.Location = new System.Drawing.Point(112, 40);
			this.checkBoxFriday.Name = "checkBoxFriday";
			this.checkBoxFriday.Size = new System.Drawing.Size(16, 16);
			this.checkBoxFriday.TabIndex = 54;
			this.checkBoxFriday.CheckedChanged += new System.EventHandler(this.checkBoxFriday_CheckedChanged);
			// 
			// checkBoxSaturday
			// 
			this.checkBoxSaturday.Checked = true;
			this.checkBoxSaturday.CheckState = System.Windows.Forms.CheckState.Checked;
			this.checkBoxSaturday.Location = new System.Drawing.Point(136, 40);
			this.checkBoxSaturday.Name = "checkBoxSaturday";
			this.checkBoxSaturday.Size = new System.Drawing.Size(16, 16);
			this.checkBoxSaturday.TabIndex = 55;
			this.checkBoxSaturday.CheckedChanged += new System.EventHandler(this.checkBoxSaturday_CheckedChanged);
			// 
			// groupBoxTimeFrame
			// 
			this.groupBoxTimeFrame.Controls.Add(this.label22);
			this.groupBoxTimeFrame.Controls.Add(this.label24);
			this.groupBoxTimeFrame.Controls.Add(this.dateTimePickerFromDate);
			this.groupBoxTimeFrame.Controls.Add(this.dateTimePickerToDate);
			this.groupBoxTimeFrame.Location = new System.Drawing.Point(576, 8);
			this.groupBoxTimeFrame.Name = "groupBoxTimeFrame";
			this.groupBoxTimeFrame.Size = new System.Drawing.Size(284, 72);
			this.groupBoxTimeFrame.TabIndex = 107;
			this.groupBoxTimeFrame.TabStop = false;
			this.groupBoxTimeFrame.Text = "Loaded Timeframe";
			// 
			// label22
			// 
			this.label22.Location = new System.Drawing.Point(8, 36);
			this.label22.Name = "label22";
			this.label22.Size = new System.Drawing.Size(30, 23);
			this.label22.TabIndex = 6;
			this.label22.Text = "End:";
			this.label22.TextAlign = System.Drawing.ContentAlignment.BottomRight;
			// 
			// label24
			// 
			this.label24.Location = new System.Drawing.Point(4, 12);
			this.label24.Name = "label24";
			this.label24.Size = new System.Drawing.Size(36, 23);
			this.label24.TabIndex = 5;
			this.label24.Text = "Begin:";
			this.label24.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
			// 
			// dateTimePickerFromDate
			// 
			this.dateTimePickerFromDate.CustomFormat = "dddd d. MMMM yyyy  -  HH:mm";
			this.dateTimePickerFromDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.dateTimePickerFromDate.Location = new System.Drawing.Point(40, 16);
			this.dateTimePickerFromDate.Name = "dateTimePickerFromDate";
			this.dateTimePickerFromDate.Size = new System.Drawing.Size(240, 20);
			this.dateTimePickerFromDate.TabIndex = 101;
			this.dateTimePickerFromDate.ValueChanged += new System.EventHandler(this.dateTimePickerFromDate_ValueChanged);
			// 
			// dateTimePickerToDate
			// 
			this.dateTimePickerToDate.CustomFormat = "dddd d. MMMM yyyy  -  HH:mm";
			this.dateTimePickerToDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.dateTimePickerToDate.Location = new System.Drawing.Point(40, 40);
			this.dateTimePickerToDate.Name = "dateTimePickerToDate";
			this.dateTimePickerToDate.Size = new System.Drawing.Size(240, 20);
			this.dateTimePickerToDate.TabIndex = 103;
			this.dateTimePickerToDate.ValueChanged += new System.EventHandler(this.dateTimePickerToDate_ValueChanged);
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.label1);
			this.groupBox1.Controls.Add(this.buttonNone);
			this.groupBox1.Controls.Add(this.checkBoxMonday);
			this.groupBox1.Controls.Add(this.checkBoxTuesday);
			this.groupBox1.Controls.Add(this.checkBoxWednesday);
			this.groupBox1.Controls.Add(this.checkBoxSunday);
			this.groupBox1.Controls.Add(this.checkBoxThursday);
			this.groupBox1.Controls.Add(this.checkBoxFriday);
			this.groupBox1.Controls.Add(this.checkBoxSaturday);
			this.groupBox1.Controls.Add(this.buttonAll);
			this.groupBox1.Location = new System.Drawing.Point(304, 8);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(264, 72);
			this.groupBox1.TabIndex = 108;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Day Filter";
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(16, 24);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(160, 16);
			this.label1.TabIndex = 56;
			this.label1.Text = "1      2      3      4      5      6      7";
			// 
			// RecordFilterDialog
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(860, 373);
			this.Controls.Add(this.groupBox1);
			this.Controls.Add(this.groupBoxTimeFrame);
			this.Controls.Add(this.cntRsl5);
			this.Controls.Add(this.cntSel5);
			this.Controls.Add(this.result5);
			this.Controls.Add(this.comboBox5);
			this.Controls.Add(this.selection5);
			this.Controls.Add(this.cntRsl4);
			this.Controls.Add(this.cntSel4);
			this.Controls.Add(this.result4);
			this.Controls.Add(this.comboBox4);
			this.Controls.Add(this.selection4);
			this.Controls.Add(this.cntRsl3);
			this.Controls.Add(this.cntSel3);
			this.Controls.Add(this.result3);
			this.Controls.Add(this.comboBox3);
			this.Controls.Add(this.selection3);
			this.Controls.Add(this.cntRsl2);
			this.Controls.Add(this.cntSel2);
			this.Controls.Add(this.result2);
			this.Controls.Add(this.comboBox2);
			this.Controls.Add(this.selection2);
			this.Controls.Add(this.statusBar1);
			this.Controls.Add(this.cntRsl1);
			this.Controls.Add(this.cntSel1);
			this.Controls.Add(this.result1);
			this.Controls.Add(this.comboBox1);
			this.Controls.Add(this.selection1);
			this.Controls.Add(this.buttonClose);
			this.Controls.Add(this.buttonLoad);
			this.Name = "RecordFilterDialog";
			this.Text = "RecordFilterDialog";
			this.Load += new System.EventHandler(this.RecordFilterDialog_Load);
			this.groupBoxTimeFrame.ResumeLayout(false);
			this.groupBox1.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion

		private void RecordFilterDialog_Load(object sender, System.EventArgs e)
		{
			this.resultList1 = new ArrayList();
			this.resultList2 = new ArrayList();
			this.resultList3 = new ArrayList();
			this.resultList4 = new ArrayList();
			this.resultList5 = new ArrayList();
			this.dateTimePickerToDate.Value = MyController.LoadTo;
			this.dateTimePickerFromDate.Value = MyController.LoadFrom;

			this.arrayListDays = new ArrayList();
			for(int i = 0; i < 7; i++)
				this.arrayListDays.Add("");

			if(MyController.InputDataSet != null)
				LoadInputDataSet();
			
		}

		internal void BuildWhereClause()
		{
			StringBuilder str = new StringBuilder();

			int count = 0;

			if(this.resultList1.Count > 0)
				count++;
			if(this.resultList2.Count > 0)
				count++;
			if(this.resultList3.Count > 0)
				count++;
			if(this.resultList4.Count > 0)
				count++;
			if(this.resultList5.Count > 0)
				count++;

			int dayCount = 0;
			count++;

			if(this.checkBoxMonday.Checked == true)
				dayCount++;
			if(this.checkBoxTuesday.Checked == true)
				dayCount++;
			if(this.checkBoxWednesday.Checked == true)
				dayCount++;
			if(this.checkBoxThursday.Checked == true)
				dayCount++;
			if(this.checkBoxFriday.Checked == true)
				dayCount++;
			if(this.checkBoxSaturday.Checked == true)
				dayCount++;
			if(this.checkBoxSunday.Checked == true)
				dayCount++;

			

			string[] where = new string[count];

			count = 0;
			
			for(int i = 0; i < this.resultList1.Count; i++)
			{
				if(i + 1 < this.resultList1.Count)
					str.Append(comboBox1.SelectedValue.ToString() + " = '" + this.resultList1[i] + "' OR ");
				else
					str.Append(comboBox1.SelectedValue.ToString() + " = '" + this.resultList1[i] + "'");
			}

			if(this.resultList1.Count > 0)
			{
				where[count] = string.Format(str.ToString());
				count++;
			}

			str = new StringBuilder();
		
			for(int i = 0; i < this.resultList2.Count; i++)
			{
				if(i + 1 < this.resultList2.Count)
					str.Append(comboBox2.SelectedValue.ToString() + " = '" + this.resultList2[i] + "' OR ");
				else
					str.Append(comboBox2.SelectedValue.ToString() + " = '" + this.resultList2[i] + "'");
				

			}

			if(this.resultList2.Count > 0)
			{
				where[count] = string.Format(str.ToString());
				count++;
			}
	
			str = new StringBuilder();

			for(int i = 0; i < this.resultList3.Count; i++)
			{
				if(i + 1 < this.resultList1.Count)
					str.Append(comboBox3.SelectedValue.ToString() + " = '" + this.resultList3[i] + "' OR ");
				else
					str.Append(comboBox3.SelectedValue.ToString() + " = '" + this.resultList3[i] + "'");
			

			}

			if(this.resultList3.Count > 0)
			{
				where[count] = string.Format(str.ToString());
				count++;
			}

			str = new StringBuilder();

			for(int i = 0; i < this.resultList4.Count; i++)
			{
				if(i + 1 < this.resultList4.Count)
					str.Append(comboBox4.SelectedValue.ToString() + " = '" + this.resultList4[i] + "' OR ");
				else
					str.Append(comboBox4.SelectedValue.ToString() + " = '" + this.resultList4[i] + "'");
			

			}

			if(this.resultList4.Count > 0)
			{
				where[count] = string.Format(str.ToString());
				count++;
			}

			str = new StringBuilder();

			for(int i = 0; i < this.resultList5.Count; i++)
			{
				if(i + 1 < this.resultList1.Count)
					str.Append(comboBox5.SelectedValue.ToString() + " = '" + this.resultList5[i] + "' OR ");
				else
					str.Append(comboBox5.SelectedValue.ToString() + " = '" + this.resultList5[i] + "'");
			
			}

			if(this.resultList5.Count > 0)
			{
				where[count] = string.Format(str.ToString());
				count++;
			}




			str = new StringBuilder();
			int dayDecount = dayCount;

			if(this.checkBoxMonday.Checked == true)
			{
				str.Append("DayArrival = '1' OR DayDeparture = '1'");		
				dayDecount--;
				if(dayDecount > 0)
					str.Append(" OR ");
			}

			if(this.checkBoxTuesday.Checked == true)
			{
				str.Append("DayArrival = '2' OR DayDeparture = '2'");		
				dayDecount--;
				if(dayDecount > 0)
					str.Append(" OR ");
			}

			if(this.checkBoxWednesday.Checked == true)
			{
				str.Append("DayArrival = '3' OR DayDeparture = '3'");		
				dayDecount--;
				if(dayDecount > 0)
					str.Append(" OR ");
			}

			if(this.checkBoxThursday.Checked == true)
			{
				str.Append("DayArrival = '4' OR DayDeparture = '4'");		
				dayDecount--;
				if(dayDecount > 0)
					str.Append(" OR ");
			}

			if(this.checkBoxFriday.Checked == true)
			{
				str.Append("DayArrival = '5' OR DayDeparture = '5'");		
				dayDecount--;
				if(dayDecount > 0)
					str.Append(" OR ");
			}

			if(this.checkBoxSaturday.Checked == true)
			{
				str.Append("DayArrival = '6' OR DayDeparture = '6'");		
				dayDecount--;
				if(dayDecount > 0)
					str.Append(" OR ");
			}

			if(this.checkBoxSunday.Checked == true)
			{
				str.Append("DayArrival = '7' OR DayDeparture = '7'");		
				dayDecount--;
				if(dayDecount > 0)
					str.Append(" OR ");
			}

			where[count] = string.Format(str.ToString());

			string whereClause = "";

			for(int i = where.Length - 1; i >= 0 ; i--)
			{
				if(where[i] != null)
				{
					whereClause = whereClause + "(" + where[i] + ")";
					if(i != 0)
						whereClause = whereClause + " AND ";
				}
			}
				 
			MyController.WhereClause = whereClause;
		}

		private void buttonClose_Click(object sender, System.EventArgs e)
		{

			
			BuildWhereClause();
			MyController.SelectService("MainDataInputArea");
			this.Close();
		}

		private void buttonLoad_Click(object sender, System.EventArgs e)
		{
			LoadAODB();
		}

		private void dateTimePickerToDate_ValueChanged(object sender, System.EventArgs e)
		{
			MyController.LoadTo = new DateTime(this.dateTimePickerToDate.Value.Year,
											   this.dateTimePickerToDate.Value.Month,
											   this.dateTimePickerToDate.Value.Day,
											   this.dateTimePickerToDate.Value.Hour,
											   this.dateTimePickerToDate.Value.Minute,
											   this.dateTimePickerToDate.Value.Second);
		}

		private void dateTimePickerFromDate_ValueChanged(object sender, System.EventArgs e)
		{
			MyController.LoadFrom = new DateTime(this.dateTimePickerFromDate.Value.Year,
												this.dateTimePickerFromDate.Value.Month,
												this.dateTimePickerFromDate.Value.Day,
												this.dateTimePickerFromDate.Value.Hour,
												this.dateTimePickerFromDate.Value.Minute,
												this.dateTimePickerFromDate.Value.Second);
		}



		private void comboBox1_SelectedValueChanged(object sender, System.EventArgs e)
		{
			ArrayList alRows = new ArrayList(); 
			alRows = RowValuesOfColumn(this.comboBox1.SelectedIndex);	
			this.selection1.DataSource = alRows;
			this.cntSel1.Text = alRows.Count.ToString();
			this.result1.DataSource = null;
			this.resultList1.Clear();
			this.cntRsl1.Clear();
		}

		private void comboBox2_SelectedValueChanged(object sender, System.EventArgs e)
		{
			ArrayList alRows = new ArrayList(); 
			alRows = RowValuesOfColumn(this.comboBox2.SelectedIndex);	
			this.selection2.DataSource = alRows;
			this.cntSel2.Text = alRows.Count.ToString();
			this.result2.DataSource = null;
			this.resultList2.Clear();
			this.cntRsl2.Clear();
		}

		private void comboBox3_SelectedValueChanged(object sender, System.EventArgs e)
		{
			ArrayList alRows = new ArrayList(); 
			alRows = RowValuesOfColumn(this.comboBox3.SelectedIndex);	
			this.selection3.DataSource = alRows;
			this.cntSel3.Text = alRows.Count.ToString();
			this.result3.DataSource = null;
			this.resultList3.Clear();
			this.cntRsl3.Clear();
		}

		private void comboBox4_SelectedValueChanged(object sender, System.EventArgs e)
		{
			ArrayList alRows = new ArrayList(); 
			alRows = RowValuesOfColumn(this.comboBox4.SelectedIndex);	
			this.selection4.DataSource = alRows;
			this.cntSel4.Text = alRows.Count.ToString();
			this.result4.DataSource = null;
			this.resultList4.Clear();
			this.cntRsl4.Clear();
		}

		private void comboBox5_SelectedValueChanged(object sender, System.EventArgs e)
		{
			ArrayList alRows = new ArrayList(); 
			alRows = RowValuesOfColumn(this.comboBox5.SelectedIndex);	
			this.selection5.DataSource = alRows;
			this.cntSel5.Text = alRows.Count.ToString();
			this.result5.DataSource = null;
			this.resultList5.Clear();
			this.cntRsl5.Clear();
		}

		private void selection1_DoubleClick(object sender, System.EventArgs e)
		{
			this.resultList1 = NoDoubleValues(this.resultList1,this.selection1.SelectedValue.ToString());
			this.result1.DataSource = null;
			this.result1.DataSource = resultList1;
			this.cntRsl1.Text = this.resultList1.Count.ToString();
		}

		private void selection2_DoubleClick(object sender, System.EventArgs e)
		{
			this.resultList2 = NoDoubleValues(this.resultList2,this.selection2.SelectedValue.ToString());
			this.result2.DataSource = null;
			this.result2.DataSource = resultList2;
			this.cntRsl2.Text = this.resultList2.Count.ToString();
		}

		private void selection3_DoubleClick(object sender, System.EventArgs e)
		{
			this.resultList3 = NoDoubleValues(this.resultList3,this.selection3.SelectedValue.ToString());
			this.result3.DataSource = null;
			this.result3.DataSource = resultList3;
			this.cntRsl3.Text = this.resultList3.Count.ToString();
		}

		private void selection4_DoubleClick(object sender, System.EventArgs e)
		{
			this.resultList4 = NoDoubleValues(this.resultList4,this.selection4.SelectedValue.ToString());
			this.result4.DataSource = null;
			this.result4.DataSource = resultList4;
			this.cntRsl4.Text = this.resultList4.Count.ToString();
		}

		private void selection5_DoubleClick(object sender, System.EventArgs e)
		{
			this.resultList5 = NoDoubleValues(this.resultList5,this.selection5.SelectedValue.ToString());
			this.result5.DataSource = null;
			this.result5.DataSource = resultList5;
			this.cntRsl5.Text = this.resultList5.Count.ToString();
		}

		internal ArrayList RowValuesOfColumn(int column)
		{
			ArrayList alRows = new ArrayList();
			
			if((object)column != null)
			{
				for(int j = 0; j < this.dataSetFlightRecords.Tables["Flight"].Rows.Count; j++)
				{
					alRows = NoDoubleValues(alRows, this.dataSetFlightRecords.Tables["Flight"].Rows[j][column].ToString());
				}
				alRows.Sort();
			}
			return alRows;
		}

		internal ArrayList NoDoubleValues(ArrayList al, string val)
		{
			bool doubleValue = false;

			for(int k = 0; k < al.Count; k++)
			{
				if(al[k].ToString() == val)
				{
					doubleValue = true;
					break;
				}
			}
			
			if(doubleValue == false && val != "")
				al.Add(val);

			return al;
		}

		internal void LoadAODB()
		{		
			ProgressWindow progress = new ProgressWindow();
			progress.Text = "Work";
			progress.Show(); 
			if(MyController.LoadDatabase(progress))
			{
				if(MyController.GetBusinessCaseDataInput("LocationManagement"))
				{
					LoadInputDataSet();
				}
			}
		}

		internal void LoadInputDataSet()
		{
			this.dataSetFlightRecords = MyController.InputDataSet;
			PrepareArrayLists();		
		}

		internal void PrepareArrayLists()
		{
			ArrayList alColumns1 = new ArrayList();
			ArrayList alColumns2 = new ArrayList();
			ArrayList alColumns3 = new ArrayList();
			ArrayList alColumns4 = new ArrayList();
			ArrayList alColumns5 = new ArrayList();

			for(int j = 0; j < this.dataSetFlightRecords.Tables["Flight"].Columns.Count; j++)
			{
				alColumns1.Add(this.dataSetFlightRecords.Tables["Flight"].Columns[j].ColumnName);
				alColumns2.Add(this.dataSetFlightRecords.Tables["Flight"].Columns[j].ColumnName);
				alColumns3.Add(this.dataSetFlightRecords.Tables["Flight"].Columns[j].ColumnName);
				alColumns4.Add(this.dataSetFlightRecords.Tables["Flight"].Columns[j].ColumnName);
				alColumns5.Add(this.dataSetFlightRecords.Tables["Flight"].Columns[j].ColumnName);
			}

			this.comboBox1.DataSource = alColumns1;
			this.comboBox2.DataSource = alColumns2;
			this.comboBox3.DataSource = alColumns3;
			this.comboBox4.DataSource = alColumns4;
			this.comboBox5.DataSource = alColumns5;	

		}

		internal void PrepareWhereClause()
		{

			this.where = "";
			
			StringBuilder str = new StringBuilder();
			
			for(int i = 0; i < this.arrayListDays.Count; i++)
			{
				if(this.arrayListDays[i].ToString() != "")
					str.Append(this.arrayListDays[i].ToString() + ",");
			}
			where = string.Format("(Type = 65 AND DaysArrival IN ({0})) OR (Type = 68 AND DaysDeparture IN ({0}))", str.ToString());

		}

		private void buttonAll_Click(object sender, System.EventArgs e)
		{
			this.checkBoxMonday.Checked = true;
			this.checkBoxTuesday.Checked = true;
			this.checkBoxWednesday.Checked = true;
			this.checkBoxThursday.Checked = true;
			this.checkBoxFriday.Checked = true;
			this.checkBoxSaturday.Checked = true;
			this.checkBoxSunday.Checked = true;
		}

		private void buttonNone_Click(object sender, System.EventArgs e)
		{
			this.checkBoxMonday.Checked = false;
			this.checkBoxTuesday.Checked = false;
			this.checkBoxWednesday.Checked = false;
			this.checkBoxThursday.Checked = false;
			this.checkBoxFriday.Checked = false;
			this.checkBoxSaturday.Checked = false;
			this.checkBoxSunday.Checked = false;		
		}

		private void checkBoxMonday_CheckedChanged(object sender, EventArgs e)
		{
				if(this.checkBoxMonday.Checked == true)
					this.arrayListDays[0] = "Monday";
				else if(this.checkBoxMonday.Checked == false)
					this.arrayListDays[0] = "";
		}

		private void checkBoxTuesday_CheckedChanged(object sender, EventArgs e)
		{
			if(this.checkBoxTuesday.Checked == true)
				this.arrayListDays[1] = "Tuesday";
			else if(this.checkBoxTuesday.Checked == false)
				this.arrayListDays[1] = "";
		}

		private void checkBoxWednesday_CheckedChanged(object sender, EventArgs e)
		{
			if(this.checkBoxWednesday.Checked == true)
				this.arrayListDays[2] = "Wednesday";
			else if(this.checkBoxWednesday.Checked == false)
				this.arrayListDays[2] = "";
		}

		private void checkBoxThursday_CheckedChanged(object sender, EventArgs e)
		{
			if(this.checkBoxThursday.Checked == true) 
				this.arrayListDays[3] = "Thursday";
			else if(this.checkBoxThursday.Checked == false)
				this.arrayListDays[3] = "";
		}

		private void checkBoxFriday_CheckedChanged(object sender, EventArgs e)
		{
			if(this.checkBoxFriday.Checked == true)
				this.arrayListDays[4] = "Friday";
			else if(this.checkBoxFriday.Checked == false)
				this.arrayListDays[4] = "";
		}

		private void checkBoxSaturday_CheckedChanged(object sender, EventArgs e)
		{
			if(this.checkBoxSaturday.Checked == true)
				this.arrayListDays[5] = "Saturday";
			else if(this.checkBoxSaturday.Checked == false)
				this.arrayListDays[5] = "";
		}

		private void checkBoxSunday_CheckedChanged(object sender, EventArgs e)
		{
			if(this.checkBoxSunday.Checked == true)
				this.arrayListDays[6] = "Sunday";
			else if(this.checkBoxSunday.Checked == false)
				this.arrayListDays[6] = "";
		}
		
	}
}
