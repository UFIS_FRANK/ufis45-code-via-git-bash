using System;
using System.Collections;
using System.Text;
using System.Data;

using Ufis.BL;
using Ufis.Data;
using Ufis.Utils;

namespace Ufis.BL.Services
{
	/// <summary>
	/// Summary description for TG.
	/// </summary>
	public class TG
	{
		private static Basic basicService = null;
		private DateTime from = DateTime.UtcNow;
		private DateTime to	  = DateTime.UtcNow;
		private DataSet dataSetSnapShot = new DataSet();

		//public TG(Basic basicService,DateTime from,DateTime to)
		public TG(Basic basicService)
		{
			if (TG.basicService == null)
			{
				TG.basicService = basicService;
				basicService.OnWhereClause += new Basic.WhereClause(WhereClauseHandler);
				TG.basicService.AddDatabaseSchema("C:\\Documents and Settings\\aje\\SandboxesMKS\\Ufis4.5\\Ufis\\Aia\\AIA_Client\\AIA_UfisAddOn\\AIA_Snapshot_Tool\\Ufis.BL.Common\\Schema.xsd","C:\\Documents and Settings\\aje\\SandboxesMKS\\Ufis4.5\\Ufis\\Aia\\AIA_Client\\AIA_UfisAddOn\\AIA_Snapshot_Tool\\Ufis.BL.Common\\Schema.xml");
				TG.basicService.CreateDatabaseBindings();
			}
		}

		public bool SetTimeFrame(DateTime from,DateTime to)
		{
			this.from = from;
			this.to	  = to;
			return true;
		}

		public bool LoadDatabase(object obj)
		{
			return TG.basicService.LoadDatabase(obj);
		}

		public bool GetBusinessCaseData(string businessCase,out DataSet ds)
		{

			TG.basicService.FillInitialDataSet(businessCase,out ds);
			//		this.dataSetSnapShot.Clear();
			//		this.dataSetSnapShot = ds;
			return true;
		}

		public bool GetBusinessDataSet(out DataSet ds)
		{
			ds = this.dataSetSnapShot;
			return true;
		}

		public bool SetBusinessDataSet(DataSet ds)
		{
			this.dataSetSnapShot = ds;
			return true;
		}

		public bool FillRotationTable(string where)
		{


			//clear the datatable rotation to avoid any mixups
			this.dataSetSnapShot.Tables["Rotation"].Rows.Clear();

			//sort the datatable 
			DataView dataViewSorted = new DataView(this.dataSetSnapShot.Tables["Flight"],where,"RotationIdentification ASC,Type ASC",DataViewRowState.CurrentRows);
			
			string flight = "";
			int rotation = 1;		

			for(int i = 0; i < dataViewSorted.Count; i++)
			{


				rotation = 1;

				//find first row that hasent been deleted
				for(int firstInit = i; firstInit < dataViewSorted.Count; firstInit++)
				{
					if(dataViewSorted[firstInit].Row.RowState == System.Data.DataRowState.Deleted)
					{
						i++;
					}
					else
						break;
				}

				if(i >= dataViewSorted.Count) 
					break;

				//if(i+rotation >= dataViewSorted.Count)


				//find second row that hasent been deleted

				for(int secondInit = i + 1; secondInit < dataViewSorted.Count; secondInit++)
				{
					if(dataViewSorted[secondInit].Row.RowState == System.Data.DataRowState.Deleted)
					{
						secondInit++;
					}
					else
					{
						rotation = secondInit - i;
						break;
					}
				}


				// do we have a rotation???
				if(i+rotation < this.dataSetSnapShot.Tables["Flight"].Rows.Count)
				{
					if( dataViewSorted.Count > i + rotation)
					{
						if(dataViewSorted[i]["RotationIdentification"].ToString() != dataViewSorted[i+rotation]["RotationIdentification"].ToString())
							rotation = 0;
					}
					else
						rotation = 0;

				}
				else if (i+rotation >= this.dataSetSnapShot.Tables["Flight"].Rows.Count)
				{
					rotation = 0;
				}



				if(i >= this.dataSetSnapShot.Tables["Flight"].Rows.Count || i+rotation >= this.dataSetSnapShot.Tables["Flight"].Rows.Count)
					break;


				// type of flight??? arrival or departure?!
				try
				{
					int valu = (int)dataViewSorted[i]["Type"];
					Type valuType = dataViewSorted.Table.Columns["Type"].DataType;
					flight = Enum.GetName(valuType,valu).ToString();
				} 
				catch (Exception)
				{

				}

				DataRow dataRowInsert = this.dataSetSnapShot.Tables["Rotation"].NewRow();
				
				foreach(DataColumn column in dataRowInsert.Table.Columns)
				{

					switch(column.ColumnName)
					{	

						case "AC ANU":
							if(flight == "Arrival" || flight == "Both") 
							{
								dataRowInsert[column.ColumnName] = dataViewSorted[i]["FlightNumber"].ToString();		
							} 
							else
							{
								dataRowInsert[column.ColumnName] = "";
							}
							break;

						case "DC DNU":
							if(flight == "Departure" || flight == "Both" || rotation > 0) 
							{
								dataRowInsert[column.ColumnName] = dataViewSorted[i+rotation]["FlightNumber"].ToString();	
							} 
							else
							{
								dataRowInsert[column.ColumnName] = "";
							}
							break;

						case "TYP":
							dataRowInsert[column.ColumnName] = dataViewSorted[i]["AcCode"].ToString();		
							break;


						case "ORG":
							if(flight == "Arrival" || flight == "Both") 
							{
								dataRowInsert[column.ColumnName] = dataViewSorted[i]["Origin"].ToString();		
							} 
							else
							{
								dataRowInsert[column.ColumnName] = "";
							}
							break;

						case "VIA A":
							if(flight == "Arrival" || flight == "Both") 
							{
								dataRowInsert[column.ColumnName] = dataViewSorted[i]["Via"].ToString();		
							} 
							else
							{
								dataRowInsert[column.ColumnName] = "";
							}
							break;

						case "STOA":
							if(flight == "Arrival" || flight == "Both") 
							{
								dataRowInsert[column.ColumnName] = dataViewSorted[i]["StandardTimeArrival"].ToString();		
							} 
							else
							{
								dataRowInsert[column.ColumnName] = "";
							}
							break;

						case "STOD":
							if(flight == "Departure" || flight == "Both" || rotation > 0) 
							{
								dataRowInsert[column.ColumnName] = dataViewSorted[i+rotation]["StandardTimeDeparture"].ToString();		
							} 
							else
							{
								dataRowInsert[column.ColumnName] = "";
							}
							break;

						case "VIA D":
							if(flight == "Departure" || flight == "Both" || rotation > 0) 
							{
								dataRowInsert[column.ColumnName] = dataViewSorted[i+rotation]["Via"].ToString();		
							} 
							else
							{
								dataRowInsert[column.ColumnName] = "";
							}
							break;

						case "DES":
							dataRowInsert[column.ColumnName] = dataViewSorted[i]["Destination"].ToString();		
							break;

						case "ArrivalId":
							if(flight == "Arrival" || flight == "Both") 
							{
								dataRowInsert[column.ColumnName] = dataViewSorted[i]["FlightId"].ToString();		
							} 
							else
							{
								dataRowInsert[column.ColumnName] = "";
							}
							break;

						case "DepartureId":
							if(flight == "Departure" || flight == "Both" || rotation > 0) 
							{
								dataRowInsert[column.ColumnName] = dataViewSorted[i+rotation]["FlightId"].ToString();		
							} 
							else
							{
								dataRowInsert[column.ColumnName] = "";
							}
							break;

						case "RotationIdArrival":
							if(flight == "Arrival" || flight == "Both") 
							{
								dataRowInsert[column.ColumnName] = dataViewSorted[i]["RotationIdentification"].ToString();		
							} 
							else
							{
								dataRowInsert[column.ColumnName] = "";
							}
							break;

						case "RotationIdDeparture":
							if(flight == "Departure" || flight == "Both" || rotation > 0) 
							{
								dataRowInsert[column.ColumnName] = dataViewSorted[i+rotation]["RotationIdentification"].ToString();		
							} 
							else
							{
								dataRowInsert[column.ColumnName] = "";
							}
							break;	

						case "RotationId":
							dataRowInsert[column.ColumnName] = dataViewSorted[i+rotation]["RotationIdentification"].ToString();		 
							break;
					}
				}

				i = i + rotation;

				this.dataSetSnapShot.Tables["Rotation"].Rows.Add(dataRowInsert);

			}

			
			return true;
		}


		public bool DaysBetween(out int days)
		{
			days = 0;

			DateTime start = new DateTime(this.from.Year, this.from.Month, this.from.Day, this.from.Hour, this.from.Minute, this.from.Second, this.from.Millisecond);
			DateTime end = new DateTime(this.to.Year, this.to.Month, this.to.Day, this.to.Hour, this.to.Minute, this.to.Second, this.to.Millisecond);

			while(DateTimeCompare(start,end,"<"))
			{
				days++;
				start = start.AddDays(1);
				if(DateTimeCompare(start,end,">"))
					start = end;
			}

			return true;
		}

		public bool DayTimeframes(DateTime start, out DateTime dateTimeFrom, out DateTime dateTimeTo)
		{
			dateTimeFrom = new DateTime(start.Year, start.Month, start.Day, start.Hour, start.Minute, start.Second, start.Millisecond);
			dateTimeTo = new DateTime(this.to.Year, this.to.Month, this.to.Day, this.to.Hour, this.to.Minute, this.to.Second, this.to.Millisecond);
			TimeSpan ts = new TimeSpan(0,0,0,1,0);
			

			if(DateTimeCompare(dateTimeFrom,dateTimeTo,"<"))
			{
				dateTimeTo = dateTimeFrom.AddDays(1);
				dateTimeTo = dateTimeTo.Subtract(ts);

				if(DateTimeCompare(dateTimeTo,this.to,">"))
					dateTimeTo = this.to;
			}

			return true;
		}



		private string[] WhereClauseHandler(string logicalTableName)
		{
			int days = 1;
			DaysBetween(out days);
			string[] where = new string[days+1];
			DateTime dateTimeFrom = new DateTime();
			DateTime dateTimeTo = this.from;
			int count = 0;
			TimeSpan timeSpanSecond = new TimeSpan(0,0,0,1,0);

			switch(logicalTableName)
			{
				case "FlightNatures":
					where = new string[1]{"ORDER BY TTYP"};
					break;
				case "ServiceTypes":
					where = new string[1]{"ORDER BY STYP"};
					break;
				case "AircraftTypes":
					where = new string[1]{"ORDER BY ACT3"};
					break;
				case "Airports":
					where = new string[1]{"ORDER BY APC3"};
					break;
				case "AirportParkingStands":
					where = new string[1]{"ORDER BY PNAM"};
					break;
				case "AirportGates":
					where = new string[1]{"ORDER BY GNAM"};
					break;
				case "AirportWaitingRooms":
					where = new string[1]{"ORDER BY WNAM"};
					break;
				case "AirportBaggageBelts":
					where = new string[1]{"ORDER BY BNAM"};
					break;
				case "AirportCounters":
					where = new string[1]{"ORDER BY CNAM"};
					break;
				case "Counter":

					StringBuilder str;
			
					ITable iAftTab = TG.basicService.Database["Flight"];
					int i = 0;
					int k = 0;
					ArrayList a = new ArrayList();
					for(int j = 0; j < iAftTab.Count;)
					{
						str = new StringBuilder();
						for(; i < iAftTab.Count; i++)
						{
							if(i == j || i == iAftTab.Count)
							{
								str.Append(iAftTab[i]["URNO"]);
								a.Add(iAftTab[i]["URNO"]);
								break;
							}
							else if(i + 1 < iAftTab.Count && i + 1 < j)
							{
								str.Append(iAftTab[i]["URNO"] + ",");
								a.Add(iAftTab[i]["URNO"]);
							}
						}
						j = j + 1000;
						where[k] = string.Format("WHERE FLNU IN ({0})", str.ToString());
						

					}	
					break;
					 

				case "Gates":
					while(DateTimeCompare(dateTimeTo,this.to,"<="))
					{
						DayTimeframes(dateTimeTo, out dateTimeFrom, out dateTimeTo);
						where[count] = string.Format("WHERE ((ADID = 'A' AND TIFA BETWEEN '{0}' AND '{1}') OR (ADID = 'D' AND TIFD BETWEEN '{0}' AND '{1}')) ORDER BY JCNT DESC",UT.DateTimeToCeda(dateTimeFrom),UT.DateTimeToCeda(dateTimeTo));
						count++;
						dateTimeTo = dateTimeTo.Add(timeSpanSecond);
						if(DateTimeCompare(dateTimeTo,this.to,"=="))
							break;
					}
					break;

				case "BaggageBelts":
					while(DateTimeCompare(dateTimeTo,this.to,"<="))
					{
						DayTimeframes(dateTimeTo, out dateTimeFrom, out dateTimeTo);
						where[count] = string.Format("WHERE ((ADID = 'A' AND TIFA BETWEEN '{0}' AND '{1}') OR (ADID = 'D' AND TIFD BETWEEN '{0}' AND '{1}')) ORDER BY JCNT DESC",UT.DateTimeToCeda(dateTimeFrom),UT.DateTimeToCeda(dateTimeTo));
						count++;
						dateTimeTo = dateTimeTo.Add(timeSpanSecond);
						if(DateTimeCompare(dateTimeTo,this.to,"=="))
							break;
					}
					break;

				case "WaitingRooms":
					while(DateTimeCompare(dateTimeTo,this.to,"<="))
					{
						DayTimeframes(dateTimeTo, out dateTimeFrom, out dateTimeTo);
						where[count] = string.Format("WHERE ((ADID = 'A' AND TIFA BETWEEN '{0}' AND '{1}') OR (ADID = 'D' AND TIFD BETWEEN '{0}' AND '{1}')) ORDER BY JCNT DESC",UT.DateTimeToCeda(dateTimeFrom),UT.DateTimeToCeda(dateTimeTo));
						count++;
						dateTimeTo = dateTimeTo.Add(timeSpanSecond);
						if(DateTimeCompare(dateTimeTo,this.to,"=="))
							break;
					}
					break;

				case "ParkingStands":
					while(DateTimeCompare(dateTimeTo,this.to,"<="))
					{
						DayTimeframes(dateTimeTo, out dateTimeFrom, out dateTimeTo);
						where[count] = string.Format("WHERE ((ADID = 'A' AND TIFA BETWEEN '{0}' AND '{1}') OR (ADID = 'D' AND TIFD BETWEEN '{0}' AND '{1}')) ORDER BY JCNT DESC",UT.DateTimeToCeda(dateTimeFrom),UT.DateTimeToCeda(dateTimeTo));
						count++;
						dateTimeTo = dateTimeTo.Add(timeSpanSecond);
						if(DateTimeCompare(dateTimeTo,this.to,"=="))
							break;
					}
					break;

				case "Flight":

					while(DateTimeCompare(dateTimeTo,this.to,"<="))
					{
						DayTimeframes(dateTimeTo, out dateTimeFrom, out dateTimeTo);
						where[count] = string.Format("WHERE ((ADID = 'A' AND TIFA BETWEEN '{0}' AND '{1}') OR (ADID = 'D' AND TIFD BETWEEN '{0}' AND '{1}')) ORDER BY JCNT DESC",UT.DateTimeToCeda(dateTimeFrom),UT.DateTimeToCeda(dateTimeTo));
						count++;
						dateTimeTo = dateTimeTo.Add(timeSpanSecond);
						if(DateTimeCompare(dateTimeTo,this.to,"=="))
							break;
					}
					break;

			}

			return where;
		}

		public bool Validate(DataRow row,out ArrayList errorMessages)
		{
			return TG.basicService.Validate(row,out errorMessages);
		}

		public bool Save(DataSet ds,out ArrayList errorMessages)
		{
			return TG.basicService.Save(ds,out errorMessages);
		}

		public bool GetBusinessDataPattern(DataColumn column,out string pattern)
		{
			return TG.basicService.GetBusinessDataPattern(column,out pattern);	
		}

		public bool InsertRow(DataTable dt,out ArrayList errorMessages)
		{
			return TG.basicService.InsertRow(dt,out errorMessages);
		}

		public bool DeleteRow(DataRow dr,out ArrayList errorMessages)
		{
			return TG.basicService.DeleteRow(dr,out errorMessages);
		}

		public bool LoadXML(string location, string businessCase, DataSet ds)
		{
			return TG.basicService.LoadXML(location, businessCase, ds);
		}


		
		public bool GetCorrespondingFlights(DataRow dataRowCurrent, out DataRow dataRowArrival, out DataRow dataRowDeparture)
		{
			string arrivalId = "";
			string departureId = "";
			DataView dataViewArrival = new DataView();
			DataView dataViewDeparture = new DataView();
			//string where = "FlightId = ";
			DataTable dt = new DataTable();
			
			if(dataRowCurrent.Table.TableName.ToString() == "Rotation")
			{
				
				arrivalId = dataRowCurrent["ArrivalId"].ToString();
				departureId = dataRowCurrent["DepartureId"].ToString();
				dt = dataRowCurrent.Table.DataSet.Tables["Flight"];
			}

			else if(dataRowCurrent.Table.TableName.ToString() == "Flight")
			{		 
				dt = dataRowCurrent.Table;
				int valu = (int)dataRowCurrent["Type"];
				Type valuType = dataRowCurrent.Table.Columns["Type"].DataType;
				string flight = Enum.GetName(valuType,valu).ToString();

				if(flight == "Arrival")
				{
					arrivalId = dataRowCurrent["FlightId"].ToString();				
				} 
				else if (flight == "Departure")
				{
					departureId = dataRowCurrent["FlightId"].ToString();
				}
				else if(flight ==  "Both")
				{
					arrivalId = dataRowCurrent["FlightId"].ToString();
					departureId = dataRowCurrent["FlightId"].ToString();
				}			
			}

			

			dataRowArrival = dataRowCurrent.Table.NewRow();
			string where;
			
			if(arrivalId.Length > 0) 
			{
				where = "FlightId = " + arrivalId;
				dataViewArrival = new DataView(dt, where, "", DataViewRowState.CurrentRows);				
				dataRowArrival = dataViewArrival[0].Row;
			}			
			
			dataRowDeparture = dataRowCurrent.Table.NewRow();
			
			if(departureId.Length > 0)
			{
				where = "FlightId = " + departureId;			
				dataViewDeparture = new DataView(dt, where,"", DataViewRowState.CurrentRows);
				dataRowDeparture = dataViewDeparture[0].Row;	
			}					

			return true;
		}


	
		public bool DeleteFlightsFromDataSet(DataRow currentFlights)
		{
			string arrivalId = "";
			string departureId = "";

			if(currentFlights.Table.TableName.ToString() == "Flight")
			{
				currentFlights.Delete();
				return true;
			}

			
			if(currentFlights.Table.TableName.ToString() == "Rotation")
			{
				arrivalId = currentFlights["ArrivalId"].ToString();
				departureId = currentFlights["DepartureId"].ToString();

				if(arrivalId.Length > 0)
				{
					DataView dataViewArrival = new DataView(currentFlights.Table.DataSet.Tables["Flight"], "FlightId = " + arrivalId, "", DataViewRowState.CurrentRows);
					if(dataViewArrival.Count == 1)
						dataViewArrival[0].Delete();
				}

				if(departureId.Length > 0)
				{
					DataView dataViewDeparture = new DataView(currentFlights.Table.DataSet.Tables["Flight"], "FlightId = " + departureId, "", DataViewRowState.CurrentRows);
					if(dataViewDeparture.Count == 1)
						dataViewDeparture[0].Delete();
				}

				return true;
			}
			

			return false;
		}

		public bool DateTimeFromString(string dateTimeString, out DateTime dateTime)
		{
			dateTime = System.DateTime.UtcNow;

			if(dateTimeString.Length > 0)
			{
				dateTime = new DateTime(Int32.Parse(dateTimeString.Substring(0,4)),Int32.Parse(dateTimeString.Substring(4,2)),Int32.Parse(dateTimeString.Substring(6,2)),Int32.Parse(dateTimeString.Substring(8,2)),Int32.Parse(dateTimeString.Substring(10,2)),Int32.Parse(dateTimeString.Substring(12,2)));
			}
			return true;
		}

		public bool DateTimeToString(DateTime input, out string output)
		{
			output = "";

			string year = input.Year.ToString();
			string month = input.Month.ToString();
			string day = input.Day.ToString();
			string hour = input.Hour.ToString();
			string minute = input.Minute.ToString(); 
			string second = input.Second.ToString();

			if(month.Length == 1)
				month = "0"+month;

			if(day.Length == 1)
				day = "0"+day;

			if(hour.Length == 1)
				hour= "0"+hour;

			if(minute.Length == 1)
				minute = "0"+minute;

			if(second.Length == 1)
				second = "0"+second;
			
			output = year + month + day	+ hour + minute + second;
 
			return true;
		}

		public bool DateTimeCompare(DateTime firstInput, DateTime secondInput, string operation)
		{

			int year = firstInput.Year;
			int month = firstInput.Month;
			int day = firstInput.Day;
			int hour = firstInput.Hour;
			int minute = firstInput.Minute; 
			int second = firstInput.Second;
			
			long firstLong = year * 10000000000 + month * 100000000 + day * 1000000 + hour * 10000 + minute * 100 + second;
			
	
			year = secondInput.Year;
			month = secondInput.Month;
			day = secondInput.Day;
			hour = secondInput.Hour;
			minute = secondInput.Minute; 
			second = secondInput.Second;
			
			long secondLong = year * 10000000000 + month * 100000000 + day * 1000000 + hour * 10000 + minute * 100 + second;
			

			switch(operation)
			{
				case ">":
					if(firstLong > secondLong)
						return true;
					break;

				case "<":
					if(firstLong < secondLong)
						return true;
					break;

				case ">=":
				case "=>":
					if(firstLong >= secondLong)
						return true;
					break;

				case "<=":
				case "=<":
					if(firstLong <= secondLong)
						return true;
					break;

				case "==":
					if(firstLong == secondLong)
						return true;
					break;

				case "!=":
				case "=!":
					if(firstLong != secondLong)
						return true;
					break;

				default:
					return false;
			}

			return false;
		}

		public bool DateTimeToString(DateTime inputDate, DateTime inputTime, out string output)
		{
			output = "";

			string year = inputDate.Year.ToString();
			string month = inputDate.Month.ToString();
			string day = inputDate.Day.ToString();
			string hour = inputTime.Hour.ToString();
			string minute = inputTime.Minute.ToString(); 
			string second = inputTime.Second.ToString();

			if(month.Length == 1)
				month = "0"+month;

			if(day.Length == 1)
				day = "0"+day;

			if(hour.Length == 1)
				hour= "0"+hour;

			if(minute.Length == 1)
				minute = "0"+minute;

			if(second.Length == 1)
				second = "0"+second;
			
			output = year + month + day	+ hour + minute + second;
 
			return true;
		}

		public bool SaveValueToCurrentInput(DataRow row, string column, string input)
		{
			//check if input is worth the hassel...
			if(input == null || column == null || row == null)
				return false;

			if(input.Length <= 0 || column.Length <= 0)
				return false;

			//get the table where input should be processed
			string dataTableName = row.Table.TableName.ToString();
	

			//type validation...
			switch(row.Table.Columns[column].DataType.FullName)
			{
				case "System.Int32":
					row[column] = int.Parse((string)input);
					break;
				case "System.String":
					row[column] = (string)input;
					break;
				case "System.DateTime":
					row[column] = UT.CedaFullDateToDateTime((string)input);
					break;
				default :	// Userdefined enumeration					
					Type type = row.Table.Columns[column].DataType;
					string[] values = Enum.GetNames(type);
					char inputValue = input.ToCharArray()[0];
					if(Char.IsLetter(inputValue))
					{
						for (int j = 0; j < values.Length; j++)
						{
							if (values[j] == input)
							{										
								Enum enumValue = (Enum)Enum.Parse(type,values[j]);
								row[column] = enumValue;
								break;
							} 
						}
					}
					else if(Char.IsNumber(inputValue))
					{								
						row[column] = Int32.Parse(input);
						break;
							 
					}
								
					break;	
			}	
			
			return true;
		
		}

		public bool Expand(string whereClause, DateTime importBegin, DateTime importEnd, DateTime outputBegin, DateTime outputEnd, int monday, int tuesday, int wednesday, int thursday, int friday, int saturday, int sunday)
		{

			//is input ok?
			//check import timespan , not greater than one week
			TimeSpan timeSpanOneWeek = new TimeSpan(7,0,0,0,0);
			
			if(importEnd - importBegin > timeSpanOneWeek)
				return false;

			if(outputEnd < outputBegin)
				return false;	
	
			if(monday > 7 || tuesday > 7 || wednesday > 7 || thursday > 7 || friday > 7 || saturday > 7 || sunday > 7)
				return false;

			ArrayList arrayListDays = new ArrayList(7);
			arrayListDays.Add(monday);
			arrayListDays.Add(tuesday);
			arrayListDays.Add(wednesday);
			arrayListDays.Add(thursday);
			arrayListDays.Add(friday);
			arrayListDays.Add(saturday);
			arrayListDays.Add(sunday);

			//bool several = false;
			//same value for all days?
			/*			for(int i = 0; i < arrayListDays.Count; i++)
						{
							for(int j = i + 1; j < arrayListDays.Count; j++)
							{
								if(several == false)
								{
									if((int)arrayListDays[i] == (int)arrayListDays[j] && (int)arrayListDays[j] != 0)
										several = true;
								}
								if(several == true && (int)arrayListDays[i] != (int)arrayListDays[j])
									return false;
							}
						}
			*/			//all input ok!

			

			while(DateTimeCompare(outputBegin, outputEnd, "<"))
			{
				//what day is today?
				int today;
				WeekDayToInteger(outputBegin, out today);

				//what day should be mapped today?
				int mappedDay = (int)arrayListDays[today-1];

				//if nothing should be done, go to tomorrow
				if(mappedDay == 0)
				{		
					outputBegin = outputBegin.AddDays(1);
					outputBegin = new DateTime(outputBegin.Year,outputBegin.Month,outputBegin.Day,0,0,0,0);
					if(outputBegin > outputEnd)
						outputBegin = outputEnd; 
				} 
				else 
				{

					//define start and end of today
					string start = "";
					DateTimeToString(outputBegin,out start);
				
					string end = "";
					outputBegin = new DateTime(outputBegin.Year,outputBegin.Month,outputBegin.Day,23,59,59,0);
					if(outputBegin > outputEnd)
						outputBegin = outputEnd;
					DateTimeToString(outputBegin,out end);




					//get table from mapped input day for today
					string whereStart = "";
					DateTimeToString(importBegin, out whereStart);
					string whereEnd = "";
					DateTimeToString(importEnd, out whereEnd);
				


				/*	DataView dataViewPrepareInput = new DataView(this.dataSetSnapShot.Tables["Flight"],whereClause,"",DataViewRowState.CurrentRows);

					DataTable dtInput = new DataTable();
					dtInput = this.dataSetSnapShot.Tables["Flight"].Clone();
					for(int i = 0; i < dataViewPrepareInput.Count; i++)
					{
						DataRow dataRow = dtInput.NewRow();
						foreach(DataColumn column in dataRow.Table.Columns)
						{
							if(dataViewPrepareInput[i][column.ColumnName].ToString().Length > 0)
								dataRow[column.ColumnName] = dataViewPrepareInput[i][column.ColumnName].ToString();
						}

						dtInput.Rows.Add(dataRow);
					}
					
*/

					string where = "(Type = " + 65 + " AND TimeframeArrival >= " + whereStart + " AND TimeframeArrival <= " + whereEnd + " AND DayArrival = " + mappedDay + ") OR (Type = 68 AND TimeframeDeparture >= " + whereStart + " AND TimeframeDeparture <= " + whereEnd + " AND DayDeparture = " + mappedDay + ")";
					
					if(whereClause.Length > 0)
						where = "(" + where + ") AND " + whereClause;
					DataView dataViewMappedDay = new DataView(this.dataSetSnapShot.Tables["Flight"], where, "" , DataViewRowState.CurrentRows);


					for(int i = 0; i < dataViewMappedDay.Count; i++)
					{
						DataRow dataRowInsert = this.dataSetSnapShot.Tables["Flight"].NewRow();
						long urno;
						GetFreeUrnoForSnapshot(this.dataSetSnapShot.Tables["Flight"], out urno);

						foreach(DataColumn column in dataRowInsert.Table.Columns)
						{
							switch(column.ColumnName)
							{	
								case "FlightId":
									dataRowInsert[column.ColumnName] = urno;
									break;

								case "RotationIdentification":
									dataRowInsert[column.ColumnName] = urno;
									break;

								case "Type":
									//i'm i arrival or departure?
									if(dataViewMappedDay[i]["Type"].ToString() == "65") //Arrival
									{
										dataRowInsert[column.ColumnName] = 65;
									}
									if(dataViewMappedDay[i]["Type"].ToString() == "68") //Departure
									{
										dataRowInsert[column.ColumnName] = 68;
									}
									break;

								case "StandardTimeArrival":
									//i'm i arrival or departure?
									if(dataViewMappedDay[i]["Type"].ToString() == "65") //Arrival
									{
										DateTime dateTimeArrivalBefore = new DateTime();
										DateTimeFromString(dataViewMappedDay[i]["StandardTimeArrival"].ToString(), out dateTimeArrivalBefore);
										DateTime dateTimeArrivalAfter = new DateTime(outputBegin.Year,outputBegin.Month,outputBegin.Day,dateTimeArrivalBefore.Hour,dateTimeArrivalBefore.Minute,dateTimeArrivalBefore.Second,0);
										string now = "";
										DateTimeToString(dateTimeArrivalAfter, out now);
										dataRowInsert[column.ColumnName] = now;
									}
									if(dataViewMappedDay[i]["Type"].ToString() == "68") //Departure
									{

									}
									break;

								case "StandardTimeDeparture":
									//i'm i arrival or departure?
									if(dataViewMappedDay[i]["Type"].ToString() == "65") //Arrival
									{
									}
									if(dataViewMappedDay[i]["Type"].ToString() == "68") //Departure
									{
										DateTime dateTimeDepartureBefore = new DateTime();
										DateTimeFromString(dataViewMappedDay[i]["StandardTimeDeparture"].ToString(), out dateTimeDepartureBefore);
										DateTime dateTimeDepartureAfter = new DateTime(outputBegin.Year,outputBegin.Month,outputBegin.Day,dateTimeDepartureBefore.Hour,dateTimeDepartureBefore.Minute,dateTimeDepartureBefore.Second,0);
										string now = "";
										DateTimeToString(dateTimeDepartureAfter, out now);
										dataRowInsert[column.ColumnName] = now;
									}
									break;

								case "DayArrival":
									//i'm i arrival or departure?
									if(dataViewMappedDay[i]["Type"].ToString() == "65") //Arrival
									{
										dataRowInsert[column.ColumnName] = today.ToString();
									}
									if(dataViewMappedDay[i]["Type"].ToString() == "68") //Departure
									{
								
									}
									break;

								case "DayDeparture":
									//i'm i arrival or departure?
									if(dataViewMappedDay[i]["Type"].ToString() == "65") //Arrival
									{
									
									}
									if(dataViewMappedDay[i]["Type"].ToString() == "68") //Departure
									{
										dataRowInsert[column.ColumnName] = today.ToString();
									}
									break;

								case "TimeframeArrival":
									//i'm i arrival or departure?
									if(dataViewMappedDay[i]["Type"].ToString() == "65") //Arrival
									{
										DateTime dateTimeArrivalBefore = new DateTime();
										DateTimeFromString(dataViewMappedDay[i]["TimeframeArrival"].ToString(), out dateTimeArrivalBefore);
										DateTime dateTimeArrivalAfter = new DateTime(outputBegin.Year,outputBegin.Month,outputBegin.Day,dateTimeArrivalBefore.Hour,dateTimeArrivalBefore.Minute,dateTimeArrivalBefore.Second,0);
										string now = "";
										DateTimeToString(dateTimeArrivalAfter, out now);
										dataRowInsert[column.ColumnName] = now;
									}
									if(dataViewMappedDay[i]["Type"].ToString() == "68") //Departure
									{

									}
									break;

								case "TimeframeDeparture":
									//i'm i arrival or departure?
									if(dataViewMappedDay[i]["Type"].ToString() == "65") //Arrival
									{

									}
									if(dataViewMappedDay[i]["Type"].ToString() == "68") //Departure
									{
										DateTime dateTimeDepartureBefore = new DateTime();
										DateTimeFromString(dataViewMappedDay[i]["TimeframeDeparture"].ToString(), out dateTimeDepartureBefore);
										DateTime dateTimeDepartureAfter = new DateTime(outputBegin.Year,outputBegin.Month,outputBegin.Day,dateTimeDepartureBefore.Hour,dateTimeDepartureBefore.Minute,dateTimeDepartureBefore.Second,0);
										string now = "";
										DateTimeToString(dateTimeDepartureAfter, out now);
										dataRowInsert[column.ColumnName] = now;
									}
									break;

								default:
									SaveValueToCurrentInput(dataRowInsert, column.ColumnName, dataViewMappedDay[i][column.ColumnName].ToString());
									break;
							}
						}
						this.dataSetSnapShot.Tables["Flight"].Rows.Add(dataRowInsert);
						
						// lets do the same for the resources of each flight
						for(int k = 0; k < dataRowInsert.Table.ChildRelations.Count; k++)
						{
							string relationName = dataRowInsert.Table.ChildRelations[k].RelationName;
							string tableName = dataRowInsert.Table.ChildRelations[k].ChildTable.TableName;
							DataRow[] dataRowChildRelation;
							dataRowChildRelation = dataRowInsert.GetChildRows(relationName);

							DataRow dataRowInsertChild = this.dataSetSnapShot.Tables[tableName].NewRow();
					
							foreach(DataColumn column in dataRowInsertChild.Table.Columns)
							{
								switch(column.ColumnName)
								{	
									case "Id":
										long id = 0;
										GetFreeUrnoForSnapshot(dataRowInsertChild.Table, out id);
										dataRowInsertChild[column.ColumnName] = id;
										break;

									case "FlightId":
										dataRowInsertChild[column.ColumnName] = urno;
										break;

									default:
										string input = "";
										if(dataViewMappedDay[i].CreateChildView(relationName).Count > 0)
											input = dataViewMappedDay[i].CreateChildView(relationName)[0][column.ColumnName].ToString();

										SaveValueToCurrentInput(dataRowInsertChild, column.ColumnName, input);
										break;
								}
							}
							this.dataSetSnapShot.Tables[tableName].Rows.Add(dataRowInsertChild);

						}


					}
					TimeSpan timeSpanSecond = new TimeSpan(0,0,0,1,0);
					outputBegin = outputBegin.Add(timeSpanSecond);
				}
			}
			return true;
		}
		
		public bool WeekDayToInteger(DateTime input, out int output)
		{
			string day = input.DayOfWeek.ToString();
			output = 0;

			if(day == "Monday")
				output = 1;
			else if(day == "Tuesday")
				output = 2;
			else if(day == "Wednesday")
				output = 3;
			else if(day == "Thursday")
				output = 4;
			else if(day == "Friday")
				output = 5;
			else if(day == "Saturday")
				output = 6;
			else if(day == "Sunday")
				output = 7;

			return true;
		}

		public bool GetFreeUrnoForSnapshot(DataTable table, out long urno)
		{
			DataView dv;
			long lastUsedUrno;

			switch(table.TableName)
			{
				case "Flight":
				case "ParkingStands":
				case "BaggageBelts":
				case "WaitingRooms":
				case "Gates":
					dv = new DataView(table,"","FlightId DESC", DataViewRowState.CurrentRows);
					lastUsedUrno = Int64.Parse(dv[0]["FlightId"].ToString());
					lastUsedUrno++;
					urno = lastUsedUrno;
					break;
				default:
					dv = new DataView(table,"","Id DESC", DataViewRowState.CurrentRows);
					if(dv.Count > 0)
						lastUsedUrno = Int64.Parse(dv[0]["Id"].ToString());
					else
						lastUsedUrno = 0;
					lastUsedUrno++;
					urno = lastUsedUrno;
					break;
			}

			return true;
		}

		public bool Compare(DataTable dataTableFirstInput, DataTable dataTableSecondInput, out DataTable dataTableFirstOutput, out DataTable dataTableSecondOutput)
		{

			//make working datatables
			DataTable dataTableFirst = new DataTable();
			DataTable dataTableSecond = new DataTable();

			//make output datatables
			dataTableFirstOutput = new DataTable();
			dataTableSecondOutput = new DataTable();
			dataTableFirstOutput = dataTableFirstInput.Clone();
			dataTableSecondOutput = dataTableSecondInput.Clone();

					
			//it only works with the table flight yet!
			switch(dataTableFirstInput.TableName)
			{
				case "Flight":
					DataView dataViewFirstArrival = new DataView(dataTableFirstInput.Copy(),"Type = 65","TimeframeArrival ASC",DataViewRowState.CurrentRows);
					dataTableFirst = dataViewFirstArrival.Table;
					
					DataView dataViewSecondArrival = new DataView(dataTableSecondInput.Copy(),"Type = 65","TimeframeArrival ASC",DataViewRowState.CurrentRows);
					dataTableSecond = dataViewSecondArrival.Table;

					//remove primary keys and unique fields becouse they are disturbing when inserting empty lines
					for(int i = 0; i < dataTableFirstOutput.Columns.Count; i++)
					{
						System.Data.DataColumn[] dc = new DataColumn[0];
						dataTableFirstOutput.PrimaryKey = dc;
						dataTableFirstOutput.Columns[i].Unique = false;
					}
					for(int i = 0; i < dataTableSecondOutput.Columns.Count; i++)
					{
						System.Data.DataColumn[] dc = new DataColumn[0];
						dataTableSecondOutput.PrimaryKey = dc;
						dataTableSecondOutput.Columns[i].Unique = false;
					}

					break;

				default:
					return false;
			}



			int rowCountFirst = 0;
			int rowCountSecond = 0;


			do
			{	
			while(rowCountFirst < dataTableFirst.Rows.Count)
			{
				rowCountSecond = 0;
				bool insertedRow = false;

				while(rowCountSecond < dataTableSecond.Rows.Count)
				{
					bool matchtingRow = false;

					foreach(DataColumn column in dataTableSecond.Columns)
					{ 
						if(dataTableSecond.Rows.Count > 0)
						{		
							if(dataTableFirst.Rows[rowCountFirst][column.ColumnName].ToString() == dataTableSecond.Rows[rowCountSecond][column.ColumnName].ToString())
							{
								//found an equal row!
								matchtingRow = true;
							}
							else
							{
								if(column.ColumnName != "FlightId" || column.ColumnName != "RotationIdentification")
								{
									if(dataTableFirst.Rows[rowCountFirst]["Type"].ToString() == "65")
									{
										if(column.ColumnName != "DayDeparture" || column.ColumnName != "TimeframeDeparture")
										{
											//different value found!
											matchtingRow = false;
											break;
										}
									}
									else if(dataTableFirst.Rows[rowCountFirst]["Type"].ToString() == "68")
									{
										if(column.ColumnName != "DayArrival" || column.ColumnName != "TimeframeArrival")
										{
											//different value found!
											matchtingRow = false;
											break;
										}
									}
								}
							}			
						}
					}
					if(matchtingRow == true)
					{
						//insert lines for equal rows

						int emptyLines = 0;
						for(int i = 0; i < dataTableSecondOutput.Rows.Count; i++)
						{
							if(dataTableSecondOutput.Rows[i]["FlightId"].ToString() == "")
								emptyLines++;
						}
						
						int diff = rowCountSecond - (dataTableSecondOutput.Rows.Count - emptyLines);

						for(int i = diff; i > 0; i--)
						{
							if(rowCountSecond -i >= 0 && rowCountSecond -i <= dataTableSecond.Rows.Count)
							{	
								//insert lines from second table that have no equal row in first table
								DataRow dr = dataTableFirstOutput.NewRow();
								foreach(DataColumn column in dataTableFirstOutput.Columns)
								{
									if(column.DataType.FullName == "System.String")
										dr[column.ColumnName] = "";
								}
								dr.RowError = "EMPTY";
								dataTableFirstOutput.Rows.Add(dr);

								dr = dataTableSecondOutput.NewRow();
								foreach(DataColumn column in dataTableSecondOutput.Columns)
								{
									if(dataTableSecond.Rows[rowCountSecond -i][column.ColumnName].ToString().Length > 0)
										dr[column.ColumnName] = dataTableSecond.Rows[rowCountSecond -i][column.ColumnName].ToString();
								}
								dataTableSecondOutput.Rows.Add(dr);
							}
						}


						insertedRow = true;

						DataRow dataRow = dataTableFirstOutput.NewRow();
						foreach(DataColumn column in dataTableFirstOutput.Columns)
						{
							if(dataTableFirst.Rows[rowCountFirst][column.ColumnName].ToString().Length > 0)
								dataRow[column.ColumnName] = dataTableFirst.Rows[rowCountFirst][column.ColumnName].ToString();
						}
						dataRow.RowError = "SAME ROW FOUND";
						dataTableFirstOutput.Rows.Add(dataRow);


						dataRow = dataTableSecondOutput.NewRow();
						foreach(DataColumn column in dataTableSecondOutput.Columns)
						{
							if(dataTableSecond.Rows[rowCountSecond][column.ColumnName].ToString().Length > 0)
								dataRow[column.ColumnName] = dataTableSecond.Rows[rowCountSecond][column.ColumnName].ToString();
						}
						dataRow.RowError = "SAME ROW FOUND";
						dataTableSecondOutput.Rows.Add(dataRow);

						break;

					}
					else if(matchtingRow == false)
					{
						rowCountSecond++;	
					}
				}

				if(insertedRow == false)
				{
					DataRow dataRow = dataTableFirstOutput.NewRow();
					foreach(DataColumn column in dataTableFirstOutput.Columns)
					{
						if(dataTableFirst.Rows[rowCountFirst][column.ColumnName].ToString().Length > 0)
							dataRow[column.ColumnName] = dataTableFirst.Rows[rowCountFirst][column.ColumnName].ToString();
					}

					dataTableFirstOutput.Rows.Add(dataRow);

					DataRow dr = dataTableSecondOutput.NewRow();
					foreach(DataColumn column in dataTableSecondOutput.Columns)
					{
						if(column.DataType.FullName == "System.String")
							dr[column.ColumnName] = "";
					}
					dr.RowError = "EMPTY";
					dataTableSecondOutput.Rows.Add(dr);
				}
				rowCountFirst++;
			}

			}while(rowCountFirst < dataTableFirst.Rows.Count && rowCountSecond < dataTableSecond.Rows.Count);

			
			return true;
		}
		

		public bool ConvertDataTablesDateTimes(DataTable input, out DataTable output)
		{

			output = input.Copy();

			for(int i = 0; i < output.Rows.Count; i++)
			{
				foreach(DataColumn column in output.Columns) 
				{
					if(column.ColumnName == "STOA" || 
						column.ColumnName == "STOD" || 
						column.ColumnName == "StandardTimeArrival" ||
						column.ColumnName == "StandardTimeDeparture" ||
						column.ColumnName == "TimeframeArrival" ||
						column.ColumnName == "TimeframeDeparture" ||
						column.ColumnName == "Begin" ||
						column.ColumnName == "End" ||
						column.ColumnName == "WaitingRoomsFirstBegin" ||
						column.ColumnName == "WaitingRoomsFirstEnd" ||
						column.ColumnName == "WaitingRoomsSecondBegin" ||
						column.ColumnName == "WaitingRoomsSecondEnd" ||
						column.ColumnName == "GatesArrivalFirstBegin" ||
						column.ColumnName == "GatesArrivalFirstEnd" ||
						column.ColumnName == "GatesArrivalSecondBegin" ||
						column.ColumnName == "GatesArrivalSecondEnd" ||
						column.ColumnName == "GatesDepartureFirstBegin" ||
						column.ColumnName == "GatesDepartureFirstEnd" ||
						column.ColumnName == "GatesDepartureSecondBegin" ||
						column.ColumnName == "GatesDepartureSecondEnd" ||
						column.ColumnName == "BaggageBeltsFirstBegin" ||
						column.ColumnName == "BaggageBeltsFirstEnd" ||
						column.ColumnName == "BaggageBeltsSecondBegin" ||
						column.ColumnName == "BaggageBeltsSecondEnd" ||
						column.ColumnName == "ParkingStandArrivalBegin" ||
						column.ColumnName == "ParkingStandArrivalEnd" ||
						column.ColumnName == "ParkingStandDepartureBegin" ||
						column.ColumnName == "ParkingStandDepartureEnd")
					{
						if(output.Rows[i][column].ToString().Length > 0)
							output.Rows[i][column] = Ufis.Utils.UT.CedaFullDateToDateTime(output.Rows[i][column].ToString()).ToString().Substring(0,16);
						else 
							output.Rows[i][column] = "";
					}
				}
			}

			return true;
		}
	}
}
