Attribute VB_Name = "MyOwnLib"
Option Explicit
Public FormatDateTime As String
Public FormatDatePart As String
Public FormatTimePart As String
Public FormatDateOnly As String
Public FormatTimeOnly As String
Private Declare Function HtmlHelpTopic Lib "hhctrl.ocx" Alias _
    "HtmlHelpA" (ByVal hwnd As Long, ByVal lpHelpFile As String, _
    ByVal wCommand As Long, ByVal dwData As Long) As Long
Public Sub ShowHtmlHelp(ByVal tHelpFile As String, ByVal tHelpPage As String)
    Const HH_DISPLAY_TOPIC = &H0
    On Error Resume Next
    ' open the help page in a modeless window
    HtmlHelpTopic MainDialog.hwnd, tHelpFile, HH_DISPLAY_TOPIC, 0
End Sub


Public Function CreateEmptyLine(FieldList As String) As String
    Dim Result As String
    Dim ItemNo As Long
    Dim FldNam As String
    Result = ""
    ItemNo = 1
    FldNam = GetRealItem(FieldList, ItemNo, ",")
    While FldNam <> ""
        Result = Result & ","
        ItemNo = ItemNo + 1
        FldNam = GetRealItem(FieldList, ItemNo, ",")
    Wend
    CreateEmptyLine = Result
End Function
Public Function CreateCleanFieldList(FieldList As String) As String
    Dim Result As String
    Dim ItemNo As Long
    Dim FldNam As String
    Result = ""
    ItemNo = 0
    FldNam = GetRealItem(FieldList, ItemNo, ",")
    While FldNam <> ""
        If Left(FldNam, 1) <> "'" Then
            Result = Result & FldNam & ","
        End If
        ItemNo = ItemNo + 1
        FldNam = GetRealItem(FieldList, ItemNo, ",")
    Wend
    Result = Left(Result, Len(Result) - 1)
    CreateCleanFieldList = Result
End Function
Public Function MyDateFormat(CedaDate As String, FullTime As Boolean) As String
    Dim Result As String
    Dim tmpDate
    Result = ""
    If CedaDate <> "" Then
        If FullTime Then
            tmpDate = CedaFullDateToVb(CedaDate)
            Result = Format(tmpDate, "dd mmm yyyy / hh:mm")
        Else
            tmpDate = CedaDateToVb(CedaDate)
            Result = Format(tmpDate, "dd mmm yyyy")
        End If
    End If
    MyDateFormat = UCase(Result)
End Function
Public Function CheckTimeValues(CheckTime As String, AsTimes As Boolean, IsReady As Boolean) As Boolean
    Dim TimeStrg As String
    Dim tmpStrg1 As String
    Dim tmpStrg2 As String
    Dim IsValid As Boolean
    Dim tmpHours As String
    Dim tmpMinute As String
    Dim valH As Integer
    Dim valM As Integer
    Dim chkH As String
    Dim chkM As String
    IsValid = False
    TimeStrg = CheckTime
    If TimeStrg = "" Then IsValid = True
    If Not IsValid Then
        If InStr(TimeStrg, ":") > 0 Then
            If Len(TimeStrg) > 3 Then
                tmpHours = GetRealItem(TimeStrg, 0, ":")
                If Len(tmpHours) < 2 Then tmpHours = Right("00" & tmpHours, 2)
                tmpMinute = GetRealItem(TimeStrg, 1, ":")
                If Len(tmpMinute) < 2 Then tmpMinute = Right("00" & tmpMinute, 2)
                IsValid = True
            End If
        Else
            If Len(TimeStrg) > 2 Then
                tmpMinute = Right(TimeStrg, 2)
                tmpHours = Left(TimeStrg, Len(TimeStrg) - 2)
                IsValid = True
            End If
        End If
        If IsValid Then
            If Len(tmpHours) <> 2 Then IsValid = False
            If Len(tmpMinute) <> 2 Then IsValid = False
            valH = Val(tmpHours)
            chkH = Right("00" & CStr(valH), 2)
            valM = Val(tmpMinute)
            chkM = Right("00" & CStr(valM), 2)
            If chkH <> tmpHours Then IsValid = False
            If chkM <> tmpMinute Then IsValid = False
            If valM > 59 Then IsValid = False
            If (AsTimes) And valH > 23 Then IsValid = False
            If IsValid Then TimeStrg = chkH & ":" & chkM
        End If
    End If
    If IsValid Then
        tmpStrg1 = Replace(CheckTime, ":", "", 1, -1, vbBinaryCompare)
        tmpStrg2 = Replace(TimeStrg, ":", "", 1, -1, vbBinaryCompare)
        If tmpStrg1 = tmpStrg2 Then CheckTime = TimeStrg
    End If
    CheckTimeValues = IsValid
End Function

Public Function SetButtonFaceStyle(CurForm As Form)
    Dim ctl As Control
    Dim strCtlType As String
    On Error Resume Next
    For Each ctl In CurForm.Controls
        strCtlType = TypeName(ctl)
        Select Case strCtlType
            Case "OptionButton", "CheckBox", "Label", "Frame"
                ctl.BackColor = MyOwnButtonFace
            'Case "StatusBar"
            '    does not work
            Case Else
     End Select
    Next
End Function
Public Sub CheckMonitorArea(UseForm As Form)
    Dim NewTop As Long
    Dim NewLeft As Long
    NewLeft = Screen.Width - UseForm.Width
    If NewLeft < 0 Then NewLeft = 0
    If NewLeft < UseForm.Left Then UseForm.Left = NewLeft
    NewTop = Screen.Height - UseForm.Height
    If NewTop < 0 Then NewTop = 0
    If NewTop < UseForm.Top Then UseForm.Top = NewTop
End Sub

Public Function VisibleTabLines(CurTab As TABLib.Tab) As Long
    'Assumes MainHeader, Header and HorizScrollBar
    VisibleTabLines = (((CurTab.Height - 90) / 15) / CurTab.LineHeight) - 3
End Function
Public Function SetLineMarkers(CurTab As TABLib.Tab, LineList As String, FirstColNo As Long, LastColNo As Long, SetIt As Boolean) As Long
    Dim LineNo As Long
    Dim FirstLine As Long
    Dim ColNo As Long
    Dim ToCol As Long
    Dim CurItem As Long
    Dim CurText As String
    FirstLine = Val(GetRealItem(LineList, 0, ","))
    'If chkStrSplitter(5).Value = 1 Then
        If LineList <> "-1" Then
            CurText = "START"
            CurItem = 0
            While CurText <> ""
                CurText = GetRealItem(LineList, CurItem, ",")
                If CurText <> "" Then
                    LineNo = Val(CurText)
                    If LineNo >= 0 Then
                        If SetIt Then
                            CurTab.SetDecorationObject LineNo, FirstColNo, "Marker1"
                            ToCol = LastColNo - 1
                            For ColNo = FirstColNo + 1 To ToCol
                                CurTab.SetDecorationObject LineNo, ColNo, "Marker1"
                            Next
                            CurTab.SetDecorationObject LineNo, LastColNo, "Marker1"
                        Else
                            CurTab.ResetLineDecorations LineNo
                        End If
                        If LineNo < FirstLine Then FirstLine = LineNo
                    End If
                End If
                CurItem = CurItem + 1
            Wend
            CurTab.RedrawTab
        End If
    'End If
    SetLineMarkers = FirstLine
End Function


