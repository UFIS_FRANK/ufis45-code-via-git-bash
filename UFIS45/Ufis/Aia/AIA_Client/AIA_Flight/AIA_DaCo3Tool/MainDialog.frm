VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{64E8E382-05E2-11D2-9B1D-9B0630BC8F12}#1.0#0"; "tab.ocx"
Begin VB.Form MainDialog 
   Caption         =   "Aircraft Record Collection Editor"
   ClientHeight    =   5910
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   14040
   BeginProperty Font 
      Name            =   "Courier New"
      Size            =   8.25
      Charset         =   161
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "MainDialog.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MouseIcon       =   "MainDialog.frx":014A
   OLEDropMode     =   1  'Manual
   ScaleHeight     =   5910
   ScaleWidth      =   14040
   StartUpPosition =   3  'Windows Default
   WindowState     =   1  'Minimized
   Begin VB.PictureBox DelayPanel 
      AutoRedraw      =   -1  'True
      BackColor       =   &H000040C0&
      Height          =   2535
      Left            =   10170
      ScaleHeight     =   2475
      ScaleWidth      =   1935
      TabIndex        =   77
      Top             =   2490
      Visible         =   0   'False
      Width           =   1995
      Begin VB.PictureBox DlyEditPanel 
         AutoRedraw      =   -1  'True
         BorderStyle     =   0  'None
         Height          =   375
         Index           =   0
         Left            =   30
         ScaleHeight     =   375
         ScaleWidth      =   1845
         TabIndex        =   78
         TabStop         =   0   'False
         Top             =   120
         Width           =   1845
         Begin VB.CheckBox chkDlyCheck 
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Index           =   0
            Left            =   780
            Style           =   1  'Graphical
            TabIndex        =   82
            Top             =   30
            Visible         =   0   'False
            Width           =   180
         End
         Begin VB.TextBox txtDlyInput 
            Alignment       =   2  'Center
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Index           =   0
            Left            =   1110
            MaxLength       =   5
            TabIndex        =   81
            Text            =   "00:15"
            Top             =   30
            Width           =   675
         End
         Begin VB.CheckBox chkDlyTick 
            Height          =   210
            Index           =   0
            Left            =   60
            TabIndex        =   79
            Top             =   75
            Width           =   195
         End
         Begin VB.Label lblDlyReason 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "A/C-Chg"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00000000&
            Height          =   195
            Index           =   0
            Left            =   300
            TabIndex        =   80
            Top             =   75
            Width           =   735
         End
      End
   End
   Begin VB.CheckBox chkTask 
      Caption         =   "Task"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Index           =   0
      Left            =   4080
      Style           =   1  'Graphical
      TabIndex        =   38
      Top             =   2520
      Visible         =   0   'False
      Width           =   1035
   End
   Begin VB.Timer MainTimer 
      Enabled         =   0   'False
      Index           =   1
      Interval        =   1000
      Left            =   5790
      Top             =   3900
   End
   Begin VB.PictureBox WorkArea 
      AutoRedraw      =   -1  'True
      BackColor       =   &H000040C0&
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2145
      Left            =   120
      ScaleHeight     =   2085
      ScaleWidth      =   3855
      TabIndex        =   6
      Top             =   2505
      Width           =   3915
      Begin VB.PictureBox MinMaxPanel 
         BorderStyle     =   0  'None
         Height          =   240
         Index           =   0
         Left            =   2520
         ScaleHeight     =   240
         ScaleWidth      =   555
         TabIndex        =   69
         Top             =   120
         Width           =   555
         Begin VB.CheckBox chkMax 
            Caption         =   "+"
            Height          =   240
            Index           =   0
            Left            =   15
            Style           =   1  'Graphical
            TabIndex        =   71
            Top             =   0
            Width           =   255
         End
         Begin VB.CheckBox chkHide 
            Caption         =   "X"
            Height          =   240
            Index           =   0
            Left            =   285
            Style           =   1  'Graphical
            TabIndex        =   70
            Top             =   0
            Width           =   255
         End
      End
      Begin VB.PictureBox fraVtSplitter 
         AutoRedraw      =   -1  'True
         BorderStyle     =   0  'None
         Height          =   1515
         Index           =   0
         Left            =   2310
         MousePointer    =   9  'Size W E
         ScaleHeight     =   1515
         ScaleWidth      =   135
         TabIndex        =   25
         Top             =   90
         Width           =   135
      End
      Begin VB.PictureBox fraCrSplitter 
         AutoRedraw      =   -1  'True
         BorderStyle     =   0  'None
         Height          =   135
         Index           =   0
         Left            =   2310
         MousePointer    =   15  'Size All
         ScaleHeight     =   135
         ScaleWidth      =   135
         TabIndex        =   24
         Top             =   1680
         Width           =   135
      End
      Begin VB.PictureBox fraHzSplitter 
         AutoRedraw      =   -1  'True
         BorderStyle     =   0  'None
         Height          =   135
         Index           =   0
         Left            =   150
         MousePointer    =   7  'Size N S
         ScaleHeight     =   135
         ScaleWidth      =   2085
         TabIndex        =   23
         Top             =   1680
         Width           =   2085
      End
      Begin TABLib.TAB FileData 
         Height          =   1485
         Index           =   0
         Left            =   180
         TabIndex        =   7
         Top             =   120
         Width           =   2055
         _Version        =   65536
         _ExtentX        =   3625
         _ExtentY        =   2619
         _StockProps     =   64
         Columns         =   10
         FontName        =   "Courier New"
      End
   End
   Begin VB.PictureBox BottomPanel 
      AutoRedraw      =   -1  'True
      BackColor       =   &H000040C0&
      Height          =   495
      Left            =   0
      ScaleHeight     =   435
      ScaleWidth      =   12600
      TabIndex        =   5
      Top             =   5100
      Visible         =   0   'False
      Width           =   12660
      Begin VB.Frame fraXButtonPanel 
         BorderStyle     =   0  'None
         Height          =   300
         Index           =   2
         Left            =   10470
         TabIndex        =   15
         Top             =   30
         Visible         =   0   'False
         Width           =   885
         Begin VB.CheckBox chkTool 
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Index           =   2
            Left            =   0
            Style           =   1  'Graphical
            TabIndex        =   16
            Top             =   0
            Width           =   855
         End
      End
      Begin VB.Frame fraXButtonPanel 
         BorderStyle     =   0  'None
         Height          =   330
         Index           =   1
         Left            =   8100
         TabIndex        =   12
         Top             =   60
         Visible         =   0   'False
         Width           =   1935
         Begin VB.CheckBox chkTool 
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   1
            Left            =   0
            Style           =   1  'Graphical
            TabIndex        =   14
            Top             =   0
            Width           =   855
         End
         Begin VB.TextBox txtFeld 
            Alignment       =   2  'Center
            BackColor       =   &H00C0C0C0&
            BeginProperty Font 
               Name            =   "Courier New"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   1
            Left            =   870
            Locked          =   -1  'True
            TabIndex        =   13
            Top             =   0
            Width           =   855
         End
      End
      Begin VB.Frame fraXButtonPanel 
         BorderStyle     =   0  'None
         Height          =   300
         Index           =   0
         Left            =   30
         TabIndex        =   9
         Top             =   30
         Visible         =   0   'False
         Width           =   1755
         Begin VB.TextBox txtFeld 
            Alignment       =   2  'Center
            BackColor       =   &H00C0C0C0&
            BeginProperty Font 
               Name            =   "Courier New"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Index           =   0
            Left            =   870
            Locked          =   -1  'True
            TabIndex        =   11
            Top             =   0
            Width           =   855
         End
         Begin VB.CheckBox chkTool 
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Index           =   0
            Left            =   0
            Style           =   1  'Graphical
            TabIndex        =   10
            Top             =   0
            Width           =   855
         End
      End
   End
   Begin VB.PictureBox TopPanel 
      AutoRedraw      =   -1  'True
      BackColor       =   &H000040C0&
      Height          =   2385
      Left            =   0
      ScaleHeight     =   2325
      ScaleWidth      =   12750
      TabIndex        =   4
      Top             =   0
      Width           =   12810
      Begin VB.PictureBox FlightPanel 
         AutoRedraw      =   -1  'True
         Height          =   1005
         Left            =   0
         ScaleHeight     =   945
         ScaleWidth      =   12075
         TabIndex        =   39
         Top             =   420
         Width           =   12135
         Begin VB.TextBox txtAftData 
            Height          =   315
            Index           =   4
            Left            =   11040
            TabIndex        =   76
            Top             =   390
            Visible         =   0   'False
            Width           =   885
         End
         Begin VB.TextBox txtAftData 
            Height          =   315
            Index           =   3
            Left            =   11040
            TabIndex        =   75
            Top             =   60
            Visible         =   0   'False
            Width           =   885
         End
         Begin VB.TextBox txtAftData 
            Height          =   315
            Index           =   2
            Left            =   10140
            TabIndex        =   74
            Top             =   390
            Visible         =   0   'False
            Width           =   885
         End
         Begin VB.TextBox txtAftData 
            Height          =   315
            Index           =   1
            Left            =   10140
            TabIndex        =   73
            Top             =   60
            Visible         =   0   'False
            Width           =   885
         End
         Begin VB.TextBox txtAftData 
            Height          =   315
            Index           =   0
            Left            =   9630
            TabIndex        =   72
            Top             =   210
            Visible         =   0   'False
            Width           =   495
         End
         Begin VB.Label lblDep 
            Alignment       =   2  'Center
            BackColor       =   &H00FFFFFF&
            BorderStyle     =   1  'Fixed Single
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   12
            Left            =   8400
            TabIndex        =   89
            Top             =   600
            Width           =   480
         End
         Begin VB.Label lblDep 
            Alignment       =   2  'Center
            BackColor       =   &H00FFFFFF&
            BorderStyle     =   1  'Fixed Single
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   11
            Left            =   7920
            TabIndex        =   88
            Top             =   600
            Width           =   480
         End
         Begin VB.Label lblArr 
            Alignment       =   2  'Center
            BackColor       =   &H00FFFFFF&
            BorderStyle     =   1  'Fixed Single
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   12
            Left            =   8400
            TabIndex        =   87
            Top             =   270
            Width           =   480
         End
         Begin VB.Label lblArr 
            Alignment       =   2  'Center
            BackColor       =   &H00FFFFFF&
            BorderStyle     =   1  'Fixed Single
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   11
            Left            =   7920
            TabIndex        =   86
            Top             =   270
            Width           =   480
         End
         Begin VB.Label Label2 
            Alignment       =   2  'Center
            BackStyle       =   0  'Transparent
            Caption         =   "Mvt/Req. Logs"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   225
            Left            =   7590
            TabIndex        =   85
            Top             =   30
            Width           =   1365
         End
         Begin VB.Label lblArr 
            Alignment       =   2  'Center
            BackColor       =   &H00FFFFFF&
            BorderStyle     =   1  'Fixed Single
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   10
            Left            =   7650
            TabIndex        =   84
            Top             =   270
            Width           =   270
         End
         Begin VB.Label lblDep 
            Alignment       =   2  'Center
            BackColor       =   &H00FFFFFF&
            BorderStyle     =   1  'Fixed Single
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   10
            Left            =   7650
            TabIndex        =   83
            Top             =   600
            Width           =   270
         End
         Begin VB.Label Label6 
            Alignment       =   2  'Center
            BackStyle       =   0  'Transparent
            Caption         =   "Date"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   225
            Left            =   2250
            TabIndex        =   68
            Top             =   30
            Width           =   1005
         End
         Begin VB.Label Label1 
            Alignment       =   2  'Center
            BackStyle       =   0  'Transparent
            Caption         =   "Flight No."
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   225
            Left            =   750
            TabIndex        =   67
            Top             =   30
            Width           =   1005
         End
         Begin VB.Label Label11 
            BackStyle       =   0  'Transparent
            Caption         =   "Remarks"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   225
            Left            =   9180
            TabIndex        =   66
            Top             =   30
            Width           =   1245
         End
         Begin VB.Label lblDep 
            BackColor       =   &H00FFFFFF&
            BorderStyle     =   1  'Fixed Single
            Caption         =   " "
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   9
            Left            =   9060
            TabIndex        =   65
            Top             =   600
            Width           =   1710
         End
         Begin VB.Label lblArr 
            BackColor       =   &H00FFFFFF&
            BorderStyle     =   1  'Fixed Single
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   9
            Left            =   9060
            TabIndex        =   64
            Top             =   270
            Width           =   1710
         End
         Begin VB.Label lblDep 
            Alignment       =   2  'Center
            BackColor       =   &H00FFFFFF&
            BorderStyle     =   1  'Fixed Single
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   8
            Left            =   6150
            TabIndex        =   63
            Top             =   270
            Width           =   1350
         End
         Begin VB.Label lblArr 
            Alignment       =   2  'Center
            BackColor       =   &H00FFFFFF&
            BorderStyle     =   1  'Fixed Single
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   8
            Left            =   6150
            TabIndex        =   62
            Top             =   270
            Width           =   1350
         End
         Begin VB.Label lblDep 
            Alignment       =   2  'Center
            BackColor       =   &H00FFFFFF&
            BorderStyle     =   1  'Fixed Single
            Caption         =   " "
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   7
            Left            =   6930
            TabIndex        =   61
            Top             =   600
            Width           =   570
         End
         Begin VB.Label lblArr 
            Alignment       =   2  'Center
            BackColor       =   &H00FFFFFF&
            BorderStyle     =   1  'Fixed Single
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   7
            Left            =   6930
            TabIndex        =   60
            Top             =   600
            Width           =   570
         End
         Begin VB.Label Label9 
            BackStyle       =   0  'Transparent
            Caption         =   "Aircraft / Type"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   225
            Left            =   6195
            TabIndex        =   59
            Top             =   30
            Width           =   1275
         End
         Begin VB.Label lblDep 
            Alignment       =   2  'Center
            BackColor       =   &H00FFFFFF&
            BorderStyle     =   1  'Fixed Single
            Caption         =   " "
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   6
            Left            =   6150
            TabIndex        =   58
            Top             =   600
            Width           =   780
         End
         Begin VB.Label lblArr 
            Alignment       =   2  'Center
            BackColor       =   &H00FFFFFF&
            BorderStyle     =   1  'Fixed Single
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   6
            Left            =   6150
            TabIndex        =   57
            Top             =   600
            Width           =   780
         End
         Begin VB.Label lblArr 
            Alignment       =   2  'Center
            BackColor       =   &H00FFFFFF&
            BorderStyle     =   1  'Fixed Single
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   5
            Left            =   5280
            TabIndex        =   56
            Top             =   270
            Width           =   720
         End
         Begin VB.Label lblDep 
            Alignment       =   2  'Center
            BackColor       =   &H00FFFFFF&
            BorderStyle     =   1  'Fixed Single
            Caption         =   " "
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   5
            Left            =   5280
            TabIndex        =   55
            Top             =   600
            Width           =   720
         End
         Begin VB.Label Label8 
            Alignment       =   2  'Center
            BackStyle       =   0  'Transparent
            Caption         =   "Actual"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   225
            Left            =   5190
            TabIndex        =   54
            Top             =   30
            Width           =   885
         End
         Begin VB.Label Label5 
            Alignment       =   2  'Center
            BackStyle       =   0  'Transparent
            Caption         =   "Estim."
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   225
            Left            =   4260
            TabIndex        =   53
            Top             =   30
            Width           =   1035
         End
         Begin VB.Label lblDep 
            Alignment       =   2  'Center
            BackColor       =   &H00FFFFFF&
            BorderStyle     =   1  'Fixed Single
            Caption         =   " "
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   4
            Left            =   4410
            TabIndex        =   52
            Top             =   600
            Width           =   720
         End
         Begin VB.Label lblArr 
            Alignment       =   2  'Center
            BackColor       =   &H00FFFFFF&
            BorderStyle     =   1  'Fixed Single
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   4
            Left            =   4410
            TabIndex        =   51
            Top             =   270
            Width           =   720
         End
         Begin VB.Label lblArr 
            Alignment       =   2  'Center
            BackColor       =   &H00FFFFFF&
            BorderStyle     =   1  'Fixed Single
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   3
            Left            =   3540
            TabIndex        =   50
            Top             =   270
            Width           =   720
         End
         Begin VB.Label lblDep 
            Alignment       =   2  'Center
            BackColor       =   &H00FFFFFF&
            BorderStyle     =   1  'Fixed Single
            Caption         =   " "
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   3
            Left            =   3540
            TabIndex        =   49
            Top             =   600
            Width           =   720
         End
         Begin VB.Label lblArr 
            Alignment       =   2  'Center
            BackColor       =   &H00FFFFFF&
            BorderStyle     =   1  'Fixed Single
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   2
            Left            =   2070
            TabIndex        =   48
            Top             =   270
            Width           =   1305
         End
         Begin VB.Label lblDep 
            Alignment       =   2  'Center
            BackColor       =   &H00FFFFFF&
            BorderStyle     =   1  'Fixed Single
            Caption         =   " "
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   2
            Left            =   2070
            TabIndex        =   47
            Top             =   600
            Width           =   1305
         End
         Begin VB.Label Label4 
            Alignment       =   2  'Center
            BackStyle       =   0  'Transparent
            Caption         =   "Sched."
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   225
            Left            =   3390
            TabIndex        =   46
            Top             =   30
            Width           =   1005
         End
         Begin VB.Label lblDep 
            Alignment       =   2  'Center
            BackColor       =   &H00FFFFFF&
            BorderStyle     =   1  'Fixed Single
            Caption         =   " "
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   1
            Left            =   1230
            TabIndex        =   45
            Top             =   600
            Width           =   675
         End
         Begin VB.Label lblDep 
            Alignment       =   2  'Center
            BackColor       =   &H00FFFFFF&
            BorderStyle     =   1  'Fixed Single
            Caption         =   " "
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   0
            Left            =   615
            TabIndex        =   44
            Top             =   600
            Width           =   615
         End
         Begin VB.Label lblDepRow 
            BackStyle       =   0  'Transparent
            Caption         =   "DEP:"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H0000C000&
            Height          =   225
            Left            =   90
            TabIndex        =   43
            Top             =   630
            Width           =   495
         End
         Begin VB.Label lblArrRow 
            BackStyle       =   0  'Transparent
            Caption         =   "ARR:"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H0000FFFF&
            Height          =   225
            Left            =   90
            TabIndex        =   42
            Top             =   300
            Width           =   495
         End
         Begin VB.Label lblArr 
            Alignment       =   2  'Center
            BackColor       =   &H00FFFFFF&
            BorderStyle     =   1  'Fixed Single
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   1
            Left            =   1230
            TabIndex        =   41
            Top             =   270
            Width           =   675
         End
         Begin VB.Label lblArr 
            Alignment       =   2  'Center
            BackColor       =   &H00FFFFFF&
            BorderStyle     =   1  'Fixed Single
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   0
            Left            =   615
            TabIndex        =   40
            Top             =   270
            Width           =   615
         End
      End
      Begin VB.TextBox txtEdit 
         BackColor       =   &H8000000F&
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9.75
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   0
         Left            =   3990
         Locked          =   -1  'True
         TabIndex        =   36
         Top             =   60
         Visible         =   0   'False
         Width           =   1035
      End
      Begin VB.CheckBox chkImpTyp 
         Caption         =   "Import"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   0
         Left            =   2850
         Style           =   1  'Graphical
         TabIndex        =   32
         Top             =   60
         Width           =   1035
      End
      Begin VB.CheckBox chkWork 
         Caption         =   "Work"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   0
         Left            =   1710
         Style           =   1  'Graphical
         TabIndex        =   31
         Top             =   60
         Visible         =   0   'False
         Width           =   1035
      End
      Begin VB.Frame fraTopButtonPanel 
         BackColor       =   &H80000010&
         BorderStyle     =   0  'None
         Height          =   315
         Index           =   0
         Left            =   30
         TabIndex        =   8
         Top             =   60
         Width           =   1575
      End
      Begin MSComctlLib.Slider FontSlider 
         Height          =   405
         Left            =   6630
         TabIndex        =   35
         Top             =   0
         Visible         =   0   'False
         Width           =   1245
         _ExtentX        =   2196
         _ExtentY        =   714
         _Version        =   393216
         LargeChange     =   2
         TickStyle       =   3
      End
      Begin VB.Label lblContext 
         Alignment       =   2  'Center
         BorderStyle     =   1  'Fixed Single
         Caption         =   "15JUN03"
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9.75
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   0
         Left            =   5100
         TabIndex        =   37
         Top             =   60
         Visible         =   0   'False
         Width           =   1035
      End
   End
   Begin VB.PictureBox RightPanel 
      Align           =   4  'Align Right
      AutoRedraw      =   -1  'True
      AutoSize        =   -1  'True
      BackColor       =   &H000040C0&
      Height          =   5595
      Left            =   12885
      ScaleHeight     =   5535
      ScaleWidth      =   1095
      TabIndex        =   2
      Top             =   0
      Width           =   1155
      Begin VB.CheckBox Check1 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   750
         Style           =   1  'Graphical
         TabIndex        =   29
         ToolTipText     =   "Bypass the Clear Function"
         Top             =   30
         Width           =   315
      End
      Begin VB.CheckBox chkOnTop 
         Caption         =   "Top"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   30
         Style           =   1  'Graphical
         TabIndex        =   27
         Top             =   30
         Width           =   720
      End
      Begin VB.CheckBox chkTabIsVisible 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   225
         Index           =   0
         Left            =   30
         MaskColor       =   &H8000000F&
         TabIndex        =   26
         Tag             =   "-1"
         Top             =   2100
         Visible         =   0   'False
         Width           =   210
      End
      Begin VB.Frame fraYButtonPanel 
         BorderStyle     =   0  'None
         Height          =   1650
         Index           =   0
         Left            =   30
         TabIndex        =   17
         Top             =   390
         Width           =   1035
         Begin VB.CheckBox chkAppl 
            Caption         =   "About"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   5
            Left            =   0
            Style           =   1  'Graphical
            TabIndex        =   22
            Tag             =   "ABOUT"
            Top             =   330
            Width           =   1035
         End
         Begin VB.CheckBox chkAppl 
            Caption         =   "Close"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   6
            Left            =   0
            Style           =   1  'Graphical
            TabIndex        =   21
            Tag             =   "CLOSE"
            Top             =   0
            Width           =   1035
         End
         Begin VB.CheckBox chkAppl 
            Caption         =   "Help"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   9
            Left            =   0
            Style           =   1  'Graphical
            TabIndex        =   20
            Tag             =   "HELP"
            Top             =   660
            Width           =   1035
         End
         Begin VB.CheckBox chkAppl 
            Caption         =   "Config"
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   7
            Left            =   0
            Style           =   1  'Graphical
            TabIndex        =   19
            Tag             =   "CONFIG"
            Top             =   1320
            Width           =   1035
         End
         Begin VB.CheckBox chkAppl 
            Caption         =   "Setup"
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   4
            Left            =   0
            Style           =   1  'Graphical
            TabIndex        =   18
            Tag             =   "SETUP"
            Top             =   990
            Width           =   1035
         End
      End
      Begin VB.HScrollBar ImpScroll 
         Height          =   315
         Left            =   30
         TabIndex        =   3
         Top             =   30
         Visible         =   0   'False
         Width           =   1035
      End
      Begin VB.PictureBox ServerPanel 
         AutoRedraw      =   -1  'True
         BackColor       =   &H000080FF&
         Height          =   525
         Left            =   0
         ScaleHeight     =   465
         ScaleWidth      =   1245
         TabIndex        =   33
         Top             =   4470
         Visible         =   0   'False
         Width           =   1305
         Begin VB.Label lblSrvName 
            Alignment       =   2  'Center
            BackStyle       =   0  'Transparent
            Caption         =   "Server"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000009&
            Height          =   225
            Index           =   0
            Left            =   120
            TabIndex        =   34
            Top             =   0
            Width           =   735
         End
         Begin VB.Shape ServerSignal 
            FillColor       =   &H0000C0C0&
            Height          =   165
            Index           =   4
            Left            =   30
            Shape           =   3  'Circle
            Top             =   225
            Width           =   195
         End
         Begin VB.Shape ServerSignal 
            FillColor       =   &H0000C0C0&
            FillStyle       =   0  'Solid
            Height          =   165
            Index           =   3
            Left            =   870
            Shape           =   3  'Circle
            Top             =   225
            Width           =   195
         End
         Begin VB.Shape ServerSignal 
            Height          =   165
            Index           =   2
            Left            =   660
            Shape           =   3  'Circle
            Top             =   225
            Width           =   195
         End
         Begin VB.Shape ServerSignal 
            Height          =   165
            Index           =   1
            Left            =   450
            Shape           =   3  'Circle
            Top             =   225
            Width           =   195
         End
         Begin VB.Shape ServerSignal 
            Height          =   165
            Index           =   0
            Left            =   240
            Shape           =   3  'Circle
            Top             =   225
            Width           =   195
         End
      End
      Begin VB.Label lblTabName 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Label1"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   195
         Index           =   0
         Left            =   300
         TabIndex        =   30
         Top             =   2130
         Visible         =   0   'False
         Width           =   585
      End
      Begin VB.Label lblTabShadow 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Shadow"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   195
         Index           =   0
         Left            =   90
         TabIndex        =   28
         Top             =   2520
         Visible         =   0   'False
         Width           =   690
      End
   End
   Begin TABLib.TAB HelperTab 
      Height          =   1815
      Index           =   0
      Left            =   6570
      TabIndex        =   1
      Top             =   2490
      Visible         =   0   'False
      Width           =   1485
      _Version        =   65536
      _ExtentX        =   2619
      _ExtentY        =   3201
      _StockProps     =   64
   End
   Begin VB.Timer MainTimer 
      Enabled         =   0   'False
      Index           =   0
      Interval        =   1000
      Left            =   5310
      Top             =   3900
   End
   Begin MSComctlLib.StatusBar StatusBar 
      Align           =   2  'Align Bottom
      Height          =   315
      Left            =   0
      TabIndex        =   0
      Top             =   5595
      Width           =   14040
      _ExtentX        =   24765
      _ExtentY        =   556
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   6
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Enabled         =   0   'False
            Object.Width           =   19005
            MinWidth        =   8819
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   6
            Alignment       =   1
            AutoSize        =   2
            Object.Width           =   1588
            MinWidth        =   1058
            TextSave        =   "21.11.2003"
         EndProperty
         BeginProperty Panel3 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   5
            Alignment       =   1
            AutoSize        =   2
            Object.Width           =   1058
            MinWidth        =   1058
            TextSave        =   "15:26"
         EndProperty
         BeginProperty Panel4 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   1
            Alignment       =   1
            AutoSize        =   2
            Enabled         =   0   'False
            Object.Width           =   953
            MinWidth        =   706
            TextSave        =   "CAPS"
         EndProperty
         BeginProperty Panel5 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   2
            Alignment       =   1
            AutoSize        =   2
            Enabled         =   0   'False
            Object.Width           =   794
            MinWidth        =   706
            TextSave        =   "NUM"
         EndProperty
         BeginProperty Panel6 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   3
            Alignment       =   1
            AutoSize        =   2
            Object.Width           =   714
            MinWidth        =   706
            TextSave        =   "INS"
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "MainDialog"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim MainAreaColor As Integer
Dim DataAreaColor As Integer
Dim WorkAreaColor As Integer
Dim ShowMaxButtons As Integer
Dim MainSections As Integer
Dim CountMainTabs As Integer
Dim CountSubTabs As Integer
Dim MinFrameSize As Long
Dim MinLeftWidth As Long
Dim MaxLeftWidth As Long
Dim MinRightWidth As Long
Dim MaxRightWidth As Long
Dim SplitterMoves As Boolean
Dim TabIsMaximized As Integer
Dim SetMyTop As Long
Dim SetMyLeft As Long
Dim SetMyHeight As Long
Dim SetMyWidth As Long
Dim RefreshOnBc As Boolean
Dim LastFocusIndex As Integer
Dim TabEditInline As Boolean
Dim TabEditInsert As Boolean
Dim TabEditUpdate As Boolean
Dim TabEditIsActive As Boolean
Dim TabEditCurLineNo As Long
Dim TabEditCurColNo As Long
Dim TabEditHitEnter As Boolean
Dim AutoSaveAodb As Boolean
Dim AutoTabRead As Boolean
Dim ExcelJustOpened As Boolean
Dim ExcelShutDown As Boolean
Public WithEvents MsExcel As Excel.Application
Attribute MsExcel.VB_VarHelpID = -1
Public MsWkBook As Workbook
Public MsWkSheet As Worksheet
Dim ExcelFields As String
Dim ExcelUpdateAllowed As Boolean
Dim ExcelRoSaveAllowed As Boolean
Dim SplitPosH As Long
Dim SplitPosV As Long
Dim MaxFormHeight As Long
Dim MaxFormWidth As Long

Private Sub ArrangeWorkButtons(PanelIndex As Integer, IniSection As String, DefaultList As String)
    Dim IniKeyWord As String
    Dim PanelTag As String
    Dim ButtonList As String
    Dim ButtonDefine As String
    Dim ButtonType As String
    Dim ButtonCapt As String
    Dim ButtonFunc As String
    Dim UsedButtons As Integer
    Dim UsedLabels As Integer
    Dim UsedFields As Integer
    Dim UseIniSection As String
    Dim LeftPos As Long
    Dim i As Long
    If PanelIndex > fraTopButtonPanel.UBound Then
        Load fraTopButtonPanel(PanelIndex)
        Set fraTopButtonPanel(PanelIndex).Container = TopPanel
    End If
    UsedButtons = 0
    For i = 0 To chkWork.UBound
        If chkWork(i).Tag <> "" Then UsedButtons = UsedButtons + 1
    Next
    UsedLabels = 0
    For i = 0 To lblContext.UBound
        If lblContext(i).Tag <> "" Then UsedLabels = UsedLabels + 1
    Next
    UsedFields = 0
    For i = 0 To txtEdit.UBound
        If txtEdit(i).Tag <> "" Then UsedFields = UsedFields + 1
    Next
    
    UseIniSection = GetIniEntry(myIniFullName, IniSection, "", "BUTTON_SECTION", IniSection)
    PanelTag = CStr(UsedButtons)
    UsedButtons = UsedButtons - 1
    UsedLabels = UsedLabels - 1
    UsedFields = UsedFields - 1
    IniKeyWord = "BUTTONS_ROW_" & CStr(PanelIndex)
    ButtonList = GetIniEntry(myIniFullName, UseIniSection, "", IniKeyWord, DefaultList)
    FlightPanel.Visible = False
    If ButtonList = "FLIGHT_PANEL" Then
        FlightPanel.Top = 0
        FlightPanel.Left = 0
        FlightPanel.Visible = True
        fraTopButtonPanel(PanelIndex).Width = 0
        fraTopButtonPanel(PanelIndex).Tag = "FLIGHT_PANEL"
        fraTopButtonPanel(PanelIndex).Visible = False
        TopPanel.Tag = CStr(PanelIndex) & ",-1"
    Else
        i = 0
        LeftPos = 0
        ButtonDefine = GetRealItem(ButtonList, i, "|")
        While ButtonDefine <> ""
            ButtonType = GetRealItem(ButtonDefine, 0, ",")
            ButtonCapt = GetRealItem(ButtonDefine, 1, ",")
            ButtonFunc = GetRealItem(ButtonDefine, 2, ",")
            Select Case ButtonType
                Case "B"    'Normal Button (CheckBox)
                    UsedButtons = UsedButtons + 1
                    If UsedButtons > chkWork.UBound Then Load chkWork(UsedButtons)
                    Set chkWork(UsedButtons).Container = fraTopButtonPanel(PanelIndex)
                    chkWork(UsedButtons).Top = 0
                    chkWork(UsedButtons).Left = LeftPos
                    chkWork(UsedButtons).Caption = ButtonCapt
                    chkWork(UsedButtons).Tag = ButtonFunc & "," & ButtonType
                    chkWork(UsedButtons).Visible = True
                    LeftPos = LeftPos + chkWork(UsedButtons).Width + 15
                Case "T", "P"
                    UsedButtons = UsedButtons + 1
                    If UsedButtons > chkWork.UBound Then Load chkWork(UsedButtons)
                    Set chkWork(UsedButtons).Container = fraTopButtonPanel(PanelIndex)
                    chkWork(UsedButtons).Top = 0
                    chkWork(UsedButtons).Left = LeftPos
                    chkWork(UsedButtons).Width = 525
                    chkWork(UsedButtons).Caption = GetRealItem(ButtonCapt, 0, ";")
                    chkWork(UsedButtons).Tag = GetRealItem(ButtonFunc, 0, ";") & "," & ButtonType & "," & CStr(UsedButtons + 1)
                    chkWork(UsedButtons).Visible = True
                    LeftPos = LeftPos + chkWork(UsedButtons).Width
                    UsedButtons = UsedButtons + 1
                    If UsedButtons > chkWork.UBound Then Load chkWork(UsedButtons)
                    Set chkWork(UsedButtons).Container = fraTopButtonPanel(PanelIndex)
                    chkWork(UsedButtons).Top = 0
                    chkWork(UsedButtons).Left = LeftPos
                    chkWork(UsedButtons).Width = 510
                    chkWork(UsedButtons).Caption = GetRealItem(ButtonCapt, 1, ";")
                    chkWork(UsedButtons).Tag = GetRealItem(ButtonFunc, 1, ";") & "," & ButtonType & "," & CStr(UsedButtons - 1)
                    chkWork(UsedButtons).Visible = True
                    LeftPos = LeftPos + chkWork(UsedButtons).Width + 15
                Case "D"    'Double Buttons
                    UsedButtons = UsedButtons + 1
                    If UsedButtons > chkWork.UBound Then Load chkWork(UsedButtons)
                    Set chkWork(UsedButtons).Container = fraTopButtonPanel(PanelIndex)
                    chkWork(UsedButtons).Top = 0
                    chkWork(UsedButtons).Left = LeftPos
                    chkWork(UsedButtons).Width = 720
                    chkWork(UsedButtons).Caption = GetRealItem(ButtonCapt, 0, ";")
                    chkWork(UsedButtons).Tag = GetRealItem(ButtonFunc, 0, ";") & ",D1," & CStr(UsedButtons + 1)
                    chkWork(UsedButtons).Visible = True
                    LeftPos = LeftPos + chkWork(UsedButtons).Width
                    UsedButtons = UsedButtons + 1
                    If UsedButtons > chkWork.UBound Then Load chkWork(UsedButtons)
                    Set chkWork(UsedButtons).Container = fraTopButtonPanel(PanelIndex)
                    chkWork(UsedButtons).Top = 0
                    chkWork(UsedButtons).Left = LeftPos
                    chkWork(UsedButtons).Width = 315
                    chkWork(UsedButtons).Caption = GetRealItem(ButtonCapt, 1, ";")
                    chkWork(UsedButtons).Tag = GetRealItem(ButtonFunc, 1, ";") & ",D2," & CStr(UsedButtons - 1)
                    chkWork(UsedButtons).Visible = True
                    LeftPos = LeftPos + chkWork(UsedButtons).Width + 15
                Case "L"
                    UsedLabels = UsedLabels + 1
                    If UsedLabels > lblContext.UBound Then Load lblContext(UsedLabels)
                    Set lblContext(UsedLabels).Container = fraTopButtonPanel(PanelIndex)
                    lblContext(UsedLabels).Top = 0
                    lblContext(UsedLabels).Left = LeftPos
                    lblContext(UsedLabels).Caption = ButtonCapt
                    lblContext(UsedLabels).Tag = ButtonFunc & "," & ButtonType
                    lblContext(UsedLabels).Visible = True
                    LeftPos = LeftPos + lblContext(UsedLabels).Width + 15
                Case "F"
                    UsedFields = UsedFields + 1
                    If UsedFields > txtEdit.UBound Then Load txtEdit(UsedFields)
                    Set txtEdit(UsedFields).Container = fraTopButtonPanel(PanelIndex)
                    txtEdit(UsedFields).Top = 0
                    txtEdit(UsedFields).Left = LeftPos
                    txtEdit(UsedFields).Text = ButtonCapt
                    txtEdit(UsedFields).Tag = ButtonFunc & "," & ButtonType
                    txtEdit(UsedFields).Visible = True
                    LeftPos = LeftPos + txtEdit(UsedFields).Width + 15
                Case Else
            End Select
            i = i + 1
            ButtonDefine = GetRealItem(ButtonList, i, "|")
        Wend
        fraTopButtonPanel(PanelIndex).Width = LeftPos
        fraTopButtonPanel(PanelIndex).Tag = PanelTag & "," & CStr(UsedButtons)
        TopPanel.Tag = CStr(PanelIndex) & ",-1"
    End If
End Sub

Private Sub LoadAodbData(UseTab As TABLib.Tab, ResetTab As Boolean, TableName As String, UseSqlKey As String)
    Dim tmpFldLst As String
    Dim tmpSqlKey As String
    Dim CedaColor As Long
    Screen.MousePointer = 11
    CedaColor = ServerSignal(2).FillColor
    ServerSignal(2).FillColor = vbGreen
    ServerSignal(2).Refresh
    ServerSignal(1).FillColor = vbYellow
    ServerSignal(1).FillStyle = 0
    ServerSignal(1).Refresh
    UseTab.ResetContent
    UseTab.Refresh
    UseTab.CedaCurrentApplication = UfisServer.ModName
    UseTab.CedaHopo = UfisServer.HOPO
    UseTab.CedaIdentifier = "IDX"
    UseTab.CedaPort = "3357"
    UseTab.CedaReceiveTimeout = "250"
    UseTab.CedaRecordSeparator = vbLf
    UseTab.CedaSendTimeout = "250"
    UseTab.CedaServerName = UfisServer.HostName
    UseTab.CedaTabext = UfisServer.TblExt
    UseTab.CedaUser = UfisServer.ModName
    UseTab.CedaWorkstation = UfisServer.GetMyWorkStationName
    If ServerIsAvailable Then
        tmpFldLst = UseTab.LogicalFieldList
        tmpSqlKey = UseSqlKey
        UseTab.CedaAction "GFR", TableName, tmpFldLst, "", tmpSqlKey
    Else
        'If HostName = "LOCAL" Then UseTab.ReadFromFile "c:\ufis\basicdata" & Trim(Str(Index)) & ".txt"
    End If
    UseTab.AutoSizeColumns
    UseTab.Refresh
    Screen.MousePointer = 0
    ServerSignal(1).FillColor = vbGreen
    ServerSignal(2).FillColor = CedaColor
    ServerSignal(2).Refresh
End Sub

Private Sub chkAppl_Click(Index As Integer)
    If chkAppl(Index).Value = 1 Then chkAppl(Index).BackColor = LightGreen Else chkAppl(Index).BackColor = MyOwnButtonFace
    Select Case Index
        Case 5  'About
            If chkAppl(Index).Value = 1 Then
                If MainLifeStyle Then MyAboutBox.SetLifeStyle = True
                If GridLifeStyle Then MyAboutBox.VersionTab.LifeStyle = True
                MyAboutBox.Show , Me
                SetFormOnTop MyAboutBox, True
                chkAppl(Index).Value = 0
            End If
        Case 6  'Close/Exit
            If chkAppl(Index).Value = 1 Then
                If chkAppl(Index).Tag = "EXIT" Then
                    ShutDownApplication True
                Else
                    ShutDownApplication False
                End If
                chkAppl(Index).Value = 0
            End If
        Case 9  'Help
            If chkAppl(Index).Value = 1 Then
                App.HelpFile = "c:\ufis\help\Telexpool.chm"
                ShowHtmlHelp "c:\ufis\help\Telexpool.chm", "Intor"
            End If
            
    End Select
End Sub
Private Sub showhelp()
    'Dim myhh As HHCTRLLib.HHCtrl
End Sub
Private Sub CheckIdentify()
    Dim FldName As String
    Dim FldItem As String
    Dim tmpAdid As String
    Dim tmpRkey As String
    Dim CurLine As Long
    Dim MaxLine As Long
    Dim LineNo As Long
    Dim AdidCol As Long
    Dim RkeyCol As Long
    FldName = "RKEY"
    FldItem = TranslateFieldItems(FldName, FileData(0).LogicalFieldList)
    RkeyCol = Val(FldItem)
    FileData(0).IndexCreate FldName, RkeyCol
    FldItem = TranslateFieldItems(FldName, FileData(1).LogicalFieldList)
    RkeyCol = Val(FldItem)
    FileData(1).IndexCreate FldName, RkeyCol
    FileData(1).SetInternalLineBuffer True
    MaxLine = FileData(0).GetLineCount - 1
    For CurLine = 0 To MaxLine
        tmpAdid = FileData(0).GetFieldValue(CurLine, "ADID")
        Select Case tmpAdid
            Case "A"
                tmpRkey = FileData(0).GetFieldValue(CurLine, "RKEY")
                LineNo = Val(FileData(1).GetLinesByIndexValue("RKEY", tmpRkey, 0))
                If LineNo = 0 Then FileData(0).SetLineColor CurLine, vbBlack, vbYellow
            Case "B"
                FileData(0).SetLineColor CurLine, vbBlack, vbCyan
            Case Else
                FileData(0).SetLineColor CurLine, vbWhite, vbRed
        End Select
    Next
    FileData(1).SetInternalLineBuffer False
    FileData(0).SetInternalLineBuffer True
    MaxLine = FileData(1).GetLineCount - 1
    For CurLine = 0 To MaxLine
        tmpAdid = FileData(1).GetFieldValue(CurLine, "ADID")
        Select Case tmpAdid
            Case "B"
                FileData(1).SetLineColor CurLine, vbBlack, vbCyan
            Case "D"
                tmpRkey = FileData(1).GetFieldValue(CurLine, "RKEY")
                LineNo = Val(FileData(0).GetLinesByIndexValue("RKEY", tmpRkey, 0))
                If LineNo = 0 Then FileData(1).SetLineColor CurLine, vbBlack, vbGreen
            Case Else
                FileData(0).SetLineColor CurLine, vbWhite, vbRed
        End Select
    Next
    FileData(1).SetInternalLineBuffer False
    
    FileData(0).Refresh
End Sub

Private Sub chkDlyTick_Click(Index As Integer)
    'MsgBox DelayPanel.Tag
    If chkDlyTick(Index).Value = 1 Then
        txtDlyInput(Index).BackColor = vbWhite
        txtDlyInput(Index).Locked = False
        txtDlyInput(Index).SetFocus
    Else
        txtDlyInput(Index).Text = txtDlyInput(Index).Tag
        txtDlyInput(Index).BackColor = NormalGray
        txtDlyInput(Index).ForeColor = vbBlack
        txtDlyInput(Index).Locked = True
    End If
End Sub

Private Sub chkDlyTick_GotFocus(Index As Integer)
    lblDlyReason(Index).ForeColor = vbYellow
End Sub

Private Sub chkDlyTick_LostFocus(Index As Integer)
    lblDlyReason(Index).ForeColor = vbBlack
End Sub

Private Sub chkHide_Click(Index As Integer)
    If chkHide(Index).Value = 1 Then
        chkTabIsVisible(Index).Value = 0
        chkHide(Index).Value = 0
    End If
End Sub

Private Sub chkImpTyp_Click(Index As Integer)
    Dim tmpTag As String
    Dim LastIndex As Integer
    TabIsMaximized = -1
    If chkImpTyp(Index).Value = 1 Then
        chkImpTyp(Index).BackColor = LightGreen
        tmpTag = TopPanel.Tag
        LastIndex = Val(GetRealItem(tmpTag, 1, ","))
        If LastIndex >= 0 Then chkImpTyp(LastIndex).Value = 0
        InitButtonSection Index
        tmpTag = TopPanel.Tag
        TopPanel.Tag = GetRealItem(tmpTag, 0, ",") & "," & CStr(Index)
    Else
        chkImpTyp(Index).BackColor = MyOwnButtonFace
        ResetMain
        TopPanel.Tag = "0,-1"
    End If
End Sub

Private Sub InitButtonSection(Index As Integer)
    Dim tmpSections As String
    Dim SectionKey As String
    Dim tmpData As String
    Dim CurItem As Long
    Dim CurIdx As Integer
    Dim ButtonRows As Integer
    
    GetApplPosSize chkImpTyp(Index).Tag, True
    WorkArea.Visible = False
    tmpData = GetIniEntry(myIniFullName, chkImpTyp(Index).Tag, "", "APPL_SIZE", "")
    
    tmpSections = GetIniEntry(myIniFullName, chkImpTyp(Index).Tag, "", "BUTTON_SECTION", chkImpTyp(Index).Tag)
    ButtonRows = Val(GetIniEntry(myIniFullName, tmpSections, "", "WORK_BUTTON_ROWS", "3"))
    For CurIdx = 1 To ButtonRows
        ArrangeWorkButtons CurIdx, chkImpTyp(Index).Tag, ""
    Next
    ArrangeTopArea
    tmpSections = GetIniEntry(myIniFullName, chkImpTyp(Index).Tag, "", "BOTTOM_SECTION", chkImpTyp(Index).Tag)
    ArrangeBottomAraea tmpSections
    WorkArea.Visible = True
    Form_Resize
    Me.Refresh
    WorkArea.Visible = False
    Me.Caption = GetIniEntry(myIniFullName, chkImpTyp(Index).Tag, "", "APPL_TITLE", Me.Caption)
    DataAreaColor = Val(GetIniEntry(myIniFullName, chkImpTyp(Index).Tag, "", "WORK_AREA_COLOR", CStr(MainAreaColor)))
    If DataAreaColor <> MainAreaColor Then
        WorkAreaColor = DataAreaColor
    End If
    SplitPosH = Val(GetIniEntry(myIniFullName, chkImpTyp(Index).Tag, "", "H_SPLIT_PERCENT", "50"))
    SplitPosV = Val(GetIniEntry(myIniFullName, chkImpTyp(Index).Tag, "", "V_SPLIT_PERCENT", "50"))
    myIniSection = GetIniEntry(myIniFullName, chkImpTyp(Index).Tag, "", "LAYOUT_SECTION", chkImpTyp(Index).Tag)
    tmpSections = GetRealItem(myIniSection, 1, "|")
    If tmpSections = "" Then
        MainSections = Abs(Val(GetIniEntry(myIniFullName, myIniSection, "", "MAIN_SECTIONS", "1"))) - 1
        tmpSections = GetIniEntry(myIniFullName, myIniSection, "", "DATA_SECTIONS", "MAIN")
    Else
        MainSections = 0
        myIniSection = GetRealItem(myIniSection, 0, "|")
    End If
    CurItem = 0
    CurIdx = 0
    SectionKey = GetRealItem(tmpSections, CurItem, ",")
    While SectionKey <> ""
        InitTabLayout CurIdx, SectionKey
        CurIdx = CurIdx + 1
        CurItem = CurItem + 1
        SectionKey = GetRealItem(tmpSections, CurItem, ",")
    Wend
    fraVtSplitter(0).Height = Screen.Height
    If MainLifeStyle Then DrawBackGround fraVtSplitter(0), WorkAreaColor, False, True
    myIniSection = chkImpTyp(Index).Tag
    WorkArea.Visible = True
    StatusBar.ZOrder
    Me.Refresh
    CheckReadOnlyMode
End Sub
Private Sub ArrangeTopArea()
    Dim TopPos As Long
    Dim i As Integer
    Dim MaxI As Integer
    Dim tmpTag As String
    Dim iFrom As Integer
    Dim iTo As Integer
    fraTopButtonPanel(0).Top = 30
    TopPos = fraTopButtonPanel(0).Top + fraTopButtonPanel(0).Height + 30
    fraTopButtonPanel(0).Height = chkWork(0).Height '+ 15
    MaxI = Val(GetRealItem(TopPanel.Tag, 0, ","))
    For i = 1 To MaxI
        tmpTag = fraTopButtonPanel(i).Tag
        If tmpTag = "FLIGHT_PANEL" Then
            fraTopButtonPanel(i).Visible = False
            FlightPanel.Top = TopPos
            'ResetFlightPanel
            FlightPanel.Visible = True
            FlightPanel.Width = Screen.Width
            DrawBackGround FlightPanel, WorkAreaColor, True, True
            TopPos = TopPos + FlightPanel.Height
        Else
            iFrom = Val(GetRealItem(tmpTag, 0, ","))
            iTo = Val(GetRealItem(tmpTag, 1, ","))
            If iTo >= iFrom Then
                fraTopButtonPanel(i).Top = TopPos
                fraTopButtonPanel(i).Height = chkWork(0).Height '+ 15
                fraTopButtonPanel(i).Visible = True
                TopPos = fraTopButtonPanel(i).Top + fraTopButtonPanel(i).Height + 15
            End If
        End If
    Next
    TopPanel.Height = TopPos + 75
    TopPanel.Width = Screen.Width + 180
    If MainLifeStyle Then DrawBackGround TopPanel, WorkAreaColor, True, True
    WorkArea.Top = TopPanel.Height
    WorkArea.Height = BottomPanel.Top - WorkArea.Top
    DelayPanel.Top = WorkArea.Top
    DelayPanel.Height = WorkArea.Height
End Sub
Private Sub ArrangeBottomAraea(CfgSection As String)
    Dim tmpData As String
    tmpData = GetIniEntry(myIniFullName, CfgSection, "", "SHOW_BOTTOM_PANEL", "NO")
    If Left(tmpData, 1) = "Y" Then BottomPanel.Visible = True Else BottomPanel.Visible = False
End Sub
Private Sub InitTabLayout(Index As Integer, SectionKey As String)
    Dim NewLeft As Long
    Dim NewTop As Long
    Dim tmpHeaderTitle As String
    Dim tmpHeaderWidth As String
    Dim tmpHeaderColor As String
    Dim tmpSqlConfig As String
    Dim tmpFieldNames As String
    Dim tmpFields As String
    Dim tmpKeys As String
    Dim tmpSqlFields As String
    Dim tmpSqlKey As String
    Dim tmpSystCols As String
    Dim TabCfgLine As String
    Dim tmpSortKeys As String
    Dim tmpStr As String
    Dim tmpItem As String
    Dim tmpPrnt As String
    Dim ItemNo As Long
    Dim i As Integer
    Dim ReturnColor As Long
    ReturnColor = -1
    If Index > chkTabIsVisible.UBound Then
        i = chkTabIsVisible.UBound
        Load FileData(Index)
        Load fraHzSplitter(Index)
        Load fraCrSplitter(Index)
        fraCrSplitter(Index).ZOrder
        Load chkTabIsVisible(Index)
        Load lblTabName(Index)
        Load lblTabShadow(Index)
        Load MinMaxPanel(Index)
        Load chkHide(Index)
        Load chkMax(Index)
        Set chkHide(Index).Container = MinMaxPanel(Index)
        Set chkMax(Index).Container = MinMaxPanel(Index)
        chkHide(Index).Visible = True
        chkMax(Index).Visible = True
        
        NewLeft = chkTabIsVisible(i).Left
        NewTop = chkTabIsVisible(i).Top + chkTabIsVisible(i).Height
        If (i > 0) And (i = MainSections) Then NewTop = NewTop + 75
        chkTabIsVisible(Index).Top = NewTop
        chkTabIsVisible(Index).Left = NewLeft
        chkTabIsVisible(Index).Value = 0
        lblTabName(Index).Top = chkTabIsVisible(Index).Top
        lblTabName(Index).Left = NewLeft + chkTabIsVisible(Index).Width + 30
        lblTabShadow(Index).Top = chkTabIsVisible(Index).Top + 15
        lblTabShadow(Index).Left = NewLeft + chkTabIsVisible(Index).Width + 45
        'fraHzSplitter(Index).Width = Screen.Width
        fraHzSplitter(Index).Width = 60000
        If MainLifeStyle Then
            DrawBackGround fraHzSplitter(Index), WorkAreaColor, True, True
            ReturnColor = DrawBackGround(fraCrSplitter(Index), WorkAreaColor, True, True)
        End If
    End If
    If Index = 0 Then
        NewLeft = chkTabIsVisible(0).Left
        lblTabName(Index).Top = chkTabIsVisible(Index).Top
        lblTabName(Index).Left = NewLeft + chkTabIsVisible(Index).Width + 30
        lblTabShadow(Index).Top = chkTabIsVisible(Index).Top + 15
        lblTabShadow(Index).Left = NewLeft + chkTabIsVisible(Index).Width + 45
        'fraHzSplitter(Index).Width = Screen.Width
        fraHzSplitter(Index).Width = 60000
        If MainLifeStyle Then
            DrawBackGround fraHzSplitter(Index), WorkAreaColor, True, True
            ReturnColor = DrawBackGround(fraCrSplitter(Index), WorkAreaColor, True, True)
        End If
    End If
    NewTop = chkTabIsVisible(Index).Top + chkTabIsVisible(Index).Height + 15
    If Index <= FileData.UBound Then
        lblTabName(Index).ZOrder
        chkTabIsVisible(Index).ZOrder
        If Index <= MainSections Then
            chkTabIsVisible(Index).Tag = "L,-1"
        Else
            chkTabIsVisible(Index).Tag = "R,-1"
        End If
        FileData(Index).ResetContent
        FileData(Index).FontSize = FontSlider.Value + 6
        FileData(Index).HeaderFontSize = FontSlider.Value + 6
        FileData(Index).LineHeight = FontSlider.Value + 6
        FileData(Index).LeftTextOffset = 1
        FileData(Index).SetTabFontBold True
        FileData(Index).MainHeader = True
        If GridLifeStyle Then
            FileData(Index).LifeStyle = True
            FileData(Index).CursorLifeStyle = True
            FileData(Index).SelectColumnBackColor = RGB(235, 235, 255)
            FileData(Index).SelectColumnTextColor = vbBlack
            'FileData(Index).ColumnSelectionLifeStyle = True
        End If
        lblTabName(Index).Caption = GetIniEntry(myIniFullName, myIniSection, "", SectionKey & "_SHORT_TITLE", SectionKey)
        lblTabShadow(Index).Caption = lblTabName(Index).Caption
        lblTabName(Index).ToolTipText = "Data Grid: " & lblTabName(Index).Caption
        tmpSortKeys = GetIniEntry(myIniFullName, myIniSection, "", SectionKey & "_SORT_FIELDS", "")
        tmpFieldNames = GetIniEntry(myIniFullName, myIniSection, "", SectionKey & "_FIELD_NAMES", "????")
        If tmpFieldNames = "????" Then tmpFieldNames = "NO CONFIGURATION FOUND!"
        tmpSystCols = GetIniEntry(myIniFullName, myIniSection, "", SectionKey & "_SYSTEM_COLS", "NO")
        If tmpSystCols = "YES" Then
            FileData(Index).LogicalFieldList = "'NO','ID','DO'," & tmpFieldNames & ",'RM','SY','ST'"
            tmpStr = CStr(ItemCount(tmpFieldNames, ","))
            tmpHeaderTitle = GetIniEntry(myIniFullName, myIniSection, "", SectionKey & "_MAIN_HEADER", lblTabName(Index).Caption)
            tmpHeaderWidth = GetIniEntry(myIniFullName, myIniSection, "", SectionKey & "_HEADER_SIZE", tmpStr)
            tmpHeaderColor = GetIniEntry(myIniFullName, myIniSection, "", SectionKey & "_HEADER_COLOR", "")
            If tmpHeaderTitle <> "" Then
                tmpHeaderWidth = "3," & tmpHeaderWidth & ",3"
                tmpHeaderTitle = "Status," & tmpHeaderTitle & ",System"
                If tmpHeaderColor <> "" Then tmpHeaderColor = "12632256," & tmpHeaderColor & ",12632256"
            End If
            FileData(Index).SetMainHeaderFont FontSlider.Value + 6, False, False, True, 0, "Courier New"
            tmpStr = GetIniEntry(myIniFullName, myIniSection, "", SectionKey & "_GRID_HEADER", tmpFieldNames)
            FileData(Index).HeaderString = "Line,I,A," & tmpStr & ",Remark,System Cells,System Status"
            tmpStr = GetIniEntry(myIniFullName, myIniSection, "", SectionKey & "_FIELD_WIDTH", "10")
            FileData(Index).HeaderLengthString = "5,1,1," & tmpStr & ",10,10,10"
            FileData(Index).ColumnWidthString = "5,1,1," & tmpStr & ",10,10,10"
            tmpStr = GetIniEntry(myIniFullName, myIniSection, "", SectionKey & "_FIELD_ALIGN", "")
            If tmpStr <> "" Then
                tmpStr = "R,L,L," & tmpStr & ",L,L,L"
                FormatTimeFields FileData(Index), tmpStr
                tmpStr = Replace(tmpStr, "CDT", "C", 1, -1, vbBinaryCompare)
                tmpStr = Replace(tmpStr, "CT", "C", 1, -1, vbBinaryCompare)
                tmpStr = Replace(tmpStr, "CD", "C", 1, -1, vbBinaryCompare)
                tmpStr = Replace(tmpStr, "FDT", "C", 1, -1, vbBinaryCompare)
                tmpStr = Replace(tmpStr, "FDP", "C", 1, -1, vbBinaryCompare)
                tmpStr = Replace(tmpStr, "FTP", "C", 1, -1, vbBinaryCompare)
                tmpStr = Replace(tmpStr, "FDO", "C", 1, -1, vbBinaryCompare)
                tmpStr = Replace(tmpStr, "FTO", "C", 1, -1, vbBinaryCompare)
                FileData(Index).ColumnAlignmentString = tmpStr
                FileData(Index).HeaderAlignmentString = tmpStr
            End If
        Else
            FileData(Index).LogicalFieldList = tmpFieldNames & ",'SK'"
            tmpStr = CStr(ItemCount(tmpFieldNames, ","))
            tmpHeaderTitle = GetIniEntry(myIniFullName, myIniSection, "", SectionKey & "_MAIN_HEADER", lblTabName(Index).Caption)
            tmpHeaderWidth = GetIniEntry(myIniFullName, myIniSection, "", SectionKey & "_HEADER_SIZE", tmpStr)
            tmpHeaderColor = GetIniEntry(myIniFullName, myIniSection, "", SectionKey & "_HEADER_COLOR", "")
            FileData(Index).SetMainHeaderFont FontSlider.Value + 6, False, False, True, 0, "Courier New"
            tmpStr = GetIniEntry(myIniFullName, myIniSection, "", SectionKey & "_GRID_HEADER", tmpFieldNames)
            FileData(Index).HeaderString = tmpStr & ",Sort Keys"
            tmpStr = GetIniEntry(myIniFullName, myIniSection, "", SectionKey & "_FIELD_WIDTH", "10")
            FileData(Index).HeaderLengthString = tmpStr & ",20"
            FileData(Index).ColumnWidthString = tmpStr & ",20"
            tmpStr = GetIniEntry(myIniFullName, myIniSection, "", SectionKey & "_FIELD_ALIGN", "")
            If tmpStr <> "" Then
                FormatTimeFields FileData(Index), tmpStr
                tmpStr = Replace(tmpStr, "CDT", "C", 1, -1, vbBinaryCompare)
                tmpStr = Replace(tmpStr, "CT", "C", 1, -1, vbBinaryCompare)
                tmpStr = Replace(tmpStr, "CD", "C", 1, -1, vbBinaryCompare)
                tmpStr = Replace(tmpStr, "FDT", "C", 1, -1, vbBinaryCompare)
                tmpStr = Replace(tmpStr, "FDP", "C", 1, -1, vbBinaryCompare)
                tmpStr = Replace(tmpStr, "FTP", "C", 1, -1, vbBinaryCompare)
                tmpStr = Replace(tmpStr, "FDO", "C", 1, -1, vbBinaryCompare)
                tmpStr = Replace(tmpStr, "FTO", "C", 1, -1, vbBinaryCompare)
                FileData(Index).ColumnAlignmentString = tmpStr & ",L"
                FileData(Index).HeaderAlignmentString = tmpStr & ",L"
            End If
            tmpHeaderWidth = tmpHeaderWidth & ",1"
            tmpHeaderTitle = tmpHeaderTitle & ",System"
            If tmpHeaderColor <> "" Then tmpHeaderColor = tmpHeaderColor & ",12632256"
        End If
        
        FileData(Index).SetMainHeaderValues tmpHeaderWidth, tmpHeaderTitle, tmpHeaderColor
        FileData(Index).CreateDecorationObject "Marker1", "L,L,T,B,R", "-1,2,2,2,2", Str(LightestBlue) & "," & Str(LightGray) & "," & Str(LightGray) & "," & Str(DarkGray) & "," & Str(DarkGray)
        FileData(Index).ShowHorzScroller True
        FileData(Index).AutoSizeByHeader = True
        FileData(Index).AutoSizeColumns
        If ReturnColor >= 0 Then FileData(Index).EmptyAreaRightColor = ReturnColor
        chkTabIsVisible(Index).Visible = True
        lblTabName(Index).Visible = True
        lblTabName(Index).Refresh
        lblTabShadow(Index).Visible = True
        lblTabShadow(Index).Refresh
        chkTabIsVisible(Index).Enabled = True
        chkTabIsVisible(Index).Value = 1
        tmpStr = GetIniEntry(myIniFullName, myIniSection, "", SectionKey & "_HIDDEN_MODE", "NO")
        If Left(tmpStr, 1) = "Y" Then
            chkTabIsVisible(Index).Value = 0
            chkTabIsVisible(Index).Enabled = False
        End If
        FileData(Index).myName = "MAIN_FILEDATA_" & CStr(Index)
        'FileData(Index).EnableHeaderSizing True
        
        tmpSqlFields = ""
        tmpSqlKey = ""
        tmpSqlConfig = GetIniEntry(myIniFullName, myIniSection, "", SectionKey & "_AODB_READER", "")
        If tmpSqlConfig <> "" Then
            ItemNo = 1
            tmpFields = GetRealItem(tmpSqlConfig, ItemNo, "|")
            tmpKeys = GetRealItem(tmpSqlConfig, ItemNo + 1, "|")
            While (tmpFields <> "") Or (tmpKeys <> "")
                If (tmpFields = "") Or (tmpFields = "?") Then tmpFields = FileData(Index).LogicalFieldList
                tmpSqlFields = tmpSqlFields & tmpFields & Chr(14)
                tmpSqlKey = tmpSqlKey & tmpKeys & Chr(14)
                ItemNo = ItemNo + 2
                tmpFields = GetRealItem(tmpSqlConfig, ItemNo, "|")
                tmpKeys = GetRealItem(tmpSqlConfig, ItemNo + 1, "|")
            Wend
        End If
        tmpStr = GetIniEntry(myIniFullName, myIniSection, "", SectionKey & "_DIALOG_ZONE", "")
        If tmpStr <> "" Then CreateDelayPanel Index, tmpStr
        
        If tmpSqlFields <> "" Then tmpSqlFields = Left(tmpSqlFields, Len(tmpSqlFields) - 1)
        If tmpSqlKey <> "" Then tmpSqlKey = Left(tmpSqlKey, Len(tmpSqlKey) - 1)
        TabCfgLine = FileData(Index).myName & Chr(15)
        TabCfgLine = TabCfgLine & "0" & Chr(15)
        TabCfgLine = TabCfgLine & "V" & Chr(15)
        TabCfgLine = TabCfgLine & "IMP" & Chr(15)
        TabCfgLine = TabCfgLine & "SYSTEM" & Chr(15)
        TabCfgLine = TabCfgLine & GetRealItem(tmpSqlConfig, 0, "|") & Chr(15)
        TabCfgLine = TabCfgLine & tmpSqlFields & Chr(15)
        TabCfgLine = TabCfgLine & tmpSqlKey & Chr(15)
        TabCfgLine = TabCfgLine & FileData(Index).LogicalFieldList & Chr(15)
        tmpStr = GetIniEntry(myIniFullName, myIniSection, "", SectionKey & "_EDITOR_CAPT", "")
        tmpItem = GetRealItem(tmpStr, 1, "|")
        If tmpItem = "" Then tmpStr = tmpStr & "|" & tmpStr
        TabCfgLine = TabCfgLine & tmpStr & Chr(15)
        tmpStr = GetIniEntry(myIniFullName, myIniSection, "", SectionKey & "_ACTION_CODE", "")
        TabCfgLine = TabCfgLine & tmpStr & Chr(15)
        tmpStr = GetIniEntry(myIniFullName, myIniSection, "", SectionKey & "_ACTION_CALL", tmpStr)
        TabCfgLine = TabCfgLine & tmpStr & Chr(15)
        tmpStr = GetIniEntry(myIniFullName, myIniSection, "", SectionKey & "_EDITOR_TYPE", "")
        TabCfgLine = TabCfgLine & tmpStr & Chr(15)
        tmpStr = GetIniEntry(myIniFullName, myIniSection, "", SectionKey & "_EDIT_FIELDS", "")
        tmpItem = ""
        tmpPrnt = ""
        If tmpStr <> "" Then
            tmpItem = GetRealItem(tmpStr, 1, "|")
            tmpPrnt = GetRealItem(tmpStr, 2, "|")
            tmpStr = GetRealItem(tmpStr, 0, "|")
            If tmpItem = "" Then tmpItem = tmpStr
            SetTabEditColumns Index, tmpItem
            If tmpPrnt = "" Then tmpPrnt = tmpItem
        End If
        tmpStr = tmpStr & "|" & tmpItem & "|" & tmpSortKeys & "|" & tmpPrnt
        TabCfgLine = TabCfgLine & tmpStr & Chr(15)
        tmpStr = GetIniEntry(myIniFullName, myIniSection, "", SectionKey & "_EDITOR_MAPP", "")
        TabCfgLine = TabCfgLine & tmpStr & Chr(15)
        HiddenData.CheckTabConfig TabCfgLine
    End If
End Sub
Private Sub SetTabEditColumns(Index As Integer, EditFields As String)
    Dim tmpCols As String
    Dim tmpFields As String
    Dim CurItem As Long
    Dim tmpFldNam As String
    tmpFields = FileData(Index).LogicalFieldList
    tmpCols = ""
    CurItem = 0
    tmpFldNam = GetRealItem(tmpFields, CurItem, ",")
    While tmpFldNam <> ""
        If InStr(EditFields, tmpFldNam) = 0 Then tmpCols = tmpCols & CStr(CurItem) & ","
        CurItem = CurItem + 1
        tmpFldNam = GetRealItem(tmpFields, CurItem, ",")
    Wend
    If tmpCols <> "" Then
        tmpCols = Left(tmpCols, Len(tmpCols) - 1)
        FileData(Index).NoFocusColumns = tmpCols
    End If
End Sub
Private Sub FormatTimeFields(UseTab As TABLib.Tab, ColAlign As String)
    Dim ItemNo As Long
    Dim tmpStr As String
    ItemNo = 0
    tmpStr = Trim(GetRealItem(ColAlign, ItemNo, ","))
    While tmpStr <> ""
        Select Case tmpStr
            Case "FDT", "CDT"
                UseTab.DateTimeSetColumn ItemNo
                UseTab.DateTimeSetInputFormatString ItemNo, "YYYYMMDDhhmm"
                UseTab.DateTimeSetOutputFormatString ItemNo, FormatDateTime
            Case "FDP", "CD"
                UseTab.DateTimeSetColumn ItemNo
                UseTab.DateTimeSetInputFormatString ItemNo, "YYYYMMDDhhmm"
                UseTab.DateTimeSetOutputFormatString ItemNo, FormatDatePart
            Case "FTP", "CT"
                UseTab.DateTimeSetColumn ItemNo
                UseTab.DateTimeSetInputFormatString ItemNo, "YYYYMMDDhhmm"
                UseTab.DateTimeSetOutputFormatString ItemNo, FormatTimePart
            Case "FDO"
                UseTab.DateTimeSetColumn ItemNo
                UseTab.DateTimeSetInputFormatString ItemNo, "YYYYMMDD"
                UseTab.DateTimeSetOutputFormatString ItemNo, FormatDateOnly
            Case "FTO"
                UseTab.DateTimeSetColumn ItemNo
                UseTab.DateTimeSetInputFormatString ItemNo, "hhmm"
                UseTab.DateTimeSetOutputFormatString ItemNo, FormatTimeOnly
            Case Else
                UseTab.DateTimeResetColumn ItemNo
        End Select
        ItemNo = ItemNo + 1
        tmpStr = GetRealItem(ColAlign, ItemNo, ",")
    Wend
End Sub
Private Sub CleanSystemFields(UseTab As TABLib.Tab)
    Dim SysFldLst As String
    Dim SysFldDat As String
    Dim CurLine As Long
    Dim MaxLine As Long
    Dim tmpData As String
    SysFldLst = "'NO','ID','DO','RM','SY','ST','SK',LSTU"
    MaxLine = UseTab.GetLineCount - 1
    For CurLine = 0 To MaxLine
        tmpData = Trim(UseTab.GetFieldValue(CurLine, "LSTU"))
        'If tmpData = "" Then tmpData = UseTab.GetFieldValue(CurLine, "CDAT")
        SysFldDat = CStr(CurLine + 1) & ",,,,,,," & tmpData
        UseTab.SetFieldValues CurLine, SysFldLst, SysFldDat
    Next
End Sub

Private Sub chkMax_Click(Index As Integer)
    If chkMax(Index).Value = 1 Then
        chkMax(Index).BackColor = LightGreen
        chkMax(Index).Caption = "-"
    Else
        chkMax(Index).BackColor = MyOwnButtonFace
        chkMax(Index).Caption = "+"
    End If
    ArrangeMaximizedTab Index
    ArrangeLayout -1
    FileData(Index).SetFocus
End Sub

Private Sub chkOnTop_Click()
    If chkOnTop.Value = 1 Then
        chkOnTop.BackColor = LightGreen
        SetFormOnTop Me, True
    Else
        chkOnTop.BackColor = MyOwnButtonFace
        SetFormOnTop Me, False
    End If
End Sub

Private Sub chkTabIsVisible_Click(Index As Integer)
    Dim tmpTag As String
    Dim tmpList As String
    Dim CountSplitter As Integer
    Dim i As Integer
    Dim j As Integer
    If Index = TabIsMaximized Then ArrangeMaximizedTab Index
    If chkTabIsVisible(Index).Value <> 2 Then
        For i = 0 To chkTabIsVisible.UBound
            FileData(i).Visible = False
            fraCrSplitter(i).Visible = False
            fraHzSplitter(i).Visible = False
        Next
        For i = 0 To fraVtSplitter.UBound
            fraVtSplitter(i).Visible = False
        Next
        CountSplitter = 0
        CountMainTabs = 0
        CountSubTabs = 0
        tmpList = ""
        tmpTag = ""
        For i = 0 To chkTabIsVisible.UBound
            If chkTabIsVisible(i).Value = 1 Then
                tmpList = tmpList & CStr(i)
                If Left(chkTabIsVisible(i).Tag, 1) = "L" Then
                    chkTabIsVisible(i).Tag = "L,-1"
                    If CountMainTabs > 0 Then
                        CountSplitter = CountSplitter + 1
                        tmpTag = "L," & CStr(CountSplitter)
                    End If
                    CountMainTabs = CountMainTabs + 1
                Else
                    chkTabIsVisible(i).Tag = "R,-1"
                    If CountSubTabs > 0 Then
                        CountSplitter = CountSplitter + 1
                        tmpTag = "R," & CStr(CountSplitter)
                    End If
                    CountSubTabs = CountSubTabs + 1
                End If
                If tmpTag <> "" Then
                    chkTabIsVisible(j).Tag = tmpTag
                    fraHzSplitter(CountSplitter).Visible = True
                    fraCrSplitter(CountSplitter).Visible = True
                    fraHzSplitter(CountSplitter).Tag = tmpList
                    tmpTag = ""
                End If
                FileData(i).Visible = True
                tmpList = CStr(i) & ","
                j = i
            End If
        Next
        If (CountMainTabs > 0) And (CountSubTabs > 0) Then
            fraVtSplitter(0).Visible = True
        Else
            For i = 0 To fraCrSplitter.UBound
                fraCrSplitter(i).Visible = False
            Next
        End If
        ArrangeLayout -1
        LastFocusIndex = -1
        If FileData(Index).Visible Then
            If chkTabIsVisible(Index).Value = 1 Then
                FileData(Index).SetFocus
                LastFocusIndex = Index
            End If
        End If
        If chkTabIsVisible(Index).Value = 0 Then
            If MainLifeStyle Then
                If (CountMainTabs + CountSubTabs) = 0 Then
                    DrawBackGround WorkArea, WorkAreaColor, True, True
                End If
            End If
        End If
    End If
End Sub

Private Sub chkTask_Click(Index As Integer)
    Dim i As Integer
    If chkTask(Index).Value = 1 Then
        InitFlightPanel True
        PushWorkButton "READAODB", 1
        For i = 0 To chkTabIsVisible.UBound
            If chkTabIsVisible(i).Value = 1 Then
                FileData(i).SetFocus
                Exit For
            End If
        Next
    End If
End Sub
Public Sub PushWorkButton(ButtonContext As String, SetValue As Integer)
    Dim i As Integer
    For i = 0 To chkWork.UBound
        If chkWork(i).Visible Then
            If InStr(chkWork(i).Tag, ButtonContext) > 0 Then
                chkWork(i).Value = SetValue
                Exit For
            End If
        End If
    Next
End Sub
Public Sub HighlightWorkButton(ButtonContext As String, UseForeColor As Long, UseBackColor As Long)
    Dim i As Integer
    For i = 0 To chkWork.UBound
        If chkWork(i).Visible Then
            If InStr(chkWork(i).Tag, ButtonContext) > 0 Then
                chkWork(i).ForeColor = UseForeColor
                chkWork(i).BackColor = UseBackColor
                Exit For
            End If
        End If
    Next
End Sub
Private Sub chkWork_Click(Index As Integer)
    Dim tmpTag As String
    Dim tmpFunc As String
    Dim tmpTyp As String
    Dim tmpToggleIdx As Integer
    tmpTag = chkWork(Index).Tag
    tmpFunc = GetRealItem(tmpTag, 0, ",")
    tmpTyp = GetRealItem(tmpTag, 1, ",")
    Select Case tmpTyp
        Case "T"
            tmpToggleIdx = Val(GetRealItem(tmpTag, 2, ","))
            If chkWork(Index).Value = 1 Then
                chkWork(tmpToggleIdx).Value = 0
            Else
                chkWork(tmpToggleIdx).Value = 1
            End If
        Case "D2"
            tmpToggleIdx = Val(GetRealItem(tmpTag, 2, ","))
            If chkWork(Index).Value = 1 Then
                chkWork(tmpToggleIdx).Enabled = False
                chkWork(tmpToggleIdx).Value = 1
            Else
                chkWork(tmpToggleIdx).Value = 0
                If Not ReadOnlyUser Then chkWork(tmpToggleIdx).Enabled = True
            End If
        Case Else
    End Select
    If chkWork(Index).Value = 1 Then
        chkWork(Index).BackColor = LightGreen
        chkWork(Index).ForeColor = vbBlack
        chkWork(Index).Refresh
        CheckWorkButton Me, chkWork(Index)
    Else
        UnCheckWorkButton Me, chkWork(Index)
        chkWork(Index).ForeColor = vbButtonText
        chkWork(Index).BackColor = MyOwnButtonFace
        chkWork(Index).Refresh
    End If
    If (Not TabEditInline) And (Not RefreshOnBc) And (Not ExcelJustOpened) Then
        If LastFocusIndex >= 0 Then
            If chkTabIsVisible(LastFocusIndex).Value = 1 Then
                FileData(LastFocusIndex).SetFocus
            End If
        End If
    End If
    ExcelJustOpened = False
End Sub

Private Sub CheckWorkButton(CurForm As Form, CurButton As CheckBox)
    Dim tmpTag As String
    Dim tmpFunc As String
    Dim tmpTyp As String
    tmpTag = CurButton.Tag
    tmpFunc = GetRealItem(tmpTag, 0, ",")
    tmpTyp = GetRealItem(tmpTag, 1, ",")
    Select Case tmpFunc
        Case "TAB_INSERT"
            If CurButton.Enabled Then HandleTabInsert CurForm, CurButton
        Case "TAB_EDIT_INSERT"
            HandleTabEditInsert CurForm, CurButton
        Case "TAB_UPDATE"
            If CurButton.Enabled Then HandleTabUpdate CurForm, CurButton
        Case "TAB_EDIT_UPDATE"
            HandleTabEditUpdate CurForm, CurButton
        Case "TAB_DELETE"
            HandleTabDelete CurForm, CurButton
        Case "TAB_READ"
            HandleTabRead CurForm, CurButton
        Case "AUTO_TAB_READ"
            HandleAutoTabRead CurForm, CurButton
        Case "TAB_PRINT"
            HandleTabPrint CurForm, CurButton
        Case "AODB_FILTER"
            HandleAodbFilter CurForm, CurButton
        Case "EXTRACT"
            HandleExtract CurForm, CurButton
        Case "FILEOPEN"
            HandleFileOpen CurForm, CurButton
        Case "READAODB"
            HandleReadAodb CurForm, CurButton
        Case "READLOCAL"
            HandleReadLocal CurForm, CurButton
        Case "SAVEAODB"
            If CurButton.Enabled Then
                HandleSaveAodb True
                CurButton.Value = 0
            End If
        Case "AUTO_SAVEAODB"
            HandleAutoSaveAodb CurForm, CurButton
        Case "SAVELOCAL"
            HandleSaveLocal CurForm, CurButton
        Case "EXCEL"
            HandleExcel CurForm, CurButton
        Case Else
            'If tmpTyp <> "T" Then CurButton.Value = 0
    End Select
End Sub
Private Sub HandleTabPrint(CurForm As Form, CurButton As CheckBox)
    Dim Index As Integer
    Dim CurOnTopValue As Integer
    Dim CurFields As String
    Dim CurHead As String
    Dim CurAlign As String
    Dim CurWidth As String
    Dim tmpPrintFields As String
    Dim tmpCurFields As String
    Dim tmpCurHead As String
    Dim tmpCurAlign As String
    Dim tmpCurWidth As String
    Dim tmpArrData As String
    Dim tmpDepData As String
    Dim tmpActData As String
    Dim FieldCnt As Long
    Dim ItemNo As Long
    Dim tmpFldNam As String
    Dim tmpData As String
    Dim tmpCall As String
    Dim tmpCode As String
    Dim tmpFltRel As String
    Dim tmpCapt As String
    
    tmpPrintFields = HiddenData.GetConfigValues(FileData(LastFocusIndex).myName, "EDIT")
    tmpPrintFields = GetRealItem(tmpPrintFields, 3, "|")
    
    tmpCurFields = FileData(LastFocusIndex).LogicalFieldList
    tmpCurHead = FileData(LastFocusIndex).GetHeaderText
    tmpCurWidth = FileData(LastFocusIndex).HeaderLengthString
    tmpCurAlign = FileData(LastFocusIndex).ColumnAlignmentString
    
    CurFields = ""
    CurHead = ""
    CurWidth = ""
    CurAlign = ""
    ItemNo = 0
    tmpFldNam = GetRealItem(tmpPrintFields, ItemNo, ",")
    While tmpFldNam <> ""
        CurFields = CurFields & tmpFldNam & ","
        CurHead = CurHead & GetFieldValue(tmpFldNam, tmpCurHead, tmpCurFields) & ","
        CurWidth = CurWidth & GetFieldValue(tmpFldNam, tmpCurWidth, tmpCurFields) & ","
        CurAlign = CurAlign & GetFieldValue(tmpFldNam, tmpCurAlign, tmpCurFields) & ","
        ItemNo = ItemNo + 1
        tmpFldNam = GetRealItem(tmpPrintFields, ItemNo, ",")
    Wend
    FieldCnt = ItemCount(CurFields, ",") - 1
    tmpArrData = ""
    tmpDepData = ""
    For Index = 0 To 5
        tmpArrData = tmpArrData & lblArr(Index).Caption & ","
        tmpDepData = tmpDepData & lblDep(Index).Caption & ","
    Next
    tmpActData = ""
    For Index = 6 To 8
        tmpData = lblArr(Index).Caption
        If tmpData = "" Then tmpData = lblDep(Index).Caption
        tmpActData = tmpActData & tmpData & ","
    Next
    tmpCall = HiddenData.GetConfigValues(FileData(LastFocusIndex).myName, "CALL")
    tmpCode = HiddenData.GetConfigValues(FileData(LastFocusIndex).myName, "CODE")
    If tmpCall = "ROT" Then tmpFltRel = tmpCode Else tmpFltRel = tmpCall
    Select Case tmpFltRel
        Case "ARR"
            tmpDepData = "--,--,--,--,--,--,"
        Case "DEP"
            tmpArrData = "--,--,--,--,--,--,"
        Case Else
    End Select
    tmpCapt = HiddenData.GetConfigValues(FileData(LastFocusIndex).myName, "CAPT")
    tmpCapt = GetRealItem(tmpCapt, 1, "|")
    
    CurOnTopValue = chkOnTop.Value
    chkOnTop.Value = 0
    Dim rpt As New PrintData
    Load rpt
    rpt.InitReportHeader "Print Report: " & Me.Caption, tmpCapt, tmpArrData, tmpDepData, tmpActData
    rpt.InitPrintLayout FileData(LastFocusIndex), FieldCnt, CurHead, CurFields, CurAlign, CurWidth
    rpt.Show , Me
    rpt.Refresh
    chkOnTop.Value = CurOnTopValue
    CurButton.Value = 0
End Sub

Private Sub HandleTabEditInsert(CurForm As Form, CurButton As CheckBox)
    Dim LineNo As Long
    Dim ColNo As Long
    If CurButton.Enabled = True Then
        If CurButton.Value = 1 Then
            PushWorkButton "TAB_EDIT_UPDATE", 0
            PushWorkButton "AUTO_TAB_READ", 0
            TabEditInline = True
            TabEditInsert = True
            TabEditUpdate = False
            If LastFocusIndex >= 0 Then
                RemoveEmptyTabLines LastFocusIndex
                InsertEmptyTabLine LastFocusIndex, -1
                FileData(LastFocusIndex).EnableInlineEdit True
                FileData(LastFocusIndex).PostEnterBehavior = 3
                FileData(LastFocusIndex).InplaceEditSendKeyEvents = True
                LineNo = FileData(LastFocusIndex).GetCurrentSelected
                If LineNo >= 0 Then
                    InsertEmptyTabLine LastFocusIndex, LineNo
                Else
                    LineNo = FileData(LastFocusIndex).GetLineCount - 1
                    FileData(LastFocusIndex).OnVScrollTo LineNo - VisibleTabLines(FileData(LastFocusIndex)) + 1
                End If
                ColNo = GetFirstEditColNo(LastFocusIndex)
                If ColNo >= 0 Then
                    FileData(LastFocusIndex).SetCurrentSelection LineNo
                    FileData(LastFocusIndex).SetLineTag LineNo, FileData(LastFocusIndex).GetLineValues(LineNo)
                    FileData(LastFocusIndex).SetFocus
                    FileData(LastFocusIndex).SetInplaceEdit LineNo, ColNo, False
                    TabEditIsActive = True
                    TabEditCurLineNo = LineNo
                    TabEditCurColNo = ColNo
                Else
                    CurButton.Value = 0
                End If
            End If
        Else
            If LastFocusIndex >= 0 Then
                FileData(LastFocusIndex).EnableInlineEdit False
                RemoveEmptyTabLines LastFocusIndex
                InsertEmptyTabLine LastFocusIndex, -1
            End If
            TabEditInline = False
            TabEditInsert = False
            TabEditIsActive = False
            TabEditCurLineNo = -1
            TabEditCurColNo = -1
        End If
    End If
End Sub
Private Sub InsertEmptyTabLine(Index As Integer, UseLineNo As Long)
    Dim tmpData As String
    Dim LineNo As Long
    If UseLineNo >= 0 Then LineNo = UseLineNo Else LineNo = FileData(Index).GetLineCount
    tmpData = CreateEmptyLine(FileData(Index).LogicalFieldList)
    FileData(Index).InsertTextLineAt LineNo, tmpData, False
    FileData(Index).SetLineStatusValue LineNo, 99
End Sub
Private Sub RemoveEmptyTabLines(Index As Integer)
    Dim MaxLine As Long
    Dim CurLine As Long
    Dim SelLine As Long
    Dim LineStatus As Long
    SelLine = FileData(Index).GetCurrentSelected
    MaxLine = FileData(Index).GetLineCount - 1
    CurLine = 0
    While CurLine <= MaxLine
        LineStatus = FileData(Index).GetLineStatusValue(CurLine)
        If LineStatus = 99 Then
            FileData(Index).DeleteLine CurLine
            If CurLine < SelLine Then SelLine = SelLine - 1
            CurLine = CurLine - 1
            MaxLine = MaxLine - 1
        End If
        CurLine = CurLine + 1
    Wend
    If SelLine >= 0 Then FileData(Index).SetCurrentSelection SelLine
End Sub
Private Sub HandleTabEditUpdate(CurForm As Form, CurButton As CheckBox)
    Dim LineNo As Long
    Dim ColNo As Long
    If CurButton.Enabled = True Then
        If CurButton.Value = 1 Then
            PushWorkButton "TAB_EDIT_INSERT", 0
            PushWorkButton "AUTO_TAB_READ", 0
            TabEditInline = True
            TabEditUpdate = True
            TabEditInsert = False
            If LastFocusIndex >= 0 Then
                RemoveEmptyTabLines LastFocusIndex
                InsertEmptyTabLine LastFocusIndex, -1
                TabEditCurLineNo = -1
                TabEditCurColNo = -1
                FileData(LastFocusIndex).EnableInlineEdit True
                FileData(LastFocusIndex).PostEnterBehavior = 3
                FileData(LastFocusIndex).InplaceEditSendKeyEvents = True
                LineNo = FileData(LastFocusIndex).GetCurrentSelected
                If LineNo >= 0 Then
                    ColNo = GetFirstEditColNo(LastFocusIndex)
                    If ColNo >= 0 Then
                        FileData(LastFocusIndex).SetLineTag LineNo, FileData(LastFocusIndex).GetLineValues(LineNo)
                        FileData(LastFocusIndex).SetFocus
                        FileData(LastFocusIndex).SetInplaceEdit LineNo, ColNo, False
                        TabEditIsActive = True
                        TabEditCurLineNo = LineNo
                        TabEditCurColNo = ColNo
                    End If
                End If
            End If
        Else
            If LastFocusIndex >= 0 Then
                FileData(LastFocusIndex).EnableInlineEdit False
                RemoveEmptyTabLines LastFocusIndex
                InsertEmptyTabLine LastFocusIndex, -1
            End If
            TabEditInline = False
            TabEditUpdate = False
            TabEditIsActive = False
            TabEditCurLineNo = -1
        End If
    End If
End Sub
Private Sub HandleAutoSaveAodb(CurForm As Form, CurButton As CheckBox)
    If CurButton.Value = 1 Then
        AutoSaveAodb = True
    Else
        AutoSaveAodb = False
    End If
End Sub
Private Sub HandleAutoTabRead(CurForm As Form, CurButton As CheckBox)
    Dim DoIt As Boolean
    If CurButton.Value = 1 Then
        DoIt = False
        If CurButton.Enabled Then
            If LastFocusIndex >= 0 Then
                If InStr(FileData(LastFocusIndex).LogicalFieldList, "READ") > 0 Then
                    DoIt = True
                End If
            End If
        End If
        If DoIt Then
            PushWorkButton "TAB_EDIT_UPDATE", 0
            PushWorkButton "TAB_EDIT_INSERT", 0
            PushWorkButton "AUTO_SAVEAODB", 1
            AutoTabRead = True
        Else
            CurButton.Value = 0
        End If
    Else
        AutoTabRead = False
        PushWorkButton "AUTO_SAVEAODB", 0
    End If
End Sub
Private Sub HandleExcel(CurForm As Form, CurButton As CheckBox)
    Dim CurFile As String
    Dim CurLine As Long
    Dim MaxLine As Long
    Dim CurCol As Long
    Dim MaxCol As Long
    Dim i As Integer
    Dim j As Integer
    Dim tmpLineData As String
    Dim tmpData As String
    Dim tmpCall As String
    Dim tmpCode As String
    Dim tmpFltRel As String
    Dim tmpCapt As String
    Dim tmpType As String
    Dim tmpPath As String
    Dim tmpFlno As String
    Dim tmpDate As String
    
    chkOnTop.Value = 0
    On Error Resume Next
    If (Not (MsExcel Is Nothing)) Or (CurButton.Value = 0) Then
        ExcelShutDown = True
        MsWkBook.Saved = True
        'MsWkBook.Close
        MsExcel.Quit
        ExcelShutDown = False
    End If
    If CurButton.Value = 1 Then
        ExcelJustOpened = True
        tmpData = GetIniEntry(myIniFullName, "MAIN", "", "EXCEL_UPDATES", "NO")
        If tmpData = "YES" Then ExcelUpdateAllowed = True Else ExcelUpdateAllowed = False
        tmpData = GetIniEntry(myIniFullName, "MAIN", "", "EXCEL_RO_SAVE", "NO")
        If tmpData = "YES" Then ExcelRoSaveAllowed = True Else ExcelRoSaveAllowed = False
        tmpType = HiddenData.GetConfigValues(FileData(LastFocusIndex).myName, "MASK")
        tmpType = GetRealItem(tmpType, 1, ",")
        tmpCall = HiddenData.GetConfigValues(FileData(LastFocusIndex).myName, "CALL")
        tmpCode = HiddenData.GetConfigValues(FileData(LastFocusIndex).myName, "CODE")
        If tmpCall = "ROT" Then tmpFltRel = tmpCode Else tmpFltRel = tmpCall
        Select Case tmpFltRel
            Case "ARR"
                tmpFlno = Trim(Replace(lblArr(0).Tag, " ", "", 1, -1, vbBinaryCompare))
                tmpDate = Trim(Left(lblArr(2).Tag, 8))
            Case "DEP"
                tmpFlno = Trim(Replace(lblDep(0).Tag, " ", "", 1, -1, vbBinaryCompare))
                tmpDate = Trim(Left(lblDep(2).Tag, 8))
            Case "ROT"
                tmpFlno = Trim(Replace(lblDep(0).Tag, " ", "", 1, -1, vbBinaryCompare))
                tmpDate = Trim(Left(lblDep(2).Tag, 8))
                If tmpFlno = "" Then
                    tmpFlno = Trim(Replace(lblArr(0).Tag, " ", "", 1, -1, vbBinaryCompare))
                    tmpDate = Trim(Left(lblArr(2).Tag, 8))
                End If
        End Select
        If tmpFlno = "" Then tmpFlno = "NONE"
        If tmpDate = "" Then tmpDate = "NONE"
        tmpPath = GetIniEntry(myIniFullName, "MAIN", "", "EXCEL_WKBOOKS", "C:\tmp")
        CurFile = tmpPath & "\" & tmpType & "_" & tmpFltRel & "_" & tmpFlno & "_" & tmpDate & ".xls"
        Set MsExcel = New Application
        MsExcel.Workbooks.Add
        Set MsWkBook = MsExcel.Workbooks(1)
        Set MsWkSheet = MsWkBook.Worksheets(1)
        Kill CurFile
        MsWkBook.SaveAs CurFile
        MsExcel.WindowState = xlNormal
        MsExcel.Top = (Me.Top / 20) + 40
        MsExcel.Left = Me.Left / 20
        MsExcel.Width = Me.Width / 20
        MsExcel.Height = Me.Height / 20
        MsExcel.Visible = True
        MsWkSheet.Name = "LogData"
        MsExcel.Windows.Arrange ArrangeStyle:=xlVertical
        MsExcel.Windows(1).WindowState = xlMaximized
        ExcelFields = HiddenData.GetConfigValues(FileData(LastFocusIndex).myName, "EDIT")
        ExcelFields = GetRealItem(ExcelFields, 3, "|")
        MaxLine = FileData(LastFocusIndex).GetLineCount - 1
        MaxCol = ItemCount(ExcelFields, ",")
        MsWkSheet.Cells.NumberFormat = "@"
        For CurLine = 0 To MaxLine
            i = CInt(CurLine) + 1
            tmpLineData = FileData(LastFocusIndex).GetFieldValues(CurLine, ExcelFields)
            For CurCol = 1 To MaxCol
                j = CInt(CurCol)
                tmpData = GetItem(tmpLineData, j, ",")
                tmpData = CleanString(tmpData, SERVER_TO_CLIENT, False)
                MsWkSheet.Cells(i, j) = tmpData
            Next
        Next
        tmpData = "1:" & CStr(j)
        MsWkSheet.Range(tmpData).Columns.AutoFit
        tmpData = "1:" & CStr(i)
        MsWkSheet.Range(tmpData).Rows.AutoFit
        MsWkBook.Save
    End If
End Sub

Private Sub UnCheckWorkButton(CurForm As Form, CurButton As CheckBox)
    Dim tmpTag As String
    Dim tmpFunc As String
    Dim tmpTyp As String
    tmpTag = CurButton.Tag
    tmpFunc = GetRealItem(tmpTag, 0, ",")
    tmpTyp = GetRealItem(tmpTag, 1, ",")
    Select Case tmpFunc
        Case "TAB_READ"
            HandleTabRead CurForm, CurButton
        Case "AUTO_TAB_READ"
            HandleAutoTabRead CurForm, CurButton
        Case "TAB_EDIT_INSERT"
            HandleTabEditInsert CurForm, CurButton
        Case "TAB_EDIT_UPDATE"
            HandleTabEditUpdate CurForm, CurButton
        Case "AUTO_SAVEAODB"
            HandleAutoSaveAodb CurForm, CurButton
        Case "EXCEL"
            HandleExcel CurForm, CurButton
        Case Else
    End Select
End Sub
Private Sub HandleTabRead(CurForm As Form, CurButton As CheckBox)
    Dim LineNo As Long
    If CurButton.Value = 1 Then
        If LastFocusIndex < 0 Then LastFocusIndex = 0
        Set ReadMaskButton = CurButton
        Set ReadMaskTab = FileData(LastFocusIndex)
        ReadMask.Left = Me.Left + ((Me.Width - ReadMask.Width) / 2)
        ReadMask.Top = Me.Top + ((Me.Height - ReadMask.Height) / 2)
        CheckMonitorArea ReadMask
        ReadMask.Show , Me
        SetFormOnTop ReadMask, True
        ReadMaskIsOpen = True
        If LastFocusIndex >= 0 Then
            LineNo = FileData(LastFocusIndex).GetCurrentSelected
            If LineNo < 0 Then LineNo = 0
            FileData(LastFocusIndex).SetCurrentSelection LineNo
        End If
    Else
        Unload ReadMask
        ReadMaskIsOpen = False
    End If
End Sub
Private Sub HandleSaveAodb(UsingRelCmd As Boolean)
    Dim UseRelCmd As Boolean
    Dim CntInsert As Long
    Dim CntUpdate As Long
    Dim CntDelete As Long
    Dim CurLine As Long
    Dim tmpUrno As String
    Dim tmpArea As String
    Dim tmpMaskType As String
    Dim tmpIndx As String
    Dim tmpEditor As String
    Dim tmpLayout As String
    Dim tmpSqlTab As String
    Dim tmpSqlFld As String
    Dim tmpSqlDat As String
    Dim tmpSqlKey As String
    Dim tmpRelHdlCmd As String
    Dim Index As Integer
    
    'UfisServer.ConnectToCeda
    For Index = 0 To chkTabIsVisible.UBound
        If chkTabIsVisible(Index).Visible Then
            UseRelCmd = UsingRelCmd
            Screen.MousePointer = 11
            SetSortLineNoAsc Index
            tmpSqlTab = HiddenData.GetConfigValues(FileData(Index).myName, "DTAB")
            tmpSqlFld = HiddenData.GetConfigValues(FileData(Index).myName, "DFLD")
            tmpSqlFld = CreateCleanFieldList(tmpSqlFld)
            tmpRelHdlCmd = ""
            FileData(Index).SetInternalLineBuffer True
            CntInsert = Val(FileData(Index).GetLinesByStatusValue(2, 0))
            If CntInsert > 0 Then
                UfisServer.UrnoPoolInit 50
                UfisServer.UrnoPoolPrepare CntInsert
                tmpRelHdlCmd = tmpRelHdlCmd & "*CMD*," & tmpSqlTab & ",IRT," & CStr(ItemCount(tmpSqlFld, ",")) & "," & tmpSqlFld & vbLf
                While CntInsert >= 0
                    CntInsert = FileData(Index).GetNextResultLine
                    If CntInsert >= 0 Then
                        tmpUrno = UfisServer.UrnoPoolGetNext
                        FileData(Index).SetFieldValues CntInsert, "URNO", tmpUrno
                        tmpSqlDat = FileData(Index).GetFieldValues(CntInsert, tmpSqlFld)
                        tmpSqlDat = CleanNullValues(tmpSqlDat)
                        tmpRelHdlCmd = tmpRelHdlCmd & tmpSqlDat & vbLf
                        FileData(Index).SetLineColor CntInsert, vbBlack, vbWhite
                        FileData(Index).SetLineStatusValue CntInsert, 0
                    End If
                Wend
            End If
            CntUpdate = Val(FileData(Index).GetLinesByStatusValue(1, 0))
            'If CntUpdate < 3 Then
            '    UseRelCmd = False
            'End If
            If CntUpdate > 0 Then
                tmpRelHdlCmd = tmpRelHdlCmd & "*CMD*," & tmpSqlTab & ",URT," & CStr(ItemCount(tmpSqlFld, ",")) & "," & tmpSqlFld & ",[URNO=:VURNO]" & vbLf
                While CntUpdate >= 0
                    CntUpdate = FileData(Index).GetNextResultLine
                    If CntUpdate >= 0 Then
                        tmpUrno = FileData(Index).GetFieldValue(CntUpdate, "URNO")
                        tmpSqlDat = FileData(Index).GetFieldValues(CntUpdate, tmpSqlFld)
                        tmpSqlDat = CleanNullValues(tmpSqlDat)
                        tmpRelHdlCmd = tmpRelHdlCmd & tmpSqlDat & "," & tmpUrno & vbLf
                        If Not UseRelCmd Then
                            tmpSqlKey = "WHERE URNO=" & tmpUrno
                            UfisServer.CallCeda CedaDataAnswer, "URT", tmpSqlTab, tmpSqlFld, tmpSqlDat, tmpSqlKey, "", 0, False, False
                        End If
                        If Index = Val(DelayPanel.Tag) Then
                            If DelayPanel.Visible Then
                                tmpArea = FileData(Index).GetFieldValue(CntUpdate, "AREA")
                                If tmpArea = "B" Then
                                    tmpIndx = FileData(Index).GetFieldValue(CntUpdate, "INDX")
                                    CheckDlyIndex Index, CntUpdate, tmpIndx, False
                                End If
                            End If
                        End If
                        FileData(Index).SetLineColor CntUpdate, vbBlack, vbWhite
                        FileData(Index).SetLineStatusValue CntUpdate, 0
                    End If
                Wend
            End If
            CntDelete = Val(FileData(Index).GetLinesByStatusValue(3, 0))
            If CntDelete > 0 Then
                tmpRelHdlCmd = tmpRelHdlCmd & "*CMD*," & tmpSqlTab & ",DRT,-1,[URNO=:VURNO]" & vbLf
                While CntDelete > 0
                    CntDelete = FileData(Index).GetNextResultLine
                    If CntDelete >= 0 Then
                        tmpUrno = FileData(Index).GetFieldValue(CntDelete, "URNO")
                        If tmpUrno <> "" Then
                            tmpRelHdlCmd = tmpRelHdlCmd & tmpUrno & vbLf
                        End If
                        FileData(Index).DeleteLine CntDelete
                    End If
                    CntDelete = Val(FileData(Index).GetLinesByStatusValue(3, 0))
                Wend
            End If
            tmpMaskType = HiddenData.GetConfigValues(FileData(Index).myName, "MASK")
            tmpEditor = GetRealItem(tmpMaskType, 0, ",")
            tmpLayout = GetRealItem(tmpMaskType, 1, ",")
            If Not UseRelCmd Then tmpRelHdlCmd = ""
            If tmpRelHdlCmd <> "" Then
                UfisServer.CallCeda CedaDataAnswer, "REL", tmpSqlTab, tmpSqlFld, tmpRelHdlCmd, "LATE,NOBC,NOACTION", "", 0, False, False
                tmpSqlFld = ""
                tmpSqlDat = txtAftData(1).Text & "," & txtAftData(3).Text
                tmpSqlKey = UfisServer.CdrhdlSock.LocalHostName
                tmpSqlKey = tmpSqlKey & "," & CStr(App.ThreadID) & "," & tmpLayout
                tmpSqlKey = tmpSqlKey & "," & HiddenData.GetConfigValues(FileData(Index).myName, "CALL")
                UfisServer.CallCeda CedaDataAnswer, "SBC", tmpSqlTab & "/REFR", tmpSqlFld, tmpSqlDat, tmpSqlKey, "", 0, False, False
            End If
            FileData(Index).AutoSizeColumns
            FileData(Index).Refresh
            If Index = Val(DelayPanel.Tag) Then
                If DelayPanel.Visible Then
                    ClearDlyIndex Index
                    InitDelayPanel Index, -1, ""
                End If
            End If
            Select Case tmpLayout
                Case "MVLG"
                    SetMvlgStatistic Index
                Case "REQU"
                    SetRequStatistic Index
                Case Else
            End Select
            FileData(Index).SetInternalLineBuffer False
            Screen.MousePointer = 0
        End If
    Next
    CheckAftTabTriggers
    If AutoSaveAodb Then
        HighlightWorkButton "SAVEAODB", vbBlack, LightGreen
    Else
        HighlightWorkButton "SAVEAODB", vbButtonText, MyOwnButtonFace
    End If
    
    'UfisServer.DisconnectFromCeda
End Sub
Private Sub CheckDlyIndex(Index As Integer, LineNo As Long, IndxVal As String, DeleteIt As Boolean)
    Dim CurLine As Long
    Dim MaxLine As Long
    Dim tmpIndx As String
    Dim tmpArea As String
    MaxLine = FileData(Index).GetLineCount - 1
    For CurLine = 0 To MaxLine
        If CurLine <> LineNo Then
            tmpArea = FileData(Index).GetFieldValue(CurLine, "AREA")
            If tmpArea = "B" Then
                tmpIndx = FileData(Index).GetFieldValue(CurLine, "INDX")
                If tmpIndx = IndxVal Then
                    If DeleteIt Then
                        FileData(Index).SetLineStatusValue CurLine, 4
                    Else
                        FileData(Index).SetLineStatusValue CurLine, 3
                    End If
                End If
            End If
        End If
    Next
    If DeleteIt Then ClearDlyIndex Index
End Sub
Private Sub ClearDlyIndex(Index As Integer)
    Dim CurLine As Long
    Dim MaxLine As Long
    MaxLine = FileData(Index).GetLineCount - 1
    CurLine = 0
    While CurLine <= MaxLine
        If FileData(Index).GetLineStatusValue(CurLine) = 4 Then
            FileData(Index).DeleteLine CurLine
            CurLine = CurLine - 1
            MaxLine = MaxLine - 1
        End If
        CurLine = CurLine + 1
    Wend
    FileData(Index).Refresh
End Sub
Private Sub CheckAftTabTriggers()
    Dim tmpSqlTab As String
    Dim tmpSqlFld As String
    Dim tmpSqlDat As String
    Dim tmpSqlKey As String
    Dim tmpCapt As String
    Dim tmpTag As String
    Dim SaveArr As Boolean
    Dim SaveDep As Boolean
    Dim i As Integer
    SaveArr = False
    SaveDep = False
    For i = 10 To 12
        tmpCapt = Trim(lblArr(i).Caption)
        tmpTag = Trim(lblArr(i).Tag)
        If tmpCapt <> tmpTag Then SaveArr = True
        tmpCapt = Trim(lblDep(i).Caption)
        tmpTag = Trim(lblDep(i).Tag)
        If tmpCapt <> tmpTag Then SaveDep = True
    Next
    If SaveArr Then
        If txtAftData(1).Text <> "" Then
            tmpSqlFld = "BAA1,BAA2,BAA3"
            tmpSqlDat = ""
            tmpSqlDat = tmpSqlDat & lblArr(10).Tag & ","
            tmpSqlDat = tmpSqlDat & lblArr(11).Tag & ","
            tmpSqlDat = tmpSqlDat & lblArr(12).Tag
            tmpSqlKey = "WHERE URNO=" & txtAftData(1)
            tmpSqlTab = "AFTTAB"
            UfisServer.CallCeda CedaDataAnswer, "UFR", tmpSqlTab, tmpSqlFld, tmpSqlDat, tmpSqlKey, "", 0, False, False
        End If
    End If
    If SaveDep Then
        If txtAftData(3).Text <> "" Then
            tmpSqlFld = "BAA1,BAA2,BAA3"
            tmpSqlDat = ""
            tmpSqlDat = tmpSqlDat & lblDep(10).Tag & ","
            tmpSqlDat = tmpSqlDat & lblDep(11).Tag & ","
            tmpSqlDat = tmpSqlDat & lblDep(12).Tag
            tmpSqlKey = "WHERE URNO=" & txtAftData(3)
            tmpSqlTab = "AFTTAB"
            UfisServer.CallCeda CedaDataAnswer, "UFR", tmpSqlTab, tmpSqlFld, tmpSqlDat, tmpSqlKey, "", 0, False, False
        End If
    End If
    For i = 10 To 12
        lblArr(i).Caption = lblArr(i).Tag
        lblDep(i).Caption = lblDep(i).Tag
    Next
End Sub
Private Sub SetMvlgStatistic(Index As Integer)
    Dim MaxLine As Long
    Dim CurLine As Long
    Dim tmpCall As String
    Dim tmpText As String
    Dim tmpFlnu As String
    Dim ArrFlag As String
    Dim DepFlag As String
    MaxLine = FileData(Index).GetLineCount - 1
    For CurLine = 0 To MaxLine
        tmpFlnu = FileData(Index).GetFieldValue(CurLine, "FLNU")
        tmpText = Trim(FileData(Index).GetFieldValue(CurLine, "TEXT"))
        If tmpFlnu = txtAftData(1).Text Then
            If tmpText <> "" Then ArrFlag = "X"
        End If
        If tmpFlnu = txtAftData(3).Text Then
            If tmpText <> "" Then DepFlag = "X"
        End If
    Next
    tmpCall = HiddenData.GetConfigValues(FileData(Index).myName, "CALL")
    If (tmpCall = "ARR") Or (tmpCall = "ROT") Then lblArr(10).Tag = ArrFlag
    If (tmpCall = "DEP") Or (tmpCall = "ROT") Then lblDep(10).Tag = DepFlag
End Sub
Private Sub SetRequStatistic(Index As Integer)
    Dim MaxLine As Long
    Dim CurLine As Long
    Dim ArrIsRead As Long
    Dim ArrUnRead As Long
    Dim DepIsRead As Long
    Dim DepUnRead As Long
    Dim tmpRead As String
    Dim tmpFlnu As String
    Dim tmpCheck As String
    MaxLine = FileData(Index).GetLineCount - 1
    For CurLine = 0 To MaxLine
        tmpCheck = Trim(FileData(Index).GetFieldValues(CurLine, "ADID,TIME"))
        If tmpCheck <> "," Then
            tmpFlnu = FileData(Index).GetFieldValue(CurLine, "FLNU")
            tmpRead = Trim(FileData(Index).GetFieldValue(CurLine, "READ"))
            If tmpFlnu = txtAftData(1) Then
                If tmpRead = "X" Then ArrUnRead = ArrUnRead + 1
                If tmpRead = "" Then ArrIsRead = ArrIsRead + 1
            End If
            If tmpFlnu = txtAftData(3) Then
                If tmpRead = "X" Then DepUnRead = DepUnRead + 1
                If tmpRead = "" Then DepIsRead = DepIsRead + 1
            End If
        End If
    Next
    If ArrIsRead > 0 Then lblArr(11).Tag = CStr(ArrIsRead) Else lblArr(11).Tag = ""
    If ArrUnRead > 0 Then lblArr(12).Tag = CStr(ArrUnRead) Else lblArr(12).Tag = ""
    If DepIsRead > 0 Then lblDep(11).Tag = CStr(DepIsRead) Else lblDep(11).Tag = ""
    If DepUnRead > 0 Then lblDep(12).Tag = CStr(DepUnRead) Else lblDep(12).Tag = ""
End Sub
Private Sub HandleTabInsert(CurForm As Form, CurButton As CheckBox)
    Dim NewLine As String
    Dim tmpMaskType As String
    Dim tmpEditor As String
    Dim tmpLayout As String
    Dim tmpFields As String
    Dim tmpDispFields As String
    Dim tmpDispData As String
    Dim tmpCall As String
    Dim tmpCode As String
    Dim tmpFltRel As String
    Dim tmpMapp As String
    Dim tmpData As String
    Dim tmpCapt As String
    Dim NewData As String
    Dim tmpTicked As String
    Dim MaxLine As Long
    If LastFocusIndex >= 0 Then
        If chkTabIsVisible(LastFocusIndex).Value = 1 Then
            tmpMaskType = HiddenData.GetConfigValues(FileData(LastFocusIndex).myName, "MASK")
            tmpEditor = GetRealItem(tmpMaskType, 0, ",")
            tmpLayout = GetRealItem(tmpMaskType, 1, ",")
            tmpFields = HiddenData.GetConfigValues(FileData(LastFocusIndex).myName, "EDIT")
            tmpFields = GetRealItem(tmpFields, 0, "|")
            tmpMapp = HiddenData.GetConfigValues(FileData(LastFocusIndex).myName, "MAPP")
            tmpCapt = "Insert " & HiddenData.GetConfigValues(FileData(LastFocusIndex).myName, "CAPT")
            tmpCapt = GetRealItem(tmpCapt, 0, "|")
            tmpCall = HiddenData.GetConfigValues(FileData(LastFocusIndex).myName, "CALL")
            tmpCode = HiddenData.GetConfigValues(FileData(LastFocusIndex).myName, "CODE")
            If tmpCall = "ROT" Then
                If tmpCode = "ROT" Then tmpFltRel = "SET,DEP" Else tmpFltRel = "FIX," & tmpCode
            Else
                tmpFltRel = "FIX," & tmpCall
            End If
            tmpFltRel = tmpFltRel & "," & lblArr(0).Tag & "," & lblDep(0).Tag
            tmpData = ""
            tmpDispFields = "USEC,CDAT,USEU,LSTU"
            tmpDispData = ""
            NewData = EditMask.GetEditorValues(Me, tmpCapt, tmpLayout, tmpFltRel, tmpMapp, tmpData, tmpDispFields, tmpDispData)
            If NewData <> "" Then
                tmpTicked = GetRealItem(NewData, 1, vbLf)
                MaxLine = FileData(LastFocusIndex).GetLineCount
                NewLine = CreateEmptyLine(FileData(LastFocusIndex).LogicalFieldList)
                FileData(LastFocusIndex).InsertTextLine NewLine, False
                tmpFields = tmpFields & ",USEC,USEU,CDAT,LSTU,FLNU"
                NewData = GetRealItem(NewData, 0, vbLf)
                NewData = NewData & "," & LoginUserName
                NewData = NewData & "," & LoginUserName
                NewData = NewData & "," & GetTimeStamp(-480)
                NewData = NewData & "," & GetTimeStamp(-480)
                Select Case tmpTicked
                    Case "0"
                        NewData = NewData & "," & txtAftData(1).Text
                    Case "1"
                        NewData = NewData & "," & txtAftData(3).Text
                    Case Else
                        NewData = NewData & ",0"
                End Select
                tmpMaskType = GetRealItem(tmpMaskType, 1, ",")
                Select Case tmpMaskType
                    Case "DLLG"
                        tmpFields = tmpFields & ",AREA"
                        NewData = NewData & "," & "A"
                    Case "DLYS"
                        tmpFields = tmpFields & ",AREA"
                        NewData = NewData & "," & "B"
                    Case "MVLG"
                        tmpFields = tmpFields & ",AREA"
                        NewData = NewData & "," & "A"
                    Case "REQU"
                        tmpFields = tmpFields & ",AREA"
                        NewData = NewData & "," & "A"
                        If InStr(tmpFields, "READ") = 0 Then
                            tmpFields = tmpFields & ",READ"
                            NewData = NewData & "," & "X"
                        End If
                        tmpFields = tmpFields & ",ADID,FLNO,STIM,ETIM"
                        Select Case tmpTicked
                            Case "0"
                                NewData = NewData & "," & "A"
                                NewData = NewData & "," & lblArr(0).Tag
                                NewData = NewData & "," & lblArr(2).Tag
                                NewData = NewData & "," & lblArr(4).Tag
                            Case "1"
                                NewData = NewData & "," & "D"
                                NewData = NewData & "," & lblDep(0).Tag
                                NewData = NewData & "," & lblDep(2).Tag
                                NewData = NewData & "," & lblDep(4).Tag
                            Case Else
                                NewData = NewData & ",A,??,,"
                        End Select
                    Case Else
                End Select
                FileData(LastFocusIndex).SetFieldValues MaxLine, tmpFields, NewData
                FileData(LastFocusIndex).SetLineStatusValue MaxLine, 2
                FileData(LastFocusIndex).SetLineColor MaxLine, vbBlack, LightGreen
                FileData(LastFocusIndex).OnVScrollTo MaxLine
                HighlightWorkButton "SAVEAODB", vbWhite, vbRed
                FileData(LastFocusIndex).AutoSizeColumns
                FileData(LastFocusIndex).Refresh
                FileData(LastFocusIndex).OnVScrollTo MaxLine - VisibleTabLines(FileData(LastFocusIndex)) + 1
            End If
        End If
    End If
    CurButton.Value = 0
End Sub

Private Sub HandleTabUpdate(CurForm As Form, CurButton As CheckBox)
    Dim NewLine As String
    Dim tmpMaskType As String
    Dim tmpEditor As String
    Dim tmpLayout As String
    Dim tmpFields As String
    Dim tmpDispFields As String
    Dim tmpDispData As String
    Dim tmpCall As String
    Dim tmpCode As String
    Dim tmpFltRel As String
    Dim tmpMapp As String
    Dim tmpData As String
    Dim tmpCapt As String
    Dim tmpFlnu As String
    Dim tmpUrno As String
    Dim NewData As String
    Dim tmpTicked As String
    Dim CurStatus As Long
    Dim LineNo As Long
    Dim IsChanged As Boolean
    If LastFocusIndex >= 0 Then
        If chkTabIsVisible(LastFocusIndex).Value = 1 Then
            LineNo = FileData(LastFocusIndex).GetCurrentSelected
            If LineNo < 0 Then
                If FileData(LastFocusIndex).GetLineCount > 0 Then
                    FileData(LastFocusIndex).SetCurrentSelection 0
                    LineNo = 0
                End If
            End If
            If LineNo >= 0 Then
                tmpMaskType = HiddenData.GetConfigValues(FileData(LastFocusIndex).myName, "MASK")
                tmpEditor = GetRealItem(tmpMaskType, 0, ",")
                tmpLayout = GetRealItem(tmpMaskType, 1, ",")
                tmpFields = HiddenData.GetConfigValues(FileData(LastFocusIndex).myName, "EDIT")
                tmpFields = GetRealItem(tmpFields, 0, "|")
                tmpMapp = HiddenData.GetConfigValues(FileData(LastFocusIndex).myName, "MAPP")
                tmpCapt = "Update " & HiddenData.GetConfigValues(FileData(LastFocusIndex).myName, "CAPT")
                tmpCapt = GetRealItem(tmpCapt, 0, "|")
                tmpCall = HiddenData.GetConfigValues(FileData(LastFocusIndex).myName, "CALL")
                tmpCode = HiddenData.GetConfigValues(FileData(LastFocusIndex).myName, "CODE")
                tmpUrno = FileData(LastFocusIndex).GetFieldValue(LineNo, "URNO")
                tmpFlnu = FileData(LastFocusIndex).GetFieldValue(LineNo, "FLNU")
                If tmpCall = "ROT" Then
                    If tmpCode = "ROT" Then
                        If tmpFlnu = txtAftData(1) Then tmpCall = "ARR"
                        If tmpFlnu = txtAftData(3) Then tmpCall = "DEP"
                        tmpFltRel = "SET," & tmpCall
                    Else
                        tmpFltRel = "FIX," & tmpCode
                    End If
                Else
                    If tmpFlnu = txtAftData(1) Then tmpCall = "ARR"
                    If tmpFlnu = txtAftData(3) Then tmpCall = "DEP"
                    tmpFltRel = "FIX," & tmpCall
                End If
                tmpFltRel = tmpFltRel & "," & lblArr(0).Tag & "," & lblDep(0).Tag
                tmpFltRel = tmpFltRel & "," & tmpUrno
                
                tmpData = FileData(LastFocusIndex).GetFieldValues(LineNo, tmpFields)
                tmpDispFields = "USEC,CDAT,USEU,LSTU"
                tmpDispData = FileData(LastFocusIndex).GetFieldValues(LineNo, tmpDispFields)
                
                NewData = EditMask.GetEditorValues(Me, tmpCapt, tmpLayout, tmpFltRel, tmpMapp, tmpData, tmpDispFields, tmpDispData)
                
                LineNo = FileData(LastFocusIndex).GetCurrentSelected
                If NewData <> "" Then
                    tmpTicked = GetRealItem(NewData, 1, vbLf)
                    NewData = GetRealItem(NewData, 0, vbLf)
                    IsChanged = False
                    Select Case tmpTicked
                        Case "0"
                            If tmpFlnu <> txtAftData(1) Then
                                tmpFields = tmpFields & ",FLNU"
                                NewData = NewData & "," & txtAftData(1).Text
                            End If
                        Case "1"
                            If tmpFlnu <> txtAftData(3) Then
                                tmpFields = tmpFields & ",FLNU"
                                NewData = NewData & "," & txtAftData(3).Text
                            End If
                        Case Else
                            NewData = NewData & ",0"
                    End Select
                    If NewData <> tmpData Then IsChanged = True
                    If IsChanged Then
                        CurStatus = FileData(LastFocusIndex).GetLineStatusValue(LineNo)
                        If CurStatus < 2 Then
                            tmpFields = tmpFields & ",USEU,LSTU"
                            NewData = NewData & "," & LoginUserName
                            NewData = NewData & "," & GetTimeStamp(-480)
                        End If
                        FileData(LastFocusIndex).SetFieldValues LineNo, tmpFields, NewData
                        If CurStatus < 2 Then
                            FileData(LastFocusIndex).SetLineStatusValue LineNo, 1
                            FileData(LastFocusIndex).SetLineColor LineNo, vbBlack, LightYellow
                        End If
                        HighlightWorkButton "SAVEAODB", vbWhite, vbRed
                    End If
                    FileData(LastFocusIndex).AutoSizeColumns
                    FileData(LastFocusIndex).Refresh
                End If
            End If
        End If
    End If
    CurButton.Value = 0
End Sub
Private Sub HandleTabDelete(CurForm As Form, CurButton As CheckBox)
    Dim CurLine As Long
    Dim tmpCapt As String
    Dim tmpMsg As String
    Dim CurStatus As Long
    If LastFocusIndex >= 0 Then
        If chkTabIsVisible(LastFocusIndex).Value = 1 Then
            CurLine = FileData(LastFocusIndex).GetCurrentSelected
            If CurLine >= 0 Then
                tmpCapt = HiddenData.GetConfigValues(FileData(LastFocusIndex).myName, "CAPT")
                tmpCapt = GetRealItem(tmpCapt, 0, "|")
                tmpMsg = "Do you want to delete this entry?"
                CurStatus = FileData(LastFocusIndex).GetLineStatusValue(CurLine)
                
                If MyMsgBox.CallAskUser(0, 0, 0, tmpCapt, tmpMsg, "stop2", "Yes,No", UserAnswer) = 1 Then
                    Select Case CurStatus
                        'Case 1  'Update
                        Case 2  'Insert
                            FileData(LastFocusIndex).DeleteLine CurLine
                        Case 3  'Delete
                        Case Else
                            FileData(LastFocusIndex).SetLineStatusValue CurLine, 3
                            FileData(LastFocusIndex).SetLineColor CurLine, vbBlack, LightRed
                            HighlightWorkButton "SAVEAODB", vbWhite, vbRed
                    End Select
                Else
                
                End If
            End If
        End If
    End If
    CurButton.Value = 0
End Sub
Private Sub HandleAodbFilter(CurForm As Form, CurButton As CheckBox)
    Dim FilterResult As String
    Dim tmpPatchCodes As String
    Dim tmpPatchData As String
    Dim i As Integer
    FilterResult = FilterDialog.InitMyLayout(Me, True, "AODB_FILTER")
    CurForm.Refresh
    If FilterResult <> "" Then
        For i = 0 To chkTabIsVisible.UBound
            If chkTabIsVisible(i).Visible Then
                FileData(i).ResetContent
                FileData(i).ColSelectionRemoveAll
                FileData(i).Refresh
            End If
        Next
        tmpPatchCodes = GetRealItem(FilterResult, 0, Chr(16))
        tmpPatchData = GetRealItem(FilterResult, 1, Chr(16))
        For i = 0 To chkTabIsVisible.UBound
            If chkTabIsVisible(i).Visible Then
                HiddenData.ReadAodbData FileData(i), False, "", tmpPatchCodes, tmpPatchData, True
                CleanSystemFields FileData(i)
                FileData(i).AutoSizeColumns
                CheckMinMaxPanel i
                FileData(i).Refresh
            End If
        Next
    End If
    CurButton.Value = 0
End Sub
Private Sub HandleReadAodb(CurForm As Form, CurButton As CheckBox)
    Dim FilterResult As String
    Dim tmpPatchCodes As String
    Dim tmpPatchData As String
    Dim LineNo As Long
    Dim SortColNo As String
    Dim i As Integer
    
    For i = 0 To chkTabIsVisible.UBound
        If chkTabIsVisible(i).Visible Then
            If (Not RefreshOnBc) Or (i = LastFocusIndex) Then
                FileData(i).ColSelectionRemoveAll
                FileData(i).ResetContent
                FileData(i).Refresh
            End If
        End If
    Next
    'Attention:
    'PatchCodes must be evaluated per TAB !!
    ' .... to do ....
    
    tmpPatchCodes = GetRealItem(FilterResult, 0, Chr(16))
    tmpPatchData = GetRealItem(FilterResult, 1, Chr(16))
    Select Case txtAftData(0).Text
        Case "ARR"
            tmpPatchCodes = "FLNU"
            tmpPatchData = txtAftData(1).Text
        Case "DEP"
            tmpPatchCodes = "FLNU"
            tmpPatchData = txtAftData(3).Text
        Case "ROT"
            tmpPatchCodes = "FLNU"
            tmpPatchData = txtAftData(1).Text & "," & txtAftData(3).Text
        Case Else
            tmpPatchCodes = ""
            tmpPatchData = ""
    End Select
    
    For i = 0 To chkTabIsVisible.UBound
        If chkTabIsVisible(i).Visible Then
            If (Not RefreshOnBc) Or (i = LastFocusIndex) Then
                HiddenData.ReadAodbData FileData(i), False, "", tmpPatchCodes, tmpPatchData, True
                CleanSystemFields FileData(i)
                SortColNo = TranslateFieldItems("LINO", FileData(i).LogicalFieldList)
                If SortColNo <> "" Then FileData(i).Sort SortColNo, True, True
                FileData(i).AutoSizeColumns
                FileData(i).Refresh
                InsertEmptyTabLine i, -1
                CheckMinMaxPanel i
                If DelayPanel.Visible Then
                    If Val(DelayPanel.Tag) = i Then
                        InitDelayPanel i, -1, ""
                    End If
                End If
            End If
        End If
    Next
    If Not AutoSaveAodb Then HighlightWorkButton "SAVEAODB", vbButtonText, MyOwnButtonFace
    CurButton.Value = 0
End Sub

Private Sub HandleExtract(CurForm As Form, CurButton As CheckBox)
    Dim NewForm As MainDialog
    Set NewForm = New MainDialog
    NewForm.Caption = "Extract Data"
    NewForm.Tag = "EXTRACT,1"
    Me.Refresh
    NewForm.Show
End Sub
Private Sub HandleSaveLocal(CurForm As Form, CurButton As CheckBox)
    Dim tmpFileName As String
    Dim i As Integer
    tmpFileName = "c:\tmp\DutyRecord"
    For i = 0 To FileData.UBound
        If FileData(i).GetLineCount > 0 Then
            FileData(i).WriteToFile tmpFileName & "_" & CStr(i) & ".txt", False
        End If
    Next
    CurButton.Value = 0
End Sub
Private Sub HandleFileOpen(CurForm As Form, CurButton As CheckBox)
    Dim tmpFileResult As String
    Dim tmpFileFilter As String
    Dim tmpFilePath As String
    tmpFilePath = GetIniEntry(myIniFullName, myIniSection, "", "FILE_PATH", "")
    tmpFileFilter = GetIniEntry(myIniFullName, myIniSection, "", "FILE_FILTER", "{All Files}{*.*}")
    tmpFileResult = FileDialog.GetFileOpenDialog(0, tmpFilePath, tmpFileFilter, 0, 0)
    CurButton.Value = 0
End Sub
Private Sub HandleReadLocal(CurForm As Form, CurButton As CheckBox)
    Dim tmpFileName As String
    Dim i As Integer
    Screen.MousePointer = 11
    tmpFileName = "c:\tmp\DutyRecord"
    For i = 0 To FileData.UBound
        FileData(i).ResetContent
        FileData(i).Refresh
        FileData(i).ReadFromFile tmpFileName & "_" & CStr(i) & ".txt"
        CleanSystemFields FileData(i)
        FileData(i).AutoSizeColumns
        CheckMinMaxPanel i
        FileData(i).Refresh
    Next
    Screen.MousePointer = 0
    CurButton.Value = 0
End Sub

Private Sub FileData_CloseInplaceEdit(Index As Integer, ByVal LineNo As Long, ByVal ColNo As Long)
    If LineNo >= 0 Then
        If TabEditCurLineNo >= 0 Then CheckChangesAndSave Index, TabEditCurLineNo
        If (Not TabEditHitEnter) And (Not TabEditInsert) Then
            TabEditIsActive = False
            TabEditCurLineNo = -1
            TabEditCurColNo = -1
        End If
    End If
End Sub

Private Sub FileData_EditPositionChanged(Index As Integer, ByVal LineNo As Long, ByVal ColNo As Long, ByVal Value As String)
    Dim tmpData As String
    Dim LineStatus As Long
    If LineNo >= 0 Then
        TabEditIsActive = True
        LineStatus = FileData(Index).GetLineStatusValue(LineNo)
        tmpData = FileData(Index).GetLineTag(LineNo)
        If tmpData = "" Then
            tmpData = FileData(Index).GetLineValues(LineNo)
            FileData(Index).SetLineTag LineNo, tmpData
        End If
        If (LineStatus <> 99) Or (TabEditInsert = True) Then
            If ColNo <> TabEditCurColNo Then
                If TabEditCurColNo >= 0 Then SetAdidFlightRelation Index, LineNo, TabEditCurColNo
            End If
            If LineNo <> TabEditCurLineNo Then
                If TabEditCurLineNo >= 0 Then
                    CheckChangesAndSave Index, TabEditCurLineNo
                End If
            Else
                If ColNo = TabEditCurColNo Then
                    CheckChangesAndSave Index, TabEditCurLineNo
                End If
            End If
            TabEditIsActive = True
            TabEditCurLineNo = LineNo
            TabEditCurColNo = ColNo
            If (TabEditHitEnter = True) And (TabEditInsert = True) Then
                FileData(Index).EnableInlineEdit False
                If LineNo >= (FileData(Index).GetLineCount - 1) Then
                    TabEditCurLineNo = LineNo + 1
                Else
                End If
                InsertEmptyTabLine Index, TabEditCurLineNo
                tmpData = FileData(Index).GetLineValues(TabEditCurLineNo)
                FileData(Index).SetLineTag TabEditCurLineNo, tmpData
                TabEditCurColNo = GetFirstEditColNo(Index)
                FileData(Index).EnableInlineEdit True
                FileData(Index).SetCurrentSelection TabEditCurLineNo
                FileData(Index).SetInplaceEdit TabEditCurLineNo, TabEditCurColNo, False
                TabEditHitEnter = False
                TabEditIsActive = True
            End If
        Else
            FileData(Index).EnableInlineEdit False
            FileData(Index).EnableInlineEdit True
        End If
    End If
End Sub
Private Sub CheckChangesAndSave(Index As Integer, LineNo As Long)
    Dim FlnuValue As Long
    Dim LineStatus As Long
    Dim tmpData As String
    Dim tmpTag As String
    Dim tmpFields As String
    Dim NewData As String
    Dim NewLine As String
    Dim tmpMaskType As String
    Dim tmpLayout As String
    Dim tmpDispData As String
    Dim tmpCall As String
    Dim tmpCode As String
    Dim tmpAdid As String
    Dim tmpTime As String
    Dim tmpFlnu As String
    Dim tmpFltRel As String
    Dim CheckAdid As String
    Dim MsgText As String
    
    tmpData = FileData(Index).GetLineValues(LineNo)
    tmpTag = FileData(Index).GetLineTag(LineNo)
    If tmpTag = "" Then
        tmpTag = tmpData
        FileData(Index).SetLineTag LineNo, tmpData
    End If
    If tmpTag <> tmpData Then
        tmpMaskType = HiddenData.GetConfigValues(FileData(LastFocusIndex).myName, "MASK")
        tmpLayout = GetRealItem(tmpMaskType, 1, ",")
        tmpCall = HiddenData.GetConfigValues(FileData(LastFocusIndex).myName, "CALL")
        tmpCode = HiddenData.GetConfigValues(FileData(LastFocusIndex).myName, "CODE")
        If tmpCall = "ROT" Then
            tmpFltRel = tmpCode
        Else
            tmpFltRel = tmpCall
        End If
        tmpAdid = Trim(FileData(Index).GetFieldValue(LineNo, "ADID"))
        Select Case tmpFltRel
            Case "ARR"
                tmpAdid = "A"
            Case "DEP"
                tmpAdid = "D"
            Case Else
        End Select
        CheckAdid = tmpAdid
        MsgText = ""
        If tmpAdid <> "" Then
            Select Case CheckAdid
                Case "A"
                    FlnuValue = Val(txtAftData(1).Text)
                    If FlnuValue <= 0 Then
                        MsgText = "Sorry, the Arrival Flight doesn't exist."
                    End If
                Case "D"
                    FlnuValue = Val(txtAftData(3).Text)
                    If FlnuValue <= 0 Then
                        MsgText = "Sorry, the Departure Flight doesn't exist."
                    End If
                Case Else
                    FlnuValue = Val(txtAftData(1).Text)
                    If FlnuValue <= 0 Then tmpAdid = "D"
                    FlnuValue = Val(txtAftData(3).Text)
                    If FlnuValue <= 0 Then tmpAdid = "A"
            End Select
        End If
        If MsgText = "" Then
            LineStatus = FileData(Index).GetLineStatusValue(LineNo)
            If (LineStatus = 99) Or (LineStatus = 2) Then
                tmpFields = "USEC,CDAT"
                NewData = LoginUserName
                NewData = NewData & "," & GetTimeStamp(-480)
            End If
            If (LineStatus <= 1) Or (LineStatus = 3) Then
                tmpFields = "USEU,LSTU"
                NewData = LoginUserName
                NewData = NewData & "," & GetTimeStamp(-480)
            End If
            
            tmpFields = tmpFields & ",FLNU"
            Select Case tmpAdid
                Case "A"
                    NewData = NewData & "," & txtAftData(1).Text
                Case "D"
                    NewData = NewData & "," & txtAftData(3).Text
                Case Else
                    If LineNo > 0 Then
                        tmpFlnu = FileData(Index).GetFieldValue(LineNo - 1, "FLNU")
                    Else
                        tmpFlnu = "-1"
                    End If
                    NewData = NewData & "," & tmpFlnu
            End Select
            
            Select Case tmpLayout
                Case "DLLG"
                    tmpFields = tmpFields & ",AREA"
                    NewData = NewData & "," & "A"
                Case "DLYS"
                    tmpFields = tmpFields & ",AREA"
                    NewData = NewData & "," & "B"
                Case "MVLG"
                    tmpFields = tmpFields & ",AREA"
                    NewData = NewData & "," & "A"
                Case "REQU"
                    tmpFields = tmpFields & ",AREA,READ"
                    tmpTime = Trim(FileData(Index).GetFieldValue(LineNo, "TIME"))
                    If (tmpTime <> "") Or (tmpAdid <> "") Then
                        NewData = NewData & ",A,X"
                    Else
                        NewData = NewData & ",A,"
                    End If
                    tmpFields = tmpFields & ",ADID,FLNO,STIM,ETIM"
                    Select Case tmpAdid
                        Case "A"
                            NewData = NewData & "," & "A"
                            NewData = NewData & "," & lblArr(0).Tag
                            NewData = NewData & "," & lblArr(2).Tag
                            NewData = NewData & "," & lblArr(4).Tag
                        Case "D"
                            NewData = NewData & "," & "D"
                            NewData = NewData & "," & lblDep(0).Tag
                            NewData = NewData & "," & lblDep(2).Tag
                            NewData = NewData & "," & lblDep(4).Tag
                        Case Else
                            If (tmpFlnu <> "-1") And (tmpAdid = "") Then
                                NewData = NewData & "," & tmpAdid & ",,,"
                            Else
                                NewData = NewData & "," & tmpAdid & ",????,,"
                            End If
                    End Select
                Case Else
            End Select
            
            FileData(Index).SetFieldValues LineNo, tmpFields, NewData
            
            If (TabEditUpdate = True) Or (LineStatus < 2) Then
                If LineStatus < 2 Then
                    FileData(Index).SetLineStatusValue LineNo, 1
                End If
            End If
            If TabEditInsert = True Then
                If LineStatus = 99 Then
                    FileData(Index).SetLineStatusValue LineNo, 2
                End If
            End If
            
            HighlightWorkButton "SAVEAODB", vbWhite, vbRed
            If AutoSaveAodb Then HandleSaveAodb True
            tmpData = FileData(Index).GetLineValues(LineNo)
            FileData(Index).SetLineTag LineNo, tmpData
        Else
            If MyMsgBox.CallAskUser(0, 0, 0, "Selected Flight", MsgText, "stop2", "", UserAnswer) = 1 Then DoNothing
        End If
    End If
End Sub
Private Sub SetAdidFlightRelation(Index As Integer, LineNo As Long, ColNo As Long)
    Dim FlnuValue As Long
    Dim tmpFields As String
    Dim NewData As String
    Dim tmpMaskType As String
    Dim tmpLayout As String
    Dim tmpAdid As String
    Dim CheckAdid As String
    Dim tmpFlnu As String
    tmpFields = GetRealItem(FileData(Index).LogicalFieldList, ColNo, ",")
    If tmpFields = "ADID" Then
        tmpMaskType = HiddenData.GetConfigValues(FileData(LastFocusIndex).myName, "MASK")
        tmpLayout = GetRealItem(tmpMaskType, 1, ",")
        If tmpLayout = "REQU" Then
            tmpAdid = Trim(FileData(Index).GetFieldValue(LineNo, "ADID"))
            CheckAdid = tmpAdid
            If tmpAdid <> "" Then
                Select Case CheckAdid
                    Case "A"
                        FlnuValue = Val(txtAftData(1).Text)
                        If FlnuValue <= 0 Then tmpAdid = "D"
                    Case "D"
                        FlnuValue = Val(txtAftData(3).Text)
                        If FlnuValue <= 0 Then tmpAdid = "A"
                    Case Else
                        FlnuValue = Val(txtAftData(1).Text)
                        If FlnuValue <= 0 Then tmpAdid = "D"
                        FlnuValue = Val(txtAftData(3).Text)
                        If FlnuValue <= 0 Then tmpAdid = "A"
                End Select
            End If
            tmpFields = "ADID,FLNO,STIM,ETIM,FLNU"
            NewData = tmpAdid
            Select Case tmpAdid
                Case "A"
                    NewData = NewData & "," & lblArr(0).Tag
                    NewData = NewData & "," & lblArr(2).Tag
                    NewData = NewData & "," & lblArr(4).Tag
                    NewData = NewData & "," & txtAftData(1).Text
                Case "D"
                    NewData = NewData & "," & lblDep(0).Tag
                    NewData = NewData & "," & lblDep(2).Tag
                    NewData = NewData & "," & lblDep(4).Tag
                    NewData = NewData & "," & txtAftData(3).Text
                Case Else
                    If LineNo > 0 Then
                        tmpFlnu = FileData(Index).GetFieldValue(LineNo - 1, "FLNU")
                    Else
                        tmpFlnu = "-1"
                    End If
                    If (tmpFlnu <> "-1") And (tmpAdid = "") Then
                        NewData = NewData & ",,,," & tmpFlnu
                    Else
                        
                        NewData = NewData & ",????,,," & tmpFlnu
                    End If
            End Select
            FileData(Index).SetFieldValues LineNo, tmpFields, NewData
            FileData(Index).Refresh
        End If
    End If
End Sub
Private Sub AutoSetReadStatus(Index As Integer, LineNo As Long)
    Dim tmpRead As String
    Dim LineStatus As Long
    Dim tmpCheck As String
    Dim tmpData As String
    If InStr(FileData(Index).LogicalFieldList, "READ") > 0 Then
        LineStatus = FileData(Index).GetLineStatusValue(LineNo)
        If LineStatus < 2 Then
            tmpCheck = Trim(FileData(Index).GetFieldValues(LineNo, "ADID,TIME"))
            If tmpCheck <> "," Then
                tmpRead = Trim(FileData(Index).GetFieldValue(LineNo, "READ"))
                If tmpRead = "" Then tmpRead = "X" Else tmpRead = ""
                tmpData = tmpRead
                tmpData = tmpData & "," & LoginUserName
                tmpData = tmpData & "," & GetTimeStamp(-480)
                FileData(Index).SetFieldValues LineNo, "READ,USEU,LSTU", tmpData
                FileData(Index).SetLineStatusValue LineNo, 1
                HighlightWorkButton "SAVEAODB", vbWhite, vbRed
                If AutoSaveAodb Then HandleSaveAodb False
                tmpData = FileData(Index).GetLineValues(LineNo)
                FileData(Index).SetLineTag LineNo, tmpData
            End If
        End If
    End If
End Sub
Private Sub SetSortLineNoAsc(Index As Integer)
    Dim MaxLine As Long
    Dim CurLine As Long
    Dim GroupLine As Long
    Dim SortLine As Long
    Dim OldSortLine As Long
    Dim OldSortVal As String
    Dim NewSortVal As String
    Dim NewMainVal As String
    Dim tmpTimeStr As String
    Dim NewValues As String
    Dim OldValues As String
    Dim tmpData As String
    Dim LineStatus As Long
    If InStr(FileData(Index).LogicalFieldList, "LINO") > 0 Then
        GroupLine = 0
        SortLine = 0
        NewMainVal = ""
        MaxLine = FileData(Index).GetLineCount - 1
        For CurLine = 0 To MaxLine
            LineStatus = FileData(Index).GetLineStatusValue(CurLine)
            If LineStatus <> 99 Then
                tmpTimeStr = Trim(FileData(Index).GetFieldValues(CurLine, "ADID,TIME"))
                If tmpTimeStr = "," Then tmpTimeStr = ""
                OldSortVal = Trim(FileData(Index).GetFieldValue(CurLine, "LINO"))
                OldSortLine = Val(OldSortVal)
                If (tmpTimeStr <> "") Or (NewMainVal = "") Then
                    GroupLine = GroupLine + 1
                    NewMainVal = Right("000" & CStr(GroupLine), 3)
                    SortLine = 1
                End If
                NewSortVal = NewMainVal & "." & Right("000" & CStr(SortLine), 3)
                If NewSortVal <> OldSortVal Then
                    FileData(Index).SetFieldValues CurLine, "LINO", NewSortVal
                    If LineStatus < 1 Then FileData(Index).SetLineStatusValue CurLine, 1
                    tmpData = FileData(Index).GetLineValues(CurLine)
                    FileData(Index).SetLineTag CurLine, tmpData
                End If
                SortLine = SortLine + 1
            End If
        Next
    End If
End Sub
Private Sub SetSortLineNoDesc(Index As Integer)
    Dim MaxLine As Long
    Dim CurLine As Long
    Dim GroupLine As Long
    Dim SortLine As Long
    Dim OldSortLine As Long
    Dim OldSortVal As String
    Dim NewSortVal As String
    Dim NewMainVal As String
    Dim tmpTimeStr As String
    Dim NewValues As String
    Dim OldValues As String
    Dim tmpData As String
    Dim LineStatus As Long
    If InStr(FileData(Index).LogicalFieldList, "LINO") > 0 Then
        GroupLine = 1000
        SortLine = 1000
        NewMainVal = ""
        MaxLine = FileData(Index).GetLineCount - 1
        For CurLine = 0 To MaxLine
            LineStatus = FileData(Index).GetLineStatusValue(CurLine)
            If LineStatus <> 99 Then
                tmpTimeStr = Trim(FileData(Index).GetFieldValues(CurLine, "ADID,TIME"))
                If tmpTimeStr = "," Then tmpTimeStr = ""
                OldSortVal = Trim(FileData(Index).GetFieldValue(CurLine, "LINO"))
                OldSortLine = Val(OldSortVal)
                If (tmpTimeStr <> "") Or (NewMainVal = "") Then
                    GroupLine = GroupLine - 1
                    NewMainVal = Right("000" & CStr(GroupLine), 3)
                    SortLine = 999
                End If
                NewSortVal = NewMainVal & "." & Right("000" & CStr(SortLine), 3)
                If NewSortVal <> OldSortVal Then
                    FileData(Index).SetFieldValues CurLine, "LINO", NewSortVal
                    If LineStatus < 1 Then FileData(Index).SetLineStatusValue CurLine, 1
                    tmpData = FileData(Index).GetLineValues(CurLine)
                    FileData(Index).SetLineTag CurLine, tmpData
                End If
                SortLine = SortLine - 1
            End If
        Next
    End If
End Sub

Private Sub FileData_GotFocus(Index As Integer)
    Dim LineNo As Long
    Dim tmpColors As String
    If Not SplitterMoves Then
        chkTabIsVisible(Index).Appearance = 0
        tmpColors = BuildMainHeaderColor(ItemCount(FileData(Index).GetMainHeaderRanges, ","), CStr(LightestBlue))
        FileData(Index).SetMainHeaderValues FileData(Index).GetMainHeaderRanges, FileData(Index).GetMainHeaderValues, tmpColors
        chkHide(Index).BackColor = DarkBlue
        chkHide(Index).ForeColor = vbWhite
        If chkMax(Index).Value = 1 Then
            chkMax(Index).BackColor = LightestBlue
            chkMax(Index).ForeColor = vbBlack
        Else
            chkMax(Index).BackColor = DarkBlue
            chkMax(Index).ForeColor = vbWhite
        End If
        FileData(Index).Refresh
        LastFocusIndex = Index
        If ReadMaskIsOpen Then
            LineNo = FileData(LastFocusIndex).GetCurrentSelected
            If LineNo < 0 Then LineNo = 0
            FileData(LastFocusIndex).SetCurrentSelection LineNo
        End If
    End If
End Sub

Private Sub FileData_HitKeyOnLine(Index As Integer, ByVal Key As Integer, ByVal LineNo As Long)
    If LineNo >= 0 Then
        If Not ReadOnlyUser Then
            If Not TabEditIsActive Then
                If Key = 13 Then PushWorkButton "TAB_EDIT_UPDATE", 1
                If Key = 45 Then PushWorkButton "TAB_EDIT_INSERT", 1
                If Key = 46 Then PushWorkButton "TAB_DELETE", 1
            Else
                If Key = 13 Then TabEditHitEnter = True
            End If
        End If
    End If
End Sub

Private Sub FileData_LostFocus(Index As Integer)
    Dim tmpColors As String
    If (Not SplitterMoves) And (Not RefreshOnBc) Then
        chkTabIsVisible(Index).Appearance = 1
        tmpColors = ""
        FileData(Index).SetMainHeaderValues FileData(Index).GetMainHeaderRanges, FileData(Index).GetMainHeaderValues, tmpColors
        chkHide(Index).BackColor = MyOwnButtonFace
        chkHide(Index).ForeColor = vbButtonText
        If chkMax(Index).Value = 1 Then
            chkMax(Index).BackColor = LightGreen
            chkMax(Index).ForeColor = vbBlack
        Else
            chkMax(Index).BackColor = MyOwnButtonFace
            chkMax(Index).ForeColor = vbButtonText
        End If
        FileData(Index).Refresh
    End If
End Sub

Private Function BuildMainHeaderColor(MaxColNo As Long, ColorValue As String) As String
    Dim tmpColors As String
    Dim i As Long
    For i = 1 To MaxColNo
        tmpColors = tmpColors & CStr(LightestBlue) & ","
    Next
    BuildMainHeaderColor = tmpColors
End Function

Private Sub FileData_RowSelectionChanged(Index As Integer, ByVal LineNo As Long, ByVal Selected As Boolean)
    Dim tmpText As String
    Dim tmpCapt As String
    Dim tmpRead As String
    Dim tmpUrno As String
    Dim PanelNo As Integer
    Dim IsRead As Integer
    Dim UseColor As Long
    If (Selected) And (LineNo >= 0) Then
        If ReadMaskIsOpen Then
            tmpUrno = FileData(Index).GetFieldValue(LineNo, "URNO")
            tmpText = FileData(Index).GetFieldValue(LineNo, "TEXT")
            tmpRead = Trim(FileData(Index).GetFieldValue(LineNo, "READ"))
            If tmpRead = "" Then
                If InStr(FileData(Index).LogicalFieldList, "READ") > 0 Then IsRead = 1 Else IsRead = -1
            Else
                IsRead = 0
            End If
            tmpCapt = HiddenData.GetConfigValues(FileData(Index).myName, "CAPT")
            tmpCapt = GetRealItem(tmpCapt, 0, "|")
            ReadMask.DisplayText FileData(Index), tmpText, tmpCapt, tmpUrno, IsRead
        End If
        CheckSynchroTabs Index, LineNo
    End If
    If DelayPanel.Visible Then
        If Val(DelayPanel.Tag) = Index Then
            tmpText = FileData(Index).GetFieldValue(LineNo, "INDX")
            If tmpText <> "" Then
                PanelNo = Val(tmpText)
                If (PanelNo >= 0) And (PanelNo <= DlyEditPanel.UBound) Then
                    If Selected Then
                        UseColor = vbYellow
                    Else
                        If chkDlyTick(PanelNo).Value = 1 Then
                            UseColor = vbWhite
                        Else
                            UseColor = NormalGray
                        End If
                    End If
                End If
                txtDlyInput(PanelNo).BackColor = UseColor
            End If
        End If
    End If
End Sub

Private Sub FileData_SendLButtonDblClick(Index As Integer, ByVal LineNo As Long, ByVal ColNo As Long)
    Static LastColNo As Long
    Dim SortCol As Long
    Dim LineStatus As Long
    Dim tmpFldName As String
    Dim tmpFields As String
    Dim tmpSort As String
    If LineNo >= 0 Then
        If (Not TabEditInline) And (Not AutoTabRead) And (Not ReadOnlyUser) Then
            LineStatus = FileData(Index).GetLineStatusValue(LineNo)
            If LineStatus <> 99 Then
                PushWorkButton "TAB_EDIT_UPDATE", 1
            Else
                PushWorkButton "TAB_EDIT_INSERT", 1
            End If
        ElseIf AutoTabRead Then
            AutoSetReadStatus Index, LineNo
        End If
    ElseIf LineNo = -1 Then
        RemoveEmptyTabLines Index
        SortCol = ColNo
        tmpSort = ""
        If InStr(FileData(Index).LogicalFieldList, "LINO") > 0 Then
            tmpFldName = GetRealItem(FileData(Index).LogicalFieldList, ColNo, ",")
            If tmpFldName <> "LINO" Then
                SortCol = -1
                tmpFields = HiddenData.GetConfigValues(FileData(LastFocusIndex).myName, "EDIT")
                tmpFields = GetRealItem(tmpFields, 2, "|")
                If InStr(tmpFields, tmpFldName) > 0 Then
                    tmpSort = TranslateFieldItems("'SK'", FileData(Index).LogicalFieldList)
                    'If tmpSort <> "" Then
                    '    SetSortLineNoAsc Index
                    '    PrepareSortKeys Index, tmpFldName
                        SortCol = Val(tmpSort)
                    'End If
                End If
            End If
        End If
        FileData(Index).ColSelectionRemoveAll
        If SortCol >= 0 Then
            If ColNo <> LastColNo Then
                If tmpSort <> "" Then
                    SetSortLineNoAsc Index
                    PrepareSortKeys Index, tmpFldName
                End If
                FileData(Index).Sort CStr(SortCol), True, True
            Else
                If FileData(Index).SortOrderASC Then
                    If tmpSort <> "" Then
                        SetSortLineNoDesc Index
                        PrepareSortKeys Index, tmpFldName
                    End If
                    FileData(Index).Sort CStr(SortCol), False, True
                Else
                    If tmpSort <> "" Then
                        SetSortLineNoAsc Index
                        PrepareSortKeys Index, tmpFldName
                    End If
                    FileData(Index).Sort CStr(SortCol), True, True
                End If
            End If
            LastColNo = ColNo
            FileData(Index).ColSelectionAdd ColNo
        End If
        InsertEmptyTabLine Index, -1
        FileData(Index).AutoSizeColumns
        FileData(Index).Refresh
    ElseIf LineNo < -1 Then
        If chkMax(Index).Value = 0 Then chkMax(Index).Value = 1 Else chkMax(Index).Value = 0
        FileData(Index).SetFocus
    End If
End Sub
Private Sub PrepareSortKeys(Index As Integer, SortField As String)
    Dim CurLine As Long
    Dim MaxLine As Long
    Dim tmpFields As String
    Dim tmpFldVal As String
    Dim tmpOldVal As String
    Dim tmpLino As String
    Dim tmpData As String
    MaxLine = FileData(Index).GetLineCount - 1
    For CurLine = 0 To MaxLine
        tmpFldVal = Trim(FileData(Index).GetFieldValue(CurLine, SortField))
        If tmpFldVal = "" Then tmpFldVal = tmpOldVal
        tmpLino = FileData(Index).GetFieldValue(CurLine, "LINO")
        tmpData = tmpFldVal & "." & tmpLino
        FileData(Index).SetFieldValues CurLine, "'SK'", tmpData
        tmpOldVal = tmpFldVal
    Next
End Sub
Private Sub ArrangeMaximizedTab(Index As Integer)
    Dim tmpTag As String
    If TabIsMaximized >= 0 Then
        tmpTag = FileData(TabIsMaximized).Tag
        FileData(TabIsMaximized).Left = Val(GetRealItem(tmpTag, 0, ","))
        FileData(TabIsMaximized).Top = Val(GetRealItem(tmpTag, 1, ","))
        FileData(TabIsMaximized).Width = Val(GetRealItem(tmpTag, 2, ","))
        FileData(TabIsMaximized).Height = Val(GetRealItem(tmpTag, 3, ","))
        TabIsMaximized = -1
    Else
        If Index >= 0 Then
            tmpTag = ""
            tmpTag = tmpTag & CStr(FileData(Index).Left) & ","
            tmpTag = tmpTag & CStr(FileData(Index).Top) & ","
            tmpTag = tmpTag & CStr(FileData(Index).Width) & ","
            tmpTag = tmpTag & CStr(FileData(Index).Height)
            FileData(Index).Tag = tmpTag
            FileData(Index).Top = FileData(0).Top
            FileData(Index).Left = 0
            FileData(Index).ZOrder
            TabIsMaximized = Index
        End If
    End If
End Sub

Private Sub FileData_SendRButtonClick(Index As Integer, ByVal LineNo As Long, ByVal ColNo As Long)
    'Dim LineStatus
    'LineStatus = FileData(Index).GetLineStatusValue(LineNo)
    'MsgBox CStr(LineStatus)
End Sub

Private Sub FontSlider_Scroll()
    Dim i As Integer
    For i = 0 To chkTabIsVisible.UBound
        If chkTabIsVisible(i).Visible Then
            FileData(i).FontSize = FontSlider.Value + 6
            FileData(i).HeaderFontSize = FontSlider.Value + 6
            FileData(i).LineHeight = FontSlider.Value + 6
            FileData(i).SetMainHeaderFont FontSlider.Value + 6, False, False, True, 0, "Courier New"
            FileData(i).AutoSizeColumns
            FileData(i).Refresh
        End If
    Next
End Sub

Private Sub Form_Activate()
    Static AlreadyShown As Boolean
    Dim CurX As Long
    Dim CurY As Long
    If Not AlreadyShown Then
        AlreadyShown = True
        Me.WindowState = vbNormal
        Me.Height = Screen.Height + 180
        Me.Width = Screen.Width + 180
        MaxFormHeight = Me.Height
        MaxFormWidth = Me.Width
        If MainLifeStyle Then
            lblArrRow.ForeColor = vbYellow
            lblDepRow.ForeColor = vbGreen
            DrawBackGround RightPanel, WorkAreaColor, True, True
            DrawBackGround ServerPanel, MainAreaColor, True, True
        Else
            lblArrRow.ForeColor = vbButtonText
            lblDepRow.ForeColor = vbButtonText
        End If
        Me.Width = SetMyWidth
        Me.Height = SetMyHeight
        Me.Left = SetMyLeft
        Me.Top = SetMyTop
        Me.Show
        'chkOnTop.Value = 1
        CheckFipsConnection
        ServerSignal(2).FillStyle = 0
        ServerSignal(2).FillColor = vbYellow
        Me.Refresh
        If Not CedaIsConnected Then
            ServerIsAvailable = False
            UfisServer.ConnectToCeda
        End If
        If CedaIsConnected Then
            ServerIsAvailable = True
            'UfisServer.DisconnectFromCeda
        End If
        AdjustServerSignals False, 0
        WorkArea.AutoRedraw = False
        CurX = 3000
        CurY = 3000
        PrintBackGroundText WorkArea, 18, CurX, CurY, "Loading Basic Data"
        CurX = CurX + 600
        CurY = CurY + 600
        PrintBackGroundText WorkArea, 14, CurX, CurY, "Airline Codes  ..."
        HiddenData.InitBasicData 0, "ALTTAB", "ALC3,ALC2,ALFN"
        CurY = CurY + 450
        PrintBackGroundText WorkArea, 14, CurX, CurY, "Aircraft Types  ..."
        HiddenData.InitBasicData 1, "ACTTAB", "ACT3,ACT5,ACFN"
        CurY = CurY + 450
        PrintBackGroundText WorkArea, 14, CurX, CurY, "Airport Codes  ..."
        HiddenData.InitBasicData 2, "APTTAB", "APC3,APC4,APFN"
        CurY = CurY + 450
        PrintBackGroundText WorkArea, 18, CurX, CurY, "Ready."
        'WorkArea.Refresh
        WorkArea.AutoRedraw = True
        UfisServer.ConnectToBcProxy
        If MainTimer(1).Tag <> "" Then
            MainTimer(1).Interval = 5
            MainTimer(1).Enabled = True
        End If
    End If
End Sub
Private Sub CheckFipsConnection()
    Dim tmpCmd As String
    Dim tmpKey As String
    Dim tmpParam As String
    Dim tmpAreaLine As String
    Dim tmpAreaName As String
    Dim FipsIsConnected As Boolean
    Dim i As Integer
    MainTimer(1).Tag = ""
    FipsIsConnected = False
    tmpCmd = Command
    If tmpCmd <> "" Then
        tmpParam = GetRealItem(tmpCmd, 0, ",")
        tmpKey = "PARAM_" & tmpParam
        tmpAreaLine = GetIniEntry(myIniFullName, "FIPS_CONNECTION", "", tmpKey, "")
        tmpAreaName = GetRealItem(tmpAreaLine, 0, ",")
        If tmpAreaName <> "" Then
            For i = 0 To chkImpTyp.UBound
                If chkImpTyp(i).Tag = tmpAreaName Then
                    MainTimer(1).Tag = "AUTO," & CStr(i)
                    FlightPanel.Tag = tmpAreaLine & "|" & tmpCmd
                    tmpParam = GetRealItem(tmpCmd, 1, "|")
                    LoginUserName = GetRealItem(tmpParam, 0, ",")
                    tmpParam = GetRealItem(tmpParam, 1, ",")
                    If tmpParam = "0" Then ReadOnlyUser = True
                    tmpParam = GetRealItem(tmpCmd, 2, "|")
                    SetMyLeft = Val(GetRealItem(tmpParam, 0, ",")) * 15
                    SetMyTop = Val(GetRealItem(tmpParam, 1, ",")) * 15
                    FipsIsConnected = True
                    Exit For
                End If
            Next
        End If
    End If
    If Not FipsIsConnected Then
        SetMyLeft = 0
        SetMyTop = 0
        'For SnapshotTool only
        MainTimer(1).Tag = "AUTO,0"
    End If
End Sub
Private Sub Form_Load()
    SetMyTop = 0
    SetMyLeft = 0
    SetMyWidth = 1024 * 15
    SetMyHeight = Screen.Height
    Me.Top = Screen.Height + 300
    Me.Left = 0
    LastFocusIndex = -1
    SetButtonFaceStyle Me
    InitApplication
    InitMyForm
End Sub

Private Sub InitApplication()
    Dim tmpData As String
    GetApplPosSize "MAIN", False
    tmpData = Trim(GetIniEntry(myIniFullName, "MAIN", "", "MAIN_LIFE_STYLE", "YES"))
    If Left(tmpData, 1) = "Y" Then MainLifeStyle = True Else MainLifeStyle = False
    tmpData = Trim(GetIniEntry(myIniFullName, "MAIN", "", "GRID_LIFE_STYLE", "YES"))
    MyPrintLogo = GetIniEntry(myIniFullName, "MAIN", "", "PRINT_LOGO", "c:\ufis\system\CustLogo.bmp")
    If Left(tmpData, 1) = "Y" Then GridLifeStyle = True Else GridLifeStyle = False
    FormatDateTime = Trim(GetIniEntry(myIniFullName, "MAIN", "", "FORMAT_DATE_TIME", "DDMMMYY'/'hh':'mm"))
    FormatDatePart = Trim(GetIniEntry(myIniFullName, "MAIN", "", "FORMAT_DATE_PART", "DDMMMYY"))
    FormatTimePart = Trim(GetIniEntry(myIniFullName, "MAIN", "", "FORMAT_TIME_PART", "hh':'mm"))
    FormatDateOnly = Trim(GetIniEntry(myIniFullName, "MAIN", "", "FORMAT_DATE_ONLY", "DDMMMYY"))
    FormatTimeOnly = Trim(GetIniEntry(myIniFullName, "MAIN", "", "FORMAT_TIME_ONLY", "hh':'mm"))
    TopPanel.Tag = "0,-1"
    InitValidSections ""
    TabIsMaximized = -1
End Sub
Private Sub GetApplPosSize(CfgSection As String, SetIt As Boolean)
    SetMyTop = Val(GetIniEntry(myIniFullName, CfgSection, "", "APPL_TOP", CStr(SetMyTop / 15))) * 15
    SetMyLeft = Val(GetIniEntry(myIniFullName, CfgSection, "", "APPL_LEFT", CStr(SetMyLeft / 15))) * 15
    SetMyHeight = Val(GetIniEntry(myIniFullName, CfgSection, "", "APPL_HEIGHT", CStr(SetMyHeight / 15))) * 15
    SetMyWidth = Val(GetIniEntry(myIniFullName, CfgSection, "", "APPL_WIDTH", CStr(SetMyWidth / 15))) * 15
    If SetIt Then
        Me.Width = SetMyWidth
        Me.Height = SetMyHeight
        Me.Left = SetMyLeft
        Me.Top = SetMyTop
        CheckMonitorArea Me
    End If
End Sub
Public Sub InitValidSections(CfgSections As String)
    Dim DefaultButtons As String
    Dim ValidSections As String
    Dim LastButton As Integer
    Dim ItmNbr As Integer
    Dim tmpData As String
    Dim ButtonIndex As Integer
    Dim BreakOut As Boolean
    Dim LeftPos As Long
    Dim TopPos As Long
    Me.Caption = GetIniEntry(myIniFullName, "MAIN", "", "APPL_TITLE", Me.Caption)
    tmpData = Trim(GetIniEntry(myIniFullName, "MAIN", "", "APPL_EXIT", "Exit,EXIT"))
    chkAppl(6).Caption = GetRealItem(tmpData, 0, ",")
    chkAppl(6).Tag = GetRealItem(tmpData, 1, ",")
    tmpData = Trim(GetIniEntry(myIniFullName, "MAIN", "", "SERVER_SIGNALS", "YES"))
    If Left(tmpData, 1) = "Y" Then
        ServerPanel.Visible = True
        ServerPanel.Tag = "SHOW"
    Else
        ServerPanel.Visible = False
        ServerPanel.Tag = "HIDE"
    End If
    DefaultButtons = "B,Restore,RESTORE"
    ArrangeWorkButtons 0, "MAIN", DefaultButtons
    MainAreaColor = Val(GetIniEntry(myIniFullName, "MAIN", "", "MAIN_AREA_COLOR", "7"))
    WorkAreaColor = MainAreaColor
    ShowMaxButtons = Abs(Val(GetIniEntry(myIniFullName, "MAIN", "", "MAX_BUTTONS", "12"))) - 1
    If ShowMaxButtons < 1 Then ShowMaxButtons = 1
    If ShowMaxButtons > 12 Then ShowMaxButtons = 12
    LastButton = Val(GetRealItem(fraTopButtonPanel(0).Tag, 1, ","))
    If LastButton >= 0 Then
        LeftPos = chkWork(LastButton).Left + chkWork(LastButton).Width + 15
    Else
        LeftPos = 0
    End If
    TopPos = 0
    For ButtonIndex = 0 To chkImpTyp.UBound
        chkImpTyp(ButtonIndex).Visible = False
        chkImpTyp(ButtonIndex).Value = 0
    Next
    ValidSections = GetIniEntry(myIniFullName, "MAIN", "", "APPL_TYPES", CfgSections)
    ButtonIndex = -1
    ItmNbr = 0
    BreakOut = False
    Do
        ItmNbr = ItmNbr + 1
        tmpData = Trim(GetItem(ValidSections, ItmNbr, ","))
        If tmpData <> "" Then
            ButtonIndex = ButtonIndex + 1
            If ButtonIndex > chkImpTyp.UBound Then Load chkImpTyp(ButtonIndex)
            Set chkImpTyp(ButtonIndex).Container = fraTopButtonPanel(0)
            If ButtonIndex <= chkImpTyp.UBound Then
                chkImpTyp(ButtonIndex).Top = TopPos
                chkImpTyp(ButtonIndex).Left = LeftPos
                chkImpTyp(ButtonIndex).Tag = tmpData
                tmpData = Trim(GetIniEntry(myIniFullName, chkImpTyp(ButtonIndex).Tag, "", "BUTTON_VISIBLE", "YES"))
                If (Left(tmpData, 1) = "Y") And (ButtonIndex <= ShowMaxButtons) Then
                    chkImpTyp(ButtonIndex).Visible = True
                    LeftPos = LeftPos + chkImpTyp(ButtonIndex).Width + 15
                Else
                    chkImpTyp(ButtonIndex).Visible = False
                End If
                tmpData = Trim(GetIniEntry(myIniFullName, chkImpTyp(ButtonIndex).Tag, "", "BUTTON_CAPTION", ""))
                If tmpData <> "" Then
                    chkImpTyp(ButtonIndex).Caption = tmpData
                    chkImpTyp(ButtonIndex).ToolTipText = LTrim(Trim(GetIniEntry(myIniFullName, chkImpTyp(ButtonIndex).Tag, "", "TOOLTIP_TEXT", "")) & " ")
                Else
                    chkImpTyp(ButtonIndex).Caption = "???" & Str(ItmNbr)
                    chkImpTyp(ButtonIndex).ToolTipText = "Missing configuration key: 'BUTTON_CAPTION' "
                    chkImpTyp(ButtonIndex).Tag = "NOTHING"
                End If
                tmpData = Trim(GetIniEntry(myIniFullName, chkImpTyp(ButtonIndex).Tag, "", "BUTTON_ACTIVATE", "NO"))
                If (Left(tmpData, 1) = "Y") Then
                    MainTimer(1).Tag = "PUSH," & CStr(ButtonIndex)
                End If
            End If
        Else
            BreakOut = True
        End If
    Loop While Not BreakOut
    fraTopButtonPanel(0).Width = LeftPos
    fraTopButtonPanel(0).Height = chkImpTyp(0).Height + 15
    TopPanel.Height = fraTopButtonPanel(0).Top + fraTopButtonPanel(0).Height + 75
    ImpScroll.Visible = False
    If ButtonIndex < 0 Then
        MsgBox "No valid configuration found."
    Else
        If ButtonIndex > ShowMaxButtons Then
            ImpScroll.Tag = Trim(Str(ShowMaxButtons))
            ImpScroll.Max = ButtonIndex - ShowMaxButtons
            ImpScroll.Left = LeftPos - Screen.TwipsPerPixelX
            'ImpScroll.Visible = True
            LeftPos = LeftPos + ImpScroll.Width + Screen.TwipsPerPixelX - Screen.TwipsPerPixelX
        End If
    End If
End Sub

Private Sub InitMyForm()
    Dim i As Integer
    Dim tmpData As String
    TopPanel.Top = 0
    TopPanel.Left = 0
    WorkArea.Top = TopPanel.Height
    WorkArea.Left = 0
    ServerPanel.Left = -30
    ServerPanel.Height = BottomPanel.Height + 15
    ServerPanel.Width = RightPanel.Width + 60
    FontSlider.Top = 0
    'FontSlider.Left = -90
    'FontSlider.Width = ServerPanel.Width + 120
    'FontSlider.Height = ServerPanel.Height - 15
    TopPanel.BackColor = MyOwnButtonFace
    WorkArea.BackColor = MyOwnButtonFace
    BottomPanel.BackColor = MyOwnButtonFace
    RightPanel.BackColor = MyOwnButtonFace
    DelayPanel.BackColor = MyOwnButtonFace
    ServerPanel.BackColor = MyOwnButtonFace
    FileData(0).Top = 0
    FileData(0).Left = 0
    For i = 0 To FileData.UBound
        FileData(i).ResetContent
        FileData(i).HeaderString = "Main Data Area"
        FileData(i).HeaderLengthString = "5000"
        FileData(i).ShowHorzScroller True
    Next
    fraVtSplitter(0).Top = 0
    fraVtSplitter(0).Left = 9000
    fraVtSplitter(0).Tag = "-1,-1"
    fraHzSplitter(0).Tag = "-1,-1"
    fraCrSplitter(0).Tag = "-1,-1"
    fraTopButtonPanel(0).Top = 30
    TopPanel.ZOrder
    BottomPanel.ZOrder
    RightPanel.ZOrder
    StatusBar.ZOrder
    MaxLeftWidth = 2500
    MaxRightWidth = 4000
    MinLeftWidth = 900
    MinRightWidth = 900
    MinFrameSize = MinLeftWidth + MinRightWidth
    ImpScroll.ZOrder
    lblSrvName(0).Caption = UfisServer.HostName.Text
    Load lblSrvName(1)
    lblSrvName(1).ForeColor = vbWhite
    lblSrvName(0).ForeColor = vbBlack
    lblSrvName(1).Visible = True
    lblSrvName(0).Left = 0
    lblSrvName(1).Left = lblSrvName(0).Left + 15
    lblSrvName(1).Top = lblSrvName(0).Top + 15
    lblSrvName(0).Width = RightPanel.Width - 90
    lblSrvName(1).Width = RightPanel.Width - 90
    lblSrvName(0).ZOrder
    ResetMain
    tmpData = GetIniEntry(myIniFullName, "MAIN", "", "FONT_SIZE", "8,11,32")
    FontSlider.Min = Val(GetItem(tmpData, 1, ","))
    FontSlider.Max = Val(GetItem(tmpData, 3, ","))
    FontSlider.Value = Val(GetItem(tmpData, 2, ","))
'    Me.Top = -Screen.Height - 300
'    Me.Left = 0
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    If UnloadMode = 0 Then
        Cancel = True
        chkAppl(6).Value = 1
    End If
End Sub

Private Sub Form_Resize()
    Dim NewHeight As Long
    Dim NewWidth As Long
    Dim NewLeft As Long
    Dim NewTop As Long
    Dim MaxTop As Long
    Dim i As Integer
    i = chkImpTyp.UBound
    NewLeft = chkImpTyp(i).Left + chkImpTyp(i).Width
    'If RightPanel.Left < NewLeft Then ImpScroll.Visible = True Else ImpScroll.Visible = False
    NewHeight = Me.ScaleHeight - TopPanel.Height - StatusBar.Height
    If BottomPanel.Visible Then NewHeight = NewHeight - BottomPanel.Height
    If NewHeight > 600 Then
        WorkArea.Height = NewHeight
        DelayPanel.Height = NewHeight
    End If
    NewWidth = Me.ScaleWidth - RightPanel.Width
    If NewWidth >= MinFrameSize Then
        TopPanel.Width = NewWidth
        BottomPanel.Width = NewWidth
        If FlightPanel.Visible Then
            FlightPanel.Width = NewWidth - 45
            If NewWidth > (lblArr(9).Left + 300) Then
                lblArr(9).Width = NewWidth - lblArr(9).Left - 210
                lblDep(9).Width = NewWidth - lblDep(9).Left - 210
            End If
        End If
        If DelayPanel.Visible Then
            NewWidth = NewWidth - DelayPanel.Width
            DelayPanel.Left = NewWidth
        End If
        If NewWidth >= MinFrameSize Then
            WorkArea.Width = NewWidth
        End If
    End If
    MaxTop = fraYButtonPanel(0).Top + fraYButtonPanel(0).Height - 390
    For i = chkTabIsVisible.UBound To 0 Step -1
        If chkTabIsVisible(i).Visible Then
            MaxTop = chkTabIsVisible(i).Top + chkTabIsVisible(i).Height + 90
            Exit For
        End If
    Next
    NewTop = Me.ScaleHeight - StatusBar.Height - BottomPanel.Height
    BottomPanel.Top = NewTop
    ServerPanel.Top = NewTop - 45
    If ServerPanel.Tag = "SHOW" Then
        If NewTop > MaxTop Then ServerPanel.Visible = True Else ServerPanel.Visible = False
    End If
    
    ArrangeLayout -1
    If MainLifeStyle Then
        If (CountMainTabs + CountSubTabs) = 0 Then
            DrawBackGround WorkArea, WorkAreaColor, True, True
        End If
        DrawBackGround DelayPanel, WorkAreaColor, False, True
        'DrawBackGround RightPanel, WorkAreaColor, True, True
        MaxFormHeight = Me.Height
        DrawBackGround TopPanel, WorkAreaColor, True, True
        DrawBackGround BottomPanel, WorkAreaColor, True, True
        DrawBackGround FlightPanel, WorkAreaColor, True, True
        MaxFormWidth = Me.Width
    End If
        
End Sub
Private Sub ArrangeLayout(Index As Integer)
    Dim NewHeight As Long
    Dim NewWidth As Long
    Dim NewLeft As Long
    Dim FraLeft As Long
    Dim NewTop As Long
    Dim LeftTop As Long
    Dim RightTop As Long
    Dim LeftHeight As Long
    Dim LeftRest As Long
    Dim RightHeight As Long
    Dim LeftWidth As Long
    Dim RightWidth As Long
    Dim ReturnColor As Long
    Dim tmpTag As String
    Dim i As Integer
    Dim j As Integer
    If TabIsMaximized >= 0 Then
        FileData(TabIsMaximized).Width = WorkArea.ScaleWidth
        FileData(TabIsMaximized).Height = WorkArea.ScaleHeight
        CheckMinMaxPanel TabIsMaximized
    Else
        LeftRest = -1
        For i = 0 To FileData.UBound
            FileData(i).Visible = False
            MinMaxPanel(i).Visible = False
        Next
        If Index <= 0 Then
            NewHeight = WorkArea.ScaleHeight
            NewWidth = WorkArea.ScaleWidth
            If CountMainTabs > 0 Then
                If CountMainTabs = 2 Then
                    LeftHeight = (NewHeight - fraHzSplitter(0).Height) * SplitPosV / 100
                    LeftRest = NewHeight - LeftHeight - fraHzSplitter(0).Height
                Else
                    LeftHeight = (NewHeight - (fraHzSplitter(0).Height * CLng(CountMainTabs - 1))) / CLng(CountMainTabs)
                End If
            End If
            If CountSubTabs > 0 Then
                RightHeight = (NewHeight - (fraHzSplitter(0).Height * CLng(CountSubTabs - 1))) / CLng(CountSubTabs)
            End If
            If (CountMainTabs > 0) And (CountSubTabs > 0) Then
                    LeftWidth = fraVtSplitter(0).Left
                    If Index < 0 Then LeftWidth = NewWidth * SplitPosH / 100
                    FraLeft = LeftWidth
                    NewLeft = FraLeft + fraVtSplitter(0).Width
                    RightWidth = NewWidth - NewLeft
                    fraVtSplitter(0).Left = FraLeft
                    fraVtSplitter(0).Height = NewHeight
                    SplitPosH = LeftWidth * 100 / NewWidth
            ElseIf CountMainTabs > 0 Then
                NewLeft = NewWidth
                FraLeft = NewLeft
                LeftWidth = NewWidth
            ElseIf CountSubTabs > 0 Then
                NewLeft = 0
                FraLeft = NewWidth
                RightWidth = NewWidth
            End If
            fraVtSplitter(0).Refresh
            If RightWidth < MinRightWidth Then RightWidth = MinRightWidth
            LeftTop = 0
            For i = 0 To chkTabIsVisible.UBound
                If chkTabIsVisible(i).Value = 1 Then
                    tmpTag = chkTabIsVisible(i).Tag
                    j = Val(Mid(tmpTag, 3))
                    If Left(tmpTag, 1) = "L" Then
                        'Left MainTabs
                        If i > 0 And LeftRest > 0 Then LeftHeight = LeftRest
                        If Index < 0 Then
                            FileData(i).Top = LeftTop
                            FileData(i).Height = LeftHeight
                        End If
                        FileData(i).Left = 0
                        FileData(i).Width = LeftWidth
                        LeftTop = LeftTop + LeftHeight
                        If Index < 0 Then
                            If j >= 0 Then
                                fraHzSplitter(j).Top = LeftTop
                                fraCrSplitter(j).Top = LeftTop
                            End If
                            LeftTop = LeftTop + fraHzSplitter(0).Height
                        End If
                        If j >= 0 Then
                            fraCrSplitter(j).Left = fraVtSplitter(0).Left - 150
                            fraCrSplitter(j).Refresh
                            fraHzSplitter(j).Left = 0
                            fraHzSplitter(j).Width = LeftWidth
                            fraHzSplitter(j).Refresh
                        End If
                    Else
                        'Right SubTabs
                        If Index < 0 Then
                            FileData(i).Top = RightTop
                            If RightHeight >= 0 Then
                                FileData(i).Height = RightHeight
                            End If
                        End If
                        FileData(i).Left = NewLeft
                        FileData(i).Width = RightWidth
                        RightTop = RightTop + RightHeight
                        If Index < 0 Then
                            If j >= 0 Then
                                fraHzSplitter(j).Top = RightTop
                                fraCrSplitter(j).Top = RightTop
                            End If
                            RightTop = RightTop + fraHzSplitter(0).Height
                        End If
                        If j >= 0 Then
                            fraCrSplitter(j).Left = fraVtSplitter(0).Left + 105
                            fraCrSplitter(j).Refresh
                            fraHzSplitter(j).Left = NewLeft
                            fraHzSplitter(j).Width = RightWidth
                            fraHzSplitter(j).Refresh
                        End If
                    End If
                End If
            Next
        End If
        For i = 0 To chkTabIsVisible.UBound
            If chkTabIsVisible(i).Value = 1 Then
                CheckMinMaxPanel i
                FileData(i).Visible = True
                FileData(i).Refresh
            End If
        Next
    End If
End Sub
Private Sub CheckMinMaxPanel(Index As Integer)
    Dim VisibleLines As Long
    Dim PanelOffset As Long
    VisibleLines = (((FileData(Index).Height - 90) / 15) / FileData(Index).LineHeight) - 3
    If VisibleLines > FileData(Index).GetLineCount Then PanelOffset = 0 Else PanelOffset = 14 * 15
    MinMaxPanel(Index).Top = FileData(Index).Top + 15
    MinMaxPanel(Index).Left = FileData(Index).Left + FileData(Index).Width - MinMaxPanel(Index).Width - PanelOffset
    MinMaxPanel(Index).Visible = True
    MinMaxPanel(Index).ZOrder
    MinMaxPanel(Index).Refresh
End Sub
Private Sub fraCrSplitter_MouseDown(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
    SplitterMoves = True
    fraCrSplitter(0).Tag = CStr(X) & "," & CStr(Y)
    CheckTabsSelected fraHzSplitter(Index).Tag, True
End Sub
Private Sub fraCrSplitter_MouseUp(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
    CheckTabsSelected fraHzSplitter(Index).Tag, False
    fraCrSplitter(0).Tag = "-1,-1"
    SplitterMoves = False
    If LastFocusIndex >= 0 Then FileData(LastFocusIndex).SetFocus
End Sub
Private Sub fraCrSplitter_MouseMove(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
    Dim NewLeft As Long
    Dim NewRight As Long
    Dim NewTop As Long
    Dim NewHeight1 As Long
    Dim NewHeight2 As Long
    Dim OldX As Long
    Dim NewX As Long
    Dim OldY As Long
    Dim NewY As Long
    Dim tmpTag As String
    Dim tmpDiff As Long
    Dim ReturnColor As Long
    Dim i1 As Integer
    Dim i2 As Integer
    tmpTag = fraCrSplitter(0).Tag
    If tmpTag = "" Then tmpTag = "-1,-1"
    If tmpTag <> "-1,-1" Then
        OldX = CLng(GetItem(tmpTag, 1, ","))
        NewX = CLng(X)
        If OldX <> NewX Then
            tmpDiff = NewX - OldX
            NewLeft = fraCrSplitter(Index).Left + tmpDiff
            tmpDiff = fraVtSplitter(0).Left - fraCrSplitter(Index).Left
            NewRight = WorkArea.ScaleWidth - NewLeft - fraVtSplitter(0).Width
            If (NewLeft >= MinLeftWidth) And (NewRight >= MinRightWidth) Then
                fraVtSplitter(0).Left = NewLeft + tmpDiff
                ArrangeLayout 0
            End If
        End If
        OldY = CLng(GetItem(tmpTag, 2, ","))
        NewY = CLng(Y)
        If OldY <> NewY Then
            tmpDiff = NewY - OldY
            NewTop = fraHzSplitter(Index).Top + tmpDiff
            tmpTag = fraHzSplitter(Index).Tag
            i1 = Val(GetRealItem(tmpTag, 0, ","))
            i2 = Val(GetRealItem(tmpTag, 1, ","))
            NewHeight1 = NewTop - FileData(i1).Top
            NewHeight2 = FileData(i2).Top + FileData(i2).Height - NewTop - fraHzSplitter(0).Height
            If (NewHeight1 > 600) And (NewHeight2 > 600) Then
                FileData(i1).Visible = False
                FileData(i2).Visible = False
                fraCrSplitter(Index).Visible = False
                fraHzSplitter(Index).Visible = False
                fraVtSplitter(0).Refresh
                FileData(i1).Height = NewHeight1
                fraHzSplitter(Index).Top = NewTop
                fraCrSplitter(Index).Top = NewTop
                NewTop = NewTop + fraHzSplitter(0).Height
                FileData(i2).Top = NewTop
                FileData(i2).Height = NewHeight2
                fraHzSplitter(Index).Visible = True
                fraHzSplitter(Index).Refresh
                fraCrSplitter(Index).Visible = True
                fraCrSplitter(Index).Refresh
                MinMaxPanel(i2).Top = FileData(i2).Top + 15
                FileData(i1).Visible = True
                FileData(i2).Visible = True
                FileData(i1).Refresh
                FileData(i2).Refresh
            End If
        End If
    End If
End Sub

Private Sub fraHzSplitter_MouseDown(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
    SplitterMoves = True
    fraHzSplitter(0).Tag = CStr(X) & "," & CStr(Y)
    CheckTabsSelected fraHzSplitter(Index).Tag, True
End Sub
Private Sub fraHzSplitter_MouseUp(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
    CheckTabsSelected fraHzSplitter(Index).Tag, False
    fraHzSplitter(0).Tag = "-1,-1"
    If (Index = 1) And (CountMainTabs = 2) Then
        SplitPosV = FileData(0).Height * 100 / (WorkArea.Height - fraHzSplitter(0).Height)
    End If
    SplitterMoves = False
    If LastFocusIndex >= 0 Then
        FileData(LastFocusIndex).SetFocus
    End If
End Sub
Private Sub fraHzSplitter_MouseMove(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
    Dim NewTop As Long
    Dim NewHeight1 As Long
    Dim NewHeight2 As Long
    Dim OldY As Long
    Dim NewY As Long
    Dim tmpTag As String
    Dim tmpDiff As Long
    Dim ReturnColor As Long
    Dim i1 As Integer
    Dim i2 As Integer
    tmpTag = fraHzSplitter(0).Tag
    If tmpTag <> "-1,-1" Then
        OldY = CLng(GetItem(tmpTag, 2, ","))
        NewY = CLng(Y)
        If OldY <> NewY Then
            tmpDiff = NewY - OldY
            NewTop = fraHzSplitter(Index).Top + tmpDiff
            tmpTag = fraHzSplitter(Index).Tag
            i1 = Val(GetRealItem(tmpTag, 0, ","))
            i2 = Val(GetRealItem(tmpTag, 1, ","))
            NewHeight1 = NewTop - FileData(i1).Top
            NewHeight2 = FileData(i2).Top + FileData(i2).Height - NewTop - fraHzSplitter(0).Height
            If (NewHeight1 > 600) And (NewHeight2 > 600) Then
                FileData(i1).Visible = False
                FileData(i2).Visible = False
                fraCrSplitter(Index).Visible = False
                fraHzSplitter(Index).Visible = False
                fraVtSplitter(0).Refresh
                FileData(i1).Height = NewHeight1
                fraHzSplitter(Index).Top = NewTop
                fraCrSplitter(Index).Top = NewTop
                NewTop = NewTop + fraHzSplitter(0).Height
                FileData(i2).Top = NewTop
                FileData(i2).Height = NewHeight2
                fraHzSplitter(Index).Visible = True
                fraHzSplitter(Index).Refresh
                fraCrSplitter(Index).Visible = True
                fraCrSplitter(Index).Refresh
                MinMaxPanel(i2).Top = FileData(i2).Top + 15
                FileData(i1).Visible = True
                FileData(i2).Visible = True
                FileData(i1).Refresh
                FileData(i2).Refresh
            End If
        End If
    End If
End Sub

Private Sub fraVtSplitter_MouseDown(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
    fraVtSplitter(0).Tag = CStr(X) & "," & CStr(Y)
End Sub

Private Sub fraVtSplitter_MouseUp(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
    fraVtSplitter(0).Tag = "-1,-1"
    If LastFocusIndex >= 0 Then FileData(LastFocusIndex).SetFocus
End Sub
Private Sub fraVtSplitter_MouseMove(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
    Dim NewLeft As Long
    Dim NewRight As Long
    Dim OldX As Long
    Dim NewX As Long
    Dim tmpTag As String
    Dim tmpDiff As Long
    tmpTag = fraVtSplitter(Index).Tag
    If tmpTag <> "-1,-1" Then
        If Index = 0 Then
            OldX = CLng(GetItem(tmpTag, 1, ","))
            NewX = CLng(X)
            If OldX <> NewX Then
                tmpDiff = NewX - OldX
                NewLeft = fraVtSplitter(Index).Left + tmpDiff
                NewRight = WorkArea.ScaleWidth - NewLeft - fraVtSplitter(Index).Width
                If (NewLeft >= MinLeftWidth) And (NewRight >= MinRightWidth) Then
                    fraVtSplitter(Index).Left = NewLeft
                    ArrangeLayout Index
                End If
            End If
        End If
    End If
End Sub
Private Sub CheckTabsSelected(TabList As String, Selected As Boolean)
    Dim tmpTag As String
    Dim i1 As Integer
    Dim i2 As Integer
    tmpTag = TabList
    i1 = Val(GetRealItem(tmpTag, 0, ","))
    i2 = Val(GetRealItem(tmpTag, 1, ","))
    If Selected Then
        If i1 >= 0 Then
            chkTabIsVisible(i1).Appearance = 0
            'lblTabName(i1).FontBold = True
        End If
        If i2 >= 0 Then
            chkTabIsVisible(i2).Appearance = 0
            'lblTabName(i2).FontBold = True
        End If
    Else
        If i1 >= 0 Then
            chkTabIsVisible(i1).Appearance = 1
            'lblTabName(i1).FontBold = False
        End If
        If i2 >= 0 Then
            chkTabIsVisible(i2).Appearance = 1
            'lblTabName(i2).FontBold = False
        End If
    End If
End Sub
Private Sub ResetMain()
    Dim i As Integer
    ResetFlightPanel
    WorkArea.Visible = False
    For i = 0 To FileData.UBound
        FileData(i).ResetContent
    Next
    For i = 0 To fraTopButtonPanel.UBound
        fraTopButtonPanel(i).Visible = False
    Next
    For i = 0 To chkTabIsVisible.UBound
        chkTabIsVisible(i).Value = 0
        chkTabIsVisible(i).Visible = False
        chkTabIsVisible(i).Tag = ""
        lblTabName(i).Visible = False
        lblTabShadow(i).Visible = False
    Next
    For i = 0 To fraHzSplitter.UBound
        fraCrSplitter(i).Visible = False
        fraHzSplitter(i).Visible = False
    Next
    For i = 0 To fraVtSplitter.UBound
        fraVtSplitter(i).Visible = False
    Next
    TopPanel.Height = fraTopButtonPanel(0).Top + fraTopButtonPanel(0).Height + 75
    WorkArea.Top = TopPanel.Height
    fraTopButtonPanel(0).Visible = True
    Form_Resize
    WorkArea.Visible = True
    AdjustServerSignals False, 0
    Me.Refresh
End Sub

Public Function AdjustServerSignals(JustReading As Boolean, SignalIdx As Integer)
    If UfisServer.HostName.Text = "LOCAL" Then
        ServerSignal(3).FillStyle = 0
        ServerSignal(3).FillColor = vbBlue
    Else
        If ServerIsAvailable Then
            ServerSignal(3).FillStyle = 0
            ServerSignal(3).FillColor = vbGreen
            If CedaIsConnected Then
                ServerSignal(2).FillStyle = 0
                ServerSignal(2).FillColor = vbGreen
            Else
                ServerSignal(2).FillStyle = 0
                ServerSignal(2).FillColor = vbBlue
            End If
            If JustReading Then
                ServerSignal(2).FillColor = vbGreen
                ServerSignal(2).Refresh
                ServerSignal(SignalIdx).FillColor = vbYellow
                ServerSignal(SignalIdx).FillStyle = 0
                ServerSignal(SignalIdx).Refresh
            Else
                ServerSignal(SignalIdx).FillColor = vbGreen
                ServerSignal(SignalIdx).FillStyle = 1
                ServerSignal(SignalIdx).Refresh
                ServerSignal(2).FillStyle = 0
                ServerSignal(2).FillColor = vbBlue
                ServerSignal(2).Refresh
            End If
        Else
            ServerSignal(3).FillStyle = 0
            ServerSignal(3).FillColor = vbRed
            ServerSignal(2).FillStyle = 0
            ServerSignal(2).FillColor = vbRed
        End If
    End If
End Function

Private Sub lblDlyReason_MouseDown(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
    If Button = 1 Then
        If Not ReadOnlyUser Then
            lblDlyReason(Index).Tag = CStr(chkDlyTick(Index).Value)
            chkDlyTick(Index).Value = 2
        End If
    End If
End Sub

Private Sub lblDlyReason_MouseUp(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
    If Button = 1 Then
        If Not ReadOnlyUser Then
            If Val(lblDlyReason(Index).Tag) = 1 Then
                chkDlyTick(Index).Value = 0
            Else
                chkDlyTick(Index).Value = 1
            End If
        End If
    End If
End Sub

Private Sub lblTabName_MouseDown(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
    If Button = 1 Then
        If chkTabIsVisible(Index).Enabled Then
            lblTabName(Index).Tag = CStr(chkTabIsVisible(Index).Value)
            chkTabIsVisible(Index).Value = 2
        End If
    End If
End Sub

Private Sub lblTabName_MouseUp(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
    If Button = 1 Then
        If chkTabIsVisible(Index).Enabled Then
            If Val(lblTabName(Index).Tag) = 1 Then
                chkTabIsVisible(Index).Value = 0
            Else
                chkTabIsVisible(Index).Value = 1
            End If
        End If
    End If
End Sub

Private Sub MainTimer_Timer(Index As Integer)
    Dim CurActionCmd As String
    Dim tmpData As String
    Dim i As Integer
    CurActionCmd = GetRealItem(MainTimer(Index).Tag, 0, ",")
    Select Case CurActionCmd
        Case "REFRESH"
            MainTimer(Index).Enabled = False
            WorkArea.Refresh
        Case "PUSH"
            MainTimer(Index).Enabled = False
            tmpData = GetRealItem(MainTimer(Index).Tag, 1, ",")
            i = Val(tmpData)
            chkImpTyp(i).Value = 1
        Case "AUTO"
            MainTimer(Index).Enabled = False
            tmpData = GetRealItem(MainTimer(Index).Tag, 1, ",")
            i = Val(tmpData)
            chkImpTyp(i).Value = 1
            chkTask(0).Value = 1    'AutoLoad (Refresh)
        Case "STOP_EXCEL"
            MainTimer(Index).Enabled = False
            PushWorkButton "EXCEL", 0
        Case Else
            MainTimer(Index).Enabled = False
    End Select
End Sub

Private Sub ResetFlightPanel()
    Dim i As Integer
    For i = 0 To lblArr.UBound
        lblArr(i).Caption = ""
        lblArr(i).BackColor = DarkGray
        lblDep(i).Caption = ""
        lblDep(i).BackColor = DarkGray
    Next
    FlightPanel.Refresh
End Sub
Private Sub InitFlightPanel(ResetIt As Boolean)
    Dim tmpTag As String
    Dim tmpFipsCfg As String
    Dim tmpFipsParam As String
    Dim tmpFipsTurnType As String
    Dim tmpArrUrno As String
    Dim tmpDepUrno As String
    Dim CedaData As String
    Dim tmpRec As String
    Dim tmpArrRec As String
    Dim tmpDepRec As String
    Dim tmpUrno As String
    Dim FldLst As String
    Dim SqlKey As String
    Dim tmpMyCall As String
    Dim MsgText As String
    Dim ArrColor As Long
    Dim DepColor As Long
    Dim FlnuValue As Long
    Dim i As Integer
    If ResetIt Then
        ResetFlightPanel
        tmpTag = FlightPanel.Tag
        tmpFipsCfg = GetRealItem(tmpTag, 0, "|")
        tmpFipsParam = GetRealItem(tmpTag, 1, "|")
        tmpFipsTurnType = GetRealItem(tmpFipsCfg, 1, ",")
        tmpArrUrno = GetRealItem(tmpFipsParam, 1, ",")
        tmpDepUrno = GetRealItem(tmpFipsParam, 2, ",")
        If (tmpArrUrno <> "") Or (tmpDepUrno <> "") Then
            FldLst = "FLNO,STOD,STOA,ETDI,ETAI,OFBL,ONBL,ACT3,ACT5,REGN,TIFA,TIFD,REM1,BAA1,BAA2,BAA3,URNO"
            SqlKey = "WHERE URNO=" & tmpArrUrno & " OR URNO=" & tmpDepUrno
            'UfisServer.ConnectToCeda
            UfisServer.CallCeda CedaData, "GFR", "AFTTAB", FldLst, "", SqlKey, "", 0, True, False
            'UfisServer.DisconnectFromCeda
        Else
            CedaData = ""
        End If
        CedaData = Replace(CedaData, vbCr, "", 1, -1, vbBinaryCompare)
        tmpRec = GetRealItem(CedaData, 0, vbLf)
        tmpUrno = GetFieldValue("URNO", tmpRec, FldLst)
        If tmpUrno = tmpArrUrno Then tmpArrRec = tmpRec
        If tmpUrno = tmpDepUrno Then tmpDepRec = tmpRec
        tmpRec = GetRealItem(CedaData, 1, vbLf)
        tmpUrno = GetFieldValue("URNO", tmpRec, FldLst)
        If tmpUrno = tmpArrUrno Then tmpArrRec = tmpRec
        If tmpUrno = tmpDepUrno Then tmpDepRec = tmpRec
        Select Case tmpFipsTurnType
            Case "ARR"
                If tmpArrRec <> "" Then ArrColor = vbWhite Else ArrColor = vbRed
                If tmpDepRec <> "" Then DepColor = NormalGray Else DepColor = DarkGray
                If MainLifeStyle Then lblArrRow.ForeColor = vbYellow Else lblArrRow.ForeColor = vbButtonText
                If MainLifeStyle Then lblDepRow.ForeColor = NormalGray Else lblDepRow.ForeColor = vbButtonText
            Case "DEP"
                If tmpDepRec <> "" Then DepColor = vbWhite Else DepColor = vbRed
                If tmpArrRec <> "" Then ArrColor = NormalGray Else ArrColor = DarkGray
                If MainLifeStyle Then lblDepRow.ForeColor = vbGreen Else lblDepRow.ForeColor = vbButtonText
                If MainLifeStyle Then lblArrRow.ForeColor = vbBlack Else lblArrRow.ForeColor = vbButtonText
            Case "ROT"
                If tmpArrRec <> "" Then ArrColor = vbWhite Else ArrColor = vbRed
                If tmpDepRec <> "" Then DepColor = vbWhite Else DepColor = vbRed
                If MainLifeStyle Then lblArrRow.ForeColor = vbYellow Else lblArrRow.ForeColor = vbButtonText
                If MainLifeStyle Then lblDepRow.ForeColor = vbGreen Else lblDepRow.ForeColor = vbButtonText
            Case Else
        End Select
        For i = 0 To lblArr.UBound
            lblArr(i).BackColor = ArrColor
            lblDep(i).BackColor = DepColor
        Next
        ShowArrRecord tmpArrRec, FldLst
        ShowDepRecord tmpDepRec, FldLst
        txtAftData(0).Text = tmpFipsTurnType
        txtAftData(1).Text = tmpArrUrno
        txtAftData(2).Text = tmpArrRec
        txtAftData(3).Text = tmpDepUrno
        txtAftData(4).Text = tmpArrRec
        For i = 0 To chkTabIsVisible.UBound
            If chkTabIsVisible(i).Visible Then
                HiddenData.UpdateTabConfig FileData(i).myName, "CALL", tmpFipsTurnType
            End If
        Next
        FlightPanel.Refresh
        MsgText = ""
        For i = 0 To chkTabIsVisible.UBound
            If chkTabIsVisible(i).Visible Then
                tmpMyCall = HiddenData.GetConfigValues(FileData(i).myName, "CALL")
                Select Case tmpMyCall
                    Case "ARR"
                        FlnuValue = Val(tmpArrUrno)
                        If FlnuValue <= 0 Then MsgText = "Warning: The Arrival Flight doesn't exist."
                    Case "DEP"
                        FlnuValue = Val(tmpDepUrno)
                        If FlnuValue <= 0 Then MsgText = "Warning: The Departure Flight doesn't exist."
                    Case Else
                End Select
            End If
        Next
        If MsgText <> "" Then
            If MyMsgBox.CallAskUser(0, 0, 0, "Selected Flight", MsgText, "info", "", UserAnswer) = 1 Then DoNothing
        End If
    End If
End Sub
Public Sub ShowArrRecord(ArrData As String, FieldList As String)
    Dim FldNam As String
    Dim FldVal As String
    Dim tmpVal As String
    Dim ItemNo As Long
    ItemNo = 0
    FldNam = GetRealItem(FieldList, ItemNo, ",")
    While FldNam <> ""
        FldVal = GetRealItem(ArrData, ItemNo, ",")
        Select Case FldNam
            Case "FLNO"
                tmpVal = Trim(Left(FldVal, 3))
                lblArr(0).Caption = tmpVal
                lblArr(0).Tag = FldVal
                tmpVal = Trim(Mid(FldVal, 4))
                lblArr(1).Caption = tmpVal
            Case "STOA"
                tmpVal = Left(FldVal, 8)
                tmpVal = MyDateFormat(tmpVal, False)
                lblArr(2).Caption = tmpVal
                lblArr(2).Tag = FldVal
                tmpVal = Mid(FldVal, 9, 4)
                If tmpVal <> "" Then tmpVal = Left(tmpVal, 2) & ":" & Right(tmpVal, 2)
                lblArr(3).Caption = tmpVal
            Case "ETAI"
                tmpVal = Mid(FldVal, 9, 4)
                If tmpVal <> "" Then tmpVal = Left(tmpVal, 2) & ":" & Right(tmpVal, 2)
                lblArr(4).Caption = tmpVal
                lblArr(4).Tag = FldVal
            Case "ONBL"
                tmpVal = Mid(FldVal, 9, 4)
                If tmpVal <> "" Then tmpVal = Left(tmpVal, 2) & ":" & Right(tmpVal, 2)
                lblArr(5).Caption = tmpVal
                lblArr(5).Tag = FldVal
            Case "ACT5"
                lblArr(6).Caption = FldVal
                If FldVal <> "" Then lblArr(6).ZOrder
            Case "ACT3"
                lblArr(7).Caption = FldVal
                If FldVal <> "" Then lblArr(7).ZOrder
            Case "REGN"
                lblArr(8).Caption = FldVal
                If FldVal <> "" Then lblArr(8).ZOrder
            Case "REM1"
                lblArr(9).Caption = FldVal
            Case "BAA1"
                lblArr(10).Caption = FldVal
                lblArr(10).Tag = FldVal
            Case "BAA2"
                lblArr(11).Caption = FldVal
                lblArr(11).Tag = FldVal
            Case "BAA3"
                lblArr(12).Caption = FldVal
                lblArr(12).Tag = FldVal
            Case Else
        End Select
        ItemNo = ItemNo + 1
        FldNam = GetRealItem(FieldList, ItemNo, ",")
    Wend
End Sub
Public Sub ShowDepRecord(DepData As String, FieldList As String)
    Dim FldNam As String
    Dim FldVal As String
    Dim tmpVal As String
    Dim ItemNo As Long
    ItemNo = 0
    FldNam = GetRealItem(FieldList, ItemNo, ",")
    While FldNam <> ""
        FldVal = GetRealItem(DepData, ItemNo, ",")
        Select Case FldNam
            Case "FLNO"
                tmpVal = Trim(Left(FldVal, 3))
                lblDep(0).Caption = tmpVal
                lblDep(0).Tag = FldVal
                tmpVal = Trim(Mid(FldVal, 4))
                lblDep(1).Caption = tmpVal
            Case "STOD"
                tmpVal = Left(FldVal, 8)
                tmpVal = MyDateFormat(tmpVal, False)
                lblDep(2).Caption = tmpVal
                lblDep(2).Tag = FldVal
                tmpVal = Mid(FldVal, 9, 4)
                If tmpVal <> "" Then tmpVal = Left(tmpVal, 2) & ":" & Right(tmpVal, 2)
                lblDep(3).Caption = tmpVal
            Case "ETDI"
                tmpVal = Mid(FldVal, 9, 4)
                If tmpVal <> "" Then tmpVal = Left(tmpVal, 2) & ":" & Right(tmpVal, 2)
                lblDep(4).Caption = tmpVal
                lblDep(4).Tag = FldVal
            Case "OFBL"
                tmpVal = Mid(FldVal, 9, 4)
                If tmpVal <> "" Then tmpVal = Left(tmpVal, 2) & ":" & Right(tmpVal, 2)
                lblDep(5).Caption = tmpVal
                lblDep(5).Tag = FldVal
            Case "ACT5"
                lblDep(6).Caption = FldVal
                If FldVal <> "" Then lblDep(6).ZOrder
            Case "ACT3"
                lblDep(7).Caption = FldVal
                If FldVal <> "" Then lblDep(7).ZOrder
            Case "REGN"
                lblDep(8).Caption = FldVal
                If FldVal <> "" Then lblDep(8).ZOrder
            Case "REM1"
                lblDep(9).Caption = FldVal
            Case "BAA1"
                lblDep(10).Caption = FldVal
                lblDep(10).Tag = FldVal
            Case "BAA2"
                lblDep(11).Caption = FldVal
                lblDep(11).Tag = FldVal
            Case "BAA3"
                lblDep(12).Caption = FldVal
                lblDep(12).Tag = FldVal
            Case Else
        End Select
        ItemNo = ItemNo + 1
        FldNam = GetRealItem(FieldList, ItemNo, ",")
    Wend
End Sub

Private Sub CreateDelayPanel(Index As Integer, CfgList As String)
    Dim ItemNo As Long
    Dim GrpNo As Long
    Dim i As Integer
    Dim CaptionList As String
    Dim tmpCapt As String
    Dim NewTop As Long
    Dim NewLeft As Long
    
    For i = 0 To DlyEditPanel.UBound
        DlyEditPanel(i).Visible = False
    Next
    NewTop = Val(GetRealItem(CfgList, 3, "|")) * 15
    If NewTop < 30 Then NewTop = DlyEditPanel(0).Top
    NewLeft = DlyEditPanel(0).Left
    
    i = 0
    GrpNo = 0
    CaptionList = GetRealItem(CfgList, GrpNo, "|")
    While GrpNo < 3
        ItemNo = 0
        tmpCapt = GetRealItem(CaptionList, ItemNo, ",")
        While tmpCapt <> ""
            i = i + 1
            If i > DlyEditPanel.UBound Then
                Load DlyEditPanel(i)
                Load chkDlyTick(i)
                Load lblDlyReason(i)
                Load txtDlyInput(i)
                Load chkDlyCheck(i)
                Set DlyEditPanel(i).Container = DelayPanel
                Set chkDlyTick(i).Container = DlyEditPanel(i)
                Set lblDlyReason(i).Container = DlyEditPanel(i)
                Set txtDlyInput(i).Container = DlyEditPanel(i)
                Set chkDlyCheck(i).Container = DlyEditPanel(i)
            End If
            lblDlyReason(i).Caption = tmpCapt
            lblDlyReason(i).Tag = Right("00" & CStr(i), 2)
            txtDlyInput(i).Text = ":"
            txtDlyInput(i).BackColor = NormalGray
            chkDlyTick(i).Value = 0
            chkDlyTick(i).Visible = True
            lblDlyReason(i).Visible = True
            txtDlyInput(i).Visible = True
            'chkDlyCheck(i).Visible = True
            DlyEditPanel(i).Top = NewTop
            DlyEditPanel(i).Left = NewLeft
            DlyEditPanel(i).Tag = CStr(GrpNo)
            
            DlyEditPanel(i).Visible = True
            If MainLifeStyle Then DrawBackGround DlyEditPanel(i), WorkAreaColor, False, True
            NewTop = NewTop + DlyEditPanel(i).Height
            ItemNo = ItemNo + 1
            tmpCapt = GetRealItem(CaptionList, ItemNo, ",")
        Wend
        NewTop = NewTop + 60
        GrpNo = GrpNo + 1
        CaptionList = GetRealItem(CfgList, GrpNo, "|")
    Wend
    DelayPanel.Tag = CStr(Index)
    DelayPanel.Visible = True
    Form_Resize
End Sub

Private Sub InitDelayPanel(Index As Integer, UrnoLine As Long, UseValue As String)
    Dim CurLine As Long
    Dim BgnLine As Long
    Dim MaxLine As Long
    Dim tmpArea As String
    Dim tmpData As String
    Dim PanelNo As Integer
    If UrnoLine < 0 Then
        For PanelNo = 0 To DlyEditPanel.UBound
            chkDlyTick(PanelNo).Value = 0
            txtDlyInput(PanelNo).Text = ":"
            txtDlyInput(PanelNo).Tag = ":"
            txtDlyInput(PanelNo).Locked = True
            txtDlyInput(PanelNo).BackColor = NormalGray
            If ReadOnlyUser Then chkDlyTick(PanelNo).Enabled = False Else chkDlyTick(PanelNo).Enabled = True
        Next
        BgnLine = 0
        MaxLine = FileData(Index).GetLineCount - 1
    Else
        BgnLine = UrnoLine
        MaxLine = UrnoLine
    End If
    For CurLine = BgnLine To MaxLine
        tmpArea = FileData(Index).GetFieldValue(CurLine, "AREA")
        If tmpArea = "B" Then
            tmpData = FileData(Index).GetFieldValue(CurLine, "INDX")
            If tmpData <> "" Then
                PanelNo = Val(tmpData)
                If (PanelNo >= 0) And (PanelNo <= DlyEditPanel.UBound) Then
                    tmpData = FileData(Index).GetFieldValue(CurLine, "TIME")
                    If UseValue <> "" Then tmpData = UseValue
                    txtDlyInput(PanelNo).Tag = tmpData
                    If UrnoLine >= 0 Then chkDlyTick(PanelNo).Value = 0
                    txtDlyInput(PanelNo).Text = tmpData
                End If
            End If
        End If
    Next
End Sub

Private Sub MsExcel_SheetChange(ByVal Sh As Object, ByVal Target As Excel.Range)
    Static AlreadyShown As Long
    Dim tmpMsg As String
    Dim tmpLineData As String
    Dim tmpData As String
    Dim i As Integer
    Dim j As Integer
    Dim CurRow As Integer
    Dim MaxRow As Integer
    Dim CurCol As Integer
    Dim MaxCol As Integer
    Dim MaxAreas As Integer
    Dim ColCount As Integer
    If Not ExcelJustOpened Then
        If (Not ExcelUpdateAllowed) Or (ReadOnlyUser) Then
            'Permission denied
            If Sh.Name = MsWkSheet.Name Then
                If (AlreadyShown Mod 5) = 0 Then
                    MsExcel.WindowState = xlMinimized
                    Me.SetFocus
                    tmpMsg = "Sorry. You shouldn't change anything."
                    If ReadOnlyUser Then tmpMsg = tmpMsg & vbNewLine & "(You are a 'Read Only' User)"
                    If AlreadyShown >= 10 Then tmpMsg = tmpMsg & vbNewLine & "Excel will be losed now."
                    If MyMsgBox.CallAskUser(0, 0, 0, "Updating the Worksheet ?", tmpMsg, "stopit", "", UserAnswer) = 1 Then DoNothing
                    SetFormOnTop Me, False
                    MsExcel.WindowState = xlNormal
                End If
                ExcelJustOpened = True
                CurRow = Target.Row
                MaxRow = CurRow + Target.Rows.Count - 1
                CurCol = Target.Column
                MaxCol = CurCol + Target.Columns.Count - 1
                For i = CurRow To MaxRow
                    tmpLineData = FileData(LastFocusIndex).GetFieldValues(CLng(i - 1), ExcelFields)
                    For j = CurCol To MaxCol
                        tmpData = GetItem(tmpLineData, j, ",")
                        tmpData = CleanString(tmpData, SERVER_TO_CLIENT, False)
                        MsWkSheet.Cells(i, j) = tmpData
                    Next
                Next
                MsWkBook.Saved = True
                ExcelJustOpened = False
                If AlreadyShown >= 10 Then
                    PushWorkButton "EXCEL", 0
                    AlreadyShown = -1
                End If
                AlreadyShown = AlreadyShown + 1
            End If
        End If
    End If
End Sub

Private Sub MsExcel_WorkbookBeforeClose(ByVal Wb As Excel.Workbook, Cancel As Boolean)
    If (Not ExcelShutDown) And (Not ExcelJustOpened) Then
        MainTimer(1).Tag = "STOP_EXCEL"
        MainTimer(1).Interval = 500
        MainTimer(1).Enabled = True
    End If
End Sub

Private Sub MsExcel_WorkbookBeforeSave(ByVal Wb As Excel.Workbook, ByVal SaveAsUI As Boolean, Cancel As Boolean)
    Dim tmpMsg As String
    If (Not ExcelShutDown) And (Not ExcelJustOpened) Then
        'Permission denied
        If (ReadOnlyUser) And (Not ExcelRoSaveAllowed) Then
            MsExcel.WindowState = xlMinimized
            Me.SetFocus
            tmpMsg = "Sorry. Access denied."
            tmpMsg = tmpMsg & vbNewLine & "(You are a 'Read Only' User)"
            If MyMsgBox.CallAskUser(0, 0, 0, "Saving the Worksheet ?", tmpMsg, "stop2", "", UserAnswer) = 1 Then DoNothing
            Cancel = True
            SetFormOnTop Me, False
            MsExcel.WindowState = xlNormal
        End If
    End If
End Sub

Private Sub txtDlyInput_Change(Index As Integer)
    Dim tmpTxt As String
    Dim CheckIt As Boolean
    If chkDlyTick(Index).Value = 1 Then
        tmpTxt = Trim(txtDlyInput(Index).Text)
        CheckIt = CheckTimeValues(tmpTxt, False, False)
        If CheckIt Then
            CheckNewDlysEntry Index, tmpTxt
        Else
            If Len(tmpTxt) >= 4 Then
                txtDlyInput(Index).BackColor = vbRed
                txtDlyInput(Index).ForeColor = vbWhite
            Else
                txtDlyInput(Index).BackColor = vbWhite
                txtDlyInput(Index).ForeColor = vbBlack
            End If
        End If
    End If
End Sub
Private Sub txtDlyInput_KeyPress(Index As Integer, KeyAscii As Integer)
    If Not ((KeyAscii >= 48 And KeyAscii <= 57) Or (KeyAscii = 8) Or (Chr(KeyAscii) = ":")) Then
        KeyAscii = 0
    End If
End Sub
Private Sub CheckNewDlysEntry(Index As Integer, DlytValue As String)
    Dim HitList As String
    Dim tmpFields As String
    Dim NewLine As String
    Dim NewData As String
    Dim tmpDlyr As String
    Dim tmpCapt As String
    Dim tmpMsg As String
    Dim TabIdx As Integer
    Dim MaxLine As Long
    Dim LineStatus As Long
    Dim ColNo As Long
    Dim FlnuValue As Long
    Dim MsgText As String
    FlnuValue = Val(txtAftData(3).Text)
    If FlnuValue > 0 Then
        tmpDlyr = Right("00" & CStr(Index), 2)
        TabIdx = Val(DelayPanel.Tag)
        ColNo = Val(TranslateFieldItems("INDX", FileData(TabIdx).LogicalFieldList))
        HitList = FileData(TabIdx).GetLinesByColumnValue(ColNo, tmpDlyr, 0)
        If HitList = "" Then
            If Len(DlytValue) = 5 Then
                'INSERT
                tmpFields = "AREA,TIME,INDX,CONT,USEC,USEU,CDAT,LSTU,FLNU"
                NewData = "B," & DlytValue & "," & tmpDlyr
                NewData = NewData & "," & Left(lblDlyReason(Index).Caption, 10)
                NewData = NewData & "," & LoginUserName
                NewData = NewData & "," & LoginUserName
                NewData = NewData & "," & GetTimeStamp(-480)
                NewData = NewData & "," & GetTimeStamp(-480)
                NewData = NewData & "," & txtAftData(3).Text
                NewLine = CreateEmptyLine(FileData(TabIdx).LogicalFieldList)
                FileData(TabIdx).InsertTextLineAt 0, NewLine, False
                MaxLine = 0
                FileData(TabIdx).SetFieldValues MaxLine, tmpFields, NewData
                FileData(TabIdx).SetLineStatusValue MaxLine, 2
                FileData(TabIdx).SetLineColor MaxLine, vbBlack, LightGreen
                HighlightWorkButton "SAVEAODB", vbWhite, vbRed
                If AutoSaveAodb Then HandleSaveAodb True
            End If
        Else
            'UPDATE (or DELETE)
            MaxLine = Val(HitList)
            LineStatus = FileData(TabIdx).GetLineStatusValue(MaxLine)
            If Len(DlytValue) > 0 Then
                If Len(DlytValue) = 5 Then
                    tmpFields = "TIME,CONT,USEU,LSTU"
                    NewData = DlytValue
                    NewData = NewData & "," & Left(lblDlyReason(Index).Caption, 10)
                    NewData = NewData & "," & LoginUserName
                    NewData = NewData & "," & GetTimeStamp(-480)
                    FileData(TabIdx).SetFieldValues MaxLine, tmpFields, NewData
                    If LineStatus <> 2 Then
                        FileData(TabIdx).SetLineStatusValue MaxLine, 1
                        FileData(TabIdx).SetLineColor MaxLine, vbBlack, LightYellow
                        HighlightWorkButton "SAVEAODB", vbWhite, vbRed
                        If AutoSaveAodb Then HandleSaveAodb True
                    End If
                End If
            Else
                If LineStatus <> 3 Then
                    tmpCapt = HiddenData.GetConfigValues(FileData(TabIdx).myName, "CAPT")
                    tmpCapt = GetRealItem(tmpCapt, 0, "|")
                    tmpMsg = "Do you want to delete this entry?"
                    If MyMsgBox.CallAskUser(0, 0, 0, tmpCapt, tmpMsg, "stop2", "Yes,No", UserAnswer) = 1 Then
                        If LineStatus <> 2 Then
                            FileData(TabIdx).SetLineStatusValue MaxLine, 3
                            FileData(TabIdx).SetLineColor MaxLine, vbBlack, LightRed
                            HighlightWorkButton "SAVEAODB", vbWhite, vbRed
                            If AutoSaveAodb Then HandleSaveAodb True
                        Else
                            FileData(TabIdx).DeleteLine MaxLine
                        End If
                    End If
                End If
            End If
        End If
        FileData(TabIdx).AutoSizeColumns
        FileData(TabIdx).Refresh
    Else
        MsgText = "Sorry, the Departure Flight doesn't exist."
        If MyMsgBox.CallAskUser(0, 0, 0, "Selected Flight", MsgText, "stop2", "", UserAnswer) = 1 Then DoNothing
    End If
End Sub

Private Sub txtDlyInput_GotFocus(Index As Integer)
    If chkDlyTick(Index).Value = 1 Then SetTextSelected
End Sub

Public Sub EvaluateBc(BcCmd As String, BcTbl As String, BcSel As String, BcFld As String, BcDat As String)
    Dim tmpUrno As String
    Dim tmpFlnu As String
    tmpUrno = GetFieldValue("URNO", BcDat, BcFld)
    If tmpUrno = "" Then tmpUrno = GetUrnoFromSqlKey(BcSel)
    If tmpUrno <> "" Then
        If InStr("AFTTAB", BcTbl) > 0 Then
            If tmpUrno = txtAftData(1).Text Then
                ShowArrRecord BcDat, BcFld
            End If
            If tmpUrno = txtAftData(3).Text Then
                ShowDepRecord BcDat, BcFld
            End If
        End If
        If InStr("DLYTAB,SRLTAB,FMLTAB", BcTbl) > 0 Then
            tmpFlnu = GetFieldValue("FLNU", BcDat, BcFld)
            Select Case BcCmd
                Case "IRT"
                    If (tmpFlnu = txtAftData(1).Text) Or (tmpFlnu = txtAftData(3).Text) Then
                        HandleBcInsert -1, BcTbl, BcSel, BcFld, BcDat
                    End If
                Case "URT"
                    If (tmpFlnu = txtAftData(1).Text) Or (tmpFlnu = txtAftData(3).Text) Then
                        HandleBcUpdate -1, BcTbl, BcSel, BcFld, BcDat
                    End If
                Case "DRT"
                    HandleBcDelete -1, BcTbl, BcSel, BcFld, BcDat
                Case "REFR"
                    HandleBcRefresh -1, BcTbl, BcSel, BcFld, BcDat
                Case Else
            End Select
            If EditMaskIsOpen Then
                If tmpUrno = EditMask.Tag Then
                    EditMask.UpdateFieldValues BcFld, BcDat
                End If
            End If
            If ReadMaskIsOpen Then
                If tmpUrno = ReadMask.Tag Then
                    ReadMaskTab.SetCurrentSelection ReadMaskTab.GetCurrentSelected
                End If
            End If
        End If
    End If
End Sub

Private Sub HandleBcRefresh(TabIdx As Integer, BcTbl As String, BcSel As String, BcFld As String, BcDat As String)
    Dim i As Integer
    Dim i1 As Integer
    Dim i2 As Integer
    Dim CurFocusTab As Integer
    Dim tmpTbl As String
    Dim tmpArrFlnu As String
    Dim tmpDepFlnu As String
    Dim tmpArea As String
    Dim tmpMyCall As String
    Dim tmpBcCall As String
    Dim tmpMyMask As String
    Dim tmpBcMask As String
    Dim tmpMyWks As String
    Dim tmpBcWks As String
    Dim tmpMyPid As String
    Dim tmpBcPid As String
    Dim LineNo As Long
    Dim DoIt As Boolean
    tmpArrFlnu = GetRealItem(BcDat, 0, ",")
    tmpDepFlnu = GetRealItem(BcDat, 1, ",")
    If (tmpArrFlnu = txtAftData(1).Text) Or (tmpDepFlnu = txtAftData(3).Text) Then
        If TabIdx < 0 Then
            i1 = 0
            i2 = chkTabIsVisible.UBound
        Else
            i1 = TabIdx
            i2 = TabIdx
        End If
        For i = i1 To i2
            If chkTabIsVisible(i).Visible Then
                tmpTbl = HiddenData.GetConfigValues(FileData(i).myName, "DTAB")
                If tmpTbl = BcTbl Then
                    DoIt = True
                    tmpMyWks = UfisServer.CdrhdlSock.LocalHostName
                    tmpBcWks = GetRealItem(BcSel, 0, ",")
                    tmpMyPid = CStr(App.ThreadID)
                    tmpBcPid = GetRealItem(BcSel, 1, ",")
                    tmpMyMask = HiddenData.GetConfigValues(FileData(i).myName, "MASK")
                    tmpMyMask = GetRealItem(tmpMyMask, 1, ",")
                    tmpBcMask = GetRealItem(BcSel, 2, ",")
                    tmpMyCall = HiddenData.GetConfigValues(FileData(i).myName, "CALL")
                    tmpBcCall = GetRealItem(BcSel, 3, ",")
                    If (tmpMyWks = tmpBcWks) And (tmpMyPid = tmpBcPid) Then DoIt = False
                    If tmpMyCall <> tmpBcCall Then DoIt = False
                    If tmpMyMask <> tmpBcMask Then DoIt = False
                    If DoIt Then
                        RefreshOnBc = True
                        CurFocusTab = LastFocusIndex
                        LastFocusIndex = i
                        PushWorkButton "READAODB", 1
                        LastFocusIndex = CurFocusTab
                        RefreshOnBc = False
                    End If
                End If
            End If
        Next
    End If
End Sub

Private Sub HandleBcInsert(TabIdx As Integer, BcTbl As String, BcSel As String, BcFld As String, BcDat As String)
    Dim i As Integer
    Dim i1 As Integer
    Dim i2 As Integer
    Dim tmpTbl As String
    Dim tmpUrno As String
    Dim tmpFlnu As String
    Dim tmpArea As String
    Dim tmpCall As String
    Dim tmpData As String
    Dim UrnoLine As Long
    Dim LineNo As Long
    Dim DoIt As Boolean
    If TabIdx < 0 Then
        i1 = 0
        i2 = chkTabIsVisible.UBound
    Else
        i1 = TabIdx
        i2 = TabIdx
    End If
    tmpUrno = GetFieldValue("URNO", BcDat, BcFld)
    For i = i1 To i2
        If chkTabIsVisible(i).Visible Then
            tmpTbl = HiddenData.GetConfigValues(FileData(i).myName, "DTAB")
            If tmpTbl = BcTbl Then
                UrnoLine = GetUrnoLine(FileData(i), tmpUrno)
                DoIt = True
                Select Case BcTbl
                    Case "DLYTAB"
                        tmpArea = GetFieldValue("AREA", BcDat, BcFld)
                        If i = Val(DelayPanel.Tag) Then
                            If tmpArea <> "B" Then DoIt = False
                        Else
                            If tmpArea <> "A" Then DoIt = False
                        End If
                    Case "FMLTAB"
                        tmpCall = HiddenData.GetConfigValues(FileData(i).myName, "CALL")
                        tmpFlnu = GetFieldValue("FLNU", BcDat, BcFld)
                        If tmpCall = "ARR" And tmpFlnu <> txtAftData(1).Text Then DoIt = False
                        If tmpCall = "DEP" And tmpFlnu <> txtAftData(3).Text Then DoIt = False
                    Case Else
                End Select
                If DoIt Then
                    If UrnoLine < 0 Then
                        LineNo = FileData(i).GetLineCount
                        FileData(i).InsertTextLine CreateEmptyLine(FileData(i).LogicalFieldList), False
                        FileData(i).SetFieldValues LineNo, BcFld, BcDat
                        tmpData = FileData(i).GetLineValues(LineNo)
                        FileData(i).SetLineTag LineNo, tmpData
                        FileData(i).AutoSizeColumns
                        FileData(i).Refresh
                        If (i = Val(DelayPanel.Tag)) And (tmpArea = "B") Then
                            If DelayPanel.Visible Then InitDelayPanel i, LineNo, ""
                        End If
                    Else
                        HandleBcUpdate i, BcTbl, BcSel, BcFld, BcDat
                    End If
                End If
            End If
        End If
    Next
End Sub
Private Sub HandleBcUpdate(TabIdx As Integer, BcTbl As String, BcSel As String, BcFld As String, BcDat As String)
    Dim i As Integer
    Dim i1 As Integer
    Dim i2 As Integer
    Dim tmpTbl As String
    Dim tmpUrno As String
    Dim tmpFlnu As String
    Dim tmpArea As String
    Dim tmpCall As String
    Dim tmpIndx As String
    Dim tmpData As String
    Dim UrnoLine As Long
    Dim DoIt As Boolean
    If TabIdx < 0 Then
        i1 = 0
        i2 = chkTabIsVisible.UBound
    Else
        i1 = TabIdx
        i2 = TabIdx
    End If
    tmpUrno = GetFieldValue("URNO", BcDat, BcFld)
    For i = i1 To i2
        If chkTabIsVisible(i).Visible Then
            tmpTbl = HiddenData.GetConfigValues(FileData(i).myName, "DTAB")
            If tmpTbl = BcTbl Then
                UrnoLine = GetUrnoLine(FileData(i), tmpUrno)
                DoIt = True
                Select Case BcTbl
                    Case "DLYTAB"
                        tmpArea = GetFieldValue("AREA", BcDat, BcFld)
                        If i = Val(DelayPanel.Tag) Then
                            If tmpArea <> "B" Then DoIt = False
                        Else
                            If tmpArea <> "A" Then DoIt = False
                        End If
                    'Case "FMLTAB"
                    '    tmpCall = HiddenData.GetConfigValues(FileData(i).myName, "CALL")
                    '    tmpFlnu = GetFieldValue("FLNU", BcDat, BcFld)
                    '    If tmpCall = "ARR" And tmpFlnu <> txtAftData(1).Text Then DoIt = False
                    '    If tmpCall = "DEP" And tmpFlnu <> txtAftData(3).Text Then DoIt = False
                    Case Else
                End Select
                If DoIt Then
                    If UrnoLine >= 0 Then
                        If (i = Val(DelayPanel.Tag)) And (tmpArea = "B") Then
                            If DelayPanel.Visible Then InitDelayPanel i, UrnoLine, ":"
                        End If
                        FileData(i).SetFieldValues UrnoLine, BcFld, BcDat
                        tmpData = FileData(i).GetLineValues(UrnoLine)
                        FileData(i).SetLineTag UrnoLine, tmpData
                        If (i = Val(DelayPanel.Tag)) And (tmpArea = "B") Then
                            If DelayPanel.Visible Then
                                InitDelayPanel i, UrnoLine, ""
                                tmpIndx = FileData(i).GetFieldValue(UrnoLine, "INDX")
                                CheckDlyIndex i, UrnoLine, tmpIndx, True
                            End If
                        End If
                        FileData(i).AutoSizeColumns
                        FileData(i).Refresh
                    End If
                End If
            End If
        End If
    Next
End Sub
Private Sub HandleBcDelete(TabIdx As Integer, BcTbl As String, BcSel As String, BcFld As String, BcDat As String)
    Dim i As Integer
    Dim i1 As Integer
    Dim i2 As Integer
    Dim tmpTbl As String
    Dim tmpUrno As String
    Dim tmpArea As String
    Dim UrnoLine As Long
    If TabIdx < 0 Then
        i1 = 0
        i2 = chkTabIsVisible.UBound
    Else
        i1 = TabIdx
        i2 = TabIdx
    End If
    tmpUrno = GetFieldValue("URNO", BcDat, BcFld)
    If tmpUrno = "" Then tmpUrno = GetUrnoFromSqlKey(BcSel)
    If tmpUrno <> "" Then
        For i = i1 To i2
            If chkTabIsVisible(i).Visible Then
                tmpTbl = HiddenData.GetConfigValues(FileData(i).myName, "DTAB")
                If tmpTbl = BcTbl Then
                    UrnoLine = GetUrnoLine(FileData(i), tmpUrno)
                    If UrnoLine >= 0 Then
                        If i = Val(DelayPanel.Tag) Then
                            tmpArea = GetFieldValue("AREA", BcDat, BcFld)
                            If (DelayPanel.Visible) And (tmpArea = "B") Then InitDelayPanel i, UrnoLine, ":"
                        End If
                        FileData(i).DeleteLine UrnoLine
                        FileData(i).AutoSizeColumns
                        FileData(i).Refresh
                    End If
                End If
            End If
        Next
    End If
End Sub
Private Function GetUrnoLine(CurTab As TABLib.Tab, CurUrno As String) As Long
    Dim UrnoLine As Long
    Dim UrnoCol As Long
    Dim tmpStrg As String
    Dim HitList As String
    UrnoLine = -1
    tmpStrg = TranslateFieldItems("URNO", CurTab.LogicalFieldList)
    If tmpStrg <> "" Then
        UrnoCol = Val(tmpStrg)
        HitList = CurTab.GetLinesByColumnValue(UrnoCol, CurUrno, 0)
        If HitList <> "" Then
            UrnoLine = Val(HitList)
        End If
    End If
    GetUrnoLine = UrnoLine
End Function
Private Function GetFirstEditColNo(Index As Integer) As Long
    Dim ColNo As Long
    Dim CurCol As Long
    Dim CurItem As Long
    Dim FirstCol As Long
    Dim tmpList As String
    Dim tmpColNbr As String
    FirstCol = -1
    tmpList = FileData(Index).NoFocusColumns
    If tmpList <> "" Then
        CurCol = -1
        CurItem = 0
        tmpColNbr = GetRealItem(tmpList, CurItem, ",")
        While ((FirstCol < 0) And (tmpColNbr <> ""))
            CurCol = CurCol + 1
            ColNo = Val(tmpColNbr)
            If ColNo > CurCol Then FirstCol = CurCol
            CurItem = CurItem + 1
            tmpColNbr = GetRealItem(tmpList, CurItem, ",")
        Wend
    End If
    GetFirstEditColNo = FirstCol
End Function
Private Sub CheckReadOnlyMode()
    Dim i As Integer
    Dim SetEnable As Boolean
    Dim tmpTag As String
    If ReadOnlyUser = True Then
        For i = 0 To chkWork.UBound
            If chkWork(i).Visible Then
                tmpTag = chkWork(i).Tag
                tmpTag = GetRealItem(tmpTag, 0, ",")
                SetEnable = False
                Select Case tmpTag
                    Case "TAB_READ"
                    Case "TAB_UPDATE", "TAB_EDIT_UPDATE"
                    Case "TAB_INSERT", "TAB_EDIT_INSERT"
                    Case "TAB_DELETE"
                    Case "SAVEAODB", "AUTO_SAVEAODB"
                    Case Else
                        SetEnable = True
                End Select
                chkWork(i).Enabled = SetEnable
            End If
        Next
    End If
End Sub
Private Sub CheckSynchroTabs(Index As Integer, SelLineNo As Long)
    Dim tmpFldNam As String
    Dim tmpFldVal As String
    Dim tmpFldRel As String
    tmpFldNam = "URNO"
    tmpFldRel = "URNO"
    tmpFldVal = FileData(Index).GetFieldValue(SelLineNo, tmpFldNam)
    ShowSynchroCursor 2, tmpFldRel, tmpFldVal
    ShowSynchroCursor 3, tmpFldRel, tmpFldVal
    ShowSynchroCursor 4, tmpFldRel, tmpFldVal
    ShowSynchroCursor 5, tmpFldRel, tmpFldVal
    ShowSynchroCursor 6, "FLNU", tmpFldVal
    
End Sub
Private Sub ShowSynchroCursor(Index As Integer, FldName As String, FldValue As String)
    Dim HitList As String
    Dim FldItem As String
    Dim ColNo As Long
    Dim FirstLine As Long
'    HitList = FileData(Index).Tag
'    If HitList <> "" Then
'        FirstLine = SetLineMarkers(FileData(Index), HitList, 0, 1, False)
'    End If
'    FldItem = TranslateFieldItems(FldName, FileData(Index).LogicalFieldList)
'    ColNo = Val(FldItem)
'    HitList = FileData(Index).GetLinesByColumnValue(ColNo, FldValue, 0)
'    If HitList <> "" Then
'        FileData(Index).Tag = HitList
'        FirstLine = SetLineMarkers(FileData(Index), HitList, 0, 1, True)
'        FileData(Index).OnVScrollTo FirstLine - 2
'    End If
End Sub
