VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.ocx"
Begin VB.Form ReadMask 
   Caption         =   "Text Reader"
   ClientHeight    =   3270
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   6210
   Icon            =   "ReadMask.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   3270
   ScaleWidth      =   6210
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin VB.PictureBox RightPanel 
      Align           =   4  'Align Right
      AutoRedraw      =   -1  'True
      Height          =   2985
      Left            =   5265
      ScaleHeight     =   2925
      ScaleWidth      =   885
      TabIndex        =   2
      Top             =   0
      Width           =   945
      Begin VB.CheckBox chkTool 
         Caption         =   "Read"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   3
         Left            =   30
         Style           =   1  'Graphical
         TabIndex        =   6
         Top             =   30
         Width           =   825
      End
      Begin VB.CheckBox chkTool 
         Caption         =   "Close"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   2
         Left            =   30
         Style           =   1  'Graphical
         TabIndex        =   5
         Top             =   1020
         Width           =   825
      End
      Begin VB.CheckBox chkTool 
         Caption         =   "Save"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   1
         Left            =   30
         Style           =   1  'Graphical
         TabIndex        =   4
         Top             =   690
         Width           =   825
      End
      Begin VB.CheckBox chkTool 
         Caption         =   "Update"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   0
         Left            =   30
         Style           =   1  'Graphical
         TabIndex        =   3
         Top             =   360
         Width           =   825
      End
   End
   Begin MSComctlLib.StatusBar StatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   1
      Top             =   2985
      Width           =   6210
      _ExtentX        =   10954
      _ExtentY        =   503
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   10451
         EndProperty
      EndProperty
   End
   Begin VB.TextBox Text1 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9.75
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1515
      Left            =   0
      Locked          =   -1  'True
      MultiLine       =   -1  'True
      ScrollBars      =   3  'Both
      TabIndex        =   0
      Top             =   0
      Width           =   3225
   End
End
Attribute VB_Name = "ReadMask"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim InitDisplay As Boolean
Dim MyTabOcx As TABLib.Tab
Dim MyTabLine As Long
Public Sub DisplayText(UseTab As TABLib.Tab, ShowText As String, ShowCaption As String, CurUrno As String, IsRead As Integer)
    InitDisplay = True
    Set MyTabOcx = UseTab
    Me.Caption = "Text Reader (" & ShowCaption & ")"
    Me.Tag = CurUrno
    Text1.Text = CleanString(ShowText, FOR_CLIENT, False)
    Text1.Tag = Text1.Text
    chkTool(3).Enabled = False
    If IsRead < 0 Then
        chkTool(3).Value = 0
        chkTool(3).Enabled = False
    Else
        chkTool(3).Value = IsRead
        chkTool(3).Enabled = True
    End If
    InitDisplay = False
End Sub

Private Sub chkTool_Click(Index As Integer)
    Dim LineNo As Long
    Dim LineStatus As Long
    Dim tmpText As String
    If chkTool(Index).Value = 1 Then
        chkTool(Index).BackColor = LightGreen
        Select Case Index
            Case 0  'Update
                chkTool(1).Enabled = True
                Text1.Locked = False
                Text1.SetFocus
                chkTool(3).Enabled = False
                chkTool(3).Value = 0
            Case 1  'Save
                LineNo = MyTabOcx.GetCurrentSelected
                LineStatus = MyTabOcx.GetLineStatusValue(LineNo)
                tmpText = Text1.Text
                Text1.Tag = Text1.Text
                tmpText = CleanString(tmpText, FOR_SERVER, False)
                tmpText = tmpText & ",X"
                tmpText = tmpText & "," & LoginUserName
                tmpText = tmpText & "," & GetTimeStamp(-480)
                MyTabOcx.SetFieldValues LineNo, "TEXT,READ,USEU,LSTU", tmpText
                If LineStatus <> 2 Then
                    MyTabOcx.SetLineStatusValue LineNo, 1
                    MyTabOcx.SetLineColor LineNo, vbBlack, LightYellow
                End If
                MyTabOcx.AutoSizeColumns
                MyTabOcx.Refresh
                MainDialog.HighlightWorkButton "SAVEAODB", vbWhite, vbRed
                chkTool(Index).Value = 0
                chkTool(0).Value = 0
            Case 2  'Close
                ReadMaskButton.Value = 0
            Case 3  'Read
                If chkTool(Index).Enabled Then
                    LineNo = MyTabOcx.GetCurrentSelected
                    LineStatus = MyTabOcx.GetLineStatusValue(LineNo)
                    tmpText = ""
                    tmpText = tmpText & "," & LoginUserName
                    tmpText = tmpText & "," & GetTimeStamp(-480)
                    MyTabOcx.SetFieldValues LineNo, "READ,USEU,LSTU", tmpText
                    If LineStatus <> 2 Then
                        MyTabOcx.SetLineStatusValue LineNo, 1
                        MyTabOcx.SetLineColor LineNo, vbBlack, LightYellow
                    End If
                    MyTabOcx.AutoSizeColumns
                    MyTabOcx.Refresh
                    MainDialog.HighlightWorkButton "SAVEAODB", vbWhite, vbRed
                    MainDialog.PushWorkButton "SAVEAODB", 1
                End If
            Case Else
        End Select
    Else
        chkTool(Index).BackColor = vbButtonFace
        Select Case Index
            Case 0
                chkTool(1).Enabled = False
                Text1.Locked = True
                Text1.Text = Text1.Tag
            Case 3  'Not Read
                If chkTool(Index).Enabled Then
                    LineNo = MyTabOcx.GetCurrentSelected
                    LineStatus = MyTabOcx.GetLineStatusValue(LineNo)
                    MyTabOcx.SetFieldValues LineNo, "READ", "X"
                    If LineStatus <> 2 Then
                        MyTabOcx.SetLineStatusValue LineNo, 1
                        MyTabOcx.SetLineColor LineNo, vbBlack, LightYellow
                    End If
                    MyTabOcx.AutoSizeColumns
                    MyTabOcx.Refresh
                    MainDialog.HighlightWorkButton "SAVEAODB", vbWhite, vbRed
                    MainDialog.PushWorkButton "SAVEAODB", 1
                End If
            Case Else
        End Select
    End If
End Sub

Private Sub Form_Load()
    ReadMaskIsOpen = True
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    If UnloadMode = 0 Then
        If ReadMaskButton.Value = 1 Then
            ReadMaskButton.Value = 0
            ReadMaskIsOpen = False
            Unload Me
        End If
    End If
End Sub

Private Sub Form_Resize()
    Dim NewSize As Long
    NewSize = Me.ScaleWidth - RightPanel.Width - (Text1.Left * 2)
    If NewSize > 300 Then Text1.Width = NewSize
    NewSize = Me.ScaleHeight - StatusBar1.Height - Text1.Top
    If NewSize > 300 Then Text1.Height = NewSize
    If RightPanel.ScaleHeight > 150 Then
        If MainLifeStyle Then DrawBackGround RightPanel, 7, True, True
    End If
End Sub

Private Sub Text1_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
End Sub
