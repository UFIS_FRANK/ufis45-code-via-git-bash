whenever sqlerror continue
host clear
host echo "----------------------------------------------------------------"
host echo "Building Table FMLTAB."
host echo "----------------------------------------------------------------"
Drop Table FMLTAB;
CREATE TABLE FMLTAB (
       URNO      NUMBER  (10) DEFAULT 0,
       FLNU      NUMBER  (10) DEFAULT 0,
       HOPO      CHAR    (3)  DEFAULT ' ',
       USEC      CHAR    (32) DEFAULT ' ',
       CDAT      CHAR    (14) DEFAULT ' ',
       USEU      CHAR    (32) DEFAULT ' ',
       LSTU      CHAR    (14) DEFAULT ' ',
       PRFL      CHAR    (1)  DEFAULT ' ',
       AREA      CHAR    (1)  DEFAULT ' ',
       TIME      CHAR    (14) DEFAULT ' ',
       TYPE      CHAR    (2)  DEFAULT ' ',
       TEXT      VARCHAR2    (1024) DEFAULT ' '
)
;
COMMIT;
host echo "Completed."
