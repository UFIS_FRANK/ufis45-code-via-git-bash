#ifndef __ADDITIONAL_REPORT_3_VIEWER_H__
#define __ADDITIONAL_REPORT_3_VIEWER_H__

#include "stdafx.h"
#include "RotationDlgCedaFlightData.h"
#include "CCSTable.h"
#include "CViewer.h"
#include "CCSPrint.h"


struct ADDITIONAL_REPORT3_LINEDATA
{
	long AUrno;
	long ARkey;
	CString		AAct;
	CString		AAlc2;
	CString		AAlc3;
	CString		ADate;
	CTime 		AEtai; 	
	CString		AFlno;
	CTime 		ALand; 	
	CTime 		AStoa;
	CString		ATtyp;
	CString		AVia3;
	CString 	AVian;


	long DUrno;
	long DRkey;
	CString		DAct;
	CString		DAlc2;
	CString		DAlc3;
	CTime 		DAirb;
	CString		DDate;
	CString		DDes3;
	CString		DDes4;
	CTime 		DEtdi;
	CString		DFlno;
	CTime		DStod;
	CString		DTtyp;
	CString		DVia3;
	CString 	DVian;

};

/////////////////////////////////////////////////////////////////////////////
// AdditionalReport1TableViewer

class AdditionalReport3TableViewer : public CViewer
{
// Constructions
public:
    AdditionalReport3TableViewer(CCSPtrArray<ROTATIONDLGFLIGHTDATA> *popData, 
		                                 const CStringArray &opShownColumns,
		                                 char *pcpInfo = NULL, char *pcpSelect = NULL);
    ~AdditionalReport3TableViewer();

    void Attach(CCSTable *popAttachWnd);
	void SetParentDlg(CDialog* ppDlg);
    virtual void ChangeViewTo(const char *pcpViewName);
	void PrintPlanToFile(char *pcpDefPath);

	int GetDepFlightCount();
// Internal data processing routines
private:
    void MakeLines();
	int  MakeLine(ROTATIONDLGFLIGHTDATA *prpAFlight, ROTATIONDLGFLIGHTDATA *prpDFlight);
	void MakeLineData(ROTATIONDLGFLIGHTDATA *prpAFlight, ROTATIONDLGFLIGHTDATA *prpDFlight, ADDITIONAL_REPORT3_LINEDATA &rpLine);
	void MakeColList(ADDITIONAL_REPORT3_LINEDATA *prlLine, 
		             CCSPtrArray<TABLE_COLUMN> &olColList, CString opCurrentAirline);
	int  CompareGefundenefluege(ADDITIONAL_REPORT3_LINEDATA *prpGefundenefluege1, ADDITIONAL_REPORT3_LINEDATA *prpGefundenefluege2);
	int  CompareFlight(ADDITIONAL_REPORT3_LINEDATA *prpFlight1, ADDITIONAL_REPORT3_LINEDATA *prpFlight2);
	int  GetVia(CCSPtrArray<VIADATA> *opVias, ROTATIONDLGFLIGHTDATA *prpFlight);

// Operations
public:
	void DeleteAll();
	int CreateLine(ADDITIONAL_REPORT3_LINEDATA &prpGefundenefluege);
	void DeleteLine(int ipLineno);
// Window refreshing routines
public:
	void UpdateDisplay();
	void DrawHeader();


// Attributes used for filtering condition
private:
	bool bmIsFromSearch;
	// Array der Header
	CStringArray omShownColumns;
	// Sapltengroese
	CUIntArray omAnzChar;

    CCSTable *pomTable;
	CCSPtrArray<ROTATIONDLGFLIGHTDATA> *pomData;
	// Liste der existierenden Airlines
	CStringArray omColumn1;
	// Auftreten der existierenden Airlines
	CUIntArray omCountColumn1;
	CUIntArray omCountColumn2;

	// Anzahl der Departures
	int imDepFlightCount;
	CDialog* pomParentDlg;
	

public:
    CCSPtrArray<ADDITIONAL_REPORT3_LINEDATA> omLines;
	CCSPtrArray<TABLE_HEADER_COLUMN> omHeaderDataArray;

	// anzuzeigende Header als Array uebergeben
	void SetColumnsToShow(const CStringArray &opShownColumns);

//Print 
	void GetHeader(void);
	CCSPtrArray <TABLE_HEADER_COLUMN> omPrintHeadHeaderArray;
	CCSPrint *pomPrint; 
	void PrintTableView(void);
	bool PrintTableLine(ADDITIONAL_REPORT3_LINEDATA *prpLine,bool bpLastLine, 
		                CString opCurrentAirline);
	bool PrintTableHeader(void);
	CString omTableName;
	char *pcmInfo;
	char *pcmSelect;
	CString omFooterName;

protected:
	// Mapping Tabellenfeldname/Spaltenueberschrift
	CString GetHeaderContent(CString opCurrentColumns);
	CString GetFieldContent(ADDITIONAL_REPORT3_LINEDATA* prlLine, 
		                    CString opCurrentColumns, CString opCurrentAirline);
	void SetFieldLength(CString opCurrentColumns);
	// Listen zum Zaehlen erstellen
	void MakeCountList(ROTATIONDLGFLIGHTDATA *prpAFlight);

};

#endif