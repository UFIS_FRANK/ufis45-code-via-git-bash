// BltDiagram.cpp : implementation file
// Modification History:
// 19-feb-01	rkr	PRF 1305 Dubai: Gantchart-Anzeige unter local unterstützt
//

#include "stdafx.h"
#include "WoResTableDlg.h"
#include "BltDiagram.h"
#include "BltDiaPropertySheet.h"
#include "DiaCedaFlightData.h"
#include "BltChart.h"
#include "BltGantt.h"
#include "TimePacket.h"
#include "RotationISFDlg.h"
#include "PrivList.h"
#include "BltOverviewTableDlg.h"
#include "BltKonflikteDlg.h"
#include "Konflikte.h"
#include "FlightSearchTableDlg.h"
#include "BltCcaTableDlg.h"
#include "PosDiagram.h"
#include "Utils.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

static void BltDiagramCf(void *popInstance, int ipDDXType,
    void *vpDataPointer, CString &ropInstanceName);

/////////////////////////////////////////////////////////////////////////////
// BltDiagram
extern int igAnsichtPageIndex;

IMPLEMENT_DYNCREATE(BltDiagram, CFrameWnd)

BltDiagram::BltDiagram()
{
	igAnsichtPageIndex = -1;
	omMaxTrackSize = CPoint(4000/*1024*/, 1500);
	omMinTrackSize = CPoint(1024 / 4, 768 / 4);
	omViewer.SetViewerKey("POSDIA");
	omViewer.SelectView("<Default>");
    imStartTimeScalePos = 104;
    imStartTimeScalePos++;              // plus one for left border of chart
    imFirstVisibleChart = -1;
	lmBkColor = lgBkColor;
	pogBltDiagram = this;
	bmIsViewOpen = false;
	bgKlebefunktion = false;
	bmRepaintAll = false;
	pomBltCcaTableDlg = NULL;
	pomBltKonflikteDlg = NULL;
	pomBltOverviewTableDlg = NULL;
	m_key = "DialogPosition\\BaggageBeltDiagram";
}

BltDiagram::~BltDiagram()
{
//    omPtrArray.RemoveAll();
	pogBltDiagram = NULL;
}

	

BEGIN_MESSAGE_MAP(BltDiagram, CFrameWnd)
	//{{AFX_MSG_MAP(BltDiagram)
	ON_WM_CREATE()
	ON_WM_DESTROY()
	ON_BN_CLICKED(IDC_WORES_BLT, OnWoRes)
	ON_BN_CLICKED(IDC_BLTOVERVIEW, OnOverview)
	ON_BN_CLICKED(IDC_BLTCCA, OnCca)
	ON_BN_CLICKED(IDC_BLTATTENTION, OnAttention)
	ON_BN_CLICKED(IDC_BEENDEN, OnBeenden)
	ON_BN_CLICKED(IDC_ANSICHT, OnAnsicht)
	ON_BN_CLICKED(IDC_INSERT, OnInsert)
    ON_WM_GETMINMAXINFO()
    ON_WM_PAINT()
    ON_WM_ERASEBKGND()
    ON_WM_SIZE()
    ON_WM_HSCROLL()
//    ON_BN_CLICKED(IDC_PREV, OnNextChart)
//    ON_BN_CLICKED(IDC_NEXT, OnPrevChart)
    ON_WM_TIMER()
    ON_MESSAGE(WM_POSITIONCHILD, OnPositionChild)
    ON_MESSAGE(WM_UPDATEDIAGRAM, OnUpdateDiagram)
    ON_CBN_SELCHANGE(IDC_VIEW, OnViewSelChange)
	ON_CBN_CLOSEUP(IDC_VIEW, OnCloseupView)
	ON_BN_CLICKED(IDC_ZEIT, OnZeit)
	ON_BN_CLICKED(IDC_SEARCH, OnSearch)
	ON_BN_CLICKED(IDC_OFFLINE, OnOffline)
	ON_WM_KEYDOWN()
	ON_WM_KEYUP()
	ON_WM_CLOSE()
	ON_MESSAGE(WM_REPAINT_ALL, RepaintAll)
	ON_WM_ACTIVATE()
	ON_BN_CLICKED(IDC_CHANGES, OnChanges)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


static void BltDiagramCf(void *popInstance, int ipDDXType,
    void *vpDataPointer, CString &ropInstanceName)
{
	BltDiagram *polDiagram = (BltDiagram *)popInstance;

	TIMEPACKET *polTimePacket ;

	switch (ipDDXType)
	{
	case REDISPLAY_ALL:
		polDiagram->RedisplayAll();
		break;
	case STAFFDIAGRAM_UPDATETIMEBAND:
		polTimePacket = (TIMEPACKET *)vpDataPointer;
		polDiagram->SetTimeBand(polTimePacket->StartTime, polTimePacket->EndTime);
		polDiagram->UpdateTimeBand();
		break;
	case SAS_VIEW_CHANGED:
        polDiagram->ViewSelChange( (char *) vpDataPointer);
		break;
	case SAS_GET_CUR_VIEW:
		polDiagram->GetCurView((char *) vpDataPointer);
		break;
	case KONFLIKTCHECK_FOLLOWS:
//		ogPosDiaFlightData.UpdateBarTimes();
		break;
	}
}


void BltDiagram::GetCurView( char * pcpView)
{
	strcpy(pcpView, omViewer.GetViewName());
}


void BltDiagram::ViewSelChange(char *pcpView)
{

    char clText[64];

	strcpy(clText, pcpView );    

	omViewer.SelectView(clText);

	
	CComboBox *polCB = (CComboBox *) omDialogBar.GetDlgItem(IDC_VIEW);

	polCB->SetCurSel(polCB->FindString( 0, clText ) );

	omViewer.SelectView(clText);


	if(IsIconic())  //RST
	{
		return;
	}


	omViewer.MakeMasstab();
	CTime olT = omViewer.GetGeometrieStartTime();
	CString olS = olT.Format("%d.%m.%Y-%H:%M");
	SetTSStartTime(olT);
	omTSDuration = omViewer.GetGeometryTimeSpan();

    
	long llTotalMin = omDuration.GetTotalMinutes() - omTSDuration.GetTotalMinutes();
	long nPos = (omTSStartTime - omStartTime).GetTotalMinutes();
	nPos = nPos * 1000L / llTotalMin;
	SetScrollPos(SB_HORZ, int(nPos), TRUE);
	
	
	
//rkr
	//Set begin to local if local
	CTime olTSStartTime = omTSStartTime;
	if(bgGatPosLocal)
		ogBasicData.UtcToLocal(olTSStartTime);
    
	omTimeScale.SetDisplayTimeFrame(olTSStartTime, omTSDuration, omTSInterval);
//	omTimeScale.SetDisplayTimeFrame(omTSStartTime, omTSDuration, omTSInterval);
    omTimeScale.Invalidate(TRUE);
	SetTSStartTime(omTSStartTime);
	ChangeViewTo(clText, false);

}


/////////////////////////////////////////////////////////////////////////////
// BltDiagram message handlers
/*void BltDiagram::PositionChild()
{
    CRect olRect;
    CRect olChartRect;
    BltChart *polChart;
    
    int ilLastY = 0;
    omClientWnd.GetClientRect(&olRect);
    for (int ilIndex = 0; ilIndex < omPtrArray.GetSize(); ilIndex++)
    {
        polChart = (BltChart *) omPtrArray.GetAt(ilIndex);
        
        if (ilIndex < imFirstVisibleChart)
        {
            polChart->ShowWindow(SW_HIDE);
            continue;
        }
        
        polChart->GetClientRect(&olChartRect);
        olChartRect.right = olRect.right;

        olChartRect.top = ilLastY;

        ilLastY += polChart->GetHeight();
        olChartRect.bottom = ilLastY;
        
        // check
        if ((polChart->GetState() != Minimized) &&
			(olChartRect.top < olRect.bottom) && (olChartRect.bottom > olRect.bottom))
        {
            olChartRect.bottom = olRect.bottom;
            polChart->SetState(Normal);
        }
        //
        
        polChart->MoveWindow(&olChartRect, FALSE);
		polChart->ShowWindow(SW_SHOW);
	}
    
	omClientWnd.Invalidate(TRUE);
	//SetAllStaffAreaButtonsColor();
	OnUpdatePrevNext();
	UpdateTimeBand();
}
*/
void BltDiagram::PositionChild()
{
   CRect olRect;
    CRect olChartRect;
    
    int ilLastY = 0;
    omClientWnd.GetClientRect(&olRect);
         
    pomChart->GetClientRect(&olChartRect);
    olChartRect.right = olRect.right;

    olChartRect.top = ilLastY;

    ilLastY += pomChart->GetHeight();
    olChartRect.bottom = ilLastY;
        
    // check
    if ((pomChart->GetState() != Minimized) &&
		(olChartRect.top < olRect.bottom) && (olChartRect.bottom > olRect.bottom))
    {
        olChartRect.bottom = olRect.bottom;
        pomChart->SetState(Normal);
    }
    //
        
    pomChart->MoveWindow(&olChartRect, FALSE);
	pomChart->ShowWindow(SW_SHOW);

    
	omClientWnd.Invalidate(TRUE);
	//SetAllStaffAreaButtonsColor();
 	UpdateTimeBand();
}

void BltDiagram::SetTSStartTime(CTime opTSStartTime)
{
    omTSStartTime = opTSStartTime;
	/*
    if ((omTSStartTime < omStartTime) ||
		(omTSStartTime > omStartTime + omDuration))
    {
        omStartTime = CTime(omTSStartTime.GetYear(), omTSStartTime.GetMonth(), omTSStartTime.GetDay(),
            0, 0, 0) - CTimeSpan(1, 0, 0, 0);
    }
      */  
    char clBuf[8];
    sprintf(clBuf, "%02d%02d%02d",
        omTSStartTime.GetDay(), omTSStartTime.GetMonth(), omTSStartTime.GetYear() % 100);
    omTSDate.SetWindowText(clBuf);
    omTSDate.Invalidate(FALSE);
}

/*
CListBox *BltDiagram::GetBottomMostGantt()
{
	// Check the size of the area for displaying charts.
	// Pichet used "omClientWnd" not the diagram itself, so we will get the size of this window
	CRect olClientRect;
	omClientWnd.GetClientRect(olClientRect);
	omClientWnd.ClientToScreen(olClientRect);

	// Searching for the bottommost chart
	BltChart *polChart;
	for (int ilLc = imFirstVisibleChart; ilLc < omPtrArray.GetSize(); ilLc++)
	{
		polChart = (BltChart *)omPtrArray[ilLc];
		CRect olRect, olChartRect;
		polChart->GetClientRect(olChartRect);
		polChart->ClientToScreen(olChartRect);
		if (!olRect.IntersectRect(&olChartRect, &olClientRect))
			break;
	}

	// Check if the chart we have found is a valid one
	--ilLc;
	if (!(0 <= ilLc && ilLc <= omPtrArray.GetSize()-1))
		return NULL;

	return &((BltChart *)omPtrArray[ilLc])->omGantt;
}
*/

int BltDiagram::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	if (CFrameWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

    SetTimer(0, (UINT) 60 * 1000, NULL);
//aiaaia
	ActivateTimer();
	//if (igTimerTimelineMinutes > 0)
	//	SetTimer(1, (UINT) 60 * 1000 * ogPosDiaFlightData.cimBartimesUpdateMinute, NULL);  // Update bar times
//aiaaia

	omDialogBar.Create( this, IDD_BLTDIAGRAM, CBRS_TOP, IDD_BLTDIAGRAM );
	SetWindowPos(&wndTop/*Most*/, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE);

    omViewer.Attach(this);
	UpdateComboBox();


    CRect olRect; GetClientRect(&olRect);
	CTime olCurrentTime;
	olCurrentTime = CTime::GetCurrentTime();
	ogBasicData.LocalToUtc(olCurrentTime);

	// Create 'BltAttention'-Button
	CButton *polButton = (CButton *)omDialogBar.GetDlgItem(IDC_BLTATTENTION2);	
 	WINDOWPLACEMENT olWndPlace;
	polButton->GetWindowPlacement( &olWndPlace ); 
	m_CB_BltAttention.Create(GetString(IMFK_ATTENTION), WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON | BS_OWNERDRAW, olWndPlace.rcNormalPosition, &omDialogBar, IDC_BLTATTENTION);
    m_CB_BltAttention.SetFont(polButton->GetFont());
	m_CB_BltAttention.EnableWindow(true);
	// Update Attention-Button
	ogKonflikte.CheckAttentionButton();
	m_CB_BltAttention.SetSecState(ogPrivList.GetStat("BLTDIAGRAMM_CB_Attention"));	


	// Create 'Wores'-Button
	polButton = (CButton *)omDialogBar.GetDlgItem(IDC_WORES_BLT2);	
	polButton->GetWindowPlacement( &olWndPlace ); 
	CString olTmp;
	olTmp.Format("%s (%d)", GetString(IDS_STRING2200),0);
	m_CB_WoRes.Create(olTmp, WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON | BS_OWNERDRAW, olWndPlace.rcNormalPosition, &omDialogBar, IDC_WORES_BLT);
    m_CB_WoRes.SetFont(polButton->GetFont());
	m_CB_WoRes.EnableWindow(true);
	m_CB_WoRes.SetSecState(ogPrivList.GetStat("BLTDIAGRAMM_CB_WORes"));	


	// Create 'Offline'-Button
	polButton = (CButton *)omDialogBar.GetDlgItem(IDC_OFFLINE2);	
	polButton->GetWindowPlacement( &olWndPlace ); 
	m_CB_Offline.Create(GetString(IDS_STRING1258), WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON | BS_OWNERDRAW, olWndPlace.rcNormalPosition, &omDialogBar, IDC_OFFLINE);
    m_CB_Offline.SetFont(polButton->GetFont());
	m_CB_Offline.EnableWindow(true);
	//m_CB_Offline.ShowWindow(SW_HIDE);
	m_CB_Offline.SetSecState(ogPrivList.GetStat("BLTDIAGRAMM_CB_Offline"));	

	// Create 'Changes'-Button
	polButton = (CButton *)omDialogBar.GetDlgItem(IDC_CHANGES);	
	polButton->GetWindowPlacement( &olWndPlace ); 
	m_CB_Changes.Create(GetString(IDS_CHANGES), WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON | BS_OWNERDRAW, olWndPlace.rcNormalPosition, &omDialogBar, IDC_CHANGES);
    m_CB_Changes.SetFont(polButton->GetFont());
	m_CB_Changes.EnableWindow(true);
	m_CB_Changes.SetSecState(ogPrivList.GetStat("DESKTOP_CB_History"));

//	m_KlebeFkt.Create( "&Klebefunktion", SS_CENTER | WS_CHILD | WS_VISIBLE, 
//						CRect(olRect.left + 400, 6,olRect.left + 480, 23), this, IDC_KLEBEFKT);

    omTSStartTime = olCurrentTime;// - CTimeSpan(0, 1, 0, 0);
    omStartTime = CTime(omTSStartTime.GetYear(), omTSStartTime.GetMonth(), omTSStartTime.GetDay(),
        0, 0, 0);// - CTimeSpan(1, 0, 0, 0);

    omDuration = CTimeSpan(3, 0, 0, 0);
    omTSDuration = CTimeSpan(0, 6, 0, 0);
    omTSInterval = CTimeSpan(0, 0, 10, 0);

	//Die wollen local
    CTime olUCT = CTime::GetCurrentTime();//olCurrentTime;
    CTime olRealUTC = olCurrentTime;
    char olBuf[16];
    sprintf(olBuf, "%02d%02d/%02d%02dz",
        olUCT.GetHour(), olUCT.GetMinute(),        
        olRealUTC.GetHour(), olRealUTC.GetMinute()
    );

    
	
	CTime olLocalTime = CTime::GetCurrentTime();
	CTime olUtcTime = CTime::GetCurrentTime();
	ogBasicData.LocalToUtc(olUtcTime);

//	char pclTimes[36];
//	char pclDate[36];

	
	CTime olUtcTimeTmp = CTime::GetCurrentTime();
	olUtcTimeTmp -= ogUtcDiff;
/*	
	if(ogUtcDiff != 0)
	{
		sprintf(pclDate, "%02d.%02d.%02d z", olUtcTimeTmp.GetDay(), olUtcTimeTmp.GetMonth(), olUtcTimeTmp.GetYear() % 100);
		sprintf(pclTimes, "%02d:%02d / %02d:%02d z", olLocalTime.GetHour(), olLocalTime.GetMinute(), olUtcTimeTmp.GetHour(), olUtcTimeTmp.GetMinute());
	}
	else
	{
		sprintf(pclDate, "%02d.%02d.%02d z", olUtcTime.GetDay(), olUtcTime.GetMonth(), olUtcTime.GetYear() % 100);
		sprintf(pclTimes, "%02d:%02d / %02d:%02d z", olLocalTime.GetHour(), olLocalTime.GetMinute(), olUtcTime.GetHour(), olUtcTime.GetMinute());
	}
    
	
	omTime.Create(pclTimes, SS_CENTER | WS_CHILD | WS_VISIBLE,
        CRect(olRect.right - 192, 3, olRect.right - 90, 22), this);

    omDate.Create(pclDate, SS_CENTER | WS_CHILD | WS_VISIBLE,
        CRect(olRect.right - 88, 3, olRect.right - 8, 22), this);
        
    omDate.SetTextColor(RGB(255,0,0));
*/	
	

    sprintf(olBuf, "%02d%02d%02d",  omTSStartTime.GetDay(), omTSStartTime.GetMonth(), omTSStartTime.GetYear() % 100);
    int ilPos = (imStartTimeScalePos - olRect.left - 80) / 2;
    omTSDate.Create(olBuf, SS_CENTER | WS_CHILD | WS_VISIBLE,
        CRect(ilPos, 35, ilPos + 80, 52), this);

    // CBitmapButton
    omBB1.Create("PREV", BS_OWNERDRAW | WS_CHILD | WS_VISIBLE,
        CRect(olRect.right - 36, 21 + 5, olRect.right - 19, (21 + 5) + 25), this, IDC_PREV);
    omBB1.LoadBitmaps("PREVU", "PREVD", NULL, "NEXTX");

    omBB2.Create("NEXT", BS_OWNERDRAW | WS_CHILD | WS_VISIBLE,
        CRect(olRect.right - 19, 21 + 5, olRect.right - 2, (21 + 5) + 25), this, IDC_NEXT);
    omBB2.LoadBitmaps("NEXTU", "NEXTD", NULL, "NEXTX");

	omBB1.ShowWindow(SW_HIDE);
	omBB2.ShowWindow(SW_HIDE);

	omTimeScale.lmBkColor = lgBkColor;
    omTimeScale.Create(NULL, "TimeScale", WS_CHILD | WS_VISIBLE,
        CRect(imStartTimeScalePos, 28, olRect.right - (36 + 2), 62), this, 0, NULL);
//rkr
	//Set begin to local if local
	CTime olTSStartTime = omTSStartTime;
	if(bgGatPosLocal)
		ogBasicData.UtcToLocal(olTSStartTime);
    
	omTimeScale.SetDisplayTimeFrame(olTSStartTime, omTSDuration, omTSInterval);
//	omTimeScale.SetDisplayTimeFrame(omTSStartTime, omTSDuration, omTSInterval);

	if(bgGatPosLocal)
		omTimeScale.UpdateCurrentTimeLine(olLocalTime);
	else
		omTimeScale.UpdateCurrentTimeLine(olRealUTC);

     // must be static
    static UINT ilIndicators[] = { ID_SEPARATOR };
    omStatusBar.Create(this, CBRS_BOTTOM | WS_CHILD | WS_VISIBLE,0);
    omStatusBar.SetIndicators(ilIndicators, sizeof(ilIndicators) / sizeof(UINT));

    UINT nID, nStyle; int cxWidth;
    omStatusBar.GetPaneInfo(0, nID, nStyle, cxWidth);
    omStatusBar.SetPaneInfo(0, nID, SBPS_STRETCH | SBPS_NORMAL, cxWidth);

    olRect.top += 64;                   // for Dialog Bar & TimeScale
    olRect.bottom -= 20;                // for Horizontal Scroll Bar
    omClientWnd.Create(NULL, "ClientWnd", WS_CHILD /*| WS_HSCROLL */ | WS_VISIBLE, olRect, this,
        0 /* IDD_CLIENTWND */, NULL);
        
	// This will fix the bug for the horizontal scroll bar in the preplan mode
    long llTSMin = (omTSStartTime - omStartTime).GetTotalMinutes();
    long llTotalMin = omDuration.GetTotalMinutes() - omTSDuration.GetTotalMinutes();
    SetScrollRange(SB_HORZ, 0, 1000, FALSE);
    SetScrollPos(SB_HORZ, (int) (1000 * llTSMin / llTotalMin), FALSE);

//    BltChart *polChart;
    CRect olPrevRect;
    omClientWnd.GetClientRect(&olRect);
    
    imFirstVisibleChart = 0;
	
    int ilLastY = 0;
    olRect.SetRect(olRect.left, ilLastY, olRect.right, ilLastY);
    
    pomChart = new BltChart;
    pomChart->SetTimeScale(&omTimeScale);
    pomChart->SetViewer(&omViewer);
    pomChart->SetStatusBar(&omStatusBar);
    pomChart->SetStartTime(omTSStartTime);
    pomChart->SetInterval(omTimeScale.GetDisplayDuration());
    pomChart->Create(NULL, "BltChart", WS_OVERLAPPED | WS_BORDER | WS_CHILD | WS_VISIBLE,
        olRect, &omClientWnd,
        0 , NULL);
    

    OnTimer(0);

	// Register DDX call back function
	//TRACE("BltDiagram: DDX Registration\n");
	ogDdx.Register(this, REDISPLAY_ALL, CString("STAFFDIAGRAM"),
		CString("Redisplay all from What-If"), BltDiagramCf);	// for what-if changes
	ogDdx.Register(this, STAFFDIAGRAM_UPDATETIMEBAND, CString("STAFFDIAGRAM"),
		CString("Update Time Band"), BltDiagramCf);	// for updating the yellow lines

 	ogDdx.Register(this, SAS_VIEW_CHANGED,CString(" "), CString("DATA_RELOAD"),BltDiagramCf);
 	ogDdx.Register(this, SAS_GET_CUR_VIEW,CString(" "), CString("DATA_RELOAD"),BltDiagramCf);
	ogDdx.Register(this, KONFLIKTCHECK_FOLLOWS,CString(" "), CString("KONFLIKTCHECK_FOLLOWS"),BltDiagramCf);


	CButton *polCB; 
	polCB = (CButton *) omDialogBar.GetDlgItem(IDC_ANSICHT);
	SetpWndStatAll(ogPrivList.GetStat("BLTDIAGRAMM_CB_Ansicht"),polCB);

	if (!bgOffRelFuncGatpos)
	{
		m_CB_Offline.ShowWindow(SW_HIDE);
	}


	ogPosDiaFlightData.Register();	 

	// Create BltCcaDlg
	if (pomBltCcaTableDlg == NULL)
	{
		pomBltCcaTableDlg = new BltCcaTableDlg(this);
//		pomBltCcaTableDlg->SetWindowPos(&wndTop,100,200, 0, 0, SWP_HIDEWINDOW | SWP_NOSIZE);
	}

	// Create OverviewTableDlg
	if (pomBltOverviewTableDlg == NULL)
	{
		pomBltOverviewTableDlg = new BltOverviewTableDlg(this);
//		pomBltOverviewTableDlg->SetWindowPos(&wndTop,100,200,750,400, SWP_HIDEWINDOW);
	}

	// Create ConflictDlg
	if (pomBltKonflikteDlg == NULL)
	{
		pomBltKonflikteDlg = new BltKonflikteDlg(this);
//		pomBltKonflikteDlg->SetWindowPos(&wndTop,100,200,700,400, SWP_HIDEWINDOW);
	}	

	// Connect to the GatPosFlightData-Class
	ogPosDiaFlightData.Connect();
	// Connect to the FlightsWithoutResource - Table
	pogWoResTableDlg->Connect();


	char pclView[100];
	strcpy(pclView, "<Default>");
	ogDdx.DataChanged((void *)this, SAS_GET_CUR_VIEW,(void *)pclView );
	omViewer.SelectView(pclView);

	CTime olFrom = TIMENULL;
	CTime olTo = TIMENULL;
	CString olFtyps;
	omViewer.GetTimeSpanAndFtyp(olFrom, olTo, olFtyps);
	if (olFrom != TIMENULL && olTo != TIMENULL)
	{
		omDuration = (olTo - olFrom) + CTimeSpan(1, 0, 0, 0);
		if (bgGatPosLocal)
			ogBasicData.UtcToLocal(olFrom);
		omStartTime = olFrom - CTimeSpan(1, 0, 0, 0);
	}
	ViewSelChange(pclView);

	// set caption
	CString olTimes;
	if (bgGatPosLocal)
		olTimes = GetString(IDS_STRING1921);
	else
		olTimes = GetString(IDS_STRING1920);
	CString olCaption = GetString(IDS_STRING1010);
	olCaption += " ("+olTimes+")";
	SetWindowText(olCaption);

//######
	olRect = CRect(0, 65, ::GetSystemMetrics(SM_CXSCREEN), ::GetSystemMetrics(SM_CYSCREEN));
	GetDialogFromReg(olRect, m_key);
	SetWindowPos(&wndTop, olRect.left, olRect.top, olRect.Width(), olRect.Height(), SWP_SHOWWINDOW);// | SWP_NOMOVE );
//######

	return 0;
}

void BltDiagram::SaveToReg() 
{
	BOOL ok = WriteDialogToReg((CWnd*) this, m_key);
}



void BltDiagram::OnBeenden() 
{
	//ogPosDiaFlightData.UnRegister();	 
	//ogPosDiaFlightData.ClearAll();	 
	DestroyWindow();	
}


void BltDiagram::OnInsert()
{
	RotationISFDlg dlg;
	dlg.DoModal();
}


void BltDiagram::OnAnsicht()
{
	bool dauer = true;
	bool blaccept = false;
	while (dauer)
	{
		dauer = false;
 
//		BltDiaPropertySheet olDlg("POSDIA", this, &omViewer, 0, GetString(IDS_STRING1645));
	 	BltDiaPropertySheet olDlg("BLTDIA", this, &omViewer, 0, GetString(IDS_STRING1645));
		bmIsViewOpen = true;
		if(olDlg.DoModal() != IDCANCEL)
		{
//			if (olDlg.FilterChanged())
//				LoadFlights();

			if (olDlg.FilterChanged() || !blaccept)
			{
				blaccept = LoadFlights();
			}

 			if (blaccept)
			{
				omViewer.MakeMasstab();
				CTime olT = omViewer.GetGeometrieStartTime();
				CString olS = olT.Format("%d.%m.%Y-%H:%M");
				SetTSStartTime(olT);
				omTSDuration = omViewer.GetGeometryTimeSpan();
		//rkr
				//Set begin to local if local
				CTime olTSStartTime = omTSStartTime;
				if(bgGatPosLocal)
					ogBasicData.UtcToLocal(olTSStartTime);
    
				omTimeScale.SetDisplayTimeFrame(olTSStartTime, omTSDuration, omTSInterval);
		//  	omTimeScale.SetDisplayTimeFrame(omTSStartTime, omTSDuration, omTSInterval);
				omTimeScale.Invalidate(TRUE);
				ChangeViewTo(omViewer.GetViewName(), false);
				long llTotalMin = omDuration.GetTotalMinutes() - omTSDuration.GetTotalMinutes();
				long nPos = (omTSStartTime - omStartTime).GetTotalMinutes();
				nPos = nPos * 1000L / llTotalMin;
				SetScrollPos(SB_HORZ, int(nPos), TRUE);

				char pclView[100];
				strcpy(pclView, omViewer.GetViewName());
				ogDdx.DataChanged((void *)this, SAS_VIEW_CHANGED,(void *)pclView );
				pogWoResTableDlg->Rebuild();

				dauer = false;

			}
			else
				dauer = true;

		}
	}
	bmIsViewOpen = false;
	UpdateComboBox();

}

bool BltDiagram::LoadFlights(char *pspView)
{
	if(pspView != NULL)
	{
		omViewer.SelectView(pspView);
	}
	else
	{
		omViewer.SelectView(omViewer.GetViewName());
	}
	
	CString olWhere;
	CTime olFrom;
	CTime olTo;
	CString olFtyps;

	bool blRotation;
//	omViewer.GetZeitraumWhereString(olWhere, blRotation, CString("") );
	CString olFOGTAB = "";
	bool blWithAcOnGround = bgShowOnGround;
	if (pogPosDiagram)
		blWithAcOnGround = pogPosDiagram->WithAcOnGround();

	if (!omViewer.GetZeitraumWhereString(olWhere, blRotation, olFOGTAB, blWithAcOnGround))
		return false;


	olWhere += CString(" AND FLNO <> ' '");
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));

	omStatusBar.SetPaneText(0, GetString(IDS_STRING1205));
	omStatusBar.UpdateWindow();

	
	omViewer.GetTimeSpanAndFtyp(olFrom, olTo, olFtyps);
	
	//check timelimit for on gound (at moment we don#t know if we should use global or local)
	if (bgShowOnGround && pogPosDiagram)
	{
		if (olFrom != TIMENULL && olTo != TIMENULL)
		{
			CTimeSpan olDuration = olTo - olFrom;
			int ilTotalMinutes = olDuration.GetTotalMinutes();
			if (ogOnGroundTimelimit.GetTotalMinutes() >= ilTotalMinutes /*&& !bmMessageDone*/)
			{
				CString olMess;
				CString olTimeLimit;
				int ilHours = ogOnGroundTimelimit.GetTotalHours();
				int ilMinutes = ogOnGroundTimelimit.GetTotalMinutes() - ilHours*60;
				olTimeLimit.Format("%d.%d",ilHours,ilMinutes);
				olMess.Format(GetString(IDS_ONGROUND_TIMELIMIT), olTimeLimit );

				if (CFPMSApp::MyTopmostMessageBox(NULL, olMess, GetString(ST_WARNING), MB_OKCANCEL | MB_ICONWARNING) == IDOK)
					bool blok = true;
				else
					return false;
			}
		}
	}

	CTime olCurr = CTime::GetCurrentTime();
	if(bgGatPosLocal) ogBasicData.LocalToUtc(olCurr);


	CButton *polCB = (CButton *) omDialogBar.GetDlgItem(IDC_ZEIT);

	if(olFrom < olCurr && olTo > olCurr)
	{
		polCB->EnableWindow(TRUE);
	}
	else
	{
		polCB->EnableWindow(FALSE);
	}
	
	
		
	ogPosDiaFlightData.SetPreSelection(olFrom, olTo, olFtyps);
	
//	ogPosDiaFlightData.ReadAllFlights(olWhere, blRotation, "");
	ogPosDiaFlightData.ReadAllFlights(olWhere, blRotation, olFOGTAB);

	omDuration = (olTo - olFrom) + CTimeSpan(1, 0, 0, 0);
	if (bgGatPosLocal)
		ogBasicData.UtcToLocal(olFrom);
	omStartTime = olFrom - CTimeSpan(1, 0, 0, 0);
	
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
	omStatusBar.SetPaneText(0, "");
	omStatusBar.UpdateWindow();

	return true;
}

void BltDiagram::OnDestroy() 
{
	SaveToReg();
//	if (bgModal == TRUE)
//		return;
	bmNoUpdatesNow = TRUE;
	omViewer.AllowUpdates(bmNoUpdatesNow);
	ogDdx.UnRegister(&omViewer, NOTUSED);

	if (pomBltOverviewTableDlg)
	{
		pomBltOverviewTableDlg->SaveToReg();
		delete pomBltOverviewTableDlg;
		pomBltOverviewTableDlg = NULL;
	}

	if (pomBltKonflikteDlg)
	{
		pomBltKonflikteDlg->SaveToReg();
		delete pomBltKonflikteDlg;
		pomBltKonflikteDlg = NULL;
	}

	if (pomBltCcaTableDlg)
	{
		pomBltCcaTableDlg->SaveToReg();
		delete pomBltCcaTableDlg;
		pomBltCcaTableDlg = NULL;
	}

	// Unregister DDX call back function
	TRACE("BltDiagram: DDX Unregistration %s\n",
		::IsWindow(GetSafeHwnd())? "": "(window is already destroyed)");
    ogDdx.UnRegister(this, NOTUSED);

	// Disconnect from the GatPosFlightData-Class
 	ogPosDiaFlightData.Disconnect();
	// Disconnect from the FlightsWithoutResource - Table
	if (pogWoResTableDlg)
		pogWoResTableDlg->Disconnect();

	CFrameWnd::OnDestroy();
}

void BltDiagram::OnClose() 
{
    // Ignore close -- This makes Alt-F4 keys no effect
	CFrameWnd::OnClose();
}

void BltDiagram::OnGetMinMaxInfo(MINMAXINFO FAR* lpMMI) 
{
    // TODO: Add your message handler code here and/or call default
    CFrameWnd::OnGetMinMaxInfo(lpMMI);

    lpMMI->ptMaxTrackSize = omMaxTrackSize;
    lpMMI->ptMinTrackSize = omMinTrackSize;
}

BOOL BltDiagram::OnEraseBkgnd(CDC* pDC) 
{
    // TODO: Add your message handler code here and/or call default
    CRect olClipRect;
    pDC->GetClipBox(&olClipRect);
    //GetClientRect(&olClipRect);

    CBrush olBrush(lmBkColor);
    CBrush *polOldBrush = pDC->SelectObject(&olBrush);
    pDC->PatBlt(olClipRect.left, olClipRect.top,
        olClipRect.Width(), olClipRect.Height(),
        PATCOPY);
    pDC->SelectObject(polOldBrush);
    
    return TRUE;
    
    //return CFrameWnd::OnEraseBkgnd(pDC);
}

void BltDiagram::OnPaint() 
{
    CPaintDC dc(this); // device context for painting
    
    // TODO: Add your message handler code here
    CRect olRect;
    GetClientRect(&olRect);
    
    // draw horizontal line
    CPen olHPen(PS_SOLID, 1, lmHilightColor);
    CPen *polOldPen = dc.SelectObject(&olHPen);
    dc.MoveTo(olRect.left, 27); dc.LineTo(olRect.right, 27);
    dc.MoveTo(olRect.left, 62); dc.LineTo(olRect.right, 62);

    CPen olTPen(PS_SOLID, 1, lmTextColor);
    dc.SelectObject(&olTPen);
    dc.MoveTo(olRect.left, 63); dc.LineTo(olRect.right, 63);
    
    
    // draw vertical line
    dc.SelectObject(&olTPen);
    dc.MoveTo(imStartTimeScalePos - 2, 27);
    dc.LineTo(imStartTimeScalePos - 2, 63);

    dc.SelectObject(&olHPen);
    dc.MoveTo(imStartTimeScalePos - 1, 27);
    dc.LineTo(imStartTimeScalePos - 1, 63);

    dc.SelectObject(polOldPen);
    // Do not call CFrameWnd::OnPaint() for painting messages
}


void BltDiagram::OnSize(UINT nType, int cx, int cy) 
{
    CFrameWnd::OnSize(nType, cx, cy);
    
    // TODO: Add your message handler code here
    CRect olRect; GetClientRect(&olRect);

    CRect olBB1Rect(olRect.right - 36, 27 + 5, olRect.right - 19, (27 + 5) + 25);
    omBB1.MoveWindow(&olBB1Rect, TRUE);
    
    CRect olBB2Rect(olRect.right - 19, 27 + 5, olRect.right - 2, (27 + 5) + 25);
    omBB2.MoveWindow(&olBB2Rect, TRUE);

    CRect olTSRect(imStartTimeScalePos, 28, olRect.right - (36 + 2), 62);
    omTimeScale.MoveWindow(&olTSRect, TRUE);

    olRect.top += 64;                   // for Dialog Bar & TimeScale
    olRect.bottom -= 20;                // for Horizontal Scroll Bar
    // LeftTop, RightBottom
    omClientWnd.MoveWindow(&olRect, TRUE);

	GetClientRect(&olRect);
//	omTime.MoveWindow( CRect(olRect.right - 192, 3, olRect.right - 90, 22));
//  omDate.MoveWindow( CRect(olRect.right - 88, 3, olRect.right - 8, 22));

	// repaint gantt
	omTimeScale.GetClientRect(&olRect);
	omViewer.ChangeViewTo(omViewer.SelectView(), omTSStartTime, omTimeScale.GetTimeFromX(olRect.Width() + 38));
	PositionChild();
//	SetAllStaffAreaButtonsColor();
}

void BltDiagram::OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar)
{
    // TODO: Add your message handler code here and/or call default
    
    //CFrameWnd::OnHScroll(nSBCode, nPos, pScrollBar);


/*	CUIntArray olLines;

	int ilCurrLine;

	BltChart *polChart;
	for (int ilLc = 0; ilLc < omPtrArray.GetSize(); ilLc++)
	{
		polChart = (BltChart *)omPtrArray[ilLc];

		ilCurrLine = polChart->omGantt.GetTopIndex();
		olLines.Add(ilCurrLine);
	}
*/
	int ilCurrLine = pomChart->GetGanttPtr()->GetTopIndex();

    long llTotalMin;
    int ilPos;
    
	CRect olRect;
	omTimeScale.GetClientRect(&olRect);
    switch (nSBCode)
    {
        case SB_LINEUP :
            //OutputDebugString("LineUp\n\r");
            
            llTotalMin = omDuration.GetTotalMinutes() - omTSDuration.GetTotalMinutes();            
            ilPos = GetScrollPos(SB_HORZ) - int (60 * 1000L / llTotalMin);
            if (ilPos <= 0)
            {
                ilPos = 0;
                SetTSStartTime(omStartTime);
            }
            else
                SetTSStartTime(omTSStartTime - CTimeSpan(0, 0, 60, 0));

            omTimeScale.SetDisplayStartTime(omTSStartTime);
            omTimeScale.Invalidate(TRUE);

			omViewer.ChangeViewTo(omViewer.SelectView(), omTSStartTime, omTimeScale.GetTimeFromX(olRect.Width() + 38));
			PositionChild();

            SetScrollPos(SB_HORZ, ilPos, TRUE);
////////////////////////////////////////////////////////////////////////
// Damkerng 06/27/96:
// This thing should be remove. The technic has been changed to use the
// method Invalidate() not only for the TimeScale, but for the entire
// omClientWnd also.
            omClientWnd.Invalidate(FALSE);
////////////////////////////////////////////////////////////////////////

        break;
        
        case SB_LINEDOWN :
            //OutputDebugString("LineDown\n\r");

            llTotalMin = omDuration.GetTotalMinutes() - omTSDuration.GetTotalMinutes();            
            ilPos = GetScrollPos(SB_HORZ) + int (60 * 1000L / llTotalMin);
            if (ilPos >= 1000)
            {
                ilPos = 1000;
                SetTSStartTime(omStartTime + CTimeSpan(0, 0, int (llTotalMin), 0));
            }
            else
                SetTSStartTime(omTSStartTime + CTimeSpan(0, 0, 60, 0));

            omTimeScale.SetDisplayStartTime(omTSStartTime);
            omTimeScale.Invalidate(TRUE);

			omViewer.ChangeViewTo(omViewer.SelectView(), omTSStartTime, omTimeScale.GetTimeFromX(olRect.Width() + 38));
			PositionChild();

			SetScrollPos(SB_HORZ, ilPos, TRUE);
////////////////////////////////////////////////////////////////////////
// Damkerng 06/27/96:
// This thing should be remove. The technic has been changed to use the
// method Invalidate() not only for the TimeScale, but for the entire
// omClientWnd also.
            omClientWnd.Invalidate(FALSE);
////////////////////////////////////////////////////////////////////////
        break;
        
        case SB_PAGEUP :
            //OutputDebugString("PageUp\n\r");
            
            llTotalMin = omDuration.GetTotalMinutes() - omTSDuration.GetTotalMinutes();            
            ilPos = GetScrollPos(SB_HORZ)
                - int ((omTSDuration.GetTotalMinutes() / 2) * 1000L / llTotalMin);
            if (ilPos <= 0)
            {
                ilPos = 0;
                SetTSStartTime(omStartTime);
            }
            else
                SetTSStartTime(omTSStartTime - CTimeSpan(0, 0, int (omTSDuration.GetTotalMinutes() / 2), 0));

            omTimeScale.SetDisplayStartTime(omTSStartTime);
            omTimeScale.Invalidate(TRUE);

			omViewer.ChangeViewTo(omViewer.SelectView(), omTSStartTime, omTimeScale.GetTimeFromX(olRect.Width() + 38));
			PositionChild();

            SetScrollPos(SB_HORZ, ilPos, TRUE);
////////////////////////////////////////////////////////////////////////
// Damkerng 06/27/96:
// This thing should be remove. The technic has been changed to use the
// method Invalidate() not only for the TimeScale, but for the entire
// omClientWnd also.
            omClientWnd.Invalidate(FALSE);
////////////////////////////////////////////////////////////////////////
        break;
        
        case SB_PAGEDOWN :
            //OutputDebugString("PageDown\n\r");
            
            llTotalMin = omDuration.GetTotalMinutes() - omTSDuration.GetTotalMinutes();            
            ilPos = GetScrollPos(SB_HORZ)
                + int ((omTSDuration.GetTotalMinutes() / 2) * 1000L / llTotalMin);
            if (ilPos >= 1000)
            {
                ilPos = 1000;
                SetTSStartTime(omStartTime + CTimeSpan(0, 0, int (llTotalMin), 0));
            }
            else
                SetTSStartTime(omTSStartTime + CTimeSpan(0, 0, int (omTSDuration.GetTotalMinutes() / 2), 0));

            omTimeScale.SetDisplayStartTime(omTSStartTime);
            omTimeScale.Invalidate(TRUE);

			omViewer.ChangeViewTo(omViewer.SelectView(), omTSStartTime, omTimeScale.GetTimeFromX(olRect.Width() + 38));
			PositionChild();

            SetScrollPos(SB_HORZ, ilPos, TRUE);
////////////////////////////////////////////////////////////////////////
// Damkerng 06/27/96:
// This thing should be remove. The technic has been changed to use the
// method Invalidate() not only for the TimeScale, but for the entire
// omClientWnd also.
            omClientWnd.Invalidate(FALSE);
////////////////////////////////////////////////////////////////////////
        break;
        
        case SB_THUMBTRACK /* pressed, any drag time */:
            //OutputDebugString("ThumbTrack\n\r");

            llTotalMin = omDuration.GetTotalMinutes() - omTSDuration.GetTotalMinutes();
            
            SetTSStartTime(omStartTime + CTimeSpan(0, 0, int (llTotalMin * nPos / 1000), 0));
			////ChangeViewTo(omViewer.SelectView());
            
            omTimeScale.SetDisplayStartTime(omTSStartTime);
            omTimeScale.Invalidate(TRUE);
            
            SetScrollPos(SB_HORZ, nPos, TRUE);

            //char clBuf[64];
            //wsprintf(clBuf, "Thumb Position : %d", nPos);
            //omStatusBar.SetWindowText(clBuf);
        break;

        //case SB_THUMBPOSITION /* released */:
            //OutputDebugString("ThumbPosition\n\r");
        //break;
////////////////////////////////////////////////////////////////////////
// Damkerng 06/27/96:
// This thing should be remove. The technic has been changed to use the
// method Invalidate() not only for the TimeScale, but for the entire
// omClientWnd also.
        case SB_THUMBPOSITION:	// the thumb was just released?
			omViewer.ChangeViewTo(omViewer.SelectView(), omTSStartTime, omTimeScale.GetTimeFromX(olRect.Width() + 38));
			PositionChild();

            omClientWnd.Invalidate(FALSE);

////////////////////////////////////////////////////////////////////////

        case SB_TOP :
        //break;
        case SB_BOTTOM :
            //OutputDebugString("TopBottom\n\r");
        break;
        
        case SB_ENDSCROLL :
            //OutputDebugString("EndScroll\n\r");
            //OutputDebugString("\n\r");
        break;
    }

/*	for (ilLc = 0; ilLc < omPtrArray.GetSize(); ilLc++)
	{
		polChart = (BltChart *)omPtrArray[ilLc];
		polChart->omGantt.SetTopIndex(olLines[ilLc]);
	}
*/
	pomChart->GetGanttPtr()->SetTopIndex(ilCurrLine);

}
/*
void BltDiagram::OnPrevChart()
{
    if (imFirstVisibleChart > 0)
    {
        imFirstVisibleChart--;
		OnUpdatePrevNext();
        PositionChild();
    }
}

void BltDiagram::OnNextChart()
{
    if (imFirstVisibleChart < omPtrArray.GetUpperBound())
    {
        imFirstVisibleChart++;
		OnUpdatePrevNext();
        PositionChild();
    }
}

void BltDiagram::OnFirstChart()
{
	imFirstVisibleChart = 0;
	OnUpdatePrevNext();
	PositionChild();
}

void BltDiagram::OnLastChart()
{
	imFirstVisibleChart = omPtrArray.GetUpperBound();
	OnUpdatePrevNext();
	PositionChild();
}
*/

void BltDiagram::ActivateTimer()
{
	if (igTimerTimelineMinutes > 0)
		SetTimer(1, (UINT) 60 * 1000 * ogPosDiaFlightData.cimBartimesUpdateMinute, NULL);  // Update bar times
}

void BltDiagram::DeActivateTimer()
{
	KillTimer(1);
}

void BltDiagram::OnTimer(UINT nIDEvent)
{
//aiaaia
	if (nIDEvent == 1 && !bgConfCheckRuns)
	{
		DeActivateTimers();
		ogPosDiaFlightData.UpdateBarTimes();
		ActivateTimers();
	}
//	now done by DDX message KONFLIKTCHECK_FOLLOWS
//aiaaia

	///// Update time line!
	if(bmNoUpdatesNow == FALSE)
	{

//		char pclTimes[100];
//		char pclDate[100];

		CTime olLocalTime = CTime::GetCurrentTime();
		CTime olUtcTime = CTime::GetCurrentTime();
		ogBasicData.LocalToUtc(olUtcTime);

		CTime olUtcTimeTmp = CTime::GetCurrentTime();
		olUtcTimeTmp -= ogUtcDiff;
/*		
		if(ogUtcDiff != 0)
		{
			sprintf(pclDate, "%02d.%02d.%02d z", olUtcTimeTmp.GetDay(), olUtcTimeTmp.GetMonth(), olUtcTimeTmp.GetYear() % 100);
			sprintf(pclTimes, "%02d:%02d / %02d:%02d z", olLocalTime.GetHour(), olLocalTime.GetMinute(), olUtcTimeTmp.GetHour(), olUtcTimeTmp.GetMinute());
		}
		else
		{
			sprintf(pclDate, "%02d.%02d.%02d z", olUtcTime.GetDay(), olUtcTime.GetMonth(), olUtcTime.GetYear() % 100);
			sprintf(pclTimes, "%02d:%02d / %02d:%02d z", olLocalTime.GetHour(), olLocalTime.GetMinute(), olUtcTime.GetHour(), olUtcTime.GetMinute());
		}
*/
		if(bgGatPosLocal)
			omTimeScale.UpdateCurrentTimeLine(olLocalTime);
		else
			omTimeScale.UpdateCurrentTimeLine(olUtcTime);
/*
		omTime.SetWindowText(pclTimes);
		omTime.Invalidate(FALSE);

		omDate.SetWindowText(pclDate);
		omDate.Invalidate(FALSE);
*/
		if(bgGatPosLocal)
			pomChart->GetGanttPtr()->SetCurrentTime(olLocalTime);
		else
			pomChart->GetGanttPtr()->SetCurrentTime(olUtcTime);

/*		BltChart *polChart;
		for (int ilIndex = 0; ilIndex < omPtrArray.GetSize(); ilIndex++)
		{
			polChart = (BltChart *) omPtrArray.GetAt(ilIndex);

			if(bgGatPosLocal)
				polChart->GetGanttPtr()->SetCurrentTime(olLocalTime);
			else
				polChart->GetGanttPtr()->SetCurrentTime(olUtcTime);
		}
*/
		CFrameWnd::OnTimer(nIDEvent);
	}
	///// Update time line! (END)
}

void BltDiagram::SetMarkTime(CTime opStartTime, CTime opEndTime)
{
		pomChart->SetMarkTime(opStartTime, opEndTime);
/*
		BltChart *polChart;
		for (int ilIndex = 0; ilIndex < omPtrArray.GetSize(); ilIndex++)
		{
			polChart = (BltChart *) omPtrArray.GetAt(ilIndex);
			polChart->SetMarkTime(opStartTime, opEndTime);
		}
*/
}


LONG BltDiagram::OnPositionChild(WPARAM wParam, LPARAM lParam)
{
    PositionChild();
    return 0L;
}

LONG BltDiagram::OnUpdateDiagram(WPARAM wParam, LPARAM lParam)
{
//	UpdateWoResButton();

    CString olStr;
    int ipGroupNo = HIWORD(lParam);
    int ipLineNo = LOWORD(lParam);
	int ilItemID = omViewer.GetItemID(ipGroupNo, ipLineNo);

	CTime olTime = CTime((time_t) lParam);

	char clBuf[255];
	int ilCount;

    BltGantt *polBltGantt = pomChart -> GetGanttPtr();
    switch (wParam)
    {
        // group message
        case UD_INSERTGROUP :
            // CreateChild(ipGroupNo);

/*
            if (ipGroupNo <= imFirstVisibleChart)
			{
                imFirstVisibleChart++;
				OnUpdatePrevNext();
			}
*/
            PositionChild();
        break;

        case UD_UPDATEGROUP :
            olStr = omViewer.GetGroupText(ipGroupNo);
            pomChart->GetChartButtonPtr()->SetWindowText(olStr);
            pomChart->GetChartButtonPtr()->Invalidate(FALSE);

            olStr = omViewer.GetGroupTopScaleText(ipGroupNo);
            pomChart->GetTopScaleTextPtr()->SetWindowText(olStr);
            pomChart->GetTopScaleTextPtr()->Invalidate(TRUE);
			//SetStaffAreaButtonColor(ipGroupNo);
        break;
        
        case UD_DELETEGROUP :
/*
            delete omPtrArray.GetAt(ipGroupNo);
            //omPtrArray.GetAt(ipGroupNo) -> DestroyWindow();
            omPtrArray.RemoveAt(ipGroupNo);
            
            if (ipGroupNo <= imFirstVisibleChart)
			{
                imFirstVisibleChart--;
				OnUpdatePrevNext();
			}
*/
            PositionChild();
        break;

        // line message
        case UD_INSERTLINE :
            polBltGantt->InsertString(ilItemID, "");
			polBltGantt->RepaintItemHeight(ilItemID);
			
			ilCount = omViewer.GetLineCount(ipGroupNo);
			sprintf(clBuf, "%d", ilCount);
			pomChart->GetCountTextPtr()->SetWindowText(clBuf);
			pomChart->GetCountTextPtr()->Invalidate(TRUE);

			//SetStaffAreaButtonColor(ipGroupNo);
            PositionChild();
        break;
        
        case UD_UPDATELINE :
		{
            polBltGantt->RepaintVerticalScale(ilItemID);
			if (polBltGantt->GetItemHeight(ilItemID) != polBltGantt->GetLineHeight(ilItemID))
			{
	            polBltGantt->RepaintItemHeight(ilItemID);
			    PositionChild();
			}
			else
			{
	            polBltGantt->RepaintGanttChart(ilItemID);
			}
		}
        break;

        case UD_DELETELINE :
            polBltGantt->DeleteString(ilItemID);
            
			ilCount = omViewer.GetLineCount(ipGroupNo);
			sprintf(clBuf, "%d", ilCount);
			pomChart->GetCountTextPtr()->SetWindowText(clBuf);
			pomChart->GetCountTextPtr()->Invalidate(TRUE);

			//SetStaffAreaButtonColor(ipGroupNo);
			PositionChild();
        break;

        case UD_UPDATELINEHEIGHT :
            pomChart->omGantt.RepaintItemHeight(ilItemID);
			//SetStaffAreaButtonColor(ipGroupNo);
            PositionChild();
		break;
    }

    return 0L;
}
/*
void BltDiagram::OnUpdatePrevNext(void)
{
	// Id 25-Sep-96
	// This will set the keyboard focus back to the bottom most list-box after this button was clicked.
	//SetFocus();

	if (imFirstVisibleChart == 0)
		GetDlgItem(IDC_NEXT)->EnableWindow(FALSE);
	else
		GetDlgItem(IDC_NEXT)->EnableWindow(TRUE);

	if (imFirstVisibleChart == omPtrArray.GetUpperBound())
		GetDlgItem(IDC_PREV)->EnableWindow(FALSE);
	else
		GetDlgItem(IDC_PREV)->EnableWindow(TRUE);
}
*/

///////////////////////////////////////////////////////////////////////////////
// Damkerng and Pichet: Date: 5 July 1995

void BltDiagram::UpdateComboBox()
{
	CComboBox *polCB = (CComboBox *) omDialogBar.GetDlgItem(IDC_VIEW);
	polCB->ResetContent();
	CStringArray olStrArr;
	omViewer.GetViews(olStrArr);
	for (int ilIndex = 0; ilIndex < olStrArr.GetSize(); ilIndex++)
	{
		polCB->AddString(olStrArr[ilIndex]);
	}
	CString olViewName = omViewer.GetViewName();
	if (olViewName.IsEmpty() == TRUE)
	{

		olViewName = omViewer.SelectView();//ogCfgData.rmUserSetup.STCV;
	}

	ilIndex = polCB->FindString(-1,olViewName);
		
	if (ilIndex != CB_ERR)
	{
		polCB->SetCurSel(ilIndex);
	}
}

void BltDiagram::ChangeViewTo(const char *pcpViewName,bool RememberPositions)
{
	if (bgNoScroll == TRUE)
		return;

/*
    for (int ilIndex = 0; ilIndex < omPtrArray.GetSize(); ilIndex++)
    {
		if(!((BltChart *)omPtrArray[ilIndex])->omGantt.bmRepaint)
		{
			bmRepaintAll = true;
			return;
		}
    }
*/

	if(!pomChart->omGantt.bmRepaint)
	{
		bmRepaintAll = true;
		return;
	}


	bool blHasFocus = false;

	if (pomChart->GetGanttPtr() != NULL)
	{
		if(	pomChart->GetGanttPtr()->GetFocus() == pomChart->GetGanttPtr() )
			blHasFocus = true;

	}



	int ilDwRef = m_dwRef;
	AfxGetApp()->DoWaitCursor(1);
	bmNoUpdatesNow = TRUE;
	omViewer.AllowUpdates(bmNoUpdatesNow);

//	CCSPtrArray <int> olChartStates;
//	olChartStates.RemoveAll();
	int ilChartState;

	CRect olRect; omTimeScale.GetClientRect(&olRect);
    omViewer.ChangeViewTo(pcpViewName, omTSStartTime, omTimeScale.GetTimeFromX(olRect.Width() + 38));

	if (RememberPositions == TRUE)
	{
		ilChartState = pomChart->GetState();
	}
		// Id 30-Sep-96
		// This will remove a lot of warning message when the user change view.
		// If we just delete a staff chart, MFC will produce two warning message.
		// First, Revoke not called before the destructor.
		// Second, calling DestroyWindow() in CWnd::~CWnd.
        //delete (BltChart *)omPtrArray.GetAt(ilIndex);
	pomChart->DestroyWindow();

    

    CRect olPrevRect;
    omClientWnd.GetClientRect(&olRect);
    
	if (RememberPositions == FALSE)
	{
	    imFirstVisibleChart = 0;
		ilChartState = Maximized;
	}
    int ilLastY = 0;
    olRect.SetRect(olRect.left, ilLastY, olRect.right, ilLastY);
    
    pomChart = new BltChart;
    pomChart->SetTimeScale(&omTimeScale);
    pomChart->SetViewer(&omViewer);
    pomChart->SetStatusBar(&omStatusBar);
    pomChart->SetStartTime(omTSStartTime);
    pomChart->SetInterval(omTimeScale.GetDisplayDuration());
	pomChart->SetState(ilChartState);

	pomChart->Create(NULL, "BltChart", WS_OVERLAPPED | WS_BORDER | WS_CHILD | WS_VISIBLE,
            olRect, &omClientWnd,
            0 /* IDD_CHART */, NULL);

	CTime olCurr = CTime::GetCurrentTime();
	//Die wollen local
	if(!bgGatPosLocal)
		ogBasicData.LocalToUtc(olCurr);
	pomChart->GetGanttPtr()->SetCurrentTime(olCurr);

	PositionChild();

	bmNoUpdatesNow = FALSE;
	omViewer.AllowUpdates(bmNoUpdatesNow);


	if (pomBltKonflikteDlg)
		pomBltKonflikteDlg->ChangeViewTo();

	if (pomBltCcaTableDlg)
		pomBltCcaTableDlg->ReloadData(&ogPosDiaFlightData);

	UpdateWoResButton();

	AfxGetApp()->DoWaitCursor(-1);

	if (pomChart->GetGanttPtr() != NULL && blHasFocus)
 			pomChart->GetGanttPtr()->SetFocus();
}


void BltDiagram::UpdateWoResButton()
{
	CString olTmp;
	COLORREF olButtonColor;

	int ilCount = pogWoResTableDlg->GetCount(BLTDIA);
	if(ilCount > 0)
	{
		if(bgFwrBlt)
			olButtonColor = ogColors[IDX_ORANGE];
		else
			olButtonColor = ::GetSysColor(COLOR_BTNFACE);

		olTmp.Format("%s (%d)", GetString(IDS_STRING2444),ilCount);
	}
	else
	{
		olTmp.Format("%s (%d)", GetString(IDS_STRING2200),0);
		olButtonColor = ::GetSysColor(COLOR_BTNFACE);
	}

	m_CB_WoRes.SetWindowText(olTmp);
	m_CB_WoRes.SetColors(olButtonColor, ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
	m_CB_WoRes.UpdateWindow();
}

void BltDiagram::UpdateChangesButton(CString& opText, COLORREF opColor)
{
	CString olTmp;
	olTmp.Format("%s (%s)", GetString(IDS_CHANGES),opText);

	m_CB_Changes.SetWindowText(olTmp);
	m_CB_Changes.SetColors(opColor, ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
	m_CB_Changes.UpdateWindow();
}

void BltDiagram::OnViewSelChange()
{
    char clText[64];
    CComboBox *polCB = (CComboBox *) omDialogBar.GetDlgItem(IDC_VIEW);
    polCB->GetLBText(polCB->GetCurSel(), clText);
	if(strcmp(clText, "<Default>") == 0)
		return;

	if (!LoadFlights(clText))
		return;

	omViewer.SelectView(clText);
	omViewer.MakeMasstab();
	CTime olT = omViewer.GetGeometrieStartTime();
	CString olS = olT.Format("%d.%m.%Y-%H:%M");
	SetTSStartTime(olT);
	omTSDuration = omViewer.GetGeometryTimeSpan();

    
	long llTotalMin = omDuration.GetTotalMinutes() - omTSDuration.GetTotalMinutes();
	long nPos = (omTSStartTime - omStartTime).GetTotalMinutes();
	nPos = nPos * 1000L / llTotalMin;
	SetScrollPos(SB_HORZ, int(nPos), TRUE);
	
	
	
//rkr
	//Set begin to local if local
	CTime olTSStartTime = omTSStartTime;
	if(bgGatPosLocal)
		ogBasicData.UtcToLocal(olTSStartTime);
    
	omTimeScale.SetDisplayTimeFrame(olTSStartTime, omTSDuration, omTSInterval);
//	omTimeScale.SetDisplayTimeFrame(omTSStartTime, omTSDuration, omTSInterval);
    omTimeScale.Invalidate(TRUE);
	SetTSStartTime(omTSStartTime);

	ogDdx.DataChanged((void *)this, SAS_VIEW_CHANGED,(void *)clText );
	pogWoResTableDlg->Rebuild();

	ChangeViewTo(clText, false);
}

void BltDiagram::OnCloseupView() 
{
	// Id 25-Sep-96
	// This will set the keyboard focus back to the bottom most list-box after this button was clicked.
	//SetFocus();
}

void BltDiagram::OnZeit()
{
	// Id 25-Sep-96
	// This will set the keyboard focus back to the bottom most list-box after this button was clicked.
	//SetFocus();

	// TODO: Add your command handler code here
	CTime olTime = CTime::GetCurrentTime();
	
	if(!bgGatPosLocal)	
		ogBasicData.LocalToUtc(olTime);


	//Die wollen local
	olTime -= CTimeSpan(0, 1, 0, 0);
	SetTSStartTime(olTime);
	//TRACE("Time: [%s]\n", olTime.Format("%H:%M"));

	omTimeScale.SetDisplayStartTime(omTSStartTime);
    omTimeScale.Invalidate(TRUE);
		
	// update scroll bar position
    long llTotalMin = omDuration.GetTotalMinutes() - omTSDuration.GetTotalMinutes();
	long nPos = (omTSStartTime - omStartTime).GetTotalMinutes();
	nPos = nPos * 1000L / llTotalMin;
    SetScrollPos(SB_HORZ, int(nPos), TRUE);

	ChangeViewTo(omViewer.SelectView());//ogCfgData.rmUserSetup.STCV);
    omClientWnd.Invalidate(FALSE);
}


void BltDiagram::OnSearch()
{
	pogFlightSearchTableDlg->Activate(omViewer.GetBaseViewName(), omViewer.GetViewName(), BLTDIA);
}

void BltDiagram::OnChanges()
{
	CallHistory(CString("BLTBELT"));
}


BOOL BltDiagram::DestroyWindow() 
{

	if ((bmIsViewOpen))
	{
		MessageBeep((UINT)-1);
		return FALSE;    // don't destroy window while view property sheet is still open
	}
	pogBltDiagram = NULL; 

	ogDdx.UnRegister(&omViewer, NOTUSED);
	bmNoUpdatesNow = TRUE;
	omViewer.AllowUpdates(bmNoUpdatesNow);
	BOOL blRc = CWnd::DestroyWindow();
	return blRc;
}

/////////XXXXX
////////////////////////////////////////////////////////////////////////
// BltDiagram keyboard handling

void BltDiagram::OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags) 
{
	// check if the control key is pressed
    BOOL blIsControl = ::GetKeyState(VK_CONTROL) & 0x8080;
		// This statement has to be fixed for using both in Windows 3.11 and NT.
		// In Windows 3.11 the bit 0x80 will be turned on if control key is pressed.
		// In Windows NT, the bit 0x8000 will be turned on if control key is pressed.

	switch (nChar)
	{
	case VK_UP:		// move the bottom most gantt chart up/down one line
	case VK_DOWN:
			pomChart->GetGanttPtr()->SendMessage(WM_USERKEYDOWN, nChar);

/*		CListBox *polGantt;
		if ((polGantt = GetBottomMostGantt()) != NULL)
			polGantt->SendMessage(WM_USERKEYDOWN, nChar);
*/
		break;
/*
	case VK_PRIOR:
		blIsControl? OnFirstChart(): OnPrevChart();
		break;
	case VK_NEXT:
		blIsControl? OnLastChart(): OnNextChart();
		break;
	case VK_LEFT:
		OnHScroll(blIsControl? SB_PAGEUP: SB_LINEUP, 0, NULL);
		break;
	case VK_RIGHT:
		OnHScroll(blIsControl? SB_PAGEDOWN: SB_LINEDOWN, 0, NULL);
		break;
*/
	case VK_HOME:
		SetScrollPos(SB_HORZ, 0, FALSE);
		OnHScroll(SB_LINEUP, 0, NULL);
		break;
	case VK_END:
		SetScrollPos(SB_HORZ, 1000, FALSE);
		OnHScroll(SB_LINEDOWN, 0, NULL);
		break;
	default:
		omDialogBar.SendMessage(WM_KEYDOWN, nChar, MAKELONG(LOWORD(nRepCnt), LOWORD(nFlags)));
		CFrameWnd::OnKeyDown(nChar, nRepCnt, nFlags);
		break;
	}
}

void BltDiagram::OnKeyUp(UINT nChar, UINT nRepCnt, UINT nFlags) 
{
	CFrameWnd::OnKeyUp(nChar, nRepCnt, nFlags);
}

////////////////////////////////////////////////////////////////////////
// BltDiagram -- implementation of DDX call back function


void BltDiagram::RedisplayAll()
{
	ChangeViewTo(omViewer.SelectView());//ogCfgData.rmUserSetup.STCV);
}

void BltDiagram::SetTimeBand(CTime opStartTime, CTime opEndTime)
{
	omTimeBandStartTime = opStartTime;
	omTimeBandEndTime = opEndTime;
}

void BltDiagram::UpdateTimeBand()
{
	pomChart->omGantt.SetMarkTime(omTimeBandStartTime, omTimeBandEndTime);
/*
	for (int ilLc = imFirstVisibleChart; ilLc < omPtrArray.GetSize(); ilLc++)
	{
		BltChart *polChart = (BltChart *)omPtrArray[ilLc];
		polChart->omGantt.SetMarkTime(omTimeBandStartTime, omTimeBandEndTime);
	}
*/
}

LONG BltDiagram::RepaintAll(WPARAM wParam, LPARAM lParam)
{
	ChangeViewTo(omViewer.GetViewName(), true);
	return 0L;
}


void BltDiagram::OnOverview()
{
	if (pomBltOverviewTableDlg != NULL)
 		pomBltOverviewTableDlg->Activate();
}


void BltDiagram::OnCca()
{
	if (pomBltCcaTableDlg != NULL)
	{
 		pomBltCcaTableDlg->Activate();
	}
}



void BltDiagram::OnAttention()
{	
	if (pomBltKonflikteDlg != NULL)
		pomBltKonflikteDlg->Attention();
}

void BltDiagram::UpdateKonflikteDlg()
{	
	if (pomBltKonflikteDlg != NULL)
		//pomBltKonflikteDlg->UpdateDisplay();
		pomBltKonflikteDlg->ChangeViewTo();
}

 
void BltDiagram::OnOffline()
{	
	ogPosDiaFlightData.ToggleOffline();
}



void BltDiagram::OnWoRes()
{
	pogWoResTableDlg->Activate(BLTDIA);
}


void BltDiagram::OnActivate(UINT nState, CWnd* pWndOther, BOOL bMinimized) 
{
	CFrameWnd::OnActivate(nState, pWndOther, bMinimized);
	
	pogWoResTableDlg->Activate(BLTDIA, false);


}


void BltDiagram::ShowTime(CTime opStart)
{
	opStart -= CTimeSpan(0, 1, 0, 0);

	if(omTSStartTime == opStart)
		return;
	
	SetTSStartTime(opStart);

	omTimeScale.SetDisplayStartTime(omTSStartTime);
    omTimeScale.Invalidate(TRUE);
		
	// update scroll bar position
    long llTotalMin = omDuration.GetTotalMinutes() - omTSDuration.GetTotalMinutes();
	long nPos = (omTSStartTime - omStartTime).GetTotalMinutes();
	nPos = nPos * 1000L / llTotalMin;
    SetScrollPos(SB_HORZ, int(nPos), TRUE);

	// redraw gantt
 	CRect olRect; omTimeScale.GetClientRect(&olRect);
    omViewer.ChangeViewTo(omViewer.SelectView(), omTSStartTime, omTimeScale.GetTimeFromX(olRect.Width() + 38));
	PositionChild();
}




bool BltDiagram::ShowFlight(long lpUrno)
{
 	CTime olStart;
	CTime olEnd;


	DIAFLIGHTDATA *prlFlight = ogPosDiaFlightData.GetFlightByUrno(lpUrno);
	if (prlFlight == NULL)
		return false;

	if (omViewer.CalculateTimes(*prlFlight, olStart, olEnd))
	{		
		if(bgGatPosLocal) ogBasicData.UtcToLocal(olStart);
		if(bgGatPosLocal) ogBasicData.UtcToLocal(olEnd);


		ShowTime(olStart);
	
		SetMarkTime(olStart, olEnd);

	}	

	return true;
}




