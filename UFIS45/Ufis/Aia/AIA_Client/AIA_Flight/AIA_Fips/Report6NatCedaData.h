#ifndef _REPORT_D_NAT_CEDA_DATA_H_
#define _REPORT_D_NAT_CEDA_DATA_H_
 

#include "CCSGlobl.h"
#include "CCSDefines.h"




struct REPORT6NATDATA
{
	char	Ttyp[7];	// Referenz AFT.TTYPURNO <--> NAT.TTYP
	char	Tnam[32];	// Klartext	

	//DataCreated by this class
	int		IsChanged;

	REPORT6NATDATA(void)
	{ 
		memset(this,'\0',sizeof(*this));
	}

}; // end REPORT6NATDATA
	



/////////////////////////////////////////////////////////////////////////////
// Class declaration

/*@Doc:
  Reads and writes flight data from and to database. Stores flight data in memory 
  and provides methods for searching and retrieving flights from its internal list. 
  Does some data preparations like pre-formatting the flight number, storing flight 
  number and gate information for the corresponding inbound/outbound flight.

  Data of this class has to be updated by CEDA through broadcasts. This procedure 
  and the necessary methods will be described later with the description of 
  broadcast handling.

*/
class Report6NatCedaData : public CCSCedaData
{
// Attributes
public:
	char pcmAftFieldList[64];


// Operations
public:
    Report6NatCedaData();
	~Report6NatCedaData();
	

	CString ReadTnam(CString opTtyp);

private:
	bool bmReadAllFlights;
	bool bmRotation;

};


#endif
