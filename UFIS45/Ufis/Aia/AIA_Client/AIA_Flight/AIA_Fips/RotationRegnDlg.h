#if !defined(AFX_ROTATIONREGNDLG_H__9B659881_7774_11D1_8345_0080AD1DC701__INCLUDED_)
#define AFX_ROTATIONREGNDLG_H__9B659881_7774_11D1_8345_0080AD1DC701__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// RotationRegnDlg.h : header file
//
#include "CCSEdit.h"

/////////////////////////////////////////////////////////////////////////////
// RotationRegnDlg dialog

class RotationRegnDlg : public CDialog
{
// Construction
public:
	RotationRegnDlg(CWnd* pParent = NULL, CString opRegn = "", CString opAct3 = "", CString opAct5 = "");

	~RotationRegnDlg();
	
	void AppExit();

// Dialog Data
	//{{AFX_DATA(RotationRegnDlg)
	enum { IDD = IDD_ROTATION_REGN };
	CCSEdit	m_CE_Regn;
	CCSEdit	m_CE_Act5;
	CButton	m_CB_Act35List;
	CCSEdit	m_CE_Act3;
	CString	m_Act3;
	CString	m_Act5;
	CString	m_Regn;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(RotationRegnDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(RotationRegnDlg)
	virtual void OnOK();
	afx_msg void OnAct35list();
	afx_msg LONG OnEditKillfocus( UINT wParam, LPARAM lParam);
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_ROTATIONREGNDLG_H__9B659881_7774_11D1_8345_0080AD1DC701__INCLUDED_)
