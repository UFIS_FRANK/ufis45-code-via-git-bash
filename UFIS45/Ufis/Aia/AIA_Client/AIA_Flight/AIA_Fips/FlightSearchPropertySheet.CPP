// FlightSearchPropertySheet.cpp : implementation file
//

// These following include files are project dependent
#include "stdafx.h"
#include "FPMS.h"
#include "CCSGlobl.h"
#include "CCSCedadata.h"
  
// These following include files are necessary
#include "cviewer.h"
#include "BasicData.h"
#include "FlightSearchPropertySheet.h"
#include "StringConst.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


/////////////////////////////////////////////////////////////////////////////
// FlightSearchPropertySheet
//





FlightSearchPropertySheet::FlightSearchPropertySheet(CString opCalledFrom, CWnd* pParentWnd,
	CViewer *popViewer, UINT iSelectPage, LPCSTR pszCaption, bool bpLocalTimes) : 
	BasePropertySheet(pszCaption, pParentWnd, popViewer, iSelectPage),
	m_SearchFlightPage(bpLocalTimes)
{
		// TODO TVO Add UniFilter
	bmViews = false;
	AddPage(&m_SearchFlightPage);
	m_SearchFlightPage.SetCalledFrom(opCalledFrom);
	//AddPage(&m_Geometrie);
	//m_Geometrie.SetCalledFrom(opCalledFrom);
//	AddPage(&m_RulesPage);
	//AddPage(&m_PSUniFilter);
	//m_PSUniFilter.SetCalledFrom(opCalledFrom);
	//AddPage(&m_SpecialFilterPage);
	//AddPage(&m_PSUniSortPage);
	//m_PSUniSortPage.SetCalledFrom(opCalledFrom);
	//AddPage(&m_SpecialSortPage);
	//m_PSUniFilter.SetCaption("Allgem. Filter");
}
													 
													 
 
FlightSearchPropertySheet::~FlightSearchPropertySheet()
{
}




void FlightSearchPropertySheet::LoadDataFromViewer()
{
	//pomViewer->GetFilter("SPECFILTER", m_SpecialFilterPage.omValues);
	//pomViewer->GetFilter("UNIFILTER", m_PSUniFilter.omValues);
	//pomViewer->GetSort("FLIGHTSORT", m_SpecialSortPage.omSortOrders);
	//pomViewer->GetSort("UNISORT", m_PSUniSortPage.omValues);
	pomViewer->GetSearch("FLIGHTSEARCH", m_SearchFlightPage.omValues);
}

void FlightSearchPropertySheet::SaveDataToViewer(CString opViewName,BOOL bpSaveToDb)
{
	//m_PSUniFilter.UpdateData();
	//m_SpecialFilterPage.UpdateData();
	//SearchFlightPage.UpdateData();
	//m_PSUniSortPage.UpdateData();
	//m_SpecialSortPage.UpdateData();

	//MWO
	//pomViewer->SetFilter("SPECFILTER", m_SpecialFilterPage.omValues);
	//pomViewer->SetFilter("UNIFILTER", m_PSUniFilter.omValues);
	//pomViewer->SetSort("FLIGHTSORT", m_SpecialSortPage.omSortOrders);
	//pomViewer->SetSort("UNISORT", m_PSUniSortPage.omValues);
	pomViewer->SetSearch("FLIGHTSEARCH", m_SearchFlightPage.omValues);

 
}

  