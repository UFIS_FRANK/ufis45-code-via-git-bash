#if !defined(AFX_GanttChartReportWnd_H__004960BF_0881_11D3_AB42_00001C019D0B__INCLUDED_)
#define AFX_GanttChartReportWnd_H__004960BF_0881_11D3_AB42_00001C019D0B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// GanttChartReportWnd.h : header file
//
#include "CColor.h"
#include "CCSPtrArray.h"
#include "GanttChartReport.h"

/////////////////////////////////////////////////////////////////////////////
// GanttChartReportWnd window

class GanttBarReport;

class GanttChartReportWnd : public CWnd
{
// Construction
public:
	GanttChartReportWnd(CWnd* ppParent, CString opXAxe, CString opYAxe, CColor opColor);

	virtual ~GanttChartReportWnd();
// Attributes
public:
	void SetData(const CUIntArray &opData);
	void SetXAxeText(const CStringArray& opXAxeText);
	void SetBarText(const CStringArray& opBarText);
// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(GanttChartReportWnd)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	//}}AFX_VIRTUAL

// Implementation
public:
	void Print(CDC* popDC, CCSPrint* popPrint, CString opHeader, CString opFooterLeft);

	// Generated message map functions
protected:
	//{{AFX_MSG(GanttChartReportWnd)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_MSG

private:
//	CRect omRectWnd;
	GanttChartReport* pomGanttChartReport;
	CString omXAxe;
	CString omYAxe;
	CColor omColor;
	CStringArray omXAxeText;
	CStringArray omBarText;
	CUIntArray omData;


	DECLARE_MESSAGE_MAP()

};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_GanttChartReportWnd_H__004960BF_0881_11D3_AB42_00001C019D0B__INCLUDED_)
