// 

#include "stdafx.h"
#include "ccsglobl.h"
#include "BasicData.h"
#include "ReportXXXCedaData.h"





////////////////////////////////////////////////////////////////////////////
// Construktor   
//
ReportXXXCedaData::ReportXXXCedaData(char* pcpCol, char* pcpName, char* pcpTable, bool bpAll) 
                  :CCSCedaData(&ogCommHandler)
{

	BEGIN_CEDARECINFO(REPORTXXXDATA,AftDataRecInfo)
	// Referenz AFT.xxx <--> xxx
	CCS_FIELD_CHAR_TRIM(Select,*pcpCol,"Feld", 1)
	CCS_FIELD_CHAR_TRIM(Name,  *pcpName,  "Name", 1)

	END_CEDARECINFO //(AFTDATA)

	bmAll = bpAll;

	// Copy the record structure
	for (int i=0; i< sizeof(AftDataRecInfo)/sizeof(AftDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&AftDataRecInfo[i],sizeof(CEDARECINFO));
		omRecInfo.Add(prpCedaRecInfo);
	} // end for

	opName = pcpCol;//Name;

	// set Tablename
    strcpy(pcmTableName,pcpTable);

	CString olFieldList(pcpCol);
	olFieldList += ",";
	olFieldList += pcpName;
	 
	// initialize field names
	strcpy(pcmAftFieldList, olFieldList.GetBuffer(0));
	pcmFieldList = pcmAftFieldList;

	int ilrst = sizeof(REPORTXXXDATA);

	lmBaseID = IDM_ROTATION;

	omExtendedWhere = "";
}




////////////////////////////////////////////////////////////////////////////
// Destruktor   
//
ReportXXXCedaData::~ReportXXXCedaData(void)
{
	TRACE("ReportXXXCedaData::~ReportXXXCedaData called\n");
	ogDdx.UnRegister(this,NOTUSED);
	omRecInfo.DeleteAll();

}



void ReportXXXCedaData::SetExtendedWhereString(CString opWhere)
{

	omExtendedWhere = opWhere;

}


CString ReportXXXCedaData::ReadCleartext(CString opTtyp)
{

	CCS_TRY

	CString olColumn(opName);

	CString olSelection("WHERE ");
	olSelection += opName;
	olSelection += " = '";
	
	// Where Klausel vervollstaendigen
	olSelection += opTtyp;
	olSelection += "'";

	if (!omExtendedWhere.IsEmpty())
		olSelection += omExtendedWhere;

	                 // ReadTable                                       // Ergebnisbuffer
	bool blRet = CedaAction("RT", pcmTableName, pcmFieldList, olSelection.GetBuffer(0), "", pcgDataBuf,
		                    "BUF1");



	if (blRet && !bmAll)
	{
		REPORTXXXDATA prTtyp;
		if (GetFirstBufferRecord(&prTtyp))
			return prTtyp.Name;
	}

	bool blRc = blRet;

	blRc = blRet;
	for (int ilLc = 0; blRc == true; ilLc++)
	{
		REPORTXXXDATA *prTtyp = new REPORTXXXDATA;
		if ((blRc = GetFirstBufferRecord(prTtyp)) == true)
		{
			omSameFlnu.Add(prTtyp->Name);
		}
		else
		{
			delete prTtyp;
		}
	}



	CCS_CATCH_ALL

	return "";

}
