// 

#include "stdafx.h"
#include "ccsglobl.h"
#include "BasicData.h"
#include "Report6NatCedaData.h"





////////////////////////////////////////////////////////////////////////////
// Construktor   
//
Report6NatCedaData::Report6NatCedaData() : CCSCedaData(&ogCommHandler)
{

	BEGIN_CEDARECINFO(REPORT6NATDATA,AftDataRecInfo)

	CCS_FIELD_CHAR_TRIM(Ttyp,"TTYP","Verkehrsart", 1)
	CCS_FIELD_CHAR_TRIM(Tnam,"TNAM","Verkehrsart Name", 1)

	END_CEDARECINFO //(AFTDATA)

	// Copy the record structure
	for (int i=0; i< sizeof(AftDataRecInfo)/sizeof(AftDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&AftDataRecInfo[i],sizeof(CEDARECINFO));
		omRecInfo.Add(prpCedaRecInfo);
	} // end for


	// set Tablename
    strcpy(pcmTableName,"NAT");
	// initialize field names
	strcpy(pcmAftFieldList,"TTYP,TNAM");
	pcmFieldList = pcmAftFieldList;

	int ilrst = sizeof(REPORT6NATDATA);

	lmBaseID = IDM_ROTATION;


}




////////////////////////////////////////////////////////////////////////////
// Destruktor   
//
Report6NatCedaData::~Report6NatCedaData(void)
{
	TRACE("Report6NatCedaData::~Report6NatCedaData called\n");
	ogDdx.UnRegister(this,NOTUSED);
	omRecInfo.DeleteAll();

}




CString Report6NatCedaData::ReadTnam(CString opTtyp)
{

	CCS_TRY

	//select ttyp,tnam from nattab where ttyp='43';
	CString olSelection("WHERE TTYP ='");
	
	// Where Klausel vervollstaendigen
	olSelection += opTtyp;
	olSelection += "'";


	                 // ReadTable                                       // Ergebnisbuffer
	bool blRet = CedaAction("RT", pcmTableName, pcmFieldList, olSelection.GetBuffer(0), "", pcgDataBuf,
		                    "BUF1");
	if (blRet)
	{
		REPORT6NATDATA prTtyp;
		if (GetFirstBufferRecord(&prTtyp))
			return prTtyp.Tnam;
	}


	CCS_CATCH_ALL

	return "";

}
