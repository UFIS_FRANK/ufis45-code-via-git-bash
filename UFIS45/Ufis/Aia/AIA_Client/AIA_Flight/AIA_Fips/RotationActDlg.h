#if !defined(AFX_ROTATIONACTDLG_H__F562F070_7882_11D1_8347_0080AD1DC701__INCLUDED_)
#define AFX_ROTATIONACTDLG_H__F562F070_7882_11D1_8347_0080AD1DC701__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// RotationActDlg.h : header file
//

#include "CCSEdit.h"

/////////////////////////////////////////////////////////////////////////////
// RotationActDlg dialog

class RotationActDlg : public CDialog
{
// Construction
public:
	RotationActDlg(CWnd* pParent = NULL, CString opAct = "");   // standard constructor

	~RotationActDlg();

	void AppExit();	

// Dialog Data
	//{{AFX_DATA(RotationActDlg)
	enum { IDD = IDD_ROTATION_ACT };
	CCSEdit	m_CE_Acfn;
	CCSEdit	m_CE_Act5;
	CCSEdit	m_CE_Act3;
	CString	m_Act3;
	CString	m_Act5;
	CString	m_Acfn;
	//}}AFX_DATA

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(RotationActDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(RotationActDlg)
	virtual void OnOK();
	afx_msg LONG OnEditKillfocus( UINT wParam, LPARAM lParam);
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_ROTATIONACTDLG_H__F562F070_7882_11D1_8347_0080AD1DC701__INCLUDED_)
