#ifndef AFX_ALTIMPORTDELDLG_H__05FA7BF1_6187_11D1_80F8_0000B43C4B01__INCLUDED_
#define AFX_ALTIMPORTDELDLG_H__05FA7BF1_6187_11D1_80F8_0000B43C4B01__INCLUDED_

// AltImportDelDlg.h : Header-Datei
//

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld CAltImportDelDlg 

class CAltImportDelDlg : public CDialog
{
// Konstruktion
public:
	CAltImportDelDlg(CWnd* pParent = NULL, CTime* oMinDate = NULL, CTime* oMaxDate = NULL, int* piRet= NULL);   // Standardkonstruktor

	CTime *pomMinDate;
	CTime *pomMaxDate;
	int *pimRet;
// Dialogfelddaten
	//{{AFX_DATA(CAltImportDelDlg)
	enum { IDD = IDD_ALTIMPORT };
	CEdit	m_CE_Datumvon;
	CEdit	m_CE_Datumbis;
	CString	m_Datumbis;
	CString	m_Datumvon;
	//}}AFX_DATA


// Überschreibungen
	// Vom Klassen-Assistenten generierte virtuelle Funktionsüberschreibungen
	//{{AFX_VIRTUAL(CAltImportDelDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	//}}AFX_VIRTUAL

// Implementierung
protected:

	// Generierte Nachrichtenzuordnungsfunktionen
	//{{AFX_MSG(CAltImportDelDlg)
	virtual void OnOK();
	virtual void OnCancel();
	virtual BOOL OnInitDialog();
	afx_msg void OnOnlygen();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio fügt zusätzliche Deklarationen unmittelbar vor der vorhergehenden Zeile ein.

#endif // AFX_ALTIMPORTDELDLG_H__05FA7BF1_6187_11D1_80F8_0000B43C4B01__INCLUDED_
