#if !defined(AFX_ROTATIONALTDLG_H__F562F072_7882_11D1_8347_0080AD1DC701__INCLUDED_)
#define AFX_ROTATIONALTDLG_H__F562F072_7882_11D1_8347_0080AD1DC701__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// RotationAltDlg.h : header file
//


#include "CCSEdit.h"

/////////////////////////////////////////////////////////////////////////////
// RotationAltDlg dialog

class RotationAltDlg : public CDialog
{
// Construction
public:
	RotationAltDlg(CWnd* pParent = NULL, CString opAlc = "");   // standard constructor

	~RotationAltDlg();

	void AppExit();

// Dialog Data
	//{{AFX_DATA(RotationAltDlg)
	enum { IDD = IDD_ROTATION_ALT };
	CCSEdit	m_CE_Alfn;
	CCSEdit	m_CE_Alc3;
	CCSEdit	m_CE_Alc2;
	CString	m_Alc2;
	CString	m_Alc3;
	CString	m_Alfn;
	//}}AFX_DATA



// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(RotationAltDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(RotationAltDlg)
	virtual void OnOK();
	afx_msg LONG OnEditKillfocus( UINT wParam, LPARAM lParam);
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_ROTATIONALTDLG_H__F562F072_7882_11D1_8347_0080AD1DC701__INCLUDED_)
