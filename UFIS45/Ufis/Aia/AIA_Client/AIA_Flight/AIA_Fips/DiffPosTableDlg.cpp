// DiffPosTableDlg.cpp : implementation file
//
// Modification History: 

#include "stdafx.h"
#include "fpms.h"
#include "resrc1.h"
#include "DiffPosTableDlg.h"
#include "SeasonDlg.h"
#include "RotationDlg.h"
#include "DiaCedaFlightData.h"

#include "PosDiagram.h"
#include "Utils.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// DiffPosTableDlg dialog


DiffPosTableDlg::DiffPosTableDlg(CWnd* pParent /*=NULL*/)
	: CDialog(DiffPosTableDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(DiffPosTableDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	bmCreated = false;
	pomParent = pParent;
	pomTable = NULL;
    CDialog::Create(DiffPosTableDlg::IDD, NULL);
	m_key = "DialogPosition\\PositionCheck";
	bmCreated = true;
}


DiffPosTableDlg::~DiffPosTableDlg()
{
	pomViewer->UnRegister();

	delete pomTable;
	pomTable = NULL;
	delete pomViewer;
}


void DiffPosTableDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(DiffPosTableDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(DiffPosTableDlg, CDialog)
	//{{AFX_MSG_MAP(DiffPosTableDlg)
	ON_WM_CLOSE()
    ON_MESSAGE(WM_TABLE_LBUTTONDBLCLK, OnTableLButtonDblclk)
    ON_MESSAGE(WM_TABLE_RETURN_PRESSED, OnTableReturnPressed)
	ON_WM_SIZE()
 	ON_WM_DESTROY()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// DiffPosTableDlg message handlers

void DiffPosTableDlg::SaveToReg()
{
	BOOL ok = WriteDialogToReg((CWnd*) this, m_key);
}


void DiffPosTableDlg::OnDestroy() 
{
	SaveToReg();
	bmCreated = false;
	CDialog::OnDestroy();
	pomTable = NULL;
}

void DiffPosTableDlg::OnSize(UINT nType, int cx, int cy) 
{
	if(bmCreated != false)
	{
		CDialog::OnSize(nType, cx, cy);
		m_resizeHelper.OnSize();
	
		if (nType != SIZE_MINIMIZED)
		{
			if (pomTable)
			{
				CRect olRect;
			    GetClientRect(&olRect);
			    olRect.InflateRect(1, 1);     // hiding the CTable window border
				pomTable->SetPosition(olRect.left, olRect.right, olRect.top, olRect.bottom);
				Rebuild();
			}
		}
	}
}	

void DiffPosTableDlg::Rebuild()
{
	pomViewer->ChangeViewTo();

	UpdateCaption();
}


int DiffPosTableDlg::GetCount() const
{
	return pomViewer->GetCount();
}

void DiffPosTableDlg::UpdateCaption()
{
	CString olGantt;

	//// set caption
	CString olCaption;
	olCaption.Format("%s   (%d)", GetString(IDS_STRING2163), pomViewer->GetCount());
	// additional text for time mode
	CString olTimes;
	if (bgGatPosLocal)
		olTimes = GetString(IDS_STRING1921);
	else
		olTimes = GetString(IDS_STRING1920);
	olCaption += " ("+olTimes+")";
	SetWindowText(olCaption);
	
}



BOOL DiffPosTableDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	m_key = "DialogPosition\\PositionCheck";
	
	SetWindowPos(pomParent, 0,0,0,0, SWP_NOSIZE  | SWP_NOMOVE);

	pomViewer = new DiffPosTableViewer(this);

	pomTable = new CCSTable;
	pomTable->SetSelectMode(LBS_MULTIPLESEL | LBS_EXTENDEDSEL);
	pomTable->SetHeaderSpacing(0);

	bmCreated = true;
	
	CRect olRect;
    GetClientRect(&olRect);

	//olRect.top = olRect.top + imDialogBarHeight;
	//olRect.bottom = olRect.bottom;// - imDialogBarHeight;

    olRect.InflateRect(1, 1);     // hiding the CTable window border
    
	pomTable->SetTableData(this, olRect.left, olRect.right, olRect.top, olRect.bottom);

	pomViewer->Attach(pomTable);

	pomViewer->ChangeViewTo();

	UpdateCaption();

	m_resizeHelper.Init(this->m_hWnd);

	this->GetWindowRect(&olRect);
	GetDialogFromReg(olRect, m_key);
	SetWindowPos(&wndTop, olRect.left, olRect.top, olRect.Width(), olRect.Height(), SWP_HIDEWINDOW);// | SWP_NOMOVE );

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void DiffPosTableDlg::OnClose() 
{
	ShowWindow(SW_HIDE);	

	//pomViewer->UnRegister();

	//CDialog::OnClose();
}


////////////////////////////////////////////////////////////////////////////
// linke Maustaste auf der Flighttable gedr�ckt
//
LONG DiffPosTableDlg::OnTableLButtonDblclk( UINT wParam, LPARAM lParam)
{
	UINT ipItem = wParam;
	DIFFPOSTABLE_LINEDATA *prlTableLine = NULL;
	prlTableLine = (DIFFPOSTABLE_LINEDATA *)pomTable->GetTextLineData(ipItem);
	if (prlTableLine != NULL)
	{
		// get flight data
		const DIAFLIGHTDATA *prlFlight = NULL;
		char clAdid=' ';
		if (prlTableLine->AUrno != 0)
		{
			prlFlight = ogPosDiaFlightData.GetFlightByUrno(prlTableLine->AUrno);
			clAdid = 'A';
		}
		if (prlFlight==NULL && prlTableLine->DUrno != 0) 
		{
			prlFlight = ogPosDiaFlightData.GetFlightByUrno(prlTableLine->DUrno);
			clAdid = 'D';
		}
		

		if(prlFlight != NULL)
		{		
			if (ogPosDiaFlightData.bmOffLine)
			{
 				pogSeasonDlg->NewData(this, prlFlight, bgGatPosLocal);
			}
			else
			{
	 			if (clAdid == 'A')
				{
					CFPMSApp::ShowFlightRecordData(this, prlFlight->Ftyp[0], prlFlight->Urno, prlFlight->Rkey, clAdid, prlFlight->Tifa, bgGatPosLocal);
				}
				else
				{
					CFPMSApp::ShowFlightRecordData(this, prlFlight->Ftyp[0], prlFlight->Urno, prlFlight->Rkey, clAdid, prlFlight->Tifd, bgGatPosLocal);
				}
			}
		}
	}
	return 0L;
}



LONG DiffPosTableDlg::OnTableReturnPressed( UINT wParam, LPARAM lParam)
{
	OnTableLButtonDblclk(wParam, lParam); 
	return 0L;
}
