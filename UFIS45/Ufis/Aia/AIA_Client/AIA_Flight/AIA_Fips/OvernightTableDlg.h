#if !defined(AFX_OVERNIGHTTABLEDLG_H__C7CD8831_241F_11D1_82D3_0080AD1DC701__INCLUDED_)
#define AFX_OVERNIGHTTABLEDLG_H__C7CD8831_241F_11D1_82D3_0080AD1DC701__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// FlightTableDlg.h : header file
//
 
#include "CCSTable.h"
#include "OvernightViewer.h"
#include "OvernightTableDlg.h"
#include "resrc1.h"
#include "DlgResizeHelper.h"

/////////////////////////////////////////////////////////////////////////////
// CSeasonTableDlg dialog

class OvernightTableDlg : public CDialog
{
// Construction
public:
	OvernightTableDlg(CWnd* pParent = NULL);   // standard constructor
	~OvernightTableDlg();

// Dialog Data
	//{{AFX_DATA(OvernightTableDlg)
	enum { IDD = IDD_OVERNIGHTTABLE };
 	CButton	m_CB_Drucken;
	CButton	m_CB_Excel;
	CButton	m_CB_Search;
	CCSEdit	m_CE_From;
	CString	m_From;
	CCSEdit	m_CE_To;
	CString	m_To;
	CString	m_days;
	CCSEdit	m_CE_days;
	//}}AFX_DATA


//attributes
public:

	int imDialogBarHeight;

	CWnd *pomParent;
	CCSTable *pomSeasonTable;
	SeasonCedaFlightData* omSeasonFlights;
	
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(OvernightTableDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation

public:

	void UpdateView();
	void SelectFlight(long lpUrno);
	void SaveToReg();



private:
	OvernightTableViewer omViewer;
	// Generated message map functions
	//{{AFX_MSG(OvernightTableDlg)
	virtual BOOL OnInitDialog();
    afx_msg LONG OnTableLButtonDblclk(UINT wParam, LONG lParam);
	afx_msg void OnExcel();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnPrint();
	afx_msg void OnClose();
//	virtual void OnCancel();
    afx_msg LONG OnTableReturnPressed(UINT wParam, LONG lParam);
	afx_msg void OnSearch();
	afx_msg void OnDestroy();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

	DlgResizeHelper m_resizeHelper;
	CString m_key;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_OVERNIGHTTABLEDLG_H__C7CD8831_241F_11D1_82D3_0080AD1DC701__INCLUDED_)
