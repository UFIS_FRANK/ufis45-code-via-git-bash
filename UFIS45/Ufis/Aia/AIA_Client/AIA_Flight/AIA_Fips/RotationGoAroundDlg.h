#ifndef AFX_ROTATIONGOAROUNDDLG_H__8BBC00F3_8B6C_11D1_8126_0000B43C4B01__INCLUDED_
#define AFX_ROTATIONGOAROUNDDLG_H__8BBC00F3_8B6C_11D1_8126_0000B43C4B01__INCLUDED_

// ReportSeqDlg.h : Header-Datei
//
#include "CCSEdit.h"
#include "resrc1.h"

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld RotationGoAroundDlg 

class RotationGoAroundDlg : public CDialog
{
// Konstruktion
public:
	RotationGoAroundDlg(CWnd* pParent = NULL, CString& opHeadline = CString(""),
						CTime* opTime = NULL, CString& opPrompt = CString(""),
						CString& opField = CString(""), CTime opRefTime = TIMENULL);   

// Dialogfelddaten
	//{{AFX_DATA(RotationGoAroundDlg)
	enum { IDD = IDD_ROT_GOAROUND };
	CCSEdit	m_CE_Time;
	CString	m_Time;
	//}}AFX_DATA

	CString omHeadline;
	CString omField;
	CString omPrompt;
	CTime*  omTime;
	CTime   omRefTime;

// Überschreibungen
	// Vom Klassen-Assistenten generierte virtuelle Funktionsüberschreibungen
	//{{AFX_VIRTUAL(RotationGoAroundDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	//}}AFX_VIRTUAL

// Implementierung
protected:

	// Generierte Nachrichtenzuordnungsfunktionen
	//{{AFX_MSG(RotationGoAroundDlg)
	virtual void OnOK();
	virtual void OnCancel();
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio fügt zusätzliche Deklarationen unmittelbar vor der vorhergehenden Zeile ein.

#endif // AFX_ROTATIONGOAROUNDDLG_H__8BBC00F3_8B6C_11D1_8126_0000B43C4B01__INCLUDED_
