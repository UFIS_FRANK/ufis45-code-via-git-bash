#ifndef __GEFUNDENEFLUEGETABLEVIEWER_H__
#define __GEFUNDENEFLUEGETABLEVIEWER_H__

#include "stdafx.h"
#include "RotationDlgCedaFlightData.h"
#include "CCSTable.h"
#include "CViewer.h"
#include "CCSPrint.h"


struct GEFUNDENEFLUEGETABLE_LINEDATA
{
	long AUrno;
	long ARkey;
	CString		AFlno;
	CString		ASeas;
	CTime		AOnbl;
	CTime		AStoa;
	CString		ADate;
	CString		ATtyp;
	CString		AOrg3;
	CString		AVia3;
	CString		AAct;

	long DUrno;
	long DRkey;
	CString		DFlno;
	CString		DSeas;
	CTime		DOfbl;
	CTime		DStod;
	CString		DDate;
	CString		DTtyp;
	CString		DDes3;
	CString		DVia3;
	CString		DAct;

	bool		Canceled;
};

/////////////////////////////////////////////////////////////////////////////
// GefundenefluegeTableViewer

class GefundenefluegeTableViewer : public CViewer
{
// Constructions
public:
    GefundenefluegeTableViewer(CCSPtrArray<ROTATIONDLGFLIGHTDATA> *popData, char *pcpInfo = NULL, char *pcpSelect = NULL);
    ~GefundenefluegeTableViewer();

    void Attach(CCSTable *popAttachWnd);
    virtual void ChangeViewTo(const char *pcpViewName);
	void PrintPlanToFile(char *pcpDefPath);
// Internal data processing routines
private:
    void MakeLines();
	int  MakeLine(ROTATIONDLGFLIGHTDATA *prpAFlight, ROTATIONDLGFLIGHTDATA *prpDFlight);
	void MakeLineData(ROTATIONDLGFLIGHTDATA *prpAFlight, ROTATIONDLGFLIGHTDATA *prpDFlight, GEFUNDENEFLUEGETABLE_LINEDATA &rpLine);
	void MakeColList(GEFUNDENEFLUEGETABLE_LINEDATA *prlLine, CCSPtrArray<TABLE_COLUMN> &olColList);
	int  CompareGefundenefluege(GEFUNDENEFLUEGETABLE_LINEDATA *prpGefundenefluege1, GEFUNDENEFLUEGETABLE_LINEDATA *prpGefundenefluege2);
	int  CompareFlight(GEFUNDENEFLUEGETABLE_LINEDATA *prpFlight1, GEFUNDENEFLUEGETABLE_LINEDATA *prpFlight2);
	int  GetVia(CCSPtrArray<VIADATA> *opVias, ROTATIONDLGFLIGHTDATA *prpFlight);

// Operations
public:
	void DeleteAll();
	int CreateLine(GEFUNDENEFLUEGETABLE_LINEDATA &prpGefundenefluege);
	void DeleteLine(int ipLineno);
// Window refreshing routines
public:
	void UpdateDisplay();
	void DrawHeader();


// Attributes used for filtering condition
private:
	bool bmIsFromSearch;
// Attributes
private:
    CCSTable *pomTable;
	CCSPtrArray<ROTATIONDLGFLIGHTDATA> *pomData;
public:
    CCSPtrArray<GEFUNDENEFLUEGETABLE_LINEDATA> omLines;
	CCSPtrArray<TABLE_HEADER_COLUMN> omHeaderDataArray;
//Print 
	void GetHeader(void); 
	void PrintTableView(void);
	bool PrintTableLine(GEFUNDENEFLUEGETABLE_LINEDATA *prpLine,bool bpLastLine);
	bool PrintTableHeader(void);
	CCSPtrArray <TABLE_HEADER_COLUMN> omPrintHeadHeaderArray;
	CCSPrint *pomPrint;
	CString omTableName;
	char *pcmInfo;
	char *pcmSelect;
	CString omFooterName;
};

#endif //__GEFUNDENEFLUEGETABLEVIEWER_H__
