// CollectHAIDlg.cpp : implementation file
//

#include "stdafx.h"
#include "CCSGlobl.h"
#include "SeasonCollectDlg.h"
#include "CedaBasicData.h"
#include "AwBasicDataDlg.h"

#include "CollectHAIDlg.h"

#include "resource.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


 

/////////////////////////////////////////////////////////////////////////////
// CollectHAIDlg dialog


CollectHAIDlg::CollectHAIDlg(CWnd* pParent)
	           :CDialog(CollectHAIDlg::IDD, pParent),
			    pomParent(pParent)
{
	//{{AFX_DATA_INIT(CollectHAIDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT

	// Nonmodal dialog
	CDialog::Create(CollectHAIDlg::IDD, NULL);

	pomParent = pParent;
 
}


CollectHAIDlg::~CollectHAIDlg()
{

	delete pomTable;

}


void CollectHAIDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CollectHAIDlg)
	DDX_Control(pDX, IDC_GMBORDER, m_CS_GMBorder);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CollectHAIDlg, CDialog)
	//{{AFX_MSG_MAP(CollectHAIDlg)
	ON_BN_CLICKED(IDC_CLOSE, OnClose)
	ON_BN_CLICKED(IDC_CLEAR, OnClear)
	ON_MESSAGE(WM_TABLE_IPEDIT_KILLFOCUS, OnTableIPEditKillfocus)
    ON_MESSAGE(WM_TABLE_LBUTTONDOWN, OnTableLButtonDown)
    ON_MESSAGE(WM_TABLE_RBUTTONDOWN, OnTableRButtonDown)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CollectHAIDlg message handlers
BOOL CollectHAIDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
		
	InitTable();
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}


void CollectHAIDlg::InitTable()
{
 
	CRect olRectBorder;
	m_CS_GMBorder.GetWindowRect( olRectBorder );
	ScreenToClient(olRectBorder);
	olRectBorder.DeflateRect(2,2);

    pomTable = new CCSTable;
	pomTable->SetSelectMode(0);        // Selection will be set manually
 	pomTable->SetHeaderSpacing(0);
	pomTable->SetMiniTable();
	//pomTable->SetIPEditModus(true);
    pomTable->SetTableData(this, olRectBorder.left, olRectBorder.right, olRectBorder.top, olRectBorder.bottom);
	pomTable->SetShowSelection(false); // Selection will be set manually
	pomTable->ResetContent();

 	pomTable->SetDefaultSeparator();
	pomTable->SetTableEditable(true);
 

	DrawHeader();

	InsertEmptyLines();
 
	pomTable->DisplayTable();

 	
}


void CollectHAIDlg::DrawHeader()
{

	// Define Header because column widths
	CCSPtrArray <TABLE_HEADER_COLUMN> omHeaderDataArray;
	TABLE_HEADER_COLUMN rlHeader;

 	rlHeader.Alignment = COLALIGN_CENTER;
	rlHeader.Font = &ogCourier_Bold_10;

	rlHeader.Length = 20; 
	rlHeader.Text = "";
	omHeaderDataArray.New(rlHeader);
	
	rlHeader.Length = 44; 
	rlHeader.Text = GetString(IDS_STRING1743);// HSNA
	omHeaderDataArray.New(rlHeader);

 	rlHeader.Length = 220; 
 	rlHeader.Text = GetString(IDS_STRING1742);//TASK
	omHeaderDataArray.New(rlHeader);
 
 	rlHeader.Length = 387; 
 	rlHeader.Text = GetString(IDS_STRING1744);//REMA
	omHeaderDataArray.New(rlHeader);
 
	pomTable->SetHeaderFields(omHeaderDataArray);
	omHeaderDataArray.DeleteAll();

	pomTable->SetDefaultSeparator();


	// Insert Header as table line, because minitable suppress header
	TABLE_COLUMN rlColumnData;
	rlColumnData.Font = &ogMS_Sans_Serif_8;
	rlColumnData.Alignment = COLALIGN_LEFT;

 	CCSPtrArray<TABLE_COLUMN> olColList;

 	rlColumnData.blIsEditable = false;	
	rlColumnData.BkColor = RGB(192,192,192);

	rlColumnData.Text = "";
	olColList.NewAt(olColList.GetSize(), rlColumnData);

 	rlColumnData.Text = GetString(IDS_STRING1743);// HSNA
 	olColList.NewAt(olColList.GetSize(), rlColumnData);

	rlColumnData.Text = GetString(IDS_STRING1742);//TASK
 	olColList.NewAt(olColList.GetSize(), rlColumnData);

	rlColumnData.Text = GetString(IDS_STRING1744);//REMA
 	olColList.NewAt(olColList.GetSize(), rlColumnData);

	pomTable->AddTextLine(olColList, (void*)(NULL));
	olColList.DeleteAll();
  
}


void CollectHAIDlg::InsertEmptyLines()
{
	CCSEDIT_ATTRIB rlAttribC1;
	CCSEDIT_ATTRIB rlAttribC2;
	CCSEDIT_ATTRIB rlAttribC3;

	rlAttribC1.Format = "XXXXX";
	rlAttribC1.TextMinLenght = 1;
	rlAttribC1.TextMaxLenght = 5;
	rlAttribC1.Style = ES_UPPERCASE;

	rlAttribC2.Format = "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX";
	rlAttribC2.TextMinLenght = 0;
	rlAttribC2.TextMaxLenght = 127;

	rlAttribC3.Format = "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX";
	rlAttribC3.TextMinLenght = 0;
	rlAttribC3.TextMaxLenght = 254;

	TABLE_COLUMN rlColumnData;
	rlColumnData.Font = &ogMS_Sans_Serif_8;
	rlColumnData.Alignment = COLALIGN_LEFT;

	char buffer[64];
	CCSPtrArray<TABLE_COLUMN> olColList;

	// Insert 20 empty lines for editing
	for (int ilLc = 1; ilLc <= 20; ilLc++)
	{
		itoa(ilLc, buffer, 10);
		rlColumnData.blIsEditable = false;	
		rlColumnData.Text = CString(buffer);
		rlColumnData.BkColor = RGB(192,192,192);
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		rlColumnData.blIsEditable = true;	
		rlColumnData.Text = "";
		rlColumnData.EditAttrib = rlAttribC1;
		rlColumnData.BkColor = RGB(255,255,255);
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		rlColumnData.EditAttrib = rlAttribC2;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		rlColumnData.EditAttrib = rlAttribC3;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		pomTable->AddTextLine(olColList, (void*)(NULL));
		olColList.DeleteAll();
	}


}


// Get Status of the table
CString CollectHAIDlg::GetStatus() const
{
	CString olRet;
	CString olTmp;
 
	int ilLines = pomTable->GetLinesCount();

	// Scan each cell of the table
	for(int  i = 0; i < ilLines; i++)
	{
		for (int j = 1; j < COLLECTHAIDLG_COLCOUNT; j++)
		{
			if(pomTable->GetTextFieldValue(i, j, olTmp))
			{
				if(!olTmp.IsEmpty() && !pomTable->GetCellStatus(i, j))
				{
					// Table cell status is false
					return GetString(IDS_STRING558); // "Agents"
				}
			}
		}
	}

	return olRet;
}
								 

// Are lines selected?
bool CollectHAIDlg::LinesSelected() const
{
	COLORREF olColor;
	int ilLines = pomTable->GetLinesCount();

	for(int  i = 1; i < ilLines; i++)
	{
		if(pomTable->GetLineColumnBkColor(i,  0, olColor))
		{
 			if (olColor == RGB(0,0,255))
			{
				return true;
			}
		}
	}
	
	return false;
}



bool CollectHAIDlg::IsLineSelected(int ipLineNo) const
{
	COLORREF olColor;
	if(pomTable->GetLineColumnBkColor(ipLineNo,  0, olColor))
	{
		if (olColor == RGB(0,0,255))
		{
			return true;
		}
	}
	return false;
}





void CollectHAIDlg::OnClose() 
{

 	CCS_TRY

	OnCancel();

	CCS_CATCH_ALL
	
}


void CollectHAIDlg::OnCancel()
{
	// Just hide window
	ShowWindow(SW_HIDE);
}


void CollectHAIDlg::OnClear()
{
	Clear();
}


// clear table
void CollectHAIDlg::Clear()
{
	pomTable->ResetContent();
	DrawHeader();
	InsertEmptyLines();
	pomTable->DisplayTable();
}


LONG CollectHAIDlg::OnTableIPEditKillfocus( UINT wParam, LPARAM lParam)
{

	CCSIPEDITNOTIFY *prlNotify = (CCSIPEDITNOTIFY*)lParam;
	if (prlNotify->Column == 1 && wParam > 0)
	{	
		CString olValue = prlNotify->Text;
		CString olTmp;
	
		// Check if HSNA-value is in basic data
 		bool blRet = ogBCD.GetField("HAG", "HSNA", olValue, "HSNA", olTmp);

		if(olValue.IsEmpty())
			blRet = true;

		if(blRet == false)
			prlNotify->UserStatus = true;

		prlNotify->Status = blRet;
	}

	return 0L;
}


LONG CollectHAIDlg::OnTableLButtonDown( UINT wParam, LPARAM lParam)
{
	COLORREF olColor;
	CCSTABLENOTIFY *prlNotify = (CCSTABLENOTIFY *) lParam;

	// Selection only if the first column is clicked
	if (wParam > 0 && prlNotify->Column < COLLECTHAIDLG_COLCOUNT)
	{
		// Set changed-flag of batch processing window
		if(pogSeasonCollectDlg != NULL)
		{
	 		pogSeasonCollectDlg->bmChanged = true; 
			pogSeasonCollectDlg->m_CB_OK.SetColors(RGB(255,0,0), ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
		}

		// Select or deselect table line
		if(pomTable->GetLineColumnBkColor(wParam,  0, olColor))
		{
			if(olColor != RGB(0,0,255))
			{
				pomTable->SetTextLineColor(wParam, RGB(255,255,255), RGB(0,0,255));
			}
			else
			{
				pomTable->SetTextLineColor(wParam, RGB(0,0,0), RGB(255,255,255));
				pomTable->ChangeColumnColor(wParam, 0, RGB(192,192,192), RGB(0,0,0));
			}
		}

	}
	
 	return -1L;
}			

 

LONG CollectHAIDlg::OnTableRButtonDown( UINT wParam, LPARAM lParam)
{

	CCSTABLENOTIFY *prlNotify = (CCSTABLENOTIFY *) lParam;

	// Basic-data window only for HSNA
	if(prlNotify->Column != 1 || prlNotify->Line < 1)
		return 0L;

	// Show basic-data window of the handling agents
	AwBasicDataDlg *polDlg = new AwBasicDataDlg(this, "HAG","HSNA,HNAM", "HSNA+");

 	if(polDlg->DoModal() == IDOK)
	{
		// Insert selected handling agent in table cell 
		pomTable->SetIPValue(prlNotify->Line, prlNotify->Column, polDlg->GetField("HSNA"));
		pomTable->DisplayTable();
	}
	delete polDlg;

	return 0L;
}




