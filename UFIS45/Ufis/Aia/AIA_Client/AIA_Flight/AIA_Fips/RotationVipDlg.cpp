// RotationVipDlg.cpp: Implementierungsdatei
//
// Modification History: 
// 22-nov-00	rkr		Status for dialog supported

#include "stdafx.h"
#include "fpms.h"
#include "RotationVipDlg.h"
#include "CCSDdx.h"
#include "CCSTime.h"
#include "CCSGlobl.h"
#include "RotationDlgCedaFlightData.h"
#include "PrivList.h"
#include "VIPDlg.h"
#include "CedaBasicData.h"
#include "utils.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


// Local function prototype
static void RotationVipDlgCf(void *popInstance, int ipDDXType,
    void *vpDataPointer, CString &ropInstanceName);





/////////////////////////////////////////////////////////////////////////////
// Dialogfeld RotationVipDlg 


RotationVipDlg::RotationVipDlg(CWnd* pParent, ROTATIONDLGFLIGHTDATA* prpAFlight , ROTATIONDLGFLIGHTDATA* prpDFlight)
	: CDialog(RotationVipDlg::IDD, pParent),
	 prmAFlight(prpAFlight),
	 prmDFlight(prpDFlight)
{
	//{{AFX_DATA_INIT(RotationVipDlg)
	//}}AFX_DATA_INIT

                         
	ogDdx.Register(this, BCD_VIP_UPDATE, CString("ROTATIONDLG_VIP"), CString("Vip Update"), RotationVipDlgCf);
	ogDdx.Register(this, BCD_VIP_DELETE, CString("ROTATIONDLG_VIP"), CString("Vip Delete"), RotationVipDlgCf);
	ogDdx.Register(this, BCD_VIP_INSERT, CString("ROTATIONDLG_VIP"), CString("Vip Insert"), RotationVipDlgCf);

	pomParent = pParent;
    pomTable = new CCSTable;

	emStatus = UNKNOWN;


	omVIPUrnos.RemoveAll();
	//omVIPUrnos.FreeExtra();

}


RotationVipDlg::~RotationVipDlg()
{

	ogDdx.UnRegister(this, NOTUSED);

	delete pomTable;

}


void RotationVipDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(RotationVipDlg)
	DDX_Control(pDX, IDC_GMBORDER, m_CS_GMBorder);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(RotationVipDlg, CDialog)
	//{{AFX_MSG_MAP(RotationVipDlg)
    ON_MESSAGE(WM_TABLE_LBUTTONDOWN, OnTableLButtonDown)
	ON_BN_CLICKED(IDC_UPDATE, OnUpdate)
	ON_BN_CLICKED(IDC_INSERT, OnInsert)
	ON_BN_CLICKED(IDC_DELETE, OnDelete)
	ON_BN_CLICKED(IDC_CLOSE, OnClose)
    ON_MESSAGE(WM_TABLE_LBUTTONDBLCLK, OnTableLButtonDblclk)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()



/////////////////////////////////////////////////////////////////////////////
// Behandlungsroutinen f�r Nachrichten RotationVipDlg 

BOOL RotationVipDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

	pomParent->EnableWindow(TRUE);
	
	InitTable();

	if (prmAFlight && prmAFlight->Urno > 0)
		FillTableLine(prmAFlight);
	if (prmDFlight && prmDFlight->Urno > 0)
		FillTableLine(prmDFlight);

	InitialStatus();

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}



void RotationVipDlg::FillTableLine(ROTATIONDLGFLIGHTDATA* prpFlight)
{

	char pclTmp[64];
	CString olVIPUrno;
	ltoa(prpFlight->Urno, pclTmp, 10);

	CCSPtrArray<RecordSet>rlVipes;
	ogBCD.GetRecords("VIP", "FLNU", pclTmp, &rlVipes);


	CCSPtrArray<TABLE_COLUMN> olColList;
	TABLE_COLUMN rlColumnData;

	rlColumnData.VerticalSeparator = SEPA_NONE;
	rlColumnData.SeparatorType = SEPA_NONE;
	rlColumnData.Font = &ogCourier_Regular_9;


	CCSEDIT_ATTRIB rlAttrib;
	rlAttrib.Style = ES_UPPERCASE;

	for(int i = 0; i < rlVipes.GetSize(); i++)
	{
		rlColumnData.Alignment = COLALIGN_LEFT;
		rlColumnData.Text = prpFlight->Flno;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		rlColumnData.Alignment = COLALIGN_LEFT;
		rlColumnData.Text = prpFlight->Tifa.Format("%H:%M");
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		rlColumnData.Alignment = COLALIGN_LEFT;
		rlColumnData.Text = prpFlight->Tifd.Format("%H:%M");
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		rlColumnData.Alignment = COLALIGN_LEFT;
		rlColumnData.Text = rlVipes[i].Values[ogBCD.GetFieldIndex("VIP","NOGR")];//ogBCD.GetField("VIP", "URNO", olVIPUrno, "NOGR");
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		rlColumnData.Alignment = COLALIGN_LEFT;
		rlColumnData.Text = rlVipes[i].Values[ogBCD.GetFieldIndex("VIP","NOPX")];//ogBCD.GetField("VIP", "URNO", olVIPUrno, "NOPX");
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		rlColumnData.Alignment = COLALIGN_LEFT;
		rlColumnData.Text = rlVipes[i].Values[ogBCD.GetFieldIndex("VIP","PAXN")];
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		rlColumnData.Alignment = COLALIGN_LEFT;
		rlColumnData.Text = rlVipes[i].Values[ogBCD.GetFieldIndex("VIP","PAXR")];
		olColList.NewAt(olColList.GetSize(), rlColumnData);


		rlColumnData.Alignment = COLALIGN_RIGHT;
		rlColumnData.Text = rlVipes[i].Values[ogBCD.GetFieldIndex("VIP","GRID")];
		olColList.NewAt(olColList.GetSize(), rlColumnData);


		pomTable->AddTextLine(olColList, NULL);
		olColList.DeleteAll();

		olVIPUrno = rlVipes[i].Values[ogBCD.GetFieldIndex("VIP","URNO")];
		if (!olVIPUrno.IsEmpty())
			omVIPUrnos.Add(olVIPUrno);
	}

	DrawHeader();

	pomTable->DisplayTable();

}


void RotationVipDlg::InitTable()
{

	CRect olRectBorder;
	m_CS_GMBorder.GetWindowRect( olRectBorder );
	ScreenToClient(olRectBorder);
	olRectBorder.DeflateRect(2,2);

	omVIPUrnos.RemoveAll();

	pomTable->SetHeaderSpacing(0);
    pomTable->SetTableData(this, olRectBorder.left, olRectBorder.right, olRectBorder.top, olRectBorder.bottom);
	pomTable->SetSelectMode(0);
	pomTable->SetShowSelection(true);
	pomTable->ResetContent();

	CCSPtrArray <TABLE_HEADER_COLUMN> omHeaderDataArray;

	
	TABLE_HEADER_COLUMN rlHeader;

	rlHeader.Alignment = COLALIGN_CENTER;
	rlHeader.Font = &ogCourier_Regular_10;

	// FLNO
	rlHeader.Length = 15; 
	rlHeader.Text = CString("");
	omHeaderDataArray.New(rlHeader);
	// TFA
	rlHeader.Length = 35; 
	rlHeader.Text = CString("");
	omHeaderDataArray.New(rlHeader);
	// TIFD
	rlHeader.Length = 60; 
	rlHeader.Text = CString("");
	omHeaderDataArray.New(rlHeader);
	// NOGR
	rlHeader.Length = 40; 
	rlHeader.Text = CString("");
	omHeaderDataArray.New(rlHeader);
	// NOPX
	rlHeader.Length = 50; 
	rlHeader.Text = CString("");
	omHeaderDataArray.New(rlHeader);
	// PAXN
	rlHeader.Length = 35; 
	rlHeader.Text = CString("");
	omHeaderDataArray.New(rlHeader);
	// PAXR
	rlHeader.Length = 50; 
	rlHeader.Text = CString("");
	omHeaderDataArray.New(rlHeader);
	// GRID
	rlHeader.Length = 30; 
	rlHeader.Text = CString("");
	omHeaderDataArray.New(rlHeader);
	
	pomTable->SetHeaderFields(omHeaderDataArray);
	pomTable->SetDefaultSeparator();
	pomTable->SetTableEditable(false);
	omHeaderDataArray.DeleteAll();

	pomTable->DisplayTable();

}


void RotationVipDlg::DrawHeader()
{
	int  ilTotalLines = 0;
	bool blNewLogicLine = false;

	CCSPtrArray <TABLE_HEADER_COLUMN> omHeaderDataArray;

//	pomTable->SetShowSelection(true);
//	pomTable->ResetContent();
	
	TABLE_HEADER_COLUMN *prlHeader[8];

	prlHeader[0] = new TABLE_HEADER_COLUMN;
	prlHeader[0]->Alignment = COLALIGN_CENTER;
	prlHeader[0]->Length = 64; 
	prlHeader[0]->Font = &ogCourier_Bold_10;
	prlHeader[0]->Text = GetString(IDS_STRING296);//Flight

	prlHeader[1] = new TABLE_HEADER_COLUMN;
	prlHeader[1]->Alignment = COLALIGN_CENTER;
	prlHeader[1]->Length = 55;
	prlHeader[1]->Font = &ogCourier_Bold_10;
	prlHeader[1]->Text = GetString(IDS_STRING1704);//TIFA

	prlHeader[2] = new TABLE_HEADER_COLUMN;
	prlHeader[2]->Alignment = COLALIGN_CENTER;
	prlHeader[2]->Length = 55; 
	prlHeader[2]->Font = &ogCourier_Bold_10;
	prlHeader[2]->Text = GetString(IDS_STRING1705);// TIFD

	prlHeader[3] = new TABLE_HEADER_COLUMN;
	prlHeader[3]->Alignment = COLALIGN_CENTER;
	prlHeader[3]->Length = 45; 
	prlHeader[3]->Font = &ogCourier_Bold_10;
	prlHeader[3]->Text = GetString(IDS_STRING1706);// NOGR

	prlHeader[4] = new TABLE_HEADER_COLUMN;
	prlHeader[4]->Alignment = COLALIGN_CENTER;
	prlHeader[4]->Length = 45; 
	prlHeader[4]->Font = &ogCourier_Bold_10;
	prlHeader[4]->Text = GetString(IDS_STRING1707);//NOPX

	prlHeader[5] = new TABLE_HEADER_COLUMN;
	prlHeader[5]->Alignment = COLALIGN_CENTER;
	prlHeader[5]->Length = 122;
	prlHeader[5]->Font = &ogCourier_Bold_10;
	prlHeader[5]->Text = GetString(IDS_STRING1708);//PAXN

	prlHeader[6] = new TABLE_HEADER_COLUMN;
	prlHeader[6]->Alignment = COLALIGN_CENTER;
	prlHeader[6]->Length = 122;
	prlHeader[6]->Font = &ogCourier_Bold_10;
	prlHeader[6]->Text = GetString(IDS_STRING1709);//PAXR


	prlHeader[7] = new TABLE_HEADER_COLUMN;
	prlHeader[7]->Alignment = COLALIGN_CENTER;
	prlHeader[7]->Length = 22;
	prlHeader[7]->Font = &ogCourier_Bold_10;
	prlHeader[7]->Text = GetString(IDS_STRING1710);//GRID


	for(int ili = 0; ili < 8; ili++)
	{
		omHeaderDataArray.Add(prlHeader[ili]);
	}

	pomTable->SetHeaderFields(omHeaderDataArray);
	omHeaderDataArray.DeleteAll();

	pomTable->SetDefaultSeparator();

}


void RotationVipDlg::OnCancel() 
{

	CDialog::OnCancel();

}


LONG RotationVipDlg::OnTableLButtonDown(UINT wParam, LONG lParam)
{
	CCSTABLENOTIFY *prlNotify = (CCSTABLENOTIFY*)lParam;
	pomTable->SelectLine(prlNotify->Line);
	return 0L;
}



void RotationVipDlg::InitDialog()
{

	InitTable();

	if (prmAFlight && prmAFlight->Urno > 0)
		FillTableLine(prmAFlight);
	if (prmDFlight && prmDFlight->Urno > 0)
		FillTableLine(prmDFlight);

	InitialStatus();

}


void RotationVipDlg::OnUpdate()
{

	int ilAnz = (pomTable->GetCTableListBox())->GetCurSel();

	if(ilAnz < 0)
	{
		MessageBox(GetString(IDS_STRING909), GetString(ST_HINWEIS));
		return;
	}
	
	if (ilAnz >= omVIPUrnos.GetSize())
		return;

	char clType = ' ';
	if (prmAFlight->Urno > 0 && prmDFlight->Urno <= 0)
		clType = 'A';
	if (prmDFlight->Urno > 0 && prmAFlight->Urno <= 0)
		clType = 'D';
	if (prmAFlight->Urno > 0 && prmDFlight->Urno > 0)
		clType = 'R';

	if (omVIPUrnos[ilAnz] == "")
	{
		MessageBox(GetString(IDS_STRING1703), GetString(ST_HINWEIS), MB_ICONEXCLAMATION);
		return;
	}

	VIPDlg polDlg(this, clType, 'U', omVIPUrnos[ilAnz]);

	char olUrnoA[64];
	ltoa(prmAFlight->Urno, olUrnoA, 10);
	char olUrnoD[64];
	ltoa(prmDFlight->Urno, olUrnoD, 10);

	if (clType == 'R')
	{
		polDlg.SetAftTabUrnoA(CString(olUrnoA));
		polDlg.SetAftTabUrnoD(CString(olUrnoD));
	}
	if (clType == 'A')
	{
		polDlg.SetAftTabUrnoA(CString(olUrnoA));
	}
	if (clType == 'D')
	{
		polDlg.SetAftTabUrnoD(CString(olUrnoD));
	}

	polDlg.setStatus(emStatus);
	polDlg.DoModal();


	pomTable->ResetContent();
	if (prmAFlight && prmAFlight->Urno > 0)
		FillTableLine(prmAFlight);
	if (prmDFlight && prmDFlight->Urno > 0)
		FillTableLine(prmDFlight);

}


void RotationVipDlg::OnInsert()
{

	char clType = ' ';
	if (prmAFlight->Urno > 0 && prmDFlight->Urno <= 0)
		clType = 'A';
	if (prmDFlight->Urno > 0 && prmAFlight->Urno <= 0)
		clType = 'D';
	if (prmAFlight->Urno > 0 && prmDFlight->Urno > 0)
		clType = 'R';

	VIPDlg polDlg(this, clType, 'I');

	char olUrnoA[64];
	ltoa(prmAFlight->Urno, olUrnoA, 10);
	char olUrnoD[64];
	ltoa(prmDFlight->Urno, olUrnoD, 10);

	if (clType == 'R')
	{
		polDlg.SetAftTabUrnoA(CString(olUrnoA));
		polDlg.SetAftTabUrnoD(CString(olUrnoD));
	}
	if (clType == 'A')
	{
		polDlg.SetAftTabUrnoA(CString(olUrnoA));
	}
	if (clType == 'D')
	{
		polDlg.SetAftTabUrnoD(CString(olUrnoD));
	}
	

	polDlg.DoModal();

	pomTable->ResetContent();
	if (prmAFlight && prmAFlight->Urno > 0)
		FillTableLine(prmAFlight);
	if (prmDFlight && prmDFlight->Urno > 0)
		FillTableLine(prmDFlight);

	InitDialog();

}


void RotationVipDlg::OnDelete()
{

	int ilAnz = (pomTable->GetCTableListBox())->GetCurSel();

	if(ilAnz < 0)
	{
		MessageBox(GetString(IDS_STRING909), GetString(ST_HINWEIS));
		return;
	}

	if(MessageBox(GetString(ST_DATENSATZ_LOESCHEN), GetString(ST_HINWEIS), MB_YESNO ) == IDYES)
	{
		if (ilAnz < omVIPUrnos.GetSize())
		{
			ogBCD.DeleteRecord("VIP", "URNO", omVIPUrnos[ilAnz], true);
			InitDialog();
		}
	}

}


LONG RotationVipDlg::OnTableLButtonDblclk( UINT wParam, LPARAM lParam)
{
#if 0
	// anhand der Mousposition die Spalte ermitteln
	int ilCol = 0;
	CPoint point;
    ::GetCursorPos(&point);
    pomDailyScheduleTable->pomListBox->ScreenToClient(&point);    

	UINT ipItem = wParam;
	DAILYSCHEDULE_LINEDATA *prlTableLine = NULL;
	prlTableLine = (DAILYSCHEDULE_LINEDATA *)pomDailyScheduleTable->GetTextLineData(ipItem);

	// Inhalt in der Zeile?
	if (prlTableLine != NULL)
	{
		// Spaltennummer
		ilCol = pomDailyScheduleTable->GetColumnnoFromPoint(point);
	}

	DAILYFLIGHTDATA *prlFlight;
	if((prlFlight = ogDailyCedaFlightData.GetFlightByUrno(prlTableLine->AUrno)) == NULL)
		prlFlight = ogDailyCedaFlightData.GetFlightByUrno(prlTableLine->DUrno);


	// Behandlung: Groundmovement
	if (strcmp (prlFlight->Ftyp, "G") == 0)
	{
		if(pogRotGroundDlg == NULL)
		{
			pogRotGroundDlg = new RotGroundDlg(this);
			pogRotGroundDlg->Create(IDD_ROTGROUND);
		}

		// gueltiger Wert der Spalte
		if (ilCol < 0)
			return 0L;

		// Klick auf Arrival oder Departure
		if (omViewer.GetColumnByIndex(ilCol).Left(1) == "A")
		{
			prlFlight = ogDailyCedaFlightData.GetFlightByUrno(prlTableLine->AUrno);
			if(prlFlight == NULL) 
				return 0L;
		}
		else
		{
			prlFlight = ogDailyCedaFlightData.GetFlightByUrno(prlTableLine->DUrno);
			if(prlFlight == NULL) 
				return 0L;
		}

		pogRotGroundDlg->SetWindowPos(&wndTop, 0,0,0,0, SWP_SHOWWINDOW | SWP_NOSIZE | SWP_NOMOVE );
		pogRotGroundDlg->NewData(this, prlFlight->Rkey, prlFlight->Urno);
	}
	else
	{
		if (prlTableLine != NULL)
		{
			CString olAdid("");
			//DAILYFLIGHTDATA *prlFlight;

			if((prlFlight = ogDailyCedaFlightData.GetFlightByUrno(prlTableLine->AUrno)) == NULL)
			{
				prlFlight = ogDailyCedaFlightData.GetFlightByUrno(prlTableLine->DUrno);
				olAdid = "D";
			}
			else
			{
				olAdid = "A";
			}
			
			// Rotationsmaske aufrufen
			if(prlFlight != NULL)
			{
				pogRotationDlg->NewData(this, prlFlight->Rkey, prlFlight->Urno, olAdid);
				omViewer.UpdateDisplay();
			}
		}
	}
#endif
	OnUpdate();

	return 0L;
}


void RotationVipDlg::OnClose()
{

	CDialog::OnCancel();

}



static void RotationVipDlgCf(void *popInstance, int ipDDXType,
    void *vpDataPointer, CString &ropInstanceName)
{

    RotationVipDlg *polDlg = (RotationVipDlg *)popInstance;


	if (ipDDXType == BCD_VIP_UPDATE || ipDDXType == BCD_VIP_DELETE || ipDDXType == BCD_VIP_INSERT)
		polDlg->InitDialog();

}

void RotationVipDlg::InitialStatus()
{
	CWnd* plWnd = NULL;
	switch (emStatus)
	{
		case UNKNOWN:
			InitialPostFlight(TRUE);
			break;
		case POSTFLIGHT:
			InitialPostFlight(FALSE);
			break;
		default:
			InitialPostFlight(TRUE);
			break;
	}
}

void RotationVipDlg::setStatus(Status epStatus)
{
	emStatus = epStatus;
}

RotationVipDlg::Status RotationVipDlg::getStatus() const
{
	return emStatus;
}

void RotationVipDlg::InitialPostFlight(BOOL bpPostFlight)
{
	CWnd* plWnd = NULL;
	plWnd = GetDlgItem(IDC_INSERT);
	if (plWnd) 
		plWnd->EnableWindow(bpPostFlight);

	plWnd = GetDlgItem(IDC_DELETE);
	if (plWnd) 
		plWnd->EnableWindow(bpPostFlight);

	plWnd = GetDlgItem(IDC_UPDATE);
	if (plWnd) 
		plWnd->EnableWindow(bpPostFlight);

	ModifyWindowText(this, GetString(IDS_STRINGWND_POSTFLIGHT), bpPostFlight);

}
