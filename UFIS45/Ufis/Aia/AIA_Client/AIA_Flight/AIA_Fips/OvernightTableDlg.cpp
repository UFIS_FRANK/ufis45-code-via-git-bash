// SeasonTableDlg.cpp : implementation file
//

#include "stdafx.h"
#include <process.h>
#include "FPMS.h"
#include "OvernightTableDlg.h"
#include "CedaBasicData.h"
#include "CCSTable.h"
#include "PrivList.h"
#include "CedaImpFlightData.h"
#include "SeasonFlightPropertySheet.H"
#include "FlightSearchTableDlg.h"
#include "CCSGlobl.h"
#include "AskBox.h"
#include "Utils.h"
#include "resrc1.h"
#include "CedaFlightUtilsData.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// OvernightTableDlg dialog
////////////////////////////////////////////////////////////////////////////
// Konstruktor
//
OvernightTableDlg::OvernightTableDlg(CWnd* pParent /*=NULL*/)
	: CDialog(OvernightTableDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(OvernightTableDlg)
	m_From = _T("");
	m_To = _T("");
	m_days = _T("");
	//}}AFX_DATA_INIT
	omSeasonFlights = new SeasonCedaFlightData();
	pomSeasonTable = NULL;
	pomParent = pParent;
	omViewer.SetViewerKey("SAISONFLIGHT");
	m_key = "DialogPosition\\OvernightReport";
//    CDialog::Create(OvernightTableDlg::IDD, pParent);
}


////////////////////////////////////////////////////////////////////////////
// Destruktor
//
OvernightTableDlg::~OvernightTableDlg()
{
	omSeasonFlights->ClearAll();
	if (omSeasonFlights)
		delete omSeasonFlights;
	if (pomSeasonTable)
		delete pomSeasonTable;
}


void OvernightTableDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(OvernightTableDlg)
 	DDX_Control(pDX, IDC_PRINT, m_CB_Drucken);
	DDX_Control(pDX, IDC_EXCEL, m_CB_Excel);
	DDX_Control(pDX, IDC_SEARCH, m_CB_Search);
	DDX_Control(pDX, IDC_FROM, m_CE_From);
	DDX_Text(pDX, IDC_FROM, m_From);
	DDX_Control(pDX, IDC_TO, m_CE_To);
	DDX_Text(pDX, IDC_TO, m_To);
	DDX_Text(pDX, IDC_DAYS, m_days);
	DDX_Control(pDX, IDC_DAYS, m_CE_days);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(OvernightTableDlg, CDialog)
	//{{AFX_MSG_MAP(OvernightTableDlg)
    ON_MESSAGE(WM_TABLE_LBUTTONDBLCLK, OnTableLButtonDblclk)
	ON_BN_CLICKED(IDC_EXCEL, OnExcel)
	ON_WM_SIZE()
	ON_BN_CLICKED(IDC_PRINT, OnPrint)
	ON_BN_CLICKED(IDC_SEARCH, OnSearch)
	ON_WM_CLOSE()
 	ON_WM_DESTROY()
    ON_MESSAGE(WM_TABLE_RETURN_PRESSED, OnTableReturnPressed)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// OvernightTableDlg message handlers

BOOL OvernightTableDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	omViewer.SetParentDlg(this);
	m_key = "DialogPosition\\OvernightReport";

//Monitorsetup
	CRect olRect;
    GetClientRect(&olRect);
    imDialogBarHeight = olRect.bottom - olRect.top;
	
	GetWindowRect(&olRect);

	int ilWidth = olRect.right -olRect.left;
	int ilWhichMonitor = ogCfgData.GetMonitorForWindow(CString(MON_SEASONSCHEDULE_STRING));
	int ilMonitors = ogCfgData.GetMonitorForWindow(CString(MON_COUNT_STRING));
	int left, top, right, bottom;
	int ilCXMonitor;

//	top = 65 ;
	top = 95 ;
	bottom = ::GetSystemMetrics(SM_CYSCREEN);

	if(ilWhichMonitor == 1)
	{
		switch(ilMonitors)
		{
		case 1:
			ilCXMonitor = ::GetSystemMetrics(SM_CXSCREEN);
			left = (int)((ilCXMonitor-ilWidth)/2);
			right = left + ilWidth;//GetSystemMetrics(SM_CXSCREEN);
			break;
		case 2:
			ilCXMonitor = (int)(::GetSystemMetrics(SM_CXSCREEN)/2);
			left = (int)((ilCXMonitor-ilWidth)/2);
			right = left + ilWidth;//GetSystemMetrics(SM_CXSCREEN);
			break;
		case 3:
			ilCXMonitor = (int)(::GetSystemMetrics(SM_CXSCREEN)/3);
			left = (int)((ilCXMonitor-ilWidth)/2);
			right = left + ilWidth;//GetSystemMetrics(SM_CXSCREEN);
			break;
		default:
			ilCXMonitor = ::GetSystemMetrics(SM_CXSCREEN);
			left = (int)((ilCXMonitor-ilWidth)/2);
			right = left + ilWidth;//GetSystemMetrics(SM_CXSCREEN);
			break;
		}
	}
	else if(ilWhichMonitor == 2)
	{
		switch(ilMonitors)
		{
		case 1:
			ilCXMonitor = ::GetSystemMetrics(SM_CXSCREEN);
			left = (int)((ilCXMonitor-ilWidth)/2);
			right = left + ilWidth;//GetSystemMetrics(SM_CXSCREEN);
			break;
		case 2:
			ilCXMonitor = (int)(::GetSystemMetrics(SM_CXSCREEN)/2);
			left = (int)((ilCXMonitor-ilWidth)/2)+(ilCXMonitor);
			right = left + ilWidth;//GetSystemMetrics(SM_CXSCREEN);
			break;
		case 3:
			ilCXMonitor = (int)(::GetSystemMetrics(SM_CXSCREEN)/3);
			left = (int)((ilCXMonitor-ilWidth)/2)+(ilCXMonitor);
			right = left + ilWidth;//GetSystemMetrics(SM_CXSCREEN);
			break;
		default:
			right = GetSystemMetrics(SM_CXSCREEN);
			break;
		}
	}
	else if(ilWhichMonitor == 3)
	{
		switch(ilMonitors)
		{
		case 1:
			ilCXMonitor = ::GetSystemMetrics(SM_CXSCREEN);
			left = (int)((ilCXMonitor-ilWidth)/2);
			right = left + ilWidth;//GetSystemMetrics(SM_CXSCREEN);
			break;
		case 2:
			ilCXMonitor = (int)(::GetSystemMetrics(SM_CXSCREEN)/2);
			left = (int)((ilCXMonitor-ilWidth)/2);
			right = left + ilWidth;//GetSystemMetrics(SM_CXSCREEN);
			break;
		case 3:
			ilCXMonitor = (int)(::GetSystemMetrics(SM_CXSCREEN)/3);
			left = (int)((ilCXMonitor-ilWidth)/2)+(2*ilCXMonitor);
			right = left + ilWidth;//GetSystemMetrics(SM_CXSCREEN);
			break;
		default:
			right = GetSystemMetrics(SM_CXSCREEN);
			break;
		}
	}
//-------------
//	MoveWindow(CRect(left, top, right, bottom));
//	SetWindowPos(&wndTop,0, 0, 0, 0, SWP_SHOWWINDOW | SWP_NOMOVE | SWP_NOSIZE);

//######
/*	m_resizeHelper.Init(this->m_hWnd);
	m_resizeHelper.Fix(IDC_EXCEL,DlgResizeHelper::kNoHFix,DlgResizeHelper::kHeight);
	m_resizeHelper.Fix(IDOK,DlgResizeHelper::kNoHFix,DlgResizeHelper::kHeight);
	m_resizeHelper.Fix(IDC_PRINT,DlgResizeHelper::kNoHFix,DlgResizeHelper::kHeight);
	m_resizeHelper.Fix(IDC_SEARCH,DlgResizeHelper::kNoHFix,DlgResizeHelper::kHeight);
	m_resizeHelper.Fix(IDC_DAYS,DlgResizeHelper::kNoHFix,DlgResizeHelper::kHeight);
	m_resizeHelper.Fix(IDC_TO,DlgResizeHelper::kNoHFix,DlgResizeHelper::kHeight);
	m_resizeHelper.Fix(IDC_FROM,DlgResizeHelper::kNoHFix,DlgResizeHelper::kHeight);
	m_resizeHelper.Fix(IDC_STATIC_DAYS,DlgResizeHelper::kNoHFix,DlgResizeHelper::kHeight);
*/
	olRect = CRect(0, 95, ::GetSystemMetrics(SM_CXSCREEN), ::GetSystemMetrics(SM_CYSCREEN));
	GetDialogFromReg(olRect, m_key);
	SetWindowPos(&wndTop, olRect.left, olRect.top, olRect.Width(), olRect.Height(), SWP_SHOWWINDOW);// | SWP_NOMOVE );
//######


    pomSeasonTable = new CCSTable;
	pomSeasonTable->SetSelectMode(LBS_MULTIPLESEL | LBS_EXTENDEDSEL);
	pomSeasonTable->SetHeaderSpacing(0);
	
	//CRect rect;
    GetClientRect(&olRect);

	olRect.top = olRect.top + imDialogBarHeight;
	olRect.bottom = olRect.bottom;// - imDialogBarHeight;

    olRect.InflateRect(1, 1);     // hiding the CTable window border
    
	pomSeasonTable->SetTableData(this, olRect.left, olRect.right, olRect.top, olRect.bottom);
	omViewer.Attach(pomSeasonTable);
	omViewer.ChangeViewTo("<Default>");
	UpdateView();



	SetWndStatAll(ogPrivList.GetStat("OVERNIGHT_CB_Drucken"),m_CB_Drucken);
	SetWndStatAll(ogPrivList.GetStat("OVERNIGHT_CB_Excel"),m_CB_Excel);
	SetWndStatAll(ogPrivList.GetStat("OVERNIGHT_CB_Search"),m_CB_Search);

	m_CE_days.SetTypeToInt(-1, 99999);

//	m_CE_From.SetTypeToTime(true, false);
	m_CE_From.SetTypeToTime(true, true);
	m_CE_From.SetWindowText(m_From);
	m_CE_To.SetTypeToTime(true, true);
	m_CE_To.SetWindowText(m_To);

	m_CE_From.SetBKColor(YELLOW);
	m_CE_To.SetBKColor(YELLOW);

	m_CB_Search.SetFocus();

	m_CE_From.SetSel(0,-1,TRUE);
	m_CE_From.SetFocus();

	m_CE_To.ShowWindow(SW_HIDE);

	ShowWindow(SW_SHOWNORMAL);


	return FALSE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void OvernightTableDlg::OnDestroy() 
{
	SaveToReg();
	CDialog::OnDestroy();
}



void OvernightTableDlg::SaveToReg() 
{
	BOOL ok = WriteDialogToReg((CWnd*) this, m_key);
}

////////////////////////////////////////////////////////////////////////////
// und Tsch�ss...
//


/*
void OvernightTableDlg::OnCancel()
{
//	omSeasonFlights->ClearAll();
//	CDialog::OnCancel();
//	CDialog::DestroyWindow();
//	delete this;
}
*/

////////////////////////////////////////////////////////////////////////////
// UpdateView
//
void OvernightTableDlg::UpdateView()
{

	CString olViewName = "<Default>";

	if (olViewName.IsEmpty() == TRUE)
	{
	//	olViewName = ogCfgData.rmUserSetup.STLV;
	}

	AfxGetApp()->DoWaitCursor(1);
    omViewer.ChangeViewTo(olViewName);
	AfxGetApp()->DoWaitCursor(-1);

	// Id 24-Sep-96
	// This will set the keyboard focus back to the list-box after this button was clicked.
	pomSeasonTable->GetCTableListBox()->SetFocus();
}


////////////////////////////////////////////////////////////////////////////
// linke Maustaste auf der Flighttable gedr�ckt
//
LONG OvernightTableDlg::OnTableLButtonDblclk( UINT wParam, LPARAM lParam)
{
	char clStat = ogPrivList.GetStat("OVERNIGHT_CB_Seasonmask");
	if(clStat != '1')
		return 0L;

	CCSTABLENOTIFY *prlNotify = (CCSTABLENOTIFY*)lParam;

	UINT ipItem = wParam;
	SEASONTABLE_LINEDATA *prlTableLine = NULL;
	prlTableLine = (SEASONTABLE_LINEDATA *)pomSeasonTable->GetTextLineData(ipItem);


	if (prlTableLine == NULL)
		return 0L;

	// determine flight
	SEASONFLIGHTDATA *prlFlight = NULL;
	SEASONFLIGHTDATA *prlDFlight = omSeasonFlights->GetFlightByUrno(prlTableLine->DUrno);
	SEASONFLIGHTDATA *prlAFlight = omSeasonFlights->GetFlightByUrno(prlTableLine->AUrno);

//	CString olAdid;
	char clAdid;
	if (prlAFlight != NULL && prlDFlight != NULL)
	{
		if(prlNotify->Column >= 16)
		{
			clAdid = 'D';
			prlFlight = prlDFlight;
		}
		else
		{
			clAdid = 'A';
			prlFlight = prlAFlight;
		}
	}
	else if (prlAFlight != NULL)
	{
		clAdid = 'A';
		prlFlight = prlAFlight;
	}
	else if (prlDFlight != NULL)
	{
		clAdid = 'D';
		prlFlight = prlDFlight;
	}

	if (prlFlight == NULL)
		return 0L;


	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
	CFPMSApp::ShowFlightRecordData(this, prlFlight->Ftyp[0], prlFlight->Urno, prlFlight->Rkey, clAdid, prlFlight->Tifa, bgReportLocal, 'S');
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));

	return 0L;
}

void OvernightTableDlg::OnExcel() 
{
	char pclConfigPath[256];
	char pclExcelPath[256];
	char pclTrenner[64];
	

    if (getenv("CEDA") == NULL)
        strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
    else
        strcpy(pclConfigPath, getenv("CEDA"));

    GetPrivateProfileString(ogAppName, "Excel", "DEFAULT",
        pclExcelPath, sizeof pclExcelPath, pclConfigPath);


    GetPrivateProfileString(ogAppName, "ExcelSeperator", ";",
        pclTrenner, sizeof pclTrenner, pclConfigPath);


	if(!strcmp(pclExcelPath, "DEFAULT"))
		CFPMSApp::MyTopmostMessageBox(this,GetString(IDS_STRING977), GetString(ST_FEHLER), MB_ICONERROR);

	CString olFileName;

	omViewer.CreateExcelFile(pclTrenner);

	olFileName = omViewer.omFileName;

	bool test = true; //only for testing error
	if (olFileName.IsEmpty() || olFileName.GetLength() > 255 || !test)
	{
		if (olFileName.IsEmpty())
		{
			CString mess = "Filename is empty !" + olFileName;
			CFPMSApp::MyTopmostMessageBox(this, mess, GetString(ST_FEHLER), MB_ICONERROR);
			return;
		}
		if (olFileName.GetLength() > 255 )
		{
			CString mess = "Length of the Filename > 255: " + olFileName;
			CFPMSApp::MyTopmostMessageBox(this, mess, GetString(ST_FEHLER), MB_ICONERROR);
			return;
		}

		CString mess = "Filename invalid: " + olFileName;
		CFPMSApp::MyTopmostMessageBox(this, mess, GetString(ST_FEHLER), MB_ICONERROR);
		return;
	}

	char pclTmp[256];
	strcpy(pclTmp, olFileName); 

   /* Set up parameters to be sent: */
	char *args[4];
	args[0] = "child";
	args[1] = pclTmp;
	args[2] = NULL;
	args[3] = NULL;

	_spawnv( _P_NOWAIT , pclExcelPath, args );


}

void OvernightTableDlg::OnSize(UINT nType, int cx, int cy) 
{
	CDialog::OnSize(nType, cx, cy);

//	m_resizeHelper.OnSize();

	if(pomSeasonTable != NULL)
	{
		CRect rect;	
		GetClientRect(&rect);
		rect.top = rect.top + imDialogBarHeight;
		rect.bottom = rect.bottom;// - imDialogBarHeight;

		rect.InflateRect(1, 1);     // hiding the CTable window border
    
		pomSeasonTable->SetPosition(rect.left, rect.right, rect.top, rect.bottom);
	}
}


void OvernightTableDlg::OnPrint() 
{
	omViewer.PrintTableView();	
}

void OvernightTableDlg::OnClose() 
{
	omSeasonFlights->ClearAll();
	CDialog::OnClose();
}


void OvernightTableDlg::OnSearch()
{
	m_CE_From.GetWindowText(m_From);
	m_CE_To.GetWindowText(m_To);

	if (!m_From.IsEmpty())
	{
		if(!m_CE_From.GetStatus())
		{
			MessageBox(GetString(IDS_STRING2204), GetString(IDS_STRING2205), MB_OK);
			return;
		}
	}
	if (m_From.IsEmpty())
	{
		MessageBox(GetString(IDS_STRING2204), GetString(IDS_STRING2205), MB_OK);
		return;
	}

	m_CE_days.GetWindowText(m_days);
	int ildays = atoi(m_days);

	if (ildays == -1)
	{
		if (CFPMSApp::MyTopmostMessageBox(this, GetString(IDS_STRING2210), GetString(ST_HINWEIS), MB_YESNO | MB_ICONWARNING) != IDYES)
 			return;
	}

	int ilRet = pogFlightSearchTableDlg->Activate("SEASONFLIGHT", "<Default>", REPORTS);
	if (ilRet == -1)
	{
		m_CE_From.SetFocus();
		return;
	}

	CString olFromStr; 
	CString olToStr;
	CString olZeitRaumA;
	CString olZeitRaumD;
	CTime olNewFrom = pogFlightSearchTableDlg->omFrom - CTimeSpan(ildays,0,0,0);
	CTime olNewTo   = pogFlightSearchTableDlg->omTo   + CTimeSpan(ildays,0,0,0);

					CString timeA  = olNewFrom.Format("%Y%m%d %H%M"); 
					CString timeD  = olNewTo.Format("%Y%m%d %H%M");
//	if(bgReportLocal) ogBasicData.UtcToLocal(olNewFrom);
//	if(bgReportLocal) ogBasicData.UtcToLocal(olNewTo);

	olFromStr = CTimeToDBString(olNewFrom, TIMENULL);
	olToStr =	CTimeToDBString(olNewTo, TIMENULL);

	if (ildays >= 0)
	{
		olZeitRaumA =		CString("((STOA BETWEEN '") +
							olFromStr + CString("' AND '") + olToStr + CString("'") +
							CString(" AND DES3 = '") + CString(pcgHome) + CString("') OR (") +
							CString("TIFA BETWEEN '") +
							olFromStr + CString("' AND '") + olToStr + CString("'") +
							CString(" AND DES3 = '") + CString(pcgHome) + CString("'))");


		olZeitRaumD =		CString("((STOD BETWEEN '") +
							olFromStr + CString("' AND '") + olToStr + CString("'") +
							CString(" AND ORG3 = '") + CString(pcgHome) + CString("') OR (") +
							CString("TIFD BETWEEN '") +
							olFromStr + CString("' AND '") + olToStr + CString("'") +
							CString(" AND ORG3 = '") + CString(pcgHome) + CString("'))");
	}
	else
	{
		olZeitRaumA =		CString("((STOA < '") +
							olFromStr + CString("'") +
							CString(" AND DES3 = '") + CString(pcgHome) + CString("') OR (") +
							CString("TIFA < '") +
							olFromStr + CString("'") +
							CString(" AND DES3 = '") + CString(pcgHome) + CString("'))");

		olToStr =	CTimeToDBString(olNewTo, TIMENULL);

		olZeitRaumD =		CString("((STOD > '") +
							olToStr + CString("'") +
							CString(" AND ORG3 = '") + CString(pcgHome) + CString("') OR (") +
							CString("TIFD > '") +
							olFromStr + CString("'") +
							CString(" AND ORG3 = '") + CString(pcgHome) + CString("'))");
	}

	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));

	CString olNewTimes = CString("(") + olZeitRaumA + CString(" OR ") + olZeitRaumD + CString(")");

	int ilPos = pogFlightSearchTableDlg->omWhere.Find(pogFlightSearchTableDlg->omWhereTime);
	CString olLeft;
	if (ilPos > 0)
		olLeft = pogFlightSearchTableDlg->omWhere.Left(ilPos);

	int ilRight = pogFlightSearchTableDlg->omWhere.GetLength() - ilPos - pogFlightSearchTableDlg->omWhereTime.GetLength();
	CString olRight;
	if (ilRight > 0)
		olRight = pogFlightSearchTableDlg->omWhere.Right(ilRight);

	CString olNewSearch = olLeft + olNewTimes + olRight + " [ROTATIONS]";

	omSeasonFlights->SetPreSelection(olNewFrom, olNewTo, pogFlightSearchTableDlg->omFtyps);
//	::MessageBox(NULL,olNewSearch,"",MB_OK); 
	omSeasonFlights->ReadAllFlights(olNewSearch, true, false);//pogFlightSearchTableDlg->bmRotation);

	CTime olFrom = HourStringToDate(m_From, TIMENULL);
	CTime olTo   = HourStringToDate(m_To, TIMENULL);

	CTime olF = pogFlightSearchTableDlg->omFrom;
	CTime olT = pogFlightSearchTableDlg->omTo;

//	if(bgReportLocal) ogBasicData.UtcToLocal(olF);
//	if(bgReportLocal) ogBasicData.UtcToLocal(olT);

	omViewer.ChangeViewTo(omSeasonFlights, olFrom, olTo, olF, olT);
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));

	CDialog::SetWindowText(omViewer.omTableName);	//Dialogbox-�berschrift
	m_CE_From.SetFocus();

}


void OvernightTableDlg::SelectFlight(long lpUrno)
{
	omViewer.SelectLine(lpUrno);
}


LONG OvernightTableDlg::OnTableReturnPressed( UINT wParam, LPARAM lParam)
{
	char clStat = ogPrivList.GetStat("OVERNIGHT_CB_Seasonmask");
	if(clStat != '1')
		return 0L;

	UINT ipItem = wParam;
	SEASONTABLE_LINEDATA *prlTableLine = NULL;
	prlTableLine = (SEASONTABLE_LINEDATA *)pomSeasonTable->GetTextLineData(ipItem);

	if (prlTableLine)
	{
		char clAdid='X';
		SEASONFLIGHTDATA *prlFlight = NULL;

		if((prlFlight = omSeasonFlights->GetFlightByUrno(prlTableLine->AUrno)) == NULL)
		{
			prlFlight = omSeasonFlights->GetFlightByUrno(prlTableLine->DUrno);
			clAdid = 'D';
		}
		else
			clAdid = 'A';
		
		// entsprechenden Diaolg aufrufen
		if(prlFlight)
			CFPMSApp::ShowFlightRecordData(this, prlFlight->Ftyp[0], prlFlight->Urno, prlFlight->Rkey, clAdid, prlFlight->Tifa, bgReportLocal, 'S');
	}

	return 0L;
}
