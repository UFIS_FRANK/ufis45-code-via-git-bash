// CedaCcaData.h

#ifndef __CEDACCADATA__
#define __CEDACCADATA__
 
#include "stdafx.h"
#include "CCSGlobl.h"
#include "CCSPtrArray.h"
#include "CCSCedadata.h"
#include "CCSddx.h"
#include "CedadiaccaData.h"

//---------------------------------------------------------------------------------------------------------
 
struct CCADATA 
{
	CTime 	 Cdat; 		// Erstellungsdatum
	CTime 	 Ckba; 		// Belegung Checkin-Schalter aktueller Beginn
	CTime 	 Ckbs; 		// Belegung Checkin-Schalter geplanter Beginn
	CTime 	 Ckea; 		// Belegung Checkin-Schalter aktuelles Ende
	CTime 	 Ckes; 		// Belegung Checkin-Schalter geplantes Ende
	char 	 Ckic[6]; 	// Checkin-Schalter
	char 	 Ckif[2]; 	// fester Checkin-Schalter
	char 	 Ckit[2]; 	// Terminal Checkin-Schalter
	int 	 Copn	; 	// Schalteröffnungszeit (in Min. vor Abflug)
	char 	 Ctyp[2]; 	// Check-In Typ (Common oder Flight)
	long 	 Flnu; 	// Eindeutige Datensatz-Nr. des zugeteilten Fluges
	CTime 	 Lstu; 		// Datum letzte Änderung
	char 	 Prfl[3]; 	// Protokollierungskennzeichen
	long 	 Urno; 		// Eindeutige Datensatz-Nr.
	char 	 Usec[33]; 	// Anwender (Ersteller)
	char 	 Useu[33]; 	// Anwender (letzte Änderung)
	char	 Rema[61];	// Bemerkung
	char	 Disp[61];	// Bemerkung
	char	 Act3[6];	// Act3 of Flight
	char     Flno[11];	// Flno of Flight
	char	 Stat[11];	// Status of Cca for Conflicts
	long	 Ghpu;		// Urno von Ghp
	long     Gpmu;		// Urno von Gpm
	long     Ghsu;		// Urno von Ghs
	char	 Cgru[258];	// Urnos aus GRN
	CTime	 Stod;

	//DataCreated by this class
	int		 IsChanged;	// Check whether Data has Changed für Relaese

	CCADATA(void);

	const CCADATA& operator= ( const DIACCADATA& s);

	bool IsOpen();
	bool UpdateFIDSMonitor();


}; // end CcaDataStrukt

//---------------------------------------------------------------------------------------------------------
// Class declaratino

class CedaCcaData: public CCSCedaData
{
// Attributes
public:
    CCSPtrArray<CCADATA> omData;

// Operations
public:
    CedaCcaData(int ipDDXNew = DONT_NEED, int ipDDXChange = DONT_NEED, int ipDDXDelete = DONT_NEED, CWnd *popParent = NULL);
	~CedaCcaData();
	void Register(void);
	void UnRegister(void);
	void ClearAll(bool bpWithRegistration = true);
    bool Read(char *pspWhere = NULL, bool bpClearAll = true, bool bpDdx = false, bool bpNoErrors = true);
    bool ReadInsert(char *pspWhere = NULL);
	bool Insert(CCADATA *prpCca);
	bool Delete(long lpUrno);
	bool ReadSpecial(const CString &opWhere);
	bool ReadSpecial(CCSPtrArray<CCADATA> &ropAct,char *pspWhere, bool bpNoErrors  = true );
	bool ExistCkic(CCADATA *prpCca);
	bool ExistSpecial(char *pspWhere);
	bool Save(CCADATA *prpCca);
	void ProcessBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);
	CCADATA  *GetCcaByUrno(long lpUrno);
	bool UpdateSpecial(CCSPtrArray<CCADATA> &ropCca, CCSPtrArray<CCADATA> &ropCcaSave, long lpFlightUrno, long lpBaseId = 0);
	void GetSpecialList(CCSPtrArray<CCADATA> &opCca, CString &opFieldDataList);
	bool DeleteSpecial(CString olFlightUrnoList);
	//void SetPreSelection(CTime &opFrom, CTime &opTo, CString &opFtyps);
	bool ReadAllFlights(const CString &opWhere);
	bool ReadCcas(char *pcpSelection = NULL, bool bpIgnoreViewSel = false);
	bool AddCcaInternal(CCADATA *prpCCA);

	void SetTableName(CString opTab)
	{
		strcpy(pcmTableName, opTab.GetBuffer(0));
	}
	void SetFilter(bool bpCommonOnly, const CString &ropFlnuList);

	bool GetCcasByFlnu(long lpFlnu, CCSPtrArray<CCADATA> &ropFlightCcaList);
	bool CheckDemands(const CCAFLIGHTDATA *prpFlight);
	bool DelAllDemByFlnu(long lpFlnu);

	// Private methods
private:
	CWnd *pomParent;
	int imDDXNew, imDDXChange, imDDXDelete;

    CMapPtrToPtr omUrnoMap;
    CMapStringToPtr omCkicMap;
	char pcmListOfFields[2048];
	CString omSelection;
	bool bmSelection;

	// for filtering
	bool bmCommonOnly;
	CString omFlnuList;
	bool IsPassFilter(const CCADATA &ropCca) const;


 	bool InsertInternal(CCADATA *prpCca, bool bpDdx = true);
	bool UpdateInternal(CCADATA *prpCca, bool bpDdx = true);
   

	void PrepareCcaData(CCADATA *prpCcaData);
    bool IsErrorCca(CCADATA *prpCca);

public:
	bool DeleteInternal(CCADATA *prpCca, bool bpDdx = true);


};

//---------------------------------------------------------------------------------------------------------


#endif //__CEDACCADATA__
