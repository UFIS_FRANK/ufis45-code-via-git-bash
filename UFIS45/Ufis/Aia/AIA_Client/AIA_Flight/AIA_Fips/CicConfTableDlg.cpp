// CicConfTableDlg.cpp : implementation file
//

#include "stdafx.h"
#include "fpms.h"
#include "CicConfTableDlg.h"
#include "SeasonDlg.h"
#include "RotationDlg.h"
#include "CcaCedaFlightData.h"
#include "DataSet.h"
#include "Utils.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CicConfTableDlg dialog


CicConfTableDlg::CicConfTableDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CicConfTableDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CicConfTableDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	isCreated = false;

    CDialog::Create(CicConfTableDlg::IDD, pParent);
	isCreated = true;

	m_key = "DialogPosition\\ConflictsCheckIn";
}

CicConfTableDlg::~CicConfTableDlg()
{
	delete pomTable;
}

void CicConfTableDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CicConfTableDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CicConfTableDlg, CDialog)
	//{{AFX_MSG_MAP(CicConfTableDlg)
	ON_WM_SIZE()
	ON_WM_CLOSE()
	ON_MESSAGE(WM_DRAGOVER, OnDragOver)
	ON_MESSAGE(WM_DROP, OnDrop)  
    ON_MESSAGE(WM_TABLE_DRAGBEGIN, OnTableDragBegin)
    ON_MESSAGE(WM_TABLE_LBUTTONDBLCLK, OnTableLButtonDblclk)
    ON_MESSAGE(WM_TABLE_LBUTTONDOWN, OnTableLButtonDown)
    ON_MESSAGE(WM_TABLE_RETURN_PRESSED, OnTableReturnPressed)
 	ON_WM_DESTROY()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CicConfTableDlg message handlers



void CicConfTableDlg::Activate()
{
	omViewer.ChangeViewTo("");
}


void CicConfTableDlg::Reset()
{
	omViewer.ClearAll();
}

void CicConfTableDlg::SaveToReg()
{
	BOOL ok = WriteDialogToReg((CWnd*) this, m_key);
}


void CicConfTableDlg::OnDestroy() 
{
	SaveToReg();
	CDialog::OnDestroy();
}


BOOL CicConfTableDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	m_key = "DialogPosition\\ConflictsCheckIn";

	CRect olRect;
	this->GetWindowRect(&olRect);
	GetDialogFromReg(olRect, m_key);
	SetWindowPos(&wndTop, olRect.left, olRect.top, olRect.Width(), olRect.Height(), SWP_HIDEWINDOW);// | SWP_NOMOVE );

	omDragDropObject.RegisterTarget(this, this);

	pomTable = new CCSTable;
	pomTable->SetSelectMode(LBS_MULTIPLESEL | LBS_EXTENDEDSEL);
	pomTable->SetHeaderSpacing(0);

	pomTable->bmMultiDrag = true;

    GetClientRect(&olRect);

	//olRect.top = olRect.top + imDialogBarHeight;
	//olRect.bottom = olRect.bottom;// - imDialogBarHeight;

    olRect.InflateRect(1, 1);     // hiding the CTable window border
    
	pomTable->SetTableData(this, olRect.left, olRect.right, olRect.top, olRect.bottom);

	omViewer.Attach(pomTable);

	omViewer.ChangeViewTo("");

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CicConfTableDlg::OnClose() 
{
	ShowWindow(SW_HIDE);	

//	omViewer.UnRegister();

	//CDialog::OnClose();
}

void CicConfTableDlg::GetCount(int& ipConf, int& ipWoDem)
{
	omViewer.GetCount(ipConf, ipWoDem);
}

////////////////////////////////////////////////////////////////////////////
// Dragbeginn einer Drag&Drop Aktion
//
LONG CicConfTableDlg::OnTableDragBegin(UINT wParam, LONG lParam)
{

	int ilCol;
	int ilLine;

	CCSPtrArray<DIACCADATA> olData;

	CICCONFTABLE_LINEDATA *prlLine = (CICCONFTABLE_LINEDATA*)pomTable->GetTextLineData(wParam);
	CPoint point;
    ::GetCursorPos(&point);
    pomTable->pomListBox->ScreenToClient(&point);    
    //ScreenToClient(&point);    

	if (prlLine != NULL)
	{
		ilLine = pomTable->GetLinenoFromPoint(point);
		ilCol = pomTable->GetColumnnoFromPoint(point);
	}
	else
		return 0L;


	if(prlLine->Urno != 0)
	{

		int ilItems[300];
		int ilAnz = 300;
		int ilAnzSel = 0;

		CListBox *polLB = pomTable->GetCTableListBox();
		if (polLB == NULL)
			return 0L;

		ilAnz = MyGetSelItems(*polLB, ilAnz, ilItems);
		if(ilAnz <= 0)
		{
			return 0L;
		}

		omDragDropObject.CreateDWordData(DIT_CCA_DEMAND, ilAnz);
		
		for(int i = 0; i < ilAnz; i++)
		{
			prlLine = (CICCONFTABLE_LINEDATA *)pomTable->GetTextLineData(ilItems[i]);
			if(prlLine->Urno != 0)
			{
				DIACCADATA *prlCca = ogCcaDiaFlightData.omCcaData.GetCcaByUrno(prlLine->Urno);

				//prlCca->Urno = ogBasicData.GetNextUrno(); --> CCAGANTT::ONDROP
				//prlCca->Flnu = prlLine->Urno;
/*					
				prlCca->Ckbs = prlLine->Ckbs;
				prlCca->Ckes = prlLine->Ckes;

				if(bgGatPosLocal) ogCcaDiaFlightData.omCcaData.StructLocalToUtc(prlCca);
*/

				if(prlCca == NULL)
				{
					int z = 0;
					return 0L;

				}
				else
				{

					if(strcmp(prlCca->Ctyp, "E") != 0)
					{
						omDragDropObject.AddDWord((long)prlLine->Urno);
					}
				}
				
//				olData.Add(prlCca);
			}
		}

		ogBcHandle.BufferBc();

		omDragDropObject.BeginDrag();

		ogBcHandle.ReleaseBuffer();

//		olData.DeleteAll();
	}
	return 0L;
}


////////////////////////////////////////////////////////////////////////////
// linke Maustaste auf der Flighttable gedr�ckt
//
LONG CicConfTableDlg::OnTableLButtonDblclk( UINT wParam, LPARAM lParam)
{
	UINT ipItem = wParam;
	CICCONFTABLE_LINEDATA *prlTableLine = NULL;
	prlTableLine = (CICCONFTABLE_LINEDATA *)pomTable->GetTextLineData(ipItem);
	if (prlTableLine != NULL)
	{
		CCAFLIGHTDATA *prlFlight = ogCcaDiaFlightData.GetFlightByUrno(prlTableLine->FlightUrno);;

		if(prlFlight != NULL)
		{
 			if (prlTableLine->Sto - CTimeSpan(2,0,0,0) < CTime::GetCurrentTime())
			{
				pogRotationDlg->NewData(this, prlFlight->Rkey, prlFlight->Urno, "D", bgGatPosLocal);
			}
			else
			{
				pogSeasonDlg->NewData(this, prlFlight->Rkey, prlFlight->Urno, DLG_CHANGE, bgGatPosLocal);
			}
		}


	}
	return 0L;
}




LONG CicConfTableDlg::OnTableLButtonDown(UINT wParam, LONG lParam)
{

	UINT ipItem = wParam;
	CICCONFTABLE_LINEDATA *prlTableLine = NULL;
	prlTableLine = (CICCONFTABLE_LINEDATA *)pomTable->GetTextLineData(ipItem);
	if (prlTableLine != NULL)
	{
		CCAFLIGHTDATA *prlFlight = ogCcaDiaFlightData.GetFlightByUrno(prlTableLine->Urno);;

		if(prlFlight != NULL)
			ogDdx.DataChanged((void *)this, SHOW_FLIGHT, (void*)&(prlFlight->Urno));
	}


	return 0L;

}

void CicConfTableDlg::OnSize(UINT nType, int cx, int cy) 
{
	if(isCreated != false)
	{
		CDialog::OnSize(nType, cx, cy);
	
		if (nType != SIZE_MINIMIZED)
		{
			pomTable->SetPosition(1, cx+1, 1/*m_nDialogBarHeight-1*/, cy+1);
		}
	}
}
LONG CicConfTableDlg::OnDragOver(UINT wParam, LONG lParam)
{
	int ilLine = -1;
	int ilCol = -1;
	int ilLogicLine = -1;
	CPoint olDropPosition;
    ::GetCursorPos(&olDropPosition);

   int ilClass = omDragDropObject.GetDataClass(); 
	if (ilClass == DIT_CCA_GANTT)
	{
		//long ilUrno = m_DragDropTarget.GetDataDWord(0);
		//GHDDATA *prlGhd = ogGhdData.GetJobByUrno(ilUrno);
		//if(prlGhd!= NULL)
		//{
		//	if(strcmp(prlGhd->Dtyp, "D") == 0)
		//	{
		//		return 0;
		//	}
		//	else
		//	{
		//		return -1L;
		//	}
		//}
		//TO DO: Pr�fen, ob DTY OK
		return 0;	// cannot interpret this object
	}

	return -1L;
}

LONG CicConfTableDlg::OnDrop(UINT wParam, LONG lParam)
{
	CCSDragDropCtrl *pomDragDropCtrl = &omDragDropObject;

	DROPEFFECT lpDropEffect = wParam;

	switch (pomDragDropCtrl->GetDataClass())
	{
	case DIT_CCA_GANTT: // Drop from this gantt to another point
		return ProcessDropCcaDuty(pomDragDropCtrl, lpDropEffect);
	}
    return -1L;

}

LONG CicConfTableDlg::ProcessDropCcaDuty(CCSDragDropCtrl *popDragDropCtrl, DROPEFFECT lpDropEffect)
{
	long ilUrno = popDragDropCtrl->GetDataDWord(0);
	DIACCADATA *prlCca = ogCcaDiaFlightData.omCcaData.GetCcaByUrno(ilUrno);
	if(prlCca != NULL)
	{
		ogDataSet.DeAssignCca(prlCca->Urno);
	}
	return 0L;
}


LONG CicConfTableDlg::OnTableReturnPressed( UINT wParam, LPARAM lParam)
{
	OnTableLButtonDblclk(wParam, lParam); 
	return 0L;
}
