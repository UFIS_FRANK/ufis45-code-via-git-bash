#ifndef _CedaFlightUtilsData_H_
#define _CedaFlightUtilsData_H_
 
#include "BasicData.h"
#include "CCSDefines.h"
#include "CCSGlobl.h"

 

/////////////////////////////////////////////////////////////////////////////
// Record structure declaration


 
struct FLIGHTUTILSDATA 
{
	char 	 Act3[5]; 	// Flugzeug-3-Letter Code (IATA)
	char 	 Act5[7]; 	// Flugzeug-5-Letter Code
	char 	 Adid[2]; 	// 
	char 	 Alc2[4]; 	// Fluggesellschaft (Airline 2-Letter Code)
	char 	 Alc3[5]; 	// Fluggesellschaft (Airline 3-Letter Code)
	char 	 Des3[5]; 	// Bestimmungsflughafen 3-Lettercode
	char 	 Des4[6]; 	// Bestimmungsflughafen 4-Lettercode
	char 	 Flno[11]; 	// komplette Flugnummer (Airline, Nummer, Suffix)
	char 	 Flns[3]; 	// Suffix
	char 	 Fltn[7]; 	// Flugnummer
	char 	 Ftyp[3]; 	// Type des Flugs [F, P, R, X, D, T, ...]
	char 	 Org3[5]; 	// Ausgangsflughafen 3-Lettercode
	char 	 Org4[6]; 	// Ausgangsflughafen 4-Lettercode
	char 	 Psta[7]; 	// Position Ankunft
	char 	 Pstd[7]; 	// Position Abflug
	char 	 Regn[14]; 	// LFZ-Kennzeichen
	long 	 Rkey; 		// Rotationsschl�ssel
	CTime 	 Stoa; 		// Planm��ige Ankunftszeit STA
	CTime 	 Stod; 		// Planm��ige Abflugzeit
	CTime 	 Tifa; 		// Zeitrahmen Ankunft
	CTime 	 Tifd; 		// Zeitrahmen Abflug
	long 	 Urno; 		// Eindeutige Datensatz-Nr.




	FLIGHTUTILSDATA(void)
	{ 
		memset(this,'\0',sizeof(*this));
		Stoa = TIMENULL;
		Stod = TIMENULL;
		Tifa = TIMENULL;
		Tifd = TIMENULL;
	}

}; // end FLIGHTUTILSDATA
	


/////////////////////////////////////////////////////////////////////////////
// Class declaration

class CedaFlightUtilsData: public CCSCedaData
{

public:

    CedaFlightUtilsData(); 
	~CedaFlightUtilsData();

	bool JoinFlight(long lpUrno1, long lpRkey1, long lpUrno2, long lpRkey2, bool bpSilent, char cpPart = ' ', bool bpUseGlobalData = false);

	bool SplitFlight(long lpUrno, long lpRkey, bool bpSilent, bool bpUseGlobalData = false);
	bool ReadAllSelectedRotations(CString& olRkeys);

	void ClearAll(void);


private:

    CMapPtrToPtr omUrnoMap;

    CCSPtrArray<FLIGHTUTILSDATA> omData;

	char pcmAftFieldList[1024];

   
	bool ReadFlights(char *pcpSelection);
	bool ReadRotation(long lpRkey);
	
	FLIGHTUTILSDATA *GetArrival(const FLIGHTUTILSDATA *prpFlight);
	FLIGHTUTILSDATA *GetDeparture(const FLIGHTUTILSDATA *prpFlight);
	FLIGHTUTILSDATA *GetFlightByUrno(long lpUrno);

	FLIGHTUTILSDATA *GetLastTowInRot(long lpUrno, long lpRkey);
	FLIGHTUTILSDATA *GetFirstTowInRot(long lpUrno, long lpRkey);

	bool UpdateFlight(FLIGHTUTILSDATA *prpFlight, FLIGHTUTILSDATA *prpFlightSave);


    // internal data access.
	bool AddFlightInternal(FLIGHTUTILSDATA *prpFlight);
	bool DeleteFlightInternal(long lpUrno);

	bool AddToKeyMap(FLIGHTUTILSDATA *prpFlight);
	bool DeleteFromKeyMap(const FLIGHTUTILSDATA *prpFlight);

};




#endif
