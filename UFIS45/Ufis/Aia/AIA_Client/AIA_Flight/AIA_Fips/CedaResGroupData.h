#ifndef _CEDARESGROUPDATA_H_
#define _CEDARESGROUPDATA_H_

#include "CCSCedadata.h"
#include "CCSPtrArray.h"

#define VCD_TEXT_MAXLENGTH 2000

struct ResGroupData;

struct VCDDATA {
    long    Urno;           // Unique Record Number
    char    Appn[34];       // name of application
    char    Ctyp[34];       // "RES_GROUPS"
	char    Ckey[34];		// Table and name of group 
    CTime	Vafr;           // Valid from
    CTime	Vato;           // Valid to
	char	Text[2001];		// Parameter string
	char	Pkno[34];		// User-ID
 
	// Constructors
	VCDDATA(void); 
 	VCDDATA(const ResGroupData &rrpResGroup);
};	

// only ckey and text needed in internal data structures
struct ResGroupData {
	CString Ctyp;
	CString Ckey;
	CString Text;
	bool fullHeight;

	// Constructors
	ResGroupData(void);
 	ResGroupData(const VCDDATA &rrpVcdData);
};



/////////////////////////////////////////////////////////////////////////////
// Class declaration

class CedaResGroupData: public CCSCedaData
{
 
 
public:
    CedaResGroupData();
 	~CedaResGroupData();

	bool GetGroupList(const CString &ropResName, CStringArray &ropGroupList) const;
	bool GetGroupResList(const CString &ropResName, const CString &ropGroupName, CPtrArray &ropUrnoList) const;
	bool GetGroupResList(const CString &ropResName, const CString &ropGroupName, bool& bpFullHeight) const;

	bool RemoveNotExistingGroups(const CString &ropResName, CStringArray &ropGroupList) const;


	bool SaveGroup(const CString &ropResName, const CString &ropGroupName, const CPtrArray &ropUrnoArray, bool& bpFullHeight);
	bool DeleteGroup(const CString &ropResName, const CString &ropGroupName);

	bool ReadGroups();

private:

    CCSPtrArray<ResGroupData> omData;
    CMapStringToPtr omCkeyMap;
  
	char pcmListOfFields[2048];

	ResGroupData *GetGroupData(const CString &ropResName, const CString &ropGroupName) const;

	bool InsertInternal(ResGroupData *prpResGroup);
	bool UpdateInternal(ResGroupData *prpResGroup, const VCDDATA &rrpVcdData);
	bool DeleteInternal(ResGroupData *prpResGroup);
	bool DeleteAllInternal();
	bool CheckMembers(ResGroupData *prpResGroup);
	
 
};

#endif
