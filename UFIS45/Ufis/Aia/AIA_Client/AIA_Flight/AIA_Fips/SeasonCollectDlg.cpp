// SeasonCollectDlg.cpp: Implementierungsdatei
//
// Modification History:
// 20-nov-00 rkr   PRF (Athen):	New Dialog Handling Agents, older one removed 
// 20-dec-00 rkr   PRF12(Athen):act-handling for rotation
// 06-feb-01 rkr   PRF F28 (Athen):	Tooltips for cells

#include "stdafx.h"
#include "fpms.h"
#include "SeasonCollectDlg.h"
#include "SeasonDlg.h"
#include "CCSTime.h"
#include "CCSDefines.h"
//#include "CCSCedadata.h" 
#include "CedaBasicData.h"
#include "AwBasicDataDlg.h"
#include "SeasonCollectAskDlg.h"
#include "BasicData.h"
#include "PrivList.h"
#include "resrc1.h"
#include "Utils.h"
#include "CedaFlightUtilsData.h"
 
#include "AskBox.h"
#include "RotationHAIDlg.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#define ID_DUPFLIGHT WM_MENU_RANGE_END+1

// Local function prototype
static void SeasonCollectCf(void *popInstance, int ipDDXType,
    void *vpDataPointer, CString &ropInstanceName);


/////////////////////////////////////////////////////////////////////////////
// Dialogfeld SeasonCollectDlg 


SeasonCollectDlg::SeasonCollectDlg(CWnd* pParent)
	: CDialog(SeasonCollectDlg::IDD, pParent)
{
	pomTable = NULL;
	bmChanged = false;
	
	ogSeasonCollectFlights.Register();	

    ogDdx.Register(this, S_DLG_GET_PREV, CString("COLLECTDLG"), CString("Flight Update/new"), SeasonCollectCf);
    ogDdx.Register(this, S_DLG_GET_NEXT, CString("COLLECTDLG"), CString("Flight Update/new"), SeasonCollectCf);
    ogDdx.Register(this, C_FLIGHT_CHANGE, CString("COLLECTDLG"), CString("Flight Update/new"), SeasonCollectCf);
    ogDdx.Register(this, C_FLIGHT_DELETE, CString("COLLECTDLG"), CString("Flight Update/new"), SeasonCollectCf);
    ogDdx.Register(this, DATA_RELOAD, CString("RotationTableViewer"), CString("Flight Update/new"), SeasonCollectCf);

	omLastSelRect.top = -1;
	omLastSelRect.left  = -1;
	omLastSelRect.bottom = -1;
	omLastSelRect.right  = -1;

	omUFRAdid = "A";

	bmInputJfno = false;
	bmInputVia = false;
	bmInputCca = false;
	imContPos = false;

	pomTable = NULL;
	pomJfnoTable = NULL;
	pomViaTableCtrl = NULL;
	pomCinsTable = NULL;

	//{{AFX_DATA_INIT(SeasonCollectDlg)
	m_Alc = _T("");
	m_Flns = _T("");
	m_Fltn = _T("");
	m_Offset = _T("");
	//}}AFX_DATA_INIT

	m_key = "DialogPosition\\BatchProcessing";
}


SeasonCollectDlg::~SeasonCollectDlg()
{
	ogSeasonCollectFlights.UnRegister();	
	ogDdx.UnRegister(this,NOTUSED);

	omInputVia.DeleteAll();
	omInputJfno.DeleteAll();
	omInputCca.DeleteAll();

	omInputData.RemoveAll();
	omInputField.RemoveAll();

	omTableFields.RemoveAll();

	CCSPtrArray<CCSCell> *prlCells;
	POSITION pos;
	void *pVoid;
	for( pos = omButtonMap.GetStartPosition(); pos != NULL; )
	{
		omButtonMap.GetNextAssoc( pos, pVoid , (void *&)prlCells );
		prlCells->RemoveAll();
		delete prlCells;
	}
	omButtonMap.RemoveAll();

	omCells.DeleteAll();
	omHButtons.DeleteAll();
	omVButtons.DeleteAll();
	omDayMap.RemoveAll();

	delete pomTable;
	pomTable = NULL;
	delete pomJfnoTable;
	pomJfnoTable = NULL;
	delete pomViaTableCtrl;
	pomViaTableCtrl = NULL;
	delete pomCinsTable;
	pomCinsTable = NULL;

	omMenuFlights.DestroyMenu();
}

void SeasonCollectDlg::SaveToReg() 
{
	BOOL ok = WriteDialogToReg((CWnd*) this, m_key);
}


BOOL SeasonCollectDlg::DestroyWindow() 
{
	SaveToReg();
	BOOL blRet =  CDialog::DestroyWindow();
	if(blRet)
	{
		delete this;
		pogSeasonCollectDlg = NULL;
	}
	return blRet;
}

void SeasonCollectDlg::OnCancel() 
{
	if(bmChanged)
	{
		AskBox olDlg(this, "Yes", "No", GetString(IDS_STRING942), GetString(ST_FRAGE));
 		if (olDlg.DoModal() != 1)
		{
 			return;
		}
	}

	DestroyWindow();
	
}

void SeasonCollectDlg::DataReload()
{
	LoadNewData( omAlc3, omFltn, omFlns, omAdid, omLoadStart, omFlno);
}


void SeasonCollectDlg::LoadNewData(CString opAlc3, CString opFltn, CString opFlns, CString opAdid, CTime opSto, CString opFlno)
{

	if(opAlc3.IsEmpty() || opFltn.IsEmpty() || opAdid.IsEmpty() || opSto == TIMENULL)
	{
		SeasonCollectAskDlg olDlg(this);

		if(olDlg.DoModal() == IDOK)
		{
			//omFlno = ogSeasonCollectFlights.CreateFlno(olDlg.m_Alc, olDlg.m_Fltn, olDlg.m_Flns);
			omAlc3 = olDlg.omAlc3;
			GetFltnInDBFormat(olDlg.m_Fltn, omFltn);
			omFlns = olDlg.m_Flns;

			omLoadStart = DateHourStringToDate(olDlg.m_From, CString("0001"));
			omLoadEnd   = DateHourStringToDate(olDlg.m_To,  CString("2359"));
			omAdid = olDlg.omAdid;
		}
		else
		{
			return;
		}
	
	}
	else
	{
		omAlc3 = opAlc3;
		omFltn = opFltn;
		omFlns = opFlns;
		omFlno = opFlno;

		omAdid = opAdid;

		CString olTo;
		CString olFrom;

		ogBCD.GetFieldBetween("SEA", "VPFR", "VPTO", CTimeToDBString(opSto, TIMENULL), "SEAS", omSeas );
		ogBCD.GetFields("SEA", "SEAS" , omSeas, "VPFR", "VPTO", olFrom, olTo);

		omLoadStart = DBStringToDateTime(olFrom);
		omLoadEnd   = DBStringToDateTime(olTo);

	}


	if (omFlns.IsEmpty())
		omFlns=" ";


	InitMatrtix();

	CString olCaption;
	
	
	olCaption.Format(GetString(IDS_STRING1338), omAlc3+omFltn+omFlns, omAdid, omLoadStart.Format("%d.%m.%Y"), omLoadEnd.Format("%d.%m.%Y"));
	CString olTimes;
	if (bgSeasonLocal)
		olTimes = GetString(IDS_STRING1921);
	else
		olTimes = GetString(IDS_STRING1920);
	olCaption += " ("+olTimes+")";
	SetWindowText(olCaption);

	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));

	ogSeasonCollectFlights.ReadAllFlights( omLoadStart,  omLoadEnd, omAlc3, omFltn, omFlns, omAdid, omFlno);



	CString olField;


	if(omAdid == "A")
	{
		olField = "AFLNO";
		omUFRAdid = "A";
	}
	else
	{
		olField = "DFLNO";
		omUFRAdid = "D";
	}

	//combobox

	int ilCount = m_CL_Showfield.GetCount();
	CString olTmp;

	for(int i = 0; i < ilCount; i++)
	{
		m_CL_Showfield.GetLBText(i, olTmp);
		if(olTmp.Right(5) == olField)
		{
			m_CL_Showfield.SetCurSel(i);
			break;
		}
	}


	FillMatrix(olField);

	InitTables(omUFRAdid);

	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));


	omLastSelRect.top = -1;
	omLastSelRect.left  = -1;
	omLastSelRect.bottom = -1;
	omLastSelRect.right  = -1;


	//SetWindowPos(&wndTop, 0,0,0,0, SWP_SHOWWINDOW | SWP_NOSIZE | SWP_NOMOVE );

	SetWndPos();

}

void SeasonCollectDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(SeasonCollectDlg)
	DDX_Control(pDX, IDC_FLNO, m_CB_NewFlno);
	DDX_Control(pDX, IDC_SPLIT, m_CB_Split);
	DDX_Control(pDX, IDC_JOIN, m_CB_Join);
	DDX_Control(pDX, IDC_STATIC_ORG, m_CS_Org);
	DDX_Control(pDX, IDC_STATIC_DES, m_CS_Des);
	DDX_Control(pDX, IDC_OFFSET, m_CE_Offset);
	DDX_Control(pDX, IDC_OFFSETSPIN, m_CB_OffsetSpin);
	DDX_Control(pDX, IDC_FLTN, m_CE_Fltn);
	DDX_Control(pDX, IDC_FLNS, m_CE_Flns);
	DDX_Control(pDX, IDC_ALC, m_CE_Alc);
	DDX_Control(pDX, IDC_DELETE, m_CB_Delete);
	DDX_Control(pDX, IDOK, m_CB_OK);
	DDX_Control(pDX, IDC_AGENTS, m_CB_Agents);
	DDX_Control(pDX, IDC_PLANUNG, m_CR_Planung);
	DDX_Control(pDX, IDC_BETRIEB, m_CR_Betrieb);
	DDX_Control(pDX, IDC_PROGNOSE, m_CR_Prognose);
	DDX_Control(pDX, IDC_NOOP, m_CR_Noop);
	DDX_Control(pDX, IDC_CINSLABEL, m_CS_CinsLabel);
	DDX_Control(pDX, IDC_CINSMAL4, m_CB_CinsMal4);
	DDX_Control(pDX, IDC_CINSBORDER, m_CS_CinsBorder);
	DDX_Control(pDX, IDC_JFNO_BORDER, m_CS_JfnoBorder);
	DDX_Control(pDX, IDC_UPDOWN, m_CB_UpDown);
	DDX_Control(pDX, IDC_NUMBER, m_CS_Number);
	DDX_Control(pDX, IDC_TABLE, m_CS_Table);
	DDX_Control(pDX, IDC_MATRIX, m_CS_Matrix);
	DDX_Control(pDX, IDC_SHOWFIELD, m_CL_Showfield);
	DDX_Text(pDX, IDC_ALC, m_Alc);
	DDX_Text(pDX, IDC_FLNS, m_Flns);
	DDX_Text(pDX, IDC_FLTN, m_Fltn);
	DDX_Text(pDX, IDC_OFFSET, m_Offset);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(SeasonCollectDlg, CDialog)
	//{{AFX_MSG_MAP(SeasonCollectDlg)
	ON_BN_CLICKED(IDC_DESELECT, OnDeselect)
	ON_WM_LBUTTONUP()
	ON_MESSAGE(WM_CELL_LBUTTONDBLCLK, OnCellLButtonDblclk)
	ON_CBN_SELCHANGE(IDC_SHOWFIELD, OnSelchangeShowfield)
	ON_BN_CLICKED(IDC_SELECTALL, OnSelectall)
	ON_BN_CLICKED(IDC_FLNO, OnFlno)
	ON_WM_LBUTTONDOWN()
	ON_WM_VSCROLL()
	ON_BN_CLICKED(IDC_TABLESDESELECT, OnTablesdeselect)
	ON_BN_CLICKED(IDC_TABLESRESET, OnTablesreset)
	ON_BN_CLICKED(IDC_CINSMAL4, OnCinsmal4)
	ON_BN_CLICKED(IDC_AGENTS, OnAgents)
	ON_MESSAGE(WM_EDIT_CHANGED, OnEditChanged)
	ON_MESSAGE(WM_BUTTON_CLICK, OnButtonClick)
	ON_BN_CLICKED(IDC_DELETE, OnDelete)
	ON_BN_CLICKED(IDC_BETRIEB, OnBetrieb)
	ON_BN_CLICKED(IDC_NOOP, OnNoop)
	ON_BN_CLICKED(IDC_PROGNOSE, OnPrognose)
	ON_BN_CLICKED(IDC_PLANUNG, OnPlanung)
	ON_MESSAGE(WM_TABLE_IPEDIT_KILLFOCUS, OnTableIPEditKillfocus)
	ON_MESSAGE(WM_TABLE_SELCHANGE, OnTableSelChanged)
	ON_MESSAGE(WM_CELL_MOUSEMOVE, OnCellMouseMove)
	ON_MESSAGE(WM_CELL_LBUTTONDOWN, OnCellLButtonDown)
	ON_MESSAGE(WM_CELL_LBUTTONUP, OnCellLButtonUp)
	ON_BN_CLICKED(IDC_SPLIT, OnSplit)
	ON_BN_CLICKED(IDC_JOIN, OnJoin)
	ON_COMMAND_RANGE(WM_MENU_RANGE_START, WM_MENU_RANGE_END, OnMenu)
	ON_MESSAGE(WM_CELL_RBUTTONDOWN, OnCellRButtonDown)
	ON_MESSAGE(WM_BUTTON_RBUTTONDOWN, OnRightButtonDown)
	ON_WM_SIZE()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Behandlungsroutinen f�r Nachrichten SeasonCollectDlg 

BOOL SeasonCollectDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
    EnableToolTips(TRUE);
	
	m_CE_Alc.SetTypeToString("X|#X|#X|#",3,2);
	m_CE_Fltn.SetTypeToString("#####",5,1);
	m_CE_Flns.SetTypeToString("X",1,0);


	// Fill combobox with valid fields

	m_CB_UpDown.SetRange(1,10);
	m_CS_Number.ShowWindow(SW_HIDE);
	m_CB_UpDown.ShowWindow(SW_HIDE);

	//m_CL_Showfield.SetFont(&ogCourier_Regular_8);
	
	if(ogPrivList.GetStat("SEASON_BATCH_Show_AFLNO") != '-')
	{
		m_CL_Showfield.AddString(GetString(IDS_STRING1260)+"                                               AFLNO");
		m_CL_Showfield.AddString(GetString(IDS_STRING2127)+"                                               AFLTI");
	}
	if(ogPrivList.GetStat("SEASON_BATCH_Show_ACSGN") != '-')
		m_CL_Showfield.AddString(GetString(IDS_BATCH_ACSGN)+"                                               ACSGN");//Ankunft callsign
	if(ogPrivList.GetStat("SEASON_BATCH_Show_AJFNO") != '-')
		m_CL_Showfield.AddString(GetString(IDS_STRING1261)+"                                               AJFNO");
	if(ogPrivList.GetStat("SEASON_BATCH_Show_AFTYP") != '-')
		m_CL_Showfield.AddString(GetString(IDS_STRING1262)+"                                               AFTYP");
	if(ogPrivList.GetStat("SEASON_BATCH_Show_ASTOA") != '-')
		m_CL_Showfield.AddString(GetString(IDS_STRING1263)+"                                               ASTOA");
	if(ogPrivList.GetStat("SEASON_BATCH_Show_ASTOD") != '-')
		m_CL_Showfield.AddString(GetString(IDS_STRING1264)+"                                               ASTOD");
	if(ogPrivList.GetStat("SEASON_BATCH_Show_AETAU") != '-')
		m_CL_Showfield.AddString(GetString(IDS_STRING1265)+"                                               AETAU");
	if(ogPrivList.GetStat("SEASON_BATCH_Show_AORG3") != '-')
		m_CL_Showfield.AddString(GetString(IDS_STRING1266)+"                                               AORG3");
	if(ogPrivList.GetStat("SEASON_BATCH_Show_AVIAL") != '-')
		m_CL_Showfield.AddString(GetString(IDS_STRING1267)+"                                               AVIAL");
	if(ogPrivList.GetStat("SEASON_BATCH_Show_AVIAN") != '-')
		m_CL_Showfield.AddString(GetString(IDS_STRING1268)+"                                                AVIAN");
	if(ogPrivList.GetStat("SEASON_BATCH_Show_AACT3") != '-')
		m_CL_Showfield.AddString(GetString(IDS_STRING1269)+"                                               AACT3");
	if(ogPrivList.GetStat("SEASON_BATCH_Show_ANOSE") != '-')
		m_CL_Showfield.AddString(GetString(IDS_STRING1270)+"                                           ANOSE");
	if(ogPrivList.GetStat("SEASON_BATCH_Show_ATTYP") != '-')
		m_CL_Showfield.AddString(GetString(IDS_STRING1271)+"                                               ATTYP");
	if(ogPrivList.GetStat("SEASON_BATCH_Show_AHTYP") != '-')
		m_CL_Showfield.AddString(GetString(IDS_STRING1272)+"                                               AHTYP");
	if(ogPrivList.GetStat("SEASON_BATCH_Show_ASTYP") != '-')
		m_CL_Showfield.AddString(GetString(IDS_STRING1273)+"                                               ASTYP");
	if(ogPrivList.GetStat("SEASON_BATCH_Show_ASTEV") != '-')
		m_CL_Showfield.AddString(GetString(IDS_STRING1274)+"                                               ASTEV");
	if(ogPrivList.GetStat("SEASON_BATCH_Show_APSTA") != '-')
		m_CL_Showfield.AddString(GetString(IDS_STRING1275)+"                                               APSTA");
	if(ogPrivList.GetStat("SEASON_BATCH_Show_AGTA1") != '-')
		m_CL_Showfield.AddString(GetString(IDS_STRING1276)+"                                               AGTA1");
	if(ogPrivList.GetStat("SEASON_BATCH_Show_AGTA2") != '-')
		m_CL_Showfield.AddString(GetString(IDS_STRING1277)+"                                               AGTA2");//Ankunft Port/Gate2
	if(ogPrivList.GetStat("SEASON_BATCH_Show_ABLT1") != '-')
		m_CL_Showfield.AddString(GetString(IDS_STRING1278)+"                                               ABLT1");//Ankunft Gep�ckband 1
	if(ogPrivList.GetStat("SEASON_BATCH_Show_ABLT2") != '-')
		m_CL_Showfield.AddString(GetString(IDS_STRING1279)+"                                               ABLT2");//Ankunft Gep�ckband 2
	if(ogPrivList.GetStat("SEASON_BATCH_Show_AEXT1") != '-')
		m_CL_Showfield.AddString(GetString(IDS_STRING1280)+"                                               AEXT1");//Ankunft Ausgang 1
	if(ogPrivList.GetStat("SEASON_BATCH_Show_AEXT2") != '-')
		m_CL_Showfield.AddString(GetString(IDS_STRING1281)+"                                               AEXT2");//Ankunft Ausgang 2
	if(ogPrivList.GetStat("SEASON_BATCH_Show_AISRE") != '-')
		m_CL_Showfield.AddString(GetString(IDS_STRING1282)+"                                               AISRE");//Ankunft Bemerkungskennzeichen


	if(ogPrivList.GetStat("SEASON_BATCH_Show_DFLNO") != '-')
	{
		m_CL_Showfield.AddString(GetString(IDS_STRING1283) + "                                             DFLNO");//Abflug  Flugnummer
		m_CL_Showfield.AddString(GetString(IDS_STRING2128) + "                                             DFLTI");
	}
	if(ogPrivList.GetStat("SEASON_BATCH_Show_DCSGN") != '-')
		m_CL_Showfield.AddString(GetString(IDS_BATCH_DCSGN)+"                                              DCSGN");//Abflug callsign
	if(ogPrivList.GetStat("SEASON_BATCH_Show_DJFNO") != '-')
		m_CL_Showfield.AddString(GetString(IDS_STRING1284) + "                                             DJFNO");//Abflug  Weitere Flugnummern
	if(ogPrivList.GetStat("SEASON_BATCH_Show_DFTYP") != '-')
		m_CL_Showfield.AddString(GetString(IDS_STRING1285) + "                                             DFTYP");//Abflug  Flugstatus
	if(ogPrivList.GetStat("SEASON_BATCH_Show_DSTOA") != '-')
		m_CL_Showfield.AddString(GetString(IDS_STRING1286) + "                                             DSTOA");//Abflug  Plan. Ankunftszeit
	if(ogPrivList.GetStat("SEASON_BATCH_Show_DSTOD") != '-')
		m_CL_Showfield.AddString(GetString(IDS_STRING1287) + "                                             DSTOD");//Abflug  Plan. Abflugsszeit
	if(ogPrivList.GetStat("SEASON_BATCH_Show_DETDU") != '-')
		m_CL_Showfield.AddString(GetString(IDS_STRING1288) + "                                             DETDU");//Abflug  Erwartete Abflugsszeit
	if(ogPrivList.GetStat("SEASON_BATCH_Show_DDES3") != '-')
		m_CL_Showfield.AddString(GetString(IDS_STRING1289) + "                                             DDES3");//Abflug  Bestimmungsflughafen
	if(ogPrivList.GetStat("SEASON_BATCH_Show_DVIAL") != '-')
		m_CL_Showfield.AddString(GetString(IDS_STRING1290) + "                                                DVIAL");//Abflug  Vias
	if(ogPrivList.GetStat("SEASON_BATCH_Show_DVIAN") != '-')
		m_CL_Showfield.AddString(GetString(IDS_STRING1291) + "                                             DVIAN");//Abflug  Anzahl Vias
	if(ogPrivList.GetStat("SEASON_BATCH_Show_DACT3") != '-')
		m_CL_Showfield.AddString(GetString(IDS_STRING1292) + "                                             DACT3");//Abflug  Flugzeugtyp
	if(ogPrivList.GetStat("SEASON_BATCH_Show_DNOSE") != '-')
		m_CL_Showfield.AddString(GetString(IDS_STRING1293) + "                                         DNOSE");//Abflug Anzahl Sitze

	if(ogPrivList.GetStat("SEASON_BATCH_Show_DTTYP") != '-')
		m_CL_Showfield.AddString(GetString(IDS_STRING1294) + "                                             DTTYP");//Abflug  Verkehrsart
	if(ogPrivList.GetStat("SEASON_BATCH_Show_DHTYP") != '-')
		m_CL_Showfield.AddString(GetString(IDS_STRING1295) + "                                             DHTYP");//Abflug  Abfertigunsart
	if(ogPrivList.GetStat("SEASON_BATCH_Show_DSTYP") != '-')
		m_CL_Showfield.AddString(GetString(IDS_STRING1296) + "                                             DSTYP");//Abflug  Servicetyp
	if(ogPrivList.GetStat("SEASON_BATCH_Show_DSTEV") != '-')
		m_CL_Showfield.AddString(GetString(IDS_STRING1297) + "                                             DSTEV");//Abflug  Statistik
	if(ogPrivList.GetStat("SEASON_BATCH_Show_DPSTD") != '-')
		m_CL_Showfield.AddString(GetString(IDS_STRING1298) + "                                             DPSTD");//Abflug  Position
	if(ogPrivList.GetStat("SEASON_BATCH_Show_DGTD1") != '-')
		m_CL_Showfield.AddString(GetString(IDS_STRING1299) + "                                             DGTD1");//Abflug  Gate1
	if(ogPrivList.GetStat("SEASON_BATCH_Show_DGTD2") != '-')
		m_CL_Showfield.AddString(GetString(IDS_STRING1300) + "                                             DGTD2");//Abflug  Gate2
	if(ogPrivList.GetStat("SEASON_BATCH_Show_DWRO1") != '-')
		m_CL_Showfield.AddString(GetString(IDS_STRING1301) + "                                             DWRO1");//Abflug  Warteraum
	if(ogPrivList.GetStat("SEASON_BATCH_Show_DCKIF") != '-')
		m_CL_Showfield.AddString(GetString(IDS_STRING1302) + "                                             DCKIF");//Abflug  Checkin Schalter von/bis
	if(ogPrivList.GetStat("SEASON_BATCH_Show_DISRE") != '-')
		m_CL_Showfield.AddString(GetString(IDS_STRING1303) + "                                             DISRE");//Abflug  Bemerkungskennzeichen



	CStringArray olHeader;
	olHeader.Add(" ");
	olHeader.Add(GetString(IDS_STRING1304));//"Mo"
	olHeader.Add(GetString(IDS_STRING1305));//"Di"
	olHeader.Add(GetString(IDS_STRING1306));//"Mi"
	olHeader.Add(GetString(IDS_STRING1307));//"Do"
	olHeader.Add(GetString(IDS_STRING1308));//"Fr"
	olHeader.Add(GetString(IDS_STRING1309));//"Sa"
	olHeader.Add(GetString(IDS_STRING1310));//"So"

	omPosMap.RemoveAll();	

	char buffer[30];
	CCSCell *prlCell;
	CCSPtrArray<CCSCell> *prlVCells;
	CCSPtrArray<CCSCell> *prlHCells;

	CCSButtonCtrl *prlButton;
	CRect olTable1Rect;
	m_CS_Matrix.GetWindowRect(olTable1Rect);

	ScreenToClient(olTable1Rect);

	olTable1Rect.left += 2; 
	olTable1Rect.top  += 2; 


	CRect olCellRect;

	int ilLastX = olTable1Rect.left;
	int ilLastY = olTable1Rect.top;

	int ilDxB = (olTable1Rect.right - olTable1Rect.left) / 7; 
	int ilDx  = (olTable1Rect.right - olTable1Rect.left - ilDxB) / 7; 
	int ilDy  = (olTable1Rect.bottom - olTable1Rect.top) / 33; 


	int ilColumn	= 0;
	int ilLine		= 0;


	for(int i = 1; ilLine <= 32; i++)
	{
		if(ilLine == 0)
		{
			if(ilColumn == 0)
			{
				ilLastX += ilDxB;
				olCellRect.top		= ilLastY;		
				olCellRect.left		= ilLastX - ilDxB;		
				olCellRect.right	= ilLastX ;		
   				olCellRect.bottom	= ilLastY + ilDy;		
			}			
			else
			{
				ilLastX += ilDx;
				olCellRect.top		= ilLastY;		
				olCellRect.left		= ilLastX - ilDx;		
				olCellRect.right	= ilLastX ;		
				olCellRect.bottom	= ilLastY + ilDy;		
			}			
			prlButton = new CCSButtonCtrl;
			prlButton->Create(olHeader[ilColumn], WS_CHILD | WS_VISIBLE | WS_BORDER, olCellRect, this, 17);
			prlButton->SetFont(&ogMSSansSerif_Bold_8,TRUE);
			omHButtons.Add(prlButton);
			prlHCells = new CCSPtrArray<CCSCell>;
			omButtonMap.SetAt(prlButton, (void*&)prlHCells);
			ilColumn++;
		}
		else
		{
			if(ilColumn == 0) // Button on left side
			{
				prlButton = new CCSButtonCtrl;
				ilLastX += ilDxB;
				olCellRect.top		= ilLastY;		
				olCellRect.left		= ilLastX - ilDxB;		
				olCellRect.right	= ilLastX;		
				olCellRect.bottom	= ilLastY + ilDy;		
				ilColumn++;
				prlButton->Create("", WS_CHILD | WS_VISIBLE | WS_BORDER, olCellRect, this, 18);
				prlButton->SetFont(&ogMSSansSerif_Bold_8,TRUE);
				omVButtons.Add(prlButton);
				prlVCells = new CCSPtrArray<CCSCell>;
				omButtonMap.SetAt(prlButton, (void*&)prlVCells);

			}
			else // Cells
			{
				ilLastX += ilDx;
				olCellRect.top		= ilLastY;		
				olCellRect.left		= ilLastX - ilDx;		
				olCellRect.right	= ilLastX;		
				olCellRect.bottom	= ilLastY + ilDy;		
				prlCell = new CCSCell;
				prlCell->Create(NULL,"", WS_CHILD | WS_VISIBLE | WS_BORDER  , olCellRect, this, 19);
				prlCell->SetColumn(ilColumn);
				prlCell->SetLine(ilLine);
				sprintf(buffer, "%d,%d", ilLine, ilColumn);
				omPosMap.SetAt(buffer, (void*&)prlCell);
				omCells.Add(prlCell);

				prlVCells->Add(prlCell);

				if(omButtonMap.Lookup(&omHButtons[ilColumn], (void*&) prlHCells) == TRUE)
				{
					prlHCells->Add(prlCell);
				}

				ilColumn++;
			}
		}
		if(ilColumn >= 8) 
		{
			ilColumn = 0;
			ilLine++;
			ilLastY += ilDy;
			ilLastX = olTable1Rect.left;
		}
	}

	CreateTables();
	InitTables(omUFRAdid);


	SetWndStatAll(ogPrivList.GetStat("SEASON_BATCH_CB_Join"),m_CE_Offset);
	SetWndStatAll(ogPrivList.GetStat("SEASON_BATCH_CB_Join"),m_CB_OffsetSpin);
	SetWndStatAll(ogPrivList.GetStat("SEASON_BATCH_CB_Join"),m_CB_Join);
	SetWndStatAll(ogPrivList.GetStat("SEASON_BATCH_CB_Join"),m_CE_Alc);
	SetWndStatAll(ogPrivList.GetStat("SEASON_BATCH_CB_Join"),m_CE_Fltn);
	SetWndStatAll(ogPrivList.GetStat("SEASON_BATCH_CB_Join"),m_CE_Flns);
	
	SetWndStatAll(ogPrivList.GetStat("SEASON_BATCH_CB_Delete"),m_CB_Delete);
	SetWndStatAll(ogPrivList.GetStat("SEASON_BATCH_CB_Split"),m_CB_Split);
	SetWndStatAll(ogPrivList.GetStat("SEASON_BATCH_CB_Save"),m_CB_OK);
	SetWndStatAll(ogPrivList.GetStat("SEASON_BATCH_CB_View"),m_CB_NewFlno);
	SetWndStatAll(ogPrivList.GetStat("SEASON_BATCH_CB_Agents"),m_CB_Agents);

	SetWndPos();
//##########
	m_resizeHelper.Init(this->m_hWnd);
	m_resizeHelper.Fix(IDC_SHOWFIELD,DlgResizeHelper::kNoHFix,DlgResizeHelper::kHeight);

	CRect olRect;
	this->GetClientRect(&olRect);
	this->GetWindowRect(&olRect);
//	CRect olRect = CRect(0, 65, ::GetSystemMetrics(SM_CXSCREEN), ::GetSystemMetrics(SM_CYSCREEN));
	GetDialogFromReg(olRect, m_key);
	SetWindowPos(&wndTop, olRect.left, olRect.top, olRect.Width(), olRect.Height(), SWP_SHOWWINDOW);// | SWP_NOMOVE );
//##########
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX-Eigenschaftenseiten sollten FALSE zur�ckgeben
}

void SeasonCollectDlg::OnSize(UINT nType, int cx, int cy) 
{
	CDialog::OnSize(nType, cx, cy);
	
	// TODO: Add your message handler code here
	m_resizeHelper.OnSize();
	if (this->pomTable != NULL && ::IsWindow(this->pomTable->m_hWnd))
	{
		CRect olrectTable;
		m_CS_Table.GetWindowRect(&olrectTable);
		ScreenToClient(olrectTable);
		olrectTable.DeflateRect(2,2);     // hiding the CTable window border
		this->pomTable->SetPosition(olrectTable.left, olrectTable.right, olrectTable.top, olrectTable.bottom);

		this->pomTable->DisplayTable();
	}

	if (this->pomJfnoTable != NULL && ::IsWindow(this->pomJfnoTable->m_hWnd))
	{
		CRect olrectTable;
		m_CS_JfnoBorder.GetWindowRect(&olrectTable);
		ScreenToClient(olrectTable);
		olrectTable.DeflateRect(2,2);     // hiding the CTable window border
		this->pomJfnoTable->SetPosition(olrectTable.left, olrectTable.right, olrectTable.top, olrectTable.bottom);

		this->pomJfnoTable->DisplayTable();
	}

	if (this->pomCinsTable != NULL && ::IsWindow(this->pomCinsTable->m_hWnd))
	{
		CRect olrectTable;
		m_CS_CinsBorder.GetWindowRect(&olrectTable);
		ScreenToClient(olrectTable);
		olrectTable.DeflateRect(2,2);     // hiding the CTable window border
		this->pomCinsTable->SetPosition(olrectTable.left, olrectTable.right, olrectTable.top, olrectTable.bottom);

		this->pomCinsTable->DisplayTable();
	}

	if (this->pomViaTableCtrl != NULL && ::IsWindow(this->pomViaTableCtrl->m_hWnd))
	{
		CRect olrectTable;
		pomViaTableCtrl->GetWindowRect(&olrectTable);
		ScreenToClient(olrectTable);
		olrectTable.DeflateRect(2,2);     // hiding the CTable window border
		this->pomViaTableCtrl->SetPosition(olrectTable);
	}


	this->Invalidate();

}

void SeasonCollectDlg::InitTables(CString opAdid)
{

	CCSPtrArray<TABLE_COLUMN> olColList;
	TABLE_COLUMN rlColumnData;

	CCSEDIT_ATTRIB rlAttrib;
	rlAttrib.Style = ES_UPPERCASE;


	TABLE_COLUMN rlTableColumn;

	olColList.New( rlTableColumn);
	olColList.New( rlTableColumn);

	olColList[0].blIsEditable = false;	
	olColList[1].blIsEditable = true;	

	olColList[0].Alignment = COLALIGN_LEFT;
	olColList[1].Alignment = COLALIGN_LEFT;

	pomTable->ResetContent();


	omTableFields.RemoveAll();


	if(opAdid == "A")
	{
		pomViaTableCtrl->SetSecStat(ogPrivList.GetStat("SEASON_BATCH_Update_AVIAL"));

		if(ogPrivList.GetStat("SEASON_BATCH_Update_AFLNO") == '1')
		{
			omTableFields.Add("ALC");
			olColList[0].Text = GetString(IDS_STRING1311);//"Flugnummer/Fluggesellschaft";
			rlAttrib.Type = KT_STRING;
			rlAttrib.Format = "XXX";
			rlAttrib.TextMinLenght = 0;
			rlAttrib.TextMaxLenght = 3;
			if(ogPrivList.GetStat("SEASON_BATCH_Update_AFLNO") == '1')
				olColList[1].blIsEditable = true;	
			else
				olColList[1].blIsEditable = false;	
			olColList[1].EditAttrib = rlAttrib;
			pomTable->AddTextLine(olColList, NULL);

			omTableFields.Add("FLTN");
			olColList[0].Text = GetString(IDS_STRING1312);//"Flugnummer/Nummerisch.";
			rlAttrib.Type = KT_STRING;
			rlAttrib.Format = "#####";
			rlAttrib.TextMinLenght = 0;
			rlAttrib.TextMaxLenght = 5;
			if(ogPrivList.GetStat("SEASON_BATCH_Update_AFLNO") == '1')
				olColList[1].blIsEditable = true;	
			else
				olColList[1].blIsEditable = false;	
			olColList[1].EditAttrib = rlAttrib;
			pomTable->AddTextLine(olColList, NULL);
		
			omTableFields.Add("FLNS");
			olColList[0].Text = GetString(IDS_STRING1313);//"Flugnummer/Suffix";
			rlAttrib.Type = KT_STRING;
			rlAttrib.Format = "X";
			rlAttrib.TextMinLenght = 0;
			rlAttrib.TextMaxLenght = 1;
			if(ogPrivList.GetStat("SEASON_BATCH_Update_AFLNO") == '1')
				olColList[1].blIsEditable = true;	
			else
				olColList[1].blIsEditable = false;	
			olColList[1].EditAttrib = rlAttrib;
			pomTable->AddTextLine(olColList, NULL);

			omTableFields.Add("FLTI");
			olColList[0].Text = GetString(IDS_STRING2129);//"Flight ID";
			rlAttrib.Type = KT_STRING;
			rlAttrib.Format = "X";
			rlAttrib.TextMinLenght = 0;
			rlAttrib.TextMaxLenght = 1;
			if(ogPrivList.GetStat("SEASON_BATCH_Update_AFLNO") == '1')
				olColList[1].blIsEditable = true;	
			else
				olColList[1].blIsEditable = false;	
			olColList[1].EditAttrib = rlAttrib;
			pomTable->AddTextLine(olColList, NULL);

		}

		if(ogPrivList.GetStat("SEASON_BATCH_Update_ACSGN") == '1')
		{
			omTableFields.Add("CSGN");
			olColList[0].Text = GetString(IDS_CALLSIGN);//"callsign
			rlAttrib.Type = KT_STRING;
			rlAttrib.Format = "XXXXXXXX";
			rlAttrib.TextMinLenght = 0;
			rlAttrib.TextMaxLenght = 8;
			if(ogPrivList.GetStat("SEASON_BATCH_Update_ACSGN") == '1')
				olColList[1].blIsEditable = true;	
			else
				olColList[1].blIsEditable = false;	
			olColList[1].EditAttrib = rlAttrib;
			pomTable->AddTextLine(olColList, NULL);
		}
			
		if(ogPrivList.GetStat("SEASON_BATCH_Update_ASTOA") == '1')
		{
			omTableFields.Add("STOA");
			olColList[0].Text = GetString(IDS_STRING1314);//"Plan. Ankuftszeit";
			rlAttrib.Type = KT_TIME;
			rlAttrib.ChangeDay	= true;
			rlAttrib.TextMaxLenght = 7;
			if(ogPrivList.GetStat("SEASON_BATCH_Update_ASTOA") == '1')
				olColList[1].blIsEditable = true;	
			else
				olColList[1].blIsEditable = false;	
			olColList[1].EditAttrib = rlAttrib;
			pomTable->AddTextLine(olColList, NULL);
		}

		if(ogPrivList.GetStat("SEASON_BATCH_Update_ASTOD") == '1')
		{
			omTableFields.Add("STOD");
			olColList[0].Text = GetString(IDS_STRING1315);//"Plan. Abflugszeit";
			rlAttrib.Type = KT_TIME;
			rlAttrib.ChangeDay	= true;
			rlAttrib.TextMaxLenght = 7;
			if(ogPrivList.GetStat("SEASON_BATCH_Update_ASTOD") == '1')
				olColList[1].blIsEditable = true;	
			else
				olColList[1].blIsEditable = false;	
			olColList[1].EditAttrib = rlAttrib;
			pomTable->AddTextLine(olColList, NULL);
		}

		if(ogPrivList.GetStat("SEASON_BATCH_Update_AETAU") == '1')
		{
			omTableFields.Add("ETAU");
			olColList[0].Text = GetString(IDS_STRING1316);//"Erwartete Ankunftszeit";
			rlAttrib.Type = KT_TIME;
			rlAttrib.ChangeDay	= true;
			rlAttrib.TextMaxLenght = 7;
			if(ogPrivList.GetStat("SEASON_BATCH_Update_AETAU") == '1')
				olColList[1].blIsEditable = true;	
			else
				olColList[1].blIsEditable = false;	
			olColList[1].EditAttrib = rlAttrib;
			pomTable->AddTextLine(olColList, NULL);
		}

		if(ogPrivList.GetStat("SEASON_BATCH_Update_AORG3") == '1')
		{
			omTableFields.Add("ORG3");
			olColList[0].Text = GetString(IDS_STRING1317);//"Herkunftsflughafen";
			rlAttrib.Type = KT_STRING;
			rlAttrib.Format = "XXX";
			rlAttrib.TextMinLenght = 0;
			rlAttrib.TextMaxLenght = 3;
			if(ogPrivList.GetStat("SEASON_BATCH_Update_AORG3") == '1')
				olColList[1].blIsEditable = true;	
			else
				olColList[1].blIsEditable = false;	
			olColList[1].EditAttrib = rlAttrib;
			pomTable->AddTextLine(olColList, NULL);
		}
		
		if(ogPrivList.GetStat("SEASON_BATCH_Update_AACT3") == '1')
		{
			omTableFields.Add("ACT3");
			olColList[0].Text = GetString(IDS_STRING1318);//"Flugzeugtyp (IATA)";
			rlAttrib.Format = "XXX";
			rlAttrib.TextMinLenght = 0;
			rlAttrib.TextMaxLenght = 3;
			if(ogPrivList.GetStat("SEASON_BATCH_Update_AACT3") == '1')
				olColList[1].blIsEditable = true;	
			else
				olColList[1].blIsEditable = false;	
			olColList[1].EditAttrib = rlAttrib;
			pomTable->AddTextLine(olColList, NULL);
		}

		if(ogPrivList.GetStat("SEASON_BATCH_Update_ANOSE") == '1')
		{
			omTableFields.Add("NOSE");
			olColList[0].Text = GetString(IDS_STRING400);//"Anzahl Sitze";
			rlAttrib.Format = "###";
			rlAttrib.TextMinLenght = 0;
			rlAttrib.TextMaxLenght = 3;
			if(ogPrivList.GetStat("SEASON_BATCH_Update_ANOSE") == '1')
				olColList[1].blIsEditable = true;	
			else
				olColList[1].blIsEditable = false;	
			olColList[1].EditAttrib = rlAttrib;
			pomTable->AddTextLine(olColList, NULL);
		}

		if(ogPrivList.GetStat("SEASON_BATCH_Update_ATTYP") == '1')
		{
			omTableFields.Add("TTYP");
			olColList[0].Text = GetString(IDS_STRING1319);//"Verkehrsart";
			rlAttrib.Format = "XXXXX";
			rlAttrib.TextMinLenght = 0;
			rlAttrib.TextMaxLenght = 5;
			if(ogPrivList.GetStat("SEASON_BATCH_Update_ATTYP") == '1')
				olColList[1].blIsEditable = true;	
			else
				olColList[1].blIsEditable = false;	
			olColList[1].EditAttrib = rlAttrib;
			pomTable->AddTextLine(olColList, NULL);
		}

		if(ogPrivList.GetStat("SEASON_BATCH_Update_AHTYP") == '1')
		{
			omTableFields.Add("HTYP");
			olColList[0].Text = GetString(IDS_STRING1320);//"Abfertigungsart";
			rlAttrib.Format = "XX";
			rlAttrib.TextMinLenght = 0;
			rlAttrib.TextMaxLenght = 2;
			if(ogPrivList.GetStat("SEASON_BATCH_Update_AHTYP") == '1')
				olColList[1].blIsEditable = true;	
			else
				olColList[1].blIsEditable = false;	
			olColList[1].EditAttrib = rlAttrib;
			pomTable->AddTextLine(olColList, NULL);
		}

		if(ogPrivList.GetStat("SEASON_BATCH_Update_ASTYP") == '1')
		{
			omTableFields.Add("STYP");
			olColList[0].Text = GetString(IDS_STRING1321);//"Servicetyp";
			rlAttrib.Format = "XX";
			rlAttrib.TextMinLenght = 0;
			rlAttrib.TextMaxLenght = 2;
			if(ogPrivList.GetStat("SEASON_BATCH_Update_ASTYP") == '1')
				olColList[1].blIsEditable = true;	
			else
				olColList[1].blIsEditable = false;	
			olColList[1].EditAttrib = rlAttrib;
			pomTable->AddTextLine(olColList, NULL);
		}

		if(ogPrivList.GetStat("SEASON_BATCH_Update_ASTEV") == '1')
		{
			omTableFields.Add("STEV");
			olColList[0].Text = GetString(IDS_STRING1322);//"Statistik";
			rlAttrib.Format = "X";
			rlAttrib.TextMinLenght = 0;
			rlAttrib.TextMaxLenght = 1;
			if(ogPrivList.GetStat("SEASON_BATCH_Update_ASTEV") == '1')
				olColList[1].blIsEditable = true;	
			else
				olColList[1].blIsEditable = false;	
			olColList[1].EditAttrib = rlAttrib;
			pomTable->AddTextLine(olColList, NULL);
		}

		if(ogPrivList.GetStat("SEASON_BATCH_Update_APSTA") == '1')
		{
			omTableFields.Add("PSTA");
			olColList[0].Text = GetString(IDS_STRING1323);//"Position";
			rlAttrib.Format = "XXXXX";
			rlAttrib.TextMinLenght = 0;
			rlAttrib.TextMaxLenght = 5;
			if(ogPrivList.GetStat("SEASON_BATCH_Update_APSTA") == '1')
				olColList[1].blIsEditable = true;	
			else
				olColList[1].blIsEditable = false;	
			olColList[1].EditAttrib = rlAttrib;
			pomTable->AddTextLine(olColList, NULL);
		}

		if(ogPrivList.GetStat("SEASON_BATCH_Update_AGTA1") == '1')
		{
			omTableFields.Add("GTA1");
			olColList[0].Text = GetString(IDS_STRING1324);//"Port/Gate 1";
			rlAttrib.Format = "XXXXX";
			rlAttrib.TextMinLenght = 0;
			rlAttrib.TextMaxLenght = 5;
			if(ogPrivList.GetStat("SEASON_BATCH_Update_AGTA1") == '1')
				olColList[1].blIsEditable = true;	
			else
				olColList[1].blIsEditable = false;	
			olColList[1].EditAttrib = rlAttrib;
			pomTable->AddTextLine(olColList, NULL);
		}

		if(ogPrivList.GetStat("SEASON_BATCH_Update_AGTA2") == '1')
		{
			omTableFields.Add("GTA2");
			olColList[0].Text = GetString(IDS_STRING1325);//"Port/Gate 2";
			rlAttrib.Format = "XXXXX";
			rlAttrib.TextMinLenght = 0;
			rlAttrib.TextMaxLenght = 5;
			if(ogPrivList.GetStat("SEASON_BATCH_Update_AGTA2") == '1')
				olColList[1].blIsEditable = true;	
			else
				olColList[1].blIsEditable = false;	
			olColList[1].EditAttrib = rlAttrib;
			pomTable->AddTextLine(olColList, NULL);
		}
		
		if(ogPrivList.GetStat("SEASON_BATCH_Update_ABLT1") == '1')
		{
			omTableFields.Add("BLT1");
			olColList[0].Text = GetString(IDS_STRING1326);//"Gep�ckband 1";
			rlAttrib.Format = "XXXXX";
			rlAttrib.TextMinLenght = 0;
			rlAttrib.TextMaxLenght = 5;
			if(ogPrivList.GetStat("SEASON_BATCH_Update_ABLT1") == '1')
				olColList[1].blIsEditable = true;	
			else
				olColList[1].blIsEditable = false;	
			olColList[1].EditAttrib = rlAttrib;
			pomTable->AddTextLine(olColList, NULL);
		}

		if(ogPrivList.GetStat("SEASON_BATCH_Update_ABLT2") == '1')
		{
			omTableFields.Add("BLT2");
			olColList[0].Text = GetString(IDS_STRING1327);//"Gep�ckband 2";
			rlAttrib.Format = "XXXXX";
			rlAttrib.TextMinLenght = 0;
			rlAttrib.TextMaxLenght = 5;
			if(ogPrivList.GetStat("SEASON_BATCH_Update_ABLT2") == '1')
				olColList[1].blIsEditable = true;	
			else
				olColList[1].blIsEditable = false;	
			olColList[1].EditAttrib = rlAttrib;
			pomTable->AddTextLine(olColList, NULL);
		}

		if(ogPrivList.GetStat("SEASON_BATCH_Update_AEXT1") == '1')
		{
			omTableFields.Add("EXT1");
			olColList[0].Text = GetString(IDS_STRING1328);//"Ausgang 1 ";
			rlAttrib.Format = "XXXXX";
			rlAttrib.TextMinLenght = 0;
			rlAttrib.TextMaxLenght = 5;
			if(ogPrivList.GetStat("SEASON_BATCH_Update_AEXT1") == '1')
				olColList[1].blIsEditable = true;	
			else
				olColList[1].blIsEditable = false;	
			olColList[1].EditAttrib = rlAttrib;
			pomTable->AddTextLine(olColList, NULL);
		}

		if(ogPrivList.GetStat("SEASON_BATCH_Update_AEXT2") == '1')
		{
			omTableFields.Add("EXT2");
			olColList[0].Text = GetString(IDS_STRING1329);//"Ausgang 2 ";
			rlAttrib.Format = "XXXXX";
			rlAttrib.TextMinLenght = 0;
			rlAttrib.TextMaxLenght = 5;
			if(ogPrivList.GetStat("SEASON_BATCH_Update_AEXT2") == '1')
				olColList[1].blIsEditable = true;	
			else
				olColList[1].blIsEditable = false;	
			olColList[1].EditAttrib = rlAttrib;
			pomTable->AddTextLine(olColList, NULL);
		}

		if(ogPrivList.GetStat("SEASON_BATCH_Update_AREM1") == '1')
		{
			omTableFields.Add("REM1");
			olColList[0].Text = GetString(ST_BEMERKUNG);
			rlAttrib.Type = KT_STRING;
			rlAttrib.Format = "X(200)";
			rlAttrib.TextMinLenght = 0;
			rlAttrib.TextMaxLenght = 200;
			if(ogPrivList.GetStat("SEASON_BATCH_Update_AREM1") == '1')
				olColList[1].blIsEditable = true;	
			else
				olColList[1].blIsEditable = false;	
			olColList[1].EditAttrib = rlAttrib;
			pomTable->AddTextLine(olColList, NULL);
		}

		if(ogPrivList.GetStat("SEASON_BATCH_Update_AREM2") == '1')
		{
			omTableFields.Add("REM2");
			olColList[0].Text = GetString(IDS_STRING1455);
			rlAttrib.Format = "X(200)";
			rlAttrib.TextMinLenght = 0;
			rlAttrib.TextMaxLenght = 200;
			if(ogPrivList.GetStat("SEASON_BATCH_Update_AREM2") == '1')
				olColList[1].blIsEditable = true;	
			else
				olColList[1].blIsEditable = false;	
			olColList[1].EditAttrib = rlAttrib;
			pomTable->AddTextLine(olColList, NULL);
		}
	}
	else
	{

		pomViaTableCtrl->SetSecStat(ogPrivList.GetStat("SEASON_BATCH_Update_AVIAL"));

		if(ogPrivList.GetStat("SEASON_BATCH_Update_DFLNO") == '1')
		{
			omTableFields.Add("ALC");
			olColList[0].Text = GetString(IDS_STRING1311);//"Flugnummer/Fluggesell.";
			rlAttrib.Format = "XXX";
			rlAttrib.TextMinLenght = 0;
			rlAttrib.TextMaxLenght = 3;
			rlAttrib.Type = KT_STRING;
			if(ogPrivList.GetStat("SEASON_BATCH_Update_DFLNO") == '1')
				olColList[1].blIsEditable = true;	
			else
				olColList[1].blIsEditable = false;	
			olColList[1].EditAttrib = rlAttrib;
			pomTable->AddTextLine(olColList, NULL);

			omTableFields.Add("FLTN");
			olColList[0].Text = GetString(IDS_STRING1312);//"Flugnummer/Nummerisch.";
			rlAttrib.Type = KT_STRING;
			rlAttrib.Format = "#####";
			rlAttrib.TextMinLenght = 0;
			rlAttrib.TextMaxLenght = 5;
			if(ogPrivList.GetStat("SEASON_BATCH_Update_DFLNO") == '1')
				olColList[1].blIsEditable = true;	
			else
				olColList[1].blIsEditable = false;	
			olColList[1].EditAttrib = rlAttrib;
			pomTable->AddTextLine(olColList, NULL);
		
			omTableFields.Add("FLNS");
			olColList[0].Text = GetString(IDS_STRING1313);//"Flugnummer/Suffix";
			rlAttrib.Type = KT_STRING;
			rlAttrib.Format = "X";
			rlAttrib.TextMinLenght = 0;
			rlAttrib.TextMaxLenght = 1;
			if(ogPrivList.GetStat("SEASON_BATCH_Update_DFLNO") == '1')
				olColList[1].blIsEditable = true;	
			else
				olColList[1].blIsEditable = false;	
			olColList[1].EditAttrib = rlAttrib;
			pomTable->AddTextLine(olColList, NULL);

			omTableFields.Add("FLTI");
			olColList[0].Text = GetString(IDS_STRING2129);//"Flight ID";
			rlAttrib.Type = KT_STRING;
			rlAttrib.Format = "X";
			rlAttrib.TextMinLenght = 0;
			rlAttrib.TextMaxLenght = 1;
			if(ogPrivList.GetStat("SEASON_BATCH_Update_AFLNO") == '1')
				olColList[1].blIsEditable = true;	
			else
				olColList[1].blIsEditable = false;	
			olColList[1].EditAttrib = rlAttrib;
			pomTable->AddTextLine(olColList, NULL);
		}

		if(ogPrivList.GetStat("SEASON_BATCH_Update_DCSGN") == '1')
		{
			omTableFields.Add("CSGN");
			olColList[0].Text = GetString(IDS_CALLSIGN);//"callsign
			rlAttrib.Type = KT_STRING;
			rlAttrib.Format = "XXXXXXXX";
			rlAttrib.TextMinLenght = 0;
			rlAttrib.TextMaxLenght = 8;
			if(ogPrivList.GetStat("SEASON_BATCH_Update_DCSGN") == '1')
				olColList[1].blIsEditable = true;	
			else
				olColList[1].blIsEditable = false;	
			olColList[1].EditAttrib = rlAttrib;
			pomTable->AddTextLine(olColList, NULL);
		}

		if(ogPrivList.GetStat("SEASON_BATCH_Update_DSTOA") == '1')
		{
			omTableFields.Add("STOA");
			olColList[0].Text = GetString(IDS_STRING1314);//"Plan. Ankuftszeit";
			rlAttrib.Type = KT_TIME;
			rlAttrib.ChangeDay	= true;
			rlAttrib.TextMaxLenght = 7;
			if(ogPrivList.GetStat("SEASON_BATCH_Update_DSTOA") == '1')
				olColList[1].blIsEditable = true;	
			else
				olColList[1].blIsEditable = false;	
			olColList[1].EditAttrib = rlAttrib;
			pomTable->AddTextLine(olColList, NULL);
		}

		if(ogPrivList.GetStat("SEASON_BATCH_Update_DSTOD") == '1')
		{
			omTableFields.Add("STOD");
			olColList[0].Text = GetString(IDS_STRING1315);//"Plan. Abflugszeit";
			rlAttrib.Type = KT_TIME;
			rlAttrib.ChangeDay	= true;
			rlAttrib.TextMaxLenght = 7;
			if(ogPrivList.GetStat("SEASON_BATCH_Update_DSTOD") == '1')
				olColList[1].blIsEditable = true;	
			else
				olColList[1].blIsEditable = false;	
			olColList[1].EditAttrib = rlAttrib;
			pomTable->AddTextLine(olColList, NULL);
		}

		if(ogPrivList.GetStat("SEASON_BATCH_Update_DETDU") == '1')
		{
			omTableFields.Add("ETDU");
			olColList[0].Text = GetString(IDS_STRING1330);//"Erwartete Abflugszeit";
			rlAttrib.Type = KT_TIME;
			rlAttrib.ChangeDay	= true;
			rlAttrib.TextMaxLenght = 7;
			if(ogPrivList.GetStat("SEASON_BATCH_Update_DETDU") == '1')
				olColList[1].blIsEditable = true;	
			else
				olColList[1].blIsEditable = false;	
			olColList[1].EditAttrib = rlAttrib;
			pomTable->AddTextLine(olColList, NULL);
		}

		if(ogPrivList.GetStat("SEASON_BATCH_Update_DDES3") == '1')
		{
			omTableFields.Add("DES3");
			olColList[0].Text = GetString(IDS_STRING1331);//"Bestimmungsflughafen";
			rlAttrib.Type = KT_STRING;
			rlAttrib.Format = "XXX";
			rlAttrib.TextMinLenght = 0;
			rlAttrib.TextMaxLenght = 3;
			if(ogPrivList.GetStat("SEASON_BATCH_Update_DDES3") == '1')
				olColList[1].blIsEditable = true;	
			else
				olColList[1].blIsEditable = false;	
			olColList[1].EditAttrib = rlAttrib;
			pomTable->AddTextLine(olColList, NULL);
		}
		
		if(ogPrivList.GetStat("SEASON_BATCH_Update_DACT3") == '1')
		{
			omTableFields.Add("ACT3");
			olColList[0].Text = GetString(IDS_STRING1318);//"Flugzeugtyp (IATA)";
			rlAttrib.Format = "XXX";
			rlAttrib.TextMinLenght = 0;
			rlAttrib.TextMaxLenght = 3;
			if(ogPrivList.GetStat("SEASON_BATCH_Update_DACT3") == '1')
				olColList[1].blIsEditable = true;	
			else
				olColList[1].blIsEditable = false;	
			olColList[1].EditAttrib = rlAttrib;
			pomTable->AddTextLine(olColList, NULL);
		}

		if(ogPrivList.GetStat("SEASON_BATCH_Update_DNOSE") == '1')
		{
			omTableFields.Add("NOSE");
			olColList[0].Text = GetString(IDS_STRING400);//"Anzahl Sitze";
			rlAttrib.Format = "###";
			rlAttrib.TextMinLenght = 0;
			rlAttrib.TextMaxLenght = 3;
			if(ogPrivList.GetStat("SEASON_BATCH_Update_DNOSE") == '1')
				olColList[1].blIsEditable = true;	
			else
				olColList[1].blIsEditable = false;	
			olColList[1].EditAttrib = rlAttrib;
			pomTable->AddTextLine(olColList, NULL);
		}

		if(ogPrivList.GetStat("SEASON_BATCH_Update_DTTYP") == '1')
		{
			omTableFields.Add("TTYP");
			olColList[0].Text = GetString(IDS_STRING1319);//"Verkehrsart";
			rlAttrib.Format = "XXXXX";
			rlAttrib.TextMinLenght = 0;
			rlAttrib.TextMaxLenght = 5;
			if(ogPrivList.GetStat("SEASON_BATCH_Update_DTTYP") == '1')
				olColList[1].blIsEditable = true;	
			else
				olColList[1].blIsEditable = false;	
			olColList[1].EditAttrib = rlAttrib;
			pomTable->AddTextLine(olColList, NULL);
		}

		if(ogPrivList.GetStat("SEASON_BATCH_Update_DHTYP") == '1')
		{
			omTableFields.Add("HTYP");
			olColList[0].Text = GetString(IDS_STRING1320);//"Abfertigungsart";
			rlAttrib.Format = "XX";
			rlAttrib.TextMinLenght = 0;
			rlAttrib.TextMaxLenght = 2;
			if(ogPrivList.GetStat("SEASON_BATCH_Update_DHTYP") == '1')
				olColList[1].blIsEditable = true;	
			else
				olColList[1].blIsEditable = false;	
			olColList[1].EditAttrib = rlAttrib;
			pomTable->AddTextLine(olColList, NULL);
		}

		if(ogPrivList.GetStat("SEASON_BATCH_Update_DSTYP") == '1')
		{
			omTableFields.Add("STYP");
			olColList[0].Text = GetString(IDS_STRING1321);//"Servicetyp";
			rlAttrib.Format = "XX";
			rlAttrib.TextMinLenght = 0;
			rlAttrib.TextMaxLenght = 2;
			if(ogPrivList.GetStat("SEASON_BATCH_Update_DSTYP") == '1')
				olColList[1].blIsEditable = true;	
			else
				olColList[1].blIsEditable = false;	
			olColList[1].EditAttrib = rlAttrib;
			pomTable->AddTextLine(olColList, NULL);
		}

		if(ogPrivList.GetStat("SEASON_BATCH_Update_DSTEV") == '1')
		{
			omTableFields.Add("STEV");
			olColList[0].Text = GetString(IDS_STRING1322);//"Statistik";
			rlAttrib.Format = "X";
			rlAttrib.TextMinLenght = 0;
			rlAttrib.TextMaxLenght = 1;
			if(ogPrivList.GetStat("SEASON_BATCH_Update_DSTEV") == '1')
				olColList[1].blIsEditable = true;	
			else
				olColList[1].blIsEditable = false;	
			olColList[1].EditAttrib = rlAttrib;
			pomTable->AddTextLine(olColList, NULL);
		}

		if(ogPrivList.GetStat("SEASON_BATCH_Update_DPSTD") == '1')
		{
			omTableFields.Add("PSTD");
			olColList[0].Text = GetString(IDS_STRING1323);//"Position";
			rlAttrib.Format = "XXXXX";
			rlAttrib.TextMinLenght = 0;
			rlAttrib.TextMaxLenght = 5;
			if(ogPrivList.GetStat("SEASON_BATCH_Update_DPSTD") == '1')
				olColList[1].blIsEditable = true;	
			else
				olColList[1].blIsEditable = false;	
			olColList[1].EditAttrib = rlAttrib;
			pomTable->AddTextLine(olColList, NULL);
		}

		if(ogPrivList.GetStat("SEASON_BATCH_Update_DGTD1") == '1')
		{
			omTableFields.Add("GTD1");
			olColList[0].Text = GetString(IDS_STRING1332);//"Gate 1";
			rlAttrib.Format = "XXXXX";
			rlAttrib.TextMinLenght = 0;
			rlAttrib.TextMaxLenght = 5;
			if(ogPrivList.GetStat("SEASON_BATCH_Update_DGTD1") == '1')
				olColList[1].blIsEditable = true;	
			else
				olColList[1].blIsEditable = false;	
			olColList[1].EditAttrib = rlAttrib;
			pomTable->AddTextLine(olColList, NULL);
		}

		if(ogPrivList.GetStat("SEASON_BATCH_Update_DGTD2") == '1')
		{
			omTableFields.Add("GTD2");
			olColList[0].Text = GetString(IDS_STRING1333);//"Gate 2";
			rlAttrib.Format = "XXXXX";
			rlAttrib.TextMinLenght = 0;
			rlAttrib.TextMaxLenght = 5;
			if(ogPrivList.GetStat("SEASON_BATCH_Update_DGTD2") == '1')
				olColList[1].blIsEditable = true;	
			else
				olColList[1].blIsEditable = false;	
			olColList[1].EditAttrib = rlAttrib;
			pomTable->AddTextLine(olColList, NULL);
		}
		
		if(ogPrivList.GetStat("SEASON_BATCH_Update_DWRO1") == '1')
		{
			omTableFields.Add("WRO1");
			olColList[0].Text = GetString(IDS_STRING1334);//"Warteraum";
			rlAttrib.Format = "XXXXX";
			rlAttrib.TextMinLenght = 0;
			rlAttrib.TextMaxLenght = 5;
			if(ogPrivList.GetStat("SEASON_BATCH_Update_DWRO1") == '1')
				olColList[1].blIsEditable = true;	
			else
				olColList[1].blIsEditable = false;	
			olColList[1].EditAttrib = rlAttrib;
			pomTable->AddTextLine(olColList, NULL);
		}

		if(ogPrivList.GetStat("SEASON_BATCH_Update_DREM1") == '1')
		{
			omTableFields.Add("REM1");
			olColList[0].Text = GetString(ST_BEMERKUNG);
			rlAttrib.Format = "X(200)";
			rlAttrib.TextMinLenght = 0;
			rlAttrib.TextMaxLenght = 200;
			if(ogPrivList.GetStat("SEASON_BATCH_Update_DREM1") == '1')
				olColList[1].blIsEditable = true;	
			else
				olColList[1].blIsEditable = false;	
			olColList[1].EditAttrib = rlAttrib;
			pomTable->AddTextLine(olColList, NULL);
		}

		if(ogPrivList.GetStat("SEASON_BATCH_Update_DREM2") == '1')
		{
			omTableFields.Add("REM2");
			olColList[0].Text = GetString(IDS_STRING1455);
			rlAttrib.Format = "X(200)";
			rlAttrib.TextMinLenght = 0;
			rlAttrib.TextMaxLenght = 200;
			if(ogPrivList.GetStat("SEASON_BATCH_Update_DREM2") == '1')
				olColList[1].blIsEditable = true;	
			else
				olColList[1].blIsEditable = false;	
			olColList[1].EditAttrib = rlAttrib;
			pomTable->AddTextLine(olColList, NULL);
		}

	}

	olColList.DeleteAll();

	pomTable->DisplayTable();


	CString olTmp;

	if(opAdid == "A")
	{
		pomCinsTable->ShowWindow(SW_HIDE);
		m_CS_CinsBorder.ShowWindow(SW_HIDE);
		m_CS_CinsLabel.ShowWindow(SW_HIDE);
		m_CB_CinsMal4.ShowWindow(SW_HIDE);

		olTmp = CString("--> ") + CString(pcgHome);
		m_CS_Org.SetWindowText("");
		m_CS_Des.SetWindowText(olTmp);

		if(ogPrivList.GetStat("SEASON_BATCH_Update_AVIAL") == '1')
			pomViaTableCtrl->ShowWindow(SW_SHOW);
		else
			pomViaTableCtrl->ShowWindow(SW_HIDE);

		if(ogPrivList.GetStat("SEASON_BATCH_Update_AJFNO") == '1')
			pomJfnoTable->ShowWindow(SW_SHOW);
		else
			pomJfnoTable->ShowWindow(SW_HIDE);

		if(ogPrivList.GetStat("SEASON_BATCH_Update_AFTYP") == '1')
		{
			m_CR_Prognose.EnableWindow(TRUE);
			m_CR_Betrieb.EnableWindow(TRUE);
			m_CR_Planung.EnableWindow(TRUE);
			m_CR_Noop.EnableWindow(TRUE);
		}
		else
		{
			m_CR_Prognose.EnableWindow(FALSE);
			m_CR_Betrieb.EnableWindow(FALSE);
			m_CR_Planung.EnableWindow(FALSE);
			m_CR_Noop.EnableWindow(FALSE);
		}


	}
	else
	{

		if(ogPrivList.GetStat("SEASON_BATCH_Update_DCKIF") == '1')
		{
			pomCinsTable->ShowWindow(SW_SHOW);
			m_CB_CinsMal4.ShowWindow(SW_SHOW);
		}
		else
		{
			pomCinsTable->ShowWindow(SW_HIDE);
			m_CB_CinsMal4.ShowWindow(SW_HIDE);
		}

		if(ogPrivList.GetStat("SEASON_BATCH_Update_DVIAL") == '1')
			pomViaTableCtrl->ShowWindow(SW_SHOW);
		else
			pomViaTableCtrl->ShowWindow(SW_HIDE);

		if(ogPrivList.GetStat("SEASON_BATCH_Update_DJFNO") == '1')
			pomJfnoTable->ShowWindow(SW_SHOW);
		else
			pomJfnoTable->ShowWindow(SW_HIDE);

		if(ogPrivList.GetStat("SEASON_BATCH_Update_DFTYP") == '1')
		{
			m_CR_Prognose.EnableWindow(TRUE);
			m_CR_Betrieb.EnableWindow(TRUE);
			m_CR_Planung.EnableWindow(TRUE);
			m_CR_Noop.EnableWindow(TRUE);
		}
		else
		{
			m_CR_Prognose.EnableWindow(FALSE);
			m_CR_Betrieb.EnableWindow(FALSE);
			m_CR_Planung.EnableWindow(FALSE);
			m_CR_Noop.EnableWindow(FALSE);
		}


		m_CS_CinsBorder.ShowWindow(SW_SHOW);
		m_CS_CinsLabel.ShowWindow(SW_SHOW);

		olTmp = CString(pcgHome)+ CString(" -->");

		m_CS_Org.SetWindowText(olTmp);
		m_CS_Des.SetWindowText("");


	}

	OnTablesdeselect();
	OnTablesreset();

}



void SeasonCollectDlg::CreateTables()
{
	char buffer[64];
	int ilLc;

	CCSPtrArray<TABLE_COLUMN> olColList;
	TABLE_COLUMN rlColumnData;

	TABLE_HEADER_COLUMN rlHeader;

	rlHeader.Alignment = COLALIGN_CENTER;
	rlHeader.Font = &ogCourier_Regular_10;
	rlHeader.Text = CString(""); 

	CCSEDIT_ATTRIB rlAttribC1;
	CCSEDIT_ATTRIB rlAttribC2;
	CCSEDIT_ATTRIB rlAttribC3;
	CCSEDIT_ATTRIB rlAttribC4;

	pomTable = new CCSTable;
	CRect olRectBorder;
	m_CS_Table.GetWindowRect( olRectBorder );
	ScreenToClient(olRectBorder);
	olRectBorder.DeflateRect(2,2);

	pomTable->SetMiniTable();
	pomTable->SetTableEditable(true);
    pomTable->SetTableData(this, olRectBorder.left, olRectBorder.right, olRectBorder.top, olRectBorder.bottom);
	pomTable->SetSelectMode(LBS_MULTIPLESEL);
	pomTable->SetIPEditModus(true);

	CCSPtrArray <TABLE_HEADER_COLUMN> omHeaderDataArray;

	pomTable->SetShowSelection(true);
	pomTable->ResetContent();
	

	rlHeader.Length = 270; 
	omHeaderDataArray.New(rlHeader);

	rlHeader.Length = 118; 
	omHeaderDataArray.New(rlHeader);

	pomTable->SetHeaderFields(omHeaderDataArray);
	omHeaderDataArray.DeleteAll();

	pomTable->SetDefaultSeparator();




	// Mintables


	// Weitere Flugnummern
	pomJfnoTable = new CCSTable;

	m_CS_JfnoBorder.GetWindowRect( olRectBorder );
	ScreenToClient(olRectBorder);
	olRectBorder.DeflateRect(2,2);

	pomJfnoTable->SetHeaderSpacing(0);
	pomJfnoTable->SetMiniTable();
	pomJfnoTable->SetTableEditable(true);
	pomJfnoTable->SetSelectMode(LBS_MULTIPLESEL);
    pomJfnoTable->SetTableData(this, olRectBorder.left, olRectBorder.right, olRectBorder.top, olRectBorder.bottom);
	pomJfnoTable->SetShowSelection(true);
	pomJfnoTable->ResetContent();
	pomJfnoTable->SetIPEditModus(true);


	rlHeader.Length = 15; 
	omHeaderDataArray.New(rlHeader);
	rlHeader.Length = 36; 
	omHeaderDataArray.New(rlHeader);
	rlHeader.Length = 46; 
	omHeaderDataArray.New(rlHeader);
	rlHeader.Length = 15; 
	omHeaderDataArray.New(rlHeader);

	pomJfnoTable->SetHeaderFields(omHeaderDataArray);
	omHeaderDataArray.DeleteAll();
	pomJfnoTable->SetDefaultSeparator();
	pomJfnoTable->DisplayTable();


	rlAttribC1.Format = "XXX";
	rlAttribC1.TextMinLenght = 0;
	rlAttribC1.TextMaxLenght = 3;
	rlAttribC1.Style = ES_UPPERCASE;

	rlAttribC2.Format = "#####";
	rlAttribC2.TextMinLenght = 0;
	rlAttribC2.TextMaxLenght = 5;

	rlAttribC3.Format = "X";
	rlAttribC3.TextMinLenght = 0;
	rlAttribC3.TextMaxLenght = 1;
	rlAttribC3.Style = ES_UPPERCASE;

	rlColumnData.Font = &ogMS_Sans_Serif_8;
	rlColumnData.Alignment = COLALIGN_LEFT;

	for (ilLc = 1; ilLc < 10; ilLc++)
	{
		itoa(ilLc, buffer, 10);
		rlColumnData.blIsEditable = false;	
		rlColumnData.Text = CString(buffer);
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.blIsEditable = true;	
		rlColumnData.Text = "";
		rlColumnData.EditAttrib = rlAttribC1;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.EditAttrib = rlAttribC2;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.EditAttrib = rlAttribC3;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		pomJfnoTable->AddTextLine(olColList, (void*)(NULL));
		olColList.DeleteAll();
	}
	pomJfnoTable->DisplayTable();




	// Vias
	pomViaTableCtrl = new ViaTableCtrl(this, "HEAD,APC3,FIDS,STOA,STOD");



	pomViaTableCtrl->ShowButton(false);

	CRect olRect;
	CRect olRect1;
	CRect olRect2;


	m_CS_JfnoBorder.GetWindowRect( olRect1 );
	ScreenToClient(olRect1);

	m_CS_Matrix.GetWindowRect( olRect2 );
	ScreenToClient(olRect2);


	olRect.bottom = 0;
	olRect.right = 0;

	olRect.left = olRect1.left + 43;
	olRect.top  = olRect2.bottom - 50;

	pomViaTableCtrl->SetPosition(olRect);




	// Check-In Table

	pomCinsTable = new CCSTable;

	m_CS_CinsBorder.GetWindowRect( olRectBorder );
	pomCinsTable->ResetContent();
	pomCinsTable->SetHeaderSpacing(0);
	pomCinsTable->SetMiniTable();
	pomCinsTable->SetTableEditable(true);
	ScreenToClient(olRectBorder);
	olRectBorder.DeflateRect(2,2);
    pomCinsTable->SetTableData(this, olRectBorder.left, olRectBorder.right, olRectBorder.top, olRectBorder.bottom);
	pomCinsTable->SetSelectMode(LBS_MULTIPLESEL);
	pomCinsTable->SetIPEditModus(true);


	pomCinsTable->SetShowSelection(true);
	pomCinsTable->ResetContent();
	

	rlHeader.Length = 19; 
	omHeaderDataArray.New(rlHeader);
	rlHeader.Length = 36; 
	omHeaderDataArray.New(rlHeader);
	rlHeader.Length = 12; 
	omHeaderDataArray.New(rlHeader);
	rlHeader.Length = 46; 
	omHeaderDataArray.New(rlHeader);
	rlHeader.Length = 46; 
	omHeaderDataArray.New(rlHeader);

	pomCinsTable->SetHeaderFields(omHeaderDataArray);
	omHeaderDataArray.DeleteAll();
	pomCinsTable->SetDefaultSeparator();


	rlAttribC1.Type = KT_STRING;
	rlAttribC1.Style = ES_UPPERCASE;
	rlAttribC1.Format = "XXX";
	rlAttribC1.TextMinLenght = 0;
	rlAttribC1.TextMaxLenght = 3;

	rlAttribC2.Type = KT_STRING;
	rlAttribC2.Style = ES_UPPERCASE;
	rlAttribC2.Format = "X";
	rlAttribC2.TextMinLenght = 0;
	rlAttribC2.TextMaxLenght = 1;

	rlAttribC3.Type = KT_TIME;
	rlAttribC3.ChangeDay	= true;
	rlAttribC3.TextMaxLenght = 7;

	rlAttribC4.Type = KT_TIME;
	rlAttribC4.ChangeDay	= true;
	rlAttribC4.TextMaxLenght = 7;

	rlColumnData.Font = &ogMS_Sans_Serif_8;
	rlColumnData.Alignment = COLALIGN_LEFT;

	for (ilLc = 1; ilLc < 40; ilLc++)
	{
		itoa(ilLc, buffer, 10);
		rlColumnData.blIsEditable = false;	
		rlColumnData.Text = CString(buffer);
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		rlColumnData.blIsEditable = true;	
		rlColumnData.Text = "";
		rlColumnData.EditAttrib = rlAttribC1;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		rlColumnData.EditAttrib = rlAttribC2;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		rlColumnData.EditAttrib = rlAttribC3;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		rlColumnData.EditAttrib = rlAttribC4;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		
		pomCinsTable->AddTextLine(olColList, (void*)(NULL));
		olColList.DeleteAll();
	}
	pomCinsTable->DisplayTable();
}



void SeasonCollectDlg::InitMatrtix() 
{
	//CString loadstart = omLoadStart.Format("%Y%m%d");
	//CString loadend = omLoadEnd.Format("%Y%m%d");
	//CString dispstart = omDispStart.Format("%Y%m%d");
	//CString dispend = omDispEnd.Format("%Y%m%d");


	if(omLoadStart == TIMENULL)
		return;
	
	CCSCell *prlCell;

	CTimeSpan olOneDay(1,0,0,0);
 
	omDispStart = DBStringToDateTime(omLoadStart.Format("%Y%m%d120000"));
	
	omDayMap.RemoveAll();	

	for(; GetDayOfWeek(omDispStart) != 1; omDispStart -= olOneDay)
		;

	omDispEnd = omDispStart;
	
	int ilCount = omCells.GetSize();
	int j = 0;

	for(int i = 0; i < ilCount; i++)
	{
		prlCell = &omCells[i];
	
		omDayMap.SetAt(omDispEnd.Format("%d%m%Y"), (void*&)prlCell);
		prlCell->SetText("");

		if((omDispEnd >= omLoadStart) && (omDispEnd <= omLoadEnd))
			prlCell->SetName(omDispEnd.Format("%d%m%Y"));
		else
			prlCell->SetName("");

		if(GetDayOfWeek(omDispEnd) == 1)
		{
			omVButtons[j].SetWindowText(omDispEnd.Format("%d.%m.%y"));
			j++;
		}
		omDispEnd += olOneDay;
	}
	InvalidateRect(NULL);
	UpdateWindow();
}



LONG SeasonCollectDlg::OnButtonClick( UINT wParam, LPARAM lParam)
{
	CCSPtrArray<CCSCell> *prlCells;
	CCSCell *prlCell;

	if(omButtonMap.Lookup((void*&) lParam, (void*&) prlCells) == TRUE)
	{

		for(int i = prlCells->GetSize() - 1; i >= 0; i--)
		{
			prlCell = &((*prlCells)[i]);
			prlCell->Select(true);
		}
		DisplayRemarks();
	}

//	DisplayRemarks();

	return 0L;

}

void SeasonCollectDlg::OnRightButtonDown( UINT wParam, LPARAM lParam)
{
	CCSPtrArray<CCSCell> *prlCells;
	CCSCell *prlCell;

	if(omButtonMap.Lookup((void*&) lParam, (void*&) prlCells) == TRUE)
	{

		// show the cursor menu
		omMenuUrno.RemoveAll();
		omMenuFlights.DestroyMenu();
		omMenuFlights.CreatePopupMenu();
		int ilCommandId = WM_MENU_RANGE_START;

		for(int i = prlCells->GetSize() - 1; i >= 0; i--)
		{
			CCSPtrArray<COLLECTFLIGHTDATA> *polDayFlights; 
			COLLECTFLIGHTDATA *prlFlight;
			prlCell = &((*prlCells)[i]);

			CString olKey("");

			olKey = prlCell->GetName() + omField.Left(1);
			
			if(olKey.GetLength() > 1)
			{
				//collection of flights on the cells day
				if((polDayFlights = ogSeasonCollectFlights.GetFlightsByDayKey(olKey)) != NULL)
				{
					// list only if more than one flight
					if ( (*polDayFlights).GetSize()  < 2)
						continue;

					for(int j = (*polDayFlights).GetSize() - 1; j >= 0; j--)
					{
						prlFlight  = &(*polDayFlights)[j];
						if (prlFlight)
						{
							CTime olTime = TIMENULL;
							if(omField.Left(1) == "A")
								olTime = prlFlight->Stoa;
							else
								olTime = prlFlight->Stod;

							if(bgSeasonLocal) ogBasicData.UtcToLocal(olTime);

							// string for the flight (formated,because need format in onmenu()!!!)
							CString olStrDay = olTime.Format("%d.%m.%Y");
							CString olStrTime = olTime.Format("%H:%M");
							//	10 Zeichen:20.02.2002 + 2Zeichen + 5Zeichen:13:56 + 2Zeichen + flugnummer	
							CString olStrMenu = olStrDay + CString(", ") + olStrTime + CString("; ") + CString(prlFlight->Flno);

							omMenuFlights.AppendMenu(MF_STRING | MF_ENABLED, ilCommandId, olStrMenu);
							omMenuUrno.Add(prlFlight->Urno);
							ilCommandId++;
						}
					}
				}
			}
		}
		CPoint point;
		::GetCursorPos(&point);
		omMenuFlights.TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON, point.x, point.y, this, NULL);
	}
}

LONG SeasonCollectDlg::OnEditChanged( UINT wParam, LPARAM lParam)
{
	/*
	if(!bmChanged)
	{
		m_CB_OK.SetColors(RGB(255,0,0), ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
	}
	*/
	if ((UINT)m_CE_Alc.imID != wParam && (UINT)m_CE_Fltn.imID != wParam && (UINT)m_CE_Flns.imID != wParam)
	{
		bmChanged = true;
	}
	return 0L;
}


void SeasonCollectDlg::OnDeselect() 
{
	for(int i = omCells.GetSize() - 1; i >= 0; i--)
	{
		omCells[i].Select(false);
	}

	DisplayRemarks();
}

void SeasonCollectDlg::OnSelectall() 
{
	for(int i = omCells.GetSize() - 1; i >= 0; i--)
	{
		omCells[i].Select(true);
	}

	DisplayRemarks();
}


void SeasonCollectDlg::OnCellMouseMove( UINT wParam, LPARAM lParam) 
{
	CCSCELLNOTIFY *prlNotify = (CCSCELLNOTIFY*)lParam;

	if(omLastSelRect.top == -1 || omLastSelRect.left  == -1 || 	omLastSelRect.bottom == -1 ||	omLastSelRect.right  == -1)
	{
		return;
	}
	omLastSelRect = omSelRect;
	omSelRect.bottom = prlNotify->Line;
	omSelRect.right  = prlNotify->Column;
	SelectRegion();

	for(int i = omCells.GetSize() - 1; i >= 0; i--) 
	{
		omCells[i].ReleaseMultiSel();
	}	
}


void SeasonCollectDlg::OnCellLButtonDown( UINT wParam, LPARAM lParam) 
{
	CCSCELLNOTIFY *prlNotify = (CCSCELLNOTIFY*)lParam;

	omSelRect.top = prlNotify->Line;
	omSelRect.left  = prlNotify->Column;
	omSelRect.bottom = prlNotify->Line;
	omSelRect.right  = prlNotify->Column;
	omLastSelRect = omSelRect;

}

// show Flightmenu if more than one flight on cell, then start seasondlg
void SeasonCollectDlg::OnCellRButtonDown( UINT wParam, LPARAM lParam) 
{
	CCSCELLNOTIFY *prlNotify = (CCSCELLNOTIFY*)lParam;

	omSelRect.top = prlNotify->Line;
	omSelRect.left  = prlNotify->Column;
	omSelRect.bottom = prlNotify->Line;
	omSelRect.right  = prlNotify->Column;
	omLastSelRect = omSelRect;
	CPoint point = prlNotify->Point;

	CCSCell *prlCell;
	CString olKey;
	CCSPtrArray<COLLECTFLIGHTDATA> *polDayFlights; 
	COLLECTFLIGHTDATA *prlFlight;

	prlCell = (CCSCell *)prlNotify->SourceControl;
	if (! prlCell)
		return;

	// e.g 0102002A or 0102002D
	olKey = prlCell->GetName() + omField.Left(1);
	
	if(olKey.GetLength() > 1)
	{
		//collection of flights on the cells day
		if((polDayFlights = ogSeasonCollectFlights.GetFlightsByDayKey(olKey)) != NULL)
		{
			// list only if more than one flight
			if ( (*polDayFlights).GetSize()  < 2)
				return;

			// show the cursor menu
			omMenuUrno.RemoveAll();
			omMenuFlights.DestroyMenu();
			omMenuFlights.CreatePopupMenu();
			int ilCommandId = WM_MENU_RANGE_START;
			for(int j = (*polDayFlights).GetSize() - 1; j >= 0; j--)
			{
				prlFlight  = &(*polDayFlights)[j];
				if (prlFlight)
				{
					CTime olTime = TIMENULL;
					if(omField.Left(1) == "A")
						olTime = prlFlight->Stoa;
					else
						olTime = prlFlight->Stod;

					if(bgSeasonLocal) ogBasicData.UtcToLocal(olTime);

					// string for the flight (formated,because need format in onmenu()!!!)
					CString olStrDay = olTime.Format("%d.%m.%Y");
					CString olStrTime = olTime.Format("%H:%M");
					//	10 Zeichen:20.02.2002 + 2Zeichen + 5Zeichen:13:56 + 2Zeichen + flugnummer	
					CString olStrMenu = olStrDay + CString(", ") + olStrTime + CString("; ") + CString(prlFlight->Flno);

					omMenuFlights.AppendMenu(MF_STRING | MF_ENABLED, ilCommandId, olStrMenu);
					omMenuUrno.Add(prlFlight->Urno);
					ilCommandId++;
				}
			}
			omMenuFlights.TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON, point.x, point.y, this, NULL);
		}
	}
}


afx_msg LONG SeasonCollectDlg::OnMenu( WPARAM wParam, LPARAM lParam )
{

	CString olKey;
	CCSPtrArray<COLLECTFLIGHTDATA> *polDayFlights; 
	COLLECTFLIGHTDATA *prlFlight;

	CString olFlightMenu;
	if(! &omMenuFlights)
		return -1L;

	omMenuFlights.GetMenuString(LOWORD(wParam), olFlightMenu, MF_BYCOMMAND);

	int test = LOWORD(wParam);
	test = test-WM_MENU_RANGE_START;
	long ilUrno = omMenuUrno.GetAt(test);

	int ilLen = olFlightMenu.GetLength();
	olKey = olFlightMenu.Left(10);
	olKey.Remove('.');
	//name of the cell
	CString olStrCell = olKey;
	//name in the daymapCMenu
	olKey = olKey + omField.Left(1);
	//time for flight
	CString olTimeStr = olFlightMenu.Mid(12,5);


	//string for the selected flightnumber
	int ilPos = olFlightMenu.Find(";")+1;
	ilLen -= ilPos; 

	CString olFlno = olFlightMenu.Right(ilLen);
	olFlno.Remove(' ');

	if(olKey.GetLength() > 1)
	{
		if((polDayFlights = ogSeasonCollectFlights.GetFlightsByDayKey(olKey)) != NULL)
		{
			for(int j = (*polDayFlights).GetSize() - 1; j >= 0; j--)
			{
				prlFlight  = &(*polDayFlights)[j];
				if (prlFlight)
				{
/*					CString olFlight(prlFlight->Flno);
					olFlight.Remove(' ');
					//selected flight is in the daymap -> show dlg
					if (olFlight.Compare(olFlno) == 0 && pogSeasonDlg)
					{
						CTime olTime = TIMENULL;
						if (strcmp(prlFlight->Adid, "A") == 0)
							olTime = prlFlight->Stoa;
						else
							olTime = prlFlight->Stod;

						if(bgSeasonLocal) ogBasicData.UtcToLocal(olTime);
						CString olTimeFlight = olTime.Format("%H:%M");
*/
						if (ilUrno == prlFlight->Urno)
//						if (olTimeFlight == olTimeStr)
							pogSeasonDlg->NewData(this, prlFlight->Rkey, prlFlight->Urno, DLG_CHANGE, bgSeasonLocal);
//					}
				}
			}
		}
	}

 	return 0;
}

void SeasonCollectDlg::OnLButtonDown(UINT nFlags, CPoint point) 
{
	
	CDialog::OnLButtonDown(nFlags, point);
}


void SeasonCollectDlg::OnCellLButtonUp( UINT wParam, LPARAM lParam) 
{

	omLastSelRect.top = -1;
	omLastSelRect.left  = -1;
	omLastSelRect.bottom = -1;
	omLastSelRect.right  = -1;
	
	ReleaseCapture();
	for(int i = omCells.GetSize() - 1; i >= 0; i--)
	{
		omCells[i].ReleaseMultiSel();
	}	

	DisplayRemarks();
}

void SeasonCollectDlg::OnLButtonUp(UINT nFlags, CPoint point) 
{
	omLastSelRect.top = -1;
	omLastSelRect.left  = -1;
	omLastSelRect.bottom = -1;
	omLastSelRect.right  = -1;
	
	ReleaseCapture();
	for(int i = omCells.GetSize() - 1; i >= 0; i--)
	{
		omCells[i].ReleaseMultiSel();
	}	
	CDialog::OnLButtonUp(nFlags, point);
}



void SeasonCollectDlg::SelectRegion() 
{
	char buffer[124];
	CCSCell *prlCell;

	CRect olLastSelRect = omLastSelRect;
	CRect olSelRect = omSelRect;

	olSelRect.NormalizeRect();		
	olLastSelRect.NormalizeRect();		

	CMapPtrToPtr olDelMap;
	

	if( ( olLastSelRect.right - olLastSelRect.left) > 100)
		return;

	if( ( olSelRect.right - olSelRect.left) > 100)
		return;

	
	for(int i = olLastSelRect.left; i <= olLastSelRect.right; i++)
	{
		for(int j = olLastSelRect.top; j <= olLastSelRect.bottom; j++)
		{
	
			sprintf(buffer, "%d,%d", j, i);
		
			if(omPosMap.Lookup(buffer, (void*&) prlCell))
			{
				olDelMap.SetAt((void*&)prlCell,(void*&)prlCell);
			}
		}
	}


	for( i = olSelRect.left; i <= olSelRect.right; i++)
	{
		for( int j = olSelRect.top; j <= olSelRect.bottom; j++)
		{
	
			sprintf(buffer, "%d,%d", j, i);
		
			if(omPosMap.Lookup(buffer, (void*&) prlCell))
			{
				olDelMap.RemoveKey((void*&)prlCell);
				prlCell->SetMultiSel(true);
			}
		}
	}

	POSITION pos;
	void *pVoid;
	for( pos = olDelMap.GetStartPosition(); pos != NULL; )
	{
		olDelMap.GetNextAssoc( pos, pVoid , (void *&)prlCell );
		prlCell->SetMultiSel(false);
	}
	olDelMap.RemoveAll();

}






void SeasonCollectDlg::FillMatrix(CString opField) 
{
	omField = opField;
	
	CCSCell *prlCell;
	CString olKey;
	CCSPtrArray<COLLECTFLIGHTDATA> *polDayFlights; 
	COLLECTFLIGHTDATA *prlFlight;

	CTimeSpan olOneDay(1,0,0,0);
	CTime olUtcTime;
	GetCurrentUtcTime(olUtcTime); 
	CTime olToday (olUtcTime.GetYear(),olUtcTime.GetMonth(),olUtcTime.GetDay(),0,0,0);

	CTime olPostflightDay = olUtcTime - ogTimeSpanPostFlight - olOneDay;
	CTime olPostflightDayFuture = olUtcTime + ogTimeSpanPostFlightInFuture;// + olOneDay;
//	CString olStrPostflightDay = olPostflightDay.Format("%Y%m%d");
//	CString olStrPostflightDayF = olPostflightDayFuture.Format("%Y%m%d");

	int ilCount = omCells.GetSize();

	for(int i = 0; i < ilCount; i++)
	{
		prlCell = &omCells[i];

//		postflights can't selected
		CString cellname = prlCell->GetName();
		CTime olCellTime = DateStringToDate(cellname);
//		CString newCellname = olCellTime.Format("%Y%m%d");

		if (olCellTime == olToday) 
			prlCell->SetFocusRect(2);

		if (ogTimeSpanPostFlight.GetDays() > 0)
		{	
			if (prlCell->GetName() == "" || olCellTime < olPostflightDay)
				prlCell->Disable(true);
		}
		if (ogTimeSpanPostFlightInFuture.GetDays() > 0)
		{
			if (prlCell->GetName() == "" || olCellTime > olPostflightDayFuture)
				prlCell->Disable(true);
		}

		olKey = prlCell->GetName() + opField.Left(1);
		if(olKey.GetLength() > 1)
		{

			if((polDayFlights = ogSeasonCollectFlights.GetFlightsByDayKey(olKey)) == NULL)
			{
				if(opField.Left(1) == "A")
					olKey = prlCell->GetName() + CString("D");
				else
					olKey = prlCell->GetName() + CString("A");
				if((polDayFlights = ogSeasonCollectFlights.GetFlightsByDayKey(olKey)) == NULL)
				{
					prlCell->SetText("");
				}
				else
				{
					prlCell->SetText("GS");
				}
			}
			else
			{
				prlFlight = &(*polDayFlights)[0];
				
				if((*polDayFlights).GetSize() > 1)
				{
					prlCell->SetBKColor(RGB(255,0,0));					
				}
				else
				{
					COLORREF olBKColor = prlCell->omBKColor;
					if (olBKColor == COLORREF(RGB(255,0,0)))
						prlCell->SetBKColor(RGB(255,255,255));	
				}

				prlCell->SetTextColor(GetColorOfFlight(prlFlight->Ftyp[0]));

				prlCell->SetText(GetValue(prlFlight, opField.Right(4)));
			}
		}
	}
}

CString SeasonCollectDlg::GetValue(COLLECTFLIGHTDATA *prpFlight, CString opField) 
{
	CString olReturn;
	CTime olTime;

	if(opField == "FLNO")
	{
		olReturn = CString(prpFlight->Flno);
	}else

	if(opField == "FLTI")
	{
		olReturn = CString(prpFlight->Flti);
	}else

	if(opField == "CSGN")
	{
		olReturn = CString(prpFlight->Csgn);
	}else

	if(opField == "ACT3")
	{
		olReturn = CString(prpFlight->Act3);
	}else

	if(opField == "STOA")
	{
		olTime = prpFlight->Stoa;
		if(bgSeasonLocal) ogBasicData.UtcToLocal(olTime);
		olReturn = olTime.Format("%H:%M");
	}else

	if(opField == "STOD")
	{
		olTime = prpFlight->Stod;
		if(bgSeasonLocal) ogBasicData.UtcToLocal(olTime);
		olReturn = olTime.Format("%H:%M");
	}else

	if(opField == "ETDU")
	{
		olTime = prpFlight->Etdi;
		if(bgSeasonLocal) ogBasicData.UtcToLocal(olTime);
		olReturn = olTime.Format("%H:%M");
	}else

	if(opField == "ETAU")
	{
		olTime = prpFlight->Etai;
		if(bgSeasonLocal) ogBasicData.UtcToLocal(olTime);
		olReturn = olTime.Format("%H:%M");
	}else

	if(opField == "FTYP")
	{
		olReturn = CString(prpFlight->Ftyp);
		if(olReturn == "X")
		{
			olReturn = "CXX";
		}else
		if(olReturn == "N")
		{
			olReturn = "NOOP";
		}else
		if(olReturn == "S")
		{
			olReturn = GetString(IDS_STRING1987);
		}else
		if(olReturn == "O")
		{
			olReturn = GetString(IDS_STRING1422);
		}else
		if(olReturn == "")
		{
			olReturn = GetString(IDS_STRING1423);
		}

	}else

	if(opField == "ORG3")
	{
		olReturn = CString(prpFlight->Org3);
	}else

	if(opField == "DES3")
	{
		olReturn = CString(prpFlight->Des3);
	}else

	if(opField == "GTA1")
	{
		olReturn = CString(prpFlight->Gta1);
	}else

	if(opField == "GTA2")
	{
		olReturn = CString(prpFlight->Gta2);
	}else

	if(opField == "GTD2")
	{
		olReturn = CString(prpFlight->Gtd2);
	}else

	if(opField == "GTD1")
	{
		olReturn = CString(prpFlight->Gtd1);
	}else

	if(opField == "TTYP")
	{
		olReturn = CString(prpFlight->Ttyp);
	}else

	if(opField == "HTYP")
	{
		olReturn = CString(prpFlight->Htyp);
	}else

	if(opField == "STEV")
	{
		olReturn = CString(prpFlight->Stev);
	}else

	if(opField == "STYP")
	{
		olReturn = CString(prpFlight->Styp);
	}else

	if(opField == "ISRE")
	{
		olReturn = CString(prpFlight->Isre);
	}else

	if(opField == "PSTA")
	{
		olReturn = CString(prpFlight->Psta);
	}else

	if(opField == "WRO1")
	{
		olReturn = CString(prpFlight->Wro1);
	}else

	if(opField == "PSTD")
	{
		olReturn = CString(prpFlight->Pstd);
	}else

	if(opField == "EXT1")
	{
		olReturn = CString(prpFlight->Ext1);
	}else

	if(opField == "EXT2")
	{
		olReturn = CString(prpFlight->Ext2);
	}else

	if(opField == "BLT1")
	{
		olReturn = CString(prpFlight->Blt1);
	}else

	if(opField == "BLT2")
	{
		olReturn = CString(prpFlight->Blt2);
	}else

		
	if(opField == "VIAL")
	{
		/*
		if(strcmp(prpFlight->Adid,"A") == 0)
		{
			olReturn = CString(prpFlight->Vial);

			int ilPos = ((atoi(prpFlight->Vian) - imContPos) * 120 ) + 1;

			if(ilPos < 0)
				olReturn = "";
			else
				olReturn = olReturn.Mid(ilPos, 3);
		}
		else
		*/
		{
			olReturn = CString(prpFlight->Vial);

			if(olReturn.GetLength() > 0 )
			{
				int ilPos = ((imContPos - 1) * 120 ) + 1;

				if(ilPos + 3 < olReturn.GetLength() )
					olReturn = olReturn.Mid(ilPos, 3);
				else
					olReturn = "";
			}
		}


	}else

	if(opField == "VIAN")
	{
		olReturn = CString(prpFlight->Vian);
	}else

	if(opField == "JFNO")
	{
		olReturn = CString(prpFlight->Jfno);


		int ilPos = ((imContPos - 1) * 9 );


		if(ilPos + 9 < olReturn.GetLength() )
			olReturn = olReturn.Mid(ilPos, 9);
		else
		{
			if(ilPos + 1 < olReturn.GetLength() )
			{
				olReturn = olReturn.Mid(ilPos ,olReturn.GetLength() - ilPos );
			}
			else
			{
				olReturn = "";
			}
		}

	}else

	if(opField == "CKIF")
	{
		olReturn = CString(prpFlight->Ckif) + CString(" - ") + CString(prpFlight->Ckit);

	}else

	if(opField == "NOSE")
	{
		olReturn = CString(prpFlight->Nose);
	}


	olReturn.TrimRight();
	olReturn.TrimLeft();

	if(olReturn.IsEmpty())
		olReturn = "---";

	return olReturn;
}

void SeasonCollectDlg::OnSelchangeShowfield() 
{
	int i = m_CL_Showfield.GetCurSel();
	CString olField;
	if(i != LB_ERR)
	{
		m_CL_Showfield.GetLBText(i, olField);
		olField = olField.Right(5);
		
		if((olField.Find("VIAL") >= 0) || (olField.Find("JFNO") >= 0) || (olField.Find("HSNA") >= 0))
		{
			m_CS_Number.ShowWindow(SW_SHOW);
			m_CB_UpDown.ShowWindow(SW_SHOW);
			m_CB_UpDown.SetPos(1);
			imContPos = 1;
		}
		else
		{
			m_CS_Number.ShowWindow(SW_HIDE);
			m_CB_UpDown.ShowWindow(SW_HIDE);
		}

		FillMatrix(olField.Right(5));

		if(omUFRAdid != olField[0])
		{
			omUFRAdid = olField[0];
			InitTables(omUFRAdid);
		}

		DisplayRemarks();
	}
}


void SeasonCollectDlg::OnFlno() 
{
	LoadNewData("", "", "", "", TIMENULL, "");

	OnDeselect();	
	OnTablesdeselect();
	OnTablesreset();
}


static void SeasonCollectCf(void *popInstance, int ipDDXType,
    void *vpDataPointer, CString &ropInstanceName)
{

    SeasonCollectDlg *polDlg = (SeasonCollectDlg *)popInstance;

    if (ipDDXType == DATA_RELOAD) 
        polDlg->DataReload();
	
    if ((ipDDXType == C_FLIGHT_CHANGE) || (ipDDXType == C_FLIGHT_DELETE))
        polDlg->ProcessFlightChange((COLLECTFLIGHTDATA *)vpDataPointer);

    if(ipDDXType == S_DLG_GET_NEXT)
        polDlg->SeasonDlgGetNext((NEW_SEASONDLG_DATA *)vpDataPointer);

    if(ipDDXType == S_DLG_GET_PREV)
        polDlg->SeasonDlgGetPrev((NEW_SEASONDLG_DATA *)vpDataPointer);


}

void SeasonCollectDlg::ProcessFlightChange(COLLECTFLIGHTDATA *prpFlight)
{
	if(prpFlight == NULL)
		return;

	CCSCell *prlCell;
	CString olKey;
	COLLECTFLIGHTDATA *prlFlight;
	CCSPtrArray<COLLECTFLIGHTDATA> *polDayFlights; 


	if(strcmp(prpFlight->Adid, "A") == 0)
		olKey = prpFlight->Tifa.Format("%d%m%Y");
	else
		olKey = prpFlight->Tifd.Format("%d%m%Y");

	if(omDayMap.Lookup(olKey,  (void*&)prlCell) == TRUE)
	{
		olKey = prlCell->GetName() + omField.Left(1);

		if((polDayFlights = ogSeasonCollectFlights.GetFlightsByDayKey(olKey)) == NULL)
		{
			if(omField.Left(1) == "A")
				olKey = prlCell->GetName() + CString("D");
			else
				olKey = prlCell->GetName() + CString("A");
			if((polDayFlights = ogSeasonCollectFlights.GetFlightsByDayKey(olKey)) == NULL)
			{
				prlCell->SetText("");
			}
			else
			{
				prlCell->SetText("GS");
			}
		}
		else
		{
			prlFlight = &(*polDayFlights)[0];
			
			if((*polDayFlights).GetSize() > 1)
			{
				prlCell->SetBKColor(RGB(255,0,0));
			}
			prlCell->SetTextColor(GetColorOfFlight(prlFlight->Ftyp[0]));

			prlCell->SetText(GetValue(prlFlight, omField.Right(4)));
		}
	}
}



void SeasonCollectDlg::OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar) 
{

	if((omField.Find("VIAL") >= 0) || (omField.Find("JFNO") >= 0) || (omField.Find("HSNA") >= 0))
	{
		if(imContPos != m_CB_UpDown.GetPos())
		{

			imContPos = m_CB_UpDown.GetPos();
			FillMatrix(omField); 
		}
	}

	CDialog::OnVScroll(nSBCode, nPos, pScrollBar);
}


void SeasonCollectDlg::OnTablesdeselect() 
{
	pomTable->SelectLine(-1,FALSE);
	
	pomViaTableCtrl->SelectColumn(-1,false);
	
	pomJfnoTable->SelectLine(-1,FALSE);
	pomCinsTable->SelectLine(-1,FALSE);
	bmChanged = false;
	m_CB_OK.SetColors(::GetSysColor(COLOR_BTNFACE), ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
}


LONG SeasonCollectDlg::OnTableSelChanged( UINT wParam, LPARAM lParam)
{
	bmChanged = true; 
	m_CB_OK.SetColors(RGB(255,0,0), ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
	return 0L;
}



void SeasonCollectDlg::OnTablesreset() 
{
	int i;
	bmChanged = true; // sonst wird button doch rot

	m_CB_OK.SetColors(::GetSysColor(COLOR_BTNFACE), ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));


	for(i = pomTable->GetLinesCount() - 1; i >= 0; i--)
	{
		pomTable->SetIPValue(i, 1, "");
	}

	pomViaTableCtrl->SetViaList("", TIMENULL, bgSeasonLocal);


	for(i = pomJfnoTable->GetLinesCount() - 1; i >= 0; i--)
	{
		pomJfnoTable->SetIPValue(i, 1, "");
		pomJfnoTable->SetIPValue(i, 2, "");
		pomJfnoTable->SetIPValue(i, 3, "");
	}

	for(i = pomCinsTable->GetLinesCount() - 1; i >= 0; i--)
	{
		pomCinsTable->SetIPValue(i, 1, "");
		pomCinsTable->SetIPValue(i, 2, "");
		pomCinsTable->SetIPValue(i, 3, "");
		pomCinsTable->SetIPValue(i, 4, "");
	}

//	pomHAIDlg->Clear();

	m_CR_Betrieb.SetCheck(FALSE);
	m_CR_Planung.SetCheck(FALSE);
	m_CR_Prognose.SetCheck(FALSE);
	m_CR_Noop.SetCheck(FALSE);
	bmChanged = false;
}

void SeasonCollectDlg::OnCinsmal4() 
{
	int i = pomCinsTable->pomListBox->GetTopIndex();
	CString olCkic;
	CString olCkit;
	CString olCkbs;
	CString olCkes;

	pomCinsTable->GetTextFieldValue(i, 1, olCkic);
	pomCinsTable->GetTextFieldValue(i, 2, olCkit);
	pomCinsTable->GetTextFieldValue(i, 3, olCkbs);
	pomCinsTable->GetTextFieldValue(i, 4, olCkes);


	ogBCD.SetSort("CIC","CNAM+",true);

	int ilCount = ogBCD.GetDataCount("CIC");

	CString  olCnam;

	for(int j = 0; j < ilCount; j++)
	{
		olCnam = ogBCD.GetField("CIC",j,"CNAM");

		if(olCnam == olCkic)
			break;
	}

	pomCinsTable->SetIPValue(i + 1, 1, ogBCD.GetField("CIC",j + 1,"CNAM"));
	pomCinsTable->SetIPValue(i + 2, 1, ogBCD.GetField("CIC",j + 2,"CNAM"));
	pomCinsTable->SetIPValue(i + 3, 1, ogBCD.GetField("CIC",j + 3,"CNAM"));

	pomCinsTable->SetIPValue(i + 1, 2, ogBCD.GetField("CIC",j + 1,"TERM"));
	pomCinsTable->SetIPValue(i + 2, 2, ogBCD.GetField("CIC",j + 2,"TERM"));
	pomCinsTable->SetIPValue(i + 3, 2, ogBCD.GetField("CIC",j + 3,"TERM"));

	pomCinsTable->SetIPValue(i + 1, 3, olCkbs);
	pomCinsTable->SetIPValue(i + 1, 4, olCkes);
	pomCinsTable->SetIPValue(i + 2, 3, olCkbs);
	pomCinsTable->SetIPValue(i + 2, 4, olCkes);
	pomCinsTable->SetIPValue(i + 3, 3, olCkbs);
	pomCinsTable->SetIPValue(i + 3, 4, olCkes);

}



void SeasonCollectDlg::OnAgents() 
{
	RotationHAIDlg polRotationHAIDlg(this, omAlc3, omAlc3);
	if (polRotationHAIDlg.DoModal() == IDOK)
	{
	}
}



void SeasonCollectDlg::GetSelectFlights(CCSPtrArray<COLLECTFLIGHTDATA> &olFlights, CString &olRkeys, bool bpJoinFlights) 
{
	CCSCell *prlCell;
	CString olKey;
	CCSPtrArray<COLLECTFLIGHTDATA> *polDayFlights; 
	COLLECTFLIGHTDATA *prlFlight;

	int ilCount = omCells.GetSize();

	CString olAdid = omUFRAdid;

	//if(bpJoinFlights)
	//	olAdid = "A";

	for(int i = 0; i < ilCount; i++)
	{
		prlCell = &omCells[i];

		if(	prlCell->IsSelect())
		{
			olKey = prlCell->GetName() + olAdid;
			if(olKey.GetLength() > 1)
			{
				if((polDayFlights = ogSeasonCollectFlights.GetFlightsByDayKey(olKey)) != NULL)
				{
					for(int j = (*polDayFlights).GetSize() - 1; j >= 0; j--)
					{
						prlFlight  = &(*polDayFlights)[j];
						
						if((bpJoinFlights && (ogSeasonCollectFlights.GetJoinFlight(prlFlight) != NULL)) || !bpJoinFlights)						
						{
							//don't put postflights into the modifylist
							if (!CheckPostFlight(prlFlight) && IsRegularFlight(prlFlight->Ftyp[0]))
							{
								olFlights.Add(prlFlight);

								char buffer[64];
								itoa(prlFlight->Rkey, buffer,10);
								olRkeys += buffer + CString(",");
							}
						}
					}
				}
			}
		}
	}

	if(olRkeys.GetLength() > 0)
		olRkeys = olRkeys.Left(olRkeys.GetLength()-1);

}



void SeasonCollectDlg::OnDelete()
{
		
	CCSPtrArray<COLLECTFLIGHTDATA> olFlights;
	COLLECTFLIGHTDATA *prlFlight;
	char buffer[64];
	CCSCell *prlCell;
	CString olKey;
	CString olUrnoList;

	COLORREF olOldSelColor;

	CString olRkeys;
	GetSelectFlights(olFlights, olRkeys);

	int ilCount = olFlights.GetSize();

	if(ilCount <= 0)
	{
		CFPMSApp::MyTopmostMessageBox(this,GetString(IDS_STRING966), GetString(IDS_STRING908), MB_ICONQUESTION | MB_OK);
//		::MessageBox(this->m_hWnd, GetString(IDS_STRING966), GetString(IDS_STRING908), MB_APPLMODAL | MB_SETFOREGROUND);
		return;
	}


	itoa(ilCount, buffer, 10);

	CString olMessage;

	if(omUFRAdid == "A")
		olMessage.Format(GetString(IDS_STRING1335), buffer); 
	else
		olMessage.Format(GetString(IDS_STRING1336), buffer); 
		

//	if( ::MessageBox(this->m_hWnd, olMessage, GetString(ST_FRAGE), MB_YESNO | MB_APPLMODAL | MB_SETFOREGROUND) == IDYES)
	if( CFPMSApp::MyTopmostMessageBox(this, olMessage, GetString(ST_FRAGE), MB_YESNO | MB_ICONQUESTION) == IDYES)
	{
		SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
		m_CB_Delete.SetColors(RGB(0,255,0), ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));

		ogBcHandle.BufferBc();

		for(int i = 0; i < ilCount; i++)
		{
			prlFlight = &olFlights[i];

			CTime olTime;
			if(strcmp(prlFlight->Adid, "A") == 0)
			{
				olTime = prlFlight->Stoa;
			}
			else
			{
				olTime = prlFlight->Stod;
			}
			if(bgSeasonLocal) ogBasicData.UtcToLocal(olTime);
			olKey = olTime.Format("%d%m%Y");

			if(omDayMap.Lookup(olKey,  (void*&)prlCell) == TRUE)
				olOldSelColor = prlCell->SetSelBKColor(RGB(0,255,0));

			ogSeasonCollectFlights.DeleteFlight(prlFlight->Urno);	

			ogBcHandle.BufferBc();

			if(omDayMap.Lookup(olKey,  (void*&)prlCell) == TRUE)
				prlCell->SetSelBKColor(olOldSelColor);

		}
		/*
		if(!olUrnoList.IsEmpty())
		{
			olUrnoList = olUrnoList.Right(olUrnoList.GetLength() - 1);

			ogSeasonCollectFlights.DeleteFlight(olUrnoList);	
		}
		*/
		ogBcHandle.ReleaseBuffer();

		m_CB_Delete.SetColors(::GetSysColor(COLOR_BTNFACE), ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
		SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
	}

	olFlights.RemoveAll();

}




void SeasonCollectDlg::OnOK() 
{

	CString olMessage = GetInputList();


	if(!olMessage.IsEmpty())
	{
		olMessage = CString(GetString(IDS_STRING967)) + olMessage;
		CFPMSApp::MyTopmostMessageBox(this,olMessage, GetString(IDS_STRING908), MB_ICONQUESTION | MB_OK);
//		::MessageBox(/*NULL*/this->m_hWnd, olMessage, GetString(IDS_STRING908), MB_APPLMODAL | MB_SETFOREGROUND);
		return;
	}

	/*
	for(int i = omInputField.GetSize() - 1; i >= 0; i--)
	{
		TRACE("\n FIELD:< %s > DATA:< %s >", omInputField[i], omInputData[i]);
	}
	*/

	CCSPtrArray<COLLECTFLIGHTDATA> olFlights;
	COLLECTFLIGHTDATA *prlFlight;
	char buffer[64];
	CCSCell *prlCell;
	CString olKey;

	CString olFieldList;
	CString olDataList;
	CString olField;
	CString olData;
	CString olSelection;
	CString olTmp;

	COLORREF olOldSelColor;


	CString olRkeys;
	GetSelectFlights(olFlights, olRkeys);

	int ilCount = olFlights.GetSize();

	if(ilCount <= 0)
	{
		CFPMSApp::MyTopmostMessageBox(this,GetString(IDS_STRING968), GetString(IDS_STRING908), MB_ICONQUESTION | MB_OK);
//		::MessageBox(this->m_hWnd, GetString(IDS_STRING968), GetString(IDS_STRING908), MB_APPLMODAL | MB_SETFOREGROUND);
		return;
	}

	itoa(ilCount, buffer, 10);
	

	if(omUFRAdid == "A")
		olMessage = CString(GetString(IDS_STRING969)) + CString(buffer) + CString(GetString(IDS_STRING970));
	else
		olMessage = CString(GetString(IDS_STRING969)) + CString(buffer) + CString(GetString(IDS_STRING971));


	AskBox olDlg(this, GetString(IDS_STRING1985), GetString(IDS_STRING1986), olMessage, GetString(ST_FRAGE));
 	if (olDlg.DoModal() == 1)
	{
		// search if act is in the modifylist
		//Act5 is automatic filled in GetInputList() if Act3 is will be filled
		CString olDataAct3 ("");
		CString olDataAct5 ("");
		CString olDataFlti ("");
		BOOL blAskForACT = FALSE;
		bool blRemoveFlti = false;

		for (int i=0; i<omInputField.GetSize(); i++)
		{
			if(omInputField[i] == "ACT3")
			{
				olDataAct3 = omInputData[i];
				blAskForACT = TRUE;
			}
			if(omInputField[i] == "ACT5")
			{
				olDataAct5 = omInputData[i];
				blAskForACT = TRUE;
			}

			if(omInputField[i] == "FLTI")
			{
				olDataFlti = omInputData[i];
 				if (olDataFlti.IsEmpty())
				{
					if (CFPMSApp::MyTopmostMessageBox(this,GetString(IDS_STRING2130), GetString(ST_FRAGE), MB_ICONQUESTION | MB_YESNO) != IDYES)
						blRemoveFlti = true;
				}

				if (olDataFlti != "I" && olDataFlti != "D" && !olDataFlti.IsEmpty() )
				{
					CFPMSApp::MyTopmostMessageBox(this,GetString(IDS_STRING2131), GetString(ST_FEHLER), MB_ICONWARNING);
					blRemoveFlti = true;
				}

				if (blRemoveFlti)
				{
					omInputData.RemoveAt(i);
					omInputField.RemoveAt(i);
				}

			}

			if (omInputField.GetSize() > 0)
			{
				bool blCheckArrAtHopo = false;
				bool blCheckDepAtHopo = false;
				CString olTime ("");
				if(omUFRAdid == "A" && omInputField[i] == "STOA")
					blCheckArrAtHopo = true;

				if(omUFRAdid == "D" && omInputField[i] == "STOD")
					blCheckDepAtHopo = true;

				if (blCheckArrAtHopo || blCheckDepAtHopo)
				{
					olTime = omInputData[i];
					CString olWarning ("");
					int ilStdCount = 0;
					for(int m = 0; m < ilCount; m++)
					{
						COLLECTFLIGHTDATA *prlFlightRot = NULL;
						prlFlight = &olFlights[m];
						if (prlFlight)
						{
							CTime olTimeRot = TIMENULL;
							CTime olTimeAkt = TIMENULL;
							if (blCheckArrAtHopo)
							{
								prlFlightRot = ogSeasonCollectFlights.GetDeparture(prlFlight);
								if (prlFlightRot)
									olTimeRot = prlFlightRot->Stod;
								if (prlFlight->Stoa != TIMENULL)
									olTimeAkt = HourStringToDate( olTime, prlFlight->Stoa);
							}
							else
							{
								prlFlightRot = ogSeasonCollectFlights.GetArrival(prlFlight);
								if (prlFlightRot)
									olTimeRot = prlFlightRot->Stoa;
								if (prlFlight->Stod != TIMENULL)
									olTimeAkt = HourStringToDate( olTime, prlFlight->Stod);
							}

							if (olTimeRot != TIMENULL && olTimeAkt != TIMENULL)
							{
								bool  blMeldung = false;
								CString olStrStd ("");
								CString olStrSta  ("");
								if (blCheckArrAtHopo && (olTimeRot < olTimeAkt))
								{
									blMeldung = true;
									olStrStd = olTimeRot.Format("%d.%m.%Y - %H:%M");
									olStrSta = olTimeAkt.Format("%d.%m.%Y - %H:%M");
								}
								if (blCheckDepAtHopo && (olTimeRot > olTimeAkt))
								{
									blMeldung = true;
									olStrStd = olTimeAkt.Format("%d.%m.%Y - %H:%M");
									olStrSta = olTimeRot.Format("%d.%m.%Y - %H:%M");
								}

								if (blMeldung)
								{
									ilStdCount ++;
									if (ilStdCount > 30)
									{
										CString olMore ("");
										olMore.Format(GetString(IDS_STRING2178), ilStdCount); 

										olWarning += olMore;
										break;
									}
									olStrStd += " < " + olStrSta + "\n";
									olWarning += olStrStd;
								}
							}
						}
					}

					if (!olWarning.IsEmpty())
					{
						CString olTmp ("");
						olTmp.Format(GetString(IDS_STRING2177), olWarning); 
						if (CFPMSApp::MyTopmostMessageBox(this, olTmp, GetString(IDS_WARNING), MB_ICONWARNING | MB_YESNOCANCEL) != IDYES)
	/*								AskBox ACDlg(this, GetString(IDS_STRING1985), GetString(IDS_STRING1986), olTmp, GetString(ST_FRAGE));
						int ilACDlg = ACDlg.DoModal();
 						if (ilACDlg == 1)
							break;
 						else*/
						{
							olFlights.RemoveAll();
							bmChanged = false;
							m_CB_OK.SetColors(::GetSysColor(COLOR_BTNFACE), ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
							return;
						}
					}
				}
			}
		}

		// if act is in the modifylist ask user how to use the act in the rotation
		BOOL blWishToSplit = FALSE;
		BOOL blActCancel = FALSE;
		if(blAskForACT)		
		{
			AskBox ACDlg(this, GetString(IDS_STRING1985), GetString(IDS_STRING1986), GetString(IDS_STRING2060), GetString(ST_FRAGE));
			int ilACDlg = ACDlg.DoModal();
 			if (ilACDlg == 1)
				blWishToSplit = FALSE;
 			else if (ilACDlg == 2)
				blWishToSplit = TRUE;
			else
				blActCancel = TRUE;
		}

		// do nothing if canceled
		if (!blActCancel)
		{
			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
			m_CB_OK.SetColors(RGB(0,255,0), ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
			
			ogBcHandle.GetBc();
			//ogBcHandle.BufferBc();

			int ilCount2 = 0;

			for(int i = 0; i < ilCount; i++)
			{
				prlFlight = &olFlights[i];

				ilCount2++;

				CTime olTime;
				if(strcmp(prlFlight->Adid, "A") == 0)
				{
					olTime = prlFlight->Stoa;
				}
				else
				{
					olTime = prlFlight->Stod;
				}
				if(bgSeasonLocal) ogBasicData.UtcToLocal(olTime);
				olKey = olTime.Format("%d%m%Y");


				if(omDayMap.Lookup(olKey,  (void*&)prlCell) == TRUE)
					olOldSelColor = prlCell->SetSelBKColor(RGB(0,255,0));


				GetDataList(prlFlight, olField, olData);

				// if the act is set to whole rotation, we must change the act for arr bevor dep.
				// outherweise the flight will slpit the rotation by itself and will not change
				// the act for arr.
				// if arr is connected the flight change the whole rotation correct.
				if(omUFRAdid == "D" && !blWishToSplit && blAskForACT)
				{
					COLLECTFLIGHTDATA *prlAFlight = ogSeasonCollectFlights.GetArrival(prlFlight);

					if (prlAFlight)
					{
						COLLECTFLIGHTDATA  prlAFlightSave = *prlAFlight;
						//Act5 is automatic filled in GetInputList() if Act3 is will be filled
						strcpy(prlAFlight->Act3, olDataAct3);
						strcpy(prlAFlight->Act5, olDataAct5);
						ogSeasonCollectFlights.UpdateFlight(prlAFlight, &prlAFlightSave);	
					}
				}

				// if the user want change only the connected arr or dep, he also want split the rotation
				if(blWishToSplit && blAskForACT)
					ogSeasonCollectFlights.SplitFlight(prlFlight, omUFRAdid[0]);	


				if(!olField.IsEmpty())
				{
					olDataList = olDataList + olData + CString("\n");
					olFieldList = olFieldList + olField + CString("\n");
					olTmp.Format("WHERE URNO = %ld", prlFlight->Urno);
					olSelection = olSelection + olTmp + CString("\n");
				}

				if((olDataList.GetLength() + olSelection.GetLength() + olFieldList.GetLength()) >= 4000  || ilCount2 >= 14)
				{
					olDataList = olDataList.Left(olDataList.GetLength() -1 );
					olFieldList = olFieldList.Left(olFieldList.GetLength() -1 );
					olSelection = olSelection.Left(olSelection.GetLength() -1 );
					ogSeasonCollectFlights.UpdateFlight(olSelection, olFieldList, olDataList);	
					
					olDataList = "";
					olFieldList = "";
					olSelection = "";
					ilCount2 = 0;

				}
				SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));

				//TRACE("\n\n FIELD:<%s>", olFieldList);
				//TRACE("\n DATA:<%s> \n\n", olDataList);
				

				//ogBcHandle.BufferBc();
				ogBcHandle.GetBc();

				if(omDayMap.Lookup(olKey,  (void*&)prlCell) == TRUE)
					prlCell->SetSelBKColor(olOldSelColor);

			}


			if(!olFieldList.IsEmpty() && !olDataList.IsEmpty())
			{
				SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
				olDataList = olDataList.Left(olDataList.GetLength() -1 );
				olFieldList = olFieldList.Left(olFieldList.GetLength() -1 );
				olSelection = olSelection.Left(olSelection.GetLength() -1 );

	/*
				CStringArray olTest;

				ExtractItemList(olDataList, &olTest, '\n');

				int il = olTest.GetSize();

				olTest.RemoveAll();



				ExtractItemList(olFieldList, &olTest, '\n');

				il = olTest.GetSize();

				olTest.RemoveAll();


				ExtractItemList(olSelection, &olTest, '\n');

				il = olTest.GetSize();

				olTest.RemoveAll();
	*/

				ogSeasonCollectFlights.UpdateFlight(olSelection, olFieldList, olDataList);	
			}

			for(int k = 0; k < 10; k++)
			{
				SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
				ogBcHandle.GetBc();
				Sleep(100);
			}

			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
		}

	}
	
	olFlights.RemoveAll();
	
	// speichern
	bmChanged = false;
	m_CB_OK.SetColors(::GetSysColor(COLOR_BTNFACE), ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));

}

bool SeasonCollectDlg::GetDataList(COLLECTFLIGHTDATA *prpFlight, CString &opFieldList, CString &opDataList) 
{
	if(prpFlight == NULL)
		return false;


	CString olField;
	CTime olTime;
	CTime olStoLocal;
	char buffer[64];

	opFieldList = CString("");
	opDataList = CString("");

	if(strcmp(prpFlight->Adid, "A") == 0)
	{
		olStoLocal = prpFlight->Stoa;
	}
	else
	{
		olStoLocal = prpFlight->Stod;
	}

	CString olTimeFields = CString("STOA,STOD,ETAU,ETDU");

	int ilCount = omInputField.GetSize();

	for(int i = 0; i < ilCount; i++)
	{
		olField = omInputField[i];


		if(olTimeFields.Find(olField) >= 0)		
		{
			if(olField == "STOA")
			{
				olTime = olStoLocal;
				if(bgSeasonLocal) ogBasicData.UtcToLocal(olTime);
				olTime = HourStringToDate(omInputData[i] , olTime ,true);
				if(strcmp(prpFlight->Adid, "A") == 0)
					olStoLocal = olTime;
				if(bgSeasonLocal) if(bgSeasonLocal) ogBasicData.LocalToUtc(olTime);
				if(olTime != TIMENULL)
					opDataList +=  CString(",") + CTimeToDBString(olTime , TIMENULL);
				else
					opDataList +=  CString(", ");

				opFieldList +=  CString(",") + olField;
			}
			if(olField == "STOD")
			{
				olTime = olStoLocal;
				if(bgSeasonLocal) ogBasicData.UtcToLocal(olTime);
				olTime = HourStringToDate(omInputData[i] , olTime,true);
				if(strcmp(prpFlight->Adid, "D") == 0)
					olStoLocal = olTime;
				if(bgSeasonLocal) ogBasicData.LocalToUtc(olTime);
				if(olTime != TIMENULL)
					opDataList +=  CString(",") + CTimeToDBString(olTime , TIMENULL);
				else
					opDataList +=  CString(", ");
				opFieldList +=  CString(",") + olField;
			}
			if(olField == "ETDU")
			{
				olTime = prpFlight->Stod;
				if(bgSeasonLocal) ogBasicData.UtcToLocal(olTime);
				olTime = HourStringToDate(omInputData[i] , olTime,true);
				if(bgSeasonLocal) ogBasicData.LocalToUtc(olTime);
				if(olTime != TIMENULL)
					opDataList +=  CString(",") + CTimeToDBString(olTime , TIMENULL);
				else
					opDataList +=  CString(", ");
				opFieldList +=  CString(",") + olField;
			}
			if(olField == "ETAU")
			{
				olTime = prpFlight->Stoa;
				if(bgSeasonLocal) ogBasicData.UtcToLocal(olTime);
				olTime = HourStringToDate(omInputData[i] , olTime,true);
				if(bgSeasonLocal) ogBasicData.LocalToUtc(olTime);
				if(olTime != TIMENULL)
					opDataList +=  CString(",") + CTimeToDBString(olTime , TIMENULL);
				else
					opDataList +=  CString(", ");
				opFieldList +=  CString(",") + olField;
			}

		}
		else
		{
			if(olField == "GTD1")
			{
				opFieldList +=  CString(",") + "GD1P";
				opDataList +=  CString(",") + omInputData[i];
			}
			else if(olField == "GTD2")
			{
				opFieldList +=  CString(",") + "GD2P";
				opDataList +=  CString(",") + omInputData[i];
			}
			else
			{
				opFieldList +=  CString(",") + olField;
				opDataList +=  CString(",") + omInputData[i];
			}

			if(olField == "REM1")
			{

				opFieldList +=  CString(",ISRE");

				CString olTmp = omInputData[i];
				olTmp.TrimRight();
				if(olTmp.IsEmpty())
				{
					opDataList +=  CString(", ");
				}
				else
				{
					opDataList +=  CString(",+");
				}

			}

			if(olField == "PSTA")
			{
				opFieldList +=  CString(",PABS");
				opDataList +=  CString(",") + CTimeToDBString(prpFlight->Tifa, TIMENULL);
				
				CTimeSpan olDura;
				GetPosDefAllocDur(omInputData[i], 0, olDura);

				opFieldList +=  CString(",PAES");
				opDataList +=  CString(",") + CTimeToDBString(prpFlight->Tifa + olDura, TIMENULL);
			}

			if(olField == "PSTD")
			{
				opFieldList +=  CString(",PDES");
				opDataList +=  CString(",") + CTimeToDBString(prpFlight->Tifd, TIMENULL);

				CTimeSpan olDura;
				GetPosDefAllocDur(omInputData[i], 0, olDura);

				opFieldList +=  CString(",PDBS");
				opDataList +=  CString(",") + CTimeToDBString(prpFlight->Tifd - olDura, TIMENULL);
			}

			if(olField == "GTA1")
			{
				opFieldList +=  CString(",GA1B");
				opDataList +=  CString(",") + CTimeToDBString(prpFlight->Tifa, TIMENULL);

				CTimeSpan olDura;
				GetGatDefAllocDur(omInputData[i], olDura);

				opFieldList +=  CString(",GA1E");
				opDataList +=  CString(",") + CTimeToDBString(prpFlight->Tifa + olDura, TIMENULL);
			}

			if(olField == "GTA2")
			{
				opFieldList +=  CString(",GA2B");
				opDataList +=  CString(",") + CTimeToDBString(prpFlight->Tifa, TIMENULL);

				CTimeSpan olDura;
				GetGatDefAllocDur(omInputData[i], olDura);
				
				opFieldList +=  CString(",GA2E");
				opDataList +=  CString(",") + CTimeToDBString(prpFlight->Tifa + olDura, TIMENULL);
			}

			if(olField == "GTD1")
			{
				opFieldList +=  CString(",GD1E");
				opDataList +=  CString(",") + CTimeToDBString(prpFlight->Tifd, TIMENULL);

				CTimeSpan olDura;
				GetGatDefAllocDur(omInputData[i], olDura);
				
				opFieldList +=  CString(",GD1B");
				opDataList +=  CString(",") + CTimeToDBString(prpFlight->Tifd - olDura, TIMENULL);
			}

			if(olField == "GTD2")
			{
				opFieldList +=  CString(",GD2E");
				opDataList +=  CString(",") + CTimeToDBString(prpFlight->Tifd, TIMENULL);

				CTimeSpan olDura;
				GetGatDefAllocDur(omInputData[i], olDura);
				
				opFieldList +=  CString(",GD2B");
				opDataList +=  CString(",") + CTimeToDBString(prpFlight->Tifd - olDura, TIMENULL);
			}

			if(olField == "WRO1")
			{
				opFieldList +=  CString(",W1ES");
				opDataList +=  CString(",") + CTimeToDBString(prpFlight->Tifd, TIMENULL);

				CTimeSpan olDura;
				GetWroDefAllocDur(omInputData[i], olDura);
				
				opFieldList +=  CString(",W1BS");
				opDataList +=  CString(",") + CTimeToDBString(prpFlight->Tifd - olDura, TIMENULL);
			}

			if(olField == "BLT1")
			{
				opFieldList +=  CString(",B1BS");
				opDataList +=  CString(",") + CTimeToDBString(prpFlight->Tifa, TIMENULL);

				CTimeSpan olDura;
				GetBltDefAllocDur(omInputData[i], olDura);
				
				opFieldList +=  CString(",B1ES");
				opDataList +=  CString(",") + CTimeToDBString(prpFlight->Tifa + olDura, TIMENULL);
			}


			if(olField == "BLT2")
			{
				opFieldList +=  CString(",B2BS");
				opDataList +=  CString(",") + CTimeToDBString(prpFlight->Tifa, TIMENULL);

				CTimeSpan olDura;
				GetBltDefAllocDur(omInputData[i], olDura);
				
				opFieldList +=  CString(",B2ES");
				opDataList +=  CString(",") + CTimeToDBString(prpFlight->Tifa + olDura, TIMENULL);
			}



		}
	}


	// weitere flugnummern
	CString olData;
	CString olBlanks = CString("         ");
	CString olFlno;
	CCSPtrArray<JFNODATA> olJfno;

	if(bmInputJfno)
	{
		ogSeasonCollectFlights.GetJfnoArray(&olJfno, prpFlight);

		ilCount = omInputJfno.GetSize();	

		for( i = 0; i < ilCount; i++)
		{
			if(omInputJfno[i].InUse)
			{
				olFlno = ogSeasonCollectFlights.CreateFlno(omInputJfno[i].Alc3, omInputJfno[i].Fltn, omInputJfno[i].Flns);
			}
			else
			{
				if(i < olJfno.GetSize())
				{
					olFlno = ogSeasonCollectFlights.CreateFlno(olJfno[i].Alc3, olJfno[i].Fltn, olJfno[i].Flns);
				}
				else
				{
					olFlno = olBlanks;
				}
			}
			if(olFlno.IsEmpty()) 
			{
				olFlno = olBlanks;
			}
			olData += olFlno;
		}
		olData.TrimRight();
		opDataList +=  CString(",") + olData;
		opFieldList +=  CString(",") + CString("JFNO");

		int ilAnz = olData.GetLength() / 9;

		if((olData.GetLength() % 9) != 0)
			ilAnz++;

		opFieldList +=  CString(",") + CString("JCNT");
		itoa(ilAnz, buffer, 10);
		opDataList +=  CString(",") + CString(buffer);;
	}
	olJfno.DeleteAll();


	////////////////////////////


	////////////////////////////

	// Vialist	

	
	CString olViaList;


	
	if(bmInputVia)
	{
		olViaList = pomViaTableCtrl->GetViaList(CString(prpFlight->Vial), olStoLocal);

		opDataList +=  CString(",") + olViaList;
		opFieldList +=  CString(",") + CString("VIAL");

	}

	/*
	CString olApc3;
	CString olApc4;
	CString olFids;
	CTime olStoa;
	CTime olStod;
	CCSPtrArray<VIADATA> olVia;
	INPUTVIADATA *prlInVia;
	VIADATA *prlVia;

	olData = "";

	if(bmInputVia)
	{
		ogSeasonCollectFlights.GetViaArray(&olVia, prpFlight);

		ilCount = omInputVia.GetSize();	

		if(omUFRAdid == "D")
		{
			for( i = 0; i < ilCount; i++)
			{
				if(olVia.GetSize() <= i)
				{
					olVia.Add(new VIADATA);
				}
				prlInVia = &omInputVia[i];

				if(prlInVia->InUse)
				{
					prlVia = &olVia[i];
					strcpy(prlVia->Fids, prlInVia->Disp);
					strcpy(prlVia->Apc3, prlInVia->Apc3);
					strcpy(prlVia->Apc4, prlInVia->Apc4);
					prlVia->Stoa = HourStringToDate(prlInVia->Sta, olStoLocal);
					if(bgSeasonLocal) ogBasicData.LocalToUtc(prlVia->Stoa);
					prlVia->Stod = HourStringToDate(prlInVia->Std, olStoLocal);
					if(bgSeasonLocal) ogBasicData.LocalToUtc(prlVia->Stod);
				}

			}
		}
		else
		{
			

			while(ilCount > olVia.GetSize())
			{
				olVia.InsertAt(0, new VIADATA);
			}


			//RST NEW
			for( i = 0; i < ilCount; i++)
			{
				prlInVia = &omInputVia[i];

				if(prlInVia->InUse)
				{
					prlVia = &olVia[olVia.GetSize() - i - 1];
					strcpy(prlVia->Fids, prlInVia->Disp);
					strcpy(prlVia->Apc3, prlInVia->Apc3);
					strcpy(prlVia->Apc4, prlInVia->Apc4);
					prlVia->Stoa = HourStringToDate(prlInVia->Sta, olStoLocal);
					if(bgSeasonLocal) ogBasicData.LocalToUtc(prlVia->Stoa);
					prlVia->Stod = HourStringToDate(prlInVia->Std, olStoLocal);
					if(bgSeasonLocal) ogBasicData.LocalToUtc(prlVia->Stod);
				}

			}

		}


		//if(omUFRAdid == "D")
		{
			ilCount = olVia.GetSize();	

			for( i = 0; i < ilCount; i++)
			{
				prlVia = &olVia[i];

				olFids = CString(prlVia->Fids);
				olApc3 = CString(prlVia->Apc3);
				olApc4 = CString(prlVia->Apc4);
				if(olFids.IsEmpty())
					olFids = " ";
				if(olApc3.GetLength() != 3)
					olApc3 = "   ";
				if(olApc4.GetLength() != 4)
					olApc4 = "    ";

				olData  +=	olFids  +
							olApc3  +
							olApc4  +
							CTimeToDBString(olVia[i].Stoa, TIMENULL) +  
							CTimeToDBString(olVia[i].Etoa, TIMENULL) +  
							CTimeToDBString(olVia[i].Land, TIMENULL) +  
							CTimeToDBString(olVia[i].Onbl, TIMENULL) +  
							CTimeToDBString(olVia[i].Stod, TIMENULL) + 
							CTimeToDBString(olVia[i].Etod, TIMENULL) +  
							CTimeToDBString(olVia[i].Airb, TIMENULL) +  
							CTimeToDBString(olVia[i].Ofbl, TIMENULL); 
			}

			
			olData.TrimRight();

			opDataList +=  CString(",") + olData;
			opFieldList +=  CString(",") + CString("VIAL");

			opFieldList +=  CString(",") + CString("VIAN");
			itoa(olVia.GetSize(), buffer, 10);
			opDataList +=  CString(",") + CString(buffer);;

			olVia.DeleteAll();
		}
		*/
		/*
		else
		{
			ilCount = olVia.GetSize();	

			for( i = ilCount - 1; i >= 0; i--)
			{
				prlVia = &olVia[i];

				olFids = CString(prlVia->Fids);
				olApc3 = CString(prlVia->Apc3);
				olApc4 = CString(prlVia->Apc4);
				if(olFids.IsEmpty())
					olFids = " ";
				if(olApc3.GetLength() != 3)
					olApc3 = "   ";
				if(olApc4.GetLength() != 4)
					olApc4 = "    ";

				olData  +=	olFids  +
							olApc3  +
							olApc4  +
							CTimeToDBString(olVia[i].Stoa, TIMENULL) +  
							CTimeToDBString(olVia[i].Etoa, TIMENULL) +  
							CTimeToDBString(olVia[i].Land, TIMENULL) +  
							CTimeToDBString(olVia[i].Onbl, TIMENULL) +  
							CTimeToDBString(olVia[i].Stod, TIMENULL) + 
							CTimeToDBString(olVia[i].Etod, TIMENULL) +  
							CTimeToDBString(olVia[i].Airb, TIMENULL) +  
							CTimeToDBString(olVia[i].Ofbl, TIMENULL); 
			}

			
			olData.TrimRight();

			opDataList +=  CString(",") + olData;
			opFieldList +=  CString(",") + CString("VIAL");

			opFieldList +=  CString(",") + CString("VIAN");
			itoa(olVia.GetSize(), buffer, 10);
			opDataList +=  CString(",") + CString(buffer);;

			olVia.DeleteAll();
		}
		*/

	//}
	
	
	if(bmInputCca)
	{
		CCSPtrArray<CCADATA> olCca;
		CCSPtrArray<CCADATA> olCcaNew;
		CedaCcaData olCcaData;
		CCADATA *prlCca;
		INPUTCCADATA *prlInCca;

		char pclWhere[128];
		sprintf(pclWhere,"WHERE FLNU = %d", prpFlight->Urno);
		olCcaData.ReadSpecial(olCca, pclWhere);

		for(int i = 0; i < olCca.GetSize(); i++)
		{
			olCcaNew.New(olCca[i]);
		}


		ilCount = omInputCca.GetSize();	

		for( i = 0; i < ilCount; i++)
		{

			if(olCcaNew.GetSize() <= i)
			{
				olCcaNew.Add(new CCADATA);
			}
			prlInCca = &omInputCca[i];

			if(prlInCca->InUse)
			{
				prlCca = &olCcaNew[i];

				strcpy(prlCca->Ckic, prlInCca->Ckic);
				strcpy(prlCca->Ckit, prlInCca->Ckit);
				strcpy(prlCca->Flno, prpFlight->Flno);
				prlCca->Stod = prpFlight->Stod;
				prlCca->Ckbs = HourStringToDate(prlInCca->Ckbs, olStoLocal);
				if(bgSeasonLocal) ogBasicData.LocalToUtc(prlCca->Ckbs);
				prlCca->Ckes = HourStringToDate(prlInCca->Ckes, olStoLocal);
				if(bgSeasonLocal) ogBasicData.LocalToUtc(prlCca->Ckes);
				

			}
		}


		// in Athens the flight set CKIF and CKIT automatically !!
		if (strcmp(pcgHome, "ATH") != 0)
		{
			CString olMax = "   ";
			CString olMin = "zzz";
			CString olCkic;
			ilCount = olCcaNew.GetSize();	

			for( i = 0; i < ilCount; i++)
			{
				olCkic = CString(olCcaNew[i].Ckic);
				olCkic.TrimRight();
				if((olCkic < olMin) && !olCkic.IsEmpty())
					olMin = olCkic;
				if((olCkic > olMax) && !olCkic.IsEmpty())
					olMax = olCkic;
			}

			if(olMax == "   ")
				olMax = " ";

			if(olMin == "zzz")
				olMin = " ";

			opDataList +=  CString(",") + olMax;
			opFieldList +=  CString(",") + CString("CKIT");

			opDataList +=  CString(",") + olMin;
			opFieldList +=  CString(",") + CString("CKIF");
		}

		olCcaData.UpdateSpecial(olCcaNew, olCca, prpFlight->Urno, IDM_SEASON_COL);
		olCcaNew.DeleteAll();
		olCca.DeleteAll();
	}


	////////////////////

	if(!opFieldList.IsEmpty())
		opFieldList = opFieldList.Right(opFieldList.GetLength() - 1);

	if(!opDataList.IsEmpty())
		opDataList = opDataList.Right(opDataList.GetLength() - 1);

	return true;
}




CString SeasonCollectDlg::GetInputList() 
{
	int ilItems[100];
	int ilAnz = 100;
	CString olFieldValue;
	CString olFieldText;
	CString olTmp2;
	CString olField;
	CString olAlc;
	CString olAlc2;
	CString olAlc3;
	CString olFltn;
	CString olFlns;
	CString olFlno;
	char buffer[256];	
	CString olRet;

	omInputData.RemoveAll();
	omInputField.RemoveAll();

	CListBox *polLB = pomTable->GetCTableListBox();
	if (polLB == NULL)
		return olRet;

	ilAnz = MyGetSelItems(*polLB, ilAnz, ilItems);

	for(int i = 0; i < ilAnz; i++)
	{
		pomTable->GetTextFieldValue(ilItems[i], 1, olFieldValue);
		pomTable->GetTextFieldValue(ilItems[i], 0, olFieldText);

		if(!pomTable->GetCellStatus(ilItems[i], 1))
		{
			olRet += olFieldText + CString("\n");
		}

		olField = omTableFields[ilItems[i]];
		if(olField == "ALC")
		{
			if (olFieldValue.IsEmpty())
			{
				CString olMess2;
				olMess2.Format(GetString(IDS_STRING1984), olFieldText);
				olRet += olMess2 + CString("\n");
			}

			olAlc = olFieldValue;
			if(ogBCD.GetField("ALT", "ALC2", "ALC3", olFieldValue, olAlc2, olAlc3 ))
			{
				omInputField.Add("ALC3");
				omInputData.Add(olAlc3);
				omInputField.Add("ALC2");
				omInputData.Add(olAlc2);
			}
		}
		else
		{
			omInputField.Add(olField);
			omInputData.Add(olFieldValue);
		}

		if(olField == "FLTN")
		{
			if (olFieldValue.IsEmpty())
			{
				CString olMess2;
				olMess2.Format(GetString(IDS_STRING1984), olFieldText);
				olRet += olMess2 + CString("\n");
			}

			olFltn = olFieldValue;
		}
		if(olField == "FLNS")
		{
 			olFlns = olFieldValue;
		}
		if(olField == "ORG3")
		{
			if (olFieldValue.IsEmpty())
			{
				CString olMess2;
				olMess2.Format(GetString(IDS_STRING1984), olFieldText);
				olRet += olMess2 + CString("\n");
			}

			omInputField.Add("ORG4");
			if(ogBCD.GetField("APT", "APC3", olFieldValue, "APC4", olTmp2 ))
				omInputData.Add(olTmp2);
			else
				omInputData.Add("");
		}
		if(olField == "DES3")
		{
			if (olFieldValue.IsEmpty())
			{
				CString olMess2;
				olMess2.Format(GetString(IDS_STRING1984), olFieldText);
				olRet += olMess2 + CString("\n");
			}

			omInputField.Add("DES4");
			if(ogBCD.GetField("APT", "APC3", olFieldValue, "APC4", olTmp2 ))
				omInputData.Add(olTmp2);
			else
				omInputData.Add("");
		}
		if(olField == "ACT3")
		{
			if (olFieldValue.IsEmpty())
			{
				CString olMess2;
				olMess2.Format(GetString(IDS_STRING1984), olFieldText);
				olRet += olMess2 + CString("\n");
			}

			omInputField.Add("ACT5");
			if(ogBCD.GetField("ACT", "ACT3", olFieldValue, "ACT5", olTmp2 ))
				omInputData.Add(olTmp2);
			else
				omInputData.Add("");
		}

		if(olField == "STOA")
		{
			if (olFieldValue.IsEmpty() && omAdid == "A")
			{
				CString olMess2;
				olMess2.Format(GetString(IDS_STRING1984), olFieldText);
				olRet += olMess2 + CString("\n");
			}
		}

		if(olField == "STOD")
		{
			if (olFieldValue.IsEmpty() && omAdid == "D")
			{
				CString olMess2;
				olMess2.Format(GetString(IDS_STRING1984), olFieldText);
				olRet += olMess2 + CString("\n");
			}
		}

		if(olField == "TTYP")
		{
			if (olFieldValue.IsEmpty())
			{
				CString olMess2;
				olMess2.Format(GetString(IDS_STRING1984), olFieldText);
				olRet += olMess2 + CString("\n");
			}
		}

		if(olField == "GTA1")
		{
			omInputField.Add("TGA1");
			if(ogBCD.GetField("GAT", "GNAM", olFieldValue, "TERM", olTmp2 ))
				omInputData.Add(olTmp2);
			else
				omInputData.Add("");
		}
		if(olField == "GTA2")
		{
			omInputField.Add("TGA2");
			if(ogBCD.GetField("GAT", "GNAM", olFieldValue, "TERM", olTmp2 ))
				omInputData.Add(olTmp2);
			else
				omInputData.Add("");
		}
		if(olField == "GTD2")
		{
			omInputField.Add("TGD2");
			if(ogBCD.GetField("GAT", "GNAM", olFieldValue, "TERM", olTmp2 ))
				omInputData.Add(olTmp2);
			else
				omInputData.Add("");
		}
		if(olField == "GTD1")
		{
			omInputField.Add("TGD1");
			if(ogBCD.GetField("GAT", "GNAM", olFieldValue, "TERM", olTmp2 ))
				omInputData.Add(olTmp2);
			else
				omInputData.Add("");
		}
		if(olField == "BLT1")
		{
			omInputField.Add("TMB1");
			if(ogBCD.GetField("BLT", "BNAM", olFieldValue, "TERM", olTmp2 ))
				omInputData.Add(olTmp2);
			else
				omInputData.Add("");
		}
		if(olField == "BLT2")
		{
			omInputField.Add("TMB2");
			if(ogBCD.GetField("GAT", "BNAM", olFieldValue, "TERM", olTmp2 ))
				omInputData.Add(olTmp2);
			else
				omInputData.Add("");
		}
		if(olField == "EXT1")
		{
			omInputField.Add("TET1");
			if(ogBCD.GetField("EXT", "ENAM", olFieldValue, "TERM", olTmp2 ))
				omInputData.Add(olTmp2);
			else
				omInputData.Add("");
		}
		if(olField == "WRO1")
		{
			omInputField.Add("TWR1");
			if(ogBCD.GetField("WRO", "WNAM", olFieldValue, "TERM", olTmp2 ))
				omInputData.Add(olTmp2);
			else
				omInputData.Add("");
		}
	}

	olFlno = ogSeasonCollectFlights.CreateFlno( olAlc, olFltn, olFlns);



	if(!olFlno.IsEmpty())
	{
		omInputField.Add("FLNO");
		omInputData.Add(olFlno);
	}
	else
	{
		if((!olAlc.IsEmpty()) || (!olFltn.IsEmpty()))
		{
			olRet += GetString(IDS_STRING1337);//CString("Ung�ltige Flugnummer\n"); 

		}
	}

	//FlugStatus

	if(m_CR_Betrieb.GetCheck() == TRUE)
	{
		omInputField.Add("FTYP");
		omInputData.Add("O");
	}
	if(m_CR_Planung.GetCheck() == TRUE)
	{
		omInputField.Add("FTYP");
		omInputData.Add("S");
	}
	if(m_CR_Noop.GetCheck() == TRUE)
	{
		omInputField.Add("FTYP");
		omInputData.Add("N");
	}
	if(m_CR_Prognose.GetCheck() == TRUE)
	{
		omInputField.Add("FTYP");
		omInputData.Add(" ");
	}
	
	// Weiter flugnummern

	JFNODATA *prlJfno;
	ilAnz = 100;
	
	polLB = pomJfnoTable->GetCTableListBox();
	if (polLB == NULL)
		return olRet;

	ilAnz = MyGetSelItems(*polLB, ilAnz, ilItems);

	bmInputJfno = false;
	omInputJfno.DeleteAll();

	if(ilAnz > 0)
	{
		bmInputJfno = true;
	
		int ilLines = pomJfnoTable->GetLinesCount();

		for( i = 0; i < ilLines; i++)
		{
			pomJfnoTable->GetTextFieldValue(i, 1, olAlc3);
			pomJfnoTable->GetTextFieldValue(i, 2, olFltn);
			pomJfnoTable->GetTextFieldValue(i, 3, olFlns);
			prlJfno = new JFNODATA;
			strcpy(prlJfno->Alc3, olAlc3);
			strcpy(prlJfno->Fltn, olFltn);
			strcpy(prlJfno->Flns, olFlns);
			omInputJfno.Add(prlJfno);
		}
		for( i = 0; i < ilAnz; i++)
		{
			prlJfno = &omInputJfno[ilItems[i]];
			prlJfno->InUse = true;

			bool	bl0 = pomJfnoTable->GetCellStatus(ilItems[i], 1); 
			bool	bl1 = pomJfnoTable->GetCellStatus(ilItems[i], 2); 
			bool	bl2 = pomJfnoTable->GetCellStatus(ilItems[i], 3); 
			if((!bl0) || (!bl1) || (!bl2))
			{
				sprintf(buffer, GetString(IDS_STRING348),ilItems[i] + 1);;//"Weitere Flugnummer in Zeile %d\n"
				olRet += CString(buffer) ; 
			}
		}
	}


	// Vias


	olRet += pomViaTableCtrl->GetStatus();



	if(pomViaTableCtrl->GetSelColumns() > 0)
		bmInputVia = true;
	else
		bmInputVia = false;



	/*
	ilAnz = 100;
	
	ilAnz = (pomViaTable->GetCTableListBox())->GetSelItems(ilAnz, ilItems);

	bmInputVia = false;
	omInputVia.DeleteAll();


	INPUTVIADATA *prlVia;

	if(ilAnz > 0)
	{
		bmInputVia = true;

		int ilLines = pomViaTable->GetLinesCount();


			for( i = 0; i < ilLines; i++)
			{

				prlVia = new INPUTVIADATA;
				omInputVia.Add(prlVia);

				prlVia->InUse = false;

				pomViaTable->GetTextFieldValue(i, 1, olFieldValue);
			
				prlVia->Apc3 = olFieldValue;

				if(olFieldValue.IsEmpty())
				{
					prlVia->Apc4 = CString("");
				}
				else
				{
					if(ogBCD.GetField("APT", "APC3", olFieldValue, "APC4", olTmp2 ))
						prlVia->Apc4 = olTmp2;
					else
						prlVia->Apc4 = CString("");
				}

				pomViaTable->GetTextFieldValue(i, 2, olFieldValue);
				prlVia->Sta = olFieldValue;

				pomViaTable->GetTextFieldValue(i, 3, olFieldValue);
				prlVia->Std = olFieldValue;

				pomViaTable->GetTextFieldValue(i, 4, olFieldValue);
				prlVia->Disp = olFieldValue;
			}




		for( i = 0; i < ilAnz; i++)
		{
			prlVia = &omInputVia[ilItems[i]];
			prlVia->InUse = true;

			if(!pomViaTable->GetCellStatus(ilItems[i], 1))
			{
				sprintf(buffer, GetString(IDS_STRING355),ilItems[i] + 1); //"Via-Flughafencode in Zeile %d\n"
				olRet += CString(buffer) ; 
			}
			if(!pomViaTable->GetCellStatus(ilItems[i], 2))
			{
				sprintf(buffer, GetString(IDS_STRING356),ilItems[i] + 1);//"Via- Planm��ige Ankunft in Zeile %d\n"
				olRet += CString(buffer) ; 
			}
			if(!pomViaTable->GetCellStatus(ilItems[i], 3))
			{
				sprintf(buffer, GetString(IDS_STRING357),ilItems[i] + 1);//"Via- Planm��iger Abflug in Zeile %d\n"
				olRet += CString(buffer) ; 
			}
		}

		for( i = omInputVia.GetSize() - 1; i >= 0; i--)
		{
			prlVia = &omInputVia[i];
			if(prlVia->InUse)
				break;
			else	
				omInputVia.DeleteAt(i);
		}
	}

	*/

	// Checkin schalter

	if(omUFRAdid == "D")
	{
	
		polLB = pomCinsTable->GetCTableListBox();
		if (polLB == NULL)
			return olRet;

		ilAnz = 100;
		ilAnz = MyGetSelItems(*polLB, ilAnz, ilItems);

		bmInputCca = false;
		omInputCca.DeleteAll();

		INPUTCCADATA *prlCca;

		if(ilAnz > 0)
		{
			bmInputCca = true;

			int ilLines = pomCinsTable->GetLinesCount();

			for( i = 0; i < ilLines; i++)
			{

				prlCca = new INPUTCCADATA;
				omInputCca.Add(prlCca);

				prlCca->InUse = false;

				pomCinsTable->GetTextFieldValue(i, 1, prlCca->Ckic);
				pomCinsTable->GetTextFieldValue(i, 2, prlCca->Ckit);
				pomCinsTable->GetTextFieldValue(i, 3, prlCca->Ckbs);
				pomCinsTable->GetTextFieldValue(i, 4, prlCca->Ckes);
			}

			for( i = 0; i < ilAnz; i++)
			{
				prlCca = &omInputCca[ilItems[i]];
				prlCca->InUse = true;

				if(!pomCinsTable->GetCellStatus(ilItems[i], 0))
				{
					sprintf(buffer, GetString(IDS_STRING392),ilItems[i] + 1); //"Bezeichnung Check-In Schalter in Zeile %d\n"
					olRet += CString(buffer) ; 
				}
				if(!pomCinsTable->GetCellStatus(ilItems[i], 2))
				{
					sprintf(buffer, GetString(IDS_STRING393),ilItems[i] + 1); //Check-In Schalter �ffnungszeit von in Zeile %d\n"
					olRet += CString(buffer) ; 
				}
				if(!pomCinsTable->GetCellStatus(ilItems[i], 3))
				{
					sprintf(buffer, GetString(IDS_STRING394),ilItems[i] + 1);//"Check-In Schalter �ffnungszeit bis in Zeile %d\n"
					olRet += CString(buffer) ; 
				}




			}

			for( i = omInputCca.GetSize() - 1; i >= 0; i--)
			{
				prlCca = &omInputCca[i];
				if(prlCca->InUse)
					break;
				else	
					omInputCca.DeleteAt(i);
			}
		}
	}
	return olRet;
}




void SeasonCollectDlg::OnBetrieb() 
{
	if(m_CR_Betrieb.GetCheck())
		m_CR_Betrieb.SetCheck(FALSE);
	else
		m_CR_Betrieb.SetCheck(TRUE);
		
	m_CR_Planung.SetCheck(FALSE);
	m_CR_Prognose.SetCheck(FALSE);
	m_CR_Noop.SetCheck(FALSE);

	bmChanged = true; 
	m_CB_OK.SetColors(RGB(255,0,0), ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
}

void SeasonCollectDlg::OnNoop() 
{
	if(m_CR_Noop.GetCheck())
		m_CR_Noop.SetCheck(FALSE);
	else
		m_CR_Noop.SetCheck(TRUE);

	m_CR_Betrieb.SetCheck(FALSE);
	m_CR_Planung.SetCheck(FALSE);
	m_CR_Prognose.SetCheck(FALSE);

	bmChanged = true; 
	m_CB_OK.SetColors(RGB(255,0,0), ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
}

void SeasonCollectDlg::OnPrognose() 
{
	if(m_CR_Prognose.GetCheck())
		m_CR_Prognose.SetCheck(FALSE);
	else
		m_CR_Prognose.SetCheck(TRUE);

	m_CR_Betrieb.SetCheck(FALSE);
	m_CR_Planung.SetCheck(FALSE);
	m_CR_Noop.SetCheck(FALSE);

	bmChanged = true; 
	m_CB_OK.SetColors(RGB(255,0,0), ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
}

void SeasonCollectDlg::OnPlanung() 
{
	if(m_CR_Planung.GetCheck())
		m_CR_Planung.SetCheck(FALSE);
	else
		m_CR_Planung.SetCheck(TRUE);

	m_CR_Betrieb.SetCheck(FALSE);
	m_CR_Prognose.SetCheck(FALSE);
	m_CR_Noop.SetCheck(FALSE);

	bmChanged = true; 
	m_CB_OK.SetColors(RGB(255,0,0), ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
}



void SeasonCollectDlg::OnCellLButtonDblclk( UINT wParam, LPARAM lParam) 
{

	CCSCELLNOTIFY *prlNotify = (CCSCELLNOTIFY*)lParam;


	CCSCell *prlCell;
	CString olKey;
	CCSPtrArray<COLLECTFLIGHTDATA> *polDayFlights; 
	COLLECTFLIGHTDATA *prlFlight;

	prlCell = (CCSCell *)prlNotify->SourceControl;

	olKey = prlCell->GetName() + omField.Left(1);
	
	if(olKey.GetLength() > 1)
	{
		if((polDayFlights = ogSeasonCollectFlights.GetFlightsByDayKey(olKey)) != NULL)
		{
			prlFlight  = &(*polDayFlights)[0];
			pogSeasonDlg->NewData(this, prlFlight->Rkey, prlFlight->Urno, DLG_CHANGE, bgSeasonLocal);


			/*			
			for(int j = (*polDayFlights).GetSize() - 1; j >= 0; j--)
			{
				prlFlight  = &(*polDayFlights)[j];
				olFlights.Add(prlFlight);
			}
			*/
		}
	}

}


void SeasonCollectDlg::SeasonDlgGetNext(NEW_SEASONDLG_DATA *popData)
{
	if(popData->Parent != this)
		return;

	CCSPtrArray<COLLECTFLIGHTDATA> *polDayFlights; 

	COLLECTFLIGHTDATA *prlFlight = ogSeasonCollectFlights.GetFlightByUrno(popData->CurrUrno);

	CCSCell *prlCell;
	CString olKey;

	if(prlFlight != NULL)
	{
		if(omField.Left(1) == "A")
			olKey = prlFlight->Stoa.Format("%d%m%Y");
		else
			olKey = prlFlight->Stod.Format("%d%m%Y");
		
		
		if(omDayMap.Lookup(olKey, (void*&) prlCell) == TRUE)
		{

			int ilCount = omCells.GetSize();

			for(int i = 0; i < ilCount; i++)
			{
				if(prlCell == &omCells[i])
				{
					break;
				}
			}

			for(i++; i < ilCount; i++)
			{
				prlCell = &omCells[i];
				olKey = prlCell->GetName() + omField.Left(1);
				if(olKey.GetLength() > 1)
				{
					if((polDayFlights = ogSeasonCollectFlights.GetFlightsByDayKey(olKey)) != NULL)
					{
						prlFlight  = &(*polDayFlights)[0];
						pogSeasonDlg->NewData(this, prlFlight->Rkey, prlFlight->Urno, DLG_CHANGE, bgSeasonLocal);
						break;
					}
				}
			}
		}
	}
}


void SeasonCollectDlg::SeasonDlgGetPrev(NEW_SEASONDLG_DATA *popData)
{
	if(popData->Parent != this)
		return;

	CCSPtrArray<COLLECTFLIGHTDATA> *polDayFlights; 

	COLLECTFLIGHTDATA *prlFlight = ogSeasonCollectFlights.GetFlightByUrno(popData->CurrUrno);

	CCSCell *prlCell;
	CString olKey;

	if(prlFlight != NULL)
	{
		if(omField.Left(1) == "A")
			olKey = prlFlight->Stoa.Format("%d%m%Y");
		else
			olKey = prlFlight->Stod.Format("%d%m%Y");
		
		
		if(omDayMap.Lookup(olKey, (void*&) prlCell) == TRUE)
		{
			for(int i = omCells.GetSize() - 1; i >= 0; i--)
			{
				if(prlCell == &omCells[i])
				{
					break;
				}
			}

			for(i--; i >= 0; i--)
			{
				prlCell = &omCells[i];
				olKey = prlCell->GetName() + omField.Left(1);
				if(olKey.GetLength() > 1)
				{
					if((polDayFlights = ogSeasonCollectFlights.GetFlightsByDayKey(olKey)) != NULL)
					{
						prlFlight  = &(*polDayFlights)[0];
						pogSeasonDlg->NewData(this, prlFlight->Rkey, prlFlight->Urno, DLG_CHANGE, bgSeasonLocal);
						break;
					}
				}
			}
		}
	}
}




LONG SeasonCollectDlg::OnTableIPEditKillfocus( UINT wParam, LPARAM lParam)
{
	CCSIPEDITNOTIFY *prlNotify = (CCSIPEDITNOTIFY*)lParam;

	CString olValue = prlNotify->Text;
	CString olField;
	bool blRet = true;
	CString olTmp;
	CString olTmp2;

	if(prlNotify->SourceTable == pomTable)
	{
		olField = omTableFields[prlNotify->Line];

		if(olField == "ALC")
		{
			blRet = ogBCD.GetField("ALT", "ALC2", "ALC3",olValue, olTmp , olTmp2 );
		}else

		if(olField == "ACT3")
		{
			blRet = ogBCD.GetField("ACT", "ACT3", olValue, "ACT5", olTmp );
		}else

		if(olField == "TTYP")
		{
			blRet = ogBCD.GetField("NAT", "TTYP", olValue, "TTYP", olTmp );
		}else

		if(olField == "HTYP")
		{
			blRet = ogBCD.GetField("HTY", "HTYP", olValue, "HTYP", olTmp );
		}else

		if(olField == "STYP")
		{
			blRet = ogBCD.GetField("STY", "STYP", olValue, "STYP",olTmp );
		}else

		if((olField == "ORG3") || (olField == "DES3"))
		{
			blRet = ogBCD.GetField("APT", "APC3", olValue, "APC4", olTmp );
		}else

		if((olField == "PSTA") || (olField == "PSTD"))
		{
			blRet = ogBCD.GetField("PST", "PNAM", olValue, "PNAM", olTmp );
		}else

		if((olField == "GTA1") || (olField == "GTA2") || (olField == "GTD2") || (olField == "GTD1"))
		{
			blRet = ogBCD.GetField("GAT", "GNAM", olValue, "TERM", olTmp );
		}else

		if((olField == "BLT1") || (olField == "BLT2"))
		{
			blRet = ogBCD.GetField("BLT", "BNAM", olValue, "TERM", olTmp );
		}else

		if((olField == "EXT1") || (olField == "EXT2"))
		{
			blRet = ogBCD.GetField("EXT", "ENAM", olValue, "TERM", olTmp );
		}else

		if(olField == "WRO1")
		{
			blRet = ogBCD.GetField("WRO", "WNAM", olValue, "TERM", olTmp );
		}


	}else
/*
	if(prlNotify->SourceTable == pomViaTable)
	{
		if(prlNotify->Column == 1)
		{
			blRet = ogBCD.GetField("APT", "APC3", olValue, "APC4", olTmp);
		}

	}else
*/
	if(prlNotify->SourceTable == pomJfnoTable)
	{
		if(prlNotify->Column == 1)
		{
			blRet = ogBCD.GetField("ALT", "ALC3", "ALC2", olValue, olTmp , olTmp2 );
		}

	}else

	if(prlNotify->SourceTable == pomCinsTable)
	{
		if(prlNotify->Column == 1)
		{
			if(blRet = ogBCD.GetField("CIC", "CNAM", olValue, "TERM", olTmp ))
			{
				//pomDCinsTable->SetIPValue(prlNotify->Line, 0, prlNotify->Text);
				pomCinsTable->SetIPValue(prlNotify->Line, 2, olTmp);
			}
			else
			{
				pomCinsTable->SetIPValue(prlNotify->Line, 2, "");
			}
		}
	}

	if(olValue.IsEmpty())
		blRet = true;

	if(blRet == false)
		prlNotify->UserStatus = true;

	prlNotify->Status = blRet;

	return 0L;
}



void SeasonCollectDlg::OnSplit() 
{

	CCSPtrArray<COLLECTFLIGHTDATA> olFlights;
	COLLECTFLIGHTDATA *prlFlight;
//	COLLECTFLIGHTDATA *prlFlight2;
	CCSCell *prlCell;
	CString olKey;
	CString olUrnoList;

	COLORREF olOldSelColor;

	CString olRkeys;

	GetSelectFlights(olFlights, olRkeys, true);

	int ilCount = olFlights.GetSize();

	if(ilCount <= 0)
	{
		CFPMSApp::MyTopmostMessageBox(this,GetString(IDS_STRING966), GetString(IDS_STRING908), MB_ICONQUESTION | MB_OK);
//		::MessageBox(this->m_hWnd, GetString(IDS_STRING966), GetString(IDS_STRING908), MB_APPLMODAL | MB_SETFOREGROUND);
		return;
	}

	ogFlightUtilsData.ClearAll();

	CString olMessage;
	olMessage.Format(GetString(IDS_STRING1392), ilCount); 
		

	if( CFPMSApp::MyTopmostMessageBox(this, olMessage, GetString(ST_FRAGE),  MB_ICONQUESTION | MB_YESNO) == IDYES)
//	if( ::MessageBox(this->m_hWnd, olMessage, GetString(ST_FRAGE), MB_YESNO | MB_APPLMODAL | MB_SETFOREGROUND) == IDYES)
	{
		SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
		ogBcHandle.BufferBc();
//		ogBcHandle.GetBc();

		ogFlightUtilsData.ReadAllSelectedRotations(olRkeys);

		for(int i = 0; i < ilCount; i++)
		{
			prlFlight = &olFlights[i];

			CTime olTime;
			if(strcmp(prlFlight->Adid, "A") == 0)
			{
				olTime = prlFlight->Stoa;
			}
			else
			{
				olTime = prlFlight->Stod;
			}
			if(bgSeasonLocal) ogBasicData.UtcToLocal(olTime);
			olKey = olTime.Format("%d%m%Y");

			if(omDayMap.Lookup(olKey,  (void*&)prlCell) == TRUE)
				olOldSelColor = prlCell->SetSelBKColor(RGB(0,255,0));

			ogSeasonCollectFlights.SplitFlight(prlFlight, omUFRAdid[0], true);	

/*			MSG olMsg;
			while (::PeekMessage(&olMsg,NULL,0,0,PM_NOREMOVE))
			{
				::GetMessage(&olMsg,NULL,0,0);
				::TranslateMessage(&olMsg);
				::DispatchMessage(&olMsg);
			}
*/
			ogBcHandle.BufferBc();
//			ogBcHandle.GetBc();

			if(omDayMap.Lookup(olKey,  (void*&)prlCell) == TRUE)
				prlCell->SetSelBKColor(olOldSelColor);

		}

		ogBcHandle.ReleaseBuffer();
		m_CB_Delete.SetColors(::GetSysColor(COLOR_BTNFACE), ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
		m_CB_OK.SetColors(::GetSysColor(COLOR_BTNFACE), ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
		SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
	}

	olFlights.RemoveAll();
	ogFlightUtilsData.ClearAll();

}


void SeasonCollectDlg::OnJoin() 
{
	int ilOffSet = m_CB_OffsetSpin.GetPos();
	CTimeSpan olSpan(1,0,0,0);

	CCSPtrArray<COLLECTFLIGHTDATA> olFlights;
	COLLECTFLIGHTDATA *prlFlight;
	CCSCell *prlCell;
	CString olKey;
	CString olUrnoList;
	char olAdid;

	long llUrno = 0L;
	long llRkey = 0L;

	COLORREF olOldSelColor;

	CString olTmp;

	m_CE_Alc.GetWindowText(m_Alc);
	m_CE_Fltn.GetWindowText(m_Fltn);
	m_CE_Flns.GetWindowText(m_Flns);

	if(!ogBCD.GetField("ALT", "ALC2", "ALC3", m_Alc, olTmp, olTmp))
	{
		CFPMSApp::MyTopmostMessageBox(this,GetString(IDS_STRING1042), GetString(IDS_STRING908), MB_ICONQUESTION | MB_OK);
//		::MessageBox(this->m_hWnd, GetString(IDS_STRING1042), GetString(IDS_STRING908), MB_APPLMODAL | MB_SETFOREGROUND);
		return;
	}

	CString olFlno = ogSeasonCollectFlights.CreateFlno(m_Alc, m_Fltn, m_Flns);

	if(olFlno.IsEmpty())
	{
		CFPMSApp::MyTopmostMessageBox(this,GetString(IDS_STRING1337), GetString(IDS_STRING908), MB_ICONQUESTION | MB_OK);
//		::MessageBox(this->m_hWnd, GetString(IDS_STRING1337), GetString(IDS_STRING908), MB_APPLMODAL | MB_SETFOREGROUND);
		return;
	}

	CString olRkeys;
	GetSelectFlights(olFlights, olRkeys, false);

	int ilCount = olFlights.GetSize();

	if(ilCount <= 0)
	{
		CFPMSApp::MyTopmostMessageBox(this,GetString(IDS_STRING966), GetString(IDS_STRING908), MB_ICONQUESTION | MB_OK);
//		::MessageBox(this->m_hWnd, GetString(IDS_STRING966), GetString(IDS_STRING908), MB_APPLMODAL | MB_SETFOREGROUND);
		return;
	}

	CString olMessage;
	olMessage.Format(GetString(IDS_STRING1393), ilCount); 
	char clChangeActRegn = 'D';				
	ogFlightUtilsData.ClearAll();

	if( CFPMSApp::MyTopmostMessageBox(this, olMessage, GetString(ST_FRAGE), MB_YESNO | MB_ICONQUESTION) == IDYES)
//	if( ::MessageBox(this->m_hWnd, olMessage, GetString(ST_FRAGE), MB_YESNO | MB_APPLMODAL | MB_SETFOREGROUND) == IDYES)
	{

		//precheck for identical aircrafttypes
		SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
//		ogSeasonCollectFlights.omJoinData.RemoveAll();

//###
		//calc maximum timeframe
		prlFlight = &olFlights[0];
		COLLECTFLIGHTDATA *prlFlightLast = &olFlights[ilCount-1];
		ASSERT(prlFlight && prlFlightLast);
		CTime olTimeFrom;
		CTime olTimeTo;

		if(strcmp(prlFlight->Adid, "A") == 0)
		{
			olAdid = 'D';
			olTimeFrom = prlFlight->Tifa;
			olTimeTo = prlFlightLast->Tifa;
		}
		else
		{
			olAdid = 'A';
			olTimeFrom = prlFlight->Tifd;
			olTimeTo = prlFlightLast->Tifd;
		}

//		CString olKeyF1 = olTimeFrom.Format("%d.%m.%Y,%H:%M");
//		CString olKeyT1 = olTimeTo.Format("%d.%m.%Y,%H:%M");

		CTimeSpan olJoinSpan(0,0,0,0);

		for(int j = 0; j < ilOffSet; j++)
		{
			if(omUFRAdid == "A")
			{
				olTimeTo += olSpan;
				olJoinSpan += olSpan;
			}
			else
			{
				olTimeFrom -= olSpan;
				olJoinSpan -= olSpan;
			}
		}

		if(omUFRAdid == "A")
			olTimeTo = CTime (olTimeTo.GetYear(),olTimeTo.GetMonth(),olTimeTo.GetDay(),23,59,59);
		else
			olTimeFrom = CTime (olTimeFrom.GetYear(),olTimeFrom.GetMonth(),olTimeFrom.GetDay(),0,0,0);

//		CString olKeyF = olTimeFrom.Format("%d.%m.%Y,%H:%M");
//		CString olKeyT = olTimeTo.Format("%d.%m.%Y,%H:%M");

		//get possible flights to join
		CCSPtrArray<COLLECTFLIGHTDATA> olJoinData;
		if(!ogSeasonCollectFlights.SearchFlightsToJoin(olFlno, olTimeFrom, olTimeTo, olAdid, olJoinData))
			return;

		//extract joindata for the selected flights, SearchFlightsToJoin sort the array by tifa(down) or tifd(up)
		CCSPtrArray<COLLECTFLIGHTDATA> olJoinDataExt;
		for (int k = 0; k < ilCount; k++)
		{
			prlFlight = &olFlights[k];
			//timespan for joining flight with actual flight
			if(strcmp(prlFlight->Adid, "A") == 0)
			{
				olTimeFrom = prlFlight->Tifa;
				if (ilOffSet == 0)
					olTimeTo = CTime (olTimeFrom.GetYear(),olTimeFrom.GetMonth(),olTimeFrom.GetDay(),23,59,59);
				else
				{
					//wenn das zeitfenster gezogen werden soll
					//olTimeTo = olTimeFrom + olJoinSpan;

					//wenn das zeitfenster versetzt werden soll
					olTimeTo = olTimeFrom + olJoinSpan;
					olTimeTo = CTime (olTimeTo.GetYear(),olTimeTo.GetMonth(),olTimeTo.GetDay(),23,59,59);
					olTimeFrom = CTime (olTimeTo.GetYear(),olTimeTo.GetMonth(),olTimeTo.GetDay(),0,0,0);
				}
			}
			else
			{
				olTimeTo = prlFlight->Tifd;
				if (ilOffSet == 0)
					olTimeFrom = CTime (olTimeTo.GetYear(),olTimeTo.GetMonth(),olTimeTo.GetDay(),0,0,0);
				else
				{
					//wenn das zeitfenster gezogen werden soll
					//olTimeFrom = olTimeTo + olJoinSpan;

					//wenn das zeitfenster versetzt werden soll
					olTimeFrom = olTimeTo + olJoinSpan;
					olTimeTo = CTime (olTimeFrom.GetYear(),olTimeFrom.GetMonth(),olTimeFrom.GetDay(),23,59,59);
					olTimeFrom = CTime (olTimeFrom.GetYear(),olTimeFrom.GetMonth(),olTimeFrom.GetDay(),0,0,0);
				}
			}

			olJoinDataExt.Add(NULL);
			for (int l = 0; l < olJoinData.GetSize(); l++)
			{
				//get all possible data
				COLLECTFLIGHTDATA *prlFlightJoin = &olJoinData[l];
				//for arrival find the nearest departure (tifd)
				if(omUFRAdid == "A")
				{
					if ( (prlFlightJoin->Tifd >= olTimeFrom) && (prlFlightJoin->Tifd <= olTimeTo) && (strcmp(prlFlightJoin->Jfno, "J") != 0) )
					{
//						olJoinDataExt.Add(prlFlightJoin);
						olJoinDataExt.SetAt(k,prlFlightJoin);
						//mark as joined
						strcpy (prlFlightJoin->Jfno, "J");
						break;
					}
					if (prlFlightJoin->Tifd > olTimeTo)
					{
//						olJoinDataExt.Add(NULL);
						break;
					}
				}
				//for departure find the nearest arrival (tifa) 
				else
				{
					//CString olKeyF = olTimeFrom.Format("%d.%m.%Y,%H:%M");
					//CString olKeyT = olTimeTo.Format("%d.%m.%Y,%H:%M");
					//CString olKeyTa = prlFlightJoin->Tifa.Format("%d.%m.%Y,%H:%M");

					if ( (prlFlightJoin->Tifa >= olTimeFrom) && (prlFlightJoin->Tifa <= olTimeTo) && (strcmp(prlFlightJoin->Jfno, "J") != 0) )
					{
//						olJoinDataExt.Add(prlFlightJoin);
						olJoinDataExt.SetAt(k,prlFlightJoin);
						//mark as joined
						strcpy (prlFlightJoin->Jfno, "J");
						break;
					}
					if (prlFlightJoin->Tifa < olTimeFrom)
					{
//						olJoinDataExt.Add(NULL);
						break;
					}
				}

			}
		}

		int ilCountJoin = olJoinDataExt.GetSize();
		ASSERT(ilCountJoin == ilCount);

		//check if act differs...
		bool blAskAct = false;
		COLLECTFLIGHTDATA *prl = NULL;
		for (k = 0; k < ilCount; k++)
		{
			prl = &olJoinDataExt.ElementAt(k);
			prlFlight = &olFlights[k];
			if (!blAskAct)
			{
				if (prl && prlFlight)
				{
					if( (strcmp(prl->Act3, prlFlight->Act3) != 0) || (strcmp(prl->Act5, prlFlight->Act5) != 0) )
						blAskAct = true;
				}
			}

			//list rkeys
			if (prl)
			{
				char buffer[64];
				itoa(prl->Rkey, buffer,10);
				olRkeys += CString(",") + buffer;
			}
		}

		//.. if differs set part for all rotations by user
		if(blAskAct)
		{
			AskBox olDlg(this, GetString(IDS_SET_ARR), GetString(IDS_SET_DEP), GetString(IDS_ASK_ACT), GetString(ST_FRAGE));
			int ilRet = olDlg.DoModal();

			if (ilRet == 0)
				return;

			if(ilRet == 1)
			{
				clChangeActRegn = 'A';
			}
			if(ilRet == 2)
			{
				clChangeActRegn = 'D';				
			}
		}

		ogFlightUtilsData.ReadAllSelectedRotations(olRkeys);

		SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
		ogBcHandle.BufferBc();

		for(int i = 0; i < ilCount; i++)
		{
			prlFlight = &olFlights[i];
			CTime olTime;

			if(strcmp(prlFlight->Adid, "A") == 0)
				olTime = prlFlight->Tifa;
			else
				olTime = prlFlight->Tifd;

			if(bgSeasonLocal) ogBasicData.UtcToLocal(olTime);
			olKey = olTime.Format("%d%m%Y");
			if(bgSeasonLocal) ogBasicData.LocalToUtc(olTime);
			
			if(omDayMap.Lookup(olKey,  (void*&)prlCell) == TRUE)
				olOldSelColor = prlCell->SetSelBKColor(RGB(0,255,0));

			prl = &olJoinDataExt.ElementAt(i);

			if (prl)
			{
				llUrno = prl->Urno;
				llRkey = prl->Rkey;

				if(omUFRAdid == "A")
				{
					ogFlightUtilsData.JoinFlight(prlFlight->Urno, prlFlight->Rkey, llUrno, llRkey, true, clChangeActRegn, true);	
				}
				else
				{
					ogFlightUtilsData.JoinFlight(llUrno, llRkey, prlFlight->Urno, prlFlight->Rkey, true, clChangeActRegn, true);	
				}
				ogBcHandle.BufferBc();

			}

			if(omDayMap.Lookup(olKey,  (void*&)prlCell) == TRUE)
				prlCell->SetSelBKColor(olOldSelColor);

		}

		ogBcHandle.ReleaseBuffer();
	
		m_CB_Delete.SetColors(::GetSysColor(COLOR_BTNFACE), ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
		m_CB_OK.SetColors(::GetSysColor(COLOR_BTNFACE), ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
		SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));

		olJoinDataExt.RemoveAll();
		olJoinData.RemoveAll();
		ogFlightUtilsData.ClearAll();
	}

	olFlights.RemoveAll();
	return;
//######

/*
		for(int i = 0; i < ilCount; i++)
		{
			prlFlight = &olFlights[i];

			CTime olTime;
			CTime olTimeFrom;
			CTime olTimeTo;
			if(strcmp(prlFlight->Adid, "A") == 0)
			{
//				olTime = prlFlight->Stoa;
				olTime = prlFlight->Tifa;
				olAdid = 'D';
			}
			else
			{
				olAdid = 'A';
//				olTime = prlFlight->Stoa;
				olTime = prlFlight->Tifd;
			}


			if(bgSeasonLocal) ogBasicData.UtcToLocal(olTime);
			olKey = olTime.Format("%d.%m.%Y");
			if(bgSeasonLocal) ogBasicData.LocalToUtc(olTime);

			CTime olTime1 = olTime;
			for(int j = 0; j < ilOffSet; j++)
			{
				if(omUFRAdid == "A")
					olTime1 += olSpan;
				else
					olTime1 -= olSpan;
			}

			if(strcmp(prlFlight->Adid, "A") == 0)
			{
				olTimeFrom = olTime;
				if (ilOffSet == 0)
					olTimeTo = CTime (olTimeFrom.GetYear(),olTimeFrom.GetMonth(),olTimeFrom.GetDay(),23,59,59);
				else
					olTimeTo = olTime1;
			}
			else
			{
				olTimeTo = olTime;
				if (ilOffSet == 0)
					olTimeFrom = CTime (olTimeTo.GetYear(),olTimeTo.GetMonth(),olTimeTo.GetDay(),0,0,0);
				else
					olTimeFrom = olTime1;
			}


			CString olAct3 = prlFlight->Act3;
			CString olAct5 = prlFlight->Act5;

			// by the first different ask for act
//			if(!ogSeasonCollectFlights.SearchFlight(olAct3, olAct5, olFlno, olTimeFrom, olTimeTo, olAdid, llUrno, llRkey) && llUrno > 0L)
			if(!ogSeasonCollectFlights.SearchFlight("", "", olFlno, olTimeFrom, olTimeTo, olAdid, llUrno, llRkey) && llUrno > 0L)
			{
				AskBox olDlg(NULL, GetString(IDS_WARNING), GetString(IDS_ASK_ACT), GetString(IDS_SET_ARR), GetString(IDS_SET_DEP));
//				AskBox olDlg(this, GetString(IDS_SET_ARR), GetString(IDS_SET_DEP), GetString(IDS_ASK_ACT), GetString(ST_FRAGE));
				int ilRet = olDlg.DoModal();

				if (ilRet == 0)
					return;

				if(ilRet == 1)
				{
					clChangeActRegn = 'A';
				}
				if(ilRet == 2)
				{
					clChangeActRegn = 'D';				
				}

				break;
			}
		}


			int l = ogSeasonCollectFlights.omJoinData.GetSize();
			bool blAsk = false;
			COLLECTFLIGHTDATA *prl = NULL;
			for (int zz = 0; zz<l; zz++)
			{
				prl = &ogSeasonCollectFlights.omJoinData.ElementAt(zz);
				prlFlight = &olFlights[zz];
				if (!blAsk)
				{
					if( (strcmp(prl->Act3, prlFlight->Act3) != 0) || (strcmp(prl->Act5, prlFlight->Act5) != 0) )
						blAsk = true;
				}

				char buffer[64];
				itoa(prl->Rkey, buffer,10);
				olRkeys += CString(",") + buffer;

			}


			ogFlightUtilsData.ReadAllSelectedRotations(olRkeys);



		SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
		ogBcHandle.BufferBc();

		m_CB_Delete.SetColors(RGB(0,255,0), ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));

		for(i = 0; i < ilCount; i++)
		{
			prlFlight = &olFlights[i];

			CTime olTime;
			CTime olTimeFrom;
			CTime olTimeTo;

			if(strcmp(prlFlight->Adid, "A") == 0)
			{
//				olTime = prlFlight->Stoa;
				olTime = prlFlight->Tifa;
				olAdid = 'D';
			}
			else
			{
				olAdid = 'A';
//				olTime = prlFlight->Stoa;
				olTime = prlFlight->Tifd;
			}
			if(bgSeasonLocal) ogBasicData.UtcToLocal(olTime);
			olKey = olTime.Format("%d%m%Y");
			if(bgSeasonLocal) ogBasicData.LocalToUtc(olTime);
			
			if(omDayMap.Lookup(olKey,  (void*&)prlCell) == TRUE)
				olOldSelColor = prlCell->SetSelBKColor(RGB(0,255,0));


			//olTime += olSpan;// * ilOffSet;

			CTime olTime1 = olTime;
			for(int j = 0; j < ilOffSet; j++)
			{
				if(omUFRAdid == "A")
					olTime1 += olSpan;
				else
					olTime1 -= olSpan;
			}


			if(strcmp(prlFlight->Adid, "A") == 0)
			{
				olTimeFrom = olTime;
				if (ilOffSet == 0)
					olTimeTo = CTime (olTimeFrom.GetYear(),olTimeFrom.GetMonth(),olTimeFrom.GetDay(),23,59,59);
				else
					olTimeTo = olTime1;
			}
			else
			{
				olTimeTo = olTime;
				if (ilOffSet == 0)
					olTimeFrom = CTime (olTimeTo.GetYear(),olTimeTo.GetMonth(),olTimeTo.GetDay(),0,0,0);
				else
					olTimeFrom = olTime1;
			}


			CString olAct3 = prlFlight->Act3;
			CString olAct5 = prlFlight->Act5;

			int l = ogSeasonCollectFlights.omJoinData.GetSize();
			COLLECTFLIGHTDATA *prl = NULL;
				prl = &ogSeasonCollectFlights.omJoinData.ElementAt(i);
				llUrno = prl->Urno;
				llRkey = prl->Rkey;
				olAct3 = prl->Act3;
				olAct5 = prl->Act5;




//			if(ogSeasonCollectFlights.SearchFlight("", "", olFlno, olTimeFrom, olTimeTo, olAdid, llUrno, llRkey))
			if (prl)
			{
				CedaFlightUtilsData olFlightUtilsData;
				if(omUFRAdid == "A")
				{
					ogFlightUtilsData.JoinFlight(prlFlight->Urno, prlFlight->Rkey, llUrno, llRkey, true, clChangeActRegn, true);	
				}
				else
				{
					ogFlightUtilsData.JoinFlight(llUrno, llRkey, prlFlight->Urno, prlFlight->Rkey, true, clChangeActRegn, true);	
				}
				ogBcHandle.BufferBc();

			}


			if(omDayMap.Lookup(olKey,  (void*&)prlCell) == TRUE)
				prlCell->SetSelBKColor(olOldSelColor);

		}

		ogBcHandle.ReleaseBuffer();
	
		m_CB_Delete.SetColors(::GetSysColor(COLOR_BTNFACE), ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
		m_CB_OK.SetColors(::GetSysColor(COLOR_BTNFACE), ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
		SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
	}

	olFlights.RemoveAll();
*/

/*
	int ilOffSet = m_CB_OffsetSpin.GetPos();


	CTimeSpan olSpan(1,0,0,0);

	CCSPtrArray<COLLECTFLIGHTDATA> olFlights;
	COLLECTFLIGHTDATA *prlFlight;
	CCSCell *prlCell;
	CString olKey;
	CString olUrnoList;
	char olAdid;

	long llUrno = 0L;
	long llRkey = 0L;

	COLORREF olOldSelColor;

	CString olTmp;

	m_CE_Alc.GetWindowText(m_Alc);
	m_CE_Fltn.GetWindowText(m_Fltn);
	m_CE_Flns.GetWindowText(m_Flns);



	if(!ogBCD.GetField("ALT", "ALC2", "ALC3", m_Alc, olTmp, olTmp))
	{
		MessageBox(GetString(IDS_STRING1042), GetString(IDS_STRING908));
		return;
	}


	CString olFlno = ogSeasonCollectFlights.CreateFlno(m_Alc, m_Fltn, m_Flns);


	if(olFlno.IsEmpty())
	{
		MessageBox(GetString(IDS_STRING1337), GetString(IDS_STRING908));
		return;
	}



	GetSelectFlights(olFlights, false);

	int ilCount = olFlights.GetSize();

	if(ilCount <= 0)
	{
		MessageBox(GetString(IDS_STRING966), GetString(IDS_STRING908));
		return;
	}




	CString olMessage;

	olMessage.Format(GetString(IDS_STRING1393), ilCount); 
	char clChangeActRegn = 'D';				
		

	if( MessageBox(olMessage, GetString(ST_FRAGE), MB_YESNO ) == IDYES)
	{

		//precheck for identical aircrafttypes
		SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
		for(int i = 0; i < ilCount; i++)
		{
			prlFlight = &olFlights[i];

			CTime olTime;
			if(strcmp(prlFlight->Adid, "A") == 0)
			{
//				olTime = prlFlight->Stoa;
				olTime = prlFlight->Tifa;
				olAdid = 'D';
			}
			else
			{
				olAdid = 'A';
//				olTime = prlFlight->Stoa;
				olTime = prlFlight->Tifd;
			}


			if(bgSeasonLocal) ogBasicData.UtcToLocal(olTime);
			olKey = olTime.Format("%d.%m.%Y");
			if(bgSeasonLocal) ogBasicData.LocalToUtc(olTime);

			for(int j = 0; j < ilOffSet; j++)
			{
				if(omUFRAdid == "A")
					olTime += olSpan;
				else
					olTime -= olSpan;
			}

			CString olAct3 = prlFlight->Act3;
			CString olAct5 = prlFlight->Act5;

			// by the first different ask for act
			if(!ogSeasonCollectFlights.SearchFlight(olAct3, olAct5, olFlno, olTime, olAdid, llUrno, llRkey))
			{
				AskBox olDlg(NULL, GetString(IDS_WARNING), GetString(IDS_ASK_ACT), GetString(IDS_SET_ARR), GetString(IDS_SET_DEP));
				int ilRet = olDlg.DoModal();

				if (ilRet == 0)
					return;

				if(ilRet == 1)
				{
					clChangeActRegn = 'A';
				}
				if(ilRet == 2)
				{
					clChangeActRegn = 'D';				
				}

				break;
			}
		}


		SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
		ogBcHandle.BufferBc();

		m_CB_Delete.SetColors(RGB(0,255,0), ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));

		for(i = 0; i < ilCount; i++)
		{
			prlFlight = &olFlights[i];

			CTime olTime;
			if(strcmp(prlFlight->Adid, "A") == 0)
			{
//				olTime = prlFlight->Stoa;
				olTime = prlFlight->Tifa;
				olAdid = 'D';
			}
			else
			{
				olAdid = 'A';
//				olTime = prlFlight->Stoa;
				olTime = prlFlight->Tifd;
			}
			if(bgSeasonLocal) ogBasicData.UtcToLocal(olTime);
			olKey = olTime.Format("%d%m%Y");
			if(bgSeasonLocal) ogBasicData.LocalToUtc(olTime);
			
			if(omDayMap.Lookup(olKey,  (void*&)prlCell) == TRUE)
				olOldSelColor = prlCell->SetSelBKColor(RGB(0,255,0));


			//olTime += olSpan;// * ilOffSet;

			for(int j = 0; j < ilOffSet; j++)
			{
				if(omUFRAdid == "A")
					olTime += olSpan;
				else
					olTime -= olSpan;
			}

			CString olAct3 = prlFlight->Act3;
			CString olAct5 = prlFlight->Act5;

			if(ogSeasonCollectFlights.SearchFlight("", "", olFlno, olTime, olAdid, llUrno, llRkey))
			{
				CedaFlightUtilsData olFlightUtilsData;
				if(omUFRAdid == "A")
				{
					olFlightUtilsData.JoinFlight(prlFlight->Urno, prlFlight->Rkey, llUrno, llRkey, true, clChangeActRegn);	
				}
				else
				{
					olFlightUtilsData.JoinFlight(llUrno, llRkey, prlFlight->Urno, prlFlight->Rkey, true, clChangeActRegn);	
				}
				ogBcHandle.BufferBc();

			}


			if(omDayMap.Lookup(olKey,  (void*&)prlCell) == TRUE)
				prlCell->SetSelBKColor(olOldSelColor);

		}

		ogBcHandle.ReleaseBuffer();
	
		m_CB_Delete.SetColors(::GetSysColor(COLOR_BTNFACE), ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
		m_CB_OK.SetColors(::GetSysColor(COLOR_BTNFACE), ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
		SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
	}

	olFlights.RemoveAll();

*/

}


void SeasonCollectDlg::SetWndPos()
{
	CRect olRect;

	GetWindowRect(&olRect);

	int ilWidth = olRect.right -olRect.left;
	int ilWhichMonitor = ogCfgData.GetMonitorForWindow(CString(MON_SEASONBATCH_STRING));
	int ilMonitors = ogCfgData.GetMonitorForWindow(CString(MON_COUNT_STRING));
	int left, top, right, bottom;
	int ilCXMonitor;

	bottom = ::GetSystemMetrics(SM_CYSCREEN);

	top = 0;  


	if(ilWhichMonitor == 1)
	{
		switch(ilMonitors)
		{
		case 1:
			ilCXMonitor = ::GetSystemMetrics(SM_CXSCREEN);
			left = (int)((ilCXMonitor-ilWidth)/2);
			right = left + ilWidth;//GetSystemMetrics(SM_CXSCREEN);
			break;
		case 2:
			ilCXMonitor = (int)(::GetSystemMetrics(SM_CXSCREEN)/2);
			left = (int)((ilCXMonitor-ilWidth)/2);
			right = left + ilWidth;//GetSystemMetrics(SM_CXSCREEN);
			break;
		case 3:
			ilCXMonitor = (int)(::GetSystemMetrics(SM_CXSCREEN)/3);
			left = (int)((ilCXMonitor-ilWidth)/2);
			right = left + ilWidth;//GetSystemMetrics(SM_CXSCREEN);
			break;
		default:
			ilCXMonitor = ::GetSystemMetrics(SM_CXSCREEN);
			left = (int)((ilCXMonitor-ilWidth)/2);
			right = left + ilWidth;//GetSystemMetrics(SM_CXSCREEN);
			break;
		}
	}
	else if(ilWhichMonitor == 2)
	{
		switch(ilMonitors)
		{
		case 1:
			ilCXMonitor = ::GetSystemMetrics(SM_CXSCREEN);
			left = (int)((ilCXMonitor-ilWidth)/2);
			right = left + ilWidth;//GetSystemMetrics(SM_CXSCREEN);
			break;
		case 2:
			ilCXMonitor = (int)(::GetSystemMetrics(SM_CXSCREEN)/2);
			left = (int)((ilCXMonitor-ilWidth)/2)+(ilCXMonitor);
			right = left + ilWidth;//GetSystemMetrics(SM_CXSCREEN);
			break;
		case 3:
			ilCXMonitor = (int)(::GetSystemMetrics(SM_CXSCREEN)/3);
			left = (int)((ilCXMonitor-ilWidth)/2)+(ilCXMonitor);
			right = left + ilWidth;//GetSystemMetrics(SM_CXSCREEN);
			break;
		default:
			right = GetSystemMetrics(SM_CXSCREEN);
			break;
		}
	}
	else if(ilWhichMonitor == 3)
	{
		switch(ilMonitors)
		{
		case 1:
			ilCXMonitor = ::GetSystemMetrics(SM_CXSCREEN);
			left = (int)((ilCXMonitor-ilWidth)/2);
			right = left + ilWidth;//GetSystemMetrics(SM_CXSCREEN);
			break;
		case 2:
			ilCXMonitor = (int)(::GetSystemMetrics(SM_CXSCREEN)/2);
			left = (int)((ilCXMonitor-ilWidth)/2);
			right = left + ilWidth;//GetSystemMetrics(SM_CXSCREEN);
			break;
		case 3:
			ilCXMonitor = (int)(::GetSystemMetrics(SM_CXSCREEN)/3);
			left = (int)((ilCXMonitor-ilWidth)/2)+(2*ilCXMonitor);
			right = left + ilWidth;//GetSystemMetrics(SM_CXSCREEN);
			break;
		default:
			right = GetSystemMetrics(SM_CXSCREEN);
			break;
		}
	}


//	SetWindowPos(&wndTop,left , top, 0, 0, SWP_SHOWWINDOW | SWP_NOSIZE);

}

BOOL SeasonCollectDlg::CheckPostFlight(COLLECTFLIGHTDATA *prpFlight)
{
	if (prpFlight == NULL)
		return FALSE;

	COLLECTFLIGHTDATA *prlAFlight = ogSeasonCollectFlights.GetArrival(prpFlight);
	COLLECTFLIGHTDATA *prlDFlight = ogSeasonCollectFlights.GetDeparture(prpFlight);

	BOOL blPost = FALSE;

	if (prlAFlight && prlDFlight)
	{
		if (prlAFlight->Tifa != TIMENULL && prlDFlight->Tifd != TIMENULL)
		{
			if (IsPostFlight(prlAFlight->Tifa) && IsPostFlight(prlDFlight->Tifd))
				blPost = TRUE;
		}
	}
	else if (!prlAFlight && prlDFlight)
	{
		if (prlDFlight->Tifd != TIMENULL)
		{
			if (IsPostFlight(prlDFlight->Tifd))
				blPost = TRUE;
		}
	}
	else if (prlAFlight && !prlDFlight)
	{
		if (prlAFlight->Tifa != TIMENULL)
		{
			if (IsPostFlight(prlAFlight->Tifa))
				blPost = TRUE;
		}
	}
	return blPost;
}


int SeasonCollectDlg::OnToolHitTest(CPoint opPoint,TOOLINFO *pTI) const
{
	static int ilLastToolTip = -1;

	CWnd *olpWnd = NULL;
	int ilItemId = -1;
	CRect olRect;

	// cursor over cellNr
	for (int i = 0; i < omCells.GetSize(); i++)
	{
		olpWnd = (CWnd*) &omCells[i];
		if (!olpWnd)
			return -1;

		olpWnd->GetWindowRect(olRect);
		this->ScreenToClient(olRect);
		if (olRect.PtInRect(opPoint))
		{
			ilItemId = i;
			break;
		}
	}

	// over cell
	if (ilItemId > -1)
	{
		// new position?
		if (ilItemId != ilLastToolTip)
		{
			ilLastToolTip = ilItemId;
			return -1;
		}

		// date of the cell
		char *olpChar = new char [256];
		CString olCellname = omCells[ilItemId].omName;
		CTime olCellTime = DateStringToDate(olCellname);

		// tooltip like: 12 Nov 2001
		CString olShowCellname = olCellTime.Format("%d %b %Y");

		// date is out of season
		if (olShowCellname.IsEmpty())
			olShowCellname = GetString(IDS_TOOLTIP_CELLDATE); //"out of season";

		// fill tooltip structure
		sprintf(olpChar, olShowCellname);
		pTI->cbSize = sizeof(*pTI);
		pTI->uFlags = TTF_IDISHWND;
		pTI->hwnd   = m_hWnd;
		pTI->uId    = (UINT) olpWnd->m_hWnd;
		pTI->lpszText = olpChar;
		return 1;
	}

	return -1;
}

void SeasonCollectDlg::DisplayRemarks()
{
	CCSPtrArray<COLLECTFLIGHTDATA> olFlights;
	CCSPtrArray<TABLE_COLUMN> olColList;
	CString olRkeys;

	GetSelectFlights(olFlights, olRkeys);
	int ilCount = olFlights.GetSize();

//	if (ilCount > 0)
//	{
		if (!pomTable)
			return;

		CString olRem1 = "";
		CString olRem2 = "";

		bool blRem1 = true;
		bool blRem2 = true;

		COLLECTFLIGHTDATA *prlFlight = NULL;

		if (ilCount > 0)
			prlFlight = &olFlights[0];

		if (prlFlight)
		{
			olRem1 = prlFlight->Rem1;
			olRem2 = prlFlight->Rem2;
		}

		for(int i = 1; i < ilCount; i++)
		{
			prlFlight = &olFlights[i];
			if ( olRem1.CompareNoCase( prlFlight->Rem1 ) != 0 )
			{
				blRem1 = false; 
				olRem1 = ""; 
			}
			if ( olRem2.CompareNoCase( prlFlight->Rem2 ) != 0 )
			{
				blRem2 = false; 
				olRem2 = ""; 
			}

			if (!blRem1 && !blRem2)
				break;
		}

		CString olFieldValue;
		CString olFieldText;
		for(i = pomTable->GetLinesCount() - 1; i >= 0; i--)
		{
			pomTable->GetTextFieldValue(i, 0, olFieldValue);
			pomTable->GetTextFieldValue(i, 1, olFieldText);

			if (olFieldValue == GetString(ST_BEMERKUNG))
			{
				TABLE_COLUMN* rlColumn = pomTable->FindColumn(i, 1);
				if (rlColumn)
				{
					rlColumn->Text = olRem1;
					pomTable->ChangeColumnData(i, 1, rlColumn);
				}
			}
			if (olFieldValue == GetString(IDS_STRING1455))
			{
				TABLE_COLUMN* rlColumn = pomTable->FindColumn(i, 1);
				if (rlColumn)
				{
					rlColumn->Text = olRem2;
					pomTable->ChangeColumnData(i, 1, rlColumn);
				}
			}
		}
//	}
}