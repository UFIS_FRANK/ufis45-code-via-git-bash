// FPMS.h : main header file for the FPMS application
//

#if !defined(AFX_CCSPRJ_H__FC3BA6F7_1AED_11D1_82C3_0080AD1DC701__INCLUDED_)
#define AFX_CCSPRJ_H__FC3BA6F7_1AED_11D1_82C3_0080AD1DC701__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols
//#include "ccsglobl.h"		
#include "BackGround.h"	
#include "CcsCedaData.h"	

/////////////////////////////////////////////////////////////////////////////
// CFPMSApp:
// See FPMS.cpp for the implementation of this class
//
#include "UFISAmSink.h"

class CFPMSApp : public CWinApp
{
public:
	CFPMSApp();
	~CFPMSApp();

    void InitialLoad(CWnd *pParent);
	void Customize();

	static void UnsetTopmostWnds();
	static void SetTopmostWnds();
	static bool ShowFlightRecordData(CWnd *opParent, char cpFTyp, long lpUrno, long lpRkey, char cpAdid, const CTime &ropTifad, bool bpLocalTimes, char cpMask = ' ');
	static int MyTopmostMessageBox(CWnd *polWnd, const CString &ropText, const CString &ropCaption, UINT ipType = 0);
	static bool DumpDebugLog(const CString &ropStr);
	static bool CheckCedaError(const CCSCedaData &ropCedaData);

	// For Com-Interface to the new TelexPool
	DWORD m_dwCookie;
	//


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CFPMSApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation

	//{{AFX_MSG(CFPMSApp)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

private:
	int ExitInstance();

	bool InitCOMInterface();
	bool SetCustomerConfig();

	char pcmLKey[32];
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CCSPRJ_H__FC3BA6F7_1AED_11D1_82C3_0080AD1DC701__INCLUDED_)
