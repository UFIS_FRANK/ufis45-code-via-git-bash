/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * 
 *	UFISTestClnt:	UFIS Application Manager Project
 *
 *	Example Application for C++ Dialog based clients
 *	
 *	ABB Airport Technologies GmbH 2000
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 *		Author			Date			Comment
 *	------------------------------------------------------------------
 *
 *		cla	AAT/IR		04/09/2000		Initial version
 *		cla AAT/IR		19/10/2000		modified for the FipsCUTE example
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

// UFISAmSink.cpp : implementation file
//

#include "stdafx.h"
#include "ButtonListDlg.h"
//#include "UFISTestClntDlg.h"
#include "UFISAmSink.h"
#include "ClntTypes.h"
#include <ATLCONV.H>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CUFISAmSink

IMPLEMENT_DYNCREATE(CUFISAmSink, CCmdTarget)

CUFISAmSink::CUFISAmSink() : m_UFISAmEventsAdvisor(IID_IUFISAmEventSink)
{
	m_pUFISAmLauncher = NULL;
	EnableAutomation();
}

CUFISAmSink::~CUFISAmSink()
{
}


BEGIN_MESSAGE_MAP(CUFISAmSink, CCmdTarget)
	//{{AFX_MSG_MAP(CUFISAmSink)
		// NOTE - the ClassWizard will add and remove mapping macros here.
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


/*----------------------------------------------------------------------------*/

BEGIN_DISPATCH_MAP(CUFISAmSink, CCmdTarget)
	//{{AFX_DISPATCH_MAP(CUFISAmSink)
	DISP_FUNCTION(CUFISAmSink, "ForwardData", OnForwardData,VT_EMPTY, VTS_I4 VTS_BSTR)
	DISP_FUNCTION(CUFISAmSink, "SetAppTag",			OnSetAppTag,VT_EMPTY, VTS_PI4)
  //}}AFX_DISPATCH_MAP
END_DISPATCH_MAP()

/*----------------------------------------------------------------------------*/

BEGIN_INTERFACE_MAP(CUFISAmSink, CCmdTarget)
	INTERFACE_PART(CUFISAmSink,IID_IUFISAmEventSink,Dispatch)
END_INTERFACE_MAP()

/*----------------------------------------------------------------------------*/

/////////////////////////////////////////////////////////////////////////////
// CUFISAmSink message handlers

BOOL CUFISAmSink::Advise(IUnknown* pSource, REFIID iid)
{
	// This GetInterface does not AddRef
	IUnknown* pUnknownSink = GetInterface(&IID_IUnknown);
	if (pUnknownSink == NULL)
	{
		return FALSE;
	}

	if (iid == IID_IUFISAmEventSink)
	{
		return m_UFISAmEventsAdvisor.Advise(pUnknownSink, pSource);
	}
	else 
	{
		return FALSE;
	}
}

/*----------------------------------------------------------------------------*/
	
BOOL CUFISAmSink::Unadvise(REFIID iid)
{
	if (iid == IID_IUFISAmEventSink)
	{
		return m_UFISAmEventsAdvisor.Unadvise();
	}
	else 
	{
		return FALSE;
	}
}

/*----------------------------------------------------------------------------*/

void CUFISAmSink::SetLauncher(CButtonListDlg* pUFISAmauncher)
{
	m_pUFISAmLauncher = pUFISAmauncher;
}

/*----------------------------------------------------------------------------*/

void CUFISAmSink::OnForwardData(long Orig,LPCSTR Data)
{
//  Skip any functionality up to now
	/*
	 * We have received a message from FipsCUTE
	 */
	//m_pUFISAmLauncher->m_rcvList.AddString(Data);
	TRACE("CUFISAmSink::OnForwardData: Message From '%ld'\n", Orig);
	if (Orig == static_cast<long>(TlxPool))
		m_pUFISAmLauncher->MessTlxpool(Data);
}

/*----------------------------------------------------------------------------*/

void CUFISAmSink::OnSetAppTag(long* AppID)
{
//	*AppID = 2;
//	AfxMessageBox("OnSetAppTag reached");
}

/*----------------------------------------------------------------------------*/
