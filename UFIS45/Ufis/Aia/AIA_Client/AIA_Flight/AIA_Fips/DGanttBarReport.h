#ifndef D_GANTT_BAR_REPORT
#define D_GANTT_BAR_REPORT

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

// GanttBarReport.h : header file
//
#include "DGanttChartReport.h"
#include "CColor.h"


/////////////////////////////////////////////////////////////////////////////
// GanttBarReport window


class DGanttBarReport
{
	public:
		DGanttBarReport(DGanttChartReport* popDGanttChartReport, CColor Color, 
			           unsigned int ipData, CString opText = "");
		virtual ~DGanttBarReport();

		void Draw(CDC* ppDCD, int ipSpace, int ipBarNo, int ipHeight, int ipHeightPrev, CString opBarText = "");
		bool Print(CDC* ppDCD, int ipSpace, int ipBarNo, int ipHeight, int ipHeightPrev, double dpFactorY);

	protected:

	private:
		CColor omColor;
		DGanttChartReport* pomDGanttChartReport;
		int imData;
		int imDataPrev;
		CString imXText;

};


#endif 
