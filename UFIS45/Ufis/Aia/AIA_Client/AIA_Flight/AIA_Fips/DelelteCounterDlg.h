#if !defined(AFX_DELELTECOUNTERDLG_H__17471151_51EC_11D2_85BD_0000C04D916B__INCLUDED_)
#define AFX_DELELTECOUNTERDLG_H__17471151_51EC_11D2_85BD_0000C04D916B__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// DelelteCounterDlg.h : header file
//

#include "CCSEdit.h"
/////////////////////////////////////////////////////////////////////////////
// DelelteCounterDlg dialog

class DelelteCounterDlg : public CDialog
{
// Construction
public:
	DelelteCounterDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(DelelteCounterDlg)
	enum { IDD = IDD_CCA_DELETE_FROMTO };
	CEdit	m_VT;
	CCSEdit	m_To;
	CCSEdit	m_From;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(DelelteCounterDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(DelelteCounterDlg)
	virtual void OnOK();
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DELELTECOUNTERDLG_H__17471151_51EC_11D2_85BD_0000C04D916B__INCLUDED_)
