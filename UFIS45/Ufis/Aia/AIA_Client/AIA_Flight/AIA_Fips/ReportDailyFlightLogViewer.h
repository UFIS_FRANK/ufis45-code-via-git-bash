#if !defined(REPORTDAILYFLIGHTLOGVIEWER_H__INCLUDED_)
#define REPORTDAILYFLIGHTLOGVIEWER_H__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
 
//#pragma warning(disable: 4786)

#include "stdafx.h"
#include "CCSTable.h"
#include "CCSDefines.h"
#include "CViewer.h"
#include "CCSPrint.h"
#include "CedaLoaTabData.h"

#include "RotationDlgCedaFlightData.h"
 
#define REPORTDAILYFLIGHTLOG_COLCOUNT 36

struct REPORTDAILYFLIGHTLOG_LINEDATA
{
	// Data of the arrival flight
 	long	AUrno;		// Urno of the arrival flight
 	CString	AFlno;		// Flightnumber
	CString	ADateDay;	// Day of Stoa
 	CString	AOrg3;		// Origin
	CString AVia3;		// first via
	CString ATTyp;		// Nature
	CTime	AStoa;		// Scheduled time of arrival
	CTime	AEtai;		// Estimated time of arrival
	CTime	ATmoa;		// Ten miles out
	CTime	ALand;		// Landing time
	CTime	AOnbl;		// Onblock time
	CString	APaxt;		// Total passengers
	CString	APsta;		// Position
	CString	AGta1;		// Gate
	CString	ABlt1;		// Belt

	// Data of the departure flight
	long	DUrno;		// Urno of the departure flights
	CString	DFlno;		// Flightnumber
	CString	DDateDay;	// Day of Stod
	CString	DDes3;		// Destination
	CString	DVia3;		// first via
	CString	DTTyp;		// Nature
	CTime	DStod;		// Scheduled time of departure
	CTime	DEtdi;		// Estimated time of departure
	CTime	DSlot;		// Slot time
	CTime	DOfbl;		// Offblock time
	CTime	DAirb;		// Takeoff time
	CString	DDcd1;		// Delay code 1
	CString	DDtd1;		// Delay time 1 in minutes
	CString	DDcd2;		// Delay code 2
	CString	DDtd2;		// Delay time 2 in minutes
	CString	DPaxt;		// Total passengers
	CString	DPstd;		// Position
	CString	DGtd1;		// Gate
	CString	DWro1;		// Waiting room

	// Common flight data
	CString	Status;		// flight status (cxx, diverted, noop, ..)
	CString	Act3;		// Aircraft type
	CString	Regn;		// Aircraft registration
	CString	Rem1;		// Remark
   
 	
	REPORTDAILYFLIGHTLOG_LINEDATA()
	{ 
 		AUrno = -1;
		AStoa = TIMENULL;
		AEtai = TIMENULL;
		ATmoa = TIMENULL;
		ALand = TIMENULL;
		AOnbl = TIMENULL;

		DUrno = -1;
		DStod = TIMENULL;
		DEtdi = TIMENULL;
		DSlot = TIMENULL;
		DOfbl = TIMENULL;
		DAirb = TIMENULL;

	}
};

 



/////////////////////////////////////////////////////////////////////////////
// Befehlsziel ReportDailyFlightLogViewer 


class ReportDailyFlightLogViewer : public CViewer
{
// Constructions
public:
    ReportDailyFlightLogViewer(bool bpLoatab = false);
    ~ReportDailyFlightLogViewer();

	// Connects the viewer with a table
    void Attach(CCSTable *popTable);
	// Load the intern line data from the given data and displays the table
    void ChangeViewTo(const CCSPtrArray<ROTATIONDLGFLIGHTDATA> &ropData, char *popDateStr);
 
	// Rebuild the table from the intern data
 	void UpdateDisplay(void);
	// Print table to paper
	void PrintTableView(void);
	// Print table to file
	bool PrintPlanToFile(const char *popFilePath) const;
	
	int GetFlightCount(void) const;
 	
private:

	// columns width of the table in char
	static int imTableColCharWidths[REPORTDAILYFLIGHTLOG_COLCOUNT];
	// columns width of the table in pixels
	int imTableColWidths[REPORTDAILYFLIGHTLOG_COLCOUNT];
	int imPrintColWidths[REPORTDAILYFLIGHTLOG_COLCOUNT];
	// table header strings
	CString omTableHeadlines[REPORTDAILYFLIGHTLOG_COLCOUNT];


	const int imOrientation;

	// Table fonts and widths for displaying
	CFont &romTableHeaderFont; 
	CFont &romTableLinesFont;
	const float fmTableHeaderFontWidth; 
	const float fmTableLinesFontWidth;

	// Table fonts and widths for printing
	CFont *pomPrintHeaderFont; 
	CFont *pomPrintLinesFont;
	float fmPrintHeaderFontWidth; 
	float fmPrintLinesFontWidth;

	
	void DrawTableHeader(void);
	// Transfer the data of the database to the intern data structures
	void MakeLines(const CCSPtrArray<ROTATIONDLGFLIGHTDATA> &ropData);
	// Create on intern line	
	bool MakeLine(const ROTATIONDLGFLIGHTDATA *prpAFlight, const ROTATIONDLGFLIGHTDATA *prpDFlight);
	// Copy the data from the db-record to the table-record
	bool MakeLineData(REPORTDAILYFLIGHTLOG_LINEDATA &rrpLineData, const ROTATIONDLGFLIGHTDATA *prpAFlight, const ROTATIONDLGFLIGHTDATA *prpDFlight);
	// Create a intern table data record
	int  CreateLine(const REPORTDAILYFLIGHTLOG_LINEDATA &rrpLine);
	// Fills one row of the table
	bool MakeColList(const REPORTDAILYFLIGHTLOG_LINEDATA &rrpLine, CCSPtrArray<TABLE_COLUMN> &ropColList) const;

	bool UtcToLocal(REPORTDAILYFLIGHTLOG_LINEDATA &rrpLineData) const;

	
	// Compares two lines of the table
	int CompareLines(const REPORTDAILYFLIGHTLOG_LINEDATA &rrpLine1, const REPORTDAILYFLIGHTLOG_LINEDATA &rrpLine2) const;

	bool PrintTableHeader(CCSPrint &ropPrint, int ipLeftOffset, int ipAddLeftOffset);
 
 	bool PrintTableLine(CCSPrint &ropPrint, int ipLineNo);
	// delete intern table lines
    void DeleteAll(void);
	// Date string
	char *pomDateStr;

	int imFlightNum;
 	// Table
 	CCSTable *pomTable;

	// Table Data
	CCSPtrArray<REPORTDAILYFLIGHTLOG_LINEDATA> omLines;

	// cleans a string(opChar) from char 0-31 (steuerzeichen) and return it in opChar
	bool CleanSteurzeichen (char *opChar, CString &opString);

	CedaLoaTabData omLoaData;
	bool bmLoatab;
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(REPORTDAILYFLIGHTLOGVIEWER_H__INCLUDED_)
