// AdditionalReport11TableViewer.cpp : implementation file
// 
// List of all flights according to rush hour


#include "stdafx.h"
#include "AdditionalReport11TableViewer.h"
#include "CcsGlobl.h"
#include <iomanip.h>
#include <iostream.h>
#include <fstream.h>
#include "BasicData.h"
#include "resrc1.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif


/////////////////////////////////////////////////////////////////////////////
// AdditionalReport11TableViewer
//


AdditionalReport11TableViewer::AdditionalReport11TableViewer(CCSPtrArray<ROTATIONDLGFLIGHTDATA> *popData,
														     const CStringArray &opShownColumns,
														     char *pcpInfo,
														     char *pcpSelect)
{

	pomData = popData;
	pcmInfo = pcpInfo;
	pcmSelect = pcpSelect;

	bmIsFromSearch = false;
    pomTable = NULL;
	imTableLine = 0;


	// Anfangs und Enddatum extrahieren
	CString olStr(pcpInfo);
	int ilIndex = olStr.Find("-");
	strcpy(pcmLastDate, (strstr(pcpInfo, "-") + 2));
	olStr.SetAt(ilIndex-1, 0);
	strcpy(pcmFirstDate, olStr);

	if ((strcmp(pcmFirstDate, pcmLastDate)) == 0)
		InitColumnsOneDay();
	else
		InitColumnsNDay();

	omShownColumns.Copy(opShownColumns);

}


AdditionalReport11TableViewer::~AdditionalReport11TableViewer()
{

	omPrintHeadHeaderArray.DeleteAll();
	omAnzChar.RemoveAll();
	omShownColumns.RemoveAll();
    DeleteAll();

	omColumn0.RemoveAll();
	omColumn1.RemoveAll();
	omColumn2.RemoveAll();
	omColumn3.RemoveAll();
	omColumn4.RemoveAll();
	omCountColumn1.RemoveAll();
	omCountColumn2.RemoveAll();

}


void AdditionalReport11TableViewer::InitColumnsNDay()
{

	char clTmp[8];
	int ilDayFirst, ilMonthFirst, ilYearFirst;
	int ilDayLast, ilMonthLast, ilYearLast;

	int i = 0;
	int j = 0;
	while(pcmFirstDate[i] != '.')
	{
		clTmp[j] = pcmFirstDate[i];
		j++;
		i++;
	}
	clTmp[j] = 0;
	ilDayFirst = atoi(clTmp);

	// integer aus Datumstring extrahieren
	j = 0;
	i++;
	while (pcmFirstDate[i] != '.')
	{
		clTmp[j] = pcmFirstDate[i];
		j++;
		i++;
	}
	clTmp[j] = 0;
	ilMonthFirst = atoi(clTmp);

	j = 0;
	i++;
	while (pcmFirstDate[i] != '\0')
	{
		clTmp[j] = pcmFirstDate[i];
		j++;
		i++;
	}
	clTmp[j] = 0;
	ilYearFirst = atoi(clTmp);

	j = 0;
	i = 0;
	while(pcmLastDate[i] != '.')
	{
		clTmp[j] = pcmLastDate[i];
		j++;
		i++;
	}
	clTmp[j] = 0;
	ilDayLast = atoi(clTmp);

	j = 0;
	i++;
	while (pcmLastDate[i] != '.')
	{
		clTmp[j] = pcmLastDate[i];
		j++;
		i++;
	}
	clTmp[j] = 0;
	ilMonthLast = atoi(clTmp);

	j = 0;
	i++;
	while (pcmLastDate[i] != '\0')
	{
		clTmp[j] = pcmLastDate[i];
		j++;
		i++;
	}
	clTmp[j] = 0;
	ilYearLast = atoi(clTmp);

	omTimeFirst = CTime(ilYearFirst, ilMonthFirst, ilDayFirst, 0, 0, 0);
	CTime t2(ilYearLast, ilMonthLast, ilDayLast, 23, 59, 59);
	CTimeSpan ts = t2 - omTimeFirst;

	lmDays = ts.GetDays();

	for (i = 0; i <= lmDays; i++)
	{
		omColumn0.Add("00:00");
		omColumn0.Add("01:00");
		omColumn0.Add("02:00");
		omColumn0.Add("03:00");
		omColumn0.Add("04:00");
		omColumn0.Add("05:00");
		omColumn0.Add("06:00");
		omColumn0.Add("07:00");
		omColumn0.Add("08:00");
		omColumn0.Add("09:00");
		omColumn0.Add("10:00");
		omColumn0.Add("11:00");
		omColumn0.Add("12:00");
		omColumn0.Add("13:00");
		omColumn0.Add("14:00");
		omColumn0.Add("15:00");
		omColumn0.Add("16:00");
		omColumn0.Add("17:00");
		omColumn0.Add("18:00");
		omColumn0.Add("19:00");
		omColumn0.Add("20:00");
		omColumn0.Add("21:00");
		omColumn0.Add("22:00");
		omColumn0.Add("23:00");
		for (int j = 0; j < 24; j++)
		{
			omCountColumn1.Add(0);
			omCountColumn2.Add(0);
		}
	}

}


void AdditionalReport11TableViewer::InitColumnsOneDay()
{

	char clTmp[8];
	int ilDayFirst, ilMonthFirst, ilYearFirst;
//	int ilDayLast, ilMonthLast, ilYearLast;

	int i = 0;
	int j = 0;
	while(pcmFirstDate[i] != '.')
	{
		clTmp[j] = pcmFirstDate[i];
		j++;
		i++;
	}
	clTmp[j] = 0;
	ilDayFirst = atoi(clTmp);

	// integer aus Datumstring extrahieren
	j = 0;
	i++;
	while (pcmFirstDate[i] != '.')
	{
		clTmp[j] = pcmFirstDate[i];
		j++;
		i++;
	}
	clTmp[j] = 0;
	ilMonthFirst = atoi(clTmp);

	j = 0;
	i++;
	while (pcmFirstDate[i] != '\0')
	{
		clTmp[j] = pcmFirstDate[i];
		j++;
		i++;
	}
	clTmp[j] = 0;
	ilYearFirst = atoi(clTmp);

	omTimeFirst = CTime(ilYearFirst, ilMonthFirst, ilDayFirst, 0, 0, 0);
	
	for (i = 0; i < 24; i++)
	{
		omCountColumn1.Add(0);
		omCountColumn2.Add(0);
	}

	omColumn0.Add("00:00");
	omColumn0.Add("01:00");
	omColumn0.Add("02:00");
	omColumn0.Add("03:00");
	omColumn0.Add("04:00");
	omColumn0.Add("05:00");
	omColumn0.Add("06:00");
	omColumn0.Add("07:00");
	omColumn0.Add("08:00");
	omColumn0.Add("09:00");
	omColumn0.Add("10:00");
	omColumn0.Add("11:00");
	omColumn0.Add("12:00");
	omColumn0.Add("13:00");
	omColumn0.Add("14:00");
	omColumn0.Add("15:00");
	omColumn0.Add("16:00");
	omColumn0.Add("17:00");
	omColumn0.Add("18:00");
	omColumn0.Add("19:00");
	omColumn0.Add("20:00");
	omColumn0.Add("21:00");
	omColumn0.Add("22:00");
	omColumn0.Add("23:00");

}



void AdditionalReport11TableViewer::SetColumnsToShow(const CStringArray &opShownColumns)
{

	CCS_TRY

	omShownColumns.Copy(opShownColumns);

	CCS_CATCH_ALL

}


void AdditionalReport11TableViewer::SetParentDlg(CDialog* ppParentDlg)
{

	pomParentDlg = ppParentDlg;

}


void AdditionalReport11TableViewer::Attach(CCSTable *popTable)
{
    pomTable = popTable;
}


int AdditionalReport11TableViewer::GetVia(CCSPtrArray<VIADATA> *opVias, ROTATIONDLGFLIGHTDATA *prpFlight)
{
	opVias->DeleteAll();

	if(prpFlight == NULL)
		return 0;
	if(strlen(prpFlight->Vial) == 0)
		return 0;

	CString olVias(prpFlight->Vial);
	CString olFids;
	CString olApc3;
	CString olApc4;

	VIADATA *prlVia;

	while(olVias.IsEmpty() != TRUE)
	{
		prlVia = new VIADATA;
		opVias->Add(prlVia);

		if(olVias.GetLength() < 120)
		{
			olVias += "                                                                                                                             ";
			olVias = olVias.Left(120);
		}


		olApc3 = olVias.Mid(1,3);
		olApc3.TrimLeft();
		sprintf(prlVia->Apc3, olApc3);

		if(olVias.GetLength() >= 120)
			olVias = olVias.Right(olVias.GetLength() - 120);
	}
	return opVias->GetSize();
}


void AdditionalReport11TableViewer::ChangeViewTo(const char *pcpViewName)
{
	TRACE("AdditionalReport11TableViewer::ChangeViewTo\n");
   
    pomTable->ResetContent();
    DeleteAll();    
    MakeLines();

	UpdateDisplay();
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
}


/////////////////////////////////////////////////////////////////////////////
// AdditionalReport11TableViewer -- code specific to this class

void AdditionalReport11TableViewer::MakeLines()
{
	ROTATIONDLGFLIGHTDATA *prlAFlight;
	ROTATIONDLGFLIGHTDATA *prlDFlight;
	ROTATIONDLGFLIGHTDATA *prlNextFlight = NULL;
	ROTATIONDLGFLIGHTDATA *prlFlight;
	int ilLineNo;

	bool blRDeparture = false;

	int ilFlightCount = pomData->GetSize();

	for (int ilLc = 0; ilLc < ilFlightCount; ilLc++)
	{
		prlFlight = &(*pomData)[ilLc];

		if(ilLc + 1 < ilFlightCount)
			prlNextFlight = &(*pomData)[ilLc + 1];	
		else
			prlNextFlight = NULL;	

		//Arrival
		if((strcmp(prlFlight->Org3, pcgHome) != 0) && (strcmp(prlFlight->Des3, pcgHome) == 0))
		{
			prlAFlight = prlFlight;
			prlDFlight = NULL;
			if(prlNextFlight != NULL)
			{
				if((prlNextFlight->Rkey == prlFlight->Rkey) && (prlFlight->Rkey != NULL))
				{
					prlDFlight = prlNextFlight;

					if((strcmp(prlNextFlight->Org3, pcgHome) == 0) && (strcmp(prlNextFlight->Des3, pcgHome) != 0))
					{
						ilLc++;
					}
					else
						blRDeparture = true;
				}
			}
		}
		else
		{
			// Departure
			if((strcmp(prlFlight->Org3, pcgHome) == 0) && (strcmp(prlFlight->Des3, pcgHome) != 0))
			{
				prlAFlight = NULL;
				prlDFlight = prlFlight;
				blRDeparture = false;
			}
			else
			{
				//Turnaround
				if((strcmp(prlFlight->Org3, pcgHome) == 0) && (strcmp(prlFlight->Des3, pcgHome) == 0))
				{
					if(blRDeparture)
					{
						blRDeparture = false;
						prlAFlight = prlFlight;
						prlDFlight = NULL;
						if(prlNextFlight != NULL)
						{
							if((prlNextFlight->Rkey == prlFlight->Rkey) && (prlFlight->Rkey != NULL))
							{
								prlDFlight = prlNextFlight;
								if((strcmp(prlNextFlight->Org3, pcgHome) == 0) && (strcmp(prlNextFlight->Des3, pcgHome) != 0))
								{
									ilLc++;
								}
								else
									blRDeparture = true;
							}
						}
					}
					else
					{
						prlAFlight		= prlFlight;
						prlDFlight		= prlFlight;
						if(prlNextFlight != NULL)
						{
							if((prlNextFlight->Rkey == prlFlight->Rkey) && (prlFlight->Rkey != NULL))
							{
								ilLc--;
								prlAFlight	 = NULL;
								blRDeparture = true;
							}
						}
					}
				}
			}
		}
		ilLineNo = MakeLine(prlAFlight, prlDFlight);

		// Listen zum Zaehlen aufbauen
/*		if ((strcmp(pcmFirstDate, pcmLastDate)) == 0)
			MakeCountListOneDay(prlAFlight, prlDFlight);
		else*/
			MakeCountListNDays(prlAFlight, prlDFlight);

	}


}

		

int AdditionalReport11TableViewer::MakeLine(ROTATIONDLGFLIGHTDATA *prpAFlight, ROTATIONDLGFLIGHTDATA *prpDFlight)
{
    ADDITIONAL_REPORT11_LINEDATA rlLine;

	if((prpAFlight != NULL) && (prpDFlight != NULL) )
	{
		MakeLineData(prpAFlight, prpDFlight, rlLine);
		return CreateLine(rlLine);
	}
	if (prpAFlight != NULL)
	{
		MakeLineData(prpAFlight, NULL, rlLine);
		return CreateLine(rlLine);
	}
	if (prpDFlight != NULL)
	{
		MakeLineData(NULL, prpDFlight, rlLine);
		return CreateLine(rlLine);
	}
	return -1;
}




void AdditionalReport11TableViewer::MakeLineData(ROTATIONDLGFLIGHTDATA *prpAFlight, ROTATIONDLGFLIGHTDATA *prpDFlight, ADDITIONAL_REPORT11_LINEDATA &rpLine)
{
	CString olStr;
	if(prpAFlight != NULL)
	{
		rpLine.AUrno =  prpAFlight->Urno;
		rpLine.ARkey =  prpAFlight->Rkey;
		rpLine.AFlno = CString(prpAFlight->Flno);
		rpLine.ADate = DateToHourDivString(prpAFlight->Onbl, prpAFlight->Stoa);
		rpLine.ATtyp = prpAFlight->Ttyp; 
		rpLine.AAct = CString(prpAFlight->Act3);
		rpLine.AAlc2 = CString(prpAFlight->Alc2);
		rpLine.AAlc3 = CString(prpAFlight->Alc3);

		rpLine.AStoa = CTime(prpAFlight->Stoa);
		rpLine.ALand = CTime(prpAFlight->Land);
		rpLine.AOnbl = CTime(prpAFlight->Onbl);
		if(bgReportLocal)
		{
			ogBasicData.UtcToLocal(rpLine.AStoa);
			ogBasicData.UtcToLocal(rpLine.ALand);
			ogBasicData.UtcToLocal(rpLine.AOnbl);
			ogBasicData.UtcToLocal(prpAFlight->Stoa);
			ogBasicData.UtcToLocal(prpAFlight->Land);
			ogBasicData.UtcToLocal(prpAFlight->Onbl);
		}
		rpLine.AEtai = CTime(prpAFlight->Etai);
		if(bgReportLocal) ogBasicData.UtcToLocal(rpLine.AEtai);
//		rpLine.ALand = CTime(prpAFlight->Land);
//		if(bgReportLocal) ogBasicData.UtcToLocal(rpLine.ALand);
	}
	else
	{

		rpLine.AUrno =  0;
		rpLine.ARkey =  0;
		rpLine.AFlno = "";
		rpLine.ADate = ""; 
		rpLine.ATtyp = ""; 
		rpLine.AAct = "";
		rpLine.AAlc2 = "";
		rpLine.AAlc3 = "";
		rpLine.AEtai = -1;
		rpLine.ALand = -1;
		rpLine.AStoa = -1;
		rpLine.AOnbl = -1;
	}


	if(prpDFlight != NULL)
	{

		rpLine.DUrno =  prpDFlight->Urno;
		rpLine.DRkey =  prpDFlight->Rkey;
		rpLine.DFlno = CString(prpDFlight->Flno); 
		rpLine.DTtyp = CString(prpDFlight->Ttyp);
		rpLine.DDes3 = CString(prpDFlight->Des3);
		rpLine.DAct = CString(prpDFlight->Act3);
		rpLine.DAlc2 = CString(prpDFlight->Alc2);
		rpLine.DAlc3 = CString(prpDFlight->Alc3);
		rpLine.DStod = CTime(prpDFlight->Stod);
		rpLine.DAirb = CTime(prpDFlight->Airb);
		rpLine.DOfbl = CTime(prpDFlight->Ofbl);
		if(bgReportLocal) 
		{
			ogBasicData.UtcToLocal(rpLine.DStod);
			ogBasicData.UtcToLocal(rpLine.DAirb);
			ogBasicData.UtcToLocal(rpLine.DOfbl);
			ogBasicData.UtcToLocal(prpDFlight->Stod);
			ogBasicData.UtcToLocal(prpDFlight->Airb);
			ogBasicData.UtcToLocal(prpDFlight->Ofbl);
		}

		if (rpLine.AAct.IsEmpty())
			rpLine.AAct = rpLine.DAct;

	}
	else
	{
		rpLine.DUrno =  0;
		rpLine.DRkey =  0;
		rpLine.DFlno = "";
		rpLine.DDate = ""; 
		rpLine.DTtyp = ""; 
		rpLine.DDes3 = "";
		rpLine.DAct = "";
		rpLine.DAlc2 = "";
		rpLine.DAlc3 = "";
		rpLine.DStod = -1;
		rpLine.DAirb = -1;
		rpLine.DOfbl = -1;
	}
    return;
}



int AdditionalReport11TableViewer::CreateLine(ADDITIONAL_REPORT11_LINEDATA &rpLine)
{
    int ilLineCount = omLines.GetSize();

    for (int ilLineno = ilLineCount; ilLineno > 0; ilLineno--)
        if (CompareFlight(&rpLine, &omLines[ilLineno-1]) >= 0)
            break;  // should be inserted after Lines[ilLineno-1]

    omLines.NewAt(ilLineno, rpLine);
	return ilLineno;
}


void AdditionalReport11TableViewer::DeleteLine(int ipLineno)
{
	omLines.DeleteAt(ipLineno);
}



void AdditionalReport11TableViewer::DeleteAll()
{
    while (omLines.GetSize() > 0)
        DeleteLine(0);
}


/////////////////////////////////////////////////////////////////////////////
// AdditionalReport11TableViewer - display drawing routine


// UpdateDisplay: Load data selected by filter conditions to the display by using "omTable"
void AdditionalReport11TableViewer::UpdateDisplay()
{

	CCS_TRY

	pomTable->ResetContent();

	ADDITIONAL_REPORT11_LINEDATA *prlLine;

	// Anzahl an Zeichen zur Berechnung der Spaltenbreite ermitteln
	int ilColumns = omShownColumns.GetSize();
	for (int i = 0; i < ilColumns; i++)
	{
		// Aufruf, damit omAnzCount Array gef�llt ist
		SetFieldLength(omShownColumns[i]);
	}
//	DrawHeader();
  
	for (int ilLc = 0; ilLc < omColumn0.GetSize(); ilLc++)
	{
		int ilColumnNo = 0;
		prlLine = &omLines[0];
		CCSPtrArray<TABLE_COLUMN> olColList;
	
		imTableLine = ilLc;

		MakeColList(prlLine, olColList, omColumn0[ilLc]);

//		pomTable->AddTextLine(olColList, (void*)(prlLine));
		olColList.DeleteAll();
	}

//	pomTable->DisplayTable();

	CCS_CATCH_ALL

}


void AdditionalReport11TableViewer::DrawHeader()
{

	CCS_TRY

	CCSPtrArray <TABLE_HEADER_COLUMN> omHeaderDataArray;

	pomTable->SetShowSelection(true);
	pomTable->ResetContent();

	TABLE_HEADER_COLUMN rlHeader;
	rlHeader.Font = &ogCourier_Bold_10;
	int i;

	// alle n Spalten (=omShownColumns.GetSize()) definieren
	int ilColums = omShownColumns.GetSize();
	int ilAnzChar = omAnzChar.GetSize();
	for (i = 0; i < ilColums; i++)
	{
		rlHeader.Alignment = COLALIGN_CENTER;
		// Anzahl an Textzeichen des Feldes der i-ten Spalte
		if (i <= ilAnzChar)
			rlHeader.AnzChar = omAnzChar[i];
		else
			break;
		// alten String l�schen, damit neuer String bei '0' anfaengt
		rlHeader.String = "";
		// Anzahl an Buchstaben 'W' setzen
		int ilAnzChar = omAnzChar[i];
		for (int j = 0; j < ilAnzChar; j++) 
			rlHeader.String += "W";
		// Text aus Konfiguration als Ueberschrift setzen
		rlHeader.Text = GetHeaderContent(omShownColumns[i]);
		// neue Spaltenheaderstruktur setzen
		omHeaderDataArray.New(rlHeader);
	}

	pomTable->SetHeaderFields(omHeaderDataArray);
	omHeaderDataArray.DeleteAll();

	pomTable->SetDefaultSeparator();

	CCS_CATCH_ALL

}




void AdditionalReport11TableViewer::MakeColList(ADDITIONAL_REPORT11_LINEDATA *prlLine, 
											   CCSPtrArray<TABLE_COLUMN> &olColList,
											   CString opCurrentDCD)
{

	CCS_TRY

	TABLE_COLUMN rlColumnData;

	rlColumnData.VerticalSeparator = SEPA_NONE;
	rlColumnData.SeparatorType = SEPA_NONE;
	rlColumnData.Font = &ogCourier_Regular_9;

	// Spaltendefinitionen bestimmen
	for (int i = 0; i < omShownColumns.GetSize(); i++)
	{
		rlColumnData.Alignment = COLALIGN_LEFT;
		// Holen des Feldinhaltes der i-ten Spalte und n-ten Zeile
		//rlColumnData.Text = GetFieldContent(omShownColumns[i]);

		olColList.NewAt(olColList.GetSize(), rlColumnData);
	}

	CCS_CATCH_ALL

}




int AdditionalReport11TableViewer::CompareFlight(ADDITIONAL_REPORT11_LINEDATA *prpLine1, ADDITIONAL_REPORT11_LINEDATA *prpLine2)
{
	int	ilCompareResult;
	CTime olTime1;
	CTime olTime2;


		ilCompareResult = (olTime1 == olTime2)? 0:
			(olTime1 > olTime2)? 1: -1;

		 return ilCompareResult;
}




//-----------------------------------------------------------------------------------------------
// Printing Routines
//-----------------------------------------------------------------------------------------------

void AdditionalReport11TableViewer::GetHeader()
{
	
	CCS_TRY
		
	TABLE_HEADER_COLUMN *prlHeader[50];

	int i;
	int ilColomns = omShownColumns.GetSize();
	CSize olSize;
    pomPrint->omCdc.SelectObject(&ogCourier_Bold_10);

	for (i = 0; i < ilColomns; i++)
	{
		if (i == 0)
			olSize = pomPrint->omCdc.GetTextExtent(omColumn0[0]);
		else
			olSize = pomPrint->omCdc.GetTextExtent(GetHeaderContent(omShownColumns[i]));
		prlHeader[i] = new TABLE_HEADER_COLUMN;
		prlHeader[i]->Alignment = COLALIGN_CENTER;
		prlHeader[i]->Font = &ogCourier_Bold_10;
		prlHeader[i]->Length = olSize.cx;
		prlHeader[i]->Text = GetHeaderContent(omShownColumns[i]);
	}

	omPrintHeadHeaderArray.DeleteAll();
	for(i = 0; i < ilColomns; i++)
	{
		omPrintHeadHeaderArray.Add(prlHeader[i]);
	}

	CCS_CATCH_ALL

}



void AdditionalReport11TableViewer::PrintTableView(GanttChartReportWnd* popGanttChartReportWnd)
{

	CCS_TRY

	CString olFooter1;
	CString olTableName = GetString(IDS_STRING1256);
	char pclHeader[256];

	CString olTimes;
	if (bgReportLocal)
		olTimes = GetString(IDS_STRING1921);
	else
		olTimes = GetString(IDS_STRING1920);

	sprintf(pclHeader, GetString(IDS_STRING1653), pcmSelect, pcmInfo, olTimes);
	CString olHeader(pclHeader);

	int ilOrientation = PRINT_LANDSCAPE;
	pomPrint = new CCSPrint(pomParentDlg,ilOrientation,45);
	pomPrint->imMaxLines = 38;

	if (pomPrint != NULL)
	{

		if (pomPrint->InitializePrinter(ilOrientation) == TRUE)
		{
			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
			pomPrint->imLineNo = pomPrint->imMaxLines + 1;
			DOCINFO	rlDocInfo;
			memset(&rlDocInfo, 0, sizeof(DOCINFO));
			rlDocInfo.cbSize = sizeof( DOCINFO );
			rlDocInfo.lpszDocName = olTableName;	
			pomPrint->omCdc.StartDoc( &rlDocInfo );
			pomPrint->imPageNo = 0;


			CString olFooter1;
			olFooter1.Format("%s %d",rlDocInfo.lpszDocName, GetFlightCount());
			popGanttChartReportWnd->Print(&pomPrint->omCdc, pomPrint, olHeader, olFooter1);

/*			if(pomPrint->imLineNo >= pomPrint->imMaxLines)
			{
				if(pomPrint->imPageNo > 0)
				{
					olFooter2.Format("Tach",pomPrint->imPageNo);
					pomPrint->PrintUIFFooter(olFooter1,"",olFooter2);
					pomPrint->omCdc.EndPage();
				}
			}
			olFooter2.Format("Tach2",pomPrint->imPageNo);
			pomPrint->PrintUIFFooter(olFooter1,"",olFooter2);
			pomPrint->omCdc.EndPage();
			pomPrint->omCdc.EndDoc();*/
		}

		SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
		delete pomPrint;
		pomPrint = NULL;
	}

	CCS_CATCH_ALL

}


//-----------------------------------------------------------------------------------------------

bool AdditionalReport11TableViewer::PrintTableHeader()
{
	pomPrint->omCdc.StartPage();
	pomPrint->imPageNo++;
	pomPrint->imLineNo = 0;
	double dgCCSPrintFactor = 2.7;
	char pclHeader[256];

	sprintf(pclHeader, GetString(IDS_STRING1653), pcmSelect, pcmInfo);
	CString olTableName(pclHeader);

	pomPrint->PrintUIFHeader(omTableName,CString(pclHeader),pomPrint->imFirstLine-10);

	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;

	rlElement.Alignment   = PRINT_LEFT;
	rlElement.FrameLeft   = PRINT_NOFRAME;
	rlElement.FrameRight  = PRINT_NOFRAME;
	rlElement.FrameTop    = PRINT_NOFRAME;
	rlElement.FrameBottom = PRINT_FRAMEMEDIUM;
	rlElement.pFont       = &pomPrint->ogCourierNew_Bold_8;

	//int ilSize = omHeaderDataArray.GetSize();
	int ilSize = omPrintHeadHeaderArray.GetSize();

	for(int i=0;i<ilSize;i++)
	{
		//rlElement.Length = (int)(omHeaderDataArray[i].Length*dgCCSPrintFactor);
		rlElement.Length = (int)(omPrintHeadHeaderArray[i].Length*dgCCSPrintFactor);
		if(rlElement.Length < igCCSPrintMinLength)rlElement.Length+=igCCSPrintMoreLength; 
		//rlElement.Text   = omHeaderDataArray[i].Text;
		rlElement.Text   = omPrintHeadHeaderArray[i].Text;
		rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
	}
	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();


	return true;
}

//-----------------------------------------------------------------------------------------------

bool AdditionalReport11TableViewer::PrintTableLine(ADDITIONAL_REPORT11_LINEDATA *prpLine,
												  bool bpLastLine, CString opCurrentAirline)
{

	CCS_TRY

	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;
	double dgCCSPrintFactor = 2.7;

    pomPrint->omCdc.SelectObject(&pomPrint->ogCourierNew_Regular_8);
	CSize olSize;
		
	rlElement.pFont = &pomPrint->ogCourierNew_Regular_8;

	int ilSize = omPrintHeadHeaderArray.GetSize();
	for(int i=0;i<ilSize;i++)
	{
		olSize = pomPrint->omCdc.GetTextExtent(omShownColumns[i]);

		if (i == 0)
			rlElement.Length = (int)((omPrintHeadHeaderArray[i].Length + 11) * dgCCSPrintFactor);
		else
			rlElement.Length = (int)(omPrintHeadHeaderArray[i].Length*dgCCSPrintFactor);
		if(rlElement.Length < igCCSPrintMinLength)
			rlElement.Length+=igCCSPrintMoreLength; 
		rlElement.Alignment  = PRINT_LEFT;
		rlElement.FrameTop   = PRINT_FRAMETHIN;
		if(bpLastLine == true)
		{
			rlElement.FrameBottom = PRINT_FRAMETHIN;
		}
		else
		{
			rlElement.FrameBottom = PRINT_FRAMETHIN/*PRINT_NOFRAME*/;
		}
		
		rlElement.FrameLeft  = PRINT_NOFRAME;
		rlElement.FrameRight = PRINT_NOFRAME;
		//rlElement.Text = GetFieldContent(omShownColumns[i]);

		rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
		
	}

	pomPrint->PrintLine(rlPrintLine);

	rlPrintLine.DeleteAll();
	
	CCS_CATCH_ALL

	return true;

}

//-----------------------------------------------------------------------------------------------


// Drucken in Datei
void AdditionalReport11TableViewer::PrintPlanToFile(char *pcpDefPath)
{

	CCS_TRY

	ofstream of;
	int ilCountLines = omColumn0.GetSize();
	int ilCountRows  = omShownColumns.GetSize();

	char pclHeader[128];
	sprintf(pclHeader, GetString(IDS_STRING1653), pcmInfo);


	of.open( pcpDefPath, ios::out);
	of << CString(pclHeader) << " "
		<< CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M") << endl << endl;

	for (int i = 0; i < ilCountRows; i++)
	{
		of << GetHeaderContent(omShownColumns[i]) << "   ";
		if (i == 0)
			of << "     ";
	}
	               
	of << endl << "------------------------------------------------------" << endl;


	// zeilen iterieren
	for(int ilLines = 0; ilLines < ilCountLines; ilLines++)
	{
		imTableLine = ilLines;

		of.setf(ios::left, ios::adjustfield);
		
		of 
/*		<< setw(strlen(omShownColumns[0]) + 14) << GetFieldContent(omShownColumns[0])
		<< setw(GetHeaderContent(omShownColumns[1]).GetLength() + 6) << GetFieldContent(omShownColumns[1])
		<< setw(GetHeaderContent(omShownColumns[2]).GetLength() + 5) << GetFieldContent(omShownColumns[2])*/
		// Zeile abschliessen
		<< endl;
	}

	// stream schliessen
	of.close();

	CCS_CATCH_ALL
	
}


int AdditionalReport11TableViewer::GetFlightCount()
{

	int ilFlightCount = 0;
	int i;

	for (i = 0; i < omCountColumn1.GetSize(); i++)
		ilFlightCount += omCountColumn1[i];
	for (i = 0; i < omCountColumn2.GetSize(); i++)
		ilFlightCount += omCountColumn2[i];
	
	return ilFlightCount;

}


void AdditionalReport11TableViewer::MakeCountListNDays(ROTATIONDLGFLIGHTDATA *prpAFlight, ROTATIONDLGFLIGHTDATA *prpDFlight)
{

	int ilCol0Index = 0;	// Index der Airline im Array omColumn0
	int ilTimeFrane = - 1;
	bool blItemExists = false;

	// keine Daten? --> tschuess
	if (prpAFlight == NULL && prpDFlight == NULL)
		return;


	int ilYearCurrent;
	int ilMonthCurrent;
	int ilDayCurrent;
	int ilSwitch;
	CString olTime;

	bool blValidTime = true;
	if (prpAFlight != NULL)
	{
		if (strcmp(pcmSelect, "LAND/AIRB") == 0)
		{
			if (prpAFlight->Land != TIMENULL)
			{
				ilYearCurrent  = atoi(prpAFlight->Land.Format("%Y").GetBuffer(0));
				ilMonthCurrent = atoi(prpAFlight->Land.Format("%m").GetBuffer(0));
				ilDayCurrent   = atoi(prpAFlight->Land.Format("%d").GetBuffer(0));
				olTime = prpAFlight->Land.Format("%H:%M");	
				ilSwitch = prpAFlight->Land.GetHour();
			}
			else
				blValidTime = false;

		}
		else if (strcmp(pcmSelect, "ONBL/OFBL") == 0)
		{
			if (prpAFlight->Onbl != TIMENULL)
			{
				ilYearCurrent  = atoi(prpAFlight->Onbl.Format("%Y").GetBuffer(0));
				ilMonthCurrent = atoi(prpAFlight->Onbl.Format("%m").GetBuffer(0));
				ilDayCurrent   = atoi(prpAFlight->Onbl.Format("%d").GetBuffer(0));
				olTime = prpAFlight->Onbl.Format("%H:%M");	
				ilSwitch = prpAFlight->Onbl.GetHour();
			}
			else
				blValidTime = false;
		}
		else
		{
			if (prpAFlight->Stoa != TIMENULL)
			{
				ilYearCurrent  = atoi(prpAFlight->Stoa.Format("%Y").GetBuffer(0));
				ilMonthCurrent = atoi(prpAFlight->Stoa.Format("%m").GetBuffer(0));
				ilDayCurrent   = atoi(prpAFlight->Stoa.Format("%d").GetBuffer(0));
				olTime = prpAFlight->Stoa.Format("%H:%M");	
				ilSwitch = prpAFlight->Stoa.GetHour();
			}
			else
				blValidTime = false;
		}

		if (blValidTime)
		{
			CTime t2(ilYearCurrent, ilMonthCurrent, ilDayCurrent, 23, 59, 59);
			CTimeSpan ts = t2 - omTimeFirst;

			long llDaysBetweenFirstAndCurrent = ts.GetDays();

			switch (ilSwitch)
			{
				case 0:
					omCountColumn1[(int)llDaysBetweenFirstAndCurrent*24 + 0] = omCountColumn1[(int)llDaysBetweenFirstAndCurrent*24 + 0] + 1;
				break;
				case 1:
					omCountColumn1[(int)llDaysBetweenFirstAndCurrent*24 + 1] = omCountColumn1[(int)llDaysBetweenFirstAndCurrent*24 + 1] + 1;
				break;
				case 2:
					omCountColumn1[(int)llDaysBetweenFirstAndCurrent*24 + 2] = omCountColumn1[(int)llDaysBetweenFirstAndCurrent*24 + 2] + 1;
				break;
				case 3:
					omCountColumn1[(int)llDaysBetweenFirstAndCurrent*24 + 3] = omCountColumn1[(int)llDaysBetweenFirstAndCurrent*24 + 3] + 1;
				break;
				case 4:
					omCountColumn1[(int)llDaysBetweenFirstAndCurrent*24 + 4] = omCountColumn1[(int)llDaysBetweenFirstAndCurrent*24 + 4] + 1;
				break;
				case 5:
					omCountColumn1[(int)llDaysBetweenFirstAndCurrent*24 + 5] = omCountColumn1[(int)llDaysBetweenFirstAndCurrent*24 + 5] + 1;
				break;
				case 6:
					omCountColumn1[(int)llDaysBetweenFirstAndCurrent*24 + 6] = omCountColumn1[(int)llDaysBetweenFirstAndCurrent*24 + 6] + 1;
				break;
				case 7:
					omCountColumn1[(int)llDaysBetweenFirstAndCurrent*24 + 7] = omCountColumn1[(int)llDaysBetweenFirstAndCurrent*24 + 7] + 1;
				break;
				case 8:
					omCountColumn1[(int)llDaysBetweenFirstAndCurrent*24 + 8] = omCountColumn1[(int)llDaysBetweenFirstAndCurrent*24 + 8] + 1;
				break;
				case 9:
					omCountColumn1[(int)llDaysBetweenFirstAndCurrent*24 + 9] = omCountColumn1[(int)llDaysBetweenFirstAndCurrent*24 + 9] + 1;
				break;
				case 10:
					omCountColumn1[(int)llDaysBetweenFirstAndCurrent*24 + 10] = omCountColumn1[(int)llDaysBetweenFirstAndCurrent*24 + 10] + 1;
				break;
				case 11:
					omCountColumn1[(int)llDaysBetweenFirstAndCurrent*24 + 11] = omCountColumn1[(int)llDaysBetweenFirstAndCurrent*24 + 11] + 1;
				break;
				case 12:
					omCountColumn1[(int)llDaysBetweenFirstAndCurrent*24 + 12] = omCountColumn1[(int)llDaysBetweenFirstAndCurrent*24 + 12] + 1;
				break;
				case 13:
					omCountColumn1[(int)llDaysBetweenFirstAndCurrent*24 + 13] = omCountColumn1[(int)llDaysBetweenFirstAndCurrent*24 + 13] + 1;
				break;
				case 14:
					omCountColumn1[(int)llDaysBetweenFirstAndCurrent*24 + 14] = omCountColumn1[(int)llDaysBetweenFirstAndCurrent*24 + 14] + 1;
				break;
				case 15:
					omCountColumn1[(int)llDaysBetweenFirstAndCurrent*24 + 15] = omCountColumn1[(int)llDaysBetweenFirstAndCurrent*24 + 15] + 1;
				break;
				case 16:
					omCountColumn1[(int)llDaysBetweenFirstAndCurrent*24 + 16] = omCountColumn1[(int)llDaysBetweenFirstAndCurrent*24 + 16] + 1;
				break;
				case 17:
					omCountColumn1[(int)llDaysBetweenFirstAndCurrent*24 + 17] = omCountColumn1[(int)llDaysBetweenFirstAndCurrent*24 + 17] + 1;
				break;
				case 18:
					omCountColumn1[(int)llDaysBetweenFirstAndCurrent*24 + 18] = omCountColumn1[(int)llDaysBetweenFirstAndCurrent*24 + 18] + 1;
				break;
				case 19:
					omCountColumn1[(int)llDaysBetweenFirstAndCurrent*24 + 19] = omCountColumn1[(int)llDaysBetweenFirstAndCurrent*24 + 19] + 1;
				break;
				case 20:
					omCountColumn1[(int)llDaysBetweenFirstAndCurrent*24 + 20] = omCountColumn1[(int)llDaysBetweenFirstAndCurrent*24 + 20] + 1;
				break;
				case 21:
					omCountColumn1[(int)llDaysBetweenFirstAndCurrent*24 + 21] = omCountColumn1[(int)llDaysBetweenFirstAndCurrent*24 + 21] + 1;
				break;
				case 22:
					omCountColumn1[(int)llDaysBetweenFirstAndCurrent*24 + 22] = omCountColumn1[(int)llDaysBetweenFirstAndCurrent*24 + 22] + 1;
				break;
				case 23:
					omCountColumn1[(int)llDaysBetweenFirstAndCurrent*24 + 23] = omCountColumn1[(int)llDaysBetweenFirstAndCurrent*24 + 23] + 1;
				break;
			}
		}
	}

	blValidTime = true;
	if (prpDFlight != NULL)
	{
		if (strcmp(pcmSelect, "LAND/AIRB") == 0)
		{
			if (prpDFlight->Airb != TIMENULL)
			{
				ilYearCurrent  = atoi(prpDFlight->Airb.Format("%Y").GetBuffer(0));
				ilMonthCurrent = atoi(prpDFlight->Airb.Format("%m").GetBuffer(0));
				ilDayCurrent   = atoi(prpDFlight->Airb.Format("%d").GetBuffer(0));
				olTime = prpDFlight->Airb.Format("%H:%M");	
				ilSwitch = prpDFlight->Airb.GetHour();
			}
			else
				blValidTime = false;
		}
		else if (strcmp(pcmSelect, "ONBL/OFBL") == 0)
		{
			if (prpDFlight->Ofbl != TIMENULL)
			{
				ilYearCurrent  = atoi(prpDFlight->Ofbl.Format("%Y").GetBuffer(0));
				ilMonthCurrent = atoi(prpDFlight->Ofbl.Format("%m").GetBuffer(0));
				ilDayCurrent   = atoi(prpDFlight->Ofbl.Format("%d").GetBuffer(0));
				olTime = prpDFlight->Ofbl.Format("%H:%M");	
				ilSwitch = prpDFlight->Ofbl.GetHour();
			}
			else
				blValidTime = false;
		}
		else
		{
			if (prpDFlight->Stod != TIMENULL)
			{
				ilYearCurrent  = atoi(prpDFlight->Stod.Format("%Y").GetBuffer(0));
				ilMonthCurrent = atoi(prpDFlight->Stod.Format("%m").GetBuffer(0));
				ilDayCurrent   = atoi(prpDFlight->Stod.Format("%d").GetBuffer(0));
				olTime = prpDFlight->Stod.Format("%H:%M");	
				ilSwitch = prpDFlight->Stod.GetHour();
			}
			else
				blValidTime = false;
		}

		if (blValidTime)
		{
			CTime t2(ilYearCurrent, ilMonthCurrent, ilDayCurrent, 23, 59, 59);
			CTimeSpan ts = t2 - omTimeFirst;

			long llDaysBetweenFirstAndCurrent = ts.GetDays();

			switch (ilSwitch)
			{
				case 0:
					omCountColumn1[(int)llDaysBetweenFirstAndCurrent*24 + 0] = omCountColumn1[(int)llDaysBetweenFirstAndCurrent*24 + 0] + 1;
				break;
				case 1:
					omCountColumn1[(int)llDaysBetweenFirstAndCurrent*24 + 1] = omCountColumn1[(int)llDaysBetweenFirstAndCurrent*24 + 1] + 1;
				break;
				case 2:
					omCountColumn1[(int)llDaysBetweenFirstAndCurrent*24 + 2] = omCountColumn1[(int)llDaysBetweenFirstAndCurrent*24 + 2] + 1;
				break;
				case 3:
					omCountColumn1[(int)llDaysBetweenFirstAndCurrent*24 + 3] = omCountColumn1[(int)llDaysBetweenFirstAndCurrent*24 + 3] + 1;
				break;
				case 4:
					omCountColumn1[(int)llDaysBetweenFirstAndCurrent*24 + 4] = omCountColumn1[(int)llDaysBetweenFirstAndCurrent*24 + 4] + 1;
				break;
				case 5:
					omCountColumn1[(int)llDaysBetweenFirstAndCurrent*24 + 5] = omCountColumn1[(int)llDaysBetweenFirstAndCurrent*24 + 5] + 1;
				break;
				case 6:
					omCountColumn1[(int)llDaysBetweenFirstAndCurrent*24 + 6] = omCountColumn1[(int)llDaysBetweenFirstAndCurrent*24 + 6] + 1;
				break;
				case 7:
					omCountColumn1[(int)llDaysBetweenFirstAndCurrent*24 + 7] = omCountColumn1[(int)llDaysBetweenFirstAndCurrent*24 + 7] + 1;
				break;
				case 8:
					omCountColumn1[(int)llDaysBetweenFirstAndCurrent*24 + 8] = omCountColumn1[(int)llDaysBetweenFirstAndCurrent*24 + 8] + 1;
				break;
				case 9:
					omCountColumn1[(int)llDaysBetweenFirstAndCurrent*24 + 9] = omCountColumn1[(int)llDaysBetweenFirstAndCurrent*24 + 9] + 1;
				break;
				case 10:
					omCountColumn1[(int)llDaysBetweenFirstAndCurrent*24 + 10] = omCountColumn1[(int)llDaysBetweenFirstAndCurrent*24 + 10] + 1;
				break;
				case 11:
					omCountColumn1[(int)llDaysBetweenFirstAndCurrent*24 + 11] = omCountColumn1[(int)llDaysBetweenFirstAndCurrent*24 + 11] + 1;
				break;
				case 12:
					omCountColumn1[(int)llDaysBetweenFirstAndCurrent*24 + 12] = omCountColumn1[(int)llDaysBetweenFirstAndCurrent*24 + 12] + 1;
				break;
				case 13:
					omCountColumn1[(int)llDaysBetweenFirstAndCurrent*24 + 13] = omCountColumn1[(int)llDaysBetweenFirstAndCurrent*24 + 13] + 1;
				break;
				case 14:
					omCountColumn1[(int)llDaysBetweenFirstAndCurrent*24 + 14] = omCountColumn1[(int)llDaysBetweenFirstAndCurrent*24 + 14] + 1;
				break;
				case 15:
					omCountColumn1[(int)llDaysBetweenFirstAndCurrent*24 + 15] = omCountColumn1[(int)llDaysBetweenFirstAndCurrent*24 + 15] + 1;
				break;
				case 16:
					omCountColumn1[(int)llDaysBetweenFirstAndCurrent*24 + 16] = omCountColumn1[(int)llDaysBetweenFirstAndCurrent*24 + 16] + 1;
				break;
				case 17:
					omCountColumn1[(int)llDaysBetweenFirstAndCurrent*24 + 17] = omCountColumn1[(int)llDaysBetweenFirstAndCurrent*24 + 17] + 1;
				break;
				case 18:
					omCountColumn1[(int)llDaysBetweenFirstAndCurrent*24 + 18] = omCountColumn1[(int)llDaysBetweenFirstAndCurrent*24 + 18] + 1;
				break;
				case 19:
					omCountColumn1[(int)llDaysBetweenFirstAndCurrent*24 + 19] = omCountColumn1[(int)llDaysBetweenFirstAndCurrent*24 + 19] + 1;
				break;
				case 20:
					omCountColumn1[(int)llDaysBetweenFirstAndCurrent*24 + 20] = omCountColumn1[(int)llDaysBetweenFirstAndCurrent*24 + 20] + 1;
				break;
				case 21:
					omCountColumn1[(int)llDaysBetweenFirstAndCurrent*24 + 21] = omCountColumn1[(int)llDaysBetweenFirstAndCurrent*24 + 21] + 1;
				break;
				case 22:
					omCountColumn1[(int)llDaysBetweenFirstAndCurrent*24 + 22] = omCountColumn1[(int)llDaysBetweenFirstAndCurrent*24 + 22] + 1;
				break;
				case 23:
					omCountColumn1[(int)llDaysBetweenFirstAndCurrent*24 + 23] = omCountColumn1[(int)llDaysBetweenFirstAndCurrent*24 + 23] + 1;
				break;
			}
		}
	}

	char tmp[16];
	// Fl�ge aus Ankunftsteil
	if (prpAFlight != NULL)
	{
		if (strcmp(pcmSelect, "LAND/AIRB") == 0)
		{
			if (prpAFlight->Land != TIMENULL)
			{
				strcpy(tmp, prpAFlight->Land.Format("%Y%m%d"));
				for (int i = 0; i < omColumn0.GetSize(); i++)
				{
					if (omColumn0[i] == prpAFlight->Land.Format("%Y%m%d"))
						omCountColumn1[i] = omCountColumn1[i] + 1;
				}
			}
		}
		if (strcmp(pcmSelect, "ONBL/OFBL") == 0)
		{
			if (prpAFlight->Onbl != TIMENULL)
			{
				strcpy(tmp, prpAFlight->Onbl.Format("%Y%m%d"));
				for (int i = 0; i < omColumn0.GetSize(); i++)
				{
					if (omColumn0[i] == prpAFlight->Onbl.Format("%Y%m%d"))
						omCountColumn1[i] = omCountColumn1[i] + 1;
				}
			}
		}
		else
		{
			if (prpAFlight->Stoa != TIMENULL)
			{
				strcpy(tmp, prpAFlight->Stoa.Format("%Y%m%d"));
				for (int i = 0; i < omColumn0.GetSize(); i++)
				{
					if (omColumn0[i] == prpAFlight->Stoa.Format("%Y%m%d"))
						omCountColumn1[i] = omCountColumn1[i] + 1;
				}
			}
		}
	}

	if (prpDFlight != NULL)
	{
		if (strcmp(pcmSelect, "LAND/AIRB") == 0)
		{
			if (prpDFlight->Airb != TIMENULL)
			{
				for (int i = 0; i < omColumn0.GetSize(); i++)
				{
					if (omColumn0[i] == prpDFlight->Airb.Format("%Y%m%d"))
						omCountColumn2[i] = omCountColumn2[i] + 1;
				}
			}
		}
		else if (strcmp(pcmSelect, "ONBL/OFBL") == 0)
		{
			if (prpDFlight->Ofbl != TIMENULL)
			{
				for (int i = 0; i < omColumn0.GetSize(); i++)
				{
					if (omColumn0[i] == prpDFlight->Ofbl.Format("%Y%m%d"))
						omCountColumn2[i] = omCountColumn2[i] + 1;
				}
			}
		}
		else
		{
			if (prpDFlight->Stod != TIMENULL)
			{
				for (int i = 0; i < omColumn0.GetSize(); i++)
				{
					if (omColumn0[i] == prpDFlight->Stod.Format("%Y%m%d"))
						omCountColumn2[i] = omCountColumn2[i] + 1;
				}
			}
		}
	}

}


int AdditionalReport11TableViewer::GetBarCount()
{

	return omColumn0.GetSize();

}


void AdditionalReport11TableViewer::MakeCountListOneDay(ROTATIONDLGFLIGHTDATA *prpAFlight, ROTATIONDLGFLIGHTDATA *prpDFlight)
{

	int ilCol0Index = 0;	// Index der Airline im Array omColumn0
	int ilTimeFrane = - 1;
	bool blItemExists = false;

	// keine Daten? --> tschuess
	if (prpAFlight == NULL && prpDFlight == NULL)
		return;

	// Fl�ge aus Ankunftsteil
	if (prpAFlight != NULL)
	{
		int ilSwitch;
		if (strcmp(pcmSelect, "LAND/AIRB") == 0)
		{
			CString olDate = prpAFlight->Land.Format("%d.%m.%y");
			CString olTime = prpAFlight->Land.Format("%H:%M");	
			ilSwitch = prpAFlight->Land.GetHour();
		}
		else if (strcmp(pcmSelect, "ONBL/OFBL") == 0)
		{
			CString olDate = prpAFlight->Onbl.Format("%d.%m.%y");
			CString olTime = prpAFlight->Onbl.Format("%H:%M");	
			ilSwitch = prpAFlight->Onbl.GetHour();
		}
		else
		{
			CString olDate = prpAFlight->Stoa.Format("%d.%m.%y");
			CString olTime = prpAFlight->Stoa.Format("%H:%M");	
			ilSwitch = prpAFlight->Stoa.GetHour();
		}

//		switch (prpAFlight->Stoa.GetHour())
		switch (ilSwitch)
		{
			case 0:
				omCountColumn1[0] = omCountColumn1[0] + 1;
			break;
			case 1:
				omCountColumn1[1] = omCountColumn1[1] + 1;
			break;
			case 2:
				omCountColumn1[2] = omCountColumn1[2] + 1;
			break;
			case 3:
				omCountColumn1[3] = omCountColumn1[3] + 1;
			break;
			case 4:
				omCountColumn1[4] = omCountColumn1[4] + 1;
			break;
			case 5:
				omCountColumn1[5] = omCountColumn1[5] + 1;
			break;
			case 6:
				omCountColumn1[6] = omCountColumn1[6] + 1;
			break;
			case 7:
				omCountColumn1[7] = omCountColumn1[7] + 1;
			break;
			case 8:
				omCountColumn1[8] = omCountColumn1[8] + 1;
			break;
			case 9:
				omCountColumn1[9] = omCountColumn1[9] + 1;
			break;
			case 10:
				omCountColumn1[10] = omCountColumn1[10] + 1;
			break;
			case 11:
				omCountColumn1[11] = omCountColumn1[11] + 1;
			break;
			case 12:
				omCountColumn1[12] = omCountColumn1[12] + 1;
			break;
			case 13:
				omCountColumn1[13] = omCountColumn1[13] + 1;
			break;
			case 14:
				omCountColumn1[14] = omCountColumn1[14] + 1;
			break;
			case 15:
				omCountColumn1[15] = omCountColumn1[15] + 1;
			break;
			case 16:
				omCountColumn1[16] = omCountColumn1[16] + 1;
			break;
			case 17:
				omCountColumn1[17] = omCountColumn1[17] + 1;
			break;
			case 18:
				omCountColumn1[18] = omCountColumn1[18] + 1;
			break;
			case 19:
				omCountColumn1[19] = omCountColumn1[19] + 1;
			break;
			case 20:
				omCountColumn1[20] = omCountColumn1[20] + 1;
			break;
			case 21:
				omCountColumn1[21] = omCountColumn1[21] + 1;
			break;
			case 22:
				omCountColumn1[22] = omCountColumn1[22] + 1;
			break;
			case 23:
				omCountColumn1[23] = omCountColumn1[23] + 1;
			break;
		}
		
	}

	if (prpDFlight != NULL)
	{
		int ilSwitch;

		if (strcmp(pcmSelect, "LAND/AIRB") == 0)
			ilSwitch = prpDFlight->Airb.GetHour();
		else if (strcmp(pcmSelect, "ONBL/OFBL") == 0)
			ilSwitch = prpDFlight->Ofbl.GetHour();
		else
			ilSwitch = prpDFlight->Stod.GetHour();


//		switch (prpDFlight->Stod.GetHour())
		switch (ilSwitch)
		{
			case 0:
				omCountColumn2[0] = omCountColumn2[0] + 1;
			break;
			case 1:
				omCountColumn2[1] = omCountColumn2[1] + 1;
			break;
			case 2:
				omCountColumn2[2] = omCountColumn2[2] + 1;
			break;
			case 3:
				omCountColumn2[3] = omCountColumn2[3] + 1;
			break;
			case 4:
				omCountColumn2[4] = omCountColumn2[4] + 1;
			break;
			case 5:
				omCountColumn2[5] = omCountColumn2[5] + 1;
			break;
			case 6:
				omCountColumn2[6] = omCountColumn2[6] + 1;
			break;
			case 7:
				omCountColumn2[7] = omCountColumn2[7] + 1;
			break;
			case 8:
				omCountColumn2[8] = omCountColumn2[8] + 1;
			break;
			case 9:
				omCountColumn2[9] = omCountColumn2[9] + 1;
			break;
			case 10:
				omCountColumn2[10] = omCountColumn2[10] + 1;
			break;
			case 11:
				omCountColumn2[11] = omCountColumn2[11] + 1;
			break;
			case 12:
				omCountColumn2[12] = omCountColumn2[12] + 1;
			break;
			case 13:
				omCountColumn2[13] = omCountColumn2[13] + 1;
			break;
			case 14:
				omCountColumn2[14] = omCountColumn2[14] + 1;
			break;
			case 15:
				omCountColumn2[15] = omCountColumn2[15] + 1;
			break;
			case 16:
				omCountColumn2[16] = omCountColumn2[16] + 1;
			break;
			case 17:
				omCountColumn2[17] = omCountColumn2[17] + 1;
			break;
			case 18:
				omCountColumn2[18] = omCountColumn2[18] + 1;
			break;
			case 19:
				omCountColumn2[19] = omCountColumn2[19] + 1;
			break;
			case 20:
				omCountColumn2[20] = omCountColumn2[20] + 1;
			break;
			case 21:
				omCountColumn2[21] = omCountColumn2[21] + 1;
			break;
			case 22:
				omCountColumn2[22] = omCountColumn2[22] + 1;
			break;
			case 23:
				omCountColumn2[23] = omCountColumn2[23] + 1;
			break;
		}
		
	}

}


CString AdditionalReport11TableViewer::GetTime(int ipIndex)
{

	if (ipIndex < omColumn0.GetSize())
		return omColumn0[ipIndex];
	else
		return "";

}


int AdditionalReport11TableViewer::GetData(int ipIndex)
{

	if (ipIndex < omCountColumn2.GetSize() && ipIndex < omCountColumn1.GetSize())
		return omCountColumn2[ipIndex] + omCountColumn1[ipIndex];
	else
		return 0;

}


// Setzen der Spaltenbreite in Anzahl Zeichen
void AdditionalReport11TableViewer::SetFieldLength(CString opCurrentColumns)
{

	CCS_TRY

	// -----------------
	// AdditionalReport1
	// -----------------
	if (opCurrentColumns == "Time")
	{
		omAnzChar.Add(10);
		return;
	}
	if (opCurrentColumns == "AFlights")
	{
		omAnzChar.Add(15);
		return;
	}
	if (opCurrentColumns == "DFlights")
	{
		omAnzChar.Add(15);
		return;
	}

	// falls kein if passt, ein default-Wert, sonst Array zu klein
	omAnzChar.Add(11);

	CCS_CATCH_ALL

}


CString AdditionalReport11TableViewer::GetHeaderContent(CString opCurrentColumns)
{

	CCS_TRY

	if (opCurrentColumns == "Time")
	{
		if ((strcmp(pcmFirstDate, pcmLastDate)) == 0)
			return GetString(IDS_STRING1625);
		else
			return GetString(IDS_STRING1626);
	}
	if (opCurrentColumns == "AFlights")
	{
		return GetString(IDS_STRING1559);
	}
	if (opCurrentColumns == "DFlights")
	{
		return GetString(IDS_STRING1560);
	}

	CCS_CATCH_ALL

	// wenn nix gefunden Leerstring zurueckgeben, sonst undefiniert
	return CString("");

}