// CicConfTableViewer.cpp : implementation file
// 
// Modification History: 


#include "stdafx.h"
#include "CicConfTableViewer.h"
#include "CcsGlobl.h"
#include <iomanip.h>
#include <iostream.h>
#include <fstream.h>
#include "BasicData.h"
#include "CedaBasicData.h"
#include "DataSet.h"
#include "CcaDiagram.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif




// Local function prototype
static void FlightTableCf(void *popInstance, int ipDDXType,
    void *vpDataPointer, CString &ropInstanceName);

/////////////////////////////////////////////////////////////////////////////
// CicConfTableViewer
//

#define IsBetween(val, start, end)  ((start) <= (val) && (val) <= (end))
#define IsOverlapped(start1, end1, start2, end2)    ((start1) <= (end2) && (start2) <= (end1))


CicConfTableViewer::CicConfTableViewer()
{
    pomTable = NULL;
	bmInit = false;
}


void CicConfTableViewer::UnRegister()
{
	ogDdx.UnRegister(this, NOTUSED);
 
}

CicConfTableViewer::~CicConfTableViewer()
{
	ogDdx.UnRegister(this, NOTUSED);
    DeleteAll();
}

void CicConfTableViewer::Attach(CCSTable *popTable)
{
    pomTable = popTable;
}

void CicConfTableViewer::ClearAll()
{
	bmInit = false;
	ogDdx.UnRegister(this, NOTUSED);
    DeleteAll();
    pomTable->ResetContent();
}



void CicConfTableViewer::ChangeViewTo(const char *pcpViewName)
{
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
	
	ogDdx.UnRegister(this, NOTUSED);

	ogDdx.Register(this, DIACCA_CHANGE, CString("DIACCA_CHANGE"), CString("DIACCA_CHANGE"), FlightTableCf);
	ogDdx.Register(this, CCA_KKEY_CHANGE,	CString("FLIGHTDIAVIEWER"), CString("Flight Changed"), FlightTableCf);


    pomTable->ResetContent();
    DeleteAll();    
	MakeLines();
   	UpdateDisplay();

	bmInit = true;
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));

}







/////////////////////////////////////////////////////////////////////////////
// CicConfTableViewer -- code specific to this class




bool CicConfTableViewer::IsPassFilter(DIACCADATA *prpCca)
{
	bool blRet = false;


	if(bgCcaDiaSeason && ogCcaDiaSingleDate != TIMENULL)
	{
		if(!IsOverlapped( ogCcaDiaSingleDate, ogCcaDiaSingleDate + CTimeSpan(0,24,0,0), prpCca->Ckbs, prpCca->Ckes  ))
			return false;
	}

	if(prpCca != NULL)
	{
		if(strcmp(prpCca->Ckic, "") != 0)
		{
			if(((strcmp(prpCca->Ctyp, "") == 0) &&
				(prpCca->IsChanged != DATA_DELETED) &&
				(prpCca->Stat[9] == '0') ) &&
			   ((prpCca->Stat[4] != '0') ||
				(prpCca->Stat[5] != '0') ||
				(prpCca->Stat[6] != '0') ||
				(prpCca->Stat[7] != '0') ||
				(prpCca->Stat[8] != '0') ))

			{
				blRet = true;
			}
		}
	}
	return blRet;
}


bool CicConfTableViewer::IsPassFilter(CCSPtrArray<DIACCADATA> &opCcas)
{
	if(opCcas.GetSize() == 0)
		return false;

	DIACCADATA *prpCca = &opCcas[0];

	bool blRet = false;
	if(prpCca != NULL)
	{
		blRet = IsPassFilter(prpCca);
	}
	return blRet;
}


void CicConfTableViewer::MakeLines()
{
	POSITION rlPos;
	CCSPtrArray<DIACCADATA> olCcas;

	if(bgCcaDiaSeason && ogCcaDiaSingleDate == TIMENULL)
	{
		for(int j = ogCcaDiaFlightData.omCcaData.omKKeyArray.GetSize() -1 ; j >= 0; j --)
		{
			olCcas.RemoveAll();
			if(ogCcaDiaFlightData.omCcaData.GetCcasByKKey(olCcas, ogCcaDiaFlightData.omCcaData.omKKeyArray[j]))
			{
				if(IsPassFilter(olCcas))
				{
					MakeLine(olCcas);
				}
			}
		}
	}
	else
	{
		for ( rlPos = ogCcaDiaFlightData.omCcaData.omUrnoMap.GetStartPosition(); rlPos != NULL; )
		{
			DIACCADATA *prlCca;
			long llUrno;
			ogCcaDiaFlightData.omCcaData.omUrnoMap.GetNextAssoc(rlPos, (void *&)llUrno, (void *&)prlCca);
			if(prlCca != NULL)
			{
				if(IsPassFilter(prlCca))
				{
					MakeLine(prlCca);
				}
			}
		}
	}
}


bool  CicConfTableViewer::MakeLine( CCSPtrArray<DIACCADATA> &opCcas)
{
	CICCONFTABLE_LINEDATA  rlLineData;
	MakeLineData(&rlLineData, opCcas);
	int ilLineNo = CreateLine(rlLineData);
	//InsertDisplayLine(ilLineNo);
	return true;
}



int  CicConfTableViewer::MakeLine(DIACCADATA *prpCca)
{
	int ilLineNo = -1;

	if(prpCca != NULL)
	{
		CICCONFTABLE_LINEDATA rlLineData;
		MakeLineData(&rlLineData, prpCca);
		ilLineNo = CreateLine(rlLineData);
	}
	return ilLineNo;
}

	
void CicConfTableViewer::MakeLineData(CICCONFTABLE_LINEDATA *prpLineData, DIACCADATA *prpCca)
{
	char buffer[65];

	if(prpCca != NULL)
	{
		prpLineData->Urno = prpCca->Urno;
		CCAFLIGHTDATA *prlFlight = ogCcaDiaFlightData.GetFlightByUrno(prpCca->Flnu);
		if(prlFlight != NULL)
		{

			prpLineData->FlightUrno = prlFlight->Urno;
			prpLineData->Flno = prlFlight->Flno;
			prpLineData->Sto  = prlFlight->Stod;
			prpLineData->Act3 = prlFlight->Act5;
			prpLineData->OrgDes = CString(prlFlight->Des3);
			prpLineData->Nose = prlFlight->Nose;
			prpLineData->Ftyp = prlFlight->Ftyp;

			prpLineData->Ckbs = prpCca->Ckbs;
			prpLineData->Ckes = prpCca->Ckes;
			prpLineData->Ckic = prpCca->Ckic;


			CString olGhpUrno;
			olGhpUrno.Format("%ld", prpCca->Ghpu);

			if(prpCca->Ghpu > 0)
			{
				prpLineData->Remark = ogBCD.GetField("GHP", "URNO", olGhpUrno, "PRNA");
				CString olCgru;
				olCgru = CString(prpCca->Cgru);
				CStringArray olStrArray;
				ExtractItemList(olCgru, &olStrArray, ';');
				int ilItemCount = olStrArray.GetSize();
				for(int i = 0; i < ilItemCount; i++)
				{
					prpLineData->CounterGroup += CString(" ") + ogBCD.GetField("GRN", "URNO", olStrArray[i], "GRPN");
				}

			}

			prpLineData->Error = false;

			if(prpCca->Ghpu != 0)
			{
				itoa(prpCca->Ghpu , buffer, 10);
				
				prpLineData->Remark = ogBCD.GetField("GHP", "URNO", CString(buffer), "PRSN");
			}


			if(prpCca->Ghsu != 0)
			{
				itoa(prpCca->Ghsu , buffer, 10);
				prpLineData->Remark += CString(" -> ") + ogBCD.GetField("GHS", "URNO", CString(buffer), "LKNM");
			}


			if(prpCca->Stat[4] != '0')
			{
				prpLineData->Reason =  GetString(IDS_STRING1640); //CString("Flugnummer hat sich ge�ndert");

			}

			if(prpCca->Stat[5] != '0')
			{
				prpLineData->Reason =  GetString(IDS_STRING1499); //CString("Cki ohne Bedarf");
				prpLineData->WoDem =  true;

			}
			if(prpCca->Stat[6] != '0')
			{
				prpLineData->Reason =  GetString(IDS_STRING1500); //CString("Regel hat sich ge�ndert");

			}
			if(prpCca->Stat[7] != '0')
			{
				prpLineData->Reason =  GetString(IDS_STRING1501); //CString("Flugzugtyp sich ge�ndert");

			}
			if(prpCca->Stat[8] != '0')
			{
				prpLineData->Reason =  GetString(IDS_STRING1502); //CString("STD hat sich ge�ndert");

			}
		}
	}
}


void CicConfTableViewer::MakeLineData(CICCONFTABLE_LINEDATA  *prpLineData, CCSPtrArray<DIACCADATA> &opCcas)
{
	char buffer[65];
	CCAFLIGHTDATA *prlFlight2;

	if(opCcas.GetSize() > 0)
	{
		DIACCADATA *prpCca = &opCcas[0];

		prpLineData->Urno = prpCca->Urno;
		CCAFLIGHTDATA *prlFlight = ogCcaDiaFlightData.GetFlightByUrno(prpCca->Flnu);
		if(prlFlight != NULL)
		{
		
			if(opCcas.GetSize() == 1)
				prlFlight2 = prlFlight;
			else
				prlFlight2 = ogCcaDiaFlightData.GetFlightByUrno(opCcas[opCcas.GetSize() - 1].Flnu);

			prpLineData->KKey = prpCca->KKey;
			prpLineData->FlightUrno = prlFlight->Urno;
			prpLineData->Flno = prlFlight->Flno;
			prpLineData->Sto  = prlFlight->Stod;
			prpLineData->Sto2  = prlFlight2->Stod;
			prpLineData->Act3 = prlFlight->Act5;
			prpLineData->OrgDes = CString(prlFlight->Des3);
			prpLineData->Nose = prlFlight->Nose;
			prpLineData->Ftyp = prlFlight->Ftyp;
			prpLineData->Freq  = prlFlight2->Freq;

			prpLineData->Ckbs = prpCca->Ckbs;
			prpLineData->Ckes = prpCca->Ckes;
			prpLineData->Ckic = prpCca->Ckic;


			CString olGhpUrno;
			olGhpUrno.Format("%ld", prpCca->Ghpu);

			if(prpCca->Ghpu > 0)
			{
				prpLineData->Remark = ogBCD.GetField("GHP", "URNO", olGhpUrno, "PRNA");
				CString olCgru;
				olCgru = CString(prpCca->Cgru);
				CStringArray olStrArray;
				ExtractItemList(olCgru, &olStrArray, ';');
				int ilItemCount = olStrArray.GetSize();
				for(int i = 0; i < ilItemCount; i++)
				{
					prpLineData->CounterGroup += CString(" ") + ogBCD.GetField("GRN", "URNO", olStrArray[i], "GRPN");
				}

			}

			prpLineData->Error = false;

			if(prpCca->Ghpu != 0)
			{
				itoa(prpCca->Ghpu , buffer, 10);
				
				prpLineData->Remark = ogBCD.GetField("GHP", "URNO", CString(buffer), "PRSN");
			}


			if(prpCca->Ghsu != 0)
			{
				itoa(prpCca->Ghsu , buffer, 10);
				prpLineData->Remark += CString(" -> ") + ogBCD.GetField("GHS", "URNO", CString(buffer), "LKNM");
			}


			if(prpCca->Stat[5] != '0')
			{
				prpLineData->Reason =  GetString(IDS_STRING1499); //CString("Cki ohne Bedarf");
				prpLineData->WoDem =  true; //CString("Cki ohne Bedarf");

			}
			if(prpCca->Stat[6] != '0')
			{
				prpLineData->Reason =  GetString(IDS_STRING1500); //CString("Regel hat sich ge�ndert");

			}
			if(prpCca->Stat[7] != '0')
			{
				prpLineData->Reason =  GetString(IDS_STRING1501); //CString("Flugzugtyp sich ge�ndert");

			}
			if(prpCca->Stat[8] != '0')
			{
				prpLineData->Reason =  GetString(IDS_STRING1502); //CString("STD hat sich ge�ndert");

			}
		}
	}
}



int CicConfTableViewer::CreateLine(CICCONFTABLE_LINEDATA &rpLine)
{
    int ilLineCount = omLines.GetSize();

    for (int ilLineno = ilLineCount; ilLineno > 0; ilLineno--)
        //if (CompareFlight(&rpLine, &omLines[ilLineno-1]) >= 0)
		if (CompareLines(&rpLine, &omLines[ilLineno-1]) >= 0)
            break;  // should be inserted after Lines[ilLineno-1]

    omLines.NewAt(ilLineno, rpLine);
	return ilLineno;
}


void CicConfTableViewer::DeleteLine(int ipLineno)
{
	omLines.DeleteAt(ipLineno);

	pomTable->DeleteTextLine(ipLineno);

}



bool CicConfTableViewer::FindLine(long lpUrno, int &rilLineno)
{
	rilLineno = -1;
    for (int i = omLines.GetSize() - 1; i >= 0; i--)
	{
	  if(omLines[i].Urno == lpUrno)
	  {
		rilLineno = i;
		return true;
	  }
	}
	return false;
}






/////////////////////////////////////////////////////////////////////////////
// CicConfTableViewer - CICCONFTABLE_LINEDATA array maintenance

void CicConfTableViewer::DeleteAll()
{
    omLines.DeleteAll();
}


/////////////////////////////////////////////////////////////////////////////
// CicConfTableViewer - display drawing routine



// UpdateDisplay: Load data selected by filter conditions to the display by using "omTable"
void CicConfTableViewer::UpdateDisplay()
{
	pomTable->ResetContent();

	DrawHeader();

	CICCONFTABLE_LINEDATA *prlLine;
    
	for (int ilLc = 0; ilLc < omLines.GetSize(); ilLc++)
	{
		int ilColumnNo = 0;
		prlLine = &omLines[ilLc];
		CCSPtrArray<TABLE_COLUMN> olColList;
	
		MakeColList(prlLine, olColList);

		pomTable->AddTextLine(olColList, (void*)(prlLine));
		olColList.DeleteAll();
	}


    pomTable->DisplayTable();
}



void CicConfTableViewer::InsertDisplayLine( int ipLineNo)
{
	if(!((ipLineNo >= 0) && (ipLineNo < omLines.GetSize())))
		return;
	CCSPtrArray<TABLE_COLUMN> olColList;
	MakeColList(&omLines[ipLineNo], olColList);
	pomTable->InsertTextLine(ipLineNo, olColList, &omLines[ipLineNo]);
	olColList.DeleteAll();
    pomTable->DisplayTable();
}






void CicConfTableViewer::DrawHeader()
{
	CCSPtrArray <TABLE_HEADER_COLUMN> omHeaderDataArray;

	pomTable->SetShowSelection(true);
	pomTable->ResetContent();

	TABLE_HEADER_COLUMN rlHeader;
	rlHeader.Font = &ogCourier_Bold_10;

	rlHeader.Alignment = COLALIGN_CENTER;
	rlHeader.Length = 67; 
	rlHeader.Text =  GetString(IDS_STRING347);//CString("Flugnummer");
	omHeaderDataArray.New(rlHeader);

	rlHeader.Alignment = COLALIGN_CENTER;
	rlHeader.Length = 40; 
	rlHeader.Text = GetString(IDS_STRING316);//CString("STD");
	omHeaderDataArray.New(rlHeader);

	if(bgCcaDiaSeason && ogCcaDiaSingleDate == TIMENULL)
	{
		rlHeader.Length = 200; 
	}
	else
	{
		rlHeader.Length = 65; 
	}

	rlHeader.Alignment = COLALIGN_CENTER;
	rlHeader.Text = GetString(IDS_STRING332);//CString("Datum");
	omHeaderDataArray.New(rlHeader);


	rlHeader.Alignment = COLALIGN_CENTER;
	rlHeader.Length = 30; 
	rlHeader.Text = GetString(IDS_STRING1447);//CString("ORG/DES");
	omHeaderDataArray.New(rlHeader);

	rlHeader.Alignment = COLALIGN_CENTER;
	rlHeader.Length = 30; 
	rlHeader.Text = GetString(IDS_STRING311);//CString("A/C");
	omHeaderDataArray.New(rlHeader);

	rlHeader.Alignment = COLALIGN_CENTER;
	rlHeader.Length = 30; 
	rlHeader.Text = GetString(IDS_STRING1508);//CString("Cki");
	omHeaderDataArray.New(rlHeader);


	rlHeader.Alignment = COLALIGN_CENTER;
	rlHeader.Length = 40; 
	rlHeader.Text = GetString(IDS_STRING1073);//CString("Von");
	omHeaderDataArray.New(rlHeader);


	rlHeader.Alignment = COLALIGN_CENTER;
	rlHeader.Length = 40; 
	rlHeader.Text = GetString(HD_DSR_AVTA_TIME);//CString("bis");
	omHeaderDataArray.New(rlHeader);

	rlHeader.Alignment = COLALIGN_CENTER;
	rlHeader.Length = 50; 
	rlHeader.Text = GetString(IDS_STRING1503);//CString("Conf");
	omHeaderDataArray.New(rlHeader);

	rlHeader.Alignment = COLALIGN_CENTER;
	rlHeader.Length = 40; 
	rlHeader.Text = GetString(IDS_STRING1504);//CString("Ftyp")
	omHeaderDataArray.New(rlHeader);

	rlHeader.Alignment = COLALIGN_CENTER;
	rlHeader.Length = 200; 
	rlHeader.Text = GetString(IDS_STRING1505);//"Grund";
	omHeaderDataArray.New(rlHeader);

	rlHeader.Alignment = COLALIGN_CENTER;
	rlHeader.Length = 300; 
	rlHeader.Text = GetString(IDS_STRING1506);//"Regel";
	omHeaderDataArray.New(rlHeader);

	rlHeader.Alignment = COLALIGN_CENTER;
	rlHeader.Length = 300; 
	rlHeader.Text = GetString(IDS_STRING1507);//"Counter Grp";
	omHeaderDataArray.New(rlHeader);


	pomTable->SetHeaderFields(omHeaderDataArray);
	omHeaderDataArray.DeleteAll();

	pomTable->SetDefaultSeparator();

}


void CicConfTableViewer::GetCount(int& ipConf, int& ipWoDem)
{
	CICCONFTABLE_LINEDATA *prlLine;
	for (int ilLc = 0; ilLc < omLines.GetSize(); ilLc++)
	{
		prlLine = &omLines[ilLc];
		if (prlLine->WoDem)
			ipWoDem++;
	}
	ipConf = omLines.GetSize();
}


void CicConfTableViewer::MakeColList(CICCONFTABLE_LINEDATA *prlLine, CCSPtrArray<TABLE_COLUMN> &olColList)
{
		TABLE_COLUMN rlColumnData;

		if(prlLine->Error == true)
		{
			rlColumnData.TextColor = COLORREF(RED);
		}

		if(prlLine->WoDem)
			rlColumnData.BkColor = COLORREF(ORANGE);
		else
			rlColumnData.BkColor = COLORREF(WHITE);


		rlColumnData.VerticalSeparator = SEPA_NONE;
		rlColumnData.SeparatorType = SEPA_NONE;
		rlColumnData.Font = &ogCourier_Regular_9;
	
		rlColumnData.Alignment = COLALIGN_LEFT;
		rlColumnData.Text = prlLine->Flno;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		rlColumnData.Alignment = COLALIGN_LEFT;
		rlColumnData.Text = prlLine->Sto.Format("%H:%M");
		olColList.NewAt(olColList.GetSize(), rlColumnData);


		if(bgCcaDiaSeason && ogCcaDiaSingleDate == TIMENULL)
		{
			if(prlLine->Sto == prlLine->Sto2)
				rlColumnData.Text = prlLine->Sto.Format("%d.%m.%Y");
			else
			{
				char buffer[64];
				itoa(prlLine->Freq, buffer, 10);
				rlColumnData.Text = prlLine->Sto.Format("%d.%m.%Y - ") + prlLine->Sto2.Format("%d.%m.%Y  ") + CString(buffer);
			}
		}
		else
		{
			rlColumnData.Text = prlLine->Sto.Format("%d.%m.%y");
		}
		rlColumnData.Alignment = COLALIGN_LEFT;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		rlColumnData.Alignment = COLALIGN_LEFT;
		rlColumnData.Text = prlLine->OrgDes;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		rlColumnData.Alignment = COLALIGN_LEFT;
		rlColumnData.Text = prlLine->Act3;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		rlColumnData.Alignment = COLALIGN_LEFT;
		rlColumnData.Text = prlLine->Ckic;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		rlColumnData.Alignment = COLALIGN_LEFT;
		rlColumnData.Text = prlLine->Ckbs.Format("%H:%M");
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		rlColumnData.Alignment = COLALIGN_LEFT;
		rlColumnData.Text = prlLine->Ckes.Format("%H:%M");
		olColList.NewAt(olColList.GetSize(), rlColumnData);
//MWO
		rlColumnData.Alignment = COLALIGN_LEFT;
		rlColumnData.Text = prlLine->Nose;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		rlColumnData.Alignment = COLALIGN_LEFT;
		rlColumnData.Text = prlLine->Ftyp;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

//end MWO
		rlColumnData.Alignment = COLALIGN_LEFT;
		rlColumnData.Text = prlLine->Reason;
		olColList.NewAt(olColList.GetSize(), rlColumnData);


		rlColumnData.Alignment = COLALIGN_LEFT;
		rlColumnData.Text = prlLine->Remark;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		rlColumnData.Alignment = COLALIGN_LEFT;
		rlColumnData.Text = prlLine->CounterGroup;
		olColList.NewAt(olColList.GetSize(), rlColumnData);


}



static void FlightTableCf(void *popInstance, int ipDDXType,
    void *vpDataPointer, CString &ropInstanceName)
{
    CicConfTableViewer *polViewer = (CicConfTableViewer *)popInstance;

	if(ipDDXType == DIACCA_CHANGE)
		polViewer->ProcessCcaChange((long *)vpDataPointer);
	if(ipDDXType == CCA_KKEY_CHANGE)
		polViewer->ProcessKKeyChange((long *)vpDataPointer);

	if(ipDDXType == DIACCA_CHANGE || ipDDXType == CCA_KKEY_CHANGE)
	{
		if (pogCcaDiagram)
			pogCcaDiagram->UpdateConfButton();
	}

}

void CicConfTableViewer::ProcessKKeyChange(long *prpCcaUrno)
{
	DeleteLine(*prpCcaUrno);

	CCSPtrArray<DIACCADATA> olCcas;	
	
	if(ogCcaDiaFlightData.omCcaData.GetCcasByKKey(olCcas, *prpCcaUrno))
	{
		if(IsPassFilter(olCcas))
		{
			int ilLine = MakeLine(olCcas);
			if(ilLine >= 0)
				InsertDisplayLine(ilLine);
		}
	}
	olCcas.RemoveAll();
}



void CicConfTableViewer::ProcessCcaChange(long *plpCcaUrno)
{
	DeleteLine(*plpCcaUrno);

	
	DIACCADATA *prlCca =ogCcaDiaFlightData.omCcaData.GetCcaByUrno(*plpCcaUrno);
	if(prlCca == NULL)
	{
		return;
	}

	if(IsPassFilter(prlCca))
	{
		int ilLine = MakeLine(prlCca);
		if(ilLine >= 0)
			InsertDisplayLine(ilLine);
	}
}





/*
int CicConfTableViewer::CompareFlight(CICCONFTABLE_LINEDATA *prpLine1, CICCONFTABLE_LINEDATA *prpLine2)
{
		return (prpLine1->Sto == prpLine2->Sto)? 0:	(prpLine1->Sto > prpLine2->Sto)? 1: -1;
}
*/

int CicConfTableViewer::CompareLines(CICCONFTABLE_LINEDATA *prpLine1, CICCONFTABLE_LINEDATA *prpLine2)
{

	if(bgCcaDiaSeason && ogCcaDiaSingleDate == TIMENULL)
	{
		if(prpLine1->Sto.Format("%H:M") == prpLine2->Sto.Format("%H:M"))
		{
			return prpLine1->Flno > prpLine2->Flno ? 1: -1;

		}
		else
		{
			return prpLine1->Sto.Format("%H:M") > prpLine2->Sto.Format("%H:M") ? 1: -1;
		}
	}
	else
	{
		return (prpLine1->Sto == prpLine2->Sto)? 0:	
		       (prpLine1->Sto > prpLine2->Sto)? 1: -1;
	}
}





bool CicConfTableViewer::DeleteLine(long lpUrno)
{
    for (int i = omLines.GetSize() - 1; i >= 0; i--)
	{
	  if((omLines[i].Urno == lpUrno) || (omLines[i].KKey == lpUrno))
	  {
		omLines.DeleteAt(i);
		pomTable->DeleteTextLine(i);
	  }
	}
	return true;
}



