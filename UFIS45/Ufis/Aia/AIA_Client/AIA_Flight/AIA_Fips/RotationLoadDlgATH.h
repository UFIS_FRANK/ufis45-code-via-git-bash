#if !defined(AFX_ROTATIONLOADDLGATH_H__D03A7EF1_D3EE_11D3_806D_00008638F9E1__INCLUDED_)
#define AFX_ROTATIONLOADDLGATH_H__D03A7EF1_D3EE_11D3_806D_00008638F9E1__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// RotationLoadDlgATH.h : header file
//
#include "resrc1.h"
#include "CCSEdit.h"
#include "RotationDlgCedaFlightData.h"

/////////////////////////////////////////////////////////////////////////////
// RotationLoadDlgATH dialog

class RotationLoadDlgATH : public CDialog
{
// Construction
public:
	RotationLoadDlgATH(CWnd* pParent = NULL, ROTATIONDLGFLIGHTDATA *prpFlight = NULL);   // standard constructor

	ROTATIONDLGFLIGHTDATA *prmFlight;


	bool bmAutoCalcPax;
	bool bmAutoCalcLoad;


// Dialog Data
	//{{AFX_DATA(RotationLoadDlgATH)
	enum { IDD = IDD_ROTATION_LOAD_ATH };
	CCSEdit	m_CE_MAIL;
	CCSEdit	m_CE_CGOT;
	CCSEdit	m_CE_BAGW;
	CCSEdit	m_CE_BAGN;
	CCSEdit	m_CE_PAXT;
	CCSEdit	m_CE_PAXI;
	CCSEdit	m_CE_PAXF;
	CCSEdit	m_CE_PAX3;
	CCSEdit	m_CE_PAX2;
	CCSEdit	m_CE_PAX1;
	CCSEdit	m_CE_PAX_INF;
	CCSEdit	m_CE_PAX_ID;
	CCSEdit	m_CE_LDM_PAX1;
	CCSEdit	m_CE_LDM_PAX2;
	CCSEdit	m_CE_LDM_PAX3;
	CCSEdit	m_CE_LDM_PAXT;
	CCSEdit	m_CE_PTM_PAXF;
	CCSEdit	m_CE_MVT_PAXI;

	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(RotationLoadDlgATH)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	BOOL SaveInApxTab(CString &ropId, CString &ropData); 

	// Generated message map functions
	//{{AFX_MSG(RotationLoadDlgATH)
	virtual void OnOK();
	virtual BOOL OnInitDialog();
	afx_msg LONG OnEditKillfocus( UINT wParam, LPARAM lParam);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()  
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_ROTATIONLOADDLGATH_H__D03A7EF1_D3EE_11D3_806D_00008638F9E1__INCLUDED_)
