#ifndef __SAISONFLUGPLANTABLEVIEWER_H__
#define __SAISONFLUGPLANTABLEVIEWER_H__

#include "stdafx.h"
#include "RotationDlgCedaFlightData.h"
#include "CCSTable.h"
#include "CViewer.h"
#include "CCSPrint.h"


struct SAISONFLUGPLANTABLE_LINEDATA
{
	long		Urno; 	// Eindeutige Datensatz-Nr.
	CString		Cntr;	// Anzahl der Bewegungen
	CString 	Alc2; 	// Zeitrahmen Abflug
	CString 	Alc3; 	// Zeitrahmen Abflug

};

/////////////////////////////////////////////////////////////////////////////
// SaisonflugplanTableViewer

class SaisonflugplanTableViewer : public CViewer
{
// Constructions
public:
    SaisonflugplanTableViewer(CCSPtrArray<ROTATIONDLGFLIGHTDATA> *popData, char *pcpInfo = NULL);
    ~SaisonflugplanTableViewer();

    void Attach(CCSTable *popAttachWnd);
    virtual void ChangeViewTo(const char *pcpViewName);
// Internal data processing routines
private:
    void MakeLines(void);
	void MakeLine(CString Alcold, int iCntr);
// Operations
public:
	void DeleteAll(void);
	void CreateLine(SAISONFLUGPLANTABLE_LINEDATA *prpSaisonflugplan);
	void DeleteLine(int ipLineno);
	void PrintPlanToFile(char *pcpDefPath);
// Window refreshing routines
public:
	void UpdateDisplay();
// Attributes used for filtering condition
private:
	bool bmIsFromSearch;
// Attributes
private:
    CCSTable *pomSaisonflugplanTable;
	CCSPtrArray<ROTATIONDLGFLIGHTDATA> *pomData;
public:
    CCSPtrArray<SAISONFLUGPLANTABLE_LINEDATA> omLines;
///////
	CCSPtrArray<TABLE_HEADER_COLUMN> omHeaderDataArray;
//Print 
	void PrintTableView();
	bool PrintTableLine(SAISONFLUGPLANTABLE_LINEDATA *prpLine,bool bpLastLine);
	bool PrintTableHeader();
	CCSPrint *pomPrint;
	char *pcmInfo;
	CString omTableName;
	CString omFooterName;

};

#endif //__SAISONFLUGPLANTABLEVIEWER_H__
