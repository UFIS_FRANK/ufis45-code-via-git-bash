// ChangeRegistraion.cpp : implementation file
//

#include "stdafx.h"
#include "fpms.h"
#include "resrc1.h"
#include "resource.h"
#include "ChangeRegistraion.h"
#include "RotationRegnDlg.h"
#include "CedaBasicData.h"
#include "BasicData.h"
#include "CCSGlobl.h"
#include "RotationDlgCedaFlightData.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CChangeRegistraion dialog


CChangeRegistraion::CChangeRegistraion(CWnd* pParent /*=NULL*/)
	: CDialog(CChangeRegistraion::IDD, pParent)
{
	//{{AFX_DATA_INIT(CChangeRegistraion)
	m_Act3 = _T("");
	m_Act5 = _T("");
	m_Regn = _T("");
	m_ActualDay = 0;
	m_Numbers = _T("");
	m_AFlights = _T("");
	//}}AFX_DATA_INIT
}


void CChangeRegistraion::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CChangeRegistraion)
	DDX_Control(pDX, IDC_EDIT_DFLIGHTS, m_CE_DFlights);
	DDX_Control(pDX, IDC_EDIT_AFLIGHTS, m_CE_AFlights);
	DDX_Control(pDX, IDC_EDIT_NUMBERS, m_CE_Numbers);
	DDX_Control(pDX, IDC_DATETIMEPICKER1, m_CE_ActualDay);
	DDX_Control(pDX, IDC_REGN, m_CE_Regn);
	DDX_Control(pDX, IDC_ACT5, m_CE_Act5);
	DDX_Control(pDX, IDC_ACT3, m_CE_Act3);
	DDX_Text(pDX, IDC_ACT3, m_Act3);
	DDX_Text(pDX, IDC_ACT5, m_Act5);
	DDX_Text(pDX, IDC_REGN, m_Regn);
	DDX_DateTimeCtrl(pDX, IDC_DATETIMEPICKER1, m_ActualDay);
	DDX_Text(pDX, IDC_EDIT_NUMBERS, m_Numbers);
	DDX_Text(pDX, IDC_EDIT_AFLIGHTS, m_AFlights);
	DDX_Text(pDX, IDC_EDIT_DFLIGHTS, m_DFlights);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CChangeRegistraion, CDialog)
	//{{AFX_MSG_MAP(CChangeRegistraion)
	ON_MESSAGE(WM_EDIT_KILLFOCUS, OnEditKillfocus)
	ON_MESSAGE(WM_EDIT_CHANGED, OnEditChanged)
	ON_MESSAGE(WM_MARK_BAR, OnMark)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CChangeRegistraion message handlers

LONG CChangeRegistraion::OnEditKillfocus( UINT wParam, LPARAM lParam)
{
	CCSEDITNOTIFY *prlNotify = (CCSEDITNOTIFY*)lParam;

	if(((UINT)m_CE_Regn.imID == wParam) && (!prlNotify->Text.IsEmpty()))
	{
		CString olAct3;
		CString olAct5;
		prlNotify->Status = true;

		
		CString olWhere;
		prlNotify->Text.TrimLeft();
		if(!prlNotify->Text.IsEmpty())
		{
			olWhere.Format("WHERE REGN = '%s'", prlNotify->Text);
			ogBCD.Read( "ACR", olWhere);
		}
		
		if(prlNotify->Status = ogBCD.GetFields("ACR", "REGN", prlNotify->Text, "ACT3", "ACT5", olAct3, olAct5))
		{
			m_CE_Act3.SetWindowText(olAct3);
			m_CE_Act5.SetWindowText(olAct5);
		}
		else
		{
			m_CE_Act3.SetWindowText(olAct3);
			m_CE_Act5.SetWindowText(olAct5);

			RotationRegnDlg olDlg(this, prlNotify->Text, olAct3, olAct5);
			if(olDlg.DoModal() == IDOK)
			{
				m_CE_Act5.SetWindowText(olDlg.m_Act5);
				m_CE_Act3.SetWindowText(olDlg.m_Act3);
				prlNotify->Status = true;
			}
			else
				m_CE_Regn.SetInitText("", true);

		}
		return 0L;
	}

	return 0L;
}

LONG CChangeRegistraion::OnEditChanged( UINT wParam, LPARAM lParam)
{
	return 0L;
}

void CChangeRegistraion::OnOK() 
{
	m_CE_ActualDay.GetTime(m_ActualDay);
	m_CE_Numbers.GetWindowText(m_Numbers);
	m_CE_Act3.GetWindowText(m_Act3);
	m_CE_Act5.GetWindowText(m_Act5);
	m_CE_Regn.GetWindowText(m_Regn);
	m_AFlights = "";
	m_CE_AFlights.SetWindowText(m_AFlights);
	m_DFlights = "";
	m_CE_DFlights.SetWindowText(m_DFlights);

	CString olNumbers;
	for (int z = 0; z < m_CE_Numbers.GetLineCount(); z++)
	{
		char olTest[1024];
		CString tmp;
		m_CE_Numbers.GetLine( z, olTest);

		if (strlen(olTest) > 0)
		{
			for (int z1 = 0; olTest[z1] != '\0'; ++z1)
			{
				if (olTest[z1] >= 0 && olTest[z1] <= 31)
					olTest[z1] = ' ';
			}
		}
		tmp = olTest;
		tmp.TrimRight();
		olNumbers += tmp + " ";
	}

	CStringArray	olFlnoList;
	CStringArray	olFlnoSplit;
	CString			olFlnoDay;
	CStringArray	olFlnoCtimeBeg;
	CStringArray	olFlnoCtimeEnd;
	CStringArray	olFlnoNumber2;
	CStringArray	olFlnoNumber3;

//	int ilNoFlights = ExtractItemList(m_Numbers, &olFlnoList, ' ');
	int ilNoFlights = ExtractItemList(olNumbers, &olFlnoList, ' ');
	int ilActPos = 0;

	// Flugnummer interpretieren (todo: 2l ps,3l pst -> aber nicht gleiche airline == nicht eindeutig)
	for (int i = 0; i < ilNoFlights; i++)
	{
		CString olFlno;
		CString olDay;
		olFlnoDay = olFlnoList[i];
		if (!olFlnoDay.IsEmpty())
		{
			ilActPos++;
			int ilSplit = ExtractItemList(olFlnoDay, &olFlnoSplit, '/');
			if (olFlnoSplit.GetSize() == 2)
			{
				olFlno = olFlnoSplit[0];
				olDay  = olFlnoSplit[1];
				olDay.Remove(' ');
				olFlnoCtimeBeg.Add(olDay);
			}
			else
			{
				char olMessage[256];
				sprintf(olMessage, "FlugSyntax %d ist nicht g�ltig",ilActPos);
				MessageBox(olMessage, GetString(ST_FEHLER),MB_ICONERROR);
				break;
			}

			olFlno.Remove(' ');
			CString olFlno2;
			CString olFlno3;
			CString olAlc3;
			CString olSelHeader;
			CString olAlc2 = olFlno.Left(2);

			bool blAlc3 = false;
			bool blAlc2 = false;

			blAlc2 = ogBCD.GetField("ALT", "ALC2", olAlc2, "ALC3", olAlc3);
			if (!blAlc2)
			{
				olAlc3 = olFlno.Left(3);
				blAlc3 = ogBCD.GetField("ALT", "ALC3", olAlc3, "ALC2", olAlc2);
			}

			if (blAlc2)
			{
				CString olFlnr = olFlno.Mid(2, olFlno.GetLength()-2 );

				for(int ilLc = 0; ilLc < olFlnr.GetLength(); ilLc++)
				{
					char clTmp = olFlnr[ilLc];
					if(! isdigit(clTmp) )
						olFlnr.SetAt(ilLc, ' ');
				}
				olFlnr.Remove(' ');

				olSelHeader = olAlc2 + "(" + olAlc3 + ") " + olFlnr;

				olFlno2 = olAlc2 + olFlnr;
				olFlno2.Insert(2, ' ');

				olFlno3 = olAlc3 + olFlnr;
			}
			else
			{
				if (blAlc3)
				{
					CString olFlnr = olFlno.Mid(3, olFlno.GetLength()-3 );

					for(int ilLc = 0; ilLc < olFlnr.GetLength(); ilLc++)
					{
						char clTmp = olFlnr[ilLc];
						if(! isdigit(clTmp) )
							olFlnr.SetAt(ilLc, ' ');
					}
					olFlnr.Remove(' ');


					olSelHeader = olAlc3 + "(" + olAlc2 + ") " + olFlnr;

					olFlno3 = olAlc3 + olFlnr;
					olFlno2 = olAlc2 + " " + olFlnr;
				}
			}

			if (!blAlc3 && !blAlc2)
			{
				char olInfo[256];
				sprintf(olInfo, "Airline not in Basic Data (%s, %s)", olAlc2, olAlc3);
				MessageBox(olInfo, GetString(ST_FEHLER), MB_ICONERROR);
				break;
			}
			else
			{
				olFlnoNumber2.Add(olFlno2);
				olFlnoNumber3.Add(olFlno3);
			}
		}
	}


	//Datum interpretieren
	int ilStartDay   = m_ActualDay.GetDay();
	int ilStartMonth = m_ActualDay.GetMonth();
	int ilStartYear = m_ActualDay.GetYear();
	CTime olStartDate (ilStartYear, ilStartMonth, ilStartDay, 0, 0, 0);

	int ilFlnoPre = 99;

	for (i = 0; i < olFlnoCtimeBeg.GetSize(); i++)
	{
		int ilFlnoDay = atoi(olFlnoCtimeBeg.GetAt(i));
		if (i == 0)
		{
		}
		else
		{
			if (ilFlnoDay < ilFlnoPre)
				ilStartMonth++;

			if (ilStartMonth == 13)
			{
				ilStartMonth = 1;
				ilStartYear++;
			}
		}

		CTime olFlnoDate (ilStartYear, ilStartMonth, ilFlnoDay, 0, 0, 0);
		ilFlnoPre = ilFlnoDay;

	

//		if (olFlnoDate >= olStartDate)
//		{
			CString oMinStr = olFlnoDate.Format( "%Y%m%d000000" );
			CString oMaxStr = olFlnoDate.Format( "%Y%m%d235959" );
			MakeUtc( oMinStr, oMaxStr );
			olFlnoCtimeBeg.SetAt(i, oMinStr);
			olFlnoCtimeEnd.Add(oMaxStr);
//		}

	}

	char olCheck[2048];
	char olCheck1[2048];
	strcpy(olCheck1, " ");
	for (i = 0; i < olFlnoCtimeBeg.GetSize(); i++)
	{
		sprintf(olCheck, "%s%s%s%s%s%s", olFlnoNumber2.GetAt(i), " / ", olFlnoNumber3.GetAt(i), " / ",  olFlnoCtimeBeg.GetAt(i), "\n");
		strcat(olCheck1,olCheck);
	}
	MessageBox(olCheck1, GetString(ST_FEHLER), MB_ICONERROR);


	//check if flight exist and get adid
	CStringArray	olAdid;
	CUIntArray	    olUrno;
	RotationDlgCedaFlightData olAllData;
	RotationDlgCedaFlightData olData;
	for (i = 0; i < olFlnoCtimeBeg.GetSize(); i++)
	{

		CString olWhereSto;
		CString olWhereTif;
		CString olWherePlusFlno;
		CString olWherePlusAct;

		olWhereSto.Format(
			"WHERE ((STOA BETWEEN '%s' AND '%s' AND DES3='%s' ) OR ( STOD BETWEEN '%s' AND '%s' AND ORG3='%s'))",
					olFlnoCtimeBeg.GetAt(i), olFlnoCtimeEnd.GetAt(i), pcgHome,
					olFlnoCtimeBeg.GetAt(i), olFlnoCtimeEnd.GetAt(i), pcgHome );

 		olWhereTif.Format( 
			"OR ((TIFA BETWEEN '%s' AND '%s' AND DES3='%s' ) OR ( TIFD BETWEEN '%s' AND '%s' AND ORG3='%s'))",
					olFlnoCtimeBeg.GetAt(i), olFlnoCtimeEnd.GetAt(i), pcgHome,
					olFlnoCtimeBeg.GetAt(i), olFlnoCtimeEnd.GetAt(i), pcgHome );

//		olWherePlusFlno.Format(" AND (FLNO = '%s' OR FLNO = '%s') ", olFlnoNumber2.GetAt(i), olFlnoNumber3.GetAt(i) );
		olWherePlusFlno.Format(" AND (FLNO LIKE '%s%%' OR FLNO LIKE '%s%%') ", olFlnoNumber2.GetAt(i), olFlnoNumber3.GetAt(i) );

		olWherePlusAct.Format(" AND (ACT3='%s' OR ACT5='%s') ", m_Act3, m_Act5 );

		CString olSelection = olWhereSto  + olWherePlusFlno;

		char olSel[1024];
		strcpy(olSel, olSelection);


//		RotationDlgCedaFlightData olData;
		olData.omData.RemoveAll();
		olData.ReadFlights(olSel);

		bool blTwoFlights = false;
		for (int j = 0; j < olData.omData.GetSize(); j++)
		{
			ROTATIONDLGFLIGHTDATA *prpFlight = &olData.omData[j];
			if (prpFlight)
			{
				if ( (strcmp(prpFlight->Adid, "A") == 0) || (strcmp(prpFlight->Adid, "D") == 0) )
				{
					if (blTwoFlights)
					{
						char olInfo[256];
						sprintf(olInfo, "More than one Flight found ( %s (%s) )", olFlnoNumber2.GetAt(j), olFlnoNumber3.GetAt(j));
						MessageBox(olInfo, GetString(ST_FEHLER), MB_ICONERROR);

					}
					else
					{
						olAdid.Add(prpFlight->Adid);
						olUrno.Add(prpFlight->Urno);
						blTwoFlights = true;
						olAllData.AddFlightInternal(prpFlight);
					}
				}
			}
			else
			{
				char olInfo[256];
				sprintf(olInfo, "Flight doesn't exits ( %s (%s) )", olFlnoNumber2.GetAt(j), olFlnoNumber3.GetAt(j));
				MessageBox(olInfo, GetString(ST_FEHLER), MB_ICONERROR);
			}

			if (!blTwoFlights)
				olAdid.Add("?");
		}


	}
/*
//	check start/landing order
	CString olNextIs (" ");
	if (olAdid.GetAt(0) == "A")
	{
		olNextIs = "D";
	}
	else if (olAdid.GetAt(0) == "D")
	{
		olNextIs = "A";
	}
	else
	{
		char olInfo[256];
		sprintf(olInfo, "First Flight is %s )", olAdid.GetAt(0));
		MessageBox(olInfo, GetString(ST_FEHLER), MB_ICONERROR);
	}

	int ilA = 1;
	int ilD = 0;
	CString olActAdid = olAdid.GetAt(0);

	for (i = 2; i < olAdid.GetSize(); i+=2)
	{
		if (olAdid.GetAt(i) != olActAdid)
		{
			char olInfo[256];
			sprintf(olInfo, "Start/Landing Order: First Flight is %s and %d Flight is %s)", olAdid.GetAt(0), i, olAdid.GetAt(i));
			MessageBox(olInfo, GetString(ST_FEHLER), MB_ICONERROR);
		}
		else
			ilA++;
	}

	olActAdid = olNextIs;
	for (i = 1; i < olAdid.GetSize(); i+=2)
	{
		if (olAdid.GetAt(i) != olActAdid)
		{
			char olInfo[256];
			sprintf(olInfo, "Start/Landing Order: First Flight is %s and %d Flight is %s)", olAdid.GetAt(0), i, olAdid.GetAt(i));
			MessageBox(olInfo, GetString(ST_FEHLER), MB_ICONERROR);
		}
		else
			ilD++;
	}

	if (ilA != ilD)
	{
		char olInfo[256];
		sprintf(olInfo, "Start/Landing Order: %s ,%d - %s ,%d)", olAdid.GetAt(0), ilA, olAdid.GetAt(1), ilD);
		MessageBox(olInfo, GetString(ST_FEHLER), MB_ICONERROR);
	}
*/


	CString olFieldList;
	CString olDataList;
	CString olField;
	CString olDataStr;
	CString olSelection;
	CString olTmp;

	int ilCount2 = 0;

	olField = "REGN,ACT3,ACT5";
	olDataStr  = m_Regn + "," + m_Act3 + "," + m_Act5;
	for(i = 0; i < olUrno.GetSize(); i++)
	{
		ROTATIONDLGFLIGHTDATA* prlFlight = &olAllData.omData[i];

		ilCount2++;


//		GetDataList(prlFlight, olField, olData);

		if(!olField.IsEmpty())
		{
			olDataList = olDataList + olDataStr + CString("\n");
			olFieldList = olFieldList + olField + CString("\n");
			olTmp.Format("WHERE URNO = %ld", olUrno.GetAt(i));
			olSelection = olSelection + olTmp + CString("\n");
			 
			CString olAfl;

			if (strcmp(prlFlight->Adid, "A") == 0)
			{
				//must be convert to local(todo)
				CString olDateStr = prlFlight->Stoa.Format("%d.%m.%Y");
				CString olTimeStr = prlFlight->Stoa.Format("%H:%M");
				olAfl.Format("%s \t%s / %s", prlFlight->Flno, olDateStr, olTimeStr);
				m_AFlights += olAfl + "\r\n";
			}
			else
			{
				//must be convert to local(todo)
				CString olDateStr = prlFlight->Stod.Format("%d.%m.%Y");
				CString olTimeStr = prlFlight->Stod.Format("%H:%M");
				olAfl.Format("%s \t%s / %s", prlFlight->Flno, olDateStr, olTimeStr);
				m_DFlights += olAfl + "\r\n";
			}
		}

		if((olDataList.GetLength() + olSelection.GetLength() + olFieldList.GetLength()) >= 4000  || ilCount2 >= 14)
		{
			olDataList = olDataList.Left(olDataList.GetLength() -1 );
			olFieldList = olFieldList.Left(olFieldList.GetLength() -1 );
			olSelection = olSelection.Left(olSelection.GetLength() -1 );
			ogRotationDlgFlights.UpdateFlight(olSelection, olFieldList, olDataList);	
			
			olDataList = "";
			olFieldList = "";
			olSelection = "";
			ilCount2 = 0;

		}
		ogBcHandle.GetBc();
	}


	if(!olFieldList.IsEmpty() && !olDataList.IsEmpty())
	{
		olDataList = olDataList.Left(olDataList.GetLength() -1 );
		olFieldList = olFieldList.Left(olFieldList.GetLength() -1 );
		olSelection = olSelection.Left(olSelection.GetLength() -1 );
		ogRotationDlgFlights.UpdateFlight(olSelection, olFieldList, olDataList);	
	}

	m_CE_AFlights.SetWindowText(m_AFlights);
	m_CE_DFlights.SetWindowText(m_DFlights);


	CTime* stoa =&olAllData.omData[0].Stoa;
	CTime* stod =&olAllData.omData[0].Stod;

	ROTATIONDLGFLIGHTDATA* prlFlight = &olAllData.omData[0];
	CString olDateStr = prlFlight->Stoa.Format("%d.%m.%Y");
	CString olTimeStr = prlFlight->Stoa.Format("%H:%M");

	CString* olAfl = new CString;
	olAfl->Format("Finished Reload Data: %s - %s", olDateStr, olTimeStr);

	LPARAM llLParam = MAKELPARAM(stoa,stod);
    this->SendMessage(WM_MARK_BAR, (WPARAM)olAfl, llLParam);

	olData.omData.RemoveAll();
	olAllData.omData.RemoveAll();

}

BOOL CChangeRegistraion::OnInitDialog()  
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here 
	m_CE_ActualDay.SetTime(&CTime::GetCurrentTime());

//	m_Regn = "B2555";
//	m_CE_Regn.SetInitText(m_Regn);   
	m_CE_Act3.SetReadOnly(true);
	m_CE_Act5.SetReadOnly(true);
//	m_CE_Regn.SetReadOnly(true);  
//	m_CE_Regn.SetSecState('0');  
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CChangeRegistraion::MakeUtc( CString& ropMinStr, CString& ropMaxStr )
{
	if(bgDailyLocal)
	{
		CTime olUtcMinDate = DBStringToDateTime(ropMinStr);
		CTime olUtcMaxDate = DBStringToDateTime(ropMaxStr);

		ogBasicData.LocalToUtc(olUtcMinDate);
		ogBasicData.LocalToUtc(olUtcMaxDate);

		ropMinStr = olUtcMinDate.Format( "%Y%m%d%H%M%S" );
		ropMaxStr = olUtcMaxDate.Format( "%Y%m%d%H%M%S" );
	}
}

LONG CChangeRegistraion::OnMark( UINT wParam, LPARAM lParam)
{

//	long llA = wParam;

//	CTime* olStoa = LOWORD(lParam);
//	CTime olStod = HIWORD(lParam);

	CString* olShow = (CString*) wParam;
	MessageBox(*olShow, GetString(ST_FEHLER), MB_ICONERROR);

	delete (CString*) wParam;

	return 0L;
}

