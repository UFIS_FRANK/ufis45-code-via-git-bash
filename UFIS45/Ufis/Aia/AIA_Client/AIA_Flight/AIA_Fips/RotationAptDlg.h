#if !defined(AFX_ROTATIONAPTDLG_H__4C62B641_7A99_11D1_8348_0080AD1DC701__INCLUDED_)
#define AFX_ROTATIONAPTDLG_H__4C62B641_7A99_11D1_8348_0080AD1DC701__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// RotationAptDlg.h : header file
//

#include "CCSEdit.h"

/////////////////////////////////////////////////////////////////////////////
// RotationAptDlg dialog

class RotationAptDlg : public CDialog
{
// Construction
public:
	RotationAptDlg(CWnd* pParent = NULL, CString opApc3 = "");   // standard constructor

	~RotationAptDlg();

	void AppExit();

// Dialog Data
	//{{AFX_DATA(RotationAptDlg)
	enum { IDD = IDD_ROTATION_APT };
	CCSEdit	m_CE_Apfn;
	CCSEdit	m_CE_Apc4;
	CCSEdit	m_CE_Apc3;
	CString	m_Apc3;
	CString	m_Apc4;
	CString	m_Apfn;
	//}}AFX_DATA


	CString omApc3;
	CString omApc4;

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(RotationAptDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(RotationAptDlg)
	virtual void OnOK();
	afx_msg LONG OnEditKillfocus( UINT wParam, LPARAM lParam);
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_ROTATIONAPTDLG_H__4C62B641_7A99_11D1_8348_0080AD1DC701__INCLUDED_)
