#ifndef __FLUGHAEFENTABLEVIEWER_H__
#define __FLUGHAEFENTABLEVIEWER_H__

#include "stdafx.h"
#include "RotationDlgCedaFlightData.h"
#include "CCSTable.h"
#include "CViewer.h"
#include "CCSPrint.h"


struct FLUGHAEFENTABLE_LINEDATA
{
	long		Urno; 	// Eindeutige Datensatz-Nr.
	CString		Cntr;	// Anzahl der Bewegungen
	CString 	Des3; 	// Bestimmungsflughafen 3-Lettercode

};

/////////////////////////////////////////////////////////////////////////////
// FlughaefenTableViewer

class FlughaefenTableViewer : public CViewer
{
// Constructions
public:
    FlughaefenTableViewer(CCSPtrArray<ROTATIONDLGFLIGHTDATA> *popData, char *pcpInfo = NULL);
    ~FlughaefenTableViewer();

    void Attach(CCSTable *popAttachWnd);
    virtual void ChangeViewTo(const char *pcpViewName);
// Internal data processing routines
private:
    void MakeLines();
	void MakeLine(CString opDes3old, int iCntr);
// Operations
public:
	void DeleteAll(void);
	void CreateLine(FLUGHAEFENTABLE_LINEDATA *prpFlughaefen);
	void DeleteLine(int ipLineno);
// Window refreshing routines
public:
	void UpdateDisplay(void);
	void PrintPlanToFile(char *pcpDefPath);
// Attributes used for filtering condition
private:
	bool bmIsFromSearch;
// Attributes
private:
    CCSTable *pomFlughaefenTable;
	CCSPtrArray<ROTATIONDLGFLIGHTDATA> *pomData;
public:
    CCSPtrArray<FLUGHAEFENTABLE_LINEDATA> omLines;
///////
	CCSPtrArray<TABLE_HEADER_COLUMN> omHeaderDataArray;
//Print 
	void PrintTableView(void);
	bool PrintTableLine(FLUGHAEFENTABLE_LINEDATA *prpLine,bool bpLastLine);
	bool PrintTableHeader(void);
	CCSPrint *pomPrint;
	char *pcmInfo;
	CString omTableName;
	CString omFooterName;

};

#endif //__FLUGHAEFENTABLEVIEWER_H__
