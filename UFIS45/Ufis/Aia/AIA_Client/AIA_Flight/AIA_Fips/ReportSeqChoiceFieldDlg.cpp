// ReportSeqFieldDlg.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "fpms.h"
#include "ReportSeqChoiceFieldDlg.h"
#include "CcsGlobl.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld CReportSeqChoiceFieldDlg 


CReportSeqChoiceFieldDlg::CReportSeqChoiceFieldDlg(CWnd* pParent, CString opHeadline, CString opSelectfield1, CString opSelectfield2,
									   CTime* poMinDate, CTime* poMaxDate, char *opSelect,bool *opChoice, 
									   CString opSetFormat, int ipMin, int ipMax)
	: CDialog(CReportSeqChoiceFieldDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CReportSeqChoiceFieldDlg)
	m_Datumbis = _T("");
	m_Datumvon = _T("");
	m_Select = _T("");
	m_Selectfield = _T("");
	m_Radio1 = 0;
	//}}AFX_DATA_INIT
	omHeadline = opHeadline;	//Dialogbox-�berschrift 
	m_Selectfield = opSelectfield1;
	m_Selectfield1 = opSelectfield1;
	m_Selectfield2 = opSelectfield2;

	pomMinDate = poMinDate;
	pomMaxDate = poMaxDate;
	pcmSelect = opSelect;
	omSetFormat = opSetFormat;

	pbmChoice = opChoice;
	imMax = ipMax;
	imMin = ipMin;

}


void CReportSeqChoiceFieldDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CReportSeqChoiceFieldDlg)
	DDX_Control(pDX, IDC_SELECTFIELD, m_CS_Selectfield);
	DDX_Control(pDX, IDC_SELECT, m_CE_Select);
	DDX_Control(pDX, IDC_DATUMVON, m_CE_Datumvon);
	DDX_Control(pDX, IDC_DATUMBIS, m_CE_Datumbis);
	DDX_Control(pDX, IDC_RADIO1, m_CE_Radio1);
	DDX_Control(pDX, IDC_RADIO2, m_CE_Radio2);
	DDX_Text(pDX, IDC_DATUMBIS, m_Datumbis);
	DDX_Text(pDX, IDC_DATUMVON, m_Datumvon);
	DDX_Text(pDX, IDC_SELECT, m_Select);
	DDX_Text(pDX, IDC_SELECTFIELD, m_Selectfield);
	DDX_Radio(pDX, IDC_RADIO1, m_Radio1);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CReportSeqChoiceFieldDlg, CDialog)
	//{{AFX_MSG_MAP(CReportSeqChoiceFieldDlg)
		ON_BN_CLICKED(IDC_RADIO1, OnRadio1)
		ON_BN_CLICKED(IDC_RADIO2, OnRadio2)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Behandlungsroutinen f�r Nachrichten CReportSeqChoiceFieldDlg 

void CReportSeqChoiceFieldDlg::OnCancel() 
{
	// TODO: Zus�tzlichen Bereinigungscode hier einf�gen
	
	CDialog::OnCancel();
}

void CReportSeqChoiceFieldDlg::OnOK() 
{
	m_CE_Datumvon.GetWindowText(m_Datumvon);
	m_CE_Datumbis.GetWindowText(m_Datumbis);
	// ist nur ein Time Feld gef�llt, �bernimmt das andere dessen Inhalt
	if(m_Datumvon.IsEmpty())
		m_CE_Datumbis.GetWindowText(m_Datumvon);
	if(m_Datumbis.IsEmpty())
		m_CE_Datumvon.GetWindowText(m_Datumbis);

	*pomMinDate = DateStringToDate(m_Datumvon);
	*pomMaxDate = DateStringToDate(m_Datumbis); 
	m_CE_Select.GetWindowText(m_Select);
	strcpy(pcmSelect, m_Select);


	if(strcmp(m_Selectfield,GetString(IDS_STRING1037)))	// Messe ist kein Pflichtfeld
	{
		if ((ogCustomer == "SHA") && (ogAppName == "FIPS"))
		{
			if(m_Datumbis.IsEmpty() || m_Datumvon.IsEmpty())
				MessageBox(GetString(IDS_STRING918), GetString(IMFK_REPORT), MB_OK);
			else
			{
				if((m_CE_Datumvon.GetStatus()) && (m_CE_Datumvon.GetStatus()))
				{
					if (*pomMinDate > *pomMaxDate)
						MessageBox(GetString(IDS_STRING1642), GetString(IMFK_REPORT), MB_OK);
					else
					{
						*pbmChoice = (m_CE_Radio1.GetCheck() == 1)?true:false;
						CDialog::OnOK();
					}
				}
				else
					MessageBox(GetString(IDS_STRING934), GetString(IMFK_REPORT), MB_OK);
			}
		}
		else
		{
			if((!strlen(pcmSelect)) || ((m_Datumbis.IsEmpty()) || (m_Datumvon.IsEmpty())) )
				MessageBox(GetString(IDS_STRING918), GetString(IMFK_REPORT), MB_OK);
			else
			{
				if((m_CE_Datumvon.GetStatus()) && (m_CE_Datumvon.GetStatus()))
					CDialog::OnOK();
				else
					MessageBox(GetString(IDS_STRING934), GetString(IMFK_REPORT), MB_OK);
			}
		}
	}
	else
	{
		if( m_Datumbis.IsEmpty() || m_Datumvon.IsEmpty() )
			MessageBox(GetString(IDS_STRING918), GetString(IMFK_REPORT), MB_OK);
		else
		{
			if((m_CE_Datumvon.GetStatus()) && (m_CE_Datumvon.GetStatus()))
				CDialog::OnOK();
			else
				MessageBox(GetString(IDS_STRING934), GetString(IMFK_REPORT), MB_OK);
		}
	}
}

BOOL CReportSeqChoiceFieldDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

	if (ogCustomer == "SHA" && ogAppName == "FIPS")
	{
		CStatic* polSelectHeadline = (CStatic*)GetDlgItem(IDC_SELECTHEADLINE);
		CStatic* polTill = (CStatic*)GetDlgItem(IDC_ST_INTERVAL_TILL);
		polSelectHeadline->ShowWindow(SW_HIDE);
		polTill->SetWindowText(GetString(IDS_STRING1604));
	}


	// MB Test Anfang
	if (omSetFormat.IsEmpty())
		m_CE_Select.SetFormat("A|#A|#A|#A|#A");
	else
	{
		m_CE_Select.SetFormat(omSetFormat);
		if (imMax != 0)
			m_CE_Select.SetTypeToString(omSetFormat, imMax, imMin);
	}
	// MB Test Ende

	CDialog::SetWindowText(omHeadline);	//Dialogbox-�berschrift
	m_CE_Datumvon.SetTypeToDate();
	m_CE_Datumvon.SetBKColor(YELLOW);
	m_Datumvon = pomMinDate->Format("%d.%m.%Y" ) ;
	m_CE_Datumvon.SetWindowText( m_Datumvon );

	m_CE_Datumbis.SetTypeToDate();
	m_CE_Datumbis.SetBKColor(YELLOW);
	m_Datumbis = pomMaxDate->Format("%d.%m.%Y" ) ;
	m_CE_Datumbis.SetWindowText( m_Datumbis );

	m_Select = pcmSelect ;
	m_CE_Select.SetWindowText(  m_Select );

	CString olCompare(GetString(IDS_STRING1036));
	//if(strcmp(m_Selectfield,GetString(IDS_STRING1037)))	
	if( m_Selectfield.Compare(GetString(IDS_STRING1037)) ) // Messe ist kein Pflichtfeld
		m_CE_Select.SetBKColor(YELLOW);
	if( !(m_Selectfield.Compare(olCompare)) )	
	{
		m_CE_Select.SetTypeToInt();
		m_CE_Select.SetWindowText(m_Select);
	}
	m_CE_Radio1.SetWindowText(m_Selectfield1);
	m_CE_Radio2.SetWindowText(m_Selectfield2);
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX-Eigenschaftenseiten sollten FALSE zur�ckgeben
}

void CReportSeqChoiceFieldDlg::OnRadio1() 
{
	m_Selectfield = m_Selectfield1;
	m_CS_Selectfield.SetWindowText(m_Selectfield);
}
	
void CReportSeqChoiceFieldDlg::OnRadio2() 
{
	m_Selectfield = m_Selectfield2;
	m_CS_Selectfield.SetWindowText(m_Selectfield);
}
	

