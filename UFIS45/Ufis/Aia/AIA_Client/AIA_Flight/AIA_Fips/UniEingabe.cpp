// UniEingabe.cpp : implementation file
//

#include "stdafx.h"
#include "CCSEdit.h"
#include "UniEingabe.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// UniEingabe dialog


UniEingabe::UniEingabe(CWnd* pParent, CString opLabel, CString opCaption, int ipTextLimit)
	: CDialog(UniEingabe::IDD, pParent)
{
	omCaption = opCaption;
	omLabel = opLabel;
	imTextLimit = ipTextLimit;
	//{{AFX_DATA_INIT(UniEingabe)
	m_Eingabe = _T("");
	//}}AFX_DATA_INIT
}


void UniEingabe::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(UniEingabe)
	DDX_Control(pDX, IDC_EINGABE, m_Text);
	DDX_Control(pDX, IDC_LABEL, m_Label);
	DDX_Text(pDX, IDC_EINGABE, m_Eingabe);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(UniEingabe, CDialog)
	//{{AFX_MSG_MAP(UniEingabe)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// UniEingabe message handlers

void UniEingabe::OnOK() 
{
	// TODO: Add extra validation here
	UpdateData();
	CDialog::OnOK();
}

BOOL UniEingabe::OnInitDialog() 
{
	CDialog::OnInitDialog();

	m_Text.LimitText(imTextLimit);
	SetWindowText(omCaption);	
	m_Label.SetWindowText(omLabel);
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}
