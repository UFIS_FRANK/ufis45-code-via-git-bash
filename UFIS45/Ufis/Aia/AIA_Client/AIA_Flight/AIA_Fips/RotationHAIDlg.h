#if !defined(AFX_ROTATIONHAIDLG_H__89A8CE31_48BB_11D3_AB92_00001C019D0B__INCLUDED_)
#define AFX_ROTATIONHAIDLG_H__89A8CE31_48BB_11D3_AB92_00001C019D0B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// RotationHAIDlg.h : header file
//
// Modification History: 
//	161100	rkr	Dialog nach Vorgaben PRF Athen umgestellt:int CheckUpArrOrDEP() neu;
//	171100	rkr	Dialog unabhängig von Datenstruktur


#include "CCSTable.h"

#include "resrc1.h"

/////////////////////////////////////////////////////////////////////////////
// RotationHAIDlg dialog

class RotationHAIDlg : public CDialog
{
// Construction
public:
	RotationHAIDlg(CWnd* pParent = NULL, CString opAlc3StrArr="", CString opAlc3StrDep="");
		           
	~RotationHAIDlg();

// Dialog Data
	//{{AFX_DATA(RotationHAIDlg)
	enum { IDD = IDD_ROTATION_HAI };
	CStatic	m_CS_GMBorder;
	//}}AFX_DATA


	void InitTable();
	void DrawHeader();
	void InitDialog();

	void FillTableLine(CString&);

	CWnd* pomParent;
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(RotationHAIDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(RotationHAIDlg)
	afx_msg void OnClose();
	afx_msg void OnDelete();
	afx_msg void OnInsert();
	afx_msg void OnUpdate();
	virtual BOOL OnInitDialog();
	virtual void OnCancel();
    afx_msg LONG OnTableLButtonDblclk(UINT wParam, LONG lParam);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

	CString omAlc3StrArr;
	CString omAlc3StrDep;
	CCSTable *pomTable;
	CStringArray omHAIUrnos;
	int CheckUpArrOrDEP();

};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_ROTATIONHAIDLG_H__89A8CE31_48BB_11D3_AB92_00001C019D0B__INCLUDED_)
