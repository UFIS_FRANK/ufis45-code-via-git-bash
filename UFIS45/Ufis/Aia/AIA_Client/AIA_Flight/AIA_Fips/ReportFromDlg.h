#ifndef AFX_REPORTFROMDLG_H__CDB257E3_8BF5_11D1_8127_0000B43C4B01__INCLUDED_
#define AFX_REPORTFROMDLG_H__CDB257E3_8BF5_11D1_8127_0000B43C4B01__INCLUDED_

// ReportFromDlg.h : Header-Datei
//
#include "CCSEdit.h"

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld CReportFromDlg 

class CReportFromDlg : public CDialog
{
// Konstruktion
public:
	CReportFromDlg(CWnd* pParent = NULL, CString opHeadline = "", CString opSelectfield = "", CTime* oDate = NULL);

	CTime *pomDate;
// Dialogfelddaten
	//{{AFX_DATA(CReportFromDlg)
	enum { IDD = IDD_REPORTFROM };
	CStatic	m_CS_Select;
	CCSEdit	m_CE_Datumvon;
	CString	m_Datumvon;
	CString	m_Select;
	//}}AFX_DATA
	CString omHeadline;



// Überschreibungen
	// Vom Klassen-Assistenten generierte virtuelle Funktionsüberschreibungen
	//{{AFX_VIRTUAL(CReportFromDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	//}}AFX_VIRTUAL

// Implementierung
protected:

	// Generierte Nachrichtenzuordnungsfunktionen
	//{{AFX_MSG(CReportFromDlg)
	virtual void OnCancel();
	virtual void OnOK();
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio fügt zusätzliche Deklarationen unmittelbar vor der vorhergehenden Zeile ein.

#endif // AFX_REPORTFROMDLG_H__CDB257E3_8BF5_11D1_8127_0000B43C4B01__INCLUDED_
