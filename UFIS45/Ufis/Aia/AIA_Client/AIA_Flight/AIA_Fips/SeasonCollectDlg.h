#ifndef AFX_SEASONCOLLECTDLG_H__0286A341_A838_11D1_A3CE_0000B45A33F5__INCLUDED_
#define AFX_SEASONCOLLECTDLG_H__0286A341_A838_11D1_A3CE_0000B45A33F5__INCLUDED_

// SeasonCollectDlg.h : Header-Datei
//
// Modification History:
// 20-nov-00 rkr   PRF (Athen):	New Dialog Handling Agents, older one removed 
// 06-feb-01 rkr   PRF F28 (Athen):	Tooltips for cells
//
//
#include "CCSCell.h"
#include "CCSButtonCtrl.h"
#include "CCSPtrArray.h"
#include "CCSTable.h"
#include "SeasonCollectCedaFlightData.h" 
#include "ViaTableCtrl.h"
#include "DlgResizeHelper.h"

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld SeasonCollectDlg 


struct INPUTVIADATA
{
	CString Apc3;
	CString Apc4;
	CString Sta;
	CString Std;
	CString Disp;
	bool InUse;
};

struct INPUTCCADATA
{
	CString Ckic;
	CString Ckit;
	CString Ckbs;
	CString Ckes;
	bool InUse;
};



class SeasonCollectDlg : public CDialog
{
// Konstruktion
public:
	SeasonCollectDlg(CWnd* pParent = NULL);   // Standardkonstruktor
	~SeasonCollectDlg();

	void LoadNewData(CString opAlc3, CString opFltn, CString opFlns, CString opAdid, CTime opSto, CString opFlno);

	void FillMatrix(CString opField);
	void SaveToReg();

	virtual int OnToolHitTest( CPoint point, TOOLINFO* pTI ) const;

// Dialogfelddaten
	//{{AFX_DATA(SeasonCollectDlg)
	enum { IDD = IDD_SEASONCOLLECT };
	CButton	m_CB_NewFlno;
	CButton	m_CB_Split;
	CButton	m_CB_Join;
	CStatic	m_CS_Org;
	CStatic	m_CS_Des;
	CStatic	m_CE_Offset;
	CSpinButtonCtrl	m_CB_OffsetSpin;
	CCSEdit	m_CE_Fltn;
	CCSEdit	m_CE_Flns;
	CCSEdit	m_CE_Alc;
	CCSButtonCtrl	m_CB_Delete;
	CCSButtonCtrl	m_CB_OK;
	CCSButtonCtrl	m_CB_Agents;
	CButton	m_CR_Planung;
	CButton	m_CR_Betrieb;
	CButton	m_CR_Prognose;
	CButton	m_CR_Noop;
	CStatic	m_CS_CinsLabel;
	CButton	m_CB_CinsMal4;
	CStatic	m_CS_CinsBorder;
	CStatic	m_CS_JfnoBorder;
	CSpinButtonCtrl	m_CB_UpDown;
	CStatic	m_CS_Number;
	CStatic	m_CS_Table;
	CStatic	m_CS_Matrix;
	CComboBox	m_CL_Showfield;
	CString	m_Alc;
	CString	m_Flns;
	CString	m_Fltn;
	CString	m_Offset;
	//}}AFX_DATA


// Überschreibungen
	// Vom Klassen-Assistenten generierte virtuelle Funktionsüberschreibungen
	//{{AFX_VIRTUAL(SeasonCollectDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	public:
	virtual BOOL DestroyWindow();
	//}}AFX_VIRTUAL

// Implementierung
private:
	BOOL CheckPostFlight(COLLECTFLIGHTDATA *prpFlight);
	void DisplayRemarks();
	DlgResizeHelper m_resizeHelper;
	CString m_key;

public:	
	void ProcessFlightChange(COLLECTFLIGHTDATA *prpFlight);

	void DataReload();

	void SeasonDlgGetPrev(NEW_SEASONDLG_DATA *popData);

	void SeasonDlgGetNext(NEW_SEASONDLG_DATA *popData);

	bool bmChanged;


protected:
	CMenu omMenuFlights;
	CUIntArray omMenuUrno;
	
	
	CTime omDispStart;
	CTime omDispEnd;

	CTime omLoadStart;
	CTime omLoadEnd;
	CString omAlc3;
	CString omFltn;
	CString omFlns;
	CString omFlno;
	CString omAdid;
	CString omUFRAdid;
	CString omSeas;
	CString omField;

	bool bmInputJfno;
	bool bmInputVia;
	bool bmInputCca;
	int imContPos;

	CCSPtrArray<CCSCell> omCells;	
	CCSPtrArray<CCSButtonCtrl> omHButtons;	
	CCSPtrArray<CCSButtonCtrl> omVButtons;	

	//CCSPtrArray<VIADATA> omInputVias;
	
	CCSPtrArray<INPUTVIADATA> omInputVia;
	CCSPtrArray<JFNODATA> omInputJfno;
	CCSPtrArray<INPUTCCADATA> omInputCca;
	
	
	
	CMapStringToPtr omDayMap;
	CMapStringToPtr omPosMap;
	CMapPtrToPtr omButtonMap;
	
	CRect omSelRect; 
	CRect omLastSelRect;

	CCSTable *pomTable;
	CCSTable *pomCinsTable;
	ViaTableCtrl *pomViaTableCtrl;
	CCSTable *pomJfnoTable;

	CStringArray omTableFields;
	CStringArray  omInputData;
	CStringArray omInputField;

	bool GetDataList(COLLECTFLIGHTDATA *prpFlight, CString &opFieldList, CString &opDataList);

	void SelectRegion();

	void InitMatrtix();

	void InitTables(CString opAdid);

	void CreateTables();

	CString GetValue(COLLECTFLIGHTDATA *prpFlight, CString opField);

	void GetSelectFlights(CCSPtrArray<COLLECTFLIGHTDATA> &olFlights, CString& olRkeysLists, bool bpJoinFlights = false);

	CString GetInputList();

	void SetWndPos();

	// Generierte Nachrichtenzuordnungsfunktionen
	//{{AFX_MSG(SeasonCollectDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnDeselect();
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnCellLButtonDblclk( UINT wParam, LPARAM lParam);
	virtual void OnCancel();
	virtual void OnOK();
	afx_msg void OnSelchangeShowfield();
	afx_msg void OnSelectall();
	afx_msg void OnFlno();
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	afx_msg void OnTablesdeselect();
	afx_msg void OnTablesreset();
	afx_msg void OnCinsmal4();
	afx_msg void OnAgents();
	afx_msg LONG OnEditChanged( UINT wParam, LPARAM lParam);
	afx_msg LONG OnButtonClick( UINT wParam, LPARAM lParam);
	afx_msg void OnDelete();
	afx_msg void OnBetrieb();
	afx_msg void OnNoop();
	afx_msg void OnPrognose();
	afx_msg void OnPlanung();
	afx_msg LONG OnTableIPEditKillfocus( UINT wParam, LPARAM lParam);
	afx_msg LONG OnTableSelChanged( UINT wParam, LPARAM lParam);
	afx_msg void OnCellMouseMove( UINT wParam, LPARAM lParam);
	afx_msg void OnCellLButtonDown( UINT wParam, LPARAM lParam);
	afx_msg void OnCellLButtonUp( UINT wParam, LPARAM lParam);
	afx_msg void OnSplit();
	afx_msg void OnJoin();
	afx_msg LONG OnMenu( WPARAM wParam, LPARAM lParam );
	afx_msg void OnCellRButtonDown( UINT wParam, LPARAM lParam);
	afx_msg void OnRightButtonDown( UINT wParam, LPARAM lParam);
	afx_msg void OnSize(UINT nType, int cx, int cy); 
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio fügt zusätzliche Deklarationen unmittelbar vor der vorhergehenden Zeile ein.

#endif // AFX_SEASONCOLLECTDLG_H__0286A341_A838_11D1_A3CE_0000B45A33F5__INCLUDED_
