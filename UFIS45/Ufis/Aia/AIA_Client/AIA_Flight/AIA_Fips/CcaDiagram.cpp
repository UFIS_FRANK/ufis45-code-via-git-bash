// CcaDiagram.cpp : implementation file
// Modification History:
// 19-feb-01	rkr	PRF 1305 Dubai: Gantchart-Anzeige unter local unterst�tzt
//

#include "stdafx.h"
#include "CcaDiagram.h"
#include "ButtonListDlg.h"
#include "CcaDiaPropertySheet.h"
#include "CcaCedaFlightData.h"
#include "CcaChart.h"
#include "CcaGantt.h"
#include "TimePacket.h"
#include "RotationISFDlg.h"
#include "CicDemandTableDlg.h"
#include "CicNoDemandTableDlg.h"
#include "CicConfTableDlg.h"
#include "CCAExpandDlg.h"
#include "DataSet.h"
#include "DelelteCounterDlg.h"
#include "AllocateCca.h"
#include "AllocateCcaParameter.h"
#include "CcaCommonTableDlg.h"
#include "FlightSearchTableDlg.h"
#include "PrivList.h"
#include "BltKonflikteDlg.h"
#include "SpotAllocation.h"
#include "Konflikte.h"
#include "Utils.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

static void CcaDiagramCf(void *popInstance, int ipDDXType,
    void *vpDataPointer, CString &ropInstanceName);

/////////////////////////////////////////////////////////////////////////////
// CcaDiagram
extern int igAnsichtPageIndex;

IMPLEMENT_DYNCREATE(CcaDiagram, CFrameWnd)

CcaDiagram::CcaDiagram()
{
	igAnsichtPageIndex = -1;
	omMaxTrackSize = CPoint(4000/*1024*/, 1500);
	omMinTrackSize = CPoint(1024 / 4, 768 / 4);
	omViewer.SetViewerKey("CCADIA");
	omViewer.SelectView("<Default>");
    imStartTimeScalePos = 104;
    imStartTimeScalePos++;              // plus one for left border of chart
    imFirstVisibleChart = -1;
	lmBkColor = lgBkColor;
	pogCcaDiagram = this;
	bmIsViewOpen = false;
	bgKlebefunktion = false;
	pogCicDemandTableDlg = NULL;//new CicDemandTableDlg(this);
	bmRepaintAll = false;
	pomCicKonflikteDlg = NULL;
	m_key = "DialogPosition\\CheckInDiagram";
}

CcaDiagram::~CcaDiagram()
{

	if(pogCicDemandTableDlg != NULL)
	{
		pogCicDemandTableDlg->SaveToReg();
		delete pogCicDemandTableDlg;
	}
	if(pogCicNoDemandTableDlg != NULL)
	{
		pogCicNoDemandTableDlg->SaveToReg();
		delete pogCicNoDemandTableDlg;
	}
	if(pogCicConfTableDlg != NULL)
	{
		pogCicConfTableDlg->SaveToReg();
		delete pogCicConfTableDlg;
	}
	pogCcaDiagram = NULL;
}


BEGIN_MESSAGE_MAP(CcaDiagram, CFrameWnd)
	//{{AFX_MSG_MAP(CcaDiagram)
	ON_WM_CREATE()
	ON_WM_DESTROY()
	ON_BN_CLICKED(IDC_OFFLINE, OnOffline)
	ON_BN_CLICKED(IDC_BEENDEN, OnBeenden)
	ON_BN_CLICKED(IDC_ANSICHT, OnAnsicht)
	ON_BN_CLICKED(IDC_INSERT, OnInsert)
	ON_BN_CLICKED(IDC_CICDEMAND, OnDemand )
	ON_BN_CLICKED(IDC_CIC_CONF, OnCicConf )
	ON_BN_CLICKED(IDC_COMMON_CCA, OnCommonCca )
	ON_BN_CLICKED(IDC_EXPAND, OnExpand )
	ON_BN_CLICKED(IDC_ALLOCATE, OnAllocate)
	ON_BN_CLICKED(IDC_FLIGHTS_WITHOUT_DEM, OnFlightsWithoutResource)
	ON_BN_CLICKED(IDC_DELETE_TIMEFRAME, OnDeleteTimeFrame)
	ON_BN_CLICKED(IDC_PRINT_GANTT, OnPrintGantt)
    ON_WM_GETMINMAXINFO()
    ON_WM_PAINT()
    ON_WM_ERASEBKGND()
    ON_WM_SIZE()
    ON_WM_HSCROLL()
    ON_WM_TIMER()
    ON_MESSAGE(WM_POSITIONCHILD, OnPositionChild)
    ON_MESSAGE(WM_UPDATEDIAGRAM, OnUpdateDiagram)
    ON_CBN_SELCHANGE(IDC_VIEW, OnViewSelChange)
    ON_CBN_SELCHANGE(IDC_SEASON_SEL, OnSeasonViewSelChange)
	ON_CBN_CLOSEUP(IDC_VIEW, OnCloseupView)
	ON_BN_CLICKED(IDC_ZEIT, OnZeit)
	ON_BN_CLICKED(IDC_SEARCH, OnSearch)
	ON_WM_KEYDOWN()
	ON_WM_KEYUP()
	ON_WM_CLOSE()
	ON_MESSAGE(WM_REPAINT_ALL, RepaintAll)
//	ON_BN_CLICKED(IDC_GATPOS_CONF, OnGatposConf)
	ON_BN_CLICKED(IDC_CCAATTENTION, OnGatposConf)
	ON_BN_CLICKED(IDC_CHANGES, OnChanges)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


static void CcaDiagramCf(void *popInstance, int ipDDXType,
    void *vpDataPointer, CString &ropInstanceName)
{
	CcaDiagram *polDiagram = (CcaDiagram *)popInstance;

	switch (ipDDXType)
	{
	case REDISPLAY_ALL:
		polDiagram->RedisplayAll();
		break;
	case STAFFDIAGRAM_UPDATETIMEBAND:
		TIMEPACKET *polTimePacket = (TIMEPACKET *)vpDataPointer;
		polDiagram->SetTimeBand(polTimePacket->StartTime, polTimePacket->EndTime);
		polDiagram->UpdateTimeBand();
		break;
	}
}

/////////////////////////////////////////////////////////////////////////////
// CcaDiagram message handlers
void CcaDiagram::PositionChild()
{

    CRect olRect;
    CRect olChartRect;
    
    int ilLastY = 0;
    omClientWnd.GetClientRect(&olRect);
         
    pomChart->GetClientRect(&olChartRect);
    olChartRect.right = olRect.right;

    olChartRect.top = ilLastY;

    ilLastY += pomChart->GetHeight();
    olChartRect.bottom = ilLastY;
        
    // check
    if ((pomChart->GetState() != Minimized) &&
		(olChartRect.top < olRect.bottom) && (olChartRect.bottom > olRect.bottom))
    {
        olChartRect.bottom = olRect.bottom;
        pomChart->SetState(Normal);
    }
    //
        
    pomChart->MoveWindow(&olChartRect, FALSE);
	pomChart->ShowWindow(SW_SHOW);

    
	omClientWnd.Invalidate(TRUE);
	//SetAllStaffAreaButtonsColor();
 	UpdateTimeBand();
    
	
}


void CcaDiagram::SetTSStartTime(CTime opTSStartTime)
{
/*
    omTSStartTime = opTSStartTime;
    if ((omTSStartTime < omStartTime) ||
		(omTSStartTime > omStartTime + omDuration))
    {
        omStartTime = CTime(omTSStartTime.GetYear(), omTSStartTime.GetMonth(), omTSStartTime.GetDay(),
            0, 0, 0) - CTimeSpan(1, 0, 0, 0);
    }
        
    char clBuf[64];
    sprintf(clBuf, "%02d%02d%02d  %d",
        omTSStartTime.GetDay(), omTSStartTime.GetMonth(), omTSStartTime.GetYear() % 100, GetDayOfWeek(omTSStartTime));
    omTSDate.SetWindowText(clBuf);
    omTSDate.Invalidate(FALSE);
*/

    omTSStartTime = opTSStartTime;
    char clBuf[20];
    sprintf(clBuf, "  %02d%02d%02d  ", 
        omTSStartTime.GetDay(), omTSStartTime.GetMonth(), omTSStartTime.GetYear() % 100);
    omTSDate.SetWindowText(clBuf);
    omTSDate.Invalidate(FALSE);
}


int CcaDiagram::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	if (CFrameWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	{
	pogCicDemandTableDlg = new CicDemandTableDlg(this);
	pogCicNoDemandTableDlg = new CicNoDemandTableDlg(this);
	pogCicConfTableDlg = new CicConfTableDlg(this);

	CRect olRect;
	GetWindowRect(olRect);
/*
	pogCicNoDemandTableDlg->MoveWindow(CRect(olRect.right - 500, olRect.bottom - 300, olRect.right - 25, olRect.bottom - 25));
	pogCicDemandTableDlg->MoveWindow(CRect(olRect.right - 1024, olRect.top + pogButtonList->m_nDialogBarHeight + 25, olRect.right -25, olRect.top + 550));
	pogCicConfTableDlg->MoveWindow(CRect(olRect.right - 1024, olRect.bottom - 300, olRect.right - 25, olRect.bottom - 25));
*/
	pogCicNoDemandTableDlg->ShowWindow(SW_HIDE);
	pogCicDemandTableDlg->ShowWindow(SW_HIDE);
	pogCicConfTableDlg->ShowWindow(SW_HIDE);
	}

	// Create ConflictDlg
	if (pomCicKonflikteDlg == NULL)
	{
		pomCicKonflikteDlg = new BltKonflikteDlg(this);
//		pomCicKonflikteDlg->SetWindowPos(&wndTop,100,200,700,400, SWP_HIDEWINDOW);
	}	

    SetTimer(0, (UINT) 60 * 1000, NULL);

//rkr130301
//    SetTimer(1, (UINT) 60 * 1000 * 2, NULL);
//    SetTimer(1, (UINT) 60 * 1000 * ogPosDiaFlightData.cimBartimesUpdateMinute, NULL);  // Update bar times
// 	now done by DDX message KONFLIKTCHECK_FOLLOWS

	omDialogBar.Create( this, IDD_CCADIAGRAM, CBRS_TOP, IDD_CCADIAGRAM );
	SetWindowPos(&wndTop, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE);

	//CButton *polCB;


    ((CButton *) omDialogBar.GetDlgItem(IDC_SEASON_SEL))->ShowWindow(SW_HIDE);



	CButton *polButton = (CButton *)omDialogBar.GetDlgItem(IDC_OFFLINE2);	
 	WINDOWPLACEMENT olWndPlace;
	polButton->GetWindowPlacement( &olWndPlace ); 
	m_CB_Offline.Create(GetString(IDS_STRING1258), WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON | BS_OWNERDRAW, olWndPlace.rcNormalPosition, &omDialogBar, IDC_OFFLINE);
    m_CB_Offline.SetFont(polButton->GetFont());
	m_CB_Offline.EnableWindow(true);

	if (ogPrivList.GetStat("CCADIAGRAMM_CB_Offline") == '0')
			m_CB_Offline.EnableWindow(false);
	if (ogPrivList.GetStat("CCADIAGRAMM_CB_Offline") == '-')
		m_CB_Offline.ShowWindow(SW_HIDE);


	// Create 'CcaAttention'-Button
	polButton = (CButton *)omDialogBar.GetDlgItem(IDC_GATPOS_CONF);	
	polButton->GetWindowPlacement( &olWndPlace ); 
	m_CB_CcaAttention.Create(GetString(IMFK_ATTENTION), WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON | BS_OWNERDRAW, olWndPlace.rcNormalPosition, &omDialogBar, IDC_CCAATTENTION);
    m_CB_CcaAttention.SetFont(polButton->GetFont());
	m_CB_CcaAttention.EnableWindow(true);
	// Update Attention-Button
	ogKonflikte.CheckAttentionButton();
	m_CB_CcaAttention.SetSecState(ogPrivList.GetStat("CCADIAGRAMM_CB_Attention"));	


	// Create 'Wores'-Button
	polButton = (CButton *)omDialogBar.GetDlgItem(IDC_CICDEMAND2);	
	polButton->GetWindowPlacement( &olWndPlace ); 
	CString olTmp;
	olTmp.Format("%s (%d)", GetString(IDS_STRING2444),0);
	m_CB_WoRes.Create(olTmp, WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON | BS_OWNERDRAW, olWndPlace.rcNormalPosition, &omDialogBar, IDC_CICDEMAND);
    m_CB_WoRes.SetFont(polButton->GetFont());
	m_CB_WoRes.EnableWindow(true);
	m_CB_WoRes.SetSecState(ogPrivList.GetStat("CCADIAGRAMM_CB_WORes"));	

	// Create 'WoDem'-Button
	polButton = (CButton *)omDialogBar.GetDlgItem(IDC_FLIGHTS_WITHOUT_DEM);	
	polButton->GetWindowPlacement( &olWndPlace ); 
	olTmp.Format("%s (%d)", GetString(IDS_STRING2445),0);
	m_CB_WoDem.Create(olTmp, WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON | BS_OWNERDRAW, olWndPlace.rcNormalPosition, &omDialogBar, IDC_FLIGHTS_WITHOUT_DEM);
    m_CB_WoDem.SetFont(polButton->GetFont());
	m_CB_WoDem.EnableWindow(true);
	m_CB_WoDem.SetSecState(ogPrivList.GetStat("CCADIAGRAMM_CB_WODemands"));	

	// Create 'WoDem'-Button
	polButton = (CButton *)omDialogBar.GetDlgItem(IDC_CIC_CONF);	
	polButton->GetWindowPlacement( &olWndPlace ); 
	olTmp.Format("%s (%d/%d)", GetString(IDS_STRING2216),0,0);
	m_CB_Conf.Create(olTmp, WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON | BS_OWNERDRAW, olWndPlace.rcNormalPosition, &omDialogBar, IDC_CIC_CONF);
    m_CB_Conf.SetFont(polButton->GetFont());
	m_CB_Conf.EnableWindow(true);
	m_CB_Conf.SetSecState(ogPrivList.GetStat("CCADIAGRAMM_CB_Conflicts"));	

	// Create 'Changes'-Button
	polButton = (CButton *)omDialogBar.GetDlgItem(IDC_CHANGES);	
	polButton->GetWindowPlacement( &olWndPlace ); 
//	olTmp = GetString(IDS_CHANGES);
	olTmp.Format("%s (%d/%d)", GetString(IDS_CHANGES),0,0);
	m_CB_Changes.Create(olTmp, WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON | BS_OWNERDRAW, olWndPlace.rcNormalPosition, &omDialogBar, IDC_CHANGES);
    m_CB_Changes.SetFont(polButton->GetFont());
	m_CB_Changes.EnableWindow(true);
	m_CB_Changes.SetSecState(ogPrivList.GetStat("DESKTOP_CB_History")); 

    omViewer.Attach(this);
	UpdateComboBox();

    CRect olRect; GetClientRect(&olRect);
	CTime olCurrentTime;
	olCurrentTime = CTime::GetCurrentTime();
	ogBasicData.LocalToUtc(olCurrentTime);


    omTSStartTime = olCurrentTime;// - CTimeSpan(0, 1, 0, 0);
    omStartTime = CTime(omTSStartTime.GetYear(), omTSStartTime.GetMonth(), omTSStartTime.GetDay(),
        0, 0, 0);// - CTimeSpan(1, 0, 0, 0);

    omDuration = CTimeSpan(3, 0, 0, 0);
    omTSDuration = CTimeSpan(0, 6, 0, 0);
    omTSInterval = CTimeSpan(0, 0, 10, 0);

	//Die wollen local
    CTime olUCT = CTime::GetCurrentTime();//olCurrentTime;
    CTime olRealUTC = olCurrentTime;
    char olBuf[64];
    sprintf(olBuf, "%02d%02d/%02d%02dz",
        olUCT.GetHour(), olUCT.GetMinute(),        
        olRealUTC.GetHour(), olRealUTC.GetMinute()
    );

    
	
	CTime olLocalTime = CTime::GetCurrentTime();
	CTime olUtcTime = CTime::GetCurrentTime();
	ogBasicData.LocalToUtc(olUtcTime);

//	char pclTimes[36];
//	char pclDate[36];
	
	CTime olUtcTimeTmp = CTime::GetCurrentTime();
	olUtcTimeTmp -= ogUtcDiff;
/*	
	if(ogUtcDiff != 0)
	{
		sprintf(pclDate, "%02d.%02d.%02d z", olUtcTimeTmp.GetDay(), olUtcTimeTmp.GetMonth(), olUtcTimeTmp.GetYear() % 100);
		sprintf(pclTimes, "%02d:%02d / %02d:%02d z", olLocalTime.GetHour(), olLocalTime.GetMinute(), olUtcTimeTmp.GetHour(), olUtcTimeTmp.GetMinute());
	}
	else
	{
		sprintf(pclDate, "%02d.%02d.%02d z", olUtcTime.GetDay(), olUtcTime.GetMonth(), olUtcTime.GetYear() % 100);
		sprintf(pclTimes, "%02d:%02d / %02d:%02d z", olLocalTime.GetHour(), olLocalTime.GetMinute(), olUtcTime.GetHour(), olUtcTime.GetMinute());
	}
    
	
	omTime.Create(pclTimes, SS_CENTER | WS_CHILD | WS_VISIBLE, CRect(olRect.right - 192, 3, olRect.right - 90, 22), this);

    omDate.Create(pclDate, SS_CENTER | WS_CHILD | WS_VISIBLE,  CRect(olRect.right - 88, 3, olRect.right - 8, 22), this);
        
    omDate.SetTextColor(RGB(255,0,0));
*/	
	

    sprintf(olBuf, "%02d%02d%02d  %d",  omTSStartTime.GetDay(), omTSStartTime.GetMonth(), omTSStartTime.GetYear() % 100, GetDayOfWeek(omTSStartTime));
    int ilPos = (imStartTimeScalePos - olRect.left - 80) / 2;
    omTSDate.Create(olBuf, SS_CENTER | WS_CHILD | WS_VISIBLE,
        CRect(ilPos, 35, ilPos + 80, 52), this);

    // CBitmapButton
    omBB1.Create("PREV", BS_OWNERDRAW | WS_CHILD | WS_VISIBLE,
        CRect(olRect.right - 36, 21 + 5, olRect.right - 19, (21 + 5) + 25), this, IDC_PREV);
    omBB1.LoadBitmaps("PREVU", "PREVD", NULL, "NEXTX");

    omBB2.Create("NEXT", BS_OWNERDRAW | WS_CHILD | WS_VISIBLE,
        CRect(olRect.right - 19, 21 + 5, olRect.right - 2, (21 + 5) + 25), this, IDC_NEXT);
    omBB2.LoadBitmaps("NEXTU", "NEXTD", NULL, "NEXTX");

	omBB1.ShowWindow(SW_HIDE);
	omBB2.ShowWindow(SW_HIDE);

	omTimeScale.lmBkColor = lgBkColor;
    omTimeScale.Create(NULL, "TimeScale", WS_CHILD | WS_VISIBLE,
        CRect(imStartTimeScalePos, 28, olRect.right - (36 + 2), 62), this, 0, NULL);
//rkr
	//Set begin to local if local
	CTime olTSStartTime = omTSStartTime;
	if(bgGatPosLocal)
		ogBasicData.UtcToLocal(olTSStartTime);
    
	omTimeScale.SetDisplayTimeFrame(olTSStartTime, omTSDuration, omTSInterval);
//	omTimeScale.SetDisplayTimeFrame(omTSStartTime, omTSDuration, omTSInterval);
	if(bgGatPosLocal)
		omTimeScale.UpdateCurrentTimeLine(olLocalTime);
	else
		omTimeScale.UpdateCurrentTimeLine(olRealUTC);
     
    // must be static
    static UINT ilIndicators[] = { ID_SEPARATOR };
    omStatusBar.Create(this, CBRS_BOTTOM | WS_CHILD | WS_VISIBLE,0);
    omStatusBar.SetIndicators(ilIndicators, sizeof(ilIndicators) / sizeof(UINT));

    UINT nID, nStyle; int cxWidth;
    omStatusBar.GetPaneInfo(0, nID, nStyle, cxWidth);
    omStatusBar.SetPaneInfo(0, nID, SBPS_STRETCH | SBPS_NORMAL, cxWidth);

    olRect.top += 64;                   // for Dialog Bar & TimeScale
    olRect.bottom -= 20;                // for Horizontal Scroll Bar
    omClientWnd.Create(NULL, "ClientWnd", WS_CHILD /*| WS_HSCROLL */ | WS_VISIBLE, olRect, this,
        0 /* IDD_CLIENTWND */, NULL);
        
	// This will fix the bug for the horizontal scroll bar in the preplan mode
    long llTSMin = (omTSStartTime - omStartTime).GetTotalMinutes();
    long llTotalMin = omDuration.GetTotalMinutes() - omTSDuration.GetTotalMinutes();
    SetScrollRange(SB_HORZ, 0, 1000, FALSE);
    SetScrollPos(SB_HORZ, (int) (1000 * llTSMin / llTotalMin), FALSE);

    CRect olPrevRect;
    omClientWnd.GetClientRect(&olRect);
    
    imFirstVisibleChart = 0;
    int ilLastY = 0;

    olRect.SetRect(olRect.left, ilLastY, olRect.right, ilLastY);
    
    pomChart = new CcaChart;
    pomChart->SetTimeScale(&omTimeScale);
    pomChart->SetViewer(&omViewer);
    pomChart->SetStatusBar(&omStatusBar);
    pomChart->SetStartTime(omTSStartTime);
    pomChart->SetInterval(omTimeScale.GetDisplayDuration());
    pomChart->Create(NULL, "CcaChart", WS_OVERLAPPED | WS_BORDER | WS_CHILD | WS_VISIBLE,
        olRect, &omClientWnd,
        0 /* IDD_CHART */, NULL);
    
    OnTimer(0);

	// Register DDX call back function
	//TRACE("CcaDiagram: DDX Registration\n");
	ogDdx.Register(this, REDISPLAY_ALL, CString("STAFFDIAGRAM"),
		CString("Redisplay all from What-If"), CcaDiagramCf);	// for what-if changes
	ogDdx.Register(this, STAFFDIAGRAM_UPDATETIMEBAND, CString("STAFFDIAGRAM"),
		CString("Update Time Band"), CcaDiagramCf);	// for updating the yellow lines
	ogDdx.Register(this, KONFLIKTCHECK_FOLLOWS, CString("STAFFDIAGRAM KONFLIKTCHECK_FOLLOWS"),
		CString("Update Time Band"), CcaDiagramCf);	// for updating the yellow lines


  
	// SEC

   CButton *polCB;

	polCB = (CButton *) omDialogBar.GetDlgItem(IDC_ALLOCATE);
	SetpWndStatAll(ogPrivList.GetStat("CCADIAGRAMM_CB_Allocate"),polCB);

	polCB = (CButton *) omDialogBar.GetDlgItem(IDC_EXPAND);
	SetpWndStatAll(ogPrivList.GetStat("CCADIAGRAMM_CB_Expand"),polCB);

	polCB = (CButton *) omDialogBar.GetDlgItem(IDC_PRINT_GANTT);

	SetpWndStatAll(ogPrivList.GetStat("CCADIAGRAMM_CB_Print"),polCB);

//	polCB = (CButton *) omDialogBar.GetDlgItem(IDC_CICDEMAND);

//	SetpWndStatAll(ogPrivList.GetStat("CCADIAGRAMM_CB_WORes"),polCB);


	polCB = (CButton *) omDialogBar.GetDlgItem(IDC_DELETE_TIMEFRAME);

	SetpWndStatAll(ogPrivList.GetStat("CCADIAGRAMM_CB_Delete"),polCB);


//	polCB = (CButton *) omDialogBar.GetDlgItem(IDC_FLIGHTS_WITHOUT_DEM);

//	SetpWndStatAll(ogPrivList.GetStat("CCADIAGRAMM_CB_WODemands"),polCB);


//	polCB = (CButton *) omDialogBar.GetDlgItem(IDC_CIC_CONF);

//	SetpWndStatAll(ogPrivList.GetStat("CCADIAGRAMM_CB_Conflicts"),polCB);

	polCB = (CButton *) omDialogBar.GetDlgItem(IDC_COMMON_CCA);

	SetpWndStatAll(ogPrivList.GetStat("CCADIAGRAMM_CB_CommonCki"),polCB);
	

	polCB = (CButton *) omDialogBar.GetDlgItem(IDC_ANSICHT);

	//SetpWndStatAll(ogPrivList.GetStat("CCADIAGRAMM_CB_Ansicht"),polCB);
/*
	polCB = (CButton *) omDialogBar.GetDlgItem(IDC_CHANGES);
	polCB->EnableWindow(true);
	polCB->ShowWindow(SW_SHOW);
	SetpWndStatAll(ogPrivList.GetStat("DESKTOP_CB_History"),polCB); 
*/

	if (!bgOffRelFuncCheckin)
	{
		m_CB_Offline.ShowWindow(SW_HIDE);
	}



	ogCcaDiaFlightData.Register();	 

	//SetWndPos();	

	// set caption
	CString olTimes;
	if (bgGatPosLocal)
		olTimes = GetString(IDS_STRING1921);
	else
		olTimes = GetString(IDS_STRING1920);
	CString olCaption = GetString(IDS_STRING1012);
	olCaption += " ("+olTimes+")";
	SetWindowText(olCaption);


//######
	olRect = CRect(0, 65, ::GetSystemMetrics(SM_CXSCREEN), ::GetSystemMetrics(SM_CYSCREEN));
	GetDialogFromReg(olRect, m_key);
	SetWindowPos(&wndTop, olRect.left, olRect.top, olRect.Width(), olRect.Height(), SWP_SHOWWINDOW);// | SWP_NOMOVE );
//######

	return 0;
}

void CcaDiagram::SaveToReg() 
{
	BOOL ok = WriteDialogToReg((CWnd*) this, m_key);
}


void CcaDiagram::OnBeenden() 
{
	ogCcaDiaFlightData.UnRegister();	 
	ogCcaDiaFlightData.ClearAll();	 
	DestroyWindow();	
}


void CcaDiagram::OnInsert()
{
	RotationISFDlg dlg;
	dlg.DoModal();
}

void CcaDiagram::OnSearch()
{
	pogFlightSearchTableDlg->Activate(omViewer.GetBaseViewName(), omViewer.GetViewName(), CCADIA);
}


void CcaDiagram::OnAnsicht()
{

	//MWO
	SetFocus();
	//END MWO

	CcaDiaPropertySheet olDlg("CCADIA", this, &omViewer, 0, GetString(IDS_STRING1646));
	bmIsViewOpen = true;
	if(olDlg.DoModal() != IDCANCEL)
	{

		if(ogCcaDiaFlightData.bmOffLine)
		{
			ogCcaDiaFlightData.bmOffLine = false;
			ogCcaDiaFlightData.omCcaData.bmOffLine = false;
			ogCcaDiaFlightData.UpdateOfflineButtons();
		}

		omViewer.SelectView(omViewer.GetViewName());

		if (olDlg.FilterChanged())
		{
			LoadFlights();
		}
 		omViewer.MakeMasstab();
		CTime olT = omViewer.GetGeometrieStartTime();

		CTime olFrom;
		CTime olTo;
		CString olFtyps;
		omViewer.GetTimeSpanAndFtyp(olFrom, olTo, olFtyps);


		CString olS = olT.Format("%d.%m.%Y-%H:%M");

		if(	bgCcaDiaSeason )
			olT = ogCcaDiaDate;

		SetTSStartTime(olT);
		omTSDuration = omViewer.GetGeometryTimeSpan();
//rkr
		//Set begin to local if local
		CTime olTSStartTime = omTSStartTime;
		if(bgGatPosLocal)
			ogBasicData.UtcToLocal(olTSStartTime);
    
		omTimeScale.SetDisplayTimeFrame(olTSStartTime, omTSDuration, omTSInterval);
//		omTimeScale.SetDisplayTimeFrame(omTSStartTime, omTSDuration, omTSInterval);
        omTimeScale.Invalidate(TRUE);
		ChangeViewTo(omViewer.GetViewName(), false);
		long llTotalMin = omDuration.GetTotalMinutes() - omTSDuration.GetTotalMinutes();
		long nPos = (omTSStartTime - omStartTime).GetTotalMinutes();
		nPos = nPos * 1000L / llTotalMin;
		SetScrollPos(SB_HORZ, int(nPos), TRUE);
		UpdateComboBox();
		ActivateTables();
	}
	bmIsViewOpen = false;


}

bool CcaDiagram::IsPassFlightFilter(CCAFLIGHTDATA *prpFlight)
{
	bool blRet = false;
	{
		/*

		if(strcmp(prpFlight->Htyp, "K") != 0)
		{
			if(strcmp(prpFlight->Ttyp, "CG") != 0)
			{
				if(strcmp(prpFlight->Flns, "F") != 0)
				{
					if(strcmp(prpFlight->Htyp, "T") != 0)
					{
						blRet = true;
					}
				}
			}
		}
		*/
	}
	return blRet;
}

bool CcaDiagram::HasAllocatedCcas(CCSPtrArray<DIACCADATA> &ropCcaList)
{
	bool blRet = false;
	int ilCount = ropCcaList.GetSize();
	for(int i = 0; ((i < ilCount) && (blRet == false)); i++)
	{
	}
	return blRet;
}


void CcaDiagram::LoadFlights(char *pspView)
{
	bmReadAll = true;
	CString olView;

	if(pspView != NULL)
	{
		omViewer.SelectView(pspView);
		olView = CString(pspView);
	}
	else
	{
		omViewer.SelectView(omViewer.GetViewName());
		olView = CString(omViewer.GetViewName());
	}
	
	CString olWhere;
	CTime olFrom;
	CTime olTo;
	CString olFtyps;
	int ilDOO;
	bool blRotation;



	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
	omStatusBar.SetPaneText(0, GetString(IDS_STRING1205));
	omStatusBar.UpdateWindow();
	
	ogDataSet.InitCcaConflictStrings();


	omViewer.GetTimeSpanAndFtyp(olFrom, olTo, olFtyps);
	TRACE("olFrom: %s,   olTo: %s\n", olFrom.Format("%d.%m.%Y %H:%M:%S"), olTo.Format("%d.%m.%Y %H:%M:%S"));
	ogCcaDiaFlightData.SetPreSelection(olFrom, olTo, olFtyps);


	// set duration and starttime
	omDuration = (olTo - olFrom) + CTimeSpan(1, 0, 0, 0);
	if (bgGatPosLocal)
		ogBasicData.UtcToLocal(olFrom);
	omStartTime = olFrom - CTimeSpan(1, 0, 0, 0);



	CTime olCurr = CTime::GetCurrentTime();
	if(bgGatPosLocal) ogBasicData.LocalToUtc(olCurr);


	CButton *polCB1 = (CButton *) omDialogBar.GetDlgItem(IDC_ZEIT);

	if(olFrom < olCurr && olTo > olCurr)
	{
		polCB1->EnableWindow(TRUE);
	}
	else
	{
		polCB1->EnableWindow(FALSE);
	}


	ilDOO = omViewer.GetDOO();

	char pclSelection[7000] = "";
	ogCcaDiaDate = TIMENULL;


    CComboBox *polCB = (CComboBox *) omDialogBar.GetDlgItem(IDC_SEASON_SEL);


	TRACE("\n-----------------------------------------------");
	TRACE("\n-----------------------------------------------");
	TRACE("\n-----------------------------------------------");
	TRACE("\n-----------------------------------------------");
	TRACE("\n-----------------------------------------------");
	TRACE("\nBegin read:  %s", CTime::GetCurrentTime().Format("%H:%M:%S"));


	if( 0 /* komprimierte Darstellung wird nicht mehr gebraucht! */ /*(ilDOO >= 0)*/ )
	{
		polCB->ShowWindow(SW_SHOW);
		polCB->ResetContent();
		polCB->AddString(CString(olView));
		polCB->SelectString(-1, CString(olView));

		ogCcaDiaSingleDate = TIMENULL;

		CString olText;
		ogCcaDiaFlightData.ClearAll();

		CTime olStart = olFrom;
		CTime olEnd;

		CString olFromTime	= olFrom.Format("%H%M");
		CString olToTime	= olTo.Format("%H%M");

		CTimeSpan olOneDay(1,0,0,0);

		TRACE("\nSTART %s------------------------------", CTime::GetCurrentTime().Format("%H:%M:%S"));

		while(olStart < olTo)
		{
			
			if(ilDOO == GetDayOfWeek(olStart))
			{
				olWhere = "";
				olStart = HourStringToDate(olFromTime, olStart);
				olEnd   = HourStringToDate(olToTime, olStart);


				if(ogCcaDiaDate == TIMENULL)
					ogCcaDiaDate = olStart;


				polCB->AddString(olStart.Format("%d.%m.%Y"));


				omViewer.GetFlightSearchWhereString(olWhere, blRotation, CString (""),false, olStart, olEnd);

				strcpy(pclSelection, olWhere);

				olText = CString("Fl�ge: ") + olStart.Format("%d.%m.%Y");
				omStatusBar.SetPaneText(0, olText);
				omStatusBar.UpdateWindow();
				ogCcaDiaFlightData.ReadFlights(pclSelection, false, true);

				olText = CString("Cca: ") + olStart.Format("%d.%m.%Y");
				omStatusBar.SetPaneText(0, olText);
				omStatusBar.UpdateWindow();
				ogCcaDiaFlightData.ReadCcas(olStart, olEnd);

				//TRACE("\n %s", olWhere);

			}
			olStart += olOneDay;
		}

		TRACE("\nEnd read:    %s", CTime::GetCurrentTime().Format("%H:%M:%S"));
		TRACE("\n-----------------------------------------------");
		TRACE("\n-----------------------------------------------");

		omViewer.SetLoadTimeFrame(ogCcaDiaDate, olEnd);
		

		bgCcaDiaSeason = true;


	}
	else
	{
		polCB->ShowWindow(SW_HIDE);

		bgCcaDiaSeason = false;
		ogCcaDiaDate = TIMENULL;

		omViewer.GetZeitraumWhereString(olWhere, blRotation, CString("") );

		//olWhere += CString(" AND FLNO > ' '");
	
		ogCcaDiaFlightData.ReadAllFlights(olWhere, blRotation);

	}


	TRACE("\nBegin read BLK:  %s", CTime::GetCurrentTime().Format("%H:%M:%S"));


	CString olText = CString("Blk... ");
	omStatusBar.SetPaneText(0, olText);
	omStatusBar.UpdateWindow();

	char pclWhere[512]="";
	
	sprintf(pclWhere, " WHERE (NATO >= '%s' and NAFR <= '%s') OR (NAFR <= '%s' and NATO = ' ') OR  (NATO >= '%s'  and NAFR = ' ')",
												 olFrom.Format("%Y%m%d%H%M00"), olTo.Format("%Y%m%d%H%M59"),
												 olTo.Format("%Y%m%d%H%M59"),
												 olFrom.Format("%Y%m%d%H%M00"));
	ogBCD.Read(CString("BLK"), pclWhere);


	TRACE("\nEnd read BLK:    %s", CTime::GetCurrentTime().Format("%H:%M:%S"));

	CString olCnams = ogCcaDiaFlightData.omCcaData.MakeBlkData(TIMENULL, ilDOO);

	TRACE("\nEnd Make BLK:    %s", CTime::GetCurrentTime().Format("%H:%M:%S"));

	TRACE("\n-----------------------------------------------");
	TRACE("\n-----------------------------------------------");

	omViewer.SetCnams(olCnams);
	omViewer.SetDOO(ilDOO);


	//int ilC5 = ogBCD.GetDataCount("BLK");


	//int ilBnam = ogBCD.GetFieldIndex("CIC", "CNAM");
	//ogBCD.SetSort("CIC", "TERM+,CNAM+", true); //true ==> sort immediately



//----------------------------------------------------------------------------------------------------
// MWO wenn Bernis ResHdl so weit ist, dann kann der nachfolgende Teil wieder raus bis END MWO

	TRACE("\nBeginn Rel:  %s", CTime::GetCurrentTime().Format("%H:%M:%S"));

	CCAFLIGHTDATA *prlFlight;
	POSITION pos;
	void *pVoid;
	int ilCurrentFlight=1;
	char pclPaneText[512]="";
	int ilFltAmount = ogCcaDiaFlightData.omUrnoMap.GetCount();

	//int ilAuskommentiertfuerTest;

	CCSPtrArray<DIACCADATA> olCcaChangedList;
	for( pos = ogCcaDiaFlightData.omUrnoMap.GetStartPosition(); pos != NULL; )
	{
		ogCcaDiaFlightData.omUrnoMap.GetNextAssoc( pos, pVoid , (void *&)prlFlight );
		sprintf(pclPaneText, GetString(IDS_STRING1490), ilCurrentFlight, ilFltAmount); // Regelwerk anwenden: Flug %d von %d ..........................
		omStatusBar.SetPaneText(0, pclPaneText);
		omStatusBar.UpdateWindow();
		ilCurrentFlight++;

		ogDataSet.CheckDemands(prlFlight, olCcaChangedList);
	}

	omStatusBar.SetPaneText(0, GetString(IDS_STRING1491)); // Speichern der Bedarfe..........................................
	omStatusBar.UpdateWindow();

	TRACE("\nEnd Rel:     %s", CTime::GetCurrentTime().Format("%H:%M:%S"));
	TRACE("\n-----------------------------------------------");
	TRACE("\n-----------------------------------------------");

	TRACE("\nBegin Save:  %s", CTime::GetCurrentTime().Format("%H:%M:%S"));

	//int ilAuskommentiertfuerTest2;
	//ogCcaDiaFlightData.omCcaData.Release(&olCcaChangedList, true);
//	int ilSSS;
//	int ilChangeListSize;
//	ilSSS = ogCcaDiaFlightData.omCcaData.omData.GetSize();
	ogCcaDiaFlightData.omCcaData.SaveInternal(&olCcaChangedList, true);
//	ilChangeListSize =  olCcaChangedList.GetSize();
	olCcaChangedList.RemoveAll();
//	ilSSS = ogCcaDiaFlightData.omCcaData.omData.GetSize();
//	ilSSS =  ogCcaDiaFlightData.omData.GetSize();



	TRACE("\nEnd Save:    %s", CTime::GetCurrentTime().Format("%H:%M:%S"));
	TRACE("\n-----------------------------------------------");
	TRACE("\n-----------------------------------------------");

	olText = CString("Load tables... ");
	omStatusBar.SetPaneText(0, olText);
	omStatusBar.UpdateWindow();

 
	//END MWO
//----------------------------------------------------------------------------------------------------
	

	if(bgCcaDiaSeason)
	{
		TRACE("\nBegin compress:  %s", CTime::GetCurrentTime().Format("%H:%M:%S"));

		olText = CString("Compress FLights/Cki... ");
		omStatusBar.SetPaneText(0, olText);
		omStatusBar.UpdateWindow();

		ogCcaDiaFlightData.Kompress();
		ogCcaDiaFlightData.omCcaData.KompressND();

		TRACE("\nEnd compress:    %s", CTime::GetCurrentTime().Format("%H:%M:%S"));
		TRACE("\n-----------------------------------------------");
		TRACE("\n-----------------------------------------------");
	
	}

	TRACE("\nBegin tables:  %s", CTime::GetCurrentTime().Format("%H:%M:%S"));

	ActivateTables();

	TRACE("\nEnd tables:    %s", CTime::GetCurrentTime().Format("%H:%M:%S"));
	TRACE("\n-----------------------------------------------");
	TRACE("\n-----------------------------------------------");




	TRACE("\n ANZ FLIGHT: %d\n", ogCcaDiaFlightData.omData.GetSize());
	TRACE("\n ANZ CCA   : %d\n", ogCcaDiaFlightData.omCcaData.omData.GetSize());
	TRACE("\n-----------------------------------------------");
	TRACE("\n-----------------------------------------------");

	olText = CString("Check overlapping... ");
	omStatusBar.SetPaneText(0, olText);
	omStatusBar.UpdateWindow();
	
	TRACE("\nBegin Overlap:  %s", CTime::GetCurrentTime().Format("%H:%M:%S"));

	int ilCount = ogBCD.GetDataCount("CIC");

	for(int i = 0; i < ilCount; i++)
	{
		ogDataSet.CheckCCaForOverlapping(ogBCD.GetField("CIC", i, "CNAM"));
	}
	TRACE("\nEnd Overlap:    %s", CTime::GetCurrentTime().Format("%H:%M:%S"));
	TRACE("\n-----------------------------------------------");
	TRACE("\n-----------------------------------------------");




	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
	omStatusBar.SetPaneText(0, "");
	omStatusBar.UpdateWindow();

	bmReadAll = false;
	UpdateWoDemButton();
	UpdateWoResButton();
	UpdateConfButton();
}

void CcaDiagram::OnDestroy() 
{
	SaveToReg();
//	if (bgModal == TRUE)
//		return;
	bmNoUpdatesNow = TRUE;
	omViewer.AllowUpdates(bmNoUpdatesNow);
	ogDdx.UnRegister(&omViewer, NOTUSED);

	if (pomCicKonflikteDlg)
	{
		pomCicKonflikteDlg->SaveToReg();
		delete pomCicKonflikteDlg;
		pomCicKonflikteDlg = NULL;
	}

	// Unregister DDX call back function
	TRACE("CcaDiagram: DDX Unregistration %s\n",
		::IsWindow(GetSafeHwnd())? "": "(window is already destroyed)");
    ogDdx.UnRegister(this, NOTUSED);

	CFrameWnd::OnDestroy();
}

void CcaDiagram::OnClose() 
{
    // Ignore close -- This makes Alt-F4 keys no effect
	CFrameWnd::OnClose();
}

void CcaDiagram::OnGetMinMaxInfo(MINMAXINFO FAR* lpMMI) 
{
    // TODO: Add your message handler code here and/or call default
    CFrameWnd::OnGetMinMaxInfo(lpMMI);

    lpMMI->ptMaxTrackSize = omMaxTrackSize;
    lpMMI->ptMinTrackSize = omMinTrackSize;
}

BOOL CcaDiagram::OnEraseBkgnd(CDC* pDC) 
{
    // TODO: Add your message handler code here and/or call default
    CRect olClipRect;
    pDC->GetClipBox(&olClipRect);
    //GetClientRect(&olClipRect);

    CBrush olBrush(SILVER/*lmBkColor*/);
    CBrush *polOldBrush = pDC->SelectObject(&olBrush);
    pDC->PatBlt(olClipRect.left, olClipRect.top,
        olClipRect.Width(), olClipRect.Height(),
        PATCOPY);
    pDC->SelectObject(polOldBrush);
    
    return TRUE;
    
    //return CFrameWnd::OnEraseBkgnd(pDC);
}

void CcaDiagram::OnPaint() 
{
    CPaintDC dc(this); // device context for painting
    
    // TODO: Add your message handler code here
    CRect olRect;
    GetClientRect(&olRect);
    
    // draw horizontal line
    CPen olHPen(PS_SOLID, 1, lmHilightColor);
    CPen *polOldPen = dc.SelectObject(&olHPen);
    dc.MoveTo(olRect.left, 27); dc.LineTo(olRect.right, 27);
    dc.MoveTo(olRect.left, 62); dc.LineTo(olRect.right, 62);

    CPen olTPen(PS_SOLID, 1, lmTextColor);
    dc.SelectObject(&olTPen);
    dc.MoveTo(olRect.left, 63); dc.LineTo(olRect.right, 63);
    
    
    // draw vertical line
    dc.SelectObject(&olTPen);
    dc.MoveTo(imStartTimeScalePos - 2, 27);
    dc.LineTo(imStartTimeScalePos - 2, 63);

    dc.SelectObject(&olHPen);
    dc.MoveTo(imStartTimeScalePos - 1, 27);
    dc.LineTo(imStartTimeScalePos - 1, 63);

    dc.SelectObject(polOldPen);
    // Do not call CFrameWnd::OnPaint() for painting messages
}


void CcaDiagram::OnSize(UINT nType, int cx, int cy) 
{
    CFrameWnd::OnSize(nType, cx, cy);
    
    // TODO: Add your message handler code here
    CRect olRect; GetClientRect(&olRect);

    CRect olBB1Rect(olRect.right - 36, 27 + 5, olRect.right - 19, (27 + 5) + 25);
    omBB1.MoveWindow(&olBB1Rect, TRUE);
    
    CRect olBB2Rect(olRect.right - 19, 27 + 5, olRect.right - 2, (27 + 5) + 25);
    omBB2.MoveWindow(&olBB2Rect, TRUE);

    CRect olTSRect(imStartTimeScalePos, 28, olRect.right - (36 + 2), 62);
    omTimeScale.MoveWindow(&olTSRect, TRUE);

    olRect.top += 64;                   // for Dialog Bar & TimeScale
    olRect.bottom -= 20;                // for Horizontal Scroll Bar
    // LeftTop, RightBottom
    omClientWnd.MoveWindow(&olRect, TRUE);

	//PositionChild();
	ChangeViewTo(omViewer.SelectView());//ogCfgData.rmUserSetup.STCV);
//	SetAllStaffAreaButtonsColor();

	GetClientRect(&olRect);
	
//	omTime.MoveWindow( CRect(olRect.right - 192, 3, olRect.right - 90, 22));
//    omDate.MoveWindow( CRect(olRect.right - 88, 3, olRect.right - 8, 22));


}

void CcaDiagram::OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar)
{
    // TODO: Add your message handler code here and/or call default
    
    //CFrameWnd::OnHScroll(nSBCode, nPos, pScrollBar);


	CTime olTSStartTime = omTSStartTime;
 	int ilCurrLine = pomChart->omGantt.GetTopIndex();

    long llTotalMin;
    int ilPos;
    
    switch (nSBCode)
    {
        case SB_LINEUP :
            //OutputDebugString("LineUp\n\r");
            
            llTotalMin = omDuration.GetTotalMinutes() - omTSDuration.GetTotalMinutes();            
            ilPos = GetScrollPos(SB_HORZ) - int (60 * 1000L / llTotalMin);
            if (ilPos <= 0)
            {
                ilPos = 0;
                SetTSStartTime(omStartTime);
            }
            else
                SetTSStartTime(omTSStartTime - CTimeSpan(0, 0, 60, 0));

//rkr25042001
			olTSStartTime = omTSStartTime;
			if(bgGatPosLocal)
				ogBasicData.UtcToLocal(olTSStartTime);

            omTimeScale.SetDisplayStartTime(olTSStartTime);
//            omTimeScale.SetDisplayStartTime(omTSStartTime);

            omTimeScale.Invalidate(TRUE);
			ChangeViewTo(omViewer.SelectView());//ogCfgData.rmUserSetup.STCV,TRUE);
            SetScrollPos(SB_HORZ, ilPos, TRUE);
////////////////////////////////////////////////////////////////////////
// Damkerng 06/27/96:
// This thing should be remove. The technic has been changed to use the
// method Invalidate() not only for the TimeScale, but for the entire
// omClientWnd also.
            omClientWnd.Invalidate(FALSE);
////////////////////////////////////////////////////////////////////////

        break;
        
        case SB_LINEDOWN :
            //OutputDebugString("LineDown\n\r");

            llTotalMin = omDuration.GetTotalMinutes() - omTSDuration.GetTotalMinutes();            
            ilPos = GetScrollPos(SB_HORZ) + int (60 * 1000L / llTotalMin);
            if (ilPos >= 1000)
            {
                ilPos = 1000;
                SetTSStartTime(omStartTime + CTimeSpan(0, 0, int (llTotalMin), 0));
            }
            else
                SetTSStartTime(omTSStartTime + CTimeSpan(0, 0, 60, 0));

//rkr25042001
			olTSStartTime = omTSStartTime;
			if(bgGatPosLocal)
				ogBasicData.UtcToLocal(olTSStartTime);

            omTimeScale.SetDisplayStartTime(olTSStartTime);
//            omTimeScale.SetDisplayStartTime(omTSStartTime);

            omTimeScale.Invalidate(TRUE);
			ChangeViewTo(omViewer.SelectView());//ogCfgData.rmUserSetup.STCV,TRUE);

			SetScrollPos(SB_HORZ, ilPos, TRUE);
////////////////////////////////////////////////////////////////////////
// Damkerng 06/27/96:
// This thing should be remove. The technic has been changed to use the
// method Invalidate() not only for the TimeScale, but for the entire
// omClientWnd also.
            omClientWnd.Invalidate(FALSE);
////////////////////////////////////////////////////////////////////////
        break;
        
        case SB_PAGEUP :
            //OutputDebugString("PageUp\n\r");
            
            llTotalMin = omDuration.GetTotalMinutes() - omTSDuration.GetTotalMinutes();            
            ilPos = GetScrollPos(SB_HORZ)
                - int ((omTSDuration.GetTotalMinutes() / 2) * 1000L / llTotalMin);
            if (ilPos <= 0)
            {
                ilPos = 0;
                SetTSStartTime(omStartTime);
            }
            else
                SetTSStartTime(omTSStartTime - CTimeSpan(0, 0, int (omTSDuration.GetTotalMinutes() / 2), 0));

//rkr25042001
			olTSStartTime = omTSStartTime;
			if(bgGatPosLocal)
				ogBasicData.UtcToLocal(olTSStartTime);

            omTimeScale.SetDisplayStartTime(olTSStartTime);
//            omTimeScale.SetDisplayStartTime(omTSStartTime);

            omTimeScale.Invalidate(TRUE);
			ChangeViewTo(omViewer.SelectView());//ogCfgData.rmUserSetup.STCV,TRUE);

            SetScrollPos(SB_HORZ, ilPos, TRUE);
////////////////////////////////////////////////////////////////////////
// Damkerng 06/27/96:
// This thing should be remove. The technic has been changed to use the
// method Invalidate() not only for the TimeScale, but for the entire
// omClientWnd also.
            omClientWnd.Invalidate(FALSE);
////////////////////////////////////////////////////////////////////////
        break;
        
        case SB_PAGEDOWN :
            //OutputDebugString("PageDown\n\r");
            
            llTotalMin = omDuration.GetTotalMinutes() - omTSDuration.GetTotalMinutes();            
            ilPos = GetScrollPos(SB_HORZ)
                + int ((omTSDuration.GetTotalMinutes() / 2) * 1000L / llTotalMin);
            if (ilPos >= 1000)
            {
                ilPos = 1000;
                SetTSStartTime(omStartTime + CTimeSpan(0, 0, int (llTotalMin), 0));
            }
            else
                SetTSStartTime(omTSStartTime + CTimeSpan(0, 0, int (omTSDuration.GetTotalMinutes() / 2), 0));

//rkr25042001
			olTSStartTime = omTSStartTime;
			if(bgGatPosLocal)
				ogBasicData.UtcToLocal(olTSStartTime);

            omTimeScale.SetDisplayStartTime(olTSStartTime);
//            omTimeScale.SetDisplayStartTime(omTSStartTime);

            omTimeScale.Invalidate(TRUE);
			ChangeViewTo(omViewer.SelectView());//ogCfgData.rmUserSetup.STCV,TRUE);

            SetScrollPos(SB_HORZ, ilPos, TRUE);
////////////////////////////////////////////////////////////////////////
// Damkerng 06/27/96:
// This thing should be remove. The technic has been changed to use the
// method Invalidate() not only for the TimeScale, but for the entire
// omClientWnd also.
            omClientWnd.Invalidate(FALSE);
////////////////////////////////////////////////////////////////////////
        break;
        
        case SB_THUMBTRACK /* pressed, any drag time */:
            //OutputDebugString("ThumbTrack\n\r");

            llTotalMin = omDuration.GetTotalMinutes() - omTSDuration.GetTotalMinutes();
            
            SetTSStartTime(omStartTime + CTimeSpan(0, 0, int (llTotalMin * nPos / 1000), 0));
			////ChangeViewTo(omViewer.SelectView());

//rkr25042001
			olTSStartTime = omTSStartTime;
			if(bgGatPosLocal)
				ogBasicData.UtcToLocal(olTSStartTime);

            omTimeScale.SetDisplayStartTime(olTSStartTime);
//            omTimeScale.SetDisplayStartTime(omTSStartTime);
            
            omTimeScale.Invalidate(TRUE);
            
            SetScrollPos(SB_HORZ, nPos, TRUE);

            //char clBuf[64];
            //wsprintf(clBuf, "Thumb Position : %d", nPos);
            //omStatusBar.SetWindowText(clBuf);
        break;

        //case SB_THUMBPOSITION /* released */:
            //OutputDebugString("ThumbPosition\n\r");
        //break;
////////////////////////////////////////////////////////////////////////
// Damkerng 06/27/96:
// This thing should be remove. The technic has been changed to use the
// method Invalidate() not only for the TimeScale, but for the entire
// omClientWnd also.
        case SB_THUMBPOSITION:	// the thumb was just released?
			ChangeViewTo(omViewer.SelectView());//ogCfgData.rmUserSetup.STCV,TRUE);
            omClientWnd.Invalidate(FALSE);

////////////////////////////////////////////////////////////////////////

        case SB_TOP :
        //break;
        case SB_BOTTOM :
            //OutputDebugString("TopBottom\n\r");
        break;
        
        case SB_ENDSCROLL :
            //OutputDebugString("EndScroll\n\r");
            //OutputDebugString("\n\r");
        break;
    }

 	pomChart->omGantt.SetTopIndex(ilCurrLine);
}


void CcaDiagram::OnTimer(UINT nIDEvent)
{
	/******************
//rkr130301
	if (nIDEvent == 1)
	{
		ogPosDiaFlightData.UpdateBarTimes();
	}
		now done by DDX message KONFLIKTCHECK_FOLLOWS
		*****************************/


	//// Update time line
	if(bmNoUpdatesNow == FALSE)
	{
		CTime olLocalTime = CTime::GetCurrentTime();
		CTime olUtcTime = CTime::GetCurrentTime();
		ogBasicData.LocalToUtc(olUtcTime);

//		char pclTimes[100];
//		char pclDate[100];
		


		CTime olUtcTimeTmp = CTime::GetCurrentTime();
		olUtcTimeTmp -= ogUtcDiff;
/*		
		if(ogUtcDiff != 0)
		{
			sprintf(pclDate, "%02d.%02d.%02d z", olUtcTimeTmp.GetDay(), olUtcTimeTmp.GetMonth(), olUtcTimeTmp.GetYear() % 100);
			sprintf(pclTimes, "%02d:%02d / %02d:%02d z", olLocalTime.GetHour(), olLocalTime.GetMinute(), olUtcTimeTmp.GetHour(), olUtcTimeTmp.GetMinute());
		}
		else
		{
			sprintf(pclDate, "%02d.%02d.%02d z", olUtcTime.GetDay(), olUtcTime.GetMonth(), olUtcTime.GetYear() % 100);
			sprintf(pclTimes, "%02d:%02d / %02d:%02d z", olLocalTime.GetHour(), olLocalTime.GetMinute(), olUtcTime.GetHour(), olUtcTime.GetMinute());
		}
*/
		if(bgGatPosLocal)
			omTimeScale.UpdateCurrentTimeLine(olLocalTime);
		else
			omTimeScale.UpdateCurrentTimeLine(olUtcTime);

/* 
		omTime.SetWindowText(pclTimes);
		omTime.Invalidate(FALSE);

		omDate.SetWindowText(pclDate);
		omDate.Invalidate(FALSE);
*/
		if(bgGatPosLocal)
			pomChart->GetGanttPtr()->SetCurrentTime(olLocalTime);
		else
			pomChart->GetGanttPtr()->SetCurrentTime(olUtcTime);

		CFrameWnd::OnTimer(nIDEvent);
	}
}

void CcaDiagram::SetMarkTime(CTime opStartTime, CTime opEndTime)
{
		pomChart->SetMarkTime(opStartTime, opEndTime);
}


LONG CcaDiagram::OnPositionChild(WPARAM wParam, LPARAM lParam)
{
    PositionChild();
    return 0L;
}

LONG CcaDiagram::OnUpdateDiagram(WPARAM wParam, LPARAM lParam)
{
	UpdateWoResButton();
	UpdateWoDemButton();
	UpdateConfButton();

    CString olStr;

    int ilGroupNo = HIWORD(lParam);
    int ilLineNo = LOWORD(lParam);

	int ilItemID = omViewer.GetItemID(ilGroupNo, ilLineNo);

	CTime olTime = CTime((time_t) lParam);

	char clBuf[255];
	int ilCount;

    CcaGantt *polCcaGantt = pomChart -> GetGanttPtr();
    switch (wParam)
    {
        // group message

        case UD_UPDATEGROUP :
            olStr = omViewer.GetGroupText(ilGroupNo);
            pomChart->GetChartButtonPtr()->SetWindowText(olStr);
            pomChart->GetChartButtonPtr()->Invalidate(FALSE);

            olStr = omViewer.GetGroupTopScaleText(ilGroupNo);
            pomChart->GetTopScaleTextPtr()->SetWindowText(olStr);
            pomChart->GetTopScaleTextPtr()->Invalidate(TRUE);
			//SetStaffAreaButtonColor(ilGroupNo);
        break;
        

        // line message
        case UD_INSERTLINE :
            polCcaGantt->InsertString(ilItemID, "");
			polCcaGantt->RepaintItemHeight(ilItemID);
			
			ilCount = omViewer.GetLineCount(ilGroupNo);
			sprintf(clBuf, "%d", ilCount);
			pomChart->GetCountTextPtr()->SetWindowText(clBuf);
			pomChart->GetCountTextPtr()->Invalidate(TRUE);

			//SetStaffAreaButtonColor(ilGroupNo);
            PositionChild();
        break;
        
        case UD_UPDATELINE :
            polCcaGantt->RepaintVerticalScale(ilItemID);
            polCcaGantt->RepaintGanttChart(ilItemID);
			//SetStaffAreaButtonColor(ilGroupNo);
        break;

        case UD_DELETELINE :
            polCcaGantt->DeleteString(ilItemID);
            
			ilCount = omViewer.GetLineCount(ilGroupNo);
			sprintf(clBuf, "%d", ilCount);
			pomChart->GetCountTextPtr()->SetWindowText(clBuf);
			pomChart->GetCountTextPtr()->Invalidate(TRUE);

			//SetStaffAreaButtonColor(ilGroupNo);
			PositionChild();
        break;

        case UD_UPDATELINEHEIGHT :
            polCcaGantt->RepaintItemHeight(ilItemID);
			//SetStaffAreaButtonColor(ilGroupNo);
            PositionChild();
		break;
    }

    return 0L;
/*
    CString olStr;
    int ipGroupNo = HIWORD(lParam);
    int ipLineNo = LOWORD(lParam);
	CTime olTime = CTime((time_t) lParam);

	char clBuf[255];
	int ilCount;

    CcaChart *polCcaChart = (CcaChart *) omPtrArray.GetAt(ipGroupNo);
    CcaGantt *polCcaGantt = polCcaChart -> GetGanttPtr();
    switch (wParam)
    {
        // group message
        case UD_INSERTGROUP :
            // CreateChild(ipGroupNo);

            if (ipGroupNo <= imFirstVisibleChart)
			{
                imFirstVisibleChart++;
				OnUpdatePrevNext();
			}
            PositionChild();
        break;

        case UD_UPDATEGROUP :
            olStr = omViewer.GetGroupText(ipGroupNo);
            polCcaChart->GetChartButtonPtr()->SetWindowText(olStr);
            polCcaChart->GetChartButtonPtr()->Invalidate(FALSE);

            olStr = omViewer.GetGroupTopScaleText(ipGroupNo);
            polCcaChart->GetTopScaleTextPtr()->SetWindowText(olStr);
            polCcaChart->GetTopScaleTextPtr()->Invalidate(TRUE);
			//SetStaffAreaButtonColor(ipGroupNo);
        break;
        
        case UD_DELETEGROUP :
            delete omPtrArray.GetAt(ipGroupNo);
            //omPtrArray.GetAt(ipGroupNo) -> DestroyWindow();
            omPtrArray.RemoveAt(ipGroupNo);
            
            if (ipGroupNo <= imFirstVisibleChart)
			{
                imFirstVisibleChart--;
				OnUpdatePrevNext();
			}
            PositionChild();
        break;

        // line message
        case UD_INSERTLINE :
            polCcaGantt->InsertString(ipLineNo, "");
			polCcaGantt->RepaintItemHeight(ipLineNo);
			
			ilCount = omViewer.GetLineCount(ipGroupNo);
			sprintf(clBuf, "%d", ilCount);
			polCcaChart->GetCountTextPtr()->SetWindowText(clBuf);
			polCcaChart->GetCountTextPtr()->Invalidate(TRUE);

			//SetStaffAreaButtonColor(ipGroupNo);
            PositionChild();
        break;
        
        case UD_UPDATELINE :
            polCcaGantt->RepaintVerticalScale(ipLineNo);
            polCcaGantt->RepaintGanttChart(ipLineNo);
            polCcaGantt->RepaintItemHeight(ipLineNo);
			//SetStaffAreaButtonColor(ipGroupNo);
        break;

        case UD_DELETELINE :
            polCcaGantt->DeleteString(ipLineNo);
            
			ilCount = omViewer.GetLineCount(ipGroupNo);
			sprintf(clBuf, "%d", ilCount);
			polCcaChart->GetCountTextPtr()->SetWindowText(clBuf);
			polCcaChart->GetCountTextPtr()->Invalidate(TRUE);

			//SetStaffAreaButtonColor(ipGroupNo);
			PositionChild();
        break;

        case UD_UPDATELINEHEIGHT :
            polCcaGantt->RepaintItemHeight(ipLineNo);
			//SetStaffAreaButtonColor(ipGroupNo);
            PositionChild();
		break;
    }

    return 0L;
*/
}

///////////////////////////////////////////////////////////////////////////////
// Damkerng and Pichet: Date: 5 July 1995

void CcaDiagram::UpdateComboBox()
{
	CComboBox *polCB = (CComboBox *) omDialogBar.GetDlgItem(IDC_VIEW);
	polCB->ResetContent();
	CStringArray olStrArr;
	omViewer.GetViews(olStrArr);
	for (int ilIndex = 0; ilIndex < olStrArr.GetSize(); ilIndex++)
	{
		polCB->AddString(olStrArr[ilIndex]);
	}
	CString olViewName = omViewer.GetViewName();
	if (olViewName.IsEmpty() == TRUE)
	{

		olViewName = omViewer.SelectView();//ogCfgData.rmUserSetup.STCV;
	}

	ilIndex = polCB->FindString(-1,olViewName);
		
	if (ilIndex != CB_ERR)
	{
		polCB->SetCurSel(ilIndex);
	}
}

void CcaDiagram::ChangeViewTo(const char *pcpViewName,bool RememberPositions)
{
	UpdateWoResButton();
	UpdateWoDemButton();
	UpdateConfButton();

	if (bgNoScroll == TRUE)
		return;

	if (pomChart != NULL)
	{
		if (!pomChart->omGantt.bmRepaint)
		{
			bmRepaintAll = true;
			return;
		}
    }
			

	bool blHasFocus = false;

	if (pomChart->GetGanttPtr() != NULL)
	{
		if(	pomChart->GetGanttPtr()->GetFocus() == pomChart->GetGanttPtr() )
			blHasFocus = true;

	}


	int ilDwRef = m_dwRef;
	AfxGetApp()->DoWaitCursor(1);
	bmNoUpdatesNow = TRUE;
	omViewer.AllowUpdates(bmNoUpdatesNow);

	CCSPtrArray <int> olChartStates;
	olChartStates.RemoveAll();

	CRect olRect; omTimeScale.GetClientRect(&olRect);

//rkr25042001
	//Set begin to local if local
	CTime olTSStartTime = omTSStartTime;
	if(bgGatPosLocal)
		ogBasicData.UtcToLocal(olTSStartTime);
    omViewer.ChangeViewTo(pcpViewName, olTSStartTime, omTimeScale.GetTimeFromX(olRect.Width() + 38));

	int ilChartState;
	int ilTopIndex;
 	if (RememberPositions == TRUE && pomChart != NULL)
	{
		ilChartState = pomChart->GetState();
		if (pomChart->GetGanttPtr() != NULL)
			ilTopIndex = pomChart->GetGanttPtr()->GetTopIndex();
	}


    CRect olPrevRect;
    omClientWnd.GetClientRect(&olRect);
    
	if (RememberPositions == FALSE)
	{
	    imFirstVisibleChart = 0;
	}
    
	int ilLastY = 0;

	pomChart->DestroyWindow();
    omClientWnd.GetClientRect(&olRect);
        
	olRect.SetRect(olRect.left, 0, olRect.right, 0);
        
    pomChart = new CcaChart;
    pomChart->SetTimeScale(&omTimeScale);
    pomChart->SetViewer(&omViewer);
    pomChart->SetStatusBar(&omStatusBar);
    pomChart->SetStartTime(omTSStartTime);
    pomChart->SetInterval(omTimeScale.GetDisplayDuration());
 	if (RememberPositions == TRUE)
	{
		pomChart->SetState(ilChartState);
	}
	else
	{
		pomChart->SetState(Maximized);
	}

    pomChart->Create(NULL, "CcaChart", WS_OVERLAPPED | WS_BORDER | WS_CHILD | WS_VISIBLE,
            olRect, &omClientWnd, 0 /* IDD_CHART */, NULL);


 	if (RememberPositions == TRUE)
	{
		if (pomChart->GetGanttPtr() != NULL)
 			pomChart->GetGanttPtr()->SetTopIndex(ilTopIndex);
	}


	CTime olCurr = CTime::GetCurrentTime();
	//Die wollen local
 	if(!bgGatPosLocal)
		ogBasicData.LocalToUtc(olCurr);
	if (pomChart->GetGanttPtr() != NULL)
	{
 			pomChart->GetGanttPtr()->SetTopIndex(ilTopIndex);
			pomChart->GetGanttPtr()->SetCurrentTime(olCurr);
//			pomChart->GetGanttPtr()->SetFonts(igFontIndex1, igFontIndex2);

	}
        
	if (pomCicKonflikteDlg)
		pomCicKonflikteDlg->ChangeViewTo();
 
	PositionChild();
	AfxGetApp()->DoWaitCursor(-1);
	bmNoUpdatesNow = FALSE;
	omViewer.AllowUpdates(bmNoUpdatesNow);

	UpdateWoResButton();
	UpdateWoDemButton();
	UpdateConfButton();

	if (pomChart->GetGanttPtr() != NULL && blHasFocus)
 			pomChart->GetGanttPtr()->SetFocus();

}

void CcaDiagram::OnGatposConf()
{	
	if (pomCicKonflikteDlg != NULL)
//		pomCicKonflikteDlg->Attention(CString("CCA"));
		pomCicKonflikteDlg->Attention(CONF_CCA);
}

void CcaDiagram::OnViewSelChange()
{
//	UpdateWoResButton();

		if(ogCcaDiaFlightData.bmOffLine)
		{
			ogCcaDiaFlightData.bmOffLine = false;
			ogCcaDiaFlightData.omCcaData.bmOffLine = false;
			ogCcaDiaFlightData.UpdateOfflineButtons();
		}




    char clText[64];
    CComboBox *polCB = (CComboBox *) omDialogBar.GetDlgItem(IDC_VIEW);
    polCB->GetLBText(polCB->GetCurSel(), clText);
	if(strcmp(clText, "<Default>") == 0)
		return;

	ogSpotAllocation.InitializePositionMatrix();

	LoadFlights(clText);
	omViewer.SelectView(clText);
	omViewer.MakeMasstab();
	CTime olT = omViewer.GetGeometrieStartTime();

	if(	bgCcaDiaSeason )
		olT = ogCcaDiaDate;


	CTime olFrom;
	CTime olTo;
	CString olFtyps;
	omViewer.GetTimeSpanAndFtyp(olFrom, olTo, olFtyps);

	//if((olFrom > olT) || (olTo < olT))
	//	olT = olFrom;

	CString olS = olT.Format("%d.%m.%Y-%H:%M");
	SetTSStartTime(olT);
	omTSDuration = omViewer.GetGeometryTimeSpan();

    
	long llTotalMin = omDuration.GetTotalMinutes() - omTSDuration.GetTotalMinutes();
	long nPos = (omTSStartTime - omStartTime).GetTotalMinutes();
	nPos = nPos * 1000L / llTotalMin;
	SetScrollPos(SB_HORZ, int(nPos), TRUE);
	
//rkr
	//Set begin to local if local
	CTime olTSStartTime = omTSStartTime;
	if(bgGatPosLocal)
		ogBasicData.UtcToLocal(olTSStartTime);
    
	omTimeScale.SetDisplayTimeFrame(olTSStartTime, omTSDuration, omTSInterval);
//	omTimeScale.SetDisplayTimeFrame(omTSStartTime, omTSDuration, omTSInterval);
    omTimeScale.Invalidate(TRUE);
	SetTSStartTime(omTSStartTime);
	ChangeViewTo(clText, false);

	ActivateTables();
}


void CcaDiagram::OnCloseupView() 
{
	// Id 25-Sep-96
	// This will set the keyboard focus back to the bottom most list-box after this button was clicked.
	//MWO
	//SetFocus();
	//END MWO
}

void CcaDiagram::OnZeit()
{
	// Id 25-Sep-96
	// This will set the keyboard focus back to the bottom most list-box after this button was clicked.
	//MWO
	//SetFocus();
	//END MWO

	// set new start time
	CTime olTime = CTime::GetCurrentTime();	
	ogBasicData.LocalToUtc(olTime);
	olTime -= CTimeSpan(0, 1, 0, 0);
	SetTSStartTime(olTime);

	// adjust timescale
	olTime = omTSStartTime;
	if(bgGatPosLocal)
		ogBasicData.UtcToLocal(olTime);
    omTimeScale.SetDisplayStartTime(olTime);
	//omTimeScale.SetDisplayStartTime(omTSStartTime);
    omTimeScale.Invalidate(TRUE);
		
	// update scroll bar position
    long llTotalMin = omDuration.GetTotalMinutes() - omTSDuration.GetTotalMinutes();
	long nPos = (omTSStartTime - omStartTime).GetTotalMinutes();
	nPos = nPos * 1000L / llTotalMin;
    SetScrollPos(SB_HORZ, int(nPos), TRUE);

	ChangeViewTo(omViewer.SelectView());//ogCfgData.rmUserSetup.STCV);
    omClientWnd.Invalidate(FALSE);
}


BOOL CcaDiagram::DestroyWindow() 
{

	if ((bmIsViewOpen))
	{
		MessageBeep((UINT)-1);
		return FALSE;    // don't destroy window while view property sheet is still open
	}
	pogCcaDiagram = NULL; 

	if (pogCommonCcaTable != NULL)
	{
		pogCommonCcaTable->SaveToReg();
		delete pogCommonCcaTable;
		pogCommonCcaTable = NULL;
	}
	ogDdx.UnRegister(&omViewer, NOTUSED);
	bmNoUpdatesNow = TRUE;
	omViewer.AllowUpdates(bmNoUpdatesNow);
	BOOL blRc = CWnd::DestroyWindow();
	return blRc;
}

/////////XXXXX
////////////////////////////////////////////////////////////////////////
// CcaDiagram keyboard handling

void CcaDiagram::OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags) 
{
	// check if the control key is pressed
    BOOL blIsControl = ::GetKeyState(VK_CONTROL) & 0x8080;
		// This statement has to be fixed for using both in Windows 3.11 and NT.
		// In Windows 3.11 the bit 0x80 will be turned on if control key is pressed.
		// In Windows NT, the bit 0x8000 will be turned on if control key is pressed.

	switch (nChar)
	{
	case VK_LEFT:
		OnHScroll(blIsControl? SB_PAGEUP: SB_LINEUP, 0, NULL);
		break;
	case VK_RIGHT:
		OnHScroll(blIsControl? SB_PAGEDOWN: SB_LINEDOWN, 0, NULL);
		break;
	case VK_HOME:
		SetScrollPos(SB_HORZ, 0, FALSE);
		OnHScroll(SB_LINEUP, 0, NULL);
		break;
	case VK_END:
		SetScrollPos(SB_HORZ, 1000, FALSE);
		OnHScroll(SB_LINEDOWN, 0, NULL);
		break;
	default:
		omDialogBar.SendMessage(WM_KEYDOWN, nChar, MAKELONG(LOWORD(nRepCnt), LOWORD(nFlags)));
		CFrameWnd::OnKeyDown(nChar, nRepCnt, nFlags);
		break;
	}
}

void CcaDiagram::OnKeyUp(UINT nChar, UINT nRepCnt, UINT nFlags) 
{
	CFrameWnd::OnKeyUp(nChar, nRepCnt, nFlags);
}

////////////////////////////////////////////////////////////////////////
// CcaDiagram -- implementation of DDX call back function

static void CcaDiagramCf(void *popInstance, enum enumDDXTypes ipDDXType,
    void *vpDataPointer, CString &ropInstanceName)
{
	CcaDiagram *polDiagram = (CcaDiagram *)popInstance;

	switch (ipDDXType)
	{
	case REDISPLAY_ALL:
		polDiagram->RedisplayAll();
		break;
	case STAFFDIAGRAM_UPDATETIMEBAND:
		TIMEPACKET *polTimePacket = (TIMEPACKET *)vpDataPointer;
		polDiagram->SetTimeBand(polTimePacket->StartTime, polTimePacket->EndTime);
		polDiagram->UpdateTimeBand();
		break;
	}
}

void CcaDiagram::RedisplayAll()
{
	ChangeViewTo(omViewer.SelectView());//ogCfgData.rmUserSetup.STCV);
}

void CcaDiagram::SetTimeBand(CTime opStartTime, CTime opEndTime)
{
	omTimeBandStartTime = opStartTime;
	omTimeBandEndTime = opEndTime;
}

void CcaDiagram::UpdateTimeBand()
{
	pomChart->omGantt.SetMarkTime(omTimeBandStartTime, omTimeBandEndTime);
}

LONG CcaDiagram::RepaintAll(WPARAM wParam, LPARAM lParam)
{
	ChangeViewTo(omViewer.GetViewName(), true);
	return 0L;
}






void CcaDiagram::OnExpand()
{
	CCAExpandDlg olDlg;
	olDlg.DoModal();

}
void CcaDiagram::OnDeleteTimeFrame()
{
	DelelteCounterDlg *polDlg = new DelelteCounterDlg(this);
	if(polDlg->DoModal() == IDOK)
	{
		OnViewSelChange();
	}
	delete polDlg;
}

void CcaDiagram::OnPrintGantt()
{
	omViewer.PrintGantt(pomChart);
}

void CcaDiagram::OnAllocate()
{

	if (bgOffRelFuncCheckin && !ogCcaDiaFlightData.bmOffLine)
	{
		MessageBox(GetString(IDS_STRING2003), GetString(ST_FEHLER), MB_OK | MB_ICONERROR);
		return;
	}


	int ilRet = 0;
	AllocateCcaParameter *polDlg = new AllocateCcaParameter(this);

	ilRet = polDlg->DoModal();
	delete polDlg;
	
	if(ilRet == -1)
		return;

	AllocateCca olAlloc(ilRet);
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
	omStatusBar.SetPaneText(0, GetString(IDS_STRING1492)); // Einteilung l�uft---------------------
	omStatusBar.UpdateWindow();
	olAlloc.Allocate();
	omStatusBar.SetPaneText(0, GetString(IDS_STRING1493)); // Speichern des Plans---------------------
	omStatusBar.UpdateWindow();

	TRACE("\n-----------------------------------------------");
	TRACE("\nBegin Save Allo: %s", CTime::GetCurrentTime().Format("%H:%M:%S"));


	ogCcaDiaFlightData.omCcaData.Release(NULL);

	TRACE("\nEnd Save Allo:   %s", CTime::GetCurrentTime().Format("%H:%M:%S"));
	TRACE("\n-----------------------------------------------");

	ogCcaDiaFlightData.CheckAllFlightsBazFields();

	if(bgCcaDiaSeason)
	{

		TRACE("\n-----------------------------------------------");
		TRACE("\nBegin compress: %s", CTime::GetCurrentTime().Format("%H:%M:%S"));
	
		ogCcaDiaFlightData.Kompress();
		ogCcaDiaFlightData.omCcaData.KompressND();

		TRACE("\nEnd compress:   %s", CTime::GetCurrentTime().Format("%H:%M:%S"));
		TRACE("\n-----------------------------------------------");
	
	}

	TRACE("\n-----------------------------------------------");
	TRACE("\nBegin repaint: %s", CTime::GetCurrentTime().Format("%H:%M:%S"));


	RepaintAll(0,0);

	TRACE("\nend repaint:   %s", CTime::GetCurrentTime().Format("%H:%M:%S"));
	TRACE("\n-----------------------------------------------");


	TRACE("\n-----------------------------------------------");
	TRACE("\nBegin tables: %s", CTime::GetCurrentTime().Format("%H:%M:%S"));

	
	ActivateTables();
	
	TRACE("\nEnd tables:   %s", CTime::GetCurrentTime().Format("%H:%M:%S"));
	TRACE("\n-----------------------------------------------");
	
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));

}



void CcaDiagram::OnCommonCca()
{

	CTime olFrom;
	CTime olTo;
	CString olFtyps;
	omViewer.GetTimeSpanAndFtyp(olFrom, olTo, olFtyps);


	if (pogCommonCcaTable == NULL)
	{
		pogCommonCcaTable = new CCcaCommonTableDlg(this, olFrom, olTo);
	}
	else
	{
		pogCommonCcaTable->SetTimeFrame(olFrom, olTo);
		pogCommonCcaTable->SetWindowPos(&wndTop, 0,0,0,0, SWP_SHOWWINDOW | SWP_NOSIZE | SWP_NOMOVE );
	}

}


void CcaDiagram::SetWndPos()
{
	CRect olRect;

	GetWindowRect(&olRect);

	int ilWidth = olRect.right -olRect.left;
	int ilWhichMonitor = ogCfgData.GetMonitorForWindow(CString(MON_CCADIA_STRING));
	int ilMonitors = ogCfgData.GetMonitorForWindow(CString(MON_COUNT_STRING));
	int left, top, right, bottom;
	int ilCXMonitor;

	bottom = ::GetSystemMetrics(SM_CYSCREEN);

	top = 0;  


	imWhichMonitor = ilWhichMonitor;
	imMonitorCount = ilMonitors;

	if(ilWhichMonitor == 1)
	{
		switch(ilMonitors)
		{
		case 1:
			ilCXMonitor = ::GetSystemMetrics(SM_CXSCREEN);
			left = (int)((ilCXMonitor-ilWidth)/2);
			right = left + ilWidth;//GetSystemMetrics(SM_CXSCREEN);
			break;
		case 2:
			ilCXMonitor = (int)(::GetSystemMetrics(SM_CXSCREEN)/2);
			left = (int)((ilCXMonitor-ilWidth)/2);
			right = left + ilWidth;//GetSystemMetrics(SM_CXSCREEN);
			break;
		case 3:
			ilCXMonitor = (int)(::GetSystemMetrics(SM_CXSCREEN)/3);
			left = (int)((ilCXMonitor-ilWidth)/2);
			right = left + ilWidth;//GetSystemMetrics(SM_CXSCREEN);
			break;
		default:
			ilCXMonitor = ::GetSystemMetrics(SM_CXSCREEN);
			left = (int)((ilCXMonitor-ilWidth)/2);
			right = left + ilWidth;//GetSystemMetrics(SM_CXSCREEN);
			break;
		}
	}
	else if(ilWhichMonitor == 2)
	{
		switch(ilMonitors)
		{
		case 1:
			ilCXMonitor = ::GetSystemMetrics(SM_CXSCREEN);
			left = (int)((ilCXMonitor-ilWidth)/2);
			right = left + ilWidth;//GetSystemMetrics(SM_CXSCREEN);
			break;
		case 2:
			ilCXMonitor = (int)(::GetSystemMetrics(SM_CXSCREEN)/2);
			left = (int)((ilCXMonitor-ilWidth)/2)+(ilCXMonitor);
			right = left + ilWidth;//GetSystemMetrics(SM_CXSCREEN);
			break;
		case 3:
			ilCXMonitor = (int)(::GetSystemMetrics(SM_CXSCREEN)/3);
			left = (int)((ilCXMonitor-ilWidth)/2)+(ilCXMonitor);
			right = left + ilWidth;//GetSystemMetrics(SM_CXSCREEN);
			break;
		default:
			right = GetSystemMetrics(SM_CXSCREEN);
			break;
		}
	}
	else if(ilWhichMonitor == 3)
	{
		switch(ilMonitors)
		{
		case 1:
			ilCXMonitor = ::GetSystemMetrics(SM_CXSCREEN);
			left = (int)((ilCXMonitor-ilWidth)/2);
			right = left + ilWidth;//GetSystemMetrics(SM_CXSCREEN);
			break;
		case 2:
			ilCXMonitor = (int)(::GetSystemMetrics(SM_CXSCREEN)/2);
			left = (int)((ilCXMonitor-ilWidth)/2);
			right = left + ilWidth;//GetSystemMetrics(SM_CXSCREEN);
			break;
		case 3:
			ilCXMonitor = (int)(::GetSystemMetrics(SM_CXSCREEN)/3);
			left = (int)((ilCXMonitor-ilWidth)/2)+(2*ilCXMonitor);
			right = left + ilWidth;//GetSystemMetrics(SM_CXSCREEN);
			break;
		default:
			right = GetSystemMetrics(SM_CXSCREEN);
			break;
		}
	}

	MoveWindow(CRect(left, top, right, ::GetSystemMetrics(SM_CYSCREEN)));


	//SetWindowPos(&wndTop,left , top, 0, 0, SWP_SHOWWINDOW | SWP_NOSIZE);



}

void CcaDiagram::OnChanges()
{
	CallHistory(CString("CCACHECKIN"));
}


void CcaDiagram::OnFlightsWithoutResource()
{
	pogCicNoDemandTableDlg->ShowWindow(SW_SHOW);
	pogCicNoDemandTableDlg->SetWindowPos(&wndTop, 0,0,0,0, SWP_SHOWWINDOW | SWP_NOSIZE | SWP_NOMOVE );
}


void CcaDiagram::OnDemand()
{
	pogCicDemandTableDlg->ShowWindow(SW_SHOW);
	pogCicDemandTableDlg->SetWindowPos(&wndTop, 0,0,0,0, SWP_SHOWWINDOW | SWP_NOSIZE | SWP_NOMOVE );

}


void CcaDiagram::OnCicConf()
{
	pogCicConfTableDlg->ShowWindow(SW_SHOW);
	pogCicConfTableDlg->SetWindowPos(&wndTop, 0,0,0,0, SWP_SHOWWINDOW | SWP_NOSIZE | SWP_NOMOVE );

}



void CcaDiagram::OnSeasonViewSelChange()
{
	UpdateWoResButton();
	UpdateWoDemButton();
	UpdateConfButton();

	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
    
	CString olText;
    CComboBox *polCB = (CComboBox *) omDialogBar.GetDlgItem(IDC_SEASON_SEL);
    
	polCB->GetLBText(polCB->GetCurSel(), olText);
	

	if(olText == CString(omViewer.GetViewName()))
	{
		ogCcaDiaSingleDate = TIMENULL;
		omTSStartTime = CTime(ogCcaDiaDate.GetYear(),ogCcaDiaDate.GetMonth(), ogCcaDiaDate.GetDay(), 0,0,0 );
		ogCcaDiaFlightData.Kompress();
		ogCcaDiaFlightData.omCcaData.KompressND();
	}
	else
	{
		ogCcaDiaSingleDate = DateStringToDate(olText);
		omTSStartTime = CTime(ogCcaDiaSingleDate.GetYear(),ogCcaDiaSingleDate.GetMonth(), ogCcaDiaSingleDate.GetDay(), 0,0,0 );
	}
	

	//omViewer.MakeMasstab();
	//CTime olT = omViewer.GetGeometrieStartTime();


	SetTSStartTime(omTSStartTime);
	omTSDuration = omViewer.GetGeometryTimeSpan();

    
	long llTotalMin = omDuration.GetTotalMinutes() - omTSDuration.GetTotalMinutes();
	long nPos = (omTSStartTime - omStartTime).GetTotalMinutes();
	nPos = nPos * 1000L / llTotalMin;
	SetScrollPos(SB_HORZ, int(nPos), TRUE);
	
//rkr
	//Set begin to local if local
	CTime olTSStartTime = omTSStartTime;
	if(bgGatPosLocal)
		ogBasicData.UtcToLocal(olTSStartTime);
    
	omTimeScale.SetDisplayTimeFrame(olTSStartTime, omTSDuration, omTSInterval);
//	omTimeScale.SetDisplayTimeFrame(omTSStartTime, omTSDuration, omTSInterval);
    omTimeScale.Invalidate(TRUE);
	SetTSStartTime(omTSStartTime);
	ChangeViewTo(omViewer.SelectView(), false);

	ActivateTables();

	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));

}

void CcaDiagram::ActivateTables()
{
	pogCicDemandTableDlg->Reset();
	pogCicDemandTableDlg->Activate();

	pogCicNoDemandTableDlg->Reset();
	pogCicNoDemandTableDlg->Activate();
	
	pogCicConfTableDlg->Reset();
	pogCicConfTableDlg->Activate();
}

void CcaDiagram::OnOffline()
{	
	ogCcaDiaFlightData.ToggleOffline();
}

void CcaDiagram::UpdateChangesButton(CString& opText, COLORREF opColor)
{
	CString olTmp;
	olTmp.Format("%s (%s)", GetString(IDS_CHANGES),opText);

	m_CB_Changes.SetWindowText(olTmp);
	m_CB_Changes.SetColors(opColor, ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
	m_CB_Changes.UpdateWindow();
}


void CcaDiagram::UpdateWoResButton()
{
	if (bmReadAll)
		return;

	CString olTmp;
	COLORREF olButtonColor;

	int ilCount = pogCicDemandTableDlg->GetDemCount();
	if(ilCount > 0)
	{
		if(bgFwrCic)
			olButtonColor = ogColors[IDX_ORANGE];
		else
			olButtonColor = ::GetSysColor(COLOR_BTNFACE);

		olTmp.Format("%s (%d)", GetString(IDS_STRING2444),ilCount);
	}
	else
	{
		olTmp.Format("%s (%d)", GetString(IDS_STRING2444),0);
		olButtonColor = ::GetSysColor(COLOR_BTNFACE);
	}

	m_CB_WoRes.SetWindowText(olTmp);
	m_CB_WoRes.SetColors(olButtonColor, ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
	m_CB_WoRes.UpdateWindow();
}

void CcaDiagram::UpdateWoDemButton()
{
	if (bmReadAll)
		return;

	CString olTmp;
	COLORREF olButtonColor;

	int ilCount = pogCicNoDemandTableDlg->GetCount();
	if(ilCount > 0)
	{
		olButtonColor = ogColors[IDX_ORANGE];
		olTmp.Format("%s (%d)", GetString(IDS_STRING2445),ilCount);

	}
	else
	{
		olTmp.Format("%s (%d)", GetString(IDS_STRING2445),0);
		olButtonColor = ::GetSysColor(COLOR_BTNFACE);
	}

	m_CB_WoDem.SetWindowText(olTmp);
	m_CB_WoDem.SetColors(olButtonColor, ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
	m_CB_WoDem.UpdateWindow();
}

void CcaDiagram::UpdateConfButton()
{
	if (bmReadAll)
		return;

	CString olTmp;
	COLORREF olButtonColor;

	int ilConf = 0;
	int ilWoDem = 0;
	pogCicConfTableDlg->GetCount(ilConf, ilWoDem);

	if(ilWoDem > 0)
		olButtonColor = ogColors[IDX_ORANGE];
	else if (ilConf > 0)
		olButtonColor = ogColors[IDX_RED];
	else
		olButtonColor = ::GetSysColor(COLOR_BTNFACE);

	olTmp.Format("%s (%d/%d)", GetString(IDS_STRING2216),ilConf,ilWoDem);

	m_CB_Conf.SetWindowText(olTmp);
	m_CB_Conf.SetColors(olButtonColor, ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
	m_CB_Conf.UpdateWindow();
}

void CcaDiagram::UpdateKonflikteDlg()
{	
	if (pomCicKonflikteDlg != NULL)
//		pomCicKonflikteDlg->UpdateDisplay();
		pomCicKonflikteDlg->ChangeViewTo();
}

