#ifndef __REPORT_SAME_DTD_TABLE_VIEWER_H__
#define __REPORT_SAME_DTD_TABLE_VIEWER_H__

#include "stdafx.h"
#include "RotationDlgCedaFlightData.h"
#include "CCSTable.h"
#include "CViewer.h"
#include "CCSPrint.h"


struct SAMEDTDTABLE_LINEDATA
{
	long AUrno;
	long ARkey;
	CString		ADate;
	CTime 		AEtai; 	
	CString		AFlno;
	CTime 		AOnbl; 	
	CString		AOrg3;
	CString		AOrg4;
	CTime		AStoa;
	CString		AVia3;
	CString 	AVian;
	CString		AAct;
	CString 	ADtd1;
	CString		ADcd1;


	long DUrno;
	long DRkey;
	CString		DAct;
	CTime 		DOfbl;
	CString		DDate;
	CString		DDes3;
	CString		DDes4;
	CTime 		DEtdi;
	CString		DFlno;
	CTime		DStod;
	CString		DVia3;
	CString 	DVian;
	CString 	DDtd1;
	CString		DDcd1;


	CString		Adid;

};

/////////////////////////////////////////////////////////////////////////////
// ReportSameDTDTableViewer

class ReportSameDTDTableViewer : public CViewer
{
// Constructions
public:
    ReportSameDTDTableViewer(CCSPtrArray<ROTATIONDLGFLIGHTDATA> *popData, char *pcpInfo = NULL, char *pcpSelect = NULL);
    ~ReportSameDTDTableViewer();

    void Attach(CCSTable *popAttachWnd);
	void SetParentDlg(CDialog* ppDlg);
    virtual void ChangeViewTo(const char *pcpViewName);
	void PrintPlanToFile(char *pcpDefPath);
// Internal data processing routines
private:
    void MakeLines();
	int  MakeLine(ROTATIONDLGFLIGHTDATA *prpAFlight, ROTATIONDLGFLIGHTDATA *prpDFlight, int ipDCodeNum);
	void MakeLineData(ROTATIONDLGFLIGHTDATA *prpAFlight, ROTATIONDLGFLIGHTDATA *prpDFlight, SAMEDTDTABLE_LINEDATA &rpLine, int ipDCodeNum);
	void MakeColList(SAMEDTDTABLE_LINEDATA *prlLine, CCSPtrArray<TABLE_COLUMN> &olColList);
	int  CompareGefundenefluege(SAMEDTDTABLE_LINEDATA *prpGefundenefluege1, SAMEDTDTABLE_LINEDATA *prpGefundenefluege2);
	int  CompareFlight(SAMEDTDTABLE_LINEDATA *prpFlight1, SAMEDTDTABLE_LINEDATA *prpFlight2);

// Operations
public:
	void DeleteAll();
	int CreateLine(SAMEDTDTABLE_LINEDATA &prpGefundenefluege);
	void DeleteLine(int ipLineno);
// Window refreshing routines
public:
	void UpdateDisplay();
	void DrawHeader();


// Attributes used for filtering condition
private:
	bool bmIsFromSearch;

// Attributes
private:
    CCSTable *pomTable;
	CDialog* pomParentDlg;
	CCSPtrArray<ROTATIONDLGFLIGHTDATA> *pomData;

public:
    CCSPtrArray<SAMEDTDTABLE_LINEDATA> omLines;
	CCSPtrArray<TABLE_HEADER_COLUMN> omHeaderDataArray;
//Print 
	void GetHeader(void); 
	void PrintTableView(void);
	bool PrintTableLine(SAMEDTDTABLE_LINEDATA *prpLine,bool bpLastLine);
	bool PrintTableHeader(void);
	CCSPtrArray <TABLE_HEADER_COLUMN> omPrintHeadHeaderArray;
	CCSPrint *pomPrint;
	CString omTableName;
	char *pcmInfo;
	char *pcmSelect;
	CString omFooterName;

};

#endif //__ReportSameDTDTableViewer_H__
