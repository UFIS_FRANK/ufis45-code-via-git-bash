// HEADER 
#ifndef _CEDACFLDATA_H_ 
#define _CEDACFLDATA_H_ 

#include "CCSCedaData.h" 

struct CFLDATA 
{
	long	Urno;
	char	Hopo[4];
	CTime	Time;
	char	Meno[5];
	char	Mety[3];
	char	Prio[3];
	char	Rtab[4];
	long	Rurn;
	char	Flst[256];
	char	Nval[256];
	char	Oval[256];
	char	Akus[33];
	CTime	Akti;
	char	Akst[3];
	char	Addi[65];
	DWORD	dwModId;

	//DataCreated by this class
	int      IsChanged;

	CFLDATA(void)
	{ 
		memset(this,'\0',sizeof(*this));
		Urno = 0;
		Rurn = 0;
		Akti = TIMENULL;
		Time = TIMENULL;
	}

}; // end CFLDATA
 
class CedaCflData : public CCSCedaData
{
public:
	CedaCflData();
	~CedaCflData();

	CCSPtrArray<CFLDATA> omData;
	
    CMapPtrToPtr omUrnoMap;
	CMapStringToPtr omRtabRurnMenoAddiMap;

	CString omRurns;

	DWORD	dwModId;

	void SetModId(DWORD	dwModId);


	char pcmFList[256];

	void ClearAll();
	
	void Register();
	void UnRegister();

	void ProcessCflBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);
	
	void InsertInternal(CFLDATA *prpCfl, bool bpDdx = true);
	
	bool UpdateInternal(CFLDATA *prpCfl);
	
	bool Read(const CString &ropRurns, bool bpDdx = true);
	bool ReadReload(const CString &ropUrnos, bool bpDdx = true);
	
	bool AddRurn(CString &opRurn);


	CFLDATA *GetCflByUrno(long lpUrno);

	bool GetCflDataByRurnUrno(CCSPtrArray<CFLDATA> &ropCflList, long lpGurn);

	bool IsConfirmed(const CString &ropRtab, long lpFUrno, char cpFPart, long lpRelFUrno, char cpRelFPart, int ipMeno) const;

	bool CreateConfirmed(long lpFUrno, char cpFPart, long lpRelFUrno, char cpRelFPart, int ipMeno, bool bpRelHdl = false);

	bool Confirm(long lpUrno, bool bpRelHdl = false);
	bool Confirm(long lpFUrno, char cpFPart, long lpRelFUrno, char cpRelFPart, int ipMeno, bool bpRelHdl = false);

	bool ReleaseDBUpdate();

private:
	CFLDATA *GetCflByRurnMenoAddi(long lpRurn, int ipMeno, const CString &ropAddi);

	CStringArray omRelCflIRTData;
	CStringArray omRelCflURTData;
	CStringArray omRelCflURTUrnos;
	CString omRelCflURTFields;
	CStringArray omRelCflDRTData;
	CString omRelCflUrnos;

};


#endif // _CEDACFLDATA_H_ 
