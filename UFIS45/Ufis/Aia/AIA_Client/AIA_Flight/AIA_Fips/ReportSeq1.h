#if !defined(AFX_REPORTSEQ1_H__F2B6C591_4E2A_11D3_AB98_00001C019D0B__INCLUDED_)
#define AFX_REPORTSEQ1_H__F2B6C591_4E2A_11D3_AB98_00001C019D0B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ReportSeq1.h : header file
//

#include "CCSEdit.h"
#include "resrc1.h"

/////////////////////////////////////////////////////////////////////////////
// ReportSeq1 dialog

class ReportSeq1 : public CDialog
{
// Construction
public:
	ReportSeq1(CWnd* pParent = NULL, CString opHeadline = "", CTime* oMinDate = NULL, CTime* oMaxDate = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(ReportSeq1)
	enum { IDD = IDD_REPORTSEQ1 };
	CCSEdit	m_CE_Datumvon;
	CString	m_Datumvon;
	//}}AFX_DATA
	CTime *pomMinDate;


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(ReportSeq1)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	CString omHeadline;

	// Generated message map functions
	//{{AFX_MSG(ReportSeq1)
	virtual void OnOK();
	virtual void OnCancel();
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_REPORTSEQ1_H__F2B6C591_4E2A_11D3_AB98_00001C019D0B__INCLUDED_)
