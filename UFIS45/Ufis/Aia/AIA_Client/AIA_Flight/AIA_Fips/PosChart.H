// stafchrt.h : header file
//

#ifndef _POSCHRT_
#define _POSCHRT_

#include "ccsdragdropctrl.h"
#include "CCS3dStatic.h"
#include "CCSGlobl.h"
#include "PosGantt.h"

#ifndef _CHART_STATE_
#define _CHART_STATE_

//enum ChartState { Minimized, Normal, Maximized };

#endif // _CHART_STATE_

/////////////////////////////////////////////////////////////////////////////
// PosChart frame

class PosDiagram;

class PosChart : public CFrameWnd
{
	friend PosDiagram;

    DECLARE_DYNCREATE(PosChart)
public:
    PosChart();           // protected constructor used by dynamic creation
    virtual ~PosChart();

private:
// Operations
//public:
    int GetHeight();
    int GetState(void) { return imState; };
    void SetState(int ipState) { imState = ipState; };
    
    CCSTimeScale *GetTimeScale(void) { return pomTimeScale; };
    void SetTimeScale(CCSTimeScale *popTimeScale)
    {
        pomTimeScale = popTimeScale;
    };

    CStatusBar *GetStatusBar(void) { return pomStatusBar; };
    void SetStatusBar(CStatusBar *popStatusBar)
    {
        pomStatusBar = popStatusBar;
    };
    
    PosDiagramViewer *GetViewer(void) { return pomViewer; };
    void SetViewer(PosDiagramViewer *popViewer)
    {
        pomViewer = popViewer;
    };

    void SetMarkTime(CTime opStatTime, CTime opEndTime);
    CTime GetStartTime(void) { return omStartTime; };
    void SetStartTime(CTime opStartTime) { omStartTime = opStartTime; };

    CTimeSpan GetInterval(void) { return omInterval; };
    void SetInterval(CTimeSpan opInterval) { omInterval = opInterval; };

    PosGantt *GetGanttPtr(void) { return &omGantt; };
    CCS3DStatic *GetTopScaleTextPtr(void) { return pomTopScaleText; };
	CCS3DStatic *GetCountTextPtr(void) { return pomCountText; };

 
// Implementation
protected:
    // Generated message map functions
    //{{AFX_MSG(PosChart)
    afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnDestroy();
    afx_msg void OnSize(UINT nType, int cx, int cy);
    afx_msg BOOL OnEraseBkgnd(CDC* pDC);
    afx_msg void OnPaint();
    afx_msg void OnChartButton();
    afx_msg LONG OnChartButtonRButtonDown(UINT, LONG);
    afx_msg LONG OnDragOver(UINT, LONG); 
    afx_msg LONG OnDrop(UINT, LONG); 
	afx_msg void OnMenuAssign();
	afx_msg void OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg void OnKeyUp(UINT nChar, UINT nRepCnt, UINT nFlags);
	//}}AFX_MSG
    DECLARE_MESSAGE_MAP()
    
protected:
    int imState;
    int imHeight;
    
    CCS3DStatic *pomTopScaleText;
	CCS3DStatic *pomCountText;
    
    CCSTimeScale *pomTimeScale;
    CStatusBar *pomStatusBar;
    PosDiagramViewer *pomViewer;
    CTime omStartTime;
    CTimeSpan omInterval;
    PosGantt omGantt;

private:    
    COLORREF lmBkColor;
    COLORREF lmTextColor;
    COLORREF lmHilightColor;
    int imStartTopScaleTextPos;
    int imStartVerticalScalePos;

// Drag-and-drop section
public:
    CCSDragDropCtrl m_ChartWindowDragDrop;
    CCSDragDropCtrl m_ChartButtonDragDrop;
    CCSDragDropCtrl m_CountTextDragDrop;
    CCSDragDropCtrl m_TopScaleTextDragDrop;

//-DTT Jul.22-----------------------------------------------------------
// No more routine ProcessPartTimeAssignment(), we use a new dialog in
// file "dassignp.h" instead.
//	void ProcessDropShifts(CCSDragDropCtrl *popDragDropCtrl, DROPEFFECT lpDropEffect);
//  void ProcessPartTimeAssignment(CCSDragDropCtrl *popDragDropCtrl);
	LONG ProcessDropShifts(CCSDragDropCtrl *popDragDropCtrl, DROPEFFECT lpDropEffect);
	LONG ProcessDropDutyBar(CCSDragDropCtrl *popDragDropCtrl, DROPEFFECT lpDropEffect);
	LONG ProcessDropFlightBar(CCSDragDropCtrl *popDragDropCtrl, DROPEFFECT lpDropEffect);
//----------------------------------------------------------------------
};

/////////////////////////////////////////////////////////////////////////////

#endif // _POSCHRT_
