// RotationGoAroundDlg.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "fpms.h"
#include "RotationGoAroundDlg.h"
#include "CCSTime.h"
#include "CcsGlobl.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld RotationGoAroundDlg 


RotationGoAroundDlg::RotationGoAroundDlg(CWnd* pParent, CString& opHeadline, CTime* opTime, CString& opPrompt, CString& opField, CTime opRefTime)
	: CDialog(RotationGoAroundDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(RotationGoAroundDlg)
	m_Time = _T("");
	//}}AFX_DATA_INIT

	omHeadline = opHeadline;	//Dialogbox-�berschrift
	omTime     = opTime;
	omField    = opField;
	omPrompt   = opPrompt;
	omRefTime  = opRefTime;
}


void RotationGoAroundDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(RotationGoAroundDlg)
	DDX_Control(pDX, IDC_TIME, m_CE_Time);
	DDX_Text(pDX, IDC_TIME, m_Time);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(RotationGoAroundDlg, CDialog)
	//{{AFX_MSG_MAP(RotationGoAroundDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Behandlungsroutinen f�r Nachrichten RotationGoAroundDlg 

void RotationGoAroundDlg::OnOK() 
{
	m_CE_Time.GetWindowText(m_Time);
	if(m_Time.IsEmpty() && *omTime != TIMENULL)
	{
		if (CFPMSApp::MyTopmostMessageBox(this,GetString(IDS_STRING2203), GetString(ST_FRAGE), MB_ICONQUESTION | MB_YESNO) != IDYES)
			return;
	}

	if (!m_Time.IsEmpty())
	{
		if(!m_CE_Time.GetStatus())
		{
			MessageBox(GetString(IDS_STRING2204), GetString(IDS_STRING2202), MB_OK);
			return;
		}
	}

	*omTime = HourStringToDate(m_Time, omRefTime);
	CDialog::OnOK();
}

void RotationGoAroundDlg::OnCancel() 
{
	// TODO: Zus�tzlichen Bereinigungscode hier einf�gen
	
	CDialog::OnCancel();
}

BOOL RotationGoAroundDlg::OnInitDialog() 
{
	CTime olTime = *omTime;
//	if (olTime == TIMENULL)
//		return FALSE;

	CDialog::OnInitDialog();
	CDialog::SetWindowText(omHeadline);	//Dialogbox-�berschrift

	m_CE_Time.SetTypeToTime(true, true);
	m_Time = DateToHourDivString(olTime, omRefTime);
	m_CE_Time.SetWindowText(m_Time);

	CWnd* wnd = GetDlgItem(IDC_STATIC_PROMPT);
	if (wnd)
		wnd->SetWindowText(omPrompt);

	wnd = GetDlgItem(IDC_STATIC_ETA);
	if (wnd)
		wnd->SetWindowText(omField);

	m_CE_Time.SetSel(0,-1,TRUE);
	m_CE_Time.SetFocus();
	return FALSE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX-Eigenschaftenseiten sollten FALSE zur�ckgeben
}


