// AdditionalReport17TableViewer.cpp : implementation file
// 
// spot using rate failure rate staistic according to time


#include "stdafx.h"
#include "resrc1.h"
#include "CcsGlobl.h"
#include "CedaBasicData.h"
#include "AdditionalReport17TableViewer.h"
#include <iomanip.h>
#include <iostream.h>
#include <fstream.h>
#include "BasicData.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif


/////////////////////////////////////////////////////////////////////////////
// AdditionalReport17TableViewer
//


AdditionalReport17TableViewer::AdditionalReport17TableViewer(char *pcpInfo, char *pcpSelect, CTime opMin, CTime opMax)
{

	pcmInfo = pcpInfo;
	pcmSelect = pcpSelect;

	opMax = opMin;

	CString olMinStr = opMin.Format( "%Y%m%d000000" );
	CString olMaxStr = opMax.Format( "%Y%m%d235959" );
	
	if(bgReportLocal)
	{
		CTime olUtcMinDate = DBStringToDateTime(olMinStr);
		CTime olUtcMaxDate = DBStringToDateTime(olMaxStr);

		ogBasicData.LocalToUtc(olUtcMinDate);
		ogBasicData.LocalToUtc(olUtcMaxDate);

		olMinStr = olUtcMinDate.Format( "%Y%m%d%H%M%S" );
		olMaxStr = olUtcMaxDate.Format( "%Y%m%d%H%M%S" );
	}

	pomParentDlg = NULL;

	omMin = DBStringToDateTime(olMinStr);
	omMax = DBStringToDateTime(olMaxStr);


	InitColumn0();
	GetRecordsFromDB();
	MakeCountListPST();
	MakeCountListPSTFailure();


}


AdditionalReport17TableViewer::~AdditionalReport17TableViewer()
{

	omColumn0.RemoveAll();
	omCountColumn1.RemoveAll();
	omCountColumn2.RemoveAll();

	omPST.DeleteAll();
	omPSTFailure.DeleteAll();

}

void AdditionalReport17TableViewer::GetRecordsFromDB()
{

	CCS_TRY

	CString olWhere;
	olWhere.Format(" ((TIFA BETWEEN '%s' AND '%s' AND DES3 = '%s' AND PSTA <> ' ') OR (TIFD BETWEEN '%s' AND '%s' AND ORG3 = '%s' AND PSTD <> ' ')) AND FTYP IN ('O','T','G','S')", CTimeToDBString(omMin, omMin), CTimeToDBString(omMax, omMax), pcgHome, CTimeToDBString(omMin, omMin), CTimeToDBString(omMax, omMax), pcgHome);

	
	//olWhere.Format(" TIFA >= '%s' AND TIFD <= '%s' AND (PSTA <> ' ' OR PSTD <> ' ')", CTimeToDBString(omMin, omMin), CTimeToDBString(omMax, omMax));
	omDailyCedaFlightData.ReadAllFlights(olWhere, false);


	olWhere.Empty();
	olWhere.Format("WHERE (VAFR < '%s' AND (VATO >= '%s' OR VATO = ' '))", 
		           CTimeToDBString(omMax, omMax), CTimeToDBString(omMin, omMin));

	ogBCD.ReadSpecial("PST", "VAFR,VATO,NAFR,NATO,PNAM", olWhere, omPSTFailure);


	CCS_CATCH_ALL

}


void AdditionalReport17TableViewer::InitColumn0()
{

	omColumn0.Add("0");
	omColumn0.Add("1");
	omColumn0.Add("2");
	omColumn0.Add("3");
	omColumn0.Add("4");
	omColumn0.Add("5");
	omColumn0.Add("6");
	omColumn0.Add("7");
	omColumn0.Add("8");
	omColumn0.Add("9");
	omColumn0.Add("10");
	omColumn0.Add("11");
	omColumn0.Add("12");
	omColumn0.Add("13");
	omColumn0.Add("14");
	omColumn0.Add("15");
	omColumn0.Add("16");
	omColumn0.Add("17");
	omColumn0.Add("18");
	omColumn0.Add("19");
	omColumn0.Add("20");
	omColumn0.Add("21");
	omColumn0.Add("22");
	omColumn0.Add("23");
	for (int i = 0; i < 25; i++)
	{
		omCountColumn1.Add(0);
		omCountColumn2.Add(0);
	}

}


// ========
// VALIDATE
// ========
void AdditionalReport17TableViewer::MakeCountListPST()
{

	/*
	CTime olTimeFrom;
	CTime olTimeTo;
	CTime olTimeToFailure;
	CTime olTimeFromFailure;

	CString olPst;
	bool blValid;

	DAILYFLIGHTDATA prlFlight;
	*/


	/////////////////////////////////////////////////////


	DAILYFLIGHTDATA *prlFlightA;
	DAILYFLIGHTDATA *prlFlightD;

	CTime olStart;
	CTime olEnd;

	POSITION pos;
	DailyCedaFlightData::RKEYLIST *prpRkey;
	void *pVoid;
	for( pos = omDailyCedaFlightData.omRkeyMap.GetStartPosition(); pos != NULL; )
	{
		omDailyCedaFlightData.omRkeyMap.GetNextAssoc( pos, pVoid , (void *&)prpRkey );

		int ilCount = prpRkey->Rotation.GetSize();

		for(int i = 0; i < ilCount; i++)
		{
			prlFlightA = &(*prpRkey).Rotation[i];

			if(prpRkey->Rotation.GetSize() == 1 && CString(prlFlightA->Adid) == "D")
			{
				prlFlightD = prlFlightA;
				prlFlightA = NULL;
			}
			else
			{
				if(i+1 <  ilCount)
				{
					prlFlightD = &(*prpRkey).Rotation[i+1];
					i++;
				}
				else
				{
					prlFlightD = NULL;
				}
			}

			// Rotation
			if((prlFlightA != NULL) && (prlFlightD != NULL))
			{
				
				olStart = prlFlightA->Tifa;
				olEnd	= prlFlightD->Tifd;
			}

			
			// Arrival
			if((prlFlightA != NULL) && (prlFlightD == NULL))
			{
				CTimeSpan olDura = prlFlightA->Paea - prlFlightA->Paba;

				if(olDura.GetTotalMinutes() == 0)
				{
					int ilMing = atoi(prlFlightA->Ming);
					int ilDura = 0;

					ilDura = 30;
					
					if( ilMing > ilDura)
					{
						ilDura = ilMing;
					}
					
					olDura = CTimeSpan(0, 0, ilDura, 0);
				}
				olStart = prlFlightA->Tifa;
				olEnd = prlFlightA->Tifa + olDura;
			}


			if((prlFlightD != NULL) && (prlFlightA == NULL))
			{
				CTimeSpan olDura = prlFlightD->Pdea - prlFlightD->Pdba;

				if(olDura.GetTotalMinutes() == 0)
				{
					int ilMing = atoi(prlFlightD->Ming);
					int ilDura = 0;

						ilDura = 30;
					if( ilMing > ilDura)
					{
						ilDura = ilMing;
					}
					
					olDura = CTimeSpan(0, 0, ilDura, 0);
				}
				
				olEnd = prlFlightD->Tifd;
				olStart = prlFlightD->Tifd - olDura;

			}

		}

		WriteBuffersPST(olStart, olEnd);


	}


	/*


	/////////////////////////////////////////////////////

	for (int i = 0; i < omDailyCedaFlightData.omData.GetSize(); i++)
	{
		blValid = false;

		prlFlight = omDailyCedaFlightData.omData[i];
		if (prlFlight.Adid == CString("A"))
		{
			olPst = prlFlight.Psta;
			for (int i = 0; i < omPSTFailure.GetSize(); i++)
			{
				RecordSet *prlRecord = &omPSTFailure[i];
				if (olPst == (*prlRecord)[4])
				{
					blValid = true;
					break;
				}
			}
			if (blValid)
			{
				olTimeFrom = prlFlight.Paba;
				olTimeTo   = prlFlight.Paea;
				if (olTimeFrom == TIMENULL)
				{
					olTimeFrom = prlFlight.Pabs;
					olTimeTo   = prlFlight.Paes;
				}
				WriteBuffersPST(olTimeFrom, olTimeTo);
			}
		}

		if (prlFlight.Adid == CString("D"))
		{
			blValid = false;

			olPst = prlFlight.Pstd;
			for (int i = 0; i < omPSTFailure.GetSize(); i++)
			{
				RecordSet *prlRecord = &omPSTFailure[i];
				if (olPst == (*prlRecord)[4])
				{
					blValid = true;
					break;
				}
			}
			if (blValid)
			{
				olTimeFrom = prlFlight.Pdba;
				olTimeTo   = prlFlight.Pdea;
				if (olTimeFrom == TIMENULL)
				{
					olTimeFrom = prlFlight.Pdbs;
					olTimeTo   = prlFlight.Pdes;
				}
				WriteBuffersPST(olTimeFrom, olTimeTo);
			}
		}

	}
	*/

}


// ============
// NOT VALIDATE
// ============
void AdditionalReport17TableViewer::MakeCountListPSTFailure()
{

	CTime olTimeToFailure;
	CTime olTimeFromFailure;

	for (int i = 0; i < omPSTFailure.GetSize(); i++)
	{
		RecordSet *prlRecord = &omPSTFailure[i];
		olTimeFromFailure = DBStringToDateTime((*prlRecord)[2]);
		olTimeToFailure   = DBStringToDateTime((*prlRecord)[3]);

		WriteBuffersPSTFailure(olTimeFromFailure, olTimeToFailure);
	}

}


void AdditionalReport17TableViewer::WriteBuffersPST(CTime olTimeFrom, CTime olTimeTo)
{

	int ilHourCurrent;
	int i;

	CString olFrom = olTimeFrom.Format("%Y%m%d %H%M");
	CString olMin  = omMin.Format("%Y%m%d 0000");
	CString olTo   = olTimeTo.Format("%Y%m%d %H%M");
	CString olMax  = omMax.Format("%Y%m%d %H%M");

	if (olTimeFrom == TIMENULL && olTimeTo == TIMENULL )
		return;
	
	// auf alle 24 Stunden + 1, wenn aktuelle Zeiten auserhalb der angegebenen
	if ( (olTimeTo == TIMENULL && olFrom < olMin) || (olFrom < olMin && olTo > olMax) )
	{
		for (i = 0; i < 24; i++)
		{
			omCountColumn1[i] = omCountColumn1[i] + 1;
		}
		return;
	}

	olFrom = olTimeFrom.Format("%Y%m%d");
	olMin  = omMin.Format("%Y%m%d");
	olTo   = olTimeTo.Format("%Y%m%d");
	olMax  = omMax.Format("%Y%m%d");

	// Datum der Von-Zeit gleich aktuellem Von-Datum bis Ende des Tages
	if (olMin == olFrom && olMax != olTo)
	{
		// Anzahl Stunden des verbliebenen Tages
		ilHourCurrent = atoi(olTimeFrom.Format("%H").GetBuffer(0));
		for (i = ilHourCurrent; i < 24; i++)
			omCountColumn1[i] = omCountColumn1[i] + 1;
	}
	
	// Datum der Bis-Zeit gleich aktuellem Bis-Datum
	if (olMax == olTo && olMin != olFrom)
	{
		// Anzahl Stunden von 00:00 bis Stunde des aktuellen Tages
		ilHourCurrent = atoi(olTimeTo.Format("%H").GetBuffer(0));
		for (int i = 0; i < ilHourCurrent; i++)
			omCountColumn1[i] = omCountColumn1[i] + 1;
	}

	// von und bis des gleichen Tages
	if (olMax == olTo && olMin == olFrom)
	{
		int ilFrom = atoi(olTimeFrom.Format("%H").GetBuffer(0));
		int ilTo   = atoi(olTimeTo.Format("%H").GetBuffer(0));
		for (int i = ilFrom; i <= ilTo; i++)
			omCountColumn1[i] = omCountColumn1[i] + 1;
	}


}


void AdditionalReport17TableViewer::WriteBuffersPSTFailure(CTime olTimeFrom, CTime olTimeTo)
{

	int ilHourCurrent;
	int i;

	CString olFrom = olTimeFrom.Format("%Y%m%d %H%M");
	CString olMin  = omMin.Format("%Y%m%d 0000");
	CString olTo   = olTimeTo.Format("%Y%m%d %H%M");
	CString olMax  = omMax.Format("%Y%m%d %H%M");

	if (olTimeFrom == TIMENULL && olTimeTo == TIMENULL)
		return;
	
	// auf alle 24 Stunden + 1, wenn aktuelle Zeiten auserhalb der angegebenen
	if ( (olTimeTo == TIMENULL && olFrom <= olMin) || (olFrom <= olMin && olTo >= olMax) )
	{
		for (i = 0; i < 24; i++)
		{
			omCountColumn2[i] = omCountColumn2[i] + 1;
		}
		return;
	}

	olFrom = olTimeFrom.Format("%Y%m%d");
	olMin  = omMin.Format("%Y%m%d");
	olTo   = olTimeTo.Format("%Y%m%d");
	olMax  = omMax.Format("%Y%m%d");

	// Datum der Von-Zeit gleich aktuellem Von-Datum bis Ende des Tages
	if (olMin == olFrom && olMax != olTo)
	{
		// Anzahl Stunden des verbliebenen Tages
		ilHourCurrent = atoi(olTimeFrom.Format("%H").GetBuffer(0));
		for (i = ilHourCurrent; i < 24; i++)
			omCountColumn2[i] = omCountColumn2[i] + 1;
	}
	
	// Datum der Bis-Zeit gleich aktuellem Bis-Datum
	if (olMax == olTo && olMin != olFrom)
	{
		// Anzahl Stunden von 00:00 bis Stunde des aktuellen Tages
		ilHourCurrent = atoi(olTimeTo.Format("%H").GetBuffer(0));
		for (int i = 0; i < ilHourCurrent; i++)
			omCountColumn2[i] = omCountColumn2[i] + 1;
	}

	// von und bis des gleichen Tages
	if (olMax == olTo && olMin == olFrom)
	{
		int ilFrom = atoi(olTimeFrom.Format("%H").GetBuffer(0));
		int ilTo   = atoi(olTimeTo.Format("%H").GetBuffer(0));
		for (int i = ilFrom; i <= ilTo; i++)
			omCountColumn2[i] = omCountColumn2[i] + 1;
	}

}


int AdditionalReport17TableViewer::GetBarCount()
{

	return omColumn0.GetSize();

}



CString AdditionalReport17TableViewer::GetTime(int ipIndex)
{

	if (ipIndex < omColumn0.GetSize())
		return omColumn0[ipIndex];
	else
		return "";

}


int AdditionalReport17TableViewer::GetDataPst(int ipIndex)
{

	if (ipIndex < omCountColumn1.GetSize())
		return omCountColumn1[ipIndex];
	else
		return 0;

}


int AdditionalReport17TableViewer::GetDataPstFailure(int ipIndex)
{

	if (ipIndex < omCountColumn2.GetSize())
		return omCountColumn2[ipIndex];
	else
		return 0;

}


int AdditionalReport17TableViewer::NumberOfPst()
{

	return omPSTFailure.GetSize();
	
}


void AdditionalReport17TableViewer::SetParentDlg(CDialog* ppParentDlg)
{

	pomParentDlg = ppParentDlg;

}


void AdditionalReport17TableViewer::PrintTableView(DGanttChartReportWnd* popDGanttChartReportWnd)
{

	CCS_TRY

	CString olFooter1;
	CString olTableName = GetString(IDS_STRING1759);
	int ilOrientation = PRINT_LANDSCAPE;
	CCSPrint* pomPrint = new CCSPrint(pomParentDlg,ilOrientation,45);
	pomPrint->imMaxLines = 38;

	char pclHeader[256];

	sprintf(pclHeader, GetString(IDS_STRING1751));
	CString olHeader(pclHeader);

	if (pomPrint != NULL)
	{

		if (pomPrint->InitializePrinter(ilOrientation) == TRUE)
		{
			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
			pomPrint->imLineNo = pomPrint->imMaxLines + 1;
			DOCINFO	rlDocInfo;
			memset(&rlDocInfo, 0, sizeof(DOCINFO));
			rlDocInfo.cbSize = sizeof( DOCINFO );
			rlDocInfo.lpszDocName = olTableName;	
			pomPrint->omCdc.StartDoc( &rlDocInfo );
			pomPrint->imPageNo = 0;

			CString olFooter1;
			olFooter1.Format("%s %d",rlDocInfo.lpszDocName, NumberOfPst());
			popDGanttChartReportWnd->Print(&pomPrint->omCdc, pomPrint, olHeader, olFooter1);
		}

		SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
		delete pomPrint;
		pomPrint = NULL;
	}

	CCS_CATCH_ALL

}
