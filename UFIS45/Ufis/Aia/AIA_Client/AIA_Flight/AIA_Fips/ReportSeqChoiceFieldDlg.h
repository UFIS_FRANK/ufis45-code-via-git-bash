#ifndef AFX_REPORTSEQCHOICEFIELDDLG_H__CDB257E1_8BF5_11D1_8127_0000B43C4B01__INCLUDED_
#define AFX_REPORTSEQCHOICEFIELDDLG_H__CDB257E1_8BF5_11D1_8127_0000B43C4B01__INCLUDED_

// ReportSeqFieldDlg.h : Header-Datei
//
#include "CCSEdit.h"
#include "resrc1.h"

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld CReportSeqChoiceFieldDlg 

class CReportSeqChoiceFieldDlg : public CDialog
{
// Konstruktion
public:
	CReportSeqChoiceFieldDlg(CWnd* pParent = NULL, CString opHeadline = "", CString opSelectfield = "",CString opSelectfield2 = "",
		               CTime* oMinDate = NULL, CTime* oMaxDate = NULL, char *opSelect = "",bool *opChoice = NULL,  
					   CString opSetFormat = "", int ipMin = 0, int ipMax = 0);

	CTime *pomMinDate;
	CTime *pomMaxDate;
	char *pcmSelect;
	bool *pbmChoice;
// Dialogfelddaten
	//{{AFX_DATA(CReportSeqChoiceFieldDlg)
	enum { IDD = IDD_REPORTSEQCHOICEFIELD }; 
	CStatic	m_CS_Selectfield;
	CCSEdit	m_CE_Select;
	CCSEdit	m_CE_Datumvon;
	CCSEdit	m_CE_Datumbis;
	CButton	m_CE_Radio1;
	CButton	m_CE_Radio2;
	CString	m_Datumbis;
	CString	m_Datumvon;
	CString	m_Select;
	CString	m_Selectfield;
	int     m_Radio1;
	//}}AFX_DATA
	CString omHeadline;
	CString m_Selectfield1;
	CString m_Selectfield2;

// Überschreibungen
	// Vom Klassen-Assistenten generierte virtuelle Funktionsüberschreibungen
	//{{AFX_VIRTUAL(CReportSeqChoiceFieldDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	//}}AFX_VIRTUAL

private:
	CString omSetFormat;
	int imMax; 
	int imMin;

// Implementierung
protected:

	// Generierte Nachrichtenzuordnungsfunktionen
	//{{AFX_MSG(CReportSeqChoiceFieldDlg)
	virtual void OnCancel();
	virtual void OnOK();
	virtual BOOL OnInitDialog();
	afx_msg void OnRadio1();
	afx_msg void OnRadio2();
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio fügt zusätzliche Deklarationen unmittelbar vor der vorhergehenden Zeile ein.

#endif // AFX_REPORTSEQFIELDDLG_H__CDB257E1_8BF5_11D1_8127_0000B43C4B01__INCLUDED_
