// GatPosConflictSetupDlg.cpp : implementation file
//

#include "stdafx.h"
#include "fpms.h"
#include "resrc1.h"
#include "GatPosConflictSetupDlg.h"
#include "GridFenster.h"
#include "CedaCfgData.h"
#include "BasicData.h"
#include "PrivList.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// GatPosConflictSetupDlg dialog


GatPosConflictSetupDlg::GatPosConflictSetupDlg(CWnd* pParent /*=NULL*/)
	: CDialog(GatPosConflictSetupDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(GatPosConflictSetupDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT

	static bool blFirst = true;
	if (blFirst)
	{
		blFirst = false;
		GXInit();
	}

	pomConflictSetup = new CGridFenster(this);

}

GatPosConflictSetupDlg::~GatPosConflictSetupDlg()
{
	delete pomConflictSetup;
	omConflicts.DeleteAll();

}
void GatPosConflictSetupDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(GatPosConflictSetupDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(GatPosConflictSetupDlg, CDialog)
	//{{AFX_MSG_MAP(GatPosConflictSetupDlg)
	ON_WM_SIZE()
	ON_WM_PAINT()
	ON_MESSAGE(GRID_MESSAGE_CELLCLICK,OnGridMessageCellClick)
	ON_MESSAGE(GRID_MESSAGE_ENDEDITING,OnGridMessageEndEditing)
	ON_BN_CLICKED(IDC_LOAD_DEFAULT, OnLoadDefault)
	ON_BN_CLICKED(IDC_SAVE_DEFAULT, OnSaveDefault)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// GatPosConflictSetupDlg message handlers

BOOL GatPosConflictSetupDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	pomConflictSetup->SubclassDlgItem(IDC_CONFLICT_SETUP, this);

	// initialize selection grid
	pomConflictSetup->Initialize();
	pomConflictSetup->SetAllowMultiSelect(false);
	pomConflictSetup->SetSortingEnabled(false);

	// disable immediate update
	pomConflictSetup->LockUpdate(TRUE);
	pomConflictSetup->GetParam()->EnableUndo(FALSE);
	// column width is static
	pomConflictSetup->GetParam()->EnableTrackColWidth(FALSE);
	// row height is static
	pomConflictSetup->GetParam()->EnableTrackRowHeight(FALSE);
	// column headers aren't numbered
	pomConflictSetup->GetParam()->SetNumberedColHeaders(FALSE);

	pomConflictSetup->SetColCount(8);

	// set the size of the row number
	pomConflictSetup->SetColWidth(0,0,20);

	// set the size of the conflict name
	pomConflictSetup->SetColWidth(1,1,300);

	// set the size of the conflict duration
	pomConflictSetup->SetColWidth(2,2,20);

	for (int i = 3; i < 3 + 6;i++)
	{
		pomConflictSetup->SetColWidth(i,i,16);
	}

//	if( __CanSeeConflictPriorityColor())		// 050323 MVy: PRF6981: conflict priority, make configurable, addendum
												// 050506 MVy: conflict priority color, security state
	{
		// append two columns and specify their length
		int cols = pomConflictSetup->GetColCount();
		pomConflictSetup->SetColCount( cols +2 );		// 050303 MVy: two columns added, now 11
		pomConflictSetup->SetColWidth( cols +1, cols +1, 16 );		// 050303 MVy: a small gap
		pomConflictSetup->SetColWidth( cols +2, cols +2, 16 );		// 050303 MVy: Conflict Priority
	};

	pomConflictSetup->SetRowHeight(0, 0, 14);

	ogCfgData.GetConflictData(omConflicts);

	pomConflictSetup->SetRowCount(omConflicts.GetSize());
	pomConflictSetup->SetScrollBarMode(SB_BOTH,gxnAutomatic|gxnEnhanced,TRUE);


	// enable immediate update
	pomConflictSetup->LockUpdate(FALSE);

	UpdateGrid();

	CWnd *polWnd = GetDlgItem(IDC_LOAD_DEFAULT);
	if (polWnd)
	{
		if(ogPrivList.GetStat("SETUP_CONFLICT_SAVE") == '0')//disabled
			polWnd->EnableWindow(FALSE);
		if(ogPrivList.GetStat("SETUP_CONFLICT_SAVE") == '-')//hidden
			polWnd->ShowWindow(SW_HIDE);
	}

	polWnd = GetDlgItem(IDC_SAVE_DEFAULT);
	if (polWnd)
	{
		if(ogPrivList.GetStat("SETUP_CONFLICT_LOAD") == '0')//disabled
			polWnd->EnableWindow(FALSE);
		if(ogPrivList.GetStat("SETUP_CONFLICT_LOAD") == '-')//hidden
			polWnd->ShowWindow(SW_HIDE);
		if(ogPrivList.GetStat("SETUP_USER") == '0')//disabled
			polWnd->EnableWindow(FALSE);
		if(ogPrivList.GetStat("SETUP_USER") == '-')//hidden
			polWnd->ShowWindow(SW_HIDE);
	}

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void GatPosConflictSetupDlg::OnSize(UINT nType, int cx, int cy) 
{
	CDialog::OnSize(nType, cx, cy);
	
	// TODO: Add your message handler code here
	if (pomConflictSetup && ::IsWindow(pomConflictSetup->m_hWnd))
	{
		CRect olRect;
		pomConflictSetup->GetWindowRect(&olRect);
		this->ScreenToClient(&olRect); 
		cx = 0.97 * cx;	// with aspect ratio between child window width / dialog width
//		cy = 0.72 * cy;	// with aspect ration between child window height / dialog height
		cy = 0.81 * cy;	// with aspect ration between child window height / dialog height
		pomConflictSetup->MoveWindow(olRect.TopLeft().x,olRect.TopLeft().y,cx,cy);
		pomConflictSetup->Redraw();
	}

}

void GatPosConflictSetupDlg::OnPaint() 
{
	CPaintDC dc(this); // device context for painting
	
	// TODO: Add your message handler code here
	CRect olRect;
	pomConflictSetup->GetWindowRect(&olRect);
	this->ScreenToClient(&olRect);
	

	::SetGraphicsMode(dc.m_hDC,GM_ADVANCED);

	CFont *polFont = this->GetFont();
	if (polFont)
	{
		CFont olFont;
		LOGFONT olLogFont;
		polFont->GetLogFont(&olLogFont);
		olLogFont.lfOrientation = 0;
		olLogFont.lfEscapement  = 0;
		olLogFont.lfWeight		= 700;
		olFont.CreateFontIndirect(&olLogFont);
		polFont = dc.SelectObject(&olFont);

		dc.TextOut(olRect.TopLeft().x+20,olRect.TopLeft().y - 15,GetString(IDS_STRING2330));
		dc.TextOut(olRect.TopLeft().x+20+300,olRect.TopLeft().y - 15,GetString(IDS_STRING2331));

		dc.SelectObject(polFont);

		olFont.DeleteObject();

		//olLogFont.lfOrientation = 750;
		//olLogFont.lfEscapement  = 750;
		olLogFont.lfWeight		= 700;
		olFont.CreateFontIndirect(&olLogFont);
		polFont = dc.SelectObject(&olFont);

		dc.TextOut(olRect.TopLeft().x+20+300+20+0*16,olRect.TopLeft().y - 95,GetString(IDS_STRING2332));
		dc.TextOut(olRect.TopLeft().x+20+300+20+1*16,olRect.TopLeft().y - 82,GetString(IDS_STRING2333));
		dc.TextOut(olRect.TopLeft().x+20+300+20+2*16,olRect.TopLeft().y - 69,GetString(IDS_STRING2334));
		dc.TextOut(olRect.TopLeft().x+20+300+20+3*16,olRect.TopLeft().y - 56,GetString(IDS_STRING2335));
		dc.TextOut(olRect.TopLeft().x+20+300+20+4*16,olRect.TopLeft().y - 43,"Belts");//GetString(IDS_STRING2336));
		dc.TextOut(olRect.TopLeft().x+20+300+20+5*16,olRect.TopLeft().y - 30,GetString(IDS_STRING2337));

		  dc.TextOut(olRect.TopLeft().x+20+300+20+6*16+8,olRect.TopLeft().y-12 ,"!");
		  dc.TextOut(olRect.TopLeft().x+20+300+20+7*16+5,olRect.TopLeft().y-12 ,"C");

		  CPen pen(PS_SOLID, 0, RGB(0, 0, 0));
		  CPen *pOldPen = dc.SelectObject(&pen);

		  dc.MoveTo(olRect.TopLeft().x+20+300+20+0*16+10, olRect.TopLeft().y - 82);
		  dc.LineTo(olRect.TopLeft().x+20+300+20+0*16+10, olRect.TopLeft().y - 3);

		  dc.MoveTo(olRect.TopLeft().x+20+300+20+1*16+10, olRect.TopLeft().y - 69);
		  dc.LineTo(olRect.TopLeft().x+20+300+20+1*16+10, olRect.TopLeft().y - 3);

		  dc.MoveTo(olRect.TopLeft().x+20+300+20+2*16+10, olRect.TopLeft().y - 56);
		  dc.LineTo(olRect.TopLeft().x+20+300+20+2*16+10, olRect.TopLeft().y - 3);

		  dc.MoveTo(olRect.TopLeft().x+20+300+20+3*16+10, olRect.TopLeft().y - 43);
		  dc.LineTo(olRect.TopLeft().x+20+300+20+3*16+10, olRect.TopLeft().y - 3);

		  dc.MoveTo(olRect.TopLeft().x+20+300+20+4*16+10, olRect.TopLeft().y - 30);
		  dc.LineTo(olRect.TopLeft().x+20+300+20+4*16+10, olRect.TopLeft().y - 3);

		  dc.MoveTo(olRect.TopLeft().x+20+300+20+5*16+10, olRect.TopLeft().y - 17);
		  dc.LineTo(olRect.TopLeft().x+20+300+20+5*16+10, olRect.TopLeft().y - 3);

		  dc.SelectObject(pOldPen);



/*		dc.TextOut(olRect.TopLeft().x+20+300+20+0*16,olRect.TopLeft().y - 5,GetString(IDS_STRING2332));
		dc.TextOut(olRect.TopLeft().x+20+300+20+1*16,olRect.TopLeft().y - 5,GetString(IDS_STRING2333));
		dc.TextOut(olRect.TopLeft().x+20+300+20+2*16,olRect.TopLeft().y - 5,GetString(IDS_STRING2334));
		dc.TextOut(olRect.TopLeft().x+20+300+20+3*16,olRect.TopLeft().y - 5,GetString(IDS_STRING2335));
		dc.TextOut(olRect.TopLeft().x+20+300+20+4*16,olRect.TopLeft().y - 5,GetString(IDS_STRING2336));
		dc.TextOut(olRect.TopLeft().x+20+300+20+5*16,olRect.TopLeft().y - 5,GetString(IDS_STRING2337));
*/
		dc.SelectObject(polFont);

		olFont.DeleteObject();
	}

	// Do not call CDialog::OnPaint() for painting messages
}

LONG GatPosConflictSetupDlg::OnGridMessageEndEditing(WPARAM wParam,LPARAM lParam)
{
	CELLPOS *prlPos = (CELLPOS *)lParam;
	
	if (prlPos)
	{
		if (prlPos->Row > 0 && prlPos->Col == 2)
		{
			CONFLICTDATA* polConflict;
			polConflict = &omConflicts[prlPos->Row - 1];

			polConflict->imDuration = atoi(pomConflictSetup->GetValueRowCol(prlPos->Row,prlPos->Col));
		}
	}

	return 1;
}

LONG GatPosConflictSetupDlg::OnGridMessageCellClick(WPARAM wParam,LPARAM lParam)
{
	CELLPOS *prlPos = (CELLPOS *)lParam;

	
//	CRowColArray olRows;
//	int ilSelCount = (int)pomConflictSetup->GetSelectedRows( olRows);


	if (prlPos)
	{
		if (prlPos->Row > 0 && prlPos->Col > 2)
		{
			CONFLICTDATA* polConflict;
			polConflict = &omConflicts[prlPos->Row - 1];

			CGXStyle olStyle;
			olStyle.SetEnabled(TRUE);
			olStyle.SetReadOnly(TRUE);
			
			pomConflictSetup->GetParam()->SetLockReadOnly(FALSE);
//			pomConflictSetup->LockUpdate(FALSE);
			switch(prlPos->Col)
			{
			case 3 :
				if (polConflict->bmGeneralDesktop)
				{
					polConflict->bmGeneralDesktop = false;
					pomConflictSetup->SetStyleRange(CGXRange(prlPos->Row,prlPos->Col,prlPos->Row,prlPos->Col),olStyle.SetControl(GX_IDS_CTRL_STATIC).SetValue("#BMP(229)"));
				}
				else
				{
					polConflict->bmGeneralDesktop = true;
					pomConflictSetup->SetStyleRange(CGXRange(prlPos->Row,prlPos->Col,prlPos->Row,prlPos->Col),olStyle.SetControl(GX_IDS_CTRL_STATIC).SetValue("#BMP(228)"));
				}
			break;
			case 4 :
				if (polConflict->bmAircraftPositions)
				{
					polConflict->bmAircraftPositions = false;
					pomConflictSetup->SetStyleRange(CGXRange(prlPos->Row,prlPos->Col,prlPos->Row,prlPos->Col),olStyle.SetControl(GX_IDS_CTRL_STATIC).SetValue("#BMP(229)"));
				}
				else
				{
					polConflict->bmAircraftPositions = true;
					pomConflictSetup->SetStyleRange(CGXRange(prlPos->Row,prlPos->Col,prlPos->Row,prlPos->Col),olStyle.SetControl(GX_IDS_CTRL_STATIC).SetValue("#BMP(228)"));
				}
			break;
			case 5 :
				if (polConflict->bmGates)
				{
					polConflict->bmGates = false;
					pomConflictSetup->SetStyleRange(CGXRange(prlPos->Row,prlPos->Col,prlPos->Row,prlPos->Col),olStyle.SetControl(GX_IDS_CTRL_STATIC).SetValue("#BMP(229)"));
				}
				else
				{
					polConflict->bmGates = true;
					pomConflictSetup->SetStyleRange(CGXRange(prlPos->Row,prlPos->Col,prlPos->Row,prlPos->Col),olStyle.SetControl(GX_IDS_CTRL_STATIC).SetValue("#BMP(228)"));
				}
			break;
			case 6 :
				if (polConflict->bmCheckinCounters)
				{
					polConflict->bmCheckinCounters = false;
					pomConflictSetup->SetStyleRange(CGXRange(prlPos->Row,prlPos->Col,prlPos->Row,prlPos->Col),olStyle.SetControl(GX_IDS_CTRL_STATIC).SetValue("#BMP(229)"));
				}
				else
				{
					polConflict->bmCheckinCounters = true;
					pomConflictSetup->SetStyleRange(CGXRange(prlPos->Row,prlPos->Col,prlPos->Row,prlPos->Col),olStyle.SetControl(GX_IDS_CTRL_STATIC).SetValue("#BMP(228)"));
				}
			break;
			case 7 :
				if (polConflict->bmBaggageBelts)
				{
					polConflict->bmBaggageBelts = false;
					pomConflictSetup->SetStyleRange(CGXRange(prlPos->Row,prlPos->Col,prlPos->Row,prlPos->Col),olStyle.SetControl(GX_IDS_CTRL_STATIC).SetValue("#BMP(229)"));
				}
				else
				{
					polConflict->bmBaggageBelts = true;
					pomConflictSetup->SetStyleRange(CGXRange(prlPos->Row,prlPos->Col,prlPos->Row,prlPos->Col),olStyle.SetControl(GX_IDS_CTRL_STATIC).SetValue("#BMP(228)"));
				}
			break;
			case 8 :
				if (polConflict->bmLounges)
				{
					polConflict->bmLounges = false;
					pomConflictSetup->SetStyleRange(CGXRange(prlPos->Row,prlPos->Col,prlPos->Row,prlPos->Col),olStyle.SetControl(GX_IDS_CTRL_STATIC).SetValue("#BMP(229)"));
				}
				else
				{
					polConflict->bmLounges = true;
					pomConflictSetup->SetStyleRange(CGXRange(prlPos->Row,prlPos->Col,prlPos->Row,prlPos->Col),olStyle.SetControl(GX_IDS_CTRL_STATIC).SetValue("#BMP(228)"));
				}
			break;
			case 9 :
				if (polConflict->bmHighPrio)
				{
					polConflict->bmHighPrio = false;
					pomConflictSetup->SetStyleRange(CGXRange(prlPos->Row,prlPos->Col,prlPos->Row,prlPos->Col),olStyle.SetControl(GX_IDS_CTRL_STATIC).SetValue("#BMP(229)"));
				}
				else
				{
					polConflict->bmHighPrio = true;
					pomConflictSetup->SetStyleRange(CGXRange(prlPos->Row,prlPos->Col,prlPos->Row,prlPos->Col),olStyle.SetControl(GX_IDS_CTRL_STATIC).SetValue("#BMP(246)"));
				}
				break ;

			case 10:		// 050303 MVy: PRF6981: conflict priority
				{
					CColorDialog dlgColor ;
					dlgColor.m_cc.Flags |= CC_RGBINIT ;		// startup with the nearest selected color
					dlgColor.m_cc.rgbResult = polConflict->clrPriority ;
					// 050303 MVy: problem: this dialog is modal, but the framework subverts this mechanism
					//		if more than one dialog is open and a prior one is closed, the program will fail continuing,
					//		for example there are null pointers
					static unsigned char ucOnlyOnce = 0 ;
					//ASSERT( !ucOnlyOnce );
					if( !ucOnlyOnce )		// this is only workaround
					{
						ucOnlyOnce++ ;
						bool bResult = dlgColor.DoModal() == IDOK ;
						ucOnlyOnce-- ;
						// problem: the grid was not updated on ok, when another dialog was tried to be opened meanwhile

						ASSERT( pomConflictSetup );
						if( bResult )
						{
							COLORREF color = dlgColor.GetColor();
							polConflict->clrPriority = color ;
							pomConflictSetup->SetStyleRange( 
								CGXRange( prlPos->Row, prlPos->Col, prlPos->Row, prlPos->Col ), 
								olStyle.SetInterior( color ).SetValue( "" )
							);
						};
					};
				};	// handle conflict column

				break;

			}
			pomConflictSetup->GetParam()->SetLockReadOnly(TRUE);
			pomConflictSetup->RedrawRowCol(prlPos->Row,prlPos->Col,prlPos->Row,prlPos->Col);
//			pomConflictSetup->Redraw();
		}
		else
			ToggleRow(prlPos);
	}
	return 1;
}

void GatPosConflictSetupDlg::ToggleRow(CELLPOS *prlPos)  
{
	bool blMark = true;
	CString olBMP = "#BMP(228)";

	if (prlPos->Row > 0 && prlPos->Col == 1)
	{
		blMark = false;
		olBMP = "#BMP(229)";
	}

	if ((prlPos->Row > 0 && prlPos->Col == 0) || (prlPos->Row > 0 && prlPos->Col == 1))
	{
		CONFLICTDATA* polConflict;
		polConflict = &omConflicts[prlPos->Row - 1];

		CGXStyle olStyle;
		olStyle.SetEnabled(TRUE);
		olStyle.SetReadOnly(TRUE);
		
		pomConflictSetup->GetParam()->SetLockReadOnly(FALSE);

		polConflict->bmGeneralDesktop = blMark;
		pomConflictSetup->SetStyleRange(CGXRange(prlPos->Row,3,prlPos->Row,3),olStyle.SetControl(GX_IDS_CTRL_STATIC).SetValue(olBMP));

		polConflict->bmAircraftPositions = blMark;
		pomConflictSetup->SetStyleRange(CGXRange(prlPos->Row,4,prlPos->Row,4),olStyle.SetControl(GX_IDS_CTRL_STATIC).SetValue(olBMP));

		polConflict->bmGates = blMark;
		pomConflictSetup->SetStyleRange(CGXRange(prlPos->Row,5,prlPos->Row,5),olStyle.SetControl(GX_IDS_CTRL_STATIC).SetValue(olBMP));

		polConflict->bmCheckinCounters = blMark;
		pomConflictSetup->SetStyleRange(CGXRange(prlPos->Row,6,prlPos->Row,6),olStyle.SetControl(GX_IDS_CTRL_STATIC).SetValue(olBMP));

		polConflict->bmBaggageBelts = blMark;
		pomConflictSetup->SetStyleRange(CGXRange(prlPos->Row,7,prlPos->Row,7),olStyle.SetControl(GX_IDS_CTRL_STATIC).SetValue(olBMP));

		polConflict->bmLounges = blMark;
		pomConflictSetup->SetStyleRange(CGXRange(prlPos->Row,8,prlPos->Row,8),olStyle.SetControl(GX_IDS_CTRL_STATIC).SetValue(olBMP));

		pomConflictSetup->GetParam()->SetLockReadOnly(TRUE);
		pomConflictSetup->RedrawRowCol(prlPos->Row,3,prlPos->Row,3);
		pomConflictSetup->RedrawRowCol(prlPos->Row,4,prlPos->Row,4);
		pomConflictSetup->RedrawRowCol(prlPos->Row,5,prlPos->Row,5);
		pomConflictSetup->RedrawRowCol(prlPos->Row,6,prlPos->Row,6);
		pomConflictSetup->RedrawRowCol(prlPos->Row,7,prlPos->Row,7);
		pomConflictSetup->RedrawRowCol(prlPos->Row,8,prlPos->Row,8);
	}
}

void GatPosConflictSetupDlg::OnLoadDefault() 
{
	// TODO: Add your control notification handler code here
    CString olRecord;

	int ilRecCount = 0;

	CStringArray olLines;
	// read default values, if there are none, use hard coded values
	if (ogCfgData.ReadConflicts(ogCfgData.pomDefaultName,olLines) == false)
	{
		ogCfgData.GetDefaultConflictSetup(olLines);
	}

	for(int i = 0; i < olLines.GetSize(); i++)
	{
		CString olRecord = olLines.GetAt(i);
		int ilMaxFields = 7;
		int ilMaxBytes  = olRecord.GetLength();
		CONFLICTDATA *prlConflictData = new CONFLICTDATA;
		DWORD ulDisplay;

		for (int ilFieldCount = 0, ilByteCount = 0; ilFieldCount < ilMaxFields; ilFieldCount++)
    	{
        // Extract next field in the specified text-line
			for (int ilFirstByteInField = ilByteCount; ilByteCount < ilMaxBytes && olRecord[ilByteCount] != ';'; ilByteCount++)
				;
			if(ilByteCount <= ilMaxBytes)
			{
				CString olField = olRecord.Mid(ilFirstByteInField,ilByteCount-ilFirstByteInField);
				ilByteCount++;
				switch(ilFieldCount)
				{
				case 0 : 
					prlConflictData->imIndex = atol((LPCSTR)olField);
				break;
				case 1 : 
					prlConflictData->imType = atol((LPCSTR)olField);
				break;
				case 2 : 
					prlConflictData->omName = olField;
				break;
				case 3 : 
					prlConflictData->imDuration = atol((LPCSTR)olField);
				break;
				case 4 :
					ulDisplay = atol((LPCSTR)olField);

					if (ulDisplay & 1 << 0)
						prlConflictData->bmGeneralDesktop = true;

					if (ulDisplay & 1 << 1)
						prlConflictData->bmAircraftPositions = true;

					if (ulDisplay & 1 << 2)
						prlConflictData->bmGates = true;

					if (ulDisplay & 1 << 3)
						prlConflictData->bmCheckinCounters = true;

					if (ulDisplay & 1 << 4)
						prlConflictData->bmBaggageBelts = true;

					if (ulDisplay & 1 << 5)
						prlConflictData->bmLounges = true;

					if (ulDisplay & 1 << 6)
						prlConflictData->bmHighPrio = true;
				break;	
				case 5:		// 050303 MVy: PRF6981: conflict priority, store as sedecimal formatted value

					prlConflictData->clrPriority = ToLong( olField );		// olField must contain a sedecimal colorcode
				break ;

				}
			}
		}

		// check, if an update is necessary
		if (i < omConflicts.GetSize())
		{
			CONFLICTDATA *prlOldData = &omConflicts[i];
			if (*prlOldData != *prlConflictData)
			{
				delete prlOldData;
				omConflicts.SetAt(i,prlConflictData);
				ilRecCount++;
			}
			else
				delete prlConflictData;
		}
		else
		{
			omConflicts.Add(prlConflictData);
			ilRecCount++;
		}
	}

	UpdateGrid();		
	
}

void GatPosConflictSetupDlg::OnSaveDefault() 
{
	// TODO: Add your control notification handler code here
	CWaitCursor olWait;

	CFGDATA rlCfg;

    for (int ilLc = 0; ilLc < omConflicts.GetSize(); ilLc++)
	{
		DWORD ulDisplay = 0;
		ulDisplay |= omConflicts[ilLc].bmGeneralDesktop << 0;
		ulDisplay |= omConflicts[ilLc].bmAircraftPositions << 1;
		ulDisplay |= omConflicts[ilLc].bmGates << 2;
		ulDisplay |= omConflicts[ilLc].bmCheckinCounters << 3;
		ulDisplay |= omConflicts[ilLc].bmBaggageBelts << 4;
		ulDisplay |= omConflicts[ilLc].bmLounges << 5;
		ulDisplay |= omConflicts[ilLc].bmHighPrio << 6;

		CFGDATA rlCfg;
		rlCfg.Urno = ogBasicData.GetNextUrno();
		sprintf(rlCfg.Ctyp, "CONFLICT%02d", ilLc);
		sprintf(rlCfg.Pkno, ogCfgData.pomDefaultName);
		sprintf(rlCfg.Text,"%d;%d;%s;%d;%u;",
		omConflicts[ilLc].imIndex,
		omConflicts[ilLc].imType,
		omConflicts[ilLc].omName,
		omConflicts[ilLc].imDuration,
		ulDisplay
		);

		int length = strlen( rlCfg.Text );
		ASSERT( length + 11 < 2001 );		// fixed length in CFGDATA.Text
		length = sprintf( rlCfg.Text + length, "0x%0.8x;", (long )omConflicts[ilLc].clrPriority );		// 050303 MVy: PRF6981: conflict priority, store as sedecimal formatted value


		ogCfgData.SaveConflictData(ogCfgData.pomDefaultName,&rlCfg);
	}	
}

void GatPosConflictSetupDlg::UpdateGrid()
{
	pomConflictSetup->GetParam()->SetLockReadOnly(FALSE);

	pomConflictSetup->SetRowCount(omConflicts.GetSize());


	// enable immediate update
	pomConflictSetup->LockUpdate(FALSE);

	for (int i = 0; i < omConflicts.GetSize(); i++)
	{
		CGXStyle olStyle; 
		olStyle.SetEnabled(FALSE);
		olStyle.SetReadOnly(TRUE);
		CONFLICTDATA olConflict;

//		COLORREF olColor = COLORREF(WHITE);
		COLORREF olColor = COLORREF(LLGREEN);
		if ( (i > 21 && i < 35) || i > 46 && i < 50 || i < 5 || i > 54)
			olColor = COLORREF(CREAM);

		olConflict = omConflicts[i];

		{
//			pomConflictSetup->SetStyleRange(CGXRange(i+1,1,i+1,1),olStyle.SetValue(olConflict.omName));
			if (!ogDisplayAircraftChanged && olConflict.omName == GetString(IDS_STRING2463))
			{
				pomConflictSetup->SetStyleRange(CGXRange(i+1,1,i+1,1),olStyle.SetInterior(COLORREF(GRAY)).SetValue(""));
				imLineAircraftChange = i;
			}
			else
			{
				pomConflictSetup->SetStyleRange(CGXRange(i+1,1,i+1,1),olStyle.SetInterior(olColor).SetValue(olConflict.omName));
			}

			if (olConflict.imDuration == 0xFFFF)	// min duration disabled ?
			{
				pomConflictSetup->SetStyleRange(CGXRange(i+1,2,i+1,2),olStyle.SetInterior(COLORREF(GRAY)).SetValue(""));
			}
			else
			{
				olStyle.SetEnabled(TRUE);
				olStyle.SetReadOnly(FALSE);
				pomConflictSetup->SetStyleRange(CGXRange(i+1,2,i+1,2),olStyle.SetValue((LONG)olConflict.imDuration));
			}

			if (olConflict.bmGeneralDesktop)
			{
				olStyle.SetEnabled(TRUE);
				olStyle.SetReadOnly(TRUE);
				pomConflictSetup->SetStyleRange(CGXRange(i+1,3,i+1,3),olStyle.SetControl(GX_IDS_CTRL_STATIC).SetValue("#BMP(228)"));
			}
			else
			{
				olStyle.SetEnabled(TRUE);
				olStyle.SetReadOnly(TRUE);
				pomConflictSetup->SetStyleRange(CGXRange(i+1,3,i+1,3),olStyle.SetControl(GX_IDS_CTRL_STATIC).SetValue("#BMP(229)"));
			}

			if (olConflict.bmAircraftPositions)
			{
				olStyle.SetEnabled(TRUE);
				olStyle.SetReadOnly(TRUE);
				pomConflictSetup->SetStyleRange(CGXRange(i+1,4,i+1,4),olStyle.SetControl(GX_IDS_CTRL_STATIC).SetValue("#BMP(228)"));
			}
			else
			{
				olStyle.SetEnabled(TRUE);
				olStyle.SetReadOnly(TRUE);
				pomConflictSetup->SetStyleRange(CGXRange(i+1,4,i+1,4),olStyle.SetControl(GX_IDS_CTRL_STATIC).SetValue("#BMP(229)"));
			}

			if (olConflict.bmGates)
			{
				olStyle.SetEnabled(TRUE);
				olStyle.SetReadOnly(TRUE);
				pomConflictSetup->SetStyleRange(CGXRange(i+1,5,i+1,5),olStyle.SetControl(GX_IDS_CTRL_STATIC).SetValue("#BMP(228)"));
			}
			else
			{
				olStyle.SetEnabled(TRUE);
				olStyle.SetReadOnly(TRUE);
				pomConflictSetup->SetStyleRange(CGXRange(i+1,5,i+1,5),olStyle.SetControl(GX_IDS_CTRL_STATIC).SetValue("#BMP(229)"));
			}

			if (olConflict.bmCheckinCounters)
			{
				olStyle.SetEnabled(TRUE);
				olStyle.SetReadOnly(TRUE);
				pomConflictSetup->SetStyleRange(CGXRange(i+1,6,i+1,6),olStyle.SetControl(GX_IDS_CTRL_STATIC).SetValue("#BMP(228)"));
			}
			else
			{
				olStyle.SetEnabled(TRUE);
				olStyle.SetReadOnly(TRUE);
				pomConflictSetup->SetStyleRange(CGXRange(i+1,6,i+1,6),olStyle.SetControl(GX_IDS_CTRL_STATIC).SetValue("#BMP(229)"));
			}

			if (olConflict.bmBaggageBelts)
			{
				olStyle.SetEnabled(TRUE);
				olStyle.SetReadOnly(TRUE);
				pomConflictSetup->SetStyleRange(CGXRange(i+1,7,i+1,7),olStyle.SetControl(GX_IDS_CTRL_STATIC).SetValue("#BMP(228)"));
			}
			else
			{
				olStyle.SetEnabled(TRUE);
				olStyle.SetReadOnly(TRUE);
				pomConflictSetup->SetStyleRange(CGXRange(i+1,7,i+1,7),olStyle.SetControl(GX_IDS_CTRL_STATIC).SetValue("#BMP(229)"));
			}

			if (olConflict.bmLounges)
			{
				olStyle.SetEnabled(TRUE);
				olStyle.SetReadOnly(TRUE);
				pomConflictSetup->SetStyleRange(CGXRange(i+1,8,i+1,8),olStyle.SetControl(GX_IDS_CTRL_STATIC).SetValue("#BMP(228)"));
			}
			else
			{
				olStyle.SetEnabled(TRUE);
				olStyle.SetReadOnly(TRUE);
				pomConflictSetup->SetStyleRange(CGXRange(i+1,8,i+1,8),olStyle.SetControl(GX_IDS_CTRL_STATIC).SetValue("#BMP(229)"));
			}

			if (olConflict.bmHighPrio)
			{
				olStyle.SetEnabled(TRUE);
				olStyle.SetReadOnly(TRUE);
				pomConflictSetup->SetStyleRange(CGXRange(i+1,9,i+1,9),olStyle.SetControl(GX_IDS_CTRL_STATIC).SetValue("#BMP(246)"));
			}
			else
			{
				olStyle.SetEnabled(TRUE);
				olStyle.SetReadOnly(TRUE);
				pomConflictSetup->SetStyleRange(CGXRange(i+1,9,i+1,9),olStyle.SetControl(GX_IDS_CTRL_STATIC).SetValue("#BMP(229)"));
			}

			{
				// 050303 MVy: PRF6981: conflict priority, setting the cell color
				olStyle.SetEnabled(TRUE);
				olStyle.SetReadOnly(TRUE);
				pomConflictSetup->SetStyleRange( 
					CGXRange( i+1, 10, i+1, 10 ),
					olStyle.SetInterior( olConflict.clrPriority ).SetValue( "" )
				);
			};
		}
	}

	pomConflictSetup->GetParam()->SetLockReadOnly(true);
	pomConflictSetup->SetScrollBarMode(SB_BOTH,gxnAutomatic|gxnEnhanced,TRUE);
	pomConflictSetup->Redraw();
}


void GatPosConflictSetupDlg::SaveConflictSetup()
{
	CWaitCursor olWait;

    for (int ilLc = 0; ilLc < omConflicts.GetSize(); ilLc++)
	{
		ogCfgData.SetConflictData(ilLc,omConflicts[ilLc],true);
	}	

}

void GatPosConflictSetupDlg::HandleAircraftChange()
{
	UpdateGrid();
}
