#ifndef AFX_KONFLIKTE_H__3C7FE601_C23A_11D1_A3F3_0000B45A33F5__INCLUDED_
#define AFX_KONFLIKTE_H__3C7FE601_C23A_11D1_A3F3_0000B45A33F5__INCLUDED_

// Konflikte.h : Header-Datei
//

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld Konflikte 

#include "CCSPtrArray.h"
#include "CCSGlobl.h"
#include "RotationCedaFlightData.h"
#include "DiaCedaFlightData.h"


struct KonfIdent
{
	long FUrno;
	char FPart;
	long RelFUrno;
	char RelFPart;
	int  Type;

	KonfIdent(void)
	{
		FUrno = 0;
		FPart = ' ';
		RelFUrno = 0;
		RelFPart = ' ';
		Type = 0;
	};

	KonfIdent(long lpFUrno, char cpFPart, long lpRelFUrno, char cpRelFPart, int ipType)
	{
		FUrno = lpFUrno;
		FPart = cpFPart;
		RelFUrno = lpRelFUrno;
		RelFPart = cpRelFPart;
		Type = ipType;
	};
	bool KonfIdent::operator==(const KonfIdent &ropOldId) const
	{
		return (FUrno == ropOldId.FUrno && FPart == ropOldId.FPart && RelFUrno == ropOldId.RelFUrno &&
			RelFPart == ropOldId.RelFPart && Type == ropOldId.Type);
	}
	bool KonfIdent::operator!=(const KonfIdent &ropOldId) const
	{
		return !operator==(ropOldId);
	}
};


struct KonfItem
{
	KonfIdent	KonfId;
	CString		Text;
};

 

struct KONFDATA
{
	KonfIdent	KonfId;
	CString		Text;
	bool		Confirmed;
	bool		Flag;  //only for internal use
	CTime		TimeOfConflict;
	int			AlreadyDisplayed;
	int			ColorIndex;
	int			Weight;
	DWORD		SetBy;
	int			SubId;
	bool		NewlyConfirmed;	//used as workaround, marks the KONFDATA if confirm is used (true) in konfliktedlg
								//if AddKonflikt the member will be set to false again
	COLORREF clrPriority ;		// 050303 MVy: PRF6981: conflict priority background color
	bool blHighPrio;

	KONFDATA(void)
	{
		SetBy = 0;
		SubId = 0;
		Confirmed = false;	
		Text.Empty();
		ColorIndex = 0;
		Flag = true;
		TimeOfConflict = TIMENULL;
		NewlyConfirmed = false;
		clrPriority = WHITE ;		// 050303 MVy: PRF6981: conflict priority background color, set default color to white
		blHighPrio = false;
	};


public:

	// 050303 MVy: get the conflict data referring to the current type
	void UpdatePriority( int iType )
	{
		CONFLICTDATA* pConflictData = ogCfgData.GetConflictDataByType( iType );
		//ASSERT( pConflictData );		// I guess the data MUST exist
		// TODO: ... sometimes not 
		if( pConflictData )
		{
			clrPriority = pConflictData->clrPriority ;		// put color from rule into conflict
			blHighPrio = pConflictData->bmHighPrio;
		}
	};	// UpdatePriority

};

struct KONFENTRY
{
	long	Urno;
	int		TypeOfObject;	
	CString	NameOfObject;
	CCSPtrArray <KONFDATA> Data;
};


enum ConflictGroups
{ CONF_ALL, CONF_BLT, CONF_POS, CONF_CCA, CONF_GAT, CONF_WRO};

 
class Konflikte
{
// Konstruktion
public:
	Konflikte();   // Standardkonstruktor

	~Konflikte();

	void ClearAll();


	CCSPtrArray<KONFENTRY> omData;

    CMapPtrToPtr omUrnoMap;

    CMapPtrToPtr omNotConfirmedMap;

	KONFDATA *GetKonflikt(const KonfIdent &ropKonfId);
	KONFDATA *GetKonflikt(long lpFUrno, int ipType);

	void ConfirmInternal(long lpFUrno, int ipType);
	void Confirm(const CPtrArray &ropKonfIds, bool bpRelHdl = false);
	void ConfirmAll(ConflictGroups ipGroup = CONF_ALL);

	void CheckFlight(ROTATIONFLIGHTDATA *prpFlight, bool bpDep = true);
	void CheckFlight(ROTATIONFLIGHTDATA *prpFlight, char cpFPart, bool bpDep = true);

	void CheckFlight(DIAFLIGHTDATA *prpFlight);
	
	void CheckFlightInternal(DIAFLIGHTDATA *prlFlightA, DIAFLIGHTDATA *prlFlightD, bool bpOnlyNeighborConf = false);
	void CheckFlightInternal2(DIAFLIGHTDATA *prlFlightA, DIAFLIGHTDATA *prlFlightD);
	void CheckFlightOnlyNeighborConf(DIAFLIGHTDATA *prlFlightA, DIAFLIGHTDATA *prlFlightD);

	void GetKonflikte(long lpFUrno, char cpFPart, DWORD dwOrigin, int ipSubOrig, CCSPtrArray<KonfItem> &opKonfList);
	void GetKonflikteForDisplay(long lpFUrno, char cpFPart, DWORD dwOrigin, int ipSubOrig, int ipDisplayType, CCSPtrArray<KonfItem> &opKonfList);

	// ResetEntry
	void ResetFlight(long lpFUrno, char cpFPart, DWORD dwOrigin );

	//void CheckEntry(long lpUrno, DWORD dwOrigin);
	void CheckFlightConf(long lpFUrno, char cpFPart, DWORD dwOrigin);


	int GetStatus(long lpFUrno, char cpFPart, DWORD dwOrigin, int ipSubOrig /*= SUB_MOD_ALL*/);
	int GetStatusByDisplay(long lpFUrno, char cpFPart, DWORD dwOrigin, int ipSubOrig,  int ipDisplayType);

	void RemoveAllForFlight(long lpUrno, DWORD dwOrigin);
	void RemoveAllForFlightWwithSubmod(long lpUrno, DWORD dwOrigin, DWORD dwSubId);

//	void SASCheckAll();
	void SASCheckAll(bool bpUseButtonList = false, bool bpTimerList = false);

	void CheckPosition(CString opPst);
	void CheckGate(CString opPst);
	void CheckWro(CString opWro);
	void CheckBlt(CString opBlt);

	void CheckFlightExtConf(long lpUrno, char cpFPart, const CString &ropBez, CedaCflData *popCflData, DWORD wpOrigin);

	/*
	int ipConfirmed = -1	unknown status 
	int ipConfirmed =  0	not confirmed
	int ipConfirmed =  1	confirmed
	*/

	void AddKonflikt(const KonfIdent &ropKonfId, CString opText, DWORD dwOrigin, int ipSubOrig = SUB_MOD_ALL, CTime opTimeOfConf = TIMENULL, int ipConfirmed = -1);

	void EnableAttentionButtonsUpdate();
	void DisableAttentionButtonsUpdate();

	void CheckAttentionButton();


	void CleanUp();

	void RemoveAll(DWORD dwOrigin);

	bool IsBltKonfliktType(int ipKonfType) const;
	bool IsNeighborKonfliktType(int ipKonfType) const;
	bool IsBltKonflikt(int ipKonfType) const;
	bool IsGatKonflikt(int ipKonfType) const;
	bool IsWroKonflikt(int ipKonfType) const;
	bool IsPstKonflikt(int ipKonfType) const;
	bool IsConflictActivated(int ipKonfType) const;
	bool CheckFlightWithCurrentTime(DIAFLIGHTDATA *prpFlight, char cpFPart);
	void ResetAllNeighborConfFlight(long lpFUrno, char cpFPart);

	CTimeSpan omNoOnblAfterLand;
	CTimeSpan omCurrEtai;
	CTimeSpan omCurrANxti;
	CTimeSpan omCurrDNxti;
	CTimeSpan omStoaStod;
	CTimeSpan omStodCurrAndNoAirb;
	CTimeSpan omCurrOfbl;
	CTimeSpan omCurrOfblStod;
	CTimeSpan omStodEtdi;
	CTimeSpan omStoaEtai;
	CTimeSpan omCurrStodGd1x;
	CTimeSpan omCtotEtod;
	CTimeSpan omOfblCtot;


	bool bmNoOnblAfterLand;
	bool bmCurrEtai;
	bool bmCurrANxti;
	bool bmCurrDNxti;
	bool bmStoaStod;
	bool bmStodCurrAndNoAirb;
	bool bmCurrOfbl;
	bool bmCurrOfblStod;
	bool bmStodEtdi;
	bool bmStoaEtai;
	bool bmCurrStodGd1x;
	bool bmFlightDataChanged;
	bool bmPosGateRelation;

	bool bmJoinConf;

	bool bmAderConf;
	bool bmActRegnChangeConf;

	bool bmPstConf;
	bool bmGatConf;
	bool bmWroConf;
	bool bmBltConf;

	void SetConfiguration();

	int imBltConflicts;

private:
	bool bmAttentionButtonUpdate;
	void CheckCommonConflicts(const ROTATIONFLIGHTDATA *prpRotFlight, const DIAFLIGHTDATA *prpDiaFlight, DWORD dwOrigin, int ipSubOrig, const CTime &ropTestTime, const CString &ropFlightText);
public:
	bool NeedCheckConflicts(DIAFLIGHTDATA *prlFlightA, DIAFLIGHTDATA *prlFlightD);
	bool NeedCheckConflicts(ROTATIONFLIGHTDATA *prlFlight);
	bool NeedCheckConflicts(DIAFLIGHTDATA *prlFlight);

};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio f�gt zus�tzliche Deklarationen unmittelbar vor der vorhergehenden Zeile ein.

#endif // AFX_KONFLIKTE_H__3C7FE601_C23A_11D1_A3F3_0000B45A33F5__INCLUDED_
