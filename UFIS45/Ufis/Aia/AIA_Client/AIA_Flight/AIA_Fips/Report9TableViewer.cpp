// SaisonflugplanTableViewer.cpp 
//
//	Fluggesellschaften eines Saisonflugplanes

#include "stdafx.h"
#include "Report9TableViewer.h"
#include "CcsGlobl.h"
#include <iomanip.h>
#include <iostream.h>
#include <fstream.h>


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif


// SaisonflugplanTableViewer
//
SaisonflugplanTableViewer::SaisonflugplanTableViewer(CCSPtrArray<ROTATIONDLGFLIGHTDATA> *popData, char *pcpInfo)
{
	pomData = popData;
	pcmInfo = pcpInfo;

	bmIsFromSearch = FALSE;
    pomSaisonflugplanTable = NULL;
}

//-----------------------------------------------------------------------------------------------

SaisonflugplanTableViewer::~SaisonflugplanTableViewer()
{
    DeleteAll();
	omHeaderDataArray.DeleteAll();
}

//-----------------------------------------------------------------------------------------------

void SaisonflugplanTableViewer::Attach(CCSTable *popTable)
{
    pomSaisonflugplanTable = popTable;
}

//-----------------------------------------------------------------------------------------------
void SaisonflugplanTableViewer::ChangeViewTo(const char *pcpViewName)
{
    DeleteAll();    
    MakeLines();
	UpdateDisplay();
}

//-----------------------------------------------------------------------------------------------
void SaisonflugplanTableViewer::MakeLines()
{
	int ilCntr = 0;
	CString olNewstr("");
	CString olOldstr(""); 

	int ilCount = pomData->GetSize();

	for (int ilLc = ilCount - 1; ilLc >= 0 ; ilLc--)
	{
		ilCntr++;
		ROTATIONDLGFLIGHTDATA *prlSaisonflugplanData = &pomData->GetAt(ilLc);
		if (strlen(prlSaisonflugplanData->Alc2))
			olNewstr = prlSaisonflugplanData->Alc2;
		else
			olNewstr = prlSaisonflugplanData->Alc3;

		if(ilLc == ilCount - 1)	// der erste
		{
			olOldstr = olNewstr;

		}
		if (olNewstr != olOldstr)	// Fluggesellschaft wechselt
		{
			MakeLine(olOldstr, ilCntr);
			olOldstr = olNewstr;
			ilCntr = 0;
		}

		if((ilLc == 0) && (ilCntr > 0))	// nach dem letzten
			MakeLine(olNewstr, ilCntr);
	}
}

//-----------------------------------------------------------------------------------------------

void SaisonflugplanTableViewer::MakeLine(CString Alcold, int ipCntr)
{
    // Update viewer data for this shift record
	char clbuffer[10];
    SAISONFLUGPLANTABLE_LINEDATA rlSaisonflugplan;
	sprintf(clbuffer,"%d", ipCntr);
	rlSaisonflugplan.Cntr = clbuffer; 
	rlSaisonflugplan.Alc3 = Alcold; 

	CreateLine(&rlSaisonflugplan);
}

//-----------------------------------------------------------------------------------------------

void SaisonflugplanTableViewer::CreateLine(SAISONFLUGPLANTABLE_LINEDATA *prpSaisonflugplan)
{
	int ilLineno = 0;
	SAISONFLUGPLANTABLE_LINEDATA rlSaisonflugplan;
	rlSaisonflugplan = *prpSaisonflugplan;
    omLines.NewAt(ilLineno, rlSaisonflugplan);
}

//-----------------------------------------------------------------------------------------------
// UpdateDisplay: Load data selected by filter conditions to the display by using "omTable"

void SaisonflugplanTableViewer::UpdateDisplay()
{
	int ilLines = omLines.GetSize();
	const int ilColumns = 2;	//<===== Spaltenanzahl setzen (von 0 ab)
	omHeaderDataArray.DeleteAll();

	pomSaisonflugplanTable->SetShowSelection(TRUE);
	pomSaisonflugplanTable->ResetContent();

	TABLE_HEADER_COLUMN rlHeader;
	rlHeader.Alignment = COLALIGN_LEFT;
	rlHeader.Font = &ogCourier_Regular_10;
	rlHeader.Length = 160; 
	rlHeader.Text = GetString(IDS_STRING1046);
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 160;
	rlHeader.Text = GetString(IDS_STRING1143);
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);

	pomSaisonflugplanTable->SetHeaderFields(omHeaderDataArray);

	pomSaisonflugplanTable->SetDefaultSeparator();
	pomSaisonflugplanTable->SetTableEditable(FALSE);

	TABLE_COLUMN rlColumnData;
	rlColumnData.Font = &ogCourier_Regular_10;
	rlColumnData.BkColor = WHITE;
	rlColumnData.Alignment = COLALIGN_LEFT;
	
    for (int ilLineNo = 0; ilLineNo < ilLines; ilLineNo++)
	{
		CCSPtrArray<TABLE_COLUMN> olColList;

		if(omLines[ilLineNo].Alc2.GetLength())
			rlColumnData.Text = omLines[ilLineNo].Alc2;
		else
			rlColumnData.Text = omLines[ilLineNo].Alc3;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Cntr;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		pomSaisonflugplanTable->AddTextLine(olColList,(void*)NULL);
		olColList.DeleteAll();
	}
	pomSaisonflugplanTable->DisplayTable();
}


//-----------------------------------------------------------------------------------------------

void SaisonflugplanTableViewer::DeleteLine(int ipLineno)
{
	omLines.DeleteAt(ipLineno);
}

//-----------------------------------------------------------------------------------------------

void SaisonflugplanTableViewer::DeleteAll()
{
    while (omLines.GetSize() > 0)
    DeleteLine(0);
}

//-----------------------------------------------------------------------------------------------
// Printing Routines
//-----------------------------------------------------------------------------------------------

void SaisonflugplanTableViewer::PrintTableView()
{
	CString olFooter1,olFooter2;
	char pclFooter[100];
	//sprintf(pclInfo, "Fluggesellschaften eines Saisonflugplanes \n%s", pcmInfo);
	sprintf(pclFooter, GetString(IDS_STRING1144), (CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M")));
	omTableName = GetString(IDS_STRING1145);
	omFooterName = pclFooter;
	int ilOrientation = PRINT_PORTRAET;//  PRINT_PORTRAET  PRINT_LANDSCAPE
	pomPrint = new CCSPrint((CWnd*)this,ilOrientation,45);// FirstLine(P=200,L=200),LeftOffset(P=200,L=50)

	if (pomPrint != NULL)
	{
		if (pomPrint->InitializePrinter(ilOrientation) == TRUE)
		{
			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
			//pomPrint->imLineNo = pomPrint->imMaxLines + 1;
			pomPrint->imLineNo = pomPrint->imMaxLines + 1 - 2;
			DOCINFO	rlDocInfo;
			memset(&rlDocInfo, 0, sizeof(DOCINFO));
			rlDocInfo.cbSize = sizeof( DOCINFO );
			rlDocInfo.lpszDocName = omTableName;	
			pomPrint->omCdc.StartDoc( &rlDocInfo );
			pomPrint->imPageNo = 0;
			int ilLines = omLines.GetSize();
			olFooter1.Format(GetString(IDS_STRING1445),ilLines,omFooterName);
			for(int i = 0; i < ilLines; i++ ) 
			{
				//if(pomPrint->imLineNo >= pomPrint->imMaxLines)
				if(pomPrint->imLineNo >= pomPrint->imMaxLines-2)
				{
					if(pomPrint->imPageNo > 0)
					{
						olFooter2.Format(GetString(IDS_STRING1198),pomPrint->imPageNo);
						pomPrint->PrintUIFFooter(olFooter1,"",olFooter2);
						pomPrint->omCdc.EndPage();
					}
					PrintTableHeader();
				}
				//if(pomPrint->imLineNo == (pomPrint->imMaxLines-1) || i == (ilLines-1))
				if(pomPrint->imLineNo == (pomPrint->imMaxLines-3) || i == (ilLines-1))
				{
					PrintTableLine(&omLines[i],true);
				}
				else
				{
					PrintTableLine(&omLines[i],false);
				}
			}
			olFooter2.Format(GetString(IDS_STRING1198),pomPrint->imPageNo);
			pomPrint->PrintUIFFooter(olFooter1,"",olFooter2);
			pomPrint->omCdc.EndPage();
			pomPrint->omCdc.EndDoc();
		}
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
	delete pomPrint;

	pomPrint = NULL;
	}
}

//-----------------------------------------------------------------------------------------------

bool SaisonflugplanTableViewer::PrintTableHeader()
{
	pomPrint->omCdc.StartPage();
	pomPrint->imPageNo++;
	pomPrint->imLineNo = 0;
	char pclHeader[100];
	sprintf(pclHeader, GetString(IDS_STRING1146), pcmInfo);
	//sprintf(pclHeader, "  vom %s", pcmInfo);
	CString olTableName(pclHeader);

	//pomPrint->PrintUIFHeader(omTableName,CString(CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M")),pomPrint->imFirstLine-10);
	pomPrint->PrintUIFHeader(" ",CString(pclHeader),pomPrint->imFirstLine-10);

	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;

	rlElement.Alignment   = PRINT_LEFT;
	rlElement.FrameLeft   = PRINT_NOFRAME;
	rlElement.FrameRight  = PRINT_NOFRAME;
	rlElement.FrameTop    = PRINT_NOFRAME;
	rlElement.FrameBottom = PRINT_FRAMEMEDIUM;
	rlElement.pFont       = &pomPrint->ogCourierNew_Bold_8;

	int ilSize = omHeaderDataArray.GetSize();
	for(int i=0;i<ilSize;i++)
	{
		rlElement.Length = (int)(omHeaderDataArray[i].Length*dgCCSPrintFactor);
		if(rlElement.Length < igCCSPrintMinLength)rlElement.Length+=igCCSPrintMoreLength; 
		rlElement.Text   = omHeaderDataArray[i].Text;
		rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
	}
	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();


	return true;
}

//-----------------------------------------------------------------------------------------------

bool SaisonflugplanTableViewer::PrintTableLine(SAISONFLUGPLANTABLE_LINEDATA *prpLine,bool bpLastLine)
{
	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;
	rlElement.pFont      = &pomPrint->ogCourierNew_Regular_8;
	int ilSize = omHeaderDataArray.GetSize();
	for(int i=0;i<ilSize;i++)
	{
		
		rlElement.Length = (int)(omHeaderDataArray[i].Length*dgCCSPrintFactor);
		if(rlElement.Length < igCCSPrintMinLength)rlElement.Length+=igCCSPrintMoreLength; 
		rlElement.Alignment  = PRINT_LEFT;
		rlElement.FrameTop   = PRINT_FRAMETHIN;
		if(bpLastLine == true)
		{
			rlElement.FrameBottom = PRINT_FRAMETHIN;
		}
		else
		{
			rlElement.FrameBottom = PRINT_NOFRAME;
		}
		rlElement.FrameLeft  = PRINT_NOFRAME;
		rlElement.FrameRight = PRINT_NOFRAME;
		switch(i)
		{
		case 0:
			{
				rlElement.FrameLeft  = PRINT_FRAMETHIN;
				if(strlen(prpLine->Alc2))
					rlElement.Text = prpLine->Alc2;
				else
					rlElement.Text = prpLine->Alc3;
			}
			break;
		case 1:
			{
				rlElement.FrameRight = PRINT_FRAMETHIN;
				rlElement.Text		= prpLine->Cntr;
			}
			break;
		}
		rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
		
	}
	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();
	return true;
}

//-----------------------------------------------------------------------------------------------

// Drucken in Datei
void SaisonflugplanTableViewer::PrintPlanToFile(char *pcpDefPath)
{
	ofstream of;
	of.open( pcpDefPath, ios::out);
	of << GetString(IDS_STRING928) << " " 
		<< pcmInfo << "    "
		<< CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M") << endl << endl;

	of  << GetString(IDS_STRING1046) << "    "
		<< GetString(IDS_STRING1043) << "       "  
		<< endl;
	of << "---------------------------------------------------------------" << endl;
	int ilCount = omLines.GetSize();
	for(int i = 0; i < ilCount; i++)
	{
		SAISONFLUGPLANTABLE_LINEDATA rlD = omLines[i];
		of.setf(ios::left, ios::adjustfield);
		of   << setw(20) << rlD.Alc3  
			 << setw(15) << rlD.Cntr << endl;
	}
	of.close();
//	MessageBox("Es wurde folgende Datei geschrieben:\nc:\\tmp\\Saisonflug.txt", "Drucken",(MB_ICONINFORMATION|MB_OK));
}