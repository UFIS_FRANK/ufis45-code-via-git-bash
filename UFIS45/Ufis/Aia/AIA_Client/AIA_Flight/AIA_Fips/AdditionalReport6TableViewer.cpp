// AdditionalReport6TableViewer.cpp : implementation file
// 
// List off all flights same TTyp (Flight Nature)
// keine Angabe einer airline

#include "stdafx.h"
#include "AdditionalReport6TableViewer.h"
#include "CcsGlobl.h"
#include <iomanip.h>
#include <iostream.h>
#include <fstream.h>
#include "BasicData.h"
#include "Utils.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif


/////////////////////////////////////////////////////////////////////////////
// AdditionalReport6TableViewer
//


AdditionalReport6TableViewer::AdditionalReport6TableViewer(CCSPtrArray<ROTATIONDLGFLIGHTDATA> *popData,
														   const CStringArray &opShownColumns,
														   char *pcpInfo,
														   char *pcpSelect)
{

	pomData = popData;
	pcmInfo = pcpInfo;
	pcmSelect = pcpSelect;


	bmIsFromSearch = false;
    pomTable = NULL;

	omShownColumns.Copy(opShownColumns);

	imFlightCount = 0;

}

AdditionalReport6TableViewer::~AdditionalReport6TableViewer()
{

	omPrintHeadHeaderArray.DeleteAll();
	omAnzChar.RemoveAll();
	omShownColumns.RemoveAll();
    DeleteAll();

	omColumn0.RemoveAll();
	omColumn1.RemoveAll();
	omColumn2.RemoveAll();
	omCountColumn1.RemoveAll();
	omCountColumn2.RemoveAll();

}


void AdditionalReport6TableViewer::SetColumnsToShow(const CStringArray &opShownColumns)
{

	CCS_TRY

	omShownColumns.Copy(opShownColumns);

	CCS_CATCH_ALL

}


void AdditionalReport6TableViewer::SetParentDlg(CDialog* ppParentDlg)
{

	pomParentDlg = ppParentDlg;

}


void AdditionalReport6TableViewer::Attach(CCSTable *popTable)
{

    pomTable = popTable;

}


int AdditionalReport6TableViewer::GetVia(CCSPtrArray<VIADATA> *opVias, ROTATIONDLGFLIGHTDATA *prpFlight)
{
	opVias->DeleteAll();

	if(prpFlight == NULL)
		return 0;
	if(strlen(prpFlight->Vial) == 0)
		return 0;

	CString olVias(prpFlight->Vial);
	CString olFids;
	CString olApc3;
	CString olApc4;

	VIADATA *prlVia;

	while(olVias.IsEmpty() != TRUE)
	{
			prlVia = new VIADATA;
			opVias->Add(prlVia);

			if(olVias.GetLength() < 120)
			{
				olVias += "                                                                                                                             ";
				olVias = olVias.Left(120);
			}


			olApc3 = olVias.Mid(1,3);
			olApc3.TrimLeft();
			sprintf(prlVia->Apc3, olApc3);

			if(olVias.GetLength() >= 120)
				olVias = olVias.Right(olVias.GetLength() - 120);

	}
	return opVias->GetSize();
}


void AdditionalReport6TableViewer::ChangeViewTo(const char *pcpViewName)
{
	TRACE("AdditionalReport6TableViewer::ChangeViewTo\n");
   
    pomTable->ResetContent();
    DeleteAll();    
    MakeLines();

	UpdateDisplay();
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
}


/////////////////////////////////////////////////////////////////////////////
// AdditionalReport6TableViewer -- code specific to this class

void AdditionalReport6TableViewer::MakeLines()
{
	ROTATIONDLGFLIGHTDATA *prlAFlight;
	ROTATIONDLGFLIGHTDATA *prlDFlight;
	ROTATIONDLGFLIGHTDATA *prlNextFlight = NULL;
	ROTATIONDLGFLIGHTDATA *prlFlight;
	int ilLineNo;

	bool blRDeparture = false;

	int ilFlightCount = pomData->GetSize();

	for (int ilLc = 0; ilLc < ilFlightCount; ilLc++)
	{
		prlFlight = &(*pomData)[ilLc];

		if(ilLc + 1 < ilFlightCount)
			prlNextFlight = &(*pomData)[ilLc + 1];	
		else
			prlNextFlight = NULL;	

		//Arrival
		if((strcmp(prlFlight->Org3, pcgHome) != 0) && (strcmp(prlFlight->Des3, pcgHome) == 0))
		{
			prlAFlight = prlFlight;
			prlDFlight = NULL;
			if(prlNextFlight != NULL)
			{
				if((prlNextFlight->Rkey == prlFlight->Rkey) && (prlFlight->Rkey != NULL))
				{
					prlDFlight = prlNextFlight;

					if((strcmp(prlNextFlight->Org3, pcgHome) == 0) && (strcmp(prlNextFlight->Des3, pcgHome) != 0))
					{
						ilLc++;
					}
					else
						blRDeparture = true;
				}
			}
		}
		else
		{
			// Departure
			if((strcmp(prlFlight->Org3, pcgHome) == 0) && (strcmp(prlFlight->Des3, pcgHome) != 0))
			{
				prlAFlight = NULL;
				prlDFlight = prlFlight;
				blRDeparture = false;
			}
			else
			{
				//Turnaround
				if((strcmp(prlFlight->Org3, pcgHome) == 0) && (strcmp(prlFlight->Des3, pcgHome) == 0))
				{
					if(blRDeparture)
					{
						blRDeparture = false;
						prlAFlight = prlFlight;
						prlDFlight = NULL;
						if(prlNextFlight != NULL)
						{
							if((prlNextFlight->Rkey == prlFlight->Rkey) && (prlFlight->Rkey != NULL))
							{
								prlDFlight = prlNextFlight;
								if((strcmp(prlNextFlight->Org3, pcgHome) == 0) && (strcmp(prlNextFlight->Des3, pcgHome) != 0))
								{
									ilLc++;
								}
								else
									blRDeparture = true;
							}
						}
					}
					else
					{
						prlAFlight		= prlFlight;
						prlDFlight		= prlFlight;
						if(prlNextFlight != NULL)
						{
							if((prlNextFlight->Rkey == prlFlight->Rkey) && (prlFlight->Rkey != NULL))
							{
								ilLc--;
								prlAFlight	 = NULL;
								blRDeparture = true;
							}
						}
					}
				}
			}
		}
		ilLineNo = MakeLine(prlAFlight, prlDFlight);

		// Listen zum Zaehlen aufbauen
		MakeCountList(prlAFlight, prlDFlight);

	}


}

		
int AdditionalReport6TableViewer::MakeLine(ROTATIONDLGFLIGHTDATA *prpAFlight, ROTATIONDLGFLIGHTDATA *prpDFlight)
{
    ADDITIONAL_REPORT6_LINEDATA rlLine;

	if((prpAFlight != NULL) && (prpDFlight != NULL) )
	{
		imFlightCount += 2;
		MakeLineData(prpAFlight, prpDFlight, rlLine);
		return CreateLine(rlLine);
	}
	if (prpAFlight != NULL)
	{
		imFlightCount++;
		MakeLineData(prpAFlight, NULL, rlLine);
		return CreateLine(rlLine);
	}
	if (prpDFlight != NULL)
	{
		imFlightCount++;
		MakeLineData(NULL, prpDFlight, rlLine);
		return CreateLine(rlLine);
	}
	return -1;
}




void AdditionalReport6TableViewer::MakeLineData(ROTATIONDLGFLIGHTDATA *prpAFlight, ROTATIONDLGFLIGHTDATA *prpDFlight, ADDITIONAL_REPORT6_LINEDATA &rpLine)
{
	CString olStr;
	if(prpAFlight != NULL)
	{
		rpLine.AUrno =  prpAFlight->Urno;
		rpLine.ARkey =  prpAFlight->Rkey;
		rpLine.AFlno = CString(prpAFlight->Flno);
		rpLine.ADate = DateToHourDivString(prpAFlight->Onbl, prpAFlight->Stoa);
		rpLine.ATtyp = prpAFlight->Ttyp; 
		rpLine.AOrg3 = CString(prpAFlight->Org3);
		//rpLine.AVia3 = CString(prpAFlight->Via3);
		rpLine.AAct = CString(prpAFlight->Act3);
		rpLine.AAlc2 = CString(prpAFlight->Alc2);
		rpLine.AAlc3 = CString(prpAFlight->Alc3);

		rpLine.AStoa = CTime(prpAFlight->Stoa);
		if(bgReportLocal) ogBasicData.UtcToLocal(rpLine.AStoa);
		rpLine.AEtai = CTime(prpAFlight->Etai);
		if(bgReportLocal) ogBasicData.UtcToLocal(rpLine.AEtai);
		rpLine.ALand = CTime(prpAFlight->Land);
		if(bgReportLocal) ogBasicData.UtcToLocal(rpLine.ALand);


		CCSPtrArray<VIADATA> opVias;
		int ilViaCnt = GetVia(&opVias, prpAFlight);

		if(opVias.GetSize() == 0)
			rpLine.AVia3 = "";
		else
		{
			rpLine.AVia3 =  opVias[ilViaCnt-1].Apc3;	// letzter Via vor Homeairport = letzte Zeile in Vial
		}
		opVias.DeleteAll();


	}
	else
	{

		rpLine.AUrno =  0;
		rpLine.ARkey =  0;
		rpLine.AFlno = "";
		rpLine.ADate = ""; 
		rpLine.ATtyp = ""; 
		rpLine.AOrg3 = "";
		rpLine.AVia3 = "";
		rpLine.AAct = "";
		rpLine.AAlc2 = "";
		rpLine.AAlc3 = "";
		rpLine.AEtai = -1;
		rpLine.ALand = -1;
	}


	if(prpDFlight != NULL)
	{

		rpLine.DUrno =  prpDFlight->Urno;
		rpLine.DRkey =  prpDFlight->Rkey;
		rpLine.DFlno = CString(prpDFlight->Flno); 
		rpLine.DTtyp = CString(prpDFlight->Ttyp);
		rpLine.DDes3 = CString(prpDFlight->Des3);
		rpLine.DAct = CString(prpDFlight->Act3);
		rpLine.DAlc2 = CString(prpDFlight->Alc2);
		rpLine.DAlc3 = CString(prpDFlight->Alc3);

		if (rpLine.AAct.IsEmpty())
			rpLine.AAct = rpLine.DAct;

		CCSPtrArray<VIADATA> opVias;
		int ilViaCnt = GetVia(&opVias, prpDFlight);

		if(opVias.GetSize() == 0)
			rpLine.DVia3 = "";
		else
		{
			rpLine.DVia3 =  opVias[0].Apc3;	// erster Via nach Homeairport = erste Zeile in Vial
		}
		opVias.DeleteAll();

	
	}
	else
	{
		rpLine.DUrno =  0;
		rpLine.DRkey =  0;
		rpLine.DFlno = "";
		rpLine.DDate = ""; 
		rpLine.DTtyp = ""; 
		rpLine.DDes3 = "";
		rpLine.DVia3 = "";
		rpLine.DAct = "";
		rpLine.DAlc2 = "";
		rpLine.DAlc3 = "";
	}
    return;
}



int AdditionalReport6TableViewer::CreateLine(ADDITIONAL_REPORT6_LINEDATA &rpLine)
{
    int ilLineCount = omLines.GetSize();

    for (int ilLineno = ilLineCount; ilLineno > 0; ilLineno--)
        if (CompareFlight(&rpLine, &omLines[ilLineno-1]) >= 0)
            break;  // should be inserted after Lines[ilLineno-1]

    omLines.NewAt(ilLineno, rpLine);
	return ilLineno;
}


void AdditionalReport6TableViewer::DeleteLine(int ipLineno)
{
	omLines.DeleteAt(ipLineno);
}



void AdditionalReport6TableViewer::DeleteAll()
{
    while (omLines.GetSize() > 0)
        DeleteLine(0);
}


/////////////////////////////////////////////////////////////////////////////
// AdditionalReport6TableViewer - display drawing routine



// UpdateDisplay: Load data selected by filter conditions to the display by using "omTable"
void AdditionalReport6TableViewer::UpdateDisplay()
{

	CCS_TRY

	pomTable->ResetContent();

	ADDITIONAL_REPORT6_LINEDATA *prlLine;

	// Anzahl an Zeichen zur Berechnung der Spaltenbreite ermitteln
	int ilColumns = omShownColumns.GetSize();
	for (int i = 0; i < ilColumns; i++)
	{
		// Aufruf, damit omAnzCount Array gef�llt ist
		SetFieldLength(omShownColumns[i]);
	}
	DrawHeader();
  
/*	for (int ilLc = 0; ilLc < omLines.GetSize(); ilLc++)
	{
		int ilColumnNo = 0;
		prlLine = &omLines[ilLc];
		CCSPtrArray<TABLE_COLUMN> olColList;
	
		MakeColList(prlLine, olColList);

		pomTable->AddTextLine(olColList, (void*)(prlLine));
		olColList.DeleteAll();
	}*/
	for (int ilLc = 0; ilLc < omColumn0.GetSize(); ilLc++)
	{
		int ilColumnNo = 0;
		prlLine = &omLines[ilLc];
		CCSPtrArray<TABLE_COLUMN> olColList;
	
		MakeColList(prlLine, olColList, omColumn0[ilLc]);

		pomTable->AddTextLine(olColList, (void*)(prlLine));
		olColList.DeleteAll();
	}

    pomTable->DisplayTable();

	CCS_CATCH_ALL

}


void AdditionalReport6TableViewer::DrawHeader()
{

	CCS_TRY

	CCSPtrArray <TABLE_HEADER_COLUMN> omHeaderDataArray;

	pomTable->SetShowSelection(true);
	pomTable->ResetContent();

	TABLE_HEADER_COLUMN rlHeader;
	rlHeader.Font = &ogCourier_Bold_10;
	int i;

	// alle n Spalten (=omShownColumns.GetSize()) definieren
	int ilColums = omShownColumns.GetSize();
	int ilAnzChar = omAnzChar.GetSize();
	for (i = 0; i < ilColums; i++)
	{
		rlHeader.Alignment = COLALIGN_CENTER;
		// Anzahl an Textzeichen des Feldes der i-ten Spalte
		if (i <= ilAnzChar)
			rlHeader.AnzChar = omAnzChar[i];
		else
			break;
		// alten String l�schen, damit neuer String bei '0' anfaengt
		rlHeader.String = "";
		// Anzahl an Buchstaben 'W' setzen
		int ilAnzChar = omAnzChar[i];
		for (int j = 0; j < ilAnzChar; j++) 
			rlHeader.String += "W";
		// Text aus Konfiguration als Ueberschrift setzen
		rlHeader.Text = GetHeaderContent(omShownColumns[i]);
		// neue Spaltenheaderstruktur setzen
		omHeaderDataArray.New(rlHeader);
	}

	pomTable->SetHeaderFields(omHeaderDataArray);
	omHeaderDataArray.DeleteAll();

	pomTable->SetDefaultSeparator();

	CCS_CATCH_ALL

}




void AdditionalReport6TableViewer::MakeColList(ADDITIONAL_REPORT6_LINEDATA *prlLine, 
													   CCSPtrArray<TABLE_COLUMN> &olColList,
													   CString opCurrentAirline)
{

	CCS_TRY

	TABLE_COLUMN rlColumnData;

	rlColumnData.VerticalSeparator = SEPA_NONE;
	rlColumnData.SeparatorType = SEPA_NONE;
	rlColumnData.Font = &ogCourier_Regular_9;
		
	// Spaltendefinitionen bestimmen
	for (int i = 0; i < omShownColumns.GetSize(); i++)
	{
		rlColumnData.Alignment = COLALIGN_LEFT;
		// Holen des Feldinhaltes der i-ten Spalte und n-ten Zeile
		rlColumnData.Text = GetFieldContent(prlLine, omShownColumns[i], opCurrentAirline);

		olColList.NewAt(olColList.GetSize(), rlColumnData);
	}

	CCS_CATCH_ALL

}




int AdditionalReport6TableViewer::CompareFlight(ADDITIONAL_REPORT6_LINEDATA *prpLine1, ADDITIONAL_REPORT6_LINEDATA *prpLine2)
{
	int	ilCompareResult;
	CTime olTime1;
	CTime olTime2;


		ilCompareResult = (olTime1 == olTime2)? 0:
			(olTime1 > olTime2)? 1: -1;

		 return ilCompareResult;
}




//-----------------------------------------------------------------------------------------------
// Printing Routines
//-----------------------------------------------------------------------------------------------

void AdditionalReport6TableViewer::GetHeader()
{
	
	CCS_TRY
		
	TABLE_HEADER_COLUMN *prlHeader[50];

	int i;
	int ilColomns = omShownColumns.GetSize();
	CSize olSize;
    pomPrint->omCdc.SelectObject(&ogCourier_Bold_10);

	for (i = 0; i < ilColomns; i++)
	{
		olSize = pomPrint->omCdc.GetTextExtent(GetHeaderContent(omShownColumns[i]));
		prlHeader[i] = new TABLE_HEADER_COLUMN;
		prlHeader[i]->Alignment = COLALIGN_CENTER;
		prlHeader[i]->Font = &ogCourier_Bold_10;
		prlHeader[i]->Length = olSize.cx;
		prlHeader[i]->Text = GetHeaderContent(omShownColumns[i]);
	}

	omPrintHeadHeaderArray.DeleteAll();
	for(i = 0; i < ilColomns; i++)
	{
		omPrintHeadHeaderArray.Add(prlHeader[i]);
	}

	CCS_CATCH_ALL

}



void AdditionalReport6TableViewer::PrintTableView()
{

	CCS_TRY

	CString olFooter1,olFooter2;
	CString olTableName = GetString(1256);
	int ilOrientation = PRINT_LANDSCAPE;
	pomPrint = new CCSPrint(pomParentDlg,ilOrientation,45);
	pomPrint->imMaxLines = 38;

	if (pomPrint != NULL)
	{

		if (pomPrint->InitializePrinter(ilOrientation) == TRUE)
		{
			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
			pomPrint->imLineNo = pomPrint->imMaxLines + 1;
			DOCINFO	rlDocInfo;
			memset(&rlDocInfo, 0, sizeof(DOCINFO));
			rlDocInfo.cbSize = sizeof( DOCINFO );
			rlDocInfo.lpszDocName = olTableName;	
			pomPrint->omCdc.StartDoc( &rlDocInfo );
			pomPrint->imPageNo = 0;
			int ilLines = omLines.GetSize();  
			olFooter1.Format("%s %d, %s, %s",GetString(IDS_STRING329),ilLines,olTableName, 
				            (CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M")));
			olFooter1.Format("%s %d",rlDocInfo.lpszDocName, imFlightCount);

			GetHeader();

			if (ilLines == 0)
				PrintTableHeader(); // Drucken leerer Tabelle
			for(int i = 0; i < omColumn0.GetSize()/*ilLines*/; i++ ) 
			{
				if(pomPrint->imLineNo >= pomPrint->imMaxLines)
				{
					if(pomPrint->imPageNo > 0)
					{
						olFooter2.Format(GetString(IDS_STRING1198),pomPrint->imPageNo);
						pomPrint->PrintUIFFooter(olFooter1,"",olFooter2);
						pomPrint->omCdc.EndPage();
					}
					PrintTableHeader();
				}
				if(pomPrint->imLineNo == (pomPrint->imMaxLines-1) || i == (ilLines-1))
				{
					PrintTableLine(&omLines[i],true, omColumn0[i]);
				}
				else
				{
					PrintTableLine(&omLines[i],false, omColumn0[i]);
				}
			}
			olFooter2.Format(GetString(IDS_STRING1198),pomPrint->imPageNo);
			pomPrint->PrintUIFFooter(olFooter1,"",olFooter2);
			pomPrint->omCdc.EndPage();
			pomPrint->omCdc.EndDoc();
		}

		omPrintHeadHeaderArray.DeleteAll();
		SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
		delete pomPrint;
		pomPrint = NULL;
	}

	CCS_CATCH_ALL

}


//-----------------------------------------------------------------------------------------------

bool AdditionalReport6TableViewer::PrintTableHeader()
{
	pomPrint->omCdc.StartPage();
	pomPrint->imPageNo++;
	pomPrint->imLineNo = 0;
	double dgCCSPrintFactor = 2.7 ;
	char pclHeader[256];

	sprintf(pclHeader, GetString(IDS_STRING1600), pcmInfo);
	CString olTableName(pclHeader);

	pomPrint->PrintUIFHeader(omTableName,CString(pclHeader),pomPrint->imFirstLine-10);

	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;

	rlElement.Alignment   = PRINT_LEFT;
	rlElement.FrameLeft   = PRINT_NOFRAME;
	rlElement.FrameRight  = PRINT_NOFRAME;
	rlElement.FrameTop    = PRINT_NOFRAME;
	rlElement.FrameBottom = PRINT_FRAMEMEDIUM;
	rlElement.pFont       = &pomPrint->ogCourierNew_Bold_8;

	//int ilSize = omHeaderDataArray.GetSize();
	int ilSize = omPrintHeadHeaderArray.GetSize();

	for(int i=0;i<ilSize;i++)
	{
		//rlElement.Length = (int)(omHeaderDataArray[i].Length*dgCCSPrintFactor);
		rlElement.Length = (int)(omPrintHeadHeaderArray[i].Length*dgCCSPrintFactor);
		if(rlElement.Length < igCCSPrintMinLength)rlElement.Length+=igCCSPrintMoreLength; 
		//rlElement.Text   = omHeaderDataArray[i].Text;
		rlElement.Text   = omPrintHeadHeaderArray[i].Text;
		rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
	}
	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();


	return true;
}

//-----------------------------------------------------------------------------------------------

bool AdditionalReport6TableViewer::PrintTableLine(ADDITIONAL_REPORT6_LINEDATA *prpLine,
														  bool bpLastLine,
														  CString opCurrentAirline)
{

	CCS_TRY

	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;
	double dgCCSPrintFactor = 2.7;

    pomPrint->omCdc.SelectObject(&pomPrint->ogCourierNew_Regular_8);
	CSize olSize;
		
	rlElement.pFont = &pomPrint->ogCourierNew_Regular_8;

	int ilSize = omPrintHeadHeaderArray.GetSize();
	for(int i=0;i<ilSize;i++)
	{
		olSize = pomPrint->omCdc.GetTextExtent(omShownColumns[i]);
		
		rlElement.Length = (int)(omPrintHeadHeaderArray[i].Length*dgCCSPrintFactor);
		if(rlElement.Length < igCCSPrintMinLength)
			rlElement.Length+=igCCSPrintMoreLength; 
		rlElement.Alignment  = PRINT_LEFT;
		rlElement.FrameTop   = PRINT_FRAMETHIN;
		if(bpLastLine == true)
		{
			rlElement.FrameBottom = PRINT_FRAMETHIN;
		}
		else
		{
			rlElement.FrameBottom = PRINT_FRAMETHIN/*PRINT_NOFRAME*/;
		}
		
		rlElement.FrameLeft  = PRINT_NOFRAME;
		rlElement.FrameRight = PRINT_NOFRAME;
		rlElement.Text = GetFieldContent(prpLine, omShownColumns[i], opCurrentAirline);

		rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
		
	}

	pomPrint->PrintLine(rlPrintLine);

	rlPrintLine.DeleteAll();
	
	CCS_CATCH_ALL

	return true;

}

//-----------------------------------------------------------------------------------------------


// Drucken in Datei
void AdditionalReport6TableViewer::PrintPlanToFile(char *pcpDefPath)
{

	CCS_TRY

	ofstream of;
	int ilCountLines = omColumn0.GetSize();
	int ilCountRows  = omShownColumns.GetSize();

	char pclHeader[256];
	sprintf(pclHeader, GetString(IDS_STRING1600), pcmInfo);


	of.open( pcpDefPath, ios::out);
	of << CString(pclHeader) << " "
		<< CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M") << endl << endl;

	for (int i = 0; i < ilCountRows; i++)
	{
		of << GetHeaderContent(omShownColumns[i]) << "   ";
	}
	               
	of << endl << "-----------------------------------------------------" << endl;


	// zeilen iterieren
	for(int ilLines = 0; ilLines < ilCountLines; ilLines++)
	{
		ADDITIONAL_REPORT6_LINEDATA rlLine = omLines[ilLines];

		of.setf(ios::left, ios::adjustfield);
		
		of 
		<< setw(strlen(omShownColumns[0]) + 4) << GetFieldContent(&rlLine, omShownColumns[0], omColumn0[ilLines])
		<< setw(GetHeaderContent(omShownColumns[1]).GetLength() + 4) << GetFieldContent(&rlLine, omShownColumns[1], omColumn0[ilLines])
		<< setw(GetHeaderContent(omShownColumns[2]).GetLength() + 4) << GetFieldContent(&rlLine, omShownColumns[2], omColumn0[ilLines])
		// Zeile abschliessen
		<< endl;
	}

	// stream schliessen
	of.close();

	CCS_CATCH_ALL
	
}


int AdditionalReport6TableViewer::GetFlightCount()
{

	return imFlightCount;

}


void AdditionalReport6TableViewer::MakeCountList(ROTATIONDLGFLIGHTDATA *prpAFlight, ROTATIONDLGFLIGHTDATA *prpDFlight)
{

	CCS_TRY

	int i;
	bool blItemExists = false;

	// keine Daten? --> tschuess
	if (prpAFlight == NULL && prpDFlight == NULL)
		return;

	// ACType aus Ankunftsteil
	if (prpAFlight != NULL)
	{
		// Pruefen, ob ACType bereits im ArrivalArray vorhanden
		for (i = 0; i < omColumn1.GetSize(); i++)
		{
			if (omColumn1[i] == CString(prpAFlight->Ttyp))
			{
				blItemExists = true;
				break;
			}
		}
		// neuer ACType in ArrivalArray einfuegen
		if (!blItemExists)
		{
			omColumn1.Add(CString(prpAFlight->Ttyp));
			omCountColumn1.Add(1);
		}
		// wenn schon vorhanden, Zaehler erhoehen
		else
		{
			omCountColumn1.SetAt(i, omCountColumn1.GetAt(i)+1);
		}

		// default wieder setzen
		blItemExists = false;
		// Pruefen, ob ACType bereits im Array vorhanden
		for (i = 0; i < omColumn0.GetSize(); i++)
		{
			if (omColumn0[i] == CString(prpAFlight->Ttyp))
			{
				blItemExists = true;
				break;
			}
		}
		if (!blItemExists)
			InsertSortInStrArray(omColumn0, CString(prpAFlight->Ttyp));
	}

	// wieder auf false als default zuruecksetzen
	blItemExists = false;

	// ACType aus Abflugsteil
	if ( prpDFlight != NULL)
	{
		// Pruefen, ob Airline bereits im Array vorhanden
		for (i = 0; i < omColumn2.GetSize(); i++)
		{
			if (omColumn2[i] == CString(prpDFlight->Ttyp))
			{
				blItemExists = true;
				break;
			}
		}
		// neue Airline in DepartureArray einfuegen
		if (!blItemExists)
		{
			omColumn2.Add(CString(prpDFlight->Ttyp));
			omCountColumn2.Add(1);
		}
		// wenn schon vorhanden, Zaehler erhoehen
		else
		{
			omCountColumn2.SetAt(i, omCountColumn2.GetAt(i)+1);
		}

		// default wieder setzen
		blItemExists = false;
		// Pruefen, ob Airline bereits im Array vorhanden
		for (i = 0; i < omColumn0.GetSize(); i++)
		{
			if (omColumn0[i] == CString(prpDFlight->Ttyp))
			{
				blItemExists = true;
				break;
			}
		}
		if (!blItemExists)
			InsertSortInStrArray(omColumn0, CString(prpDFlight->Ttyp));
	}

	// wieder auf false als default zuruecksetzen
	blItemExists = false;


	CCS_CATCH_ALL

}


CString AdditionalReport6TableViewer::GetFieldContent(ADDITIONAL_REPORT6_LINEDATA* prlLine,
										              CString opCurrentColumns,
													  CString opCurrentACType)
{

	CString olFormat;
	int i = 0;

	// -----------------
	// AdditionalReport1
	// -----------------
	if (opCurrentColumns == "FlightNature")
	{
		return opCurrentACType;
	}
	if (opCurrentColumns == "AFlights")
	{
		for (int i = 0; i < omColumn1.GetSize(); i++)
		{
			if (omColumn1[i] == opCurrentACType)
			{
				olFormat.Format("%d", omCountColumn1[i]);
				return olFormat;
			}
		}

		return "0";
	}
	if (opCurrentColumns == "DFlights")
	{
		for (int i = 0; i < omColumn2.GetSize(); i++)
		{
			if (omColumn2[i] == opCurrentACType)
			{
				olFormat.Format("%d", omCountColumn2[i]);
				return olFormat;
			}
		}

		return "0";
	}

	return "";

}


// Setzen der Spaltenbreite in Anzahl Zeichen
void AdditionalReport6TableViewer::SetFieldLength(CString opCurrentColumns)
{

	CCS_TRY

	// -----------------
	// AdditionalReport1
	// -----------------
	if (opCurrentColumns == "FlightNature")
	{
		omAnzChar.Add(8);
		return;
	}
	if (opCurrentColumns == "AFlights")
	{
		omAnzChar.Add(15);
		return;
	}
	if (opCurrentColumns == "DFlights")
	{
		omAnzChar.Add(15);
		return;
	}

	// falls kein if passt, ein default-Wert, sonst Array zu klein
	omAnzChar.Add(11);

	CCS_CATCH_ALL

}


CString AdditionalReport6TableViewer::GetHeaderContent(CString opCurrentColumns)
{

	CCS_TRY

	if (opCurrentColumns == "FlightNature")
	{
		return GetString(IDS_STRING1597);
	}
	if (opCurrentColumns == "AFlights")
	{
		return GetString(IDS_STRING1559);
	}
	if (opCurrentColumns == "DFlights")
	{
		return GetString(IDS_STRING1560);
	}

	CCS_CATCH_ALL

	// wenn nix gefunden Leerstring zurueckgeben, sonst undefiniert
	return CString("");

}