// DailyTableViewer.cpp : implementation file
// 
// Modification History: 


#include "stdafx.h"
#include "CcsGlobl.h"
#include "CCSDefines.h"
#include "CedaCfgData.h"
#include "CViewer.h"
#include "DailyTableViewer.h"
#include <iomanip.h>
#include <iostream.h>
#include <fstream.h>
#include "BasicData.h"
#include "resrc1.h"
#include "Utils.h"
#include "CedaAptLocalUtc.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif


#define COLOR_OPERATION BLACK
#define COLOR_SCHEDULED BLUE
#define COLOR_PROGNOSE  GREEN
#define COLOR_CXX		RED
#define COLOR_NOOP		ORANGE
#define COLOR_TMOA		YELLOW
#define COLOR_LAND		GREEN
#define COLOR_OFBL		GREEN
#define COLOR_ONBL		GRAY
#define COLOR_AIRB		SILVER


static void DailyFlightTableCf(void *popInstance, int ipDDXType,
    void *vpDataPointer, CString &ropInstanceName);

/////////////////////////////////////////////////////////////////////////////
// DailyTableViewer
//

DailyTableViewer::DailyTableViewer()
{

	SetModul("SEASON");

	bmIsFromSearch = false;
    pomTable = NULL;
	ogDdx.Register(this, DATA_RELOAD,CString("DailyTableViewer"), CString("Flight changed"),DailyFlightTableCf);
    ogDdx.Register(this, DS_FLIGHT_UPDATE, CString("DailyTableViewer"), CString("Flight Update/new"), DailyFlightTableCf);
    ogDdx.Register(this, DS_FLIGHT_CHANGE, CString("DailyTableViewer"), CString("Flight Update/new"), DailyFlightTableCf);
    ogDdx.Register(this, DS_FLIGHT_DELETE, CString("DailyTableViewer"), CString("Flight Update/new"), DailyFlightTableCf);
    ogDdx.Register(this, DS_FLIGHT_INSERT, CString("DailyTableViewer"), CString("Flight Update/new"), DailyFlightTableCf);

	bmCarriageReturn = false;		// kein horizontaler Tabellenunbruch als Default
	imColumnNoCarriageReturn = 0;	// Spaltennummer bei der ein Umbruch stattfinden mu�
	pomPrint = NULL;

}


DailyTableViewer::~DailyTableViewer()
{

	omSort.RemoveAll();
	omPrintHeadHeaderArray.RemoveAll();
	omAnzChar.RemoveAll();
	omShownColumns.RemoveAll();
	omShownColumnsSize.RemoveAll();
	ogDdx.UnRegister(this, NOTUSED);
	omPrintHeadHeaderArray.DeleteAll(); 
	delete pomPrint;
    DeleteAll();

}


void DailyTableViewer::Attach(CCSTable *popTable)
{

    pomTable = popTable;

}


void DailyTableViewer::SetParentDlg(CDialog* ppParentDlg)
{

	pomParentDlg = ppParentDlg;

}


void DailyTableViewer::ChangeViewTo(const char *pcpViewName)
{

	CCS_TRY

	if(strcmp(pcpViewName, "") != 0)
		SelectView(pcpViewName);
	
	CString olWhere;
	CTime olFrom;
	CTime olTo;
	CString olFtyps;

	bool blRotation;

	GetFlightWhereString(olWhere, blRotation);
	
	SetTableSort();
	GetTimeSpanAndFtyp(olFrom, olTo, olFtyps);
	ogDailyCedaFlightData.SetPreSelection(olFrom, olTo, olFtyps);

	// for testing: hard coded examples
	//olWhere = CString(" TIFA >= '1999032000000' AND TIFD <= '19990308235959'");
	//olWhere = CString(" FLNO >= 'YP 7100' AND FLNO <= 'YP 7500' AND TIFA >= '1999030801000' AND TIFD <= '19990308235959'");
	//olWhere = CString(" (BETWEEN TIFA >= '1999022800000' AND TIFA <= '19990228235959' OR (BETWEEN TIFD >= '1999022800000' AND TIFD <= '19990228235959')");
	//olWhere = CString(" TIFA BETWEEN '1999022800000' AND '19990228235959' OR TIFD BETWEEN '1999022800000' AND '19990228235959'");
	
	ogDailyCedaFlightData.ReadAllFlights(olWhere, blRotation);

    pomTable->ResetContent();
    DeleteAll();    
    MakeLines(&ogDailyCedaFlightData.omData);

	UpdateDisplay();

	CCS_CATCH_ALL

}


void DailyTableViewer::SetTableSort()
{
	CCS_TRY

	CStringArray olSpezSort;
	CStringArray olUniSort;

	omSort.RemoveAll();

	GetSort("FLIGHTSORT", olSpezSort);
	GetSort("UNISORT", olUniSort);


	//////////////////////////////////////////////////////
	// Sortarray  zusammenbasteln

	CString olTmp("|");

	for(int i = 0; i < olSpezSort.GetSize(); i ++)
	{

		if(olSpezSort[i] == "Flugnummer")
		{
			olTmp = olTmp + CString("AFLNO") + CString("|") + CString("DFLNO") + CString("|");		
			omSort.Add("AFLNO+");
			omSort.Add("DFLNO+");
		}
		if(olSpezSort[i] == "A/C-Type")
		{
			olTmp = olTmp + CString("ACT3") + CString("|");
			omSort.Add("ACT3+");
		}
		if(olSpezSort[i] == "Origin")
		{
			olTmp = olTmp + CString("AORG3") + CString("|");
			omSort.Add("AORG3+");
		}
		if(olSpezSort[i] == "Destination")
		{
			olTmp = olTmp + CString("DDES3") + CString("|");
			omSort.Add("DDES3+");
		}
		if(olSpezSort[i] == "Ankunftzeit")
		{
			olTmp = olTmp + CString("ASTOA") + CString("|");
			omSort.Add("ASTOA+");
		}
		if(olSpezSort[i] == "Abflugzeit")
		{
			olTmp = olTmp + CString("DSTOD") + CString("|");
			omSort.Add("DSTOD+");
		}
	}
	

	for( i = 0; i < olUniSort.GetSize(); i ++)
	{
		if(olTmp.Find(olUniSort[i].Left(5)) < 0)
		{
			omSort.Add(olUniSort[i]);
		}
	}

	CCS_CATCH_ALL

}



bool DailyTableViewer::IsPassFilter(DAILYFLIGHTDATA *prpDailyFlight)
{
	CCS_TRY

		if(prpDailyFlight == NULL)
		return false;
	
	bool blRet = true;


    return (blRet);

	CCS_CATCH_ALL

}



/////////////////////////////////////////////////////////////////////////////
// DailyTableViewer -- code specific to this class

void DailyTableViewer::MakeLines(CCSPtrArray<DAILYFLIGHTDATA> *popFlights, bool bpInsert)
{

	CCS_TRY

	DAILYFLIGHTDATA *prlAFlight;
	DAILYFLIGHTDATA *prlDFlight;
	DAILYFLIGHTDATA *prlNextFlight = NULL;
	DAILYFLIGHTDATA *prlFlight;
	int ilLineNo;

	bool blRDeparture = false;

	int ilFlightCount = popFlights->GetSize();


	for (int ilLc = 0; ilLc < ilFlightCount; ilLc++)
	{
		prlFlight = &(*popFlights)[ilLc];
		//TRACE("\n %s %ld %s", prlFlight->Flno, prlFlight->Rkey, prlFlight->Adid);
	}

	for ( ilLc = 0; ilLc < ilFlightCount; ilLc++)
	{
		prlFlight = &(*popFlights)[ilLc];

		if(ilLc + 1 < ilFlightCount)
			prlNextFlight = &(*popFlights)[ilLc + 1];	
		else
			prlNextFlight = NULL;	

		if(!((strcmp(prlFlight->Org3, pcgHome) != 0) && (strcmp(prlFlight->Des3, pcgHome) != 0)))
		{

			//Arrival
			if((strcmp(prlFlight->Org3, pcgHome) != 0) && (strcmp(prlFlight->Des3, pcgHome) == 0))
			{
				prlAFlight = prlFlight;
				prlDFlight = NULL;
				if(prlNextFlight != NULL)
				{
					if((prlNextFlight->Rkey == prlFlight->Rkey) && (prlFlight->Rkey != NULL))
					{
						prlDFlight = prlNextFlight;

						if((strcmp(prlNextFlight->Org3, pcgHome) == 0) && (strcmp(prlNextFlight->Des3, pcgHome) != 0))
						{
							ilLc++;
						}
						else
							blRDeparture = true;
					}
				}
			}
			else
			{
				// Departure
				if((strcmp(prlFlight->Org3, pcgHome) == 0) && (strcmp(prlFlight->Des3, pcgHome) != 0))
				{
					prlAFlight = NULL;
					prlDFlight = prlFlight;
					blRDeparture = false;
				}
				else
				{
					//Turnaround
					if((strcmp(prlFlight->Org3, pcgHome) == 0) && (strcmp(prlFlight->Des3, pcgHome) == 0))
					{
						if(blRDeparture)
						{
							blRDeparture = false;
							prlAFlight = prlFlight;
							prlDFlight = NULL;
							if(prlNextFlight != NULL)
							{
								if((prlNextFlight->Rkey == prlFlight->Rkey) && (prlFlight->Rkey != NULL))
								{
									prlDFlight = prlNextFlight;
									if((strcmp(prlNextFlight->Org3, pcgHome) == 0) && (strcmp(prlNextFlight->Des3, pcgHome) != 0))
									{
										ilLc++;
									}
									else
										blRDeparture = true;
								}
							}
						}
						else
						{
							/*prlAFlight		= prlFlight;
							prlDFlight		= prlFlight;
							if(prlNextFlight != NULL)
							{
								if((prlNextFlight->Rkey == prlFlight->Rkey) && (prlFlight->Rkey != NULL))
								{
									ilLc--;
									prlAFlight	 = NULL;
									blRDeparture = true;
								}
							}*/
							prlAFlight	 = NULL;
							prlDFlight	 = prlFlight;
							blRDeparture = true;
							ilLc--;
						}
					}
				}
			}
			ilLineNo = MakeLine(prlAFlight, prlDFlight);

			if(bpInsert)
				InsertDisplayLine(ilLineNo);
		}
	}

	//calculate the numbers of ARR and DEP
	int ilLines = omLines.GetSize();  
	int ilArr = 0;
	int ilDep = 0;
	int ilDepArr = 0;
	for(int j = 0; j < ilLines; j++ ) 
	{
		DAILYSCHEDULE_LINEDATA rlLine = omLines[j];
		if (!rlLine.AFlno.IsEmpty())
		{
			ilArr++;
			ilDepArr++;
		}
		if (!rlLine.DFlno.IsEmpty())
		{
			ilDep++;
			ilDepArr++;
		}
	} 

	CString olTimes;
	if (bgDailyLocal)
		olTimes = GetString(IDS_STRING1921);
	else
		olTimes = GetString(IDS_STRING1920);

	CString olTableName = GetString(IDS_STRING1775);//"Daily rotations";
	char pclHeader[256];
	sprintf(pclHeader, GetString(IDS_R_FLIGHT), olTableName, ilDepArr, ilArr, ilDep, olTimes);
	omTableName = pclHeader;
	CCS_CATCH_ALL

}

		




int DailyTableViewer::MakeLine(DAILYFLIGHTDATA *prpAFlight, DAILYFLIGHTDATA *prpDFlight)
{

	CCS_TRY

    DAILYSCHEDULE_LINEDATA rlLine;
	bool blRetA = IsPassFilter(prpAFlight); 
	bool blRetD = IsPassFilter(prpDFlight); 

	if(blRetA && blRetD )
	{
		MakeLineData(prpAFlight, prpDFlight, rlLine);
		return CreateLine(rlLine);
	}
	if(blRetA)
	{
		MakeLineData(prpAFlight, NULL, rlLine);
		return CreateLine(rlLine);
	}
	if(blRetD)
	{
		MakeLineData(NULL, prpDFlight, rlLine);
		return CreateLine(rlLine);
	}

	CCS_CATCH_ALL


	return -1;
}




void DailyTableViewer::MakeLineData(DAILYFLIGHTDATA *prpAFlight, DAILYFLIGHTDATA *prpDFlight, DAILYSCHEDULE_LINEDATA &rpLine)
{

	CCS_TRY

	rpLine.Act3 = ""; 
	rpLine.Act5 = ""; 
	rpLine.Regn = ""; 

	CString olStr;
	if(prpAFlight != NULL)
	{
 		rpLine.AUrno = prpAFlight->Urno;
		rpLine.ARkey = prpAFlight->Rkey;
		rpLine.ATifa = prpAFlight->Tifa; 
		rpLine.AStoa = prpAFlight->Stoa; 


 		rpLine.AFlno = prpAFlight->Flno;
		rpLine.ATtyp = prpAFlight->Ttyp; 
		rpLine.AOrg3 = prpAFlight->Org3;
		rpLine.ADes3 = prpAFlight->Des3;
		rpLine.APsta = prpAFlight->Psta;
		rpLine.AGta1 = prpAFlight->Gta1; 
		rpLine.AGta2 = prpAFlight->Gta2;
		rpLine.AFtyp = prpAFlight->Ftyp;
		rpLine.ABlt1 = prpAFlight->Blt1;

		if(!CString(prpAFlight->Blt2).IsEmpty())
		{
			rpLine.ABlt1 += CString("/") + CString(prpAFlight->Blt2);

		}


		olStr.TrimRight();
		rpLine.AVial =   olStr + CString(" ") + CString(prpAFlight->Vian);

		rpLine.Act5 = CString(prpAFlight->Act5);
		rpLine.Act3 = CString(prpAFlight->Act3);
		rpLine.Regn = CString(prpAFlight->Regn);
		
		rpLine.AAirb = CTime(prpAFlight->Airb);
		rpLine.AAiru = CTime(prpAFlight->Airu);
		rpLine.ACsgn = CString(prpAFlight->Csgn);
		rpLine.ASlot = CTime(prpAFlight->Slot);
		rpLine.ADes4 = CString(prpAFlight->Des4);
		rpLine.AEtdc = CTime(prpAFlight->Etdc);
		rpLine.AEtai = CTime(prpAFlight->Etai);
		rpLine.AGta1 = CString(prpAFlight->Gta1);
		rpLine.AGta2 = CString(prpAFlight->Gta2);
		rpLine.AIfra = CString(prpAFlight->Ifra);
		rpLine.AIsre = CString(prpAFlight->Isre);
		rpLine.ALand = CTime(prpAFlight->Land);
		rpLine.ALstu = CTime(prpAFlight->Lstu);
		rpLine.AOnbe = CTime(prpAFlight->Onbe);
		rpLine.AOnbl = CTime(prpAFlight->Onbl);
		rpLine.AOrg4 = CString(prpAFlight->Org4);
		rpLine.ARemp = CString(prpAFlight->Remp);
		rpLine.ARwya = CString(prpAFlight->Rwya);
		rpLine.ATmoa = CTime(prpAFlight->Tmoa);
		rpLine.AUseu = CString(prpAFlight->Useu);
		rpLine.AVia4 = CString(prpAFlight->Via4);
		rpLine.AVia3 = CString(prpAFlight->Via3);
		rpLine.AVian = CString(prpAFlight->Vian);
		rpLine.ATtyp = CString(prpAFlight->Ttyp);
		rpLine.AChgi = CString(prpAFlight->Chgi);
		rpLine.ARtyp = CString(prpAFlight->Rtyp);
		rpLine.AAdid = CString(prpAFlight->Adid);
		rpLine.ANxti = CTime(prpAFlight->Nxti);
		rpLine.AAdid = CString(prpAFlight->Adid);
		rpLine.APaba = CTime(prpAFlight->Paba);
		rpLine.APaea = CTime(prpAFlight->Paea);
		rpLine.APdba = CTime(prpAFlight->Pdba);
		rpLine.APdea = CTime(prpAFlight->Pdea);
		rpLine.AMing = CString(prpAFlight->Ming);
		rpLine.AVial = CString(prpAFlight->Vial);
		rpLine.ACkif = CString(prpAFlight->Ckif);
		rpLine.ACkit = CString(prpAFlight->Ckit);
		rpLine.ABlt2 = CString(prpAFlight->Blt2);
		rpLine.AVip	 = prpAFlight->Vip;
		rpLine.AFlti = CString(prpAFlight->Flti[0]);
	}
	else
	{

		rpLine.AUrno =  0;
		rpLine.ARkey =  0;
		rpLine.ATifa = TIMENULL; 
		rpLine.AStoa = TIMENULL; 
		rpLine.AFlno = "";
		rpLine.ATtyp = ""; 
		rpLine.AOrg3 = "";
		rpLine.ADes3 = "";
		rpLine.APsta = "";
		rpLine.AGta1 = ""; 
		rpLine.AGta2 = "";
		rpLine.AVial = "";
		rpLine.AFtyp = "";
		rpLine.ABlt1 = "";
		rpLine.AAirb = TIMENULL;
		rpLine.AAiru = TIMENULL;
		rpLine.ACsgn = "";
		rpLine.ASlot = 0;
		rpLine.ADes4 = "";
		rpLine.AEtdc = TIMENULL;
		rpLine.AEtai = TIMENULL;
		rpLine.AGta1 = "";
		rpLine.AGta2 = "";
		rpLine.AIfra = "";
		rpLine.AIsre = "";
		rpLine.ALand = TIMENULL;
		rpLine.ALstu = TIMENULL;
		rpLine.AOnbe = TIMENULL;
		rpLine.AOnbl = TIMENULL;
		rpLine.AOrg4 = "";
		rpLine.ARemp = "";
		rpLine.ARwya = "";
		rpLine.ATmoa = TIMENULL;
		rpLine.AUseu = "";
		rpLine.AVia4 = "";
		rpLine.AVia3 = "";
		rpLine.AVian = "";
		rpLine.AChgi = "";
		rpLine.ARtyp = "";
		rpLine.AAdid = "";
		rpLine.ANxti = TIMENULL;
		rpLine.AAdid = "";
		rpLine.APaba = TIMENULL;
		rpLine.APaea = TIMENULL;
		rpLine.APdba = TIMENULL;
		rpLine.APdea = TIMENULL;
		rpLine.AMing = "";
		rpLine.AVial = "";
		rpLine.ACkif = "";
		rpLine.ACkit = "";
		rpLine.ABlt2 = "";
		rpLine.AVip  = false;
		rpLine.AFlti = "";
	}


	if(prpDFlight != NULL)
	{

		rpLine.DUrno = prpDFlight->Urno;
		rpLine.DRkey = prpDFlight->Rkey;
		rpLine.DTifd = prpDFlight->Tifd; 
		rpLine.DStod = prpDFlight->Stod;
		rpLine.DFlno = CString(prpDFlight->Flno); 
		rpLine.DTtyp = CString(prpDFlight->Ttyp);
		rpLine.DDes3 = CString(prpDFlight->Des3);
		rpLine.DOrg3 = CString(prpDFlight->Org3);
		rpLine.DPstd = CString(prpDFlight->Pstd);
		rpLine.DGtd1 = CString(prpDFlight->Gtd1);
		rpLine.DGtd2 = CString(prpDFlight->Gtd2);
		rpLine.DFtyp = CString(prpDFlight->Ftyp);

		if(rpLine.Act5.IsEmpty())
			rpLine.Act5 = CString(prpDFlight->Act5);

		if(rpLine.Act3.IsEmpty())
			rpLine.Act3 = CString(prpDFlight->Act3);

		if(rpLine.Regn.IsEmpty())
			rpLine.Regn = CString(prpDFlight->Regn);


		olStr.TrimRight();
		if(olStr.GetLength() != 3) olStr = "   ";
		rpLine.DVial =   olStr + CString(" ") + CString(prpDFlight->Vian);


		rpLine.DAirb = prpDFlight->Airb;		
		rpLine.DAiru = prpDFlight->Airu;		
		rpLine.DCsgn = CString(prpDFlight->Csgn);
		rpLine.DSlot = prpDFlight->Slot;		
		rpLine.DDes3 = CString(prpDFlight->Des3);
		rpLine.DDes4 = CString(prpDFlight->Des4);
		rpLine.DEtdc = prpDFlight->Etdc;		
		rpLine.DEtdi = prpDFlight->Etdi;		
		rpLine.DFlno = CString(prpDFlight->Flno);
		rpLine.DFtyp = CString(prpDFlight->Ftyp);
		rpLine.DIfrd = CString(prpDFlight->Ifrd);
		rpLine.DIsre = CString(prpDFlight->Isre);
		rpLine.DLand = prpDFlight->Land;		
		rpLine.DLstu = prpDFlight->Lstu;		
		rpLine.DOfbl = prpDFlight->Ofbl;		
		rpLine.DOrg4 = CString(prpDFlight->Org4);
		rpLine.DPstd = CString(prpDFlight->Pstd);
		rpLine.DRemp = CString(prpDFlight->Remp);
		rpLine.DRwyd = CString(prpDFlight->Rwyd);
		rpLine.DStab = CString(prpDFlight->Stab);
		rpLine.DUseu = CString(prpDFlight->Useu);
		rpLine.DVia4 = CString(prpDFlight->Via4);
		rpLine.DVia3 = CString(prpDFlight->Via3);
		rpLine.DVian = CString(prpDFlight->Vian);
		rpLine.DTtyp = CString(prpDFlight->Ttyp);
		rpLine.DChgi = CString(prpDFlight->Chgi);
		rpLine.DRtyp = CString(prpDFlight->Rtyp);
		rpLine.DAdid = CString(prpDFlight->Adid);
		rpLine.DNxti = prpDFlight->Nxti;		
		rpLine.DBlt1 = CString(prpDFlight->Blt1);
		rpLine.DPaba = prpDFlight->Paba;		
		rpLine.DPaea = prpDFlight->Paea;			
		rpLine.DPdba = prpDFlight->Pdba;		
		rpLine.DPdea = prpDFlight->Pdea;		
		rpLine.DMing = CString(prpDFlight->Ming);
		rpLine.DDAder = CString(prpDFlight->Ader);
		rpLine.DBlt2 = CString(prpDFlight->Blt2);
		rpLine.DVip	 = prpDFlight->Vip;	


		CString opMin;
		CString opMax;

//beg PRF 1976
		opMin = prpDFlight->Ckif;
		opMax = prpDFlight->Ckit;

//		if flight has information use this one, else look up cca-data
		if( opMax.IsEmpty() && opMin.IsEmpty() )
			ogDailyCedaFlightData.omCcaData.GetMinMaxCkic(prpDFlight->Urno, opMin, opMax);


//		ogDailyCedaFlightData.omCcaData.GetMinMaxCkic(prpDFlight->Urno, opMin, opMax);
//end PRF 1976

		rpLine.DCkif = opMin;
		rpLine.DCkit = opMax;

		rpLine.DFlti = CString(prpDFlight->Flti[0]);
	}
	else
	{
		rpLine.DUrno =  0;
		rpLine.DRkey =  0;
		rpLine.DTifd = TIMENULL; 
		rpLine.DStod = TIMENULL; 
		rpLine.DFlno = "";
		rpLine.DTtyp = ""; 
		rpLine.DDes3 = "";
		rpLine.DOrg3 = "";
		rpLine.DPstd = "";
		rpLine.DGtd1 = ""; 
		rpLine.DGtd2 = "";
		rpLine.DVial = "";
		rpLine.DFtyp = "";

		rpLine.DAirb = TIMENULL; 	
		rpLine.DAiru = TIMENULL;
		rpLine.DCsgn = "";
		rpLine.DSlot = TIMENULL; 		
		rpLine.DDes3 = "";
		rpLine.DDes4 = "";
		rpLine.DEtdc = TIMENULL; 	
		rpLine.DEtdi = TIMENULL; 	
		rpLine.DFlno = "";
		rpLine.DFtyp = ""; 
		rpLine.DIfra = ""; 
		rpLine.DIfrd = ""; 
		rpLine.DIsre = "";
		rpLine.DLand = TIMENULL; 	
		rpLine.DLstu = TIMENULL; 	
		rpLine.DOfbl = TIMENULL; 	
		rpLine.DOrg4 = "";
		rpLine.DPstd = "";
		rpLine.DRemp = "";
		rpLine.DRwyd = "";
		rpLine.DStab = "";
		rpLine.DUseu = "";
		rpLine.DVia4 = "";
		rpLine.DVian = "";
		rpLine.DChgi = ""; 	
		rpLine.DRtyp = "";
		rpLine.DAdid = "";
		rpLine.DNxti = TIMENULL;
		rpLine.DBlt1 = "";	
		rpLine.DPaba = TIMENULL;
		rpLine.DPaea = TIMENULL;
		rpLine.DPdba = TIMENULL;
		rpLine.DPdea = TIMENULL;
		rpLine.DMing = ""; 	
		rpLine.DDAder = ""; 	
		rpLine.DCkif = "";
		rpLine.DCkit = "";
		rpLine.DBlt2 = "";
		rpLine.DVip  = false;
		rpLine.DFlti = "";
		rpLine.DVial = "";
	}

	if (bgDailyLocal)
	{
		UtcToLocal(rpLine);
	}

	CCS_CATCH_ALL

    return;
}



int DailyTableViewer::CreateLine(DAILYSCHEDULE_LINEDATA &rpLine)
{

	int ilLineCount = omLines.GetSize();

    for (int ilLineno = ilLineCount; ilLineno > 0; ilLineno--)
        if (CompareFlight(&rpLine, &omLines[ilLineno-1]) >= 0)
            break;  // should be inserted after Lines[ilLineno-1]

    omLines.NewAt(ilLineno, rpLine);

	return ilLineno;
	
}


void DailyTableViewer::DeleteLine(int ipLineno)
{

	CCS_TRY

	omLines.DeleteAt(ipLineno);

	CCS_CATCH_ALL

}



int DailyTableViewer::FindLine(long lpUrno, int &rilLineno1, int &rilLineno2)
{

	int ilTreffer = 0;
	rilLineno1 = -1;
	rilLineno2 = -1;

    for (int i = omLines.GetSize() - 1; i >= 0; i--)
	{
	  if ((omLines[i].AUrno == lpUrno) || (omLines[i].DUrno == lpUrno))
	  {

        ilTreffer++;
		if(ilTreffer == 1)
			rilLineno1 = i;
		if(ilTreffer == 2)
		{
			rilLineno2 = i;
			break;
		}
	  }
	}


	return ilTreffer;

}



bool DailyTableViewer::FindLine(long lpUrno, int &rilLineno)
{

	for ( rilLineno = omLines.GetSize() - 1; rilLineno >= 0; rilLineno--)
	{
      if ((omLines[rilLineno].AUrno == lpUrno) || (omLines[rilLineno].DUrno == lpUrno))
		return true;
	}


	return false;

}



/////////////////////////////////////////////////////////////////////////////
// DailyTableViewer - DAILYSCHEDULE_LINEDATA array maintenance

void DailyTableViewer::DeleteAll()
{
	CCS_TRY

    while (omLines.GetSize() > 0)
        DeleteLine(0);

	CCS_CATCH_ALL

}


/////////////////////////////////////////////////////////////////////////////
// DailyTableViewer - display drawing routine



// UpdateDisplay: Load data selected by filter conditions to the display by using "omTable"
void DailyTableViewer::UpdateDisplay()
{

	CCS_TRY

	pomTable->ResetContent();


	DAILYSCHEDULE_LINEDATA *prlLine;

	// Anzahl an Zeichen zur Berechnung der Spaltenbreite ermitteln
	int ilColumns = omShownColumns.GetSize();
	omShownColumnsSize.RemoveAll();
	for (int i = 0; i < ilColumns; i++)
	{
		// Aufruf, damit omAnzCount Array gef�llt ist
		SetFieldLength(omShownColumns[i]);
	}
	DrawHeader();
  
	for (int ilLc = 0; ilLc < omLines.GetSize(); ilLc++)
	{
		int ilColumnNo = 0;
		prlLine = &omLines[ilLc];
		CCSPtrArray<TABLE_COLUMN> olColList;
	
		MakeColList(prlLine, olColList);

		pomTable->AddTextLine(olColList, (void*)(prlLine));
		olColList.DeleteAll();
	}

    pomTable->DisplayTable();

	CCS_CATCH_ALL

}



void DailyTableViewer::InsertDisplayLine( int ipLineNo)
{

	CCS_TRY

	if(!((ipLineNo >= 0) && (ipLineNo < omLines.GetSize())))
		return;
	CCSPtrArray<TABLE_COLUMN> olColList;
	MakeColList(&omLines[ipLineNo], olColList);
	pomTable->InsertTextLine(ipLineNo, olColList, &omLines[ipLineNo]);
	olColList.DeleteAll();
    //pomTable->DisplayTable();

	CCS_CATCH_ALL

}



void DailyTableViewer::DrawHeader()
{

	CCS_TRY

	CCSPtrArray <TABLE_HEADER_COLUMN> omHeaderDataArray;

	pomTable->SetShowSelection(true);
	pomTable->ResetContent();

	TABLE_HEADER_COLUMN rlHeader;
	rlHeader.Font = &ogCourier_Bold_10;
	int i;

	// alle n Spalten (=omShownColumns.GetSize()) definieren
	int ilColums = omShownColumns.GetSize();
	int ilAnzChar = omAnzChar.GetSize();
	for (i = 0; i < ilColums; i++)
	{
		rlHeader.Alignment = COLALIGN_CENTER;
		// Anzahl an Textzeichen des Feldes der i-ten Spalte
		if (i <= ilAnzChar)
			rlHeader.AnzChar = omAnzChar[i];
		else
			break;
		// alten String l�schen, damit neuer String bei '0' anfaengt
		rlHeader.String = "";
		// Anzahl an Buchstaben 'W' setzen
		int ilAnzChar = omAnzChar[i];
		for (int j = 0; j < ilAnzChar; j++) 
			rlHeader.String += "W";
		// Text aus Konfiguration als Ueberschrift setzen
		rlHeader.Text = GetHeaderContent(omShownColumns[i]);;
		// neue Spaltenheaderstruktur setzen
		omHeaderDataArray.New(rlHeader);
	}

	pomTable->SetHeaderFields(omHeaderDataArray);
	omHeaderDataArray.DeleteAll();

	pomTable->SetDefaultSeparator();

	CCS_CATCH_ALL

}



void DailyTableViewer::MakeColList(DAILYSCHEDULE_LINEDATA *prlLine, CCSPtrArray<TABLE_COLUMN> &olColList)
{
	
	CCS_TRY

	COLORREF olBkColorArr = WHITE;
	COLORREF olBkColorDep = WHITE;

	TABLE_COLUMN rlColumnData;

	COLORREF olLineColor = COLOR_OPERATION;

	CCSEDIT_ATTRIB rlAttrib;
	rlAttrib.Style = ES_UPPERCASE | ES_WANTRETURN;

	// Backgroundcolor Arrival
	if (prlLine->ATmoa != TIMENULL)
		olBkColorArr = COLOR_TMOA;

	if (prlLine->ALand != TIMENULL)
		olBkColorArr = COLOR_LAND;

	if (prlLine->AOnbl != TIMENULL)
		olBkColorArr = COLOR_ONBL;


	// Backgroundcolor Departure
	if (prlLine->DAirb != TIMENULL)
		olBkColorDep = COLOR_AIRB;

	if (prlLine->DOfbl != TIMENULL)
		olBkColorDep = COLOR_OFBL;
	
	
	if(prlLine->AFtyp == "S")
		olLineColor = COLOR_SCHEDULED;


	rlColumnData.TextColor = olLineColor;

	rlColumnData.VerticalSeparator = SEPA_NONE;
	rlColumnData.SeparatorType = SEPA_NONE;
	rlColumnData.Font = &ogCourier_Regular_9;



	// Spaltendefinitionen bestimmen
	for (int i = 0; i < omShownColumns.GetSize(); i++)
	{
		rlColumnData.Alignment = COLALIGN_LEFT;
		// Holen des Feldinhaltes der i-ten Spalte und n-ten Zeile
		rlColumnData.Text = GetFieldContent(prlLine, omShownColumns[i]);
		// Setzen der Attribute beim 
		rlColumnData.EditAttrib = rlAttrib;
		// Setzen der Hintergrundfarbe der aktuellen Spalte fuer Ankunft
		if (omShownColumns[i].Left(1) == "A")
			rlColumnData.BkColor = olBkColorArr;
		
		// Setzen der Hintergrundfarbe der aktuellen Spalte fuer Abflug
		if (omShownColumns[i].Left(1) == "D")
			rlColumnData.BkColor = olBkColorDep;


		if(omShownColumns[i] == "AFLNO" || omShownColumns[i] == "DFLNO")
		{

			if (omShownColumns[i].Left(1) == "A")
			{
				if(prlLine->AFtyp == "X" || prlLine->AFtyp == "D" || prlLine->AFtyp == "B" || prlLine->AFtyp == "Z") 
					rlColumnData.TextColor = COLOR_CXX;

				if(prlLine->AFtyp == "N") 
					rlColumnData.TextColor = COLOR_NOOP;

				if(prlLine->AFtyp == "S") 
					rlColumnData.TextColor = COLOR_SCHEDULED;

				if(prlLine->AFtyp == "") 
					rlColumnData.TextColor = COLOR_PROGNOSE;


			}
			else
			{
				if(prlLine->DFtyp == "X" || prlLine->DFtyp == "D" || prlLine->DFtyp == "B" || prlLine->DFtyp == "Z") 
					rlColumnData.TextColor = COLOR_CXX;

				if(prlLine->DFtyp == "N") 
					rlColumnData.TextColor = COLOR_NOOP;


				if(prlLine->DFtyp == "S") 
					rlColumnData.TextColor = COLOR_SCHEDULED;

				if(prlLine->DFtyp == "") 
					rlColumnData.TextColor = COLOR_PROGNOSE;


			}
		}
		else
		{
			rlColumnData.TextColor = BLACK;
		}


		// InlineEdit erm�glichen, falls kein postflight
		if (CheckPostFlight(prlLine))
			rlColumnData.blIsEditable = false;
		else
			rlColumnData.blIsEditable = true;


		// Ausschalten bestimmter Spalten des InlineEdit
		if (omShownColumns[i] == "DVN")
			rlColumnData.blIsEditable = false;
		if (omShownColumns[i] == "AVIA4")
			rlColumnData.blIsEditable = false;
		if (omShownColumns[i] == "AVN")
			rlColumnData.blIsEditable = false;
		if (omShownColumns[i] == "ABLT1/ABLT2")
			rlColumnData.blIsEditable = false;
		if (omShownColumns[i] == "AREMP/DREMP")
			rlColumnData.blIsEditable = false;
		//flugnummern
		if (omShownColumns[i] == "AFLNO" || omShownColumns[i] == "DFLNO")
			rlColumnData.blIsEditable = false;
		//datum
		if (omShownColumns[i] == "ASTOADate" || omShownColumns[i] == "DSTODDate")
			rlColumnData.blIsEditable = false;
		//id (international, domestic etc)
		if (omShownColumns[i] == "AFLTI" || omShownColumns[i] == "DFLTI")
			rlColumnData.blIsEditable = false;
		//via ports
		if (omShownColumns[i] == "AVIA3L" || omShownColumns[i] == "DVIA3L")
			rlColumnData.blIsEditable = false;
		//actual times
		if (omShownColumns[i] == "ALAND/AONLB" || omShownColumns[i] == "DOFBL/DAIRB")
			rlColumnData.blIsEditable = false;
		//positions
		if (omShownColumns[i] == "APSTA/DPSTD")
			rlColumnData.blIsEditable = false;
		//check-in
		if (omShownColumns[i] == "DCKIF/DCKIT")
			rlColumnData.blIsEditable = false;
		//gates
		if (omShownColumns[i] == "DGTD1/DGTD2")
			rlColumnData.blIsEditable = false;


		if((prlLine->AFtyp == "Z") || (prlLine->AFtyp == "B") || (prlLine->DFtyp == "Z") || (prlLine->DFtyp == "B"))
		{
			if ((omShownColumns[i] == "ADES4") || (omShownColumns[i] == "AORG4") || 
			    (omShownColumns[i] == "ADES3") || (omShownColumns[i] == "AORG3") || 
			    (omShownColumns[i] == "DDES3") || (omShownColumns[i] == "DORG3") || 
			    (omShownColumns[i] == "DDES4") || (omShownColumns[i] == "DORG4"))
				rlColumnData.blIsEditable = false;


			if ((omShownColumns[i] == "AVIA4") || (omShownColumns[i] == "DVIA4") || 
			    (omShownColumns[i] == "DVN") || (omShownColumns[i] == "AVN")) 
			{
				rlColumnData.blIsEditable = false;
				rlColumnData.Text = "";
			}

			if ((omShownColumns[i] == "AVIA3L") || (omShownColumns[i] == "DVIA3L"))
			{
				rlColumnData.blIsEditable = false;
				rlColumnData.Text = "";
			}


		}


		if((prlLine->AFtyp == "Z") || (prlLine->DFtyp == "Z") || (prlLine->AFtyp == "B") || (prlLine->DFtyp == "B"))
		{
			if ((omShownColumns[i] == "AAIRB") || (omShownColumns[i] == "DAIRB") || 
			    (omShownColumns[i] == "AOFBL") || (omShownColumns[i] == "DOFBL")) 
			{
				rlColumnData.blIsEditable = false;
			}

			if (omShownColumns[i] == "ALAND/AONLB" || omShownColumns[i] == "DOFBL/DAIRB")
			{
				rlColumnData.blIsEditable = false;
			}
		}



		if((prlLine->AFtyp == "B") || (prlLine->DFtyp == "B") )
		{
			if((omShownColumns[i] == "AAIRB") || (omShownColumns[i] == "DAIRB") || (omShownColumns[i] == "ALAND") || (omShownColumns[i] == "DLAND")) 
			{
				rlColumnData.blIsEditable = false;
				rlColumnData.Text = "";
			}

			if (omShownColumns[i] == "ALAND/AONLB" || omShownColumns[i] == "DOFBL/DAIRB")
			{
				rlColumnData.blIsEditable = false;
				rlColumnData.Text = "";
			}
		}




		olColList.NewAt(olColList.GetSize(), rlColumnData);
	}

	CCS_CATCH_ALL

}


CString DailyTableViewer::GetColumnByIndex(int ipIndex)
{

	// g�ltiger Index?
	if (ipIndex >= 0 && ipIndex < omShownColumns.GetSize())
		return omShownColumns[ipIndex];
	else
		return CString("");

}


static void DailyFlightTableCf(void *popInstance, int ipDDXType,
    void *vpDataPointer, CString &ropInstanceName)
{

	CCS_TRY

    DailyTableViewer *polViewer = (DailyTableViewer *)popInstance;

    if (ipDDXType == DS_FLIGHT_UPDATE)
        polViewer->ProcessFlightUpdate();
    if (ipDDXType == DS_FLIGHT_CHANGE)
        polViewer->ProcessFlightChange((DAILYFLIGHTDATA *)vpDataPointer);
	if (ipDDXType == DS_FLIGHT_DELETE)
        polViewer->ProcessFlightDelete((DAILYSCHEDULERKEYLIST *)vpDataPointer);
	if (ipDDXType == DS_FLIGHT_INSERT)
        polViewer->ProcessFlightInsert((DAILYSCHEDULERKEYLIST  *)vpDataPointer);
	if (ipDDXType == DATA_RELOAD)
        polViewer->ChangeViewTo("");

	CCS_CATCH_ALL

}



void DailyTableViewer::ProcessFlightUpdate()
{

	CCS_TRY

	GetSelectedRow();

    pomTable->ResetContent();
    DeleteAll();
    MakeLines(&ogDailyCedaFlightData.omData);
	UpdateDisplay();

	SetSelectedRow();

	CCS_CATCH_ALL

}
	


void DailyTableViewer::SelectLine(long ilCurrUrno)
{

	CCS_TRY

	if(ilCurrUrno <= 0)
		return;

	for(int i = omLines.GetSize() -1 ; i >= 0; i--)
	{
		if((omLines[i].AUrno == ilCurrUrno) || (omLines[i].DUrno == ilCurrUrno))
		{
			(pomTable->GetCTableListBox())->SetSel(i);
			break;
		}
	}

	CCS_CATCH_ALL

}




bool DailyTableViewer::CreateExcelFile(CString opTrenner)
{
/*
	ofstream of;

	// stream oeffnen mit pfad und dateiname
	of.open(GetString(IDS_STRING1386), ios::out);
*/
	ofstream of;

	char* tmpvar;
	tmpvar = getenv("TMP");
	if (tmpvar == NULL)
		tmpvar = getenv("TEMP");

	if (tmpvar == NULL)
		return false;

	CString olTableName;
	olTableName.Format("%s -   %s", omTableName, (CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M")) );


	CString olFileName = olTableName;
	olFileName.Remove('*');
	olFileName.Remove('.');
	olFileName.Remove(':');
	olFileName.Remove('/');
	olFileName.Replace(" ", "_");
	
	char pHeader[256];
	strcpy (pHeader, tmpvar);
	CString path = pHeader;
	omFileName =  path + "\\" + olFileName + ".csv";

	of.open( omFileName, ios::out);


	of  << setw(1) << omTableName << "     "  << CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M")  << endl;

	of	<< setw(1) << GetString(IDS_STRING296)//Flight
		<< setw(1) << opTrenner
		<< setw(1) << GetString(IDS_STRING332) //CString("Datum");
		<< setw(1) << opTrenner
		<< setw(1) << GetString(IDS_STRING1815) //CString("ID");
		<< setw(1) << opTrenner
		<< setw(1) << GetString(IDS_STRING301)//Na
		<< setw(1) << opTrenner
		<< setw(1) << GetString(IDS_STRING298)//ORG
		<< setw(1) << opTrenner
		<< setw(1) << GetString(IDS_STRING299)//VIA
		<< setw(1) << opTrenner
		<< setw(1) << GetString(IDS_STRING321)//ATD
		<< setw(1) << opTrenner
		<< setw(1) << GetString(IDS_STRING323)//STA
		<< setw(1) << opTrenner
		<< setw(1) << GetString(IDS_STRING302)//ETA
		<< setw(1) << opTrenner
		<< setw(1) << GetString(IDS_STRING303)//TMO
		<< setw(1) << opTrenner
		<< setw(1) << GetString(IDS_DRT_AATA_AOBL)//ATA/ONB
		<< setw(1) << opTrenner

		<< setw(1) << GetString(IDS_DRT_POSAD) //POS A/D
		<< setw(1) << opTrenner
		<< setw(1) << GetString(IDS_STRING311)//A/C
		<< setw(1) << opTrenner
		<< setw(1) << GetString(IDS_STRING310)//REG
		<< setw(1) << opTrenner

		<< setw(1) << GetString(IDS_STRING296)//Flight
		<< setw(1) << opTrenner
		<< setw(1) << GetString(IDS_STRING332) //CString("Datum");
		<< setw(1) << opTrenner
		<< setw(1) << GetString(IDS_STRING1815) //CString("ID");
		<< setw(1) << opTrenner
		<< setw(1) << GetString(IDS_STRING301)//Na
		<< setw(1) << opTrenner
		<< setw(1) << GetString(IDS_STRING299)//VIA
		<< setw(1) << opTrenner
		<< setw(1) << GetString(IDS_STRING315)//DES
		<< setw(1) << opTrenner
		<< setw(1) << GetString(IDS_STRING316)//STD
		<< setw(1) << opTrenner
		<< setw(1) << GetString(IDS_STRING317)//ETD
		<< setw(1) << opTrenner
		<< setw(1) << GetString(IDS_DRT_DOFB_DATD)//OFB/ATD
		<< setw(1) << opTrenner
		<< setw(1) << GetString(IDS_STRING2126)//CTR
		<< setw(1) << opTrenner
		<< setw(1) << GetString(IDS_DRT_GATE) //GATE
		<< setw(1) << opTrenner
		<< setw(1) << GetString(IDS_STRING2123) //State

		<< endl;




	int ilCountLines = omLines.GetSize();
	int ilCountRows  = omShownColumns.GetSize();

	// zeilen iterieren
	for(int ilLines = 0; ilLines < ilCountLines; ilLines++)
	{

		DAILYSCHEDULE_LINEDATA rlLine = omLines[ilLines];

		//zus�tzlich status ermitteln
		CString olState;
		if ( strcmp( rlLine.AFtyp, "" ) == 0 )
			olState = CString(" ");
		else
			olState = rlLine.AFtyp;

		olState += CString("-");

		if ( strcmp( rlLine.DFtyp, "" ) == 0 )
			olState += CString(" ");
		else
			olState += rlLine.DFtyp;

		// Spalten- und Feldinhalt ueber Spaltenaray (omShownColumns) iterieren
		for (int ilCols = 0; ilCols < ilCountRows; ilCols++)
		{
			of.setf(ios::left, ios::adjustfield);

			of << setw(omAnzChar[ilCols]) << GetFieldContent(&rlLine, omShownColumns[ilCols])
		       << setw(1) << opTrenner;
		}

		of << setw(1) << olState;

		// Zeile abschliessen
		of << endl;
	
	}

	// stream schliessen
	of.close();

	return true;

}


void DailyTableViewer::ProcessFlightChange(DAILYFLIGHTDATA *prpFlight)
{

	CCS_TRY

	if(prpFlight == NULL)
		return;

	int ilItem1 = -1;
	int ilItem2 = -1;
	DAILYSCHEDULE_LINEDATA *prlLine;
	
	int ilItems[3000];
	int ilAnz = 3000;

	CListBox *polLB = pomTable->GetCTableListBox();
	if (polLB == NULL)
		return;

	ilAnz = MyGetSelItems(*polLB, ilAnz, ilItems);


	//Turnaround
	if((strcmp(prpFlight->Org3, pcgHome) == 0) && (strcmp(prpFlight->Des3, pcgHome) == 0))
	{
		FindLine(prpFlight->Urno,ilItem1, ilItem2);
	}
	else
	{
		FindLine(prpFlight->Urno, ilItem1);
	}

	if((ilItem1 >= 0) && (ilItem2 >= 0) && (ilItem1 == ilItem2))
	{
		MakeLineData(prpFlight, prpFlight, omLines[ilItem1]);
		CCSPtrArray<TABLE_COLUMN> olColList;
		MakeColList(&omLines[ilItem1], olColList);
		prlLine = &omLines[ilItem1];
		pomTable->ChangeTextLine(ilItem1, &olColList, (void *&)prlLine);
		olColList.DeleteAll();
	}
	else
	{
		if(ilItem1 >= 0)
		{
			prlLine =  &omLines[ilItem1]; 
			if(prpFlight->Urno == prlLine->AUrno)
			{
				MakeLineData(prpFlight, ogDailyCedaFlightData.GetFlightByUrno(prlLine->DUrno), omLines[ilItem1]);
			}
			if(prpFlight->Urno == prlLine->DUrno)
			{
				MakeLineData(ogDailyCedaFlightData.GetFlightByUrno(prlLine->AUrno), prpFlight, omLines[ilItem1]);
			}
			CCSPtrArray<TABLE_COLUMN> olColList;
			MakeColList(&omLines[ilItem1], olColList);
			prlLine = &omLines[ilItem1];
			pomTable->ChangeTextLine(ilItem1, &olColList, (void *&)prlLine);
			olColList.DeleteAll();
		}	
		if(ilItem2 >= 0)
		{
			prlLine =  &omLines[ilItem2]; 
			if(prpFlight->Urno == prlLine->AUrno)
			{
				MakeLineData(prpFlight, ogDailyCedaFlightData.GetFlightByUrno(prlLine->DUrno), omLines[ilItem2]);
			}
			if(prpFlight->Urno == prlLine->DUrno)
			{
				MakeLineData(ogDailyCedaFlightData.GetFlightByUrno(prlLine->AUrno), prpFlight, omLines[ilItem2]);
			}
			CCSPtrArray<TABLE_COLUMN> olColList;
			MakeColList(&omLines[ilItem2], olColList);
			prlLine = &omLines[ilItem2];
			pomTable->ChangeTextLine(ilItem2, &olColList, (void *&)prlLine);
			olColList.DeleteAll();
		}	
	}

	for(int i = ilAnz - 1; i >= 0; i--)
		SelectLine(ilItems[i]);

	return;

	CCS_CATCH_ALL

}


void DailyTableViewer::ProcessFlightDelete(DAILYSCHEDULERKEYLIST *prpRotation)
{

	CCS_TRY

	if(prpRotation == NULL)
		return;
	if(prpRotation->Rotation.GetSize() <= 0)
		return;
	DeleteRotation(prpRotation->Rotation[0].Rkey);

	CCS_CATCH_ALL

}


void DailyTableViewer::DeleteRotation(long lpRkey)
{

	CCS_TRY

    for (int i = omLines.GetSize() - 1; i >= 0; i--)
	{
		if ((omLines[i].ARkey == lpRkey) || (omLines[i].DRkey == lpRkey))
		{
			DeleteLine(i);
			pomTable->DeleteTextLine(i);
		}
	}

	CCS_CATCH_ALL

}



void DailyTableViewer::ProcessFlightInsert(DAILYSCHEDULERKEYLIST *prpRotation)
{

	CCS_TRY

	if(prpRotation == NULL)
		return;
	if(prpRotation->Rotation.GetSize() == 0)
		return;

	bool bmDisplay = false;


	if(omLines.GetSize() == 0)
		bmDisplay = true;


	DeleteRotation(prpRotation->Rotation[0].Rkey);
	MakeLines(&prpRotation->Rotation, true);

	if(bmDisplay)
	    pomTable->DisplayTable();

	CCS_CATCH_ALL

}


int DailyTableViewer::CompareFlight(DAILYSCHEDULE_LINEDATA *prpLine1, DAILYSCHEDULE_LINEDATA *prpLine2)
{

	int	ilCompareResult;
	CTime olTime1;
	CTime olTime2;

	int ilCount = omSort.GetSize();
	if(ilCount == 0)
	{
		if(prpLine1->AStoa == TIMENULL)
			olTime1 = prpLine1->DStod;
		else
			olTime1 = prpLine1->AStoa;

		if(prpLine2->AStoa == TIMENULL)
			olTime2 = prpLine2->DStod;
		else
			olTime2 = prpLine2->AStoa;

		ilCompareResult = (olTime1 == olTime2)? 0:
			(olTime1 > olTime2)? 1: -1;

		 return ilCompareResult;
	}
	else
	{
		for (int i = 0; i < ilCount; i++)
		{

			ilCompareResult = 0;
			if (omSort[i].GetLength() < 6)
			{
				if (strcmp(omSort[i].Right(1), "+") == 0)
					ilCompareResult = (GetFieldContent(prpLine1, omSort[i].Left(5))) == (GetFieldContent(prpLine2, omSort[i].Left(5))) ? 0 : (GetFieldContent(prpLine1, omSort[i].Left(5))) > (GetFieldContent(prpLine2, omSort[i].Left(5))) ? 1 : -1;
				if (strcmp(omSort[i].Right(1), "-") == 0)
					ilCompareResult = (GetFieldContent(prpLine1, omSort[i].Left(5))) == (GetFieldContent(prpLine2, omSort[i].Left(5))) ? 0 : (GetFieldContent(prpLine1, omSort[i].Left(5))) < (GetFieldContent(prpLine2, omSort[i].Left(5))) ? 1 : -1;
			}
			else
			{
				if (strcmp(omSort[i], "ASTOA+") == 0 || strcmp(omSort[i], "ASTOADate+") == 0 || strcmp(omSort[i], "ASTOATime+") == 0)
					ilCompareResult = (prpLine1->AStoa == prpLine2->AStoa)? 0: (prpLine1->AStoa > prpLine2->AStoa)? 1: -1;
				else 
				if (strcmp(omSort[i], "ASTOA-") == 0 || strcmp(omSort[i], "ASTOADate-") == 0 || strcmp(omSort[i], "ASTOATime-") == 0)
					ilCompareResult = (prpLine1->AStoa == prpLine2->AStoa)? 0: (prpLine1->AStoa < prpLine2->AStoa)? 1: -1;
				else 

				if (strcmp(omSort[i], "DSTOD+") == 0 || strcmp(omSort[i], "DSTODDate+") == 0 || strcmp(omSort[i], "DSTODTime+") == 0)
					ilCompareResult = (prpLine1->DStod == prpLine2->DStod)? 0: (prpLine1->DStod > prpLine2->DStod)? 1: -1;
				else 
				if (strcmp(omSort[i], "DSTOD-") == 0 || strcmp(omSort[i], "DSTODDate-") == 0 || strcmp(omSort[i], "DSTODTime-") == 0)
					ilCompareResult = (prpLine1->DStod == prpLine2->DStod)? 0: (prpLine1->DStod < prpLine2->DStod)? 1: -1;
			}

			// Check the result of this sorting order, return if unequality is found
			if (ilCompareResult != 0)
				return ilCompareResult;

		}
		return 0;	// we can say that these two lines are equal
	}

}




//-----------------------------------------------------------------------------------------------
// Printing Routines
//-----------------------------------------------------------------------------------------------


void DailyTableViewer::GetHeader()
{

	CCS_TRY

		
	TABLE_HEADER_COLUMN *prlHeader[50];
	int ilFlnoPlus = 20 ;	// Zusatzplatz f�r Ftyp auf Druck


	int i;
	int ilColomns = omShownColumns.GetSize();
	CSize olSize;
    pomPrint->omCdc.SelectObject(&ogCourier_Bold_8);

	for (i = 0; i < ilColomns; i++)
	{
		olSize = pomPrint->omCdc.GetTextExtent(omShownColumnsSize[i]);
		prlHeader[i] = new TABLE_HEADER_COLUMN;
		prlHeader[i]->Alignment = COLALIGN_CENTER;
		prlHeader[i]->Font = &ogCourier_Bold_8;
		prlHeader[i]->Length = olSize.cx;
		prlHeader[i]->Text = GetHeaderContent(omShownColumns[i]);//omShownColumns[i];
		/*if( ilFlnoPlus > 0 )
			prlHeader[0]->Text += "    "+ CString("T");*/
	}

	omPrintHeadHeaderArray.DeleteAll();
	for(i = 0; i < ilColomns; i++)
	{
		omPrintHeadHeaderArray.Add(prlHeader[i]);
	}


	CCS_CATCH_ALL

}


void DailyTableViewer::PrintTableView()
{

	CCS_TRY

	CString olFooter1,olFooter2;
	CString olTableName = GetString(1256);
	int ilOrientation = PRINT_LANDSCAPE;//  PRINT_PORTRAET  PRINT_LANDSCAPE
	pomPrint = new CCSPrint(pomParentDlg,ilOrientation,45);// FirstLine(P=200,L=200),LeftOffset(P=200,L=50)
	pomPrint->imMaxLines = 38;  // (P=57,L=38)
	//omBitmap.LoadBitmap(IDB_HAJLOGO);
	//pomPrint->SetBitmaps(&omBitmap,&omBitmap);
	if (pomPrint != NULL)
	{

		if (pomPrint->InitializePrinter(ilOrientation) == TRUE)
		{
			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
			pomPrint->imLineNo = pomPrint->imMaxLines + 1;
			DOCINFO	rlDocInfo;
			memset(&rlDocInfo, 0, sizeof(DOCINFO));
			rlDocInfo.cbSize = sizeof( DOCINFO );
			rlDocInfo.lpszDocName = olTableName;	
			//rlDocInfo.lpszDocName = TABLENAME;
			pomPrint->omCdc.StartDoc( &rlDocInfo );
			pomPrint->imPageNo = 0;
			int ilLines = omLines.GetSize();  
			olFooter1.Format("%s %d, %s, %s",GetString(IDS_STRING329),ilLines,olTableName, 
				            (CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M")));
			olFooter1.Format("%s %d",rlDocInfo.lpszDocName,ilLines);

			GetHeader();

			if (ilLines == 0)
				PrintTableHeader(); // Drucken leerer Tabelle
			for(int i = 0; i < ilLines; i++ ) 
			{
				if(pomPrint->imLineNo >= pomPrint->imMaxLines)
				{
					if(pomPrint->imPageNo > 0)
					{
						olFooter2.Format(GetString(IDS_STRING1198),pomPrint->imPageNo);
						pomPrint->PrintUIFFooter(olFooter1,"",olFooter2);
						pomPrint->omCdc.EndPage();
					}
					PrintTableHeader();
				}
				if(pomPrint->imLineNo == (pomPrint->imMaxLines-1) || i == (ilLines-1))
				{
					PrintTableLine(&omLines[i],true);
				}
				else
				{
					PrintTableLine(&omLines[i],false);
				}
			}
			olFooter2.Format(GetString(IDS_STRING1198),pomPrint->imPageNo);
			pomPrint->PrintUIFFooter(olFooter1,"",olFooter2);
			pomPrint->omCdc.EndPage();
			pomPrint->omCdc.EndDoc();
		}

		omPrintHeadHeaderArray.DeleteAll();
		SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
		delete pomPrint;
		//omBitmap.DeleteObject();
		pomPrint = NULL;
	}

	CCS_CATCH_ALL

}

//-----------------------------------------------------------------------------------------------

bool DailyTableViewer::PrintTableHeader()
{

	pomPrint->omCdc.StartPage();
	pomPrint->imPageNo++;
	pomPrint->imLineNo = 0;
	double dgCCSPrintFactor = 2.7 ;
	CString olTableName(GetString(IDS_STRING1775));

	//pomPrint->PrintUIFHeader(olTableName,CString(CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M")),pomPrint->imFirstLine-10);
	pomPrint->PrintUIFHeader(olTableName, "", pomPrint->imFirstLine-10);
//,CString(CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M"))
	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;

	rlElement.Alignment   = PRINT_LEFT;
	rlElement.FrameLeft   = PRINT_FRAMEMEDIUM;//PRINT_NOFRAME;
	rlElement.FrameRight  = PRINT_FRAMEMEDIUM;//PRINT_NOFRAME;
	rlElement.FrameTop    = PRINT_FRAMEMEDIUM;//PRINT_NOFRAME;
	rlElement.FrameBottom = PRINT_FRAMETHIN;
	rlElement.pFont       = &pomPrint->ogCourierNew_Bold_8;

	//int ilSize = omHeaderDataArray.GetSize();
	int ilSize = omPrintHeadHeaderArray.GetSize();


	int i;
	int ilTableLength = 0;				// Laenge in logischen Einheiten
	int ilCurrentColumnSize = 0;		// Groesse der aktuellen Spalte in der Schleife
	for(i = 0; i < ilSize; i++)
	{
		ilCurrentColumnSize = (int)(omPrintHeadHeaderArray[i].Length*dgCCSPrintFactor);
		if(ilCurrentColumnSize < igCCSPrintMinLength)
			ilCurrentColumnSize += igCCSPrintMoreLength; 
		ilTableLength = ilTableLength + ilCurrentColumnSize;
	
		// Druckbereich mit Laenge der berechneten Tabelle vergleichen
		if (pomPrint->omCdc.GetDeviceCaps(HORZRES) < 
			(int)(MulDiv((ilTableLength),pomPrint->GetLogPixelsX(), 72) * 0.284) + pomPrint->GetLeftOffset())
		{
			// Tabelle horizontal laenger als Platz zum Drucken 
			bmCarriageReturn = true;
			// Spalte des Umbruchs merken
			imColumnNoCarriageReturn = i - 1;
			break;
		}

	}
	// Tabellenlaenge umrechnen
	
	CPoint olPoint(ilTableLength, 0);
	pomPrint->omCdc.LPtoDP(&olPoint); 

	for(i = 0; i < ilSize; i++)
	{
		//rlElement.Length = (int)(omHeaderDataArray[i].Length*dgCCSPrintFactor);
		rlElement.Length = (int)(omPrintHeadHeaderArray[i].Length*dgCCSPrintFactor);
		if(rlElement.Length < igCCSPrintMinLength)
			rlElement.Length+=igCCSPrintMoreLength; 
		rlElement.Text   = omPrintHeadHeaderArray[i].Text;
		rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
	}

	// ein- oder zweizeiliger Header
	if (bmCarriageReturn)
		pomPrint->PrintDoubleLine(rlPrintLine, imColumnNoCarriageReturn);
	else
		pomPrint->PrintLine(rlPrintLine);

	rlPrintLine.DeleteAll();

	return true;

}

//-----------------------------------------------------------------------------------------------

bool DailyTableViewer::PrintTableLine(DAILYSCHEDULE_LINEDATA *prpLine,bool bpLastLine)
{

	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;
	double dgCCSPrintFactor = 2.7;

    pomPrint->omCdc.SelectObject(&pomPrint->ogCourierNew_Regular_6);
	CSize olSize;
		
	rlElement.pFont = &pomPrint->ogCourierNew_Regular_6;

	int ilSize = omPrintHeadHeaderArray.GetSize();
	for(int i=0;i<ilSize && i<omShownColumnsSize.GetSize();i++)
	{
		olSize = pomPrint->omCdc.GetTextExtent(omShownColumnsSize[i]);
		
		rlElement.Length = (int)(omPrintHeadHeaderArray[i].Length*dgCCSPrintFactor);
		if(rlElement.Length < igCCSPrintMinLength)
			rlElement.Length+=igCCSPrintMoreLength; 
		rlElement.Alignment  = PRINT_LEFT;
		rlElement.FrameTop   = PRINT_FRAMETHIN;
		if(bpLastLine == true)
		{
			rlElement.FrameBottom = PRINT_FRAMETHIN;
		}
		else
		{
			rlElement.FrameBottom = PRINT_FRAMETHIN/*PRINT_NOFRAME*/;
		}
		
		rlElement.FrameLeft  = PRINT_FRAMETHIN;
		rlElement.FrameRight = PRINT_FRAMETHIN;
		rlElement.Text = GetFieldContent(prpLine, omShownColumns[i]);

		rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
		
	}

	if (bmCarriageReturn)
		pomPrint->PrintDoubleLine(rlPrintLine, imColumnNoCarriageReturn);		
	else
		pomPrint->PrintLine(rlPrintLine);

	rlPrintLine.DeleteAll();

	return true;

}

//-----------------------------------------------------------------------------------------------

void DailyTableViewer::SetColumnsToShow(const CStringArray &opShownColumns)
{

	CCS_TRY

	omShownColumns.Copy(opShownColumns);

	CCS_CATCH_ALL

}


void DailyTableViewer::GetSelectedRow()
{

	CCS_TRY

	// selektierte Zeile anhand der URNO merken
	imSelection = pomTable->GetCurSel();
	
	if (imSelection > -1)
	{
		lmUrnoSelect = omLines[imSelection].AUrno;
		// Urno Arrival oder Departure
		if (lmUrnoSelect == 0)
			lmUrnoSelect = omLines[imSelection].DUrno;
	}

	// erste sichtbare Zeile merken
	imTopIndex = pomTable->GetCTableListBox()->GetTopIndex();
	if (imTopIndex > -1 && omLines.GetSize() > 0)
		lmUrnoFirst = omLines[imTopIndex].AUrno;
	if (lmUrnoFirst == 0)
		lmUrnoFirst = omLines[imTopIndex].DUrno;

	CCS_CATCH_ALL

}


void DailyTableViewer::SetSelectedRow()
{

	CCS_TRY

	// selektierte Zeile anhand der URNO setzen
	pomTable->SelectLine(imSelection);
	
	// erste sichtbare Zeile setzen
	pomTable->GetCTableListBox()->SetTopIndex(imTopIndex);

	CCS_CATCH_ALL

}


CString DailyTableViewer::GetFieldContent(DAILYSCHEDULE_LINEDATA* prlLine,
										  CString opCurrentColumns)
{

	CString olFormat;

	// rotation
	if (opCurrentColumns == "RACT3")
	{
		return prlLine->Act3;
	}
	if (opCurrentColumns == "RREGN")
	{
		return prlLine->Regn;
	}

	// ARRIVAL
	if (opCurrentColumns == "AACT3")
	{
		return prlLine->Act3;
	}
	if (opCurrentColumns == "AACT5")
	{
		return prlLine->Act5;
	}
	if (opCurrentColumns == "AAIRB")
	{
		return DateToHourDivString(prlLine->AAirb, prlLine->AStoa); //falls die Uhrzeit ein anderer Tag ist
	}
	if (opCurrentColumns == "AAIRU")
	{
		return DateToHourDivString(prlLine->AAiru, prlLine->AStoa);
	}
	if (opCurrentColumns == "ACSGN")
	{
		return prlLine->ACsgn;
	}
	if (opCurrentColumns == "ASLOT")
	{
		return DateToHourDivString(prlLine->ASlot, prlLine->AStoa);//prlLine->ASlot.Format("%d.%m.%Y-%H:%M");
	}
	if (opCurrentColumns == "ADES3")
	{
		return prlLine->ADes3;
	}
	if (opCurrentColumns == "ADES4")
	{
		return prlLine->ADes4;
	}
	if (opCurrentColumns == "AETDC")
	{
		return DateToHourDivString(prlLine->AEtdc, prlLine->AStoa);
	}
	if (opCurrentColumns == "AETAI")
	{
		return DateToHourDivString(prlLine->AEtai, prlLine->AStoa);
	}
	if (opCurrentColumns == "AETDI")
	{
		return DateToHourDivString(prlLine->AEtdi, prlLine->AStoa);
	}
	if (opCurrentColumns == "AFLNO")
	{
		return prlLine->AFlno;
	}
	if (opCurrentColumns == "AFTYP")
	{
		return prlLine->AFtyp;
	}
	if (opCurrentColumns == "AGTA1")
	{
		return prlLine->AGta1;
	}
	if (opCurrentColumns == "AGTA2")
	{
		return prlLine->AGta2;
	}
	if (opCurrentColumns == "AIFRA")
	{
		return prlLine->AIfra;
	}
	if (opCurrentColumns == "AIFRD")
	{
		return prlLine->AIfrd;
	}
	if (opCurrentColumns == "AISRE")
	{
		return prlLine->AIsre;
	}
	if (opCurrentColumns == "ALAND")
	{
		CString olTmp;
		if(prlLine->ALand == TIMENULL)
		{
			olTmp = "       ";
		}		
		else
		{
			olTmp = DateToHourDivString(prlLine->ALand, prlLine->AStoa);
		}
		return olTmp;
	}
	if (opCurrentColumns == "ALSTU")
	{
		return DateToHourDivString(prlLine->ALstu, prlLine->AStoa);
	}
	if (opCurrentColumns == "AOFBL")
	{
		return DateToHourDivString(prlLine->AOfbl, prlLine->AStoa);
	}
	if (opCurrentColumns == "AONBE")
	{
		return DateToHourDivString(prlLine->AOnbe, prlLine->AStoa);
	}
	if (opCurrentColumns == "AONBL")
	{
		return DateToHourDivString(prlLine->AOnbl, prlLine->AStoa);
	}
	if (opCurrentColumns == "AORG3")
	{
		return prlLine->AOrg3;
	}
	if (opCurrentColumns == "AORG4")
	{
		return prlLine->AOrg4;
	}
	if (opCurrentColumns == "APSTA")
	{
		return prlLine->APsta;
	}
	if (opCurrentColumns == "APSTD")
	{
		return prlLine->APstd;
	}
	if (opCurrentColumns == "AREGN")
	{
		return prlLine->Regn;
	}
	if (opCurrentColumns == "AREMP")
	{
		return prlLine->ARemp;
	}
	if (opCurrentColumns == "ARKEY")
	{
		olFormat.Format("%ld",prlLine->ARkey);
		return olFormat;
	}
	if (opCurrentColumns == "ARWYA")
	{
		return prlLine->ARwya;
	}
	if (opCurrentColumns == "ARWYD")
	{
		return prlLine->ARwyd;
	}
	if (opCurrentColumns == "ASTAB")
	{
		return prlLine->AStab;
	}
	if (opCurrentColumns == "ASTOATime")
	{
		return prlLine->AStoa.Format("%H:%M");
	}
	if (opCurrentColumns == "ASTOADate")
	{
//		return prlLine->AStoa.Format("%d");
		return prlLine->AStoa.Format("%d.%m.%Y");
	}
	if (opCurrentColumns == "ASTODTime")
	{
		return prlLine->AStod.Format("%H:%M");
	}
	if (opCurrentColumns == "ASTODDate")
	{
//		return prlLine->AStod.Format("%d");
		return prlLine->AStod.Format("%H:%M");
	}
	if (opCurrentColumns == "ATIFA")
	{
		return DateToHourDivString(prlLine->ATifa, prlLine->AStoa);
	}
	if (opCurrentColumns == "ATIFD")
	{
		return DateToHourDivString(prlLine->ATifd, prlLine->AStoa);
	}
	if (opCurrentColumns == "ATMOA")
	{
		return DateToHourDivString(prlLine->ATmoa, prlLine->AStoa);
	}
	if (opCurrentColumns == "AURNO")
	{
		olFormat.Format("%ld",prlLine->AUrno);
		return olFormat;
	}
	if (opCurrentColumns == "AUSEU")
	{
		return prlLine->AUseu;
	}
	if (opCurrentColumns == "AVIA4")
	{
		return prlLine->AVia4;
	}
	if (opCurrentColumns == "AVN")
	{
		return prlLine->AVian;
	}
	if (opCurrentColumns == "ATTYP")
	{
		return prlLine->ATtyp;
	}
	if (opCurrentColumns == "ACHGI")
	{
		return prlLine->AChgi;
	}
	if (opCurrentColumns == "ARTYP")
	{
		return prlLine->ARtyp;
	}
	if (opCurrentColumns == "AADID")
	{
		return prlLine->AAdid;
	}
	if (opCurrentColumns == "ANXTI")
	{
		return DateToHourDivString(prlLine->ANxti, prlLine->AStoa);
	}
	if (opCurrentColumns == "ABLT1")
	{
		return prlLine->ABlt1;
	}
	if (opCurrentColumns == "APABA")
	{
		return DateToHourDivString(prlLine->APaba, prlLine->AStoa);
	}
	if (opCurrentColumns == "APAEA")
	{
		return DateToHourDivString(prlLine->APaea, prlLine->AStoa);
	}
	if (opCurrentColumns == "APDBA")
	{
		return DateToHourDivString(prlLine->APdba, prlLine->AStoa);
	}
	if (opCurrentColumns == "APDEA")
	{
		return DateToHourDivString(prlLine->APdea, prlLine->AStoa);
	}
	if (opCurrentColumns == "AMING")
	{
		return prlLine->AMing;
	}
	if (opCurrentColumns == "AADER")
	{
		return prlLine->AAder;
	}
	if (opCurrentColumns == "AVIAL")
	{
		return prlLine->AVial;
	}
	if (opCurrentColumns == "AVIA3L")
	{
		if (prlLine->AVia3.IsEmpty())
			return " ";

		if (strcmp(prlLine->AVian,"0") == 0 || strcmp(prlLine->AVian,"1") == 0)
			return prlLine->AVia3;
		else
			return prlLine->AVia3 + "("  + CString(prlLine->AVian) + ")";
	}
	if (opCurrentColumns == "ABLT1/ABLT2")
	{
		// nix
		if (prlLine->ABlt1.IsEmpty() && prlLine->ABlt2.IsEmpty())
			return " ";
		// nur ABlt1
		if (!prlLine->ABlt1.IsEmpty() && prlLine->ABlt2.IsEmpty())
			return prlLine->ABlt1;
		// nur ABlt2
		if (prlLine->ABlt1.IsEmpty() && !prlLine->ABlt2.IsEmpty())
			return prlLine->ABlt2;
		// beides
		if (!prlLine->ABlt1.IsEmpty() && !prlLine->ABlt2.IsEmpty())
			return prlLine->ABlt1 + "/" + prlLine->ABlt2;
	}
	if (opCurrentColumns == "AREMP/DREMP")
	{
		// nix
		if (prlLine->ARemp.IsEmpty() && prlLine->DRemp.IsEmpty())
			return " ";
		// nur ARemp
		if (!prlLine->ARemp.IsEmpty() && prlLine->DRemp.IsEmpty())
			return prlLine->ARemp;
		// nur DRemp
		if (prlLine->ARemp.IsEmpty() && !prlLine->DRemp.IsEmpty())
			return prlLine->DRemp;
		// beides
		if (!prlLine->ARemp.IsEmpty() && !prlLine->DRemp.IsEmpty())
			return prlLine->ARemp + "/" + prlLine->DRemp;
	}
	if (opCurrentColumns == "APSTA/DPSTD")
	{
		// nix
		if (prlLine->APsta.IsEmpty() && prlLine->DPstd.IsEmpty())
			return " ";
		// nur APsta
		if (!prlLine->APsta.IsEmpty() && prlLine->DPstd.IsEmpty())
			return prlLine->APsta + "/";
		// nur DPstd
		if (prlLine->APsta.IsEmpty() && !prlLine->DPstd.IsEmpty())
			return "/" + prlLine->DPstd;
		// beides
		if (!prlLine->APsta.IsEmpty() && !prlLine->DPstd.IsEmpty())
			return prlLine->APsta + "/" + prlLine->DPstd;
	}
	if (opCurrentColumns == "ACKIF/ACKIT")
	{
		// nix
		if (prlLine->ACkif.IsEmpty() && prlLine->ACkit.IsEmpty())
			return " ";
		// nur ACkif
		if (!prlLine->ACkif.IsEmpty() && prlLine->ACkit.IsEmpty())
			return prlLine->ACkif;
		// nur ACkit
		if (prlLine->ACkif.IsEmpty() && !prlLine->ACkit.IsEmpty())
			return prlLine->ACkit;
		// beides
		if (!prlLine->ACkif.IsEmpty() && !prlLine->ACkit.IsEmpty())
			return prlLine->ACkif + "-" + prlLine->ACkit;
	}
	if (opCurrentColumns == "ALAND/AONLB")
	{
		CString	olTmp1 = DateToHourDivString(prlLine->ALand, prlLine->AStoa);
		CString olTmp2 = DateToHourDivString(prlLine->AOnbl, prlLine->AStoa);
		if (olTmp1.IsEmpty() && olTmp2.IsEmpty())
			return " ";
		if (!olTmp1.IsEmpty() && olTmp2.IsEmpty())
			return olTmp1 + "/";
		if (olTmp1.IsEmpty() && !olTmp2.IsEmpty())
			return "/" + olTmp2;
		if (!olTmp1.IsEmpty() && !olTmp2.IsEmpty())
			return olTmp1 + "/" + olTmp2;
	}
	if (opCurrentColumns == "AFLTI")
	{
		if (!prlLine->AFlti.IsEmpty())
			return prlLine->AFlti;
		else
			return " ";
	}

	
	// DEPARTURE
	if (opCurrentColumns == "DACT3")
	{
		return prlLine->Act3;
	}
	if (opCurrentColumns == "DACT5")
	{
		return prlLine->Act5;
	}
	if (opCurrentColumns == "DAIRB")
	{
		return DateToHourDivString(prlLine->DAirb, prlLine->DStod);
	}
	if (opCurrentColumns == "DAIRU")
	{
		return DateToHourDivString(prlLine->DAiru, prlLine->DStod);
	}
	if (opCurrentColumns == "DCSGN")
	{
		return prlLine->DCsgn;
	}
	if (opCurrentColumns == "DSLOT")
	{
		return DateToHourDivString(prlLine->DSlot, prlLine->DStod);
	}
	if (opCurrentColumns == "DDES3")
	{
		return prlLine->DDes3;
	}
	if (opCurrentColumns == "DDES4")
	{
		return prlLine->DDes4;
	}
	if (opCurrentColumns == "DETDC")
	{
		return DateToHourDivString(prlLine->DEtdc, prlLine->DStod);
	}
	if (opCurrentColumns == "DETAI")
	{
		return DateToHourDivString(prlLine->DEtai, prlLine->DStod);
	}
	if (opCurrentColumns == "DETDI")
	{
		return DateToHourDivString(prlLine->DEtdi, prlLine->DStod);
	}
	if (opCurrentColumns == "DFLNO")
	{
		return prlLine->DFlno;
	}
	if (opCurrentColumns == "DFTYP")
	{
		return prlLine->DFtyp;
	}
	if (opCurrentColumns == "DGTA1")
	{
		return prlLine->DGta1;
	}
	if (opCurrentColumns == "DGTA2")
	{
		return prlLine->DGta2;
	}
	if (opCurrentColumns == "DGTD1")
	{
		return prlLine->DGtd1;
	}
	if (opCurrentColumns == "DGTD2")
	{
		return prlLine->DGtd2;
	}
	if (opCurrentColumns == "DIFRA")
	{
		return prlLine->DIfra;
	}
	if (opCurrentColumns == "DIFRD")
	{
		return prlLine->DIfrd;
	}
	if (opCurrentColumns == "DISRE")
	{
		return prlLine->DIsre;
	}
	if (opCurrentColumns == "DLAND")
	{
		return DateToHourDivString(prlLine->DLand, prlLine->DStod);
	}
	if (opCurrentColumns == "DLSTU")
	{
		return DateToHourDivString(prlLine->DLstu, prlLine->DStod);
	}
	if (opCurrentColumns == "DOFBL")
	{
		return DateToHourDivString(prlLine->DOfbl, prlLine->DStod);
	}
	if (opCurrentColumns == "DONBE")
	{
		return DateToHourDivString(prlLine->DOnbe, prlLine->DStod);
	}
	if (opCurrentColumns == "DONBL")
	{
		return DateToHourDivString(prlLine->DOnbl, prlLine->DStod);
	}
	if (opCurrentColumns == "DORG3")
	{
		return prlLine->DOrg3;
	}
	if (opCurrentColumns == "DORG4")
	{
		return prlLine->DOrg4;
	}
	if (opCurrentColumns == "DPSTA")
	{
		return prlLine->DPsta;
	}
	if (opCurrentColumns == "DPSTD")
	{
		return prlLine->DPstd;
	}
	if (opCurrentColumns == "DREGN")
	{
		return prlLine->Regn;
	}
	if (opCurrentColumns == "DREMP")
	{
		return prlLine->DRemp;
	}
	if (opCurrentColumns == "DRKEY")
	{
		olFormat.Format("%ld",prlLine->DRkey);
		return olFormat;
	}
	if (opCurrentColumns == "DRWYA")
	{
		return prlLine->DRwya;
	}
	if (opCurrentColumns == "DRWYD")
	{
		return prlLine->DRwyd;
	}
	if (opCurrentColumns == "DTAB")
	{
		return prlLine->DStab;
	}
	if (opCurrentColumns == "DSTOATime")
	{
		return prlLine->DStoa.Format("%H:%M");
	}
	if (opCurrentColumns == "DSTOADate")
	{
//		return prlLine->DStoa.Format("%d");
		return prlLine->DStoa.Format("%d.%m.%Y");
	}
	if (opCurrentColumns == "DSTODTime")
	{
		return prlLine->DStod.Format("%H:%M");
	}
	if (opCurrentColumns == "DSTODDate")
	{
//		return prlLine->DStod.Format("%d");
		return prlLine->DStod.Format("%d.%m.%Y");
	}
	if (opCurrentColumns == "DTIFA")
	{
		return DateToHourDivString(prlLine->DTifa, prlLine->DStod);
	}
	if (opCurrentColumns == "DTIFD")
	{
		return DateToHourDivString(prlLine->DTifd, prlLine->DStod);
	}
	if (opCurrentColumns == "DTMOA")
	{
		return DateToHourDivString(prlLine->DTmoa, prlLine->DStod);
	}
	if (opCurrentColumns == "DURNO")
	{
		olFormat.Format("%ld",prlLine->DUrno);
		return olFormat;
	}
	if (opCurrentColumns == "DUSEU")
	{
		return prlLine->DUseu;
	}
	if (opCurrentColumns == "DVIA4")
	{
		return prlLine->DVia4;
	}
	if (opCurrentColumns == "DVN")
	{
		return prlLine->DVian;
	}
	if (opCurrentColumns == "DTTYP")
	{
		return prlLine->DTtyp;
	}
	if (opCurrentColumns == "DCHGI")
	{
		return prlLine->DChgi;
	}
	if (opCurrentColumns == "DRTYP")
	{
		return prlLine->DRtyp;
	}
	if (opCurrentColumns == "DADID")
	{
		return prlLine->DAdid;
	}
	if (opCurrentColumns == "DNXTI")
	{
		return DateToHourDivString(prlLine->DNxti, prlLine->DStod);//prlLine->DNxti.Format("%H:%M");
	}
	if (opCurrentColumns == "DBLT1")
	{
		return prlLine->DBlt1;
	}
	if (opCurrentColumns == "DPABA")
	{
		return DateToHourDivString(prlLine->DPaba, prlLine->DStod);//prlLine->DPaba.Format("%H:%M");
	}
	if (opCurrentColumns == "DPAEA")
	{
		return DateToHourDivString(prlLine->DPaea, prlLine->DStod);//prlLine->DPaea.Format("%H:%M");
	}
	if (opCurrentColumns == "DPDBA")
	{
		return DateToHourDivString(prlLine->DPdba, prlLine->DStod);//prlLine->DPdba.Format("%H:%M");
	}
	if (opCurrentColumns == "DPDEA")
	{
		return DateToHourDivString(prlLine->DPdea, prlLine->DStod);//prlLine->DPdea.Format("%H:%M");
	}
	if (opCurrentColumns == "DMING")
	{
		return prlLine->DMing;
	}
	if (opCurrentColumns == "DDADER")
	{
		return prlLine->DDAder;
	}
	if (opCurrentColumns == "DVIAL")
	{
		return prlLine->DVial;
	}
	if (opCurrentColumns == "DVIA3L")
	{
		if (prlLine->DVia3.IsEmpty())
			return " ";

		if (strcmp(prlLine->DVian,"0") == 0 || strcmp(prlLine->DVian,"1") == 0)
			return prlLine->DVia3;
		else
			return prlLine->DVia3 + "("  + CString(prlLine->DVian) + ")";
	}
	if (opCurrentColumns == "DCKIF/DCKIT")
	{
		// nix
		if (prlLine->DCkif.IsEmpty() && prlLine->DCkit.IsEmpty())
			return " ";
		// nur DCKIF
		if (!prlLine->DCkif.IsEmpty() && prlLine->DCkit.IsEmpty())
			return prlLine->DCkif;
		// nur DCKIT
		if (prlLine->DCkif.IsEmpty() && !prlLine->DCkit.IsEmpty())
			return prlLine->DCkit;
		// beides
		if (!prlLine->DCkif.IsEmpty() && !prlLine->DCkit.IsEmpty())
		{
			if(prlLine->DCkif != prlLine->DCkit)
				return prlLine->DCkif+ "-" + prlLine->DCkit;
			else
				return prlLine->DCkif;
		}
	}
	if (opCurrentColumns == "DBLT2")
	{
		return prlLine->DBlt2;
	}
	if (opCurrentColumns == "DFLTI")
	{
		if (!prlLine->DFlti.IsEmpty())
			return prlLine->DFlti;
		else
			return "";
	}
	if (opCurrentColumns == "DGTD1/DGTD2")
	{
		// nix
		if (prlLine->DGtd1.IsEmpty() && prlLine->DGtd2.IsEmpty())
			return " ";
		// nur DGtd1
		if (!prlLine->DGtd1.IsEmpty() && prlLine->DGtd2.IsEmpty())
			return prlLine->DGtd1;
		// nur DGtd2
		if (prlLine->DGtd1.IsEmpty() && !prlLine->DGtd2.IsEmpty())
			return prlLine->DGtd2;
		// beides
		if (!prlLine->DGtd1.IsEmpty() && !prlLine->DGtd2.IsEmpty())
			return prlLine->DGtd1 + "/" + prlLine->DGtd2;
	}
	if (opCurrentColumns == "DOFBL/DAIRB")
	{
		CString	olTmp1 = DateToHourDivString(prlLine->DOfbl, prlLine->DStod);
		CString olTmp2 = DateToHourDivString(prlLine->DAirb, prlLine->DStod);
		if (olTmp1.IsEmpty() && olTmp2.IsEmpty())
			return " ";
		if (!olTmp1.IsEmpty() && olTmp2.IsEmpty())
			return olTmp1 + "/";
		if (olTmp1.IsEmpty() && !olTmp2.IsEmpty())
			return "/" + olTmp2;
		if (!olTmp1.IsEmpty() && !olTmp2.IsEmpty())
			return olTmp1 + "/" + olTmp2;
	}

	// ROTATION
	if (opCurrentColumns == "ACT3")
	{
		return prlLine->Act3;
	}
	if (opCurrentColumns == "ACT5")
	{
		return prlLine->Act5;
	}
	if (opCurrentColumns == "REGN")
	{
		return prlLine->Regn;
	}
	if (opCurrentColumns == "VIP")
	{
		// nur Ankunft
		if (prlLine->AVip && !prlLine->DVip)
			return "VIP/";
		// nur Abflug 
		if (!prlLine->AVip  && prlLine->DVip)
			return "/VIP";
		// Ankunft und Abflug
		if (prlLine->AVip  && prlLine->DVip)
			return "/VIP/";
		// weder noch
		if (!prlLine->AVip  && !prlLine->DVip)
			return " ";
	}

	return "";

}


// Setzen der Spaltenbreite in Anzahl Zeichen
void DailyTableViewer::SetFieldLength(CString opCurrentColumns)
{
 	CString olFormat;

	//rotation
	if (opCurrentColumns == "RACT3")
	{
		omAnzChar.Add(3);
		omShownColumnsSize.Add("XXX");
		return;
	}
	if (opCurrentColumns == "RREGN")
	{
		omShownColumnsSize.Add("XXXXXXX");
		omAnzChar.Add(9);
		return;
	}
	// ARRIVAL
	if (opCurrentColumns == "AACT3")
	{
		omAnzChar.Add(3);
		omShownColumnsSize.Add("XXX");
		return;
	}
	if (opCurrentColumns == "AACT5")
	{
		omAnzChar.Add(5);
		omShownColumnsSize.Add("XXXXX");
		return;
	}
	if (opCurrentColumns == "AAIRB")
	{
		omAnzChar.Add(6);
		omShownColumnsSize.Add("XXXX");

		return;
	}
	if (opCurrentColumns == "AAIRU")
	{
		omAnzChar.Add(6);
		omShownColumnsSize.Add("XXXX");

		return;
	}
	if (opCurrentColumns == "ACSGN")
	{
		omAnzChar.Add(9);
		omShownColumnsSize.Add("XXXXXXXXX");
		return;
	}
	if (opCurrentColumns == "ASLOT")
	{
		omShownColumnsSize.Add("XXXXX");
		omAnzChar.Add(5);
		return;
	}
	if (opCurrentColumns == "ADES3")
	{
		omShownColumnsSize.Add("XXXXX");
		omAnzChar.Add(5);
		return;
	}
	if (opCurrentColumns == "ADES4")
	{
		omShownColumnsSize.Add("XXXXXX");
		omAnzChar.Add(6);
		return;
	}
	if (opCurrentColumns == "AETDC")
	{
		omShownColumnsSize.Add("XXXX");
		omAnzChar.Add(4);
		return;
	}
	if (opCurrentColumns == "AETAI")
	{
		omShownColumnsSize.Add("XXXX");
		omAnzChar.Add(6);
		return;
	}
	if (opCurrentColumns == "AETDI")
	{
		omShownColumnsSize.Add("XXXX");
		omAnzChar.Add(6);
		return;
	}
	if (opCurrentColumns == "AFLNO")
	{
		omShownColumnsSize.Add("XXXXXXXX");

		omAnzChar.Add(8);
		return;
	}
	if (opCurrentColumns == "AFTYP")
	{
		omShownColumnsSize.Add("XXXXX");
		omAnzChar.Add(5);
		return;
	}
	if (opCurrentColumns == "AGTA1")
	{
		omShownColumnsSize.Add("XXX");
		omAnzChar.Add(3);
		return;
	}
	if (opCurrentColumns == "AGTA2")
	{
		omShownColumnsSize.Add("XXX");
		omAnzChar.Add(3);
		return;
	}
	if (opCurrentColumns == "AGTD1")
	{
		omShownColumnsSize.Add("XXX");
		omAnzChar.Add(3);
		return;
	}
	if (opCurrentColumns == "AGTD2")
	{
		omAnzChar.Add(3);
		omShownColumnsSize.Add("XXX");
		return;
	}
	if (opCurrentColumns == "AIFRA")
	{
		omAnzChar.Add(1);
		omShownColumnsSize.Add("X");
		return;
	}
	if (opCurrentColumns == "AIFRD")
	{
		omShownColumnsSize.Add("XXXX");
		omAnzChar.Add(4);
		return;
	}
	if (opCurrentColumns == "AISRE")
	{
		omShownColumnsSize.Add("X");
		omAnzChar.Add(1);
		return;
	}
	if (opCurrentColumns == "ALAND")
	{
		omShownColumnsSize.Add("XXXX");
		omAnzChar.Add(6);
		return;
	}
	if (opCurrentColumns == "ALSTU")
	{
		omShownColumnsSize.Add("XXXX");
		omAnzChar.Add(4);
		return;
	}
	if (opCurrentColumns == "AOFBL")
	{
		omAnzChar.Add(6);
		omShownColumnsSize.Add("XXXX");
		return;
	}
	if (opCurrentColumns == "AONBE")
	{
		omAnzChar.Add(6);
		omShownColumnsSize.Add("XXXX");
		return;
	}
	if (opCurrentColumns == "AONBL")
	{
		omAnzChar.Add(6);
		omShownColumnsSize.Add("XXXX");
		return;
	}
	if (opCurrentColumns == "AORG3")
	{
		omShownColumnsSize.Add("XXX");
		omAnzChar.Add(3);
		return;
	}
	if (opCurrentColumns == "AORG4")
	{
		omShownColumnsSize.Add("XXXX");
		omAnzChar.Add(4);
		return;
	}
	if (opCurrentColumns == "APSTA")
	{
		omAnzChar.Add(3);
		omShownColumnsSize.Add("XXX");
		return;
	}
	if (opCurrentColumns == "APSTD")
	{
		omShownColumnsSize.Add("XXXX");
		omAnzChar.Add(4);
		return;
	}
	if (opCurrentColumns == "AREGN")
	{
		omShownColumnsSize.Add("XXXXXXX");
		omAnzChar.Add(9);
		return;
	}
	if (opCurrentColumns == "AREMP")
	{
		omShownColumnsSize.Add("XXXX");
		omAnzChar.Add(4);
		return;
	}
	if (opCurrentColumns == "ARKEY")
	{
		omAnzChar.Add(4);
		omShownColumnsSize.Add("XXXX");
		return;
	}
	if (opCurrentColumns == "ARWYA")
	{
		omShownColumnsSize.Add("XXX");
		omAnzChar.Add(3);
		return;
	}
	if (opCurrentColumns == "ARWYD")
	{
		omShownColumnsSize.Add("XXXX");
		omAnzChar.Add(4);
		return;
	}
	if (opCurrentColumns == "ASTAB")
	{
		omAnzChar.Add(4);
		omShownColumnsSize.Add("XXXX");
		return;
	}
	if (opCurrentColumns == "ASTOATime")
	{
		omShownColumnsSize.Add("XXXX");
		omAnzChar.Add(6);
		return;
	}
	if (opCurrentColumns == "ASTOADate")
	{
//		omShownColumnsSize.Add("XX");
		omShownColumnsSize.Add("XXXXXXXX");
		omAnzChar.Add(8);
		return;
	}
	if (opCurrentColumns == "ASTODTime")
	{
		omShownColumnsSize.Add("XXXX");
		omAnzChar.Add(6);
		return;
	}
	if (opCurrentColumns == "ASTODDate")
	{
		omShownColumnsSize.Add("XXXXXXXX");
		omAnzChar.Add(8);
		return;
	}
	if (opCurrentColumns == "ATIFA")
	{
		omShownColumnsSize.Add("XXXX");
		omAnzChar.Add(6);
		return;
	}
	if (opCurrentColumns == "ATIFD")
	{
		omShownColumnsSize.Add("XXXX");
		omAnzChar.Add(6);
		return;
	}
	if (opCurrentColumns == "ATMOA")
	{
		omShownColumnsSize.Add("XXXX");
		omAnzChar.Add(6);
		return;
	}
	if (opCurrentColumns == "AURNO")
	{
		omShownColumnsSize.Add("XXXX");
		omAnzChar.Add(4);
		return;
	}
	if (opCurrentColumns == "AUSEU")
	{
		omAnzChar.Add(4);
		omShownColumnsSize.Add("XXXX");
		return;
	}
	if (opCurrentColumns == "AVIA4")
	{
		omShownColumnsSize.Add("XXX");
		omAnzChar.Add(4);
		return;
	}
	if (opCurrentColumns == "AVN")
	{
		omShownColumnsSize.Add("XX");
		omAnzChar.Add(1);
		return;
	}
	if (opCurrentColumns == "ATTYP")
	{
		omShownColumnsSize.Add("XX");
		omAnzChar.Add(2);
		return;
	}
	if (opCurrentColumns == "ACHGI")
	{
		omAnzChar.Add(4);
		omShownColumnsSize.Add("XXXX");
		return;
	}
	if (opCurrentColumns == "ARTYP")
	{
		omShownColumnsSize.Add("XXXX");
		omAnzChar.Add(4);
		return;
	}
	if (opCurrentColumns == "AADID")
	{
		omShownColumnsSize.Add("XXXX");
		omAnzChar.Add(4);
		return;
	}
	if (opCurrentColumns == "ANXTI")
	{
		omAnzChar.Add(4);
		omShownColumnsSize.Add("XXXX");
		return;
	}
	if (opCurrentColumns == "ABLT1")
	{
		omAnzChar.Add(4);
		omShownColumnsSize.Add("XXXX");
		return;
	}
	if (opCurrentColumns == "APABA")
	{
		omAnzChar.Add(4);
		omShownColumnsSize.Add("XXXX");
		return;
	}
	if (opCurrentColumns == "APAEA")
	{
		omAnzChar.Add(4);
		omShownColumnsSize.Add("XXXX");
		return;
	}
	if (opCurrentColumns == "APDBA")
	{
		omShownColumnsSize.Add("XXXX");
		omAnzChar.Add(4);
		return;
	}
	if (opCurrentColumns == "APDEA")
	{
		omAnzChar.Add(4);
		omShownColumnsSize.Add("XXXX");
		return;
	}
	if (opCurrentColumns == "AMING")
	{
		omAnzChar.Add(4);
		omShownColumnsSize.Add("XXXX");
		return;
	}
	if (opCurrentColumns == "AADER")
	{
		omAnzChar.Add(4);
		omShownColumnsSize.Add("XXXX");
		return;
	}
	if (opCurrentColumns == "AVIAL")
	{
		omAnzChar.Add(5);
		omShownColumnsSize.Add("XXXX");
		return;
	}
	if (opCurrentColumns == "AVIA3L")
	{
		omAnzChar.Add(5);
		omShownColumnsSize.Add("XXXX");
		return;
	}
	if (opCurrentColumns == "ABLT1/ABLT2")
	{
		omShownColumnsSize.Add("XXXXXXXXXXX");
		omAnzChar.Add(11);
		return;
	}
	if (opCurrentColumns == "AREMP/DREMP")
	{
		omShownColumnsSize.Add("XXXXXXXXXXXXXXXXXX");
		omAnzChar.Add(18);
		return;
	}
	if (opCurrentColumns == "APSTA/DPSTD")
	{
		omShownColumnsSize.Add("XXXXXXXXXX");
		omAnzChar.Add(8);
		return;
	}
	if (opCurrentColumns == "AFLTI")
	{
		omAnzChar.Add(1);
		omShownColumnsSize.Add("XX");
		return;
	}
	if (opCurrentColumns == "ALAND/AONLB")
	{
		omShownColumnsSize.Add("XXXXXXXXXXX");
		omAnzChar.Add(13);
		return;
	}
	
	// DEPARTURE
	if (opCurrentColumns == "DACT3")
	{
		omShownColumnsSize.Add("XXX");
		omAnzChar.Add(3);
		return;
	}
	if (opCurrentColumns == "DACT5")
	{
		omShownColumnsSize.Add("XXXXX");
		omAnzChar.Add(5);
		return;
	}
	if (opCurrentColumns == "DAIRB")
	{
		omShownColumnsSize.Add("XXXX");
		omAnzChar.Add(4);
		return;
	}
	if (opCurrentColumns == "DAIRU")
	{
		omShownColumnsSize.Add("XXXX");
		omAnzChar.Add(4);
		return;
	}
	if (opCurrentColumns == "DCSGN")
	{
		omShownColumnsSize.Add("XXXXXXXXX");
		omAnzChar.Add(9);
		return;
	}
	if (opCurrentColumns == "DSLOT")
	{
		omShownColumnsSize.Add("XXXXX");
		omAnzChar.Add(5);
		return;
	}
	if (opCurrentColumns == "DDES3")
	{
		omShownColumnsSize.Add("XXX");
		omAnzChar.Add(3);
		return;
	}
	if (opCurrentColumns == "DDES4")
	{
		omShownColumnsSize.Add("XXXX");

		omAnzChar.Add(4);
		return;
	}
	if (opCurrentColumns == "DETDC")
	{
		omShownColumnsSize.Add("XXXXX");
		omAnzChar.Add(5);
		return;
	}
	if (opCurrentColumns == "DETAI")
	{
		omShownColumnsSize.Add("XXXX");
		omAnzChar.Add(6);
		return;
	}
	if (opCurrentColumns == "DETDI")
	{
		omShownColumnsSize.Add("XXXX");
		omAnzChar.Add(6);
		return;
	}
	if (opCurrentColumns == "DFLNO")
	{
		omShownColumnsSize.Add("XXXXXXXX");
		omAnzChar.Add(8);
		return;
	}
	if (opCurrentColumns == "DFTYP")
	{
		omShownColumnsSize.Add("XXXXX");
		omAnzChar.Add(5);
		return;
	}
	if (opCurrentColumns == "DGTA1")
	{
		omShownColumnsSize.Add("XXXXX");
		omAnzChar.Add(5);
		return;
	}
	if (opCurrentColumns == "DGTA2")
	{
		omShownColumnsSize.Add("XXX");
		omAnzChar.Add(3);
		return;
	}
	if (opCurrentColumns == "DGTD1")
	{
		omShownColumnsSize.Add("XXXXXXX");
		omAnzChar.Add(3);
		return;
	}
	if (opCurrentColumns == "DGTD2")
	{
		omShownColumnsSize.Add("XXX");
		omAnzChar.Add(3);
		return;
	}
	if (opCurrentColumns == "DIFRA")
	{
		omShownColumnsSize.Add("X");
		omAnzChar.Add(1);
		return;
	}
	if (opCurrentColumns == "DIFRD")
	{
		omShownColumnsSize.Add("X");
		omAnzChar.Add(1);
		return;
	}
	if (opCurrentColumns == "DISRE")
	{
		omShownColumnsSize.Add("X");
		omAnzChar.Add(1);
		return;
	}
	if (opCurrentColumns == "DLAND")
	{
		omShownColumnsSize.Add("XXXX");
		omAnzChar.Add(4);
		return;
	}
	if (opCurrentColumns == "DLSTU")
	{
		omShownColumnsSize.Add("XXXX");
		omAnzChar.Add(4);
		return;
	}
	if (opCurrentColumns == "DOFBL")
	{
		omShownColumnsSize.Add("XXXXX");
		omAnzChar.Add(6);
		return;
	}
	if (opCurrentColumns == "DONBE")
	{
		omShownColumnsSize.Add("XXXXX");
		omAnzChar.Add(6);
		return;
	}
	if (opCurrentColumns == "DONBL")
	{
		omShownColumnsSize.Add("XXXXX");
		omAnzChar.Add(6);
		return;
	}
	if (opCurrentColumns == "DORG3")
	{
		omShownColumnsSize.Add("XXXX");
		omAnzChar.Add(4);
		return;
	}
	if (opCurrentColumns == "DORG4")
	{
		omShownColumnsSize.Add("XXXX");
		omAnzChar.Add(4);
		return;
	}
	if (opCurrentColumns == "DPSTA")
	{
		omShownColumnsSize.Add("XXXX");
		omAnzChar.Add(4);
		return;
	}
	if (opCurrentColumns == "DPSTD")
	{
		omShownColumnsSize.Add("XXXXX");
		omAnzChar.Add(5);
		return;
	}
	if (opCurrentColumns == "DREGN")
	{
		omShownColumnsSize.Add("XXXXXXXXX");
		omAnzChar.Add(9);
		return;
	}
	if (opCurrentColumns == "DREMP")
	{
		omShownColumnsSize.Add("XXXX");
		omAnzChar.Add(4);
		return;
	}
	if (opCurrentColumns == "DRKEY")
	{
		omShownColumnsSize.Add("XXXX");
		omAnzChar.Add(4);
		return;
	}
	if (opCurrentColumns == "DRWYA")
	{
		omShownColumnsSize.Add("XXXX");
		omAnzChar.Add(4);
		return;
	}
	if (opCurrentColumns == "DRWYD")
	{
		omShownColumnsSize.Add("XXX");
		omAnzChar.Add(3);
		return;
	}
	if (opCurrentColumns == "DTAB")
	{
		omShownColumnsSize.Add("XXXX");
		omAnzChar.Add(4);
		return;
	}
	if (opCurrentColumns == "DSTOATime")
	{
		omShownColumnsSize.Add("XXXX");
		omAnzChar.Add(6);
		return;
	}
	if (opCurrentColumns == "DSTOADate")
	{
//		omShownColumnsSize.Add("XX");
//		omAnzChar.Add(2);
		omShownColumnsSize.Add("XXXXXXXXX");
		omAnzChar.Add(8);
		return;
	}
	if (opCurrentColumns == "DSTODTime")
	{
		omShownColumnsSize.Add("XXXX");
		omAnzChar.Add(6);
		return;
	}
	if (opCurrentColumns == "DSTODDate")
	{
		omShownColumnsSize.Add("XXXXXXXX");
		omAnzChar.Add(8);
		return;
	}
	if (opCurrentColumns == "DTIFA")
	{
		omShownColumnsSize.Add("XXXX");
		omAnzChar.Add(6);
		return;
	}
	if (opCurrentColumns == "DTIFD")
	{
		omShownColumnsSize.Add("XXXX");
		omAnzChar.Add(6);
		return;
	}
	if (opCurrentColumns == "DTMOA")
	{
		omShownColumnsSize.Add("XXXXXX");
		omAnzChar.Add(6);
		return;
	}
	if (opCurrentColumns == "DURNO")
	{
		omShownColumnsSize.Add("XXXX");
		omAnzChar.Add(4);
		return;
	}
	if (opCurrentColumns == "DUSEU")
	{
		omShownColumnsSize.Add("XXXX");
		omAnzChar.Add(4);
		return;
	}
	if (opCurrentColumns == "DVIA4")
	{
		omShownColumnsSize.Add("XXX");
		omAnzChar.Add(4);
		return;
	}
	if (opCurrentColumns == "DVN")
	{
		omShownColumnsSize.Add("XX");
		omAnzChar.Add(1);
		return;
	}
	if (opCurrentColumns == "DTTYP")
	{
		omShownColumnsSize.Add("XX");
		omAnzChar.Add(2);
		return;
	}
	if (opCurrentColumns == "DCHGI")
	{
		omShownColumnsSize.Add("XXXX");
		omAnzChar.Add(4);
		return;
	}
	if (opCurrentColumns == "DRTYP")
	{
		omAnzChar.Add(4);
		omShownColumnsSize.Add("XXXX");
		return;
	}
	if (opCurrentColumns == "DADID")
	{
		omAnzChar.Add(4);
		omShownColumnsSize.Add("XXXX");
		return;
	}
	if (opCurrentColumns == "DNXTI")
	{
		omShownColumnsSize.Add("XXXX");
		omAnzChar.Add(4);
		return;
	}
	if (opCurrentColumns == "DBLT1")
	{
		omShownColumnsSize.Add("XXXX");
		omAnzChar.Add(4);
		return;
	}
	if (opCurrentColumns == "DPABA")
	{
		omAnzChar.Add(4);
		omShownColumnsSize.Add("XXXX");
		return;
	}
	if (opCurrentColumns == "DPAEA")
	{
		omAnzChar.Add(4);
		omShownColumnsSize.Add("XXXX");
		return;
	}
	if (opCurrentColumns == "DPDBA")
	{
		omAnzChar.Add(4);
		omShownColumnsSize.Add("XXXX");
		return;
	}
	if (opCurrentColumns == "DPDEA")
	{
		omShownColumnsSize.Add("XXXX");
		omAnzChar.Add(4);
		return;
	}
	if (opCurrentColumns == "DMING")
	{
		omAnzChar.Add(4);
		omShownColumnsSize.Add("XXXX");
		return;
	}
	if (opCurrentColumns == "DDADER")
	{
		omAnzChar.Add(4);
		omShownColumnsSize.Add("XXXX");
		return;
	}
	if (opCurrentColumns == "DVIAL")
	{
		omAnzChar.Add(5);
		omShownColumnsSize.Add("XXX");
		return;
	}
	if (opCurrentColumns == "DVIA3L")
	{
		omAnzChar.Add(5);
		omShownColumnsSize.Add("XXX");
		return;
	}
	if (opCurrentColumns == "DCKIF/DCKIT")
	{
		omShownColumnsSize.Add("XXX");
		omAnzChar.Add(6);
		return;
	}
	if (opCurrentColumns == "VIP")
	{
		omShownColumnsSize.Add("XXXXXXX");
		omAnzChar.Add(5);
		return;
	}
	if (opCurrentColumns == "DBLT2")
	{
		omShownColumnsSize.Add("XXX");
		omAnzChar.Add(3);
		return;
	}
	if (opCurrentColumns == "DGTD1/DGTD2")
	{
		omShownColumnsSize.Add("XXXXXXXXXX");
		omAnzChar.Add(6);
		return;
	}
	if (opCurrentColumns == "DOFBL/DAIRB")
	{
		omShownColumnsSize.Add("XXXXXXXXXXX");
		omAnzChar.Add(13);
		return;
	}
	if (opCurrentColumns == "DFLTI")
	{
		omShownColumnsSize.Add("XX");
		omAnzChar.Add(1);
		return;
	}
	
	// falls kein if passt, ein default-Wert, sonst Array zu klein
	omAnzChar.Add(4);
/*
	// ROTATION
	if (opCurrentColumns == "AACT3" || opCurrentColumns == "DACT3")
	{
		omAnzChar.Add(3);
	}
	if (opCurrentColumns == "AACT5" || opCurrentColumns == "DACT5")
	{
		omAnzChar.Add(5);
	}
	if (opCurrentColumns == "AREGN" || opCurrentColumns == "DREGN")
	{
		omAnzChar.Add(9);
	}
*/
}



CString DailyTableViewer::GetHeaderContent(CString opCurrentColumns)
{

	CString olFormat;

	// rotation
	if (opCurrentColumns == "RACT3")
	{
		return GetString(IDS_DRT_AC);
	}
	if (opCurrentColumns == "RREGN")
	{
		return GetString(IDS_DRT_REG);
	}

	// ARRIVAL
	if (opCurrentColumns == "AACT3")
	{
		return GetString(IDS_STRING311);
	}
	if (opCurrentColumns == "AACT5")
	{
		return GetString(IDS_STRING311);
	}
	if (opCurrentColumns == "AAIRB")
	{
		return GetString(IDS_DRT_AATD);
		return GetString(IDS_STRING321);
	}
	if (opCurrentColumns == "AAIRU")
	{
		//return GetString(IDS_STRING);
	}
	if (opCurrentColumns == "ACSGN")
	{
		return GetString(IDS_STRING297);
	}
	if (opCurrentColumns == "ASLOT")
	{
		return GetString(IDS_STRING320);
	}
	if (opCurrentColumns == "ADES3")
	{
		//return GetString(IDS_STRING);
	}
	if (opCurrentColumns == "ADES4")
	{
		return GetString(IDS_STRING315);
	}
	if (opCurrentColumns == "AETDC")
	{
		return GetString(IDS_STRING318);
	}
	if (opCurrentColumns == "AETAI")
	{
		return GetString(IDS_DRT_AETA);
		return GetString(IDS_STRING302);
	}
	if (opCurrentColumns == "AETDI")
	{
		return GetString(IDS_STRING317);
	}
	if (opCurrentColumns == "AFLNO")
	{
		return GetString(IDS_DRT_AFLNR);
		return GetString(IDS_STRING296);
	}
	if (opCurrentColumns == "AFTYP")
	{
		//return GetString(IDS_STRING);
	}
	if (opCurrentColumns == "AGTA1")
	{
		return GetString(IDS_STRING1774);
	}
	if (opCurrentColumns == "AGTA2")
	{
		//return GetString(IDS_STRING);
	}
	if (opCurrentColumns == "AGTD1")
	{
		return GetString(IDS_STRING322);
	}
	if (opCurrentColumns == "AGTD2")
	{
		return GetString(IDS_STRING322);
	}
	if (opCurrentColumns == "AIFRA")
	{
		return GetString(IDS_STRING300);
	}
	if (opCurrentColumns == "AIFRD")
	{
		return GetString(IDS_STRING300);
	}
	if (opCurrentColumns == "AISRE")
	{
		return GetString(IDS_STRING313);
	}
	if (opCurrentColumns == "ALAND")
	{
		return GetString(IDS_STRING304);
	}
	if (opCurrentColumns == "ALSTU")
	{
		//return GetString(IDS_STRING);
	}
	if (opCurrentColumns == "AOFBL")
	{
		return GetString(IDS_STRING319);
	}
	if (opCurrentColumns == "AONBE")
	{
		return GetString(IDS_STRING306);
	}
	if (opCurrentColumns == "AONBL")
	{
		return GetString(IDS_STRING307);
	}
	if (opCurrentColumns == "AORG3")
	{
		return GetString(IDS_DRT_AORG);
		return GetString(IDS_STRING298);
	}
	if (opCurrentColumns == "AORG4")
	{
		return GetString(IDS_STRING298);
	}
	if (opCurrentColumns == "APSTA")
	{
		return GetString(IDS_STRING308);
	}
	if (opCurrentColumns == "APSTD")
	{
		return GetString(IDS_STRING308);
	}
	if (opCurrentColumns == "AREGN")
	{
		return GetString(IDS_STRING310);
	}
	if (opCurrentColumns == "AREMP")
	{
		return GetString(IDS_STRING314);
	}
	if (opCurrentColumns == "ARKEY")
	{
		//return GetString(IDS_STRING);
	}
	if (opCurrentColumns == "ARWYA")
	{
		return GetString(IDS_STRING305);
	}
	if (opCurrentColumns == "ARWYD")
	{
		return GetString(IDS_STRING305);
	}
	if (opCurrentColumns == "ASTAB")
	{
		//return GetString(IDS_STRING);
	}
	if (opCurrentColumns == "ASTOATime")
	{
		return GetString(IDS_STRING323);
	}
	if (opCurrentColumns == "ASTOADate")
	{
		return GetString(IDS_DRT_ADATE);
		return GetString(IDS_STRING323);
	}
	if (opCurrentColumns == "ASTODTime")
	{
		return GetString(IDS_DRT_ASTA);
		return GetString(IDS_STRING316);
	}
	if (opCurrentColumns == "ASTODDate")
	{
		return GetString(IDS_DRT_ADATE);
		return GetString(IDS_STRING316);
	}
	if (opCurrentColumns == "ATIFA")
	{
		//return GetString(IDS_STRING);
	}
	if (opCurrentColumns == "ATIFD")
	{
		//return GetString(IDS_STRING);
	}
	if (opCurrentColumns == "ATMOA")
	{
		return GetString(IDS_DRT_ATMO);
		return GetString(IDS_STRING303);
	}
	if (opCurrentColumns == "AURNO")
	{
		//return GetString(IDS_STRING);
	}
	if (opCurrentColumns == "AUSEU")
	{
		//return GetString(IDS_STRING);
	}
	if (opCurrentColumns == "AVIA4")
	{
		//return GetString(IDS_STRING);
	}
	if (opCurrentColumns == "AVN")
	{
		return CString("NV");
	}
	if (opCurrentColumns == "ATTYP")
	{
		return GetString(IDS_DRT_ANA);
		return GetString(IDS_STRING301);
	}
	if (opCurrentColumns == "ACHGI")
	{
		//return GetString(IDS_STRING);
	}
	if (opCurrentColumns == "ARTYP")
	{
		//return GetString(IDS_STRING);
	}
	if (opCurrentColumns == "AADID")
	{
		//return GetString(IDS_STRING);
	}
	if (opCurrentColumns == "ANXTI")
	{
		//return GetString(IDS_STRING);
	}
	if (opCurrentColumns == "ABLT1")
	{
		//return GetString(IDS_STRING);
	}
	if (opCurrentColumns == "ABLT1/ABLT2")
	{
		return GetString(IDS_STRING1555);
	}

	if (opCurrentColumns == "APABA")
	{
		//return GetString(IDS_STRING);
	}
	if (opCurrentColumns == "APAEA")
	{
		//return GetString(IDS_STRING);
	}
	if (opCurrentColumns == "APDBA")
	{
		//return GetString(IDS_STRING);
	}
	if (opCurrentColumns == "APDEA")
	{
		//return GetString(IDS_STRING);
	}
	if (opCurrentColumns == "AMING")
	{
		//return GetString(IDS_STRING);
	}
	if (opCurrentColumns == "AADER")
	{
		//return GetString(IDS_STRING);
	}
	if (opCurrentColumns == "AVIA4")
	{
		return GetString(IDS_DRT_AVIA);
		return GetString(IDS_STRING299);
	}
	if (opCurrentColumns == "AVIAL")
	{
		return GetString(IDS_DRT_AVIA);
	}
	if (opCurrentColumns == "AVIA3L")
	{
		return GetString(IDS_DRT_AVIA);
	}
	if (opCurrentColumns == "AFLTI")
	{
		return GetString(IDS_DRT_AID);
	}
	if (opCurrentColumns == "ALAND/AONLB")
	{
		return GetString(IDS_DRT_AATA_AOBL);
	}
	if (opCurrentColumns == "APSTA/APSTD")
	{
		return GetString(IDS_DRT_POSAD);
	}
	
	// DEPARTURE
	if (opCurrentColumns == "DACT3")
	{
		return GetString(IDS_STRING311);
	}
	if (opCurrentColumns == "DACT5")
	{
		return GetString(IDS_STRING311);
	}
	if (opCurrentColumns == "DAIRB")
	{
		return GetString(IDS_STRING321);
	}
	if (opCurrentColumns == "DAIRU")
	{
		//return GetString(IDS_STRING);
	}
	if (opCurrentColumns == "DCSGN")
	{
		return GetString(IDS_STRING297);
	}
	if (opCurrentColumns == "DSLOT")
	{
		return GetString(IDS_STRING320);
	}
	if (opCurrentColumns == "DDES3")
	{
		//return GetString(IDS_STRING);
	}
	if (opCurrentColumns == "DDES4")
	{
		return GetString(IDS_STRING315);
	}
	if (opCurrentColumns == "DETDC")
	{
		return GetString(IDS_STRING318);
	}
	if (opCurrentColumns == "DETAI")
	{
		return GetString(IDS_STRING302);
	}
	if (opCurrentColumns == "DETDI")
	{
		return GetString(IDS_DRT_DETD);
		return GetString(IDS_STRING317);
	}
	if (opCurrentColumns == "DFLNO")
	{
		return GetString(IDS_DRT_DFLNR);
		return GetString(IDS_STRING296);
	}
	if (opCurrentColumns == "DFTYP")
	{
		//return GetString(IDS_STRING);
	}
	if (opCurrentColumns == "DGTA1")
	{
		return GetString(IDS_STRING1774);
	}
	if (opCurrentColumns == "DGTA2")
	{
		//return GetString(IDS_STRING);
	}
	if (opCurrentColumns == "DGTD1")
	{
		return GetString(IDS_STRING322);
	}
	if (opCurrentColumns == "DGTD2")
	{
		return GetString(IDS_STRING322);
	}
	if (opCurrentColumns == "DIFRA")
	{
		return GetString(IDS_STRING300);
	}
	if (opCurrentColumns == "DIFRD")
	{
		return GetString(IDS_STRING300);
	}
	if (opCurrentColumns == "DISRE")
	{
		return GetString(IDS_STRING313);
	}
	if (opCurrentColumns == "DLAND")
	{
		return GetString(IDS_STRING304);
	}
	if (opCurrentColumns == "DLSTU")
	{
		//return GetString(IDS_STRING);
	}
	if (opCurrentColumns == "DOFBL")
	{
		return GetString(IDS_STRING319);
	}
	if (opCurrentColumns == "DONBE")
	{
		return GetString(IDS_STRING306);
	}
	if (opCurrentColumns == "DONBL")
	{
		return GetString(IDS_STRING307);
	}
	if (opCurrentColumns == "DORG3")
	{
		//return GetString(IDS_STRING);
	}
	if (opCurrentColumns == "DORG4")
	{
		//return GetString(IDS_STRING);
	}
	if (opCurrentColumns == "DPSTA")
	{
		return GetString(IDS_STRING308);
	}
	if (opCurrentColumns == "DPSTD")
	{
		return GetString(IDS_STRING308);
	}
	if (opCurrentColumns == "DREGN")
	{
		return GetString(IDS_STRING310);
	}
	if (opCurrentColumns == "DREMP")
	{
		return GetString(IDS_STRING314);
	}
	if (opCurrentColumns == "DRKEY")
	{
		//return GetString(IDS_STRING);
	}
	if (opCurrentColumns == "DRWYA")
	{
		return GetString(IDS_STRING305);
	}
	if (opCurrentColumns == "DRWYD")
	{
		return GetString(IDS_STRING305);
	}
	if (opCurrentColumns == "DTAB")
	{
		//return GetString(IDS_STRING);
	}
	if (opCurrentColumns == "DSTOATime")
	{
		return GetString(IDS_STRING323);
	}
	if (opCurrentColumns == "DSTOADate")
	{
		return GetString(IDS_DRT_DDATE);
		return GetString(IDS_STRING323);
	}
	if (opCurrentColumns == "DSTODTime")
	{
		return GetString(IDS_DRT_DSTD);
		return GetString(IDS_STRING316);
	}
	if (opCurrentColumns == "DSTODDate")
	{
		return GetString(IDS_DRT_DDATE);
		return GetString(IDS_STRING316);
	}
	if (opCurrentColumns == "DTIFA")
	{
		//return GetString(IDS_STRING);
	}
	if (opCurrentColumns == "DTIFD")
	{
		//return GetString(IDS_STRING);
	}
	if (opCurrentColumns == "DTMOA")
	{
		return GetString(IDS_STRING303);
	}
	if (opCurrentColumns == "DURNO")
	{
		//return GetString(IDS_STRING);
	}
	if (opCurrentColumns == "DUSEU")
	{
		//return GetString(IDS_STRING);
	}
	if (opCurrentColumns == "DVIA4")
	{
		//return GetString(IDS_STRING);
	}
	if (opCurrentColumns == "DVN")
	{
		return CString("NV");
	}
	if (opCurrentColumns == "DTTYP")
	{
		return GetString(IDS_DRT_DNA);
		return GetString(IDS_STRING301);
	}
	if (opCurrentColumns == "DCHGI")
	{
		//return GetString(IDS_STRING);
	}
	if (opCurrentColumns == "DRTYP")
	{
		//return GetString(IDS_STRING);
	}
	if (opCurrentColumns == "DADID")
	{
		//return GetString(IDS_STRING);
	}
	if (opCurrentColumns == "DVIA4")
	{
		return GetString(IDS_DRT_DVIA);
		return GetString(IDS_STRING299);
	}
	if (opCurrentColumns == "DNXTI")
	{
		//return GetString(IDS_STRING);
	}
	if (opCurrentColumns == "DBLT1")
	{
		//return GetString(IDS_STRING);
	}
	if (opCurrentColumns == "DPABA")
	{
		//return GetString(IDS_STRING);
	}
	if (opCurrentColumns == "DPAEA")
	{
		//return GetString(IDS_STRING);
	}
	if (opCurrentColumns == "DPDBA")
	{
		//return GetString(IDS_STRING);
	}
	if (opCurrentColumns == "DPDEA")
	{
		//return GetString(IDS_STRING);
	}
	if (opCurrentColumns == "DMING")
	{
		//return GetString(IDS_STRING);
	}
	if (opCurrentColumns == "DDADER")
	{
		//return GetString(IDS_STRING);
	}
	if (opCurrentColumns == "DVIAL")
	{
		return GetString(IDS_DRT_DVIA);
	}
	if (opCurrentColumns == "DVIA3L")
	{
		return GetString(IDS_DRT_DVIA);
	}
	if (opCurrentColumns == "DCKIF/DCKIT")
	{
		return GetString(IDS_DRT_CKI);
		return GetString(IDS_STRING1553);
	}
	if (opCurrentColumns == "DBLT2")
	{
		return "";//GetString(IDS_STRING);
	}
	if (opCurrentColumns == "APSTA/DPSTD")
	{
		return GetString(IDS_DRT_POSAD);
		return GetString(IDS_STRING1554);
	}
	if (opCurrentColumns == "VIP")
	{
		return GetString(IDS_STRING1556);
	}
	if (opCurrentColumns == "AREMP/DREMP")
	{
		return GetString(IDS_STRING1557);
	}
	if (opCurrentColumns == "DFLTI")
	{
		return GetString(IDS_DRT_DID);
	}
	if (opCurrentColumns == "DDES3")
	{
		return GetString(IDS_DRT_DDEST);
	}
	if (opCurrentColumns == "DOFBL/DAIRB")
	{
		return GetString(IDS_DRT_DOFB_DATD);
	}
	if (opCurrentColumns == "DGTD1/DGTD2")
	{
		return GetString(IDS_DRT_GATE);
	}

/*	// ROTATION
	if (opCurrentColumns == "AACT3" || opCurrentColumns == "DACT3")
	{
		return GetString(IDS_DRT_AC);
		return GetString(IDS_STRING311);
	}
	if (opCurrentColumns == "AACT5" || opCurrentColumns == "DACT5")
	{
		return GetString(IDS_STRING311);
	}
	if (opCurrentColumns == "AREGN" || opCurrentColumns == "DREGN")
	{
		return GetString(IDS_DRT_REG);
		return GetString(IDS_STRING310);
	}
*/
	// wenn nix gefunden Leerstring zurueckgeben, sonst undefiniert
	return CString("");

}



bool DailyTableViewer::UtcToLocal(DAILYSCHEDULE_LINEDATA &rpLine)
{						   
	// Arrival
	ogBasicData.UtcToLocal(rpLine.AStoa);
	ogBasicData.UtcToLocal(rpLine.ATifa);

	if (bgRealLocal)
	{
		CedaAptLocalUtc::AptUtcToLocal(rpLine.AAirb, rpLine.AOrg4);
		CedaAptLocalUtc::AptUtcToLocal(rpLine.AAiru, rpLine.AOrg4);
	}
	else
	{
		ogBasicData.UtcToLocal(rpLine.AAirb);
		ogBasicData.UtcToLocal(rpLine.AAiru);
	}

	ogBasicData.UtcToLocal(rpLine.ASlot);
	ogBasicData.UtcToLocal(rpLine.AEtdc);
	ogBasicData.UtcToLocal(rpLine.AEtai);
	ogBasicData.UtcToLocal(rpLine.ALand);
	ogBasicData.UtcToLocal(rpLine.ALstu);
	ogBasicData.UtcToLocal(rpLine.AOnbe);
	ogBasicData.UtcToLocal(rpLine.AOnbl);
	ogBasicData.UtcToLocal(rpLine.ANxti);
	ogBasicData.UtcToLocal(rpLine.APaba);
	ogBasicData.UtcToLocal(rpLine.APaea);
	ogBasicData.UtcToLocal(rpLine.APdba);
	ogBasicData.UtcToLocal(rpLine.APdea);
	ogBasicData.UtcToLocal(rpLine.ATmoa);


	// Departure
	ogBasicData.UtcToLocal(rpLine.DStod);
	ogBasicData.UtcToLocal(rpLine.DTifd);
	ogBasicData.UtcToLocal(rpLine.DAirb);
	ogBasicData.UtcToLocal(rpLine.DAiru);
	ogBasicData.UtcToLocal(rpLine.DSlot);
	ogBasicData.UtcToLocal(rpLine.DEtdc);
	ogBasicData.UtcToLocal(rpLine.DEtdi);
	ogBasicData.UtcToLocal(rpLine.DLand);
	ogBasicData.UtcToLocal(rpLine.DLstu);
	ogBasicData.UtcToLocal(rpLine.DOfbl);
	ogBasicData.UtcToLocal(rpLine.DNxti);
	ogBasicData.UtcToLocal(rpLine.DPaba);
	ogBasicData.UtcToLocal(rpLine.DPaea);
	ogBasicData.UtcToLocal(rpLine.DPdba);
	ogBasicData.UtcToLocal(rpLine.DPdea);


	return true;
}

BOOL DailyTableViewer::CheckPostFlight(DAILYSCHEDULE_LINEDATA *prpTableLine)
{
	if (prpTableLine == NULL)
		return FALSE;

	DAILYFLIGHTDATA	*prlFlight = ogDailyCedaFlightData.GetFlightByUrno(prpTableLine->AUrno);
	if (prlFlight == NULL)
		prlFlight = ogDailyCedaFlightData.GetFlightByUrno(prpTableLine->DUrno);

	if (prlFlight == NULL)
		return FALSE;

	DAILYFLIGHTDATA *prlAFlight = ogDailyCedaFlightData.GetArrival(prlFlight);
	DAILYFLIGHTDATA *prlDFlight = ogDailyCedaFlightData.GetDeparture(prlFlight);

	BOOL blPost = FALSE;

	if (prlAFlight && prlDFlight)
	{
		if (prlAFlight->Tifa != TIMENULL && prlDFlight->Tifd != TIMENULL)
		{
			if (IsPostFlight(prlAFlight->Tifa) && IsPostFlight(prlDFlight->Tifd))
				blPost = TRUE;
		}
	}
	else if (!prlAFlight && prlDFlight)
	{
		if (prlDFlight->Tifd != TIMENULL)
		{
			if (IsPostFlight(prlDFlight->Tifd))
				blPost = TRUE;
		}
	}
	else if (prlAFlight && !prlDFlight)
	{
		if (prlAFlight->Tifa != TIMENULL)
		{
			if (IsPostFlight(prlAFlight->Tifa))
				blPost = TRUE;
		}
	}
	return blPost;
}
