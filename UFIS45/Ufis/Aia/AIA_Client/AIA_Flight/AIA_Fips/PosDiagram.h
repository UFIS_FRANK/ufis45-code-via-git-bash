#if !defined(AFX_POSDIAGRAM_H__DAC99481_8801_11D1_B43F_0000B45A33F5__INCLUDED_)
#define AFX_STAFFDIAGRAM_H__DAC99481_8801_11D1_B43F_0000B45A33F5__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// PosDiagram.h : header file
//
 
#include "PosDiaViewer.h"
#include "CCSClientWnd.h"
#include "CCS3dStatic.h"
#include "CCSButtonCtrl.h"
#include "CCSTimeScale.h"
#include "CViewer.h"

class DiffPosTableDlg;

/////////////////////////////////////////////////////////////////////////////
// PosDiagram frame

enum 
{
	POS_ZEITRAUM_PAGE,
	POS_FLUGSUCHEN_PAGE,
	POS_GEOMETRIE_PAGE
};

class PosChart;

class PosDiagram : public CFrameWnd
{
	DECLARE_DYNCREATE(PosDiagram)
public:
	PosDiagram();           // protected constructor used by dynamic creation
	~PosDiagram();

	void ActivateTimer();
	void DeActivateTimer();

	void RedisplayAll();

	// Time Band data, use the TIMENULL value for hiding the marker
	void SetTimeBand(CTime opStartTime, CTime opEndTime);
	void UpdateTimeBand();
 	void SetMarkTime(CTime opStartTime, CTime opEndTime);

	bool ShowFlight(long lpFlightUrno, bool bpArrival);
	bool ShowFlight(const DIAFLIGHTDATA *prpAFlight, const DIAFLIGHTDATA *prpDFlight);

 	void ViewSelChange();
	void ViewSelChange(char *pcpView);
	void GetCurView( char * pcpView);

	void UpdateGanttButtons();

	bool WithAcOnGround();

	bool bmRepaintAll;
	CCSButtonCtrl m_CB_Offline;
	CCSButtonCtrl m_CB_WoRes;
	CCSButtonCtrl m_CB_Changes;
    CTime omStartTime;
    CTimeSpan omDuration;

	void UpdateChangesButton(CString& opText, COLORREF opColor);
	void SaveToReg();

	CDialogBar omDialogBar;
	void UpdateWoResButton();

    CTime omTSStartTime;
    CTimeSpan omTSDuration;
    CTimeSpan omTSInterval;

private:

	bool UpdateDia();
	void ResetStateVariables();
    void PositionChild();
    void SetTSStartTime(CTime opTSStartTime);
	void UpdateComboBox();
	void ChangeViewTo(const char *pcpViewName,bool RememberPositions = true);
	void PrePlanMode(BOOL bpToSet,CTime opPrePlanTime);
	void ToggleOnline(void);
	CListBox *GetBottomMostGantt();
  
	void ShowTime(CTime opStart);

	void SetSaveButtonColor(COLORREF opCol);

	bool LoadFlights(const char *pspView = NULL);



//	CDialogBar omDialogBar;
	PosDiagramViewer omViewer;
	CString omOldWhere;

//    CCS3DStatic omTime;
//    CCS3DStatic omDate;
    CCS3DStatic omTSDate;

    CButton m_KlebeFkt;
    CCSTimeScale omTimeScale;
     
    CCSClientWnd omClientWnd;
    CStatusBar omStatusBar;
    
	PosChart *pomChart;
    int imFirstVisibleChart;
    int imStartTimeScalePos;
    
	CString omCaptionText;
	bool bmIsViewOpen;

	CCSButtonCtrl	m_CB_Save;
	CCSButtonCtrl	m_CB_PosCheck;


 	BOOL bmNoUpdatesNow;


 	CTime omTimeBandStartTime;
	CTime omTimeBandEndTime;

    CPoint omMaxTrackSize;
    CPoint omMinTrackSize;
    COLORREF lmBkColor;
    COLORREF lmTextColor;
    COLORREF lmHilightColor;

	bool bmWithAcOnGround;
	bool bmMessageDone;

	DiffPosTableDlg *pomDiffPosTableDlg;

	CString m_key;

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(PosDiagram)
	virtual BOOL DestroyWindow();
	//}}AFX_VIRTUAL

// Implementation

	// Generated message map functions
	//{{AFX_MSG(PosDiagram)
	afx_msg void OnPosCheck();
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnBeenden();
	afx_msg void OnDestroy();
	afx_msg void OnOffline();
	afx_msg void OnAnsicht();
	afx_msg void OnWoRes();
//	afx_msg void OnInsert();
	afx_msg void OnAllocate();
	afx_msg void OnClose();
    afx_msg void OnGetMinMaxInfo(MINMAXINFO FAR* lpMMI);
    afx_msg void OnPaint();
    afx_msg BOOL OnEraseBkgnd(CDC* pDC);
    afx_msg void OnSize(UINT nType, int cx, int cy);
    afx_msg void OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
    afx_msg void OnTimer(UINT nIDEvent);
    afx_msg LONG OnPositionChild(WPARAM wParam, LPARAM lParam);
    afx_msg LONG OnUpdateDiagram(WPARAM wParam, LPARAM lParam);
	afx_msg LONG RepaintAll(WPARAM wParam, LPARAM lParam);
	afx_msg void OnViewSelChange();
	afx_msg void OnCloseupView();
	afx_msg void OnZeit();
	afx_msg void OnSearch();
	afx_msg void OnDaily();
	afx_msg void OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg void OnKeyUp(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg void OnPrint();
	afx_msg void OnActivate(UINT nState, CWnd* pWndOther, BOOL bMinimized);
	afx_msg void OnCheckAcOnground();
	afx_msg void OnChanges();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.


#endif // !defined(AFX_STAFFDIAGRAM_H__DAC99481_8801_11D1_B43F_0000B45A33F5__INCLUDED_)
