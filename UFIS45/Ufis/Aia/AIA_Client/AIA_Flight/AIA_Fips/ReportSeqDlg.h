#ifndef AFX_REPORTSEQDLG_H__8BBC00F3_8B6C_11D1_8126_0000B43C4B01__INCLUDED_
#define AFX_REPORTSEQDLG_H__8BBC00F3_8B6C_11D1_8126_0000B43C4B01__INCLUDED_

// ReportSeqDlg.h : Header-Datei
//
#include "CCSEdit.h"

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld CReportSeqDlg 

class CReportSeqDlg : public CDialog
{
// Konstruktion
public:
	CReportSeqDlg(CWnd* pParent = NULL, CString opHeadline = "", CTime* oMinDate = NULL, CTime* oMaxDate = NULL);   

	CTime *pomMinDate;
	CTime *pomMaxDate;
// Dialogfelddaten
	//{{AFX_DATA(CReportSeqDlg)
	enum { IDD = IDD_REPORTSEQ };
	CCSEdit	m_CE_Datumvon;
	CCSEdit	m_CE_Datumbis;
	CString	m_Datumbis;
	CString	m_Datumvon;
	//}}AFX_DATA
	CString omHeadline;

// Überschreibungen
	// Vom Klassen-Assistenten generierte virtuelle Funktionsüberschreibungen
	//{{AFX_VIRTUAL(CReportSeqDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	//}}AFX_VIRTUAL

// Implementierung
protected:

	// Generierte Nachrichtenzuordnungsfunktionen
	//{{AFX_MSG(CReportSeqDlg)
	virtual void OnOK();
	virtual void OnCancel();
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio fügt zusätzliche Deklarationen unmittelbar vor der vorhergehenden Zeile ein.

#endif // AFX_REPORTSEQDLG_H__8BBC00F3_8B6C_11D1_8126_0000B43C4B01__INCLUDED_
