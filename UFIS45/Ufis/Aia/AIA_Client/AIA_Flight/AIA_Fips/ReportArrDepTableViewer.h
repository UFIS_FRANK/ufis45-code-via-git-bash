#ifndef __REPORT_ARR_DEP_TABLE_VIEWER_H__
#define __REPORT_ARR_DEP_TABLE_VIEWER_H__

#include "stdafx.h"
#include "RotationDlgCedaFlightData.h"
#include "ReportXXXCedaData.h"

#include "CCSTable.h"
#include "CViewer.h"
#include "CCSPrint.h"


struct SAMEARRDEPTABLE_LINEDATA
{
	long AUrno;
	long ARkey;
	CString		ABlt1;
	CString		ADate;
	CTime 		AEtai; 	
	CString		AFlno;
	CString		AGta1;
	CTime 		ALand; 	
	CString		AOrg3;
	CString		AOrg4;
	CString		APsta;
	CString		ARegn;
	CTime		AStoa;
	CString		ATtyp;
	CString		AVia3;
	CString 	AVian;
	CString		AAct;


	long DUrno;
	long DRkey;
	CString		DAct;
	CTime 		DAirb;
	CString		DDate;
	CString		DDes3;
	CString		DDes4;
	CTime 		DEtdi;
	CString		DFlno;
	CString		DGtd1;
	CString		DPstd;
	CString		DRegn;
	CTime		DStod;
	CString		DTtyp;
	CString		DVia3;
	CString 	DVian;
	CString 	DCkic;

	CString		Adid;

};

/////////////////////////////////////////////////////////////////////////////
// ReportArrDepTableViewer

class ReportArrDepTableViewer : public CViewer
{
// Constructions
public:
    ReportArrDepTableViewer(CCSPtrArray<ROTATIONDLGFLIGHTDATA> *popData, int ipArrDep,
		                    char *pcpInfo = NULL, char *pcpSelect = NULL);
    ~ReportArrDepTableViewer();

    void Attach(CCSTable *popAttachWnd);
	void SetParentDlg(CDialog* ppDlg);
    virtual void ChangeViewTo(const char *pcpViewName);
	void PrintPlanToFile(char *pcpDefPath);

// Internal data processing routines
private:
    void MakeLines();
	int  MakeLine(ROTATIONDLGFLIGHTDATA *prpAFlight, ROTATIONDLGFLIGHTDATA *prpDFlight);
	void MakeLineData(ROTATIONDLGFLIGHTDATA *prpAFlight, ROTATIONDLGFLIGHTDATA *prpDFlight, SAMEARRDEPTABLE_LINEDATA &rpLine);
	void MakeColList(SAMEARRDEPTABLE_LINEDATA *prlLine, CCSPtrArray<TABLE_COLUMN> &olColList);
	int  CompareGefundenefluege(SAMEARRDEPTABLE_LINEDATA *prpGefundenefluege1, SAMEARRDEPTABLE_LINEDATA *prpGefundenefluege2);
	int  CompareFlight(SAMEARRDEPTABLE_LINEDATA *prpFlight1, SAMEARRDEPTABLE_LINEDATA *prpFlight2);
	int  GetVia(CCSPtrArray<VIADATA> *opVias, ROTATIONDLGFLIGHTDATA *prpFlight);

// Operations
public:
	void DeleteAll();
	int CreateLine(SAMEARRDEPTABLE_LINEDATA &prpGefundenefluege);
	void DeleteLine(int ipLineno);
// Window refreshing routines
public:
	void UpdateDisplay();
	void DrawHeader();
	int GetFlightCount();


// Attributes used for filtering condition
private:
	bool bmIsFromSearch;

// Attributes
private:
    CCSTable *pomTable;
	CDialog* pomParentDlg;
	CCSPtrArray<ROTATIONDLGFLIGHTDATA> *pomData;
	int imArrDep;
	int imFlightCount;

public:
    CCSPtrArray<SAMEARRDEPTABLE_LINEDATA> omLines;
	CCSPtrArray<TABLE_HEADER_COLUMN> omHeaderDataArray;
//Print 
	void GetHeader(void); 
	void PrintTableView(void);
	bool PrintTableLine(SAMEARRDEPTABLE_LINEDATA *prpLine,bool bpLastLine);
	bool PrintTableHeader(void);
	CCSPtrArray <TABLE_HEADER_COLUMN> omPrintHeadHeaderArray;
	CCSPrint *pomPrint;
	CString omTableName;
	char *pcmInfo;
	char *pcmSelect;
	CString omFooterName;

};

#endif //__ReportArrDepTableViewer_H__
