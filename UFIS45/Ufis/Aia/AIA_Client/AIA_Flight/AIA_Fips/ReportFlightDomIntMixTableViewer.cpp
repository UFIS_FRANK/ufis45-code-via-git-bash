// ReportFlightDomIntMixTableViewer.cpp : implementation file
// 
// List off all flights per airline
// keine Angabe einer airline

#include "stdafx.h"
#include "ReportFlightDomIntMixTableViewer.h"
#include "CcsGlobl.h"
#include <iomanip.h>
#include <iostream.h>
#include <fstream.h>
#include "BasicData.h"
#include "resrc1.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif


/////////////////////////////////////////////////////////////////////////////
// ReportFlightDomIntMixTableViewer
//

static int CompareRotFlight(const ROTATIONDLGFLIGHTDATA **prpFlight1, const ROTATIONDLGFLIGHTDATA **prpFlight2)
{
	int	ilCompareResult;
	CTime olTime1;
	CTime olTime2;

	if((strcmp((**prpFlight1).Org3, pcgHome) != 0) && (strcmp((**prpFlight1).Des3, pcgHome) == 0))
	{
		olTime1 = (**prpFlight1).Tifa;
		if(olTime1 == TIMENULL)
			olTime1 = (**prpFlight1).Stoa;
	}
	else
	{
		olTime1 = (**prpFlight1).Tifd;
		if(olTime1 == TIMENULL)
			olTime1 = (**prpFlight1).Stod;
	}
	
	if((strcmp((**prpFlight2).Org3, pcgHome) != 0) && (strcmp((**prpFlight2).Des3, pcgHome) == 0))
	{
		olTime2 = (**prpFlight2).Tifa;
		if(olTime2 == TIMENULL)
			olTime2 = (**prpFlight2).Stoa;
	}
	else
	{
		olTime2 = (**prpFlight2).Tifd;
		if(olTime2 == TIMENULL)
			olTime2 = (**prpFlight2).Stod;
	}



	ilCompareResult = (olTime1 == olTime2)? 0:(olTime1 > olTime2)? 1: -1;
	

 return ilCompareResult;
}

ReportFlightDomIntMixTableViewer::ReportFlightDomIntMixTableViewer(CCSPtrArray<ROTATIONDLGFLIGHTDATA> *popData,
																		   char *pcpInfo,
																		   char *pcpSelect)
{

	pomData = popData;
	pcmInfo = pcpInfo;
	pcmSelect = pcpSelect;


	bmIsFromSearch = false;
    pomTable = NULL;

//	omShownColumns.Copy(opShownColumns);

}

ReportFlightDomIntMixTableViewer::~ReportFlightDomIntMixTableViewer()
{

	omPrintHeadHeaderArray.DeleteAll();
	omAnzChar.RemoveAll();
//	omShownColumns.RemoveAll();
    DeleteAll();

	omColumn0.RemoveAll();
	omColumn1.RemoveAll();
	omColumn2.RemoveAll();
	omColumn3.RemoveAll();
	omCountColumn1.RemoveAll();
	omCountColumn2.RemoveAll();
	omCountColumn3.RemoveAll();

}


void ReportFlightDomIntMixTableViewer::SetColumnsToShow(const CStringArray &opShownColumns)
{

	CCS_TRY

//	omShownColumns.Copy(opShownColumns);

	CCS_CATCH_ALL

}


void ReportFlightDomIntMixTableViewer::SetParentDlg(CDialog* ppParentDlg)
{

	pomParentDlg = ppParentDlg;

}


void ReportFlightDomIntMixTableViewer::Attach(CCSTable *popTable)
{
    pomTable = popTable;
}



void ReportFlightDomIntMixTableViewer::ChangeViewTo(const char *pcpViewName)
{

    pomTable->ResetContent();
    DeleteAll();    
    MakeLines();

	UpdateDisplay();
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
}


/////////////////////////////////////////////////////////////////////////////
// ReportFlightDomIntMixTableViewer -- code specific to this class

void ReportFlightDomIntMixTableViewer::MakeLines()
{

	ROTATIONDLGFLIGHTDATA *prlFlight;
	FLIGHT_DOM_INT_MIXED_LINEDATA rlLine;
	
	int ilAndf = 0;
	int ilAnif = 0;
	int ilAnmf = 0;
	int ilAnrf = 0;
	int ilDndf = 0;
	int ilDnif = 0;
	int ilDnmf = 0;
	int ilDnrf = 0;
	CString olCurrDate = "";
	CString olLastDate = "";
	CString olLastSortDate = "";
	CString olCurrSortDate = "";
	bool blRDeparture = false;

	int ilFlightCount = pomData->GetSize();

	pomData->Sort(CompareRotFlight);
	for (int ilLc = 0; ilLc < ilFlightCount; ilLc++)
	{
		prlFlight = &(*pomData)[ilLc];

		//Arrival
		if((strcmp(prlFlight->Org3, pcgHome) != 0) && (strcmp(prlFlight->Des3, pcgHome) == 0))
		{
			CTime olTifa1 = prlFlight->Tifa;
			if(olTifa1 == TIMENULL)
				olTifa1 = prlFlight->Stoa;

			if(bgReportLocal) 
				ogBasicData.UtcToLocal(olTifa1);

			olCurrDate = olTifa1.Format("%d.%m.%Y");
			olCurrSortDate = olTifa1.Format("%Y%m%d");
			if(olLastDate.IsEmpty())
			{
				olLastSortDate = olCurrSortDate;
				olLastDate = olCurrDate;
			}
			if(ilLc +1 == ilFlightCount)
			{
				olCurrDate = "";
			}

			if(olLastDate == olCurrDate )
			{
				if(*prlFlight->Flti == 'D')
				{
					ilAndf++;
				}
				if(*prlFlight->Flti == 'I')
				{
					ilAnif++;
				}
				if(*prlFlight->Flti == 'R')
				{
					ilAnrf++;
				}
				if(*prlFlight->Flti == 'M')
				{
					ilAnmf++;
				}
			}
			else
			{
				if(ilLc +1 == ilFlightCount)
				{
					if(*prlFlight->Flti == 'D')
					{
						ilAndf++;
					}
					if(*prlFlight->Flti == 'I')
					{
						ilAnif++;
					}
					if(*prlFlight->Flti == 'R')
					{
						ilAnrf++;
					}
					if(*prlFlight->Flti == 'M')
					{
						ilAnmf++;
					}
				}


				rlLine.Date = olLastDate;
				rlLine.SortDate = olLastSortDate;
				rlLine.Andf = ilAndf;
				rlLine.Anif = ilAnif;
				rlLine.Anrf = ilAnrf;
				rlLine.Anmf = ilAnmf;

				rlLine.Dndf = ilDndf;
				rlLine.Dnif = ilDnif;
				rlLine.Dnrf = ilDnrf;
				rlLine.Dnmf = ilDnmf;

				rlLine.Tndf = ilDndf + ilAndf;
				rlLine.Tnif = ilDnif + ilAnif;
				rlLine.Tnrf = ilDnrf + ilAnrf;
				rlLine.Tnmf = ilDnmf + ilAnmf;

				rlLine.Taf = ilAndf + ilAnif + ilAnrf + ilAnmf;
				rlLine.Tdf = ilDndf + ilDnif + ilDnrf + ilDnmf;
				rlLine.Ttf = rlLine.Tdf + rlLine.Taf;
			
				CreateLine(rlLine);
								
				ilAndf = 0;
				ilAnif = 0;
				ilAnrf = 0;
				ilAnmf = 0;

				ilDndf = 0;
				ilDnif = 0;
				ilDnrf = 0;
				ilDnmf = 0;

				if(*prlFlight->Flti == 'D')
				{
					ilAndf++;
				}
				if(*prlFlight->Flti == 'I')
				{
					ilAnif++;
				}
				if(*prlFlight->Flti == 'R')
				{
					ilAnrf++;
				}
				if(*prlFlight->Flti == 'M')
				{
					ilAnmf++;
				}
				olLastDate = olCurrDate;
				olLastSortDate = olCurrSortDate;
			}
		}
		else
		{
			// Departure
			if((strcmp(prlFlight->Org3, pcgHome) == 0) && (strcmp(prlFlight->Des3, pcgHome) != 0))
			{
				CTime olTifa1 = prlFlight->Tifd;
				if(olTifa1 == TIMENULL)
					olTifa1 = prlFlight->Stod;

				if(bgReportLocal) 
					ogBasicData.UtcToLocal(olTifa1);

				olCurrDate = olTifa1.Format("%d.%m.%Y");
				olCurrSortDate = olTifa1.Format("%Y%m%d");
				if(olLastDate.IsEmpty())
				{
					olLastSortDate = olCurrSortDate;
					olLastDate = olCurrDate;
				}
				if(ilLc +1 == ilFlightCount)
				{
					olCurrDate = "";
				}

				if(olLastDate == olCurrDate)
				{
					if(*prlFlight->Flti == 'D')
					{
						ilDndf++;
					}
					if(*prlFlight->Flti == 'I')
					{
						ilDnif++;
					}
					if(*prlFlight->Flti == 'R')
					{
						ilDnrf++;
					}
					if(*prlFlight->Flti == 'M')
					{
						ilDnmf++;
					}
				}
				else
				{
					if(ilLc +1 == ilFlightCount)
					{
						if(*prlFlight->Flti == 'D')
						{
							ilDndf++;
						}
						if(*prlFlight->Flti == 'I')
						{
							ilDnif++;
						}
						if(*prlFlight->Flti == 'R')
						{
							ilDnrf++;
						}
						if(*prlFlight->Flti == 'M')
						{
							ilDnmf++;
						}
					}

					rlLine.Date = olLastDate;
					rlLine.SortDate = olLastSortDate;
					rlLine.Andf = ilAndf;
					rlLine.Anif = ilAnif;
					rlLine.Anrf = ilAnrf;
					rlLine.Anmf = ilAnmf;

					rlLine.Dndf = ilDndf;
					rlLine.Dnif = ilDnif;
					rlLine.Dnrf = ilDnrf;
					rlLine.Dnmf = ilDnmf;

					rlLine.Tndf = ilDndf + ilAndf;
					rlLine.Tnif = ilDnif + ilAnif;
					rlLine.Tnrf = ilDnrf + ilAnrf;
					rlLine.Tnmf = ilDnmf + ilAnmf;
			
					rlLine.Taf = ilAndf + ilAnif + ilAnrf + ilAnmf;
					rlLine.Tdf = ilDndf + ilDnif + ilDnrf + ilDnmf;
					rlLine.Ttf = rlLine.Tdf + rlLine.Taf;

					CreateLine(rlLine);
									
					ilAndf = 0;
					ilAnif = 0;
					ilAnrf = 0;
					ilAnmf = 0;

					ilDndf = 0;
					ilDnif = 0;
					ilDnrf = 0;
					ilDnmf = 0;

					if(*prlFlight->Flti == 'D')
					{
						ilDndf++;
					}
					if(*prlFlight->Flti == 'I')
					{
						ilDnif++;
					}
					if(*prlFlight->Flti == 'R')
					{
						ilDnrf++;
					}
					if(*prlFlight->Flti == 'M')
					{
						ilDnmf++;
					}
					olLastSortDate = olCurrSortDate;
					olLastDate = olCurrDate;
				}
				
			}
		}
		
	}

	ilAndf = 0;
	ilAnif = 0;
	ilAnmf = 0;
	ilAnrf = 0;
	ilDndf = 0;
	ilDnif = 0;
	ilDnmf = 0;
	ilDnrf = 0;

	int ilLineCount = omLines.GetSize();
	for (int illc1 = 0; illc1 < ilLineCount; illc1++)
	{
		ilAndf +=omLines[illc1].Andf;
		ilAnif +=omLines[illc1].Anif;
		ilAnmf +=omLines[illc1].Anmf;
		ilAnrf +=omLines[illc1].Anrf;
	
		ilDndf +=omLines[illc1].Dndf;
		ilDnif +=omLines[illc1].Dnif;
		ilDnmf +=omLines[illc1].Dnmf;
		ilDnrf +=omLines[illc1].Dnrf;
	}

	rlLine.Date = "*Total";
	rlLine.SortDate = "9*Total";
	rlLine.Andf = ilAndf;
	rlLine.Anif = ilAnif;
	rlLine.Anrf = ilAnrf;
	rlLine.Anmf = ilAnmf;

	rlLine.Dndf = ilDndf;
	rlLine.Dnif = ilDnif;
	rlLine.Dnrf = ilDnrf;
	rlLine.Dnmf = ilDnmf;

	rlLine.Tndf = ilDndf + ilAndf;
	rlLine.Tnif = ilDnif + ilAnif;
	rlLine.Tnrf = ilDnrf + ilAnrf;
	rlLine.Tnmf = ilDnmf + ilAnmf;

	rlLine.Taf = ilAndf + ilAnif + ilAnrf + ilAnmf;
	rlLine.Tdf = ilDndf + ilDnif + ilDnrf + ilDnmf;
	rlLine.Ttf = rlLine.Tdf + rlLine.Taf;


	CreateLine(rlLine);

}

		





int ReportFlightDomIntMixTableViewer::CreateLine(FLIGHT_DOM_INT_MIXED_LINEDATA &rpLine)
{
    int ilLineCount = omLines.GetSize();

    for (int ilLineno = ilLineCount; ilLineno > 0; ilLineno--)
        if (CompareFlight(&rpLine, &omLines[ilLineno-1]) >= 0)
            break;  // should be inserted after Lines[ilLineno-1]

    omLines.NewAt(ilLineno, rpLine);
	return ilLineno;
}


void ReportFlightDomIntMixTableViewer::DeleteLine(int ipLineno)
{
	omLines.DeleteAt(ipLineno);
}



void ReportFlightDomIntMixTableViewer::DeleteAll()
{
    while (omLines.GetSize() > 0)
        DeleteLine(0);
}


/////////////////////////////////////////////////////////////////////////////
// ReportFlightDomIntMixTableViewer - display drawing routine



// UpdateDisplay: Load data selected by filter conditions to the display by using "omTable"
void ReportFlightDomIntMixTableViewer::UpdateDisplay()
{

	CCS_TRY

	pomTable->ResetContent();

	FLIGHT_DOM_INT_MIXED_LINEDATA *prlLine;

	// Anzahl an Zeichen zur Berechnung der Spaltenbreite ermitteln

	DrawHeader();
  
	for (int ilLc = 0; ilLc < omLines.GetSize(); ilLc++)
	{
		int ilColumnNo = 0;
		prlLine = &omLines[ilLc];
		CCSPtrArray<TABLE_COLUMN> olColList;
	
		MakeColList(prlLine, olColList);

		pomTable->AddTextLine(olColList, (void*)(prlLine));
		olColList.DeleteAll();
	}


    pomTable->DisplayTable();

	CCS_CATCH_ALL

}

void ReportFlightDomIntMixTableViewer::MakeColList(FLIGHT_DOM_INT_MIXED_LINEDATA *prlLine, CCSPtrArray<TABLE_COLUMN> &olColList)
{

	TABLE_COLUMN rlColumnData;
	rlColumnData.VerticalSeparator = SEPA_NONE;
	rlColumnData.SeparatorType = SEPA_NONE;
	rlColumnData.Font = &ogCourier_Regular_9;


	rlColumnData.Alignment = COLALIGN_LEFT;

	rlColumnData.Text = prlLine->Date;
	olColList.NewAt(olColList.GetSize(), rlColumnData);


	rlColumnData.Alignment = COLALIGN_LEFT;

	rlColumnData.Text.Format( "%d",prlLine->Andf);
	olColList.NewAt(olColList.GetSize(), rlColumnData);

	rlColumnData.Text.Format( "%d",prlLine->Dndf);
	olColList.NewAt(olColList.GetSize(), rlColumnData);

	rlColumnData.Text.Format( "%d",prlLine->Tndf);
	olColList.NewAt(olColList.GetSize(), rlColumnData);

	rlColumnData.Alignment = COLALIGN_LEFT;

	rlColumnData.Text.Format( "%d",prlLine->Anif);
	olColList.NewAt(olColList.GetSize(), rlColumnData);

	rlColumnData.Text.Format( "%d",prlLine->Dnif);
	olColList.NewAt(olColList.GetSize(), rlColumnData);

	rlColumnData.Text.Format( "%d",prlLine->Tnif);
	olColList.NewAt(olColList.GetSize(), rlColumnData);

	rlColumnData.Alignment = COLALIGN_LEFT;

	rlColumnData.Text.Format( "%d",prlLine->Anrf);
	olColList.NewAt(olColList.GetSize(), rlColumnData);

	rlColumnData.Text.Format( "%d",prlLine->Dnrf);
	olColList.NewAt(olColList.GetSize(), rlColumnData);

	rlColumnData.Text.Format( "%d",prlLine->Tnrf);
	olColList.NewAt(olColList.GetSize(), rlColumnData);

	rlColumnData.Alignment = COLALIGN_LEFT;

	rlColumnData.Text.Format( "%d",prlLine->Anmf);
	olColList.NewAt(olColList.GetSize(), rlColumnData);

	rlColumnData.Text.Format( "%d",prlLine->Dnmf);
	olColList.NewAt(olColList.GetSize(), rlColumnData);

	rlColumnData.Text.Format( "%d",prlLine->Tnmf);
	olColList.NewAt(olColList.GetSize(), rlColumnData);

	rlColumnData.Text.Format( "%d",prlLine->Taf);
	olColList.NewAt(olColList.GetSize(), rlColumnData);

	rlColumnData.Text.Format( "%d",prlLine->Tdf);
	olColList.NewAt(olColList.GetSize(), rlColumnData);

	rlColumnData.Text.Format( "%d",prlLine->Ttf);
	olColList.NewAt(olColList.GetSize(), rlColumnData);




}



void ReportFlightDomIntMixTableViewer::DrawHeader()
{
	int  ilTotalLines = 0;
	bool blNewLogicLine = false;

	CCSPtrArray <TABLE_HEADER_COLUMN> omHeaderDataArray;

	pomTable->SetShowSelection(true);
	pomTable->ResetContent();
	
	TABLE_HEADER_COLUMN *prlHeader[16];


	prlHeader[0] = new TABLE_HEADER_COLUMN;
	prlHeader[0]->Alignment = COLALIGN_CENTER;
	prlHeader[0]->Length = 80; 
	prlHeader[0]->Font = &ogCourier_Bold_10;
	prlHeader[0]->Text = GetString(IDS_STRING332);//Date

	prlHeader[1] = new TABLE_HEADER_COLUMN;
	prlHeader[1]->Alignment = COLALIGN_CENTER;
	prlHeader[1]->Length = 50;
	prlHeader[1]->Font = &ogCourier_Bold_10;
	prlHeader[1]->Text = GetString(IDS_STRING1782);//Dom. Arr

	prlHeader[2] = new TABLE_HEADER_COLUMN;
	prlHeader[2]->Alignment = COLALIGN_CENTER;
	prlHeader[2]->Length = 50; 
	prlHeader[2]->Font = &ogCourier_Bold_10;
	prlHeader[2]->Text = GetString(IDS_STRING1783);//Dom. Dep.

	prlHeader[3] = new TABLE_HEADER_COLUMN;
	prlHeader[3]->Alignment = COLALIGN_CENTER;
	prlHeader[3]->Length = 50; 
	prlHeader[3]->Font = &ogCourier_Bold_10;
	prlHeader[3]->Text = GetString(IDS_STRING1784);// Dom Tot
	prlHeader[3]->FrameRight  = PRINT_FRAMETHIN;

	prlHeader[4] = new TABLE_HEADER_COLUMN;
	prlHeader[4]->Alignment = COLALIGN_CENTER;
	prlHeader[4]->Length = 50; 
	prlHeader[4]->Font = &ogCourier_Bold_10;
	prlHeader[4]->Text = GetString(IDS_STRING1785);// Int. Arr.

	prlHeader[5] = new TABLE_HEADER_COLUMN;
	prlHeader[5]->Alignment = COLALIGN_CENTER;
	prlHeader[5]->Length = 50; 
	prlHeader[5]->Font = &ogCourier_Bold_10;
	prlHeader[5]->Text = GetString(IDS_STRING1786);// Int. Dep

	prlHeader[6] = new TABLE_HEADER_COLUMN;
	prlHeader[6]->Alignment = COLALIGN_CENTER;
	prlHeader[6]->Length = 50; 
	prlHeader[6]->Font = &ogCourier_Bold_10;
	prlHeader[6]->Text = GetString(IDS_STRING1787);// Int. Tot
	prlHeader[6]->FrameRight  = PRINT_FRAMETHIN;

	prlHeader[7] = new TABLE_HEADER_COLUMN;
	prlHeader[7]->Alignment = COLALIGN_CENTER;
	prlHeader[7]->Length = 50; 
	prlHeader[7]->Font = &ogCourier_Bold_10;
	prlHeader[7]->Text = GetString(IDS_STRING1788);// Reg Arr

	prlHeader[8] = new TABLE_HEADER_COLUMN;
	prlHeader[8]->Alignment = COLALIGN_CENTER;
	prlHeader[8]->Length = 50; 
	prlHeader[8]->Font = &ogCourier_Bold_10;
	prlHeader[8]->Text = GetString(IDS_STRING1789);// Reg Dep

	prlHeader[9] = new TABLE_HEADER_COLUMN;
	prlHeader[9]->Alignment = COLALIGN_CENTER;
	prlHeader[9]->Length = 50; 
	prlHeader[9]->Font = &ogCourier_Bold_10;
	prlHeader[9]->Text = GetString(IDS_STRING1790);// Reg Tot
	prlHeader[9]->FrameRight  = PRINT_FRAMETHIN;

	prlHeader[10] = new TABLE_HEADER_COLUMN;
	prlHeader[10]->Alignment = COLALIGN_CENTER;
	prlHeader[10]->Length = 50; 
	prlHeader[10]->Font = &ogCourier_Bold_10;
	prlHeader[10]->Text = GetString(IDS_STRING1791);// Com Arr

	prlHeader[11] = new TABLE_HEADER_COLUMN;
	prlHeader[11]->Alignment = COLALIGN_CENTER;
	prlHeader[11]->Length = 50; 
	prlHeader[11]->Font = &ogCourier_Bold_10;
	prlHeader[11]->Text = GetString(IDS_STRING1792);// Comb Dep

	prlHeader[12] = new TABLE_HEADER_COLUMN;
	prlHeader[12]->Alignment = COLALIGN_CENTER;
	prlHeader[12]->Length = 50; 
	prlHeader[12]->Font = &ogCourier_Bold_10;
	prlHeader[12]->Text = GetString(IDS_STRING1793);// Comb Tot
	prlHeader[12]->FrameRight  = PRINT_FRAMETHIN;

	prlHeader[13] = new TABLE_HEADER_COLUMN;
	prlHeader[13]->Alignment = COLALIGN_CENTER;
	prlHeader[13]->Length = 50; 
	prlHeader[13]->Font = &ogCourier_Bold_10;
	prlHeader[13]->Text = GetString(IDS_STRING1794);// Arr. Tot

	prlHeader[14] = new TABLE_HEADER_COLUMN;
	prlHeader[14]->Alignment = COLALIGN_CENTER;
	prlHeader[14]->Length = 50; 
	prlHeader[14]->Font = &ogCourier_Bold_10;
	prlHeader[14]->Text = GetString(IDS_STRING1795);// Dep. Tot

	prlHeader[15] = new TABLE_HEADER_COLUMN;
	prlHeader[15]->Alignment = COLALIGN_CENTER;
	prlHeader[15]->Length = 50; 
	prlHeader[15]->Font = &ogCourier_Bold_10;
	prlHeader[15]->Text = GetString(IDS_STRING1796);// Total Total
	prlHeader[15]->FrameRight  = PRINT_FRAMETHIN;


	for(int ili = 0; ili < 16; ili++)
	{
		omHeaderDataArray.Add( prlHeader[ili]);
	}

	pomTable->SetHeaderFields(omHeaderDataArray);
	omHeaderDataArray.DeleteAll();

	pomTable->SetDefaultSeparator();

}






int ReportFlightDomIntMixTableViewer::CompareFlight(FLIGHT_DOM_INT_MIXED_LINEDATA *prpLine1, FLIGHT_DOM_INT_MIXED_LINEDATA *prpLine2)
{
	int	ilCompareResult;

		ilCompareResult = (prpLine1->SortDate == prpLine2->SortDate)? 0:
			(prpLine1->SortDate > prpLine2->SortDate)? 1: -1;

		 return ilCompareResult;
}

void ReportFlightDomIntMixTableViewer::GetHeader()
{
		
	omPrintHeadHeaderArray.DeleteAll();  // BWi

	TABLE_HEADER_COLUMN *prlHeader[16];

	prlHeader[0] = new TABLE_HEADER_COLUMN;
	prlHeader[0]->Alignment = COLALIGN_CENTER;
	prlHeader[0]->Length = 70; 
	prlHeader[0]->Font = &ogCourier_Bold_10;
	prlHeader[0]->Text = GetString(IDS_STRING332);//Date

	prlHeader[1] = new TABLE_HEADER_COLUMN;
	prlHeader[1]->Alignment = COLALIGN_CENTER;
	prlHeader[1]->Length = 60;
	prlHeader[1]->Font = &ogCourier_Bold_10;
	prlHeader[1]->Text = GetString(IDS_STRING1782);//Dom. Arr

	prlHeader[2] = new TABLE_HEADER_COLUMN;
	prlHeader[2]->Alignment = COLALIGN_CENTER;
	prlHeader[2]->Length = 60; 
	prlHeader[2]->Font = &ogCourier_Bold_10;
	prlHeader[2]->Text = GetString(IDS_STRING1783);//Dom. Dep.

	prlHeader[3] = new TABLE_HEADER_COLUMN;
	prlHeader[3]->Alignment = COLALIGN_CENTER;
	prlHeader[3]->Length = 60; 
	prlHeader[3]->Font = &ogCourier_Bold_10;
	prlHeader[3]->Text = GetString(IDS_STRING1784);// Dom Tot
	prlHeader[3]->FrameRight  = PRINT_FRAMETHIN;

	prlHeader[4] = new TABLE_HEADER_COLUMN;
	prlHeader[4]->Alignment = COLALIGN_CENTER;
	prlHeader[4]->Length = 60; 
	prlHeader[4]->Font = &ogCourier_Bold_10;
	prlHeader[4]->Text = GetString(IDS_STRING1785);// Int. Arr.

	prlHeader[5] = new TABLE_HEADER_COLUMN;
	prlHeader[5]->Alignment = COLALIGN_CENTER;
	prlHeader[5]->Length = 60; 
	prlHeader[5]->Font = &ogCourier_Bold_10;
	prlHeader[5]->Text = GetString(IDS_STRING1786);// Int. Dep

	prlHeader[6] = new TABLE_HEADER_COLUMN;
	prlHeader[6]->Alignment = COLALIGN_CENTER;
	prlHeader[6]->Length = 60; 
	prlHeader[6]->Font = &ogCourier_Bold_10;
	prlHeader[6]->Text = GetString(IDS_STRING1787);// Int. Tot
	prlHeader[6]->FrameRight  = PRINT_FRAMETHIN;

	prlHeader[7] = new TABLE_HEADER_COLUMN;
	prlHeader[7]->Alignment = COLALIGN_CENTER;
	prlHeader[7]->Length = 60; 
	prlHeader[7]->Font = &ogCourier_Bold_10;
	prlHeader[7]->Text = GetString(IDS_STRING1788);// Reg Arr

	prlHeader[8] = new TABLE_HEADER_COLUMN;
	prlHeader[8]->Alignment = COLALIGN_CENTER;
	prlHeader[8]->Length = 60; 
	prlHeader[8]->Font = &ogCourier_Bold_10;
	prlHeader[8]->Text = GetString(IDS_STRING1789);// Reg Dep

	prlHeader[9] = new TABLE_HEADER_COLUMN;
	prlHeader[9]->Alignment = COLALIGN_CENTER;
	prlHeader[9]->Length = 60; 
	prlHeader[9]->Font = &ogCourier_Bold_10;
	prlHeader[9]->Text = GetString(IDS_STRING1790);// Reg Tot
	prlHeader[9]->FrameRight  = PRINT_FRAMETHIN;

	prlHeader[10] = new TABLE_HEADER_COLUMN;
	prlHeader[10]->Alignment = COLALIGN_CENTER;
	prlHeader[10]->Length = 60; 
	prlHeader[10]->Font = &ogCourier_Bold_10;
	prlHeader[10]->Text = GetString(IDS_STRING1791);// Com Arr

	prlHeader[11] = new TABLE_HEADER_COLUMN;
	prlHeader[11]->Alignment = COLALIGN_CENTER;
	prlHeader[11]->Length = 60; 
	prlHeader[11]->Font = &ogCourier_Bold_10;
	prlHeader[11]->Text = GetString(IDS_STRING1792);// Comb Dep

	prlHeader[12] = new TABLE_HEADER_COLUMN;
	prlHeader[12]->Alignment = COLALIGN_CENTER;
	prlHeader[12]->Length = 60; 
	prlHeader[12]->Font = &ogCourier_Bold_10;
	prlHeader[12]->Text = GetString(IDS_STRING1793);// Comb Tot
	prlHeader[12]->FrameRight  = PRINT_FRAMETHIN;

	prlHeader[13] = new TABLE_HEADER_COLUMN;
	prlHeader[13]->Alignment = COLALIGN_CENTER;
	prlHeader[13]->Length = 50; 
	prlHeader[13]->Font = &ogCourier_Bold_10;
	prlHeader[13]->Text = GetString(IDS_STRING1794);// Arr. Tot

	prlHeader[14] = new TABLE_HEADER_COLUMN;
	prlHeader[14]->Alignment = COLALIGN_CENTER;
	prlHeader[14]->Length = 60; 
	prlHeader[14]->Font = &ogCourier_Bold_10;
	prlHeader[14]->Text = GetString(IDS_STRING1795);// Dep. Tot

	prlHeader[15] = new TABLE_HEADER_COLUMN;
	prlHeader[15]->Alignment = COLALIGN_CENTER;
	prlHeader[15]->Length = 60; 
	prlHeader[15]->Font = &ogCourier_Bold_10;
	prlHeader[15]->Text = GetString(IDS_STRING1796);// Total Total
	prlHeader[15]->FrameRight  = PRINT_FRAMETHIN;


	for(int ili = 0; ili < 16; ili++)
	{
		omPrintHeadHeaderArray.Add( prlHeader[ili]);
	}

}





//-----------------------------------------------------------------------------------------------
// Printing Routines
//-----------------------------------------------------------------------------------------------
void ReportFlightDomIntMixTableViewer::PrintTableView()
{
	CString olFooter1,olFooter2;
	char pclFooter[256];

	sprintf(pclFooter, "", (CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M")));

	omFooterName = pclFooter;
	CString olTableName = "";
	int ilOrientation = PRINT_LANDSCAPE;//  PRINT_PORTRAET  PRINT_LANDSCAPE
	pomPrint = new CCSPrint(pomParentDlg,ilOrientation,45);

	if (pomPrint != NULL)
	{
		if (pomPrint->InitializePrinter(ilOrientation) == TRUE)
		{
			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
			pomPrint->imLineNo = pomPrint->imMaxLines + 1;
			DOCINFO	rlDocInfo;
			memset(&rlDocInfo, 0, sizeof(DOCINFO));
			rlDocInfo.cbSize = sizeof( DOCINFO );
			rlDocInfo.lpszDocName = olTableName;	
			//rlDocInfo.lpszDocName = TABLENAME;	
			pomPrint->omCdc.StartDoc( &rlDocInfo );
			pomPrint->imPageNo = 0;
			int ilLines = omLines.GetSize();
			olFooter1.Format(GetString(IDS_STRING1445), ilLines,omFooterName);

			GetHeader();

			if (ilLines == 0)
				PrintTableHeader(); // Drucken leerer Tabelle
			for(int i = 0; i < ilLines; i++ ) 
			{
				if(pomPrint->imLineNo >= pomPrint->imMaxLines)
				{
					if(pomPrint->imPageNo > 0)
					{
						olFooter2.Format(GetString(IDS_STRING1198),pomPrint->imPageNo);
						pomPrint->PrintUIFFooter(olFooter1,"",olFooter2);
						pomPrint->omCdc.EndPage();
					}
					PrintTableHeader();
				}
				if(pomPrint->imLineNo == (pomPrint->imMaxLines-1) || i == (ilLines-1))
				{
					PrintTableLine(&omLines[i],true);
				}
				else
				{
					PrintTableLine(&omLines[i],false);
				}
			}
			olFooter2.Format(GetString(IDS_STRING1198),pomPrint->imPageNo);
			pomPrint->PrintUIFFooter(olFooter1,"",olFooter2);
			pomPrint->omCdc.EndPage();
			pomPrint->omCdc.EndDoc();
		}

		omPrintHeadHeaderArray.DeleteAll();
		SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
		delete pomPrint;
		pomPrint = NULL;
	}
}

//-----------------------------------------------------------------------------------------------

bool ReportFlightDomIntMixTableViewer::PrintTableHeader()
{
	pomPrint->omCdc.StartPage();
	pomPrint->imPageNo++;
	pomPrint->imLineNo = 0;
	double dgCCSPrintFactor = 2.7 ;
	
	CString olTableName;

	pomParentDlg->GetWindowText(olTableName);
	
	pomPrint->PrintUIFHeader(omTableName,olTableName,pomPrint->imFirstLine-10);

	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;

	rlElement.Alignment   = PRINT_CENTER;
	rlElement.FrameLeft   = PRINT_NOFRAME;
	rlElement.FrameRight  = PRINT_NOFRAME;
	rlElement.FrameTop    = PRINT_NOFRAME;
	rlElement.FrameBottom = PRINT_FRAMEMEDIUM;
	rlElement.pFont       = &pomPrint->ogCourierNew_Bold_8;

	int ilSize = omPrintHeadHeaderArray.GetSize();

	for(int i = 0; i < ilSize; i++)
	{
		//rlElement.Length = (int)(omHeaderDataArray[i].Length*dgCCSPrintFactor);
		rlElement.Length = (int)(omPrintHeadHeaderArray[i].Length*dgCCSPrintFactor);
		if(rlElement.Length < igCCSPrintMinLength)rlElement.Length+=igCCSPrintMoreLength; 
		//rlElement.Text   = omHeaderDataArray[i].Text;
		rlElement.Text   = omPrintHeadHeaderArray[i].Text;
		rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
	}
	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();


	return true;
}

//-----------------------------------------------------------------------------------------------

bool ReportFlightDomIntMixTableViewer::PrintTableLine(FLIGHT_DOM_INT_MIXED_LINEDATA *prpLine,bool bpLastLine)
{
	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;
	double dgCCSPrintFactor = 2.7 ;
	rlElement.pFont      = &pomPrint->ogCourierNew_Regular_8;
	int ilSize = omPrintHeadHeaderArray.GetSize();

	for(int i=0;i<ilSize;i++)
	{
		rlElement.Length = (int)(omPrintHeadHeaderArray[i].Length*dgCCSPrintFactor);
		if(rlElement.Length < igCCSPrintMinLength)rlElement.Length+=igCCSPrintMoreLength; 
		rlElement.Alignment  = PRINT_CENTER;
		rlElement.FrameTop   = PRINT_FRAMETHIN;
		if(bpLastLine == true)
		{
			rlElement.FrameBottom = PRINT_FRAMETHIN;
		}
		else
		{
			rlElement.FrameBottom = PRINT_NOFRAME;
		}
		rlElement.FrameLeft  = PRINT_NOFRAME;
		rlElement.FrameRight = PRINT_NOFRAME;

		switch(i)
		{
		
			case 0:
			{
					rlElement.FrameLeft  = PRINT_FRAMETHIN;
					rlElement.FrameRight = PRINT_FRAMETHIN;
					rlElement.Text = prpLine->Date;
			}
			break;
			case 1:
			{

					rlElement.Text.Format("%d",prpLine->Andf);
			}
			break;
			case 2:
			{

					rlElement.Text.Format("%d",prpLine->Dndf);
			}
			break;
			case 3:
			{
					rlElement.FrameRight  = PRINT_FRAMETHIN;
					rlElement.Text.Format("%d",prpLine->Tndf);
			}
			break;
			case 4:
			{

					rlElement.Text.Format("%d",prpLine->Anif);
			}
			break;
			case 5:
			{

					rlElement.Text.Format("%d",prpLine->Dnif);
			}
			break;
			case 6:
			{
					rlElement.FrameRight  = PRINT_FRAMETHIN;
					rlElement.Text.Format("%d",prpLine->Tnif);
			}
			break;
			case 7:
			{
					rlElement.Text.Format("%d",prpLine->Anrf);
			}
			break;
			case 8:
			{

					rlElement.Text.Format("%d",prpLine->Dnrf);
			}
			break;
			case 9:
			{

					rlElement.FrameRight  = PRINT_FRAMETHIN;
					rlElement.Text.Format("%d",prpLine->Tnrf);
			}
			break;
			case 10:
			{

					rlElement.Text.Format("%d",prpLine->Anmf);
			}
			break;
			case 11:
			{

					rlElement.Text.Format("%d",prpLine->Dnmf);
			}
			break;
			case 12:
			{

					rlElement.FrameRight  = PRINT_FRAMETHIN;
					rlElement.Text.Format("%d",prpLine->Tnmf);
			}
			break;
			case 13:
			{

					rlElement.Text.Format("%d",prpLine->Taf);
			}
			break;
			case 14:
			{

					rlElement.Text.Format("%d",prpLine->Tdf);
			}
			break;
			case 15:
			{

					rlElement.FrameRight  = PRINT_FRAMETHIN;
					rlElement.Text.Format("%d",prpLine->Ttf);
			}
			break;
			
		}

		rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
		
	}
	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();
	return true;
}

//-----------------------------------------------------------------------------------------------


// Drucken in Datei
void ReportFlightDomIntMixTableViewer::PrintPlanToFile(char *pcpDefPath)
{

	CCS_TRY

	char pclHeadlineSelect[200];
	
//	sprintf(pclHeadlineSelect, GetString(IDS_STRING1798), pcmSelect, pcmInfo, pomData->GetSize());

	pomParentDlg->GetWindowText(pclHeadlineSelect,150);
	ofstream of;
	of.open( pcpDefPath, ios::out);
	of << CString(pclHeadlineSelect) << " "
	   << CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M") << endl << endl;

	of << GetString(IDS_STRING332) << setw(16)<< GetString(IDS_STRING1782) << setw(12) 
	   << GetString(IDS_STRING1783) << setw(12) << GetString(IDS_STRING1784) << setw(12) 
	   << GetString(IDS_STRING1785) << setw(12) << GetString(IDS_STRING1786) << setw(12) 
	   << GetString(IDS_STRING1787) << setw(12) << GetString(IDS_STRING1788) << setw(12) 
	   << GetString(IDS_STRING1789) << setw(12) << GetString(IDS_STRING1790) << setw(12) 
	   << GetString(IDS_STRING1791) << setw(12) << GetString(IDS_STRING1792) << setw(12) 
	   << GetString(IDS_STRING1793) << setw(12) << GetString(IDS_STRING1794) << setw(12) 
	   << GetString(IDS_STRING1795) << setw(12) << GetString(IDS_STRING1796) << endl;
	       
	of << "------------------------------------------------" << endl;
	int ilCount = omLines.GetSize();


	CStringArray olDaten;
	for(int i = 0; i < ilCount; i++)
	{
		FLIGHT_DOM_INT_MIXED_LINEDATA rlD = omLines[i];

	
		olDaten.Add(rlD.Date);
		CString olTmp;
		olTmp.Format("%d",rlD.Andf);
		olDaten.Add(olTmp);
		olTmp.Format("%d",rlD.Dndf);
		olDaten.Add(olTmp);
		olTmp.Format("%d",rlD.Tndf);
		olDaten.Add(olTmp);
		olTmp.Format("%d",rlD.Anif);
		olDaten.Add(olTmp);
		olTmp.Format("%d",rlD.Dnif);
		olDaten.Add(olTmp);
		olTmp.Format("%d",rlD.Tnif);
		olDaten.Add(olTmp);
		olTmp.Format("%d",rlD.Anrf);
		olDaten.Add(olTmp);
		olTmp.Format("%d",rlD.Dnrf);
		olDaten.Add(olTmp);
		olTmp.Format("%d",rlD.Tnrf);
		olDaten.Add(olTmp);
		olTmp.Format("%d",rlD.Anmf);
		olDaten.Add(olTmp);
		olTmp.Format("%d",rlD.Dnmf);
		olDaten.Add(olTmp);
		olTmp.Format("%d",rlD.Tnmf);
		olDaten.Add(olTmp);
		olTmp.Format("%d",rlD.Taf);
		olDaten.Add(olTmp);
		olTmp.Format("%d",rlD.Tdf);
		olDaten.Add(olTmp);
		olTmp.Format("%d",rlD.Ttf);
		olDaten.Add(olTmp);

		of.setf(ios::left, ios::adjustfield);
		of   << setw(16) << olDaten[0].GetBuffer(0)
			 << setw(12) << olDaten[1].GetBuffer(0)
			 << setw(12) << olDaten[2].GetBuffer(0)
			 << setw(12) << olDaten[3].GetBuffer(0)
			 << setw(12) << olDaten[4].GetBuffer(0)
			 << setw(12) << olDaten[5].GetBuffer(0)
			 << setw(12) << olDaten[6].GetBuffer(0)
			 << setw(12) << olDaten[7].GetBuffer(0)
			 << setw(12) << olDaten[8].GetBuffer(0)
			 << setw(12) << olDaten[9].GetBuffer(0)
			 << setw(12) << olDaten[10].GetBuffer(0)
			 << setw(12) << olDaten[11].GetBuffer(0)
			 << setw(12) << olDaten[12].GetBuffer(0)
			 << setw(12) << olDaten[13].GetBuffer(0)
			 << setw(12) << olDaten[14].GetBuffer(0)
			 << setw(12) << olDaten[15].GetBuffer(0)
			<< endl;
		
		olDaten.RemoveAll();
	}
	of.close();

	CCS_CATCH_ALL

}
