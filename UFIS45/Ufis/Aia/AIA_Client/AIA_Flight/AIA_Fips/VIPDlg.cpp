// VeryImpPers.cpp : implementation file
//

#include "stdafx.h"
#include "VIPDlg.h"
#include "resrc1.h"
#include "CCSGlobl.h"
#include "CedaBasicData.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif



static void FlightTableCf(void *popInstance, int ipDDXType,
    void *vpDataPointer, CString &ropInstanceName);



/////////////////////////////////////////////////////////////////////////////
// VIPDlg dialog
//---------------------------------------------------------------------------

VIPDlg::VIPDlg(CWnd* pParent, char opType, char opMode, CString opUrno)
	   :CDialog(VIPDlg::IDD, pParent),
	    omUrno(opUrno),
		omMode(opMode),
		omType(opType)
{

	//{{AFX_DATA_INIT(VIPDlg)

	//}}AFX_DATA_INIT
	omUrnoA = "";
	omUrnoD = "";

	emStatus = RotationVipDlg::Status::UNKNOWN;

	ogDdx.Register(this, BCD_VIP_UPDATE, CString("ROTATIONDLG_VIP"), CString("Vip Update"), FlightTableCf);
	ogDdx.Register(this, BCD_VIP_DELETE, CString("ROTATIONDLG_VIP"), CString("Vip Delete"), FlightTableCf);
	ogDdx.Register(this, BCD_VIP_INSERT, CString("ROTATIONDLG_VIP"), CString("Vip Insert"), FlightTableCf);

}



VIPDlg::~VIPDlg()
{

	ogDdx.UnRegister(this, NOTUSED);


}

void VIPDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(VIPDlg)
	DDX_Control(pDX, IDOK, m_OK);
	DDX_Control(pDX, IDC_CDAT_D, m_CdatD);
	DDX_Control(pDX, IDC_CDAT_T, m_CdatT);
	DDX_Control(pDX, IDC_LSTU_D, m_LstuD);
	DDX_Control(pDX, IDC_LSTU_T, m_LstuT);
	DDX_Control(pDX, IDC_NOGR, m_NoGr);
	DDX_Control(pDX, IDC_NOPX, m_NoPx);
	DDX_Control(pDX, IDC_PAXN, m_Paxn);
	DDX_Control(pDX, IDE_PAXR, m_Paxr);
	DDX_Control(pDX, IDC_USEC, m_Usec);
	DDX_Control(pDX, IDC_USEU, m_Useu);
	DDX_Control(pDX, IDC_GRID, m_Grid);
	DDX_Check(pDX, IDC_CH_ARRIVAL, m_CH_Arrival);
	DDX_Check(pDX, IDC_CH_DEPARTURE, m_CH_Departure);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(VIPDlg, CDialog)
	//{{AFX_MSG_MAP(VIPDlg)
	ON_BN_CLICKED(IDC_CH_ARRIVAL, OnChArrival)
	ON_BN_CLICKED(IDC_CH_DEPARTURE, OnChDeparture)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// VIPDlg message handlers
//---------------------------------------------------------------------------

BOOL VIPDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

	CString olTemp;

	if (omUrno != "-1")
	{
		m_CdatD.SetBKColor(SILVER);
		olTemp = ogBCD.GetField("VIP", "URNO", omUrno, "CDAT");
		m_CdatD.SetInitText(DBStringToDateTime(olTemp).Format("%d.%m.%Y"));
		m_CdatT.SetBKColor(SILVER);
		m_CdatT.SetInitText(DBStringToDateTime(olTemp).Format("%H:%M"));

		m_LstuD.SetBKColor(SILVER);
		olTemp = ogBCD.GetField("VIP", "URNO", omUrno, "LSTU");
		m_LstuD.SetInitText(DBStringToDateTime(olTemp).Format("%d.%m.%Y"));
		m_LstuT.SetBKColor(SILVER);
		m_LstuT.SetInitText(DBStringToDateTime(olTemp).Format("%H:%M"));

		m_Usec.SetBKColor(SILVER);
		m_Usec.SetInitText(ogBCD.GetField("VIP", "URNO", omUrno, "USEC"));

		m_Useu.SetBKColor(SILVER);
		m_Useu.SetInitText(ogBCD.GetField("VIP", "URNO", omUrno, "USEU"));

		m_NoGr.SetFormat("x|#x|#x|#");
		m_NoGr.SetTextLimit(1,3);
		m_NoGr.SetBKColor(YELLOW);
		m_NoGr.SetTextErrColor(RED);
		m_NoGr.SetInitText(ogBCD.GetField("VIP", "URNO", omUrno, "NOGR"));

		m_Grid.SetFormat("x|#x|#x|#");
		m_Grid.SetTextErrColor(RED);
		m_Grid.SetInitText(ogBCD.GetField("VIP", "URNO", omUrno, "GRID"));

		m_NoPx.SetFormat("x|#x|#x|#");
		m_NoPx.SetTextLimit(1,3);
		m_NoPx.SetBKColor(YELLOW);
		m_NoPx.SetTextErrColor(RED);
		m_NoPx.SetInitText(ogBCD.GetField("VIP", "URNO", omUrno, "NOPX"));

		m_Paxn.SetTypeToString("X(64)",64,1);
		m_Paxn.SetTextErrColor(RED);
		m_Paxn.SetInitText(ogBCD.GetField("VIP", "URNO", omUrno, "PAXN"));

		m_Paxr.SetTypeToString("X(64)",64,1);
		m_Paxr.SetTextErrColor(RED);
		m_Paxr.SetInitText(ogBCD.GetField("VIP", "URNO", omUrno, "PAXR"));
	}
	else
	{
		m_CdatD.SetBKColor(SILVER);
		m_CdatT.SetBKColor(SILVER);

		m_LstuD.SetBKColor(SILVER);
		m_LstuT.SetBKColor(SILVER);

		m_Usec.SetBKColor(SILVER);

		m_Useu.SetBKColor(SILVER);
                         
		m_NoGr.SetFormat("x|#x|#x|#");
		m_NoGr.SetTextLimit(1,3);
		m_NoGr.SetBKColor(YELLOW);
		m_NoGr.SetTextErrColor(RED);

		m_NoPx.SetFormat("x|#x|#x|#");
		m_NoPx.SetTextLimit(1,3);
		m_NoPx.SetBKColor(YELLOW);
		m_NoPx.SetTextErrColor(RED);

		m_Grid.SetFormat("x|#x|#x|#");
		m_Grid.SetTextErrColor(RED);

		m_Paxn.SetTypeToString("X(64)",64,1);
		m_Paxn.SetTextErrColor(RED);

		m_Paxr.SetTypeToString("X(64)",64,1);
		m_Paxr.SetTextErrColor(RED);
	}

	CButton* polCHArrival   = (CButton*)GetDlgItem(IDC_CH_ARRIVAL);
	CButton* polCHDeparture = (CButton*)GetDlgItem(IDC_CH_DEPARTURE);

	switch (omType)
	{
		case 'A':
			polCHArrival->EnableWindow(false);
			polCHDeparture->EnableWindow(false);
			polCHArrival->SetCheck(1);
			polCHDeparture->SetCheck(0);
		break;
		case 'D':
			polCHArrival->EnableWindow(false);
			polCHDeparture->EnableWindow(false);
			polCHArrival->SetCheck(0);
			polCHDeparture->SetCheck(1);
		break;
		case 'R':
			polCHArrival->EnableWindow(true);
			polCHDeparture->EnableWindow(true);
			m_CH_Arrival   = false;
			m_CH_Departure = false;;
		break;
		default:
		break;
	}


	if( omMode == 'U')
	{
		CString olFlnu = ogBCD.GetField("VIP", "URNO", omUrno, "FLNU");
		if (olFlnu == omUrnoA)
		{
			m_CH_Arrival   = true;
			m_CH_Departure = false;;
		}
		if  (olFlnu == omUrnoD)
		{
			m_CH_Arrival   = false;
			m_CH_Departure = true;;
		}

		polCHArrival->EnableWindow(false);
		polCHDeparture->EnableWindow(false);

		UpdateData(false);
	}

	InitialStatus();

	return TRUE;

}

//---------------------------------------------------------------------------

void VIPDlg::OnOK() 
{

	UpdateData(true);

	AfxGetApp()->DoWaitCursor(1);
	CString olErrorText;
	bool ilStatus = true;
	if(m_NoGr.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += GetString(ST_EINGABEFELD) + "NOGR\" " + GetString(ST_BADFORMAT);
	}
	if(m_NoPx.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += GetString(ST_EINGABEFELD) + "NOPX\" " + GetString(ST_BADFORMAT);
	}
/*
	if(m_Paxn.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += GetString(ST_EINGABEFELD) + "PAXN\" " + GetString(ST_BADFORMAT);
	}
	if(m_Paxr.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += GetString(ST_EINGABEFELD) + "PAXR\" " + GetString(ST_BADFORMAT);
	}
*/
	if (!m_CH_Arrival && !m_CH_Departure)
	{
		ilStatus = false;
		olErrorText += GetString(ST_EINGABEFELD) + "Arrival/Departure\" " + GetString(ST_BADFORMAT);
	}


	AfxGetApp()->DoWaitCursor(-1);
	if (ilStatus == true)
	{
		
		CString olFlnu;
		CString olNoGr;
		CString olNoPx;
		CString olPaxn;
		CString olPaxr;
		CString olGrid;
		CString olUser = CString(pcgUser);
		CString olTime = CTimeToDBString(CTime::GetCurrentTime(), TIMENULL);
		olFlnu = ogBCD.GetField("VIP", "URNO", omUrno, "FLNU");
		m_NoGr.GetWindowText(olNoGr);
		m_NoPx.GetWindowText(olNoPx);
		m_Paxn.GetWindowText(olPaxn);
		m_Paxr.GetWindowText(olPaxr);
		m_Grid.GetWindowText(olGrid);

		switch (omMode)
		{
			case 'U':	// UPDATE
			{
/*				RecordSet olRecordSetUpdate;
				olRecordSetUpdate = ogBCD.GetRecord("VIP", "URNO", omUrno, olRecordSetUpdate);
				olRecordSetUpdate.Values[ogBCD.GetFieldIndex("VIP", "URNO")];
				ogBCD.SetRecord("VIP", "URNO", omUrno, olRecordSetUpdate.Values, true);*/

				CStringArray olTmp;
				if (omType == 'A' || omType == 'D')
				{
					ogBCD.SetField("VIP", "URNO", omUrno, "NOGR", olNoGr, false);
					ogBCD.SetField("VIP", "URNO", omUrno, "NOPX", olNoPx, false);
					ogBCD.SetField("VIP", "URNO", omUrno, "PAXN", olPaxn, false);
					ogBCD.SetField("VIP", "URNO", omUrno, "PAXR", olPaxr, false);
					ogBCD.SetField("VIP", "URNO", omUrno, "FLNU", olFlnu, false);
					ogBCD.SetField("VIP", "URNO", omUrno, "GRID", olGrid, false);
					ogBCD.SetField("VIP", "URNO", omUrno, "USEU", olUser, false);
					ogBCD.SetField("VIP", "URNO", omUrno, "LSTU", olTime, false);
					ogBCD.Save("VIP");
				}
				else
				{
					// update nur Arrival, vorher auch Arrival
					if (m_CH_Arrival && !m_CH_Departure && omUrnoA == olFlnu)
					{
						ogBCD.SetField("VIP", "URNO", omUrno, "NOGR", olNoGr, false);
						ogBCD.SetField("VIP", "URNO", omUrno, "NOPX", olNoPx, false);
						ogBCD.SetField("VIP", "URNO", omUrno, "PAXN", olPaxn, false);
						ogBCD.SetField("VIP", "URNO", omUrno, "PAXR", olPaxr, false);
						ogBCD.SetField("VIP", "URNO", omUrno, "FLNU", olFlnu, false);
						ogBCD.SetField("VIP", "URNO", omUrno, "USEU", olUser, false);
						ogBCD.SetField("VIP", "URNO", omUrno, "LSTU", olTime, false);
						ogBCD.SetField("VIP", "URNO", omUrno, "GRID", olGrid, false);
						ogBCD.Save("VIP");

					}
					// update nur Departure, vorher auch Departure
					if (!m_CH_Arrival && m_CH_Departure && omUrnoD == olFlnu)
					{
						ogBCD.SetField("VIP", "URNO", omUrno, "NOGR", olNoGr, false);
						ogBCD.SetField("VIP", "URNO", omUrno, "NOPX", olNoPx, false);
						ogBCD.SetField("VIP", "URNO", omUrno, "PAXN", olPaxn, false);
						ogBCD.SetField("VIP", "URNO", omUrno, "PAXR", olPaxr, false);
						ogBCD.SetField("VIP", "URNO", omUrno, "FLNU", olFlnu, false);
						ogBCD.SetField("VIP", "URNO", omUrno, "USEU", olUser, false);
						ogBCD.SetField("VIP", "URNO", omUrno, "LSTU", olTime, false);
						ogBCD.SetField("VIP", "URNO", omUrno, "GRID", olGrid, false);
						ogBCD.Save("VIP");
					}
					// update, statt vorher Departure nun Arrival
					if (m_CH_Arrival && !m_CH_Departure && omUrnoD == olFlnu)
					{
						ogBCD.SetField("VIP", "URNO", omUrno, "NOGR", olNoGr, false);
						ogBCD.SetField("VIP", "URNO", omUrno, "NOPX", olNoPx, false);
						ogBCD.SetField("VIP", "URNO", omUrno, "PAXN", olPaxn, false);
						ogBCD.SetField("VIP", "URNO", omUrno, "PAXR", olPaxr, false);
						ogBCD.SetField("VIP", "URNO", omUrno, "FLNU", omUrnoA, false);
						ogBCD.SetField("VIP", "URNO", omUrno, "USEU", olUser, false);
						ogBCD.SetField("VIP", "URNO", omUrno, "LSTU", olTime, false);
						ogBCD.SetField("VIP", "URNO", omUrno, "GRID", olGrid, false);
						ogBCD.Save("VIP");
					}
					// update, statt vorher Arrival nun Departure
					if (!m_CH_Arrival && m_CH_Departure && omUrnoA == olFlnu)
					{
						ogBCD.SetField("VIP", "URNO", omUrno, "NOGR", olNoGr, false);
						ogBCD.SetField("VIP", "URNO", omUrno, "NOPX", olNoPx, false);
						ogBCD.SetField("VIP", "URNO", omUrno, "PAXN", olPaxn, false);
						ogBCD.SetField("VIP", "URNO", omUrno, "PAXR", olPaxr, false);
						ogBCD.SetField("VIP", "URNO", omUrno, "USEU", olUser, false);
						ogBCD.SetField("VIP", "URNO", omUrno, "LSTU", olTime, false);
						ogBCD.SetField("VIP", "URNO", omUrno, "GRID", olGrid, false);
						ogBCD.SetField("VIP", "URNO", omUrno, "FLNU", omUrnoD, false);
						ogBCD.Save("VIP");
					}
					// vorher Arrival, nun Arrival und Departure
					if (m_CH_Arrival && m_CH_Departure && omUrnoA == olFlnu)
					{
						olTmp.Add(olNoGr);
						ogBCD.SetField("VIP", "URNO", omUrno, "NOGR", olNoGr, false);
						ogBCD.SetField("VIP", "URNO", omUrno, "NOPX", olNoPx, false);
						ogBCD.SetField("VIP", "URNO", omUrno, "PAXN", olPaxn, false);
						ogBCD.SetField("VIP", "URNO", omUrno, "USEU", olUser, false);
						ogBCD.SetField("VIP", "URNO", omUrno, "LSTU", olTime, false);
						ogBCD.SetField("VIP", "URNO", omUrno, "PAXR", olPaxr, false);
						ogBCD.SetField("VIP", "URNO", omUrno, "GRID", olGrid, false);
						ogBCD.SetField("VIP", "URNO", omUrno, "FLNU", omUrnoA, false);
						ogBCD.Save("VIP");
						// Departure anlegen
						RecordSet olRecordSetInsert(ogBCD.GetFieldCount("VIP"));				
						olRecordSetInsert[ogBCD.GetFieldIndex("VIP", "NOGR")] = olNoGr;
						olRecordSetInsert[ogBCD.GetFieldIndex("VIP", "NOPX")] = olNoPx;
						olRecordSetInsert[ogBCD.GetFieldIndex("VIP", "PAXN")] = olPaxn;
						olRecordSetInsert[ogBCD.GetFieldIndex("VIP", "PAXR")] = olPaxr;
						olRecordSetInsert[ogBCD.GetFieldIndex("VIP", "FLNU")] = omUrnoD;
						olRecordSetInsert[ogBCD.GetFieldIndex("VIP", "GRID")] = olGrid;
						olRecordSetInsert[ogBCD.GetFieldIndex("VIP", "USEC")] = olUser;
						olRecordSetInsert[ogBCD.GetFieldIndex("VIP", "CDAT")] = olTime;
					
						ogBCD.InsertRecord("VIP", olRecordSetInsert, true);
					}
					// vorher Departure, nun Departure und Arrival
					if (m_CH_Arrival && m_CH_Departure && omUrnoD == olFlnu)
					{
						olTmp.Add(olNoGr);
						ogBCD.SetField("VIP", "URNO", omUrno, "NOGR", olNoGr, false);
						ogBCD.SetField("VIP", "URNO", omUrno, "NOPX", olNoPx, false);
						ogBCD.SetField("VIP", "URNO", omUrno, "PAXN", olPaxn, false);
						ogBCD.SetField("VIP", "URNO", omUrno, "PAXR", olPaxr, false);
						ogBCD.SetField("VIP", "URNO", omUrno, "GRID", olGrid, false);
						ogBCD.SetField("VIP", "URNO", omUrno, "USEU", olUser, false);
						ogBCD.SetField("VIP", "URNO", omUrno, "LSTU", olTime, false);
						ogBCD.SetField("VIP", "URNO", omUrno, "FLNU", omUrnoD, false);
						ogBCD.Save("VIP");
						// Departure anlegen
						RecordSet olRecordSetInsert(ogBCD.GetFieldCount("VIP"));				
						olRecordSetInsert[ogBCD.GetFieldIndex("VIP", "NOGR")] = olNoGr;
						olRecordSetInsert[ogBCD.GetFieldIndex("VIP", "NOPX")] = olNoPx;
						olRecordSetInsert[ogBCD.GetFieldIndex("VIP", "PAXN")] = olPaxn;
						olRecordSetInsert[ogBCD.GetFieldIndex("VIP", "PAXR")] = olPaxr;
						olRecordSetInsert[ogBCD.GetFieldIndex("VIP", "FLNU")] = omUrnoA;
						olRecordSetInsert[ogBCD.GetFieldIndex("VIP", "USEC")] = olUser;
						olRecordSetInsert[ogBCD.GetFieldIndex("VIP", "CDAT")] = olTime;
						olRecordSetInsert[ogBCD.GetFieldIndex("VIP", "GRID")] = olGrid;
						ogBCD.InsertRecord("VIP", olRecordSetInsert, true);
					}
				}
			}
			break;

			case 'I':	// INSERT
			{
				if (omType == 'A')
				{
					RecordSet olRecordSetInsert(ogBCD.GetFieldCount("VIP"));				
					olRecordSetInsert[ogBCD.GetFieldIndex("VIP", "NOGR")] = olNoGr;
					olRecordSetInsert[ogBCD.GetFieldIndex("VIP", "NOPX")] = olNoPx;
					olRecordSetInsert[ogBCD.GetFieldIndex("VIP", "PAXN")] = olPaxn;
					olRecordSetInsert[ogBCD.GetFieldIndex("VIP", "PAXR")] = olPaxr;
					olRecordSetInsert[ogBCD.GetFieldIndex("VIP", "USEC")] = olUser;
					olRecordSetInsert[ogBCD.GetFieldIndex("VIP", "CDAT")] = olTime;
					olRecordSetInsert[ogBCD.GetFieldIndex("VIP", "FLNU")] = omUrnoA;
					olRecordSetInsert[ogBCD.GetFieldIndex("VIP", "GRID")] = olGrid;
					ogBCD.InsertRecord("VIP", olRecordSetInsert, true);
				}
				if (omType == 'D')
				{
					RecordSet olRecordSetInsert(ogBCD.GetFieldCount("VIP"));				
					olRecordSetInsert[ogBCD.GetFieldIndex("VIP", "NOGR")] = olNoGr;
					olRecordSetInsert[ogBCD.GetFieldIndex("VIP", "NOPX")] = olNoPx;
					olRecordSetInsert[ogBCD.GetFieldIndex("VIP", "PAXN")] = olPaxn;
					olRecordSetInsert[ogBCD.GetFieldIndex("VIP", "PAXR")] = olPaxr;
					olRecordSetInsert[ogBCD.GetFieldIndex("VIP", "USEC")] = olUser;
					olRecordSetInsert[ogBCD.GetFieldIndex("VIP", "CDAT")] = olTime;
					olRecordSetInsert[ogBCD.GetFieldIndex("VIP", "FLNU")] = omUrnoD;
					olRecordSetInsert[ogBCD.GetFieldIndex("VIP", "GRID")] = olGrid;
					ogBCD.InsertRecord("VIP", olRecordSetInsert, true);

				}
				if (omType == 'R' && m_CH_Arrival)
				{
					RecordSet olRecordSetInsert(ogBCD.GetFieldCount("VIP"));				
					olRecordSetInsert[ogBCD.GetFieldIndex("VIP", "NOGR")] = olNoGr;
					olRecordSetInsert[ogBCD.GetFieldIndex("VIP", "NOPX")] = olNoPx;
					olRecordSetInsert[ogBCD.GetFieldIndex("VIP", "PAXN")] = olPaxn;
					olRecordSetInsert[ogBCD.GetFieldIndex("VIP", "PAXR")] = olPaxr;
					olRecordSetInsert[ogBCD.GetFieldIndex("VIP", "USEC")] = olUser;
					olRecordSetInsert[ogBCD.GetFieldIndex("VIP", "CDAT")] = olTime;
					olRecordSetInsert[ogBCD.GetFieldIndex("VIP", "FLNU")] = omUrnoA;
					olRecordSetInsert[ogBCD.GetFieldIndex("VIP", "GRID")] = olGrid;
					ogBCD.InsertRecord("VIP", olRecordSetInsert, true);
				}
				if (omType == 'R' && m_CH_Departure)
				{
					RecordSet olRecordSetInsert(ogBCD.GetFieldCount("VIP"));				
					olRecordSetInsert[ogBCD.GetFieldIndex("VIP", "NOGR")] = olNoGr;
					olRecordSetInsert[ogBCD.GetFieldIndex("VIP", "NOPX")] = olNoPx;
					olRecordSetInsert[ogBCD.GetFieldIndex("VIP", "PAXN")] = olPaxn;
					olRecordSetInsert[ogBCD.GetFieldIndex("VIP", "USEC")] = olUser;
					olRecordSetInsert[ogBCD.GetFieldIndex("VIP", "CDAT")] = olTime;
					olRecordSetInsert[ogBCD.GetFieldIndex("VIP", "PAXR")] = olPaxr;
					olRecordSetInsert[ogBCD.GetFieldIndex("VIP", "FLNU")] = omUrnoD;
					olRecordSetInsert[ogBCD.GetFieldIndex("VIP", "GRID")] = olGrid;
					ogBCD.InsertRecord("VIP", olRecordSetInsert, true);
				}
			}
			break;

			default:
			break;
		}
		//ogBCD.Save("VIP");
		CDialog::OnOK();
	}
	else
	{
		Beep(440,70);
		MessageBox(olErrorText, GetString(ST_HINWEIS), MB_ICONEXCLAMATION);
		m_NoGr.SetFocus();
		m_NoGr.SetSel(0,-1);
	}
}

//---------------------------------------------------------------------------

void VIPDlg::OnCancel() 
{
	// TODO: Add extra cleanup here
	
	CDialog::OnCancel();
}

//---------------------------------------------------------------------------
void VIPDlg::OnChDeparture()
{

	// TODO: Add your control notification handler code here

}


void VIPDlg::OnChArrival()
{

	// TODO: Add your control notification handler code here

}


void VIPDlg::SetAftTabUrnoA(CString opUrno)
{

	omUrnoA = opUrno;

}


void VIPDlg::SetAftTabUrnoD(CString opUrno)
{

	omUrnoD = opUrno;

}


void VIPDlg::InitDialog()
{

	CString olTemp;

	olTemp = ogBCD.GetField("VIP", "URNO", omUrno, "CDAT");
	m_CdatD.SetInitText(DBStringToDateTime(olTemp).Format("%d.%m.%Y"));
	m_CdatT.SetInitText(DBStringToDateTime(olTemp).Format("%H:%M"));

	olTemp = ogBCD.GetField("VIP", "URNO", omUrno, "LSTU");
	m_LstuD.SetInitText(DBStringToDateTime(olTemp).Format("%d.%m.%Y"));
	m_LstuT.SetInitText(DBStringToDateTime(olTemp).Format("%H:%M"));

	m_Usec.SetInitText(ogBCD.GetField("VIP", "URNO", omUrno, "USEC"));


	m_Useu.SetInitText(ogBCD.GetField("VIP", "URNO", omUrno, "USEU"));

	m_NoGr.SetInitText(ogBCD.GetField("VIP", "URNO", omUrno, "NOGR"));

	m_NoPx.SetInitText(ogBCD.GetField("VIP", "URNO", omUrno, "NOPX"));

	m_Paxn.SetInitText(ogBCD.GetField("VIP", "URNO", omUrno, "PAXN"));

	m_Paxr.SetInitText(ogBCD.GetField("VIP", "URNO", omUrno, "PAXR"));

	InitialStatus();
}



static void FlightTableCf(void *popInstance, int ipDDXType,
    void *vpDataPointer, CString &ropInstanceName)
{

    VIPDlg *polDlg = (VIPDlg*)popInstance;

	if (ipDDXType == BCD_VIP_UPDATE || ipDDXType == BCD_VIP_DELETE || ipDDXType == BCD_VIP_INSERT)
		polDlg->InitDialog();

}

void VIPDlg::InitialStatus()
{
	CWnd* plWnd = NULL;
	switch (emStatus)
	{
		case RotationVipDlg::Status::UNKNOWN:
			InitialPostFlight(TRUE);
			break;
		case RotationVipDlg::Status::POSTFLIGHT:
			InitialPostFlight(FALSE);
			break;
		default:
			InitialPostFlight(TRUE);
			break;
	}
}

void VIPDlg::setStatus(RotationVipDlg::Status epStatus)
{
	emStatus = epStatus;
}

RotationVipDlg::Status VIPDlg::getStatus() const
{
	return emStatus;
}

void VIPDlg::InitialPostFlight(BOOL bpPostFlight)
{
	CWnd* plWnd = NULL;
	plWnd = GetDlgItem(IDC_NOGR);
	if (plWnd) 
		plWnd->EnableWindow(bpPostFlight);

	plWnd = GetDlgItem(IDC_NOPX);
	if (plWnd) 
		plWnd->EnableWindow(bpPostFlight);

	plWnd = GetDlgItem(IDC_PAXN);
	if (plWnd) 
		plWnd->EnableWindow(bpPostFlight);

	plWnd = GetDlgItem(IDC_GRID);
	if (plWnd) 
		plWnd->EnableWindow(bpPostFlight);

	plWnd = GetDlgItem(IDC_CH_ARRIVAL);
	if (plWnd) 
		plWnd->EnableWindow(bpPostFlight);

	plWnd = GetDlgItem(IDC_CH_DEPARTURE);
	if (plWnd) 
		plWnd->EnableWindow(bpPostFlight);

	plWnd = GetDlgItem(IDE_PAXR);
	if (plWnd) 
		plWnd->EnableWindow(bpPostFlight);
}
