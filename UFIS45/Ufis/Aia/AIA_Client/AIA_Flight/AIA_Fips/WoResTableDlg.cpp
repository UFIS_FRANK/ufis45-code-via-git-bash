// WoResTableDlg.cpp : implementation file
//
// Modification History: 
//	061100	rkr		OnTableDragBegin auf erweiterte Tabellenstruktur angepasst
//					(Spalten ETA(i), ETD(i), NAture f�r arrival und departure erweitert)
//	081100	rkr		PRF44(Athen):OnDragOver() und OnDrop() zum hinzuf�gen aus XXXGantt unterst�tzt.
//					In OnInitDialog() mu� hierf�r CCSDragDropCtrl->RegisterTarget() impl. werden.

#include "stdafx.h"
#include "fpms.h"
#include "resrc1.h"
#include "WoResTableDlg.h"
#include "SeasonDlg.h"
#include "RotationDlg.h"
#include "DiaCedaFlightData.h"

#include "PosDiagram.h"
#include "GatDiagram.h"
#include "BltDiagram.h"
#include "WroDiagram.h"
#include "Utils.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// WoResTableDlg dialog


WoResTableDlg::WoResTableDlg(CWnd* pParent /*=NULL*/)
	: CDialog(WoResTableDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(WoResTableDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT

	pomTable = NULL;
	imMode = FIPSMODULES_NONE;
	imConnectCounter = 0;
	bmUpdate = false;

    CDialog::Create(WoResTableDlg::IDD, NULL);
	m_key = "DialogPosition\\WithoutResources";
}


WoResTableDlg::~WoResTableDlg()
{
	pomViewer->UnRegister();

	delete pomTable;
	delete pomViewer;
}


void WoResTableDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(WoResTableDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(WoResTableDlg, CDialog)
	//{{AFX_MSG_MAP(WoResTableDlg)
	ON_WM_CLOSE()
    ON_MESSAGE(WM_TABLE_DRAGBEGIN, OnTableDragBegin)
    ON_MESSAGE(WM_TABLE_LBUTTONDBLCLK, OnTableLButtonDblclk)
    ON_MESSAGE(WM_TABLE_LBUTTONDOWN, OnTableLButtonDown)
    ON_MESSAGE(WM_DROP, OnDrop)  
    ON_MESSAGE(WM_DRAGOVER, OnDragOver)
    ON_MESSAGE(WM_TABLE_RETURN_PRESSED, OnTableReturnPressed)
	ON_WM_SIZE()
 	ON_WM_DESTROY()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// WoResTableDlg message handlers



bool WoResTableDlg::Connect()
{
	imConnectCounter++;
	return true;
}


bool WoResTableDlg::Disconnect()
{
	if (imConnectCounter > 0)
	{
		imConnectCounter--;
		if (imConnectCounter == 0)
			OnClose();
	}

	return true;
}



void WoResTableDlg::Activate(FipsModules ipMode, bool bpShow)
{
	// rebuild table only if new mode is given
	if (imMode != ipMode)
	{
		imMode = ipMode;

		pomViewer->ChangeViewTo(ipMode);

		UpdateCaption();
	}

	if(bpShow)
	{
		pomViewer->ChangeViewTo(ipMode);
		SetWindowPos(&wndTop,0,0,0,0, SWP_NOSIZE | SWP_NOMOVE | SWP_SHOWWINDOW);
		bmUpdate = true;
	}

}


void WoResTableDlg::Rebuild()
{
	FipsModules ilMode = imMode;

	imMode = FIPSMODULES_NONE;
	Activate(ilMode, false);
}


void WoResTableDlg::UpdateCaption()
{
	CString olGantt;

	if(imMode == POSDIA)
		olGantt  = GetString(IDS_STRING361);

	if(imMode == GATDIA)
		olGantt  = GetString(IDS_STRING1218);

	if(imMode == BLTDIA)
		olGantt  = GetString(IDS_STRING1219);

	if(imMode == WRODIA)
		olGantt  = GetString(IDS_STRING1222);

	//// set caption
	CString olCaption;
	olCaption.Format("%s( %s )   (%d)", GetString(IDS_STRING1813), olGantt, GetCount(imMode));
	// additional text for time mode
	CString olTimes;
	if (bgGatPosLocal)
		olTimes = GetString(IDS_STRING1921);
	else
		olTimes = GetString(IDS_STRING1920);
	olCaption += " ("+olTimes+")";
	SetWindowText(olCaption);
	
}


int WoResTableDlg::GetCount(FipsModules ipMode) const
{
	return pomViewer->GetCount(ipMode);
}


void WoResTableDlg::SaveToReg()
{
	BOOL ok = WriteDialogToReg((CWnd*) this, m_key);
}


void WoResTableDlg::OnDestroy() 
{
	SaveToReg();
	CDialog::OnDestroy();
}



BOOL WoResTableDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	m_key = "DialogPosition\\WithoutResources";

	CRect olRect;
	this->GetWindowRect(&olRect);
	GetDialogFromReg(olRect, m_key);
	SetWindowPos(&wndTop, olRect.left, olRect.top, olRect.Width(), olRect.Height(), SWP_HIDEWINDOW);// | SWP_NOMOVE );
//	SetWindowPos( &wndTopMost  , 0,0,0,0, SWP_NOSIZE  | SWP_NOMOVE);

	pomViewer = new WoResTableViewer(this);

	pomTable = new CCSTable;
	pomTable->SetSelectMode(LBS_MULTIPLESEL | LBS_EXTENDEDSEL);
	pomTable->SetHeaderSpacing(0);

//	CRect olRect;
    GetClientRect(&olRect);

//	CCSDragDropCtrl mu� f�r die DargDrop-Funtionalit�t registriert werden.
	omDragDropObject.RegisterTarget(this, this);

    olRect.InflateRect(1, 1);     // hiding the CTable window border
    
	pomTable->SetTableData(this, olRect.left, olRect.right, olRect.top, olRect.bottom);

	pomViewer->Attach(pomTable);

	pomViewer->ChangeViewTo(FIPSMODULES_NONE);

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void WoResTableDlg::OnClose() 
{
	ShowWindow(SW_HIDE);	
	bmUpdate = false;

	//pomViewer->UnRegister();

	//CDialog::OnClose();
}


////////////////////////////////////////////////////////////////////////////
// Drag&Drop - Behandlung
//
LONG WoResTableDlg::OnDrop(UINT i, LONG l)
{

    int ilClass = omDragDropObject.GetDataClass(); 
	//	�berpr�fung aus welchem xxxGantt das Objekt stammt.
	if (ilClass == DIT_POS_GANTT )
	{
		DIAFLIGHTDATA *prlFlightA  = ogPosDiaFlightData.GetFlightByUrno(omDragDropObject.GetDataDWord(0));
		DIAFLIGHTDATA *prlFlightD  = ogPosDiaFlightData.GetFlightByUrno(omDragDropObject.GetDataDWord(1));

		//	die change-function von DiaCedaFlightData f�hren �ber BC das update aus
		if (prlFlightA != NULL)
			ogPosDiaFlightData.ChangeFlightPos(*prlFlightA, 'A', " ");
		if (prlFlightD != NULL)
			ogPosDiaFlightData.ChangeFlightPos(*prlFlightD, 'D', " ");
	}
	else if (ilClass == DIT_GAT_GANTT )
	{
		DIAFLIGHTDATA *prlFlight = ogPosDiaFlightData.GetFlightByUrno(omDragDropObject.GetDataDWord(0));
		if (prlFlight != NULL)
			ogPosDiaFlightData.ChangeFlightGat(*prlFlight, (char)omDragDropObject.GetDataDWord(1), " ", omDragDropObject.GetDataDWord(2));
	}
	else if (ilClass == DIT_BLT_GANTT )
	{
		DIAFLIGHTDATA *prlFlight = ogPosDiaFlightData.GetFlightByUrno(omDragDropObject.GetDataDWord(0));
		if (prlFlight != NULL)
			ogPosDiaFlightData.ChangeFlightBlt(*prlFlight, " ", " ", omDragDropObject.GetDataDWord(1));
	}
	else if (ilClass == DIT_WRO_GANTT )
	{
		DIAFLIGHTDATA *prlFlight = ogPosDiaFlightData.GetFlightByUrno(omDragDropObject.GetDataDWord(0));
		if (prlFlight != NULL)
			ogPosDiaFlightData.ChangeFlightWro(*prlFlight, " ");
	}

	return 0L;
}

LONG WoResTableDlg::OnDragOver(UINT wParam, LONG lParam)
{
	return 0L;
}

// Dragbeginn einer Drag&Drop Aktion
LONG WoResTableDlg::OnTableDragBegin(UINT wParam, LONG lParam)
{

	WORESTABLE_LINEDATA *prlLine = (WORESTABLE_LINEDATA*)pomTable->GetTextLineData(wParam);
	if (prlLine == NULL)
		return 0L;

	if (CheckPostFlight(prlLine))
		return 0L;

	// Which part of the rotation shall be processed
	CPoint point;
    ::GetCursorPos(&point);
    pomTable->pomListBox->ScreenToClient(&point);    
	//long llUrno = 0;

	/*
	bool blArrival = true;
	if (prlLine != NULL)
	{
 		int ilCol = pomTable->GetColumnnoFromPoint(point);
 		if (ilCol < 11)
		{
 			blArrival = true;
		}
		if (ilCol > 12)
		{
 			blArrival = false;
		}

	}
	else
		return 0L;
*/

	bool blValid = false;

 	if(imMode == WRODIA && prlLine->DUrno > 0)
	{
		omDragDropObject.CreateDWordData(DIT_FLIGHT, 1);
		omDragDropObject.AddDWord(prlLine->DUrno);
		blValid = true;
	}
 			
	if (imMode == POSDIA && (prlLine->AUrno > 0 || prlLine->DUrno > 0))
	{
		omDragDropObject.CreateDWordData(DIT_FLIGHT, 2);
		omDragDropObject.AddDWord(prlLine->AUrno);
		omDragDropObject.AddDWord(prlLine->DUrno);
		blValid = true;
	}


	if(imMode == GATDIA && prlLine->DUrno > 0)
	{
		omDragDropObject.CreateDWordData(DIT_FLIGHT, 3);
 		omDragDropObject.AddDWord(prlLine->DUrno);
		omDragDropObject.AddDWord('D');
		DIAFLIGHTDATA *prlFlight  = ogPosDiaFlightData.GetFlightByUrno(prlLine->DUrno);
		if(CString(prlFlight->Gtd1).IsEmpty())
		{
			omDragDropObject.AddDWord(1);
		}
		else
		{
			omDragDropObject.AddDWord(2);
		}
		blValid = true;
	}
			


	if(imMode == BLTDIA && prlLine->AUrno > 0)
	{
		omDragDropObject.CreateDWordData(DIT_FLIGHT, 2);
		omDragDropObject.AddDWord(prlLine->AUrno);
		DIAFLIGHTDATA *prlFlight  = ogPosDiaFlightData.GetFlightByUrno(prlLine->AUrno);
 		if(CString(prlFlight->Blt1).IsEmpty())
		{
			omDragDropObject.AddDWord(1);
		}
		else
		{
			omDragDropObject.AddDWord(2);
		}
		blValid = true;
	}


	if (!blValid)
 		return 0L;


	ogBcHandle.BufferBc();

	omDragDropObject.BeginDrag();

	ogBcHandle.ReleaseBuffer();

	return 0L;
}


////////////////////////////////////////////////////////////////////////////
// linke Maustaste auf der Flighttable gedr�ckt
//
LONG WoResTableDlg::OnTableLButtonDblclk( UINT wParam, LPARAM lParam)
{
	UINT ipItem = wParam;
	WORESTABLE_LINEDATA *prlTableLine = NULL;
	prlTableLine = (WORESTABLE_LINEDATA *)pomTable->GetTextLineData(ipItem);
	if (prlTableLine != NULL)
	{
		// get flight data
		const DIAFLIGHTDATA *prlFlight = NULL;
		char clAdid=' ';
		if (prlTableLine->AUrno != 0)
		{
			prlFlight = ogPosDiaFlightData.GetFlightByUrno(prlTableLine->AUrno);
			clAdid = 'A';
		}
		if (prlFlight==NULL && prlTableLine->DUrno != 0) 
		{
			prlFlight = ogPosDiaFlightData.GetFlightByUrno(prlTableLine->DUrno);
			clAdid = 'D';
		}
		

		if(prlFlight != NULL)
		{		
			if (ogPosDiaFlightData.bmOffLine)
			{
 				pogSeasonDlg->NewData(this, prlFlight, bgGatPosLocal);
			}
			else
			{
	 			if (clAdid == 'A')
				{
					CFPMSApp::ShowFlightRecordData(this, prlFlight->Ftyp[0], prlFlight->Urno, prlFlight->Rkey, clAdid, prlFlight->Tifa, bgGatPosLocal);
				}
				else
				{
					CFPMSApp::ShowFlightRecordData(this, prlFlight->Ftyp[0], prlFlight->Urno, prlFlight->Rkey, clAdid, prlFlight->Tifd, bgGatPosLocal);
				}
			}
		}
	}
	return 0L;
}




LONG WoResTableDlg::OnTableLButtonDown(UINT wParam, LONG lParam)
{
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));

	UINT ipItem = wParam;
	WORESTABLE_LINEDATA *prlTableLine = NULL;
	prlTableLine = (WORESTABLE_LINEDATA *)pomTable->GetTextLineData(ipItem);
	if (prlTableLine != NULL)
	{
		const DIAFLIGHTDATA *prlAFlight = ogPosDiaFlightData.GetFlightByUrno(prlTableLine->AUrno);;
		const DIAFLIGHTDATA *prlDFlight = ogPosDiaFlightData.GetFlightByUrno(prlTableLine->DUrno);;

		// which column is clicked?
		CPoint point;
		::GetCursorPos(&point);
		pomTable->pomListBox->ScreenToClient(&point);    
 		int ilCol = pomTable->GetColumnnoFromPoint(point);

		// show the timerange which the flight would be occupied
		if(imMode == POSDIA && pogPosDiagram != NULL)
		{
			pogPosDiagram->ShowFlight(prlAFlight, prlDFlight);
		}
		if(imMode == GATDIA && pogGatDiagram != NULL)
		{
 			if (prlDFlight != NULL)
				pogGatDiagram->ShowFlight(prlDFlight->Urno, 'D');
 
		}
			
		if(imMode == BLTDIA && pogBltDiagram != NULL)
		{
			if (prlAFlight != NULL)
				pogBltDiagram->ShowFlight(prlAFlight->Urno);
		}


		if(imMode == WRODIA && pogWroDiagram != NULL)
		{
			if (prlDFlight != NULL)
				pogWroDiagram->ShowFlight(prlDFlight->Urno);
		}
	}

	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));

	return 0L;

}

BOOL WoResTableDlg::CheckPostFlight(const WORESTABLE_LINEDATA *prpTableLine)
{
	if (prpTableLine == NULL)
		return FALSE;

	DIAFLIGHTDATA *prlAFlight = NULL;
	DIAFLIGHTDATA *prlDFlight = NULL;

	if(prpTableLine->AUrno != 0)
		prlAFlight = ogPosDiaFlightData.GetFlightByUrno(prpTableLine->AUrno);

	if(prpTableLine->DUrno != 0)
		prlDFlight = ogPosDiaFlightData.GetFlightByUrno(prpTableLine->DUrno);


	BOOL blPost = FALSE;

	if (prlAFlight && prlDFlight)
	{
		if (prlAFlight->Tifa != TIMENULL && prlDFlight->Tifd != TIMENULL)
		{
			if (IsPostFlight(prlAFlight->Tifa) && IsPostFlight(prlDFlight->Tifd))
				blPost = TRUE;
		}
	}
	else if (!prlAFlight && prlDFlight)
	{
		if (prlDFlight->Tifd != TIMENULL)
		{
			if (IsPostFlight(prlDFlight->Tifd))
				blPost = TRUE;
		}
	}
	else if (prlAFlight && !prlDFlight)
	{
		if (prlAFlight->Tifa != TIMENULL)
		{
			if (IsPostFlight(prlAFlight->Tifa))
				blPost = TRUE;
		}
	}

	CWnd* opWnd = (CWnd*) this;
	if (opWnd)
	{
		if (blPost)
			ModifyWindowText(opWnd, GetString(IDS_STRINGWND_POSTFLIGHT), FALSE);
		else
			ModifyWindowText(opWnd, GetString(IDS_STRINGWND_POSTFLIGHT), TRUE);
	}

	return blPost;

}

BOOL WoResTableDlg::PostFlight(const DIAFLIGHTDATA *prpAFlight, const DIAFLIGHTDATA *prpDFlight)
{
	ASSERT(FALSE);
	return FALSE;

	if (prpAFlight && IsPostFlight(prpAFlight->Tifa))
		return TRUE;
		
	if (prpDFlight && IsPostFlight(prpDFlight->Tifd))
		return TRUE;

	return FALSE;
}


LONG WoResTableDlg::OnTableReturnPressed( UINT wParam, LPARAM lParam)
{
	OnTableLButtonDblclk(wParam, lParam); 
	return 0L;
}

void WoResTableDlg::OnSize(UINT nType, int cx, int cy) 
{

	CDialog::OnSize(nType, cx, cy);

	if(pomTable != NULL)
	{
		CRect rect;	
		GetClientRect(&rect);

		rect.InflateRect(1, 1);     // hiding the CTable window border
    
		pomTable->SetPosition(rect.left, rect.right, rect.top, rect.bottom);
	}
	
}
