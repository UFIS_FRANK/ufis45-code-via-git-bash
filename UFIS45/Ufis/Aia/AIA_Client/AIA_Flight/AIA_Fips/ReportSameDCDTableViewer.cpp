// ReportSameDCDTableViewer.cpp : implementation file
// 
// List of all flights that the same Delay Reason
// Angabe eines TType

#include "stdafx.h"
#include "ReportSameDCDTableViewer.h"
#include "ReportXXXCedaData.h"
#include "CcsGlobl.h"
#include <iomanip.h>
#include <iostream.h>
#include <fstream.h>
#include "BasicData.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif



/////////////////////////////////////////////////////////////////////////////
// ReportSameDCDTableViewer
//

int ReportSameDCDTableViewer::GetVia(CCSPtrArray<VIADATA> *opVias, ROTATIONDLGFLIGHTDATA *prpFlight)
{
	opVias->DeleteAll();

	if(prpFlight == NULL)
		return 0;
	if(strlen(prpFlight->Vial) == 0)
		return 0;

	CString olVias(prpFlight->Vial);
	CString olFids;
	CString olApc3;
	CString olApc4;

	VIADATA *prlVia;

	while(olVias.IsEmpty() != TRUE)
	{
			prlVia = new VIADATA;
			opVias->Add(prlVia);

			if(olVias.GetLength() < 120)
			{
				olVias += "                                                                                                                             ";
				olVias = olVias.Left(120);
			}


			olApc3 = olVias.Mid(1,3);
			olApc3.TrimLeft();
			sprintf(prlVia->Apc3, olApc3);

			if(olVias.GetLength() >= 120)
				olVias = olVias.Right(olVias.GetLength() - 120);

	}
	return opVias->GetSize();
}



ReportSameDCDTableViewer::ReportSameDCDTableViewer(CCSPtrArray<ROTATIONDLGFLIGHTDATA> *popData, char *pcpInfo, char *pcpSelect)
{

	pomData = popData;
	pcmInfo = pcpInfo;
	pcmSelect = pcpSelect;


	bmIsFromSearch = false;
    pomTable = NULL;

}


ReportSameDCDTableViewer::~ReportSameDCDTableViewer()
{
	omPrintHeadHeaderArray.DeleteAll();  // BWi
    DeleteAll();
}


void ReportSameDCDTableViewer::SetParentDlg(CDialog* ppParentDlg)
{

	pomParentDlg = ppParentDlg;

}


void ReportSameDCDTableViewer::Attach(CCSTable *popTable)
{
    pomTable = popTable;
}


char* ReportSameDCDTableViewer::GetDname()
{

	CCS_TRY
	CCS_CATCH_ALL

	return omDname.GetBuffer(0);

}


void ReportSameDCDTableViewer::ChangeViewTo(const char *pcpViewName)
{

    pomTable->ResetContent();
    DeleteAll();    
    MakeLines();

	UpdateDisplay();
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));

	// holen des Klartextes des Selects (aft.ttyp nat.tnam �ber nat.ttyp)
	ReportXXXCedaData olReportXXXCedaData("DECN", "DENA", "DEN");
	omDname = olReportXXXCedaData.ReadCleartext(CString(pcmSelect));
	
}


/////////////////////////////////////////////////////////////////////////////
// ReportSameDCDTableViewer -- code specific to this class

void ReportSameDCDTableViewer::MakeLines()
{
	ROTATIONDLGFLIGHTDATA *prlAFlight;
	ROTATIONDLGFLIGHTDATA *prlDFlight;
	ROTATIONDLGFLIGHTDATA *prlNextFlight = NULL;
	ROTATIONDLGFLIGHTDATA *prlFlight;
	int ilLineNo;

	bool blRDeparture = false;

	int ilFlightCount = pomData->GetSize();

	for (int ilLc = 0; ilLc < ilFlightCount; ilLc++)
	{
		prlFlight = &(*pomData)[ilLc];

		if(ilLc + 1 < ilFlightCount)
			prlNextFlight = &(*pomData)[ilLc + 1];	
		else
			prlNextFlight = NULL;	

		//Arrival
		if((strcmp(prlFlight->Org3, pcgHome) != 0) && (strcmp(prlFlight->Des3, pcgHome) == 0))
		{
			prlAFlight = prlFlight;
			prlDFlight = NULL;
			if(prlNextFlight != NULL)
			{
				if((prlNextFlight->Rkey == prlFlight->Rkey) && (prlFlight->Rkey != NULL))
				{
					prlDFlight = prlNextFlight;

					if((strcmp(prlNextFlight->Org3, pcgHome) == 0) && (strcmp(prlNextFlight->Des3, pcgHome) != 0))
					{
						ilLc++;
					}
					else
						blRDeparture = true;
				}
			}
		}
		else
		{
			// Departure
			if((strcmp(prlFlight->Org3, pcgHome) == 0) && (strcmp(prlFlight->Des3, pcgHome) != 0))
			{
				prlAFlight = NULL;
				prlDFlight = prlFlight;
				blRDeparture = false;
			}
			else
			{
				//Turnaround
				if((strcmp(prlFlight->Org3, pcgHome) == 0) && (strcmp(prlFlight->Des3, pcgHome) == 0))
				{
					if(blRDeparture)
					{
						blRDeparture = false;
						prlAFlight = prlFlight;
						prlDFlight = NULL;
						if(prlNextFlight != NULL)
						{
							if((prlNextFlight->Rkey == prlFlight->Rkey) && (prlFlight->Rkey != NULL))
							{
								prlDFlight = prlNextFlight;
								if((strcmp(prlNextFlight->Org3, pcgHome) == 0) && (strcmp(prlNextFlight->Des3, pcgHome) != 0))
								{
									ilLc++;
								}
								else
									blRDeparture = true;
							}
						}
					}
					else
					{
						prlAFlight		= prlFlight;
						prlDFlight		= prlFlight;
						if(prlNextFlight != NULL)
						{
							if((prlNextFlight->Rkey == prlFlight->Rkey) && (prlFlight->Rkey != NULL))
							{
								ilLc--;
								prlAFlight	 = NULL;
								blRDeparture = true;
							}
						}
					}
				}
			}
		}
		ilLineNo = MakeLine(prlAFlight, prlDFlight);

	}


}

		

int ReportSameDCDTableViewer::MakeLine(ROTATIONDLGFLIGHTDATA *prpAFlight, ROTATIONDLGFLIGHTDATA *prpDFlight)
{
    SAMEDCDTABLE_LINEDATA rlLine;

	if((prpAFlight != NULL) && (prpDFlight != NULL) )
	{
		MakeLineData(prpAFlight, NULL, rlLine);
		CreateLine(rlLine);
		MakeLineData(NULL, prpDFlight, rlLine);
		return CreateLine(rlLine);
	}
	if (prpAFlight != NULL)
	{
		MakeLineData(prpAFlight, NULL, rlLine);
		return CreateLine(rlLine);
	}
	if (prpDFlight != NULL)
	{
		MakeLineData(NULL, prpDFlight, rlLine);
		return CreateLine(rlLine);
	}
	return -1;
}




void ReportSameDCDTableViewer::MakeLineData(ROTATIONDLGFLIGHTDATA *prpAFlight, ROTATIONDLGFLIGHTDATA *prpDFlight, SAMEDCDTABLE_LINEDATA &rpLine)
{

	CString olStr;
	if(prpAFlight != NULL)
	{
		rpLine.Adid = "A";
		rpLine.AUrno =  prpAFlight->Urno;
		rpLine.ARkey =  prpAFlight->Rkey;
		rpLine.AFlno = CString(prpAFlight->Flno);
		rpLine.AStoa = prpAFlight->Stoa; 
		if(bgReportLocal)
			ogBasicData.UtcToLocal(rpLine.AStoa);
		rpLine.ADate = prpAFlight->Stoa.Format("%d.%m.%y");
		rpLine.AOrg3 = CString(prpAFlight->Org3);
		rpLine.AOrg4 = CString(prpAFlight->Org4);
		rpLine.AAct  = CString(prpAFlight->Act3);
		rpLine.ALand = CTime(prpAFlight->Onbl); 
		if(bgReportLocal) 
			ogBasicData.UtcToLocal(rpLine.ALand);

		CCSPtrArray<VIADATA> opVias;
		int ilViaCnt = GetVia(&opVias, prpAFlight);

		if(opVias.GetSize() == 0)
			rpLine.AVia3 = "";
		else
		{
			rpLine.AVia3 =  opVias[ilViaCnt-1].Apc3;
		}
		opVias.DeleteAll();
	}
	else
	{

		rpLine.AUrno =  0;
		rpLine.ARkey =  0;
		rpLine.AFlno = "";
		rpLine.AStoa = TIMENULL; 
		rpLine.ALand = TIMENULL; 
		rpLine.AEtai = TIMENULL; 
		rpLine.ADate = ""; 
		rpLine.AOrg3 = "";
		rpLine.AOrg4 = "";
		rpLine.AVia3 = "";
		rpLine.AAct = "";
	}


	if(prpDFlight != NULL)
	{
		rpLine.Adid = "D";

		rpLine.DUrno =  prpDFlight->Urno;
		rpLine.DRkey =  prpDFlight->Rkey;
		rpLine.DFlno = CString(prpDFlight->Flno); 
		rpLine.DDes3 = CString(prpDFlight->Des3);
		rpLine.DDes4 = CString(prpDFlight->Des4);
		rpLine.DAct  = CString(prpDFlight->Act3);

		rpLine.DAirb = CTime(prpDFlight->Ofbl);
		if(bgReportLocal) 
			ogBasicData.UtcToLocal(rpLine.DAirb);
		rpLine.DEtdi = CTime(prpDFlight->Etdi);
		if(bgReportLocal) 
			ogBasicData.UtcToLocal(rpLine.DEtdi);
		rpLine.DStod = prpDFlight->Stod;
		if(bgReportLocal)
			ogBasicData.UtcToLocal(rpLine.DStod);
		rpLine.DDate = prpDFlight->Stod.Format("%d.%m.%y");
		rpLine.DVian = prpDFlight->Vian;

		if (rpLine.AAct.IsEmpty())
			rpLine.AAct = rpLine.DAct;


		CCSPtrArray<VIADATA> opVias;
		int ilViaCnt = GetVia(&opVias, prpDFlight);

		if(opVias.GetSize() == 0)
			rpLine.DVia3 = "";
		else
			rpLine.DVia3 =  opVias[0].Apc3;	// erster Via nach Homeairport = erste Zeile in Vial
		opVias.DeleteAll();

	
	}
	else
	{
		rpLine.DUrno =  0;
		rpLine.DRkey =  0;
		rpLine.DEtdi = TIMENULL; 
		rpLine.DAirb = TIMENULL; 
		rpLine.DFlno = "";
		rpLine.DDate = ""; 
		rpLine.DDes3 = "";
		rpLine.DDes4 = "";
		rpLine.DVia3 = "";
		rpLine.DAct = "";
	}

    return;

}



int ReportSameDCDTableViewer::CreateLine(SAMEDCDTABLE_LINEDATA &rpLine)
{
    int ilLineCount = omLines.GetSize();

    for (int ilLineno = ilLineCount; ilLineno > 0; ilLineno--)
        if (CompareFlight(&rpLine, &omLines[ilLineno-1]) >= 0)
            break;  // should be inserted after Lines[ilLineno-1]

    omLines.NewAt(ilLineno, rpLine);
	return ilLineno;
}


void ReportSameDCDTableViewer::DeleteLine(int ipLineno)
{
	omLines.DeleteAt(ipLineno);
}



void ReportSameDCDTableViewer::DeleteAll()
{
    while (omLines.GetSize() > 0)
        DeleteLine(0);
}


/////////////////////////////////////////////////////////////////////////////
// ReportSameDCDTableViewer - display drawing routine



// UpdateDisplay: Load data selected by filter conditions to the display by using "omTable"
void ReportSameDCDTableViewer::UpdateDisplay()
{
	pomTable->ResetContent();

	DrawHeader();

	SAMEDCDTABLE_LINEDATA *prlLine;
    
	for (int ilLc = 0; ilLc < omLines.GetSize(); ilLc++)
	{
		int ilColumnNo = 0;
		prlLine = &omLines[ilLc];
		CCSPtrArray<TABLE_COLUMN> olColList;
	
		MakeColList(prlLine, olColList);

		pomTable->AddTextLine(olColList, (void*)(prlLine));
		olColList.DeleteAll();
	}


    pomTable->DisplayTable();
}




void ReportSameDCDTableViewer::DrawHeader()
{
	int  ilTotalLines = 0;
	bool blNewLogicLine = false;

	CCSPtrArray <TABLE_HEADER_COLUMN> omHeaderDataArray;

	pomTable->SetShowSelection(true);
	pomTable->ResetContent();
	
	TABLE_HEADER_COLUMN *prlHeader[7];

	prlHeader[0] = new TABLE_HEADER_COLUMN;
	prlHeader[0]->Alignment = COLALIGN_CENTER;
	prlHeader[0]->Length = 67; 
	prlHeader[0]->Font = &ogCourier_Bold_10;
	prlHeader[0]->Text = GetString(IDS_STRING1078);//FLIGHT

	prlHeader[1] = new TABLE_HEADER_COLUMN;
	prlHeader[1]->Alignment = COLALIGN_CENTER;
	prlHeader[1]->Length = 80;
	prlHeader[1]->Font = &ogCourier_Bold_10;
	prlHeader[1]->Text = GetString(IDS_STRING1584);//TYPE

	prlHeader[2] = new TABLE_HEADER_COLUMN;
	prlHeader[2]->Alignment = COLALIGN_CENTER;
	prlHeader[2]->Length = 60; 
	prlHeader[2]->Font = &ogCourier_Bold_10;
	prlHeader[2]->Text = GetString(IDS_STRING1585);// ORG/DES

	prlHeader[3] = new TABLE_HEADER_COLUMN;
	prlHeader[3]->Alignment = COLALIGN_CENTER;
	prlHeader[3]->Length = 70; 
	prlHeader[3]->Font = &ogCourier_Bold_10;
	prlHeader[3]->Text = GetString(IDS_STRING332);//Date

	prlHeader[4] = new TABLE_HEADER_COLUMN;
	prlHeader[4]->Alignment = COLALIGN_CENTER;
	prlHeader[4]->Length = 45; 
	prlHeader[4]->Font = &ogCourier_Bold_10;
	prlHeader[4]->Text = GetString(IDS_STRING1421);//Schedule

	prlHeader[5] = new TABLE_HEADER_COLUMN;
	prlHeader[5]->Alignment = COLALIGN_CENTER;
	prlHeader[5]->Length = 45; 
	prlHeader[5]->Font = &ogCourier_Bold_10;
	prlHeader[5]->Text = GetString(IDS_STRING1586);//Actual

	prlHeader[6] = new TABLE_HEADER_COLUMN;
	prlHeader[6]->Alignment = COLALIGN_CENTER;
	prlHeader[6]->Length = 45; 
	prlHeader[6]->Font = &ogCourier_Bold_10;
	prlHeader[6]->Text = GetString(IDS_STRING1569);//AC-Type

	for(int ili = 0; ili < 7; ili++)
	{
		omHeaderDataArray.Add(prlHeader[ili]);
	}

	pomTable->SetHeaderFields(omHeaderDataArray);
	omHeaderDataArray.DeleteAll();

	pomTable->SetDefaultSeparator();

}



void ReportSameDCDTableViewer::MakeColList(SAMEDCDTABLE_LINEDATA *prlLine, CCSPtrArray<TABLE_COLUMN> &olColList)
{

	TABLE_COLUMN rlColumnData;

	rlColumnData.VerticalSeparator = SEPA_NONE;
	rlColumnData.SeparatorType = SEPA_NONE;
	rlColumnData.Font = &ogCourier_Regular_9;

	rlColumnData.Alignment = COLALIGN_LEFT;
	if ((strcmp (prlLine->Adid, "A")) == 0)
		rlColumnData.Text = prlLine->AFlno;
	if ((strcmp (prlLine->Adid, "D")) == 0)
		rlColumnData.Text = prlLine->DFlno;
	olColList.NewAt(olColList.GetSize(), rlColumnData);

	rlColumnData.Alignment = COLALIGN_LEFT;
	if ((strcmp (prlLine->Adid, "A")) == 0)
		rlColumnData.Text = "Arrival";
	if ((strcmp (prlLine->Adid, "D")) == 0)
		rlColumnData.Text = "Departure";
	olColList.NewAt(olColList.GetSize(), rlColumnData);

	rlColumnData.Alignment = COLALIGN_LEFT;
	if ((strcmp (prlLine->Adid, "A")) == 0)
		rlColumnData.Text = prlLine->AOrg3;
	if ((strcmp (prlLine->Adid, "D")) == 0)
		rlColumnData.Text = prlLine->DDes3;
	olColList.NewAt(olColList.GetSize(), rlColumnData);

	rlColumnData.Alignment = COLALIGN_LEFT;
	if ((strcmp (prlLine->Adid, "A")) == 0)
		rlColumnData.Text = prlLine->ADate;
	if ((strcmp (prlLine->Adid, "D")) == 0)
		rlColumnData.Text = prlLine->DDate;
	olColList.NewAt(olColList.GetSize(), rlColumnData);

	rlColumnData.Alignment = COLALIGN_LEFT;
	if ((strcmp (prlLine->Adid, "A")) == 0)
		rlColumnData.Text = prlLine->AStoa.Format("%H:%M");
	if ((strcmp (prlLine->Adid, "D")) == 0)
		rlColumnData.Text = prlLine->DStod.Format("%H:%M");
	olColList.NewAt(olColList.GetSize(), rlColumnData);

	rlColumnData.Alignment = COLALIGN_LEFT;
	if ((strcmp (prlLine->Adid, "A")) == 0)
		rlColumnData.Text = prlLine->ALand.Format("%H:%M");
	if ((strcmp (prlLine->Adid, "D")) == 0)
		rlColumnData.Text = prlLine->DAirb.Format("%H:%M");
	olColList.NewAt(olColList.GetSize(), rlColumnData);

	rlColumnData.Alignment = COLALIGN_LEFT;
	if ((strcmp (prlLine->Adid, "A")) == 0)
		rlColumnData.Text = prlLine->AAct;
	if ((strcmp (prlLine->Adid, "D")) == 0)
		rlColumnData.Text = prlLine->DAct;
	olColList.NewAt(olColList.GetSize(), rlColumnData);

}




int ReportSameDCDTableViewer::CompareFlight(SAMEDCDTABLE_LINEDATA *prpLine1, SAMEDCDTABLE_LINEDATA *prpLine2)
{

	// Sort fields order: Type, Date, Schedule

	if (prpLine1->Adid == prpLine2->Adid)
	{	
	 	CTime *polTime1;
		CTime *polTime2;
		CString *polDate1;
		CString *polDate2;

		if (prpLine1->Adid == "A")
		{
			polDate1 = &prpLine1->ADate;
			polTime1 = &prpLine1->AStoa;
		}
		else
		{
			polDate1 = &prpLine1->DDate;
			polTime1 = &prpLine1->DStod;
		}
		if (prpLine2->Adid == "A")
		{
			polDate2 = &prpLine2->ADate;
			polTime2 = &prpLine2->AStoa;
		}
		else
		{
			polDate2 = &prpLine2->DDate;
			polTime2 = &prpLine2->DStod;
		}

		if (*polDate1 == *polDate2)
		{
			return (*polTime1 == *polTime2)? 0:
				(*polTime1 > *polTime2)? 1: -1;
		}
		else
		{
			return strcmp(*polDate1, *polDate2);
		}
	}
	else
	{
		if (prpLine1->Adid == "A")
			return -1;
		else
			return 1;
	}


}



//-----------------------------------------------------------------------------------------------
// Printing Routines
//-----------------------------------------------------------------------------------------------


void ReportSameDCDTableViewer::GetHeader()
{
		
	omPrintHeadHeaderArray.DeleteAll();  // BWi

	TABLE_HEADER_COLUMN *prlHeader[7];

	prlHeader[0] = new TABLE_HEADER_COLUMN;
	prlHeader[0]->Alignment = COLALIGN_CENTER;
	prlHeader[0]->Length = 67; 
	prlHeader[0]->Font = &ogCourier_Bold_10;
	prlHeader[0]->Text = GetString(IDS_STRING1078);//FLIGHT

	prlHeader[1] = new TABLE_HEADER_COLUMN;
	prlHeader[1]->Alignment = COLALIGN_CENTER;
	prlHeader[1]->Length = 80;
	prlHeader[1]->Font = &ogCourier_Bold_10;
	prlHeader[1]->Text = GetString(IDS_STRING1584);//TYPE

	prlHeader[2] = new TABLE_HEADER_COLUMN;
	prlHeader[2]->Alignment = COLALIGN_CENTER;
	prlHeader[2]->Length = 60; 
	prlHeader[2]->Font = &ogCourier_Bold_10;
	prlHeader[2]->Text = GetString(IDS_STRING1585);// ORG/DES

	prlHeader[3] = new TABLE_HEADER_COLUMN;
	prlHeader[3]->Alignment = COLALIGN_CENTER;
	prlHeader[3]->Length = 70; 
	prlHeader[3]->Font = &ogCourier_Bold_10;
	prlHeader[3]->Text = GetString(IDS_STRING332);//Date

	prlHeader[4] = new TABLE_HEADER_COLUMN;
	prlHeader[4]->Alignment = COLALIGN_CENTER;
	prlHeader[4]->Length = 45; 
	prlHeader[4]->Font = &ogCourier_Bold_10;
	prlHeader[4]->Text = GetString(IDS_STRING1421);//Schedule

	prlHeader[5] = new TABLE_HEADER_COLUMN;
	prlHeader[5]->Alignment = COLALIGN_CENTER;
	prlHeader[5]->Length = 45; 
	prlHeader[5]->Font = &ogCourier_Bold_10;
	prlHeader[5]->Text = GetString(IDS_STRING1586);//Actual

	prlHeader[6] = new TABLE_HEADER_COLUMN;
	prlHeader[6]->Alignment = COLALIGN_CENTER;
	prlHeader[6]->Length = 45; 
	prlHeader[6]->Font = &ogCourier_Bold_10;
	prlHeader[6]->Text = GetString(IDS_STRING1569);//AC-Type

	for(int ili = 0; ili < 7; ili++)
	{
		omPrintHeadHeaderArray.Add( prlHeader[ili]);
	}

}



void ReportSameDCDTableViewer::PrintTableView()
{
	CString olFooter1,olFooter2;
	char pclFooter[256];

	sprintf(pclFooter, GetString(IDS_STRING1620), (CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M")));

	omFooterName = pclFooter;
	CString olTableName = GetString(IDS_STRING1620);
	int ilOrientation = PRINT_LANDSCAPE;//  PRINT_PORTRAET  PRINT_LANDSCAPE
	pomPrint = new CCSPrint(pomParentDlg,ilOrientation,45);

	if (pomPrint != NULL)
	{
		if (pomPrint->InitializePrinter(ilOrientation) == TRUE)
		{
			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
			pomPrint->imLineNo = pomPrint->imMaxLines + 1;
			DOCINFO	rlDocInfo;
			memset(&rlDocInfo, 0, sizeof(DOCINFO));
			rlDocInfo.cbSize = sizeof( DOCINFO );
			rlDocInfo.lpszDocName = olTableName;	
			//rlDocInfo.lpszDocName = TABLENAME;	
			pomPrint->omCdc.StartDoc( &rlDocInfo );
			pomPrint->imPageNo = 0;
			int ilLines = omLines.GetSize();  
			olFooter1.Format(GetString(IDS_STRING1445), ilLines,omFooterName);

			GetHeader();

			if (ilLines == 0)
				PrintTableHeader(); // Drucken leerer Tabelle
			for(int i = 0; i < ilLines; i++ ) 
			{
				if(pomPrint->imLineNo >= pomPrint->imMaxLines)
				{
					if(pomPrint->imPageNo > 0)
					{
						olFooter2.Format(GetString(IDS_STRING1198),pomPrint->imPageNo);
						pomPrint->PrintUIFFooter(olFooter1,"",olFooter2);
						pomPrint->omCdc.EndPage();
					}
					PrintTableHeader();
				}
				if(pomPrint->imLineNo == (pomPrint->imMaxLines-1) || i == (ilLines-1))
				{
					PrintTableLine(&omLines[i],true);
				}
				else
				{
					PrintTableLine(&omLines[i],false);
				}
			}
			olFooter2.Format(GetString(IDS_STRING1198),pomPrint->imPageNo);
			pomPrint->PrintUIFFooter(olFooter1,"",olFooter2);
			pomPrint->omCdc.EndPage();
			pomPrint->omCdc.EndDoc();
		}

		omPrintHeadHeaderArray.DeleteAll();
		SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
		delete pomPrint;
		pomPrint = NULL;
	}
}

//-----------------------------------------------------------------------------------------------

bool ReportSameDCDTableViewer::PrintTableHeader()
{
	pomPrint->omCdc.StartPage();
	pomPrint->imPageNo++;
	pomPrint->imLineNo = 0;
	double dgCCSPrintFactor = 2.7 ;
	char pclHeader[200];

	sprintf(pclHeader, GetString(IDS_STRING1617), pcmSelect, omDname.GetBuffer(0), pcmInfo, 
		    pomData->GetSize());
	CString olTableName(pclHeader);

	//pomPrint->PrintUIFHeader(omTableName,CString(pclHeader), pomPrint->imFirstLine-10);
	pomPrint->PrintUIFHeader(CString(""),CString(""), pomPrint->imFirstLine-10);

	CFont *polOldFont;
	CPen  *polOldPen; 
	CSize olSize;

	int ilHorzRes = pomPrint->omCdc.GetDeviceCaps(HORZRES);
	polOldPen = pomPrint->omCdc.SelectObject(&pomPrint->ThinPen);

	#define MMX(x)    ((int)(MulDiv((x),pomPrint->omCdc.GetDeviceCaps(LOGPIXELSX), 72)*0.284))
	#define MMY(x)    ((int)(MulDiv((x),pomPrint->omCdc.GetDeviceCaps(LOGPIXELSY), 72)*0.284))
			
	pomPrint->omCdc.MoveTo(MMX(pomPrint->imLeftOffset),MMY(pomPrint->imFirstLine-10));
	pomPrint->omCdc.LineTo(ilHorzRes-MMX(20),MMY(pomPrint->imFirstLine-10));

	polOldFont = pomPrint->omCdc.SelectObject(&pomPrint->omMediumFont_Bold);
	
	olSize = pomPrint->omCdc.GetTextExtent(pclHeader);
	pomPrint->omCdc.SelectObject(&pomPrint->omMediumFont_Bold);
	CRect olRect;
	
	if (olSize.cx > ilHorzRes-MMX(20))
	{
		olRect.top = olSize.cy * 2;
		olRect.right = ilHorzRes-MMX(20);
		pomPrint->omCdc.DrawText(CString(pclHeader), olRect, DT_LEFT|DT_WORDBREAK);
	}
	else
	{
		pomPrint->PrintUIFHeader(omTableName,CString(pclHeader), pomPrint->imFirstLine-10);
	}

	pomPrint->omCdc.SelectObject(&pomPrint->omMediumFont_Bold);
	pomPrint->omCdc.TextOut(MMX(100+50)+olSize.cx,MMY(pomPrint->imFirstLine-10-65), omTableName, strlen(omTableName));

	pomPrint->omCdc.SelectObject(polOldFont);
	pomPrint->omCdc.SelectObject(polOldPen);



	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;

	rlElement.Alignment   = PRINT_LEFT;
	rlElement.FrameLeft   = PRINT_NOFRAME;
	rlElement.FrameRight  = PRINT_NOFRAME;
	rlElement.FrameTop    = PRINT_NOFRAME;
	rlElement.FrameBottom = PRINT_FRAMEMEDIUM;
	rlElement.pFont       = &pomPrint->ogCourierNew_Bold_8;

	int ilSize = omPrintHeadHeaderArray.GetSize();

	for(int i = 0; i < ilSize; i++)
	{
		//rlElement.Length = (int)(omHeaderDataArray[i].Length*dgCCSPrintFactor);
		rlElement.Length = (int)(omPrintHeadHeaderArray[i].Length*dgCCSPrintFactor);
		if(rlElement.Length < igCCSPrintMinLength)rlElement.Length+=igCCSPrintMoreLength; 
		//rlElement.Text   = omHeaderDataArray[i].Text;
		rlElement.Text   = omPrintHeadHeaderArray[i].Text;
		rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
	}
	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();


	return true;
}

//-----------------------------------------------------------------------------------------------

bool ReportSameDCDTableViewer::PrintTableLine(SAMEDCDTABLE_LINEDATA *prpLine,bool bpLastLine)
{
	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;
	double dgCCSPrintFactor = 2.7 ;
	rlElement.pFont      = &pomPrint->ogCourierNew_Regular_8;
	int ilSize = omPrintHeadHeaderArray.GetSize();

	for(int i=0;i<ilSize;i++)
	{
		rlElement.Length = (int)(omPrintHeadHeaderArray[i].Length*dgCCSPrintFactor);
		if(rlElement.Length < igCCSPrintMinLength)rlElement.Length+=igCCSPrintMoreLength; 
		rlElement.Alignment  = PRINT_LEFT;
		rlElement.FrameTop   = PRINT_FRAMETHIN;
		if(bpLastLine == true)
		{
			rlElement.FrameBottom = PRINT_FRAMETHIN;
		}
		else
		{
			rlElement.FrameBottom = PRINT_NOFRAME;
		}
		rlElement.FrameLeft  = PRINT_FRAMETHIN;
		rlElement.FrameRight = PRINT_NOFRAME;


		switch(i)
		{
			case 0:
			{
				if ((strcmp (prpLine->Adid, "A")) == 0)
					rlElement.Text = prpLine->AFlno;
				if ((strcmp (prpLine->Adid, "D")) == 0)
					rlElement.Text = prpLine->DFlno;
			}
			break;
			case 1:
			{
				if ((strcmp (prpLine->Adid, "A")) == 0)
					rlElement.Text = "Arrival";
				if ((strcmp (prpLine->Adid, "D")) == 0)
					rlElement.Text = "Departure";
			}
			break;
			case 2:
			{
				if ((strcmp (prpLine->Adid, "A")) == 0)
					rlElement.Text = prpLine->AOrg3;
				if ((strcmp (prpLine->Adid, "D")) == 0)
					rlElement.Text = prpLine->DDes3;
			}
			break;
			case 3:
			{
				if ((strcmp (prpLine->Adid, "A")) == 0)
					rlElement.Text = prpLine->ADate;
				if ((strcmp (prpLine->Adid, "D")) == 0)
					rlElement.Text = prpLine->DDate;
			}
			break;
			case 4:
			{
				if ((strcmp (prpLine->Adid, "A")) == 0)
					rlElement.Text = prpLine->AStoa.Format("%H:%M");
				if ((strcmp (prpLine->Adid, "D")) == 0)
					rlElement.Text = prpLine->DStod.Format("%H:%M");
			}
			break;
			case 5:
			{
				if ((strcmp (prpLine->Adid, "A")) == 0)
					rlElement.Text = prpLine->ALand.Format("%H:%M");
				if ((strcmp (prpLine->Adid, "D")) == 0)
					rlElement.Text = prpLine->DAirb.Format("%H:%M");
			}
			break;
			case 6:
			{
				if ((strcmp (prpLine->Adid, "A")) == 0)
					rlElement.Text = prpLine->AAct;
				if ((strcmp (prpLine->Adid, "D")) == 0)
					rlElement.Text = prpLine->DAct;
				rlElement.FrameRight = PRINT_FRAMETHIN;
			}
			break;
		}

		rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
		
	}
	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();
	return true;
}

//-----------------------------------------------------------------------------------------------


// Drucken in Datei
void ReportSameDCDTableViewer::PrintPlanToFile(char *pcpDefPath)
{

	CCS_TRY

	char pclHeadlineSelect[200];
	sprintf(pclHeadlineSelect, GetString(IDS_STRING1617), pcmSelect, omDname.GetBuffer(0), 
		    pcmInfo, pomData->GetSize());

	ofstream of;
	of.open( pcpDefPath, ios::out);
	of << CString(pclHeadlineSelect) << " "
	   << CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M") << endl << endl;

	of << GetString(IDS_STRING1078) << "     "<< GetString(IDS_STRING1584) << "     " 
	   << GetString(IDS_STRING1585)  << "    "  << GetString(IDS_STRING332) << "    " 
	   << GetString(IDS_STRING1421) << "   " << GetString(IDS_STRING1586) << "   " 
	   << GetString(IDS_STRING1569)<< endl;
	       
	of << "------------------------------------------------------------------" << endl;
	int ilCount = omLines.GetSize();


	CStringArray olDaten;
	for(int i = 0; i < ilCount; i++)
	{
		SAMEDCDTABLE_LINEDATA rlD = omLines[i];

		if ((strcmp (rlD.Adid, "A")) == 0)
		{
			olDaten.Add(rlD.AFlno);
			olDaten.Add("Arrival");
			olDaten.Add(rlD.AOrg3);
			olDaten.Add(rlD.ADate);
			olDaten.Add(rlD.AStoa.Format("%H:%M"));
			olDaten.Add(rlD.ALand.Format("%H:%M"));
			olDaten.Add(rlD.AAct);
		}
		if ((strcmp (rlD.Adid, "D")) == 0)
		{
			olDaten.Add(rlD.DFlno);
			olDaten.Add("Departure");
			olDaten.Add(rlD.DDes3);
			olDaten.Add(rlD.DDate);
			olDaten.Add(rlD.DStod.Format("%H:%M"));
			olDaten.Add(rlD.DAirb.Format("%H:%M"));
			olDaten.Add(rlD.DAct);
		}

		of.setf(ios::left, ios::adjustfield);
		of   << setw(10) << olDaten[0].GetBuffer(0)
			 << setw(12) << olDaten[1].GetBuffer(0)
			 << setw(8) << olDaten[2].GetBuffer(0)
			 << setw(11) << olDaten[3].GetBuffer(0)
			 << setw(9) << olDaten[4].GetBuffer(0)
			 << setw(10) << olDaten[5].GetBuffer(0)
			 << setw(9) << olDaten[6].GetBuffer(0)
			 << endl;
		
		olDaten.RemoveAll();
	}
	of.close();

	CCS_CATCH_ALL

}
