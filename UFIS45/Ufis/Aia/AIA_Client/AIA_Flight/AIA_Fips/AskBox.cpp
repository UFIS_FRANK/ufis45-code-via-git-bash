// AskBox.cpp : implementation file
//

#include "stdafx.h"
#include "fpms.h"
#include "AskBox.h"
#include "BasicData.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// AskBox dialog


//AskBox::AskBox(CWnd* pParent , CString opButton1Text, CString opButton2Text, CString opMessage, CString opCaption)
AskBox::AskBox(CWnd* pParent , CString opButton1Text, CString opButton2Text, CString opMessage, CString opCaption, CString opCancelText)
	: CDialog(AskBox::IDD, pParent)
{
	omButtonText1 = opButton1Text;
	omButtonText2 = opButton2Text;
	omMessage = opMessage;
	omCaption = opCaption;
	omCancelText = opCancelText;
	

	//{{AFX_DATA_INIT(AskBox)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


BOOL AskBox::OnInitDialog() 
{
/*
	CDialog::OnInitDialog();
	
	m_CB_Button1.SetWindowText(omButtonText1);
	m_CB_Button2.SetWindowText(omButtonText2);
	m_CS_Text.SetWindowText(omMessage);

	CWnd* plCWnd= GetDlgItem(IDCANCEL);
	if(plCWnd)
		plCWnd->SetWindowText(omCancelText);

	SetWindowText(omCaption);

	CFPMSApp::UnsetTopmostWnds();

	CWnd* plParent = GetParent();
	if (plParent)
	{
		CRect r;
		GetParent()->GetClientRect(r);
		GetParent()->ClientToScreen(r);
		SetWindowPos( &wndTop, r.left, r.top, 0, 0, SWP_NOSIZE | SWP_SHOWWINDOW);
	}
	else
		SetWindowPos( &wndTop, 400, 300,0,0, SWP_NOSIZE | SWP_SHOWWINDOW);

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
*/
	CDialog::OnInitDialog();

	// get height of window
	CRect olWndRect;
	GetClientRect(olWndRect);
	int ilWndHeight = olWndRect.bottom;

	// get height of cancel button
	CWnd* m_CB_Cancel = GetDlgItem(IDCANCEL);
	ASSERT(m_CB_Cancel);
	CRect olCancelRect;
	m_CB_Cancel->GetClientRect(olCancelRect);

	//// calculate button widths
	CSize olButton1Size;
	CSize olButton2Size;

	CDC *polDC = GetDC();
	CString olText;
	olText.Format(" %s ", omButtonText1);
	olButton1Size = polDC->GetTextExtent(olText);

	olText.Format(" %s ", omButtonText2);
	olButton2Size = polDC->GetTextExtent(olText);

	//// set button size and pos
	m_CB_Button1.SetWindowPos(NULL, 10, ilWndHeight-10-olCancelRect.Height(), 
		olButton1Size.cx, olCancelRect.Height(), SWP_NOZORDER);
		
	m_CB_Button2.SetWindowPos(NULL, 10+olButton1Size.cx+10, ilWndHeight-10-olCancelRect.Height(), 
		olButton2Size.cx, olCancelRect.Height(), SWP_NOZORDER);

	// calculate width of the window
	int ilWndWidth = 10+olButton1Size.cx+10+olButton2Size.cx+20+olCancelRect.Width()+10;
	int ilMessWidth = GetMessageWidth(polDC);
	if (ilMessWidth + 10 > ilWndWidth)
		ilWndWidth = ilMessWidth + 10;

	// set cancel button pos
	m_CB_Cancel->SetWindowPos(NULL, ilWndWidth-10-olCancelRect.Width(), ilWndHeight-10-olCancelRect.Height(), 
		0, 0, SWP_NOZORDER | SWP_NOSIZE);


	// set button text
	m_CB_Button1.SetWindowText(omButtonText1);
	m_CB_Button2.SetWindowText(omButtonText2);
	m_CS_Text.SetWindowText(omMessage);
	m_CB_Cancel->SetWindowText(omCancelText);
	SetWindowText(omCaption);

	// set window size and pos
	GetWindowRect(olWndRect);
	this->SetWindowPos( NULL, 400, 300, ilWndWidth ,olWndRect.Height(), SWP_NOZORDER);

	CFPMSApp::UnsetTopmostWnds();

	CPoint point;
	::GetCursorPos(&point);

	this->SetWindowPos(&wndTop, point.x, point.y,0,0, SWP_SHOWWINDOW | SWP_NOSIZE);// | SWP_NOMOVE );

/*
	CWnd* plParent = GetParent();
	if (plParent)
	{
		CRect r;
		GetParent()->GetClientRect(r);
		GetParent()->ClientToScreen(r);
		SetWindowPos( &wndTop, r.left, r.top, 0, 0, SWP_NOSIZE | SWP_SHOWWINDOW);
	}
	else
		SetWindowPos( &wndTop, 400, 300,0,0, SWP_NOSIZE | SWP_SHOWWINDOW);

*/

	return TRUE;  // return TRUE unless you set the focus to a control
	            // EXCEPTION: OCX Property Pages should return FALSE
}

int AskBox::GetMessageWidth(CDC *popDC) const
{
	if (popDC == NULL)
		return 0;

	int ilMaxWidth = -1;
	int ilCurrWidth;
	CStringArray olStrs;
	int ilStrs = ExtractItemList(omMessage, &olStrs, '\n');

	for (int i = 0; i < ilStrs; i++)
	{
		ilCurrWidth = popDC->GetTextExtent(olStrs[i]).cx;
		ilMaxWidth = max(ilMaxWidth, ilCurrWidth);
	}

	return ilMaxWidth;
}

void AskBox::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(AskBox)
	DDX_Control(pDX, IDC_BUTTON1, m_CB_Button1);
	DDX_Control(pDX, IDC_BUTTON2, m_CB_Button2);
	DDX_Control(pDX, IDC_TEXT, m_CS_Text);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(AskBox, CDialog)
	//{{AFX_MSG_MAP(AskBox)
	ON_BN_CLICKED(IDC_BUTTON1, OnButton1)
	ON_BN_CLICKED(IDC_BUTTON2, OnButton2)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// AskBox message handlers

void AskBox::OnButton1() 
{
	CFPMSApp::SetTopmostWnds();
	 EndDialog( 1 );
}

void AskBox::OnButton2() 
{
	CFPMSApp::SetTopmostWnds();
	 EndDialog( 2 );
}

void AskBox::OnCancel() 
{
	
	CFPMSApp::SetTopmostWnds();
	 EndDialog( 0 );
}

