// ReportSameALTableViewer.cpp : implementation file
// 
// List of all flights that the same AC-Type
// Angabe eines AC-Typen

#include "stdafx.h"
#include "ReportSameALTableViewer.h"
#include "CcsGlobl.h"
#include <iomanip.h>
#include <iostream.h>
#include <fstream.h>
#include "BasicData.h"

#include "ReportXXXCedaData.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif


/////////////////////////////////////////////////////////////////////////////
// ReportSameALTableViewer
//

int ReportSameALTableViewer::GetVia(CCSPtrArray<VIADATA> *opVias, ROTATIONDLGFLIGHTDATA *prpFlight)
{
	opVias->DeleteAll();

	if(prpFlight == NULL)
		return 0;
	if(strlen(prpFlight->Vial) == 0)
		return 0;

	CString olVias(prpFlight->Vial);
	CString olFids;
	CString olApc3;
	CString olApc4;

	VIADATA *prlVia;

	while(olVias.IsEmpty() != TRUE)
	{
			prlVia = new VIADATA;
			opVias->Add(prlVia);

			if(olVias.GetLength() < 120)
			{
				olVias += "                                                                                                                             ";
				olVias = olVias.Left(120);
			}


			olApc3 = olVias.Mid(1,3);
			olApc3.TrimLeft();
			sprintf(prlVia->Apc3, olApc3);

			if(olVias.GetLength() >= 120)
				olVias = olVias.Right(olVias.GetLength() - 120);

	}
	return opVias->GetSize();
}



ReportSameALTableViewer::ReportSameALTableViewer(CCSPtrArray<ROTATIONDLGFLIGHTDATA> *popData, char *pcpInfo, char *pcpSelect)
{
	pomData = popData;
	pcmInfo = pcpInfo;
	pcmSelect = pcpSelect;


	bmIsFromSearch = false;
    pomTable = NULL;
}

ReportSameALTableViewer::~ReportSameALTableViewer()
{
	omPrintHeadHeaderArray.DeleteAll();  // BWi
    DeleteAll();
}


void ReportSameALTableViewer::SetParentDlg(CDialog* ppParentDlg)
{

	pomParentDlg = ppParentDlg;

}


void ReportSameALTableViewer::Attach(CCSTable *popTable)
{
    pomTable = popTable;
}


CString ReportSameALTableViewer::GetAlfn()
{

	return omAlfn;

}
	

void ReportSameALTableViewer::ChangeViewTo(const char *pcpViewName)
{

    pomTable->ResetContent();
    DeleteAll();    
    MakeLines();

	UpdateDisplay();
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));

	// holen des Klartextes des Selects (aft.ttyp nat.tnam �ber nat.ttyp)
//	omAlfn = omReport1AltCedaData.ReadAlfn(CString(pcmSelect));

	ReportXXXCedaData olReportXXXCedaData("ALC2", "ALFN", "ALT");
	omAlfn = olReportXXXCedaData.ReadCleartext(CString(pcmSelect));

}


/////////////////////////////////////////////////////////////////////////////
// ReportSameALTableViewer -- code specific to this class

void ReportSameALTableViewer::MakeLines()
{
	ROTATIONDLGFLIGHTDATA *prlAFlight;
	ROTATIONDLGFLIGHTDATA *prlDFlight;
	ROTATIONDLGFLIGHTDATA *prlNextFlight = NULL;
	ROTATIONDLGFLIGHTDATA *prlFlight;
	int ilLineNo;

	bool blRDeparture = false;

	int ilFlightCount = pomData->GetSize();

	for (int ilLc = 0; ilLc < ilFlightCount; ilLc++)
	{
		prlFlight = &(*pomData)[ilLc];

		if(ilLc + 1 < ilFlightCount)
			prlNextFlight = &(*pomData)[ilLc + 1];	
		else
			prlNextFlight = NULL;	

		//Arrival
		if((strcmp(prlFlight->Org3, pcgHome) != 0) && (strcmp(prlFlight->Des3, pcgHome) == 0))
		{
			prlAFlight = prlFlight;
			prlDFlight = NULL;
			if(prlNextFlight != NULL)
			{
				if((prlNextFlight->Rkey == prlFlight->Rkey) && (prlFlight->Rkey != NULL))
				{
					prlDFlight = prlNextFlight;

					if((strcmp(prlNextFlight->Org3, pcgHome) == 0) && (strcmp(prlNextFlight->Des3, pcgHome) != 0))
					{
						ilLc++;
					}
					else
						blRDeparture = true;
				}
			}
		}
		else
		{
			// Departure
			if((strcmp(prlFlight->Org3, pcgHome) == 0) && (strcmp(prlFlight->Des3, pcgHome) != 0))
			{
				prlAFlight = NULL;
				prlDFlight = prlFlight;
				blRDeparture = false;
			}
			else
			{
				//Turnaround
				if((strcmp(prlFlight->Org3, pcgHome) == 0) && (strcmp(prlFlight->Des3, pcgHome) == 0))
				{
					if(blRDeparture)
					{
						blRDeparture = false;
						prlAFlight = prlFlight;
						prlDFlight = NULL;
						if(prlNextFlight != NULL)
						{
							if((prlNextFlight->Rkey == prlFlight->Rkey) && (prlFlight->Rkey != NULL))
							{
								prlDFlight = prlNextFlight;
								if((strcmp(prlNextFlight->Org3, pcgHome) == 0) && (strcmp(prlNextFlight->Des3, pcgHome) != 0))
								{
									ilLc++;
								}
								else
									blRDeparture = true;
							}
						}
					}
					else
					{
						prlAFlight		= prlFlight;
						prlDFlight		= prlFlight;
						if(prlNextFlight != NULL)
						{
							if((prlNextFlight->Rkey == prlFlight->Rkey) && (prlFlight->Rkey != NULL))
							{
								ilLc--;
								prlAFlight	 = NULL;
								blRDeparture = true;
							}
						}
					}
				}
			}
		}
		ilLineNo = MakeLine(prlAFlight, prlDFlight);

	}


}

		

int ReportSameALTableViewer::MakeLine(ROTATIONDLGFLIGHTDATA *prpAFlight, ROTATIONDLGFLIGHTDATA *prpDFlight)
{
    SAMEALTABLE_LINEDATA rlLine;

	if((prpAFlight != NULL) && (prpDFlight != NULL) )
	{
		MakeLineData(prpAFlight, NULL, rlLine);
		CreateLine(rlLine);
		MakeLineData(NULL, prpDFlight, rlLine);
		return CreateLine(rlLine);
	}
	if (prpAFlight != NULL)
	{
		MakeLineData(prpAFlight, NULL, rlLine);
		return CreateLine(rlLine);
	}
	if (prpDFlight != NULL)
	{
		MakeLineData(NULL, prpDFlight, rlLine);
		return CreateLine(rlLine);
	}
	return -1;
}




void ReportSameALTableViewer::MakeLineData(ROTATIONDLGFLIGHTDATA *prpAFlight, ROTATIONDLGFLIGHTDATA *prpDFlight, SAMEALTABLE_LINEDATA &rpLine)
{

	CString olStr;
	if(prpAFlight != NULL)
	{
		rpLine.Adid = "A";
		rpLine.AUrno =  prpAFlight->Urno;
		rpLine.ARkey =  prpAFlight->Rkey;
		rpLine.AFlno = CString(prpAFlight->Flno);
		rpLine.AStoa = prpAFlight->Stoa; 
		if(bgReportLocal)
			ogBasicData.UtcToLocal(rpLine.AStoa);
		rpLine.ADate = prpAFlight->Stoa.Format("%d.%m.%y");/*DateToHourDivString(prpAFlight->Onbl, prpAFlight->Stoa);*/
		rpLine.AOrg3 = CString(prpAFlight->Org3);
		rpLine.AOrg4 = CString(prpAFlight->Org4);
		rpLine.ATtyp = CString(prpAFlight->Ttyp);
		rpLine.AAct  = CString(prpAFlight->Act3);
		rpLine.ALand = CTime(prpAFlight->Land); 
		if(bgReportLocal) 
			ogBasicData.UtcToLocal(rpLine.ALand);

		CCSPtrArray<VIADATA> opVias;
		int ilViaCnt = GetVia(&opVias, prpAFlight);

		if(opVias.GetSize() == 0)
			rpLine.AVia3 = "";
		else
		{
			rpLine.AVia3 =  opVias[ilViaCnt-1].Apc3;	// letzter Via vor Homeairport = letzte Zeile in Vial
		}
		opVias.DeleteAll();
	}
	else
	{

		rpLine.AUrno =  0;
		rpLine.ARkey =  0;
		rpLine.AFlno = "";
		rpLine.AStoa = TIMENULL; 
		rpLine.ALand = TIMENULL; 
		rpLine.AEtai = TIMENULL; 
		rpLine.ATtyp = "";
		rpLine.ADate = ""; 
		rpLine.AOrg3 = "";
		rpLine.AOrg4 = "";
		rpLine.AVia3 = "";
		rpLine.AAct = "";
	}


	if(prpDFlight != NULL)
	{
		rpLine.Adid = "D";

		rpLine.DUrno =  prpDFlight->Urno;
		rpLine.DRkey =  prpDFlight->Rkey;
		rpLine.DFlno = CString(prpDFlight->Flno); 
		rpLine.DDes3 = CString(prpDFlight->Des3);
		rpLine.DDes4 = CString(prpDFlight->Des4);
		rpLine.DTtyp = CString(prpDFlight->Ttyp);
		rpLine.DAct  = CString(prpDFlight->Act3);

		rpLine.DAirb = CTime(prpDFlight->Airb);
		if(bgReportLocal) 
			ogBasicData.UtcToLocal(rpLine.DAirb);
		rpLine.DEtdi = CTime(prpDFlight->Etdi);
		if(bgReportLocal) 
			ogBasicData.UtcToLocal(rpLine.DEtdi);
		rpLine.DStod = prpDFlight->Stod;
		if(bgReportLocal)
			ogBasicData.UtcToLocal(rpLine.DStod);
		rpLine.DDate = prpDFlight->Stod.Format("%d.%m.%y");
		rpLine.DVian = prpDFlight->Vian;

		if (rpLine.AAct.IsEmpty())
			rpLine.AAct = rpLine.DAct;


		CCSPtrArray<VIADATA> opVias;
		int ilViaCnt = GetVia(&opVias, prpDFlight);

		if(opVias.GetSize() == 0)
			rpLine.DVia3 = "";
		else
			rpLine.DVia3 =  opVias[0].Apc3;	// erster Via nach Homeairport = erste Zeile in Vial
		opVias.DeleteAll();

	
	}
	else
	{
		rpLine.DUrno =  0;
		rpLine.DRkey =  0;
		rpLine.DEtdi = TIMENULL; 
		rpLine.DAirb = TIMENULL; 
		rpLine.DStod = TIMENULL; 
		rpLine.DFlno = "";
		rpLine.DDate = ""; 
		rpLine.DDes3 = "";
		rpLine.DDes4 = "";
		rpLine.DTtyp = "";
		rpLine.DVia3 = "";
		rpLine.DAct = "";
	}

    return;

}



int ReportSameALTableViewer::CreateLine(SAMEALTABLE_LINEDATA &rpLine)
{
    int ilLineCount = omLines.GetSize();

    for (int ilLineno = ilLineCount; ilLineno > 0; ilLineno--)
        if (CompareFlight(&rpLine, &omLines[ilLineno-1]) >= 0)
            break;  // should be inserted after Lines[ilLineno-1]

    omLines.NewAt(ilLineno, rpLine);
	return ilLineno;
}


void ReportSameALTableViewer::DeleteLine(int ipLineno)
{
	omLines.DeleteAt(ipLineno);
}



void ReportSameALTableViewer::DeleteAll()
{
    while (omLines.GetSize() > 0)
        DeleteLine(0);
}


/////////////////////////////////////////////////////////////////////////////
// ReportSameALTableViewer - display drawing routine



// UpdateDisplay: Load data selected by filter conditions to the display by using "omTable"
void ReportSameALTableViewer::UpdateDisplay()
{
	pomTable->ResetContent();

	DrawHeader();

	SAMEALTABLE_LINEDATA *prlLine;
    
	for (int ilLc = 0; ilLc < omLines.GetSize(); ilLc++)
	{
		int ilColumnNo = 0;
		prlLine = &omLines[ilLc];
		CCSPtrArray<TABLE_COLUMN> olColList;
	
		MakeColList(prlLine, olColList);

		pomTable->AddTextLine(olColList, (void*)(prlLine));
		olColList.DeleteAll();
	}


    pomTable->DisplayTable();
}




void ReportSameALTableViewer::DrawHeader()
{
	int  ilTotalLines = 0;
	bool blNewLogicLine = false;

	CCSPtrArray <TABLE_HEADER_COLUMN> omHeaderDataArray;

	pomTable->SetShowSelection(true);
	pomTable->ResetContent();
	
	TABLE_HEADER_COLUMN *prlHeader[7];

	prlHeader[0] = new TABLE_HEADER_COLUMN;
	prlHeader[0]->Alignment = COLALIGN_CENTER;
	prlHeader[0]->Length = 67; 
	prlHeader[0]->Font = &ogCourier_Bold_10;
	prlHeader[0]->Text = GetString(IDS_STRING1078);//FLIGHT

	prlHeader[1] = new TABLE_HEADER_COLUMN;
	prlHeader[1]->Alignment = COLALIGN_CENTER;
	prlHeader[1]->Length = 60; 
	prlHeader[1]->Font = &ogCourier_Bold_10;
	prlHeader[1]->Text = GetString(IDS_STRING1584);// Type

	prlHeader[2] = new TABLE_HEADER_COLUMN;
	prlHeader[2]->Alignment = COLALIGN_CENTER;
	prlHeader[2]->Length = 70; 
	prlHeader[2]->Font = &ogCourier_Bold_10;
	prlHeader[2]->Text = GetString(IDS_STRING332);//Date

	prlHeader[3] = new TABLE_HEADER_COLUMN;
	prlHeader[3]->Alignment = COLALIGN_CENTER;
	prlHeader[3]->Length = 45; 
	prlHeader[3]->Font = &ogCourier_Bold_10;
	prlHeader[3]->Text = GetString(IDS_STRING1421);//Schedule

	prlHeader[4] = new TABLE_HEADER_COLUMN;
	prlHeader[4]->Alignment = COLALIGN_CENTER;
	prlHeader[4]->Length = 45; 
	prlHeader[4]->Font = &ogCourier_Bold_10;
	prlHeader[4]->Text = GetString(IDS_STRING1586);//Actual

	prlHeader[5] = new TABLE_HEADER_COLUMN;
	prlHeader[5]->Alignment = COLALIGN_CENTER;
	prlHeader[5]->Length = 45; 
	prlHeader[5]->Font = &ogCourier_Bold_10;
	prlHeader[5]->Text = GetString(IDS_STRING301);//Na (Nature,TTYP)

	prlHeader[6] = new TABLE_HEADER_COLUMN;
	prlHeader[6]->Alignment = COLALIGN_CENTER;
	prlHeader[6]->Length = 45; 
	prlHeader[6]->Font = &ogCourier_Bold_10;
	prlHeader[6]->Text = GetString(IDS_STRING299);//Via

	for(int ili = 0; ili < 7; ili++)
	{
		omHeaderDataArray.Add(prlHeader[ili]);
	}

	pomTable->SetHeaderFields(omHeaderDataArray);
	omHeaderDataArray.DeleteAll();

	pomTable->SetDefaultSeparator();

}



void ReportSameALTableViewer::MakeColList(SAMEALTABLE_LINEDATA *prlLine, CCSPtrArray<TABLE_COLUMN> &olColList)
{

	TABLE_COLUMN rlColumnData;

	rlColumnData.VerticalSeparator = SEPA_NONE;
	rlColumnData.SeparatorType = SEPA_NONE;
	rlColumnData.Font = &ogCourier_Regular_9;

	rlColumnData.Alignment = COLALIGN_LEFT;
	if ((strcmp (prlLine->Adid, "A")) == 0)
		rlColumnData.Text = prlLine->AFlno;
	if ((strcmp (prlLine->Adid, "D")) == 0)
		rlColumnData.Text = prlLine->DFlno;
	olColList.NewAt(olColList.GetSize(), rlColumnData);

	rlColumnData.Alignment = COLALIGN_LEFT;
	if ((strcmp (prlLine->Adid, "A")) == 0)
		rlColumnData.Text = "Arrival";
	if ((strcmp (prlLine->Adid, "D")) == 0)
		rlColumnData.Text = "Departure";
	olColList.NewAt(olColList.GetSize(), rlColumnData);

	rlColumnData.Alignment = COLALIGN_LEFT;
	if ((strcmp (prlLine->Adid, "A")) == 0)
		rlColumnData.Text = prlLine->ADate;
	if ((strcmp (prlLine->Adid, "D")) == 0)
		rlColumnData.Text = prlLine->DDate;
	olColList.NewAt(olColList.GetSize(), rlColumnData);

	rlColumnData.Alignment = COLALIGN_LEFT;
	if ((strcmp (prlLine->Adid, "A")) == 0)
		rlColumnData.Text = prlLine->AStoa.Format("%H:%M");
	if ((strcmp (prlLine->Adid, "D")) == 0)
		rlColumnData.Text = prlLine->DStod.Format("%H:%M");
	olColList.NewAt(olColList.GetSize(), rlColumnData);

	rlColumnData.Alignment = COLALIGN_LEFT;
	if ((strcmp (prlLine->Adid, "A")) == 0)
		rlColumnData.Text = prlLine->ALand.Format("%H:%M");
	if ((strcmp (prlLine->Adid, "D")) == 0)
		rlColumnData.Text = prlLine->DAirb.Format("%H:%M");
	olColList.NewAt(olColList.GetSize(), rlColumnData);

	rlColumnData.Alignment = COLALIGN_LEFT;
	if ((strcmp (prlLine->Adid, "A")) == 0)
		rlColumnData.Text = prlLine->ATtyp;
	if ((strcmp (prlLine->Adid, "D")) == 0)
		rlColumnData.Text = prlLine->DTtyp;
	olColList.NewAt(olColList.GetSize(), rlColumnData);

	rlColumnData.Alignment = COLALIGN_LEFT;
	if ((strcmp (prlLine->Adid, "A")) == 0)
		rlColumnData.Text = prlLine->AVia3;
	if ((strcmp (prlLine->Adid, "D")) == 0)
		rlColumnData.Text = prlLine->DVia3;
	olColList.NewAt(olColList.GetSize(), rlColumnData);

}




int ReportSameALTableViewer::CompareFlight(SAMEALTABLE_LINEDATA *prpLine1, SAMEALTABLE_LINEDATA *prpLine2)
{
	// Sort fields order: Type, Schedule

	if (prpLine1->Adid == prpLine2->Adid)
	{
 		CTime olTime1;
		CTime olTime2;
		
		if (prpLine1->Adid == "A")
		{
			olTime1 = prpLine1->AStoa;
			olTime2 = prpLine2->AStoa;
		}
		else
		{
			olTime1 = prpLine1->DStod;
			olTime2 = prpLine2->DStod;
		}

		return (olTime1 == olTime2)? 0:
			(olTime1 > olTime2)? 1: -1;
	}
	else
	{
		if (prpLine1->Adid == "A")
			return -1;
		else
			return 1;
	}
}



//-----------------------------------------------------------------------------------------------
// Printing Routines
//-----------------------------------------------------------------------------------------------


void ReportSameALTableViewer::GetHeader()
{
	
	omPrintHeadHeaderArray.DeleteAll();  // BWi

	TABLE_HEADER_COLUMN *prlHeader[7];
	prlHeader[0] = new TABLE_HEADER_COLUMN;
	prlHeader[0]->Alignment = COLALIGN_CENTER;
	prlHeader[0]->Length = 67; 
	prlHeader[0]->Font = &ogCourier_Bold_10;
	prlHeader[0]->Text = GetString(IDS_STRING1078);

	prlHeader[1] = new TABLE_HEADER_COLUMN;
	prlHeader[1]->Alignment = COLALIGN_CENTER;
	prlHeader[1]->Length = 60; 
	prlHeader[1]->Font = &ogCourier_Bold_10;
	prlHeader[1]->Text = GetString(IDS_STRING1584);

	prlHeader[2] = new TABLE_HEADER_COLUMN;
	prlHeader[2]->Alignment = COLALIGN_CENTER;
	prlHeader[2]->Length = 70; 
	prlHeader[2]->Font = &ogCourier_Bold_10;
	prlHeader[2]->Text = GetString(IDS_STRING332);

	prlHeader[3] = new TABLE_HEADER_COLUMN;
	prlHeader[3]->Alignment = COLALIGN_CENTER;
	prlHeader[3]->Length = 45; 
	prlHeader[3]->Font = &ogCourier_Bold_10;
	prlHeader[3]->Text = GetString(IDS_STRING1421);//Schedule

	prlHeader[4] = new TABLE_HEADER_COLUMN;
	prlHeader[4]->Alignment = COLALIGN_CENTER;
	prlHeader[4]->Length = 45; 
	prlHeader[4]->Font = &ogCourier_Bold_10;
	prlHeader[4]->Text = GetString(IDS_STRING1586);//Actual

	prlHeader[5] = new TABLE_HEADER_COLUMN;
	prlHeader[5]->Alignment = COLALIGN_CENTER;
	prlHeader[5]->Length = 45; 
	prlHeader[5]->Font = &ogCourier_Bold_10;
	prlHeader[5]->Text = GetString(IDS_STRING301);//Na (Nature,TTYP)

	prlHeader[6] = new TABLE_HEADER_COLUMN;
	prlHeader[6]->Alignment = COLALIGN_CENTER;
	prlHeader[6]->Length = 45; 
	prlHeader[6]->Font = &ogCourier_Bold_10;
	prlHeader[6]->Text = GetString(IDS_STRING299);//Via


	for(int ili = 0; ili < 7; ili++)
	{
		omPrintHeadHeaderArray.Add( prlHeader[ili]);
	}

}




void ReportSameALTableViewer::PrintTableView()
{
	CString olFooter1,olFooter2;
	char pclFooter[256];

	sprintf(pclFooter, GetString(IDS_STRING1570), (CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M")));

	omFooterName = pclFooter;
	CString olTableName = GetString(IDS_STRING1570);
	int ilOrientation = PRINT_LANDSCAPE;//  PRINT_PORTRAET  PRINT_LANDSCAPE
	pomPrint = new CCSPrint(pomParentDlg,ilOrientation,45);

	if (pomPrint != NULL)
	{
		if (pomPrint->InitializePrinter(ilOrientation) == TRUE)
		{
			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
			pomPrint->imLineNo = pomPrint->imMaxLines + 1;
			DOCINFO	rlDocInfo;
			memset(&rlDocInfo, 0, sizeof(DOCINFO));
			rlDocInfo.cbSize = sizeof( DOCINFO );
			rlDocInfo.lpszDocName = olTableName;	
			//rlDocInfo.lpszDocName = TABLENAME;	
			pomPrint->omCdc.StartDoc( &rlDocInfo );
			pomPrint->imPageNo = 0;
			int ilLines = omLines.GetSize();  
			olFooter1.Format(GetString(IDS_STRING1445), ilLines,omFooterName);

			GetHeader();

			if (ilLines == 0)
				PrintTableHeader(); // Drucken leerer Tabelle
			for(int i = 0; i < ilLines; i++ ) 
			{
				if(pomPrint->imLineNo >= pomPrint->imMaxLines)
				{
					if(pomPrint->imPageNo > 0)
					{
						olFooter2.Format(GetString(IDS_STRING1198),pomPrint->imPageNo);
						pomPrint->PrintUIFFooter(olFooter1,"",olFooter2);
						pomPrint->omCdc.EndPage();
					}
					PrintTableHeader();
				}
				if(pomPrint->imLineNo == (pomPrint->imMaxLines-1) || i == (ilLines-1))
				{
					PrintTableLine(&omLines[i],true);
				}
				else
				{
					PrintTableLine(&omLines[i],false);
				}
			}
			olFooter2.Format(GetString(IDS_STRING1198),pomPrint->imPageNo);
			pomPrint->PrintUIFFooter(olFooter1,"",olFooter2);
			pomPrint->omCdc.EndPage();
			pomPrint->omCdc.EndDoc();
		}

		omPrintHeadHeaderArray.DeleteAll();
		SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
		delete pomPrint;
		pomPrint = NULL;
	}
}

//-----------------------------------------------------------------------------------------------

bool ReportSameALTableViewer::PrintTableHeader()
{
	pomPrint->omCdc.StartPage();
	pomPrint->imPageNo++;
	pomPrint->imLineNo = 0;
	double dgCCSPrintFactor = 2.7 ;
	char pclHeader[256];


	sprintf(pclHeader, GetString(IDS_STRING1602), pcmSelect, GetAlfn().GetBuffer(0), pcmInfo, pomData->GetSize());
	CString olTableName(pclHeader);

	pomPrint->PrintUIFHeader(omTableName,CString(pclHeader),pomPrint->imFirstLine-10);

	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;

	rlElement.Alignment   = PRINT_LEFT;
	rlElement.FrameLeft   = PRINT_NOFRAME;
	rlElement.FrameRight  = PRINT_NOFRAME;
	rlElement.FrameTop    = PRINT_NOFRAME;
	rlElement.FrameBottom = PRINT_FRAMEMEDIUM;
	rlElement.pFont       = &pomPrint->ogCourierNew_Bold_8;

	//int ilSize = omHeaderDataArray.GetSize();
	int ilSize = omPrintHeadHeaderArray.GetSize();

	for(int i=0;i<ilSize;i++)
	{
			//rlElement.Length = (int)(omHeaderDataArray[i].Length*dgCCSPrintFactor);
			rlElement.Length = (int)(omPrintHeadHeaderArray[i].Length*dgCCSPrintFactor);
			if(rlElement.Length < igCCSPrintMinLength)rlElement.Length+=igCCSPrintMoreLength; 
			//rlElement.Text   = omHeaderDataArray[i].Text;
			rlElement.Text   = omPrintHeadHeaderArray[i].Text;
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
	}
	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();


	return true;
}

//-----------------------------------------------------------------------------------------------

bool ReportSameALTableViewer::PrintTableLine(SAMEALTABLE_LINEDATA *prpLine,bool bpLastLine)
{
	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;
	double dgCCSPrintFactor = 2.7 ;
	rlElement.pFont      = &pomPrint->ogCourierNew_Regular_8;
	int ilSize = omPrintHeadHeaderArray.GetSize();

	for(int i=0;i<ilSize;i++)
	{
		rlElement.Length = (int)(omPrintHeadHeaderArray[i].Length*dgCCSPrintFactor);
		if(rlElement.Length < igCCSPrintMinLength)rlElement.Length+=igCCSPrintMoreLength; 
		rlElement.Alignment  = PRINT_LEFT;
		rlElement.FrameTop   = PRINT_FRAMETHIN;
		if(bpLastLine == true)
		{
			rlElement.FrameBottom = PRINT_FRAMETHIN;
		}
		else
		{
			rlElement.FrameBottom = PRINT_NOFRAME;
		}
		rlElement.FrameLeft = PRINT_FRAMETHIN;
		rlElement.FrameRight = PRINT_NOFRAME;


		switch(i)
		{
			case 0:
			{
				if ((strcmp (prpLine->Adid, "A")) == 0)
					rlElement.Text = prpLine->AFlno;
				if ((strcmp (prpLine->Adid, "D")) == 0)
					rlElement.Text = prpLine->DFlno;
			}
			break;
			case 1:
			{
				if ((strcmp (prpLine->Adid, "A")) == 0)
					rlElement.Text = "Arrival";
				if ((strcmp (prpLine->Adid, "D")) == 0)
					rlElement.Text = "Departure";
			}
			break;
			case 2:
			{
				if ((strcmp (prpLine->Adid, "A")) == 0)
					rlElement.Text = prpLine->ADate;
				if ((strcmp (prpLine->Adid, "D")) == 0)
					rlElement.Text = prpLine->DDate;
			}
			break;
			case 3:
			{
				if ((strcmp (prpLine->Adid, "A")) == 0)
					rlElement.Text = prpLine->AStoa.Format("%H:%M");
				if ((strcmp (prpLine->Adid, "D")) == 0)
					rlElement.Text = prpLine->DStod.Format("%H:%M");
			}
			break;
			case 4:
			{
				if ((strcmp (prpLine->Adid, "A")) == 0)
					rlElement.Text = prpLine->ALand.Format("%H:%M");
				if ((strcmp (prpLine->Adid, "D")) == 0)
					rlElement.Text = prpLine->DAirb.Format("%H:%M");
			}
			break;
			case 5:
			{
				if ((strcmp (prpLine->Adid, "A")) == 0)
					rlElement.Text = prpLine->ATtyp;
				if ((strcmp (prpLine->Adid, "D")) == 0)
					rlElement.Text = prpLine->DTtyp;
			}
			break;
			case 6:
			{
				if ((strcmp (prpLine->Adid, "A")) == 0)
					rlElement.Text = prpLine->AVia3;
				if ((strcmp (prpLine->Adid, "D")) == 0)
					rlElement.Text = prpLine->DVia3;
				rlElement.FrameRight = PRINT_FRAMETHIN;
			}
			break;
		}

		rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
		
	}
	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();
	return true;
}

//-----------------------------------------------------------------------------------------------


// Drucken in Datei
void ReportSameALTableViewer::PrintPlanToFile(char *pcpDefPath)
{

	CCS_TRY

	char pclHeadlineSelect[256];
	sprintf(pclHeadlineSelect, GetString(IDS_STRING1602), pcmSelect, GetAlfn().GetBuffer(0), pcmInfo, pomData->GetSize());

	ofstream of;
	of.open( pcpDefPath, ios::out);
	of << CString(pclHeadlineSelect) << " "
	   << CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M") << endl << endl;

	of << GetString(IDS_STRING1078) << "     "<< GetString(IDS_STRING1584) << "     " 
	   << GetString(IDS_STRING332)  << "     "  << GetString(IDS_STRING1421) << "   " 
	   << GetString(IDS_STRING1586) << "   " << GetString(IDS_STRING301) << "   "
	   << GetString(IDS_STRING299)  << endl;
	of << "-------------------------------------------------------------" << endl;

	CStringArray olDaten;
	int ilCount = omLines.GetSize();


	for(int i = 0; i < ilCount; i++)
	{
		SAMEALTABLE_LINEDATA rlD = omLines[i];

		if ((strcmp (rlD.Adid, "A")) == 0)
		{
			olDaten.Add(rlD.AFlno);
			olDaten.Add("Arrival");
			olDaten.Add(rlD.ADate);
			olDaten.Add(rlD.AStoa.Format("%H:%M"));
			olDaten.Add(rlD.ALand.Format("%H:%M"));
			olDaten.Add(rlD.ATtyp);
			olDaten.Add(rlD.AVia3);
		}
		if ((strcmp (rlD.Adid, "D")) == 0)
		{
			olDaten.Add(rlD.DFlno);
			olDaten.Add("Departure");
			olDaten.Add(rlD.DDate);
			olDaten.Add(rlD.DStod.Format("%H:%M"));
			olDaten.Add(rlD.DAirb.Format("%H:%M"));
			olDaten.Add(rlD.DTtyp);
			olDaten.Add(rlD.DVia3);
		}

		of.setf(ios::left, ios::adjustfield);
		of   << setw(10) << olDaten[0].GetBuffer(0)
			 << setw(12) << olDaten[1].GetBuffer(0)
			 << setw(11) << olDaten[2].GetBuffer(0)
			 << setw(11) << olDaten[3].GetBuffer(0)
			 << setw(8) << olDaten[4].GetBuffer(0)
			 << setw(9) << olDaten[5].GetBuffer(0)
			 << setw(9) << olDaten[6].GetBuffer(0)
			 << endl;
		
		olDaten.RemoveAll();
	}
	of.close();

	CCS_CATCH_ALL

}
