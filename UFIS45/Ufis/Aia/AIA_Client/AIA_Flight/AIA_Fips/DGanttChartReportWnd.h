#if !defined(AFX_DGanttChartReportWnd_H__004960BF_0881_11D3_AB42_00001C019D0B__INCLUDED_)
#define AFX_DGanttChartReportWnd_H__004960BF_0881_11D3_AB42_00001C019D0B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DGanttChartReportWnd.h : header file
//
#include "CColor.h"
#include "CCSPtrArray.h"
#include "DGanttChartReport.h"

/////////////////////////////////////////////////////////////////////////////
// DGanttChartReportWnd window

class DGanttBarReport;

class DGanttChartReportWnd : public CWnd
{
// Construction
public:
	DGanttChartReportWnd(CWnd* ppParent, CString opXAxe, CString opYAxe, CString opTotalNumber, CColor opColor);

	virtual ~DGanttChartReportWnd();
// Attributes
public:
	void SetData1(const CUIntArray &opData);
	void SetData2(const CUIntArray &opData);
	void SetXAxeText(const CStringArray& opXAxeText);
	void SetBarText(const CStringArray& opBarText);
	void SetTotalNumnber(int ipTotalNumber);
	void SetLegende(CString opLeft, CString opRight, CString opMiddle);
// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(DGanttChartReportWnd)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	//}}AFX_VIRTUAL

// Implementation
public:
	void Print(CDC* popDC, CCSPrint* popPrint, CString opHeader, CString opFooterLeft);

	// Generated message map functions
protected:
	//{{AFX_MSG(DGanttChartReportWnd)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_MSG

private:
//	CRect omRectWnd;
	DGanttChartReport* pomDGanttChartReport;
	CString omXAxe;
	CString omYAxe;
	CString omTotalNumber; 
	CColor omColor;
	CString omLeft;
	CString omRight;
	CString omMiddle;
	CStringArray omXAxeText;
	CStringArray omBarText;
	CUIntArray omData1;
	CUIntArray omData2;
	int imTotalNumber;


	DECLARE_MESSAGE_MAP()

};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DGanttChartReportWnd_H__004960BF_0881_11D3_AB42_00001C019D0B__INCLUDED_)
