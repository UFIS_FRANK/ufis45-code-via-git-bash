// ReportFromFieldDlg.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "fpms.h"
#include "ReportFromFieldDlg.h"
#include "CedaBasicData.h"
#include "CCSTime.h"
#include "CcsGlobl.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld CReportFromFieldDlg 


CReportFromFieldDlg::CReportFromFieldDlg(CWnd* pParent,CString opHeadline,CString opSelectfield,CString opDatefield,CTime* poDate,char *opSelect)
	: CDialog(CReportFromFieldDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CReportFromFieldDlg)
	m_Datumvon = _T("");
	m_Select = _T("");
	m_Selectfield = _T("");
	m_Datefield = _T("");
	//}}AFX_DATA_INIT

	omHeadline = opHeadline;	//Dialogbox-�berschrift
	m_Datefield = opDatefield;
	pomDate = poDate;
	m_Selectfield = opSelectfield;
	m_Select = opSelect;
	pcmSelect = opSelect;

}


void CReportFromFieldDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CReportFromFieldDlg)
	DDX_Control(pDX, IDC_DATEFIELD, m_CS_Datefield);
	DDX_Control(pDX, IDC_SELECTFIELD, m_CS_Selectfield);
	DDX_Control(pDX, IDC_SELECT, m_CE_Select);
	DDX_Control(pDX, IDC_DATUMVON, m_CE_Datumvon);
	DDX_Text(pDX, IDC_DATUMVON, m_Datumvon);
	DDX_Text(pDX, IDC_SELECT, m_Select);
	DDX_Text(pDX, IDC_SELECTFIELD, m_Selectfield);
	DDX_Text(pDX, IDC_DATEFIELD, m_Datefield);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CReportFromFieldDlg, CDialog)
	//{{AFX_MSG_MAP(CReportFromFieldDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Behandlungsroutinen f�r Nachrichten CReportFromFieldDlg 

void CReportFromFieldDlg::OnCancel() 
{
	// TODO: Zus�tzlichen Bereinigungscode hier einf�gen
	
	CDialog::OnCancel();
}

void CReportFromFieldDlg::OnOK() 
{
	bool ilStatus = true;
	CString olErrorText;
	CString olKeineDaten(GetString(IDS_STRING1038));
	CString olNichtFormat(GetString(IDS_STRING1039));


	if(m_CE_Select.GetStatus() == false)
	{
		ilStatus = false;
		if(m_CE_Select.GetWindowTextLength() == 0)
		{
			olErrorText += GetString(IDS_STRING1040) +  olKeineDaten;
		}
		else
		{
			olErrorText += GetString(IDS_STRING1040) + olNichtFormat;
		}
	}
	if (ilStatus == true)
	{
		if(m_CE_Datumvon.GetStatus() == false)
		{
			ilStatus = false;
			if(m_CE_Datumvon.GetWindowTextLength() == 0)
			{
				olErrorText += GetString(IDS_STRING1041) +  olKeineDaten;
			}
			else
			{
				olErrorText += GetString(IDS_STRING1041) + olNichtFormat;
			}
		}
	}

	if (ilStatus == true)
	{
		CString olTmp;
		CString olTmp2;
		m_CE_Datumvon.GetWindowText(m_Datumvon);
		*pomDate = DateStringToDate(m_Datumvon);	//CTime *pomDate;
		m_CE_Select.GetWindowText(m_Select);
		strcpy(pcmSelect, m_Select);

		bool blRet = ogBCD.GetField("ALT", "ALC2", "ALC3", m_Select, olTmp2, olTmp);
		if(!blRet)
		{
			ilStatus = false;
			olErrorText += GetString(IDS_STRING1042);
		}

		
		if(*pomDate == TIMENULL) 
		{
				ilStatus = false;
				olErrorText += GetString(IDS_STRING1043);
		}

		if (ilStatus == true)
		{
			int lRet = pomDate->GetDayOfWeek();
			if(pomDate->GetDayOfWeek() != 2)		//2=Mon
			{
				ilStatus = false;
				olErrorText += GetString(IDS_STRING1044);
			}

			else
				CDialog::OnOK();
		}
	}
	if (ilStatus == false)
	{
		Beep(440,70);
		MessageBox(olErrorText, GetString(ST_FEHLER), MB_ICONEXCLAMATION);
		m_CE_Select.SetFocus();
		m_CE_Select.SetSel(0,-1);

		//CDialog::OnCancel();
	}


}

BOOL CReportFromFieldDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	CDialog::SetWindowText(omHeadline);	//Dialogbox-�berschrift
	m_CE_Select.SetWindowText(m_Select);
	//m_CE_Select.SetWindowText("");
	m_CE_Datumvon.SetTypeToDate(true);
	m_CE_Datumvon.SetBKColor(YELLOW);

	m_Datumvon = pomDate->Format("%d.%m.%Y" ) ;
	m_CE_Datumvon.SetWindowText( m_Datumvon );

	//m_CE_Select.SetTypeToString("X(3)",3);
	m_CE_Select.SetTypeToString("x|#x|#x|#",3,2);
	m_CE_Select.SetBKColor(YELLOW);
	m_CE_Select.SetTextErrColor(RED);

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX-Eigenschaftenseiten sollten FALSE zur�ckgeben
}
