// MessefluegeTableViewer.cpp 
//
//	Statistik Messefl�ge
#include "stdafx.h"
#include "Report6TableViewer.h"
#include "CcsGlobl.h"

#include <iomanip.h>
#include <iostream.h>
#include <fstream.h>
#include "BasicData.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif


// MessefluegeTableViewer
//
MessefluegeTableViewer::MessefluegeTableViewer(CCSPtrArray<ROTATIONDLGFLIGHTDATA> *popData, char *pcpInfo)
{
	pomData = popData;
	pcmInfo = pcpInfo;

	bmIsFromSearch = FALSE;
    pomMessefluegeTable = NULL;
}

//-----------------------------------------------------------------------------------------------

MessefluegeTableViewer::~MessefluegeTableViewer()
{
    DeleteAll();
	omHeaderDataArray.DeleteAll();
}

//-----------------------------------------------------------------------------------------------

void MessefluegeTableViewer::Attach(CCSTable *popTable)
{
    pomMessefluegeTable = popTable;
}

//-----------------------------------------------------------------------------------------------
int MessefluegeTableViewer::CompareMessefluege(MESSEFLUEGETABLE_LINEDATA *prpMessefluege1, MESSEFLUEGETABLE_LINEDATA *prpMessefluege2)
{
	return 0;
}

//-----------------------------------------------------------------------------------------------
void MessefluegeTableViewer::ChangeViewTo(const char *pcpViewName)
{
    DeleteAll();    
    MakeLines();
	UpdateDisplay();
}

//-----------------------------------------------------------------------------------------------

void MessefluegeTableViewer::MakeLines()
{
	CTime olTifdold;
	int ilLCtr = 0;
	int ilACtr = 0;
	int ilDCtr = 0;
	int ilASum = 0;
	int ilDSum = 0;
	CString olNewstr("");
	CString olOldstr(""); 

	int ilCount = pomData->GetSize();

	for (int ilLc = 0; ilLc < ilCount; ilLc++)
	{
		ROTATIONDLGFLIGHTDATA *prlMessefluegeData = &pomData->GetAt(ilLc);
		if(bgReportLocal)
			ogBasicData.UtcToLocal(prlMessefluegeData->Tifd);
		olNewstr = prlMessefluegeData->Tifd.Format( "%Y%m%d" );
/* am 2/11/98 hbe
		ilLCtr++;
		if (!strcmp(prlMessefluegeData->Adid, "A"))
		{
			ilACtr++;ilASum++;
		}
		else if (!strcmp(prlMessefluegeData->Adid, "D"))
		{
			ilDCtr++;ilDSum++;
		}
*/
		if(ilLc == 0)
		{
			olOldstr = olNewstr;
			olTifdold = prlMessefluegeData->Tifd;
		}
		if (olNewstr != olOldstr)	// Datum wechselt
		{
			MakeLine(olTifdold, ilACtr, ilDCtr, ilLCtr);
			olTifdold = prlMessefluegeData->Tifd;
			olOldstr = olTifdold.Format( "%Y%m%d" );
			ilLCtr = 0;
			ilACtr = 0;
			ilDCtr = 0;
		}

		ilLCtr++;
		if (!strcmp(prlMessefluegeData->Adid, "A"))
		{
			ilACtr++;ilASum++;
		}
		else if (!strcmp(prlMessefluegeData->Adid, "D"))
		{
			ilDCtr++;ilDSum++;
		}

		if((ilLc == ilCount-1) && (ilLCtr > 0))	// nach dem letzten

			MakeLine(prlMessefluegeData->Tifd, ilACtr, ilDCtr, ilLCtr);
	}
	MakeResultLine( ilASum, ilDSum, ilCount);

}

//-----------------------------------------------------------------------------------------------

void MessefluegeTableViewer::MakeLine(CTime opTifdold, int ipACtr, int ipDCtr, int ipLCtr)
{
    // Update viewer data for this shift record
	char clbuffer[10];
    MESSEFLUEGETABLE_LINEDATA rlMessefluege;
	rlMessefluege.Tifd = opTifdold.Format("%d.%m.%Y"); 
	sprintf(clbuffer,"%d", ipACtr);
	rlMessefluege.ACtr = clbuffer; 
	sprintf(clbuffer,"%d", ipDCtr);
	rlMessefluege.DCtr = clbuffer; 
	sprintf(clbuffer,"%d", ipLCtr);
	rlMessefluege.LCtr = clbuffer; 

	CreateLine(&rlMessefluege);
}

//-----------------------------------------------------------------------------------------------

void MessefluegeTableViewer::MakeResultLine(int ipACtr, int ipDCtr, int ipLCtr)
{
    // Update viewer data for this shift record
	char clbuffer[10];
    MESSEFLUEGETABLE_LINEDATA rlMessefluege;
	rlMessefluege.Tifd = GetString(IDS_STRING1200);//"Gesamt:"; 
	sprintf(clbuffer,"%d", ipACtr);
	rlMessefluege.ACtr = clbuffer; 
	sprintf(clbuffer,"%d", ipDCtr);
	rlMessefluege.DCtr = clbuffer; 
	sprintf(clbuffer,"%d", ipLCtr);
	rlMessefluege.LCtr = clbuffer; 

	CreateLine(&rlMessefluege);
}

//-----------------------------------------------------------------------------------------------

void MessefluegeTableViewer::CreateLine(MESSEFLUEGETABLE_LINEDATA *prpMessefluege)
{
    int ilLineCount = omLines.GetSize();
    for (int ilLineno = ilLineCount; ilLineno > 0; ilLineno--)
        if (CompareMessefluege(prpMessefluege, &omLines[ilLineno-1]) >= 0)
	        break;							 // should be inserted after Lines[ilLineno-1]
	MESSEFLUEGETABLE_LINEDATA rlMessefluege;
	rlMessefluege = *prpMessefluege;
    omLines.NewAt(ilLineno, rlMessefluege);
}

//-----------------------------------------------------------------------------------------------
// UpdateDisplay: Load data selected by filter conditions to the display by using "omTable"

void MessefluegeTableViewer::UpdateDisplay()
{
	int ilLines = omLines.GetSize();
	const int ilColumns = 2;	//<===== Spaltenanzahl setzen (von 0 ab)
	omHeaderDataArray.DeleteAll();

	pomMessefluegeTable->SetShowSelection(TRUE);
	pomMessefluegeTable->ResetContent();

	TABLE_HEADER_COLUMN rlHeader;
	rlHeader.Alignment = COLALIGN_LEFT;
	rlHeader.Font = &ogCourier_Regular_10;
	rlHeader.Length = 120;
	rlHeader.Text = CString("Datum");
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 120;
	rlHeader.Text = CString("A");
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 120;
	rlHeader.Text = CString("D");
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 120; 
	rlHeader.Text = CString("Total");
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);

	pomMessefluegeTable->SetHeaderFields(omHeaderDataArray);

	pomMessefluegeTable->SetDefaultSeparator();
	pomMessefluegeTable->SetTableEditable(FALSE);

	TABLE_COLUMN rlColumnData;
	rlColumnData.Font = &ogCourier_Regular_10;
	rlColumnData.BkColor = WHITE;
	rlColumnData.Alignment = COLALIGN_LEFT;
	
    for (int ilLineNo = 0; ilLineNo < ilLines; ilLineNo++)
	{
		CCSPtrArray<TABLE_COLUMN> olColList;

		rlColumnData.Text = omLines[ilLineNo].Tifd;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].ACtr;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].DCtr;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].LCtr;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		pomMessefluegeTable->AddTextLine(olColList,(void*)NULL);
		olColList.DeleteAll();
	}
	pomMessefluegeTable->DisplayTable();
}



//-----------------------------------------------------------------------------------------------

void MessefluegeTableViewer::DeleteLine(int ipLineno)
{
	omLines.DeleteAt(ipLineno);
}

//-----------------------------------------------------------------------------------------------

void MessefluegeTableViewer::DeleteAll()
{
    while (omLines.GetSize() > 0)
    DeleteLine(0);
}

//-----------------------------------------------------------------------------------------------
// Printing Routines
//-----------------------------------------------------------------------------------------------

void MessefluegeTableViewer::PrintTableView()
{
	CString olFooter1,olFooter2;
	char pclFooter[100];
	//sprintf(pclInfo, "Statistik Messefluege (Abfl�ge) %s", pcmInfo);
	sprintf(pclFooter, GetString(IDS_STRING1127), (CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M")));
	omFooterName = pclFooter;
	omTableName = GetString(IDS_STRING1128);
	int ilOrientation = PRINT_PORTRAET;//  PRINT_PORTRAET  PRINT_LANDSCAPE
	pomPrint = new CCSPrint((CWnd*)this,ilOrientation,45);// FirstLine(P=200,L=200),LeftOffset(P=200,L=50)

	if (pomPrint != NULL)
	{
		if (pomPrint->InitializePrinter(ilOrientation) == TRUE)
		{
			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
			//pomPrint->imLineNo = pomPrint->imMaxLines + 1;
			pomPrint->imLineNo = pomPrint->imMaxLines + 1 - 2;
			DOCINFO	rlDocInfo;
			memset(&rlDocInfo, 0, sizeof(DOCINFO));
			rlDocInfo.cbSize = sizeof( DOCINFO );
			rlDocInfo.lpszDocName = omTableName;	
			pomPrint->omCdc.StartDoc( &rlDocInfo );
			pomPrint->imPageNo = 0;
			int ilLines = omLines.GetSize();
			olFooter1.Format("Tage: %d %s",(ilLines-1),omFooterName);
			for(int i = 0; i < ilLines; i++ ) 
			{
				//if(pomPrint->imLineNo >= pomPrint->imMaxLines)
				if(pomPrint->imLineNo >= pomPrint->imMaxLines-2)
				{
					if(pomPrint->imPageNo > 0)
					{
						olFooter2.Format(GetString(IDS_STRING1198),pomPrint->imPageNo);
						pomPrint->PrintUIFFooter(olFooter1,"",olFooter2);
						pomPrint->omCdc.EndPage();
					}
					PrintTableHeader();
				}
				if(pomPrint->imLineNo == (pomPrint->imMaxLines-3) || i == (ilLines-1))
				{
					PrintTableLine(&omLines[i],true);
				}
				else
				{
					PrintTableLine(&omLines[i],false);
				}
			}
			olFooter2.Format(GetString(IDS_STRING1198),pomPrint->imPageNo);
			pomPrint->PrintUIFFooter(olFooter1,"",olFooter2);
			pomPrint->omCdc.EndPage();
			pomPrint->omCdc.EndDoc();
		}
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
	delete pomPrint;

	pomPrint = NULL;
	}
}

//-----------------------------------------------------------------------------------------------

bool MessefluegeTableViewer::PrintTableHeader()
{
	pomPrint->omCdc.StartPage();
	pomPrint->imPageNo++;
	pomPrint->imLineNo = 0;

	char pclHeader[100];
	sprintf(pclHeader, GetString(IDS_STRING1129), pcmInfo);
	//sprintf(pclHeader, "  vom %s", pcmInfo);
	//mPrint->PrintUIFHeader(omTableName,CString(CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M")),pomPrint->imFirstLine-10);
	pomPrint->PrintUIFHeader("",CString(pclHeader),pomPrint->imFirstLine-10);

	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;

	rlElement.Alignment   = PRINT_LEFT;
	rlElement.FrameLeft   = PRINT_NOFRAME;
	rlElement.FrameRight  = PRINT_NOFRAME;
	rlElement.FrameTop    = PRINT_NOFRAME;
	rlElement.FrameBottom = PRINT_FRAMEMEDIUM;
	rlElement.pFont       = &pomPrint->ogCourierNew_Bold_8;

	int ilSize = omHeaderDataArray.GetSize();
	for(int i=0;i<ilSize;i++)
	{
		rlElement.Length = (int)(omHeaderDataArray[i].Length*dgCCSPrintFactor);
		if(rlElement.Length < igCCSPrintMinLength)rlElement.Length+=igCCSPrintMoreLength; 
		rlElement.Text   = omHeaderDataArray[i].Text;
		rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
	}
	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();


	return true;
}

//-----------------------------------------------------------------------------------------------

bool MessefluegeTableViewer::PrintTableLine(MESSEFLUEGETABLE_LINEDATA *prpLine,bool bpLastLine)
{
	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;
	rlElement.pFont      = &pomPrint->ogCourierNew_Regular_8;
	int ilSize = omHeaderDataArray.GetSize();
	for(int i=0;i<ilSize;i++)
	{
		//if(i!=1&&i!=1)// <= setzen, wenn Spalten nicht angezeigt werden sollen 
		{
			rlElement.Length = (int)(omHeaderDataArray[i].Length*dgCCSPrintFactor);
			if(rlElement.Length < igCCSPrintMinLength)rlElement.Length+=igCCSPrintMoreLength; 
			rlElement.Alignment  = PRINT_LEFT;
			rlElement.FrameTop   = PRINT_FRAMETHIN;
			if(bpLastLine == true)
			{
				rlElement.FrameBottom = PRINT_FRAMETHIN;
			}
			else
			{
				rlElement.FrameBottom = PRINT_NOFRAME;
			}
			rlElement.FrameLeft  = PRINT_NOFRAME;
			rlElement.FrameRight = PRINT_NOFRAME;
			switch(i)
			{
			case 0:
				{
					rlElement.FrameLeft  = PRINT_FRAMETHIN;
					rlElement.Text		= prpLine->Tifd;
				}
				break;
			case 1:
				{
					rlElement.FrameRight = PRINT_FRAMETHIN;
					rlElement.Text		= prpLine->ACtr;
				}
				break;
			case 2:
				{
					rlElement.FrameRight = PRINT_FRAMETHIN;
					rlElement.Text		= prpLine->DCtr;
				}
				break;
			case 3:
				{
					rlElement.FrameRight = PRINT_FRAMETHIN;
					rlElement.Text		= prpLine->LCtr;
				}
				break;
			}
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
		}
	}
	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();
	return true;
}

//-----------------------------------------------------------------------------------------------


// Drucken in Datei
void MessefluegeTableViewer::PrintPlanToFile(char *pcpDefPath)
{
		ofstream of;
		of.open( pcpDefPath, ios::out);
		of << GetString(IDS_STRING925) << " " 
			<< pcmInfo << "    "
			<< CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M") << endl << endl;

		of  << GetString(IDS_STRING332)  << "       "
			<< GetString(IDS_STRING1130) << "           "
			<< GetString(IDS_STRING1131) << "           "
			<< GetString(IDS_STRING1132) << "       "  
			<< endl;


		of << "-------------------------------------------------------" << endl;
		int ilCount = omLines.GetSize();
		for(int i = 0; i < ilCount; i++)
		{
			MESSEFLUEGETABLE_LINEDATA rlD = omLines[i];
			of.setf(ios::left, ios::adjustfield);
			of   << setw(12) << rlD.Tifd  
				 << setw(12) << rlD.ACtr
				 << setw(12) << rlD.DCtr
				 << setw(12) << rlD.LCtr << endl;
		}
		of.close();
//	MessageBox("Es wurde folgende Datei geschrieben:\nc:\\tmp\\Messefluege.txt", "Drucken",(MB_ICONINFORMATION|MB_OK));
}

