#if !defined(AFX_CCSUNISORTPAGE_H__18568972_4EA3_11D1_9881_000001014864__INCLUDED_)
#define AFX_CCSUNISORTPAGE_H__18568972_4EA3_11D1_9881_000001014864__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// ccsunisortpage.h : header file
//

#include "Ansicht.h"
//#include "CCSBasePSPage.h"
//#include "CCSPageBuffer.h"
/////////////////////////////////////////////////////////////////////////////
// CPSUniSortPage dialog

class CPSUniSortPage : public CPropertyPage//CBasePropSheetPage
{
	DECLARE_DYNCREATE(CPSUniSortPage)

// Construction
public:
	CPSUniSortPage();
	~CPSUniSortPage();
   //void ResetBufferPtr(){CBasePropSheetPage::ResetBufferPtr(); pomPageBuffer = NULL;};
public:
//	CPageBuffer *pomPageBuffer;
// Dialog Data
	//{{AFX_DATA(CPSUniSortPage)
	enum { IDD = IDD_PSUNISORT_PAGE };
	CListBox	m_ListFields;
	CString	m_EditSort;
	CEdit E_SortText;
	//}}AFX_DATA
	CStringArray omDescArray;
	CStringArray omFieldArray;

	CStringArray omValues;
	CString omCalledFrom;
	void SetCalledFrom(CString opCalledFrom);

	void SetData();
	void GetData();

	void SetFieldAndDescriptionArrays();
	void InitMask();

	void InitFieldAndDescArrays (const CStringArray &opFieldArray,
		                         const CStringArray &opDescArray);
// Overrides
	// ClassWizard generate virtual function overrides
	//{{AFX_VIRTUAL(CPSUniSortPage)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL
	CString GetPrivList(){return("");};
//   void *GetData(); 
//   void InitialScreen();

// Implementation
protected:
	// Generated message map functions
	//{{AFX_MSG(CPSUniSortPage)
	afx_msg void OnButtonAsc();
	afx_msg void OnButtonDelete();
	afx_msg void OnButtonDesc();
	afx_msg void OnButtonNew();
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
//	void InitialValues(int ipRow);

};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CCSUNISORTPAGE_H__18568972_4EA3_11D1_9881_000001014864__INCLUDED_)
