#if !defined(AFX_INFODLG_H__2F171691_5FFA_11D1_B3CC_0000C016B067__INCLUDED_)
#define AFX_INFODLG_H__2F171691_5FFA_11D1_B3CC_0000C016B067__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// InfoDlg.h : header file
//
#include "CCSEdit.h"
#include "CCSGlobl.h"
#include "resrc1.h"

/////////////////////////////////////////////////////////////////////////////
// InfoDlg dialog

class InfoDlg : public CDialog
{
// Construction
public:
	InfoDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(InfoDlg)
	enum { IDD = IDD_INFO };
	CEdit	m_Username;
	CEdit	m_Server;
	CCSEdit	m_COPYRIGHT1;
	CCSEdit	m_COPYRIGHT2;
	CCSEdit	m_UFIS1;
	CCSEdit	m_UFIS2;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(InfoDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(InfoDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_INFODLG_H__2F171691_5FFA_11D1_B3CC_0000C016B067__INCLUDED_)
