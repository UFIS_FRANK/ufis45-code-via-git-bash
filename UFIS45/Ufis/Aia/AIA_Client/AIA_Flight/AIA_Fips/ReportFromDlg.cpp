// ReportFromDlg.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "fpms.h"
#include "ReportFromDlg.h"
#include "CCSTime.h"
#include "CCSEdit.h"
#include "CcsGlobl.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld CReportFromDlg 


CReportFromDlg::CReportFromDlg(CWnd* pParent, CString opHeadline, CString opSelectfield, CTime* poDate)
	: CDialog(CReportFromDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CReportFromDlg)
	m_Datumvon = _T("");
	m_Select = _T("");
	//}}AFX_DATA_INIT

	omHeadline = opHeadline;	//Dialogbox-�berschrift
	m_Select = opSelectfield;
	pomDate = poDate;

}


void CReportFromDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CReportFromDlg)
	DDX_Control(pDX, IDC_SELECT, m_CS_Select);
	DDX_Control(pDX, IDC_DATUMVON, m_CE_Datumvon);
	DDX_Text(pDX, IDC_DATUMVON, m_Datumvon);
	DDX_Text(pDX, IDC_SELECT, m_Select);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CReportFromDlg, CDialog)
	//{{AFX_MSG_MAP(CReportFromDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Behandlungsroutinen f�r Nachrichten CReportFromDlg 

void CReportFromDlg::OnCancel() 
{
	// TODO: Zus�tzlichen Bereinigungscode hier einf�gen
	
	CDialog::OnCancel();
}

void CReportFromDlg::OnOK() 
{
	m_CE_Datumvon.GetWindowText(m_Datumvon);
	*pomDate = DateStringToDate(m_Datumvon);
	
	if( m_Datumvon.IsEmpty() )
		MessageBox(GetString(IDS_STRING918), GetString(IMFK_REPORT), MB_OK);
	else
	{
		if(m_CE_Datumvon.GetStatus() )
			CDialog::OnOK();
	}
}

BOOL CReportFromDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	CDialog::SetWindowText(omHeadline);	//Dialogbox-�berschrift
	m_CE_Datumvon.SetTypeToDate();
	m_CE_Datumvon.SetBKColor(YELLOW);
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX-Eigenschaftenseiten sollten FALSE zur�ckgeben
}
