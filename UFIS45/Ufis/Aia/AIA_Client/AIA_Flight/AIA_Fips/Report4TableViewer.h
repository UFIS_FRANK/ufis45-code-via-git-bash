#ifndef __MESSEDAYSTOPTABLEVIEWER_H__
#define __MESSEDAYSTOPTABLEVIEWER_H__

#include "stdafx.h"
#include "RotationDlgCedaFlightData.h"
#include "CCSTable.h"
#include "CViewer.h"
#include "CCSPrint.h"


struct MESSEDAYSTOPTABLE_LINEDATA
{
	long AUrno;
	long ARkey;
	CString		AStev;
	CString		AFlno;
	CTime		AStoa;
	CString		AOrg3;
	CString		AVia3;
	CString		AAct;
	//CString		AAct5;
	CString		AGta1;
	CString		APsta;
	CString		ATtyp;

	long DUrno;
	long DRkey;
	CString		DStev;
	CString		DFlno;
	CTime		DStod;
	CString		DTtyp;
	CString		DDes3;
	CString		DVia3;
	CString		DAct;
	//CString		DAct5;
	CString		DGtd1;
	CString		DPstd;
	CString		DWro1;

};

/////////////////////////////////////////////////////////////////////////////
// MessedaystopTableViewer

class MessedaystopTableViewer : public CViewer
{
// Constructions
public:
    MessedaystopTableViewer(CCSPtrArray<ROTATIONDLGFLIGHTDATA> *popData, char *pcpInfo = NULL, char *pcpSelect = NULL);
    ~MessedaystopTableViewer();

    void Attach(CCSTable *popAttachWnd);
    virtual void ChangeViewTo(int* piCnt= NULL);
// Internal data processing routines
private:
    void PrepareLines();
    void MakeLines();
	int  MakeLine(ROTATIONDLGFLIGHTDATA *prpAFlight, ROTATIONDLGFLIGHTDATA *prpDFlight);
	void MakeLineData(ROTATIONDLGFLIGHTDATA *prpAFlight, ROTATIONDLGFLIGHTDATA *prpDFlight, MESSEDAYSTOPTABLE_LINEDATA &rpLine);
	void MakeColList(MESSEDAYSTOPTABLE_LINEDATA *prlLine, CCSPtrArray<TABLE_COLUMN> &olColList);
	bool FindLine(long lpUrno, int &rilLineno);
	int  FindLine(long lpUrno, int &rilLineno1, int &rilLineno2);
	int  CompareMessedaystop(MESSEDAYSTOPTABLE_LINEDATA *prpMessedaystop1, MESSEDAYSTOPTABLE_LINEDATA *prpMessedaystop2);
	int  CompareFlight(MESSEDAYSTOPTABLE_LINEDATA *prpFlight1, MESSEDAYSTOPTABLE_LINEDATA *prpFlight2);
	bool PrintFrameBottom(void);
	int  GetVia(CCSPtrArray<VIADATA> *opVias, ROTATIONDLGFLIGHTDATA *prpFlight);
// Operations
public:
	void DeleteAll();
	int CreateLine(MESSEDAYSTOPTABLE_LINEDATA &prpMessedaystop);
	void DeleteLine(int ipLineno);
	void PrintPlanToFile(char *pcpDefPath);
// Window refreshing routines
public:
	void UpdateDisplay();
	void DrawHeader();


// Attributes used for filtering condition
private:
	bool bmIsFromSearch;
	int imDay;
// Attributes
private:
    CCSTable *pomTable;
	CCSPtrArray<ROTATIONDLGFLIGHTDATA> *pomData;
	CCSPtrArray<ROTATIONDLGFLIGHTDATA> *pomSortData;
public:
    CCSPtrArray<MESSEDAYSTOPTABLE_LINEDATA> omLines;
	CCSPtrArray<TABLE_HEADER_COLUMN> omHeaderDataArray;
//Print 
	void GetHeader(void); 
	void PrintTableView(void);
	bool PrintTableLine(MESSEDAYSTOPTABLE_LINEDATA *prpLine,bool bpLastLine);
	bool PrintTableHeader(CTime popTifd);
	CCSPtrArray <TABLE_HEADER_COLUMN> omPrintHeadHeaderArray;
	CCSPrint *pomPrint;
	CString omTableName;
	CStringArray omSort;
	char *pcmSelect;
	char *pcmInfo;
	char *pcmSpan;
	int *pimCnt;
	CString omFooterName;
	int imMesse;  // Messekennung - 0 = Messeflüge im Daystop, 1 = Leste Messeflüge <Messename>
};

#endif //__MESSEDAYSTOPTABLEVIEWER_H__
