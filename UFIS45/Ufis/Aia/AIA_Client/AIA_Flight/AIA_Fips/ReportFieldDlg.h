#ifndef AFX_REPORTFIELDDLG_H__87C03A31_A901_11D1_8147_0000B43C4B01__INCLUDED_
#define AFX_REPORTFIELDDLG_H__87C03A31_A901_11D1_8147_0000B43C4B01__INCLUDED_

// ReportFieldDlg.h : Header-Datei
//
#include "CCSEdit.h"

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld CReportFieldDlg 

class CReportFieldDlg : public CDialog
{
// Konstruktion
public:
	CReportFieldDlg(CWnd* pParent = NULL,char *opSelect = "");

	char *pcmSelect;

// Dialogfelddaten
	//{{AFX_DATA(CReportFieldDlg)
	enum { IDD = IDD_REPORTFIELD };
	CCSEdit	m_CE_Select;
	CButton	m_Cancel;
	CButton	m_Ok;
	CString	m_Select;
	//}}AFX_DATA


// Überschreibungen
	// Vom Klassen-Assistenten generierte virtuelle Funktionsüberschreibungen
	//{{AFX_VIRTUAL(CReportFieldDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	//}}AFX_VIRTUAL

// Implementierung
protected:

	// Generierte Nachrichtenzuordnungsfunktionen
	//{{AFX_MSG(CReportFieldDlg)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	virtual void OnCancel();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio fügt zusätzliche Deklarationen unmittelbar vor der vorhergehenden Zeile ein.

#endif // AFX_REPORTFIELDDLG_H__87C03A31_A901_11D1_8147_0000B43C4B01__INCLUDED_
