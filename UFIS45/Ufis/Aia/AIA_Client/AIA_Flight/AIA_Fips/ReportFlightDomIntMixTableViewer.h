#ifndef __FLIGHT_DOM_INT_MIX_VIEWER_H__
#define __FLIGHT_DOM_INT_MIX_VIEWER_H__

#include "stdafx.h"
#include "RotationDlgCedaFlightData.h"
#include "CCSTable.h"
#include "CViewer.h"
#include "CCSPrint.h"


struct FLIGHT_DOM_INT_MIXED_LINEDATA
{

	CString		Date;
	CString		SortDate;

	long AUrno;
	long ARkey;

	int Andf;
	int Anif;
	int Anmf;
	int Anrf;

	long DUrno;
	long DRkey;

	int Dndf;
	int Dnif;
	int Dnmf;
	int Dnrf;

	int Tndf;
	int Tnif;
	int Tnmf;
	int Tnrf;

	int Taf;
	int Tdf;
	int Ttf;



};

/////////////////////////////////////////////////////////////////////////////
// ReportFlightDomIntMixViewer

class ReportFlightDomIntMixTableViewer : public CViewer
{
// Constructions
public:
    ReportFlightDomIntMixTableViewer(CCSPtrArray<ROTATIONDLGFLIGHTDATA> *popData, 
		                                 char *pcpInfo = NULL, char *pcpSelect = NULL);
    ~ReportFlightDomIntMixTableViewer();

    void Attach(CCSTable *popAttachWnd);
	void SetParentDlg(CDialog* ppDlg);
    virtual void ChangeViewTo(const char *pcpViewName);
	void PrintPlanToFile(char *pcpDefPath);
// Internal data processing routines
private:
    void MakeLines();
	int  MakeLine(ROTATIONDLGFLIGHTDATA *prpAFlight, ROTATIONDLGFLIGHTDATA *prpDFlight);
	void MakeLineData(ROTATIONDLGFLIGHTDATA *prpAFlight, ROTATIONDLGFLIGHTDATA *prpDFlight, FLIGHT_DOM_INT_MIXED_LINEDATA &rpLine);
	void MakeColList(FLIGHT_DOM_INT_MIXED_LINEDATA *prlLine, CCSPtrArray<TABLE_COLUMN> &olColList);
	int  CompareGefundenefluege(FLIGHT_DOM_INT_MIXED_LINEDATA *prpGefundenefluege1, FLIGHT_DOM_INT_MIXED_LINEDATA *prpGefundenefluege2);
	int  CompareFlight(FLIGHT_DOM_INT_MIXED_LINEDATA *prpFlight1, FLIGHT_DOM_INT_MIXED_LINEDATA *prpFlight2);
	int  GetVia(CCSPtrArray<VIADATA> *opVias, ROTATIONDLGFLIGHTDATA *prpFlight);

// Operations
public:
	void DeleteAll();
	int CreateLine(FLIGHT_DOM_INT_MIXED_LINEDATA &prpGefundenefluege);
	void DeleteLine(int ipLineno);

// Window refreshing routines
public:
	void UpdateDisplay();
	void DrawHeader();


// Attributes used for filtering condition
private:
	bool bmIsFromSearch;
	// Array der Header
	CStringArray omShownColumns;
	// Sapltengroese
	CUIntArray omAnzChar;

    CCSTable *pomTable;
	CDialog* pomParentDlg;
	CCSPtrArray<ROTATIONDLGFLIGHTDATA> *pomData;
	// Liste der existierenden Airlines
	CStringArray omColumn0;
	CStringArray omColumn1;
	CStringArray omColumn2;
	CStringArray omColumn3;
	// Auftreten der existierenden Airlines
	CUIntArray omCountColumn0;
	CUIntArray omCountColumn1;
	CUIntArray omCountColumn2;
	CUIntArray omCountColumn3;

public:
    CCSPtrArray<FLIGHT_DOM_INT_MIXED_LINEDATA> omLines;
	CCSPtrArray<TABLE_HEADER_COLUMN> omHeaderDataArray;

	// anzuzeigende Header als Array uebergeben
	void SetColumnsToShow(const CStringArray &opShownColumns);

//Print 
	void GetHeader(void);
	CCSPtrArray <TABLE_HEADER_COLUMN> omPrintHeadHeaderArray;
	CCSPrint *pomPrint; 
	void PrintTableView(void);
	bool PrintTableLine(FLIGHT_DOM_INT_MIXED_LINEDATA *prpLine,bool bpLastLine);
	bool PrintTableHeader(void);
	CString omTableName;
	char *pcmInfo;
	char *pcmSelect;
	CString omFooterName;

protected:
	// Mapping Tabellenfeldname/Spaltenueberschrift
	CString GetHeaderContent(CString opCurrentColumns);
	CString GetFieldContent(FLIGHT_DOM_INT_MIXED_LINEDATA* prlLine, 
		                    CString opCurrentColumns, CString opCurrentAirline);
	void SetFieldLength(CString opCurrentColumns);
	// Listen zum Zaehlen erstellen
	void MakeCountList(ROTATIONDLGFLIGHTDATA *prpAFlight, ROTATIONDLGFLIGHTDATA *prpDFlight);

};

#endif