#ifndef AFX_REPORT17DAILYTABLEVIEWER_H__EA915900_F6D9_11D1_A439_0000B45A33F5__INCLUDED_
#define AFX_REPORT17DAILYTABLEVIEWER_H__EA915900_F6D9_11D1_A439_0000B45A33F5__INCLUDED_

// Report17DailyTableViewer.h : Header-Datei
//

#include "stdafx.h"
#include "Table.h"
#include "CViewer.h"
#include "CCSPrint.h"



struct REPORT17TABLE_LINEDATA
{
	long		Urno; 	// Eindeutige Datensatz-Nr.

	CString	Alc ;		
	CString	FltnIn ;
	CString	FltnOut ;
	CString	Act ;
	CString	Nose ;
	CString	Org ;
	CString	Sta ;
	CString	ChtOrg ;
	CString	ViaIn ;
	CString	RemIn ;

	CString	Des ;
	CString	Std ;
	CString	ChtDes ;
	CString	ViaOut ;
	CString	RemOut ;

	CString	Pos ;
	CString	Gp ;
	CString	Ctr ;
	CString	Pax ;
	CString	Einteil ;

	int Folgezeile ;
	
	REPORT17TABLE_LINEDATA() { Folgezeile=0; Urno=0l ; }

};


struct DAILYURNOKEYLIST
{
	CCSPtrArray<REPORT17TABLE_LINEDATA> Rotation;
}; 


/////////////////////////////////////////////////////////////////////////////
// Befehlsziel Report17DailyTableViewer 


class Report17DailyTableViewer : public CViewer
{
// Constructions
public:
    Report17DailyTableViewer(char *pcpInfo=NULL, char *pcpSelect=NULL, char *pspVR=NULL);
    ~Report17DailyTableViewer();

    void Attach(CTable *popAttachWnd);
    virtual void ChangeViewTo(const char *pcpViewName);
// Internal data processing routines
private:
    void MakeLines(void);
	void CompressLines(void);
	bool IsEqualLine(const REPORT17TABLE_LINEDATA &rrpLine1, const REPORT17TABLE_LINEDATA &rrpLine2);
	bool MakeTimeRange(const REPORT17TABLE_LINEDATA &rrpLine1, const REPORT17TABLE_LINEDATA &rrpLine2, CString &ropNewRange);

	void MakeLine(long AUrno, long DUrno);	
	int CreateLine(REPORT17TABLE_LINEDATA &rpLine, long lpUrno);
	int  CompareFlight(long lpNewUrno, long lpCmpUrno);

	void GetHeader(void);
	void GetPrintHeader(void);

// Operations
public:
	void DeleteAll(void);
	void DeleteLine(int ipLineno);
	void UpdateDisplay(void);
	void PrintTableView(void);
	bool PrintTableHeader(void);
	void PrintPlanToFile(char *pcpDefPath);
	bool PrintTableLine(REPORT17TABLE_LINEDATA *prpLine, bool bpLastLine);
	CCSPtrArray <TABLE_HEADER_COLUMN> omPrintHeadHeaderArray;
// Attributes
private:
	bool bmSeason;
    CTable *pomTable;
	CString omFromStr;
	CString omToStr;
///////
//Print 
	CCSPrint *pomPrint;
	CString omTableName;
	CString omFooterName;
	char *pcmInfo;
	char *pcmSelect;
	CString olVerkehrsRechte ;


public:
    CCSPtrArray<REPORT17TABLE_LINEDATA> omLines;
	CMapStringToPtr omLineKeyMap;

protected:
	bool GetVias( CString&, ROTATIONDLGFLIGHTDATA*, CString& ropDes );
	void SetPrintItem( PRINTELEDATA&, CString, bool bpLF=false, bool bpRF=false);
	void SortFreqStrArray( CStringArray& ropSortFreq, CStringArray& opStrArray ) ;
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio f�gt zus�tzliche Deklarationen unmittelbar vor der vorhergehenden Zeile ein.

#endif // AFX_REPORT17DAILYTABLEVIEWER_H__EA915900_F6D9_11D1_A439_0000B45A33F5__INCLUDED_
