#ifndef REPORT_TABLE_SPECIAL_DLG
#define REPORT_TABLE_SPECIAL_DLG

// ReportTableDlg.h : Header-Datei
//
#include "CCSTable.h"
#include "Table.h"
#include "RotationDlgCedaFlightData.h"

// SHA
#include "ReportDomIntTranTableViewer.h"


/////////////////////////////////////////////////////////////////////////////
// Dialogfeld CReportTableSpecialDlg 

class CReportTableSpecialDlg : public CDialog
{
// Konstruktion
public:
	CReportTableSpecialDlg(CWnd* pParent = NULL, int igTable = 0, CCSPtrArray<ROTATIONDLGFLIGHTDATA> *popData = NULL,
					char *pcpInfo = NULL, char *pcpSelect = NULL, char *pcpInfo2 = NULL, int ipArrDep = 0 );
	~CReportTableSpecialDlg();

// Dialogfelddaten
	//{{AFX_DATA(CReportTableSpecialDlg)
	enum { IDD = IDD_REPORT_TABLE_SPECIAL };
	CButton	m_CB_Beenden;
	CButton	m_CB_Papier;
	CButton	m_CB_Drucken;
	CButton	m_CB_Datei;
	//}}AFX_DATA
	CString omHeadline;	//Dialogbox-Überschrift


// Überschreibungen
	// Vom Klassen-Assistenten generierte virtuelle Funktionsüberschreibungen
	//{{AFX_VIRTUAL(CReportTableSpecialDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	//}}AFX_VIRTUAL

public:

	int imDialogBarHeight;

// Implementierung
protected:

	// Generierte Nachrichtenzuordnungsfunktionen
	//{{AFX_MSG(CReportTableSpecialDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnDatei();
	afx_msg void OnDrucken();
	afx_msg void OnPapier();
	afx_msg void OnBeenden();
	virtual void OnCancel();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

private:
	CCSTable *pomReportTable;
	CTable *pomReportCTable;
	int miTable;
	char *pcmInfo;
	char *pcmInfo2;
	char *pcmSelect;
	bool bmCommonDlg;

	CCSPtrArray<ROTATIONDLGFLIGHTDATA> *pomData;

	// SHA
	ReportDomIntTranTableViewer* pomReportDomIntTranTableViewer;

	int imArrDep;
	CStatic* omStatic;

};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio fügt zusätzliche Deklarationen unmittelbar vor der vorhergehenden Zeile ein.

#endif 
