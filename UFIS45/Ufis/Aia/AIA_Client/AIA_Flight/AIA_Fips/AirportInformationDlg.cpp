// AirportInformationDlg.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "AirportInformationDlg.h"
#include "PrivList.h"
#include "PrintInfo.h"
#include "Utils.h"

#include "CcsDdx.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

// Local function prototype
static void AirportInformationDlgCf(void *popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld AirportInformationDlg 
//---------------------------------------------------------------------------
AirportInformationDlg::AirportInformationDlg(CWnd* pParent /*=NULL*/) : CDialog(AirportInformationDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(AirportInformationDlg)
	//}}AFX_DATA_INIT

    ogDdx.Register(this, INF_CHANGE, CString("AIRPORTINFORMATIONDLG"), CString("AirportInformationDlg Update"), AirportInformationDlgCf);
    ogDdx.Register(this, INF_NEW,    CString("AIRPORTINFORMATIONDLG"), CString("AirportInformationDlg New"),    AirportInformationDlgCf);
    ogDdx.Register(this, INF_DELETE, CString("AIRPORTINFORMATIONDLG"), CString("AirportInformationDlg Delete"), AirportInformationDlgCf);

	omListBoxData.pomApc3  = NULL;
	omListBoxData.Apc3Name = "";
	omListBoxData.pomAppl  = NULL;
	omListBoxData.ApplName = "";
	omListBoxData.pomFunk  = NULL;
	omListBoxData.FunkName = "";
	omListBoxData.pomArea  = NULL;
	omListBoxData.AreaName = "";
	omListBoxData.pomInfo  = NULL;
	omListBoxData.InfoUrno = 0;

	imInfoStatus = NO_INFO_STATUS;
	m_key = "DialogPosition\\Info";
}

AirportInformationDlg::~AirportInformationDlg(void)
{
	ogDdx.UnRegister(this, NOTUSED);
	ogInfData.ClearAll(false);

}

void AirportInformationDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(AirportInformationDlg)
	DDX_Control(pDX, IDC_NEWINFO, m_NewInfoButton);
	DDX_Control(pDX, IDC_FUNK_LIST, m_FunkList);
	DDX_Control(pDX, IDC_DATE_LIST, m_DateList);
	DDX_Control(pDX, IDC_AREA_LIST, m_AreaList);
	DDX_Control(pDX, IDC_APPL_LIST, m_ApplList);
	DDX_Control(pDX, IDC_APC3_LIST, m_Apc3List);
	DDX_Control(pDX, IDC_APC3_EDIT, m_Apc3Edit);
	DDX_Control(pDX, IDC_APPL_EDIT, m_ApplEdit);
	DDX_Control(pDX, IDC_AREA_EDIT, m_AreaEdit);
	DDX_Control(pDX, IDC_FUNK_EDIT, m_FunkEdit);
	DDX_Control(pDX, IDC_RESET, m_ResetButton);
	DDX_Control(pDX, IDC_PRINT,  m_PrintButton);
	DDX_Control(pDX, IDC_SEND,	 m_SendButton);
	DDX_Control(pDX, IDC_INSERT, m_InsertButton);
	DDX_Control(pDX, IDC_CHANGE, m_ChangeButton);
	DDX_Control(pDX, IDC_CREATE, m_CreateButton);
	DDX_Control(pDX, IDC_TEXTFIELD,	m_TextField);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(AirportInformationDlg, CDialog)
	//{{AFX_MSG_MAP(AirportInformationDlg)
	ON_BN_CLICKED(IDC_CHANGE,	OnChange)
	ON_BN_CLICKED(IDC_INSERT,	OnInsert)
	ON_BN_CLICKED(IDC_PRINT,	OnPrint)
	ON_BN_CLICKED(IDC_SEND,		OnSend)
	ON_LBN_SELCHANGE(IDC_AREA_LIST, OnSelchangeAreaList)
	ON_LBN_SELCHANGE(IDC_APPL_LIST, OnSelchangeApplList)
	ON_LBN_SELCHANGE(IDC_APC3_LIST, OnSelchangeApc3List)
	ON_LBN_SELCHANGE(IDC_FUNK_LIST, OnSelchangeFunkList)
	ON_LBN_SELCHANGE(IDC_DATE_LIST, OnSelchangeDateList)
	ON_BN_CLICKED(IDC_CREATE,	OnCreate)
	ON_BN_CLICKED(IDC_RESET,	OnReset)
	ON_BN_CLICKED(IDC_NEWINFO, OnNewinfo)
	ON_WM_PAINT()
	ON_WM_SIZE()
 	ON_WM_DESTROY()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Behandlungsroutinen f�r Nachrichten AirportInformationDlg 
//---------------------------------------------------------------------------

void AirportInformationDlg::SaveToReg()
{
	BOOL ok = WriteDialogToReg((CWnd*) this, m_key);
}


void AirportInformationDlg::OnDestroy() 
{
	SaveToReg();
	CDialog::OnDestroy();
}

void AirportInformationDlg::OnSize(UINT nType, int cx, int cy)  
{
	CDialog::OnSize(nType, cx, cy);
	
	// TODO: Add your message handler code here
	m_resizeHelper.OnSize();
	this->Invalidate();
}


BOOL AirportInformationDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

	m_key = "DialogPosition\\Info";

	CRect olApc3EditRect;
	char clStat,clStat1,clStat2;
	imInfoStatus = NO_INFO_STATUS;
	imNewInfoNo = 0;
	imTotalNewInfoNo = 0;

	omReadErrTxt.LoadString(ST_READERR);
	omInsertErrTxt.LoadString(ST_INSERTERR);
	omUpdateErrTxt.LoadString(ST_UPDATEERR);
	omNoDataErrTxt.LoadString(ST_NODATA);
	omBadFormatErrTxt.LoadString(ST_BADFORMAT);


	m_TextField.GetWindowRect(omTextFieldRecDown);
	ScreenToClient(omTextFieldRecDown);
	m_Apc3Edit.GetWindowRect(olApc3EditRect);
	ScreenToClient(olApc3EditRect);
	omTextFieldRecUp = CRect(omTextFieldRecDown.left,omTextFieldRecDown.top,omTextFieldRecDown.right,omTextFieldRecDown.bottom);
//	omTextFieldRecUp = CRect(omTextFieldRecDown.left,olApc3EditRect.top,omTextFieldRecDown.right,omTextFieldRecDown.bottom);
	m_Apc3Edit.ShowWindow(SW_HIDE); 
	m_ApplEdit.ShowWindow(SW_HIDE); 
	m_AreaEdit.ShowWindow(SW_HIDE); 
	m_FunkEdit.ShowWindow(SW_HIDE); 
	m_TextField.MoveWindow(omTextFieldRecUp);
	m_TextField.SetReadOnly(TRUE);

	clStat1 = '1';
	clStat2 = ogPrivList.GetStat("AInfo.m_CreateButton");
	SetWndStatPrio_1(clStat1,clStat2,m_CreateButton);
	clStat2 = ogPrivList.GetStat("AInfo.m_InsertButton");
	SetWndStatPrio_1(clStat1,clStat2,m_InsertButton);
	clStat2 = ogPrivList.GetStat("AInfo.m_ChangeButton");
	SetWndStatPrio_1(clStat1,clStat2,m_ChangeButton);
	clStat2 = ogPrivList.GetStat("AInfo.m_PrintButton");
	SetWndStatPrio_1(clStat1,clStat2,m_PrintButton);
	clStat1 = '0';

	clStat = ogPrivList.GetStat("AInfo.m_SendButton");
	SetWndStatPrio_1(clStat1,clStat2,m_SendButton);

	//clStat = ogPrivList.GetStat("AInfo.m_Apc3Edit");
	//clStat = ogPrivList.GetStat("AInfo.m_ApplEdit");
	//clStat = ogPrivList.GetStat("AInfo.m_AreaEdit");
	//clStat = ogPrivList.GetStat("AInfo.m_FunkEdit");
	//m_Apc3Edit.SetSecState(ogPrivList.GetStat("AInfo.m_Apc3Edit"));

	m_Apc3Edit.SetFormat("x|#x|#x|#");
	m_Apc3Edit.SetTextLimit(1,3);
	m_Apc3Edit.SetBKColor(YELLOW);  
	m_Apc3Edit.SetTextErrColor(RED);
	//------------------------------------
	m_ApplEdit.SetTypeToString("X(32)",32,1);
	m_ApplEdit.SetBKColor(YELLOW);  
	m_ApplEdit.SetTextErrColor(RED);
	//------------------------------------
	m_AreaEdit.SetTypeToString("X(32)",32,1);
	m_AreaEdit.SetBKColor(YELLOW);  
	m_AreaEdit.SetTextErrColor(RED);
	//------------------------------------
	m_FunkEdit.SetTypeToString("X(32)",32,1);
	m_FunkEdit.SetBKColor(YELLOW);  
	m_FunkEdit.SetTextErrColor(RED);


	if(ogInfData.Read("APC3,APPL,AREA,CDAT,FUNK,LSTU,URNO,USEC,USEU")==false)
	{
		ogInfData.omLastErrorMessage.MakeLower();
		if(ogInfData.omLastErrorMessage.Find("no data found")==-1)
		{
			omErrorTxt.Format("%s %d",omReadErrTxt,ogInfData.imLastReturnCode);
			MessageBox(omErrorTxt, GetString(ST_FEHLER), MB_ICONEXCLAMATION);
		}
	}	

	imTotalNewInfoNo = ogInfData.omNewInfoData.GetSize();
	if(imTotalNewInfoNo > 0)
	{
		m_NewInfoButton.SetColors(RED,GRAY,WHITE);
		CString olTemp;
		olTemp.Format("%d/%d",imNewInfoNo,imTotalNewInfoNo);
		m_NewInfoButton.SetWindowText(olTemp);
	}
	else
	{
		m_NewInfoButton.SetColors(SILVER,GRAY,WHITE);
	}

	FillList(IDC_APC3_LIST);

	m_resizeHelper.Init(this->m_hWnd);
	m_resizeHelper.Fix(IDCANCEL,DlgResizeHelper::kNoHFix,DlgResizeHelper::kHeight);
	m_resizeHelper.Fix(IDC_PRINT,DlgResizeHelper::kNoHFix,DlgResizeHelper::kHeight);
	m_resizeHelper.Fix(IDC_CHANGE,DlgResizeHelper::kNoHFix,DlgResizeHelper::kHeight);
	m_resizeHelper.Fix(IDC_INSERT,DlgResizeHelper::kNoHFix,DlgResizeHelper::kHeight);
	m_resizeHelper.Fix(IDC_SEND,DlgResizeHelper::kNoHFix,DlgResizeHelper::kHeight);
	m_resizeHelper.Fix(IDC_CREATE,DlgResizeHelper::kNoHFix,DlgResizeHelper::kHeight);
	m_resizeHelper.Fix(IDC_NEWINFO,DlgResizeHelper::kNoHFix,DlgResizeHelper::kHeight);
	m_resizeHelper.Fix(IDC_RESET,DlgResizeHelper::kNoHFix,DlgResizeHelper::kHeight);

	CRect olRect;
	this->GetWindowRect(&olRect);
	GetDialogFromReg(olRect, m_key);
	SetWindowPos(&wndTop, olRect.left, olRect.top, olRect.Width(), olRect.Height(), SWP_SHOWWINDOW);// | SWP_NOMOVE );


	return TRUE;
}

//---------------------------------------------------------------------------

void AirportInformationDlg::OnPaint() 
{
	CPaintDC olDC(this); // device context for painting
	
	CRect olrectInfo;
	GetWindowRect(&olrectInfo);
	ScreenToClient(&olrectInfo);
	CPen olGrayPen;
	olGrayPen.CreatePen(PS_SOLID, 1, COLORREF(GRAY)); 
	olDC.SelectObject(&olGrayPen);
	//olDC.MoveTo(0, 0);
	//olDC.LineTo(olrectInfo.right,0);
	//olDC.MoveTo(0, 32);
	//olDC.LineTo(olrectInfo.right,32);
	
	// Kein Aufruf von CDialog::OnPaint() f�r Zeichnungsnachrichten
}

//---------------------------------------------------------------------------

void AirportInformationDlg::FillList(UINT ipListBoxID,bool bpSetLBD /*=true*/) 
{
	int ilSelLine;

	UINT ilListBoxID = ipListBoxID;
	if(bpSetLBD == true)
	{
		if(ilListBoxID == IDC_APC3_LIST)
		{
			if(m_Apc3List.GetCount() > 0)
			{
				ilSelLine = m_Apc3List.GetCurSel();
				if(ilSelLine != -1)
				{
					m_Apc3List.GetText(ilSelLine,omListBoxData.Apc3Name);
				}
				else 
				{
					m_Apc3List.GetText(0,omListBoxData.Apc3Name);
				}
			}
			else 
			{
				omListBoxData.Apc3Name = "";
			}
				omListBoxData.ApplName = "";
				omListBoxData.FunkName = "";
				omListBoxData.AreaName = "";
				omListBoxData.InfoUrno = 0;
		}
		if(ilListBoxID == IDC_APPL_LIST)
		{
			if(m_ApplList.GetCount() > 0)
			{
				ilSelLine = m_ApplList.GetCurSel();
				if(ilSelLine != -1)
				{
					m_ApplList.GetText(ilSelLine,omListBoxData.ApplName);
				}
				else 
				{
					m_ApplList.GetText(0,omListBoxData.ApplName);
				}
			}
			else 
			{
				omListBoxData.ApplName = "";
			}
				omListBoxData.FunkName = "";
				omListBoxData.AreaName = "";
				omListBoxData.InfoUrno = 0;
		}
		if(ilListBoxID == IDC_FUNK_LIST)
		{
			if(m_FunkList.GetCount() > 0)
			{
				ilSelLine = m_FunkList.GetCurSel();
				if(ilSelLine != -1)
				{
					m_FunkList.GetText(ilSelLine,omListBoxData.FunkName);
				}
				else 
				{
					m_FunkList.GetText(0,omListBoxData.FunkName);
				}
			}
			else 
			{
				omListBoxData.FunkName = "";
			}
				omListBoxData.AreaName = "";
				omListBoxData.InfoUrno = 0;
		}
		if(ilListBoxID == IDC_AREA_LIST)
		{
			if(m_AreaList.GetCount() > 0)
			{
				ilSelLine = m_AreaList.GetCurSel();
				if(ilSelLine != -1)
				{
					m_AreaList.GetText(ilSelLine,omListBoxData.AreaName);
				}
				else 
				{
					m_AreaList.GetText(0,omListBoxData.AreaName);
				}
			}
			else 
			{
				omListBoxData.AreaName = "";
			}
				omListBoxData.InfoUrno = 0;
		}
		if(ilListBoxID == IDC_DATE_LIST)
		{
			if(m_DateList.GetCount() > 0)
			{
				CString olDateText;

				ilSelLine = m_DateList.GetCurSel();
				if(ilSelLine != -1)
				{
					m_DateList.GetText(ilSelLine,olDateText);
				}
				else 
				{
					m_DateList.GetText(0,olDateText);
				}
				omListBoxData.InfoUrno = atol(olDateText.Mid(olDateText.Find("URNO=")+5));
			}
			else 
			{
				omListBoxData.InfoUrno = 0;
			}
		}
	}
	////////////////////////////////////////////////////////////////////////////////
	POSITION olPos;
	CString olListText;
	if(ogInfData.omApc3Map.GetCount() > 0 )
	{
		if(ilListBoxID == IDC_APC3_LIST || bpSetLBD == false)
		{
			Apc3Info *prlInf;
			if(omListBoxData.Apc3Name == "" || bpSetLBD == false)
			{
				if(ogInfData.omApc3Map.GetCount() > 0 )
				{
					m_Apc3List.ResetContent();
					for(olPos = ogInfData.omApc3Map.GetStartPosition(); olPos != NULL; )
					{
						ogInfData.omApc3Map.GetNextAssoc(olPos,olListText,(void*&)prlInf);
						m_Apc3List.AddString(olListText);
					}
					if(m_Apc3List.GetCount() > 0)
					{
						if(bpSetLBD == true)
						{
							m_Apc3List.GetText(0,omListBoxData.Apc3Name);
							m_Apc3List.SetCurSel(0);
						}
						else
						{
							m_Apc3List.SelectString(-1,omListBoxData.Apc3Name);
						}
						omListBoxData.pomApc3 = ogInfData.FindInf(omListBoxData.Apc3Name);
					}
				}
			}
			else
			{
				omListBoxData.pomApc3 = ogInfData.FindInf(omListBoxData.Apc3Name);
			}
			ilListBoxID = IDC_APPL_LIST;
		}
		if(ilListBoxID == IDC_APPL_LIST)
		{
			ApplInfo *prlInf;
			if(omListBoxData.ApplName == "" || bpSetLBD == false)
			{
				if(omListBoxData.pomApc3->omApplMap.GetCount() > 0 )
				{
					m_ApplList.ResetContent();
					for(olPos = omListBoxData.pomApc3->omApplMap.GetStartPosition(); olPos != NULL; )
					{
						omListBoxData.pomApc3->omApplMap.GetNextAssoc(olPos,olListText,(void*&)prlInf);
						m_ApplList.AddString(olListText);
					}
					if(m_ApplList.GetCount() > 0)
					{
						if(bpSetLBD == true)
						{
							m_ApplList.GetText(0,omListBoxData.ApplName);
							m_ApplList.SetCurSel(0);
						}
						else
						{
							m_ApplList.SelectString(-1,omListBoxData.ApplName);
						}
						omListBoxData.pomAppl = omListBoxData.pomApc3->FindInf(omListBoxData.ApplName);
					}
				}
			}
			else
			{
				omListBoxData.pomAppl = omListBoxData.pomApc3->FindInf(omListBoxData.ApplName);
			}
			ilListBoxID = IDC_FUNK_LIST;
		}
		if(ilListBoxID == IDC_FUNK_LIST)
		{
			FunkInfo *prlInf;
			if(omListBoxData.FunkName == "" || bpSetLBD == false)
			{
				if(omListBoxData.pomAppl->omFunkMap.GetCount() > 0 )
				{
					m_FunkList.ResetContent();
					for(olPos = omListBoxData.pomAppl->omFunkMap.GetStartPosition(); olPos != NULL; )
					{
						omListBoxData.pomAppl->omFunkMap.GetNextAssoc(olPos,olListText,(void*&)prlInf);
						m_FunkList.AddString(olListText);
					}
					if(m_FunkList.GetCount() > 0)
					{
						if(bpSetLBD == true)
						{
							m_FunkList.GetText(0,omListBoxData.FunkName);
							m_FunkList.SetCurSel(0);
						}
						else
						{
							m_FunkList.SelectString(-1,omListBoxData.FunkName);
						}
						omListBoxData.pomFunk = omListBoxData.pomAppl->FindInf(omListBoxData.FunkName);
					}
				}
			}
			else
			{
				omListBoxData.pomFunk = omListBoxData.pomAppl->FindInf(omListBoxData.FunkName);
			}
			ilListBoxID = IDC_AREA_LIST;
		}
		if(ilListBoxID == IDC_AREA_LIST)
		{
			AreaInfo *prlInf;
			if(omListBoxData.AreaName == "" || bpSetLBD == false)
			{
				if(omListBoxData.pomFunk->omAreaMap.GetCount() > 0 )
				{
					m_AreaList.ResetContent();
					for(olPos = omListBoxData.pomFunk->omAreaMap.GetStartPosition(); olPos != NULL; )
					{
						omListBoxData.pomFunk->omAreaMap.GetNextAssoc(olPos,olListText,(void*&)prlInf);
						m_AreaList.AddString(olListText);
					}
					if(m_AreaList.GetCount() > 0)
					{
						if(bpSetLBD == true)
						{
							m_AreaList.GetText(0,omListBoxData.AreaName);
							m_AreaList.SetCurSel(0);
						}
						else
						{
							m_AreaList.SelectString(-1,omListBoxData.AreaName);
						}
						omListBoxData.pomArea = omListBoxData.pomFunk->FindInf(omListBoxData.AreaName);
					}
				}
			}
			else
			{
				omListBoxData.pomArea = omListBoxData.pomFunk->FindInf(omListBoxData.AreaName);
			}
			ilListBoxID = IDC_DATE_LIST;
		}
		if(ilListBoxID == IDC_DATE_LIST)
		{
			INFDATA *prlInf;
			if(omListBoxData.InfoUrno == 0 || bpSetLBD == false)
			{
				if(omListBoxData.pomArea->omUrnoMap.GetCount() > 0 )
				{
					long llUrno;
					m_DateList.ResetContent();
					CTime  olTmpTime = -1;

					for(olPos = omListBoxData.pomArea->omUrnoMap.GetStartPosition(); olPos != NULL; )
					{
						omListBoxData.pomArea->omUrnoMap.GetNextAssoc(olPos,(void*&)llUrno,(void*&)prlInf);
						olListText.Format("%-16s/%-32s         URNO=%i",prlInf->Lstu.Format("%d.%m.%Y/%H:%M"),prlInf->Useu,llUrno);
						
						int i=0;
						for(;i < m_DateList.GetCount();i++)
						{
							CString olTmpString;
							m_DateList.GetText(i,olTmpString);
							CString olTmp = olTmpString.Mid(6,4);
							olTmp.TrimRight();
							if(olTmpString != "" && !olTmp.IsEmpty())
							{

								//			CTime( int nYear, int nMonth, int nDay, int nHour, int nMin, int nSec);
								olTmpTime = CTime(atoi(olTmpString.Mid(6,4)),atoi(olTmpString.Mid(3,2)),atoi(olTmpString.Mid(0,2)),atoi(olTmpString.Mid(11,2)),atoi(olTmpString.Mid(14,2)),0);
								if(prlInf->Lstu>olTmpTime)
								{
									m_DateList.InsertString(i,olListText);//0=fist pos, -1=last pos
									break;
								}
							}
						}
						if(i == m_DateList.GetCount())
						{
							m_DateList.InsertString(-1,olListText);//0=fist pos, -1=last pos
						}
					}
					if(m_DateList.GetCount() > 0)
					{
						if(bpSetLBD == true)
						{
							m_DateList.GetText(0,olListText);
							omListBoxData.InfoUrno = atol(olListText.Mid(olListText.Find("URNO=")+5));
						}
						else
						{
							INFDATA *polInf;
							polInf = omListBoxData.pomArea->FindInf(omListBoxData.InfoUrno);
							CString olTemp;
							olTemp.Format("%-16s/%-32s         URNO=%i",polInf->Lstu.Format("%d.%m.%Y/%H:%M"),polInf->Useu,omListBoxData.InfoUrno);
							m_DateList.SelectString(-1,olTemp);
						}
						omListBoxData.pomInfo = omListBoxData.pomArea->FindInf(omListBoxData.InfoUrno);
					}
				}
			}
			else
			{
				omListBoxData.pomInfo = omListBoxData.pomArea->FindInf(omListBoxData.InfoUrno);
			}
		}
	}
}

//---------------------------------------------------------------------------

void AirportInformationDlg::OnChange() 
{
	char clStat1,clStat2;
	if(imInfoStatus != CHANGE_INFO)
	{
		if(m_DateList.GetCurSel()!= -1)
		{
			imInfoStatus = CHANGE_INFO;
			m_InsertButton.SetCheck(0);
			m_CreateButton.SetCheck(0);

			clStat1 = '1';
			clStat2 = ogPrivList.GetStat("AInfo.m_SendButton");
			SetWndStatPrio_1(clStat1,clStat2,m_SendButton);
			clStat1 = '0';
			clStat2 = ogPrivList.GetStat("AInfo.m_PrintButton");
			SetWndStatPrio_1(clStat1,clStat2,m_PrintButton);

			CString olTemp;
			imNewInfoNo = 0;
			olTemp.Format("%d/%d",imNewInfoNo,ogInfData.omNewInfoData.GetSize());
			m_NewInfoButton.SetWindowText(olTemp);

			m_Apc3List.EnableWindow(FALSE);
			m_ApplList.EnableWindow(FALSE);
			m_FunkList.EnableWindow(FALSE);
			m_AreaList.EnableWindow(FALSE);
			m_DateList.EnableWindow(FALSE);

			m_Apc3Edit.ShowWindow(SW_HIDE); 
			m_ApplEdit.ShowWindow(SW_HIDE); 
			m_AreaEdit.ShowWindow(SW_HIDE); 
			m_FunkEdit.ShowWindow(SW_HIDE);

			m_TextField.MoveWindow(omTextFieldRecUp);
			m_TextField.SetReadOnly(FALSE);
			m_TextField.SetFocus();
		}
		else
		{
			imInfoStatus = NO_INFO_STATUS;
			m_ChangeButton.SetCheck(0);
			m_InsertButton.SetCheck(0);
			m_CreateButton.SetCheck(0);

			Beep(440,70);
			MessageBox(GetString(IDS_STRING901), GetString(ST_FEHLER), MB_ICONEXCLAMATION);


			clStat1 = '0';
			clStat2 = ogPrivList.GetStat("AInfo.m_SendButton");
			SetWndStatPrio_1(clStat1,clStat2,m_SendButton);

			CString olTemp;
			imNewInfoNo = 0;
			olTemp.Format("%d/%d",imNewInfoNo,ogInfData.omNewInfoData.GetSize());
			m_NewInfoButton.SetWindowText(olTemp);

			m_Apc3List.EnableWindow(TRUE);
			m_ApplList.EnableWindow(TRUE);
			m_FunkList.EnableWindow(TRUE);
			m_AreaList.EnableWindow(TRUE);
			m_DateList.EnableWindow(TRUE);

			m_Apc3Edit.ShowWindow(SW_HIDE); 
			m_ApplEdit.ShowWindow(SW_HIDE); 
			m_AreaEdit.ShowWindow(SW_HIDE); 
			m_FunkEdit.ShowWindow(SW_HIDE);

			m_TextField.MoveWindow(omTextFieldRecUp);
			m_TextField.SetReadOnly(TRUE);
		}
	}
	else
	{
		imInfoStatus = NO_INFO_STATUS;
		m_ChangeButton.SetCheck(0);

		clStat1 = '0';
		clStat2 = ogPrivList.GetStat("AInfo.m_SendButton");
		SetWndStatPrio_1(clStat1,clStat2,m_SendButton);
		clStat1 = '1';
		clStat2 = ogPrivList.GetStat("AInfo.m_PrintButton");
		SetWndStatPrio_1(clStat1,clStat2,m_PrintButton);


		m_Apc3List.EnableWindow(TRUE);
		m_ApplList.EnableWindow(TRUE);
		m_FunkList.EnableWindow(TRUE);
		m_AreaList.EnableWindow(TRUE);
		m_DateList.EnableWindow(TRUE);

		m_TextField.SetReadOnly(TRUE);
	}
	m_resizeHelper.OnSize();
	this->Invalidate();
}

//---------------------------------------------------------------------------

void AirportInformationDlg::OnInsert() 
{
	char clStat1,clStat2;
	if(imInfoStatus != INSERT_INFO)
	{
		imInfoStatus = INSERT_INFO;

		m_ChangeButton.SetCheck(0);
		m_CreateButton.SetCheck(0);

		clStat1 = '1';
		clStat2 = ogPrivList.GetStat("AInfo.m_SendButton");
		SetWndStatPrio_1(clStat1,clStat2,m_SendButton);
		clStat1 = '0';
		clStat2 = ogPrivList.GetStat("AInfo.m_PrintButton");
		SetWndStatPrio_1(clStat1,clStat2,m_PrintButton);

		CString olTemp;
		imNewInfoNo = 0;
		olTemp.Format("%d/%d",imNewInfoNo,ogInfData.omNewInfoData.GetSize());
		m_NewInfoButton.SetWindowText(olTemp);

		m_FunkList.EnableWindow(TRUE);
		m_AreaList.EnableWindow(TRUE);
		m_ApplList.EnableWindow(TRUE);
		m_Apc3List.EnableWindow(TRUE);
		m_DateList.EnableWindow(FALSE);

		m_DateList.SetCurSel(-1);
		
		m_Apc3Edit.ShowWindow(SW_HIDE); 
		m_ApplEdit.ShowWindow(SW_HIDE); 
		m_AreaEdit.ShowWindow(SW_HIDE); 
		m_FunkEdit.ShowWindow(SW_HIDE);
		m_TextField.MoveWindow(omTextFieldRecUp);

		m_TextField.SetFocus();
		m_TextField.SetReadOnly(FALSE);
	}
	else
	{
		imInfoStatus = NO_INFO_STATUS;
		m_InsertButton.SetCheck(0);

		clStat1 = '0';
		clStat2 = ogPrivList.GetStat("AInfo.m_SendButton");
		SetWndStatPrio_1(clStat1,clStat2,m_SendButton);
		clStat1 = '1';
		clStat2 = ogPrivList.GetStat("AInfo.m_PrintButton");
		SetWndStatPrio_1(clStat1,clStat2,m_PrintButton);

		m_DateList.EnableWindow(TRUE);
		m_TextField.SetReadOnly(TRUE);
	}
	m_resizeHelper.OnSize();
	this->Invalidate();
}

//---------------------------------------------------------------------------

void AirportInformationDlg::OnCreate() 
{
	char clStat1,clStat2;
	if(imInfoStatus != CREATE_INFO)
	{
		imInfoStatus = CREATE_INFO;

		CString olTemp;
		imNewInfoNo = 0;
		olTemp.Format("%d/%d",imNewInfoNo,ogInfData.omNewInfoData.GetSize());
		m_NewInfoButton.SetWindowText(olTemp);

		m_InsertButton.SetCheck(0);
		m_ChangeButton.SetCheck(0);
		
		m_Apc3List.EnableWindow(TRUE);
		m_ApplList.EnableWindow(TRUE);
		m_FunkList.EnableWindow(TRUE);
		m_AreaList.EnableWindow(TRUE);
		m_DateList.EnableWindow(FALSE);

		m_DateList.SetCurSel(-1);

		
		m_TextField.MoveWindow(omTextFieldRecDown);
		m_TextField.SetReadOnly(FALSE);

		m_Apc3Edit.ShowWindow(SW_SHOW); 
		m_ApplEdit.ShowWindow(SW_SHOW); 
		m_AreaEdit.ShowWindow(SW_SHOW); 
		m_FunkEdit.ShowWindow(SW_SHOW);
		

		clStat1 = '1';
		clStat2 = ogPrivList.GetStat("AInfo.m_SendButton");
		SetWndStatPrio_1(clStat1,clStat2,m_SendButton);
		clStat1 = '0';
		clStat2 = ogPrivList.GetStat("AInfo.m_PrintButton");
		SetWndStatPrio_1(clStat1,clStat2,m_PrintButton);
		
		m_ApplEdit.SetFocus();

		m_Apc3Edit.SetInitText(pcgHome);
		m_ApplEdit.SetInitText("");
		m_AreaEdit.SetInitText("");
		m_FunkEdit.SetInitText("");
	}
	else
	{
		imInfoStatus = NO_INFO_STATUS;
		m_CreateButton.SetCheck(0);

		clStat1 = '0';
		clStat2 = ogPrivList.GetStat("AInfo.m_SendButton");
		SetWndStatPrio_1(clStat1,clStat2,m_SendButton);
		clStat1 = '1';
		clStat2 = ogPrivList.GetStat("AInfo.m_PrintButton");
		SetWndStatPrio_1(clStat1,clStat2,m_PrintButton);

		m_DateList.EnableWindow(TRUE);

		m_Apc3Edit.ShowWindow(SW_HIDE); 
		m_ApplEdit.ShowWindow(SW_HIDE); 
		m_AreaEdit.ShowWindow(SW_HIDE); 
		m_FunkEdit.ShowWindow(SW_HIDE);

		m_TextField.MoveWindow(omTextFieldRecUp);
		m_TextField.SetReadOnly(TRUE);
	}
	m_resizeHelper.OnSize();
	this->Invalidate();
}

//---------------------------------------------------------------------------

void AirportInformationDlg::OnSend() 
{
	omErrorTxt.Empty();
	int ilMaxTextLength = 2000;
	if(imInfoStatus != NO_INFO_STATUS)
	{
		AfxGetApp()->DoWaitCursor(1);
		INFDATA rlInf;
		bool ilStatus = true;
		switch(imInfoStatus)
		{
			/////////////////////////////////////////////////////////////////
			case CHANGE_INFO:////////////////////////////////////////////////
			{
				if(omListBoxData.Apc3Name == "") ilStatus = false;
				if(omListBoxData.ApplName == "") ilStatus = false;
				if(omListBoxData.FunkName == "") ilStatus = false;
				if(omListBoxData.AreaName == "") ilStatus = false;
								
				if(ilStatus == true)
				{
					CString olInfoText;
					m_TextField.GetWindowText(olInfoText);
					ogInfData.TranslateText(&rlInf, olInfoText, false);
					int ilToGreate = ogInfData.imTextLength - ilMaxTextLength;
					if( ilToGreate > 0)
					{
						Beep(440,70);
						CString olTemp;
						olTemp.Format("Sie haben %d Zeichen zuviel eingegeben.",ilToGreate);
						MessageBox(olTemp, GetString(ST_FEHLER), MB_ICONEXCLAMATION);
					}
					else
					{
						strcpy(rlInf.Apc3,omListBoxData.pomInfo->Apc3);
						strcpy(rlInf.Appl,omListBoxData.pomInfo->Appl);
						strcpy(rlInf.Funk,omListBoxData.pomInfo->Funk);
						strcpy(rlInf.Area,omListBoxData.pomInfo->Area);
						rlInf.Urno = omListBoxData.pomInfo->Urno;
						strcpy(rlInf.Useu,pcgUser);
						rlInf.Cdat = omListBoxData.pomInfo->Cdat;
						rlInf.Lstu = CTime::GetCurrentTime();

						if(ogInfData.Update(&rlInf)==false)
						{
							omErrorTxt.Format("%s \n%d",omUpdateErrTxt,ogInfData.imLastReturnCode);
							MessageBox(omErrorTxt, GetString(ST_FEHLER), MB_ICONEXCLAMATION);
						}	
						else
						{
							OnChange();
						}
					}
				}
				else
				{
					Beep(440,70);
					MessageBox(GetString(IDS_STRING902), GetString(ST_FEHLER), MB_ICONEXCLAMATION);
				}
			}
			break;
			/////////////////////////////////////////////////////////////////
			case INSERT_INFO:////////////////////////////////////////////////
			{
				if(omListBoxData.Apc3Name == "") ilStatus = false;
				if(omListBoxData.ApplName == "") ilStatus = false;
				if(omListBoxData.FunkName == "") ilStatus = false;
				if(omListBoxData.AreaName == "") ilStatus = false;
								
				if(ilStatus == true)
				{
					CString olInfoText;
					m_TextField.GetWindowText(olInfoText);
					ogInfData.TranslateText(&rlInf, olInfoText, false);
					int ilToGreate = ogInfData.imTextLength - ilMaxTextLength;
					if( ilToGreate > 0)
					{
						Beep(440,70);
						CString olTemp;
						olTemp.Format("Sie haben %d Zeichen zuviel eingegeben.",ilToGreate);
						MessageBox(olTemp,  GetString(ST_FEHLER), MB_ICONEXCLAMATION);
					}
					else
					{
						strcpy(rlInf.Apc3,omListBoxData.Apc3Name);
						strcpy(rlInf.Appl,omListBoxData.ApplName);
						strcpy(rlInf.Funk,omListBoxData.FunkName);
						strcpy(rlInf.Area,omListBoxData.AreaName);

						rlInf.Urno = ogBasicData.GetNextUrno();
						long llOldUrno = omListBoxData.InfoUrno;
						omListBoxData.InfoUrno = rlInf.Urno;

						rlInf.Cdat = CTime::GetCurrentTime();
						rlInf.Lstu = CTime::GetCurrentTime();
						strcpy(rlInf.Usec,pcgUser);
						strcpy(rlInf.Useu,pcgUser);

						INFDATA *prlInf = new INFDATA;
						*prlInf = rlInf;
						if(ogInfData.Insert(prlInf)==false)
						{
							delete prlInf;
							omListBoxData.InfoUrno = llOldUrno;
							omErrorTxt.Format("%s \n%d",omInsertErrTxt,ogInfData.imLastReturnCode);
							MessageBox(omErrorTxt,  GetString(ST_FEHLER), MB_ICONEXCLAMATION);
						}	
						else
						{
							OnInsert();
						}
					}
				}
				else
				{
					Beep(440,70);
					MessageBox(GetString(IDS_STRING902), GetString(ST_FEHLER), MB_ICONEXCLAMATION);
				}
			}
			break;
			/////////////////////////////////////////////////////////////////
			case CREATE_INFO:////////////////////////////////////////////////
			{
				CString olTemp;
				m_Apc3Edit.GetWindowText(olTemp);
				olTemp.TrimLeft();
				olTemp.TrimRight();
				m_Apc3Edit.SetInitText(olTemp);

				m_ApplEdit.GetWindowText(olTemp);
				olTemp.TrimLeft();
				olTemp.TrimRight();
				m_ApplEdit.SetInitText(olTemp);

				m_AreaEdit.GetWindowText(olTemp);
				olTemp.TrimLeft();
				olTemp.TrimRight();
				m_AreaEdit.SetInitText(olTemp);

				m_FunkEdit.GetWindowText(olTemp);
				olTemp.TrimLeft();
				olTemp.TrimRight();
				m_FunkEdit.SetInitText(olTemp);

				if(m_Apc3Edit.GetStatus() == false)
				{
					ilStatus = false;
					if(m_Apc3Edit.GetWindowTextLength() == 0)
					{
						omErrorTxt += "Flughafen > " +  omNoDataErrTxt;
					}
					else
					{
						omErrorTxt += "Flughafen > " + omBadFormatErrTxt;
					}
				}
				if(m_ApplEdit.GetStatus() == false)
				{
					ilStatus = false;
					if(m_ApplEdit.GetWindowTextLength() == 0)
					{
						omErrorTxt += "Programm > " +  omNoDataErrTxt;
					}
					else
					{
						omErrorTxt += "Programm > " + omBadFormatErrTxt;
					}
				}
				if(m_AreaEdit.GetStatus() == false)
				{
					ilStatus = false;
					if(m_AreaEdit.GetWindowTextLength() == 0)
					{
						omErrorTxt += "Funktionsbereich > " +  omNoDataErrTxt;
					}
					else
					{
						omErrorTxt += "Funktionsbereich > " + omBadFormatErrTxt;
					}
				}
				if(m_FunkEdit.GetStatus() == false)
				{
					ilStatus = false;
					if(m_FunkEdit.GetWindowTextLength() == 0)
					{
						omErrorTxt += "Arbeitsbereich > " +  omNoDataErrTxt;
					}
					else
					{
						omErrorTxt += "Arbeitsbereich > " + omBadFormatErrTxt;
					}
				}
				//Konsistenz-Check//
				if(ilStatus == true)
				{
					//char clWhere[100]; //MBR 27.04.1999 --> Puffer zu klein ==> Absturz
					char clWhere[200]; 
					m_Apc3Edit.GetWindowText(rlInf.Apc3,4);
					m_ApplEdit.GetWindowText(rlInf.Appl,32);
					m_FunkEdit.GetWindowText(rlInf.Funk,32);
					m_AreaEdit.GetWindowText(rlInf.Area,32);

					sprintf(clWhere,"WHERE APC3='%s' AND APPL='%s' AND FUNK='%s' AND AREA='%s'",rlInf.Apc3,rlInf.Appl,rlInf.Funk,rlInf.Area);
					if(ogInfData.ReadSpecialData(NULL,clWhere,"URNO",false) == true)
					{
						ilStatus = false;
						omErrorTxt += "Dieser Pfad wurde existiert bereits";
					}
				}
				if(ilStatus == true)
				{
					CString olInfoText;
					m_TextField.GetWindowText(olInfoText);
					ogInfData.TranslateText(&rlInf, olInfoText, false);
					int ilToGreate = ogInfData.imTextLength - ilMaxTextLength;
					if( ilToGreate > 0)
					{
						Beep(440,70);
						CString olTemp;
						olTemp.Format("Sie haben %d Zeichen zuviel eingegeben.",ilToGreate);
						MessageBox(olTemp, GetString(ST_FEHLER), MB_ICONEXCLAMATION);
					}
					else
					{
						rlInf.Urno = ogBasicData.GetNextUrno();
						rlInf.Cdat = CTime::GetCurrentTime();
						rlInf.Lstu = CTime::GetCurrentTime();
						strcpy(rlInf.Usec,pcgUser);
						strcpy(rlInf.Useu,pcgUser);

						INFDATA *prlInf = new INFDATA;
						*prlInf = rlInf;
						if(ogInfData.Insert(prlInf)==false)
						{
							delete prlInf;

							omErrorTxt.Format("%s \n%d",omInsertErrTxt,ogInfData.imLastReturnCode);
							MessageBox(omErrorTxt,  GetString(ST_FEHLER), MB_ICONEXCLAMATION);
						}	
						else
						{
							OnCreate();

							omListBoxData.Apc3Name = rlInf.Apc3;
							omListBoxData.ApplName = rlInf.Appl;
							omListBoxData.FunkName = rlInf.Funk;
							omListBoxData.AreaName = rlInf.Area;
							omListBoxData.InfoUrno = rlInf.Urno;
							FillList(IDC_APC3_LIST,false);
						}
					}
				}
				else
				{
					Beep(440,70);
					MessageBox(omErrorTxt,  GetString(ST_FEHLER), MB_ICONEXCLAMATION);
				}
			}
			break;
			/////////////////////////////////////////////////////////////////
			default://///////////////////////////////////////////////////////
			{
				// Do nothing
			}
			break;
		}
		AfxGetApp()->DoWaitCursor(-1);
	}
	m_resizeHelper.OnSize();
	this->Invalidate();
}

//---------------------------------------------------------------------------

void AirportInformationDlg::OnNewinfo() 
{
	char clStat1,clStat2;
	if(ogInfData.omNewInfoData.GetSize() > 0)
	{
		AfxGetApp()->DoWaitCursor(1);
		imInfoStatus = NEW_INFO;

		m_InsertButton.SetCheck(0);
		m_ChangeButton.SetCheck(0);
		m_CreateButton.SetCheck(0);

		m_Apc3List.EnableWindow(TRUE);
		m_ApplList.EnableWindow(TRUE);
		m_FunkList.EnableWindow(TRUE);
		m_AreaList.EnableWindow(TRUE);
		m_DateList.EnableWindow(TRUE);

		m_Apc3Edit.ShowWindow(SW_HIDE); 
		m_ApplEdit.ShowWindow(SW_HIDE); 
		m_AreaEdit.ShowWindow(SW_HIDE); 
		m_FunkEdit.ShowWindow(SW_HIDE);

		m_TextField.MoveWindow(omTextFieldRecUp);
		m_TextField.SetReadOnly(TRUE);


		clStat1 = '0';
		clStat2 = ogPrivList.GetStat("AInfo.m_SendButton");
		SetWndStatPrio_1(clStat1,clStat2,m_SendButton);
		clStat1 = '1';
		clStat2 = ogPrivList.GetStat("AInfo.m_PrintButton");
		SetWndStatPrio_1(clStat1,clStat2,m_PrintButton);

		CString olTemp;
		int ilNewInfos = ogInfData.omNewInfoData.GetSize();
		imNewInfoNo++;
		if(imNewInfoNo > ilNewInfos)
			imNewInfoNo = 1;

		olTemp.Format("%d/%d",imNewInfoNo,ilNewInfos);
		m_NewInfoButton.SetWindowText(olTemp);

		int olPos = imNewInfoNo - 1;

		INFDATA *polInf = ogInfData.GetInfByUrno(ogInfData.omNewInfoData[olPos].Urno);


		if (polInf != NULL)
		{
			omListBoxData.Apc3Name = polInf->Apc3;
			omListBoxData.ApplName = polInf->Appl;
			omListBoxData.FunkName = polInf->Funk;
			omListBoxData.AreaName = polInf->Area;
			omListBoxData.InfoUrno = polInf->Urno;
			FillList(IDC_APC3_LIST,false);

			CString olInfoText;
			char clWhere[100];
			CCSPtrArray<INFDATA> olInfCPA;
			sprintf(clWhere,"WHERE URNO=%ld",ogInfData.omNewInfoData[olPos].Urno);
			if(ogInfData.ReadSpecialData(&olInfCPA,clWhere,"TEXT",false) == true)
			{
				INFDATA	rlInf = olInfCPA[0];
				ogInfData.TranslateText(&rlInf, olInfoText, true);
				m_TextField.SetWindowText(olInfoText);
			}
			olInfCPA.DeleteAll();

			if(imNewInfoNo == ilNewInfos)
				m_NewInfoButton.SetColors(SILVER,GRAY,WHITE);
		}
		AfxGetApp()->DoWaitCursor(-1);
	}
	m_resizeHelper.OnSize();
	this->Invalidate();
}

//---------------------------------------------------------------------------

void AirportInformationDlg::OnPrint() 
{
	if(imInfoStatus == NO_INFO_STATUS)
	{
		if(m_DateList.GetCurSel()!= -1)
		{
			CString olInfoText;
			char clWhere[100];
			CCSPtrArray<INFDATA> olInf;

			sprintf(clWhere,"WHERE URNO=%ld",omListBoxData.pomInfo->Urno);
			if(ogInfData.ReadSpecialData(&olInf,clWhere,"",false) == true)
			{
				INFDATA	rlInf = olInf[0];
				PrintInfo olPInfo("Airport Information",(CWnd*)this);
				if(olPInfo.InitializePrinter() == true)
				{
					SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
					olPInfo.Print(rlInf);
					SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
				}
			}
			olInf.DeleteAll();
		}
		else
		{
			Beep(440,70);
			MessageBox(GetString(IDS_STRING901), GetString(ST_FEHLER), MB_ICONEXCLAMATION);
		}
	}
}

//---------------------------------------------------------------------------

void AirportInformationDlg::OnReset() 
{
	m_TextField.SetWindowText("");
	m_TextField.SetFocus();
	CString olTemp;
	olTemp.Format("0/%d",ogInfData.omNewInfoData.GetSize());
	m_NewInfoButton.SetWindowText(olTemp);
	m_resizeHelper.OnSize();
	this->Invalidate();
}

//---------------------------------------------------------------------------

void AirportInformationDlg::OnSelchangeApc3List() 
{
	CString olSelectListBoxText;
	FillList(IDC_APC3_LIST);
	m_Apc3List.GetText(m_Apc3List.GetCurSel(),olSelectListBoxText);
	m_Apc3Edit.SetInitText(olSelectListBoxText);
}
//-----------------
void AirportInformationDlg::OnSelchangeApplList() 
{
	CString olSelectListBoxText;
	FillList(IDC_APPL_LIST);
	m_ApplList.GetText(m_ApplList.GetCurSel(),olSelectListBoxText);
	m_ApplEdit.SetInitText(olSelectListBoxText);
}
//-----------------
void AirportInformationDlg::OnSelchangeFunkList() 
{
	CString olSelectListBoxText;
	FillList(IDC_FUNK_LIST);
	m_FunkList.GetText(m_FunkList.GetCurSel(),olSelectListBoxText);
	m_FunkEdit.SetInitText(olSelectListBoxText);
}
//-----------------
void AirportInformationDlg::OnSelchangeAreaList() 
{
	CString olSelectListBoxText;
	FillList(IDC_AREA_LIST);
	m_AreaList.GetText(m_AreaList.GetCurSel(),olSelectListBoxText);
	m_AreaEdit.SetInitText(olSelectListBoxText);
}
//-----------------
void AirportInformationDlg::OnSelchangeDateList() 
{
	FillList(IDC_DATE_LIST);

	CString olTemp;
	imNewInfoNo = 0;
	olTemp.Format("%d/%d",imNewInfoNo,ogInfData.omNewInfoData.GetSize());
	m_NewInfoButton.SetWindowText(olTemp);

	CString olInfoText;
	char clWhere[100];
	CCSPtrArray<INFDATA> olInf;

	sprintf(clWhere,"WHERE URNO=%ld",omListBoxData.pomInfo->Urno);
	if(ogInfData.ReadSpecialData(&olInf,clWhere,"TEXT",false) == true)
	{
		INFDATA	rlInf = olInf[0];
		ogInfData.TranslateText(&rlInf, olInfoText, true);
		m_TextField.SetWindowText(olInfoText);
	}
	olInf.DeleteAll();
}

//---------------------------------------------------------------------------

static void AirportInformationDlgCf(void *popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	AirportInformationDlg *polViewer = (AirportInformationDlg *)popInstance;
	INFDATA	*polInf = (INFDATA *)vpDataPointer;
    if (ipDDXType == INF_NEW || ipDDXType == INF_CHANGE)
	{
		if(ogInfData.omNewInfoData.GetSize() > 0 && ogInfData.omNewInfoData.GetSize() > polViewer->imTotalNewInfoNo)
		{
			polViewer->imTotalNewInfoNo = ogInfData.omNewInfoData.GetSize();
			CString olTemp;
			olTemp.Format("%d/%d",polViewer->imNewInfoNo,ogInfData.omNewInfoData.GetSize());
			polViewer->m_NewInfoButton.SetWindowText(olTemp);
			polViewer->m_NewInfoButton.SetColors(RED,GRAY,WHITE);
		}

		if(polViewer->omListBoxData.pomApc3 != NULL)
		{
			polViewer->FillList(IDC_APC3_LIST,false);
		}
		else
		{
			polViewer->FillList(IDC_APC3_LIST);
		}
	}
} 

//---------------------------------------------------------------------------

