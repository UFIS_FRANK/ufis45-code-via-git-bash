// SetupDlg.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "CCSEdit.h"
#include "SetupDlg.h"
#include "CedaCfgData.h"
#include "BasicData.h"
#include "CCSGlobl.h"
#include "CCSDDX.h"
#include "PrivList.h"
#include "Konflikte.h"
#include "ResGroupDlg.h"
#include "SpotAllocation.h"
#include "PosDiagram.h"
#include "GatDiagram.h"
#include "BltDiagram.h"
#include "WroDiagram.h"
#include "CcaDiagram.h"
#include "Utils.h"
#include "KonflikteDlg.h"
#include "ButtonListDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld SetupDlg 


SetupDlg::SetupDlg(CWnd* pParent /*=NULL*/)
	: CDialog(SetupDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(SetupDlg)
	m_Monitors = -1;
	m_Batch1 = -1;
	m_Daily1 = -1;
	m_DailyRot1 = -1;
	m_FlightDia1 = -1;
	m_Season1 = -1;
	m_SeasonRot1 = -1;
	m_CcaDia1 = -1;
	m_superUser = false;
	m_initUserCB = false;
	//}}AFX_DATA_INIT
//	pomConflictDlg = new GatPosConflictSetupDlg();
	pomConflictDlg = NULL;
	m_key = "DialogPosition\\Setup";
}


void SetupDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(SetupDlg)
	DDX_Control(pDX, IDOK, m_CB_Save);
	DDX_Control(pDX, IDC_CONF20, m_CC_Conf20);
	DDX_Control(pDX, IDC_CONF19, m_CC_Conf19);
	DDX_Control(pDX, IDC_CONF18_MIN, m_CE_Conf18_Min);
	DDX_Control(pDX, IDC_CONF18, m_CC_Conf18);
	DDX_Control(pDX, IDC_CONF17, m_CC_Conf17);
	DDX_Control(pDX, IDC_CONF16, m_CC_Conf16);
	DDX_Control(pDX, IDC_CONF15, m_CC_Conf15);
	DDX_Control(pDX, IDC_CONF14, m_CC_Conf14);
	DDX_Control(pDX, IDC_CONF12_MIN, m_CE_Conf12_Min);
	DDX_Control(pDX, IDC_CONF9_MIN, m_CE_Conf9_Min);
	DDX_Control(pDX, IDC_CONF7_MIN, m_CE_Conf7_Min);
	DDX_Control(pDX, IDC_CONF4_MIN, m_CE_Conf4_Min);
	DDX_Control(pDX, IDC_CONF3_MIN, m_CE_Conf3_Min);
	DDX_Control(pDX, IDC_CONF2_MIN, m_CE_Conf2_Min);
	DDX_Control(pDX, IDC_CONF13_MIN, m_CE_Conf13_Min);
	DDX_Control(pDX, IDC_CONF11_MIN, m_CE_Conf11_Min);
	DDX_Control(pDX, IDC_CONF10_MIN, m_CE_Conf10_Min);
	DDX_Control(pDX, IDC_CONF9, m_CC_Conf9);
	DDX_Control(pDX, IDC_CONF8, m_CC_Conf8);
	DDX_Control(pDX, IDC_CONF7, m_CC_Conf7);
	DDX_Control(pDX, IDC_CONF6, m_CC_Conf6);
	DDX_Control(pDX, IDC_CONF5, m_CC_Conf5);
	DDX_Control(pDX, IDC_CONF4, m_CC_Conf4);
	DDX_Control(pDX, IDC_CONF3, m_CC_Conf3);
	DDX_Control(pDX, IDC_CONF2, m_CC_Conf2);
	DDX_Control(pDX, IDC_CONF13, m_CC_Conf13);
	DDX_Control(pDX, IDC_CONF12, m_CC_Conf12);
	DDX_Control(pDX, IDC_CONF11, m_CC_Conf11);
	DDX_Control(pDX, IDC_CONF10, m_CC_Conf10);
	DDX_Control(pDX, IDC_CONF1, m_CC_Conf1);
	DDX_Control(pDX, IDC_STATIC_FONT, m_CS_Font);
	DDX_Control(pDX, IDC_PKNO_CB, m_UserCB);
	DDX_Radio(pDX, IDC_RADIO1, m_Monitors);
	DDX_Radio(pDX, IDC_BATCH1, m_Batch1);
	DDX_Radio(pDX, IDC_DAILY1, m_Daily1);
	DDX_Radio(pDX, IDC_DAILYROT1, m_DailyRot1);
	DDX_Radio(pDX, IDC_FLIGHTDIA1, m_FlightDia1);
	DDX_Radio(pDX, IDC_SEASON1, m_Season1);
	DDX_Radio(pDX, IDC_SEASONROT1, m_SeasonRot1);
	DDX_Radio(pDX, IDC_CCADIA1, m_CcaDia1);
	DDX_Control(pDX, IDC_EDIT_ARCHIVE, m_CE_Archive);
	DDX_Control(pDX, IDC_EDIT_ARCHIVE_FUTURE, m_CE_Archive_Future);
	DDX_Control(pDX, IDC_RESGROUPS, m_CB_ResGroups);
	DDX_Control(pDX, IDC_EDIT_GATEBUFFER, m_CE_GateBuffer);
	DDX_Control(pDX, IDC_EDIT_POSBUFFER, m_CE_PosBuffer);
	DDX_Control(pDX, IDC_EDIT_XDAYS, m_CE_XDays);
	DDX_Control(pDX, IDC_EDIT_TIMELINEBUFFER, m_CE_TimelineBuffer);
	DDX_Control(pDX, IDC_EDIT_WINGOVERBUFFER, m_CE_WingBuffer);
	DDX_Control(pDX, IDC_CHECK_AC_CHANGE, m_Conf_AChange);
	DDX_Control(pDX, IDC_COMBO_POS_WO_CONF, m_CB_PosGroupNoConfl);
	//}}AFX_DATA_MAP
}
SetupDlg::~SetupDlg()
{
	omMonitorButtons1.RemoveAll();
	omMonitorButtons2.RemoveAll();
	omMonitorButtons3.RemoveAll();
	delete pomConflictDlg;
	pomConflictDlg = NULL;
}


BEGIN_MESSAGE_MAP(SetupDlg, CDialog)
	//{{AFX_MSG_MAP(SetupDlg)
	ON_CBN_SELCHANGE(IDC_PKNO_CB, OnSelchangePknoCb)
	ON_BN_CLICKED(IDC_RADIO1, OnRadio1)
	ON_BN_CLICKED(IDC_RADIO2, OnRadio2)
	ON_BN_CLICKED(IDC_RADIO3, OnRadio3)
	ON_BN_CLICKED(IDC_FONT, OnFont)
	ON_BN_CLICKED(IDC_RESGROUPS, OnResGroups)
	ON_WM_SIZE()
 	ON_WM_DESTROY()
	ON_BN_CLICKED(IDC_CHECK_AC_CHANGE, OnDispAC)
	ON_CBN_SELCHANGE(IDC_COMBO_POS_WO_CONF, OnSelchangePosWoConf)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Behandlungsroutinen f�r Nachrichten SetupDlg 

void SetupDlg::OnSelchangePosWoConf() 
{
	CString olPosGrp;
    m_CB_PosGroupNoConfl.GetLBText(m_CB_PosGroupNoConfl.GetCurSel(), ogPosWoConflicts);
}


void SetupDlg::OnSelchangePknoCb() 
{
	// TODO: Code f�r die Behandlungsroutine der Steuerelement-Benachrichtigung hier einf�gen

	if (!m_superUser)
		return;

	if (pomConflictDlg)
	{
		delete pomConflictDlg;
		pomConflictDlg = NULL;
	}


	CString olSelUser;
    m_UserCB.GetLBText(m_UserCB.GetCurSel(), olSelUser);

	ogBasicData.omUserID = olSelUser;
	strcpy(CCSCedaData::pcmUser, olSelUser);

	ogCfgData.ReadCfgData();
	ogCfgData.SetCfgData();
	ogCfgData.ReadMonitorSetup();
	ogCfgData.ReadFontSetup();
//	ogCfgData.ReadPopsReportDailyAlcSetup();
	ogCfgData.ReadConflictSetup();	

	ogCfgData.ReadAllConfigData();	

//	ogSpotAllocation.Init();
	
//	ogKonflikte.SetConfiguration();

	ogCfgData.ReadGateBufferTimeSetup();	
	ogCfgData.ReadPosBufferTimeSetup();	
	ogCfgData.ReadArchivePastBufferTimeSetup();	
	ogCfgData.ReadArchiveFutureBufferTimeSetup();	
	ogCfgData.ReadXDaysSetup();	
	ogCfgData.ReadTimelineBufferSetup();	
	ogCfgData.ReadWingoverlapBufferSetup();	
	ogCfgData.ReadDisplayAircaftChangeSetup();	

	ogResGroupData.ReadGroups();
	ogCfgData.ReadPosWoConflictsSetup();

	OnInitDialog();
}

void SetupDlg::OnRadio1() 
{
	CObject *polObj;
	POSITION pos;
    for(pos = omMonitorButtons1.GetHeadPosition(); pos != NULL; )
    {
	    polObj = omMonitorButtons1.GetNext(pos);
		if(polObj != NULL)
		{
			((CButton *)polObj)->EnableWindow(TRUE);
			((CButton *)polObj)->SetCheck(1);
		}
    }	
    for(pos = omMonitorButtons2.GetHeadPosition(); pos != NULL; )
    {
	    polObj = omMonitorButtons2.GetNext(pos);
		if(polObj != NULL)
		{
			((CButton *)polObj)->EnableWindow(FALSE);
			((CButton *)polObj)->SetCheck(0);
		}
    }	
    for(pos = omMonitorButtons3.GetHeadPosition(); pos != NULL; )
    {
	    polObj = omMonitorButtons3.GetNext(pos);
		if(polObj != NULL)
		{
			((CButton *)polObj)->EnableWindow(FALSE);
			((CButton *)polObj)->SetCheck(0);
		}
    }	 
}
 
void SetupDlg::OnRadio2() 
{

	CObject *polObj;
	POSITION pos;
    for(pos = omMonitorButtons1.GetHeadPosition(); pos != NULL; )
    {
	    polObj = omMonitorButtons1.GetNext(pos);
		if(polObj != NULL)
		{
			((CButton *)polObj)->EnableWindow(TRUE);
			((CButton *)polObj)->SetCheck(1);
		}
    }	
    for(pos = omMonitorButtons2.GetHeadPosition(); pos != NULL; )
    {
	    polObj = omMonitorButtons2.GetNext(pos);
		if(polObj != NULL)
		{
			((CButton *)polObj)->EnableWindow(TRUE);
			((CButton *)polObj)->SetCheck(0);
		}
    }	
    for(pos = omMonitorButtons3.GetHeadPosition(); pos != NULL; )
    {
	    polObj = omMonitorButtons3.GetNext(pos);
		if(polObj != NULL)
		{
			((CButton *)polObj)->EnableWindow(FALSE);
			((CButton *)polObj)->SetCheck(0);
		}
    }	
}

void SetupDlg::OnRadio3() 
{
	CObject *polObj;
	POSITION pos;
    for(pos = omMonitorButtons1.GetHeadPosition(); pos != NULL; )
    {
	    polObj = omMonitorButtons1.GetNext(pos);
		if(polObj != NULL)
		{
			((CButton *)polObj)->EnableWindow(TRUE);
		}
    }	
    for(pos = omMonitorButtons2.GetHeadPosition(); pos != NULL; )
    {
	    polObj = omMonitorButtons2.GetNext(pos);
		if(polObj != NULL)
		{
			((CButton *)polObj)->EnableWindow(TRUE);
		}
    }	
    for(pos = omMonitorButtons3.GetHeadPosition(); pos != NULL; )
    {
	    polObj = omMonitorButtons3.GetNext(pos);
		if(polObj != NULL)
		{
			((CButton *)polObj)->EnableWindow(TRUE);
		}
    }	
}


void SetupDlg::OnOK() 
{

	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));


	if(UpdateData() == TRUE)
	{
		char plcS1[20]="";
		char plcS2[20]="";
		char plcS3[20]="";
		char plcS4[20]="";
		char plcS5[20]="";
		char plcS6[20]="";
		char plcS7[20]="";
		char plcS8[20]="";

		sprintf(plcS1, "%d", m_Monitors+1);
		sprintf(plcS2, "%d", m_Season1+1);
		sprintf(plcS3, "%d", m_Daily1+1);
		sprintf(plcS4, "%d", m_SeasonRot1+1);
		sprintf(plcS5, "%d", m_Batch1+1);
		sprintf(plcS6, "%d", m_DailyRot1+1);
		sprintf(plcS7, "%d", m_FlightDia1+1);
		sprintf(plcS8, "%d", m_CcaDia1+1);

		sprintf(ogCfgData.rmMonitorSetup.Text, "%s=%s#%s=%s#%s=%s#%s=%s#%s=%s#%s=%s#%s=%s#%s=%s",
								MON_COUNT_STRING, plcS1,			
								MON_SEASONSCHEDULE_STRING, plcS2,	
								MON_DAILYSCHEDULE_STRING, plcS3,	
								MON_SEASONROTDLG_STRING, plcS4,	    
								MON_SEASONBATCH_STRING, plcS5,	    
								MON_DAILYROTDLG_STRING, plcS6,	    
								MON_FLIGHTDIA_STRING, plcS7,	    
								MON_CCADIA_STRING, plcS8);	    



		if(ogCfgData.rmMonitorSetup.IsChanged != DATA_NEW)
		{
			ogCfgData.rmMonitorSetup.IsChanged = DATA_CHANGED;
		}
		ogCfgData.SaveCfg(&ogCfgData.rmMonitorSetup);


		// FONT 

		if(ogCfgData.rmFontSetup.IsChanged != DATA_NEW)
		{
			ogCfgData.rmFontSetup.IsChanged = DATA_CHANGED;
		}

		ogCfgData.SaveCfg(&ogCfgData.rmFontSetup);


		if(!CString(ogCfgData.rmFontSetup.Text).IsEmpty())
		{
		
			CStringArray olValues;
			
			if(ExtractItemList(CString(ogCfgData.rmFontSetup.Text), &olValues, '#') == 14)
			{
				ogSetupFont.DeleteObject();
				ogSetupFont.CreateFont( atoi(olValues[0]), atoi(olValues[1]), atoi(olValues[2]), atoi(olValues[3]), atoi(olValues[4]), atoi(olValues[5]), atoi(olValues[6]), atoi(olValues[7]), atoi(olValues[8]), atoi(olValues[9]), atoi(olValues[10]), atoi(olValues[11]), atoi(olValues[12]), olValues[13]);
				ogDdx.DataChanged((void *)this, TABLE_FONT_CHANGED, NULL);
			}
		}



		///////////////////////


		CString olConfStr;
		CString olTmp;


		if(m_CC_Conf1.GetCheck())
			olConfStr = "1;";
		else
			olConfStr = "0;";

		olConfStr += "-;";

		if(m_CC_Conf2.GetCheck())
			olConfStr += "1;";
		else
			olConfStr += "0;";

		m_CE_Conf2_Min.GetWindowText(olTmp);
		olConfStr += olTmp + CString(";");



		if(m_CC_Conf3.GetCheck())
			olConfStr += "1;";
		else
			olConfStr += "0;";

		m_CE_Conf3_Min.GetWindowText(olTmp);
		olConfStr += olTmp + CString(";");



		if(m_CC_Conf4.GetCheck())
			olConfStr += "1;";
		else
			olConfStr += "0;";

		m_CE_Conf4_Min.GetWindowText(olTmp);
		olConfStr += olTmp + CString(";");


		if(m_CC_Conf5.GetCheck())
			olConfStr += "1;";
		else
			olConfStr += "0;";

		olConfStr += "-;";

		if(m_CC_Conf6.GetCheck())
			olConfStr += "1;";
		else
			olConfStr += "0;";

		olConfStr += "-;";


		if(m_CC_Conf7.GetCheck())
			olConfStr += "1;";
		else
			olConfStr += "0;";

		m_CE_Conf7_Min.GetWindowText(olTmp);
		olConfStr += olTmp + CString(";");


		if(m_CC_Conf8.GetCheck())
			olConfStr += "1;";
		else
			olConfStr += "0;";

		olConfStr += "-;";


		if(m_CC_Conf9.GetCheck())
			olConfStr += "1;";
		else
			olConfStr += "0;";

		m_CE_Conf9_Min.GetWindowText(olTmp);
		olConfStr += olTmp + CString(";");

			
		if(m_CC_Conf10.GetCheck())
			olConfStr += "1;";
		else
			olConfStr += "0;";

		m_CE_Conf10_Min.GetWindowText(olTmp);
		olConfStr += olTmp + CString(";");


		if(m_CC_Conf11.GetCheck())
			olConfStr += "1;";
		else
			olConfStr += "0;";

		m_CE_Conf11_Min.GetWindowText(olTmp);
		olConfStr += olTmp + CString(";");
		
			
		if(m_CC_Conf12.GetCheck())
			olConfStr += "1;";
		else
			olConfStr += "0;";

		m_CE_Conf12_Min.GetWindowText(olTmp);
		olConfStr += olTmp + CString(";");

		
		if(m_CC_Conf13.GetCheck())
			olConfStr += "1;";
		else
			olConfStr += "0;";

		m_CE_Conf13_Min.GetWindowText(olTmp);
		olConfStr += olTmp + CString(";");


		if(m_CC_Conf14.GetCheck())
			olConfStr += "1;";
		else
			olConfStr += "0;";

		olConfStr += "-;";

		if(m_CC_Conf15.GetCheck())
			olConfStr += "1;";
		else
			olConfStr += "0;";

		olConfStr += "-;";

		if(m_CC_Conf16.GetCheck())
			olConfStr += "1;";
		else
			olConfStr += "0;";

		olConfStr += "-;";


		if(m_CC_Conf17.GetCheck())
			olConfStr += "1;";
		else
			olConfStr += "0;";

		olConfStr += "-;";


		if(m_CC_Conf18.GetCheck())
			olConfStr += "1;";
		else
			olConfStr += "0;";

		m_CE_Conf18_Min.GetWindowText(olTmp);
		olConfStr += olTmp + CString(";");

		if(m_CC_Conf19.GetCheck())
			olConfStr += "1;";
		else
			olConfStr += "0;";

		olConfStr += "-;";

		if(m_CC_Conf20.GetCheck())
			olConfStr += "1;";
		else
			olConfStr += "0;";

		olConfStr += "-;";
/*
		//this is a roundabout for the postflightdays.
		//we use the conflictstring to store the timespan.
		//the better way is to create a own record for settings. 
		olConfStr += "0;";

		int ilDays = 0;
		CString olPostFlightPre = "0";
		m_CE_Archive.GetWindowText(olTmp);
		if (olTmp.GetLength() > 0)
		{
			ilDays = abs(atoi(olTmp));
			char buffer[128];
			itoa(ilDays, buffer, 10);
			m_CE_Archive.SetInitText( CString(buffer) );
			m_CE_Archive.GetWindowText(olPostFlightPre);

			olConfStr += CString(buffer) + CString(";");
		}
		else
			olConfStr += "0;";

		//fill the global member
		ogTimeSpanPostFlight = CTimeSpan(ilDays,0,0,0);

		int ilDaysFuture = 0;
		CString olPostFlightPost = "0";
		m_CE_Archive_Future.GetWindowText(olTmp);
		if (olTmp.GetLength() > 0)
		{
			ilDaysFuture = abs(atoi(olTmp));
			char buffer[128];
			itoa(ilDaysFuture, buffer, 10);
			m_CE_Archive_Future.SetInitText( CString(buffer) );
			m_CE_Archive.GetWindowText(olPostFlightPost);

			olConfStr += CString(buffer) + CString(";");
		}
		else
			olConfStr += "0;";

		//fill the global member
		ogTimeSpanPostFlightInFuture = CTimeSpan(ilDaysFuture,0,0,0);
*/

		strcpy(ogCfgData.rmConflictSetup.Text , olConfStr);

		if(ogCfgData.rmConflictSetup.IsChanged != DATA_NEW)
		{
			ogCfgData.rmConflictSetup.IsChanged = DATA_CHANGED;
		}
		ogCfgData.SaveCfg(&ogCfgData.rmConflictSetup);

		ogKonflikte.SetConfiguration();



	//buffertime for gates
		CString olGateBufferStr;
		int ilMinutes = 0;
		m_CE_GateBuffer.GetWindowText(olTmp);
		if (olTmp.GetLength() > 0)
		{
			ilMinutes = abs(atoi(olTmp));
			char buffer[128];
			itoa(ilMinutes, buffer, 10);
			m_CE_GateBuffer.SetInitText( CString(buffer) );

			olGateBufferStr = CString(buffer);
		}
		else
			olGateBufferStr = "0";

		//fill the global member
		ogGateAllocBufferTime = CTimeSpan(0,0,ilMinutes,0);
		strcpy(ogCfgData.rmGateBufferTimeSetup.Text , olGateBufferStr);

		if(ogCfgData.rmGateBufferTimeSetup.IsChanged != DATA_NEW)
		{
			ogCfgData.rmGateBufferTimeSetup.IsChanged = DATA_CHANGED;
		}
		ogCfgData.SaveCfg(&ogCfgData.rmGateBufferTimeSetup);

	//buffertime for positions
		CString olPosBufferStr;
		ilMinutes = 0;
		m_CE_PosBuffer.GetWindowText(olTmp);
		if (olTmp.GetLength() > 0)
		{
			ilMinutes = abs(atoi(olTmp));
			char buffer[128];
			itoa(ilMinutes, buffer, 10);
			m_CE_PosBuffer.SetInitText( CString(buffer) );

			olPosBufferStr = CString(buffer);
		}
		else
			olPosBufferStr = "0";

		//fill the global member
		ogPosAllocBufferTime = CTimeSpan(0,0,ilMinutes,0);
		strcpy(ogCfgData.rmPosBufferTimeSetup.Text , olPosBufferStr);

		if(ogCfgData.rmPosBufferTimeSetup.IsChanged != DATA_NEW)
		{
			ogCfgData.rmPosBufferTimeSetup.IsChanged = DATA_CHANGED;
		}
		ogCfgData.SaveCfg(&ogCfgData.rmPosBufferTimeSetup);


	//buffertime for archive-past
		CString olArchPastBufferStr;
		int ilDays = 0;
		m_CE_Archive.GetWindowText(olTmp);
		if (olTmp.GetLength() > 0)
		{
			ilDays = abs(atoi(olTmp));
			char buffer[128];
			itoa(ilDays, buffer, 10);
			m_CE_Archive.SetInitText( CString(buffer) );

			olArchPastBufferStr = CString(buffer);
		}
		else
			olArchPastBufferStr = "0";

		//fill the global member
		ogTimeSpanPostFlight = CTimeSpan(ilDays,0,0,0);
		strcpy(ogCfgData.rmArchivePastBufferTimeSetup.Text , olArchPastBufferStr);

		if(ogCfgData.rmArchivePastBufferTimeSetup.IsChanged != DATA_NEW)
		{
			ogCfgData.rmArchivePastBufferTimeSetup.IsChanged = DATA_CHANGED;
		}
		ogCfgData.SaveCfg(&ogCfgData.rmArchivePastBufferTimeSetup);

	//buffertime for archive-future
		CString olArchFutBufferStr;
		ilDays = 0;
		m_CE_Archive_Future.GetWindowText(olTmp);
		if (olTmp.GetLength() > 0)
		{
			ilDays = abs(atoi(olTmp));
			char buffer[128];
			itoa(ilDays, buffer, 10);
			m_CE_Archive_Future.SetInitText( CString(buffer) );

			olArchFutBufferStr = CString(buffer);
		}
		else
			olArchFutBufferStr = "0";

		//fill the global member
		ogTimeSpanPostFlightInFuture = CTimeSpan(ilDays,0,0,0);
		strcpy(ogCfgData.rmArchiveFutureBufferTimeSetup.Text , olArchFutBufferStr);

		if(ogCfgData.rmArchiveFutureBufferTimeSetup.IsChanged != DATA_NEW)
		{
			ogCfgData.rmArchiveFutureBufferTimeSetup.IsChanged = DATA_CHANGED;
		}
		ogCfgData.SaveCfg(&ogCfgData.rmArchiveFutureBufferTimeSetup);


	//xdays
		CString olxdays;
		ilDays = 0;
		m_CE_XDays.GetWindowText(olTmp);
		if (olTmp.GetLength() > 0)
		{
			ilDays = abs(atoi(olTmp));
			char buffer[128];
			itoa(ilDays, buffer, 10);
			m_CE_XDays.SetInitText( CString(buffer) );

			olxdays = CString(buffer);
		}
		else
			olxdays = "0";

		//fill the global member
		ogXDays = CTimeSpan(ilDays,0,0,0);
		strcpy(ogCfgData.rmXDaysSetup.Text , olxdays);

		if(ogCfgData.rmXDaysSetup.IsChanged != DATA_NEW)
		{
			ogCfgData.rmXDaysSetup.IsChanged = DATA_CHANGED;
		}
		ogCfgData.SaveCfg(&ogCfgData.rmXDaysSetup);

	//PosGrpWoConfl
		strcpy(ogCfgData.rmPosWoConflicts.Text , ogPosWoConflicts);

		if(ogCfgData.rmPosWoConflicts.IsChanged != DATA_NEW)
		{
			ogCfgData.rmPosWoConflicts.IsChanged = DATA_CHANGED;
		}
		ogCfgData.SaveCfg(&ogCfgData.rmPosWoConflicts);

		// get list of resources in group
		CPtrArray olUrnoList;
		ogGroupResList.RemoveAll();
		if (ogResGroupData.GetGroupResList("PST", ogPosWoConflicts, olUrnoList))
		{
			// fills the resource group list box
			CString olResName;
			CString olUrno;
			for (int i = 0; i < olUrnoList.GetSize(); i++)
			{
				olUrno.Format("%ld", (long) olUrnoList[i]);
				if (ogBCD.GetField("PST", "URNO", olUrno, "PNAM", olResName) == true)
				{
					ogGroupResList.SetAt(olResName, "1");
				}
			}
		}



	//buffertime for timeline
		CString olTimeBufferStr;
		ilMinutes = 0;
		m_CE_TimelineBuffer.GetWindowText(olTmp);
		if (olTmp.GetLength() > 0)
		{
			ilMinutes = abs(atoi(olTmp));
			char buffer[128];
			itoa(ilMinutes, buffer, 10);
			m_CE_TimelineBuffer.SetInitText( CString(buffer) );

			olTimeBufferStr = CString(buffer);
		}
		else
			olTimeBufferStr = "0";

	//fill the global member
		ogTimelineBuffer = CTimeSpan(0,0,ilMinutes,0);
		strcpy(ogCfgData.rmTimelineBuffer.Text , olTimeBufferStr);

		if(ogCfgData.rmTimelineBuffer.IsChanged != DATA_NEW)
		{
			ogCfgData.rmTimelineBuffer.IsChanged = DATA_CHANGED;
		}
		ogCfgData.SaveCfg(&ogCfgData.rmTimelineBuffer);

	//buffertime for wingoverlap
		CString olWingoverBufferStr;
		ilMinutes = 0;
		m_CE_WingBuffer.GetWindowText(olTmp);
		if (olTmp.GetLength() > 0)
		{
			ilMinutes = abs(atoi(olTmp));
			char buffer[128];
			itoa(ilMinutes, buffer, 10);
			m_CE_WingBuffer.SetInitText( CString(buffer) );

			olWingoverBufferStr = CString(buffer);
		}
		else
			olWingoverBufferStr = "0";

	//fill the global member
		ogWingoverBuffer = CTimeSpan(0,0,ilMinutes,0);
		strcpy(ogCfgData.rmWingoverBuffer.Text , olWingoverBufferStr);

		if(ogCfgData.rmWingoverBuffer.IsChanged != DATA_NEW)
		{
			ogCfgData.rmWingoverBuffer.IsChanged = DATA_CHANGED;
		}
		ogCfgData.SaveCfg(&ogCfgData.rmWingoverBuffer);

	//display aircraft changed
		CString olAChange("");
		if(m_Conf_AChange.GetCheck())
		{
			olAChange = "1";
			ogDisplayAircraftChanged = true;
		}
		else
		{
			olAChange = "0";
			ogDisplayAircraftChanged = false;
		}

		strcpy(ogCfgData.rmDisplayAircraftChanged.Text , olAChange);

		if(ogCfgData.rmDisplayAircraftChanged.IsChanged != DATA_NEW)
		{
			ogCfgData.rmDisplayAircraftChanged.IsChanged = DATA_CHANGED;
		}
		ogCfgData.SaveCfg(&ogCfgData.rmDisplayAircraftChanged);


		//------

		pomConflictDlg->SaveConflictSetup();
		ogKonflikte.SetConfiguration();


		ogKonflikte.CheckAttentionButton();
		pogKonflikteDlg->ChangeViewTo();

		if (pogButtonList)
			pogButtonList->UpdateFlightDia();
		if (pogPosDiagram)
			pogPosDiagram->RedisplayAll();
		if (pogGatDiagram)
			pogGatDiagram->RedisplayAll();
		if (pogBltDiagram)
			pogBltDiagram->RedisplayAll();
		if (pogWroDiagram)
			pogWroDiagram->RedisplayAll();
		if (pogCcaDiagram)
			pogCcaDiagram->RedisplayAll();


		SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));

		if (!m_superUser)
			CDialog::OnOK();
	}
}

void SetupDlg::SaveToReg()
{
	BOOL ok = WriteDialogToReg((CWnd*) this, m_key);
}


void SetupDlg::OnDestroy() 
{
	SaveToReg();
	CDialog::OnDestroy();
}



BOOL SetupDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	m_key = "DialogPosition\\Setup";

	SetSecState();	

	CWnd *polWnd;
	polWnd = GetDlgItem(IDC_CONFLICTS);

	if (!pomConflictDlg)
	{
		pomConflictDlg = new GatPosConflictSetupDlg();
		polWnd = GetDlgItem(IDC_CONFLICTS);
		pomConflictDlg->Create(pomConflictDlg->IDD,polWnd);
	}
		pomConflictDlg->ShowWindow(SW_NORMAL);

		SetSecState();	


		if (polWnd != NULL)
		{
			CRect olRect;
			polWnd->GetClientRect(&olRect);
//			olRect.InflateRect(-10,-15);
			pomConflictDlg->MoveWindow(&olRect);
		}

		polWnd = GetDlgItem(IDC_TEXT_CONF1);
		if (polWnd)
			polWnd->ShowWindow(SW_HIDE);
		polWnd = GetDlgItem(IDC_TEXT_CONF2);
		if (polWnd)
			polWnd->ShowWindow(SW_HIDE);
		polWnd = GetDlgItem(IDC_TEXT_CONF3);
		if (polWnd)
			polWnd->ShowWindow(SW_HIDE);
		polWnd = GetDlgItem(IDC_TEXT_CONF4);
		if (polWnd)
			polWnd->ShowWindow(SW_HIDE);
		polWnd = GetDlgItem(IDC_TEXT_CONF5);
		if (polWnd)
			polWnd->ShowWindow(SW_HIDE);
		polWnd = GetDlgItem(IDC_TEXT_CONF6);
		if (polWnd)
			polWnd->ShowWindow(SW_HIDE);
		polWnd = GetDlgItem(IDC_TEXT_CONF7);
		if (polWnd)
			polWnd->ShowWindow(SW_HIDE);
		polWnd = GetDlgItem(IDC_TEXT_CONF9);
		if (polWnd)
			polWnd->ShowWindow(SW_HIDE);
		polWnd = GetDlgItem(IDC_TEXT_CONF10);
		if (polWnd)
			polWnd->ShowWindow(SW_HIDE);
		polWnd = GetDlgItem(IDC_TEXT_CONF11);
		if (polWnd)
			polWnd->ShowWindow(SW_HIDE);
		polWnd = GetDlgItem(IDC_TEXT_CONF12);
		if (polWnd)
			polWnd->ShowWindow(SW_HIDE);
		polWnd = GetDlgItem(IDC_TEXT_CONF13);
		if (polWnd)
			polWnd->ShowWindow(SW_HIDE);
		polWnd = GetDlgItem(IDC_TEXT_CONF18);
		if (polWnd)
			polWnd->ShowWindow(SW_HIDE);
		polWnd = GetDlgItem(IDC_TEXT_CONF19);
		if (polWnd)
			polWnd->ShowWindow(SW_HIDE);
		polWnd = GetDlgItem(IDC_TEXT_CONF20);
		if (polWnd)
			polWnd->ShowWindow(SW_HIDE);
		polWnd = GetDlgItem(IDC_TEXT_SHOW_CONFLICT);
		if (polWnd)
			polWnd->ShowWindow(SW_HIDE);
		polWnd = GetDlgItem(IDC_TEXT_MIN);
		if (polWnd)
			polWnd->ShowWindow(SW_HIDE);


		polWnd = GetDlgItem(IDC_CONF1);
		if (polWnd)
			polWnd->ShowWindow(SW_HIDE);
		polWnd = GetDlgItem(IDC_CONF2);
		if (polWnd)
			polWnd->ShowWindow(SW_HIDE);
		polWnd = GetDlgItem(IDC_CONF3);
		if (polWnd)
			polWnd->ShowWindow(SW_HIDE);
		polWnd = GetDlgItem(IDC_CONF4);
		if (polWnd)
			polWnd->ShowWindow(SW_HIDE);
		polWnd = GetDlgItem(IDC_CONF5);
		if (polWnd)
			polWnd->ShowWindow(SW_HIDE);
		polWnd = GetDlgItem(IDC_CONF6);
		if (polWnd)
			polWnd->ShowWindow(SW_HIDE);
		polWnd = GetDlgItem(IDC_CONF7);
		if (polWnd)
			polWnd->ShowWindow(SW_HIDE);
		polWnd = GetDlgItem(IDC_CONF9);
		if (polWnd)
			polWnd->ShowWindow(SW_HIDE);
		polWnd = GetDlgItem(IDC_CONF10);
		if (polWnd)
			polWnd->ShowWindow(SW_HIDE);
		polWnd = GetDlgItem(IDC_CONF11);
		if (polWnd)
			polWnd->ShowWindow(SW_HIDE);
		polWnd = GetDlgItem(IDC_CONF12);
		if (polWnd)
			polWnd->ShowWindow(SW_HIDE);
		polWnd = GetDlgItem(IDC_CONF13);
		if (polWnd)
			polWnd->ShowWindow(SW_HIDE);
		polWnd = GetDlgItem(IDC_CONF18);
		if (polWnd)
			polWnd->ShowWindow(SW_HIDE);
		polWnd = GetDlgItem(IDC_CONF19);
		if (polWnd)
			polWnd->ShowWindow(SW_HIDE);
		polWnd = GetDlgItem(IDC_CONF20);
		if (polWnd)
			polWnd->ShowWindow(SW_HIDE);

		polWnd = GetDlgItem(IDC_CONF2_MIN);
		if (polWnd)
			polWnd->ShowWindow(SW_HIDE);
		polWnd = GetDlgItem(IDC_CONF3_MIN);
		if (polWnd)
			polWnd->ShowWindow(SW_HIDE);
		polWnd = GetDlgItem(IDC_CONF4_MIN);
		if (polWnd)
			polWnd->ShowWindow(SW_HIDE);
		polWnd = GetDlgItem(IDC_CONF7_MIN);
		if (polWnd)
			polWnd->ShowWindow(SW_HIDE);
		polWnd = GetDlgItem(IDC_CONF9_MIN);
		if (polWnd)
			polWnd->ShowWindow(SW_HIDE);
		polWnd = GetDlgItem(IDC_CONF10_MIN);
		if (polWnd)
			polWnd->ShowWindow(SW_HIDE);
		polWnd = GetDlgItem(IDC_CONF11_MIN);
		if (polWnd)
			polWnd->ShowWindow(SW_HIDE);
		polWnd = GetDlgItem(IDC_CONF12_MIN);
		if (polWnd)
			polWnd->ShowWindow(SW_HIDE);
		polWnd = GetDlgItem(IDC_CONF13_MIN);
		if (polWnd)
			polWnd->ShowWindow(SW_HIDE);
		polWnd = GetDlgItem(IDC_CONF18_MIN);
		if (polWnd)
			polWnd->ShowWindow(SW_HIDE);


//Mon 1
	polWnd = GetDlgItem(IDC_SEASON1);
	if(polWnd != NULL)
	{
		omMonitorButtons1.AddTail(polWnd);
	}
	polWnd = GetDlgItem(IDC_SEASONROT1);
	if(polWnd != NULL)
	{
		omMonitorButtons1.AddTail(polWnd);
	}
	polWnd = GetDlgItem(IDC_BATCH1);
	if(polWnd != NULL)
	{
		omMonitorButtons1.AddTail(polWnd);
	}
	polWnd = GetDlgItem(IDC_DAILY1);
	if(polWnd != NULL)
	{
		omMonitorButtons1.AddTail(polWnd);
	}
	polWnd = GetDlgItem(IDC_DAILYROT1);
	if(polWnd != NULL)
	{
		omMonitorButtons1.AddTail(polWnd);
	}
	polWnd = GetDlgItem(IDC_FLIGHTDIA1);
	if(polWnd != NULL)
	{
		omMonitorButtons1.AddTail(polWnd);
	}
	polWnd = GetDlgItem(IDC_CCADIA1);
	if(polWnd != NULL)
	{
		omMonitorButtons1.AddTail(polWnd);
	}
// Mon2 
	polWnd = GetDlgItem(IDC_SEASON2);
	if(polWnd != NULL)
	{
		omMonitorButtons2.AddTail(polWnd);
	}
	polWnd = GetDlgItem(IDC_SEASONROT2);
	if(polWnd != NULL)
	{
		omMonitorButtons2.AddTail(polWnd);
	}
	polWnd = GetDlgItem(IDC_BATCH2);
	if(polWnd != NULL)
	{
		omMonitorButtons2.AddTail(polWnd);
	}
	polWnd = GetDlgItem(IDC_DAILY2);
	if(polWnd != NULL)
	{
		omMonitorButtons2.AddTail(polWnd);
	}
	polWnd = GetDlgItem(IDC_DAILYROT2);
	if(polWnd != NULL)
	{
		omMonitorButtons2.AddTail(polWnd);
	}
	polWnd = GetDlgItem(IDC_FLIGHTDIA2);
	if(polWnd != NULL)
	{
		omMonitorButtons2.AddTail(polWnd);
	}
	polWnd = GetDlgItem(IDC_CCADIA2);
	if(polWnd != NULL)
	{
		omMonitorButtons2.AddTail(polWnd);
	}
//Mon 3
	polWnd = GetDlgItem(IDC_SEASON3);
	if(polWnd != NULL)
	{
		omMonitorButtons3.AddTail(polWnd);
	}
	polWnd = GetDlgItem(IDC_SEASONROT3);
	if(polWnd != NULL)
	{
		omMonitorButtons3.AddTail(polWnd);
	}
	polWnd = GetDlgItem(IDC_BATCH3);
	if(polWnd != NULL)
	{
		omMonitorButtons3.AddTail(polWnd);
	}
	polWnd = GetDlgItem(IDC_DAILY3);
	if(polWnd != NULL)
	{
		omMonitorButtons3.AddTail(polWnd);
	}
	polWnd = GetDlgItem(IDC_DAILYROT3);
	if(polWnd != NULL)
	{
		omMonitorButtons3.AddTail(polWnd);
	}
	polWnd = GetDlgItem(IDC_FLIGHTDIA3);
	if(polWnd != NULL)
	{
		omMonitorButtons3.AddTail(polWnd);
	}
	polWnd = GetDlgItem(IDC_CCADIA3);
	if(polWnd != NULL)
	{
		omMonitorButtons3.AddTail(polWnd);
	}


	
	m_Monitors = ogCfgData.GetMonitorForWindow(MON_COUNT_STRING) - 1;
	m_Season1 = ogCfgData.GetMonitorForWindow(MON_SEASONSCHEDULE_STRING) -1;
	m_Daily1 = ogCfgData.GetMonitorForWindow(MON_DAILYSCHEDULE_STRING)-1;
	m_SeasonRot1 = ogCfgData.GetMonitorForWindow(MON_SEASONROTDLG_STRING)-1;
	m_Batch1 = ogCfgData.GetMonitorForWindow(MON_SEASONBATCH_STRING)-1;
	m_DailyRot1 = ogCfgData.GetMonitorForWindow(MON_DAILYROTDLG_STRING)-1;
	m_FlightDia1 = ogCfgData.GetMonitorForWindow(MON_FLIGHTDIA_STRING)-1;
	m_CcaDia1 = ogCfgData.GetMonitorForWindow(MON_CCADIA_STRING)-1;


	if(m_Monitors == 0)
	{
		OnRadio1();
	}
	else if(m_Monitors == 1)
	{
		OnRadio2();
	}

	if (!m_superUser)
	{
		m_UserCB.AddString(ogBasicData.omUserID);
		m_UserCB.SetCurSel(0);
	}
	else
	{
		if (!m_initUserCB)
		{
			if (!InitUserCB())
			{
				m_UserCB.AddString(ogBasicData.omUserID);
				m_UserCB.SetCurSel(0);
				m_superUser = false;
			}
			m_initUserCB = true;
		}
	}
	m_UserCB.SetCurSel(m_UserCB.FindString(0, ogBasicData.omUserID));

	UpdateData(FALSE);

	m_CS_Font.SetFont(&ogSetupFont);
	m_CS_Font.SetWindowText("LH 4711");

	CRect olRect;
	GetWindowRect(olRect);

	int left = (1024 - (olRect.right - olRect.left)) /2;
	int top =  (768  - (olRect.bottom - olRect.top)) /2;

//	SetWindowPos(&wndTop,left, top, 0, 0, SWP_HIDEWINDOW | SWP_NOSIZE);


	//Conflicts



	m_CE_Conf2_Min.SetWindowText("");
	m_CE_Conf3_Min.SetWindowText("");
	m_CE_Conf4_Min.SetWindowText("");
	m_CE_Conf7_Min.SetWindowText("");
	m_CE_Conf9_Min.SetWindowText("");
	m_CE_Conf10_Min.SetWindowText("");
	m_CE_Conf11_Min.SetWindowText("");
	m_CE_Conf13_Min.SetWindowText("");

	m_CC_Conf9.SetCheck(1);
	m_CC_Conf8.SetCheck(1);
	m_CC_Conf7.SetCheck(1);
	m_CC_Conf6.SetCheck(1);
	m_CC_Conf5.SetCheck(1);
	m_CC_Conf4.SetCheck(1);
	m_CC_Conf3.SetCheck(1);
	m_CC_Conf2.SetCheck(1);
	m_CC_Conf13.SetCheck(1);
	m_CC_Conf12.SetCheck(1);
	m_CC_Conf11.SetCheck(1);
	m_CC_Conf10.SetCheck(1);
	m_CC_Conf1.SetCheck(1);
	m_CC_Conf18.SetCheck(1);
	m_CC_Conf19.SetCheck(1);
	m_CC_Conf20.SetCheck(1);

	m_CE_Archive.SetTypeToString("###",3,0);
	m_CE_Archive_Future.SetTypeToString("###",3,0);
	m_CE_GateBuffer.SetTypeToString("###",3,0);
	m_CE_PosBuffer.SetTypeToString("###",3,0);
	m_CE_XDays.SetTypeToString("###",3,0);

	m_CE_TimelineBuffer.SetTypeToString("###",3,0);
	m_CE_WingBuffer.SetTypeToString("###",3,0);

	m_CE_TimelineBuffer.SetInitText("0");
	m_CE_WingBuffer.SetInitText("0");

	m_CE_XDays.SetInitText("0");

	m_CE_Archive.SetInitText("0");
	m_CE_Archive_Future.SetInitText("0");

	m_CE_GateBuffer.SetInitText("0");
	m_CE_PosBuffer.SetInitText("0");

	UpdateWindow();

	CString olTmp = ogCfgData.GetConflictSetup();

	CStringArray olConf;


	if( !olTmp.IsEmpty() )
	{
		ExtractItemList(olTmp, &olConf, ';');

		if( olConf.GetSize() >= 35)
		{

			m_CC_Conf1.SetCheck( atoi(olConf[0]) );

			m_CC_Conf2.SetCheck( atoi(olConf[2]) );
			m_CE_Conf2_Min.SetWindowText(olConf[3]);

			m_CC_Conf3.SetCheck( atoi(olConf[4]) );
			m_CE_Conf3_Min.SetWindowText( olConf[5] );
			
			m_CC_Conf4.SetCheck( atoi(olConf[6]) );
			m_CE_Conf4_Min.SetWindowText( olConf[7] );

			m_CC_Conf5.SetCheck( atoi(olConf[8]) );

			m_CC_Conf6.SetCheck( atoi(olConf[10]) );
			
			m_CC_Conf7.SetCheck( atoi(olConf[12]) );
			m_CE_Conf7_Min.SetWindowText( olConf[13] );

			m_CC_Conf8.SetCheck( atoi(olConf[14]) );
			
			m_CC_Conf9.SetCheck( atoi(olConf[16]) );
			m_CE_Conf9_Min.SetWindowText( olConf[17] );
			
			m_CC_Conf10.SetCheck( atoi(olConf[18]) );
			m_CE_Conf10_Min.SetWindowText( olConf[19] );
			
			m_CC_Conf11.SetCheck( atoi(olConf[20]) );
			m_CE_Conf11_Min.SetWindowText( olConf[21] );

			m_CC_Conf12.SetCheck( atoi(olConf[22]) );
			m_CE_Conf12_Min.SetWindowText( olConf[23] );
			
			m_CC_Conf13.SetCheck( atoi(olConf[24]) );
			m_CE_Conf13_Min.SetWindowText( olConf[25] );

			m_CC_Conf14.SetCheck( atoi(olConf[26]) );
			m_CC_Conf15.SetCheck( atoi(olConf[28]) );
			m_CC_Conf16.SetCheck( atoi(olConf[30]) );
			m_CC_Conf17.SetCheck( atoi(olConf[32]) );

		}

		if( olConf.GetSize() >= 36)
		{
			m_CC_Conf18.SetCheck( atoi(olConf[34]) );
			m_CE_Conf18_Min.SetWindowText( olConf[35] );
		}
		if( olConf.GetSize() >= 37)
		{
			m_CC_Conf19.SetCheck( atoi(olConf[36]) );
		}
		if( olConf.GetSize() >= 39)
		{
			m_CC_Conf20.SetCheck( atoi(olConf[38]) );
		}

/*
		//this is a roundabout for the postflightdays.
		//we use the conflictstring to store and retrive the timespan.
		//the better way is to work with a own record. 
		//field[40] isn't used!!
		int ilDays = 0;
		if( olConf.GetSize() >= 42)
		{
			m_CE_Archive.SetInitText( olConf[41] );
			ilDays = atoi(olConf[41]);
		}
		ilDays = abs(ilDays);
		ogTimeSpanPostFlight = CTimeSpan(ilDays,0,0,0);

		int ilDaysFuture = 0;
		if( olConf.GetSize() >= 43)
		{
			m_CE_Archive_Future.SetInitText( olConf[42] );
			ilDaysFuture = atoi(olConf[42]);
		}
		ilDaysFuture = abs(ilDaysFuture);
		ogTimeSpanPostFlightInFuture = CTimeSpan(ilDaysFuture,0,0,0);
*/
	}
	else
	{
		char buffer[128];


		itoa(  ogKonflikte.omNoOnblAfterLand.GetTotalMinutes(), buffer, 10);
		m_CE_Conf2_Min.SetWindowText( CString(buffer) );

		itoa( ogKonflikte.omCurrEtai.GetTotalMinutes(), buffer, 10);
		m_CE_Conf3_Min.SetWindowText( CString(buffer) );

		itoa( ogKonflikte.omCurrANxti.GetTotalMinutes(), buffer, 10);
		m_CE_Conf4_Min.SetWindowText( CString(buffer) );
		
		/*
		itoa( buffer, omStoaStod.GetTotalMinutes(), 10);
		m_CE_Conf6_Min.SetWindowText( CString(buffer) );
		*/

		itoa( ogKonflikte.omStodCurrAndNoAirb.GetTotalMinutes(), buffer, 10);
		m_CE_Conf7_Min.SetWindowText( CString(buffer) );
	
		itoa( ogKonflikte.omCurrOfbl.GetTotalMinutes(), buffer, 10);
		m_CE_Conf9_Min.SetWindowText( CString(buffer) );
		
		itoa( ogKonflikte.omCurrOfblStod.GetTotalMinutes(), buffer, 10);
		m_CE_Conf10_Min.SetWindowText( CString(buffer) );
				
		itoa( ogKonflikte.omStodEtdi.GetTotalMinutes(), buffer, 10);
		m_CE_Conf11_Min.SetWindowText( CString(buffer) );
		
		itoa( ogKonflikte.omCurrDNxti.GetTotalMinutes(), buffer, 10);
		m_CE_Conf12_Min.SetWindowText( CString(buffer) );
				
		itoa( ogKonflikte.omCurrStodGd1x.GetTotalMinutes(), buffer, 10);
		m_CE_Conf13_Min.SetWindowText( CString(buffer) );

		itoa( ogKonflikte.omStoaEtai.GetTotalMinutes(), buffer, 10);
		m_CE_Conf18_Min.SetWindowText( CString(buffer) );
/*
//		postflight days
		int ilDays = 0;
		ilDays = ogTimeSpanPostFlight.GetDays();
		char bufferTmp[128];
		itoa(ilDays, bufferTmp, 10);
		m_CE_Archive.SetInitText(CString(bufferTmp));

		int ilDaysFuture = 0;
		ilDaysFuture = ogTimeSpanPostFlightInFuture.GetDays();
		char bufferTmp1[128];
		itoa(ilDaysFuture, bufferTmp1, 10);
		m_CE_Archive_Future.SetInitText(CString(bufferTmp1));

*/
	}

	CStringArray olGroupList;
	if (ogResGroupData.GetGroupList("PST", olGroupList)) 
	{
		// Fill the combobox with the group names
		m_CB_PosGroupNoConfl.AddString("");
		for (int i = 0; i < olGroupList.GetSize(); i++)
		{
			m_CB_PosGroupNoConfl.AddString(olGroupList[i]);
		}
		m_CB_PosGroupNoConfl.SelectString(-1, ogPosWoConflicts);
	}

	//buffertime for gates
	CString olGateBufferStr = ogCfgData.GetGateBufferTimeSetup();
	if( !olGateBufferStr.IsEmpty() )
	{
		m_CE_GateBuffer.SetInitText(CString(olGateBufferStr));
	}
	else
	{
		int ilMinutes = 0;
		ilMinutes = ogGateAllocBufferTime.GetTotalMinutes();
		char bufferTmp[128];
		itoa(ilMinutes, bufferTmp, 10);
		m_CE_GateBuffer.SetInitText(CString(bufferTmp));
	}

	//buffertime for position
	CString olPosBufferStr = ogCfgData.GetPosBufferTimeSetup();
	if( !olPosBufferStr.IsEmpty() )
	{
		m_CE_PosBuffer.SetInitText(CString(olPosBufferStr));
	}
	else
	{
		int ilMinutes = 0;
		ilMinutes = ogPosAllocBufferTime.GetTotalMinutes();
		char bufferTmp[128];
		itoa(ilMinutes, bufferTmp, 10);
		m_CE_PosBuffer.SetInitText(CString(bufferTmp));
	}

	//buffertime for archive-past
	CString olArchPastBufferStr = ogCfgData.GetArchivePastBufferTimeSetup();
	if( !olArchPastBufferStr.IsEmpty() )
	{
		m_CE_Archive.SetInitText(CString(olArchPastBufferStr));
	}
	else
	{
		int ilDays = 0;
		ilDays = ogTimeSpanPostFlight.GetDays();
		char bufferTmp[128];
		itoa(ilDays, bufferTmp, 10);
		m_CE_Archive.SetInitText(CString(bufferTmp));
	}

	//buffertime for archive-future
	CString olArchFutBufferStr = ogCfgData.GetArchiveFutureBufferTimeSetup();
	if( !olArchFutBufferStr.IsEmpty() )
	{
		m_CE_Archive_Future.SetInitText(CString(olArchFutBufferStr));
	}
	else
	{
		int ilDays = 0;
		ilDays = ogTimeSpanPostFlightInFuture.GetDays();
		char bufferTmp[128];
		itoa(ilDays, bufferTmp, 10);
		m_CE_Archive_Future.SetInitText(CString(bufferTmp));
	}

	//xdays
	CString olxdays = ogCfgData.GetXDaysSetup();
	if( !olxdays.IsEmpty() )
	{
		m_CE_XDays.SetInitText(CString(olxdays));
	}
	else
	{
		int ilDays = 0;
		ilDays = ogXDays.GetDays();
		char bufferTmp[128];
		itoa(ilDays, bufferTmp, 10);
		m_CE_XDays.SetInitText(CString(bufferTmp));
	}

	//buffertime for timeline
	olPosBufferStr = ogCfgData.GetTimelineBufferSetup();
	if( !olPosBufferStr.IsEmpty() )
	{
		m_CE_TimelineBuffer.SetInitText(CString(olPosBufferStr));
	}
	else
	{
		int ilMinutes = 0;
		ilMinutes = ogTimelineBuffer.GetTotalMinutes();
		char bufferTmp[128];
		itoa(ilMinutes, bufferTmp, 10);
		m_CE_TimelineBuffer.SetInitText(CString(bufferTmp));
	}

	//buffertime for wingoverlap
	olPosBufferStr = ogCfgData.GetWingoverlapBufferSetup();
	if( !olPosBufferStr.IsEmpty() )
	{
		m_CE_WingBuffer.SetInitText(CString(olPosBufferStr));
	}
	else
	{
		int ilMinutes = 0;
		ilMinutes = ogWingoverBuffer.GetTotalMinutes();
		char bufferTmp[128];
		itoa(ilMinutes, bufferTmp, 10);
		m_CE_WingBuffer.SetInitText(CString(bufferTmp));
	}

	//diaplay aircraftchange
	olPosBufferStr = ogCfgData.GetDisplayAircraftChangedSetup();
	if( !olPosBufferStr.IsEmpty() )
	{
		m_Conf_AChange.SetCheck(atoi(olPosBufferStr));
	}
	else
	{
		m_Conf_AChange.SetCheck(ogDisplayAircraftChanged);
	}



	m_resizeHelper.Init(this->m_hWnd);
	m_resizeHelper.Fix(IDC_PKNO_CB,DlgResizeHelper::kNoHFix,DlgResizeHelper::kHeight);

	this->GetWindowRect(&olRect);
	GetDialogFromReg(olRect, m_key);
	SetWindowPos(&wndTop, olRect.left, olRect.top, olRect.Width(), olRect.Height(), SWP_SHOWWINDOW);// | SWP_NOMOVE );


	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX-Eigenschaftenseiten sollten FALSE zur�ckgeben
}

void SetupDlg::OnSize(UINT nType, int cx, int cy)  
{
	CDialog::OnSize(nType, cx, cy);
	
	// TODO: Add your message handler code here
	m_resizeHelper.OnSize();
	if (this->pomConflictDlg != NULL && ::IsWindow(this->pomConflictDlg->m_hWnd))
	{
		CWnd* polWnd = GetDlgItem(IDC_CONFLICTS);
		if (polWnd != NULL)
		{
			CRect olRect;
			polWnd->GetClientRect(&olRect);
//			olRect.InflateRect(-10,-15);
			pomConflictDlg->MoveWindow(&olRect);
		}
	}
	this->Invalidate();
}

bool SetupDlg::InitUserCB()
{
 	int ilUSID = ogBCD.GetFieldIndex("SEC", "USID");

	for(int i = 0; i < ogBCD.GetDataCount("SEC"); i++)
	{
		RecordSet rlRec;
		ogBCD.GetRecord("SEC", i, rlRec);
		
		CString olUser = rlRec[ilUSID];
		if(!olUser.IsEmpty())
			m_UserCB.AddString(olUser);
	}


/*
	m_UserCB.AddString(ogBasicData.omUserID);
	m_UserCB.AddString("UFIS$ADMIN");
	m_UserCB.AddString("silva");
	m_UserCB.AddString("rogerio");
	m_UserCB.AddString("rosa");
*/
	m_UserCB.SetCurSel(m_UserCB.GetCurSel());



	return true;
}


void SetupDlg::EnableAll()
{
	m_CB_Save.SetSecState('1');			
	m_CE_Archive.SetSecState('1');		
	m_CE_Archive_Future.SetSecState('1');		
	m_CE_GateBuffer.SetSecState('1');		
	m_CE_PosBuffer.SetSecState('1');		
	m_CB_ResGroups.SetSecState('1');			
	m_CE_XDays.SetSecState('1');
	m_CE_TimelineBuffer.SetSecState('1');
	m_CE_WingBuffer.SetSecState('1');
	m_Conf_AChange.ShowWindow(SW_SHOW);

	m_UserCB.ShowWindow(SW_SHOW);
	m_CE_Conf2_Min.ShowWindow(SW_SHOW);
	m_CE_Conf3_Min.ShowWindow(SW_SHOW);
	m_CE_Conf4_Min.ShowWindow(SW_SHOW);
	m_CE_Conf7_Min.ShowWindow(SW_SHOW);
	m_CE_Conf9_Min.ShowWindow(SW_SHOW);
	m_CE_Conf10_Min.ShowWindow(SW_SHOW);
	m_CE_Conf11_Min.ShowWindow(SW_SHOW);
	m_CE_Conf12_Min.ShowWindow(SW_SHOW);
	m_CE_Conf13_Min.ShowWindow(SW_SHOW);
	m_CE_Conf18_Min.ShowWindow(SW_SHOW);

	m_CC_Conf1.ShowWindow(SW_SHOW);
	m_CC_Conf2.ShowWindow(SW_SHOW);
	m_CC_Conf3.ShowWindow(SW_SHOW);
	m_CC_Conf4.ShowWindow(SW_SHOW);
	m_CC_Conf5.ShowWindow(SW_SHOW);
	m_CC_Conf6.ShowWindow(SW_SHOW);
	m_CC_Conf7.ShowWindow(SW_SHOW);
//	m_CC_Conf8.ShowWindow(SW_SHOW);
	m_CC_Conf9.ShowWindow(SW_SHOW);
	m_CC_Conf10.ShowWindow(SW_SHOW);
	m_CC_Conf11.ShowWindow(SW_SHOW);
	m_CC_Conf12.ShowWindow(SW_SHOW);
	m_CC_Conf13.ShowWindow(SW_SHOW);
/*	m_CC_Conf14.ShowWindow(SW_SHOW);
	m_CC_Conf15.ShowWindow(SW_SHOW);
	m_CC_Conf16.ShowWindow(SW_SHOW);
	m_CC_Conf17.ShowWindow(SW_SHOW);*/
	m_CC_Conf18.ShowWindow(SW_SHOW);
	m_CC_Conf19.ShowWindow(SW_SHOW);
	m_CC_Conf20.ShowWindow(SW_SHOW);

	m_UserCB.EnableWindow(TRUE);
	m_CE_Conf2_Min.EnableWindow(TRUE);
	m_CE_Conf3_Min.EnableWindow(TRUE);
	m_CE_Conf4_Min.EnableWindow(TRUE);
	m_CE_Conf7_Min.EnableWindow(TRUE);
	m_CE_Conf9_Min.EnableWindow(TRUE);
	m_CE_Conf10_Min.EnableWindow(TRUE);
	m_CE_Conf11_Min.EnableWindow(TRUE);
	m_CE_Conf12_Min.EnableWindow(TRUE);
	m_CE_Conf13_Min.EnableWindow(TRUE);
	m_CE_Conf18_Min.EnableWindow(TRUE);

	m_CC_Conf1.EnableWindow(TRUE);
	m_CC_Conf2.EnableWindow(TRUE);
	m_CC_Conf3.EnableWindow(TRUE);
	m_CC_Conf4.EnableWindow(TRUE);
	m_CC_Conf5.EnableWindow(TRUE);
	m_CC_Conf6.EnableWindow(TRUE);
	m_CC_Conf7.EnableWindow(TRUE);
//	m_CC_Conf8.EnableWindow(TRUE);
	m_CC_Conf9.EnableWindow(TRUE);
	m_CC_Conf10.EnableWindow(TRUE);
	m_CC_Conf11.EnableWindow(TRUE);
	m_CC_Conf12.EnableWindow(TRUE);
	m_CC_Conf13.EnableWindow(TRUE);
/*	m_CC_Conf14.EnableWindow(TRUE);
	m_CC_Conf15.EnableWindow(TRUE);
	m_CC_Conf16.EnableWindow(TRUE);
	m_CC_Conf17.EnableWindow(TRUE);*/
	m_CC_Conf18.EnableWindow(TRUE);
	m_CC_Conf19.EnableWindow(TRUE);
	m_CC_Conf20.EnableWindow(TRUE);
}

void SetupDlg::SetSecState()
{
	if(ogPrivList.GetStat("SETUP_USER") == '1')
	{
		EnableAll();
		m_superUser = true;
		return;
	}
	else
	{
		m_UserCB.EnableWindow(FALSE);		
		if(ogPrivList.GetStat("SETUP_USER") == '-')
			m_UserCB.ShowWindow(SW_HIDE);
		if(ogPrivList.GetStat("SETUP_USER") == '0')
			m_UserCB.ShowWindow(SW_SHOW);
	}



	m_CB_Save.SetSecState(ogPrivList.GetStat("SETUP_CB_Save"));			


	//m_CE_TimelineBuffer.SetSecState('1');
	//m_CE_WingBuffer.SetSecState('1');
	//m_Conf_AChange.ShowWindow(SW_SHOW);

	m_CE_TimelineBuffer.SetSecState(ogPrivList.GetStat("SETUP_BUFFERTIME_TIMELINE"));		
	if(ogPrivList.GetStat("SETUP_BUFFERTIME_TIMELINE") == '-')
	{
		CWnd *polWnd = GetDlgItem(IDC_STATIC_TIMELINE);
		if (polWnd)
			polWnd->ShowWindow(SW_HIDE);
	}

	m_CE_WingBuffer.SetSecState(ogPrivList.GetStat("SETUP_BUFFERTIME_WINGOVER"));		
	if(ogPrivList.GetStat("SETUP_BUFFERTIME_WINGOVER") == '-')
	{
		CWnd *polWnd = GetDlgItem(IDC_STATIC_WINGOVER);
		if (polWnd)
			polWnd->ShowWindow(SW_HIDE);
	}

	char clStat = ogPrivList.GetStat("SETUP_POSGRP_NO_CONFL");
	if(clStat == '-')
	{
		m_CB_PosGroupNoConfl.ShowWindow(SW_HIDE);
		CWnd *polWnd = GetDlgItem(IDC_STATIC_POSGRP);
		if (polWnd)
			polWnd->ShowWindow(SW_HIDE);
	}
	else if (clStat == '0')
	{
		m_CB_PosGroupNoConfl.EnableWindow(FALSE);
		CWnd *polWnd = GetDlgItem(IDC_STATIC_POSGRP);
		if (polWnd)
			polWnd->EnableWindow(FALSE);
	}


	clStat = ogPrivList.GetStat("SETUP_DISPLAY_ACHANGE");
	if(clStat == '0')
	{
		m_Conf_AChange.EnableWindow(FALSE);
	}
	else if (clStat == '-')
	{
		m_Conf_AChange.ShowWindow(SW_HIDE);
	}


	m_CE_XDays.SetSecState(ogPrivList.GetStat("SETUP_XDAYS"));		
	if(ogPrivList.GetStat("SETUP_XDAYS") == '-')
	{
		CWnd *polWnd = GetDlgItem(IDC_STATIC_XDAYS);
		if (polWnd)
			polWnd->ShowWindow(SW_HIDE);
	}

	m_CE_Archive.SetSecState(ogPrivList.GetStat("SETUP_POSTFLIGHT_DAYS"));		
	if(ogPrivList.GetStat("SETUP_POSTFLIGHT_DAYS") == '-')
	{
		CWnd *polWnd = GetDlgItem(IDC_STATIC_ARCHIVE);
		if (polWnd)
			polWnd->ShowWindow(SW_HIDE);
	}


	m_CE_Archive_Future.SetSecState(ogPrivList.GetStat("SETUP_POSTFLIGHT_DAYS_FUTURE"));		
	if(ogPrivList.GetStat("SETUP_POSTFLIGHT_DAYS_FUTURE") == '-')
	{
		CWnd *polWnd = GetDlgItem(IDC_STATIC_ARCHIVE_FUTURE);
		if (polWnd)
			polWnd->ShowWindow(SW_HIDE);
	}

	m_CE_GateBuffer.SetSecState(ogPrivList.GetStat("SETUP_BUFFERTIME_GATE"));		
	if(ogPrivList.GetStat("SETUP_BUFFERTIME_GATE") == '-')
	{
		CWnd *polWnd = GetDlgItem(IDC_STATIC_GATEBUFFER);
		if (polWnd)
			polWnd->ShowWindow(SW_HIDE);
	}

	m_CE_PosBuffer.SetSecState(ogPrivList.GetStat("SETUP_BUFFERTIME_POS"));		
	if(ogPrivList.GetStat("SETUP_BUFFERTIME_POS") == '-')
	{
		CWnd *polWnd = GetDlgItem(IDC_STATIC_POSBUFFER);
		if (polWnd)
			polWnd->ShowWindow(SW_HIDE);
	}


	m_CB_ResGroups.SetSecState(ogPrivList.GetStat("SETUP_CB_ResGroups"));			


	clStat = ogPrivList.GetStat("SETUP_DAILY_CONFLICTS");
	if(clStat == '0')
	{
		m_CE_Conf2_Min.EnableWindow(FALSE);
		m_CE_Conf3_Min.EnableWindow(FALSE);
		m_CE_Conf4_Min.EnableWindow(FALSE);
		m_CE_Conf7_Min.EnableWindow(FALSE);
		m_CE_Conf9_Min.EnableWindow(FALSE);
		m_CE_Conf10_Min.EnableWindow(FALSE);
		m_CE_Conf11_Min.EnableWindow(FALSE);
		m_CE_Conf12_Min.EnableWindow(FALSE);
		m_CE_Conf13_Min.EnableWindow(FALSE);
		m_CE_Conf18_Min.EnableWindow(FALSE);
	
		m_CC_Conf1.EnableWindow(FALSE);
		m_CC_Conf2.EnableWindow(FALSE);
		m_CC_Conf3.EnableWindow(FALSE);
		m_CC_Conf4.EnableWindow(FALSE);
		m_CC_Conf5.EnableWindow(FALSE);
		m_CC_Conf6.EnableWindow(FALSE);
		m_CC_Conf7.EnableWindow(FALSE);
		m_CC_Conf8.EnableWindow(FALSE);
		m_CC_Conf9.EnableWindow(FALSE);
		m_CC_Conf10.EnableWindow(FALSE);
		m_CC_Conf11.EnableWindow(FALSE);
		m_CC_Conf12.EnableWindow(FALSE);
		m_CC_Conf13.EnableWindow(FALSE);
		m_CC_Conf14.EnableWindow(FALSE);
		m_CC_Conf15.EnableWindow(FALSE);
		m_CC_Conf16.EnableWindow(FALSE);
		m_CC_Conf17.EnableWindow(FALSE);
		m_CC_Conf18.EnableWindow(FALSE);
		m_CC_Conf19.EnableWindow(FALSE);
		m_CC_Conf20.EnableWindow(FALSE);
	}


	if(clStat == '-')
	{
		m_CE_Conf2_Min.ShowWindow(SW_HIDE);
		m_CE_Conf3_Min.ShowWindow(SW_HIDE);
		m_CE_Conf4_Min.ShowWindow(SW_HIDE);
		m_CE_Conf7_Min.ShowWindow(SW_HIDE);
		m_CE_Conf9_Min.ShowWindow(SW_HIDE);
		m_CE_Conf10_Min.ShowWindow(SW_HIDE);
		m_CE_Conf11_Min.ShowWindow(SW_HIDE);
		m_CE_Conf12_Min.ShowWindow(SW_HIDE);
		m_CE_Conf13_Min.ShowWindow(SW_HIDE);
		m_CE_Conf18_Min.ShowWindow(SW_HIDE);
	
		m_CC_Conf1.ShowWindow(SW_HIDE);
		m_CC_Conf2.ShowWindow(SW_HIDE);
		m_CC_Conf3.ShowWindow(SW_HIDE);
		m_CC_Conf4.ShowWindow(SW_HIDE);
		m_CC_Conf5.ShowWindow(SW_HIDE);
		m_CC_Conf6.ShowWindow(SW_HIDE);
		m_CC_Conf7.ShowWindow(SW_HIDE);
		m_CC_Conf8.ShowWindow(SW_HIDE);
		m_CC_Conf9.ShowWindow(SW_HIDE);
		m_CC_Conf10.ShowWindow(SW_HIDE);
		m_CC_Conf11.ShowWindow(SW_HIDE);
		m_CC_Conf12.ShowWindow(SW_HIDE);
		m_CC_Conf13.ShowWindow(SW_HIDE);
		m_CC_Conf14.ShowWindow(SW_HIDE);
		m_CC_Conf15.ShowWindow(SW_HIDE);
		m_CC_Conf16.ShowWindow(SW_HIDE);
		m_CC_Conf17.ShowWindow(SW_HIDE);
		m_CC_Conf18.ShowWindow(SW_HIDE);
		m_CC_Conf19.ShowWindow(SW_HIDE);
		m_CC_Conf20.ShowWindow(SW_HIDE);
	}


}


void SetupDlg::OnFont() 
{
	CString olTmp;
	CFontDialog olDlg;
	LPLOGFONT lpLogFont = new LOGFONT;

	if(olDlg.DoModal() == IDOK)
	{
	  olDlg.GetCurrentFont( lpLogFont );

	//test
		CFont olFont;

		int nHeight					= lpLogFont->lfHeight; 
		int nWidth					= lpLogFont->lfWidth; 
		int nEscapement				= lpLogFont->lfEscapement; 
		int nOrientation			= lpLogFont->lfOrientation; 
		int nWeight					= lpLogFont->lfWeight; 
		int bItalic					= lpLogFont->lfItalic; 
		int bUnderline				= lpLogFont->lfUnderline; 
		int cStrikeOut				= lpLogFont->lfStrikeOut; 
		int nCharSet				= lpLogFont->lfCharSet; 
		int nOutPrecision			= lpLogFont->lfOutPrecision; 
		int nClipPrecision			= lpLogFont->lfClipPrecision; 
		int nQuality				= lpLogFont->lfQuality; 
		int nPitchAndFamily			= lpLogFont->lfPitchAndFamily; 
		CString lpszFacename		= CString(lpLogFont->lfFaceName); 

		sprintf(ogCfgData.rmFontSetup.Text, "%d#%d#%d#%d#%d#%d#%d#%d#%d#%d#%d#%d#%d#%s",
																								nHeight,			
																								nWidth,			
																								nEscapement,		
																								nOrientation,	
																								nWeight,			
																								bItalic,			
																								bUnderline,		
																								cStrikeOut,		
																								nCharSet,		
																								nOutPrecision,	
																								nClipPrecision,	
																								nQuality,		
																								nPitchAndFamily,	
																								lpszFacename);

		olFont. CreateFont( nHeight, nWidth, nEscapement, nOrientation, nWeight, bItalic, bUnderline, cStrikeOut, nCharSet, nOutPrecision, nClipPrecision, nQuality, nPitchAndFamily, lpszFacename );
		m_CS_Font.SetFont(&olFont);
		m_CS_Font.SetWindowText("LH 4711");
	}
}





afx_msg void SetupDlg::OnResGroups()
{
	ResGroupDlg olResGroupDlg(&ogResGroupData, this);

	olResGroupDlg.DoModal();

	m_CB_PosGroupNoConfl.ResetContent();
	CStringArray olGroupList;
	if (ogResGroupData.GetGroupList("PST", olGroupList)) 
	{
		// Fill the combobox with the group names
		m_CB_PosGroupNoConfl.AddString("");
		for (int i = 0; i < olGroupList.GetSize(); i++)
		{
			m_CB_PosGroupNoConfl.AddString(olGroupList[i]);
		}
		m_CB_PosGroupNoConfl.SelectString(-1, ogPosWoConflicts);
	}

}

int SetupDlg::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	if (CDialog::OnCreate(lpCreateStruct) == -1)
		return -1;
	
	// TODO: Add your specialized creation code here
	
	return 0;
}

void SetupDlg::OnDispAC() 
{
	ogDisplayAircraftChanged = false;
	if (m_Conf_AChange.GetCheck())
	{
		ogDisplayAircraftChanged = true;
	}

	if (pomConflictDlg)
		pomConflictDlg->HandleAircraftChange();
}




