#ifndef REPORTSEQFIELD3DATETIMEDLG_H__INCLUDED_
#define REPORTSEQFIELD3DATETIMEDLG_H__INCLUDED_

// ReportSeqDateTimeDlg.h : Header-Datei
//
#include "CCSEdit.h"
#include "resrc1.h"




enum BasicDataType
{REPSEQFIELD3_NONE=0, REPSEQFIELD3_AIRLINECODE, REPSEQFIELD3_DELAYCODE, REPSEQFIELD3_DELAYTIME};


/////////////////////////////////////////////////////////////////////////////
// Dialogfeld ReportSeqField3DateTimeDlg 

class ReportSeqField3DateTimeDlg : public CDialog
{
// Konstruktion
public:
	ReportSeqField3DateTimeDlg(CWnd* popParent, const CString &ropHeadline, CTime* popMinDate, CTime *popMaxDate, 
		const CString &ropField1Text, BasicDataType ipField1Type, 
		const CString &ropField2Text, BasicDataType ipField2Type, 
		const CString &ropField3Text, BasicDataType ipField3Type);
 
	CTime *pomMinDate;
	CTime *pomMaxDate;
	CString m_Alc2;
	CString m_Alc3;
// Dialogfelddaten
	//{{AFX_DATA(ReportSeqField3DateTimeDlg)
	enum { IDD = IDD_REPORTSEQFIELD3DATETIME };
	CCSEdit	m_CE_DateFrom;
	CCSEdit	m_CE_DateTo;
	CCSEdit	m_CE_TimeFrom;
	CCSEdit	m_CE_TimeTo;
	CString	m_DateFrom;
	CString	m_DateTo;
	CString	m_TimeFrom;
	CString	m_TimeTo;
	CCSEdit	m_CE_Field1;
	CCSEdit	m_CE_Field2;
	CCSEdit	m_CE_Field3;
	CString m_Field1;
	CString m_Field2;
	CString m_Field3;
	//}}AFX_DATA
	CString omHeadline;

 	// Vom Klassen-Assistenten generierte virtuelle Funktionsüberschreibungen
	//{{AFX_VIRTUAL(ReportSeqField3DateTimeDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	//}}AFX_VIRTUAL

// Implementierung
protected:

	// Generierte Nachrichtenzuordnungsfunktionen
	//{{AFX_MSG(ReportSeqField3DateTimeDlg)
	virtual void OnOK();
	virtual void OnCancel();
	virtual BOOL OnInitDialog();
	afx_msg LONG OnEditKillfocus( UINT wParam, LPARAM lParam);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

	const CString omField1Text;
	const BasicDataType imField1Type;

	const CString omField2Text;
	const BasicDataType imField2Type;

	const CString omField3Text;
	const BasicDataType imField3Type;

	void SetFieldType(BasicDataType ipFieldType, CCSEdit &ropField);


};

#endif  // REPORTSEQFIELD3DATETIMEDLG_H__INCLUDED_


//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio fügt zusätzliche Deklarationen unmittelbar vor der vorhergehenden Zeile ein.

