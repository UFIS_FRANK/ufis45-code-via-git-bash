#ifndef AFX_REPORTSELECTDLG_H__8BBC00F2_8B6C_11D1_8126_0000B43C4B01__INCLUDED_
#define AFX_REPORTSELECTDLG_H__8BBC00F2_8B6C_11D1_8126_0000B43C4B01__INCLUDED_

// ReportSelectDlg.h : Header-Datei
//

#include "ReportTableDlg.h"
#include "SeasonCedaFlightData.h"
#include "DlgResizeHelper.h"


// 2/99 hbe : Portionsweises Einlesen der Flugdaten ( RotationDlgCedaFlightData )  

// Zeitr�ume f�r partial read
enum READRANGE { RRDefault=7, RRDay=1, RRWeek=7, RRFortnight=14, RRMonth=28 } ;	

struct PARTIALREAD
{
	CString WhereClause ;	// gesamte WHERE-BEDINGUNG
	CTime StartTime ;		// Tag von
	CTime EndTime ;			// Tag bis
	int Range	;			// entspr. READRANGE
	int Case ;				// Reportkennung ( case 5-7 = Messestatistik / case 14 = POPS Season )

	void *omData ;			// Flugdaten
	CString Fields ;		// Feldliste
	bool onlyAFT ;			// AKtuelle oder mit Archive
	bool Sort ;				// Sortierung ?
	int	SortArt ;			// welche Sortierung
	
	PARTIALREAD() ;
	void Preset() ;		// Vorbelegung f�r Hauptanwendung ( alle POPS case 14 ! )
} ;


enum { X_TERMDEF=0, X_TERMNO=0, X_TERM1, X_TERM2, X_TERM12 } ;		// Kennung Terminal CheckBox (X) 


/////////////////////////////////////////////////////////////////////////////
// Vorbelegungswerte f�r wiederholten Dlg-Aufruf

class CLastDlgValues
{

private:

	// Zeitraum
	CString MinDate ;
	CString MaxDate ;
	CTime MinTime ;
	CTime MaxTime ;

	// Daily OPS-Plan
	bool Season ;	
	int Term ;
	CString Airlines ;
	CString AirlineEx ;
	CString Verkehr ;

	CString Select ;	// Airlinecodes
	CString Saison ;	// Saisonkennung ( case 8 )	
	CString Messe ;		// freier Text ( case 4 )	
	CString Flno ;		// Flugnummer ( case 2 )	
	CString Minuten ;	// Flugnummer ( case 3 + 6 )	
	int Radio ;			// Buttons ( case 11 )


public:
	// Konstruktor
	CLastDlgValues() ;
	// Destruktor dient als Reset !
	~CLastDlgValues() ;	

	void SetPreset(int ipTable, CTime & pStart, CTime & pEnd, CString & pSelect);
	
	CString GetMinDate() { return MinDate ; } ;
	CString GetMaxDate() { return MaxDate ; } ;
	CTime GetMinTime() { return MinTime ; } ;
	CTime GetMaxTime() { return MaxTime ; } ;
	CString GetAirlines() { return Airlines ; } ;
	CString GetAirlineEx() { return AirlineEx ; } ;
	CString GetVerkehr() { return Verkehr ; } ;
	CString GetSelect() { return Select ; } ;
	CString GetSaison() { return Saison ; } ;
	CString GetMesse() { return Messe ; } ;
	CString GetFlno() { return Flno ; } ;
	CString GetMinuten() { return Minuten ; } ;
	bool GetSeason() { return Season ; } ;
	int GetTerm() { return Term ; } ;
	int GetRadio() { return Radio ; } ;

	void SetMinDate( CString opMin = "" ) { MinDate = opMin ; } ;
	void SetMaxDate(CString opMax = "" ) {  MaxDate = opMax ; } ;
	void SetMinTime( CTime opMin = TIMENULL ) { MinTime = opMin ; } ;
	void SetMaxTime(CTime opMax = TIMENULL ) {  MaxTime = opMax ; } ;
	void SetAirlines( CString opStr = "") { Airlines = opStr ; } ;
	void SetAirlineEx( CString opStr = "") { AirlineEx = opStr ; } ;
	void SetVerkehr( CString opStr = "" ) { Verkehr = opStr ; } ;
	void SetSelect( CString opStr = "" ) { Select = opStr ; } ;
	void SetSaison( CString opStr = "" ) { Saison = opStr ; } ;
	void SetSeason( bool bpS = false ) { Season = bpS ; } ;
	void SetTerm( int ipT = X_TERMDEF ) { Term = ipT ; } ;
	void SetRadio( int ipT = 1 ) { Radio = ipT ; } ;
	void SetMesse( CString opStr = "" ) { Messe = opStr ; } ;
	void SetFlno( CString opStr = "" ) { Flno = opStr ; } ;
	void SetMinuten( CString opStr = "180" ) { Minuten = opStr ; } ;

} ;



/////////////////////////////////////////////////////////////////////////////
// Dialogfeld CReportSelectDlg 

class CReportSelectDlg : public CDialog
{
// Konstruktion
public:

	CReportSelectDlg(CWnd* pParent = NULL);   // Standardkonstruktor
	~CReportSelectDlg();

	void SaveToReg();

// nur f�r Season
	// bool ReadPartialWhere( const char *pspWhere, UINT ipRange = RRDefault );
	// f�r Season + Statistiken
	bool ReadPartialWhere( PARTIALREAD *prpPartial, CProgressCtrl *prpProgress = NULL );

	CCSPtrArray<ROTATIONDLGFLIGHTDATA> prmFlight;

	class CLastDlgValues omLastValues ;		// Presetwerte f�r Dlg

// Dialogfelddaten
	//{{AFX_DATA(CReportSelectDlg)
	enum { IDD = IDD_REPORTSELECT };
	CButton	m_CE_UtcToLocal;
	CButton	m_ST_Progress;
	CProgressCtrl	m_PR_Read;
	CButton	m_CR_List1;
	CButton	m_CR_List2;
	CButton	m_CR_List3;
	CButton	m_CR_List4;
	CButton	m_CR_List5;
	CButton	m_CR_List6;
	CButton	m_CR_List7;
	CButton	m_CR_List8;
	CButton	m_CR_List9;
	CButton	m_CR_List10;
	CButton	m_CR_List11;
	CButton	m_CR_List12;
	CButton	m_CR_List13;
	CButton	m_CR_List14;
	CButton	m_CR_List15;
	CButton	m_CR_List16;
	CButton	m_CR_List18;
	CButton	m_CR_List19;
	CButton	m_CR_List20;
	CButton	m_CR_List21;
	CButton	m_CR_List22;
	CButton	m_CR_List23;
	CButton	m_CR_List24;
	CButton	m_CR_List25;
	CButton	m_CR_List26;
	CButton	m_CR_List27;
	int		m_List1;
	CButton	m_CE_Loatab;
	//}}AFX_DATA


// �berschreibungen
	// Vom Klassen-Assistenten generierte virtuelle Funktions�berschreibungen
	//{{AFX_VIRTUAL(CReportSelectDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterst�tzung
	//}}AFX_VIRTUAL

	int imArrDep;	// 0 = Arr; 1= Dep

// Implementierung
	void MakeUtc( CString& ropMin, CString& ropMax ) ;
	void ShowStatus() ;
	void HideStatus() ;


protected:

	// Generierte Nachrichtenzuordnungsfunktionen
	//{{AFX_MSG(CReportSelectDlg)
	virtual void OnOK();
	virtual BOOL OnInitDialog();
	virtual void OnCancel();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnDestroy();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
	CViewer *pomCurrentViewer;


private:
	CReportTableDlg *pomReportTableDlg;
	CString omDefaultWhereSto ;		// Default Where-Clause �ber Standard Time of ...
	CString omDefaultWhereTif ;		// Default Where-Clause �ber Timeframe ...

	DlgResizeHelper m_resizeHelper;
	CString m_key;

	bool StartTableDlg();
};

//---------------------------------------------------------------------------------------------------------

extern SeasonCedaFlightData omCedaFlightData;
extern RotationDlgCedaFlightData omDailyCedaFlightData;	// f�r Daily OPS-Plan


#endif // AFX_REPORTSELECTDLG_H__8BBC00F2_8B6C_11D1_8126_0000B43C4B01__INCLUDED_
