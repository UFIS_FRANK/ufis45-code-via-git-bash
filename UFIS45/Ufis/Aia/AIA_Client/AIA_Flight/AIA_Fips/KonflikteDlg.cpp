// KonflikteDlg.cpp : implementation file
//

#include "stdafx.h"
#include "fpms.h"
#include "KonflikteDlg.h"
#include "RotationDlg.h"
#include "ButtonlistDlg.h"
#include "CCSGlobl.h"
#include "CCSDdx.h"
#include "RotGroundDlg.h"
#include "SeasonDlg.h"
#include "PrivList.h"
#include "Utils.h"

#include <iomanip.h>
#include <iostream.h>
#include <fstream.h>
#include <process.h>
#include "resrc1.h"
#include "CCSPrint.h"

 
#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif



// Local function prototype
static void KonfTableCf(void *popInstance, int ipDDXType,void *vpDataPointer, CString &ropInstanceName);



/////////////////////////////////////////////////////////////////////////////
// KonflikteDlg dialog




KonflikteDlg::KonflikteDlg(CWnd* pParent /*=NULL*/)
	: CDialog(KonflikteDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(KonflikteDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT

	pomTable = NULL;
	bmIsActiv = false;
	bmIsCreated = false;
    CDialog::Create(KonflikteDlg::IDD, pParent);
	bmIsCreated = true;
	m_key = "DialogPosition\\ConflictsAndAttention";

}


KonflikteDlg::~KonflikteDlg()
{
	ogDdx.UnRegister(this,NOTUSED);
	delete pomTable;
}



void KonflikteDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(KonflikteDlg)
	DDX_Control(pDX, IDC_ALLCONFIRM, m_CB_AllConfirm);
	DDX_Control(pDX, IDC_CONFIRM, m_CB_Confirm);
	DDX_Control(pDX, IDCANCEL, m_CB_Cancel);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(KonflikteDlg, CDialog)
	//{{AFX_MSG_MAP(KonflikteDlg)
	ON_BN_CLICKED(IDC_CONFIRM, OnConfirm)
	ON_BN_CLICKED(IDC_ALLCONFIRM, OnAllconfirm)
	ON_MESSAGE(WM_TABLE_LBUTTONDBLCLK, OnTableLButtonDblclk)
	ON_WM_TIMER()
    ON_MESSAGE(WM_TABLE_RETURN_PRESSED, OnTableReturnPressed)
	ON_BN_CLICKED(IDC_PRINT, OnPrint)
	ON_BN_CLICKED(IDC_EXCEL, OnExcel)
	ON_WM_SIZE()
 	ON_WM_DESTROY()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// KonflikteDlg message handlers

BOOL KonflikteDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	m_key = "DialogPosition\\ConflictsAndAttention";
	
    SetTimer(39, (UINT)  120 * 60 * 1000, NULL); // cleanup konf


	ogDdx.Register(this, KONF_DELETE,	CString(""), CString(""),	KonfTableCf);
	ogDdx.Register(this, KONF_INSERT,	CString(""), CString(""),	KonfTableCf);
	ogDdx.Register(this, KONF_CHANGE,	CString(""), CString(""),	KonfTableCf);

	m_CB_Confirm.SetSecState(ogPrivList.GetStat("CONFLICTS_CONFIRM"));		
	m_CB_AllConfirm.SetSecState(ogPrivList.GetStat("CONFLICTS_CONFIRMALL"));		

	CRect olRect;
	this->GetWindowRect(&olRect);
	GetDialogFromReg(olRect, m_key);
	SetWindowPos(&wndTop, olRect.left, olRect.top, olRect.Width(), olRect.Height(), SWP_HIDEWINDOW);// | SWP_NOMOVE );


	InitTable();

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void KonflikteDlg::SaveToReg()
{
	BOOL ok = WriteDialogToReg((CWnd*) this, m_key);
}


void KonflikteDlg::OnDestroy() 
{
	SaveToReg();
	CDialog::OnDestroy();
}

void KonflikteDlg::InitTable() 
{

	CRect rect;
    GetClientRect(&rect);
	rect.top += 50; 

    pomTable = new CCSTable;
	pomTable->SetSelectMode(LBS_MULTIPLESEL | LBS_EXTENDEDSEL);
	//pomTable->SetHeaderSpacing(0);


    rect.InflateRect(1, 1);     // hiding the CTable window border
    
	pomTable->SetTableData(this, rect.left, rect.right, rect.top, rect.bottom);


 

	CCSPtrArray <TABLE_HEADER_COLUMN> omHeaderDataArray;

	pomTable->ResetContent();
	
	TABLE_HEADER_COLUMN rlHeader;

	rlHeader.Alignment = COLALIGN_LEFT;
	rlHeader.Length = 2500; 
	rlHeader.Font = &ogCourier_Regular_10;
	rlHeader.Text = GetString(IDS_STRING1429);

	omHeaderDataArray.New(rlHeader);
	
	pomTable->SetHeaderFields(omHeaderDataArray);
	pomTable->SetDefaultSeparator();
	pomTable->SetTableEditable(false);
	omHeaderDataArray.DeleteAll();
	pomTable->DisplayTable();

}


void KonflikteDlg::Attention() 
{
	ShowWindow(SW_SHOW);
	SetWindowPos(&wndTopMost, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE);
	pomTable->ResetContent();
	bmAttention = true;
	bmIsActiv = true;
	m_CB_AllConfirm.ShowWindow(SW_SHOW);
	m_CB_Confirm.ShowWindow(SW_SHOW);
	SetWindowText(GetString(IMFK_ATTENTION));
	ChangeViewTo();
}


void KonflikteDlg::Conflicts() 
{
	ShowWindow(SW_SHOW);
	SetWindowPos(&wndTopMost, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE);
	pomTable->ResetContent();
	bmAttention = false;
	bmIsActiv = true;
	m_CB_AllConfirm.ShowWindow(SW_HIDE);
	m_CB_Confirm.ShowWindow(SW_HIDE);
	SetWindowText(GetString(IMFK_KONFLICTS));
	ChangeViewTo();
}





void KonflikteDlg::ChangeViewTo()
{
	int ilTopIndex = pomTable->GetCTableListBox()->GetTopIndex();

	DeleteAll();    
	MakeLines();
	UpdateDisplay();

	if (ilTopIndex > omLines.GetSize()-1)
		ilTopIndex = omLines.GetSize()-1;

	pomTable->GetCTableListBox()->SetTopIndex(ilTopIndex);
}


void KonflikteDlg::DeleteAll()
{
	omLines.DeleteAll();
}


void KonflikteDlg::MakeLines()
{
	KONFENTRY *prlEntry;
	KONFDATA  *prlData;

	for(int i = ogKonflikte.omData.GetSize() - 1; i >= 0; i--)
	{
		prlEntry = &ogKonflikte.omData[i];

		for(int j = prlEntry->Data.GetSize() - 1; j >= 0; j--)
		{
			prlData = &prlEntry->Data[j];
//			if(! ogKonflikte.IsBltKonfliktType(prlData->KonfId.Type))
			if (ogCfgData.CheckKonfliktForDisplay(prlData->KonfId.Type,CONF_ALL) == true)
				MakeLine(prlData->KonfId, prlData);
		}
	}
}


 
void KonflikteDlg::MakeLine(const KonfIdent &rrpKonfId, KONFDATA *prpData)
{
	if (prpData == NULL)
		return;

	KONF_LINEDATA rlLine;

	//if(prpData->SetBy != 0)
	{
		if((bmAttention && !prpData->Confirmed) || !bmAttention)
		{
			rlLine.KonfId = rrpKonfId;
			CString olStr = prpData->Text;
			if (prpData->blHighPrio)
				olStr = "! " + olStr;
			else
				olStr = "  " + olStr;
			olStr.Replace('\n', '-');
//			prpData->Text.Replace('\n', '-');
//			rlLine.Text = prpData->Text;
			rlLine.Text = olStr;
			rlLine.TimeOfConflict = prpData->TimeOfConflict;
			rlLine.clrPriority = prpData->clrPriority ;		// 050303 MVy: PRF6981: set line color
			rlLine.blHighPrio = prpData->blHighPrio ;		// 050303 MVy: PRF6981: set line color
			CreateLine(rlLine);
		}
	}

}



int KonflikteDlg::CreateLine(KONF_LINEDATA &rpKonf)
{
	if (FindLine(rpKonf.KonfId) >= 0) return -1;

    int ilLineCount = omLines.GetSize();

    for (int ilLineno = ilLineCount; ilLineno > 0; ilLineno--)
        if (CompareKonf(&rpKonf, &omLines[ilLineno-1]) >= 0)
            break;  // should be inserted after Lines[ilLineno-1]

    omLines.NewAt(ilLineno, rpKonf);
	if (rpKonf.blHighPrio == true)
	{
	}

    return ilLineno;
}


int KonflikteDlg::CompareKonf(KONF_LINEDATA *prpKonf1, KONF_LINEDATA *prpKonf2)
{
	int	ilCompareResult = 0;
	if (prpKonf1->blHighPrio == true && prpKonf2->blHighPrio == false)
		ilCompareResult = -1;
	else if (prpKonf1->blHighPrio == false && prpKonf2->blHighPrio == true)
		ilCompareResult = 1;
	else
	{
		ilCompareResult = (prpKonf1->TimeOfConflict == prpKonf2->TimeOfConflict)? 0:
		(prpKonf1->TimeOfConflict > prpKonf2->TimeOfConflict)? 1: -1;
	}

	return ilCompareResult;
}



void KonflikteDlg::UpdateDisplay()
{
	CCSPtrArray<TABLE_COLUMN> olColList;
	TABLE_COLUMN rlColumnData;

	int ilLine = pomTable->pomListBox->GetTopIndex();

  	pomTable->ResetContent();
    for (int ilLc = 0; ilLc < omLines.GetSize(); ilLc++)
	{
		rlColumnData.Text = omLines[ilLc].Text;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		olColList[0].BkColor = omLines[ilLc].clrPriority ;		// 050303 MVy: PRF6981: set background color of cell
		pomTable->AddTextLine(olColList , &omLines[ilLc]);
		olColList.DeleteAll();
    }                                           
    pomTable->DisplayTable();

	if (ilLine >= 0)
		pomTable->pomListBox->SetTopIndex(ilLine);
}

void KonflikteDlg::OnConfirm() 
{
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
	
	KONF_LINEDATA *prlLine;
	int ilAnzSel = 0;

	CListBox *polLB = pomTable->GetCTableListBox();
	if (polLB == NULL)
		return;

	int *ilItems = NULL;
	int ilAnz = polLB->GetSelCount();
	if(ilAnz > 0)
	{
		ilItems = new int[ilAnz];
	}
	else
		return;

	if(ilItems != NULL)
		ilAnz = polLB->GetSelItems(ilAnz, ilItems);


	CPtrArray olKonfIds;
	for(int i = 0; i < ilAnz; i++)
	{
		prlLine = (KONF_LINEDATA *)pomTable->GetTextLineData(ilItems[i]);
		if(prlLine != NULL)
		{
			// collect records to confirm
			olKonfIds.Add((void *)&prlLine->KonfId);
		}
	}
	ogKonflikte.Confirm(olKonfIds);

	if(ilItems != NULL)
		delete [] ilItems;

	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
}


/*
void KonflikteDlg::OnConfirm() 
{

	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
	
	int ilItems[3000];
	int ilAnz = 3000;
	KONF_LINEDATA *prlLine;
	int ilAnzSel = 0;

	CListBox *polLB = pomTable->GetCTableListBox();
	if (polLB == NULL)
		return;

	ilAnz = MyGetSelItems(*polLB, ilAnz, ilItems);
	if(ilAnz <= 0)
	{
		//MessageBox(GetString(IDS_STRING968), GetString(ST_FEHLER), MB_ICONWARNING);
		return;
	}

	CPtrArray olKonfIds;
	for(int i = 0; i < ilAnz; i++)
	{
		prlLine = (KONF_LINEDATA *)pomTable->GetTextLineData(ilItems[i]);
		if(prlLine != NULL)
		{
			// collect records to confirm
			olKonfIds.Add((void *)&prlLine->KonfId);
		}
	}
	ogKonflikte.Confirm(olKonfIds);

	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));


}
*/

void KonflikteDlg::OnAllconfirm() 
{
	if (pomTable->GetLinesCount() > 0)
	{
		if (MessageBox(GetString(IDS_STRING1967), GetString(ST_FRAGE), MB_YESNO | MB_ICONQUESTION | MB_SYSTEMMODAL | MB_DEFBUTTON2) == IDYES)
		{
			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
			ogKonflikte.ConfirmAll();
			OnCancel();
	
			//pogButtonList->m_CB_Achtung.SetColors(::GetSysColor(COLOR_BTNFACE), ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
		}
	}
}

void KonflikteDlg::OnSize(UINT nType, int cx, int cy) 
{
	if(bmIsCreated != false && cx > 250)
	{
		CDialog::OnSize(nType, cx, cy);
	
		if (nType != SIZE_MINIMIZED)
		{
			// resize table
			pomTable->SetPosition(1, cx+1, 50+1, cy+1);
			// reposition buttons
			m_CB_AllConfirm.SetWindowPos(this, cx-234,3,0,0, SWP_NOSIZE | SWP_NOZORDER | SWP_SHOWWINDOW);
			m_CB_Confirm.SetWindowPos(this, cx-159,3,0,0, SWP_NOSIZE | SWP_NOZORDER | SWP_SHOWWINDOW);
			m_CB_Cancel.SetWindowPos(this, cx-77,3,0,0, SWP_NOSIZE | SWP_NOZORDER | SWP_SHOWWINDOW);
			m_CB_AllConfirm.Invalidate();
			m_CB_Confirm.Invalidate();
			m_CB_Cancel.Invalidate();
			if (!bmAttention)
			{
				m_CB_AllConfirm.ShowWindow(SW_HIDE);
				m_CB_Confirm.ShowWindow(SW_HIDE);
			}
		}
	}
}

void KonflikteDlg::OnCancel() 
{
	omLines.DeleteAll();
	bmIsActiv = false;
	ShowWindow(SW_HIDE);
	//CDialog::OnCancel();
}


static void KonfTableCf(void *popInstance, int ipDDXType,void *vpDataPointer, CString &ropInstanceName)
{
	if (vpDataPointer == NULL)
		return;
	KonflikteDlg *polDlg = (KonflikteDlg *)popInstance;

	if (ipDDXType == KONF_DELETE)
		polDlg->DdxDeleteKonf(*(KonfIdent *)vpDataPointer);
	if (ipDDXType == KONF_INSERT)
		polDlg->DdxInsertKonf(*(KonfIdent *)vpDataPointer);
	if (ipDDXType == KONF_CHANGE)
		polDlg->DdxChangeKonf(*(KonfIdent *)vpDataPointer);

}

void KonflikteDlg::DdxChangeKonf(KonfIdent &rrpIdNotify)
{
	if(!bmIsActiv)
		return;

	DdxDeleteKonf(rrpIdNotify);

	DdxInsertKonf(rrpIdNotify);


/*
	KONFDATA  *prlData = NULL;
	KONF_LINEDATA *prlLine;
	CCSPtrArray<TABLE_COLUMN> olColList;
	TABLE_COLUMN rlColumnData;

	int ilLineNo = FindLine(prlNotify->Urno, prlNotify->Type);

	if(ilLineNo >= 0)
	{
		prlData = ogKonflikte.GetKonflikt(prlNotify->Urno, prlNotify->Type);

		if(prlData != NULL)
		{
			prlLine = &omLines[ilLineNo];
			prlLine->Text = CString(prlData->Text);
			prlLine->TimeOfConflict = prlData->TimeOfConflict;

			rlColumnData.Text = omLines[ilLineNo].Text;
			olColList.NewAt(olColList.GetSize(), rlColumnData);
			pomTable->ChangeTextLine(ilLineNo, &olColList , &omLines[ilLineNo]);
			olColList.DeleteAll();
		}
	}
	*/
}


void KonflikteDlg::DdxInsertKonf(KonfIdent &rrpIdNotify)
{
	if(!bmIsActiv)
		return;

//	int ilLine = pomTable->pomListBox->GetTopIndex();

	KONFDATA  *prlData = NULL;
	KONF_LINEDATA rlLine;
	CCSPtrArray<TABLE_COLUMN> olColList;
	TABLE_COLUMN rlColumnData;
	int ilLineNo;

	prlData = ogKonflikte.GetKonflikt(rrpIdNotify);

	if(prlData != NULL)
	{

		if(prlData->SetBy != 0)
		{
			if((bmAttention && !prlData->Confirmed) || !bmAttention)
			{
				rlLine.KonfId = rrpIdNotify;

				CString olStr = prlData->Text;
				olStr.Replace('\n', '-');
				if (prlData->blHighPrio)
					olStr = "! " + olStr;
				else
					olStr = "  " + olStr;
				rlLine.Text = olStr;
				rlLine.TimeOfConflict = prlData->TimeOfConflict;
				rlLine.blHighPrio = prlData->blHighPrio ;		// 050303 MVy: PRF6981: set line color

				if (ogCfgData.CheckKonfliktForDisplay(prlData->KonfId.Type,CONF_ALL) == true)
				{
				
					ilLineNo = CreateLine(rlLine);

					if (ilLineNo != -1)
					{
						rlColumnData.Text = rlLine.Text;
						olColList.NewAt(olColList.GetSize(), rlColumnData);
						olColList[0].BkColor = prlData->clrPriority ;		// 050303 MVy: PRF6981: set background color of cell
						pomTable->InsertTextLine(ilLineNo, olColList , &omLines[ilLineNo]);
						olColList.DeleteAll();
					}
				}
			}
		}
	}

//	if (ilLine >= 0)
//		pomTable->pomListBox->SetTopIndex(ilLine);

}


void KonflikteDlg::DdxDeleteKonf(KonfIdent &rrpIdNotify)
{
	if(!bmIsActiv)
		return;

	int ilLineNo = FindLine(rrpIdNotify);

	if(ilLineNo >= 0)
	{
		omLines.DeleteAt(ilLineNo);
		pomTable->DeleteTextLine(ilLineNo);
	}

//	if (ilLine >= 0)
//		pomTable->pomListBox->SetTopIndex(ilLine);
}


int KonflikteDlg::FindLine(const KonfIdent &rrpKonfId)
{
	for(int i = omLines.GetSize() - 1; i >= 0; i--)
	{
		if(omLines[i].KonfId == rrpKonfId)
			break;
	}
	return i;
}


LONG KonflikteDlg::OnTableLButtonDblclk( UINT wParam, LPARAM lParam)
{
	CCSTABLENOTIFY olNotify = *(CCSTABLENOTIFY*)lParam;

	KONF_LINEDATA *prlLine = (KONF_LINEDATA*)pomTable->GetTextLineData(wParam);

	if(prlLine == NULL)
		return 0L;

	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));


	if (ogPosDiaFlightData.bmOffLine)
	{
		// only show resource data in offline mode
 		pogSeasonDlg->NewData(this, ogPosDiaFlightData.GetFlightByUrno(prlLine->KonfId.FUrno), bgGatPosLocal);
	}
	else
	{
		// get flight data
		char clFTyp;
		long llUrno = 0;
		long llRkey;
		CTime olTifad;
		bool blLocal;
		// try dia data
		DIAFLIGHTDATA *prlDiaFlight = ogPosDiaFlightData.GetFlightByUrno(prlLine->KonfId.FUrno);
		if (prlDiaFlight)
		{
			llUrno = prlDiaFlight->Urno;
			llRkey = prlDiaFlight->Rkey;
			clFTyp = prlDiaFlight->Ftyp[0];
			if (prlLine->KonfId.FPart == 'A')
				olTifad = prlDiaFlight->Tifa;
			else
				olTifad = prlDiaFlight->Tifd;
			blLocal = bgGatPosLocal;
		}
		else
		{
			// try daily data
			ROTATIONFLIGHTDATA *prlDailyFlight = ogRotationFlights.GetFlightByUrno(prlLine->KonfId.FUrno);
			if (prlDailyFlight)
			{
				llUrno = prlDailyFlight->Urno;
				llRkey = prlDailyFlight->Rkey;
				clFTyp = prlDailyFlight->Ftyp[0];
				if (prlLine->KonfId.FPart == 'A')
					olTifad = prlDailyFlight->Tifa;
				else
					olTifad = prlDailyFlight->Tifd;
				blLocal = bgDailyLocal;
			}
			else
			{
				// flight not found!!!
				CFPMSApp::DumpDebugLog("ERROR (KonflikteDlg::OnTableLButtonDblclk): FLIGHT NOT FOUND IN INTERNAL DATA!");
				SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
				return 0L;
			}
		}


		// display rotation mask!
 		CFPMSApp::ShowFlightRecordData(this, clFTyp, llUrno, llRkey, prlLine->KonfId.FPart, olTifad, blLocal); 



		/*
		// try to show flight in daily rotation dialog mask
		if (!pogRotationDlg->NewData(this, 0L, prlLine->KonfId.FUrno, "X", bgDailyLocal))
		{
 			// show towing dialog mask
			if(pogRotGroundDlg == NULL)
			{
				pogRotGroundDlg = new RotGroundDlg(this);
				pogRotGroundDlg->Create(IDD_ROTGROUND);
			}

			pogRotGroundDlg->SetWindowPos(&wndTop, 0,0,0,0, SWP_SHOWWINDOW | SWP_NOSIZE | SWP_NOMOVE );
			pogRotGroundDlg->NewData(this, 0L, prlLine->KonfId.FUrno, bgDailyLocal);
		}
		*/
	}
 
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
	return 0L;
}



void KonflikteDlg::OnTimer(UINT nIDEvent) 
{
	if (nIDEvent == 39)	
	{
		ogKonflikte.CleanUp();

	}
}


LONG KonflikteDlg::OnTableReturnPressed( UINT wParam, LPARAM lParam)
{
	OnTableLButtonDblclk(wParam, lParam); 
	return 0L;
}

void KonflikteDlg::OnPrint() 
{
	// TODO: Add your control notification handler code here
	CString olFooter1,olFooter2;
	CString olTableName;
	if (bmAttention)
		olTableName = GetString(IDS_STRING2151);//"Attentions";
	else
		olTableName = GetString(IDS_STRING2150);//"Conflicts";
		
	int ilOrientation = PRINT_LANDSCAPE;//  PRINT_PORTRAET  PRINT_LANDSCAPE
	pomPrint = new CCSPrint((CWnd*)this,ilOrientation,45);// FirstLine(P=200,L=200),LeftOffset(P=200,L=50)
	pomPrint->imMaxLines = 38;  // (P=57,L=38)

	if (pomPrint != NULL)
	{
		if (pomPrint->InitializePrinter(ilOrientation) == TRUE)
		{
			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
			pomPrint->imLineNo = pomPrint->imMaxLines + 1;
			DOCINFO	rlDocInfo;
			memset(&rlDocInfo, 0, sizeof(DOCINFO));
			rlDocInfo.cbSize = sizeof( DOCINFO );
			rlDocInfo.lpszDocName = olTableName;	
			//rlDocInfo.lpszDocName = TABLENAME;	
			pomPrint->omCdc.StartDoc( &rlDocInfo );
			pomPrint->imPageNo = 0;
			int ilLines = omLines.GetSize();  

			olFooter1.Format("%s -   %s", olTableName, (CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M")) );
//			GetHeader(); 

			if (ilLines == 0)
				PrintTableHeader(); // Drucken leerer Tabelle
			for(int i = 0; i < ilLines; i++ ) 
			{
				if(pomPrint->imLineNo >= pomPrint->imMaxLines)
				{
					if(pomPrint->imPageNo > 0)
					{
						olFooter2.Format(GetString(IDS_STRING1198),pomPrint->imPageNo);
						pomPrint->PrintUIFFooter(olFooter1,"",olFooter2);
						pomPrint->omCdc.EndPage();
					}
					PrintTableHeader();
				}
				if(pomPrint->imLineNo == (pomPrint->imMaxLines-1) || i == (ilLines-1))
				{
					PrintTableLine(&omLines[i],true);
				}
				else
				{
					PrintTableLine(&omLines[i],false);
				}
			}
			olFooter2.Format(GetString(IDS_STRING1198),pomPrint->imPageNo);
			pomPrint->PrintUIFFooter(olFooter1,"",olFooter2);
			pomPrint->omCdc.EndPage();
			pomPrint->omCdc.EndDoc();
		}

//		omPrintHeadHeaderArray.DeleteAll();
		SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
		delete pomPrint;
		pomPrint = NULL;
	}
	
}

bool KonflikteDlg::PrintTableLine(KONF_LINEDATA *prpLine,bool bpLastLine)
{
	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;
	double dgCCSPrintFactor = 2.7 ;
	rlElement.pFont      = &pomPrint->ogCourierNew_Regular_8;
	int ilSize = 1;//omPrintHeadHeaderArray.GetSize();
	for(int i=0;i<ilSize;i++)
	{
		rlElement.Length = (int)(1000*dgCCSPrintFactor);
		if(rlElement.Length < igCCSPrintMinLength)rlElement.Length+=igCCSPrintMoreLength; 
		rlElement.Alignment  = PRINT_LEFT;
		rlElement.FrameTop   = PRINT_FRAMETHIN;
		if(bpLastLine == true)
		{
			rlElement.FrameBottom = PRINT_FRAMETHIN;
		}
		else
		{
			rlElement.FrameBottom = PRINT_NOFRAME;
		}
//		rlElement.FrameLeft  = PRINT_NOFRAME;
//		rlElement.FrameRight = PRINT_NOFRAME;
			switch(i)
			{
				case 0:
				{
					rlElement.FrameLeft  = PRINT_FRAMETHIN;
					rlElement.Text		= prpLine->Text;
					rlElement.FrameRight = PRINT_FRAMETHIN;
				}
				break;
			}


		rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
		
	}
	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();
	return true;
}

bool KonflikteDlg::PrintTableHeader()
{
	CString olBuffer;
	if (bmAttention)
		olBuffer = GetString(IDS_STRING2151);//"Attentions";
	else
		olBuffer = GetString(IDS_STRING2150);//"Conflicts";

	CString olTableName;
	olTableName.Format("%s -   %s", olBuffer, (CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M")) );

	pomPrint->omCdc.StartPage();
	pomPrint->imPageNo++;
	pomPrint->imLineNo = 0;
	double dgCCSPrintFactor = 2.7 ;
	pomPrint->PrintUIFHeader(olTableName, "", pomPrint->imFirstLine-10);

	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;

	rlElement.Alignment   = PRINT_LEFT;
	rlElement.FrameLeft   = PRINT_NOFRAME;
	rlElement.FrameRight  = PRINT_NOFRAME;
	rlElement.FrameTop    = PRINT_NOFRAME;
	rlElement.FrameBottom = PRINT_FRAMEMEDIUM;
	rlElement.pFont       = &pomPrint->ogCourierNew_Bold_8;

	int ilSize = 1;//omPrintHeadHeaderArray.GetSize();

	for(int i=0;i<ilSize;i++)
	{
			rlElement.Length = (int)(1000*dgCCSPrintFactor);
			if(rlElement.Length < igCCSPrintMinLength)rlElement.Length+=igCCSPrintMoreLength; 
			rlElement.Text   = "FLIGHT               CONFLICT";//omPrintHeadHeaderArray[i].Text;
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
	}
	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();


	return true;

}

void KonflikteDlg::OnExcel() 
{
	// TODO: Add your control notification handler code here
	
	char pclConfigPath[256];
	char pclExcelPath[256];
	char pclTrenner[64];
	

    if (getenv("CEDA") == NULL)
        strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
    else
        strcpy(pclConfigPath, getenv("CEDA"));

    GetPrivateProfileString(ogAppName, "Excel", "DEFAULT",
        pclExcelPath, sizeof pclExcelPath, pclConfigPath);


    GetPrivateProfileString(ogAppName, "ExcelSeperator", ";",
        pclTrenner, sizeof pclTrenner, pclConfigPath);


	if(!strcmp(pclExcelPath, "DEFAULT"))
		CFPMSApp::MyTopmostMessageBox(this,GetString(IDS_STRING977), GetString(ST_FEHLER), MB_ICONERROR);


	CreateExcelFile(pclTrenner);

	bool test = true; //only for testing error
	if (omFileName.IsEmpty() || omFileName.GetLength() > 255 || !test)
	{
		if (omFileName.IsEmpty())
		{
			CString mess = "Filename is empty !" + omFileName;
			CFPMSApp::MyTopmostMessageBox(this, mess, GetString(ST_FEHLER), MB_ICONERROR);
			return;
		}
		if (omFileName.GetLength() > 255 )
		{
			CString mess = "Length of the Filename > 255: " + omFileName;
			CFPMSApp::MyTopmostMessageBox(this, mess, GetString(ST_FEHLER), MB_ICONERROR);
			return;
		}

		CString mess = "Filename invalid: " + omFileName;
		CFPMSApp::MyTopmostMessageBox(this, mess, GetString(ST_FEHLER), MB_ICONERROR);
		return;
	}

	char pclTmp[256];
	strcpy(pclTmp, omFileName); 

   /* Set up parameters to be sent: */
	char *args[4];
	args[0] = "child";
	args[1] = pclTmp;
	args[2] = NULL;
	args[3] = NULL;

	_spawnv( _P_NOWAIT , pclExcelPath, args );
}

bool KonflikteDlg::CreateExcelFile(CString opTrenner)
{
	ofstream of;

	char* tmpvar;
	tmpvar = getenv("TMP");
	if (tmpvar == NULL)
		tmpvar = getenv("TEMP");

	if (tmpvar == NULL)
		return false;

	CString olBuffer;
	if (bmAttention)
		olBuffer = GetString(IDS_STRING2151);//"Attentions";
	else
		olBuffer = GetString(IDS_STRING2150);//"Conflicts";

	CString olTableName;
	olTableName.Format("%s -   %s", olBuffer, (CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M")) );


	CString olFileName = olTableName;
	olFileName.Remove('*');
	olFileName.Remove('.');
	olFileName.Remove(':');
	olFileName.Remove('/');
	olFileName.Replace(" ", "_");
	
	char pHeader[256];
	strcpy (pHeader, tmpvar);
	CString path = pHeader;
	omFileName =  path + "\\" + olFileName + ".csv";

	of.open( omFileName, ios::out);
	of.setf(ios::left, ios::adjustfield);


	of  << setw(1) << olTableName
		<< endl;

	of	<< setw(1) << "FLIGHT               CONFLICT"
		<< endl;


	int ilCountLines = omLines.GetSize();

	// zeilen iterieren
	for(int ilLines = 0; ilLines < ilCountLines; ilLines++)
	{
		KONF_LINEDATA rlLine = omLines[ilLines];

		of << setw(1) << rlLine.Text
		   << endl;
	}

	// stream schliessen
	of.close();

	return true;

}
