// ReportTimeFrameTableViewer.cpp : implementation file
// 
// flight statstics according to time frame


#include "stdafx.h"
#include "ReportSameTimeFrameTableViewer.h"
//#include "ReportXXXCedaData.h"
#include "CedaBasicData.h"
#include "CcsGlobl.h"
#include <iomanip.h>
#include <iostream.h>
#include <fstream.h>
#include "BasicData.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif


// inhalt der APT.APTT ist flughafenspezifisch, f�r SHA:
#define INTL	'I'
#define DOM		'D'


/////////////////////////////////////////////////////////////////////////////
// ReportTimeFrameTableViewer
//

ReportTimeFrameTableViewer::ReportTimeFrameTableViewer(CCSPtrArray<ROTATIONDLGFLIGHTDATA> *popData,
													   const CStringArray &opShownColumns,
													   char *pcpInfo,
													   char *pcpSelect)
{

	pomData = popData;
	pcmInfo = pcpInfo;
	pcmSelect = pcpSelect;

	bmIsFromSearch = false;
    pomTable = NULL;
	imTableLine = 0;


	// Anfangs und Enddatum extrahieren
	CString olStr(pcpInfo);
	int ilIndex = olStr.Find("-");
	strcpy(pcmLastDate, (strstr(pcpInfo, "-") + 2));
	olStr.SetAt(ilIndex-1, 0);
	strcpy(pcmFirstDate, olStr);

	InitColumnsDays();

	omShownColumns.Copy(opShownColumns);

}


ReportTimeFrameTableViewer::~ReportTimeFrameTableViewer()
{

	omPrintHeadHeaderArray.DeleteAll();
	omAnzChar.RemoveAll();
	omShownColumns.RemoveAll();
    DeleteAll();

	omColumn0.RemoveAll();
	omColumn1.RemoveAll();
	omColumn2.RemoveAll();
	omColumn3.RemoveAll();
	omColumn4.RemoveAll();
	omCountColumn1.Add(0);
	omCountColumn2.Add(0);
	omCountColumn3.Add(0);
	omCountColumn4.Add(0);
	omCountColumn5.Add(0);
	omCountColumn6.Add(0);
	omCountColumn7.Add(0);
	omCountColumn8.Add(0);

}


void ReportTimeFrameTableViewer::InitColumnsDays()
{

	char clTmp[8];
	int ilDayFirst, ilMonthFirst, ilYearFirst;
	int ilDayLast, ilMonthLast, ilYearLast;

	int i = 0;
	int j = 0;
	while(pcmFirstDate[i] != '.')
	{
		clTmp[j] = pcmFirstDate[i];
		j++;
		i++;
	}
	clTmp[j] = 0;
	ilDayFirst = atoi(clTmp);

	// integer aus Datumstring extrahieren
	j = 0;
	i++;
	while (pcmFirstDate[i] != '.')
	{
		clTmp[j] = pcmFirstDate[i];
		j++;
		i++;
	}
	clTmp[j] = 0;
	ilMonthFirst = atoi(clTmp);

	j = 0;
	i++;
	while (pcmFirstDate[i] != '\0')
	{
		clTmp[j] = pcmFirstDate[i];
		j++;
		i++;
	}
	clTmp[j] = 0;
	ilYearFirst = atoi(clTmp);

	j = 0;
	i = 0;
	while(pcmLastDate[i] != '.')
	{
		clTmp[j] = pcmLastDate[i];
		j++;
		i++;
	}
	clTmp[j] = 0;
	ilDayLast = atoi(clTmp);

	j = 0;
	i++;
	while (pcmLastDate[i] != '.')
	{
		clTmp[j] = pcmLastDate[i];
		j++;
		i++;
	}
	clTmp[j] = 0;
	ilMonthLast = atoi(clTmp);

	j = 0;
	i++;
	while (pcmLastDate[i] != '\0')
	{
		clTmp[j] = pcmLastDate[i];
		j++;
		i++;
	}
	clTmp[j] = 0;
	ilYearLast = atoi(clTmp);

	CTime t1(ilYearFirst, ilMonthFirst, ilDayFirst, 0, 0, 0);
	CTime t2(ilYearLast, ilMonthLast, ilDayLast, 23, 59, 59);
	CTimeSpan ts = t2 - t1;  // Subtract 2 CTimes

	CTimeSpan olAddDay(1, 0, 0, 0);

	long llDays = ts.GetDays();
	for (i = 0; i <= llDays; i++)
	{
		omColumn0.Add(t1.Format("%d/%m/%Y"));
		omCountColumn1.Add(0);
		omCountColumn2.Add(0);
		omCountColumn3.Add(0);
		omCountColumn4.Add(0);
		omCountColumn5.Add(0);
		omCountColumn6.Add(0);
		omCountColumn7.Add(0);
		omCountColumn8.Add(0);
		t1 = t1 + olAddDay;
	}
	omColumn0.Add(t1.Format("Total"));
	omCountColumn1.Add(0);
	omCountColumn2.Add(0);
	omCountColumn3.Add(0);
	omCountColumn4.Add(0);
	omCountColumn5.Add(0);
	omCountColumn6.Add(0);
	omCountColumn7.Add(0);
	omCountColumn8.Add(0);

}


void ReportTimeFrameTableViewer::SetColumnsToShow(const CStringArray &opShownColumns)
{

	CCS_TRY

	omShownColumns.Copy(opShownColumns);

	CCS_CATCH_ALL

}


void ReportTimeFrameTableViewer::SetParentDlg(CDialog* ppParentDlg)
{

	pomParentDlg = ppParentDlg;

}


void ReportTimeFrameTableViewer::Attach(CCSTable *popTable)
{
    pomTable = popTable;
}


int ReportTimeFrameTableViewer::GetVia(CCSPtrArray<VIADATA> *opVias, ROTATIONDLGFLIGHTDATA *prpFlight)
{
	opVias->DeleteAll();

	if(prpFlight == NULL)
		return 0;
	if(strlen(prpFlight->Vial) == 0)
		return 0;

	CString olVias(prpFlight->Vial);
	CString olFids;
	CString olApc3;
	CString olApc4;

	VIADATA *prlVia;

	while(olVias.IsEmpty() != TRUE)
	{
		prlVia = new VIADATA;
		opVias->Add(prlVia);

		if(olVias.GetLength() < 120)
		{
			olVias += "                                                                                                                             ";
			olVias = olVias.Left(120);
		}


		olApc3 = olVias.Mid(1,3);
		olApc3.TrimLeft();
		sprintf(prlVia->Apc3, olApc3);

		if(olVias.GetLength() >= 120)
			olVias = olVias.Right(olVias.GetLength() - 120);
	}
	return opVias->GetSize();
}


void ReportTimeFrameTableViewer::ChangeViewTo(const char *pcpViewName)
{

    pomTable->ResetContent();
    DeleteAll();    
    MakeLines();

	UpdateDisplay();
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
										  
}


/////////////////////////////////////////////////////////////////////////////
// ReportTimeFrameTableViewer -- code specific to this class

void ReportTimeFrameTableViewer::MakeLines()
{
	ROTATIONDLGFLIGHTDATA *prlAFlight;
	ROTATIONDLGFLIGHTDATA *prlDFlight;
	ROTATIONDLGFLIGHTDATA *prlNextFlight = NULL;
	ROTATIONDLGFLIGHTDATA *prlFlight;
	int ilLineNo;

	bool blRDeparture = false;

	int ilFlightCount = pomData->GetSize();

	for (int ilLc = 0; ilLc < ilFlightCount; ilLc++)
	{
		prlFlight = &(*pomData)[ilLc];

		if(ilLc + 1 < ilFlightCount)
			prlNextFlight = &(*pomData)[ilLc + 1];	
		else
			prlNextFlight = NULL;	

		//Arrival
		if((strcmp(prlFlight->Org3, pcgHome) != 0) && (strcmp(prlFlight->Des3, pcgHome) == 0))
		{
			prlAFlight = prlFlight;
			prlDFlight = NULL;
			if(prlNextFlight != NULL)
			{
				if((prlNextFlight->Rkey == prlFlight->Rkey) && (prlFlight->Rkey != NULL))
				{
					prlDFlight = prlNextFlight;

					if((strcmp(prlNextFlight->Org3, pcgHome) == 0) && (strcmp(prlNextFlight->Des3, pcgHome) != 0))
					{
						ilLc++;
					}
					else
						blRDeparture = true;
				}
			}
		}
		else
		{
			// Departure
			if((strcmp(prlFlight->Org3, pcgHome) == 0) && (strcmp(prlFlight->Des3, pcgHome) != 0))
			{
				prlAFlight = NULL;
				prlDFlight = prlFlight;
				blRDeparture = false;
			}
			else
			{
				//Turnaround
				if((strcmp(prlFlight->Org3, pcgHome) == 0) && (strcmp(prlFlight->Des3, pcgHome) == 0))
				{
					if(blRDeparture)
					{
						blRDeparture = false;
						prlAFlight = prlFlight;
						prlDFlight = NULL;
						if(prlNextFlight != NULL)
						{
							if((prlNextFlight->Rkey == prlFlight->Rkey) && (prlFlight->Rkey != NULL))
							{
								prlDFlight = prlNextFlight;
								if((strcmp(prlNextFlight->Org3, pcgHome) == 0) && (strcmp(prlNextFlight->Des3, pcgHome) != 0))
								{
									ilLc++;
								}
								else
									blRDeparture = true;
							}
						}
					}
					else
					{
						prlAFlight		= prlFlight;
						prlDFlight		= prlFlight;
						if(prlNextFlight != NULL)
						{
							if((prlNextFlight->Rkey == prlFlight->Rkey) && (prlFlight->Rkey != NULL))
							{
								ilLc--;
								prlAFlight	 = NULL;
								blRDeparture = true;
							}
						}
					}
				}
			}
		}
		ilLineNo = MakeLine(prlAFlight, prlDFlight);

		// Listen zum Zaehlen aufbauen
		MakeCountListNDays(prlAFlight, prlDFlight);

	}
	
	MakeTotalLine();

}


int ReportTimeFrameTableViewer::MakeLine(ROTATIONDLGFLIGHTDATA *prpAFlight, ROTATIONDLGFLIGHTDATA *prpDFlight)
{
    TIMEFRAMETABLE_LINEDATA rlLine;

	if((prpAFlight != NULL) && (prpDFlight != NULL) )
	{
		MakeLineData(prpAFlight, prpDFlight, rlLine);
		return CreateLine(rlLine);
	}
	if (prpAFlight != NULL)
	{
		MakeLineData(prpAFlight, NULL, rlLine);
		return CreateLine(rlLine);
	}
	if (prpDFlight != NULL)
	{
		MakeLineData(NULL, prpDFlight, rlLine);
		return CreateLine(rlLine);
	}
	return -1;
}




void ReportTimeFrameTableViewer::MakeLineData(ROTATIONDLGFLIGHTDATA *prpAFlight, ROTATIONDLGFLIGHTDATA *prpDFlight, TIMEFRAMETABLE_LINEDATA &rpLine)
{

	CString olStr;

	if(prpAFlight != NULL)
	{
		rpLine.AUrno =  prpAFlight->Urno;
		rpLine.ARkey =  prpAFlight->Rkey;
		rpLine.AFlno = CString(prpAFlight->Flno);
		rpLine.ADate = DateToHourDivString(prpAFlight->Onbl, prpAFlight->Stoa);
		rpLine.ATtyp = prpAFlight->Ttyp; 
		rpLine.AAct = CString(prpAFlight->Act3);
		rpLine.AAlc2 = CString(prpAFlight->Alc2);
		rpLine.AAlc3 = CString(prpAFlight->Alc3);

		rpLine.AStoa = CTime(prpAFlight->Stoa);
		if(bgReportLocal) 
		{
			ogBasicData.UtcToLocal(rpLine.AStoa);
			ogBasicData.UtcToLocal(prpAFlight->Stoa);
		}
		rpLine.AEtai = CTime(prpAFlight->Etai);
		if(bgReportLocal) ogBasicData.UtcToLocal(rpLine.AEtai);
		rpLine.ALand = CTime(prpAFlight->Land);
		if(bgReportLocal) ogBasicData.UtcToLocal(rpLine.ALand);

		rpLine.APaxt = CString(prpAFlight->Paxt);
		rpLine.APaxi = CString(prpAFlight->Paxi);

		olStr = CString(prpAFlight->Org3);
	}
	else
	{
		rpLine.AUrno =  0;
		rpLine.ARkey =  0;
		rpLine.AFlno = "";
		rpLine.ADate = ""; 
		rpLine.ATtyp = ""; 
		rpLine.AAct = "";
		rpLine.AAlc2 = "";
		rpLine.AAlc3 = "";
		rpLine.AEtai = -1;
		rpLine.ALand = -1;
		rpLine.AStoa = -1;
		rpLine.APaxt = "";
		rpLine.APaxi = "";
	}


	if(prpDFlight != NULL)
	{

		rpLine.DUrno =  prpDFlight->Urno;
		rpLine.DRkey =  prpDFlight->Rkey;
		rpLine.DFlno = CString(prpDFlight->Flno); 
		rpLine.DTtyp = CString(prpDFlight->Ttyp);
		rpLine.DDes3 = CString(prpDFlight->Des3);
		rpLine.DAct = CString(prpDFlight->Act3);
		rpLine.DAlc2 = CString(prpDFlight->Alc2);
		rpLine.DAlc3 = CString(prpDFlight->Alc3);
		rpLine.DStod = CTime(prpDFlight->Stod);
		if(bgReportLocal) 
		{
			ogBasicData.UtcToLocal(rpLine.DStod);
			ogBasicData.UtcToLocal(prpDFlight->Stod);
		}
		if (rpLine.AAct.IsEmpty())
			rpLine.AAct = rpLine.DAct;
		rpLine.DPaxt = CString(prpDFlight->Paxt);
		rpLine.DPaxi = CString(prpDFlight->Paxi);

		olStr = CString(prpDFlight->Des3);

	}
	else
	{
		rpLine.DUrno =  0;
		rpLine.DRkey =  0;
		rpLine.DFlno = "";
		rpLine.DDate = ""; 
		rpLine.DTtyp = ""; 
		rpLine.DDes3 = "";
		rpLine.DAct = "";
		rpLine.DAlc2 = "";
		rpLine.DAlc3 = "";
		rpLine.DStod = -1;
		rpLine.DPaxt = "";
		rpLine.DPaxi = "";
	}

	omAPTT = ogBCD.GetField("APT", "APC3", olStr, "APTT");
//	ReportXXXCedaData olReportXXXCedaData("APC3", "APTT", "APT");
//	omAPTT = olReportXXXCedaData.ReadCleartext(olStr);

}



int ReportTimeFrameTableViewer::CreateLine(TIMEFRAMETABLE_LINEDATA &rpLine)
{
    int ilLineCount = omLines.GetSize();

    for (int ilLineno = ilLineCount; ilLineno > 0; ilLineno--)
        if (CompareFlight(&rpLine, &omLines[ilLineno-1]) >= 0)
            break;  // should be inserted after Lines[ilLineno-1]

    omLines.NewAt(ilLineno, rpLine);
	return ilLineno;
}


void ReportTimeFrameTableViewer::DeleteLine(int ipLineno)
{
	omLines.DeleteAt(ipLineno);
}



void ReportTimeFrameTableViewer::DeleteAll()
{
    while (omLines.GetSize() > 0)
        DeleteLine(0);
}


/////////////////////////////////////////////////////////////////////////////
// ReportTimeFrameTableViewer - display drawing routine


// UpdateDisplay: Load data selected by filter conditions to the display by using "omTable"
void ReportTimeFrameTableViewer::UpdateDisplay()
{

	CCS_TRY

	pomTable->ResetContent();

	TIMEFRAMETABLE_LINEDATA *prlLine;

	// Anzahl an Zeichen zur Berechnung der Spaltenbreite ermitteln
	int ilColumns = omShownColumns.GetSize();
	for (int i = 0; i < ilColumns; i++)
	{
		// Aufruf, damit omAnzCount Array gef�llt ist
		SetFieldLength(omShownColumns[i]);
	}
	DrawHeader();
  
	for (int ilLc = 0; ilLc < omColumn0.GetSize(); ilLc++)
	{
		int ilColumnNo = 0;
		prlLine = &omLines[0];
		CCSPtrArray<TABLE_COLUMN> olColList;
	
		imTableLine = ilLc;

		MakeColList(prlLine, olColList, omColumn0[ilLc]);

		pomTable->AddTextLine(olColList, (void*)(prlLine));
		olColList.DeleteAll();
	}

    pomTable->DisplayTable();

	CCS_CATCH_ALL

}


void ReportTimeFrameTableViewer::DrawHeader()
{

	CCS_TRY

	CCSPtrArray <TABLE_HEADER_COLUMN> omHeaderDataArray;

	pomTable->SetShowSelection(true);
	pomTable->ResetContent();

	TABLE_HEADER_COLUMN rlHeader;
	rlHeader.Font = &ogCourier_Bold_10;
	int i;

	// alle n Spalten (=omShownColumns.GetSize()) definieren
	int ilColums = omShownColumns.GetSize();
	int ilAnzChar = omAnzChar.GetSize();
	for (i = 0; i < ilColums; i++)
	{
		rlHeader.Alignment = COLALIGN_CENTER;
		// Anzahl an Textzeichen des Feldes der i-ten Spalte
		if (i <= ilAnzChar)
			rlHeader.AnzChar = omAnzChar[i];
		else
			break;
		// alten String l�schen, damit neuer String bei '0' anfaengt
		rlHeader.String = "";
		// Anzahl an Buchstaben 'W' setzen
		int ilAnzChar = omAnzChar[i];
		for (int j = 0; j < ilAnzChar; j++) 
			rlHeader.String += "W";
		// Text aus Konfiguration als Ueberschrift setzen
		rlHeader.Text = GetHeaderContent(omShownColumns[i]);
		// neue Spaltenheaderstruktur setzen
		omHeaderDataArray.New(rlHeader);
	}

	pomTable->SetHeaderFields(omHeaderDataArray);
	omHeaderDataArray.DeleteAll();

	pomTable->SetDefaultSeparator();

	CCS_CATCH_ALL

}


void ReportTimeFrameTableViewer::MakeColList(TIMEFRAMETABLE_LINEDATA *prlLine, 
											   CCSPtrArray<TABLE_COLUMN> &olColList,
											   CString opCurrentDCD)
{

	CCS_TRY

	TABLE_COLUMN rlColumnData;

	rlColumnData.VerticalSeparator = SEPA_NONE;
	rlColumnData.SeparatorType = SEPA_NONE;
	rlColumnData.Font = &ogCourier_Regular_9;

	// Spaltendefinitionen bestimmen
	for (int i = 0; i < omShownColumns.GetSize(); i++)
	{
		rlColumnData.Alignment = COLALIGN_LEFT;
		// Holen des Feldinhaltes der i-ten Spalte und n-ten Zeile
		rlColumnData.Text = GetFieldContent(prlLine, omShownColumns[i]);

		olColList.NewAt(olColList.GetSize(), rlColumnData);
	}

	CCS_CATCH_ALL

}




int ReportTimeFrameTableViewer::CompareFlight(TIMEFRAMETABLE_LINEDATA *prpLine1, TIMEFRAMETABLE_LINEDATA *prpLine2)
{
	int	ilCompareResult;
	CTime olTime1;
	CTime olTime2;


		ilCompareResult = (olTime1 == olTime2)? 0:
			(olTime1 > olTime2)? 1: -1;

		 return ilCompareResult;
}




//-----------------------------------------------------------------------------------------------
// Printing Routines
//-----------------------------------------------------------------------------------------------

void ReportTimeFrameTableViewer::GetHeader()
{
	
	CCS_TRY
		
	TABLE_HEADER_COLUMN *prlHeader[20];

	int i;
	int ilColomns = omShownColumns.GetSize();
	CSize olSize;
    pomPrint->omCdc.SelectObject(&ogCourier_Bold_10);

	for (i = 0; i < ilColomns; i++)
	{
		if (i == 0)
			olSize = pomPrint->omCdc.GetTextExtent(omColumn0[i]);
		else
			olSize = pomPrint->omCdc.GetTextExtent(GetHeaderContent(omShownColumns[i]));
		prlHeader[i] = new TABLE_HEADER_COLUMN;
		prlHeader[i]->Alignment = COLALIGN_CENTER;
		prlHeader[i]->Font = &ogCourier_Bold_10;
		prlHeader[i]->Length = olSize.cx;
		prlHeader[i]->Text = GetHeaderContent(omShownColumns[i]);
	}

	omPrintHeadHeaderArray.DeleteAll();
	for(i = 0; i < ilColomns; i++)
	{
		omPrintHeadHeaderArray.Add(prlHeader[i]);
	}

	CCS_CATCH_ALL

}



void ReportTimeFrameTableViewer::PrintTableView()
{

	CCS_TRY

	CString olFooter1,olFooter2;
	CString olTableName = GetString(1256);
	int ilOrientation = PRINT_LANDSCAPE;
	pomPrint = new CCSPrint(pomParentDlg,ilOrientation,45);
	pomPrint->imMaxLines = 38;

	if (pomPrint != NULL)
	{

		if (pomPrint->InitializePrinter(ilOrientation) == TRUE)
		{
			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
			pomPrint->imLineNo = pomPrint->imMaxLines + 1;
			DOCINFO	rlDocInfo;
			memset(&rlDocInfo, 0, sizeof(DOCINFO));
			rlDocInfo.cbSize = sizeof( DOCINFO );
			rlDocInfo.lpszDocName = olTableName;	
			pomPrint->omCdc.StartDoc( &rlDocInfo );
			pomPrint->imPageNo = 0;
			int ilLines = omLines.GetSize();  
			olFooter1.Format("%s %d, %s, %s",GetString(IDS_STRING329),ilLines,olTableName, 
				            (CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M")));
			olFooter1.Format("%s %d",rlDocInfo.lpszDocName, GetFlightCount());

			GetHeader();

			if (ilLines == 0)
				PrintTableHeader(); // Drucken leerer Tabelle
			for(int i = 0; i < omColumn0.GetSize()/*ilLines*/; i++ ) 
			{
				if(pomPrint->imLineNo >= pomPrint->imMaxLines)
				{
					if(pomPrint->imPageNo > 0)
					{
						olFooter2.Format(GetString(IDS_STRING1198),pomPrint->imPageNo);
						pomPrint->PrintUIFFooter(olFooter1,"",olFooter2);
						pomPrint->omCdc.EndPage();
					}
					PrintTableHeader();
				}
				imTableLine = i;
				if(pomPrint->imLineNo == (pomPrint->imMaxLines-1) || i == (ilLines-1))
				{
					PrintTableLine(&omLines[0],true, omColumn0[i]);
				}
				else
				{
					PrintTableLine(&omLines[0],false, omColumn0[i]);
				}
			}
			olFooter2.Format(GetString(IDS_STRING1198),pomPrint->imPageNo);
			pomPrint->PrintUIFFooter(olFooter1,"",olFooter2);
			pomPrint->omCdc.EndPage();
			pomPrint->omCdc.EndDoc();
		}

		omPrintHeadHeaderArray.DeleteAll();
		SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
		delete pomPrint;
		pomPrint = NULL;
	}

	CCS_CATCH_ALL

}


//-----------------------------------------------------------------------------------------------

bool ReportTimeFrameTableViewer::PrintTableHeader()
{
	pomPrint->omCdc.StartPage();
	pomPrint->imPageNo++;
	pomPrint->imLineNo = 0;
	double dgCCSPrintFactor = 2.7;
	char pclHeader[256];

	sprintf(pclHeader, GetString(IDS_STRING1654), pcmInfo);
	CString olTableName(pclHeader);

	pomPrint->PrintUIFHeader(omTableName,CString(pclHeader),pomPrint->imFirstLine-10);

	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;

	rlElement.Alignment   = PRINT_LEFT;
	rlElement.FrameLeft   = PRINT_NOFRAME;
	rlElement.FrameRight  = PRINT_NOFRAME;
	rlElement.FrameTop    = PRINT_NOFRAME;
	rlElement.FrameBottom = PRINT_FRAMEMEDIUM;
	rlElement.pFont       = &pomPrint->ogCourierNew_Bold_8;

	int ilSize = omPrintHeadHeaderArray.GetSize();

	for(int i=0;i<ilSize;i++)
	{
		rlElement.Length = (int)(omPrintHeadHeaderArray[i].Length * dgCCSPrintFactor);
		if(rlElement.Length < igCCSPrintMinLength)
			rlElement.Length += igCCSPrintMoreLength; 
		rlElement.Text   = omPrintHeadHeaderArray[i].Text;
		rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
	}
	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();


	return true;
}

//-----------------------------------------------------------------------------------------------

bool ReportTimeFrameTableViewer::PrintTableLine(TIMEFRAMETABLE_LINEDATA *prpLine,
												 bool bpLastLine, CString opCurrentAirline)
{

	CCS_TRY

	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;
	double dgCCSPrintFactor = 2.7;

    pomPrint->omCdc.SelectObject(&pomPrint->ogCourierNew_Regular_8);
	CSize olSize;
		
	rlElement.pFont = &pomPrint->ogCourierNew_Regular_8;

	int ilSize = omPrintHeadHeaderArray.GetSize();
	for(int i=0;i<ilSize;i++)
	{
		olSize = pomPrint->omCdc.GetTextExtent(omShownColumns[i]);

		rlElement.Length = (int)(omPrintHeadHeaderArray[i].Length * dgCCSPrintFactor);
		if(rlElement.Length < igCCSPrintMinLength)
			rlElement.Length += igCCSPrintMoreLength; 

		rlElement.Alignment  = PRINT_LEFT;
		rlElement.FrameTop   = PRINT_FRAMETHIN;
		if(bpLastLine == true)
		{
			rlElement.FrameBottom = PRINT_FRAMETHIN;
		}
		else
		{
			rlElement.FrameBottom = PRINT_FRAMETHIN;
		}
		
		rlElement.FrameLeft  = PRINT_NOFRAME;
		rlElement.FrameRight = PRINT_NOFRAME;
		rlElement.Text = GetFieldContent(NULL, omShownColumns[i]);

		rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
		
	}

	pomPrint->PrintLine(rlPrintLine);

	rlPrintLine.DeleteAll();
	
	CCS_CATCH_ALL

	return true;

}

//-----------------------------------------------------------------------------------------------


// Drucken in Datei
void ReportTimeFrameTableViewer::PrintPlanToFile(char *pcpDefPath)
{

	CCS_TRY

	ofstream of;
	int ilCountLines = omColumn0.GetSize();
	int ilCountRows  = omShownColumns.GetSize();

	char pclHeader[128];
	sprintf(pclHeader, GetString(IDS_STRING1654), pcmInfo);


	of.open( pcpDefPath, ios::out);
	of << CString(pclHeader) << " "
		<< CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M") << endl << endl;

	for (int i = 0; i < ilCountRows; i++)
	{
		of << GetHeaderContent(omShownColumns[i]) << "   ";
	}
	               
	of << endl << "-------------------------------------------------------------------------------------------------------------" 
	   << endl;


	// zeilen iterieren
	for(int ilLines = 0; ilLines < ilCountLines; ilLines++)
	{
		imTableLine = ilLines;

		of.setf(ios::left, ios::adjustfield);
		
		of 
		<< setw(strlen(omShownColumns[0]) + 9) << GetFieldContent(NULL, omShownColumns[0])
		<< setw(GetHeaderContent(omShownColumns[1]).GetLength() + 1) << GetFieldContent(NULL, omShownColumns[1])
		<< setw(GetHeaderContent(omShownColumns[2]).GetLength() + 3) << GetFieldContent(NULL, omShownColumns[2])
		<< setw(GetHeaderContent(omShownColumns[3]).GetLength() + 3) << GetFieldContent(NULL, omShownColumns[3])
		<< setw(GetHeaderContent(omShownColumns[4]).GetLength() + 3) << GetFieldContent(NULL, omShownColumns[4])
		<< setw(GetHeaderContent(omShownColumns[5]).GetLength() + 4) << GetFieldContent(NULL, omShownColumns[5])
		<< setw(GetHeaderContent(omShownColumns[6]).GetLength() + 3) << GetFieldContent(NULL, omShownColumns[6])
		<< setw(GetHeaderContent(omShownColumns[7]).GetLength()+2) << GetFieldContent(NULL, omShownColumns[7])
		<< setw(GetHeaderContent(omShownColumns[8]).GetLength()) << GetFieldContent(NULL, omShownColumns[8])
		// Zeile abschliessen
		<< endl;
	}

	// stream schliessen
	of.close();

	CCS_CATCH_ALL
	
}


int ReportTimeFrameTableViewer::GetFlightCount()
{

	return omCountColumn7[omCountColumn7.GetSize()-1] + omCountColumn8[omCountColumn8.GetSize()-1];

}


void ReportTimeFrameTableViewer::MakeCountListNDays(ROTATIONDLGFLIGHTDATA *prpAFlight, ROTATIONDLGFLIGHTDATA *prpDFlight)
{

	CCS_TRY

	// keine Daten? --> tschuess
	if (prpAFlight == NULL && prpDFlight == NULL)
		return;

	// Fl�ge aus Ankunftsteil
	if (prpAFlight != NULL)
	{
		int ilSize = omColumn0.GetSize() -1;
		for (int i = 0; i < ilSize; i++)
		{
			if (omColumn0[i] == prpAFlight->Stoa.Format("%d/%m/%Y"))
			{
				switch (omAPTT.GetBuffer(0)[0])
				{
					case INTL:
						omCountColumn1[i] = omCountColumn1[i] + 1;
					break;
					case DOM:
						omCountColumn3[i] = omCountColumn3[i] + 1;
					break;
					default:
						omCountColumn5[i] = omCountColumn5[i] + 1;
					break;
				}
				omCountColumn7[i] = omCountColumn1[i] + omCountColumn3[i] + omCountColumn5[i];
			}
		}
	}

	if (prpDFlight != NULL)
	{
		int ilSize = omColumn0.GetSize() - 1;
		// letzte Zeile = Total
		for (int i = 0; i < ilSize; i++)
		{
			if (omColumn0[i] == prpDFlight->Stod.Format("%d/%m/%Y"))
			{
				switch (omAPTT.GetBuffer(0)[0])
				{
					case INTL:
						omCountColumn2[i] = omCountColumn2[i] + 1;
					break;
					case DOM:
						omCountColumn4[i] = omCountColumn4[i] + 1;
					break;
					default:
						omCountColumn6[i] = omCountColumn6[i] + 1;
					break;
				}
			}
			omCountColumn8[i] = omCountColumn2[i] + omCountColumn4[i] + omCountColumn6[i];
		}
	}

	CCS_CATCH_ALL

}


void ReportTimeFrameTableViewer::MakeTotalLine()
{

	CCS_TRY

	int ilSize = omColumn0.GetSize() -1;
	for (int i = 0; i < ilSize; i++)
	{
		omCountColumn1[ilSize] += omCountColumn1[i];
		omCountColumn2[ilSize] += omCountColumn2[i];
		omCountColumn3[ilSize] += omCountColumn3[i];
		omCountColumn4[ilSize] += omCountColumn4[i];
		omCountColumn5[ilSize] += omCountColumn5[i];
		omCountColumn6[ilSize] += omCountColumn6[i];
		omCountColumn7[ilSize] += omCountColumn7[i];
		omCountColumn8[ilSize] += omCountColumn8[i];
	}

	CCS_CATCH_ALL

}


CString ReportTimeFrameTableViewer::GetFieldContent(TIMEFRAMETABLE_LINEDATA* prlLine,
										            CString opCurrentColumns)

{

	CString olFormat;
	int i = 0;

	if (opCurrentColumns == "Date")
	{
		return omColumn0[imTableLine];
	}
	if (opCurrentColumns == "IntlArr")
	{
		olFormat.Format("%d", omCountColumn1[imTableLine]);
		return olFormat;
	}
	if (opCurrentColumns == "IntlDep")
	{
		olFormat.Format("%d", omCountColumn2[imTableLine]);
		return olFormat;
	}
	if (opCurrentColumns == "DomArr")
	{		
		olFormat.Format("%d", omCountColumn3[imTableLine]);
		return olFormat;
	}
	if (opCurrentColumns == "DomDep")
	{
		olFormat.Format("%d", omCountColumn4[imTableLine]);
		return olFormat;
	}
	if (opCurrentColumns == "OtherArr")
	{
		olFormat.Format("%d", omCountColumn5[imTableLine]);
		return olFormat;
	}
	if (opCurrentColumns == "OtherDep")
	{
		olFormat.Format("%d", omCountColumn6[imTableLine]);
		return olFormat;
	}
	if (opCurrentColumns == "TotalArr")
	{
		olFormat.Format("%d", omCountColumn7[imTableLine]);
		return olFormat;
	}
	if (opCurrentColumns == "TotalDep")
	{
		olFormat.Format("%d", omCountColumn8[imTableLine]);
		return olFormat;
	}

	return "";

}


// Setzen der Spaltenbreite in Anzahl Zeichen
void ReportTimeFrameTableViewer::SetFieldLength(CString opCurrentColumns)
{

	CCS_TRY

	if (opCurrentColumns == "Date")
	{
		omAnzChar.Add(10);
		return;
	}
	if (opCurrentColumns == "IntlArr")
	{
		omAnzChar.Add(7);
		return;
	}
	if (opCurrentColumns == "IntlDep")
	{
		omAnzChar.Add(9);
		return;
	}
	if (opCurrentColumns == "DomArr")
	{
		omAnzChar.Add(7);
		return;
	}
	if (opCurrentColumns == "DomDep")
	{
		omAnzChar.Add(7);
		return;
	}
	if (opCurrentColumns == "OtherArr")
	{
		omAnzChar.Add(9);
		return;
	}
	if (opCurrentColumns == "OtherDep")
	{
		omAnzChar.Add(7);
		return;
	}
	if (opCurrentColumns == "TotalArr")
	{
		omAnzChar.Add(7);
		return;
	}
	if (opCurrentColumns == "TotalDep")
	{
		omAnzChar.Add(7);
		return;
	}

	// falls kein if passt, ein default-Wert, sonst Array zu klein
	omAnzChar.Add(11);

	CCS_CATCH_ALL

}


CString ReportTimeFrameTableViewer::GetHeaderContent(CString opCurrentColumns)
{

	CCS_TRY

	if (opCurrentColumns == "Date")
	{
		return GetString(IDS_STRING1626);
	}
	if (opCurrentColumns == "IntlArr")
	{
		return GetString(IDS_STRING1658);
	}
	if (opCurrentColumns == "IntlDep")
	{
		return GetString(IDS_STRING1659);
	}
	if (opCurrentColumns == "DomArr")
	{
		return GetString(IDS_STRING1660);
	}
	if (opCurrentColumns == "DomDep")
	{
		return GetString(IDS_STRING1661);
	}
	if (opCurrentColumns == "OtherArr")
	{
		return GetString(IDS_STRING1662);
	}
	if (opCurrentColumns == "OtherDep")
	{
		return GetString(IDS_STRING1663);
	}
	if (opCurrentColumns == "TotalArr")
	{
		return GetString(IDS_STRING1664);
	}
	if (opCurrentColumns == "TotalDep")
	{
		return GetString(IDS_STRING1665);
	}

	CCS_CATCH_ALL

	// wenn nix gefunden Leerstring zurueckgeben, sonst undefiniert
	return CString("");

}