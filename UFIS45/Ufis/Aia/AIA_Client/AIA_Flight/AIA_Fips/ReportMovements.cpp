// ReportMovements.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "fpms.h"
#include "ReportMovements.h"
#include "CcsGlobl.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld CReportMovementsDlg 


CReportMovementsDlg::CReportMovementsDlg(CWnd* pParent, CString opHeadline, CTime* poMinDate, CTime* poMaxDate, int *opSelect)
	: CDialog(CReportMovementsDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CReportMovementsDlg)
	m_List1 = -1;
	m_Datumbis = _T("");
	m_Datumvon = _T("");
	//}}AFX_DATA_INIT
	omHeadline = opHeadline;	//Dialogbox-�berschrift
	pomMinDate = poMinDate;
	pomMaxDate = poMaxDate;
	pimSelect = opSelect;

}


void CReportMovementsDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CReportMovementsDlg)
	DDX_Control(pDX, IDC_DATUMVON, m_CE_Datumvon);
	DDX_Control(pDX, IDC_DATUMBIS, m_CE_Datumbis);
	DDX_Control(pDX, IDC_LVG, m_CR_List1);
	DDX_Radio(pDX, IDC_LVG, m_List1);
	DDX_Text(pDX, IDC_DATUMBIS, m_Datumbis);
	DDX_Text(pDX, IDC_DATUMVON, m_Datumvon);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CReportMovementsDlg, CDialog)
	//{{AFX_MSG_MAP(CReportMovementsDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Behandlungsroutinen f�r Nachrichten CReportMovementsDlg 

void CReportMovementsDlg::OnCancel() 
{
	// TODO: Zus�tzlichen Bereinigungscode hier einf�gen
	
	CDialog::OnCancel();
}

void CReportMovementsDlg::OnOK() 
{
	UpdateData(TRUE);
	*pimSelect = m_List1;

	m_CE_Datumvon.GetWindowText(m_Datumvon);
	m_CE_Datumbis.GetWindowText(m_Datumbis);
	// ist nur ein Time Feld gef�llt, �bernimmt das andere dessen Inhalt
	if(m_Datumvon.IsEmpty())
		m_CE_Datumbis.GetWindowText(m_Datumvon);
	if(m_Datumbis.IsEmpty())
		m_CE_Datumvon.GetWindowText(m_Datumbis);

	*pomMinDate = DateStringToDate(m_Datumvon);
	*pomMaxDate = DateStringToDate(m_Datumbis); 

	if( (m_Datumbis.IsEmpty()) || (m_Datumvon.IsEmpty()) )
		MessageBox(GetString(IDS_STRING918), GetString(IMFK_REPORT), MB_OK);
	else
	{
		if((m_CE_Datumvon.GetStatus()) && (m_CE_Datumvon.GetStatus()))
			CDialog::OnOK();
		else
			MessageBox(GetString(IDS_STRING934), GetString(IMFK_REPORT), MB_OK);
	}
}

BOOL CReportMovementsDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	CDialog::SetWindowText(omHeadline);	//Dialogbox-�berschrift

//	m_List1 = 0;
	// mit Preset
	m_List1 = *pimSelect ;
	UpdateData(FALSE);

	m_CE_Datumvon.SetTypeToDate();
	m_CE_Datumbis.SetTypeToDate();
	m_CE_Datumvon.SetBKColor(YELLOW);
	m_CE_Datumbis.SetBKColor(YELLOW);

	m_Datumvon = pomMinDate->Format("%d.%m.%Y" ) ;
	m_CE_Datumvon.SetWindowText( m_Datumvon );

	m_Datumbis = pomMaxDate->Format("%d.%m.%Y" ) ;
	m_CE_Datumbis.SetWindowText( m_Datumbis );

	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX-Eigenschaftenseiten sollten FALSE zur�ckgeben
}
