#ifndef _DAILY_VIP_CEDA_DATA_H_
#define _DAILY_VIP_CEDA_DATA_H_
 

#include "CCSGlobl.h"
#include "CCSDefines.h"





struct DAILYVIPDATA
{
	char	Flnu[12];	// Referenz AFT.URNO <--> VIP.FLNU

	//DataCreated by this class
	int		IsChanged;

	DAILYVIPDATA(void)
	{ 
		memset(this,'\0',sizeof(*this));
	}

}; // end DAILYVIPDATA
	



/////////////////////////////////////////////////////////////////////////////
// Class declaration

/*@Doc:
  Reads and writes flight data from and to database. Stores flight data in memory 
  and provides methods for searching and retrieving flights from its internal list. 
  Does some data preparations like pre-formatting the flight number, storing flight 
  number and gate information for the corresponding inbound/outbound flight.

  Data of this class has to be updated by CEDA through broadcasts. This procedure 
  and the necessary methods will be described later with the description of 
  broadcast handling.

*/
class DailyVipCedaData : public CCSCedaData
{
// Attributes
public:
    CMapPtrToPtr omFlnuMap;
    CCSPtrArray<DAILYVIPDATA>omData;

	char pcmAftFieldList[2048];


// Operations
public:
    DailyVipCedaData();
	~DailyVipCedaData();
	


	void ClearAll(void);

	bool ReadAllVips(const CUIntArray& olUrnos);
	bool LookUpUrno(long lpUrno);

	bool AddToKeyMap(DAILYVIPDATA *prpFlight );
	bool DeleteFromKeyMap(DAILYVIPDATA *prpFlight );

private:
	CString omSelection;
	bool bmReadAllFlights;

	bool bmRotation ;
	CString omFtyps;
	CTime omFrom;
	CTime omTo;

};


#endif
