
 
#include "stdafx.h"
#include <afxwin.h>
#include "CCSGlobl.h"
#include "CCSCedaData.h"
#include "CedaCountData.h"
#include "BasicData.h"


 
CedaCountData::CedaCountData(const CString &ropTableName)
{
	// Create an array of CEDARECINFO for COUNTDATA
	BEGIN_CEDARECINFO(COUNTDATA,CountDataRecInfo)
 		CCS_FIELD_LONG(Urno,"URNO", "Eindeutige Datensatz-Nr.", 1)
 	END_CEDARECINFO //(COUNTDATA)

	// Copy the record structure
	for (int i=0; i< sizeof(CountDataRecInfo)/sizeof(CountDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&CountDataRecInfo[i],sizeof(CEDARECINFO));
		omRecInfo.Add(prpCedaRecInfo);
	} // end for

	// initialize field names
	strcpy(pcmTableName, ropTableName);
	strcpy(pcmFList,"URNO");
	pcmFieldList = pcmFList;


}; // end Constructor



CedaCountData::~CedaCountData()
{
	omRecInfo.DeleteAll();
}


int CedaCountData::GetCount(CString popSelection)
{
	if(popSelection.IsEmpty()) return -1;

	if (CedaAction("RT", popSelection.GetBuffer(0)))
		return GetBufferSize();

	return -1;
}


bool CedaCountData::SendBC(CString opTable, CString opSel, CString opFields, CString opData)
{
	return CedaAction("SBC", opTable.GetBuffer(0), opFields.GetBuffer(0), opSel.GetBuffer(0), "", opData.GetBuffer(0));
}

 