// AbfluegeTableViewer.cpp 
//
//	Liste aller Abfl�ge (BGS)

#include "stdafx.h"
#include "Report2TableViewer.h"
#include <iomanip.h>
#include <iostream.h>
#include <fstream.h>
#include "BasicData.h"
#include "CedaCcaData.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif



// AbfluegeTableViewer
//
int AbfluegeTableViewer::GetVia(CCSPtrArray<VIADATA> *opVias, ROTATIONDLGFLIGHTDATA *prpFlight)
{
	opVias->DeleteAll();

	if(prpFlight == NULL)
		return 0;
	if(strlen(prpFlight->Vial) == 0)
		return 0;

	CString olVias(prpFlight->Vial);
	CString olFids;
	CString olApc3;
	CString olApc4;

	VIADATA *prlVia;

	while(olVias.IsEmpty() != TRUE)
	{
			prlVia = new VIADATA;
			opVias->Add(prlVia);

			if(olVias.GetLength() < 120)
			{
				olVias += "                                                                                                                             ";
				olVias = olVias.Left(120);
			}


			olApc3 = olVias.Mid(1,3);
			olApc3.TrimLeft();
			sprintf(prlVia->Apc3, olApc3);

			if(olVias.GetLength() >= 120)
				olVias = olVias.Right(olVias.GetLength() - 120);

	}
	return opVias->GetSize();
}



AbfluegeTableViewer::AbfluegeTableViewer(CCSPtrArray<ROTATIONDLGFLIGHTDATA> *popData, char *pcpInfo)
{
	pomData = popData;
	pcmInfo = pcpInfo;

	bmIsFromSearch = FALSE;
    pomAbfluegeTable = NULL;
}

//-----------------------------------------------------------------------------------------------

AbfluegeTableViewer::~AbfluegeTableViewer()
{
	omDCins.DeleteAll();
	omDCinsSave.DeleteAll();
    DeleteAll();
	omHeaderDataArray.DeleteAll();
}

//-----------------------------------------------------------------------------------------------

void AbfluegeTableViewer::Attach(CCSTable *popTable)
{
    pomAbfluegeTable = popTable;
}

//-----------------------------------------------------------------------------------------------
int AbfluegeTableViewer::CompareAbfluege(ABFLUEGETABLE_LINEDATA *prpAbfluege1, ABFLUEGETABLE_LINEDATA *prpAbfluege2)
{
	// Sortierung nach TIFD
	// hbe 6/98

	return( 0 ) ;

	if( prpAbfluege1 != NULL && prpAbfluege2 != NULL ) 
	{
		return (prpAbfluege1->Tifd == prpAbfluege2->Tifd) ? 0 : (prpAbfluege1->Tifd > prpAbfluege2->Tifd ) ? 1 : -1;
	}
	else
	{
		// Ist dies m�glich !?
		VERIFY( false ) ;
		if( prpAbfluege1 == NULL && prpAbfluege2 == NULL ) return 0 ; 
		else if( prpAbfluege1 == NULL )
		{
			return -1 ; 
		}
		else {
			return 1 ;
		}
	}
}

//-----------------------------------------------------------------------------------------------
void AbfluegeTableViewer::ChangeViewTo(const char *pcpViewName)
{
    DeleteAll();    
    MakeLines();
	UpdateDisplay();
}

//-----------------------------------------------------------------------------------------------

void AbfluegeTableViewer::MakeLines() //ROTATIONDLGFLIGHTDATA *prpAbfluege)
{

	int ilCount = pomData->GetSize();	// f�r Schleife �ber alle Flights
	int ilLc = 0;
	CedaCcaData olCcaData;

	bool blTestListe = true ;	// true: CCA �ber Urnoliste ( => Turbo Selection: WHERE FLNU IN ..... !! 
	CStringArray olUrnoList ;
	CString olSelection ;
	CString olFlugUrnos("") ;


	if( blTestListe )	// nur wegen Performancevergleich
	{
		// kommagetrennten String der FlightUrnos aufbauen
		for ( ilLc = 0; ilLc < ilCount; ilLc++)
		{
			ROTATIONDLGFLIGHTDATA *prlAbfluegeData = &pomData->GetAt(ilLc);
			if(prlAbfluegeData->Urno > 0)
			{
				CString olNextUrno ;
				olNextUrno.Format("%ld", prlAbfluegeData->Urno ) ; 
				if( ! olFlugUrnos.IsEmpty() ) olFlugUrnos += "," ;
				olFlugUrnos += olNextUrno ; 
			}
		}

		// kommagetrennte FlightUrno-Liste ( je 100 Eintr�ge ) aufbauen
		ilCount = SplitItemList(olFlugUrnos, &olUrnoList, 100) ;
		for( ilLc = 0; ilLc < ilCount; ilLc++)
		{
			olSelection = CString("WHERE FLNU IN (");
			olSelection += olUrnoList[ilLc] + CString(")");

			char olCcaWhere[2048] ;	 // h�ngt von Eintr�ge (Para. 3) in SplitItemList( ..., 100 ) ab !!
			if( olSelection.GetLength() < 2048 ) 
			{
				strcpy( olCcaWhere, olSelection ) ;
				olCcaData.Read( olCcaWhere, ilLc ? false : true, false);
			}
			else
			{
				Beep( 500, 500 ) ;
				AfxMessageBox("MakeLines(): Speicher�berlauf !?" ) ;
				ASSERT( FALSE ) ;
			}
		}
	}
	

	ilCount = pomData->GetSize();
	for ( ilLc = 0; ilLc < ilCount; ilLc++)
	{

		ROTATIONDLGFLIGHTDATA *prlAbfluegeData = &pomData->GetAt(ilLc);

        // Update viewer data for this shift record
        ABFLUEGETABLE_LINEDATA rlAbfluege;
		rlAbfluege.Flno = prlAbfluegeData->Flno; 

		if(bgReportLocal)
			ogBasicData.UtcToLocal(prlAbfluegeData->Tifd);

		if((strcmp(rlAbfluege.Tifd.Format("%d.%m.%Y"), prlAbfluegeData->Tifd.Format("%d.%m.%Y"))))
		{
			rlAbfluege.Wro1 = "";
			rlAbfluege.Tifd = TIMENULL;
			rlAbfluege.Ttyp = "";
			rlAbfluege.Des3 = "";
			rlAbfluege.Via3 = "";
			rlAbfluege.Act = "";
			rlAbfluege.Gtd1 = "";
			rlAbfluege.Gtd2 = "";
			rlAbfluege.Pstd = "";
			rlAbfluege.Cicf = TIMENULL;
			rlAbfluege.Flno = ""; 
			CreateLine(&rlAbfluege);
			rlAbfluege.Flno = prlAbfluegeData->Tifd.Format("%d.%m.%Y"); 
			CreateLine(&rlAbfluege);
			rlAbfluege.Flno = ""; 
			CreateLine(&rlAbfluege);
		}

		rlAbfluege.Flno = prlAbfluegeData->Flno; 
		rlAbfluege.Tifd = prlAbfluegeData->Tifd; 
		rlAbfluege.Ttyp = prlAbfluegeData->Ttyp;
		rlAbfluege.Des3 = prlAbfluegeData->Des3;
		//rlAbfluege.Via3 = prlAbfluegeData->Via3;
		rlAbfluege.Act = prlAbfluegeData->Act3;
		rlAbfluege.Gtd1 = prlAbfluegeData->Gtd1;
		rlAbfluege.Gtd2 = prlAbfluegeData->Gtd2;
		rlAbfluege.Pstd = prlAbfluegeData->Pstd;
		rlAbfluege.Wro1 = prlAbfluegeData->Wro1;

		omDCins.DeleteAll();
		omDCinsSave.DeleteAll();
		rlAbfluege.Cicf = TIMENULL;



		// �ffnungszeiten CheckIn-Schalter
		// TODO: aktuell werden keine unterschiedliche Zeiten der Schalter eines Fluges ber�cksichtigt !
		if( ! blTestListe )
		{
			// langsame Methode vor 8/98: blTestListe sollte "true" sein !! 
			if(prlAbfluegeData->Urno > 0)
			{
				CedaCcaData olData;
				char pclWhere[128];
				sprintf(pclWhere,"WHERE FLNU = %d", prlAbfluegeData->Urno);
				olData.ReadSpecial(omDCins, pclWhere);
				for(int i = 0; i < omDCins.GetSize(); i++)
				{
					omDCinsSave.New(omDCins[i]);

					if((omDCins[i].Ckba) != TIMENULL)	// Belegung Checkin-Schalter aktueller Beginn
					{
						rlAbfluege.Cicf = omDCins[i].Ckba;
						if(bgReportLocal)
							ogBasicData.UtcToLocal(rlAbfluege.Cicf);
						break;
					}
					else if((omDCins[i].Ckbs) != TIMENULL)	// Belegung Checkin-Schalter geplanter Beginn
					{
						rlAbfluege.Cicf = omDCins[i].Ckbs;
						if(bgReportLocal)
							ogBasicData.UtcToLocal(rlAbfluege.Cicf);
						break;
					}
				}
			}
		}
		else
		{
			// neue (schnelle) Version
			if(prlAbfluegeData->Urno > 0)
			{
				int ilCcaSize = olCcaData.omData.GetSize() ;
				for(int i = 0; i < ilCcaSize; i++)
				{
					if( prlAbfluegeData->Urno == olCcaData.omData[i].Flnu )
					{
						// TRACE("Flug [%ld] in Satz [%d] [%ld]\n", prlAbfluegeData->Urno, i, olCcaData.omData[i].Flnu ) ;
						// TODO: fr�heste �ffnungszeit ermitteln und plus Kennung bei Abweichungen anzeigen !
						if((olCcaData.omData[i].Ckba) != TIMENULL)	// Belegung Checkin-Schalter aktueller Beginn
						{
							rlAbfluege.Cicf = olCcaData.omData[i].Ckba;
							if(bgReportLocal)
								ogBasicData.UtcToLocal(rlAbfluege.Cicf);
							break;
						}
						else if((olCcaData.omData[i].Ckbs) != TIMENULL)	// Belegung Checkin-Schalter geplanter Beginn
						{
							rlAbfluege.Cicf = olCcaData.omData[i].Ckbs;
							if(bgReportLocal)
								ogBasicData.UtcToLocal(rlAbfluege.Cicf);
							break;
						}

					}
				}

			}
		}

		CCSPtrArray<VIADATA> opVias;
		int ilViaCnt = GetVia(&opVias, prlAbfluegeData);

		if(opVias.GetSize() == 0)
			rlAbfluege.Via3 = "";
		else
		{
			rlAbfluege.Via3 =  opVias[0].Apc3;	// erster Via nach Homeairport = erste Zeile in Vial
		}
		opVias.DeleteAll();

		CreateLine(&rlAbfluege);

  }	// end for( iLc = ... Flight-Schleife

}

//-----------------------------------------------------------------------------------------------

void AbfluegeTableViewer::CreateLine(ABFLUEGETABLE_LINEDATA *prpAbfluege)
{
    int ilLineCount = omLines.GetSize();
    for (int ilLineno = ilLineCount; ilLineno > 0; ilLineno--)
        if (CompareAbfluege(prpAbfluege, &omLines[ilLineno-1]) >= 0)
	        break;							 // should be inserted after Lines[ilLineno-1]
	ABFLUEGETABLE_LINEDATA rlAbfluege;
	rlAbfluege = *prpAbfluege;
    omLines.NewAt(ilLineno, rlAbfluege);
}

//-----------------------------------------------------------------------------------------------
// UpdateDisplay: Load data selected by filter conditions to the display by using "omTable"

void AbfluegeTableViewer::UpdateDisplay()
{
	int ilLines = omLines.GetSize();
	const int ilColumns = 2;	//<===== Spaltenanzahl setzen (von 0 ab)
	omHeaderDataArray.DeleteAll();

	pomAbfluegeTable->SetShowSelection(TRUE);
	pomAbfluegeTable->ResetContent();

	TABLE_HEADER_COLUMN rlHeader;
	rlHeader.Alignment = COLALIGN_LEFT;
	rlHeader.Font = &ogCourier_Regular_10;
	rlHeader.Length = 90; 
	rlHeader.Text = GetString(IDS_STRING1078);
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 45;
	rlHeader.Text = GetString(IDS_STRING1090);
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 25;
	rlHeader.Text = GetString(IDS_STRING301);
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 38;
	rlHeader.Text = GetString(IDS_STRING1082);
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 30;
	rlHeader.Text = GetString(IDS_STRING299);
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 60;
	rlHeader.Text = GetString(IDS_STRING337);
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 30;
	rlHeader.Text = GetString(IDS_STRING1091);
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 30;
	rlHeader.Text = GetString(IDS_STRING1092);
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 38;
	rlHeader.Text = GetString(IDS_STRING308);
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 30;
	rlHeader.Text = GetString(IDS_STRING1093);
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 90;
	rlHeader.Text = GetString(IDS_STRING1094);
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);

	pomAbfluegeTable->SetHeaderFields(omHeaderDataArray);

	pomAbfluegeTable->SetDefaultSeparator();
	pomAbfluegeTable->SetTableEditable(FALSE);

	TABLE_COLUMN rlColumnData;
	rlColumnData.Font = &ogCourier_Regular_10;
	rlColumnData.BkColor = WHITE;
	rlColumnData.Alignment = COLALIGN_LEFT;
	
    for (int ilLineNo = 0; ilLineNo < ilLines; ilLineNo++)
	{
		CCSPtrArray<TABLE_COLUMN> olColList;

		// Tagesdatum in die Bildschirm-Tabelle
		/*
		if(ilLineNo > 1)
		{	// mind. eine Zeile gedruckt + neues Tagesdatum 
			if((strcmp(omLines[ilLineNo].Tifd.Format("%d.%m.%Y"), omLines[ilLineNo-1].Tifd.Format("%d.%m.%Y"))))
			{
				CCSPtrArray<TABLE_COLUMN> olColList2;
				rlColumnData.Font = &ogCourier_Bold_10;
				rlColumnData.Text = omLines[ilLineNo].Tifd.Format("%d.%m.%Y");
				rlColumnData.Font = &ogCourier_Regular_10;
				olColList2.NewAt(olColList2.GetSize(), rlColumnData);
				pomAbfluegeTable->AddTextLine(olColList2,(void*)NULL);
				olColList2.DeleteAll();
				ilLines++;
			}
		}

*/
		rlColumnData.Text = omLines[ilLineNo].Flno;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Tifd.Format("%H:%M");
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Ttyp;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Des3;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Via3;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Act;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Gtd1;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Gtd2;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Pstd;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Wro1;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Cicf.Format("%H:%M");
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		pomAbfluegeTable->AddTextLine(olColList,(void*)NULL);
		olColList.DeleteAll();
	}
	pomAbfluegeTable->DisplayTable();
}

//-----------------------------------------------------------------------------------------------

void AbfluegeTableViewer::DeleteLine(int ipLineno)
{
	omLines.DeleteAt(ipLineno);
}

//-----------------------------------------------------------------------------------------------

void AbfluegeTableViewer::DeleteAll()
{
    while (omLines.GetSize() > 0)
    DeleteLine(0);
}


//-----------------------------------------------------------------------------------------------
// Printing Routines
//-----------------------------------------------------------------------------------------------

void AbfluegeTableViewer::PrintTableView()
{
	CString olFooter1,olFooter2;
	char pclFooter[100];
	sprintf(pclFooter, GetString(IDS_STRING1095), (CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M")));
	omFooterName = pclFooter;
	omTableName = GetString(IDS_STRING1096);
	int ilOrientation = PRINT_LANDSCAPE;//  PRINT_PORTRAET  PRINT_LANDSCAPE
	pomPrint = new CCSPrint((CWnd*)this,ilOrientation,45);// FirstLine(P=200,L=200),LeftOffset(P=200,L=50)

	if (pomPrint != NULL)
	{
		// Leerzeilen einf�gen 8/98
		int ilAnzLLines = 0 ;
		if((ogCustomer == "HAJ") && (ogAppName == "FPMS")) ilAnzLLines = 0 ;
		
		if (pomPrint->InitializePrinter(ilOrientation) == TRUE)
		{
			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
			pomPrint->imLineNo = pomPrint->imMaxLines + 1;
			DOCINFO	rlDocInfo;
			memset(&rlDocInfo, 0, sizeof(DOCINFO));
			rlDocInfo.cbSize = sizeof( DOCINFO );
			rlDocInfo.lpszDocName = omTableName;	
			pomPrint->omCdc.StartDoc( &rlDocInfo );
			pomPrint->imPageNo = 0;
			int ilLines = omLines.GetSize();
			// hbe am 7/7/98: Zwischen�berschriften nicht mitz�hlen !
			olFooter1.Format(GetString(IDS_STRING1445),pomData->GetSize(), omFooterName);

			for(int i = 0; i < ilLines; i++ ) 
			{

				if (omLines[i].Tifd == TIMENULL)	// Leerzeilen und Tagesumbruchzeilen werden ausgefiltert
					continue;

				if(pomPrint->imLineNo >= pomPrint->imMaxLines)
				{
					if(pomPrint->imPageNo > 0)
					{
						olFooter2.Format(GetString(IDS_STRING1198),pomPrint->imPageNo);
						pomPrint->PrintUIFFooter(olFooter1,"",olFooter2);
						pomPrint->omCdc.EndPage();
					}
					PrintTableHeader(omLines[i].Tifd);
				}
				else if(i > 1)
				{	// mind. eine Zeile gedruckt + neues Tagesdatum -> neue Seite
					if((strcmp(omLines[i].Tifd.Format("%d.%m.%Y"), omLines[i-1].Tifd.Format("%d.%m.%Y"))))
					{
						olFooter2.Format(GetString(IDS_STRING1198),pomPrint->imPageNo);
						pomPrint->PrintUIFFooter(olFooter1,"",olFooter2);
						pomPrint->omCdc.EndPage();
						PrintTableHeader(omLines[i].Tifd);
					}
				}


				// if(pomPrint->imLineNo == (pomPrint->imMaxLines-1) || i == (ilLines-1))
				if(pomPrint->imLineNo == (pomPrint->imMaxLines-(ilAnzLLines+1)) || i == (ilLines-1))
				{
					if( ilAnzLLines )
					{
						ABFLUEGETABLE_LINEDATA prLLine ;
						PrintTableLine(&omLines[i],true);
						for( int index=0 ; index < ilAnzLLines-1 ; index++ )
							PrintTableLine(&prLLine,false);
						PrintTableLine(&prLLine,true);
					}
					else
					{
						PrintTableLine(&omLines[i],true);
					}
				}
				else
				{
					PrintTableLine(&omLines[i],false);
				}
			}
			olFooter2.Format(GetString(IDS_STRING1198),pomPrint->imPageNo);
			pomPrint->PrintUIFFooter(olFooter1,"",olFooter2);
			pomPrint->omCdc.EndPage();
			pomPrint->omCdc.EndDoc();
		}
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
	delete pomPrint;

	pomPrint = NULL;
	}
}

//-----------------------------------------------------------------------------------------------

bool AbfluegeTableViewer::PrintTableHeader(CTime popTifd)
{
	pomPrint->omCdc.StartPage();
	pomPrint->imPageNo++;
	pomPrint->imLineNo = 0;
	double dgCCSPrintFactor = 2.7 ;

	char pclHeader[100];
	//sprintf(pclHeader, "%s  vom %s", omTableName, pcmInfo);

	sprintf(pclHeader, GetString(IDS_STRING1089), popTifd.Format("%d.%m.%Y"));
	
	//pomPrint->PrintUIFHeader(omTableName,CString(CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M")),pomPrint->imFirstLine-10);
	pomPrint->PrintUIFHeader("",CString(pclHeader),pomPrint->imFirstLine-10);

	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;

	rlElement.Alignment   = PRINT_LEFT;
	rlElement.FrameLeft   = PRINT_NOFRAME;
	rlElement.FrameRight  = PRINT_NOFRAME;
	rlElement.FrameTop    = PRINT_NOFRAME;
	rlElement.FrameBottom = PRINT_FRAMEMEDIUM;
	//rlElement.pFont       = &pomPrint->ogCourierNew_Bold_8;
	rlElement.pFont       = &pomPrint->ogCourierNew_Bold_12;

	int ilSize = omHeaderDataArray.GetSize();
	for(int i=0;i<ilSize;i++)
	{
		rlElement.Length = (int)(omHeaderDataArray[i].Length*dgCCSPrintFactor);
		if(rlElement.Length < igCCSPrintMinLength)rlElement.Length+=igCCSPrintMoreLength; 
		rlElement.Text   = omHeaderDataArray[i].Text;
		rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
	}
	pomPrint->PrintLine(rlPrintLine);

	// ganz oben eine Leerzeile
	rlElement.Length = 0;
	pomPrint->PrintLine(rlPrintLine);

	rlPrintLine.DeleteAll();


	return true;
}

//-----------------------------------------------------------------------------------------------

bool AbfluegeTableViewer::PrintTableLine(ABFLUEGETABLE_LINEDATA *prpLine,bool bpLastLine)
{
	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;
	double dgCCSPrintFactor = 2.7 ;
	//rlElement.pFont      = &pomPrint->ogCourierNew_Regular_8;
	rlElement.pFont      = &pomPrint->ogCourierNew_Regular_12;

	int ilSize = omHeaderDataArray.GetSize();


	for(int i=0;i<ilSize;i++)
	{
		
		rlElement.Length = (int)(omHeaderDataArray[i].Length*dgCCSPrintFactor);
		if(rlElement.Length < igCCSPrintMinLength)rlElement.Length+=igCCSPrintMoreLength; 
		rlElement.Alignment  = PRINT_LEFT;
		//rlElement.FrameTop   = PRINT_FRAMETHIN;
		if(bpLastLine == true)
		{
			//rlElement.FrameBottom = PRINT_FRAMETHIN;
		}
		else
		{
			rlElement.FrameBottom = PRINT_NOFRAME;
		}
		rlElement.FrameLeft  = PRINT_NOFRAME;
		rlElement.FrameRight = PRINT_NOFRAME;
		switch(i)
		{
		case 0:
			{
				rlElement.Text		= prpLine->Flno;
			}
			break;
		case 1:
			{
				rlElement.Text		= prpLine->Tifd.Format("%H:%M");
			}
			break;
		case 2:
			{
				rlElement.Text		= prpLine->Ttyp;
			}
			break;
		case 3:
			{
				rlElement.Text		= prpLine->Des3;
			}
			break;
		case 4:
			{
				rlElement.Text		= prpLine->Via3;
			}
			break;
		case 5:
			{
				rlElement.Text		= prpLine->Act;
			}
			break;
		case 6:
			{
				rlElement.Text		= prpLine->Gtd1;
			}
			break;
		case 7:
			{
				rlElement.Text		= prpLine->Gtd2;
			}
			break;
		case 8:
			{
				rlElement.Text		= prpLine->Pstd;
			}
			break;
		case 9:
			{
				rlElement.Text		= prpLine->Wro1;
			}
			break;
		case 10:
			{
				rlElement.Text		= prpLine->Cicf.Format("%H:%M");

			}

		}

		rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

	}
	// letzte leere Spalte
	rlElement.Length = 1200;
	if(rlElement.Length < igCCSPrintMinLength)rlElement.Length+=igCCSPrintMoreLength; 
		rlElement.Alignment  = PRINT_LEFT;
	rlElement.FrameLeft  = PRINT_NOFRAME;
	rlElement.FrameRight = PRINT_NOFRAME;
	rlElement.Text		 = " ";
	rlElement.FrameBottom   = PRINT_FRAMETHIN;
	rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

	pomPrint->PrintLine(rlPrintLine);

	// doppelter Zeilenabstand
	rlElement.Alignment  = PRINT_RIGHT;
	rlElement.Length = 0;
	pomPrint->PrintLine(rlPrintLine);

	rlPrintLine.DeleteAll();
	return true;
}


//-----------------------------------------------------------------------------------------------

// Drucken in Datei
void AbfluegeTableViewer::PrintPlanToFile(char *pcpDefPath)
{
		ofstream of;
		of.open( pcpDefPath, ios::out);
		of << GetString(IDS_STRING921) << "  " 
			<< CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M") << endl << endl;

		of  << GetString(IDS_STRING1078) << "    "
			<< GetString(IDS_STRING1090) << "  "
			<< GetString(IDS_STRING301) << "  "  
			<< GetString(IDS_STRING1082) << "  "
			<< GetString(IDS_STRING299) << "   "
			<< GetString(IDS_STRING337) << "    " 
			<< GetString(IDS_STRING335) << "  "
			<< GetString(IDS_STRING336) << "  "
			<< GetString(IDS_STRING308) << "  "
			<< GetString(IDS_STRING1093) << "   "
			<< GetString(IDS_STRING1097)  
			<< endl;



		of << "------------------------------------------------------------------------------" << endl;
		int ilCount = omLines.GetSize();
		for(int i = 0; i < ilCount; i++)
		{
			ABFLUEGETABLE_LINEDATA rlD = omLines[i];

			of.setf(ios::left, ios::adjustfield);
			of   << setw(11) << rlD.Flno  
				 << setw(6) << rlD.Tifd.Format("%H%M")
				 << setw(4) << rlD.Ttyp
				 << setw(6) << rlD.Des3
				 << setw(6) << rlD.Via3 
				 //<< setw(7) << rlD.Act5
				 << setw(7) << rlD.Act
				 << setw(7) << rlD.Gtd1
				 << setw(7) << rlD.Gtd2
				 << setw(6) << rlD.Pstd
				 << setw(6) << rlD.Wro1
				 << setw(11) << rlD.Cicf.Format("%H%M") << endl;
		}
		of.close();
}





//-----------------------------------------------------------------------------------------------
