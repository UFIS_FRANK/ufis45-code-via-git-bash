// AllocateCcaParameter.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "fpms.h"
#include "AllocateCcaParameter.h"
#include "ccsglobl.h"
#include "resrc1.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld AllocateCcaParameter 


AllocateCcaParameter::AllocateCcaParameter(CWnd* pParent /*=NULL*/)
	: CDialog(AllocateCcaParameter::IDD, pParent)
{
	//{{AFX_DATA_INIT(AllocateCcaParameter)
		// HINWEIS: Der Klassen-Assistent f�gt hier Elementinitialisierung ein
	//}}AFX_DATA_INIT
}


void AllocateCcaParameter::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(AllocateCcaParameter)
		// HINWEIS: Der Klassen-Assistent f�gt hier DDX- und DDV-Aufrufe ein
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(AllocateCcaParameter, CDialog)
	//{{AFX_MSG_MAP(AllocateCcaParameter)
	ON_BN_CLICKED(IDC_RESET, OnReset)
	ON_BN_CLICKED(IDC_CANCEL, OnCancel)
	ON_BN_CLICKED(IDC_ALL, OnAll)
	ON_BN_CLICKED(IDC_NOTALLOCATED, OnNotallocated)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Behandlungsroutinen f�r Nachrichten AllocateCcaParameter 
BOOL AllocateCcaParameter::OnInitDialog() 
{
	CDialog::OnInitDialog();
	CPoint point;
	::GetCursorPos(&point);
	this->SetWindowPos(&wndTop, point.x, point.y,0,0, SWP_SHOWWINDOW | SWP_NOSIZE);// | SWP_NOMOVE );

	//UpdateData(FALSE);
	// TODO: Add extra initialization here
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void AllocateCcaParameter::OnAll() 
{
	if(MessageBox( GetString(IDS_STRING1772), GetString(IDS_STRING1811), MB_YESNO ) != IDYES)
		return;

	EndDialog(0);	
}

void AllocateCcaParameter::OnNotallocated() 
{
	if(MessageBox( GetString(IDS_STRING1772), GetString(IDS_STRING1811), MB_YESNO ) != IDYES)
		return;
	
	EndDialog(1);	
}

void AllocateCcaParameter::OnReset() 
{
	if(MessageBox( GetString(IDS_STRING1772), GetString(IDS_STRING1812), MB_YESNO ) != IDYES)
		return;

	EndDialog(2);	
}

void AllocateCcaParameter::OnCancel() 
{
	EndDialog(-1);	
}

