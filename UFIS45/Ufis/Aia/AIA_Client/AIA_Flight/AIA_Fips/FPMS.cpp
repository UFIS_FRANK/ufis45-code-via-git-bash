// FPMS.cpp : Defines the class behaviors for the application.
//

#include "stdafx.h" 
#include "FPMS.h"
#include "ufis.h"


#include "CCSTime.h"
#include "ButtonListDlg.h"
#include "LoginDlg.h"
#include "BasicData.h"
#include "BackGround.h"
#include "SeasonCedaFlightData.h"
#include "RotationCedaFlightData.h"
#include "CCSClientWnd.h"
#include "PrivList.h"
#include "RegisterDlg.h"
#include "CedaCfgData.h"
#include "CViewer.h"
#include "CedaBasicData.h"
#include "CedaInfData.h"
#include "CCSGlobl.h"

#include "CedaGrmData.h"
#include "DataSet.h"
#include "Konflikte.h"
#include "SpotAllocation.h"
#include "CedaResGroupData.h"
#include "RotationDlg.h"
#include "SeasonDlg.h"
#include "RotGroundDlg.h"
#include "WoResTableDlg.h"
#include "CedaAptLocalUtc.h"

#include "UFISAmSink.h"
#include "ClntTypes.h"

#include "resrc1.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif



COLORREF CCSClientWnd::lmBkColor = lgBkColor;
COLORREF CCSClientWnd::lmTextColor = lgTextColor;
COLORREF CCSClientWnd::lmHilightColor = lgHilightColor;

/////////////////////////////////////////////////////////////////////////////
// CFPMSApp

BEGIN_MESSAGE_MAP(CFPMSApp, CWinApp)
	//{{AFX_MSG_MAP(CFPMSApp)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG
	ON_COMMAND(ID_HELP, CWinApp::OnHelp)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CFPMSApp construction

CFPMSApp::CFPMSApp()
{
	// TODO: add construction code here,
	// Place all significant initialization in InitInstance
	pcmLKey[0] = '\0';	
	_CrtSetDbgFlag( _CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);
}

CFPMSApp::~CFPMSApp()
{
	TRACE("CFPMSApp::~CFPMSApp\n");

	if (pogBCLog)
	{
		(*pogBCLog) << "\n\nFIPS: Broadcast-Log end " << CTime::GetCurrentTime().Format("%d.%m.%y %H:%M:%S") << "\n";

		pogBCLog->close();
		delete pogBCLog;
	}

	if (pogBackGround)
	{
		pogBackGround->DestroyWindow();
		delete pogBackGround;
	}

	DeleteBrushes();
	ogMapLblUrnoToKonftype.RemoveAll();

	if (bgDebug)
	{
		CTime olCurrTime = CTime::GetCurrentTime();
		of_debug << endl << "FIPS TERMINATED! (" << olCurrTime.Format("%d.%m.%Y %H:%M:%S") << ")" << endl;
		of_debug.close();
	}
	//ExitProcess(0);

}

 
int CFPMSApp::ExitInstance() 
{

	if (pogButtonList != NULL)
		pogButtonList->SendMessage(WM_CLOSE, 0, 0);

	AatHelp::ExitApp();
	ogBcHandle.ExitApp();

   return CWinApp::ExitInstance();
}



/////////////////////////////////////////////////////////////////////////////
// The one and only CFPMSApp object

CFPMSApp theApp;



static void InitSeasonFlightTableDefaultView()
{
   	CViewer olViewer;
	CStringArray olPossibleFilters;
	CStringArray olGeoFilter;

	// Read the default value from the database in the server
	//TRACE("InitPrePlanTableDefaultView\n");

	olPossibleFilters.RemoveAll();
	olPossibleFilters.Add("SPECFILTER");
	olPossibleFilters.Add("UNIFILTER");
	//olPossibleFilters.Add("FLIGHTSEARCH");
	// Initialize the default view for the gate diagram
	olViewer.SetViewerKey("SAISONFLIGHT");
	CString olLastView = olViewer.SelectView();	// remember the current view
	olViewer.CreateView("<Default>", olPossibleFilters);
	olViewer.SelectView("<Default>");
	//if (olLastView != "")
	//	olViewer.SelectView(olLastView);	// restore the previously selected view




	olViewer.SetViewerKey("ROTATIONFLIGHT");
	olLastView = olViewer.SelectView();	// remember the current view
	olViewer.CreateView("<Default>", olPossibleFilters);
	olViewer.SelectView("<Default>");
	//if (olLastView != "")
	//	olViewer.SelectView(olLastView);	// restore the previously selected view

	olViewer.SetViewerKey("DAILYFLIGHT");
	olLastView = olViewer.SelectView();	// remember the current view
	olViewer.CreateView("<Default>", olPossibleFilters);
	olViewer.SelectView("<Default>");

	olPossibleFilters.RemoveAll();
	olPossibleFilters.Add("DUMMY");
	olPossibleFilters.Add("GEOMETRIE");
	olViewer.SetViewerKey("FLIGHTDIA");
	olLastView = olViewer.SelectView();	// remember the current view
	olViewer.CreateView("<Default>", olPossibleFilters);
	olViewer.SelectView("<Default>");
	olViewer.SetFilter("GEOMETRIE", olGeoFilter);
	//if (olLastView != "")
	//	olViewer.SelectView(olLastView);	// restore the previously selected view

	/*
	olPossibleFilters.RemoveAll();
	olPossibleFilters.Add("DUMMY");
	olPossibleFilters.Add("GEOMETRIE");
	olViewer.SetViewerKey("GATPOSDIA");
	olLastView = olViewer.SelectView();	// remember the current view
	olViewer.CreateView("<Default>", olPossibleFilters);
	olViewer.SelectView("<Default>");
	olViewer.SetFilter("GEOMETRIE", olGeoFilter);
	if (olLastView != "")
		olViewer.SelectView(olLastView);	// restore the previously selected view

	olPossibleFilters.RemoveAll();
	olPossibleFilters.Add("DUMMY");
	olPossibleFilters.Add("GEOMETRIE");
	olViewer.SetViewerKey("BLTDIA");
	olLastView = olViewer.SelectView();	// remember the current view
	olViewer.CreateView("<Default>", olPossibleFilters);
	olViewer.SelectView("<Default>");
	olViewer.SetFilter("GEOMETRIE", olGeoFilter);
	if (olLastView != "")
		olViewer.SelectView(olLastView);	// restore the previously selected view

	olPossibleFilters.RemoveAll();
	olPossibleFilters.Add("DUMMY");
	olPossibleFilters.Add("GEOMETRIE");
	olViewer.SetViewerKey("GATDIA");
	olLastView = olViewer.SelectView();	// remember the current view
	olViewer.CreateView("<Default>", olPossibleFilters);
	olViewer.SelectView("<Default>");
	olViewer.SetFilter("GEOMETRIE", olGeoFilter);
	if (olLastView != "")
		olViewer.SelectView(olLastView);	// restore the previously selected view


	olPossibleFilters.RemoveAll();
	olPossibleFilters.Add("DUMMY");
	olPossibleFilters.Add("GEOMETRIE");
	olViewer.SetViewerKey("WRODIA");
	olLastView = olViewer.SelectView();	// remember the current view
	olViewer.CreateView("<Default>", olPossibleFilters);
	olViewer.SelectView("<Default>");
	olViewer.SetFilter("GEOMETRIE", olGeoFilter);
	if (olLastView != "")
		olViewer.SelectView(olLastView);	// restore the previously selected view


  */
	olPossibleFilters.RemoveAll();
	olPossibleFilters.Add("DUMMY");
	olPossibleFilters.Add("GEOMETRIE");
	olViewer.SetViewerKey("POSDIA");
	olLastView = olViewer.SelectView();	// remember the current view
	olViewer.CreateView("<Default>", olPossibleFilters);
	olViewer.SelectView("<Default>");
	olViewer.SetFilter("GEOMETRIE", olGeoFilter);
	if (olLastView != "")
		olViewer.SelectView(olLastView);	// restore the previously selected view


	olPossibleFilters.RemoveAll();
	olPossibleFilters.Add("DUMMY");
	olPossibleFilters.Add("GEOMETRIE");
	olViewer.SetViewerKey("CCADIA");
	olLastView = olViewer.SelectView();	// remember the current view
	olViewer.CreateView("<Default>", olPossibleFilters);
	olViewer.SelectView("<Default>");
	olViewer.SetFilter("GEOMETRIE", olGeoFilter);
	if (olLastView != "")
		olViewer.SelectView(olLastView);	// restore the previously selected view


}






/////////////////////////////////////////////////////////////////////////////
// CFPMSApp initialization

BOOL CFPMSApp::InitInstance()
{

	CCSCedaData::bmVersCheck = true;

	// Initialize OLE libraries
	if (!AfxOleInit())
	{
		AfxMessageBox(IDP_OLE_INIT_FAILED);
		return FALSE;
	}

	AfxEnableControlContainer();

	// Standard initialization
	// If you are not using these features and wish to reduce the size
	//  of your final executable, you should remove from the following
	//  the specific initialization routines you do not need.

	#ifdef _AFXDLL
		Enable3dControls();			// Call this when using MFC in a shared DLL
	#else
		Enable3dControlsStatic();	// Call this when linking to MFC statically
	#endif

	// Parse the command line to see if launched as OLE server
	if (RunEmbedded() || RunAutomated())
	{
		// Register all OLE server (factories) as running.  This enables the
		//  OLE libraries to create objects from other applications.
		COleTemplateServer::RegisterAll();
	}
	else
	{
		// When a server application is launched stand-alone, it is a good idea
		//  to update the system registry in case it has been damaged.
		COleObjectFactory::UpdateRegistryAll();
	}


	AfxGetApp()->LoadIcon(IDI_FPMS);


  

	/////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////
	// CCS-Initialization
	/////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////

	Customize();

	CTime oltmptime = CTime::GetCurrentTime();
	CString oltmpstr = oltmptime.Format("Datum: %d.%m.%Y");
	of_catch.open("CritErr.txt", ios::app);
	of_catch << oltmpstr.GetBuffer(0) << endl;
	of_catch << "=================" << endl;



	/////////////////////////////////////////////////////////////////////////////
	// Standard CCS-Initialization
	/////////////////////////////////////////////////////////////////////////////

	
	ogLog.SetAppName(ogAppName);
	ogCommHandler.SetAppName(ogAppName);


	// Standard CCS-Fonts and Brushes initialization (see ccsglobl.h)
    InitFont();
	CreateBrushes();
	
	// CCS-Desktop background with an bitmap (logo or copyright etc.. 
	// loaded in the methode CBackGround::OnPaint)
//	pogBackGround = new CBackGround;
//	pogBackGround->Create(NULL, IDD_BACKGROUND);


	ogBcHandle.SetCloMessage(GetString(IDS_STRING1391));

    if (ogCommHandler.Initialize() != true)  // connection error?
    {
        AfxMessageBox(CString("CedaCom:\n") + ogCommHandler.LastError());
   		ogCommHandler.CleanUpCom();	
		ExitProcess(0);
   		return FALSE;
	}




	::UfisDllAdmin("TRACE", "ON", "FATAL");


	// INIT Tablenames and Homeairport
	char pclConfigPath[256];
	char pclUser[256];
	char pclPassword[256];
	char pclDebug[256];

    if (getenv("CEDA") == NULL)
        strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
    else
        strcpy(pclConfigPath, getenv("CEDA"));

	char pclSeasonLocal[64];
	char pclGatPosLocal[64];
	char pclDailyLocal[64];
	char pclBCLogFile[200];
	char pclDailyWithAlt[10];
	char pclCreateDaily_OrgAtdBlue[10];
	char pclFidsRemark[3];
	char pclOffRelease[10];
	char pclAirport4LC[64];
	char pclPosBuffer[11];
	char pclViewEditFilter[64];
	char pclViewEditSort[64];
	char pclShowOnGround[64];
	char pclViewOnGroundTimelimit[64];
	char pclCheckAllConflicts[64];
	char pclRealLocal[64];

	char pclBltHeight[64];
	char pclGatHeight[64];
	char pclPosHeight[64];
	char pclWroHeight[64];
	char pclCcaHeight[64];
	char pclOverlapHeight[64];

	char pclFwrPos[64];
	char pclFwrGat[64];
	char pclFwrBlt[64];
	char pclFwrWro[64];
	char pclFwrCic[64];

	char pclConflictsForAirb[64];
	char pclConflictsForDataChange[64];
	char pclLblConfACTchange[64];

	char pclTimerTimelineMinutes[64];

    GetPrivateProfileString("GLOBAL", "HOMEAIRPORT", "HAJ", pcgHome, sizeof pcgHome, pclConfigPath);
    GetPrivateProfileString("GLOBAL", "TABLEEXTENSION", "HAJ", pcgTableExt, sizeof pcgTableExt, pclConfigPath);
    GetPrivateProfileString(ogAppName, "SEASONLOCAL", "FALSE", pclSeasonLocal, sizeof pclSeasonLocal, pclConfigPath);
    GetPrivateProfileString(ogAppName, "DAILYLOCAL", "FALSE", pclDailyLocal, sizeof pclDailyLocal, pclConfigPath);
    GetPrivateProfileString(ogAppName, "GATPOSLOCAL", "FALSE", pclGatPosLocal, sizeof pclGatPosLocal, pclConfigPath);
	GetPrivateProfileString(ogAppName, "BC-LOGFILE", "", pclBCLogFile, sizeof pclBCLogFile, pclConfigPath);
    GetPrivateProfileString(ogAppName, "DAILY_WITHALTKEY", "FALSE", pclDailyWithAlt, sizeof pclDailyWithAlt, pclConfigPath);
	GetPrivateProfileString(ogAppName, "DAILY_ORGATDBLUE", "FALSE", pclCreateDaily_OrgAtdBlue, sizeof pclCreateDaily_OrgAtdBlue, pclConfigPath);
	GetPrivateProfileString(ogAppName, "FIDS_REMARK", "2", pclFidsRemark, sizeof pclFidsRemark, pclConfigPath);
    GetPrivateProfileString(ogAppName, "DEBUG", "DEFAULT", pclDebug, sizeof pclDebug, pclConfigPath);
    GetPrivateProfileString(ogAppName, "OFFLINE_RELEASE", "TRUE", pclOffRelease, sizeof pclOffRelease, pclConfigPath);
    GetPrivateProfileString(ogAppName, "LKEY", "", pcmLKey, sizeof pcmLKey, pclConfigPath);
    GetPrivateProfileString(ogAppName, "AIRPORT4LC", "TRUE", pclAirport4LC, sizeof pclAirport4LC, pclConfigPath);
    GetPrivateProfileString(ogAppName, "POSBUFFER", "0", pclPosBuffer, sizeof pclPosBuffer, pclConfigPath);

    GetPrivateProfileString(ogAppName, "EDIT_VIEW_FILTER", "0", pclViewEditFilter, sizeof pclViewEditFilter, pclConfigPath);
    GetPrivateProfileString(ogAppName, "EDIT_VIEW_SORT"  , "0", pclViewEditSort, sizeof pclViewEditSort, pclConfigPath);

    GetPrivateProfileString(ogAppName, "SHOW_ON_GROUND"  , "TRUE", pclShowOnGround, sizeof pclShowOnGround, pclConfigPath);
    GetPrivateProfileString(ogAppName, "ROT_GROUNDTIME_LIMIT"  , "720", pclViewOnGroundTimelimit, sizeof pclViewOnGroundTimelimit, pclConfigPath);
//aiaaia 
    GetPrivateProfileString(ogAppName, "CHECK_ALL_CONFLICTS"  , "FALSE", pclCheckAllConflicts, sizeof pclCheckAllConflicts, pclConfigPath);
//aiaaia

    GetPrivateProfileString(ogAppName, "SHOW_REAL_LOCAL_TIMES", "FALSE", pclRealLocal, sizeof pclRealLocal, pclConfigPath);

    GetPrivateProfileString("GLOBAL", "USER", "DEFAULT", pclUser, sizeof pclUser, pclConfigPath);
    GetPrivateProfileString("GLOBAL", "PASSWORD", "DEFAULT", pclPassword, sizeof pclPassword, pclConfigPath);

    GetPrivateProfileString(ogAppName, "SHOW_FULL_BELT_HEIGHT ", "FALSE", pclBltHeight, sizeof pclBltHeight, pclConfigPath);
    GetPrivateProfileString(ogAppName, "SHOW_FULL_GATE_HEIGHT ", "FALSE", pclGatHeight, sizeof pclGatHeight, pclConfigPath);
    GetPrivateProfileString(ogAppName, "SHOW_FULL_POSITION_HEIGHT ", "FALSE", pclPosHeight, sizeof pclPosHeight, pclConfigPath);
    GetPrivateProfileString(ogAppName, "SHOW_FULL_LOUNGE_HEIGHT ", "FALSE", pclWroHeight, sizeof pclWroHeight, pclConfigPath);
    GetPrivateProfileString(ogAppName, "SHOW_FULL_CKI_HEIGHT ", "FALSE", pclCcaHeight, sizeof pclCcaHeight, pclConfigPath);
    GetPrivateProfileString(ogAppName, "DISTANCE_OF_OVERLAPHEIGHT "  , "50", pclOverlapHeight, sizeof pclOverlapHeight, pclConfigPath);

    GetPrivateProfileString(ogAppName, "POS_FWR_BUTTON_COLOURED ", "TRUE", pclFwrPos, sizeof pclFwrPos, pclConfigPath);
    GetPrivateProfileString(ogAppName, "GAT_FWR_BUTTON_COLOURED ", "TRUE", pclFwrGat, sizeof pclFwrGat, pclConfigPath);
    GetPrivateProfileString(ogAppName, "BLT_FWR_BUTTON_COLOURED ", "TRUE", pclFwrBlt, sizeof pclFwrBlt, pclConfigPath);
    GetPrivateProfileString(ogAppName, "LGE_FWR_BUTTON_COLOURED ", "TRUE", pclFwrWro, sizeof pclFwrWro, pclConfigPath);
    GetPrivateProfileString(ogAppName, "CKI_FWR_BUTTON_COLOURED ", "TRUE", pclFwrCic, sizeof pclFwrCic, pclConfigPath);

	GetPrivateProfileString(ogAppName,  "CONFLICTS_FOR_AIRB", "TRUE", pclConflictsForAirb, sizeof pclConflictsForAirb, pclConfigPath );
	GetPrivateProfileString(ogAppName,  "CONFLICTS_FOR_DATA_CHANGE", "FALSE", pclConflictsForDataChange, sizeof pclConflictsForDataChange, pclConfigPath );
	GetPrivateProfileString(ogAppName,  "TKEY_FOR_LBL_ACT_CHANGE", "CF04", pclLblConfACTchange, sizeof pclLblConfACTchange, pclConfigPath );

	GetPrivateProfileString(ogAppName,  "MINUTES_FOR_TIMELINE_MOVE", "2", pclTimerTimelineMinutes, sizeof pclTimerTimelineMinutes, pclConfigPath );
	//ogBcHandle.SetHomeAirport(pcgHome);


	if (strlen(pclBCLogFile) != 0)
	{
		pogBCLog = new ofstream;
		pogBCLog->open(pclBCLogFile, ios::out | ios::trunc);
		pogBCLog->setf(ios::unitbuf);   

		(*pogBCLog) << "FIPS: Broadcast-Log begin " << CTime::GetCurrentTime().Format("%d.%m.%y %H:%M:%S") << "\n\n";
	}
	else
	{
		pogBCLog = NULL;
	}


	// logging for debugging
	bgDebug = false;
	if(strcmp(pclDebug, "DEFAULT") != 0)
	{
		bgDebug = true;
		of_debug.open( pclDebug, ios::out);

		CTime olCurrTime = CTime::GetCurrentTime();

		of_debug << "FIPS STARTED! (" << olCurrTime.Format("%d.%m.%Y %H:%M:%S") << ")" << endl;
		of_debug << "==========================================" << endl << endl;
	}

	ogTkeyLblConfACTchange = CString(pclLblConfACTchange);

	int ilTimerTimelineMinutes = atoi(pclTimerTimelineMinutes);

	if (ilTimerTimelineMinutes != 0)
	{
		igTimerTimelineMinutes = ilTimerTimelineMinutes;
	}
	ogPosDiaFlightData.SetBartimesUpdateMinute(igTimerTimelineMinutes);


	if(strcmp(pclConflictsForDataChange, "TRUE") == 0)
		bgConflictsForDataChange = true;
	else
		bgConflictsForDataChange = false;

	if(strcmp(pclConflictsForAirb, "TRUE") == 0)
		bgConflictsForAirb = true;
	else
		bgConflictsForAirb = false;

	if(strcmp(pclFwrPos, "TRUE") == 0)
		bgFwrPos = true;
	else
		bgFwrPos = false;

	if(strcmp(pclFwrGat, "TRUE") == 0)
		bgFwrGat = true;
	else
		bgFwrGat = false;

	if(strcmp(pclFwrBlt, "TRUE") == 0)
		bgFwrBlt = true;
	else
		bgFwrBlt = false;

	if(strcmp(pclFwrWro, "TRUE") == 0)
		bgFwrWro = true;
	else
		bgFwrWro = false;

	if(strcmp(pclFwrCic, "TRUE") == 0)
		bgFwrCic = true;
	else
		bgFwrCic = false;



	if(strcmp(pclSeasonLocal, "TRUE") == 0)
		bgSeasonLocal = true;
	else
		bgSeasonLocal = false;

	if(strcmp(pclRealLocal, "TRUE") == 0)
		bgRealLocal = true;
	else
		bgRealLocal = false;


	if(strcmp(pclGatPosLocal, "TRUE") == 0)
		bgGatPosLocal = true;
	else
		bgGatPosLocal = false;


	if(strcmp(pclDailyLocal, "TRUE") == 0)
		bgDailyLocal = true;
	else
		bgDailyLocal = false;


	if(strcmp(pclDailyWithAlt, "TRUE") == 0)
		bgDailyWithAlt = true;
	else
		bgDailyWithAlt = false;

	if(strcmp(pclCreateDaily_OrgAtdBlue, "TRUE") == 0)
		bgCreateDaily_OrgAtdBlue = true;
	else
		bgCreateDaily_OrgAtdBlue = false;

	if(strcmp(pclViewEditFilter, "TRUE") == 0)
		bgViewEditFilter = true;
	else
		bgViewEditFilter = false;

	if(strcmp(pclViewEditSort, "TRUE") == 0)
		bgViewEditSort = true;
	else
		bgViewEditSort = false;

	if(strcmp(pclShowOnGround, "TRUE") == 0)
		bgShowOnGround = true;
	else
		bgShowOnGround = false;

	if(strcmp(pclCheckAllConflicts, "TRUE") == 0)
		bgCheckAllConflicts = true;
	else
		bgCheckAllConflicts = false;

	int ilGroundTimelimit = atoi(pclViewOnGroundTimelimit);
	ogOnGroundTimelimit = CTimeSpan(0,0,ilGroundTimelimit,0);

	if(strcmp(pclBltHeight, "TRUE") == 0)
		bgBltHeight = true;
	else
		bgBltHeight = false;

	if(strcmp(pclCcaHeight, "TRUE") == 0)
		bgCcaHeight = true;
	else
		bgCcaHeight = false;

	if(strcmp(pclGatHeight, "TRUE") == 0)
		bgGatHeight = true;
	else
		bgGatHeight = false;

	if(strcmp(pclPosHeight, "TRUE") == 0)
		bgPosHeight = true;
	else
		bgPosHeight = false;

	if(strcmp(pclWroHeight, "TRUE") == 0)
		bgWroHeight = true;
	else
		bgWroHeight = false;

	double dlOverlapHeight = atof(pclOverlapHeight) / 100;
	if (dlOverlapHeight > 0.05)
		dgOverlapHeight = dlOverlapHeight;
	else
		dgOverlapHeight = 0.10;

	int ilFidsNum = atoi(pclFidsRemark);
	switch(ilFidsNum)
	{
		case 1: ogFIDRemarkField = "BEME"; break;
		case 2: ogFIDRemarkField = "BEMD"; break;
		case 3: ogFIDRemarkField = "BET3"; break;
		case 4: ogFIDRemarkField = "BET4"; break;
		default: ogFIDRemarkField = "BEMD"; break;
	}


	if(strcmp(pclOffRelease, "TRUE") == 0)
	{
		bgOffRelFuncGatpos = true;
		bgOffRelFuncCheckin = true;
	}
	else
	{
		bgOffRelFuncGatpos = false;
		bgOffRelFuncCheckin = false;
	}

	if(strcmp(pclAirport4LC, "TRUE") == 0)
		bgAirport4LC = true;
	else
		bgAirport4LC = false;

	//rkr20042001
	// this part is only to perform the value to the next version
	// the posbuffer now is stored in an own record of cedacfgdata.
	// you can remove this part and delete the entry in ceda.ini by time
	int ilPosBuffer = atoi(pclPosBuffer);
	ogPosAllocBufferTime = CTimeSpan(0,0,ilPosBuffer,0);
	// you can remove this part and delete the entry in ceda.ini by time
	//rkr20042001




/*	// set postflight days
	int ilPostFlightDays = atoi(pclPostFlightDays);
	if (ilPostFlightDays > 0)
	{
		ogTimeSpanPostFlight = CTimeSpan(ilPostFlightDays, 0, 0, 0);
	}
*/


	SetCustomerConfig();


	strcpy(CCSCedaData::pcmTableExt, pcgTableExt);
	strcpy(CCSCedaData::pcmHomeAirport, pcgHome);
	strcpy(CCSCedaData::pcmApplName, ogAppName);
	strcpy(CCSCedaData::pcmVersion, pcgVersion);
	strcpy(CCSCedaData::pcmInternalBuild, pcgInternalBuild);

	
	ogBcHandle.AddTableCommand(CString("AFT") + CString(pcgTableExt), CString("SPR"), BC_FLIGHT_CHANGE, true);
	ogBcHandle.AddTableCommand(CString("AFT") + CString(pcgTableExt), CString("ISF"), BC_FLIGHT_CHANGE, true);
	ogBcHandle.AddTableCommand(CString("AFT") + CString(pcgTableExt), CString("IFR"), BC_FLIGHT_CHANGE, true);
	ogBcHandle.AddTableCommand(CString("AFT") + CString(pcgTableExt), CString("UFR"), BC_FLIGHT_CHANGE, true);
	ogBcHandle.AddTableCommand(CString("AFT") + CString(pcgTableExt), CString("DFR"), BC_FLIGHT_DELETE, true);
	ogBcHandle.AddTableCommand(CString("AFT") + CString(pcgTableExt), CString("JOF"), BC_FLIGHT_CHANGE, true);
	ogBcHandle.AddTableCommand(CString("AFT") + CString(pcgTableExt), CString("UPS"), BC_FLIGHT_CHANGE, true);
	ogBcHandle.AddTableCommand(CString("AFT") + CString(pcgTableExt), CString("UPJ"), BC_FLIGHT_CHANGE, true);
	ogBcHandle.AddTableCommand(CString("AFT") + CString(pcgTableExt), CString("RAC"), BC_RAC_RELOAD, true);
	ogBcHandle.AddTableCommand(CString("RELAFT"), CString("SBC"), AFT_RELOAD, true);

	ogBcHandle.AddTableCommand(CString("SDB") + CString(pcgTableExt), CString("SPR"), BC_FLIGHT_CHANGE, true);
	ogBcHandle.AddTableCommand(CString("SDB") + CString(pcgTableExt), CString("ISF"), BC_FLIGHT_CHANGE, true);
	ogBcHandle.AddTableCommand(CString("SDB") + CString(pcgTableExt), CString("IFR"), BC_FLIGHT_CHANGE, true);
	ogBcHandle.AddTableCommand(CString("SDB") + CString(pcgTableExt), CString("UFR"), BC_FLIGHT_CHANGE, true);
	ogBcHandle.AddTableCommand(CString("SDB") + CString(pcgTableExt), CString("DFR"), BC_FLIGHT_DELETE, true);
	ogBcHandle.AddTableCommand(CString("SDB") + CString(pcgTableExt), CString("JOF"), BC_FLIGHT_CHANGE, true);
	ogBcHandle.AddTableCommand(CString("SDB") + CString(pcgTableExt), CString("UPS"), BC_FLIGHT_CHANGE, true);
	ogBcHandle.AddTableCommand(CString("SDB") + CString(pcgTableExt), CString("UPJ"), BC_FLIGHT_CHANGE, true);

	ogBcHandle.AddTableCommand(CString("ARC") + CString(pcgTableExt), CString("SPR"), BC_FLIGHT_CHANGE, true);
	ogBcHandle.AddTableCommand(CString("ARC") + CString(pcgTableExt), CString("ISF"), BC_FLIGHT_CHANGE, true);
	ogBcHandle.AddTableCommand(CString("ARC") + CString(pcgTableExt), CString("IFR"), BC_FLIGHT_CHANGE, true);
	ogBcHandle.AddTableCommand(CString("ARC") + CString(pcgTableExt), CString("UFR"), BC_FLIGHT_CHANGE, true);
	ogBcHandle.AddTableCommand(CString("ARC") + CString(pcgTableExt), CString("DFR"), BC_FLIGHT_DELETE, true);
	ogBcHandle.AddTableCommand(CString("ARC") + CString(pcgTableExt), CString("JOF"), BC_FLIGHT_CHANGE, true);
	ogBcHandle.AddTableCommand(CString("ARC") + CString(pcgTableExt), CString("UPS"), BC_FLIGHT_CHANGE, true);
	ogBcHandle.AddTableCommand(CString("ARC") + CString(pcgTableExt), CString("UPJ"), BC_FLIGHT_CHANGE, true);

	ogBcHandle.AddTableCommand(CString(""), CString("SPR"), BC_FLIGHT_CHANGE, true);
	ogBcHandle.AddTableCommand(CString(""), CString("ISF"), BC_FLIGHT_CHANGE, true);
	ogBcHandle.AddTableCommand(CString(""), CString("IFR"), BC_FLIGHT_CHANGE, true);
	ogBcHandle.AddTableCommand(CString(""), CString("UFR"), BC_FLIGHT_CHANGE, true);
	ogBcHandle.AddTableCommand(CString(""), CString("DFR"), BC_FLIGHT_DELETE, true);
	ogBcHandle.AddTableCommand(CString(""), CString("JOF"), BC_FLIGHT_CHANGE, true);
	ogBcHandle.AddTableCommand(CString(""), CString("UPS"), BC_FLIGHT_CHANGE, true);
	ogBcHandle.AddTableCommand(CString(""), CString("UPJ"), BC_FLIGHT_CHANGE, true);

	ogBcHandle.AddTableCommand(CString("CCA") + CString(pcgTableExt), CString("IRT"), BC_CCA_NEW, true);
	ogBcHandle.AddTableCommand(CString("CCA") + CString(pcgTableExt), CString("DRT"), BC_CCA_DELETE, true);
	ogBcHandle.AddTableCommand(CString("CCA") + CString(pcgTableExt), CString("URT"), BC_CCA_CHANGE, true);

 
	ogBcHandle.AddTableCommand(CString("INF") + CString(pcgTableExt), CString("IRT"), BC_INF_NEW, false);
	ogBcHandle.AddTableCommand(CString("INF") + CString(pcgTableExt), CString("URT"), BC_INF_CHANGE, false);
	ogBcHandle.AddTableCommand(CString("INF") + CString(pcgTableExt), CString("DRT"), BC_INF_DELETE, false);

	ogBcHandle.AddTableCommand(CString("CFL") + CString(pcgTableExt), CString("IRT"), BC_CFL_NEW, true);
	ogBcHandle.AddTableCommand(CString("CFL") + CString(pcgTableExt), CString("URT"), BC_CFL_CHANGE, true);
	//ogBcHandle.AddTableCommand(CString("CFL") + CString(pcgTableExt), CString("IBT"), BC_CFL_NEW, true);
	//ogBcHandle.AddTableCommand(CString("CFL") + CString(pcgTableExt), CString("UBT"), BC_CFL_CHANGE, true);
	ogBcHandle.AddTableCommand(CString("RELCFL"), CString("SBC"), CFL_RELOAD, true);
	ogBcHandle.AddTableCommand(CString("SBCCFR"), CString("SBC"), CFR_RELOAD, true);

	ogBcHandle.AddTableCommand(CString("BCTEST"), CString("SBC"), BC_TEST, true);
	ogBcHandle.AddTableCommand(CString("BCLOCC"), CString("SBC"), BC_LOCCHNG, true);



//	CString theKey = "HKEY_CURRENT_USER\\Software\\Fips\\FipsLogin";
	CString theKey = "UFIS";
	SetRegistryKey (theKey);


	CLoginDialog olLoginDlg(pcgTableExt,ogAppName,pogBackGround);

	if((strcmp(pclUser, "DEFAULT") != 0) && (strcmp(pclPassword, "DEFAULT") != 0))
	{
		strcpy(pcgUser,pclUser);
		strcpy(pcgPasswd,pclPassword);
		ogBasicData.omUserID = pclUser;
		ogCommHandler.SetUser(pclUser);

		strcpy(CCSCedaData::pcmReqId, ogCommHandler.pcmReqId);
		strcpy(CCSCedaData::pcmUser, pclUser);


		if(!ogPrivList.Login(pcgTableExt,pclUser,pclPassword,ogAppName))
			return FALSE;
	}
	else
	{
		if( olLoginDlg.DoModal() != IDCANCEL )
		{
			int ilStartApp = IDOK;
			if(ogPrivList.GetStat("InitModu") == '1')
			{
				RegisterDlg olRegisterDlg;
				ilStartApp = olRegisterDlg.DoModal();
			}
			if(ilStartApp != IDOK)
			{
				return FALSE;
			}

		}
		else
		{
			return FALSE;
		}
	}

	ogCfgData.ReadUfisCedaConfig();



/*
	// ++++++++++++++++++++++++++++++ login SUS	
	Beep( 400,100 ) ;
	strcpy( pcgUser, "hbe" ) ;
	strcpy( pcgPasswd, "hbe" ) ;
	ogBasicData.omUserID = "hbe" ;
	ogCommHandler.SetUser("hbe");
*/
	if (pogBackGround)
	{
		pogBackGround->InvalidateRect(NULL);
		pogBackGround->UpdateWindow();
	}

	//CWinThread* pThread = AfxBeginThread(Test, NULL, THREAD_PRIORITY_NORMAL); 



	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));		


	//InitialLoadDialog
    InitialLoad(pogBackGround);

	ogDataSet.Init();


	//Set Homeairport 4 letter code
	CString olApc4;
	ogBCD.GetField("APT","APC3", CString(pcgHome), "APC4", olApc4);
	strcpy(pcgHome4, olApc4);
	CedaAptLocalUtc::InitCedaAptLocalUtc(olApc4);


	ogBasicData.SetLocalDiff();

	CCSCedaData::omLocalDiff1 = ogBasicData.GetLocalDiff1();
	CCSCedaData::omLocalDiff2 = ogBasicData.GetLocalDiff2();
	CCSCedaData::omTich = ogBasicData.GetTich() ;

//	CCSCedaData olInitApt;
// 	olInitApt.InitAptUtcLocal("LGAV"); 


	InitCOMInterface();

	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));		


//	CButtonListDlg dlg(pogBackGround);
	CButtonListDlg dlg(NULL);
	m_pMainWnd = &dlg;

	pogButtonList = &dlg; 	
	
	int nResponse = dlg.DoModal();
	if (nResponse == IDOK)
	{
		// TODO: Place code here to handle when the dialog is
		//  dismissed with OK
	}
	else if (nResponse == IDCANCEL)
	{
		// TODO: Place code here to handle when the dialog is
		//  dismissed with Cancel
	}

	pogButtonList = NULL;
 
	of_catch.close();

	// Since the dialog has been closed, return FALSE so that we exit the
	//  application, rather than start the application's message pump.
	return FALSE;
}



bool CFPMSApp::InitCOMInterface()
{

	HRESULT olRes = CoInitialize(NULL);
	if (olRes == E_INVALIDARG)
		int asdf = 0;
	if (olRes == E_OUTOFMEMORY)
		int asdf = 0;
	if (olRes == E_UNEXPECTED)
		int asdf = 0;
	if (olRes == S_FALSE)
		int asdf = 0;
	if (olRes == RPC_E_CHANGED_MODE)
		int asdf = 0;

 	if(olRes != S_OK && olRes != S_FALSE)
	{
		//AfxMessageBox(_T("CoInitializeEx failed!"));
		return false;
	}
	

	try
	{
		pConnect = IUFISAmPtr(CLSID_UFISAm);
	}
	catch(_com_error error)
	{
		//AfxMessageBox(error.ErrorMessage());
		return false;
	}


	try
	{
		BOOL Res = m_UFISAmSink.Advise(pConnect,IID_IUFISAmEventSink);
		ASSERT(Res == TRUE);
	}
	catch(_com_error error)
	{
		//AfxMessageBox(error.ErrorMessage());
		return false;
	}
	/*
	 * Assign AppTag immediately after advising the sink!
	 */
	pConnect->AssignAppTag(static_cast<long>(Fips));

	bgComOk = true;

	CFPMSApp::DumpDebugLog("COM-Interface succesfully initialized!");

	return true;
}



void CFPMSApp::InitialLoad(CWnd *pParent)
{
	CCS_TRY

	//CTime olTime = CTime::GetCurrentTime();
	//TRACE("\n\nbeginn InitialLoad  %s", olTime.Format("%H:%M:%S"));

	pogInitialLoad = new CInitialLoadDlg(pParent); 

	int ilGant = 1;

	ogBCD.SetTableExtension(CString(pcgTableExt));
	ogCcaDiaFlightData.omCcaData.SetTableName(CString("CCA") + CString(pcgTableExt));


	ogBCD.SetSystabErrorMsg( GetString(IDS_STRING1656), GetString(ST_FEHLER ) );


	//ogGrnData.SetTableName(CString("GRN") + CString(pcgTableExt));
	
/*	ogGrmData.SetTableName(CString("GRM") + CString(pcgTableExt));
	ogCcaDiaFlightData.omCcaData.SetTableName(CString("CCA") + CString(pcgTableExt));
	ogBltDiaFlightData
	ogGatDiaFlightData	
	ogPosDiaFlightData	
	ogWroDiaFlightData	
	
	ogCcaData.SetTableName(CString("CCA") + CString(pcgTableExt));
*/


//////////////////////////////////////////////////////////////////////////////////////
// TEST

	/*
	pogInitialLoad->SetMessage("Flugdaten werden geladen..");
	ogBCD.SetObject("AFT", "URNO,RKEY,HTYP,DES3,ORG3,NOSE,REM1,ADID,FLNO,ALC2,ALC3,FLTN,FLNS,STOA,STOD,ETAI,ETDI,ACT3,ACT5,VIAL,VIAN,FTYP");

	ogBCD.Read(CString("AFT"), "(TIFA > '19980101000000' AND TIFA < '19981025000000') OR (TIFD > '19980101000000' AND TIFD < '19981025000000')");
	
	//ogBCD.Read(CString("AFT"), "URNO = 6635830");

	int ilCount2 = ogBCD.GetDataCount("AFT");


	RecordSet rlRecord(ogBCD.GetFieldCount("AFT"));				


	ogBCD.GetRecord("AFT", "URNO", "6635830", rlRecord);



	CString olStr;

	RecordSet *prlRecord;

	for(int g = 0 ; g <= 1000 ; g++)
	{
		prlRecord = new RecordSet(200);
	}
	*/





/*



	pogInitialLoad->SetMessage("Flugdaten werden geladen..");
	ogBCD.SetObject("AFT","ACT5,ADID,ALC2,ALC3,DES3,HTYP,FLTN,NOSE,ORG3,REM1,RKEY,STOA,STOD,URNO,VIAL,VIAN,FTYP");

	ogBCD.Read(CString("AFT"), "(TIFA > '19980701000000' AND TIFA < '19980801000000') OR (TIFD > '19980701000000' AND TIFD < '19980801000000') AND (ALC2 <> 'US' AND ALC2 <> 'UA' AND (DOOA = '1' OR DOOD = '1'))");
	//ogBCD.Read(CString("AFT"), "TIFA > '19980616000000' AND TIFD < '19981024000000' AND ALC2 <> 'US' AND ALC2 <> 'UA' AND (DOOA = '1' OR DOOD = '1')");
	//ogBCD.Read(CString("AFT"), "TIFA > '19980716000000' AND TIFD < '19981024000000'");
	
	//ogBCD.Read(CString("AFT"), "URNO = 6635830");

	int ilCount2 = ogBCD.GetDataCount("AFT");



*/






//////////////////////////////////////////////////////////////////////////////////////

	pogInitialLoad->SetRange(30);

	// toms tables
	pogInitialLoad->SetMessage(GetString(IDS_STRING2347));
	ogBCD.SetObject("ALO","ALOC,URNO");
	ogBCD.AddKeyMap("ALO","ALOC");
	ogBCD.Read("ALO");
	pogInitialLoad->SetProgress(ilGant);

	pogInitialLoad->SetMessage(GetString(IDS_STRING2348));
	ogBCD.SetObject("SGR","UGTY,STYP,URNO");
	ogBCD.AddKeyMap("SGR","UGTY");
	ogBCD.AddKeyMap("SGR","STYP");
	ogBCD.Read("SGR");
	pogInitialLoad->SetProgress(ilGant);

	pogInitialLoad->SetMessage(GetString(IDS_STRING2349));
	ogBCD.SetObject("SGM","UVAL,VALU,USGR,URNO,UGTY,PRFL,SORT"); 
	ogBCD.AddKeyMap("SGM","USGR");
	ogBCD.AddKeyMap("SGM","UGTY");
	ogBCD.AddKeyMap("SGM","UVAL");
	ogBCD.AddKeyMap("SGM","SORT");
	ogBCD.Read("SGM");
	pogInitialLoad->SetProgress(ilGant);
	// toms tables

	// Fluggesellschaften
	pogInitialLoad->SetMessage(GetString(IDS_STRING954));
	ogBCD.SetObject("ALT", "URNO,ALC2,ALC3,ALFN,TERM,CASH");
	ogBCD.SetObjectDesc("ALT", GetString(IDS_STRING955));
	ogBCD.SetTableHeader("ALT", "ALFN", GetString(IDS_STRING985));
	ogBCD.SetTableHeader("ALT", "ALC2", GetString(IDS_STRING987));
	ogBCD.SetTableHeader("ALT", "ALC3", GetString(IDS_STRING988));
	ogBCD.AddKeyMap("ALT", "ALC2");
	ogBCD.AddKeyMap("ALT", "ALC3");
	ogBCD.Read(CString("ALT"), CString("WHERE ALC3 <> ' '"));
	pogInitialLoad->SetProgress(ilGant);
	//ogBCD.SetDdxType(CString("ALT"), "IRT", S_FLIGHT_INSERT);
	//ogBCD.SetDdxType(CString("ALT"), "DRT", S_FLIGHT_DELETE);
	//ogBCD.SetDdxType(CString("ALT"), "URT", S_FLIGHT_UPDATE);
	

	// Delaycodes
	pogInitialLoad->SetMessage(GetString(IDS_STRING992));
	ogBCD.SetObject("DEN", "URNO,DECA,DECN,DENA");
	ogBCD.SetObjectDesc("DEN", "Delaycodes");
	ogBCD.SetTableHeader("DEN", "DENA", GetString(IDS_STRING985));
	ogBCD.SetTableHeader("DEN", "DECN", GetString(IDS_STRING993));
	ogBCD.SetTableHeader("DEN", "DECA", GetString(IDS_STRING994));
	ogBCD.AddKeyMap("DEN", "DECA");
	ogBCD.AddKeyMap("DEN", "DECN");
	ogBCD.Read(CString("DEN"));
	pogInitialLoad->SetProgress(ilGant);

	//Checkin-Counter Blockierungen
	ogBCD.SetObject("BLK", "BURN,DAYS,NAFR,NATO,RESN,TABN,TYPE,TIFR,TITO,URNO,IBIT");
	ogBCD.SetObjectDesc("BLK", "Blockade");
	ogBCD.Read("BLK");

	ogBCD.SetDdxType("BLK", "IRT", BLK_CHANGE);
	ogBCD.SetDdxType("BLK", "DRT", BLK_CHANGE);
	ogBCD.SetDdxType("BLK", "URT", BLK_CHANGE);


	//Checkin-Counter Blockierungen f�r REPORT19
	ogBCD.SetObject("AAA", "BLK", CString("BURN,DAYS,NAFR,NATO,RESN,TABN,TYPE,TIFR,TITO,URNO"));

	// Registrierungen
	pogInitialLoad->SetMessage(GetString(IDS_STRING995));
	ogBCD.SetObject("ACR", "URNO,REGN,ACT3,ACT5,UFIS,APUI");
	ogBCD.SetObjectDesc("ACR", GetString(IDS_STRING996));
	//ogBCD.AddKeyMap("ACR", "REGN");
	//ogBCD.Read(CString("ACR"), CString("WHERE UFIS='1'"));
	pogInitialLoad->SetProgress(ilGant * 2);

	ogBCD.SetObject("GHS", "URNO,LKNM");
	ogBCD.Read(CString("GHS"));

	ogBCD.SetObject("GHD", "URNO,DUST,FKEY,EURN,GHSU");
	ogBCD.SetBcMode("GHD", -1);
	//ogBCD.Read(CString("ACR"));



	// Flugzeugtypen
	pogInitialLoad->SetMessage(GetString(IDS_STRING997));
	ogBCD.SetObject("ACT", "URNO,ACT3,ACT5,ACFN,SEAT,ACWS,ACHE,ACLE");
	ogBCD.SetObjectDesc("ACT", GetString(IDS_STRING998));
	ogBCD.SetTableHeader("ACT", "ACFN", GetString(IDS_STRING985));
	ogBCD.SetTableHeader("ACT", "ACT3", GetString(IDS_STRING988));
	ogBCD.SetTableHeader("ACT", "ACT5", GetString(IDS_STRING991));
	ogBCD.AddKeyMap("ACT", "ACT5");
	ogBCD.AddKeyMap("ACT", "ACT3");
	ogBCD.Read(CString("ACT"));
	pogInitialLoad->SetProgress(ilGant);


	// Flugh�fen
	pogInitialLoad->SetMessage(GetString(IDS_STRING999));
	ogBCD.SetObject("APT", "URNO,APC3,APC4,APFN,TDI1,TDI2,TICH,APTT,TDIS,TDIW,TZON");
	ogBCD.SetObjectDesc("APT", GetString(IDS_STRING990));
	ogBCD.SetTableHeader("APT", "APFN", GetString(IDS_STRING985));
	ogBCD.SetTableHeader("APT", "APC3", GetString(IDS_STRING988));
	ogBCD.SetTableHeader("APT", "APC4", GetString(IDS_STRING989));
	ogBCD.AddKeyMap("APT", "APC3");
	ogBCD.AddKeyMap("APT", "APC4");
	ogBCD.Read(CString("APT"));
	pogInitialLoad->SetProgress(ilGant);

	// Gep�ckb�nder
	pogInitialLoad->SetMessage(GetString(IDS_STRING1000));
	ogBCD.SetObject("BLT", "URNO,TERM,BNAM,NAFR,NATO,MAXF,DEFD,BLTT");
	ogBCD.SetTableHeader("BLT", "TERM", GetString(IDS_STRING1001));
	ogBCD.SetTableHeader("BLT", "BNAM", GetString(IDS_STRING986));
	ogBCD.SetObjectDesc("BLT", GetString(IDS_STRING1010));
	ogBCD.AddKeyMap("BLT", "BNAM");

	ogBCD.SetDdxType("BLT", "IRT", BCD_BLT_CHANGE);
	ogBCD.SetDdxType("BLT", "DRT", BCD_BLT_CHANGE);
	ogBCD.SetDdxType("BLT", "URT", BCD_BLT_CHANGE);
	
	ogBCD.Read(CString("BLT"));
	pogInitialLoad->SetProgress(ilGant);

	// Checkin Schalter
	pogInitialLoad->SetMessage(GetString(IDS_STRING1011));
	ogBCD.SetObject("CIC");
	//ogBCD.SetObject("CIC", "URNO,TERM,CNAM,NAFR,NATO,HALL,CICR,VATO,VAFR,RGBL");
	ogBCD.SetObjectDesc("CIC", GetString(IDS_STRING1012));
	ogBCD.SetTableHeader("CIC", "TERM", GetString(IDS_STRING1001));
	ogBCD.SetTableHeader("CIC", "CNAM", GetString(IDS_STRING986));
	ogBCD.AddKeyMap("CIC", "CNAM");
	ogBCD.Read(CString("CIC"));

	ogBCD.SetDdxType("CIC", "IRT", BCD_CIC_CHANGE);
	ogBCD.SetDdxType("CIC", "DRT", BCD_CIC_CHANGE);
	ogBCD.SetDdxType("CIC", "URT", BCD_CIC_CHANGE);

	
	
	pogInitialLoad->SetProgress(ilGant);

	// Ausg�nge
	pogInitialLoad->SetMessage(GetString(IDS_STRING1013));
	ogBCD.SetObject("EXT", "URNO,ENAM,TERM");
	ogBCD.SetObjectDesc("EXT", GetString(IDS_STRING1014));
	ogBCD.SetTableHeader("EXT", "TERM", GetString(IDS_STRING1001));
	ogBCD.SetTableHeader("EXT", "ENAM", GetString(IDS_STRING986));
	ogBCD.AddKeyMap("EXT", "ENAM");
	ogBCD.Read(CString("EXT"));
	pogInitialLoad->SetProgress(ilGant);

	// Fids Remarks
	pogInitialLoad->SetMessage(GetString(IDS_STRING1015));
	CString olFIDFields;
	olFIDFields.Format("URNO,%s,CODE", ogFIDRemarkField);
	ogBCD.SetObject("FID", olFIDFields);
	ogBCD.SetObjectDesc("FID", GetString(IDS_STRING1016));
	ogBCD.SetTableHeader("FID", ogFIDRemarkField, GetString(IDS_STRING985));
	ogBCD.SetTableHeader("FID", "CODE", GetString(IDS_STRING1017));
	ogBCD.AddKeyMap("FID", "CODE");
	ogBCD.Read(CString("FID"));

	
	ogBCD.SetObject("FID_CIC", "FID", olFIDFields);
	ogBCD.SetObjectDesc("FID_CIC", GetString(IDS_STRING1016));
	ogBCD.SetTableHeader("FID_CIC", ogFIDRemarkField, GetString(IDS_STRING985));
	ogBCD.SetTableHeader("FID_CIC", "CODE", GetString(IDS_STRING1017));
	ogBCD.AddKeyMap("FID_CIC", "CODE");
	ogBCD.Read(CString("FID_CIC"), "WHERE REMT = 'C'");
	
		
	pogInitialLoad->SetProgress(ilGant);



	// Gates
	pogInitialLoad->SetMessage(GetString(IDS_STRING1018));
	ogBCD.SetObject("GAT", "URNO,GNAM,TERM,NAFR,NATO,RGA1,RGA2,RBAB,RESB,BUSG,GATR,DEFD");
	ogBCD.SetObjectDesc("GAT", GetString(IDS_STRING1019));
	ogBCD.SetTableHeader("GAT", "TERM", GetString(IDS_STRING1001));
	ogBCD.SetTableHeader("GAT", "GNAM", GetString(IDS_STRING986));
	ogBCD.AddKeyMap("GAT", "GNAM");
	ogBCD.AddKeyMap("GAT", "RGA1");

	ogBCD.SetDdxType("GAT", "IRT", BCD_GAT_CHANGE);
	ogBCD.SetDdxType("GAT", "DRT", BCD_GAT_CHANGE);
	ogBCD.SetDdxType("GAT", "URT", BCD_GAT_CHANGE);


	ogBCD.Read(CString("GAT"));
	pogInitialLoad->SetProgress(ilGant);

	// Abfertigungsarten
	pogInitialLoad->SetMessage(GetString(IDS_STRING1020));
	ogBCD.SetObject("HTY", "URNO,HNAM,HTYP");
	ogBCD.SetTableHeader("HTY", "HTYP", GetString(IDS_STRING1009));
	ogBCD.SetTableHeader("HTY", "HNAM", GetString(IDS_STRING985));
	ogBCD.SetObjectDesc("HTY", GetString(IDS_STRING1021));
	ogBCD.AddKeyMap("HTY", "HTYP");
	ogBCD.Read(CString("HTY"));
	pogInitialLoad->SetProgress(ilGant);

	// Verkehrsarten
	pogInitialLoad->SetMessage(GetString(IDS_STRING1022));
	ogBCD.SetObject("NAT", "URNO,TNAM,TTYP,ALGA,ALPO,ALBB,ALWR");
	ogBCD.SetTableHeader("NAT", "TTYP", GetString(IDS_STRING1009));
	ogBCD.SetTableHeader("NAT", "TNAM", GetString(IDS_STRING985));
	ogBCD.SetObjectDesc("NAT", GetString(IDS_STRING1023));
	ogBCD.AddKeyMap("NAT", "TTYP");
	ogBCD.Read(CString("NAT"));

	ogBCD.SetDdxType("NAT", "IRT", BCD_NAT_CHANGE);
	ogBCD.SetDdxType("NAT", "DRT", BCD_NAT_CHANGE);
	ogBCD.SetDdxType("NAT", "URT", BCD_NAT_CHANGE);
	
	
	pogInitialLoad->SetProgress(ilGant);

	// Positionen
	pogInitialLoad->SetMessage(GetString(IDS_STRING1024));
	ogBCD.SetObject("PST", "URNO,PNAM,RESN,NAFR,NATO,CDAT,POSR,VATO,VAFR,TAXI,GPUS,DEFD,BRGS");
	ogBCD.SetObjectDesc("PST", GetString(IDS_STRING1025));
	ogBCD.SetTableHeader("PST", "PNAM", GetString(IDS_STRING986));
	ogBCD.AddKeyMap("PST", "PNAM");

	ogBCD.SetDdxType("PST", "IRT", BCD_PST_CHANGE);
	ogBCD.SetDdxType("PST", "DRT", BCD_PST_CHANGE);
	ogBCD.SetDdxType("PST", "URT", BCD_PST_CHANGE);


	ogBCD.Read(CString("PST"));
	pogInitialLoad->SetProgress(ilGant);

	// Start und Landebahnen
	pogInitialLoad->SetMessage(GetString(IDS_STRING1026));
	ogBCD.SetObject("RWY", "URNO,RNAM,RTYP,RNUM");
	ogBCD.SetObjectDesc("RWY", GetString(IDS_STRING1027));
	ogBCD.AddKeyMap("RWY", "RNAM");
	ogBCD.AddKeyMap("RWY", "RNUM");
	ogBCD.Read(CString("RWY"));
	pogInitialLoad->SetProgress(ilGant);


	ogBCD.SetObject("TWY");
	ogBCD.AddKeyMap("TWY", "TNAM");
	ogBCD.Read(CString("TWY"));


	// Season
	pogInitialLoad->SetMessage(GetString(IDS_STRING1028));
	ogBCD.SetObject("SEA", "URNO,SEAS,VPFR,VPTO");
	ogBCD.SetObjectDesc("SEA", GetString(IDS_STRING1007));
	ogBCD.AddKeyMap("SEA", "SEAS");
	ogBCD.Read(CString("SEA"));
	pogInitialLoad->SetProgress(ilGant);

	ogBCD.SetObject("AWI");


/*
	CString olLkey;
	if (strlen(pcmLKey) == 0)
		olLkey = ogCfgData.GetLkey();
	else
		olLkey = pcmLKey;

	CString olSel = CString("WHERE LKEY='") + olLkey + CString("'");


	ogBCD.SetObject("LBL");
	ogBCD.AddKeyMap("LBL", "URNO");
	ogBCD.AddKeyMap("LBL", "TKEY");
	ogBCD.Read(CString("LBL"), olSel);
*/

	CString olLkey;
	if (strlen(pcmLKey) == 0)
		olLkey = ogLkeyFlight;//ogCfgData.GetLkey();
	else
		olLkey = pcmLKey;

	if (olLkey.IsEmpty())
		olLkey = "ENG";

	CString olSel = CString("WHERE LKEY='") + olLkey + CString("'");


	ogBCD.SetObject("LBL");
	ogBCD.AddKeyMap("LBL", "URNO");
	ogBCD.AddKeyMap("LBL", "TKEY");
	ogBCD.SetSort("LBL","URNO+",true);
	ogBCD.Read(CString("LBL"), olSel);

	int ilCount = ogBCD.GetDataCount("LBL");
	for(int i = 0; i < ilCount; i++)
	{
		RecordSet rlRec;
		ogBCD.GetRecord("LBL", i, rlRec);
		CString llUrno = rlRec[ogBCD.GetFieldIndex("LBL", "URNO")];
		CString olTkey = rlRec[ogBCD.GetFieldIndex("LBL", "TKEY")];
		char bufferTmp[128];
		itoa(i, bufferTmp, 10);
//		if (ogTkeyLblConfACTchange != olTkey && bgConflictsForDataChange)
//			ogMapLblUrnoToKonftype.SetAt(llUrno,bufferTmp);
		if (bgConflictsForDataChange && ogTkeyLblConfACTchange != olTkey)
			ogMapLblUrnoToKonftype.SetAt(llUrno,bufferTmp);

		if (ogTkeyLblConfACTchange == olTkey)
			ogMapLblUrnoToKonftype.SetAt(llUrno,bufferTmp);
	}


	// VIPs
	// MBR Anf
	ogBCD.SetObject("VIP");
	ogBCD.SetObjectDesc("VIP", GetString(IDS_STRING1749));
	ogBCD.Read(CString("VIP"));
	pogInitialLoad->SetProgress(ilGant);

	ogBCD.SetDdxType("VIP", "IRT", BCD_VIP_INSERT);
	ogBCD.SetDdxType("VIP", "DRT", BCD_VIP_DELETE);
	ogBCD.SetDdxType("VIP", "URT", BCD_VIP_UPDATE);

	// HAI
	ogBCD.SetObject("HAI", "CDAT,FLNU,HOPO,HSNA,LSTU,PRFL,REMA,TASK,URNO,USEC,USEU,ALTU");
	ogBCD.SetObjectDesc("HAI", GetString(IDS_STRING1769));
	ogBCD.AddKeyMap("HAI", "TASK");
	ogBCD.AddKeyMap("HAI", "REMA");
	ogBCD.AddKeyMap("HAI", "HSNA");
	ogBCD.AddKeyMap("HAI", "ALTU");
	int ilCou = ogBCD.Read(CString("HAI"));  

	ogBCD.SetDdxType("HAI", "IRT", BCD_HAI_INSERT);
	ogBCD.SetDdxType("HAI", "DRT", BCD_HAI_DELETE);
	ogBCD.SetDdxType("HAI", "URT", BCD_HAI_UPDATE);

	ogBCD.SetObject("HAG", "URNO,HSNA,HNAM,TELE,FAXN");
	ogBCD.SetObjectDesc("HAG", GetString(IDS_STRING1769));
	ogBCD.SetTableHeader("HAG", "HSNA", GetString(IDS_STRING1768));
	ogBCD.SetTableHeader("HAG", "HNAM", GetString(IDS_STRING1769));
	ogBCD.AddKeyMap("HAG", "HNAM");
	ogBCD.AddKeyMap("HAG", "TELE");
	ogBCD.AddKeyMap("HAG", "FAXN");
	ogBCD.Read(CString("HAG"));

	ogBCD.SetDdxType("HAG", "IRT", BCD_HAI_INSERT);
	ogBCD.SetDdxType("HAG", "DRT", BCD_HAI_DELETE);
	ogBCD.SetDdxType("HAG", "URT", BCD_HAI_UPDATE);
	// MBR Ende


	// Servicetypen
	pogInitialLoad->SetMessage(GetString(IDS_STRING1006));
	ogBCD.SetObject("STY", "URNO,SNAM,STYP");
	ogBCD.SetObjectDesc("STY", GetString(IDS_STRING1008));
	ogBCD.SetTableHeader("STY", "SNAM", GetString(IDS_STRING986));
	ogBCD.SetTableHeader("STY", "STYP", GetString(IDS_STRING1009));
	ogBCD.AddKeyMap("STY", "STYP");
	ogBCD.Read(CString("STY"));
	pogInitialLoad->SetProgress(ilGant);


	// Warter�ume
	pogInitialLoad->SetMessage(GetString(IDS_STRING1003));
	ogBCD.SetObject("WRO", "URNO,WNAM,TERM,NAFR,NATO,GTE1,GTE2,DEFD,MAXF,BRCA");
	ogBCD.SetObjectDesc("WRO", GetString(IDS_STRING1002));
	ogBCD.SetTableHeader("WRO", "WNAM", GetString(IDS_STRING986));
	ogBCD.SetTableHeader("WRO", "TERM", GetString(IDS_STRING1001));
	ogBCD.AddKeyMap("WRO", "WNAM");
	ogBCD.AddKeyMap("WRO", "GTE1");

	ogBCD.SetDdxType("WRO", "IRT", BCD_WRO_CHANGE);
	ogBCD.SetDdxType("WRO", "DRT", BCD_WRO_CHANGE);
	ogBCD.SetDdxType("WRO", "URT", BCD_WRO_CHANGE);
	
	
	ilCou = ogBCD.Read(CString("WRO"));
	pogInitialLoad->SetProgress(ilGant);


	//useres
	ogBCD.SetObject("SEC", "URNO,USID,NAME,TYPE");
	olSel.Format("WHERE TYPE = 'U'");
	ogBCD.Read(CString("SEC"), olSel);
	int ilCountUser = ogBCD.GetDataCount("SEC");
	/////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////
	CString olTable;



	// Belegungsvorgaben
	pogInitialLoad->SetMessage(GetString(IDS_STRING1443));
	ogBCD.SetObject("PGR", "ACGR,ACT5,ALCA,ALCD,ALGA,ALGD,APGA,APGD,BUTA,BUTB,CDAT,DEST,FLNA,FLND,FLSA,FLSD,GATR,HTYA,HTYD,LSTU,ORIG,POSR,PRCO,PRFL,PRIO,PRNA,PRSN,REFP,REGN,TIFR,TTYA,URNO,USEC,VAFR,VDAA,VDAD,VIAA,VIAD,TTYD,USEU");
		
	//ogBCD.AddKeyMap("WRO", "WNAM");
	ogBCD.Read(CString("PGR"));

	int ilCountPgr = ogBCD.GetDataCount("PGR");

	pogInitialLoad->SetProgress(ilGant);

	
	// Gruppenname
	pogInitialLoad->SetMessage(GetString(IDS_STRING1444));
	ogBCD.SetObject("GRN");

	olSel.Format("WHERE APPL='%s' OR APPL='%s' OR APPL='%s'", "GATPOS","POPS", ogAppName);
	ogBCD.Read(CString("GRN"), olSel);
	pogInitialLoad->SetProgress(ilGant);

	olTable = CString("GRN") + CString(pcgTableExt);

	// Gruppenname
	ogGrmData.SetTableName(CString("GRM") + CString(pcgTableExt));
	ogGrmData.Read();


	ogBCD.SetObject("GRM");
	ogBCD.AddKeyMap("GRM", "VALU");

	olSel.Format("WHERE GURN IN (SELECT URNO FROM %s WHERE APPL='%s' OR APPL='%s' OR APPL='%s')", olTable, "GATPOS","POPS", ogAppName);
	ogBCD.Read(CString("GRM"), olSel);
	pogInitialLoad->SetProgress(ilGant);

	/////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////

	ogBCD.SetObject("GHP");
	ogBCD.SetObject("GPM");
	ogBCD.AddKeyMap("GPM", "RKEY");



	olSel.Format("WHERE APPL='%s' OR APPL='%s' OR APPL='%s'", "GATPOS","POPS", ogAppName);
	ogBCD.Read(CString("GHP"), olSel);

	int h = ogBCD.GetDataCount("GHP");

	olTable = CString("GHP") + CString(pcgTableExt);


	olSel.Format("WHERE RKEY IN (SELECT URNO FROM %s WHERE APPL='%s' OR APPL='%s' OR APPL='%s')", olTable, "GATPOS","POPS", ogAppName);
	ogBCD.Read(CString("GPM"), olSel);

	int j = ogBCD.GetDataCount("GPM");
	
	pogInitialLoad->SetProgress(ilGant);

	// Konfigurationsdaten
	pogInitialLoad->SetMessage(GetString(IDS_STRING1004));
	ogCfgData.GetFlightConfig();
	ogCfgData.ReadCfgData();
	ogCfgData.SetCfgData();
	ogCfgData.ReadMonitorSetup();
	ogCfgData.ReadFontSetup();
	ogCfgData.ReadPopsReportDailyAlcSetup();
	ogCfgData.ReadConflictSetup();	
	ogCfgData.ReadAllConfigData();

	ogSpotAllocation.Init();
	
	ogKonflikte.SetConfiguration();
	ogCfgData.ReadGateBufferTimeSetup();	
	ogCfgData.ReadPosBufferTimeSetup();	
	ogCfgData.ReadArchivePastBufferTimeSetup();	
	ogCfgData.ReadArchiveFutureBufferTimeSetup();	
	ogCfgData.ReadXDaysSetup();	
	ogCfgData.ReadTimelineBufferSetup();	
	ogCfgData.ReadWingoverlapBufferSetup();	
	ogCfgData.ReadDisplayAircaftChangeSetup();	 


	pogInitialLoad->SetProgress(ilGant);

	// Ansichten
	pogInitialLoad->SetMessage(GetString(IDS_STRING1005));
	InitSeasonFlightTableDefaultView();
	pogInitialLoad->SetProgress(ilGant);


	// resource groups
	pogInitialLoad->SetMessage(GetString(IDS_STRING1976));
	ogResGroupData.ReadGroups();
	pogInitialLoad->SetProgress(ilGant);
	ogCfgData.ReadPosWoConflictsSetup();	 



	if (pogInitialLoad != NULL)
	{
		pogInitialLoad->DestroyWindow();
		pogInitialLoad = NULL;
	}


	CCS_CATCH_ALL

	//olTime = CTime::GetCurrentTime();
	//TRACE("\n\nend    InitialLoad  %s", olTime.Format("%H:%M:%S"));

}





void CFPMSApp::Customize()
{
	
	//!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	//
	// in den Projektsettings die Sprache ggf. umstellen  ( Englisch(UK) und _ENGLISH )
	//
	//!!!!!!!!!!!!!!!!!!!!!!!!!!!!


	// Deutsche Versionen !!!

		// Hannover Langenhagen

		//ogAppName	= "FPMS";
		//ogCustomer	= "HAJ";

		// POPS f�r FAG   --> hier sind Anpassungen vorhanden !!! 

		//ogAppName	= "POPS";
		//ogCustomer	= "BVD";


	// Englische Versionen !!!


		// FIPS-MASTERVERSION  

		//ogAppName	= "FIPS";
		//ogCustomer	= "";

	
		// FIPS-MASTERVERSION  

		ogAppName	= "FIPS";
		ogCustomer	= "SHA";


		// FIPS f�r Lissabon 

//		ogAppName	= "FIPS";
//		ogCustomer	= "LIS";


	//---------------------------------------------------------------------------
}





bool CFPMSApp::ShowFlightRecordData(CWnd *opParent, char cpFTyp, long lpUrno, long lpRkey, char cpAdid, const CTime &ropTifad, bool bpLocalTimes, char cpMask)
{

	if (cpFTyp == 'T' || cpFTyp == 'G')
	{
		// ACTIVATE TOWING-DIALOG
		if(pogRotGroundDlg == NULL)
		{
			pogRotGroundDlg = new RotGroundDlg(opParent);
			pogRotGroundDlg->Create(IDD_ROTGROUND);
		}

		pogRotGroundDlg->SetWindowPos(&opParent->wndTop, 0,0,0,0, SWP_SHOWWINDOW | SWP_NOSIZE | SWP_NOMOVE );
		pogRotGroundDlg->NewData(opParent, lpRkey, lpUrno, cpAdid, bpLocalTimes);
	}
	else
	{
		// ACTIVATE ROTATION-MASK
		bool blSeasonMask = true;
		switch (cpMask)
		{
		case 'S':
			blSeasonMask = true;
			break;
		case 'D':
			blSeasonMask = false;
			break;
		default:
			{
				// decide with the given time if daily- or season-rotation-mask shall be activated		
				CTime olCurrTime = CTime::GetCurrentTime();
				// SeasonMask if the given time is at least 2 days in the future
				if (ropTifad - CTimeSpan(2,0,0,0) < CTime::GetCurrentTime())
				{	
					blSeasonMask = false;
				}
				else
				{
					blSeasonMask = true;
				}
			}
			break;
		}

		if (blSeasonMask)
		{
			pogSeasonDlg->NewData(opParent, lpRkey, lpUrno, DLG_CHANGE, bpLocalTimes, cpAdid);
		}
		else
		{
 			pogRotationDlg->NewData(opParent, lpRkey, lpUrno, cpAdid, bpLocalTimes);
		}
	}

	return true;
}


bool CFPMSApp::SetCustomerConfig()
{
	// Configuration for Shanghai (new Airport: Pudong)
	if (strcmp(pcgHome, "PVG") == 0)
	{
		// no offline/release functions!
		//bgOffRelFuncGatpos = false;
		//bgOffRelFuncCheckin =	false;
	}

	// Configuration for Athens
	if (strcmp(pcgHome, "ATH") == 0)
	{
		// enable additional baggage belt check
		bgExtBltCheck = true;
		// enable yellow bars when flight is international
		bgFlightIntYelBar = true;
	}


	return true;
}
 

int CFPMSApp::MyTopmostMessageBox(CWnd *polWnd, const CString &ropText, const CString &ropCaption, UINT ipType /* = 0 */)
{
	HWND hWnd = NULL;
	if (polWnd != NULL)
		hWnd = polWnd->m_hWnd;

	UnsetTopmostWnds();
	int ilRet = ::MessageBox(hWnd, ropText, ropCaption, ipType | MB_APPLMODAL | MB_TOPMOST);
	SetTopmostWnds();

//	int ilRet = ::MessageBox(hWnd, ropText, ropCaption, ipType | MB_APPLMODAL | MB_SETFOREGROUND);
	return ilRet;
}


void CFPMSApp::UnsetTopmostWnds()
{
	if(pogWoResTableDlg)
		pogWoResTableDlg->SetWindowPos( &pogWoResTableDlg->wndNoTopMost, 0,0,0,0, SWP_NOSIZE  | SWP_NOMOVE);
}


void CFPMSApp::SetTopmostWnds()
{
	if(pogWoResTableDlg)
		pogWoResTableDlg->SetWindowPos( &pogWoResTableDlg->wndTopMost  , 0,0,0,0, SWP_NOSIZE  | SWP_NOMOVE);
}


bool CFPMSApp::DumpDebugLog(const CString &ropStr)
{

	if (!bgDebug) return false;

	CTime olCurrTime = CTime::GetCurrentTime();
	of_debug << olCurrTime.Format("%d.%m.%Y %H:%M:%S:  ") << ropStr << endl;

	return true;
}


bool CFPMSApp::CheckCedaError(const CCSCedaData &ropCedaData)
{
	if (ropCedaData.imLastReturnCode != ropCedaData.RC_SUCCESS)
	{
		CString olMsg;
		// Dump into logfile
		olMsg.Format("ERROR: Ceda reports: '%s'", ropCedaData.omLastErrorMessage);
		DumpDebugLog(olMsg);

		// display message
		olMsg = CString("CEDA reports:\n\n") + ropCedaData.omLastErrorMessage;
		//CFPMSApp::MyTopmostMessageBox(pogButtonList, olMsg, GetString(ST_FEHLER), MB_ICONERROR);

		return false;
	}
	return true;
}






