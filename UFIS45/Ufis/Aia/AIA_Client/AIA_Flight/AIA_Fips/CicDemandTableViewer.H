#ifndef __CicDemandTableViewer_H__

#include "stdafx.h"
#include "Fpms.h"
#include "CCSGlobl.h"
#include "CCSTable.h"
#include "DiaCedaFlightData.h"
#include "CViewer.h"

/////////////////////////////////////////////////////////////////////////////
// Record structure declaration

struct CICDEMANDTABLE_LINEDATA
{
	long Urno;
	long FlightUrno;
	long KKey;
	CString		Flno;
	CTime		Sto;
	CTime		Sto2;
	CString		Act3;
	CString		OrgDes;
	CTime		Ckbs;
	CTime		Ckes;
	CString     Ckic;
	CString		Nose;
	CString		Ftyp;
	CString		Remark;
	CString		CounterGroup;
	bool	    Error;	
	int			Freq;

	CICDEMANDTABLE_LINEDATA(void)
	{ 
		FlightUrno = 0;
		KKey = 0;
		Ckbs = -1;
		Ckes = -1;
		Urno = 0;
		Sto = -1;
		Sto2= -1;
		Freq = 0;
		Error = false;
	}
};



/////////////////////////////////////////////////////////////////////////////
// CicDemandTableViewer

/////////////////////////////////////////////////////////////////////////////
// Class declaration of CicDemandTableViewer

//@Man:
//@Memo: CicDemandTableViewer
//@See:  STDAFX, CCSCedaData, CedaDIAFLIGHTDATA, CCSPTRARRAY,  CCSTable
/*@Doc:
  No comment on this up to now.
*/
class CicDemandTableViewer : public CViewer
{
// Constructions
public:
    //@ManMemo: Default constructor
    CicDemandTableViewer();
    //@ManMemo: Default destructor
    ~CicDemandTableViewer();

	void UnRegister();

	void ClearAll();

    //@ManMemo: Attach
	void Attach(CCSTable *popAttachWnd);
    //void Attach(CTable *popAttachWnd);
	/* void ChangeViewTo(const char *pcpViewName, CString opDate); */
    //@ManMemo: ChangeViewTo
    void ChangeViewTo(const char *pcpViewName);
    int GetDemCount();
    //@ManMemo: omLines
    CCSPtrArray<CICDEMANDTABLE_LINEDATA> omLines;
// Internal data processing routines
private:
//MWO
	bool IsPassFilter(DIACCADATA *prpCca);

	bool IsPassFilter(CCSPtrArray<DIACCADATA> &opCcas);

	int  CompareFlight(CICDEMANDTABLE_LINEDATA *prpFlight1, CICDEMANDTABLE_LINEDATA *prpFlight2);
	
	int CompareLines(CICDEMANDTABLE_LINEDATA *prpLine1, CICDEMANDTABLE_LINEDATA *prpLine2);
	
	bool DeleteLine(long lpUrno);
//MWO END


	void MakeLines();
	
	void MakeLineData(CICDEMANDTABLE_LINEDATA *prpLineData, DIACCADATA *prpCca);
	void MakeLineData(CICDEMANDTABLE_LINEDATA *prpLineData, CCSPtrArray<DIACCADATA> &opCcas);


	bool MakeLine(DIACCADATA *prpCca);
	bool MakeLine( CCSPtrArray<DIACCADATA> &opCcas);

	
	void MakeColList(CICDEMANDTABLE_LINEDATA *prlLine, CCSPtrArray<TABLE_COLUMN> &olColList);
	
	int  CreateLine(CICDEMANDTABLE_LINEDATA &rpLine);

	bool FindLine(long lpUrno, int &rilLineno);
	
	void SelectLine(long ilCurrUrno);

	void InsertDisplayLine( int ipLineNo);

	void UtcToLocal(CICDEMANDTABLE_LINEDATA &rrpLineData);


// Operations
public:
    //@ManMemo: DeleteAll
	void DeleteAll();
    //@ManMemo: DeleteLine
	void DeleteLine(int ipLineno);
 
// Window refreshing routines
public:
	void UpdateDisplay();

	void DrawHeader();


	void ProcessCcaDelete(long *plpCcaUrno);
	void ProcessCcaChange(long *plpCcaUrno);
	void ProcessKKeyChange(long *plpCcaUrno);


	

	bool bmInit;


private:
	CCSTable *pomTable;


// Methods which handle changes (from Data Distributor)
public:


};

#endif //__CicDemandTableViewer_H__
