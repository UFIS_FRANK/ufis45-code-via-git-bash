#if !defined(AFX_SORTFLIGHTPAGE_H__D5E00FB1_5034_11D1_9883_000001014864__INCLUDED_)
#define AFX_SORTFLIGHTPAGE_H__D5E00FB1_5034_11D1_9883_000001014864__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// SortFlightPage.h : header file
//

#include "Ansicht.h"
//#include "CCSBasePSPage.h"
//#include "CCSPageBuffer.h"
/////////////////////////////////////////////////////////////////////////////
// CSortFlightPage dialog

class CSortFlightPage : public CPropertyPage
{
	DECLARE_DYNCREATE(CSortFlightPage)

// Construction
public:
	CSortFlightPage();
	~CSortFlightPage();

	CStringArray omValues;
   CStringArray omSortOrders;

// Dialog Data
	//{{AFX_DATA(CSortFlightPage)
	enum { IDD = IDD_PSSORT_FLIGHTPAGE };
		// NOTE - ClassWizard will add data members here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	BOOL	m_Group;
	int		m_SortOrder0;
	int		m_SortOrder1;
	int		m_SortOrder2;
	//}}AFX_DATA
// Overrides
	// ClassWizard generate virtual function overrides
	//{{AFX_VIRTUAL(CSortFlightPage)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnCommand(WPARAM wParam, LPARAM lParam);
	//}}AFX_VIRTUAL
//   void ResetBufferPtr(){CBasePropSheetPage::ResetBufferPtr(); pomPageBuffer = NULL;};
//   void *GetData(); // { return (void *)pomPageBuffer;};
//   void InitialScreen();
   CString GetPrivList();

// Implementation
protected:

//   CPageBuffer *pomPageBuffer;
	// Generated message map functions
	//{{AFX_MSG(CSortFlightPage)
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

// Helper routines
private:
	int GetSortOrder(const char *pcpSortKey);
	CString GetSortKey(int ipSortOrder);
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SORTFLIGHTPAGE_H__D5E00FB1_5034_11D1_9883_000001014864__INCLUDED_)
