// BltCcaTableDlg.cpp : implementation file
//

#include "stdafx.h"
#include "fpms.h"
#include "resrc1.h"
#include "BltCcaTableDlg.h"
#include "SeasonDlg.h"
#include "RotationDlg.h"
#include "DiaCedaFlightData.h"
#include "Utils.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// BltCcaTableDlg dialog


BltCcaTableDlg::BltCcaTableDlg(CWnd* pParent /*=NULL*/)
	: CDialog(BltCcaTableDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(BltCcaTableDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	pomViewer = new BltCcaTableViewer(this);
	pomFlightData = NULL;
	imButtonSpace = 35;
	isCreated = false;
    CDialog::Create(BltCcaTableDlg::IDD, NULL);
	isCreated = true;
	m_key = "DialogPosition\\BaggageBeltCheckIn";

	CRect olRect;
	this->GetWindowRect(&olRect);
	OnSize(0, olRect.Width(), olRect.Height()); //Move buttons
	//omCcaData.Register();

}


BltCcaTableDlg::~BltCcaTableDlg()
{
	delete pomTable;
	delete pomViewer;
}


void BltCcaTableDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(BltCcaTableDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(BltCcaTableDlg, CDialog)
	//{{AFX_MSG_MAP(BltCcaTableDlg)
	ON_WM_SIZE()
	ON_WM_CLOSE()
    ON_MESSAGE(WM_TABLE_LBUTTONDBLCLK, OnTableLButtonDblclk)
	ON_BN_CLICKED(IDC_SAVE, OnSave)
	ON_BN_CLICKED(IDC_PRINT, OnPrint)
	ON_BN_CLICKED(IDCLOSE, OnClose)
 	ON_WM_DESTROY()
 	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// BltCcaTableDlg message handlers



void BltCcaTableDlg::Activate(void)
{
	// show window
	
	//SetWindowPos(&wndTop,0,0,0,0, SWP_SHOWWINDOW | SWP_NOSIZE | SWP_NOMOVE);
	// Rebuild table
	this->ShowWindow(SW_SHOW);
 	if (pomFlightData)
		pomViewer->ChangeViewTo(pomFlightData);

	UpdateCaption();
	// Resize table
	// (Its necessary for displaying the scroll-bars at the first time)
/*	CRect olRect;
    GetClientRect(&olRect);
	pomTable->SetPosition(1, olRect.Width()+1, 1 + imButtonSpace, olRect.Height()+1);
*/
}


void BltCcaTableDlg::UpdateCaption(void)
{
	// set caption
	CString olCaption;
	olCaption.Format(GetString(IDS_STRING1937), pomTable->GetLinesCount());
	// additional text for time mode
	CString olTimes;
	if (bgGatPosLocal)
		olTimes = GetString(IDS_STRING1921);
	else
		olTimes = GetString(IDS_STRING1920);
	olCaption += " ("+olTimes+")";
	SetWindowText(olCaption);
}


void BltCcaTableDlg::ReloadData(DiaCedaFlightData *popFlightData)
{
	pomFlightData = popFlightData;
	/*
	if (ropFlightData.omData.GetSize() < 1)
	{
		omCcaData.ClearAll(false);
		return;
	}
	*/
	//// load cca-data
	if (pomFlightData)
		pomFlightData->ReadAllCca();
	/*
	// load departure flights
	const int cilMaxWhere = 2048;	// Max. L�nge Suchstring f�r Cca Where Clause
	// kommagetrennten String der RKeys aufbauen
	int ilCount = ropFlightData.omData.GetSize() ;
	CString olRkeys;
	for ( int ilLc = 0; ilLc < ilCount; ilLc++)
	{
		if( ropFlightData.omData[ilLc].Rkey > 0)
		{
			CString olNextRkey ;
			olNextRkey.Format("%ld", ropFlightData.omData[ilLc].Rkey ) ; 
			if( ! olRkeys.IsEmpty() ) 
				olRkeys += "," ;
			olRkeys += olNextRkey; 
		}
	}




	// kommagetrennte Rkey-Liste ( je 100 Eintr�ge ) aufbauen
	CStringArray olRkeyList ;
	ilCount = SplitItemList(olRkeys, &olRkeyList, 100) ;
	CString olSelection;
	char olCcaWhere[cilMaxWhere] ;	 // cimMaxWhere h�ngt von Eintr�ge (Para. 3) in SplitItemList( ..., 100 ) ab !!
	for( ilLc = 0; ilLc < ilCount; ilLc++)
	{
		olSelection.Format("WHERE CKIC <> ' ' AND FLNU IN (SELECT URNO FROM AFT%s WHERE RKEY IN (", pcgTableExt);
		olSelection += olRkeyList[ilLc] + CString("))");
		ASSERT( olSelection.GetLength() < cilMaxWhere ) ;
		strcpy(olCcaWhere, olSelection);
		// nur fehlerfreie einlesen
		if (ilLc == 0)
			omCcaData.ClearAll(false);
		omCcaData.Read( olCcaWhere, false, false);
	}
	*/
 	
}



BOOL BltCcaTableDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	m_key = "DialogPosition\\BaggageBeltCheckIn"; 

/*
	CPoint point;
	::GetCursorPos(&point);
	this->SetWindowPos(&wndTop, point.x, point.y,0,0, SWP_HIDEWINDOW | SWP_NOSIZE);// | SWP_NOMOVE );
*/
	CRect olRect;
	this->GetWindowRect(&olRect);
	GetDialogFromReg(olRect, m_key);
	SetWindowPos(&wndTop, olRect.left, olRect.top, olRect.Width(), olRect.Height(), SWP_HIDEWINDOW);// | SWP_NOMOVE );

	// create table
	pomTable = new CCSTable;
	pomTable->SetSelectMode(LBS_MULTIPLESEL | LBS_EXTENDEDSEL);
	pomTable->SetHeaderSpacing(0);
	
    GetClientRect(&olRect);
	imMinDlgWidth = olRect.Width();

    olRect.InflateRect(1, 1);     // hiding the CTable window border
    
	pomTable->SetTableData(this, olRect.left, olRect.right, olRect.top + imButtonSpace, olRect.bottom);

	pomViewer->Attach(pomTable);

	// Rebuild table
	pomViewer->ChangeViewTo(NULL);


	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void BltCcaTableDlg::SaveToReg()
{
	BOOL ok = WriteDialogToReg((CWnd*) this, m_key);
}


void BltCcaTableDlg::OnDestroy() 
{
	SaveToReg();
	CDialog::OnDestroy();
}


void BltCcaTableDlg::OnSize(UINT nType, int cx, int cy) 
{
	if(isCreated != false)
	{

		{
			CDialog::OnSize(nType, cx, cy);
	
			if (nType != SIZE_MINIMIZED)
			{
				// resize table
				pomTable->SetPosition(1, cx+1, 1 + imButtonSpace, cy+1);

				// repositioning close-button
				if (cx > 300)
				{
					CWnd *polCloseB = GetDlgItem(IDCLOSE);
					if (polCloseB != NULL)
					{
						CRect olRect;
						polCloseB->GetClientRect(&olRect);
						polCloseB->MoveWindow(cx - olRect.Width() - 5, 8, olRect.Width(), olRect.Height());
					}
				}
			}
		}
	}
}



void BltCcaTableDlg::OnClose() 
{
	// only hide window
	SaveToReg();
	ShowWindow(SW_HIDE);	

	pomViewer->UnRegister();

	//CDialog::OnClose();
}


void BltCcaTableDlg::OnSave() 
{

	// Get filename
	LPOPENFILENAME polOfn = new OPENFILENAME;
	char buffer[256] = "";
	char buffer2[256] = "";
	LPSTR lpszStr;
	lpszStr = buffer;
	CString olStr = CString("c:\\tmp\\") + CString("*.xls");
	strcpy(buffer,(LPCTSTR)olStr);

	memset(polOfn, 0, sizeof(*polOfn));
	polOfn->lStructSize = sizeof(*polOfn) ;
	polOfn->lpstrFilter = "Export File(*.xls)\0*.xls\0";
	polOfn->lpstrFile = (LPSTR) buffer;
	polOfn->nMaxFile = 256;
	strcpy(buffer2, GetString(IDS_STRING1077));
	polOfn->lpstrTitle = buffer2;

	if(GetOpenFileName(polOfn) == TRUE)
	{
		CString olDefPath(polOfn->lpstrFile);
		if (olDefPath.Find('.') == -1)
			olDefPath += ".xls";
 
		// Print to file
		pomViewer->PrintPlanToFile(olDefPath);
	}

	delete polOfn;

}


void BltCcaTableDlg::OnPrint() 
{
	pomViewer->PrintTableView();
}

 
////////////////////////////////////////////////////////////////////////////
// linke Maustaste auf der Flighttable gedr�ckt
//
LONG BltCcaTableDlg::OnTableLButtonDblclk( UINT wParam, LPARAM lParam)
{
	if (!pomFlightData) return 0L;

	UINT ipItem = wParam;
	const BLTCCATABLE_LINEDATA *prlTableLine = NULL;
	// get actual table line
	prlTableLine = (BLTCCATABLE_LINEDATA *)pomTable->GetTextLineData(ipItem);
	if (prlTableLine != NULL)
	{
		// get flight data
		const DIAFLIGHTDATA *prlFlight = pomFlightData->GetFlightByUrno(prlTableLine->FUrno);

		if(prlFlight != NULL)
		{
			// show Daily-Rotation Mask			
 			CString olAdid;
			if(strcmp(prlFlight->Des3, pcgHome) == 0)
			{
 				olAdid = "A";
			}
			else
			{
 				olAdid = "D";
			}

  			pogRotationDlg->NewData(this, prlFlight->Rkey, prlFlight->Urno, olAdid, bgGatPosLocal);
		}
	}
	return 0L;
}


