// HAIDlg.cpp : implementation file
//

#include "stdafx.h"
#include "fpms.h"
#include "CCSGlobl.h"
#include "CedaBasicData.h"
#include "AwBasicDataDlg.h"
#include "HAIDlg.h"
#include "resrc1.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


static void FlightTableCf(void *popInstance, int ipDDXType,
                          void *vpDataPointer, CString &ropInstanceName);


/////////////////////////////////////////////////////////////////////////////
// HAIDlg dialog


HAIDlg::HAIDlg(CWnd* pParent, char opType, char opMode, CString opUrno)
	   :CDialog(HAIDlg::IDD, pParent),
	    omUrno(opUrno),
		omMode(opMode),
		omType(opType)
{
	//{{AFX_DATA_INIT(HAIDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	omUrnoA = "";
	omUrnoD = "";

	ogDdx.Register(this, BCD_HAI_UPDATE, CString("ROTATIONDLG_HAI"), CString("Hai Update"), FlightTableCf);
	ogDdx.Register(this, BCD_HAI_DELETE, CString("ROTATIONDLG_HAI"), CString("Hai Delete"), FlightTableCf);
	ogDdx.Register(this, BCD_HAI_INSERT, CString("ROTATIONDLG_HAI"), CString("Hai Insert"), FlightTableCf);

}


HAIDlg::~HAIDlg()
{

	ogDdx.UnRegister(this, NOTUSED);


}


void HAIDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(HAIDlg)
	DDX_Control(pDX, IDOK, m_OK);
	DDX_Control(pDX, IDC_CDAT_D, m_CdatD);
	DDX_Control(pDX, IDC_CDAT_T, m_CdatT);
	DDX_Control(pDX, IDC_LSTU_D, m_LstuD);
	DDX_Control(pDX, IDC_LSTU_T, m_LstuT);
	DDX_Control(pDX, IDC_HSNA, m_HSNA);
	DDX_Control(pDX, IDC_TASK, m_TASK);
	DDX_Control(pDX, IDC_REMA, m_REMA);
	DDX_Control(pDX, IDC_USEC, m_Usec);
	DDX_Control(pDX, IDC_USEU, m_Useu);
	DDX_Check(pDX, IDC_CH_ARRIVAL, m_CH_Arrival);
	DDX_Check(pDX, IDC_CH_DEPARTURE, m_CH_Departure);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(HAIDlg, CDialog)
	//{{AFX_MSG_MAP(HAIDlg)
	ON_BN_CLICKED(IDC_CH_ARRIVAL, OnChArrival)
	ON_BN_CLICKED(IDC_CH_DEPARTURE, OnChDeparture)
	ON_BN_CLICKED(IDC_HSNALIST, OnHsnalist)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// HAIDlg message handlers

BOOL HAIDlg::OnInitDialog() 
{

	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	//ogBCD.Read(CString("HAI"));

	CString olTemp;

	m_CdatD.SetBKColor(SILVER);
	olTemp = ogBCD.GetField("HAI", "URNO", omUrno, "CDAT");
	m_CdatD.SetInitText(DBStringToDateTime(olTemp).Format("%d.%m.%Y"));
	m_CdatT.SetBKColor(SILVER);
	m_CdatT.SetInitText(DBStringToDateTime(olTemp).Format("%H:%M"));

	m_LstuD.SetBKColor(SILVER);
	olTemp = ogBCD.GetField("HAI", "URNO", omUrno, "LSTU");
	m_LstuD.SetInitText(DBStringToDateTime(olTemp).Format("%d.%m.%Y"));
	m_LstuT.SetBKColor(SILVER);
	m_LstuT.SetInitText(DBStringToDateTime(olTemp).Format("%H:%M"));

	m_Usec.SetBKColor(SILVER);
	m_Usec.SetInitText(ogBCD.GetField("HAI", "URNO", omUrno, "USEC"));

	m_Useu.SetBKColor(SILVER);
	m_Useu.SetInitText(ogBCD.GetField("HAI", "URNO", omUrno, "USEU"));

	m_HSNA.SetFormat("x|#x|#x|#x|#x|#");
	m_HSNA.SetTextLimit(1,5);
	m_HSNA.SetTextErrColor(RED);
	m_HSNA.SetBKColor(YELLOW);
	m_HSNA.SetInitText(ogBCD.GetField("HAI", "URNO", omUrno, "HSNA"));

	m_TASK.SetTypeToString("X(127)",127,1);
	m_TASK.SetTextErrColor(RED);
	m_TASK.SetInitText(ogBCD.GetField("HAI", "URNO", omUrno, "TASK"));

	m_REMA.SetTypeToString("X(254)",254,1);
	m_REMA.SetTextErrColor(RED);
	m_REMA.SetInitText(ogBCD.GetField("HAI", "URNO", omUrno, "REMA"));

	CButton* polCHArrival   = (CButton*)GetDlgItem(IDC_CH_ARRIVAL);
	CButton* polCHDeparture = (CButton*)GetDlgItem(IDC_CH_DEPARTURE);

	switch (omType)
	{
		case 'A':
			polCHArrival->EnableWindow(false);
			polCHDeparture->EnableWindow(false);
			polCHArrival->SetCheck(1);
			polCHDeparture->SetCheck(0);
		break;
		case 'D':
			polCHArrival->EnableWindow(false);
			polCHDeparture->EnableWindow(false);
			polCHArrival->SetCheck(0);
			polCHDeparture->SetCheck(1);
		break;
		case 'R':
			polCHArrival->EnableWindow(true);
			polCHDeparture->EnableWindow(true);
			m_CH_Arrival   = false;
			m_CH_Departure = false;;
		break;
		default:
		break;
	}
	
	if( omMode == 'U')
	{
		CString olFlnu = ogBCD.GetField("HAI", "URNO", omUrno, "FLNU");
		if (olFlnu == omUrnoA)
		{
			m_CH_Arrival   = true;
			m_CH_Departure = false;;
		}
		if  (olFlnu == omUrnoD)
		{
			m_CH_Arrival   = false;
			m_CH_Departure = true;
		}

		polCHArrival->EnableWindow(false);
		polCHDeparture->EnableWindow(false);

		UpdateData(false);
	}

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}


void HAIDlg::OnCancel() 
{
	// TODO: Add extra cleanup here
	
	CDialog::OnCancel();
}


void HAIDlg::OnOK() 
{

	CCS_TRY

	UpdateData(true);

	AfxGetApp()->DoWaitCursor(1);
	CString olErrorText;
	bool ilStatus = true;
	if(m_HSNA.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += GetString(ST_EINGABEFELD) + "HSNA\" " + GetString(ST_BADFORMAT);
	}
/*	if(m_TASK.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += GetString(ST_EINGABEFELD) + "TASK\" " + GetString(ST_BADFORMAT);
	}
	if(m_REMA.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += GetString(ST_EINGABEFELD) + "REMA\" " + GetString(ST_BADFORMAT);
	}*/
	if (!m_CH_Arrival && !m_CH_Departure)
	{
		ilStatus = false;
		olErrorText += GetString(ST_EINGABEFELD) + "Arrival/Departure\" " + GetString(ST_BADFORMAT);
	}


	AfxGetApp()->DoWaitCursor(-1);
	if (ilStatus == true)
	{
		CString olFlnu;
		CString olHSNA;
		CString olTASK;
		CString olREMA;
		olFlnu = ogBCD.GetField("HAI", "URNO", omUrno, "FLNU");
		m_HSNA.GetWindowText(olHSNA);
		m_TASK.GetWindowText(olTASK);
		m_REMA.GetWindowText(olREMA);

		switch (omMode)
		{
			case 'U':	// UPDATE
			{
				CStringArray olTmp;
				if (omType == 'A' || omType == 'D')
				{
					ogBCD.SetField("HAI", "URNO", omUrno, "HSNA", olHSNA, false);
					ogBCD.SetField("HAI", "URNO", omUrno, "TASK", olTASK, false);
					ogBCD.SetField("HAI", "URNO", omUrno, "REMA", olREMA, false);
					ogBCD.SetField("HAI", "URNO", omUrno, "FLNU", olFlnu, false);
					ogBCD.Save("HAI");
				}
				else
				{
					// update nur Arrival, vorher auch Arrival
					if (m_CH_Arrival && !m_CH_Departure && omUrnoA == olFlnu)
					{
						ogBCD.SetField("HAI", "URNO", omUrno, "HSNA", olHSNA, false);
						ogBCD.SetField("HAI", "URNO", omUrno, "TASK", olTASK, false);
						ogBCD.SetField("HAI", "URNO", omUrno, "REMA", olREMA, false);
						ogBCD.SetField("HAI", "URNO", omUrno, "FLNU", olFlnu, false);
						ogBCD.Save("HAI");

					}
					// update nur Departure, vorher auch Departure
					if (!m_CH_Arrival && m_CH_Departure && omUrnoD == olFlnu)
					{
						ogBCD.SetField("HAI", "URNO", omUrno, "HSNA", olHSNA, false);
						ogBCD.SetField("HAI", "URNO", omUrno, "TASK", olTASK, false);
						ogBCD.SetField("HAI", "URNO", omUrno, "REMA", olREMA, false);
						ogBCD.SetField("HAI", "URNO", omUrno, "FLNU", olFlnu, false);
						ogBCD.Save("HAI");
					}
					// update, statt vorher Departure nun Arrival
					if (m_CH_Arrival && !m_CH_Departure && omUrnoD == olFlnu)
					{
						ogBCD.SetField("HAI", "URNO", omUrno, "HSNA", olHSNA, false);
						ogBCD.SetField("HAI", "URNO", omUrno, "TASK", olTASK, false);
						ogBCD.SetField("HAI", "URNO", omUrno, "REMA", olREMA, false);
						ogBCD.SetField("HAI", "URNO", omUrno, "FLNU", omUrnoA, false);
						ogBCD.Save("HAI");
					}
					// update, statt vorher Arrival nun Departure
					if (!m_CH_Arrival && m_CH_Departure && omUrnoA == olFlnu)
					{
						ogBCD.SetField("HAI", "URNO", omUrno, "HSNA", olHSNA, false);
						ogBCD.SetField("HAI", "URNO", omUrno, "TASK", olTASK, false);
						ogBCD.SetField("HAI", "URNO", omUrno, "REMA", olREMA, false);
						ogBCD.SetField("HAI", "URNO", omUrno, "FLNU", omUrnoD, false);
						ogBCD.Save("HAI");
					}
					// vorher Arrival, nun Arrival und Departure
					if (m_CH_Arrival && m_CH_Departure && omUrnoA == olFlnu)
					{
						olTmp.Add(olHSNA);
						ogBCD.SetField("HAI", "URNO", omUrno, "HSNA", olHSNA, false);
						ogBCD.SetField("HAI", "URNO", omUrno, "TASK", olTASK, false);
						ogBCD.SetField("HAI", "URNO", omUrno, "REMA", olREMA, false);
						ogBCD.SetField("HAI", "URNO", omUrno, "FLNU", omUrnoA, false);
						ogBCD.Save("HAI");
						// Departure anlegen
						RecordSet olRecordSetInsert(ogBCD.GetFieldCount("HAI"));				
						olRecordSetInsert[ogBCD.GetFieldIndex("HAI", "HSNA")] = olHSNA;
						olRecordSetInsert[ogBCD.GetFieldIndex("HAI", "TASK")] = olTASK;
						olRecordSetInsert[ogBCD.GetFieldIndex("HAI", "REMA")] = olREMA;
						olRecordSetInsert[ogBCD.GetFieldIndex("HAI", "FLNU")] = omUrnoD;
						ogBCD.InsertRecord("HAI", olRecordSetInsert, true);
					}
					// vorher Departure, nun Departure und Arrival
					if (m_CH_Arrival && m_CH_Departure && omUrnoD == olFlnu)
					{
						olTmp.Add(olHSNA);
						ogBCD.SetField("HAI", "URNO", omUrno, "HSNA", olHSNA, false);
						ogBCD.SetField("HAI", "URNO", omUrno, "TASK", olTASK, false);
						ogBCD.SetField("HAI", "URNO", omUrno, "REMA", olREMA, false);
						ogBCD.SetField("HAI", "URNO", omUrno, "FLNU", omUrnoD, false);
						ogBCD.Save("HAI");
						// Departure anlegen
						RecordSet olRecordSetInsert(ogBCD.GetFieldCount("HAI"));				
						olRecordSetInsert[ogBCD.GetFieldIndex("HAI", "HSNA")] = olHSNA;
						olRecordSetInsert[ogBCD.GetFieldIndex("HAI", "TASK")] = olTASK;
						olRecordSetInsert[ogBCD.GetFieldIndex("HAI", "REMA")] = olREMA;
						olRecordSetInsert[ogBCD.GetFieldIndex("HAI", "FLNU")] = omUrnoA;
						ogBCD.InsertRecord("HAI", olRecordSetInsert, true);
					}
				}
			}
			break;

			case 'I':	// INSERT
			{
				if (omType == 'A')
				{
					RecordSet olRecordSetInsert(ogBCD.GetFieldCount("HAI"));				
					olRecordSetInsert[ogBCD.GetFieldIndex("HAI", "HSNA")] = olHSNA;
					olRecordSetInsert[ogBCD.GetFieldIndex("HAI", "TASK")] = olTASK;
					olRecordSetInsert[ogBCD.GetFieldIndex("HAI", "REMA")] = olREMA;
					olRecordSetInsert[ogBCD.GetFieldIndex("HAI", "FLNU")] = omUrnoA;
					ogBCD.InsertRecord("HAI", olRecordSetInsert, true);
				}
				if (omType == 'D')
				{
					RecordSet olRecordSetInsert(ogBCD.GetFieldCount("HAI"));				
					olRecordSetInsert[ogBCD.GetFieldIndex("HAI", "HSNA")] = olHSNA;
					olRecordSetInsert[ogBCD.GetFieldIndex("HAI", "TASK")] = olTASK;
					olRecordSetInsert[ogBCD.GetFieldIndex("HAI", "REMA")] = olREMA;
					olRecordSetInsert[ogBCD.GetFieldIndex("HAI", "FLNU")] = omUrnoD;
					ogBCD.InsertRecord("HAI", olRecordSetInsert, true);

				}
				if (omType == 'R' && m_CH_Arrival)
				{
					RecordSet olRecordSetInsert(ogBCD.GetFieldCount("HAI"));				
					olRecordSetInsert[ogBCD.GetFieldIndex("HAI", "HSNA")] = olHSNA;
					olRecordSetInsert[ogBCD.GetFieldIndex("HAI", "TASK")] = olTASK;
					olRecordSetInsert[ogBCD.GetFieldIndex("HAI", "REMA")] = olREMA;
					olRecordSetInsert[ogBCD.GetFieldIndex("HAI", "FLNU")] = omUrnoA;
					ogBCD.InsertRecord("HAI", olRecordSetInsert, true);
				}
				if (omType == 'R' && m_CH_Departure)
				{
					RecordSet olRecordSetInsert(ogBCD.GetFieldCount("HAI"));				
					olRecordSetInsert[ogBCD.GetFieldIndex("HAI", "HSNA")] = olHSNA;
					olRecordSetInsert[ogBCD.GetFieldIndex("HAI", "TASK")] = olTASK;
					olRecordSetInsert[ogBCD.GetFieldIndex("HAI", "REMA")] = olREMA;
					olRecordSetInsert[ogBCD.GetFieldIndex("HAI", "FLNU")] = omUrnoD;
					ogBCD.InsertRecord("HAI", olRecordSetInsert, true);
				}
			}
			break;

			default:
			break;
		}
		//ogBCD.Save("HAI");
		CDialog::OnOK();
	}
	else
	{
		Beep(440,70);
		MessageBox(olErrorText, GetString(ST_HINWEIS), MB_ICONEXCLAMATION);
		m_HSNA.SetFocus();
		m_HSNA.SetSel(0,-1);
	}

	CCS_CATCH_ALL

}


void HAIDlg::OnHsnalist() 
{

	CCS_TRY

	AwBasicDataDlg *polDlg = new AwBasicDataDlg(this, "HAG","HSNA,HNAM", "HSNA+");
	if(polDlg->DoModal() == IDOK)
	{
		m_HSNA.SetInitText(polDlg->GetField("HSNA"), true);	
	}
	delete polDlg;

	CCS_CATCH_ALL

}


void HAIDlg::OnChDeparture()
{

	// TODO: Add your control notification handler code here

}


void HAIDlg::OnChArrival()
{

	// TODO: Add your control notification handler code here

}


void HAIDlg::SetAftTabUrnoA(CString opUrno)
{

	omUrnoA = opUrno;

}


void HAIDlg::SetAftTabUrnoD(CString opUrno)
{

	omUrnoD = opUrno;

}


void HAIDlg::InitDialog()
{

	//ogBCD.Read(CString("HAI"));
	
	CString olTemp;

	olTemp = ogBCD.GetField("HAI", "URNO", omUrno, "CDAT");
	m_CdatD.SetInitText(DBStringToDateTime(olTemp).Format("%d.%m.%Y"));
	m_CdatT.SetInitText(DBStringToDateTime(olTemp).Format("%H:%M"));

	olTemp = ogBCD.GetField("HAI", "URNO", omUrno, "LSTU");
	m_LstuD.SetInitText(DBStringToDateTime(olTemp).Format("%d.%m.%Y"));
	m_LstuT.SetInitText(DBStringToDateTime(olTemp).Format("%H:%M"));

	m_Usec.SetInitText(ogBCD.GetField("HAI", "URNO", omUrno, "USEC"));

	m_Useu.SetInitText(ogBCD.GetField("HAI", "URNO", omUrno, "USEU"));

	m_HSNA.SetInitText(ogBCD.GetField("HAI", "URNO", omUrno, "HSNA"));

	m_TASK.SetInitText(ogBCD.GetField("HAI", "URNO", omUrno, "TASK"));

	m_REMA.SetInitText(ogBCD.GetField("HAI", "URNO", omUrno, "REMA"));

}



static void FlightTableCf(void *popInstance, int ipDDXType,
                          void *vpDataPointer, CString &ropInstanceName)
{

    HAIDlg *polDlg = (HAIDlg*)popInstance;

	if (ipDDXType == BCD_VIP_UPDATE || ipDDXType == BCD_VIP_DELETE || ipDDXType == BCD_VIP_INSERT)
		polDlg->InitDialog();

}
