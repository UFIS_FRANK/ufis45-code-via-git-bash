#if !defined(AFX_ROTATIONGPUDLG_H__4F349052_D3DE_11D3_806D_00008638F9E1__INCLUDED_)
#define AFX_ROTATIONGPUDLG_H__4F349052_D3DE_11D3_806D_00008638F9E1__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// RotationGpuDlg.h : header file
//

#include "resrc1.h"
#include "CCSEdit.h"
#include "RotationDlgCedaFlightData.h"
 
/////////////////////////////////////////////////////////////////////////////
// RotationGpuDlg dialog

class RotationGpuDlg : public CDialog
{
// Construction
public:
	RotationGpuDlg(CWnd* pParent = NULL, ROTATIONDLGFLIGHTDATA *prpFlight = NULL);   // standard constructor


	ROTATIONDLGFLIGHTDATA *prmFlight;

// Dialog Data
	//{{AFX_DATA(RotationGpuDlg)
	enum { IDD = IDD_ROTATION_GPU };
	CCSEdit	m_CE_Gaae3;
	CCSEdit	m_CE_Gaae2;
	CCSEdit	m_CE_Gaae1;
	CCSEdit	m_CE_Gaab3;
	CCSEdit	m_CE_Gaab2;
	CCSEdit	m_CE_Gaab1;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(RotationGpuDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(RotationGpuDlg)
	virtual void OnOK();
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_ROTATIONGPUDLG_H__4F349052_D3DE_11D3_806D_00008638F9E1__INCLUDED_)
