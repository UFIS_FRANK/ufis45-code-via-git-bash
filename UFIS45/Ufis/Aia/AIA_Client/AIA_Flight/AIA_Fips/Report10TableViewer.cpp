// FlughaefenTableViewer.cpp 
//
//	Liste aller angeflogenen Flugh�fen mit Frequenz
#include "stdafx.h"
#include "Report10TableViewer.h"
#include "CcsGlobl.h"

#include <iomanip.h>
#include <iostream.h>
#include <fstream.h>


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif


// FlughaefenTableViewer
//
FlughaefenTableViewer::FlughaefenTableViewer(CCSPtrArray<ROTATIONDLGFLIGHTDATA> *popData, char *pcpInfo)
{
	pomData = popData;
	pcmInfo = pcpInfo;

	bmIsFromSearch = FALSE;
    pomFlughaefenTable = NULL;
}

//-----------------------------------------------------------------------------------------------

FlughaefenTableViewer::~FlughaefenTableViewer()
{
    DeleteAll();
	omHeaderDataArray.DeleteAll();
}

//-----------------------------------------------------------------------------------------------

void FlughaefenTableViewer::Attach(CCSTable *popTable)
{
    pomFlughaefenTable = popTable;
}

//-----------------------------------------------------------------------------------------------
void FlughaefenTableViewer::ChangeViewTo(const char *pcpViewName)
{
    DeleteAll();    
    MakeLines();
	UpdateDisplay();
}

//-----------------------------------------------------------------------------------------------

void FlughaefenTableViewer::MakeLines()
{
	int ilCntr = 0;
	CString olNewstr("");
	CString olOldstr(""); 

	int ilCount = pomData->GetSize();

	for (int ilLc = ilCount - 1; ilLc >= 0 ; ilLc--)
	{
		ilCntr++;
		ROTATIONDLGFLIGHTDATA *prlFlughaefenData = &pomData->GetAt(ilLc);
		olNewstr = prlFlughaefenData->Des3;

		if(ilLc == ilCount - 1)	// der erste
		{
			olOldstr = olNewstr;
		}
		if (olNewstr != olOldstr)	// Des3 wechselt
		{
			MakeLine(olOldstr, ilCntr);
			olOldstr = olNewstr;
			ilCntr = 0;
		}

		if((ilLc == 0) && (ilCntr > 0))	// nach dem letzten
			MakeLine(olNewstr, ilCntr);
	}
}

//-----------------------------------------------------------------------------------------------

void FlughaefenTableViewer::MakeLine(CString opDes3old, int ipCntr)
{
    // Update viewer data for this shift record
	char clbuffer[10];
    FLUGHAEFENTABLE_LINEDATA rlFlughaefen;
	sprintf(clbuffer,"%d", ipCntr);
	rlFlughaefen.Cntr = clbuffer; 
	rlFlughaefen.Des3 = opDes3old; 

	CreateLine(&rlFlughaefen);
}

//-----------------------------------------------------------------------------------------------

void FlughaefenTableViewer::CreateLine(FLUGHAEFENTABLE_LINEDATA *prpFlughaefen)
{
	int ilLineno = 0;
	FLUGHAEFENTABLE_LINEDATA rlFlughaefen;
	rlFlughaefen = *prpFlughaefen;
    omLines.NewAt(ilLineno, rlFlughaefen);
}

//-----------------------------------------------------------------------------------------------
// UpdateDisplay: Load data selected by filter conditions to the display by using "omTable"

void FlughaefenTableViewer::UpdateDisplay()
{
	int ilLines = omLines.GetSize();
	const int ilColumns = 2;	//<===== Spaltenanzahl setzen (von 0 ab)
	omHeaderDataArray.DeleteAll();

	pomFlughaefenTable->SetShowSelection(TRUE);
	pomFlughaefenTable->ResetContent();

	TABLE_HEADER_COLUMN rlHeader;
	rlHeader.Alignment = COLALIGN_LEFT;
	rlHeader.Font = &ogCourier_Regular_10;
	rlHeader.Length = 80; 
	rlHeader.Text = GetString(IDS_STRING990);
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 80;
	rlHeader.Text = GetString(IDS_STRING1138);
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);

	pomFlughaefenTable->SetHeaderFields(omHeaderDataArray);

	pomFlughaefenTable->SetDefaultSeparator();
	pomFlughaefenTable->SetTableEditable(FALSE);

	TABLE_COLUMN rlColumnData;
	rlColumnData.Font = &ogCourier_Regular_10;
	rlColumnData.BkColor = WHITE;
	rlColumnData.Alignment = COLALIGN_LEFT;
	
    for (int ilLineNo = 0; ilLineNo < ilLines; ilLineNo++)
	{
		CCSPtrArray<TABLE_COLUMN> olColList;

		rlColumnData.Text = omLines[ilLineNo].Des3;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Cntr;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		pomFlughaefenTable->AddTextLine(olColList,(void*)NULL);
		olColList.DeleteAll();
	}
	pomFlughaefenTable->DisplayTable();
}


//-----------------------------------------------------------------------------------------------

void FlughaefenTableViewer::DeleteLine(int ipLineno)
{
	omLines.DeleteAt(ipLineno);
}

//-----------------------------------------------------------------------------------------------

void FlughaefenTableViewer::DeleteAll()
{
    while (omLines.GetSize() > 0)
    DeleteLine(0);
}

//-----------------------------------------------------------------------------------------------
// Printing Routines
//-----------------------------------------------------------------------------------------------

void FlughaefenTableViewer::PrintTableView()
{
	CString olFooter1,olFooter2;
	char pclFooter[100];
	//sprintf(pclInfo, "Angeflogene Flugh�fen mit Frequenz %s", pcmInfo);
	sprintf(pclFooter, GetString(IDS_STRING1147), (CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M")));
	omTableName = GetString(IDS_STRING1148);
	omFooterName = pclFooter;
	int ilOrientation = PRINT_PORTRAET;//  PRINT_PORTRAET  PRINT_LANDSCAPE
	pomPrint = new CCSPrint((CWnd*)this,ilOrientation,45);// FirstLine(P=200,L=200),LeftOffset(P=200,L=50)

	if (pomPrint != NULL)
	{
		if (pomPrint->InitializePrinter(ilOrientation) == TRUE)
		{
			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
			//pomPrint->imLineNo = pomPrint->imMaxLines + 1;
			pomPrint->imLineNo = pomPrint->imMaxLines + 1 - 2;
			DOCINFO	rlDocInfo;
			memset(&rlDocInfo, 0, sizeof(DOCINFO));
			rlDocInfo.cbSize = sizeof( DOCINFO );
			rlDocInfo.lpszDocName = omTableName;	
			pomPrint->omCdc.StartDoc( &rlDocInfo );
			pomPrint->imPageNo = 0;
			int ilLines = omLines.GetSize();
			olFooter1.Format(GetString(IDS_STRING1445), ilLines,omFooterName);
			for(int i = 0; i < ilLines; i++ ) 
			{
				//if(pomPrint->imLineNo >= pomPrint->imMaxLines)
				if(pomPrint->imLineNo >= pomPrint->imMaxLines-2)
				{
					if(pomPrint->imPageNo > 0)
					{
						olFooter2.Format(GetString(IDS_STRING1198),pomPrint->imPageNo);
						pomPrint->PrintUIFFooter(olFooter1,"",olFooter2);
						pomPrint->omCdc.EndPage();
					}
					PrintTableHeader();
				}
				//if(pomPrint->imLineNo == (pomPrint->imMaxLines-1) || i == (ilLines-1))
				if(pomPrint->imLineNo == (pomPrint->imMaxLines-3) || i == (ilLines-1))
				{
					PrintTableLine(&omLines[i],true);
				}
				else
				{
					PrintTableLine(&omLines[i],false);
				}
			}
			olFooter2.Format(GetString(IDS_STRING1198),pomPrint->imPageNo);
			pomPrint->PrintUIFFooter(olFooter1,"",olFooter2);
			pomPrint->omCdc.EndPage();
			pomPrint->omCdc.EndDoc();
		}
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
	delete pomPrint;

	pomPrint = NULL;
	}
}

//-----------------------------------------------------------------------------------------------

bool FlughaefenTableViewer::PrintTableHeader()
{
	pomPrint->omCdc.StartPage();
	pomPrint->imPageNo++;
	pomPrint->imLineNo = 0;
	char pclHeader[100];
	//sprintf(pclHeader, "Angeflogene Flugh�fen mit Frequenz  vom %s", pcmInfo);
	sprintf(pclHeader, GetString(IDS_STRING1149), pcmInfo);
	CString olTableName(pclHeader);

	//pomPrint->PrintUIFHeader(omTableName,CString(CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M")),pomPrint->imFirstLine-10);
	pomPrint->PrintUIFHeader("",CString(pclHeader),pomPrint->imFirstLine-10);

	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;

	rlElement.Alignment   = PRINT_LEFT;
	rlElement.FrameLeft   = PRINT_NOFRAME;
	rlElement.FrameRight  = PRINT_NOFRAME;
	rlElement.FrameTop    = PRINT_NOFRAME;
	rlElement.FrameBottom = PRINT_FRAMEMEDIUM;
	rlElement.pFont       = &pomPrint->ogCourierNew_Bold_8;

	int ilSize = omHeaderDataArray.GetSize();
	for(int i=0;i<ilSize;i++)
	{
		rlElement.Length = (int)(omHeaderDataArray[i].Length*dgCCSPrintFactor);
		if(rlElement.Length < igCCSPrintMinLength)rlElement.Length+=igCCSPrintMoreLength; 
		rlElement.Text   = omHeaderDataArray[i].Text;
		rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
	}
	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();


	return true;
}

//-----------------------------------------------------------------------------------------------

bool FlughaefenTableViewer::PrintTableLine(FLUGHAEFENTABLE_LINEDATA *prpLine,bool bpLastLine)
{
	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;
	rlElement.pFont      = &pomPrint->ogCourierNew_Regular_8;
	int ilSize = omHeaderDataArray.GetSize();
	for(int i=0;i<ilSize;i++)
	{
		
		rlElement.Length = (int)(omHeaderDataArray[i].Length*dgCCSPrintFactor);
		if(rlElement.Length < igCCSPrintMinLength)rlElement.Length+=igCCSPrintMoreLength; 
		rlElement.Alignment  = PRINT_LEFT;
		rlElement.FrameTop   = PRINT_FRAMETHIN;
		if(bpLastLine == true)
		{
			rlElement.FrameBottom = PRINT_FRAMETHIN;
		}
		else
		{
			rlElement.FrameBottom = PRINT_NOFRAME;
		}
		rlElement.FrameLeft  = PRINT_NOFRAME;
		rlElement.FrameRight = PRINT_NOFRAME;
		switch(i)
		{
		case 0:
			{
				rlElement.FrameLeft  = PRINT_FRAMETHIN;
				rlElement.Text		= prpLine->Des3;
			}
			break;
		case 1:
			{
				rlElement.FrameRight = PRINT_FRAMETHIN;
				rlElement.Text		= prpLine->Cntr;
			}
			break;
		}
		rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
		
	}
	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();
	return true;
}

//-----------------------------------------------------------------------------------------------


// Drucken in Datei
void FlughaefenTableViewer::PrintPlanToFile(char *pcpDefPath)
{
	ofstream of;
	of.open( pcpDefPath, ios::out);
	of << GetString(IDS_STRING1148) << ": " 
		<< pcmInfo << "    "
		<< CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M") << endl << endl;

	of  << GetString(IDS_STRING990) << "   " 
		<< GetString(IDS_STRING1138) << "    " 
		<< endl;

	of << "---------------------------------------------------------------" << endl;
	int ilCount = omLines.GetSize();
	for(int i = 0; i < ilCount; i++)
	{
		FLUGHAEFENTABLE_LINEDATA rlD = omLines[i];
		of.setf(ios::left, ios::adjustfield);
		of   << setw(12) << rlD.Des3  
			 << setw(12) << rlD.Cntr << endl; 
	}
	of.close();
//	MessageBox("Es wurde folgende Datei geschrieben:\nc:\\tmp\\Flughaefen.txt", "Drucken",(MB_ICONINFORMATION|MB_OK));
}
