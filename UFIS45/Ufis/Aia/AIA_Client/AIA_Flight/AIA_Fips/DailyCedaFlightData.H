#ifndef _DAILY_CEDA_FLIGHT_DATA_H_
#define _DAILY_CEDA_FLIGHT_DATA_H_
 
#include "CCSGlobl.h"
#include "CCSDefines.h"
#include "DailyVipCedaData.h"
#include "CedaDiaCcaData.h"

//void ProcessFlightCf(void *vpInstance,enum enumDDXTypes ipDDXType, void *vpDataPointer, CString &ropInstanceName);




/////////////////////////////////////////////////////////////////////////////
// Record structure declaration

//@Man: ROTATIONFLIGHTDATAStruct
//@See: DailyCedaFlightData
/*@Doc:
  A structure for reading flight data. We read all data from database (or get 
  the data from AFTLSG) into this struct and store the data in omData.

*/

 
struct DAILYFLIGHTDATA 
{
	char 	 Act3[5]; 	// Flugzeug-3-Letter Code
	char 	 Act5[7]; 	// Flugzeug-5-Letter Code
	CTime 	 Airb; 	// Startzeit (Beste Zeit)
	CTime 	 Airu; 	// Startzeit User
	char 	 Csgn[10]; 	// Call- Sign
	CTime 	 Slot; 		// Slot-Zeit
	char 	 Des3[5]; 	// Bestimmungsflughafen 4-Lettercode
	char 	 Des4[6]; 	// Bestimmungsflughafen 4-Lettercode
	CTime 	 Etdc; 	// ETD-Confirmed
	CTime 	 Etai; 	// ETA
	CTime 	 Etdi; 	// ETD
	char 	 Flno[11]; 	// komplette Flugnummer (Airline, Nummer, Suffix)
	char 	 Ftyp[3]; 	// Type des Flugs [F, P, R, X, D, T, ...]
	char 	 Gta1[7]; 	// Gate 1 Ankunft
	char 	 Gta2[7]; 	// Gate 2 Ankunft
	char 	 Gtd1[7]; 	// Gate 1 Abflug
	char 	 Gtd2[7]; 	// Doppelgate Abflug
	char 	 Ifra[3]; 	// Anflugverfahren
	char 	 Ifrd[3]; 	// Abflugverfahren
	char 	 Isre[3]; 	// Bemerkungskennzeichen intern
	CTime 	 Land; 	// Landezeit (Beste Zeit)
	CTime 	 Lstu; 	// Datum letzte �nderung
	CTime 	 Ofbl; 	// Offblock- Zeit (Beste Zeit)
	CTime 	 Onbe; 	// Est. Onblock-Zeit
	CTime 	 Onbl; 	// Onblock-Zeit (Beste Zeit)
	char 	 Org3[5]; 	// Ausgangsflughafen 4-Lettercode
	char 	 Org4[6]; 	// Ausgangsflughafen 4-Lettercode
	char 	 Psta[7]; 	// Position Ankunft
	char 	 Pstd[7]; 	// Position Abflug
	char 	 Regn[14]; 	// LFZ-Kennzeichen
	char 	 Remp[6]; 	// Public Remark FIDS
	long 	 Rkey; 	// Rotationsschl�ssel
	char 	 Rwya[6]; 	// Landebahn
	char 	 Rwyd[6]; 	// Startbahn
	char 	 Stab[3]; 	// Datenstatus AIRB
	CTime 	 Stoa; 	// Planm��ige Ankunftszeit STA
	CTime 	 Stod; 	// Planm��ige Abflugzeit
	CTime 	 Tifa; 	// Zeitrahmen Ankunft
	CTime 	 Tifd; 	// Zeitrahmen Abflug
	CTime 	 Tmoa; 	// TMO (Beste Zeit)
	long 	 Urno; 	// Eindeutige Datensatz-Nr.
	char 	 Useu[34]; 	// Anwender (letzte �nderung)
	char 	 Via4[5]; 		// Liste der Zwischenstationen/-zeiten
	char 	 Vian[4]; 		// Anzahl der Zwischenstationen - 1
	char 	 Ttyp[7]; 		// Verkehrsart
	char 	 Chgi[22]; 		// 
	char 	 Rtyp[3]; 		// 
	char 	 Adid[3]; 		// 
	CTime 	 Nxti; 			// 
	char 	 Blt1[7]; 		// 
	CTime 	 Paba; 			// Belegung Position Ankunft aktueller Beginn
	CTime 	 Paea; 			// Belegung Position Ankunft aktuelles Ende
	CTime 	 Pdba; 			// Belegung Position Abflug aktueller Beginn
	CTime 	 Pdea; 			// Belegung Position Abflug aktuelles Ende
	CTime 	 Pabs; 			// Belegung Position Ankunft geplanter Beginn
	CTime 	 Paes; 			// Belegung Position Ankunft geplantes Ende
	CTime 	 Pdbs; 			// Belegung Position Abflug geplanter Beginn
	CTime 	 Pdes; 			// Belegung Position Abflug geplantes Ende
	char 	 Ming[6]; 		// Mindestbodenzeit
	char 	 Ader[3]; 		// Mindestbodenzeit
	char	 Vial[1026];	// Liste der Zwischenstationen/-zeiten
	char	 Ckif[7];		// Check-In From
	char	 Ckit[7];		// Check-In To
	char	 Blt2[7];		// Gep�ckband 2
	char	 Flti[3];		// International,Domestic etc.
	char	 Via3[4];		// letzte(f�r arr) / erste(f�r dep) zwischenstation

	bool	 Vip;			// Referenz AFT.URNO <--> VIP.FLNU?

	//DataCreated by this class
	int      IsChanged;

	DAILYFLIGHTDATA(void)
	{ 
		memset(this,'\0',sizeof(*this));
		Airb = -1;
		Slot = -1;
		Etdc = -1;
		Etai = -1;
		Etdi = -1;
		Land = -1;
		Lstu = -1;
		Ofbl = -1;
		Onbe = -1;
		Onbl = -1;
		Stoa = -1;
		Stod = -1;
		Tifa = -1;
		Tifd = -1;
		Tmoa = -1;
		Nxti = -1;
		Paba = -1;
		Paea = -1;
		Pdba = -1;
		Pdea = -1;
		Pabs = -1;
		Paes = -1;
		Pdbs = -1;
		Pdes = -1;
		Vip  = false;
	}

}; // end DAILYFLIGHTDATA
	

struct DAILYSCHEDULERKEYLIST
{
	CCSPtrArray<DAILYFLIGHTDATA> Rotation;

};



/////////////////////////////////////////////////////////////////////////////
// Class declaration

//@Man:
//@Memo: Read and write flight data
//@See: CedaData
/*@Doc:
  Reads and writes flight data from and to database. Stores flight data in memory 
  and provides methods for searching and retrieving flights from its internal list. 
  Does some data preparations like pre-formatting the flight number, storing flight 
  number and gate information for the corresponding inbound/outbound flight.

  Data of this class has to be updated by CEDA through broadcasts. This procedure 
  and the necessary methods will be described later with the description of 
  broadcast handling.

  {\bf DailyCedaFlightData} handles data from the EIOHDL server process.
	See the specification for table descriptions.
*/
class DailyCedaFlightData: public CCSCedaData
{
// Attributes
public:
    //@ManMemo: A map, containing the Keya/Keyd fields of all loaded flights.
    //CMapStringToPtr omKeyMap;
    CMapPtrToPtr omUrnoMap;
	CMapPtrToPtr omRkeyMap;
   //@ManMemo: DAILYFLIGHTDATA records read by ReadAllFlight().
    CCSPtrArray<DAILYFLIGHTDATA> omData;

	char pcmAftFieldList[2048];


// Operations
public:
    //@ManMemo: Default constructor
    /*@Doc:
      Initialize {\bf omRecInfo} for offsets and types for each fields in 
	  {\bf DAILYFLIGHTDATA}, the {\bf pcmTableName} with table name {\bf AFTLSG}, 
	  the data members {\bf pcmFieldList} contains a list of used fields of 
	  {\bf ROTATIONFLIGHTDATAStruct}.
    */
    //DailyCedaFlightData(CCSCedaCom *popCommHandler = NULL);  : CCSCedaData(&ogCommHandler)
    DailyCedaFlightData();
    //@ManMemo: Default destructor
	~DailyCedaFlightData();
	



    //@ManMemo: Clear all flights.
    /*@Doc:
      Clear all flights.
      This method will clear {\bf omData} and {\bf omKeyMap}.
    */
	void ClearAll(void);

	// bei DragandDrop
	bool JoinFlightPreCheck(long &ll1Urno, long &ll2Urno);
	bool UpdateFlight(DAILYFLIGHTDATA *prpFlight, DAILYFLIGHTDATA *prpFlightSave);
	// InlineEdit
	bool SaveSpecial(char *pclSelection, char *pclField, char *pclValue);
	DAILYFLIGHTDATA *GetArrival(DAILYFLIGHTDATA *prpFlight);
	DAILYFLIGHTDATA *GetDeparture(DAILYFLIGHTDATA *prpFlight);

	// internal data access.
	bool AddFlightInternal(DAILYFLIGHTDATA *prpFlight, bool bpDdx = true);
	bool DeleteFlightInternal(long lpUrno, bool bpDdx = true);
	bool DeleteFlightsInternal(long lpRkey, bool bpDdx = true);

	bool ReadAllFlights(CString &opWhere, bool bpRotation );
	bool ReadFlights(char *pcpSelection, bool bpDdx = true, bool bpIngoreView = false);
	bool ReadRkeys(CString &olRkeys, char *pcpSelection);



	bool AddToKeyMap(DAILYFLIGHTDATA *prpFlight );
	bool DeleteFromKeyMap(DAILYFLIGHTDATA *prpFlight );
	DAILYFLIGHTDATA *GetFlightByUrno(long lpUrno);

	void ProcessFlightBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);
	bool ProcessFlightUFR(BcStruct *prlBcStruct);
	bool ProcessFlightISF(BcStruct *prlBcStruct);
//	bool PreSelectionCheck(CString &opData, CString &opFields, bool bpCheckOldNewVal = false);
	bool PreSelectionCheck(CString &opData, CString &opFields, long lpRkey, bool bpCheckOldNewVal = false);
	void SetPreSelection(CTime &opFrom, CTime &opTo, CString &opFtyps);
	void ReloadFlights(void *vpDataPointer);

	bool bmRotation ;
	CString omFtyps;
	CTime omFrom;
	CTime omTo;

	void ProcessCcaChange(long *lpUrno);

	CedaDiaCcaData omCcaData;

	void ReadAllCca();


	CString omSelection;
	bool bmReadAllFlights;
	struct RKEYLIST
	{
		CCSPtrArray<DAILYFLIGHTDATA> Rotation;

	};

	private:
		DailyVipCedaData omDailyVipCedaData;

};


#endif
