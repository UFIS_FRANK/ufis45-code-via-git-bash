#if !defined(AFX_CCSSPECFILTERPAGE_H__18568971_4EA3_11D1_9881_000001014864__INCLUDED_)
#define AFX_CCSSPECFILTERPAGE_H__18568971_4EA3_11D1_9881_000001014864__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// ccsspecfilterpage.h : header file
//
#include "CCSEdit.h"
#include "Ansicht.h"
#include "CCSPtrArray.h"
//#include "CCSPageBuffer.h"
//#include "CCSBasePSPage.h"

struct SPECFILTER_ITEMS
{
	CString Item;
};
struct SPECFILTER_TABLES
{
	CString TableName;
	CString SelectedItems;
	//CCSPtrArray<SPECFILTER_ITEMS> SelectedItems;
};
/////////////////////////////////////////////////////////////////////////////
// CSpecFilterPage dialog

class CSpecFilterPage : public CPropertyPage//CBasePropSheetPage
{
	DECLARE_DYNCREATE(CSpecFilterPage)

// Construction
public:
	CSpecFilterPage();
	~CSpecFilterPage();

	CCSPtrArray<SPECFILTER_TABLES> omData;

	CStringArray omValues;
	void GetData();	
	void SetData();	

	// Dialog Data
	//{{AFX_DATA(CSpecFilterPage)
	enum { IDD = IDD_PSSPECFILTER_PAGE };
	CListBox	m_List2;
	CListBox	m_List1;
	CComboBox   m_Combo;
	//}}AFX_DATA

	CStringArray omTables;
	CStringArray omRealTables;
// Overrides
	// ClassWizard generate virtual function overrides
	//{{AFX_VIRTUAL(CSpecFilterPage)
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL
   //void ResetBufferPtr(){CBasePropSheetPage::ResetBufferPtr(); pomPageBuffer = NULL;};
   CString GetPrivList();
   SPECFILTER_TABLES *GetTable(CString opTable);
	void InitMask();

// Implementation
public:
	//CPageBuffer *pomPageBuffer;
	CStringArray omPossibleItems;
	CStringArray omSelectedItems;
	int imTable;
	CString omTable;
	CString omOldTable;
	// Generated message map functions
	//{{AFX_MSG(CSpecFilterPage)
	afx_msg void OnButtonAdd();
	afx_msg void OnButtonRemove();
	afx_msg void OnSelchangeComboTable();
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

//   void *GetData();
//   void InitialScreen();
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CCSSPECFILTERPAGE_H__18568971_4EA3_11D1_9881_000001014864__INCLUDED_)
