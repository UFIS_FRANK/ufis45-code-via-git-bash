// ReportTimeFrameSelctDlg.cpp : implementation file
//

#include "stdafx.h"
#include "fpms.h"
#include "CCSGlobl.h"
#include "ReportTimeFrameSelctDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// ReportTimeFrameSelctDlg dialog


ReportTimeFrameSelctDlg::ReportTimeFrameSelctDlg(CWnd* pParent /*=NULL*/)
	: CDialog(ReportTimeFrameSelctDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(ReportTimeFrameSelctDlg)
	m_ListTableGantt = -1;
	//}}AFX_DATA_INIT
}


void ReportTimeFrameSelctDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(ReportTimeFrameSelctDlg)
	DDX_Radio(pDX, IDC_TABLE, m_ListTableGantt);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(ReportTimeFrameSelctDlg, CDialog)
	//{{AFX_MSG_MAP(ReportTimeFrameSelctDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// ReportTimeFrameSelctDlg message handlers

void ReportTimeFrameSelctDlg::OnOK() 
{
	
	// TODO: Add extra validation here
	UpdateData(TRUE);

	if (m_ListTableGantt != -1)
	{
		CDialog::OnOK();
	}
	else
	{
		Beep(500, 100) ;
		CString olMessage ;
		olMessage.Format(GetString(IDS_STRING1460), m_ListTableGantt);
		MessageBox(olMessage, GetString(IDS_WARNING), MB_ICONWARNING);
	}

}
