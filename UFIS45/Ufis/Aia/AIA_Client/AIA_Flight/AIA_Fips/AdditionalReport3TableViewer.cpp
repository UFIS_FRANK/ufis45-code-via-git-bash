// AdditionalReport3TableViewer.cpp : implementation file
// 
// List of all flights take off from the same destination
// keine Angabe einer destination

#include "stdafx.h"
#include "AdditionalReport3TableViewer.h"
#include "CcsGlobl.h"
#include <iomanip.h>
#include <iostream.h>
#include <fstream.h>
#include "BasicData.h"
#include "Utils.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif


/////////////////////////////////////////////////////////////////////////////
// AdditionalReport3TableViewer
//


AdditionalReport3TableViewer::AdditionalReport3TableViewer(CCSPtrArray<ROTATIONDLGFLIGHTDATA> *popData,
																		   const CStringArray &opShownColumns,
																		   char *pcpInfo,
																		   char *pcpSelect)
{

	pomData = popData;
	pcmInfo = pcpInfo;
	pcmSelect = pcpSelect;


	bmIsFromSearch = false;
    pomTable = NULL;

	omShownColumns.Copy(opShownColumns);

	imDepFlightCount = 0;

}

AdditionalReport3TableViewer::~AdditionalReport3TableViewer()
{

	omPrintHeadHeaderArray.DeleteAll();
	omAnzChar.RemoveAll();
	omShownColumns.RemoveAll();
    DeleteAll();

	omColumn1.RemoveAll();
	omCountColumn1.RemoveAll();
	omCountColumn2.RemoveAll();

}


void AdditionalReport3TableViewer::SetColumnsToShow(const CStringArray &opShownColumns)
{

	CCS_TRY

	omShownColumns.Copy(opShownColumns);

	CCS_CATCH_ALL

}


void AdditionalReport3TableViewer::SetParentDlg(CDialog* ppParentDlg)
{

	pomParentDlg = ppParentDlg;

}


void AdditionalReport3TableViewer::Attach(CCSTable *popTable)
{
    pomTable = popTable;
}


int AdditionalReport3TableViewer::GetVia(CCSPtrArray<VIADATA> *opVias, ROTATIONDLGFLIGHTDATA *prpFlight)
{
	opVias->DeleteAll();

	if(prpFlight == NULL)
		return 0;
	if(strlen(prpFlight->Vial) == 0)
		return 0;

	CString olVias(prpFlight->Vial);
	CString olFids;
	CString olApc3;
	CString olApc4;

	VIADATA *prlVia;

	while(olVias.IsEmpty() != TRUE)
	{
			prlVia = new VIADATA;
			opVias->Add(prlVia);

			if(olVias.GetLength() < 120)
			{
				olVias += "                                                                                                                             ";
				olVias = olVias.Left(120);
			}


			olApc3 = olVias.Mid(1,3);
			olApc3.TrimLeft();
			sprintf(prlVia->Apc3, olApc3);

			if(olVias.GetLength() >= 120)
				olVias = olVias.Right(olVias.GetLength() - 120);

	}
	return opVias->GetSize();
}


void AdditionalReport3TableViewer::ChangeViewTo(const char *pcpViewName)
{
	TRACE("AdditionalReport3TableViewer::ChangeViewTo\n");
   
    pomTable->ResetContent();
    DeleteAll();    
    MakeLines();

	UpdateDisplay();
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
}


/////////////////////////////////////////////////////////////////////////////
// AdditionalReport3TableViewer -- code specific to this class

void AdditionalReport3TableViewer::MakeLines()
{
	ROTATIONDLGFLIGHTDATA *prlAFlight;
	ROTATIONDLGFLIGHTDATA *prlDFlight;
	ROTATIONDLGFLIGHTDATA *prlNextFlight = NULL;
	ROTATIONDLGFLIGHTDATA *prlFlight;
	int ilLineNo;

	bool blRDeparture = false;

	int ilFlightCount = pomData->GetSize();

	for (int ilLc = 0; ilLc < ilFlightCount; ilLc++)
	{
		prlFlight = &(*pomData)[ilLc];

		if(ilLc + 1 < ilFlightCount)
			prlNextFlight = &(*pomData)[ilLc + 1];	
		else
			prlNextFlight = NULL;	

		//Arrival
		if((strcmp(prlFlight->Org3, pcgHome) != 0) && (strcmp(prlFlight->Des3, pcgHome) == 0))
		{
			prlAFlight = prlFlight;
			prlDFlight = NULL;
			if(prlNextFlight != NULL)
			{
				if((prlNextFlight->Rkey == prlFlight->Rkey) && (prlFlight->Rkey != NULL))
				{
					prlDFlight = prlNextFlight;

					if((strcmp(prlNextFlight->Org3, pcgHome) == 0) && (strcmp(prlNextFlight->Des3, pcgHome) != 0))
					{
						ilLc++;
					}
					else
						blRDeparture = true;
				}
			}
		}
		else
		{
			// Departure
			if((strcmp(prlFlight->Org3, pcgHome) == 0) && (strcmp(prlFlight->Des3, pcgHome) != 0))
			{
				prlAFlight = NULL;
				prlDFlight = prlFlight;
				blRDeparture = false;
			}
			else
			{
				//Turnaround
				if((strcmp(prlFlight->Org3, pcgHome) == 0) && (strcmp(prlFlight->Des3, pcgHome) == 0))
				{
					if(blRDeparture)
					{
						blRDeparture = false;
						prlAFlight = prlFlight;
						prlDFlight = NULL;
						if(prlNextFlight != NULL)
						{
							if((prlNextFlight->Rkey == prlFlight->Rkey) && (prlFlight->Rkey != NULL))
							{
								prlDFlight = prlNextFlight;
								if((strcmp(prlNextFlight->Org3, pcgHome) == 0) && (strcmp(prlNextFlight->Des3, pcgHome) != 0))
								{
									ilLc++;
								}
								else
									blRDeparture = true;
							}
						}
					}
					else
					{
						prlAFlight		= prlFlight;
						prlDFlight		= prlFlight;
						if(prlNextFlight != NULL)
						{
							if((prlNextFlight->Rkey == prlFlight->Rkey) && (prlFlight->Rkey != NULL))
							{
								ilLc--;
								prlAFlight	 = NULL;
								blRDeparture = true;
							}
						}
					}
				}
			}
		}
		ilLineNo = MakeLine(prlAFlight, prlDFlight);

		// Listen zum Zaehlen aufbauen
		MakeCountList(prlDFlight);

	}


}

		

int AdditionalReport3TableViewer::MakeLine(ROTATIONDLGFLIGHTDATA *prpAFlight, ROTATIONDLGFLIGHTDATA *prpDFlight)
{
    ADDITIONAL_REPORT3_LINEDATA rlLine;

	
	if((prpAFlight != NULL) && (prpDFlight != NULL) )
	{
		imDepFlightCount++;
		MakeLineData(prpAFlight, prpDFlight, rlLine);
		return CreateLine(rlLine);
	}
	if (prpAFlight != NULL)
	{
		MakeLineData(prpAFlight, NULL, rlLine);
		return CreateLine(rlLine);
	}
	if (prpDFlight != NULL)
	{
		imDepFlightCount++;
		MakeLineData(NULL, prpDFlight, rlLine);
		return CreateLine(rlLine);
	}
	return -1;
}




void AdditionalReport3TableViewer::MakeLineData(ROTATIONDLGFLIGHTDATA *prpAFlight, ROTATIONDLGFLIGHTDATA *prpDFlight, ADDITIONAL_REPORT3_LINEDATA &rpLine)
{
	CString olStr;
	if(prpAFlight != NULL)
	{
		rpLine.AUrno =  prpAFlight->Urno;
		rpLine.ARkey =  prpAFlight->Rkey;
		rpLine.AFlno = CString(prpAFlight->Flno);
		rpLine.ADate = DateToHourDivString(prpAFlight->Onbl, prpAFlight->Stoa);
		rpLine.ATtyp = prpAFlight->Ttyp; 
		//rpLine.AVia3 = CString(prpAFlight->Via3);
		rpLine.AAct = CString(prpAFlight->Act3);
		rpLine.AAlc2 = CString(prpAFlight->Alc2);
		rpLine.AAlc3 = CString(prpAFlight->Alc3);

		rpLine.AStoa = CTime(prpAFlight->Stoa);
		if(bgReportLocal) ogBasicData.UtcToLocal(rpLine.AStoa);
		rpLine.AEtai = CTime(prpAFlight->Etai);
		if(bgReportLocal) ogBasicData.UtcToLocal(rpLine.AEtai);
		rpLine.ALand = CTime(prpAFlight->Land);
		if(bgReportLocal) ogBasicData.UtcToLocal(rpLine.ALand);
		rpLine.AVian = CString(prpAFlight->Vian);

		CCSPtrArray<VIADATA> opVias;
		int ilViaCnt = GetVia(&opVias, prpAFlight);

		if(opVias.GetSize() == 0)
			rpLine.AVia3 = "";
		else
		{
			rpLine.AVia3 =  opVias[ilViaCnt-1].Apc3;	// letzter Via vor Homeairport = letzte Zeile in Vial
		}
		opVias.DeleteAll();

	}
	else
	{
		rpLine.AUrno =  0;
		rpLine.ARkey =  0;
		rpLine.AFlno = "";
		rpLine.ADate = ""; 
		rpLine.ATtyp = ""; 
		rpLine.AVia3 = "";
		rpLine.AAct  = "";
		rpLine.AAlc2 = "";
		rpLine.AAlc3 = "";
		rpLine.AEtai = -1;
		rpLine.ALand = -1;
		rpLine.AVian = "";
	}


	if(prpDFlight != NULL)
	{
		rpLine.DUrno =  prpDFlight->Urno;
		rpLine.DRkey =  prpDFlight->Rkey;
		rpLine.DFlno = CString(prpDFlight->Flno); 
		rpLine.DTtyp = CString(prpDFlight->Ttyp);
		rpLine.DDes3 = CString(prpDFlight->Des3);
		rpLine.DDes4 = CString(prpDFlight->Des4);
		rpLine.DAct = CString(prpDFlight->Act3);
		rpLine.DAlc2 = CString(prpAFlight->Alc2);
		rpLine.DAlc3 = CString(prpAFlight->Alc3);
		rpLine.DAirb = CTime(prpDFlight->Airb);
		if(bgReportLocal) 
			ogBasicData.UtcToLocal(rpLine.DAirb);
		rpLine.DEtdi = CTime(prpDFlight->Etdi);
		if(bgReportLocal) 
			ogBasicData.UtcToLocal(rpLine.DEtdi);
		rpLine.DStod = prpDFlight->Stod;
		if(bgReportLocal)
			ogBasicData.UtcToLocal(rpLine.DStod);
		rpLine.DDate = prpDFlight->Stod.Format("%d.%m.%y");
		rpLine.DVian = prpDFlight->Vian;

		if (rpLine.AAct.IsEmpty())
			rpLine.AAct = rpLine.DAct;

		CCSPtrArray<VIADATA> opVias;
		int ilViaCnt = GetVia(&opVias, prpDFlight);

		if(opVias.GetSize() == 0)
			rpLine.DVia3 = "";
		else
		{
			rpLine.DVia3 =  opVias[0].Apc3;	// erster Via nach Homeairport = erste Zeile in Vial
		}
		opVias.DeleteAll();

	
	}
	else
	{
		rpLine.DUrno =  0;
		rpLine.DRkey =  0;
		rpLine.DFlno = "";
		rpLine.DDate = ""; 
		rpLine.DTtyp = ""; 
		rpLine.DDes3 = "";
		rpLine.DDes4 = "";
		rpLine.DVia3 = "";
		rpLine.DAct = "";
		rpLine.DAlc2 = "";
		rpLine.DAlc3 = "";
		rpLine.DEtdi = TIMENULL; 
		rpLine.DAirb = TIMENULL; 

	}
    return;
}



int AdditionalReport3TableViewer::CreateLine(ADDITIONAL_REPORT3_LINEDATA &rpLine)
{
    int ilLineCount = omLines.GetSize();

    for (int ilLineno = ilLineCount; ilLineno > 0; ilLineno--)
        if (CompareFlight(&rpLine, &omLines[ilLineno-1]) >= 0)
            break;  // should be inserted after Lines[ilLineno-1]

    omLines.NewAt(ilLineno, rpLine);
	return ilLineno;
}


void AdditionalReport3TableViewer::DeleteLine(int ipLineno)
{
	omLines.DeleteAt(ipLineno);
}



void AdditionalReport3TableViewer::DeleteAll()
{
    while (omLines.GetSize() > 0)
        DeleteLine(0);
}


/////////////////////////////////////////////////////////////////////////////
// AdditionalReport3TableViewer - display drawing routine



// UpdateDisplay: Load data selected by filter conditions to the display by using "omTable"
void AdditionalReport3TableViewer::UpdateDisplay()
{

	CCS_TRY

	pomTable->ResetContent();

	ADDITIONAL_REPORT3_LINEDATA *prlLine;

	// Anzahl an Zeichen zur Berechnung der Spaltenbreite ermitteln
	int ilColumns = omShownColumns.GetSize();
	for (int i = 0; i < ilColumns; i++)
	{
		// Aufruf, damit omAnzCount Array gef�llt ist
		SetFieldLength(omShownColumns[i]);
	}
	DrawHeader();
  
	for (int ilLc = 0; ilLc < omColumn1.GetSize(); ilLc++)
	{
		int ilColumnNo = 0;
		prlLine = &omLines[ilLc];
		CCSPtrArray<TABLE_COLUMN> olColList;
	
		MakeColList(prlLine, olColList, omColumn1[ilLc]);

		pomTable->AddTextLine(olColList, (void*)(prlLine));
		olColList.DeleteAll();
	}

    pomTable->DisplayTable();

	CCS_CATCH_ALL

}


void AdditionalReport3TableViewer::DrawHeader()
{

	CCS_TRY

	CCSPtrArray <TABLE_HEADER_COLUMN> omHeaderDataArray;

	pomTable->SetShowSelection(true);
	pomTable->ResetContent();

	TABLE_HEADER_COLUMN rlHeader;
	rlHeader.Font = &ogCourier_Bold_10;
	int i;

	// alle n Spalten (=omShownColumns.GetSize()) definieren
	int ilColums = omShownColumns.GetSize();
	int ilAnzChar = omAnzChar.GetSize();
	for (i = 0; i < ilColums; i++)
	{
		rlHeader.Alignment = COLALIGN_CENTER;
		// Anzahl an Textzeichen des Feldes der i-ten Spalte
		if (i <= ilAnzChar)
			rlHeader.AnzChar = omAnzChar[i];
		else
			break;
		// alten String l�schen, damit neuer String bei '0' anfaengt
		rlHeader.String = "";
		// Anzahl an Buchstaben 'W' setzen
		int ilAnzChar = omAnzChar[i];
		for (int j = 0; j < ilAnzChar; j++) 
			rlHeader.String += "W";
		// Text aus Konfiguration als Ueberschrift setzen
		rlHeader.Text = GetHeaderContent(omShownColumns[i]);
		// neue Spaltenheaderstruktur setzen
		omHeaderDataArray.New(rlHeader);
	}

	pomTable->SetHeaderFields(omHeaderDataArray);
	omHeaderDataArray.DeleteAll();

	pomTable->SetDefaultSeparator();

	CCS_CATCH_ALL

}




void AdditionalReport3TableViewer::MakeColList(ADDITIONAL_REPORT3_LINEDATA *prlLine, 
                                               CCSPtrArray<TABLE_COLUMN> &olColList,
											   CString opCurrentAirline)
{

	CCS_TRY

	TABLE_COLUMN rlColumnData;

	rlColumnData.VerticalSeparator = SEPA_NONE;
	rlColumnData.SeparatorType = SEPA_NONE;
	rlColumnData.Font = &ogCourier_Regular_9;
		
	// Spaltendefinitionen bestimmen
	for (int i = 0; i < omShownColumns.GetSize(); i++)
	{
		rlColumnData.Alignment = COLALIGN_LEFT;
		// Holen des Feldinhaltes der i-ten Spalte und n-ten Zeile
		rlColumnData.Text = GetFieldContent(prlLine, omShownColumns[i], opCurrentAirline);

		olColList.NewAt(olColList.GetSize(), rlColumnData);
	}

	CCS_CATCH_ALL

}




int AdditionalReport3TableViewer::CompareFlight(ADDITIONAL_REPORT3_LINEDATA *prpLine1, ADDITIONAL_REPORT3_LINEDATA *prpLine2)
{
	int	ilCompareResult;
	CTime olTime1;
	CTime olTime2;


		ilCompareResult = (olTime1 == olTime2)? 0:
			(olTime1 > olTime2)? 1: -1;

		 return ilCompareResult;
}




//-----------------------------------------------------------------------------------------------
// Printing Routines
//-----------------------------------------------------------------------------------------------

void AdditionalReport3TableViewer::GetHeader()
{
	
	CCS_TRY
		
	TABLE_HEADER_COLUMN *prlHeader[50];

	int i;
	int ilColomns = omShownColumns.GetSize();
	CSize olSize;
    pomPrint->omCdc.SelectObject(&ogCourier_Bold_10);

	for (i = 0; i < ilColomns; i++)
	{
		olSize = pomPrint->omCdc.GetTextExtent(GetHeaderContent(omShownColumns[i]));
		prlHeader[i] = new TABLE_HEADER_COLUMN;
		prlHeader[i]->Alignment = COLALIGN_CENTER;
		prlHeader[i]->Font = &ogCourier_Bold_10;
		prlHeader[i]->Length = olSize.cx;
		prlHeader[i]->Text = GetHeaderContent(omShownColumns[i]);
	}

	omPrintHeadHeaderArray.DeleteAll();
	for(i = 0; i < ilColomns; i++)
	{
		omPrintHeadHeaderArray.Add(prlHeader[i]);
	}

	CCS_CATCH_ALL

}



void AdditionalReport3TableViewer::PrintTableView()
{

	CCS_TRY

	CString olFooter1,olFooter2;
	CString olTableName = GetString(1256);
	int ilOrientation = PRINT_LANDSCAPE;//  PRINT_PORTRAET  PRINT_LANDSCAPE
	pomPrint = new CCSPrint(pomParentDlg,ilOrientation,45);// FirstLine(P=200,L=200),LeftOffset(P=200,L=50)
	pomPrint->imMaxLines = 38;  // (P=57,L=38)

	if (pomPrint != NULL)
	{

		if (pomPrint->InitializePrinter(ilOrientation) == TRUE)
		{
			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
			pomPrint->imLineNo = pomPrint->imMaxLines + 1;
			DOCINFO	rlDocInfo;
			memset(&rlDocInfo, 0, sizeof(DOCINFO));
			rlDocInfo.cbSize = sizeof( DOCINFO );
			rlDocInfo.lpszDocName = olTableName;	
			pomPrint->omCdc.StartDoc( &rlDocInfo );
			pomPrint->imPageNo = 0;
			int ilLines = omLines.GetSize();  
			olFooter1.Format("%s %d, %s, %s",GetString(IDS_STRING329),ilLines,olTableName, 
				            (CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M")));
			olFooter1.Format("%s %d",rlDocInfo.lpszDocName,ilLines);

			GetHeader();

			if (ilLines == 0)
				PrintTableHeader(); // Drucken leerer Tabelle
			for(int i = 0; i < omColumn1.GetSize(); i++ ) 
			{
				if(pomPrint->imLineNo >= pomPrint->imMaxLines)
				{
					if(pomPrint->imPageNo > 0)
					{
						olFooter2.Format(GetString(IDS_STRING1198),pomPrint->imPageNo);
						pomPrint->PrintUIFFooter(olFooter1,"",olFooter2);
						pomPrint->omCdc.EndPage();
					}
					PrintTableHeader();
				}
				if(pomPrint->imLineNo == (pomPrint->imMaxLines-1) || i == (ilLines-1))
				{
					PrintTableLine(&omLines[i],true, omColumn1[i]);
				}
				else
				{
					PrintTableLine(&omLines[i],false, omColumn1[i]);
				}
			}
			olFooter2.Format(GetString(IDS_STRING1198),pomPrint->imPageNo);
			pomPrint->PrintUIFFooter(olFooter1,"",olFooter2);
			pomPrint->omCdc.EndPage();
			pomPrint->omCdc.EndDoc();
		}

		omPrintHeadHeaderArray.DeleteAll();
		SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
		delete pomPrint;
		pomPrint = NULL;
	}

	CCS_CATCH_ALL

}


//-----------------------------------------------------------------------------------------------

bool AdditionalReport3TableViewer::PrintTableHeader()
{
	pomPrint->omCdc.StartPage();
	pomPrint->imPageNo++;
	pomPrint->imLineNo = 0;
	double dgCCSPrintFactor = 2.7 ;
	char pclHeader[256];

	sprintf(pclHeader, GetString(IDS_STRING1577), pcmInfo, GetDepFlightCount());
	CString olTableName(pclHeader);

	//pomPrint->PrintUIFHeader(omTableName,CString(CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M")),pomPrint->imFirstLine-10);
	pomPrint->PrintUIFHeader(omTableName,CString(pclHeader),pomPrint->imFirstLine-10);

	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;

	rlElement.Alignment   = PRINT_LEFT;
	rlElement.FrameLeft   = PRINT_NOFRAME;
	rlElement.FrameRight  = PRINT_NOFRAME;
	rlElement.FrameTop    = PRINT_NOFRAME;
	rlElement.FrameBottom = PRINT_FRAMEMEDIUM;
	rlElement.pFont       = &pomPrint->ogCourierNew_Bold_8;

	//int ilSize = omHeaderDataArray.GetSize();
	int ilSize = omPrintHeadHeaderArray.GetSize();

	for(int i=0;i<ilSize;i++)
	{
			//rlElement.Length = (int)(omHeaderDataArray[i].Length*dgCCSPrintFactor);
			rlElement.Length = (int)(omPrintHeadHeaderArray[i].Length*dgCCSPrintFactor);
			if(rlElement.Length < igCCSPrintMinLength)rlElement.Length+=igCCSPrintMoreLength; 
			//rlElement.Text   = omHeaderDataArray[i].Text;
			rlElement.Text   = omPrintHeadHeaderArray[i].Text;
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
	}
	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();


	return true;
}

//-----------------------------------------------------------------------------------------------

bool AdditionalReport3TableViewer::PrintTableLine(ADDITIONAL_REPORT3_LINEDATA *prpLine,
														  bool bpLastLine,
														  CString opCurrentAirline)
{

	CCS_TRY

	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;
	double dgCCSPrintFactor = 2.7;

    pomPrint->omCdc.SelectObject(&pomPrint->ogCourierNew_Regular_8);
	CSize olSize;
		
	rlElement.pFont = &pomPrint->ogCourierNew_Regular_8;

	int ilSize = omPrintHeadHeaderArray.GetSize();
	for(int i=0;i<ilSize;i++)
	{
		olSize = pomPrint->omCdc.GetTextExtent(omShownColumns[i]);
		
		rlElement.Length = (int)(omPrintHeadHeaderArray[i].Length*dgCCSPrintFactor);
		if(rlElement.Length < igCCSPrintMinLength)
			rlElement.Length+=igCCSPrintMoreLength; 
		rlElement.Alignment  = PRINT_LEFT;
		rlElement.FrameTop   = PRINT_FRAMETHIN;
		if(bpLastLine == true)
		{
			rlElement.FrameBottom = PRINT_FRAMETHIN;
		}
		else
		{
			rlElement.FrameBottom = PRINT_FRAMETHIN/*PRINT_NOFRAME*/;
		}
		
		rlElement.FrameLeft  = PRINT_NOFRAME;
		rlElement.FrameRight = PRINT_NOFRAME;
		rlElement.Text = GetFieldContent(prpLine, omShownColumns[i], opCurrentAirline);

		rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
		
	}

	pomPrint->PrintLine(rlPrintLine);

	rlPrintLine.DeleteAll();
	
	CCS_CATCH_ALL

	return true;

}

//-----------------------------------------------------------------------------------------------


// Drucken in Datei
void AdditionalReport3TableViewer::PrintPlanToFile(char *pcpDefPath)
{

	CCS_TRY

	ofstream of;
	int ilCountLines = omColumn1.GetSize();
	int ilCountRows  = omShownColumns.GetSize();

	char pclHeadlineSelect[120];
	sprintf(pclHeadlineSelect, GetString(IDS_STRING1577), pcmInfo, GetDepFlightCount());

	of.open( pcpDefPath, ios::out);
	of << CString(pclHeadlineSelect) << " "
		<< CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M") << endl << endl;

	for (int i = 0; i < ilCountRows; i++)
	{
		of << GetHeaderContent(omShownColumns[i]) << "  ";
	}
	of << endl << "-------------------------------------------------------------------" << endl;


	// zeilen iterieren
	for(int ilLines = 0; ilLines < ilCountLines; ilLines++)
	{
		ADDITIONAL_REPORT3_LINEDATA rlLine = omLines[ilLines];

		// Spalten- und Feldinhalt ueber Spaltenaray (omShownColumns) iterieren
		for (int ilCols = 0; ilCols < ilCountRows; ilCols++)
		{
			of.setf(ios::left, ios::adjustfield);

			of << setw(omAnzChar[ilCols] + 13) << GetFieldContent(&rlLine, omShownColumns[ilCols], omColumn1[ilLines]);
		}

		// Zeile abschliessen
		of << endl;
	
	}

	// stream schliessen
	of.close();

	CCS_CATCH_ALL
	
}


int AdditionalReport3TableViewer::GetDepFlightCount()
{

	return imDepFlightCount;

}


void AdditionalReport3TableViewer::MakeCountList(ROTATIONDLGFLIGHTDATA *prpDFlight)
{

	CCS_TRY

	int i;
	bool blItemExists = false;

	// keine Daten? --> tschuess
	if (prpDFlight == NULL)
		return;

	// Origin airport aus Ankunftsteil
	if (prpDFlight != NULL)
	{
		// Pruefen, ob Org bereits im Array vorhanden
		for (i = 0; i < omColumn1.GetSize(); i++)
		{
			if (omColumn1[i] == CString(prpDFlight->Des3))
			{
				blItemExists = true;
				break;
			}
		}
		// neue Org in Array einfuegen
		if (!blItemExists)
		{
			InsertSortInStrArray(omColumn1, CString(prpDFlight->Des3));
 			omCountColumn1.Add(1);
			omCountColumn2.Add(atoi(prpDFlight->Vian));
		}
		// wenn schon vorhanden, Zaehler erhoehen
		else
		{
			omCountColumn1.SetAt(i, omCountColumn1.GetAt(i)+1);
			omCountColumn2.SetAt(i, omCountColumn2.GetAt(i)+ atoi(prpDFlight->Vian));
		}

	}

	CCS_CATCH_ALL

}


CString AdditionalReport3TableViewer::GetFieldContent(ADDITIONAL_REPORT3_LINEDATA* prlLine,
										                      CString opCurrentColumns,
															  CString opCurrentAirline)
{

	CString olFormat;
	int i = 0;

	if (opCurrentColumns == "Airport")
	{
		return opCurrentAirline;
	}
	if (opCurrentColumns == "Flights")
	{
		for (int i = 0; i < omColumn1.GetSize(); i++)
		{
			if (omColumn1[i] == opCurrentAirline)
			{
				olFormat.Format("%d", omCountColumn1[i]);
				return olFormat;
			}
		}
	}
	if (opCurrentColumns == "ViaFlights")
	{
		for (int i = 0; i < omColumn1.GetSize(); i++)
		{
			if (omColumn1[i] == opCurrentAirline)
			{
				olFormat.Format("%d", omCountColumn2[i]);
				return olFormat;
			}
		}
	}

	return "";

}


// Setzen der Spaltenbreite in Anzahl Zeichen
void AdditionalReport3TableViewer::SetFieldLength(CString opCurrentColumns)
{

	CCS_TRY

	if (opCurrentColumns == "Airport")
	{
		omAnzChar.Add(8);
		return;
	}
	if (opCurrentColumns == "Flights")
	{
		omAnzChar.Add(15);
		return;
	}
	if (opCurrentColumns == "ViaFlights")
	{
		omAnzChar.Add(15);
		return;
	}

	// falls kein if passt, ein default-Wert, sonst Array zu klein
	omAnzChar.Add(11);

	CCS_CATCH_ALL

}


CString AdditionalReport3TableViewer::GetHeaderContent(CString opCurrentColumns)
{

	CCS_TRY


	// ------------------------------------------
	// AdditionalReport2 same originating Airport
	// ------------------------------------------
	if (opCurrentColumns == "Airport")
	{
		return GetString(IDS_STRING1579);
	}
	if (opCurrentColumns == "Flights")
	{
		return GetString(IDS_STRING1567);
	}
	if (opCurrentColumns == "ViaFlights")
	{
		return GetString(IDS_STRING1568);
	}

	CCS_CATCH_ALL

	// wenn nix gefunden Leerstring zurueckgeben, sonst undefiniert
	return CString("");

}