// RegisterDlg.cpp : implementation file
//

#include "stdafx.h"
#include "FPMS.h"
#include "RegisterDlg.h"
#include "CedaInitModuData.h"
#include "PrivList.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// RegisterDlg dialog


RegisterDlg::RegisterDlg(CWnd* pParent /*=NULL*/) : CDialog(RegisterDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(RegisterDlg)
	//}}AFX_DATA_INIT
}


void RegisterDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(RegisterDlg)
	DDX_Control(pDX, IDC_TIME, m_TIME);
	DDX_Control(pDX, IDC_TEXT, m_TEXT);
	DDX_Control(pDX, IDC_ABBRECHEN,		m_ABBRECHEN);
	DDX_Control(pDX, IDC_STARTEN,		m_STARTEN);
	DDX_Control(pDX, IDC_REGISTRIEREN,	m_RREGISTER);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(RegisterDlg, CDialog)
	//{{AFX_MSG_MAP(RegisterDlg)
	ON_BN_CLICKED(IDC_ABBRECHEN, OnAbbrechen)
	ON_BN_CLICKED(IDC_REGISTRIEREN, OnRegistrieren)
	ON_BN_CLICKED(IDC_STARTEN, OnStarten)
    ON_WM_TIMER()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// RegisterDlg message handlers
//---------------------------------------------------------------------------

BOOL RegisterDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	CString olTmpText,olTmpTime;
	if(ogPrivList.GetSize()>1)
	{
		olTmpText.Format(GetString(IDS_STRING1231), ogAppName); 

		olTmpTime.Format(GetString(IDS_STRING1232), ogAppName); 
		
		SetTimer(4711,1000,NULL);
		imTimer = 15;
	}
	if(ogPrivList.GetSize()==1)
	{
		olTmpText.Format(GetString(IDS_STRING1233), ogAppName);  

		m_STARTEN.EnableWindow(FALSE);	
	}
	m_TEXT.SetWindowText(olTmpText);
	m_TIME.SetWindowText(olTmpTime);
	return TRUE;  
}

//---------------------------------------------------------------------------

void RegisterDlg::OnAbbrechen() 
{
	KillTimer(4711);
	CDialog::OnCancel();
}

//---------------------------------------------------------------------------

void RegisterDlg::OnRegistrieren() 
{
	KillTimer(4711);
	m_TIME.SetWindowText(GetString(IDS_STRING1234));
	m_TIME.InvalidateRect(NULL);
	m_TIME.UpdateWindow();
	AfxGetApp()->DoWaitCursor(1);
	m_ABBRECHEN.EnableWindow(FALSE);	
	m_STARTEN.EnableWindow(FALSE);	
	m_RREGISTER.EnableWindow(FALSE);
	ogInitModuData.SendInitModu();
	AfxGetApp()->DoWaitCursor(-1);
	CDialog::OnCancel();
}

//---------------------------------------------------------------------------

void RegisterDlg::OnStarten() 
{
	KillTimer(4711);
	CDialog::OnOK();
}

//---------------------------------------------------------------------------
void RegisterDlg::OnTimer(UINT nIDEvent)
{
	CDialog::OnTimer(nIDEvent);
	CString olTmpTime;
	imTimer--;
	if(imTimer == 0)
	{
		KillTimer(4711);
		OnStarten();
	}
	else
	{
		olTmpTime.Format(GetString(IDS_STRING1235),ogAppName,imTimer);
		m_TIME.SetWindowText(olTmpTime);
	}
}


