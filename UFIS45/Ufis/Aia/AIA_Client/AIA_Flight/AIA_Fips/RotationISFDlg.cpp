// RotationISFDlg.cpp : implementation file
//

#include "stdafx.h"
#include "RotationISFDlg.h"
#include "RotationRegnDlg.h"
#include "RotationDlgCedaFlightData.h"
#include "RotationAltDlg.h"
#include "RotationActDlg.h"
#include "RotationAptDlg.h"
#include "Fpms.h"

#include "CedaBasicData.h"
#include "AwBasicDataDlg.h"

#include "resource.h"
#include "resrc1.h"



#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


// Local function prototype
static void IsfCf(void *popInstance, int ipDDXType,
    void *vpDataPointer, CString &ropInstanceName);



/////////////////////////////////////////////////////////////////////////////
// RotationISFDlg dialog


RotationISFDlg::RotationISFDlg(CWnd* pParent /*=NULL*/)
	: CDialog(RotationISFDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(RotationISFDlg)
	m_Stev = _T("");
	m_Act5 = _T("");
	m_OrgDes = _T("");
	m_Regn = _T("");
	m_Sto = _T("");
	m_Alc = _T("");
	m_Flns = _T("");
	m_Fltn = _T("");
	m_Act3 = _T("");
	m_StoDate = _T("");
	m_Ttyp = _T("");
	m_Csgn = _T("");
	//}}AFX_DATA_INIT


    ogDdx.Register(this, APP_EXIT, CString("ISFDLG"), CString("Appli. exit"), IsfCf);

}


RotationISFDlg::~RotationISFDlg()
{
	ogDdx.UnRegister(this,NOTUSED);
}


static void IsfCf(void *popInstance, int ipDDXType,
    void *vpDataPointer, CString &ropInstanceName)
{
    RotationISFDlg *polDlg = (RotationISFDlg *)popInstance;

    if (ipDDXType == APP_EXIT)
        polDlg->AppExit();

}


void RotationISFDlg::AppExit()
{
	EndDialog(IDCANCEL);
}


void RotationISFDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(RotationISFDlg)
	DDX_Control(pDX, IDC_CSGN, m_CE_Csgn);
	DDX_Control(pDX, IDC_TTYP, m_CE_Ttyp);
	DDX_Control(pDX, IDC_STO_DATE, m_CE_StoDate);
	DDX_Control(pDX, IDC_ACT3, m_CE_Act3);
	DDX_Control(pDX, IDC_ORGORDES, m_CR_OrgOrDes);
	DDX_Control(pDX, IDC_ORGORDES2, m_CR_OrgOrDes2);
	DDX_Control(pDX, IDC_FLTN, m_CE_Fltn);
	DDX_Control(pDX, IDC_FLNS, m_CE_Flns);
	DDX_Control(pDX, IDC_ALC, m_CE_Alc);
	DDX_Control(pDX, IDC_STO, m_CE_Sto);
	DDX_Control(pDX, IDC_REGN, m_CE_Regn);
	DDX_Control(pDX, IDC_ORGDES, m_CE_OrgDes);
	DDX_Control(pDX, IDC_ACT5, m_CE_Act5);
	DDX_Control(pDX, IDC_STEV, m_CE_Stev);
	DDX_Text(pDX, IDC_STEV, m_Stev);
	DDX_Text(pDX, IDC_ACT5, m_Act5);
	DDX_Text(pDX, IDC_ORGDES, m_OrgDes);
	DDX_Text(pDX, IDC_REGN, m_Regn);
	DDX_Text(pDX, IDC_STO, m_Sto);
	DDX_Text(pDX, IDC_ALC, m_Alc);
	DDX_Text(pDX, IDC_FLNS, m_Flns);
	DDX_Text(pDX, IDC_FLTN, m_Fltn);
	DDX_Text(pDX, IDC_ACT3, m_Act3);
	DDX_Text(pDX, IDC_STO_DATE, m_StoDate);
	DDX_Text(pDX, IDC_TTYP, m_Ttyp);
	DDX_Text(pDX, IDC_CSGN, m_Csgn);
	DDX_Control(pDX, IDC_STYP, m_CE_Styp);
	DDX_Text(pDX, IDC_STYP, m_Styp);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(RotationISFDlg, CDialog)
	//{{AFX_MSG_MAP(RotationISFDlg)
	ON_BN_CLICKED(IDC_ALC3LIST, OnAlc3list)
	ON_BN_CLICKED(IDC_ACT35LIST, OnAct35list)
	ON_BN_CLICKED(IDC_ORGDESLIST, OnOrgdeslist)
	ON_MESSAGE(WM_EDIT_KILLFOCUS, OnEditKillfocus)
	ON_BN_CLICKED(IDC_TTYPLIST, OnTtyplist)
	ON_BN_CLICKED(IDC_STYPLIST, OnStyplist)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// RotationISFDlg message handlers

void RotationISFDlg::OnOK() 
{
	
	UpdateData(TRUE);

	ROTATIONDLGFLIGHTDATA *prlFlight = new ROTATIONDLGFLIGHTDATA;

	CCSPtrArray<CCADATA> olCins;

	CString olMess;
	
	CTime	olSto = DateHourStringToDate(m_StoDate, m_Sto);

	if(bgDailyLocal)
		ogBasicData.LocalToUtc(olSto);

	if((m_CR_OrgOrDes.GetCheck() == 0) && (m_CR_OrgOrDes2.GetCheck() == 0))
		olMess += GetString(IDS_STRING1250);//"Selektion Ankunft/Abflug fehlt!\n"; 
		


	if(!m_CE_Styp.GetStatus())
		olMess += GetString(IDS_STRING2201) + CString("\n");//"Service Type!\n"; 

	if(!m_CE_Ttyp.GetStatus() || m_Ttyp.IsEmpty())
		olMess += GetString(IDS_STRING360) + CString("\n");//"Verkehrsart!\n"; 

	if(olSto == TIMENULL)
		olMess += GetString(IDS_STRING1243) + CString("\n");//"Ungültiges Datum/Zeit\n"; 

	m_Act5.TrimRight();
	m_Act3.TrimRight();
	if((!m_CE_Act5.GetStatus() || m_Act5.IsEmpty()) || (!m_CE_Act3.GetStatus() || m_Act3.IsEmpty()))
		olMess += GetString(IDS_STRING1244) + CString("\n");//"A/C Typ nicht in StammDaten erfasst!\n"; 

	m_Alc.TrimRight();
	if(!m_CE_Alc.GetStatus() && !m_Alc.IsEmpty())
		olMess += GetString(IDS_STRING1245) + CString("\n");//"Fluggesellschaft nicht in Stammdaten erfasst!\n"; 

	m_OrgDes.TrimRight();
	if(!m_CE_OrgDes.GetStatus() || m_OrgDes.IsEmpty())
		olMess += GetString(IDS_STRING1251) + CString("\n");//"Flughafen nicht in Stammdaten erfasst!\n"; 

	if(!m_CE_Regn.GetStatus() && !m_Regn.IsEmpty())
		olMess += GetString(IDS_STRING1252) + CString("\n");//"LFZ-Kennzeichen nicht in Stammdaten erfasst!\n"; 

	CString olFlno = ogRotationDlgFlights.CreateFlno(m_Alc, m_Fltn, m_Flns); 
	if (olFlno.IsEmpty() && m_Csgn.IsEmpty())
		olMess += GetString(IDS_FLNO_OR_CSGN) + CString("\n");//"Flightnumber or Callsign must be filled!\n"; 


	if(!olMess.IsEmpty())
	{
		MessageBox(olMess, GetString(ST_FEHLER));
		delete prlFlight;
		return;
	}

	char pclDay[3];
	GetDayOfWeek(olSto, pclDay);

	strcpy(prlFlight->Flno, olFlno);

	strcpy(prlFlight->Styp, m_Styp);
	strcpy(prlFlight->Ttyp, m_Ttyp);
	strcpy(prlFlight->Stev, m_Stev);
	strcpy(prlFlight->Act3, m_Act3);
	strcpy(prlFlight->Act5, m_Act5);
	strcpy(prlFlight->Fltn, m_Fltn);
	strcpy(prlFlight->Flns, m_Flns);
	strcpy(prlFlight->Ftyp, "O");
	strcpy(prlFlight->Vian, "0");

	strcpy(prlFlight->Alc3, "");
	strcpy(prlFlight->Alc2, "");

	strcpy(prlFlight->Csgn, m_Csgn);

	if(m_Alc.GetLength() > 1)
	{
		CString olTmp;
		bool blRet = false;
		
		
		if(m_Alc.GetLength() == 3)
		{
			if(ogBCD.GetField("ALT", "ALC3", m_Alc, "ALC2", olTmp ))
			{
				strcpy(prlFlight->Alc3, m_Alc);
				strcpy(prlFlight->Alc2, olTmp);
			}
		}
		else
		{
			if(ogBCD.GetField("ALT", "ALC2", m_Alc, "ALC3", olTmp ))
			{
				strcpy(prlFlight->Alc3, olTmp);
				strcpy(prlFlight->Alc2, m_Alc);
			}
		}
	}


	strcpy(prlFlight->Regn, m_Regn);

	if(m_CR_OrgOrDes.GetCheck() == TRUE)
	{
		prlFlight->Stoa = olSto;

		if(omOrgDes3 == CString(pcgHome))
			prlFlight->Stod = olSto;

		strcpy(prlFlight->Org4, omOrgDes4);
		strcpy(prlFlight->Org3, omOrgDes3);
		strcpy(prlFlight->Des3, pcgHome);
		strcpy(prlFlight->Des4, pcgHome4);

		CString olText;
		olText = ogRotationDlgFlights.CheckDupFlights(olSto, olSto, CString(pclDay), CString(pclDay), CString("1"), olSto, olSto, prlFlight, NULL, olCins);

		if( !olText.IsEmpty())
		{
			olText += GetString(IDS_STRING1698);
			if(MessageBox(olText, GetString(IDS_WARNING), MB_YESNO) == IDYES)
				ogRotationDlgFlights.AddSeasonFlights(olSto, olSto, CString(pclDay),CString(pclDay), CString("1"), olSto, olSto, prlFlight, NULL, olCins);
		}
		else
			ogRotationDlgFlights.AddSeasonFlights(olSto, olSto, CString(pclDay),CString(pclDay), CString("1"), olSto, olSto, prlFlight, NULL, olCins);
	}
	else
	{
		prlFlight->Stod = olSto;

		if(omOrgDes3 == CString(pcgHome))
			prlFlight->Stoa = olSto;
		
		strcpy(prlFlight->Des4, omOrgDes4);
		strcpy(prlFlight->Des3, omOrgDes3);
		strcpy(prlFlight->Org3, pcgHome);
		strcpy(prlFlight->Org4, pcgHome4);

//		ogRotationDlgFlights.AddSeasonFlights(olSto, olSto, CString(pclDay), CString(pclDay), CString("1"), olSto, olSto, NULL, prlFlight, olCins);
		CString olText;
		olText = ogRotationDlgFlights.CheckDupFlights(olSto, olSto, CString(pclDay), CString(pclDay), CString("1"), olSto, olSto, NULL, prlFlight, olCins);

		if( !olText.IsEmpty())
		{
			olText += GetString(IDS_STRING1698);
			if(MessageBox(olText, GetString(IDS_WARNING), MB_YESNO) == IDYES)
				ogRotationDlgFlights.AddSeasonFlights(olSto, olSto, CString(pclDay),CString(pclDay), CString("1"), olSto, olSto, NULL, prlFlight, olCins);
		}
		else
			ogRotationDlgFlights.AddSeasonFlights(olSto, olSto, CString(pclDay),CString(pclDay), CString("1"), olSto, olSto, NULL, prlFlight, olCins);
	}

	delete prlFlight;
	CDialog::OnOK();
}

void RotationISFDlg::OnCancel() 
{
	CDialog::OnCancel();
}

BOOL RotationISFDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	m_CR_OrgOrDes.SetCheck(FALSE);	
	m_CR_OrgOrDes2.SetCheck(FALSE);	

	m_CE_Sto.SetBKColor(YELLOW);
	m_CE_Ttyp.SetBKColor(YELLOW);
	m_CE_StoDate.SetBKColor(YELLOW);
	m_CE_Sto.SetTypeToTime();
	m_CE_StoDate.SetTypeToDate();
	m_CE_Act5.SetTypeToString("X|#X|#X|#X|#X|#",5,0);
	m_CE_Act5.SetBKColor(YELLOW);
	m_CE_Act3.SetTypeToString("X|#X|#X|#",3,0);
	m_CE_Act3.SetBKColor(YELLOW);
	m_CE_Regn.SetTypeToString("XXXXXXXXXXXX",12,0);
	m_CE_OrgDes.SetTypeToString("XXXX",4,0);
	m_CE_OrgDes.SetBKColor(YELLOW);

	m_CE_Alc.SetTypeToString("X|#X|#X|#",3,2);
	m_CE_Fltn.SetTypeToString("#####",5,2);
	m_CE_Flns.SetTypeToString("X",1,0);

	m_CE_Stev.SetTypeToString("X",1,0);


/*	CRect olRect;
	m_CE_Ttyp.GetWindowRect(olRect);
	ScreenToClient(olRect);
	olRect.right += 10;
	m_CE_Ttyp.MoveWindow(olRect);*/
	m_CE_Ttyp.SetTypeToString("X|#X|#X|#X|#X|#",5,1);
	m_CE_Styp.SetTypeToString("X|#X|#X|#X|#X|#",5,0);


	CTime olCurUtc = CTime::GetCurrentTime();
	
	if(!bgDailyLocal)
		ogBasicData.LocalToUtc(olCurUtc);

	m_CE_Sto.SetInitText(olCurUtc.Format("%H:%M"));
	m_CE_StoDate.SetInitText(olCurUtc.Format("%d.%m.%Y"));
	
	m_CE_Csgn.SetTypeToString("X|#X|#X|#X|#X|#X|#X|#X|#",8,0);

	CPoint point;
	::GetCursorPos(&point);
	this->SetWindowPos(&wndTop, point.x, point.y,0,0, SWP_SHOWWINDOW | SWP_NOSIZE);// | SWP_NOMOVE );

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void RotationISFDlg::OnAlc3list() 
{
	AwBasicDataDlg *polDlg = new AwBasicDataDlg(this, "ALT","ALC2,ALC3,ALFN", "ALFN+,ALC2+");
	if(polDlg->DoModal() == IDOK)
	{
		if(polDlg->GetField("ALC2").IsEmpty())
			m_CE_Alc.SetInitText(polDlg->GetField("ALC3"), true);	
		else
			m_CE_Alc.SetInitText(polDlg->GetField("ALC2"), true);	
	}
	delete polDlg;
}

void RotationISFDlg::OnAct35list() 
{
	AwBasicDataDlg *polDlg = new AwBasicDataDlg(this, "ACT","ACT3,ACT5,ACFN", "ACFN+,ACT3+");
	if(polDlg->DoModal() == IDOK)
	{
		m_CE_Act3.SetInitText(polDlg->GetField("ACT3"), true);	
		m_CE_Act5.SetInitText(polDlg->GetField("ACT5"), true);	
	}
	delete polDlg;
}

void RotationISFDlg::OnTtyplist() 
{
	CString olText;
	m_CE_Ttyp.GetWindowText(olText);

	AwBasicDataDlg *polDlg = new AwBasicDataDlg(this, "NAT","TTYP,TNAM", "TNAM+", olText);
	if(polDlg->DoModal() == IDOK)
	{
		m_CE_Ttyp.SetInitText(polDlg->GetField("TTYP"), true);	
	}
	delete polDlg;
}

void RotationISFDlg::OnStyplist() 
{
	CString olText;
	m_CE_Styp.GetWindowText(olText);

	AwBasicDataDlg *polDlg = new AwBasicDataDlg(this, "STY","STYP,SNAM", "SNAM+", olText);
//	polDlg->SetSecState("ROTATIONDLG_CE_DStyp");
	if(polDlg->DoModal() == IDOK)
	{
		m_CE_Styp.SetInitText(polDlg->GetField("STYP"), true);	
	}
	delete polDlg;
}


void RotationISFDlg::OnOrgdeslist() 
{
	AwBasicDataDlg *polDlg = new AwBasicDataDlg(this, "APT","APC3,APC4,APFN", "APFN+");
	if(polDlg->DoModal() == IDOK)
	{
		omOrgDes4 = polDlg->GetField("APC4");
		omOrgDes3 = polDlg->GetField("APC3");
		m_CE_OrgDes.SetInitText(omOrgDes4 , true);	
	}
	delete polDlg;
}


LONG RotationISFDlg::OnEditKillfocus( UINT wParam, LPARAM lParam)
{
	CCSEDITNOTIFY *prlNotify = (CCSEDITNOTIFY*)lParam;
	prlNotify->Text.TrimRight();
	if(prlNotify->Text.IsEmpty())
		return 0L;

	CString olTmp;

	if((UINT)m_CE_OrgDes.imID == wParam)
	{
		CString olTmp;
		prlNotify->UserStatus = true;
		omOrgDes3 = "";
		omOrgDes4 = "";

		if(prlNotify->Text.GetLength() == 4)
		{
			if(prlNotify->Status = ogBCD.GetField("APT", "APC4", prlNotify->Text, "APC3", olTmp ))
			{
				omOrgDes3 = olTmp;
				omOrgDes4 = prlNotify->Text;
			}
		}
		else
		{
			if(prlNotify->Status = ogBCD.GetField("APT", "APC3", prlNotify->Text, "APC4", olTmp ))
			{
				omOrgDes4 = olTmp;
				omOrgDes3 = prlNotify->Text;
			}
		}

		if(!prlNotify->Status)
		{
			RotationAptDlg olDlg(this, prlNotify->Text);
			if(olDlg.DoModal() == IDOK)
			{
				omOrgDes4 = olDlg.m_Apc4;
				omOrgDes3 = olDlg.m_Apc3;
				prlNotify->Status = true;
			}

		}
		m_CE_OrgDes.SetInitText(omOrgDes4, true);
		
		return 0L;
	}

	if((UINT)m_CE_Alc.imID == wParam)
	{
		CString olTmp;
		prlNotify->UserStatus = true;
		if(prlNotify->Text.GetLength() == 3)
			prlNotify->Status = ogBCD.GetField("ALT", "ALC3", prlNotify->Text, "ALC2", olTmp );
		else
			prlNotify->Status = ogBCD.GetField("ALT", "ALC2", prlNotify->Text, "ALC3", olTmp );

		if(!prlNotify->Status)
		{

			RotationAltDlg olDlg(this, prlNotify->Text);
			if(olDlg.DoModal() == IDOK)
			{
				prlNotify->Status = true;
				if(olDlg.m_Alc2.IsEmpty())
					m_CE_Alc.SetInitText(olDlg.m_Alc3, true);
				else
					m_CE_Alc.SetInitText(olDlg.m_Alc2, true);
			}
		}
		return 0L;
	}


	CString olRegn;
	m_CE_Regn.GetWindowText(olRegn);
	
	if((UINT)m_CE_Act5.imID == wParam)
	{
		CString olTmp;

		prlNotify->UserStatus = true;
		if(prlNotify->Status = ogBCD.GetField("ACT", "ACT5", prlNotify->Text, "ACT3", olTmp ))
		{
			CString olAct3;
			m_CE_Act3.GetWindowText(olAct3);
			if(olAct3.IsEmpty())
				m_CE_Act3.SetInitText(olTmp);
			else
			{
				CString olUrno = ogBCD.GetFieldExt("ACT", "ACT3", "ACT5", olAct3, prlNotify->Text, "URNO");
				if(olUrno.IsEmpty())
					m_CE_Act3.SetInitText(olTmp);
			}
		}

		if(!olRegn.IsEmpty())
		{

			
			CString olWhere;
			olRegn.TrimLeft();
			if(!olRegn.IsEmpty())
			{
				olWhere.Format("WHERE REGN = '%s'", olRegn);
				ogBCD.Read( "ACR", olWhere);
			}
			
			
			
			ogBCD.GetField("ACR", "REGN", olRegn, "ACT5", olTmp );
			if(olTmp != prlNotify->Text)
			{
				m_CE_Regn.SetStatus(false);
			}
		}
		
		return 0L;
	}

	if((UINT)m_CE_Act3.imID == wParam)
	{
		CString olTmp;

		prlNotify->UserStatus = true;
		if(prlNotify->Status = ogBCD.GetField("ACT", "ACT3", prlNotify->Text, "ACT5", olTmp ))
		{
			CString olAct5;
			m_CE_Act5.GetWindowText(olAct5);
			if(olAct5.IsEmpty())
				m_CE_Act5.SetInitText(olTmp);
			else
			{
				CString olUrno = ogBCD.GetFieldExt("ACT", "ACT3", "ACT5", prlNotify->Text, olAct5, "URNO");
				if(olUrno.IsEmpty())
					m_CE_Act5.SetInitText(olTmp);
			}

		}
		if(!olRegn.IsEmpty())
		{

			
			CString olWhere;
			olRegn.TrimLeft();
			if(!olRegn.IsEmpty())
			{
				olWhere.Format("WHERE REGN = '%s'", olRegn);
				ogBCD.Read( "ACR", olWhere);
			}
			
			
			ogBCD.GetField("ACR", "REGN", olRegn, "ACT3", olTmp );
			if(olTmp != prlNotify->Text)
			{
				m_CE_Regn.SetStatus(false);
			}
		}
		
		return 0L;
	}

	
	if((UINT)m_CE_Regn.imID == wParam)
	{
		CString olAct3;
		CString olAct5;
		bool blRet = false;

		
		CString olWhere;
		prlNotify->Text.TrimLeft();
		if(!prlNotify->Text.IsEmpty())
		{
			olWhere.Format("WHERE REGN = '%s'", prlNotify->Text);
			ogBCD.Read( "ACR", olWhere);
		}
		
		
		
		blRet = ogBCD.GetField("ACR", "REGN", prlNotify->Text, "ACT3", olAct3 );
		blRet = ogBCD.GetField("ACR", "REGN", prlNotify->Text, "ACT5", olAct5 );

		if(blRet)
		{
			m_CE_Act5.SetInitText(olAct5, true);
			m_CE_Act3.SetInitText(olAct3, true);
		}
		else
		{
			CString	olRegn;
			m_CE_Regn.GetWindowText(olRegn);
			m_CE_Act3.GetWindowText(olAct3);
			m_CE_Act5.GetWindowText(olAct5);

			RotationRegnDlg olDlg(this, olRegn, olAct3, olAct5);
			if(olDlg.DoModal() == IDOK)
			{
				m_CE_Regn.SetInitText(olDlg.m_Regn, true);
				m_CE_Act5.SetInitText(olDlg.m_Act5, true);
				m_CE_Act3.SetInitText(olDlg.m_Act3, true);
				blRet = true;
			}
			else
				m_CE_Regn.SetInitText("", true);
		}
		prlNotify->UserStatus = true;
		prlNotify->Status = blRet;
		return 0L;
	}

	if((UINT)m_CE_Styp.imID == wParam && !prlNotify->Text.IsEmpty()) 
	{
		CString olTmp;
		prlNotify->Status  = ogBCD.GetField("STY", "STYP", prlNotify->Text, "STYP", olTmp );
		prlNotify->UserStatus = true;

		if(prlNotify->Status == true && !prlNotify->Text.IsEmpty())
		{
			m_CE_Styp.SetInitText(prlNotify->Text, true);
		}
		
		return 0L;
//		prlNotify->UserStatus = true;
//		prlNotify->UserStatus  = ogBCD.GetField("STY", "STYP", prlNotify->Text, "STYP", olTmp );
	}	

	if((UINT)m_CE_Ttyp.imID == wParam)
	{
		CString olTmp;
		prlNotify->Status  = ogBCD.GetField("NAT", "TTYP", prlNotify->Text, "TTYP", olTmp );
		prlNotify->UserStatus = true;

		if(prlNotify->Status == true && !prlNotify->Text.IsEmpty())
		{
			m_CE_Ttyp.SetInitText(prlNotify->Text, true);
		}
		
		return 0L;
//		prlNotify->UserStatus = true;
//		prlNotify->UserStatus  = ogBCD.GetField("NAT", "TTYP", prlNotify->Text, "TTYP", olTmp );
	}	
	return 0L;
}


BOOL RotationISFDlg::DestroyWindow() 
{
	BOOL blRet =  CDialog::DestroyWindow();
	return blRet;
}

