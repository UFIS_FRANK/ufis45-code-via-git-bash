// ReportTableDlg.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "resrc1.h"
#include "fpms.h"
#include "AdditionalReportTableDlg.h"
#include "ReportSelectDlg.h"
#include "RotationDlgCedaFlightData.h"
#include "Utils.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld CAdditionalReportTableDlg 


CAdditionalReportTableDlg::CAdditionalReportTableDlg(CWnd* pParent, int iTable, 
													 CCSPtrArray<ROTATIONDLGFLIGHTDATA> *popData,
                                                     char *pcpInfo, char *pcpSelect, 
													 char *pcpInfo2, CTime opMin, CTime opMax)
	                      :CDialog(CAdditionalReportTableDlg::IDD, pParent)
{

	pomData = popData;
	pcmInfo = pcpInfo;

	pcmInfo2 = pcpInfo2;	
	pcmSelect = pcpSelect;
	miTable = iTable;

	omMin = opMin;
	omMax = opMax;
	
	bmCommonDlg = false;

    // ab 2/99 DailySeason auf Datei
    pomReportTable = NULL;
	pomReportCTable = NULL;

	pomGanttChartReportWnd  = NULL;
	pomDGanttChartReportWnd = NULL;

	pomAdditionalReport1TableViewer = NULL;		//	case 0
	pomAdditionalReport2TableViewer = NULL;		//	case 1
	pomAdditionalReport3TableViewer = NULL;		//	case 3
	pomAdditionalReport4TableViewer = NULL;		//  case 4
	pomAdditionalReport5TableViewer = NULL;		//  case 2
	pomAdditionalReport6TableViewer = NULL;		//  case 5
	pomAdditionalReport7TableViewer = NULL;		//  case 6
	pomAdditionalReport8TableViewer = NULL;		//  case 7
	pomAdditionalReport9TableViewer = NULL;		//  case 9
	pomAdditionalReport10TableViewer = NULL;	//  case 10
	pomAdditionalReport11TableViewer = NULL;	//  case 12
	pomAdditionalReport12TableViewer = NULL;	//	case 13
	pomAdditionalReport16TableViewer = NULL;	//	case 16
	pomAdditionalReport17TableViewer = NULL;	//	case 17

	m_key = "DialogPosition\\Reports";
}


CAdditionalReportTableDlg::~CAdditionalReportTableDlg()
{
	if (pomReportTable != NULL)
	{
		delete pomReportTable;
	}
	if (pomReportCTable != NULL)
	{
		delete pomReportCTable;
	}

	if (pomAdditionalReport1TableViewer != NULL)
	{
		delete pomAdditionalReport1TableViewer;
	}
	if (pomAdditionalReport2TableViewer != NULL)
	{
		delete pomAdditionalReport2TableViewer;
	}
	if (pomAdditionalReport3TableViewer != NULL)
	{
		delete pomAdditionalReport3TableViewer;
	}
	if (pomAdditionalReport4TableViewer != NULL)
	{
		delete pomAdditionalReport4TableViewer;
	}
	if (pomAdditionalReport5TableViewer != NULL)
	{
		delete pomAdditionalReport5TableViewer;
	}
	if (pomAdditionalReport6TableViewer != NULL)
	{
		delete pomAdditionalReport6TableViewer;
	}
	if (pomAdditionalReport7TableViewer != NULL)
	{
		delete pomAdditionalReport7TableViewer;
	}
	if (pomAdditionalReport8TableViewer != NULL)
	{
		delete pomAdditionalReport8TableViewer;
	}
	if (pomAdditionalReport9TableViewer != NULL)
	{
		delete pomAdditionalReport9TableViewer;
	}
	if (pomAdditionalReport10TableViewer != NULL)
	{
		delete pomAdditionalReport10TableViewer;
	}
	if (pomAdditionalReport11TableViewer != NULL)
	{
		delete pomAdditionalReport11TableViewer;
	}
	if (pomAdditionalReport12TableViewer != NULL)
	{
		delete pomAdditionalReport12TableViewer;
	}
	if (pomAdditionalReport16TableViewer != NULL)
	{
		delete pomAdditionalReport16TableViewer;
	}
	if (pomAdditionalReport17TableViewer != NULL)
	{
		delete pomAdditionalReport17TableViewer;
	}

	if (pomGanttChartReportWnd != NULL)
	{
		delete pomGanttChartReportWnd;
	}
	if (pomDGanttChartReportWnd != NULL)
	{
		delete pomDGanttChartReportWnd;
	}



}


void CAdditionalReportTableDlg::DoDataExchange(CDataExchange* pDX)
{

	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAdditionalReportTableDlg)
	DDX_Control(pDX, IDC_BEENDEN, m_CB_Beenden);
	DDX_Control(pDX, IDC_PAPIER, m_CB_Papier);
	DDX_Control(pDX, IDC_DRUCKEN, m_CB_Drucken);
	DDX_Control(pDX, IDC_DATEI, m_CB_Datei);
	//}}AFX_DATA_MAP

}


BEGIN_MESSAGE_MAP(CAdditionalReportTableDlg, CDialog)
	//{{AFX_MSG_MAP(CAdditionalReportTableDlg)
	ON_BN_CLICKED(IDC_DATEI, OnDatei)
	ON_BN_CLICKED(IDC_DRUCKEN, OnDrucken)
	ON_BN_CLICKED(IDC_PAPIER, OnPapier)
	ON_BN_CLICKED(IDC_BEENDEN, OnBeenden)
	ON_WM_SIZE()
 	ON_WM_DESTROY()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Behandlungsroutinen f�r Nachrichten CAdditionalReportTableDlg 
void CAdditionalReportTableDlg::SaveToReg()
{
	BOOL ok = WriteDialogToReg((CWnd*) this, m_key);
}


void CAdditionalReportTableDlg::OnDestroy() 
{
	SaveToReg();
	CDialog::OnDestroy();
}

void CAdditionalReportTableDlg::OnSize(UINT nType, int cx, int cy)  
{
	CDialog::OnSize(nType, cx, cy);
	
	// TODO: Add your message handler code here
	m_resizeHelper.OnSize();

	if(pomReportTable != NULL)
	{
		CRect rect;	
		GetClientRect(&rect);
		rect.InflateRect(1, 1);     // hiding the CTable window border
		pomReportTable->SetPosition(rect.left, rect.right, rect.top+imDialogBarHeight, rect.bottom);
	}
	if(pomReportCTable != NULL)
	{
		CRect rect;	
		GetClientRect(&rect);
		rect.InflateRect(1, 1);     // hiding the CTable window border
		pomReportCTable->SetPosition(rect.left, rect.right, rect.top+imDialogBarHeight, rect.bottom);
	}

	this->Invalidate();
}



BOOL CAdditionalReportTableDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

	m_key = "DialogPosition\\Reports";
	
	// Table Initialisierung 
	CRect olrectTable;
	GetClientRect(&olrectTable);
    imDialogBarHeight = olrectTable.bottom - olrectTable.top;
    
	// extend dialog window to current screen width
    olrectTable.left = 0;
//    olrectTable.top = 65;
    olrectTable.top = 95;
    olrectTable.right = 1024;
    olrectTable.bottom = 768;
    MoveWindow(&olrectTable);
	
	int ilFlightCount;
	bool blValidTable = true ;

	// Default Ausgabe: Drucker
	m_CB_Papier.SetCheck( TRUE ) ;

	if( miTable < 14 )  // Reports verwenden CTable, au�er ...
	{
		pomReportTable = new CCSTable;
		pomReportTable->imSelectMode = 0;

		GetClientRect(&olrectTable);
		olrectTable.top = olrectTable.top + imDialogBarHeight;

		olrectTable.InflateRect(1,1);     // hiding the CTable window border
		//pomReportTable->SetTableData(this, olrectTable.left+2, olrectTable.right-2, olrectTable.top+28, olrectTable.bottom);
		pomReportTable->SetTableData(this, olrectTable.left, olrectTable.right, olrectTable.top, olrectTable.bottom);
		ilFlightCount = pomData->GetSize();
	}
	else if( miTable <= 19 ) // Wochenplan, Daily-Pl�ne und Inventar verwenden CTable
	{
		pomReportCTable = new CTable;
		pomReportCTable->imSelectMode = 0;

		GetClientRect(&olrectTable);
		olrectTable.top = olrectTable.top + imDialogBarHeight;

		olrectTable.InflateRect(1,1);     // hiding the CTable window border
		pomReportCTable->SetTableData(this, olrectTable.left, olrectTable.right, olrectTable.top, (olrectTable.bottom)+28);
	}
	else
	{
		blValidTable = false ;
		Beep( 500, 500 ) ;
		CString olMessage ;
		olMessage.Format( GetString( IDS_STRING1460) , miTable ) ;
		MessageBox( olMessage, "ReportTableDlg", MB_ICONWARNING ) ;
	}


	if( blValidTable )
	{
		switch( miTable )
		{
			case 0:	// all flights per airline
			{
				// to configure the header of the table
				CStringArray olColumnsToShow; 

				// Rotation
				olColumnsToShow.Add("Airlines");
				olColumnsToShow.Add("AFlights");
				olColumnsToShow.Add("DFlights");
				olColumnsToShow.Add("VFlights");

				pomAdditionalReport1TableViewer = 
					new AdditionalReport1TableViewer(pomData, olColumnsToShow, pcmInfo, pcmSelect);
				pomAdditionalReport1TableViewer->SetParentDlg(this);
				pomAdditionalReport1TableViewer->Attach(pomReportTable);

				olColumnsToShow.RemoveAll();

				SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
				pomAdditionalReport1TableViewer->ChangeViewTo("dummy");
				SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));

				char pclHeadlineSelect[120];
				sprintf(pclHeadlineSelect, GetString(IDS_STRING1608), pcmInfo, ilFlightCount);
				omHeadline = pclHeadlineSelect;
			}
			break;
			case 1: // Statistics of Flights that take of from the same originating Aiport
			{
				// to configure the header of the table
				CStringArray olColumnsToShow; 

				// Rotation
				olColumnsToShow.Add("Airport");
				olColumnsToShow.Add("Flights");
				olColumnsToShow.Add("ViaFlights");

				pomAdditionalReport2TableViewer = 
					new AdditionalReport2TableViewer(pomData, olColumnsToShow, pcmInfo, pcmSelect);
				pomAdditionalReport2TableViewer->SetParentDlg(this);
				pomAdditionalReport2TableViewer->Attach(pomReportTable);

				olColumnsToShow.RemoveAll();

				SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
				pomAdditionalReport2TableViewer->ChangeViewTo("dummy");
				SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));

				char pclHeadlineSelect[120];
				ilFlightCount = pomData->GetSize();
				sprintf(pclHeadlineSelect, GetString(IDS_STRING1571), pcmInfo, 
						pomAdditionalReport2TableViewer->GetArrFlightCount());
				omHeadline = pclHeadlineSelect;
			}
			break;
			case 2:	// same AC-Registration
			{
				// to configure the header of the table
				CStringArray olColumnsToShow; 

				olColumnsToShow.Add("AC-Regn");
				olColumnsToShow.Add("AFlights");
				olColumnsToShow.Add("DFlights");

				pomAdditionalReport5TableViewer = 
					new AdditionalReport5TableViewer(pomData, olColumnsToShow, pcmInfo, pcmSelect);
				pomAdditionalReport5TableViewer->SetParentDlg(this);
				pomAdditionalReport5TableViewer->Attach(pomReportTable);

				SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
				pomAdditionalReport5TableViewer->ChangeViewTo("dummy");
				SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));

				char pclHeadlineSelect[120];
				ilFlightCount = pomData->GetSize();
				sprintf(pclHeadlineSelect, GetString(IDS_STRING1592), pcmInfo, ilFlightCount);
				omHeadline = pclHeadlineSelect;
			}
			break;
			case 3:	// same destination airport
			{
				
				// to configure the header of the table
				CStringArray olColumnsToShow; 

				// Rotation
				olColumnsToShow.Add("Airport");
				olColumnsToShow.Add("Flights");
				olColumnsToShow.Add("ViaFlights");

				pomAdditionalReport3TableViewer = 
					new AdditionalReport3TableViewer(pomData, olColumnsToShow, pcmInfo, pcmSelect);
				pomAdditionalReport3TableViewer->SetParentDlg(this);
				pomAdditionalReport3TableViewer->Attach(pomReportTable);

				olColumnsToShow.RemoveAll();

				SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
				pomAdditionalReport3TableViewer->ChangeViewTo("dummy");
				SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));

				char pclHeadlineSelect[120];
				ilFlightCount = pomData->GetSize();
				sprintf(pclHeadlineSelect, GetString(IDS_STRING1577), pcmInfo, 
						pomAdditionalReport3TableViewer->GetDepFlightCount());
				omHeadline = pclHeadlineSelect;
				
			}
			break;
			case 4: // same AC-Type
			{
				// to configure the header of the table
				CStringArray olColumnsToShow; 

				olColumnsToShow.Add("ACType");
				olColumnsToShow.Add("AFlights");
				olColumnsToShow.Add("DFlights");

				pomAdditionalReport4TableViewer = 
					new AdditionalReport4TableViewer(pomData, olColumnsToShow, pcmInfo, pcmSelect);
				pomAdditionalReport4TableViewer->SetParentDlg(this);
				pomAdditionalReport4TableViewer->Attach(pomReportTable);

				olColumnsToShow.RemoveAll();

				SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
				pomAdditionalReport4TableViewer->ChangeViewTo("dummy");
				SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));

				char pclHeadlineSelect[120];
				ilFlightCount = pomData->GetSize();
				sprintf(pclHeadlineSelect, GetString(IDS_STRING1582), pcmInfo, 
						pomAdditionalReport4TableViewer->GetFlightCount());
				omHeadline = pclHeadlineSelect;
			}
			break;
			case 5:	// same Flight Nature
			{
				// to configure the header of the table
				CStringArray olColumnsToShow; 

				olColumnsToShow.Add("FlightNature");
				olColumnsToShow.Add("AFlights");
				olColumnsToShow.Add("DFlights");

				pomAdditionalReport6TableViewer = 
					new AdditionalReport6TableViewer(pomData, olColumnsToShow, pcmInfo, pcmSelect);
				pomAdditionalReport6TableViewer->SetParentDlg(this);
				pomAdditionalReport6TableViewer->Attach(pomReportTable);

				olColumnsToShow.RemoveAll();

				SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
				pomAdditionalReport6TableViewer->ChangeViewTo("dummy");
				SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));

				char pclHeadlineSelect[120];
				ilFlightCount = pomData->GetSize();
				sprintf(pclHeadlineSelect, GetString(IDS_STRING1603), pcmInfo, 
						pomAdditionalReport6TableViewer->GetFlightCount());
				omHeadline = pclHeadlineSelect;
			}
			break;
			case 6:	// Delay Reason
			{
				// to configure the header of the table
				CStringArray olColumnsToShow; 

				olColumnsToShow.Add("DCode");
				olColumnsToShow.Add("DReason");
				olColumnsToShow.Add("Airlines");
				olColumnsToShow.Add("AFlights");
				olColumnsToShow.Add("DFlights");

				pomAdditionalReport7TableViewer = 
					new AdditionalReport7TableViewer(pomData, olColumnsToShow, pcmInfo, pcmSelect);
				pomAdditionalReport7TableViewer->SetParentDlg(this);
				pomAdditionalReport7TableViewer->Attach(pomReportTable);

				olColumnsToShow.RemoveAll();

				SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
				pomAdditionalReport7TableViewer->ChangeViewTo("dummy");
				SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));

				char pclHeadlineSelect[120];
				ilFlightCount = pomData->GetSize();
				sprintf(pclHeadlineSelect, GetString(IDS_STRING1609), pcmInfo, 
						pomAdditionalReport7TableViewer->GetFlightCount());
				omHeadline = pclHeadlineSelect;
			}
			break;
			case 7:	// Delay Time
			{
				// to configure the header of the table
				CStringArray olColumnsToShow; 

				olColumnsToShow.Add("DTime");
				olColumnsToShow.Add("Airlines");
				olColumnsToShow.Add("AFlights");
				olColumnsToShow.Add("DFlights");

				pomAdditionalReport8TableViewer = 
					new AdditionalReport8TableViewer(pomData, olColumnsToShow, pcmInfo, pcmSelect);
				pomAdditionalReport8TableViewer->SetParentDlg(this);
				pomAdditionalReport8TableViewer->Attach(pomReportTable);

				olColumnsToShow.RemoveAll();

				SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
				pomAdditionalReport8TableViewer->ChangeViewTo("dummy");
				SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));

				char pclHeadlineSelect[120];
				ilFlightCount = pomData->GetSize();
				sprintf(pclHeadlineSelect, GetString(IDS_STRING1615), pcmInfo, 
						pomAdditionalReport8TableViewer->GetFlightCount());
				omHeadline = pclHeadlineSelect;
			}
			break;
			case 8:	
			{
			}
			break;
			case 9:	// flight cancelled
			{
				CStringArray olColumnsToShow; 

				olColumnsToShow.Add("Time");
				olColumnsToShow.Add("AFlights");
				olColumnsToShow.Add("DFlights");

				pomAdditionalReport9TableViewer = 
					new AdditionalReport9TableViewer(pomData, olColumnsToShow, pcmInfo, pcmSelect);
				pomAdditionalReport9TableViewer->SetParentDlg(this);
				pomAdditionalReport9TableViewer->Attach(pomReportTable);

				olColumnsToShow.RemoveAll();

				SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
				pomAdditionalReport9TableViewer->ChangeViewTo("dummy");
				SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));

				char pclHeadlineSelect[120];
				ilFlightCount = pomData->GetSize();
				sprintf(pclHeadlineSelect, GetString(IDS_STRING1624), pcmInfo, 
						pomAdditionalReport9TableViewer->GetFlightCount());
				omHeadline = pclHeadlineSelect;
			}
			break;
			case 10:		// SHA: Flight statistic according to arrival/departure each day
			{
				CStringArray olColumnsToShow; 

				olColumnsToShow.Add("Time");
				olColumnsToShow.Add("AFlights");
				olColumnsToShow.Add("DFlights");

				pomAdditionalReport10TableViewer = 
					new AdditionalReport10TableViewer(pomData, olColumnsToShow, pcmInfo, pcmSelect);
				pomAdditionalReport10TableViewer->SetParentDlg(this);
				pomAdditionalReport10TableViewer->Attach(pomReportTable);

				olColumnsToShow.RemoveAll();

				SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
				pomAdditionalReport10TableViewer->ChangeViewTo("dummy");
				SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));

				char pclHeadlineSelect[120];
				ilFlightCount = pomData->GetSize();
				sprintf(pclHeadlineSelect, GetString(IDS_STRING1631), pcmInfo, 
						pomAdditionalReport10TableViewer->GetFlightCount());
				omHeadline = pclHeadlineSelect;
			}
			break;
			case 11:
			{
			}
			break;
			case 12:	// SHA: Flight statistic according to rush hour
			{
				CStringArray olColumnsToShow;
				olColumnsToShow.Add("Time");
				olColumnsToShow.Add("AFlights");
				olColumnsToShow.Add("DFlights");

				pomAdditionalReport11TableViewer = 
					new AdditionalReport11TableViewer(pomData, olColumnsToShow, pcmInfo, pcmSelect);
				pomAdditionalReport11TableViewer->SetParentDlg(this);
				pomAdditionalReport11TableViewer->Attach(pomReportTable);

				olColumnsToShow.RemoveAll();

				SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
				pomAdditionalReport11TableViewer->ChangeViewTo("dummy");
				SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));

				char pclHeadlineSelect[120];

				CString olTimes;
				if (bgReportLocal)
					olTimes = GetString(IDS_STRING1921);
				else
					olTimes = GetString(IDS_STRING1920);

				ilFlightCount = pomData->GetSize();
				sprintf(pclHeadlineSelect, GetString(IDS_STRING1655), pcmSelect, pcmInfo, 
						pomAdditionalReport11TableViewer->GetFlightCount(), olTimes);
				omHeadline = pclHeadlineSelect;

				
				CRect olRect;
				GetClientRect(&olRect);

				// Anzhal Balken
				int ilBarCount = pomAdditionalReport11TableViewer->GetBarCount();
				CUIntArray olData;
				CStringArray olXAxeText;
				m_CB_Datei.EnableWindow(false);

				for (int i = 0; i < ilBarCount; i++)
				{
					olData.Add(pomAdditionalReport11TableViewer->GetData(i));
					olXAxeText.Add(pomAdditionalReport11TableViewer->GetTime(i));
				}
				CColor olColor;
				if (pomGanttChartReportWnd)
					delete pomGanttChartReportWnd;
				pomGanttChartReportWnd = new GanttChartReportWnd(this, "by hour", 
					                                             "the number of flights", olColor);

				// Daten (=Balkenhoehe) �bergeben
				pomGanttChartReportWnd->SetData(olData);
				// Text der XAxe �bergeben
				pomGanttChartReportWnd->SetXAxeText(olXAxeText);

				pomGanttChartReportWnd->Create(NULL, "Chart", WS_CHILD | WS_VISIBLE, olRect, this, 0);
				pomGanttChartReportWnd->MoveWindow(olRect, true);


			}
			break;
			case 13:	// SHA: Flight statistic according to time frame
			{
				CStringArray olColumnsToShow;
				olColumnsToShow.Add("Time");
				olColumnsToShow.Add("AFlights");
				olColumnsToShow.Add("DFlights");

				pomAdditionalReport12TableViewer = 
					new AdditionalReport12TableViewer(pomData, olColumnsToShow, pcmInfo, pcmSelect);
				pomAdditionalReport12TableViewer->SetParentDlg(this);
				pomAdditionalReport12TableViewer->Attach(pomReportTable);

				olColumnsToShow.RemoveAll();

				SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
				pomAdditionalReport12TableViewer->ChangeViewTo("dummy");
				SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));

				char pclHeadlineSelect[120];
				ilFlightCount = pomData->GetSize();
				sprintf(pclHeadlineSelect, GetString(IDS_STRING1652), pcmInfo, 
						pomAdditionalReport12TableViewer->GetFlightCount());
				omHeadline = pclHeadlineSelect;
				
				CRect olRect;
				GetClientRect(&olRect);

				// Anzhal Balken
				int ilBarCount = pomAdditionalReport12TableViewer->GetBarCount();
				CUIntArray olData;
				CStringArray olXAxeText;
				CString olNo;
				m_CB_Datei.EnableWindow(false);

				int ilXAxe = 0;
				for (int i = 0; i < ilBarCount; i++, ilXAxe++)
				{
					olData.Add(pomAdditionalReport12TableViewer->GetData(i));
					olNo.Format("%d", ilXAxe);
					olXAxeText.Add(olNo);
				}
				CColor olColor;
				if (pomGanttChartReportWnd)
					delete pomGanttChartReportWnd;

				pomGanttChartReportWnd = new GanttChartReportWnd(this, "by day", 
					                                             "the number of flights", olColor);
				// Daten (=Balkenhoehe) �bergeben
				pomGanttChartReportWnd->SetData(olData);
				// Text der XAxe �bergeben
				pomGanttChartReportWnd->SetXAxeText(olXAxeText);

				pomGanttChartReportWnd->Create(NULL, "Chart", WS_CHILD | WS_VISIBLE, olRect, this, 0);
				pomGanttChartReportWnd->MoveWindow(olRect, true);
			}
			break;

			case 16:
			{
				pomAdditionalReport16TableViewer = 
					new AdditionalReport16TableViewer(pcmInfo, pcmSelect,omMin, omMax);
				pomAdditionalReport16TableViewer->SetParentDlg(this);

				omHeadline = GetString(IDS_STRING1750);
				
				CRect olRect;
				GetClientRect(&olRect);

				// Anzahl Balken
				int ilBarCount = pomAdditionalReport16TableViewer->GetBarCount();
				CUIntArray olData1;
				CUIntArray olData2;
				CStringArray olXAxeText;
				CString olNo;
				m_CB_Datei.EnableWindow(false);

				int ilXAxe = 0;
				for (int i = 0; i < ilBarCount; i++, ilXAxe++)
				{
					olData1.Add(pomAdditionalReport16TableViewer->GetDataCca(i));
					olNo.Format("%d", ilXAxe);
					olXAxeText.Add(olNo);
				}
				for (i = 0; i < ilBarCount; i++, ilXAxe++)
				{
					olData2.Add(pomAdditionalReport16TableViewer->GetDataCic(i));
				}
				CColor olColor;
				if (pomDGanttChartReportWnd)
					delete pomDGanttChartReportWnd;

				pomDGanttChartReportWnd = new DGanttChartReportWnd(this, GetString(IDS_STRING1763), 
		                                                           GetString(IDS_STRING1766), 
													               GetString(IDS_STRING1767),
													               olColor);
				// Daten (=Balkenhoehe) �bergeben
				pomDGanttChartReportWnd->SetData1(olData1);
				pomDGanttChartReportWnd->SetData2(olData2);
				// Text der XAxe �bergeben
				pomDGanttChartReportWnd->SetXAxeText(olXAxeText);
				pomDGanttChartReportWnd->SetTotalNumnber(pomAdditionalReport16TableViewer->NumberOfCic());
				// Text der XAxe �bergeben
				pomDGanttChartReportWnd->SetXAxeText(olXAxeText);
				CString olDate = omMin.Format("%Y.%m.%d");
				olDate += " ";
				olDate += GetString(IDS_STRING1762);
				pomDGanttChartReportWnd->SetLegende(GetString(IDS_STRING1760), GetString(IDS_STRING1761), olDate);

				pomDGanttChartReportWnd->Create(NULL, "Chart", WS_CHILD | WS_VISIBLE, olRect, this, 0);
				pomDGanttChartReportWnd->MoveWindow(olRect, true);
			}
			break;

			case 17:
			{
				pomAdditionalReport17TableViewer = new AdditionalReport17TableViewer(pcmInfo, pcmSelect,
					                                                                 omMin, omMax);
				pomAdditionalReport17TableViewer->SetParentDlg(this);

				omHeadline = GetString(IDS_STRING1751);
				
				CRect olRect;
				GetClientRect(&olRect);

				// Anzhal Balken
				int ilBarCount = pomAdditionalReport17TableViewer->GetBarCount();
				CUIntArray olData1;
				CUIntArray olData2;
				CStringArray olXAxeText;
				CString olNo;
				m_CB_Datei.EnableWindow(false);

				int ilXAxe = 0;
				for (int i = 0; i < ilBarCount; i++, ilXAxe++)
				{
					olData1.Add(pomAdditionalReport17TableViewer->GetDataPst(i));
					olNo.Format("%d", ilXAxe);
					olXAxeText.Add(olNo);
				}
				for (i = 0; i < ilBarCount; i++, ilXAxe++)
				{
					olData2.Add(pomAdditionalReport17TableViewer->GetDataPstFailure(i));
				}
				
				CColor olColor;
				if (pomDGanttChartReportWnd)
					delete pomDGanttChartReportWnd;

				pomDGanttChartReportWnd = new DGanttChartReportWnd(this, GetString(IDS_STRING1763)/*"time(hour)"*/, 
		                                                           GetString(IDS_STRING1764)/*"number of spots"*/, 
													               GetString(IDS_STRING1765),
													               olColor);
				// Daten (=Balkenhoehe) �bergeben
				pomDGanttChartReportWnd->SetData1(olData1);
				pomDGanttChartReportWnd->SetData2(olData2);
				pomDGanttChartReportWnd->SetTotalNumnber(pomAdditionalReport17TableViewer->NumberOfPst());
				// Text der XAxe �bergeben
				pomDGanttChartReportWnd->SetXAxeText(olXAxeText);
				CString olDate = omMin.Format("%Y.%m.%d");
				olDate += " ";
				olDate += GetString(IDS_STRING1754);
				pomDGanttChartReportWnd->SetLegende(GetString(IDS_STRING1752), GetString(IDS_STRING1753), olDate);

				pomDGanttChartReportWnd->Create(NULL, "Chart", WS_CHILD | WS_VISIBLE, olRect, this, 0);
				pomDGanttChartReportWnd->MoveWindow(olRect, true);

			}
			break;

			default:
			{
				Beep( 500, 500 ) ;
				MessageBox( GetString(ST_FEHLER)+" serious data damage !" ,"ReportTableDlg", MB_ICONERROR ) ;
			}

		}	// end switch( miTable ...
	}	// end if( ValidTable...

	CDialog::SetWindowText(omHeadline);	//Dialogbox-�berschrift

	m_resizeHelper.Init(this->m_hWnd);
	m_resizeHelper.Fix(IDC_BEENDEN,DlgResizeHelper::kNoHFix,DlgResizeHelper::kHeight);
	m_resizeHelper.Fix(IDC_DRUCKEN,DlgResizeHelper::kNoHFix,DlgResizeHelper::kHeight);
	m_resizeHelper.Fix(IDC_PAPIER,DlgResizeHelper::kNoHFix,DlgResizeHelper::kHeight);
	m_resizeHelper.Fix(IDC_DATEI,DlgResizeHelper::kNoHFix,DlgResizeHelper::kHeight);
	m_resizeHelper.Fix(IDC_STATIC,DlgResizeHelper::kWidth,DlgResizeHelper::kHeight);

	CRect olRect;
	this->GetWindowRect(&olRect);
	GetDialogFromReg(olRect, m_key);
	SetWindowPos(&wndTop, olRect.left, olRect.top, olRect.Width(), olRect.Height(), SWP_SHOWWINDOW);// | SWP_NOMOVE );

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX-Eigenschaftenseiten sollten FALSE zur�ckgeben
}



void CAdditionalReportTableDlg::OnDatei() 
{
	// Daily-Pl�ne nur auf Papier 
	/*if (m_CB_Datei.GetCheck())
	{
		MessageBox( GetString(IDS_STRING1403), MB_OK ) ;
	}*/
}

void CAdditionalReportTableDlg::OnDrucken() 
{

	// Wer geht nur auf Papier raus ? : Daily-Pl�ne 

	bool blAlwaysPaper = false ;

	if( ! blAlwaysPaper )
	{
		if((m_CB_Papier.GetCheck() != 1) && (m_CB_Datei.GetCheck() != 1))
		{
			// kein Druck-Medium gew�hlt
			// hbe: vorher /*IDS_STRING935*/
			if( MessageBox(GetString(IDS_STRING1434), GetString(IMFK_PRINT), MB_YESNO|MB_ICONQUESTION ) != IDYES ) return ;
			else blAlwaysPaper = true ;
		}
	}

	// ----------------------------------------------------------- Druckerausgabe 
	if (m_CB_Papier.GetCheck() == 1 || blAlwaysPaper )
	{
		switch(miTable)
		{
			case 0:	
			{
				pomAdditionalReport1TableViewer->PrintTableView();
			}
			break;
			case 1:	
			{
				pomAdditionalReport2TableViewer->PrintTableView();
			}
			break;
			case 2:	
			{
				pomAdditionalReport5TableViewer->PrintTableView();
			}
			break;
			case 3:	
			{
				pomAdditionalReport3TableViewer->PrintTableView();
			}
			break;
			case 4:	
			{
				pomAdditionalReport4TableViewer->PrintTableView();
			}
			break;
			case 5:	
			{
				pomAdditionalReport6TableViewer->PrintTableView();
			}
			break;
			case 6:	
			{
				pomAdditionalReport7TableViewer->PrintTableView();
			}
			break;
			case 7:
			{
				pomAdditionalReport8TableViewer->PrintTableView();
			}
			break;
			case 8:
			{
			}
			break;
			case 9:
			{
				pomAdditionalReport9TableViewer->PrintTableView();
			}
			break;
			case 10:
			{
				pomAdditionalReport10TableViewer->PrintTableView();
			}
			break;
			case 11:
			{
			}
			break;
			case 12:
			{
				pomAdditionalReport11TableViewer->PrintTableView(pomGanttChartReportWnd);
			}
			break;
			case 13:
			{
				pomAdditionalReport12TableViewer->PrintTableView(pomGanttChartReportWnd);
			}
			break;
			case 14:
			{
			}
			break;
			case 15:
			{
			}
			break;
			case 16:
			{
				pomAdditionalReport16TableViewer->PrintTableView(pomDGanttChartReportWnd);
			}
			break;
			case 17:
			{
				pomAdditionalReport17TableViewer->PrintTableView(pomDGanttChartReportWnd);
			}
			break;
			case 18:
			{
			}
			break;

			case 19:
			{
			}
			break;
		}
	}


	// -------------------------------------- Drucken in Datei: alle au�er OPS-Plan
	if (m_CB_Datei.GetCheck() == 1 && ! blAlwaysPaper )
	{

		LPOPENFILENAME polOfn = new OPENFILENAME;
		char buffer[256] = "";
		char buffer2[256] = "";
		LPSTR lpszStr;
		lpszStr = buffer;
		CString olStr = CString("c:\\tmp\\") + CString("*.txt");
		strcpy(buffer,(LPCTSTR)olStr);

		memset(polOfn, 0, sizeof(*polOfn));
		polOfn->lStructSize = sizeof(*polOfn) ;
		polOfn->lpstrFilter = "Export File(*.txt)\0*.txt\0";
		polOfn->lpstrFile = (LPSTR) buffer;
		polOfn->nMaxFile = 256;
		strcpy(buffer2, GetString(IDS_STRING1077));
		polOfn->lpstrTitle = buffer2;
		bmCommonDlg	= true;

		if(GetOpenFileName(polOfn) == TRUE)
		{
			bmCommonDlg	= false;
			char pclDefPath[512];
			strcpy(pclDefPath, polOfn->lpstrFile);
			int ilPathlen = strlen(pclDefPath);
			//ilPathlen = ilPathlen -4;
			pclDefPath[(ilPathlen)] = '\0';
			for(int iLen = ilPathlen; iLen > 0; iLen--)
			{
				if(pclDefPath[iLen] == '\\')
				{
/*					if((ilPathlen - iLen) > 8)	// Dateiname zu lang
					{
						pclDefPath[ilPathlen - (ilPathlen - iLen) + 9] = '\0';
					}
					break;*/
				}
			}
			if ((strstr(pclDefPath, ".")) == 0)
				strcat(pclDefPath, ".txt");

			switch(miTable)
			{
				case 0:	
				{
					pomAdditionalReport1TableViewer->PrintPlanToFile(pclDefPath);
				}
				break;
				case 1:	
				{
					pomAdditionalReport2TableViewer->PrintPlanToFile(pclDefPath);
				}
				break;
				case 2:
				{
					pomAdditionalReport5TableViewer->PrintPlanToFile(pclDefPath);
				}
				break;
				case 3:
				{
					pomAdditionalReport3TableViewer->PrintPlanToFile(pclDefPath);
				}
				break;
				case 4:	
				{
					pomAdditionalReport4TableViewer->PrintPlanToFile(pclDefPath);
				}
				break;
				case 5:
				{
					pomAdditionalReport6TableViewer->PrintPlanToFile(pclDefPath);
				}
				break;
				case 6:
				{
					pomAdditionalReport7TableViewer->PrintPlanToFile(pclDefPath);
				}
				break;
				case 7:
				{
					pomAdditionalReport8TableViewer->PrintPlanToFile(pclDefPath);
				}
				break;
				case 8:
				{
				}
				break;
				case 9:
				{
					pomAdditionalReport9TableViewer->PrintPlanToFile(pclDefPath);
				}
				break;
				case 10:
				{
					pomAdditionalReport10TableViewer->PrintPlanToFile(pclDefPath);
				}
				break;
				case 12:
				{
					pomAdditionalReport11TableViewer->PrintPlanToFile(pclDefPath);
				}
				break;
				case 13:
				{
					pomAdditionalReport12TableViewer->PrintPlanToFile(pclDefPath);
				}
				break;
				case 16:
				{
					pomAdditionalReport12TableViewer->PrintPlanToFile(pclDefPath);
				}
				break;
				case 17:
				{
				}
				break;
			}
		}
		else
 			bmCommonDlg	= false;

		delete polOfn;

	}

}

void CAdditionalReportTableDlg::OnPapier() 
{
	;
}


void CAdditionalReportTableDlg::OnBeenden() 
{

	if (!bmCommonDlg)
		CDialog::OnCancel();

}


void CAdditionalReportTableDlg::OnCancel() 
{

	if (!bmCommonDlg)
		CDialog::OnCancel();

}
