#if !defined(AFX_CHANGEREGISTRAION_H__9C764794_D368_11D5_801D_0001022205EE__INCLUDED_)
#define AFX_CHANGEREGISTRAION_H__9C764794_D368_11D5_801D_0001022205EE__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ChangeRegistraion.h : header file
//
#include "resrc1.h"
#include "CCSEdit.h"
#include <afxdtctl.h>


/////////////////////////////////////////////////////////////////////////////
// CChangeRegistraion dialog

class CChangeRegistraion : public CDialog
{
// Construction
public:
	CChangeRegistraion(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CChangeRegistraion)
	enum { IDD = IDD_CHANGEREGISTRATION };
	CEdit	m_CE_DFlights;
	CEdit	m_CE_AFlights;
	CEdit	m_CE_Numbers;
	CDateTimeCtrl	m_CE_ActualDay;
	CCSEdit	m_CE_Regn;
	CEdit	m_CE_Act5;
	CEdit	m_CE_Act3;
	CString	m_Act3; 
	CString	m_Act5;
	CString	m_Regn;
	CTime	m_ActualDay;
	CString	m_Numbers;
	CString	m_AFlights;
	CString	m_DFlights;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CChangeRegistraion)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CChangeRegistraion)
	afx_msg LONG OnEditKillfocus( UINT wParam, LPARAM lParam);
	afx_msg LONG OnEditChanged( UINT wParam, LPARAM lParam);
	virtual void OnOK();
	virtual BOOL OnInitDialog();
	afx_msg LONG OnMark( UINT wParam, LPARAM lParam);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

private:
	void MakeUtc( CString& ropMinStr, CString& ropMaxStr );


};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CHANGEREGISTRAION_H__9C764794_D368_11D5_801D_0001022205EE__INCLUDED_)
