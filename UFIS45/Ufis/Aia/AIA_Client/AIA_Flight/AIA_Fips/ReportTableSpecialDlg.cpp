// ReportTableDlg.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "fpms.h"
#include "ReportTableSpecialDlg.h"
#include "ReportSelectDlg.h"
#include "RotationDlgCedaFlightData.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld CReportTableSpecialDlg 


CReportTableSpecialDlg::CReportTableSpecialDlg(CWnd* pParent, int iTable, 
											   CCSPtrArray<ROTATIONDLGFLIGHTDATA> *popData,
								               char *pcpInfo, char *pcpSelect, 
											   char *pcpInfo2, int ipArrDep )
	                   :CDialog(CReportTableSpecialDlg::IDD, pParent)
{

	pomData = popData;
	pcmInfo = pcpInfo;
	pcmInfo2 = pcpInfo2;	
	pcmSelect = pcpSelect;
	miTable = iTable;
    pomReportTable = NULL;
	pomReportCTable = NULL;

	// SHA-Reports
	pomReportDomIntTranTableViewer	= NULL;	// case 11	
	imArrDep = ipArrDep;
	bmCommonDlg = false;

}

CReportTableSpecialDlg::~CReportTableSpecialDlg()
{

	// SHA - Reports
	if (pomReportDomIntTranTableViewer)
		delete pomReportDomIntTranTableViewer;

}

void CReportTableSpecialDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CReportTableSpecialDlg)
	DDX_Control(pDX, IDC_BEENDEN, m_CB_Beenden);
	DDX_Control(pDX, IDC_PAPIER, m_CB_Papier);
	DDX_Control(pDX, IDC_DRUCKEN, m_CB_Drucken);
	DDX_Control(pDX, IDC_DATEI, m_CB_Datei);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CReportTableSpecialDlg, CDialog)
	//{{AFX_MSG_MAP(CReportTableSpecialDlg)
	ON_BN_CLICKED(IDC_DATEI, OnDatei)
	ON_BN_CLICKED(IDC_DRUCKEN, OnDrucken)
	ON_BN_CLICKED(IDC_PAPIER, OnPapier)
	ON_BN_CLICKED(IDC_BEENDEN, OnBeenden)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Behandlungsroutinen f�r Nachrichten CReportTableSpecialDlg 

BOOL CReportTableSpecialDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// Table Initialisierung 
	CRect olrectTable;

	GetClientRect(&olrectTable);
    imDialogBarHeight = olrectTable.bottom - olrectTable.top;

//	CStatic* polStaticFlights = (CStatic*)GetDlgItem(IDC_STATIC_FLIGHTS);
//	CStatic* polStaticPassengers = (CStatic*)GetDlgItem(IDC_STATIC_PASSENGERS);

   
	// extend dialog window to current screen width
    olrectTable.left = 0;
//    olrectTable.top = 65;
    olrectTable.top = 95;
    olrectTable.right = 1024;
    olrectTable.bottom = 768;
    MoveWindow(&olrectTable);
	
	int ilFlightCount;
	bool blValidTable = true ;

	// Default Ausgabe: Drucker
	m_CB_Papier.SetCheck( TRUE ) ;

	pomReportTable = new CCSTable;
	pomReportTable->imSelectMode = 0;

	GetClientRect(&olrectTable);
	olrectTable.top = olrectTable.top + imDialogBarHeight;

	olrectTable.InflateRect(1,1);     // hiding the CTable window border
	//pomReportTable->SetTableData(this, olrectTable.left+2, olrectTable.right-2, olrectTable.top+28, olrectTable.bottom);
	pomReportTable->SetTableData(this, olrectTable.left, olrectTable.right, olrectTable.top, olrectTable.bottom);
	ilFlightCount = pomData->GetSize();


	if( blValidTable )
	{
		switch( miTable )
		{
			case 11:	//	Statistik Sitzplatzangebot nach LVGs und/oder Destinationen
				// SHA: Statistic according to domestic/international/transfer
				if ((ogCustomer == "SHA") && (ogAppName == "FIPS"))
				{
					CStringArray olColumnsToShow; 

					// Rotation
					olColumnsToShow.Add("Date");
					olColumnsToShow.Add("FArrival");
					olColumnsToShow.Add("FDeparture");
					olColumnsToShow.Add("FTotal");
					olColumnsToShow.Add("PArrival");
					olColumnsToShow.Add("PDeparture");
					olColumnsToShow.Add("PTransit");
					olColumnsToShow.Add("PTotal");

					pomReportDomIntTranTableViewer = new ReportDomIntTranTableViewer(pomData, olColumnsToShow, pcmInfo, pcmSelect);
					pomReportDomIntTranTableViewer->SetParentDlg(this);
					pomReportDomIntTranTableViewer->Attach(pomReportTable);

					olColumnsToShow.RemoveAll();
					
					SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
					pomReportDomIntTranTableViewer->ChangeViewTo("dummy");
					SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));

					char pclHeadlineSelect[120];
					ilFlightCount = pomData->GetSize();
					sprintf(pclHeadlineSelect, GetString(IDS_STRING1666), pcmInfo, 
						    pomReportDomIntTranTableViewer->GetFlightCount());
					omHeadline = pclHeadlineSelect;
				}
			break;
			default:
				blValidTable = false ;
				Beep( 500, 500 ) ;
				CString olMessage ;
				olMessage.Format( GetString( IDS_STRING1460) , miTable ) ;
				MessageBox( olMessage, "ReportTableDlg", MB_ICONWARNING ) ;
			break;
		}	// end switch( miTable ...
	}	// end if( ValidTable ...

	CDialog::SetWindowText(omHeadline);	//Dialogbox-�berschrift

	
	// Test mit Static Text �ber Spalten
/*	RECT olRect;
	olRect.left   = 100;
	olRect.right  = 200;
	olRect.top    = 30;
	olRect.bottom = 30 + 15;
	omStatic = new	CStatic();

	omStatic->Create("Flights", WS_VISIBLE | WS_CHILD | SS_GRAYRECT | SS_CENTER, olRect, this);*/
	// Test Ende


	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX-Eigenschaftenseiten sollten FALSE zur�ckgeben
}



void CReportTableSpecialDlg::OnDatei() 
{

}

void CReportTableSpecialDlg::OnDrucken() 
{

	if((m_CB_Papier.GetCheck() != 1) && (m_CB_Datei.GetCheck() != 1))
	{
		// kein Druck-Medium gew�hlt
		// hbe: vorher /*IDS_STRING935*/
		if( MessageBox(GetString(IDS_STRING1434), GetString(IMFK_PRINT), MB_YESNO|MB_ICONQUESTION ) != IDYES ) 
			return;
	}

	// ----------------------------------------------------------- Druckerausgabe 
	if (m_CB_Papier.GetCheck() == 1 )
	{
		switch(miTable)
		{
			case 11:	//	Sitzplatzangebot nach LVGs und/oder Destinationen
			{
				pomReportDomIntTranTableViewer->PrintTableView();
			}
			break;
		}
	}


	// -------------------------------------- Drucken in Datei: alle au�er OPS-Plan
	if (m_CB_Datei.GetCheck() == 1 )
	{

		LPOPENFILENAME polOfn = new OPENFILENAME;
		char buffer[256] = "";
		char buffer2[256] = "";
		LPSTR lpszStr;
		lpszStr = buffer;
		CString olStr = CString("c:\\tmp\\") + CString("*.txt");
		strcpy(buffer,(LPCTSTR)olStr);

		memset(polOfn, 0, sizeof(*polOfn));
		polOfn->lStructSize = sizeof(*polOfn) ;
		polOfn->lpstrFilter = "Export File(*.txt)\0*.txt\0";
		polOfn->lpstrFile = (LPSTR) buffer;
		polOfn->nMaxFile = 256;
		strcpy(buffer2, GetString(IDS_STRING1077));
		polOfn->lpstrTitle = buffer2;

		bmCommonDlg = true;
		if(GetOpenFileName(polOfn) == TRUE)
		{
			bmCommonDlg = false;
			char pclDefPath[512];
			strcpy(pclDefPath, polOfn->lpstrFile);
			int ilPathlen = strlen(pclDefPath);
			//ilPathlen = ilPathlen -4;
			pclDefPath[(ilPathlen)] = '\0';
			for(int iLen = ilPathlen; iLen > 0; iLen--)
			{
				if(pclDefPath[iLen] == '\\')
				{
/*					if((ilPathlen - iLen) > 8)	// Dateiname zu lang
					{
						pclDefPath[ilPathlen - (ilPathlen - iLen) + 9] = '\0';
					}*/
					break;
				}
			}
			if ((strstr(pclDefPath, ".")) == 0)
				strcat(pclDefPath, ".txt");

			switch(miTable)
			{
			case 11:	//	Sitzplatzangebot nach LVGs und/oder Destinationen
			{
				pomReportDomIntTranTableViewer->PrintPlanToFile(pclDefPath);
			}
			break;

			}

		}
		else
			bmCommonDlg = false;

		delete polOfn;

	}

}

void CReportTableSpecialDlg::OnPapier() 
{
	;
}


void CReportTableSpecialDlg::OnBeenden() 
{

	if (!bmCommonDlg)
		CDialog::OnCancel();

}


void CReportTableSpecialDlg::OnCancel() 
{

	if (!bmCommonDlg)
		CDialog::OnCancel();

}
