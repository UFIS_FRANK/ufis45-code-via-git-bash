// DGanttChartReportWnd.cpp : implementation file
//

#include "stdafx.h"
#include "DGanttChartReportWnd.h"
#include "DGanttBarReport.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif



#define SPACE_TOP_BUTTON	50
#define SPACE_RIGHT_LEFT	15


/////////////////////////////////////////////////////////////////////////////
// DGanttChartReportWnd

DGanttChartReportWnd::DGanttChartReportWnd(CWnd* ppParent, CString opXAxe, CString opYAxe, 
										 CString opTotalNumber, CColor opColor)
{

	// Pointer des ScrollViews initialisieren
	pomDGanttChartReport = NULL;

	// Text der X-Achse an klassenweite Vraibale �bergeben
	omXAxe = opXAxe;
	// Text der Y-Achse an klassenweite Vraibale �bergeben
	omYAxe = opYAxe;
	// Text Gesamtanzahl aller ....	
	omTotalNumber = opTotalNumber;

	// Farbe der Bars
	omColor.r = opColor.r;
	omColor.g = opColor.g;
	omColor.b = opColor.b;

}


DGanttChartReportWnd::~DGanttChartReportWnd()
{

	//delete pomDGanttChartReport;

}


BEGIN_MESSAGE_MAP(DGanttChartReportWnd, CWnd)
	//{{AFX_MSG_MAP(DGanttChartReportWnd)
    ON_WM_CREATE()
	// NOTE - the ClassWizard will add and remove mapping macros here.
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// DGanttChartReportWnd message handlers

int DGanttChartReportWnd::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	
	if (CWnd::OnCreate(lpCreateStruct) == -1)
		return -1;
	
	CRect olRect;
	GetClientRect(&olRect);

	// Window etwas kleiner als Dialog
	olRect.bottom -= SPACE_TOP_BUTTON;
	olRect.top    += SPACE_TOP_BUTTON;
	olRect.left   += SPACE_RIGHT_LEFT;
	olRect.right  -= SPACE_RIGHT_LEFT;

	pomDGanttChartReport = new DGanttChartReport(this, olRect, omXAxe, omYAxe, omTotalNumber, omColor);

	pomDGanttChartReport->SetXAxeText(omXAxeText);
	pomDGanttChartReport->SetData1(omData1);
	pomDGanttChartReport->SetData2(omData2);
	pomDGanttChartReport->SetBarText(omBarText);
	pomDGanttChartReport->SetTotalNumnber(imTotalNumber);
	pomDGanttChartReport->SetLegende(omLeft, omRight, omMiddle);

	pomDGanttChartReport->Create(NULL, "Chart", WS_HSCROLL, olRect, this, 0);
	pomDGanttChartReport->ShowWindow(SW_SHOWNORMAL);
	pomDGanttChartReport->OnInitialUpdate();


    return 0;

}


void DGanttChartReportWnd::SetData1(const CUIntArray& opData)
{

	omData1.Copy(opData);

}


void DGanttChartReportWnd::SetData2(const CUIntArray& opData)
{

	omData2.Copy(opData);

}


void DGanttChartReportWnd::SetXAxeText(const CStringArray& opXAxeText)
{

	omXAxeText.Copy(opXAxeText);

}


void DGanttChartReportWnd::SetBarText(const CStringArray& opBarText)
{

	omBarText.Copy(opBarText);

}


void DGanttChartReportWnd::SetLegende(CString opLeft, CString opRight, CString opMiddle)
{

	omLeft   = opLeft;
	omRight  = opRight;
	omMiddle = opMiddle;

}


void DGanttChartReportWnd::Print(CDC* popDC, CCSPrint* popPrint, CString opHeader, CString opFooterLeft)
{

	pomDGanttChartReport->Print(popDC, popPrint, opHeader, opFooterLeft);

}


void DGanttChartReportWnd::SetTotalNumnber(int ipTotalNumber)
{

	imTotalNumber = ipTotalNumber;

}