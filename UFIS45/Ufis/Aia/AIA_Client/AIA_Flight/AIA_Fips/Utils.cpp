// Utils.cpp : Defines utility functions
//
// Modification History: 
// 22-nov-00	rkr		IsPostFlight() added
// 05-dec-00	rkr		ModifyWindowText() added
//

#include "stdafx.h"
#include "CCSGlobl.h"
#include "CedaBasicData.h"
#include "BasicData.h"
#include "fpms.h"
#include "process.h"
#include "resrc1.h"
#include "ButtonListDlg.h"
#include "BltDiagram.h"
#include "GatDiagram.h"
#include "PosDiagram.h"
#include "WroDiagram.h"

#include "Utils.h"

void ActivateTimers()
{
	if (pogButtonList)
	{
		pogButtonList->ActivateTimer();
	}

	if (pogBltDiagram)
	{
		pogBltDiagram->ActivateTimer();
	}

	if (pogGatDiagram)
	{
		pogGatDiagram->ActivateTimer();
	}

	if (pogPosDiagram)
	{
		pogPosDiagram->ActivateTimer();
	}

	if (pogWroDiagram)
	{
		pogWroDiagram->ActivateTimer();
	}
}

void DeActivateTimers()
{
	if (pogButtonList)
	{
		pogButtonList->DeActivateTimer();
	}

	if (pogBltDiagram)
	{
		pogBltDiagram->DeActivateTimer();
	}

	if (pogGatDiagram)
	{
		pogGatDiagram->DeActivateTimer();
	}

	if (pogPosDiagram)
	{
		pogPosDiagram->DeActivateTimer();
	}

	if (pogWroDiagram)
	{
		pogWroDiagram->DeActivateTimer();
	}
}

BOOL WriteDialogToReg(CWnd* opWnd, CString& opKey)
{
	if(opWnd && ::IsWindow(opWnd->m_hWnd) )
	{
		CRect olRect;
		opWnd->GetWindowRect(&olRect);

		CWinApp* pApp = AfxGetApp();

		if (pApp)
		{
			if (pApp->WriteProfileInt(opKey, "Left", olRect.left) == FALSE)
				return FALSE;
			if (pApp->WriteProfileInt(opKey, "Top", olRect.top) == FALSE)
				return FALSE;
			if (pApp->WriteProfileInt(opKey, "Width", olRect.Width()) == FALSE)
				return FALSE;
			if (pApp->WriteProfileInt(opKey, "Height",  olRect.Height()) == FALSE)
				return FALSE;

			return TRUE;
		}
		return FALSE;
	}

	return FALSE;
}

bool GetDialogFromReg(CRect& opRect, CString& opKey)
{
	CWinApp* pApp = AfxGetApp();
	if (pApp && !opKey.IsEmpty())
	{
		UINT left = pApp->GetProfileInt(opKey, "Left",  opRect.left);
		UINT top  = pApp->GetProfileInt(opKey, "Top",   opRect.top);
		UINT cx   = pApp->GetProfileInt(opKey, "Width", opRect.Width());
		UINT cy   = pApp->GetProfileInt(opKey, "Height",opRect.Height());

		opRect = CRect(left,top,left+cx,top+cy);
		return true;
	}

	return false;
}


void CallHistory(CString opField)
{

	if (opField.IsEmpty())// || bgLoacationChangesStarted)
		return;

	CString olWks = CString(ogCommHandler.pcmReqId);
	CString olListener = olWks + CString(pcgUser);

	CString olTable = opField.Left(3);
	CString olField = opField.Mid(3,opField.GetLength()-3);

//	strcpy(pclExcelPath, "C:\\Data_Changes.exe");
	char pclConfigPath[256];
	char pclExcelPath[256];

	if (getenv("CEDA") == NULL)
		strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
	else
		strcpy(pclConfigPath, getenv("CEDA"));

	GetPrivateProfileString(ogAppName, "DATA_CHANGES", "DEFAULT",
		pclExcelPath, sizeof pclExcelPath, pclConfigPath);


	if(!strcmp(pclExcelPath, "DEFAULT"))
	{
		CFPMSApp::MyTopmostMessageBox(NULL, GetString(IDS_DATACHANGES), GetString(ST_FEHLER), MB_ICONERROR);
		return;
	}

	CString olTimeMode = "U";
	if (bgGatPosLocal)
		olTimeMode = "L";

	char slRunTxt[512] = ""; 
	sprintf(slRunTxt,"%s;%s,%s,%s,%s,%s,%s,%s,%s", "0", "0", "AFT", "", "LOCATIONS", olTimeMode, olListener, "C", CString(pcgUser));

	char *args[4];
	args[0] = "child";
	args[1] = slRunTxt;
	args[2] = NULL;
	args[3] = NULL;
	int ilReturn = _spawnv(_P_NOWAIT,pclExcelPath,args);
	if (ilReturn != -1)
	{
		bgLoacationChangesStarted = true;
	}
}

int InsertSortInStrArray(CStringArray &ropStrArray, const CString &ropStr) 
{
	for (int i=0; i < ropStrArray.GetSize(); i++)
	{
		if (ropStr < ropStrArray[i])
		{
			break;
		}
	}
	ropStrArray.InsertAt(i, ropStr);
	return i;
}


bool FindInStrArray(const CStringArray &ropStrArray, const CString &ropStr) 
{
	for (int i=0; i < ropStrArray.GetSize(); i++)
	{
		if (ropStr == ropStrArray[i])
			return true;
	}
	return false;
}


int FindIdInStrArray(const CStringArray &ropStrArray, const CString &ropStr) 
{
	for (int i=0; i < ropStrArray.GetSize(); i++)
	{
		if (ropStr == ropStrArray[i])
			return i;
	}
	return -1;
}


bool IsRegularFlight(char Ftyp)
{
	return (Ftyp != 'T' && Ftyp != 'G');
}


bool IsArrivalFlight(const char *pcpOrg, const char *pcpDes, char Ftyp)
{
	return (IsArrival(pcpOrg, pcpDes) && IsRegularFlight(Ftyp));		
}


bool IsArrival(const char *pcpOrg, const char *pcpDes)
{
	return (strcmp(pcpDes, pcgHome) == 0);
}



bool IsDepartureFlight(const char *pcpOrg, const char *pcpDes, char Ftyp)
{
	return (IsDeparture(pcpOrg, pcpDes) && IsRegularFlight(Ftyp));		
}


bool IsDeparture(const char *pcpOrg, const char *pcpDes)
{
	return (strcmp(pcpOrg, pcgHome) == 0);
}
 

bool IsCircular(const char *pcpOrg, const char *pcpDes)
{
	return (IsArrival(pcpOrg, pcpDes) && IsDeparture(pcpOrg, pcpDes));
}


bool IsCircularFlight(const char *pcpOrg, const char *pcpDes, char Ftyp)
{
	return (IsCircular(pcpOrg, pcpDes) && IsRegularFlight(Ftyp));
}


bool DelayCodeToAlpha(char *pcpDcd)
{
	CString olDcdAlpha;
	if (!ogBCD.GetField("DEN", "DECN", CString(pcpDcd), "DECA", olDcdAlpha))
		return false;

	strncpy(pcpDcd, olDcdAlpha, 3);  
	return true;
}


bool DelayCodeToNum(char *pcpDcd)
{
	CString olDcdNum;
	if (!ogBCD.GetField("DEN", "DECA", CString(pcpDcd), "DECN", olDcdNum))
		return false;

	strncpy(pcpDcd, olDcdNum, 3);  
	return true;
}



int FindInListBox(const CListBox &ropListBox, const CString &ropStr)
{
	int ilCount = ropListBox.GetCount();
	CString olItem;
	for (int i = 0; i < ilCount; i++)
	{
		ropListBox.GetText(i, olItem);
		if (olItem == ropStr)
			return i;
	}
	return -1;
}


// A substitute for CListBox::GetSelItems(...)
int MyGetSelItems(CListBox &ropLB, int ipMaxAnz, int *prpItems)
{
	int ilAnz = 0;
	// Scan all Items
	for (int ilLBCount = 0; ilLBCount < ropLB.GetCount(); ilLBCount++)
	{
		if (ilAnz >= ipMaxAnz) break;

		if (ropLB.GetSel(ilLBCount) > 0)
		{
			// store index in array
			prpItems[ilAnz] = ilLBCount;
			ilAnz++;
		}
	}
	// return the count of selected items
	return ilAnz;
}


COLORREF GetColorOfFlight(char FTyp)
{
	switch (FTyp)
	{
	case 'S': return COLOR_FLIGHT_SCHEDULED;
	case ' ':
	case '\0': return COLOR_FLIGHT_PROGNOSE;
	case 'D': return COLOR_FLIGHT_DIVERTED;
	case 'R': return COLOR_FLIGHT_REROUTED;
	case 'B': return COLOR_FLIGHT_RETTAXI;
	case 'Z': return COLOR_FLIGHT_RETFLIGHT;
	case 'X': return COLOR_FLIGHT_CXX;
	case 'N': return COLOR_FLIGHT_NOOP;
	}

	return COLOR_FLIGHT_OPERATION;
 
}


long MyGetUrnoFromSelection(const CString &ropSel)
{
	int ilFirst = ropSel.Find("URNO");
	if (ilFirst == -1) return -1;
	ilFirst += 4;

	CString olNums("0123456789");

	while (ilFirst < ropSel.GetLength())
	{
		if (olNums.Find(ropSel[ilFirst]) != -1)
			break;
		ilFirst++;
	}
	if (ilFirst >= ropSel.GetLength())
		return -1;

	int ilLast = ilFirst;
	while (ilLast < ropSel.GetLength())
	{
		if (olNums.Find(ropSel[ilLast]) == -1)
			break;
		ilLast++;
	}

	long llUrno = atol(ropSel.Mid(ilFirst,ilLast-ilFirst));
	if (llUrno == 0)
		return -1;
	else
		return llUrno;
}



bool GetPosDefAllocDur(const CString &ropPos, int ipMinGT, CTimeSpan &ropDura)
{
	CString olDura;

	if (!ogBCD.GetField("PST", "PNAM", ropPos, "DEFD", olDura))
		olDura = "0";
	
	// get the maximum of minimal ground time and default allocation duration
	int ilDura = max(atoi(olDura), ipMinGT);

	if (ilDura > 0)
	{
		ropDura = CTimeSpan(0, 0, ilDura, 0);
	}
	else
	{
		// if no minimal ground time and no default allocation duration exists
		// get the hard coded default allocation 
		ropDura = ogPosDefAllocDur;
	}

	return true;
}


bool GetGatDefAllocDur(const CString &ropGate, CTimeSpan &ropDura)
{
	CString olDura;

	if (!ogBCD.GetField("GAT", "GNAM", ropGate, "DEFD", olDura))
		olDura = "0";

	if (atoi(olDura) <= 0)
	{
		ropDura = ogGatDefAllocDur;
	}
	else
	{
		ropDura = CTimeSpan(0, 0, atoi(olDura), 0);
	}
	return true;
}


bool GetBltDefAllocDur(const CString &ropBlt, CTimeSpan &ropDura)
{
	CString olDura;

	if (!ogBCD.GetField("BLT", "BNAM", ropBlt, "DEFD", olDura))
		olDura = "0";

	if (atoi(olDura) <= 0)
	{
		ropDura = ogBltDefAllocDur;
	}
	else
	{
		ropDura = CTimeSpan(0, 0, atoi(olDura), 0);
	}
	return true;
}



bool GetWroDefAllocDur(const CString &ropWro, CTimeSpan &ropDura)
{
	CString olDura;

	if (!ogBCD.GetField("WRO", "WNAM", ropWro, "DEFD", olDura))
		olDura = "0";

	if (atoi(olDura) <= 0)
	{
		ropDura = ogWroDefAllocDur;
	}
	else
	{
		ropDura = CTimeSpan(0, 0, atoi(olDura), 0);
	}
	return true;
}



bool GetFltnInDBFormat(const CString &ropUserFltn, CString &ropDBFltn)
{
	int ilFltn = atoi(ropUserFltn);
	ropDBFltn.Format("%03d", ilFltn);

	return true;
}



bool GetCurrentUtcTime(CTime &ropUtcTime)
{
	ropUtcTime = CTime::GetCurrentTime();
	ogBasicData.LocalToUtc(ropUtcTime);
	return true;
}


// true/false; outside/inside the timelimit
bool IsPostFlight(const CTime& opTimeFromFlight)
{
	if (opTimeFromFlight == TIMENULL)
		return false;

	// there's no timespan set (don't except negative values)
	CTimeSpan olSpanPast (0,0,0,0);
	CTimeSpan olSpanFuture (0,0,0,0);

	if (ogTimeSpanPostFlight.GetTotalSeconds() > 0)
		olSpanPast = ogTimeSpanPostFlight;

	if (ogTimeSpanPostFlightInFuture.GetTotalSeconds() > 0)
		olSpanFuture = ogTimeSpanPostFlightInFuture;

//	CString curTime = CTime::GetCurrentTime().Format("%Y%m%d%H%M%S");
//	CString flightTime = opTimeFromFlight.Format("%Y%m%d%H%M%S");

	CTime olUtcTime;
	GetCurrentUtcTime(olUtcTime);

	CTime olBegin = olUtcTime - olSpanPast;
	CTime olEnd   = olUtcTime + olSpanFuture;

	if (olSpanPast.GetTotalSeconds() > 0)
	{
		if (opTimeFromFlight < olBegin)
			return true;
	}

	if (olSpanFuture.GetTotalSeconds() > 0)
	{
		if (opTimeFromFlight > olEnd)
			return true;
	}
	
	return false;
}

BOOL ModifyWindowText(CWnd *opWnd, CString& opStrWnd, BOOL bplClean)
{
	if (opWnd)
	{
		CString olStrWndOld;
		opWnd->GetWindowText(olStrWndOld);

		if (olStrWndOld.Find(opStrWnd) < 0)
		{
			olStrWndOld += 	opStrWnd;
			opWnd->SetWindowText(olStrWndOld);
		}

		if (bplClean)
		{
			int i = olStrWndOld.Find(opStrWnd);
			if (i >= 0)
			{
				olStrWndOld.TrimRight(opStrWnd);
				opWnd->SetWindowText(olStrWndOld);
			}
		}

		return TRUE;
	}
	else
		return FALSE;

}


bool CallUIFforBLK(CString& opTable, const long lpUrno)
{
	char pclConfigPath[256];
	char pclExcelPath[256];

	if (getenv("CEDA") == NULL)
		strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
	else
		strcpy(pclConfigPath, getenv("CEDA"));

	GetPrivateProfileString(ogAppName, "BDPS-UIF", "DEFAULT",
		pclExcelPath, sizeof pclExcelPath, pclConfigPath);

	if(!strcmp(pclExcelPath, "DEFAULT"))
		CFPMSApp::MyTopmostMessageBox(NULL, GetString(IDS_STRING981), GetString(ST_FEHLER), MB_ICONERROR);

//	char buffer[32];
//	ltoa(prlLine->Urno, buffer, 10);
//	char s = '"';

	CString	olTables;
	olTables.Format("Resource:%s:%d",opTable,lpUrno);
	//olTables.Format("%cResource:PST:%d%c",s,prlLine->Urno,s);

	char *args[4];
	char slRunTxt[256];
	args[0] = "child";
//	sprintf(slRunTxt,"%c%s%c %c%s%c %c%s%c %c%s%c",s,ogAppName,s,s,pcgUser,s,s,pcgPasswd,s,s,olTables,s);
	sprintf(slRunTxt,"%s,%s,%s,%s",ogAppName,pcgUser,pcgPasswd,olTables);
	args[1] = slRunTxt;
	args[2] = NULL;
	args[3] = NULL;
	int ilReturn = _spawnv(_P_NOWAIT,pclExcelPath,args);

	return true;
}



bool DBStrIsEmpty(const char *prpStr)
{
	if (prpStr == NULL)
		return false;

	if (prpStr[0] == '\0')
		return true;
	if (prpStr[0] == ' ' && prpStr[1] == '\0')
		return true;

	return false;
}

//TODO use the cviewer or paramters for the possible field checks and the data (at this point only tifa,tifd, ftyp can be used)
//TODO
bool NewPreSelectionCheck(CString &opData, CString &opFields, CString opFtyps, CTime opFrom, CTime opTo)
{
	// return false means DB-Read has no Sinn
	// return true means DB-Read has Sinn

	CTime olTifa = TIMENULL;
	CTime olTifd = TIMENULL;
	CTime olTifaOld = TIMENULL;
	CTime olTifdOld = TIMENULL;
	CString olFtyp ("");
	CTime olTmp;
	CString olTemp;
	CString olAdid;	

	bool blTif = false;
	bool blReload = false;

	if (egStructBcfields == EXT_CHANGED_OLDVAL)
	{
		// if no field is set as changed -> no check possible -> don't reload
//		if (	!GetListItemByField(opData, opFields, CString("#TIFA"), olTmp)
//			&&	!GetListItemByField(opData, opFields, CString("#TIFD"), olTmp) )
//			&&	!GetListItemByField(opData, opFields, CString("#FTYP"), olTemp) )
//			return false;



		CString olFtyp("");
		GetListItemByField(opData, opFields, CString("ADID"), olAdid);

		if(GetListItemByField(opData, opFields, CString("#TIFA"), olTifaOld))
		{
			if (olAdid == "A" || olAdid == "B" || olAdid.IsEmpty())
			{
				GetListItemByField(opData, opFields, CString("TIFA"), olTifa);

				if(olTifa != TIMENULL)
				{
					if (olTifa < opTo && olTifa > opFrom)
						blReload = true;

					if(olTifaOld != TIMENULL)
					{
						if (olTifaOld < opTo && olTifaOld > opFrom) 
						{
							if (GetListItemByField(opData, opFields, CString("#FTYP"), olFtyp) || GetListItemByField(opData, opFields, CString("FTYP"), olFtyp))
							{
								if (GetListItemByField(opData, opFields, CString("FTYP"), olFtyp))
								{
									if(opFtyps.Find(olFtyp) >= 0 && blReload)
										blReload = true;
									else
										blReload = false;
								}
								else
									blReload = false;
							}
							else
								blReload = false;
						}
					}
				}
			}
		}
		else
		{
			if (olAdid == "A" || olAdid == "B" || olAdid.IsEmpty())
			{
				if (GetListItemByField(opData, opFields, CString("#FTYP"), olFtyp))
				{
					GetListItemByField(opData, opFields, CString("TIFA"), olTifa);

					if(olTifa != TIMENULL)
					{
						if (olTifa < opTo && olTifa > opFrom)
							blReload = true;

						if (GetListItemByField(opData, opFields, CString("FTYP"), olFtyp))
						{
							if(opFtyps.Find(olFtyp) >= 0 && blReload)
								blReload = true;
							else
								blReload = false;
						}
						else
							blReload = false;

					}
				}
				else
				{
					
					GetListItemByField(opData, opFields, CString("TIFA"), olTifa);

					if(olTifa != TIMENULL)
					{
						if (olTifa < opTo && olTifa > opFrom)
							blReload = true;

						if (GetListItemByField(opData, opFields, CString("FTYP"), olFtyp))
						{
							if(opFtyps.FindOneOf(olFtyp) >= 0 && blReload)
								blReload = true;
							else
								blReload = false;
						}
						else
							blReload = false;

					}
					
				}
			}
		}

		if(GetListItemByField(opData, opFields, CString("#TIFD"), olTifdOld))
		{
			if (olAdid == "D" || olAdid == "B" || olAdid.IsEmpty())
			{
				GetListItemByField(opData, opFields, CString("TIFD"), olTifd);

				if(olTifd != TIMENULL)
				{
					if (olTifd < opTo && olTifd > opFrom)
						blReload = true;

					if(olTifdOld != TIMENULL)
					{
						if (olTifdOld < opTo && olTifdOld > opFrom) 
						{
							if (GetListItemByField(opData, opFields, CString("#FTYP"), olFtyp) || GetListItemByField(opData, opFields, CString("FTYP"), olFtyp))
							{
								if (GetListItemByField(opData, opFields, CString("FTYP"), olFtyp))
								{
									if(opFtyps.Find(olFtyp) >= 0  && blReload)
										blReload = true;
									else
										blReload = false;
								}
								else
									blReload = false;
							}
							else
								blReload = false;
						}
					}
				}
			}
		}
		else
		{
			if (olAdid == "D" || olAdid == "B" || olAdid.IsEmpty())
			{
				if (GetListItemByField(opData, opFields, CString("#FTYP"), olFtyp))
				{
					GetListItemByField(opData, opFields, CString("TIFD"), olTifd);

					if(olTifd != TIMENULL)
					{
						if (olTifd < opTo && olTifd > opFrom)
							blReload = true;

						if (GetListItemByField(opData, opFields, CString("FTYP"), olFtyp))
						{
							if(opFtyps.Find(olFtyp) >= 0 && blReload)
								blReload = true;
							else
								blReload = false;
						}
						else
							blReload = false;
					}
				}
				else
				{
					
					GetListItemByField(opData, opFields, CString("TIFD"), olTifd);

					if(olTifd != TIMENULL && !blReload)
					{
						if (olTifd < opTo && olTifd > opFrom)
							blReload = true;

						if (GetListItemByField(opData, opFields, CString("FTYP"), olFtyp))
						{
							if(opFtyps.FindOneOf(olFtyp) >= 0 && blReload)
								blReload = true;
							else
								blReload = false;
						}
						else
							blReload = false; 
					}
					
				}
			}
		}
/*
		
		if(GetListItemByField(opData, opFields, CString("#TIFD"), olTifdOld))
		{
			if (olAdid == "D" || olAdid == "B" || olAdid.IsEmpty())
			{
				GetListItemByField(opData, opFields, CString("TIFD"), olTifd);

				if(olTifd != TIMENULL)
				{
					if (olTifd < opTo && olTifd > opFrom)
						blReload = true;

					if(olTifdOld != TIMENULL)
					{
						if (olTifdOld < opTo && olTifdOld > opFrom)
							blReload = false;
					}
				}
			}
		}
*/

		return blReload;
	}
	else if (egStructBcfields == EXT_CHANGED_NEWVAL)
	{
//		CString olField = "TIFA,TIFD,FTPY,LSTU,[CF],TIFA";
//		CString olData  = "tifa,tifd,ftyp,lstu,,tifa";

		int ilPos = opFields.Find("[CF]",0);
		if (ilPos == -1)
			return false;

		CStringArray olArray;
		ExtractItemList(opFields, &olArray);

		CStringArray olArrayData;
		ExtractItemList(opData, &olArrayData);

		if (olArray.GetSize() != olArrayData.GetSize())
			return false;


		int ilIndex = -1; 
		for (int i = olArray.GetSize() - 1; i >= 0; i--)
		{
			if (olArray.GetAt(i) == "[CF]")
			{
				ilIndex = i;
				break;
			}
		}

		if (ilIndex != -1 && ilIndex < olArray.GetSize()-1)
		{
			CString olNewFields;
			CString olNewData;
			for (int i = ilIndex+1; i < olArray.GetSize(); i++)
			{
				if (i < olArray.GetSize()-1)
					olNewFields += olArray.GetAt(i) + CString(",");
				else
					olNewFields += olArray.GetAt(i);

				if (i < olArrayData.GetSize()-1)
					olNewData += olArrayData.GetAt(i) + CString(",");
				else
					olNewData += olArrayData.GetAt(i);
			}

			// if no field is set as changed -> no check possible -> don't reload
			if (	!GetListItemByField(olNewData, olNewFields, CString("TIFA"), olTmp)
				&&	!GetListItemByField(olNewData, olNewFields, CString("TIFD"), olTmp) 
				&&	!GetListItemByField(olNewData, olNewFields, CString("FTYP"), olTemp) )
				return false;


			GetListItemByField(opData, opFields, CString("ADID"), olAdid);

		//	if(GetListItemByField(opData, opFields, CString("#TIFA"), olTmp))
			{
				if(!GetListItemByField(opData, opFields, CString("TIFA"), olTifa))
					blTif = true;

				if(olTifa != TIMENULL)
				{
					if (olAdid == "A" || olAdid == "B" || olAdid.IsEmpty())
					{
						if (olTifa < opTo && olTifa > opFrom)
							blTif = true;
					}
				}
			}

		//	if(GetListItemByField(opData, opFields, CString("#TIFD"), olTmp))
			{
				if(!GetListItemByField(opData, opFields, CString("TIFD"), olTifd))
					blTif = true;

				if(olTifd != TIMENULL)
				{
					if (olAdid == "D" || olAdid == "B" || olAdid.IsEmpty())
					{
						if (olTifd < opTo && olTifd > opFrom)
							blTif = true;
					}
				}
			}


			if (blTif)	// data is the timeframe -> check the other values if reload
			{
				if(GetListItemByField(olNewData, olNewFields, CString("FTYP"), olTemp))
				{
					if (!blReload)
					{
						if(!GetListItemByField(opData, opFields, CString("FTYP"), olFtyp))
							blReload = true;

						if(opFtyps.Find(olFtyp) >= 0 && opFtyps.Find(olTemp) >= 0)
							blReload = false;
						else
						{
							if(opFtyps.Find(olFtyp) >= 0)
								blReload = true;
						}
					}
				}
			}

			// data is in the timeframe and no reload by the other values needed
			if (blTif && !blReload)
			{
				// only if the timeframe hasn't changed -> reload
				if (	GetListItemByField(olNewData, olNewFields, CString("TIFA"), olTmp)
					||	GetListItemByField(olNewData, olNewFields, CString("TIFD"), olTmp) )
					blReload = true;
			}

			return blReload;

		}
		else 
			return false;

	}

	return false;
}

/*
bool NewPreSelectionCheck(CString &opData, CString &opFields, CString opFtyps, CTime opFrom, CTime opTo)
{
	// return false means DB-Read has no Sinn
	// return true means DB-Read has Sinn

	CTime olTifa = TIMENULL;
	CTime olTifd = TIMENULL;
	CString olFtyp ("");
	CTime olTmp;
	CString olTemp;
	CString olAdid;	

	bool blTif = false;
	bool blReload = false;

	if (egStructBcfields == EXT_CHANGED_OLDVAL)
	{
		// if no field is set as changed -> no check possible -> don't reload
		if (	!GetListItemByField(opData, opFields, CString("#TIFA"), olTmp)
			&&	!GetListItemByField(opData, opFields, CString("#TIFD"), olTmp) 
			&&	!GetListItemByField(opData, opFields, CString("#FTYP"), olTemp) )
			return false;


		GetListItemByField(opData, opFields, CString("ADID"), olAdid);

	//	if(GetListItemByField(opData, opFields, CString("#TIFA"), olTmp))
		{
			if(!GetListItemByField(opData, opFields, CString("TIFA"), olTifa))
				blTif = true;

			if(olTifa != TIMENULL)
			{
				if (olAdid == "A" || olAdid == "B" || olAdid.IsEmpty())
				{
					if (olTifa < opTo && olTifa > opFrom)
						blTif = true;
				}
			}
		}

	//	if(GetListItemByField(opData, opFields, CString("#TIFD"), olTmp))
		{
			if(!GetListItemByField(opData, opFields, CString("TIFD"), olTifd))
				blTif = true;

			if(olTifd != TIMENULL)
			{
				if (olAdid == "D" || olAdid == "B" || olAdid.IsEmpty())
				{
					if (olTifd < opTo && olTifd > opFrom)
						blTif = true;
				}
			}
		}


		if (blTif)	// data is the timeframe -> check the other values if reload
		{
			if(GetListItemByField(opData, opFields, CString("#FTYP"), olTemp))
			{
				if (!blReload)
				{
					if(!GetListItemByField(opData, opFields, CString("FTYP"), olFtyp))
						blReload = true;

					if(opFtyps.Find(olFtyp) >= 0 && opFtyps.Find(olTemp) >= 0)
						blReload = false;
					else
					{
						if(opFtyps.Find(olFtyp) >= 0)
							blReload = true;
					}
				}
			}
		}

		// data is in the timeframe and no reload by the other values needed
		if (blTif && !blReload)
		{
			// only if the timeframe hasn't changed -> reload
			if (	GetListItemByField(opData, opFields, CString("#TIFA"), olTmp)
				||	GetListItemByField(opData, opFields, CString("#TIFD"), olTmp) )
				blReload = true;
		}

		return blReload;
	}
	else if (egStructBcfields == EXT_CHANGED_NEWVAL)
	{
//		CString olField = "TIFA,TIFD,FTPY,LSTU,[CF],TIFA";
//		CString olData  = "tifa,tifd,ftyp,lstu,,tifa";

		int ilPos = opFields.Find("[CF]",0);
		if (ilPos == -1)
			return false;

		CStringArray olArray;
		ExtractItemList(opFields, &olArray);

		CStringArray olArrayData;
		ExtractItemList(opData, &olArrayData);

		if (olArray.GetSize() != olArrayData.GetSize())
			return false;


		int ilIndex = -1; 
		for (int i = olArray.GetSize() - 1; i >= 0; i--)
		{
			if (olArray.GetAt(i) == "[CF]")
			{
				ilIndex = i;
				break;
			}
		}

		if (ilIndex != -1 && ilIndex < olArray.GetSize()-1)
		{
			CString olNewFields;
			CString olNewData;
			for (int i = ilIndex+1; i < olArray.GetSize(); i++)
			{
				if (i < olArray.GetSize()-1)
					olNewFields += olArray.GetAt(i) + CString(",");
				else
					olNewFields += olArray.GetAt(i);

				if (i < olArrayData.GetSize()-1)
					olNewData += olArrayData.GetAt(i) + CString(",");
				else
					olNewData += olArrayData.GetAt(i);
			}

			// if no field is set as changed -> no check possible -> don't reload
			if (	!GetListItemByField(olNewData, olNewFields, CString("TIFA"), olTmp)
				&&	!GetListItemByField(olNewData, olNewFields, CString("TIFD"), olTmp) 
				&&	!GetListItemByField(olNewData, olNewFields, CString("FTYP"), olTemp) )
				return false;


			GetListItemByField(opData, opFields, CString("ADID"), olAdid);

		//	if(GetListItemByField(opData, opFields, CString("#TIFA"), olTmp))
			{
				if(!GetListItemByField(opData, opFields, CString("TIFA"), olTifa))
					blTif = true;

				if(olTifa != TIMENULL)
				{
					if (olAdid == "A" || olAdid == "B" || olAdid.IsEmpty())
					{
						if (olTifa < opTo && olTifa > opFrom)
							blTif = true;
					}
				}
			}

		//	if(GetListItemByField(opData, opFields, CString("#TIFD"), olTmp))
			{
				if(!GetListItemByField(opData, opFields, CString("TIFD"), olTifd))
					blTif = true;

				if(olTifd != TIMENULL)
				{
					if (olAdid == "D" || olAdid == "B" || olAdid.IsEmpty())
					{
						if (olTifd < opTo && olTifd > opFrom)
							blTif = true;
					}
				}
			}


			if (blTif)	// data is the timeframe -> check the other values if reload
			{
				if(GetListItemByField(olNewData, olNewFields, CString("FTYP"), olTemp))
				{
					if (!blReload)
					{
						if(!GetListItemByField(opData, opFields, CString("FTYP"), olFtyp))
							blReload = true;

						if(opFtyps.Find(olFtyp) >= 0 && opFtyps.Find(olTemp) >= 0)
							blReload = false;
						else
						{
							if(opFtyps.Find(olFtyp) >= 0)
								blReload = true;
						}
					}
				}
			}

			// data is in the timeframe and no reload by the other values needed
			if (blTif && !blReload)
			{
				// only if the timeframe hasn't changed -> reload
				if (	GetListItemByField(olNewData, olNewFields, CString("TIFA"), olTmp)
					||	GetListItemByField(olNewData, olNewFields, CString("TIFD"), olTmp) )
					blReload = true;
			}

			return blReload;

		}
		else 
			return false;

	}

	return false;
}
*/