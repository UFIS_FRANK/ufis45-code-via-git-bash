#ifndef AFX_ROTATIONVIPDLG_H__6858C611_6588_11D1_80FD_0000B43C4B01__INCLUDED_
#define AFX_ROTATIONVIPDLG_H__6858C611_6588_11D1_80FD_0000B43C4B01__INCLUDED_

// RotationVipDlg.h : Header-Datei
//
// Modification History: 
// 22-nov-00	rkr		Status for dialog supported

#include "CCSTable.h" 
#include "CCSPtrArray.h" 
#include "CCSEdit.h" 
#include "resrc1.h" 
#include "RotationDlgCedaFlightData.h" 

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld RotationVipDlg 

class RotationVipDlg : public CDialog
{
public:
	//Dialogstatus
	enum Status 
	{
		UNKNOWN		,
		POSTFLIGHT
	};

// Konstruktion
public:
	RotationVipDlg(CWnd* pParent = NULL, ROTATIONDLGFLIGHTDATA* prpAFlight = NULL, 
		           ROTATIONDLGFLIGHTDATA* prpDFlight = NULL);
	~RotationVipDlg();

// Dialogfelddaten
	//{{AFX_DATA(RotationVipDlg)
	enum { IDD = IDD_ROTATION_VIP };
	CStatic	m_CS_GMBorder;
	//}}AFX_DATA


	void InitTable();
	void Header();
	void DrawHeader();

	void FillTableLine(ROTATIONDLGFLIGHTDATA* prpFlight);

	void InitDialog();
	void setStatus(Status epStatus = UNKNOWN);
	Status getStatus() const;

	CWnd* pomParent;
// Überschreibungen
	// Vom Klassen-Assistenten generierte virtuelle Funktionsüberschreibungen
	//{{AFX_VIRTUAL(RotationVipDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	//}}AFX_VIRTUAL

// Implementierung
protected:

	// Generierte Nachrichtenzuordnungsfunktionen
	//{{AFX_MSG(RotationVipDlg)
	virtual BOOL OnInitDialog();
	virtual void OnCancel();
    afx_msg LONG OnTableLButtonDown(UINT wParam, LONG lParam);
	afx_msg void OnUpdate();
	afx_msg void OnInsert();
	afx_msg void OnDelete();
	afx_msg void OnClose();
    afx_msg LONG OnTableLButtonDblclk(UINT wParam, LONG lParam);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

	ROTATIONDLGFLIGHTDATA* prmAFlight;
	ROTATIONDLGFLIGHTDATA* prmDFlight;
	CCSTable *pomTable;
	CStringArray omVIPUrnos;

private:
	Status emStatus;
	void InitialPostFlight(BOOL);
	void InitialStatus();
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio fügt zusätzliche Deklarationen unmittelbar vor der vorhergehenden Zeile ein.

#endif // AFX_RotationVipDlg_H__6858C611_6588_11D1_80FD_0000B43C4B01__INCLUDED_
