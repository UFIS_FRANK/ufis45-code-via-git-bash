#ifndef ADDITIONAL_REPORT_TABLE_DLG_H
#define ADDITIONAL_REPORT_TABLE_DLG_H

// ReportTableDlg.h : Header-Datei
//
#include "CCSTable.h"
#include "Table.h"
#include "RotationDlgCedaFlightData.h"

#include "GanttChartReportWnd.h"
#include "DGanttChartReportWnd.h"
#include "AdditionalReport1TableViewer.h"
#include "AdditionalReport2TableViewer.h"
#include "AdditionalReport3TableViewer.h"
#include "AdditionalReport4TableViewer.h"
#include "AdditionalReport5TableViewer.h"
#include "AdditionalReport6TableViewer.h"
#include "AdditionalReport7TableViewer.h"
#include "AdditionalReport8TableViewer.h"
#include "AdditionalReport9TableViewer.h"
#include "AdditionalReport10TableViewer.h"
#include "AdditionalReport11TableViewer.h"
#include "AdditionalReport12TableViewer.h"
#include "AdditionalReport16TableViewer.h"
#include "AdditionalReport17TableViewer.h"

#include "DlgResizeHelper.h"

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld CAdditionalReportTableDlg 

class CAdditionalReportTableDlg : public CDialog
{
// Konstruktion
public:
	CAdditionalReportTableDlg(CWnd* pParent = NULL, int igTable = 0, CCSPtrArray<ROTATIONDLGFLIGHTDATA> *popData = NULL,
					char *pcpInfo = NULL, char *pcpSelect = NULL, char *pcpInfo2 = NULL,
					CTime opMin = TIMENULL, CTime opMax = TIMENULL);
	~CAdditionalReportTableDlg();

// Dialogfelddaten
	//{{AFX_DATA(CAdditionalReportTableDlg)
	enum { IDD = IDD_REPORTTABLE };
	CButton	m_CB_Beenden;
	CButton	m_CB_Papier;
	CButton	m_CB_Drucken;
	CButton	m_CB_Datei;
	//}}AFX_DATA
	CString omHeadline;	//Dialogbox-Überschrift


// Überschreibungen
	// Vom Klassen-Assistenten generierte virtuelle Funktionsüberschreibungen
	//{{AFX_VIRTUAL(CAdditionalReportTableDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	//}}AFX_VIRTUAL

public:

	int imDialogBarHeight;
	void SaveToReg();

// Implementierung
protected:

	// Generierte Nachrichtenzuordnungsfunktionen
	//{{AFX_MSG(CAdditionalReportTableDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnDatei();
	afx_msg void OnDrucken();
	afx_msg void OnPapier();
	afx_msg void OnBeenden();
	virtual void OnCancel();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnDestroy();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

private:
	CCSTable *pomReportTable;
	CTable *pomReportCTable;
	int miTable;
	char *pcmInfo;
	char *pcmInfo2;
	char *pcmSelect;
	bool bmCommonDlg;
	CTime omMax;
	CTime omMin;

	CCSPtrArray<ROTATIONDLGFLIGHTDATA> *pomData;
	
	AdditionalReport1TableViewer*  pomAdditionalReport1TableViewer;
	AdditionalReport2TableViewer*  pomAdditionalReport2TableViewer;
	AdditionalReport3TableViewer*  pomAdditionalReport3TableViewer;
	AdditionalReport4TableViewer*  pomAdditionalReport4TableViewer;
	AdditionalReport5TableViewer*  pomAdditionalReport5TableViewer;
	AdditionalReport6TableViewer*  pomAdditionalReport6TableViewer;
	AdditionalReport7TableViewer*  pomAdditionalReport7TableViewer;
	AdditionalReport8TableViewer*  pomAdditionalReport8TableViewer;
	AdditionalReport9TableViewer*  pomAdditionalReport9TableViewer;
	AdditionalReport10TableViewer* pomAdditionalReport10TableViewer;
	AdditionalReport11TableViewer* pomAdditionalReport11TableViewer;
	AdditionalReport12TableViewer* pomAdditionalReport12TableViewer;
	AdditionalReport16TableViewer* pomAdditionalReport16TableViewer;
	AdditionalReport17TableViewer* pomAdditionalReport17TableViewer;

	GanttChartReportWnd* pomGanttChartReportWnd;
	DGanttChartReportWnd* pomDGanttChartReportWnd;

	DlgResizeHelper m_resizeHelper;
	CString m_key;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio fügt zusätzliche Deklarationen unmittelbar vor der vorhergehenden Zeile ein.

#endif
