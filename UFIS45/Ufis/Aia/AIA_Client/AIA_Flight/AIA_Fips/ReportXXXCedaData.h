#ifndef _REPORT_XXX_CEDA_DATA_H_
#define _REPORT_XXX_CEDA_DATA_H_
 

#include "CCSGlobl.h"
#include "CCSDefines.h"




struct REPORTXXXDATA
{
	char	Select[8];	// Referenz AFT.xxx <--> xxx
	char	Name[256];	// Klartext des Codes

	//DataCreated by this class
	int		IsChanged;

	REPORTXXXDATA(void)
	{ 
		memset(this,'\0',sizeof(*this));
	}

};
	


class ReportXXXCedaData : public CCSCedaData
{
// Attributes
public:
	char pcmAftFieldList[64];
	CStringArray omSameFlnu;


// Operations
public:
    ReportXXXCedaData(char* pcpSelect, char* pcpName, char* pcpTable, bool bpAll = false);
	~ReportXXXCedaData();
	

	CString ReadCleartext(CString opTtyp);
	void SetExtendedWhereString(CString opWhere);

private:
	bool bmReadAllFlights;
	bool bmRotation;
	char* opName;
	CString omExtendedWhere;
	bool bmAll;

};


#endif
