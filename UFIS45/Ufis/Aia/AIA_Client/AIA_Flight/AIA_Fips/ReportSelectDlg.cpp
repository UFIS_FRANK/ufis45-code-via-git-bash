// ReportSelectDlg.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "resrc1.h"
#include "fpms.h"
#include "ReportFromDlg.h"
#include "ReportFieldDlg.h"
#include "ReportFromFieldDlg.h"
#include "ReportTableSpecialDlg.h"
#include "DailyPrintDlg.h"
#include "DailyCcaPrintDlg.h"
#include "ReportSelectDlg.h"
#include "ReportSeqDlg.h"
#include "ReportSeq1.h"
#include "ReportSeqFieldDlg.h"
#include "ReportSeqChoiceFieldDlg.h"
#include "ReportSeqField3DateTimeDlg.h"
#include "ReportSeqDateTimeDlg.h"
#include "ReportSortDlg.h"
#include "ReportArrDepSelctDlg.h" 
#include "ReportTimeFrameSelctDlg.h"
#include "ReportTableDlg.h"
#include "AdditionalReportTableDlg.h"
#include "RotationDlgCedaFlightData.h"
#include "SeasonCedaFlightData.h"
#include "CedaBasicData.h"
#include "Afxdisp.h"
#include "PrivList.h"
#include "ReportMovements.h"
#include "OvernightTableDlg.h"
#include "Utils.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif



// lokale Variablen , Definitionen und Funktionen
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#define _MIT_TRACE_		FALSE

// f�r Report Wochenplan
SeasonCedaFlightData omCedaFlightData;

// f�r Report der POPS Daily/Saisonliste + der CCAListe
RotationDlgCedaFlightData omDailyCedaFlightData( false );	// false => inkl. Archivtabelle 
															// sonstige Reports s. OnOK() olData


// Konstanten f�r WHERE CLAUSES
///////////////////////////////////////////////////////
const bool blAllFtyps = true ;		// Reports mit allen Flugtypen ( au�er f�r HAJ )

const CString comFtypAll("") ;								// Ftypen : alle
const CString comFtypO("AND FTYP='O'") ;					// Ftypen : operation
const CString comFtypOS("AND (FTYP='O' OR FTYP='S')") ;	// Ftypen : operation + schedule
const CString comFtyp_SGT("AND (FTYP<>'S' AND FTYP<>'G' AND FTYP<>'T')");
  
								// Positionen zur Auswertung der Selektion( WHERE Bedingung POPS Daily !)
const int imWherePos1 = 9 ;		// Endtag
const int imWherePos2 = 36 ;	// Starttag
const int imWherePos3 = 114 ;	// Verkehrstag bei Saison

const int imProgRange = 15 ;	// Max-Range Progressanzeige



static int CompareToAlc2(const ROTATIONDLGFLIGHTDATA **e1, const ROTATIONDLGFLIGHTDATA **e2);
static int CompareToDes3(const ROTATIONDLGFLIGHTDATA **e1, const ROTATIONDLGFLIGHTDATA **e2);
static int CompareToTtypAlc(const ROTATIONDLGFLIGHTDATA **e1, const ROTATIONDLGFLIGHTDATA **e2);
static int CompareToDes3Alc(const ROTATIONDLGFLIGHTDATA **e1, const ROTATIONDLGFLIGHTDATA **e2);
static int CompareToAlcDes3(const ROTATIONDLGFLIGHTDATA **e1, const ROTATIONDLGFLIGHTDATA **e2);
static int CompareToTtyp(const ROTATIONDLGFLIGHTDATA **e1, const ROTATIONDLGFLIGHTDATA **e2);
static int CompareToAlc3(const ROTATIONDLGFLIGHTDATA **e1, const ROTATIONDLGFLIGHTDATA **e2);
static int CompareToAlc2Des3(const ROTATIONDLGFLIGHTDATA **e1, const ROTATIONDLGFLIGHTDATA **e2);
static int CompareRotation(const ROTATIONDLGFLIGHTDATA **e1, const ROTATIONDLGFLIGHTDATA **e2);
static int CompareSkey(const ROTATIONDLGFLIGHTDATA **e1, const ROTATIONDLGFLIGHTDATA **e2);
static int CompareToAdid(const ROTATIONDLGFLIGHTDATA **e1, const ROTATIONDLGFLIGHTDATA **e2);

static int CompareChrono(const ROTATIONDLGFLIGHTDATA **e1, const ROTATIONDLGFLIGHTDATA **e2);


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////


// Struct PARTIALREAD
// Schrittweise auf DB zugreifen ( bei Listungen mit Intervallen ) 
PARTIALREAD::PARTIALREAD()
{
	WhereClause.Empty() ;
	StartTime = TIMENULL ;
	EndTime = TIMENULL ;
	Range = RRDefault ;
	Case = -1 ;
	omData = NULL ;
	Fields.Empty() ;
	onlyAFT = true ;
	Sort = false ;
	SortArt = 0 ;
}

// Vorbelegung f�r Hauptanwendung ( alle, au�er POPS case 14+15 ! )
void PARTIALREAD::Preset()
{
	// alle Intervallzugriffe f�r "NORMALE" Reports werden unter 0 zusammengefa�t    
	Case = 0 ;

	// hbe 5/99
	// WhereClause = "WHERE (STOA BETWEEN '%s' AND '%s' OR STOD BETWEEN '%s' AND '%s')";
	WhereClause.Format("WHERE ((STOA BETWEEN '%%s' AND '%%s' AND DES3='%s' ) OR ( STOD BETWEEN '%%s' AND '%%s' AND ORG3='%s'))",
				pcgHome, pcgHome );
	Range = RRWeek ;
	onlyAFT = false ;
	Sort = true ;
	SortArt = RotationDlgCedaFlightData::DefSort ;

	// m�ssen immer versorgt werden !
	// StartTime = DBStringToDateTime(oMinStr) ;
	// EndTime  = DBStringToDateTime(oMaxStr) ;
	// omData = &olData.omData ;
	// Fields = "TIFA,....." ;
}



/////////////////////////////////////////////////////////////////////////////
// Dialogfeld CReportSelectDlg Preset 

// Konstruktor
CLastDlgValues::CLastDlgValues()
{
	Season=false; Term=X_TERMDEF; 
	AirlineEx="US UA";	Verkehr="AEK" ; 
	MinTime = CTime::GetCurrentTime() ; MaxTime = CTime::GetCurrentTime() ;
	Minuten = "180" ; Radio = 0 ;
	Airlines.Empty(); Select.Empty(); Saison.Empty(); Messe.Empty(); Flno.Empty(); 
} ;

// Destruktor ( entspr. Reset )
CLastDlgValues::~CLastDlgValues()
{
	MinDate.Empty() ;
	MaxDate.Empty() ;
	MinTime = CTime::GetCurrentTime() ;
	MaxTime = CTime::GetCurrentTime() ;
	Season = false ;
	Term = X_TERMDEF ;
	Airlines.Empty() ;
	AirlineEx = "US UA" ;
	Verkehr = "AEK" ;
	Minuten = "180" ; Radio = 0 ;
	Airlines.Empty(); Select.Empty(); Saison.Empty(); Messe.Empty(); Flno.Empty(); 
}


void CLastDlgValues::SetPreset(int ipTable, CTime & pStart, CTime & pEnd, CString & pSelect)
{
	switch( ipTable )
	{
	case -1:
		// Reset Presetstruct
		// ~CLastDlgValues() ;
		break;
	case 12:
	{
		// Wochenplan
		int ilHeute = CTime( CTime::GetCurrentTime() ).GetDayOfWeek() ;
		CTimeSpan olJump(6,0,0,0) ;	// Default f�r Sonntag
		if( ilHeute != 1 )
		{
			olJump = CTimeSpan( ilHeute - 2, 0,0,0 ) ;
		}
		pStart = CTime::GetCurrentTime() - olJump ;
		pEnd = CTime() ;
		pSelect = GetSelect() ;
		// nur 2Ltr Code
		// if( pSelect.GetLength() > 2 ) pSelect.Empty() ;
	}

	break ;
	
	default:
		break ;


	}

}



/////////////////////////////////////////////////////////////////////////////
// Dialogfeld CReportSelectDlg 


CReportSelectDlg::CReportSelectDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CReportSelectDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CReportSelectDlg)
	m_List1 = -1;
	//}}AFX_DATA_INIT
	pomReportTableDlg = NULL;
	// Default Where-Clauses f�r Reports
	omDefaultWhereSto.Format(
		" ((STOA BETWEEN '%%s' AND '%%s' AND DES3='%s' ) OR ( STOD BETWEEN '%%s' AND '%%s' AND ORG3='%s'))",
				pcgHome, pcgHome );
 	omDefaultWhereTif.Format( 
		"WHERE ((TIFA BETWEEN '%%s' AND '%%s' AND DES3='%s' ) OR ( TIFD BETWEEN '%%s' AND '%%s' AND ORG3='%s'))",
				pcgHome, pcgHome );
	CDialog::Create(CReportSelectDlg::IDD, pParent);

	m_key = "DialogPosition\\ReportsSelection";

}

CReportSelectDlg::~CReportSelectDlg()
{
	if (pomReportTableDlg != NULL)
	{
		pomReportTableDlg->DestroyWindow();
		delete pomReportTableDlg;
	}
}

void CReportSelectDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CReportSelectDlg)
	DDX_Control(pDX, IDC_UTCLOCAL, m_CE_UtcToLocal);
	DDX_Control(pDX, IDC_ST_PROG, m_ST_Progress);
	DDX_Control(pDX, IDC_PR_READ, m_PR_Read);
	DDX_Control(pDX, IDC_LIST1, m_CR_List1);
	DDX_Control(pDX, IDC_LIST2, m_CR_List2);
	DDX_Control(pDX, IDC_LIST3, m_CR_List3);
	DDX_Control(pDX, IDC_LIST4, m_CR_List4);
	DDX_Control(pDX, IDC_LIST5, m_CR_List5);
	DDX_Control(pDX, IDC_LIST6, m_CR_List6);
	DDX_Control(pDX, IDC_LIST7, m_CR_List7);
	DDX_Control(pDX, IDC_LIST8, m_CR_List8);
	DDX_Control(pDX, IDC_LIST9, m_CR_List9);
	DDX_Control(pDX, IDC_LIST10, m_CR_List10);
	DDX_Control(pDX, IDC_LIST11, m_CR_List11);
	DDX_Control(pDX, IDC_LIST12, m_CR_List12);
	DDX_Control(pDX, IDC_LIST13, m_CR_List13);
	DDX_Control(pDX, IDC_LIST14, m_CR_List14);
	DDX_Control(pDX, IDC_LIST15, m_CR_List15);
	DDX_Control(pDX, IDC_LIST16, m_CR_List16);
	DDX_Control(pDX, IDC_LIST18, m_CR_List18);
	DDX_Control(pDX, IDC_LIST19, m_CR_List19);
	DDX_Control(pDX, IDC_LIST20, m_CR_List20);
	DDX_Control(pDX, IDC_LIST21, m_CR_List21);
	DDX_Control(pDX, IDC_LIST22, m_CR_List22);
	DDX_Control(pDX, IDC_LIST23, m_CR_List23);
	DDX_Control(pDX, IDC_LIST24, m_CR_List24);
	DDX_Control(pDX, IDC_LIST25, m_CR_List25);
	DDX_Control(pDX, IDC_LIST26, m_CR_List26);
	DDX_Control(pDX, IDC_LIST27, m_CR_List27);
	DDX_Radio(pDX, IDC_LIST20, m_List1);
	DDX_Control(pDX, IDC_LOATAB, m_CE_Loatab);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CReportSelectDlg, CDialog)
	//{{AFX_MSG_MAP(CReportSelectDlg)
	ON_WM_SIZE()
 	ON_WM_DESTROY()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// Behandlungsroutinen f�r Nachrichten CReportSelectDlg 


// wenn gew�nscht Zeiten in Utc wandeln
void CReportSelectDlg::MakeUtc( CString& ropMinStr, CString& ropMaxStr )
{
	if(bgReportLocal)
	{
		CTime olUtcMinDate = DBStringToDateTime(ropMinStr);
		CTime olUtcMaxDate = DBStringToDateTime(ropMaxStr);

		ogBasicData.LocalToUtc(olUtcMinDate);
		ogBasicData.LocalToUtc(olUtcMaxDate);

		ropMinStr = olUtcMinDate.Format( "%Y%m%d%H%M%S" );
		ropMaxStr = olUtcMaxDate.Format( "%Y%m%d%H%M%S" );
	}
}

// Progressbar anzeigen
void CReportSelectDlg::ShowStatus() 
{
	m_PR_Read.ShowWindow( SW_SHOW ) ;	
	m_ST_Progress.ShowWindow( SW_SHOW ) ;
	m_ST_Progress.SetWindowText( GetString( IDS_STRING1205) ) ;
	m_ST_Progress.UpdateWindow() ;
	m_PR_Read.SetPos( 1 ) ;	
}

// Progressbar weg
void CReportSelectDlg::HideStatus() 
{
	m_ST_Progress.SetWindowText( "" ) ;
	m_ST_Progress.UpdateWindow() ;
	m_PR_Read.ShowWindow( SW_HIDE ) ;	
	m_ST_Progress.ShowWindow( SW_HIDE ) ;
}



void CReportSelectDlg::OnOK() 
{
	UpdateData(TRUE);

	StartTableDlg();

	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));

} // end OnOK()





bool CReportSelectDlg::StartTableDlg()
{

	bgReportLocal = m_CE_UtcToLocal.GetCheck() ? true : false ;
	bool blUseLoatab = m_CE_Loatab.GetCheck() ? true : false ;

	RotationDlgCedaFlightData olData;	// POPS: see omDailyCedaFlightData 
	RotationDlgCedaFlightData olData2;	// POPS: see omDailyCedaFlightData 

	char pclErrbuf[256];
	bool blOk = false;
	CTime olUtcMinDate;
	CTime olUtcMaxDate;

	// 03/2/99: Abfrage FTYP='x' in Abh�ngigkeit vom Kunden defnieren
	//			HAJ		 = S oder O
	//			sonstige = alle Ftypen
	//				au�er f�r Abfl�ge BGS, Arbeitsflugplan, Inverntar ( nur O ) 
	//				Messe ( <> G AND <> T ),
	//				Daily/Season + CCA ( nur f�r POPS )
	
	CString olFtyp( comFtypO ) ;	// default Operation
	if((ogCustomer == "HAJ") && (ogAppName == "FPMS"))
	{
		olFtyp = comFtypOS ;	// Operation oder Schedule
	}
	else if( blAllFtyps )
	{
		olFtyp = comFtypAll ;	// alle Ftypen
	}
	// Reports in SHA nur mit operativen Fl�gen
	if((ogCustomer == "SHA") && (ogAppName == "FIPS"))
	{
		// nur Operation und Schedule Fl�ge
		olFtyp = comFtypOS;	
	}

	// abh�ngig von der gew�hlten Liste wird ein Auswahldialog
	// zur weiteren Einschr�nkung des Suchergebnisses aufgezogen.
	TRACE("CReportSelectDlg::StartTableDlg: Liste Nr.: %d\n", m_List1);
	if (m_CR_List1.GetCheck() == 1)
	{
		TRACE("CReportSelectDlg::StartTableDlg: List1\n");
		CTime olMinDate = omLastValues.GetMinTime() ;
		CTime olMaxDate = omLastValues.GetMaxTime() ;
		char pclSelect[128];
		
		if (ogCustomer != "SHA" && ogAppName != "FIPS")
			strcpy(pclSelect, omLastValues.GetSelect() );
		else
			pclSelect[0] = 0;

		CString olStr1, olStr2;
		if (ogCustomer == "SHA" && ogAppName == "FIPS")
		{
			olStr1 = GetString(IDS_STRING1562);
			olStr2 = GetString(IDS_STRING1046);
		}
		else
		{
			olStr1 = GetString(IDS_STRING1045);
			olStr2 = GetString(IDS_STRING1046);
		}

		CReportSeqFieldDlg olReportDlg(this, olStr1, olStr2, &olMinDate, &olMaxDate, pclSelect, "A|#A|#A|#", 2, 3);

		if(olReportDlg.DoModal() == IDOK)
		{
			
			ShowStatus();
			m_PR_Read.StepIt() ;

			// R�ckgabewerte festhalten
			omLastValues.SetMinTime( olMinDate ) ;
			omLastValues.SetMaxTime( olMaxDate ) ;
			omLastValues.SetSelect( pclSelect ) ;

			blOk = true;

			CString oMinStr = olMinDate.Format( "%Y%m%d000000" );
			CString oMaxStr = olMaxDate.Format( "%Y%m%d235959" );
			MakeUtc( oMinStr, oMaxStr ) ;

			CString olAlc("") ;
			if(strlen(pclSelect) == 2)
			{
				olAlc.Format(" AND ALC2='%s'", pclSelect ) ;
			}
			else if(strlen(pclSelect) == 3)
			{
				olAlc.Format(" AND ALC3='%s'", pclSelect ) ;
			}
			// f�r SHA keine explizite Angabe einer ALC notwendig
			else if ((ogCustomer == "SHA") && (ogAppName == "FIPS") && (strlen(pclSelect) == 0))
			{
			
			}
			else
			{
				// Airlines nur 2Ltr oder 3Ltr-Code !
				MessageBox("2Ltr/3Ltr-Code only!", GetString(IDS_WARNING), MB_ICONWARNING );
 				return false;
			}

			// Portionsweise einlesen
			// 2/99 hbe
			PARTIALREAD olPartial ;
			olPartial.Preset() ;
			CString olWherePlus ;
			olWherePlus.Format("%s %s", olAlc, olFtyp );
			olPartial.WhereClause += olWherePlus;
			olPartial.StartTime = DBStringToDateTime(oMinStr) ;
			olPartial.EndTime  = DBStringToDateTime(oMaxStr) ;
			olPartial.omData = &olData.omData ;
			olPartial.Fields = "RKEY,SKEY,SEAS,FLNO,TTYP,FTYP,ORG3,VIA3,ACT3,DES3,VIA3,ALC2,ALC3,STOA,STOD,ONBL,OFBL,VIAL,AIRB,LAND";
			
			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));

			bool blReadRet = ReadPartialWhere( &olPartial, &m_PR_Read ) ;
			if( ! blReadRet )
			{
				//sprintf(pclErrbuf,"\nKein Suchergebnis bei <%s>.\n", pclWhere);AfxMessageBox(pclErrbuf);
				SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
				MessageBox(GetString(IDS_STRING919), GetString(IDS_STRING920), MB_OK);
			}
			else	
			{
				char pclInfo[100];
				oMinStr = olMinDate.Format( "%d.%m.%Y" );
				oMaxStr = olMaxDate.Format( "%d.%m.%Y" );
				sprintf(pclInfo, "%s - %s", oMinStr, oMaxStr);
				olData.omData.Sort(CompareSkey);	// verkn�pfte Fl�ge werden zusammengestellt

				// Additional summery report for Shanghai if no airline selected
				if ((ogCustomer == "SHA") && (ogAppName == "FIPS") && (strlen(pclSelect) == 0))
				{
					CAdditionalReportTableDlg olAdditionalReportTableDlg(this, 0, &(olData.omData), pclInfo, pclSelect);
					olAdditionalReportTableDlg.DoModal();
				}
				else
				{
					CReportTableDlg olReportTableDlg(this, 0, &(olData.omData), pclInfo, pclSelect);
					olReportTableDlg.DoModal();
				}
			}

			HideStatus() ;
		}
	}
	else if (m_CR_List2.GetCheck() == 1)
	{
		TRACE("CReportSelectDlg::StartTableDlg: List2\n");
		CTime olMinDate = omLastValues.GetMinTime() ;
		CTime olMaxDate = omLastValues.GetMaxTime() ;
		char pclSelect[128];

		// SHA: Statistic of Flights that take of from the same originating Airport
		if ((ogCustomer == "SHA") && (ogAppName == "FIPS"))
		{
			pclSelect[0] = 0;	
			CReportSeqFieldDlg olReportDlg(this, GetString(IDS_STRING1565), GetString(IDS_STRING1563), &olMinDate, &olMaxDate, pclSelect, "AAAA", 3, 4);

			if(olReportDlg.DoModal() == IDOK)
			{
				
				ShowStatus() ;
				m_PR_Read.StepIt() ;

				// R�ckgabewerte festhalten
				omLastValues.SetMinTime( olMinDate ) ;
				omLastValues.SetMaxTime( olMaxDate ) ;
				omLastValues.SetSelect( pclSelect ) ;

				blOk = true;

				CString oMinStr = olMinDate.Format( "%Y%m%d000000" );
				CString oMaxStr = olMaxDate.Format( "%Y%m%d235959" );
				MakeUtc( oMinStr, oMaxStr ) ;

				CString olOrg("") ;
				if(strlen(pclSelect) == 3)
				{
					olOrg.Format(" AND ORG3='%s'", pclSelect ) ;
				}
				else if(strlen(pclSelect) == 4)
				{
					olOrg.Format(" AND ORG4='%s'", pclSelect ) ;
				}
				// f�r SHA keine explizite Angabe einer ORG notwendig
				else if ((ogCustomer == "SHA") && (ogAppName == "FIPS") && (strlen(pclSelect) == 0))
				{
				
				}
				else
				{
					// Airlines nur 2Ltr oder 3Ltr-Code !
					MessageBox("3Ltr/4Ltr-Code only!", GetString(IDS_WARNING), MB_ICONWARNING );
					return false;
				}

				// Portionsweise einlesen
				// 2/99 hbe
				PARTIALREAD olPartial ;
				olPartial.Preset() ;
				CString olWherePlus ;
				olWherePlus.Format("%s %s", olOrg, olFtyp );
				olPartial.WhereClause.Format("WHERE (STOA BETWEEN '%%s' AND '%%s' AND DES3='%s' )", 
					               pcgHome, pcgHome );
				olPartial.WhereClause += olWherePlus;
				olPartial.StartTime = DBStringToDateTime(oMinStr) ;
				olPartial.EndTime  = DBStringToDateTime(oMaxStr) ;
				olPartial.omData = &olData.omData ;
				olPartial.Fields = "RKEY,SKEY,SEAS,FLNO,TTYP,VIA3,ACT3,DES3,ALC2,ALC3,STOA,STOD,ONBL,OFBL,VIAL,ORG3,ORG4,FLNO,STOA,ETAI,LAND,ACT3,ACT5,VIAN";
				
				SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));

				bool blReadRet = ReadPartialWhere( &olPartial, &m_PR_Read ) ;
				if( ! blReadRet )
				{
					//sprintf(pclErrbuf,"\nKein Suchergebnis bei <%s>.\n", pclWhere);AfxMessageBox(pclErrbuf);
					SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
					MessageBox(GetString(IDS_STRING919), GetString(IDS_STRING920), MB_OK);
				}
				else	
				{
					char pclInfo[100];
					oMinStr = olMinDate.Format( "%d.%m.%Y" );
					oMaxStr = olMaxDate.Format( "%d.%m.%Y" );
					sprintf(pclInfo, "%s - %s", oMinStr, oMaxStr);
					olData.omData.Sort(CompareSkey);	// verkn�pfte Fl�ge werden zusammengestellt

					// Additional summery report for if no point of origin selected
					if ((ogCustomer == "SHA") && (ogAppName == "FIPS") && (strlen(pclSelect) == 0))
					{
						CAdditionalReportTableDlg olAdditionalReportTableDlg(this, 1, &(olData.omData), pclInfo, pclSelect);
						olAdditionalReportTableDlg.DoModal();
					}
					else
					{
						CReportTableDlg olReportTableDlg(this, 1, &(olData.omData), pclInfo, pclSelect);
						olReportTableDlg.DoModal();
					}
				}

				HideStatus() ;
			}
		}
		else
		{
			CReportSeqDlg olReportDlg(this, GetString(IDS_STRING1047), &olMinDate, &olMaxDate);

			if(olReportDlg.DoModal() == IDOK)
			{
				ShowStatus() ;
				m_PR_Read.StepIt() ;

				// R�ckgabewerte festhalten
				omLastValues.SetMinTime( olMinDate ) ;
				omLastValues.SetMaxTime( olMaxDate ) ;

				blOk = true;
				CString oMinStr = olMinDate.Format( "%Y%m%d000000" );
				CString oMaxStr = olMaxDate.Format( "%Y%m%d235959" );

				MakeUtc(oMinStr, oMaxStr ) ;

				// immer Ftyp operation
				olFtyp = comFtypO ;
				SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));

				// Portionsweise einlesen
				// 2/99 hbe
				PARTIALREAD olPartial ;
				olPartial.Preset() ;
				// olPartial.WhereClause.Format("WHERE (TIFD BETWEEN '%%s' AND '%%s' AND ORG3='%s' ) AND ADID='D' %s", pcgHome, olFtyp);
				olPartial.WhereClause.Format("WHERE (TIFD BETWEEN '%%s' AND '%%s' AND ORG3='%s' ) %s", pcgHome, olFtyp);
				olPartial.StartTime = DBStringToDateTime(oMinStr) ;
				olPartial.EndTime  = DBStringToDateTime(oMaxStr) ;
				olPartial.omData = &olData.omData ;
				olPartial.Fields = "FLNO,TIFD,TTYP,DES3,VIA3,ACT3,GTD1,GTD2,PSTD,WRO1,URNO,VIAL";
				olPartial.Case = 1 ;
				olPartial.SortArt = RotationDlgCedaFlightData::FlightSort ;

				bool blReadRet = ReadPartialWhere( &olPartial, &m_PR_Read ) ;
				if( ! blReadRet )
				{
					//sprintf(pclErrbuf,"\nKein Suchergebnis bei <%s>.\n", pclWhere);	AfxMessageBox(pclErrbuf);
					SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
					MessageBox(GetString(IDS_STRING919), GetString(IDS_STRING921), MB_OK);
				}
				else	
				{
					char pclInfo[100];
					oMinStr = olMinDate.Format( "%d.%m.%Y" );
					oMaxStr = olMaxDate.Format( "%d.%m.%Y" );
					sprintf(pclInfo, "%s - %s", oMinStr, oMaxStr);
					CReportTableDlg olReportTableDlg(this, 1, &(olData.omData), pclInfo);
					olReportTableDlg.DoModal();
				}

				HideStatus() ;
			}
		}


	}
	else if (m_CR_List3.GetCheck() == 1)
	{
		TRACE("CReportSelectDlg::StartTableDlg: List3\n");
		CTime olMinDate = omLastValues.GetMinTime() ;
		CTime olMaxDate = omLastValues.GetMaxTime() ;
		char pclSelect[128];

		// SHA: Statistic of Flights that the same AC-Regsitration
		if ((ogCustomer == "SHA") && (ogAppName == "FIPS"))
		{
			pclSelect[0] = 0;
			CReportSeqFieldDlg olReportDlg(this, GetString(IDS_STRING1589), GetString(IDS_STRING1588), 
				                           &olMinDate, &olMaxDate, pclSelect, 
										   /*"X(12)"*/"#|A|'.'|'-'#|A|'.'|'-'#|A|'.'|'-'#|A|'.'|'-'#|A|'.'|'-'#|A|'.'|'-'#|A|'.'|'-'#|A|'.'|'-'#|A|'.'|'-'#|A|'.'|'-'#|A|'.'|'-'#|A|'.'|'-'", 5, 12);

			if(olReportDlg.DoModal() == IDOK)
			{
				
				ShowStatus() ;
				m_PR_Read.StepIt() ;

				// R�ckgabewerte festhalten
				omLastValues.SetMinTime( olMinDate ) ;
				omLastValues.SetMaxTime( olMaxDate ) ;
				omLastValues.SetSelect( pclSelect ) ;

				blOk = true;

				CString oMinStr = olMinDate.Format( "%Y%m%d000000" );
				CString oMaxStr = olMaxDate.Format( "%Y%m%d235959" );
				MakeUtc( oMinStr, oMaxStr ) ;

				CString olACType("");
				if(strlen(pclSelect) <= 12 && strlen(pclSelect) > 0)
				{
					olACType.Format(" AND REGN='%s'", pclSelect ) ;
				}
				// f�r SHA keine explizite Angabe einer Registrierung notwendig
				else if ((ogCustomer == "SHA") && (ogAppName == "FIPS") && (strlen(pclSelect) == 0))
				{
				
				}
				else
				{
					// REGN max 12 Zeichen lang
					MessageBox("12 Characters only!", GetString(IDS_WARNING), MB_ICONWARNING );
					return false;
				}

				// Portionsweise einlesen
				// 2/99 hbe
				PARTIALREAD olPartial ;
				olPartial.Preset() ;
				CString olWherePlus ;
				olWherePlus.Format("%s %s", olACType, olFtyp );
				olPartial.WhereClause += olWherePlus;
				olPartial.StartTime = DBStringToDateTime(oMinStr) ;
				olPartial.EndTime  = DBStringToDateTime(oMaxStr) ;
				olPartial.omData = &olData.omData ;
				olPartial.Fields = "RKEY,SKEY,SEAS,FLNO,ACT3,DES3,STOA,STOD,ORG3,ORG4,DES3,DES4,FLNO,STOA,AIRB,LAND,ACT3,ACT5,ADID,REGN";
				
				SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));

				bool blReadRet = ReadPartialWhere( &olPartial, &m_PR_Read ) ;
				if( ! blReadRet )
				{
					//sprintf(pclErrbuf,"\nKein Suchergebnis bei <%s>.\n", pclWhere);AfxMessageBox(pclErrbuf);
					SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
					MessageBox(GetString(IDS_STRING919), GetString(IDS_STRING920), MB_OK);
				}
				else	
				{
					char pclInfo[100];
					oMinStr = olMinDate.Format( "%d.%m.%Y" );
					oMaxStr = olMaxDate.Format( "%d.%m.%Y" );
					sprintf(pclInfo, "%s - %s", oMinStr, oMaxStr);
					olData.omData.Sort(CompareSkey);	// verkn�pfte Fl�ge werden zusammengestellt

					// Additional summery report for if no point of origin selected
					if ((ogCustomer == "SHA") && (ogAppName == "FIPS") && (strlen(pclSelect) == 0))
					{
						CAdditionalReportTableDlg olAdditionalReportTableDlg(this, 2, &(olData.omData), pclInfo, pclSelect);
						olAdditionalReportTableDlg.DoModal();
					}
					else
					{
						CReportTableDlg olReportTableDlg(this, 2, &(olData.omData), pclInfo, pclSelect);
						olReportTableDlg.DoModal();
					}
				}

				HideStatus() ;
			}
		}
		else
		{
			RotationDlgCedaFlightData olAllData;

			strcpy(pclSelect, omLastValues.GetFlno() ) ;

			CReportSeqFieldDlg olReportDlg(this, GetString(IDS_STRING1048), GetString(IDS_STRING347), &olMinDate, &olMaxDate, pclSelect);

			if(olReportDlg.DoModal() == IDOK)
			{
				ShowStatus();
				m_PR_Read.StepIt() ;
				
				// R�ckgabewerte festhalten
				omLastValues.SetMinTime( olMinDate ) ;
				omLastValues.SetMaxTime( olMaxDate ) ;
				omLastValues.SetFlno( pclSelect ) ;

				blOk = true;

				CString oMinStr = olMinDate.Format( "%Y%m%d000000" );
				CString oMaxStr = olMaxDate.Format( "%Y%m%d235959" );
				MakeUtc( oMinStr, oMaxStr ) ;

				SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));

				// Portionsweise einlesen
				// 2/99 hbe
				PARTIALREAD olPartial ;
				olPartial.Preset() ;
				CString olWherePlus ;
				olWherePlus.Format(" AND FLNO='%s' %s", pclSelect, olFtyp );
				olPartial.WhereClause += olWherePlus;
				olPartial.StartTime = DBStringToDateTime(oMinStr) ;
				olPartial.EndTime  = DBStringToDateTime(oMaxStr) ;
				olPartial.omData = &olData.omData ;
				olPartial.Fields = "RKEY,SKEY,STEV,FLNO,STOA,TTYP,ORG3,VIA3,ACT3,GTA1,GTA2,PSTA,STOD,DES3,GTD1,GTD2,PSTD,WRO1,LSTU,URNO,VIAL";
				
				bool blReadRet = ReadPartialWhere( &olPartial, &m_PR_Read ) ;
				if( ! blReadRet )
				{
					//sprintf(pclErrbuf,"\nKein Suchergebnis bei <%s>.\n", pclWhere);	AfxMessageBox(pclErrbuf);
					SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
					MessageBox(GetString(IDS_STRING919), GetString(IDS_STRING922), MB_OK);
				}
				else	
				{
					char pclRkeyList[600];
					RotationDlgCedaFlightData olRkeyData;	// gibts schon , n�tig ?
					int ilLines = olData.omData.GetSize();  
					char pclWhere[160] ;
					char pclFields[160];

					for(int i=0;i<ilLines;i++)
					{
						if(i == 0)
							sprintf(pclRkeyList,"%d",olData.omData[i].Rkey);
						else
						{
							sprintf(pclRkeyList,"%s,%d",pclRkeyList,olData.omData[i].Rkey);
						}
					}
					sprintf(pclWhere,"WHERE RKEY IN (%s)",pclRkeyList);
					strcpy( pclFields, olPartial.Fields ) ; 
					if(!olData.ReadSpecial(&olAllData.omData, pclWhere, pclFields,false,true))
					{
						SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
						MessageBox(GetString(IDS_STRING919), GetString(IDS_STRING922), MB_OK);
					}
					else
					{
						char pclInfo[100];
						oMinStr = olMinDate.Format( "%d.%m.%Y" );
						oMaxStr = olMaxDate.Format( "%d.%m.%Y" );
						sprintf(pclInfo, "%s - %s", oMinStr, oMaxStr);
						olAllData.omData.Sort(CompareSkey);	// verkn�pfte Fl�ge werden zusammengestellt
						CReportTableDlg olReportTableDlg(this, 2, &(olAllData.omData), pclInfo, pclSelect);
						olReportTableDlg.DoModal();
					}

				}
				HideStatus() ;
			}
		}
	}
	else if (m_CR_List4.GetCheck() == 1)
	{
		TRACE("CReportSelectDlg::StartTableDlg: List4\n");
		CTime olDate;
		CTime olMinDate = omLastValues.GetMinTime() ;
		CTime olMaxDate = omLastValues.GetMaxTime() ;
		char pclSelect[128];

		// SHA: Statistic of Flights that take of from the same destination Airport
		if ((ogCustomer == "SHA") && (ogAppName == "FIPS"))
		{
			pclSelect[0] = 0;

			CReportSeqFieldDlg olReportDlg(this, GetString(IDS_STRING1574), GetString(IDS_STRING1575), &olMinDate, &olMaxDate, pclSelect, "AAAA", 3, 4);

			if(olReportDlg.DoModal() == IDOK)
			{
				
				ShowStatus() ;
				m_PR_Read.StepIt() ;

				// R�ckgabewerte festhalten
				omLastValues.SetMinTime( olMinDate ) ;
				omLastValues.SetMaxTime( olMaxDate ) ;
				omLastValues.SetSelect( pclSelect ) ;

				blOk = true;

				CString oMinStr = olMinDate.Format( "%Y%m%d000000" );
				CString oMaxStr = olMaxDate.Format( "%Y%m%d235959" );
				MakeUtc( oMinStr, oMaxStr ) ;

				CString olDes("");
				if(strlen(pclSelect) == 3)
				{
					olDes.Format(" AND DES3='%s'", pclSelect ) ;
				}
				else if(strlen(pclSelect) == 4)
				{
					olDes.Format(" AND DES4='%s'", pclSelect ) ;
				}
				// f�r SHA keine explizite Angabe einer destination notwendig
				else if ((ogCustomer == "SHA") && (ogAppName == "FIPS") && (strlen(pclSelect) == 0))
				{
				
				}
				else
				{
					MessageBox("3Ltr/4Ltr-Code only!", GetString(IDS_WARNING), MB_ICONWARNING );
					return false;
				}

				// Portionsweise einlesen
				// 2/99 hbe
				PARTIALREAD olPartial ;
				olPartial.Preset() ;
				CString olWherePlus ;
				olWherePlus.Format("%s %s", olDes, olFtyp );
				olPartial.WhereClause.Format("WHERE (STOD BETWEEN '%%s' AND '%%s' AND ORG3='%s' )", 
					               pcgHome, pcgHome );
				olPartial.WhereClause += olWherePlus;
				olPartial.StartTime = DBStringToDateTime(oMinStr) ;
				olPartial.EndTime  = DBStringToDateTime(oMaxStr) ;
				olPartial.omData = &olData.omData ;
				olPartial.Fields = "RKEY,SKEY,SEAS,FLNO,TTYP,VIA3,ACT3,DES3,ALC2,ALC3,STOA,STOD,ONBL,OFBL,VIAL,ORG3,ORG4,DES3,DES4,FLNO,STOA,ETAI,LAND,AIRB,ETDI,ACT3,ACT5,VIAN";
				
				SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));

				bool blReadRet = ReadPartialWhere( &olPartial, &m_PR_Read ) ;
				if( ! blReadRet )
				{
					//sprintf(pclErrbuf,"\nKein Suchergebnis bei <%s>.\n", pclWhere);AfxMessageBox(pclErrbuf);
					SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
					MessageBox(GetString(IDS_STRING919), GetString(IDS_STRING920), MB_OK);
				}
				else	
				{
					char pclInfo[100];
					oMinStr = olMinDate.Format( "%d.%m.%Y" );
					oMaxStr = olMaxDate.Format( "%d.%m.%Y" );
					sprintf(pclInfo, "%s - %s", oMinStr, oMaxStr);
					olData.omData.Sort(CompareSkey);	// verkn�pfte Fl�ge werden zusammengestellt

					// Additional summery report for if no point of origin selected
					if ((ogCustomer == "SHA") && (ogAppName == "FIPS") && (strlen(pclSelect) == 0))
					{
						CAdditionalReportTableDlg olAdditionalReportTableDlg(this, 3, &(olData.omData), pclInfo, pclSelect);
						olAdditionalReportTableDlg.DoModal();
					}
					else
					{
						CReportTableDlg olReportTableDlg(this, 3, &(olData.omData), pclInfo, pclSelect);
						olReportTableDlg.DoModal();
					}
				}

				HideStatus() ;
			}
		}
		else
		{
			strcpy(pclSelect, omLastValues.GetMinuten() ) ;
			CReportSeqFieldDlg olReportDlg(this, GetString(IDS_STRING1049), GetString(IDS_STRING1050), &olMinDate, &olMaxDate, pclSelect);

			if(olReportDlg.DoModal() == IDOK)
			{
				ShowStatus() ;
				m_PR_Read.StepIt() ;

				// R�ckgabewerte festhalten
				omLastValues.SetMinTime( olMinDate ) ;
				omLastValues.SetMaxTime( olMaxDate ) ;
				omLastValues.SetMinuten( pclSelect ) ;

				blOk = true;

				CString oMinStr = olMinDate.Format( "%Y%m%d000000" );
				CString oMaxStr = olMaxDate.Format( "%Y%m%d235959" );
				MakeUtc( oMinStr, oMaxStr ) ;

				SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));

				// Portionsweise einlesen
				// 2/99 hbe
				PARTIALREAD olPartial ;
				olPartial.Preset() ;
				olPartial.WhereClause += " AND STEV='M' AND FTYP<>'T' AND FTYP<>'G'";
				olPartial.StartTime = DBStringToDateTime(oMinStr) ;
				olPartial.EndTime  = DBStringToDateTime(oMaxStr) ;
				olPartial.omData = &olData.omData ;
				olPartial.Fields = "RKEY,SKEY,ADID,STEV,FLNO,TIFA,ORG3,VIA3,ACT3,GTA1,GTD1,STOA,STOD,DES3,WRO1,PSTA,PSTD,SEAS,ADID,VIAL";

				bool blReadRet = ReadPartialWhere( &olPartial, &m_PR_Read ) ;
				if( ! blReadRet )
				{
					//sprintf(pclErrbuf,"\nKein Suchergebnis bei <%s>.\n", pclWhere);	AfxMessageBox(pclErrbuf);
					SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
					MessageBox(GetString(IDS_STRING919), GetString(IDS_STRING923), MB_OK);
				}
				else	
				{
					char pclInfo[100];
					oMinStr = olMinDate.Format( "%d.%m.%Y" );
					oMaxStr = olMaxDate.Format( "%d.%m.%Y" );
					sprintf(pclInfo, "%s - %s", oMinStr, oMaxStr);
					
					olData.omData.Sort(CompareSkey);	// verkn�pfte Fl�ge werden zusammengestellt
					CReportTableDlg olReportTableDlg(this, 3, &(olData.omData), pclInfo, pclSelect);
					olReportTableDlg.DoModal();
				}

				HideStatus() ;
			}
		}
	}
	else if (m_CR_List5.GetCheck() == 1)
	{
		TRACE("CReportSelectDlg::StartTableDlg: List5\n");
		CTime olDate;
		char pclSelect[128];
		CTime olMinDate = omLastValues.GetMinTime() ;
		CTime olMaxDate = omLastValues.GetMaxTime() ;


		// SHA: Statistic of Flights that the same AC-Type
		if ((ogCustomer == "SHA") && (ogAppName == "FIPS"))
		{
			pclSelect[0] = 0;
			CReportSeqFieldDlg olReportDlg(this, GetString(IDS_STRING1580), GetString(IDS_STRING1569), &olMinDate, &olMaxDate, pclSelect,"A|#A|#A|#A|#A|#", 3, 5);

			if(olReportDlg.DoModal() == IDOK)
			{
				
				ShowStatus() ;
				m_PR_Read.StepIt() ;

				// R�ckgabewerte festhalten
				omLastValues.SetMinTime( olMinDate ) ;
				omLastValues.SetMaxTime( olMaxDate ) ;
				omLastValues.SetSelect( pclSelect ) ;

				blOk = true;

				CString oMinStr = olMinDate.Format( "%Y%m%d000000" );
				CString oMaxStr = olMaxDate.Format( "%Y%m%d235959" );
				MakeUtc( oMinStr, oMaxStr ) ;

				CString olACType("");
				if(strlen(pclSelect) == 3)
				{
					olACType.Format(" AND ACT3='%s'", pclSelect ) ;
				}
				else if(strlen(pclSelect) == 5 || strlen(pclSelect) == 4)
				{
					olACType.Format(" AND ACT5='%s'", pclSelect ) ;
				}
				// f�r SHA keine explizite Angabe einer ACT notwendig
				else if ((ogCustomer == "SHA") && (ogAppName == "FIPS") && (strlen(pclSelect) == 0))
				{
				
				}
				else
				{
					// AC-Type nur 2Ltr oder 3Ltr-Code !
					MessageBox("IATA/ICAO-Code only!", GetString(IDS_WARNING), MB_ICONWARNING );
					return false;
				}

				// Portionsweise einlesen
				// 2/99 hbe
				PARTIALREAD olPartial ;
				olPartial.Preset() ;
				CString olWherePlus ;
				olWherePlus.Format("%s %s", olACType, olFtyp );
				olPartial.WhereClause += olWherePlus;
				olPartial.StartTime = DBStringToDateTime(oMinStr) ;
				olPartial.EndTime  = DBStringToDateTime(oMaxStr) ;
				olPartial.omData = &olData.omData ;
				olPartial.Fields = "RKEY,SKEY,SEAS,FLNO,TTYP,ACT3,DES3,STOA,STOD,ORG3,ORG4,DES3,DES4,FLNO,STOA,AIRB,LAND,ACT3,ACT5,ADID";
				
				SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));

				bool blReadRet = ReadPartialWhere( &olPartial, &m_PR_Read ) ;
				if( ! blReadRet )
				{
					//sprintf(pclErrbuf,"\nKein Suchergebnis bei <%s>.\n", pclWhere);AfxMessageBox(pclErrbuf);
					SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
					MessageBox(GetString(IDS_STRING919), GetString(IDS_STRING920), MB_OK);
				}
				else	
				{
					char pclInfo[100];
					oMinStr = olMinDate.Format( "%d.%m.%Y" );
					oMaxStr = olMaxDate.Format( "%d.%m.%Y" );
					sprintf(pclInfo, "%s - %s", oMinStr, oMaxStr);
					olData.omData.Sort(CompareSkey);	// verkn�pfte Fl�ge werden zusammengestellt

					// Additional summery report for if no point of origin selected
					if ((ogCustomer == "SHA") && (ogAppName == "FIPS") && (strlen(pclSelect) == 0))
					{
						CAdditionalReportTableDlg olAdditionalReportTableDlg(this, 4, &(olData.omData), pclInfo, pclSelect);
						olAdditionalReportTableDlg.DoModal();
					}
					else
					{
						CReportTableDlg olReportTableDlg(this, 4, &(olData.omData), pclInfo, pclSelect);
						olReportTableDlg.DoModal();
					}
				}

				HideStatus() ;
			}
		}
		else
		{
			strcpy(pclSelect, omLastValues.GetMesse() ) ;

			CReportSeqFieldDlg olReportDlg(this, GetString(IDS_STRING1051), GetString(IDS_STRING1037), &olMinDate, &olMaxDate, pclSelect);

			if(olReportDlg.DoModal() == IDOK)
			{
				ShowStatus() ;
				m_PR_Read.StepIt() ;

				// R�ckgabewerte festhalten
				omLastValues.SetMinTime( olMinDate ) ;
				omLastValues.SetMaxTime( olMaxDate ) ;
				omLastValues.SetMesse( pclSelect ) ;

				blOk = true;

				CString oMinStr = olMinDate.Format( "%Y%m%d000000" );
				CString oMaxStr = olMaxDate.Format( "%Y%m%d235959" );
				MakeUtc(oMinStr, oMaxStr ) ;

				SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
				// Portionsweise einlesen
				// 2/99 hbe
				PARTIALREAD olPartial ;
				olPartial.Preset() ;
				olPartial.WhereClause += " AND STEV='M' AND FTYP<>'T' AND FTYP<>'G'";
				olPartial.StartTime = DBStringToDateTime(oMinStr) ;
				olPartial.EndTime  = DBStringToDateTime(oMaxStr) ;
				olPartial.omData = &olData.omData ;
				olPartial.Fields = "RKEY,SKEY,STEV,FLNO,TIFA,ORG3,VIA3,ACT3,STOA,STOD,DES3,SEAS,GTA1,PSTA,TTYP,GTD1,PSTD,WRO1,URNO,VIAL,CKIT,CKIF";

				bool blReadRet = ReadPartialWhere( &olPartial, &m_PR_Read ) ;
				if( ! blReadRet )
				{
					//sprintf(pclErrbuf,"\nKein Suchergebnis bei <%s>.\n", pclWhere);	AfxMessageBox(pclErrbuf);
					SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
					MessageBox(GetString(IDS_STRING919), GetString(IDS_STRING924), MB_OK);
				}
				else	
				{

					char pclInfo[100];
					oMinStr = olMinDate.Format( "%d.%m.%Y" );
					oMaxStr = olMaxDate.Format( "%d.%m.%Y" );
					sprintf(pclInfo, "%s - %s", oMinStr, oMaxStr);
					olData.omData.Sort(CompareSkey);	// verkn�pfte Fl�ge werden zusammengestellt
					CReportTableDlg olReportTableDlg(this, 4, &(olData.omData), pclInfo, pclSelect);
					olReportTableDlg.DoModal();
				}

				HideStatus() ;
			}
		}
	}
	else if (m_CR_List6.GetCheck() == 1)
	{
		TRACE("CReportSelectDlg::StartTableDlg: List6\n");
		CTime olMinDate = omLastValues.GetMinTime() ;
		CTime olMaxDate = omLastValues.GetMaxTime() ;
		char pclSelect[128];

		// SHA: Statistic of Flights that the same Flight Nature (TTYP)
		if ((ogCustomer == "SHA") && (ogAppName == "FIPS"))
		{
			pclSelect[0] = 0;
			CReportSeqFieldDlg olReportDlg(this, GetString(IDS_STRING1596), GetString(IDS_STRING1597), &olMinDate, &olMaxDate, pclSelect, "XXXXX", 1, 5);

			if(olReportDlg.DoModal() == IDOK)
			{
				
				ShowStatus() ;
				m_PR_Read.StepIt() ;

				// R�ckgabewerte festhalten
				omLastValues.SetMinTime( olMinDate ) ;
				omLastValues.SetMaxTime( olMaxDate ) ;
				omLastValues.SetSelect( pclSelect ) ;

				blOk = true;

				CString oMinStr = olMinDate.Format( "%Y%m%d000000" );
				CString oMaxStr = olMaxDate.Format( "%Y%m%d235959" );
				MakeUtc( oMinStr, oMaxStr ) ;

				CString olTType("");
				if(strlen(pclSelect) <= 5 && strlen(pclSelect) > 0)
				{
					olTType.Format(" AND TTYP='%s'", pclSelect ) ;
				}
				// f�r SHA keine explizite Angabe einer ACT notwendig
				else if ((ogCustomer == "SHA") && (ogAppName == "FIPS") && (strlen(pclSelect) == 0))
				{
				
				}
				else
				{
					MessageBox("5Ltr-Code only!", GetString(IDS_WARNING), MB_ICONWARNING );
					return false;
				}

				// Portionsweise einlesen
				// 2/99 hbe
				PARTIALREAD olPartial ;
				olPartial.Preset() ;
				CString olWherePlus ;
				olWherePlus.Format("%s %s", olTType, olFtyp );
				olPartial.WhereClause += olWherePlus;
				olPartial.StartTime = DBStringToDateTime(oMinStr) ;
				olPartial.EndTime  = DBStringToDateTime(oMaxStr) ;
				olPartial.omData = &olData.omData ;
				olPartial.Fields = "RKEY,SKEY,SEAS,FLNO,TTYP,ACT3,DES3,STOA,STOD,ORG3,ORG4,DES3,DES4,FLNO,STOA,AIRB,LAND,ACT3,ACT5,ADID";
				
				SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));

				bool blReadRet = ReadPartialWhere( &olPartial, &m_PR_Read ) ;
				if( ! blReadRet )
				{
					//sprintf(pclErrbuf,"\nKein Suchergebnis bei <%s>.\n", pclWhere);AfxMessageBox(pclErrbuf);
					SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
					MessageBox(GetString(IDS_STRING919), GetString(IDS_STRING920), MB_OK);
				}
				else	
				{
					char pclInfo[100];
					oMinStr = olMinDate.Format( "%d.%m.%Y" );
					oMaxStr = olMaxDate.Format( "%d.%m.%Y" );
					sprintf(pclInfo, "%s - %s", oMinStr, oMaxStr);
					olData.omData.Sort(CompareSkey);	// verkn�pfte Fl�ge werden zusammengestellt

					// Additional summery report for if no point of origin selected
					if ((ogCustomer == "SHA") && (ogAppName == "FIPS") && (strlen(pclSelect) == 0))
					{
						CAdditionalReportTableDlg olAdditionalReportTableDlg(this, 5, &(olData.omData), pclInfo, pclSelect);
						olAdditionalReportTableDlg.DoModal();
					}
					else
					{
						CReportTableDlg olReportTableDlg(this, 5, &(olData.omData), pclInfo, pclSelect);
						olReportTableDlg.DoModal();
					}
				}

				HideStatus() ;
			}
		}
		else
		{
			CReportSeqDlg olReportDlg(this, GetString(IDS_STRING1052), &olMinDate, &olMaxDate);

			if(olReportDlg.DoModal() == IDOK)
			{
				
				ShowStatus() ;
				m_PR_Read.StepIt() ;

				// R�ckgabewerte festhalten
				omLastValues.SetMinTime( olMinDate ) ;
				omLastValues.SetMaxTime( olMaxDate ) ;

				blOk = true;

				CString oMinStr = olMinDate.Format( "%Y%m%d000000" );
				CString oMaxStr = olMaxDate.Format( "%Y%m%d235959" );
				MakeUtc( oMinStr, oMaxStr ) ;
				SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));

				// Portionsweise einlesen
				// 2/99 hbe
				PARTIALREAD olPartial ;
				// olPartial.WhereClause = "WHERE (TIFD BETWEEN '%s' AND '%s' OR TIFA BETWEEN '%s' AND '%s') AND (STEV='M' AND FTYP<>'T' AND FTYP<>'G')";
				olPartial.WhereClause = omDefaultWhereTif;
				olPartial.WhereClause += " AND (STEV='M' AND FTYP<>'T' AND FTYP<>'G')";
				olPartial.StartTime = DBStringToDateTime(oMinStr) ;
				olPartial.EndTime  = DBStringToDateTime(oMaxStr) ;
				olPartial.Range = RRWeek ;
				olPartial.Case = 5 ;
				olPartial.omData = &olData.omData ;
				olPartial.Fields = "TIFA,TIFD,ADID" ;
				olPartial.onlyAFT = false ;
				olPartial.Sort = true ;
				olPartial.SortArt = RotationDlgCedaFlightData::RotationSort ;

				bool blReadRet = ReadPartialWhere( &olPartial, &m_PR_Read ) ;
				if( ! blReadRet )
				{
					//sprintf(pclErrbuf,"\nKein Suchergebnis bei <%s>.\n", pclWhere);	AfxMessageBox(pclErrbuf);
					SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
					MessageBox(GetString(IDS_STRING919), GetString(IDS_STRING925), MB_OK);
				}
				else	
				{
					char pclInfo[100];
					oMinStr = olMinDate.Format( "%d.%m.%Y" );
					oMaxStr = olMaxDate.Format( "%d.%m.%Y" );
					sprintf(pclInfo, "%s - %s", oMinStr, oMaxStr);
					olData.omData.Sort(CompareChrono);	// Fl�ge chronologisch zusammenstellen
					CReportTableDlg olReportTableDlg(this, 5, &(olData.omData), pclInfo);
					olReportTableDlg.DoModal();
				}
				HideStatus() ;
			}
		}	
	}
	else if (m_CR_List7.GetCheck() == 1)
	{
		TRACE("CReportSelectDlg::StartTableDlg: List7\n");
		CTime olMinDate = omLastValues.GetMinTime() ;
		CTime olMaxDate = omLastValues.GetMaxTime() ;
		char pclSelect[128];

		// SHA: flight statistics according to delay reason
		if ((ogCustomer == "SHA") && (ogAppName == "FIPS"))
		{
			pclSelect[0] = 0;
			CReportSeqFieldDlg olReportDlg(this, GetString(IDS_STRING1606), GetString(IDS_STRING1607), &olMinDate, &olMaxDate, pclSelect, "A|#A|#A|#A|#A", 2, 5);

			if(olReportDlg.DoModal() == IDOK)
			{
				
				ShowStatus() ;
				m_PR_Read.StepIt() ;

				// R�ckgabewerte festhalten
				omLastValues.SetMinTime( olMinDate ) ;
				omLastValues.SetMaxTime( olMaxDate ) ;
				omLastValues.SetSelect( pclSelect ) ;

				blOk = true;

				CString oMinStr = olMinDate.Format( "%Y%m%d000000" );
				CString oMaxStr = olMaxDate.Format( "%Y%m%d235959" );
				MakeUtc( oMinStr, oMaxStr ) ;

				CString olDcd("");
				if(strlen(pclSelect) <= 2 && strlen(pclSelect) > 0)
				{
					olDcd.Format(" AND (DCD1='%s' OR DCD2='%s')", pclSelect, pclSelect ) ;
				}
				// f�r SHA keine explizite Angabe eines DCD notwendig
				else if ((ogCustomer == "SHA") && (ogAppName == "FIPS") && (strlen(pclSelect) == 0))
				{
					// nur Fluege finden mit Angabe irgendeiner DCD
					olDcd = " AND DCD1<>' '";
				}
				else
				{
					MessageBox("2Ltr-Code only!", GetString(IDS_WARNING), MB_ICONWARNING );
					return false;
				}

				// Portionsweise einlesen
				// 2/99 hbe
				PARTIALREAD olPartial ;
				olPartial.Preset() ;
				CString olWherePlus ;
				olWherePlus.Format("%s %s", olDcd, olFtyp );
				olPartial.WhereClause += olWherePlus;
				olPartial.StartTime = DBStringToDateTime(oMinStr) ;
				olPartial.EndTime  = DBStringToDateTime(oMaxStr) ;
				olPartial.omData = &olData.omData ;
				olPartial.Fields = "RKEY,SKEY,SEAS,FLNO,TTYP,ACT3,DES3,STOA,STOD,ORG3,ORG4,DES3,DES4,FLNO,STOA,OFBL,ONBL,ACT3,ACT5,ADID,ALC3,ALC2,ADID,DCD1";
				
				SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));

				bool blReadRet = ReadPartialWhere( &olPartial, &m_PR_Read ) ;
				if( ! blReadRet )
				{
					//sprintf(pclErrbuf,"\nKein Suchergebnis bei <%s>.\n", pclWhere);AfxMessageBox(pclErrbuf);
					SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
					MessageBox(GetString(IDS_STRING919), GetString(IDS_STRING920), MB_OK);
				}
				else	
				{
					char pclInfo[100];
					oMinStr = olMinDate.Format( "%d.%m.%Y" );
					oMaxStr = olMaxDate.Format( "%d.%m.%Y" );
					sprintf(pclInfo, "%s - %s", oMinStr, oMaxStr);
					olData.omData.Sort(CompareSkey);	// verkn�pfte Fl�ge werden zusammengestellt

					// Additional summery report for if no point of origin selected
					if ((ogCustomer == "SHA") && (ogAppName == "FIPS") && (strlen(pclSelect) == 0))
					{
						CAdditionalReportTableDlg olAdditionalReportTableDlg(this, 6, &(olData.omData), pclInfo, pclSelect);
						olAdditionalReportTableDlg.DoModal();
					}
					else
					{
						CReportTableDlg olReportTableDlg(this, 6, &(olData.omData), pclInfo, pclSelect);
						olReportTableDlg.DoModal();
					}
				}

				HideStatus() ;
			}
		}
		else
		{
			strcpy(pclSelect, omLastValues.GetMinuten() ) ;

			CReportSeqFieldDlg olReportDlg(this, GetString(IDS_STRING1053), GetString(IDS_STRING1054), &olMinDate, &olMaxDate, pclSelect);


			if(olReportDlg.DoModal() == IDOK)
			{
				ShowStatus() ;
				m_PR_Read.StepIt() ;

				// R�ckgabewerte festhalten
				omLastValues.SetMinTime( olMinDate ) ;
				omLastValues.SetMaxTime( olMaxDate ) ;
				omLastValues.SetMinuten( pclSelect ) ;

				blOk = true;

				CString oMinStr = olMinDate.Format( "%Y%m%d000000" );
				CString oMaxStr = olMaxDate.Format( "%Y%m%d235959" );
				MakeUtc( oMinStr, oMaxStr ) ;

				SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));

				// Portionsweise einlesen
				// 2/99 hbe
				PARTIALREAD olPartial ;
				// olPartial.WhereClause = "WHERE (TIFD BETWEEN '%s' AND '%s' OR TIFA BETWEEN '%s' AND '%s') AND (STEV='M' AND FTYP<>'T' AND FTYP<>'G')";
				olPartial.WhereClause = omDefaultWhereTif;
				olPartial.WhereClause += " AND (STEV='M' AND FTYP<>'T' AND FTYP<>'G')";
				olPartial.StartTime = DBStringToDateTime(oMinStr) ;
				olPartial.EndTime  = DBStringToDateTime(oMaxStr) ;
				olPartial.Range = RRWeek ;
				olPartial.Case = 6 ;
				olPartial.omData = &olData.omData ;
				olPartial.Fields = "TIFA,ORG3,DES3,RKEY,SKEY,STOA,STOD,ADID" ;
				olPartial.onlyAFT = false ;
				olPartial.Sort = true ;
				olPartial.SortArt = RotationDlgCedaFlightData::DefSort ;

				bool blReadRet = ReadPartialWhere( &olPartial, &m_PR_Read ) ;
				if( ! blReadRet )
				{
					//sprintf(pclErrbuf,"\nKein Suchergebnis bei <%s>.\n", pclWhere);	AfxMessageBox(pclErrbuf);
					SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
					MessageBox(GetString(IDS_STRING919), GetString(IDS_STRING926), MB_OK);
				}
				else	
				{
					char pclInfo[100];
					oMinStr = olMinDate.Format( "%d.%m.%Y" );
					oMaxStr = olMaxDate.Format( "%d.%m.%Y" );
					sprintf(pclInfo, "%s - %s", oMinStr, oMaxStr);
					olData.omData.Sort(CompareSkey);	// verkn�pfte Fl�ge werden zusammengestellt
					CReportTableDlg olReportTableDlg(this, 6, &(olData.omData), pclInfo, pclSelect);
					olReportTableDlg.DoModal();
				}
				HideStatus() ;
			}
		}
	}
	else if (m_CR_List8.GetCheck() == 1)
	{
		TRACE("CReportSelectDlg::StartTableDlg: List8\n");
		CTime olMinDate = omLastValues.GetMinTime() ;
		CTime olMaxDate = omLastValues.GetMaxTime() ;

		// SHA: flight statistics according to delay time
		if ((ogCustomer == "SHA") && (ogAppName == "FIPS"))
		{
			char pclSelect[128];
			pclSelect[0] = 0;
			CReportSeqFieldDlg olReportDlg(this, GetString(IDS_STRING1613), GetString(IDS_STRING1614), &olMinDate, &olMaxDate, pclSelect, "####", 1, 4);

			if(olReportDlg.DoModal() == IDOK)
			{
				
				ShowStatus() ;
				m_PR_Read.StepIt() ;

				// R�ckgabewerte festhalten
				omLastValues.SetMinTime( olMinDate ) ;
				omLastValues.SetMaxTime( olMaxDate ) ;
				omLastValues.SetSelect( pclSelect ) ;

				blOk = true;

				CString oMinStr = olMinDate.Format( "%Y%m%d000000" );
				CString oMaxStr = olMaxDate.Format( "%Y%m%d235959" );
				MakeUtc( oMinStr, oMaxStr ) ;

				CString olDcd("");
				if(strlen(pclSelect) <= 4 && strlen(pclSelect) > 0)
				{
					CString olSelect(pclSelect);
					int ilSelectLength = olSelect.GetLength();
					switch (ilSelectLength)
					{
						case 1:
							olSelect = "000" + olSelect;
						break;
						case 2:
							olSelect = "00" + olSelect;
						break;
						case 3:
							olSelect = "0" + olSelect;
						break;
					}

					olDcd.Format(" AND (DTD1>='%s' OR DTD2>='%s')", olSelect, olSelect);
				}
				else if ((ogCustomer == "SHA") && (ogAppName == "FIPS") && (strlen(pclSelect) == 0))
				{
					olDcd = " AND DTD1>'0014'";
				}
				else
				{
					MessageBox("4 digits only!", GetString(IDS_WARNING), MB_ICONWARNING );
					return false;
				}

				// Portionsweise einlesen
				// 2/99 hbe
				PARTIALREAD olPartial ;
				olPartial.Preset() ;
				CString olWherePlus ;
				olWherePlus.Format("%s %s", olDcd, olFtyp );
				olPartial.WhereClause += olWherePlus;
				olPartial.StartTime = DBStringToDateTime(oMinStr);
				olPartial.EndTime  = DBStringToDateTime(oMaxStr);
				olPartial.omData = &olData.omData;
 				olPartial.Fields = "RKEY,SKEY,SEAS,FLNO,TTYP,STOA,STOD,ORG3,ORG4,DES3,DES4,FLNO,OFBL,ONBL,ACT3,ACT5,ADID,ALC3,ALC2,ADID,DCD1,DCD2,DTD1,DTD2";
				
				SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));

				bool blReadRet = ReadPartialWhere( &olPartial, &m_PR_Read ) ;
				if( ! blReadRet )
				{
					//sprintf(pclErrbuf,"\nKein Suchergebnis bei <%s>.\n", pclWhere);AfxMessageBox(pclErrbuf);
					SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
					MessageBox(GetString(IDS_STRING919), GetString(IDS_STRING920), MB_OK);
				}
				else	
				{
					char pclInfo[100];
					oMinStr = olMinDate.Format( "%d.%m.%Y" );
					oMaxStr = olMaxDate.Format( "%d.%m.%Y" );
					sprintf(pclInfo, "%s - %s", oMinStr, oMaxStr);
					olData.omData.Sort(CompareSkey);	// verkn�pfte Fl�ge werden zusammengestellt

					// Additional summery report for if no point of origin selected
					if ((ogCustomer == "SHA") && (ogAppName == "FIPS") && (strlen(pclSelect) == 0))
					{
						CAdditionalReportTableDlg olAdditionalReportTableDlg(this, 7, &(olData.omData), pclInfo, pclSelect);
						olAdditionalReportTableDlg.DoModal();
					}
					else
					{
						CReportTableDlg olReportTableDlg(this, 7, &(olData.omData), pclInfo, pclSelect);
						olReportTableDlg.DoModal();
					}
				}

				HideStatus() ;
			}
		}
		else
		{
			CReportSeqDlg olReportDlg(this, GetString(IDS_STRING1607), &olMinDate, &olMaxDate);

			if(olReportDlg.DoModal() == IDOK)
			{
				ShowStatus() ;
				m_PR_Read.StepIt() ;

				// R�ckgabewerte festhalten
				omLastValues.SetMinTime( olMinDate ) ;
				omLastValues.SetMaxTime( olMaxDate ) ;

				blOk = true;

				CString oMinStr = olMinDate.Format( "%Y%m%d000000" );
				CString oMaxStr = olMaxDate.Format( "%Y%m%d235959" );
				/***
				if(bgReportLocal)
				{
					olUtcMinDate = DBStringToDateTime(oMinStr);
					olUtcMaxDate = DBStringToDateTime(oMaxStr);

					ogBasicData.LocalToUtc(olUtcMinDate);
					ogBasicData.LocalToUtc(olUtcMaxDate);

					oMinStr = olUtcMinDate.Format( "%Y%m%d%H%M%S" );
					oMaxStr = olUtcMaxDate.Format( "%Y%m%d%H%M%S" );
				}
				***/
				MakeUtc( oMinStr, oMaxStr ) ;
				SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));

				// Portionsweise einlesen
				// 2/99 hbe
				PARTIALREAD olPartial ;
				// olPartial.WhereClause.Format("WHERE TIFD BETWEEN '%%s' AND '%%s' AND ADID='D' %s", olFtyp );
				olPartial.WhereClause.Format("WHERE TIFD BETWEEN '%%s' AND '%%s' AND ORG3='%s' %s", pcgHome, olFtyp );
				olPartial.StartTime = DBStringToDateTime(oMinStr) ;
				olPartial.EndTime  = DBStringToDateTime(oMaxStr) ;
				olPartial.Range = RRWeek ;
				olPartial.Case = 7 ;
				olPartial.omData = &olData.omData ;
				olPartial.Fields = "ALC2,ALC3,DES3,TIFD" ;
				olPartial.onlyAFT = false ;
				olPartial.Sort = true ;
				olPartial.SortArt = RotationDlgCedaFlightData::DefSort ;

				bool blReadRet = ReadPartialWhere( &olPartial, &m_PR_Read ) ;
				if( ! blReadRet )
				{
					//sprintf(pclErrbuf,"\nKein Suchergebnis bei <%s>.\n", pclWhere);	AfxMessageBox(pclErrbuf);
					SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
					MessageBox(GetString(IDS_STRING919), GetString(IDS_STRING927), MB_OK);
				}
				else	
				{
					char pclInfo[100];
					oMinStr = olMinDate.Format( "%d.%m.%Y" );
					oMaxStr = olMaxDate.Format( "%d.%m.%Y" );
					sprintf(pclInfo, "%s - %s", oMinStr, oMaxStr);
					//olData.omData.Sort(CompareToDes3Alc);
					//olData.omData.Sort(CompareToAlcDes3);
					olData.omData.Sort(CompareToAlc2Des3);
					CReportTableDlg olReportTableDlg(this, 7, &(olData.omData), pclInfo);
					olReportTableDlg.DoModal();
				}
				HideStatus() ;
			}
		}
	}
	else if (m_CR_List9.GetCheck() == 1) //	Fluggesellschaften eines Saisonflugplanes
	{
		TRACE("CReportSelectDlg::StartTableDlg: List9\n");
//		CTime olMinDate = TIMENULL;
//		CTime olMaxDate = TIMENULL;
		CTime olMinDate = omLastValues.GetMinTime() ;
		CTime olMaxDate = omLastValues.GetMaxTime() ;

		char pclSelect[128];

		// SHA: statistics according flight cancelled (detailed)
		if ((ogCustomer == "SHA") && (ogAppName == "FIPS"))
		{
			char pclSelect[128];
			pclSelect[0] = 0;
			CReportSeqDlg olReportDlg(this, GetString(IDS_STRING1623), &olMinDate, &olMaxDate);

			if(olReportDlg.DoModal() == IDOK)
			{
				ShowStatus() ;
				m_PR_Read.StepIt() ;

				// R�ckgabewerte festhalten
				omLastValues.SetMinTime( olMinDate ) ;
				omLastValues.SetMaxTime( olMaxDate ) ;
				omLastValues.SetSelect( pclSelect ) ;

				blOk = true;

				CString oMinStr = olMinDate.Format( "%Y%m%d000000" );
				CString oMaxStr = olMaxDate.Format( "%Y%m%d235959" );
				MakeUtc( oMinStr, oMaxStr ) ;

				CString olX = " AND FTYP='X'";

				// Portionsweise einlesen
				// 2/99 hbe
				PARTIALREAD olPartial ;
				olPartial.Preset() ;
				CString olWherePlus ;
				olWherePlus.Format("%s", olX);
				olPartial.WhereClause += olWherePlus;
				olPartial.StartTime = DBStringToDateTime(oMinStr);
				olPartial.EndTime  = DBStringToDateTime(oMaxStr);
				olPartial.omData = &olData.omData;
 				olPartial.Fields = "RKEY,SKEY,SEAS,FLNO,TTYP,ACT3,DES3,STOA,STOD,ORG3,ORG4,DES3,DES4,FLNO,STOA,AIRB,LAND,ACT3,ACT5,ADID,ALC3,ALC2,ADID,FTYP";
				
				SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));

				bool blReadRet = ReadPartialWhere( &olPartial, &m_PR_Read ) ;
				if( ! blReadRet )
				{
					//sprintf(pclErrbuf,"\nKein Suchergebnis bei <%s>.\n", pclWhere);AfxMessageBox(pclErrbuf);
					SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
					MessageBox(GetString(IDS_STRING919), GetString(IDS_STRING920), MB_OK);
				}
				else	
				{
					char pclInfo[100];
					oMinStr = olMinDate.Format( "%d.%m.%Y" );
					oMaxStr = olMaxDate.Format( "%d.%m.%Y" );
					sprintf(pclInfo, "%s - %s", oMinStr, oMaxStr);
					olData.omData.Sort(CompareSkey);	// verkn�pfte Fl�ge werden zusammengestellt

					CReportTableDlg olReportTableDlg(this, 8, &(olData.omData), pclInfo, pclSelect);
					olReportTableDlg.DoModal();
				}

				HideStatus() ;
			}
		}
		else
		{
			strcpy(pclSelect, omLastValues.GetSaison() ) ;

			CReportFieldDlg olReportDlg(this, pclSelect);

			if(olReportDlg.DoModal() == IDOK)
			{
				ShowStatus() ;
				m_PR_Read.StepIt() ;

				omLastValues.SetSaison( pclSelect ) ;

				blOk = true;
				bool blRet;
				CString olFrom;
				CString olTo;
				CString olMinStr;
				CString olMaxStr;

				blRet = ogBCD.GetField("SEA", "SEAS", pclSelect, "VPFR", olMinStr);
				if(blRet == TRUE)
					blRet = ogBCD.GetField("SEA", "SEAS", pclSelect, "VPTO", olMaxStr);
				if (blRet == FALSE)
				{
					SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
					MessageBox(GetString(IDS_STRING919), GetString(IDS_STRING928), MB_OK);
				}
				else
				{
					MakeUtc( olMinStr, olMaxStr ) ;

					SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));

					// 2/99 hbe
					PARTIALREAD olPartial ;
					olPartial.Preset() ;
					CString olWherePlus;
					// olWherePlus.Format(" AND ADID='D' %s", olFtyp );
					olPartial.WhereClause.Format("WHERE STOD BETWEEN '%%s' AND '%%s' AND ORG3='%s' %s", pcgHome, olFtyp );
					// olWherePlus.Format(" %s", olFtyp );
					olPartial.WhereClause += olWherePlus ;
					olPartial.StartTime = DBStringToDateTime(olMinStr) ;
					olPartial.EndTime  = DBStringToDateTime(olMaxStr) ;
					olPartial.omData = &olData.omData ;
					olPartial.Fields = "ALC2,ALC3" ;

					bool blReadRet = ReadPartialWhere( &olPartial, &m_PR_Read ) ;
					if( ! blReadRet )
					{
						SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
						MessageBox(GetString(IDS_STRING919), GetString(IDS_STRING928), MB_OK);
					}
					else	
					{
						char pclInfo[100];
						ogBasicData.UtcToLocal(olUtcMinDate);
						ogBasicData.UtcToLocal(olUtcMaxDate);
						olMinStr = olUtcMinDate.Format( "%d.%m.%Y" );
						olMaxStr = olUtcMaxDate.Format( "%d.%m.%Y" );
						sprintf(pclInfo, "%s - %s", olMinStr, olMaxStr);
						olData.omData.Sort(CompareToAlc2);
						CReportTableDlg olReportTableDlg(this, 8, &(olData.omData), pclInfo);
						olReportTableDlg.DoModal();
					}
				}

				HideStatus();
			}
		}
	}
	else if (m_CR_List10.GetCheck() == 1) //	Liste aller angeflogenen Flugh�fen mit Frequenz
	{
		TRACE("CReportSelectDlg::StartTableDlg: List10\n");
		CTime olMinDate = omLastValues.GetMinTime() ;
		CTime olMaxDate = omLastValues.GetMaxTime() ;

		// SHA: statistics according flight cancelled (summary)
		if ((ogCustomer == "SHA") && (ogAppName == "FIPS"))
		{
			char pclSelect[128];
			pclSelect[0] = 0;
			CReportSeqDlg olReportDlg(this, GetString(IDS_STRING1633), &olMinDate, &olMaxDate);

			if(olReportDlg.DoModal() == IDOK)
			{
				ShowStatus() ;
				m_PR_Read.StepIt() ;

				// R�ckgabewerte festhalten
				omLastValues.SetMinTime( olMinDate ) ;
				omLastValues.SetMaxTime( olMaxDate ) ;
				omLastValues.SetSelect( pclSelect ) ;

				blOk = true;

				CString oMinStr = olMinDate.Format( "%Y%m%d000000" );
				CString oMaxStr = olMaxDate.Format( "%Y%m%d235959" );
				MakeUtc( oMinStr, oMaxStr ) ;

				CString olX = " AND FTYP='X'";

				// Portionsweise einlesen
				// 2/99 hbe
				PARTIALREAD olPartial ;
				olPartial.Preset() ;
				CString olWherePlus ;
				olWherePlus.Format("%s", olX);
				olPartial.WhereClause += olWherePlus;
				olPartial.StartTime = DBStringToDateTime(oMinStr);
				olPartial.EndTime  = DBStringToDateTime(oMaxStr);
				olPartial.omData = &olData.omData;
 				olPartial.Fields = "RKEY,SKEY,SEAS,FLNO,TTYP,ACT3,DES3,STOA,STOD,ORG3,ORG4,DES3,DES4,FLNO,STOA,AIRB,LAND,ACT3,ACT5,ADID,ALC3,ALC2,ADID,FTYP";
				
				SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));

				bool blReadRet = ReadPartialWhere( &olPartial, &m_PR_Read ) ;
				if( ! blReadRet )
				{
					//sprintf(pclErrbuf,"\nKein Suchergebnis bei <%s>.\n", pclWhere);AfxMessageBox(pclErrbuf);
					SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
					MessageBox(GetString(IDS_STRING919), GetString(IDS_STRING920), MB_OK);
				}
				else	
				{
					char pclInfo[100];
					oMinStr = olMinDate.Format( "%d.%m.%Y" );
					oMaxStr = olMaxDate.Format( "%d.%m.%Y" );
					sprintf(pclInfo, "%s - %s", oMinStr, oMaxStr);
					olData.omData.Sort(CompareSkey);	// verkn�pfte Fl�ge werden zusammengestellt

					CAdditionalReportTableDlg olAdditionalReportTableDlg(this, 9, &(olData.omData), pclInfo, pclSelect);
					olAdditionalReportTableDlg.DoModal();
				}

				HideStatus() ;
			}
		}
		else
		{
			CReportSeqDlg olReportDlg(this, GetString(IDS_STRING1056), &olMinDate, &olMaxDate);

			if(olReportDlg.DoModal() == IDOK)
			{
				ShowStatus() ;
				m_PR_Read.StepIt() ;

				// R�ckgabewerte festhalten
				omLastValues.SetMinTime( olMinDate ) ;
				omLastValues.SetMaxTime( olMaxDate ) ;

				blOk = true;
				CString oMinStr = olMinDate.Format( "%Y%m%d000000" );
				CString oMaxStr = olMaxDate.Format( "%Y%m%d235959" );
				MakeUtc( oMinStr, oMaxStr ) ;

				SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));

				// Portionsweise einlesen
				// 2/99 hbe
				PARTIALREAD olPartial ;
				olPartial.Preset() ;
				// olPartial.WhereClause.Format("WHERE (TIFD BETWEEN '%%s' AND '%%s') AND ADID='D' %s", olFtyp);
				olPartial.WhereClause.Format("WHERE TIFD BETWEEN '%%s' AND '%%s' AND ORG3='%s' %s", pcgHome, olFtyp);
				olPartial.StartTime = DBStringToDateTime(oMinStr) ;
				olPartial.EndTime  = DBStringToDateTime(oMaxStr) ;
				olPartial.omData = &olData.omData ;
				olPartial.Fields = "DES3,ADID,TTYP";
				olPartial.Case = 9 ;

				bool blReadRet = ReadPartialWhere( &olPartial, &m_PR_Read ) ;
				if( ! blReadRet )
				{
					SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
					MessageBox(GetString(IDS_STRING919), GetString(IDS_STRING929), MB_OK);
				}
				else	
				{
					char pclInfo[100];
					oMinStr = olMinDate.Format( "%d.%m.%Y" );
					oMaxStr = olMaxDate.Format( "%d.%m.%Y" );
					sprintf(pclInfo, "%s - %s", oMinStr, oMaxStr);
					olData.omData.Sort(CompareToDes3);
					CReportTableDlg olReportTableDlg(this, 9, &(olData.omData), pclInfo);
					olReportTableDlg.DoModal();
				}

				HideStatus() ;
			}
		}
	}
	else if (m_CR_List11.GetCheck() == 1) //	Arbeitsflugplan
	{
		TRACE("CReportSelectDlg::StartTableDlg: List11\n");
		CTime olDate;
		CTime olMinDate = omLastValues.GetMinTime() ;
		CTime olMaxDate = omLastValues.GetMaxTime() ;

		// SHA: Statistic according to arr/dep flight each day
		if ((ogCustomer == "SHA") && (ogAppName == "FIPS"))
		{
			char pclSelect[128];
			pclSelect[0] = 0;
			CReportSeqDlg olReportDlg(this, GetString(IDS_STRING1630), &olMinDate, &olMaxDate);

			if(olReportDlg.DoModal() == IDOK)
			{
				ShowStatus() ;
				m_PR_Read.StepIt() ;

				// R�ckgabewerte festhalten
				omLastValues.SetMinTime( olMinDate ) ;
				omLastValues.SetMaxTime( olMaxDate ) ;
				omLastValues.SetSelect( pclSelect ) ;

				blOk = true;

				CString oMinStr = olMinDate.Format( "%Y%m%d000000" );
				CString oMaxStr = olMaxDate.Format( "%Y%m%d235959" );
				MakeUtc( oMinStr, oMaxStr ) ;

				CString olOrgDes;
				CString oMinStrTmp = olMinDate.Format( "%d.%m.%Y" );
				CString oMaxStrTmp = olMaxDate.Format( "%d.%m.%Y" );
				if (oMinStrTmp == oMaxStrTmp || oMaxStrTmp.IsEmpty())
				{
					// Abfrage, ob Arrival oder Departure
					ReportArrDepSelctDlg olReportArrDepSelctDlg(this);
					if (olReportArrDepSelctDlg.DoModal() == IDOK)
					{
						imArrDep = olReportArrDepSelctDlg.m_ListArrDep;
					}
					else
					{
						return false;
					}
				}

				// Portionsweise einlesen
				// 2/99 hbe
				PARTIALREAD olPartial;
				olPartial.Preset();
				CString olWherePlus;
				olWherePlus.Format(" %s", olFtyp);
				olPartial.WhereClause += olWherePlus;
				olPartial.StartTime = DBStringToDateTime(oMinStr);
				olPartial.EndTime  = DBStringToDateTime(oMaxStr);
				olPartial.omData = &olData.omData;
 				olPartial.Fields = "RKEY,SKEY,SEAS,FLNO,TTYP,ACT3,DES3,STOA,STOD,ORG3,ORG4,DES3,DES4,FLNO,STOA,AIRB,LAND,ACT3,ACT5,ADID,ALC3,ALC2,ADID,FTYP,REGN,BLT1,GTA1,GTD1,BLT1,PSTD,PSTA,CKIT,URNO";
				
				SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));

				bool blReadRet = ReadPartialWhere( &olPartial, &m_PR_Read ) ;
				if( ! blReadRet )
				{
					//sprintf(pclErrbuf,"\nKein Suchergebnis bei <%s>.\n", pclWhere);AfxMessageBox(pclErrbuf);
					SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
					MessageBox(GetString(IDS_STRING919), GetString(IDS_STRING920), MB_OK);
				}
				else	
				{
					char pclInfo[100];
					oMinStr = olMinDate.Format( "%d.%m.%Y" );
					oMaxStr = olMaxDate.Format( "%d.%m.%Y" );
					sprintf(pclInfo, "%s - %s", oMinStr, oMaxStr);
					olData.omData.Sort(CompareSkey);	// verkn�pfte Fl�ge werden zusammengestellt

					if (oMinStr == oMaxStr || oMaxStr.IsEmpty())
					{
						CReportTableDlg olReportTableDlg(this, 10, &(olData.omData), pclInfo, 
							                             pclSelect, NULL, imArrDep);
						olReportTableDlg.DoModal();
					}
					else
					{
						CAdditionalReportTableDlg olAdditionalReportTableDlg(this, 10, 
							                                                 &(olData.omData),
																			 pclInfo, 
																			 pclSelect);
						olAdditionalReportTableDlg.DoModal();
					}
				}

				HideStatus() ;
			}
		}
		else
		{
			CReportSeqDlg olReportDlg(this, GetString(IDS_STRING1057), &olMinDate, &olMaxDate);

			if(olReportDlg.DoModal() == IDOK)
			{
				ShowStatus() ;
				m_PR_Read.StepIt() ;

				// R�ckgabewerte festhalten
				omLastValues.SetMinTime( olMinDate ) ;
				omLastValues.SetMaxTime( olMaxDate ) ;

				blOk = true;
				CString olMinStr = olMinDate.Format( "%Y%m%d000000" );
				CString olMaxStr = olMaxDate.Format( "%Y%m%d235959" );
				MakeUtc( olMinStr, olMaxStr ) ;
				
				// immer Ftyp operation
				olFtyp = comFtypO ;


				SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));

				// 2/99 hbe
				PARTIALREAD olPartial ;
				olPartial.Preset() ;
				// neu 23/2/99
				// 5/99
				// olPartial.WhereClause.Format(
				//	"WHERE ( (STOA BETWEEN '%%s' AND '%%s' AND DES3='%s' ) OR ( STOD BETWEEN '%%s' AND '%%s' AND ORG3='%s'))",
				//	pcgHome, pcgHome );
				olPartial.WhereClause += " "+ comFtypO;

				olPartial.StartTime = DBStringToDateTime(olMinStr) ;
				olPartial.EndTime  = DBStringToDateTime(olMaxStr) ;
				olPartial.omData = &olData.omData ;
				olPartial.Fields = "RKEY,SKEY,FLNO,STOA,TTYP,ORG3,VIA3,ACT3,GTA1,PSTA,STOD,DES3,GTD1,GTD2,PSTD,RWYA,REM1,RWYD,REGN,WRO1,AIRB,LAND,VIAL,BLT1,EXT1,URNO,CKIF,CKIT";

				bool blReadRet = ReadPartialWhere( &olPartial, &m_PR_Read ) ;
				if( ! blReadRet )
				{
					SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
					MessageBox(GetString(IDS_STRING919), GetString(IDS_STRING930), MB_OK);
				}
				else	
				{
					char pclInfo[100];
					olMinStr = olMinDate.Format( "%d.%m.%Y" );
					olMaxStr = olMaxDate.Format( "%d.%m.%Y" );
					sprintf(pclInfo, "%s - %s", olMinStr, olMaxStr);

					olData.omData.Sort(CompareSkey);	// verkn�pfte Fl�ge werden zusammengestellt
					CReportTableDlg olReportTableDlg(this, 10, &(olData.omData), pclInfo);
					olReportTableDlg.DoModal();
				}

				HideStatus() ;
			}
		}
	}
	else if (m_CR_List12.GetCheck() == 1) //	Statistik Sitzplatzangebot nach LVGs und/oder Destinationen
	{
		TRACE("CReportSelectDlg::StartTableDlg: List12\n");
		int ilSelect;
		char pclSelect[128];
		CTime olMinDate = omLastValues.GetMinTime() ;
		CTime olMaxDate = omLastValues.GetMaxTime() ;

		// SHA: Flight statistic according to domestic/international/transfer
		if ((ogCustomer == "SHA") && (ogAppName == "FIPS"))
		{
			char pclSelect[128];
			pclSelect[0] = 0;
			CReportSeqDlg olReportDlg(this, GetString(IDS_STRING1634), &olMinDate, &olMaxDate);

			if(olReportDlg.DoModal() == IDOK)
			{
				ShowStatus() ;
				m_PR_Read.StepIt() ;

				// R�ckgabewerte festhalten
				omLastValues.SetMinTime( olMinDate ) ;
				omLastValues.SetMaxTime( olMaxDate ) ;
				omLastValues.SetSelect( pclSelect ) ;

				blOk = true;

				CString oMinStr = olMinDate.Format( "%Y%m%d000000" );
				CString oMaxStr = olMaxDate.Format( "%Y%m%d235959" );
				MakeUtc( oMinStr, oMaxStr ) ;

				CString olOrgDes;
				CString oMinStrTmp = olMinDate.Format( "%d.%m.%Y" );
				CString oMaxStrTmp = olMaxDate.Format( "%d.%m.%Y" );

				PARTIALREAD olPartial;
				olPartial.Preset();
				CString olWherePlus;
				olWherePlus.Format(" %s", olFtyp );
				olPartial.WhereClause += olWherePlus;
				olPartial.StartTime = DBStringToDateTime(oMinStr);
				olPartial.EndTime  = DBStringToDateTime(oMaxStr);
				olPartial.omData = &olData.omData;
 				olPartial.Fields = "RKEY,SKEY,SEAS,FLNO,TTYP,ACT3,DES3,STOA,STOD,ORG3,ORG4,DES3,DES4,FLNO,URNO,PAXI,PAXT";
				
				SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));

				bool blReadRet = ReadPartialWhere( &olPartial, &m_PR_Read ) ;
				if( ! blReadRet )
				{
					SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
					MessageBox(GetString(IDS_STRING919), GetString(IDS_STRING920), MB_OK);
				}
				else	
				{
					char pclInfo[100];
					oMinStr = olMinDate.Format( "%d.%m.%Y" );
					oMaxStr = olMaxDate.Format( "%d.%m.%Y" );
					sprintf(pclInfo, "%s - %s", oMinStr, oMaxStr);
					olData.omData.Sort(CompareSkey);	

					CReportTableSpecialDlg olReportTableSpecialDlg(this, 11, 
							                                       &(olData.omData), 
																   pclInfo, pclSelect, 
																   NULL, imArrDep);
					olReportTableSpecialDlg.DoModal();
				}

				HideStatus() ;
			}
		}
		else
		{
			ilSelect = omLastValues.GetRadio() ;
			CReportSortDlg olReportDlg(this, GetString(IDS_STRING1058), &olMinDate, &olMaxDate, &ilSelect);

			if(olReportDlg.DoModal() == IDOK)
			{
				ShowStatus() ;
				m_PR_Read.StepIt() ;

				// R�ckgabewerte festhalten
				omLastValues.SetMinTime( olMinDate ) ;
				omLastValues.SetMaxTime( olMaxDate ) ;
				omLastValues.SetRadio( ilSelect ) ;

				blOk = true;
				CString oMinStr = olMinDate.Format( "%Y%m%d000000" );
				CString oMaxStr = olMaxDate.Format( "%Y%m%d235959" );
				MakeUtc( oMinStr, oMaxStr ) ;


				SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));

				// 2/99 hbe
				PARTIALREAD olPartial ;
				olPartial.Preset() ;
				// olPartial.WhereClause.Format("WHERE (TIFD BETWEEN '%%s' AND '%%s' OR TIFA BETWEEN '%%s' AND '%%s') AND ADID='D' %s", olFtyp );
				// olPartial.WhereClause.Format("%s AND ADID='D' %s", omDefaultWhereTif, olFtyp );
				// olPartial.WhereClause.Format("%s %s", omDefaultWhereTif, olFtyp );
				olPartial.WhereClause.Format("WHERE (TIFD BETWEEN '%%s' AND '%%s' AND ORG3='%s') %s", pcgHome, olFtyp );
				olPartial.StartTime = DBStringToDateTime(oMinStr) ;
				olPartial.EndTime  = DBStringToDateTime(oMaxStr) ;
				olPartial.omData = &olData.omData ;
				olPartial.Fields = "ALC2,ALC3,DES3,ACT3,ACT5";	 // ACT3,ACT5 f�r SEAT

				
				// if(!olData.ReadSpecial(&olData.omData, pclWhere, pclFields,false,true))
				bool blReadRet = ReadPartialWhere( &olPartial, &m_PR_Read ) ;
 				if( ! blReadRet )
				{
					//sprintf(pclErrbuf,"\nKein Suchergebnis bei <%s>.\n", pclWhere);	AfxMessageBox(pclErrbuf);
					SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
					MessageBox(GetString(IDS_STRING919), GetString(IDS_STRING931), MB_OK);
				}
				else	
				{
					char pclInfo[100];
					oMinStr = olMinDate.Format( "%d.%m.%Y" );
					oMaxStr = olMaxDate.Format( "%d.%m.%Y" );
					sprintf(pclInfo, "%s - %s", oMinStr, oMaxStr);

					// TRACE("Anzahl Datens�tze [%d]\n", olData.omData.GetSize() ) ;

					if(ilSelect == 0)	//	Sortieren nach LVGs
					{
						olData.omData.Sort(CompareToAlc2);
						strcpy(pclSelect, GetString(IDS_STRING1532) );
						CReportTableDlg olReportTableDlg(this, 12, &(olData.omData), pclInfo, pclSelect);
						olReportTableDlg.DoModal();
					}
					else if(ilSelect == 1)	//	Sortieren nach Destinationen
					{
						olData.omData.Sort(CompareToDes3);
						strcpy(pclSelect, GetString(IDS_STRING1533) );
						CReportTableDlg olReportTableDlg(this, 13, &(olData.omData), pclInfo, pclSelect);
						olReportTableDlg.DoModal();
					}
					else if(ilSelect == 2)	//	Sortieren nach LVGs und Destinationen
					{
						olData.omData.Sort(CompareToAlc2Des3);
						strcpy(pclSelect, GetString(IDS_STRING1534));
						CReportTableDlg olReportTableDlg(this, 11, &(olData.omData), pclInfo, pclSelect);
						olReportTableDlg.DoModal();
					}
				}

				HideStatus() ;
			}
		}
	}
	else if (m_CR_List13.GetCheck() == 1) //	Wochenplan
	{
		TRACE("CReportSelectDlg::StartTableDlg: List13\n");
		CTime olMinDate = omLastValues.GetMinTime() ;
		CTime olMaxDate = omLastValues.GetMaxTime() ;

		// SHA: Flight statistic according to rush hour
		if ((ogCustomer == "SHA") && (ogAppName == "FIPS"))
		{
//			char pclSelect[256];
//			pclSelect[0] = 0;

			int ilSelect = omLastValues.GetRadio() ;
			CReportMovementsDlg olReportDlg1(this, GetString(IDS_STRING1637), &olMinDate, &olMaxDate, &ilSelect);

			if(olReportDlg1.DoModal() == IDOK)
			{
				ShowStatus() ;
				m_PR_Read.StepIt() ;

				// R�ckgabewerte festhalten
				omLastValues.SetMinTime( olMinDate ) ;
				omLastValues.SetMaxTime( olMaxDate ) ;
				omLastValues.SetRadio( ilSelect ) ;

				blOk = true;
				CString oMinStr = olMinDate.Format( "%Y%m%d000000" );
				CString oMaxStr = olMaxDate.Format( "%Y%m%d235959" );
				MakeUtc( oMinStr, oMaxStr ) ;

				CString olOrgDes;
//				CString oMinStrTmp = olMinDate.Format( "%d.%m.%Y" );
//				CString oMaxStrTmp = olMaxDate.Format( "%d.%m.%Y" );

				PARTIALREAD olPartial;
				olPartial.Preset();

				char pclSelect[256];
				CString olWherePlusActTime;
				if (ilSelect == 1)
				{
/*
					olPartial.WhereClause.Format("WHERE ((TIFA BETWEEN '%%s' AND '%%s' AND DES3='%s' ) OR ( TIFD BETWEEN '%%s' AND '%%s' AND ORG3='%s'))",
								pcgHome, pcgHome );
					strcpy (pclSelect, "ONBL/OFBL");
					olWherePlusActTime.Format("AND ((ONBL BETWEEN '%s' AND '%s') OR ( OFBL BETWEEN '%s' AND '%s' ))",
								oMinStr, oMaxStr, oMinStr, oMaxStr);
*/
					olPartial.WhereClause.Format("WHERE ((TIFA BETWEEN '%%s' AND '%%s' AND DES3='%s' AND ONBL BETWEEN '%s' AND '%s') OR  (TIFD BETWEEN '%%s' AND '%%s' AND ORG3='%s' AND OFBL BETWEEN '%s' AND '%s'))",
													 pcgHome, oMinStr, oMaxStr, pcgHome , oMinStr, oMaxStr);
					strcpy (pclSelect, "ONBL/OFBL");
				}
				else if (ilSelect == 2)
				{
/*
					olPartial.WhereClause.Format("WHERE ((TIFA BETWEEN '%%s' AND '%%s' AND DES3='%s' ) OR ( TIFD BETWEEN '%%s' AND '%%s' AND ORG3='%s'))",
								pcgHome, pcgHome );
					strcpy (pclSelect, "LAND/AIRB");
					olWherePlusActTime.Format("AND ((LAND BETWEEN '%s' AND '%s') OR ( AIRB BETWEEN '%s' AND '%s' ))",
								oMinStr, oMaxStr, oMinStr, oMaxStr);
*/
					olPartial.WhereClause.Format("WHERE ((TIFA BETWEEN '%%s' AND '%%s' AND DES3='%s' AND LAND BETWEEN '%s' AND '%s') OR  (TIFD BETWEEN '%%s' AND '%%s' AND ORG3='%s' AND AIRB BETWEEN '%s' AND '%s'))",
													 pcgHome, oMinStr, oMaxStr, pcgHome , oMinStr, oMaxStr);
					strcpy (pclSelect, "LAND/AIRB");
				}
				else 
				{
					strcpy (pclSelect, "STA/STD");
//					olPartial.Preset();
				}


				CString olWherePlus;
				olWherePlus.Format("%s %s", olOrgDes, olFtyp );
				olPartial.WhereClause += olWherePlusActTime + olWherePlus;
				olPartial.StartTime = DBStringToDateTime(oMinStr);
				olPartial.EndTime  = DBStringToDateTime(oMaxStr);
				olPartial.omData = &olData.omData;
 				olPartial.Fields = "RKEY,SKEY,SEAS,FLNO,TTYP,ACT3,DES3,STOA,STOD,ORG3,ORG4,DES3,DES4,FLNO,URNO,PAXI,PAXT,ONBL,OFBL,LAND,AIRB";
				olPartial.Case = 0;
				
				SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));

				bool blReadRet = ReadPartialWhere( &olPartial, &m_PR_Read ) ;
				HideStatus();
				if( ! blReadRet )
				{
					//sprintf(pclErrbuf,"\nKein Suchergebnis bei <%s>.\n", pclWhere);AfxMessageBox(pclErrbuf);
					SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
					MessageBox(GetString(IDS_STRING919), GetString(IDS_STRING920), MB_OK);
				}
				else	
				{
					char pclInfo[100];
					oMinStr = olMinDate.Format( "%d.%m.%Y" );
					oMaxStr = olMaxDate.Format( "%d.%m.%Y" );
					sprintf(pclInfo, "%s - %s", oMinStr, oMaxStr);
					olData.omData.Sort(CompareSkey);	// verkn�pfte Fl�ge werden zusammengestellt

					CAdditionalReportTableDlg olAdditionalReportTableDlg(this, 12, 
							                                             &(olData.omData),
																		 pclInfo, 
																		 pclSelect);
					olAdditionalReportTableDlg.DoModal();
				}

				HideStatus();
//				return false;
			}

/*



			CReportSeqDlg olReportDlg(this, GetString(IDS_STRING1637), &olMinDate, &olMaxDate);

			if(olReportDlg.DoModal() == IDOK)
			{
				ShowStatus() ;
				m_PR_Read.StepIt() ;

				omLastValues.SetMinTime( olMinDate ) ;
				omLastValues.SetMaxTime( olMaxDate ) ;
				omLastValues.SetSelect( pclSelect ) ;

				blOk = true;

				CString oMinStr = olMinDate.Format( "%Y%m%d000000" );
				CString oMaxStr = olMaxDate.Format( "%Y%m%d235959" );
				MakeUtc( oMinStr, oMaxStr ) ;

				CString olOrgDes;
				CString oMinStrTmp = olMinDate.Format( "%d.%m.%Y" );
				CString oMaxStrTmp = olMaxDate.Format( "%d.%m.%Y" );

				PARTIALREAD olPartial;
				olPartial.Preset();
				CString olWherePlus;
				olWherePlus.Format("%s %s", olOrgDes, olFtyp );
				olPartial.WhereClause += olWherePlus;
				olPartial.StartTime = DBStringToDateTime(oMinStr);
				olPartial.EndTime  = DBStringToDateTime(oMaxStr);
				olPartial.omData = &olData.omData;
 				olPartial.Fields = "RKEY,SKEY,SEAS,FLNO,TTYP,ACT3,DES3,STOA,STOD,ORG3,ORG4,DES3,DES4,FLNO,URNO,PAXI,PAXT";
				
				SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));

				bool blReadRet = ReadPartialWhere( &olPartial, &m_PR_Read ) ;
				if( ! blReadRet )
				{
					//sprintf(pclErrbuf,"\nKein Suchergebnis bei <%s>.\n", pclWhere);AfxMessageBox(pclErrbuf);
					SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
					MessageBox(GetString(IDS_STRING919), GetString(IDS_STRING920), MB_OK);
				}
				else	
				{
					char pclInfo[100];
					oMinStr = olMinDate.Format( "%d.%m.%Y" );
					oMaxStr = olMaxDate.Format( "%d.%m.%Y" );
					sprintf(pclInfo, "%s - %s", oMinStr, oMaxStr);
					olData.omData.Sort(CompareSkey);	// verkn�pfte Fl�ge werden zusammengestellt

					CAdditionalReportTableDlg olAdditionalReportTableDlg(this, 12, 
							                                             &(olData.omData),
																		 pclInfo, 
																		 pclSelect);
					olAdditionalReportTableDlg.DoModal();
				}

				HideStatus();
			}
*/
		}
		else
		{
			CString olWhere;
			char pclSelect[256];
			char pclWhere[256];
			CTimeSpan olTimeSpan( 6, 0, 0, 0 );
			//CTimeSpan olTimeSpan( 26, 0, 0, 0 ); zum testen

			// Preset
			omLastValues.SetPreset( 12, olMinDate, olMaxDate, olWhere ) ;
			strcpy( pclSelect, olWhere ) ;
			olWhere.Empty() ;

			CReportFromFieldDlg olReportDlg(this, GetString(IDS_STRING1059), GetString(IDS_STRING955), GetString(IDS_STRING1381),&olMinDate, pclSelect);

			if(olReportDlg.DoModal() == IDOK)
			{
				// R�ckgabewerte festhalten
				omLastValues.SetSelect( pclSelect ) ;
				
				blOk = true;
				CString oMinStr = olMinDate.Format( "%Y%m%d000000" );
				olMaxDate = olMinDate + olTimeSpan;
				CString oMaxStr = olMaxDate.Format( "%Y%m%d235959" );

				//***
				if(bgReportLocal)
				{
					olUtcMinDate = DBStringToDateTime(oMinStr);
					olUtcMaxDate = DBStringToDateTime(oMaxStr);

					ogBasicData.LocalToUtc(olUtcMinDate);
					ogBasicData.LocalToUtc(olUtcMaxDate);

					oMinStr = olUtcMinDate.Format( "%Y%m%d%H%M%S" );
					oMaxStr = olUtcMaxDate.Format( "%Y%m%d%H%M%S" );
				}
				//***

				// immer Ftyp operation + schedule !
				olFtyp = comFtypOS ;

				CString olAlc("") ;
				if(strlen(pclSelect) == 2)
				{
					olAlc.Format(" AND ALC2='%s'", pclSelect ) ;
				}
				else if(strlen(pclSelect) == 3)
				{
					olAlc.Format(" AND ALC3='%s'", pclSelect ) ;
				}
				else
				{
					// Airlines nur 2Ltr oder 3Ltr-Code !
					MessageBox("2Ltr/3Ltr-Code only!", GetString(IDS_STRING955), MB_ICONWARNING );
					return false;
				}
				
	//			sprintf(pclWhere,
	//				"((STOA<='%s' AND STOA>='%s') OR (STOD<='%s' AND STOD>='%s'))%s %s",
	//				oMaxStr, oMinStr, oMaxStr, oMinStr, olAlc, olFtyp );
				sprintf( pclWhere, omDefaultWhereSto, oMinStr, oMaxStr, oMinStr, oMaxStr ) ;
				CString olWherePlus ;
				olWherePlus.Format(" %s %s", olAlc, olFtyp );
				strcat(pclWhere, olWherePlus) ; 
				SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));

				if(!omCedaFlightData.ReadAllFlights(CString(pclWhere), false) )
				{
					SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
					MessageBox(GetString(IDS_STRING919), GetString(IDS_STRING932), MB_OK);
				}
				else	
				{
					char pclInfo[100];
					oMinStr = olMinDate.Format( "%d.%m.%Y" );
					oMaxStr = olMaxDate.Format( "%d.%m.%Y" );
					sprintf(pclInfo, "%s - %s", oMinStr, oMaxStr);

					CReportTableDlg olReportTableDlg(this, 14, NULL, pclInfo, pclSelect);
					olReportTableDlg.DoModal();
				}
			}
		}
	}

	// Inventory (A/C On Ground)
	else if (m_CR_List14.GetCheck() == 1)
	{
		TRACE("CReportSelectDlg::StartTableDlg: List14\n");

		CCSCedaData* olCCSData = new CCSCedaData;
		char pclFieldList[524];
		char pclWhere[256];
		char pclFields[100];

		sprintf(pclWhere, "WHERE STAT = 'O'");
		strcpy(pclFieldList, "URNO,STAT,FTYP,FLNU");

		CCSPtrArray<RecordSet> pomData;
		if (!olCCSData->CedaAction("RT","FOG",pclFieldList,pclWhere,"","","BUF1",false,0,&pomData,4))
		{
			CFPMSApp::MyTopmostMessageBox(this, GetString(IDS_NO_FOGTAB), GetString(ST_FEHLER), MB_OK | MB_ICONERROR);								
			return false;
		}

		CString olFlnus;
		for(int i = 0; i < pomData.GetSize(); i++)
		{
//			if (i > 20)
//				break;
			RecordSet rlRec = pomData.GetAt(i);

			if (rlRec.Values.GetSize() == 4)
			{
				CString olFLNU = rlRec.Values.GetAt(3);
				if(!olFLNU.IsEmpty())
				{
					if (olFlnus.IsEmpty())
						olFlnus += olFLNU;
					else
						olFlnus += "," + olFLNU; 
				}
			}
		}

		pomData.DeleteAll();
		delete olCCSData;

//		olFlnus = "178330812,178352456,178840074,178848105,178850194,178850199,178851081,178886975";

		if (olFlnus.IsEmpty())
		{
			MessageBox(GetString(IDS_STRING919), GetString(IDS_STRING933), MB_OK);
			return false;
		}
		else
		{	
			//Changed for the PRF 8212
			sprintf(pclWhere,"WHERE RKEY IN (%s) AND FTYP NOT IN ('N','X') ",olFlnus);
			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
			
			sprintf(pclFields,"URNO,REGN,ACT3,ACT5,PSTA,TIFA,ADID,ONBL,LAND,FLNO,CSGN,RKEY,FTYP");

			bool blOk = true;
			CStringArray olFlnuList;
			for(int i = SplitItemList(olFlnus, &olFlnuList, 250) - 1; i >= 0; i--)
			{
				sprintf(pclWhere,"WHERE RKEY IN (%s) AND FTYP NOT IN ('N','X') ",olFlnuList[i]);
				if(!olData.ReadSpecial(&olData.omData, pclWhere, pclFields, true, false) )
					blOk = false;
			}


			if(!blOk)
			{
				SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
				MessageBox(GetString(IDS_STRING919), GetString(IDS_STRING933), MB_OK);
			}
			else
			{
				char pclInfo[100];
				CString olUtcStr;
			
				CTime olCurrDate = CTime::GetCurrentTime();
				olUtcStr = olCurrDate.Format( "%d.%m.%Y" );
				sprintf(pclInfo, "%s", olUtcStr);
				int li_count = 0;
				long liRkey = 0;
				RotationDlgCedaFlightData olData5;
					olData.omData.Sort(CompareSkey);	
				for(int j =(olData.omData.GetSize()-1);j>=0;j--)
				{
						if((olData.omData[j].Onbl!= TIMENULL) && (liRkey != olData.omData[j].Rkey))
						{
							liRkey = olData.omData[j].Rkey;
							if((strcmp(olData.omData[j].Adid,"B")==0) && (strcmp(olData.omData[j].Ftyp,"T")==0))
							{
								if(j>0)
								{
									if(strcmp(CString(olData.omData[j].Rkey),CString(olData.omData[j-1].Rkey))==0)
 										olData.omData[j].Tifa = olData.omData[j-1].Tifa;
									else
										continue;
								}
							}
							olData5.omData.NewAt(li_count,olData.omData[j]);
							li_count++;
						}
					
				}
				CReportTableDlg olReportTableDlg(this, 15, &(olData5.omData), pclInfo);
				olReportTableDlg.DoModal();
			}
		}
	}
	/*
	else if (m_CR_List14.GetCheck() == 1) //	Inventar
	{
		TRACE("CReportSelectDlg::StartTableDlg: List14\n");
		// SHA: Flight statistic according to time frame
		if ((ogCustomer == "SHA") && (ogAppName == "FIPS"))
		{
			int ilTableGantt;

			CTime olMinDate = omLastValues.GetMinTime() ;
			CTime olMaxDate = omLastValues.GetMaxTime() ;

			char pclSelect[128];
			pclSelect[0] = 0;
			CReportSeqDlg olReportDlg(this, GetString(IDS_STRING1644), &olMinDate, &olMaxDate);

			if(olReportDlg.DoModal() == IDOK)
			{
				ShowStatus();
				m_PR_Read.StepIt() ;

				// R�ckgabewerte festhalten
				omLastValues.SetMinTime( olMinDate ) ;
				omLastValues.SetMaxTime( olMaxDate ) ;
				omLastValues.SetSelect( pclSelect ) ;

				blOk = true;

				CString oMinStr = olMinDate.Format( "%Y%m%d000000" );
				CString oMaxStr = olMaxDate.Format( "%Y%m%d235959" );
				MakeUtc( oMinStr, oMaxStr ) ;

				CString olOrgDes;
				CString oMinStrTmp = olMinDate.Format( "%d.%m.%Y" );
				CString oMaxStrTmp = olMaxDate.Format( "%d.%m.%Y" );

				// Abfrage, ob Tabellen- oder Bolkenansicht
				ReportTimeFrameSelctDlg olReportTimeFrameSelctDlg(this);
				if (olReportTimeFrameSelctDlg.DoModal() == IDOK)
				{
					ilTableGantt = olReportTimeFrameSelctDlg.m_ListTableGantt;
				}
				else
				{
					return false;
				}
				ilTableGantt = 1;

				// Portionsweise einlesen
				// 2/99 hbe
				PARTIALREAD olPartial;
				olPartial.Preset();
				CString olWherePlus;
				olWherePlus.Format("%s %s", olOrgDes, olFtyp );
				olPartial.WhereClause += olWherePlus;
				olPartial.StartTime = DBStringToDateTime(oMinStr);
				olPartial.EndTime  = DBStringToDateTime(oMaxStr);
				olPartial.omData = &olData.omData;
 				olPartial.Fields = "RKEY,SKEY,SEAS,FLNO,TTYP,ACT3,DES3,STOA,STOD,ORG3,ORG4,DES3,DES4,FLNO,URNO,PAXI,PAXT";
				
				SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));

				bool blReadRet = ReadPartialWhere( &olPartial, &m_PR_Read ) ;
				if( ! blReadRet )
				{
					//sprintf(pclErrbuf,"\nKein Suchergebnis bei <%s>.\n", pclWhere);AfxMessageBox(pclErrbuf);
					SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
					MessageBox(GetString(IDS_STRING919), GetString(IDS_STRING920), MB_OK);
				}
				else	
				{
					char pclInfo[100];
					oMinStr = olMinDate.Format( "%d.%m.%Y" );
					oMaxStr = olMaxDate.Format( "%d.%m.%Y" );
					sprintf(pclInfo, "%s - %s", oMinStr, oMaxStr);
					olData.omData.Sort(CompareSkey);	// verkn�pfte Fl�ge werden zusammengestellt

					if (ilTableGantt == 0)
					{
						CReportTableDlg olReportTableDlg(this, 13, &(olData.omData), 
							                             pclInfo, pclSelect, NULL);
						olReportTableDlg.DoModal();
					}
					else
					{
						CAdditionalReportTableDlg olAdditionalReportTableDlg(this, 13, 
							                                                 &(olData.omData),
																			 pclInfo, 
																			 pclSelect);
						olAdditionalReportTableDlg.DoModal();
					}
				}

				HideStatus();
			}
		}
		else
		{
			CString olUtcStr;
			CTime olCurrDate = CTime::GetCurrentTime();
			char pclWhere[256];
			char pclFields[100];

			blOk = true;
			//***
			if(bgReportLocal)
				ogBasicData.LocalToUtc(olCurrDate);
			//***
			olUtcStr = olCurrDate.Format( "%Y%m%d%H%M%S" );

			// immer Ftyp operation
			olFtyp = comFtypO ;
			sprintf(pclWhere,
				"WHERE REGN<>' ' AND ((TIFA<='%s' AND DES3='%s' AND ONBL<>' ') OR (TIFD<='%s' AND ORG3='%s' AND OFBL<>' ')) AND RTYP<>'E' %s",
					olUtcStr, pcgHome, olUtcStr, pcgHome, olFtyp );

			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));

			sprintf(pclFields,"URNO,REGN,ACT3,ACT5,PSTA,TIFA,ADID,ONBL");
			if(!olData.ReadSpecial(&olData.omData, pclWhere, pclFields, true, false) )
			{
				SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
				MessageBox(GetString(IDS_STRING919), GetString(IDS_STRING933), MB_OK);
			}
			else	
			{
				char pclInfo[100];
				olUtcStr = olCurrDate.Format( "%d.%m.%Y" );
				sprintf(pclInfo, "%s", olUtcStr);

				//CReportTableDlg olReportTableDlg(this, 15, NULL, pclInfo);
				CReportTableDlg olReportTableDlg(this, 15, &(olData.omData), pclInfo);
				olReportTableDlg.DoModal();
			}
		}
	}
	*/
	else if (m_CR_List16.GetCheck() == 1) //	DAILY / SEASONAL Flug-Plan
	{
		TRACE("CReportSelectDlg::StartTableDlg: List16\n");
		char olWhere[512+1] ;		// Selection : Vorsicht bei �nderungen, wird bzgl. Tagen ausgewertet!!
									//				s. imWherePos1 - 5 !
		char olWherePart[512+1] ;	// f�r Partial Read

		int ilNoFlights = 0;
		bool blReadRet = false ;
		// Presets
		int ilTerminal = omLastValues.GetTerm() ;			 
		// bool blSeason = omLastValues.GetSeason() ;			
		bool blSeason = false ;			
		char pslVerkehr[20] ;		

		strcpy( pslVerkehr, omLastValues.GetVerkehr() ) ;

		// liefert WHERE Bedingung in olWhere zur�ck sowie sonstige Bedingungen
		CDailyPrint *polPrintDlg = new CDailyPrint( this, olWhere, &ilTerminal, pslVerkehr, &blSeason, olWherePart );

		if (polPrintDlg->DoModal() == IDOK)
		{
			// n�chsten Preset festhalten
			omLastValues.SetSeason( blSeason ) ;
			omLastValues.SetTerm( ilTerminal ) ;
			omLastValues.SetVerkehr( CString( pslVerkehr ) ) ;
			
			// Auswahl entsprechend olWhere aus CeDa 
			if(_MIT_TRACE_) TRACE( "olWhere: [%s] \nTerminal [%d] Verkehrsrechte [%s] Season [%d]\n", olWhere, ilTerminal, pslVerkehr, blSeason ) ;
			CString olFlugTag( olWhere ) ;

			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
			omDailyCedaFlightData.ClearAll();

			// mit Statusanzeige 
			ShowStatus() ;

			if(!blSeason) 
			{
				m_PR_Read.StepIt() ;
				blReadRet=omDailyCedaFlightData.ReadFlights( olWhere ) ;
				m_PR_Read.StepIt() ;
			}
			else
			{
				omDailyCedaFlightData.SetDailySeason( true ) ;
				// Portionsweise einlesen
				// 2/99 hbe
				PARTIALREAD olPartial ;
				olPartial.WhereClause = olWherePart ;
				olPartial.StartTime = omLastValues.GetMinTime() ;
				olPartial.EndTime  = omLastValues.GetMaxTime() + CTimeSpan(0,23,59,0) ;
				olPartial.Range = RRWeek ;
				olPartial.Case = 14 ;
				// TRACE("Season [%s] - [%s]\n", olPartial.StartTime.Format("%Y%m%d%H%M%S"), olPartial.EndTime.Format("%Y%m%d%H%M%S") ) ; 
				blReadRet = ReadPartialWhere( &olPartial, &m_PR_Read ) ;
			}

			if( !blReadRet ) 
			{
				SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
				HideStatus() ;
				MessageBox(GetString(IDS_STRING919), GetString(IDS_STRING1400), MB_OK);
			}
			else	
			{
				int ilViewerNo = 14 ;	// Viewer17/18: Daily/Seasonal
				CString olFlugTag( olWhere ) ;
				int ilNoFlights = omDailyCedaFlightData.omData.GetSize() ;
				char olInfo1[50]; 
				char olInfo2[50];

				olFlugTag = olWhere ;
				olFlugTag = olFlugTag.Mid( imWherePos1, 14 ) ;
				CTime olTag = DBStringToDateTime( olFlugTag ) ;

				if( bgReportLocal )	ogBasicData.UtcToLocal( olTag ) ;
				
				// Wochentag bestimmen				
				m_ST_Progress.SetWindowText( GetString( IDS_STRING1418) ) ;
				m_ST_Progress.UpdateWindow() ;
				m_PR_Read.StepIt() ;

				int ilWDay = olTag.GetDayOfWeek() ;
				ilWDay = (ilWDay == 1) ? 7 : ilWDay - 1 ;	// Verkehrstage Mo=1, Di=2 .... So=7
				int ilWDStr = imMontagsID - 1 + ilWDay ;	// IDS_STRING1159 - 1165 = Wochentage
				
				if( ! blSeason )
				{
					if(_MIT_TRACE_) TRACE( "Anzahl Fl�ge [%d] am [%s]\n", ilNoFlights, olFlugTag ) ;
					sprintf( olInfo1,"%s, %s", GetString(ilWDStr),olTag.Format("%d.%m.%Y"));
					sprintf( olInfo2, "%s %d", GetString( IDS_STRING1105 ), ilWDay ) ;
				}
				else
				{

					if(_MIT_TRACE_) TRACE( "Anzahl Fl�ge [%d] im Zeitraum\n", ilNoFlights ) ;
					olFlugTag = olWhere ;
					olFlugTag = olFlugTag.Right( olFlugTag.GetLength() - imWherePos3 ) ;
					olFlugTag = olFlugTag.SpanIncluding("DOA=\'1234567" ) ;
					if( ! olFlugTag.IsEmpty() )
					{
						char clVT = olFlugTag.GetAt( olFlugTag.FindOneOf("1234567")) ;
						ilWDay = atoi( CString(clVT) ) ;
						ilWDStr = imMontagsID - 1 + ilWDay ;	// IDS_STRING1159 - 1165 = Wochentage
					}
					CString olWDS( GetString(ilWDStr) ) ;
					olWDS.MakeUpper() ;
					sprintf( olInfo1,"%s", olWDS );
					olFlugTag = olWhere ;
					olFlugTag = olFlugTag.Mid( imWherePos2, 14 ) ;
					CTime olBisTag = DBStringToDateTime( olFlugTag ) ;

					if( bgReportLocal )	ogBasicData.UtcToLocal( olBisTag ) ;
					
					sprintf( olInfo2," %s %s %s %s", GetString(HD_DSR_AVFA_TIME ), olBisTag.Format("%d.%m.%Y"),
												GetString(HD_DSR_AVTA_TIME),olTag.Format("%d.%m.%Y") ) ;
				}


				// bzgl. Terminal selektieren
				if( ilTerminal != X_TERMNO )
				{
					m_ST_Progress.SetWindowText( GetString( IDS_STRING1420) ) ;
					m_ST_Progress.UpdateWindow() ;
					for( int ilIndex = ilNoFlights-1 ; ilIndex >= 0 ; ilIndex-- )
					{
						CString olTerm ;
						bool blKillen = false ;
						int blAlcRet = ogBCD.GetField( "ALT", "ALC2", omDailyCedaFlightData.omData[ilIndex].Alc2, "TERM", olTerm ) ;
						if( ! blAlcRet )
							blAlcRet = ogBCD.GetField( "ALT", "ALC3", omDailyCedaFlightData.omData[ilIndex].Alc3, "TERM", olTerm ) ;

						if(_MIT_TRACE_) TRACE( "Urno [%ld] [%s / %s] = Term [%s]\n", omDailyCedaFlightData.omData[ilIndex].Urno,
							 omDailyCedaFlightData.omData[ilIndex].Alc2, omDailyCedaFlightData.omData[ilIndex].Alc3, olTerm ) ;
						if(_MIT_TRACE_) TRACE( "Home [%s] Org/Des [%s / %s] \n", pcgHome, omDailyCedaFlightData.omData[ilIndex].Org3,
							 omDailyCedaFlightData.omData[ilIndex].Des3 ) ;

						m_PR_Read.StepIt() ;
						
						switch( ilTerminal )
						{
						case X_TERM1:
							if( olTerm.Find( '1' ) < 0 ) blKillen = true ;
							break ;
						case X_TERM2:
							if( olTerm.Find( '2' ) < 0 ) blKillen = true ;
							break ;
						case X_TERM12:
							if( olTerm.FindOneOf( "12" ) < 0 ) blKillen = true ;
							break ;
						default:
							// case X_TERMNO : keine Auswahl
							;						 
						}

						if( blKillen ) 
						{
							omDailyCedaFlightData.DeleteFlightInternal( omDailyCedaFlightData.omData[ilIndex].Urno ) ;
						}
					
					}
				}	// end if( ilTermial ...

				HideStatus() ;
				
				if( omDailyCedaFlightData.omData.GetSize() > 0 )
				{
				
					// case 14+3 : Viewer 17 = DAILY
					// case 14+4 : Viewer 18 = SEASONAL	
					if( blSeason ) ilViewerNo += 4 ;
					else ilViewerNo += 3 ;
					CReportTableDlg olReportTableDlg(this, ilViewerNo, NULL, olInfo2, olInfo1, pslVerkehr );
					olReportTableDlg.DoModal();
				}
				else
				{
					MessageBox( GetString( IDS_STRING966), GetString( IDS_STRING1400 ), MB_ICONWARNING ) ; 
				}

			}	// end if/else (  ReadFlights(...
		}

		delete polPrintDlg;
		omDailyCedaFlightData.SetDailySeason( false ) ;
	}
	else if (m_CR_List19.GetCheck() == 1) //	DAILY CheckIn-Counter-Plan
	{
		TRACE("CReportSelectDlg::StartTableDlg: List19\n");
		char olWhere[512+1] ;		// Selection : Vorsicht bei �nderungen, wird bzgl. Tagen ausgewertet!!
									//				s. imWherePos1 - 5 !
		int ilNoFlights = 0;
		bool blReadRet = false ;		 
		bool blSeason = false ;		// akt. (9/98) Counterreport immer Daily !		

		// liefert WHERE Bedingung in olWhere zur�ck
		CDailyCcaPrintDlg *polPrintDlg = new CDailyCcaPrintDlg( this, olWhere );

		if (polPrintDlg->DoModal() == IDOK)
		{
 			
			// Auswahl entsprechend olWhere aus CeDa 
			if(_MIT_TRACE_) TRACE( "olWhere: [%s] \n", olWhere ) ;
			CString olFlugTag( olWhere ) ;

			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
			omDailyCedaFlightData.ClearAll();

			// mit Statusanzeige 
			ShowStatus() ;

			if(!blSeason) 
			{
				m_PR_Read.StepIt() ;
				blReadRet=omDailyCedaFlightData.ReadFlights( olWhere ) ;
				m_PR_Read.StepIt() ;
			}
			else
			{
				// Portionsweise einlesen 
				// noch nicht f�r ReadSpecial() vorbereitet !
				blReadRet = false ;
				VERIFY( blReadRet ) ;
			}

			if( !blReadRet ) 
			{
				SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
				HideStatus() ;
				
				if( _MIT_TRACE_ )
				{
					sprintf(pclErrbuf,"\nKein Suchergebnis bei <%s>.\n", olWhere);
					AfxMessageBox(pclErrbuf);
				}
				MessageBox(GetString(IDS_STRING919), GetString(IDS_STRING1461), MB_OK);
			}
			else	
			{
				int ilViewerNo = 19 ;	// Viewer19: Daily Counterplanung
				CString olFlugTag( olWhere ) ;
				int ilNoFlights = omDailyCedaFlightData.omData.GetSize() ;
				char olInfo1[50]; 
				char olInfo2[50];
				char olInfoList[50];	// kommagetrennt Liste mit WHERE Infos

				olFlugTag = olFlugTag.Mid( imWherePos2, 14 ) ;
				sprintf( olInfoList,"%s,", olFlugTag ) ;	// Startzeitpunkt
				olFlugTag = olWhere ;
				olFlugTag = olFlugTag.Mid( imWherePos1, 14 ) ;
				strcat( olInfoList, olFlugTag ) ;	// Endzeitpunkt
				CTime olTag = DBStringToDateTime( olFlugTag ) ;

				if( bgReportLocal )	ogBasicData.UtcToLocal( olTag ) ;
				
				// Wochentag bestimmen				
				m_ST_Progress.SetWindowText( GetString( IDS_STRING1418) ) ;
				m_ST_Progress.UpdateWindow() ;
				m_PR_Read.StepIt() ;

				int ilWDay = olTag.GetDayOfWeek() ;
				ilWDay = (ilWDay == 1) ? 7 : ilWDay - 1 ;	// Verkehrstage Mo=1, Di=2 .... So=7
				int ilWDStr = imMontagsID - 1 + ilWDay ;	// IDS_STRING1159 - 1165 = Wochentage
				
				if( ! blSeason )
				{
					if(_MIT_TRACE_) TRACE( "Anzahl Fl�ge [%d] am [%s]\n", ilNoFlights, olFlugTag ) ;
					sprintf( olInfo1,"%s, %s", GetString(ilWDStr),olTag.Format("%d.%m.%Y"));
				}
				else
				{
					// akt. immer daily !
					ASSERT( false ) ;
				}

 				strcat( olInfoList, ",0" );
				strcpy( olInfo2, " " ) ;
 
				HideStatus() ;
				
				if( omDailyCedaFlightData.omData.GetSize() > 0 )
				{
					// olInfoList: kommagetrennt Liste mit Daten zur Where-Clause: Terminal,Starttag,Endtag
					CReportTableDlg olReportTableDlg(this, ilViewerNo, NULL, olInfo2, olInfo1, olInfoList );
					olReportTableDlg.DoModal();
				}
				else
				{
					MessageBox( GetString( IDS_STRING966), GetString( IDS_STRING1461 ), MB_ICONWARNING ) ; 
				}

			}	// end if/else (  ReadFlights(...
		}

		delete polPrintDlg;
	}
	else if (m_CR_List15.GetCheck() == 1)
	{
		TRACE("CReportSelectDlg::StartTableDlg: List15\n");
		CTime olMinDate = omLastValues.GetMinTime() ;
		CTime olMaxDate = omLastValues.GetMaxTime() ;

		// SHA: Check-in counter using rate failure rate stastic according to time
		if ((ogCustomer == "SHA") && (ogAppName == "FIPS"))
		{
			char pclSelect[256];
			pclSelect[0] = 0;
			ReportSeq1 olReportDlg(this, GetString(IDS_STRING1750), &olMinDate, &olMaxDate);

			if(olReportDlg.DoModal() == IDOK)
			{
				omLastValues.SetMinTime( olMinDate ) ;
				omLastValues.SetMaxTime( olMaxDate ) ;
				omLastValues.SetSelect( pclSelect ) ;

				blOk = true;

				CString oMinStr = olMinDate.Format( "%Y%m%d000000" );
				CString oMaxStr = olMaxDate.Format( "%Y%m%d235959" );

				MakeUtc( oMinStr, oMaxStr ) ;

				//CString olOrgDes;
				//CString oMinStrTmp = olMinDate.Format( "%d.%m.%Y" );
				//CString oMaxStrTmp = olMaxDate.Format( "%d.%m.%Y" );

				CTime StartTime = DBStringToDateTime(oMinStr);
				CTime EndTime  = DBStringToDateTime(oMaxStr);
/*				
				//sprintf(pclErrbuf,"\nKein Suchergebnis bei <%s>.\n", pclWhere);AfxMessageBox(pclErrbuf);
				MessageBox(GetString(IDS_STRING919), GetString(IDS_STRING920), MB_OK);
*/

				char pclInfo[100];
				oMinStr = olMinDate.Format( "%d.%m.%Y" );
				oMaxStr = olMaxDate.Format( "%d.%m.%Y" );
				sprintf(pclInfo, "%s - %s", oMinStr, oMaxStr);

				strcpy(pclSelect,"CCA,CKBA,CKEA");

																	 
																	 
				CAdditionalReportTableDlg olAdditionalReportTableDlg(this, 16, NULL,
																	 pclInfo, pclSelect, NULL,
																	 olMinDate, olMaxDate);

				olAdditionalReportTableDlg.DoModal();

				HideStatus();
			}
		}
	}
	else if (m_CR_List18.GetCheck() == 1)
	{
		TRACE("CReportSelectDlg::StartTableDlg: List18\n");
		CTime olMinDate = omLastValues.GetMinTime() ;
		CTime olMaxDate = omLastValues.GetMaxTime() ;

		// SHA: Spot using rate failure rate statistic according to time
		if ((ogCustomer == "SHA") && (ogAppName == "FIPS"))
		{
			char pclSelect[256];
			pclSelect[0] = 0;
			ReportSeq1 olReportDlg(this, GetString(IDS_STRING1751), &olMinDate, &olMaxDate);

			if(olReportDlg.DoModal() == IDOK)
			{
				omLastValues.SetMinTime( olMinDate ) ;
				omLastValues.SetMaxTime( olMaxDate ) ;
				omLastValues.SetSelect( pclSelect ) ;

				blOk = true;

				CString oMinStr = olMinDate.Format( "%Y%m%d000000" );
				CString oMaxStr = olMaxDate.Format( "%Y%m%d235959" );
				MakeUtc( oMinStr, oMaxStr ) ;

				CString olOrgDes;
				CString oMinStrTmp = olMinDate.Format( "%d.%m.%Y" );
				CString oMaxStrTmp = olMaxDate.Format( "%d.%m.%Y" );

				CTime StartTime = DBStringToDateTime(oMinStr);
				CTime EndTime  = DBStringToDateTime(oMaxStr);
/*				
				muss in die klasse mit der ogBCD-abfrage
				//sprintf(pclErrbuf,"\nKein Suchergebnis bei <%s>.\n", pclWhere);AfxMessageBox(pclErrbuf);
				MessageBox(GetString(IDS_STRING919), GetString(IDS_STRING920), MB_OK);
*/

				char pclInfo[100];
				oMinStr = olMinDate.Format( "%d.%m.%Y" );
				oMaxStr = olMaxDate.Format( "%d.%m.%Y" );
				sprintf(pclInfo, "%s - %s", oMinStr, oMaxStr);

				strcpy(pclSelect,"PST,VAFR,VATO,NAFR,NATO");

				CAdditionalReportTableDlg olAdditionalReportTableDlg(this, 17, NULL,
																	 pclInfo, pclSelect, NULL,
																	 olMinDate, olMaxDate);
				olAdditionalReportTableDlg.DoModal();

				HideStatus();
			}
		}
	}
	else if (m_CR_List20.GetCheck() == 1)
	{
		TRACE("CReportSelectDlg::StartTableDlg: List20\n");
 		CTime olDate;
		CTime olMinDate = omLastValues.GetMinTime() ;
		CTime olMaxDate = omLastValues.GetMaxTime() ;

		// Daily flight log
		if ((ogCustomer == "SHA") && (ogAppName == "FIPS"))
		{
 
			CString olTableName;
			m_CR_List20.GetWindowText(olTableName);
			olTableName.Remove('&');
			// Ask for the time span to display
			CReportSeqDateTimeDlg olReportDlg(this, olTableName, &olMinDate, &olMaxDate);
			if(olReportDlg.DoModal() == IDOK)
			{
				
				ShowStatus();
				m_PR_Read.StepIt() ;

				// R�ckgabewerte festhalten
				omLastValues.SetMinTime( olMinDate ) ;
				omLastValues.SetMaxTime( olMaxDate ) ;
	
				blOk = true;

				CString olMinStr = olMinDate.Format( "%Y%m%d%H%M%S" );
				CString olMaxStr = olMaxDate.Format( "%Y%m%d%H%M%S" );
				MakeUtc( olMinStr, olMaxStr ) ;
 
				PARTIALREAD olPartial;
				olPartial.Preset();
				CString olWherePlus;
				olWherePlus.Format(" %s", comFtyp_SGT);
				olPartial.WhereClause += olWherePlus;

 				olPartial.StartTime = DBStringToDateTime(olMinStr);
				olPartial.EndTime  = DBStringToDateTime(olMaxStr);
				olPartial.omData = &olData.omData;	
				olPartial.Fields = "ACT3,AIRB,BLT1,DCD1,DCD2,DES3,DTD1,DTD2,ETAI,ETDI,FLNO,FTYP,GTA1,GTD1,LAND,OFBL,ONBL,ORG3,PAXT,PSTA,PSTD,REGN,REM1,RKEY,SLOT,STOA,STOD,TIFA,TIFD,TMOA,TTYP,URNO,VIA3,WRO1";
  				
				SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
				// Read data
				bool blReadRet = ReadPartialWhere( &olPartial, &m_PR_Read ) ;
				if( ! blReadRet )
				{
					//sprintf(pclErrbuf,"\nKein Suchergebnis bei <%s>.\n", pclWhere);AfxMessageBox(pclErrbuf);
					SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
					MessageBox(GetString(IDS_STRING919), GetString(IDS_STRING920), MB_OK);
				}
				else	
				{
					char pclInfo[100];
					olMinStr = olMinDate.Format( "%a, %d.%m.%Y %H:%M" );
					olMaxStr = olMaxDate.Format( "%a, %d.%m.%Y %H:%M" );
					sprintf(pclInfo, "%s - %s", olMinStr, olMaxStr);
					olData.omData.Sort(CompareSkey);	// verkn�pfte Fl�ge werden zusammengestellt

 					CReportTableDlg olReportTableDlg(this, 26, &(olData.omData), pclInfo, NULL, NULL, 0, NULL, blUseLoatab);
					olReportTableDlg.DoModal();
				}
 				HideStatus() ;
			}
		}
	}
	else if (m_CR_List21.GetCheck() == 1) //	Statistic of Overnight Flights
	{
		OvernightTableDlg olDlg (this);
//		OvernightTableDlg olDlg = new OvernightTableDlg(this);
//		olDlg.ShowAnsicht();
//		olDlg.SetWindowPos(&wndTop, 0,0,0,0, SWP_SHOWWINDOW | SWP_NOSIZE | SWP_NOMOVE );
		olDlg.DoModal();

		int k = 1;


/*

		TRACE("CReportSelectDlg::StartTableDlg: List21\n");
		CTime olMinDate = omLastValues.GetMinTime() ;
		CTime olMaxDate = omLastValues.GetMaxTime() ;
//		char pclSelect[128];

		// SHA: statistics according flight cancelled (detailed)
		if ((ogCustomer == "SHA") && (ogAppName == "FIPS"))
		{
			char pclSelect[128];
			pclSelect[0] = 0;
			CReportSeqDlg olReportDlg(this, GetString(IDS_STRING1770), &olMinDate, &olMaxDate);

			if(olReportDlg.DoModal() == IDOK)
			{
				ShowStatus() ;
				m_PR_Read.StepIt() ;

				// R�ckgabewerte festhalten
				omLastValues.SetMinTime( olMinDate ) ;
				omLastValues.SetMaxTime( olMaxDate ) ;
				omLastValues.SetSelect( pclSelect ) ;

				blOk = true;

				CString oMinStr = olMinDate.Format( "%Y%m%d000000" );
				CString oMaxStr = olMaxDate.Format( "%Y%m%d235959" );
				MakeUtc( oMinStr, oMaxStr ) ;

//				CString olX = " AND FTYP<>'X'";
				CString olX = " AND FTYP NOT IN ('G','T','X')";

				// Portionsweise einlesen
				// 2/99 hbe
				PARTIALREAD olPartial ;
				CString olWherePlus ;
				olPartial.Preset();
				olWherePlus.Format("WHERE (STOA BETWEEN '%%s' AND '%%s' AND DES3='%s' ) ",pcgHome);
				olWherePlus += olX;

				olWherePlus += "[ROTATIONS]";		


				olPartial.WhereClause = olWherePlus;
				olPartial.StartTime = DBStringToDateTime(oMinStr);
				olPartial.EndTime  = DBStringToDateTime(oMaxStr);
				olPartial.omData = &olData.omData;
// 				olPartial.Fields = "RKEY";
				olPartial.Fields = "RKEY,SKEY,STEV,FLNO,TIFA,TIFD,ORG3,VIA3,ACT3,STOA,STOD,DES3,ALC2,ALC3,SEAS,GTA1,PSTA,TTYP,GTD1,PSTD,WRO1,URNO,VIAL,CKIT,CKIF";
				
				SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));

				bool blReadRet = ReadPartialWhere( &olPartial, &m_PR_Read ) ;
				if( ! blReadRet )
				{
					//sprintf(pclErrbuf,"\nKein Suchergebnis bei <%s>.\n", pclWhere);AfxMessageBox(pclErrbuf);
					SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
					MessageBox(GetString(IDS_STRING919), GetString(IDS_STRING920), MB_OK);
				}
				else	
				{
						char pclInfo[100];
						oMinStr = olMinDate.Format( "%d.%m.%Y" );
						oMaxStr = olMaxDate.Format( "%d.%m.%Y" );
						sprintf(pclInfo, "%s - %s", oMinStr, oMaxStr);
//						olData2.omData.Sort(CompareSkey);	// verkn�pfte Fl�ge werden zusammengestellt
						olData.omData.Sort(CompareSkey);	// verkn�pfte Fl�ge werden zusammengestellt

//						CReportTableDlg olReportTableDlg(this, 20, &(olData2.omData), pclInfo, pclSelect,NULL,0,&olData2);
						CReportTableDlg olReportTableDlg(this, 20, &(olData.omData), pclInfo, pclSelect,NULL,0,&olData);
						olReportTableDlg.DoModal();
					//}

				}

				HideStatus() ;
			}
		}
*/

	}
	else if (m_CR_List22.GetCheck() == 1) //	Statistic of DomIntMixed Flights
	{
		TRACE("CReportSelectDlg::StartTableDlg: List22\n");
		CTime olMinDate = omLastValues.GetMinTime() ;
		CTime olMaxDate = omLastValues.GetMaxTime() ;
//		char pclSelect[128];

		// SHA: statistics according flight cancelled (detailed)
		if ((ogCustomer == "SHA") && (ogAppName == "FIPS"))
		{
			char pclSelect[128];
			CString olStr1, olStr2,olStr3;
			olStr3 = "Handling Agents";
			bool blChoice = true;
			
			olStr1 = GetString(IDS_STRING1781);
			olStr2 = GetString(IDS_STRING1046);
			olStr3 = GetString(IDS_STRING1797);
			
			CReportSeqChoiceFieldDlg olReportDlg(this, olStr1, olStr2,olStr3, &olMinDate, &olMaxDate, pclSelect,&blChoice, "A|#A|#A|#", 2, 3);

			pclSelect[0] = 0;
			
			if(olReportDlg.DoModal() == IDOK)
			{
				ShowStatus() ;
				m_PR_Read.StepIt() ;

				// R�ckgabewerte festhalten
				omLastValues.SetMinTime( olMinDate ) ;
				omLastValues.SetMaxTime( olMaxDate ) ;

				blOk = true;

				CString oMinStr = olMinDate.Format( "%Y%m%d000000" );
				CString oMaxStr = olMaxDate.Format( "%Y%m%d235959" );
				MakeUtc( oMinStr, oMaxStr ) ;

				CString olSelectText("") ;

				if(blChoice)
				{
					if(strlen(pclSelect) == 2)
					{
						olSelectText.Format(" AND ALC2='%s'", pclSelect ) ;
					}
					else if(strlen(pclSelect) == 3)
					{
						olSelectText.Format(" AND ALC3='%s'", pclSelect ) ;
					}
				}
				else
				{
					CString olHagHsna = pclSelect;
					CString olAlc3Tmp = "";

					if (!olHagHsna.IsEmpty())
					{
						//recordset von allen Hsna in Haitab
						CCSPtrArray<RecordSet>rlHais;
						ogBCD.GetRecords("HAI", "HSNA", olHagHsna, &rlHais);

						for(int ilCount = 0; ilCount < rlHais.GetSize(); ilCount++)
						{
							CString olAltu = rlHais[ilCount].Values[ogBCD.GetFieldIndex("HAI","ALTU")];

							if (!olAltu.IsEmpty())
							{
								CString olAlc3 = "";
								if (ogBCD.GetField("ALT", "URNO", olAltu, "ALC3", olAlc3))
								{
									if (!olAlc3.IsEmpty())
									{
										if(olAlc3Tmp.IsEmpty())					
											olAlc3Tmp += CString("'") + olAlc3 + CString("'");
										else
											olAlc3Tmp += CString(",'") + olAlc3 + CString("'");
									}
								}
							}
						}
					}
					if (!olAlc3Tmp.IsEmpty())	
						olSelectText = "AND ALC3 IN (" + olAlc3Tmp + CString(") ");
					else
					{
						SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
						MessageBox(GetString(IDS_STRING919), GetString(IDS_STRING920), MB_OK);
						HideStatus() ;
						return false;
					}
					
/*
					olSelectText.Format(" WHERE HSNA='%s'", pclSelect ) ;
					//ogBCD.SetObject("HAI");
					if(CString(pclSelect).IsEmpty())
						ogBCD.Read("HAI");
					else
						ogBCD.Read("HAI",olSelectText);

					CString olTmpText;
					CString olKeyList;
					for(int i = 0; i < ogBCD.GetDataCount("HAI"); i++)
					{
						olTmpText = ogBCD.GetField("HAI", i, "FLNU");
						if(!olTmpText.IsEmpty())
						{						
							olKeyList   += olTmpText + CString(",");
						}
					}
					olKeyList = olKeyList.Left(olKeyList.GetLength() -1 );

					olSelectText = CString("AND URNO IN (") + olKeyList + CString(")");
*/

				}

				


				// Portionsweise einlesen
				// 2/99 hbe
				PARTIALREAD olPartial ;
				CString olWherePlus ;
				olPartial.Preset();
				
				olWherePlus = olSelectText;
				olPartial.WhereClause += olWherePlus;
				olPartial.StartTime = DBStringToDateTime(oMinStr);
				olPartial.EndTime  = DBStringToDateTime(oMaxStr);
				olPartial.omData = &olData.omData;
 				olPartial.Fields = "RKEY,SKEY,STEV,FLNO,TIFA,TIFD,ORG3,VIA3,ACT3,STOA,STOD,DES3,ALC2,ALC3,SEAS,PSTA,TTYP,PSTD,WRO1,URNO,FLTI,PAXI,PAXF";
			
				SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));

				bool blReadRet = ReadPartialWhere( &olPartial, &m_PR_Read ) ;
				if( ! blReadRet )
				{
					//sprintf(pclErrbuf,"\nKein Suchergebnis bei <%s>.\n", pclWhere);AfxMessageBox(pclErrbuf);
					SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
					MessageBox(GetString(IDS_STRING919), GetString(IDS_STRING920), MB_OK);
				}
				else	
				{
					
					char pclInfo[100];
					oMinStr = olMinDate.Format( "%d.%m.%Y" );
					oMaxStr = olMaxDate.Format( "%d.%m.%Y" );
					sprintf(pclInfo, "%s - %s", oMinStr, oMaxStr);
					olData.omData.Sort(CompareSkey);	// verkn�pfte Fl�ge werden zusammengestellt

					CReportTableDlg olReportTableDlg(this, 21, &(olData.omData), pclInfo, pclSelect,NULL,blChoice?0:1);
					olReportTableDlg.DoModal();
					

				}

				HideStatus() ;
			}
		}
		else
		{
		}
	}
	else if (m_CR_List23.GetCheck() == 1) //	Statistic of DomIntMixed Flights
	{
		TRACE("CReportSelectDlg::StartTableDlg: List23\n");
		CTime olMinDate = omLastValues.GetMinTime() ;
		CTime olMaxDate = omLastValues.GetMaxTime() ;
//		char pclSelect[128];

		// SHA: statistics according flight cancelled (detailed)
		if ((ogCustomer == "SHA") && (ogAppName == "FIPS"))
		{
			char pclSelect[128];
			CString olStr1, olStr2,olStr3;
			olStr3 = "Handling Agents";
			bool blChoice = true;
			
			olStr1 = GetString(IDS_STRING1889);
			olStr2 = GetString(IDS_STRING1046);
			olStr3 = GetString(IDS_STRING1797);
			
			CReportSeqChoiceFieldDlg olReportDlg(this, olStr1, olStr2,olStr3, &olMinDate, &olMaxDate, pclSelect,&blChoice, "A|#A|#A|#", 2, 3);

			pclSelect[0] = 0;
			
			if(olReportDlg.DoModal() == IDOK)
			{
				ShowStatus() ;
				m_PR_Read.StepIt() ;

				// R�ckgabewerte festhalten
				omLastValues.SetMinTime( olMinDate ) ;
				omLastValues.SetMaxTime( olMaxDate ) ;

				blOk = true;

				CString oMinStr = olMinDate.Format( "%Y%m%d000000" );
				CString oMaxStr = olMaxDate.Format( "%Y%m%d235959" );
				MakeUtc( oMinStr, oMaxStr ) ;

				CString olSelectText("") ;

				if(blChoice)
				{
					if(strlen(pclSelect) == 2)
					{
						olSelectText.Format(" AND ALC2='%s'", pclSelect ) ;
					}
					else if(strlen(pclSelect) == 3)
					{
						olSelectText.Format(" AND ALC3='%s'", pclSelect ) ;
					}
				}
				else
				{
					CString olHagHsna = pclSelect;
					CString olAlc3Tmp = "";

					if (!olHagHsna.IsEmpty())
					{
						//recordset von allen Hsna in Haitab
						CCSPtrArray<RecordSet>rlHais;
						ogBCD.GetRecords("HAI", "HSNA", olHagHsna, &rlHais);

						for(int ilCount = 0; ilCount < rlHais.GetSize(); ilCount++)
						{
							CString olAltu = rlHais[ilCount].Values[ogBCD.GetFieldIndex("HAI","ALTU")];

							if (!olAltu.IsEmpty())
							{
								CString olAlc3 = "";
								if (ogBCD.GetField("ALT", "URNO", olAltu, "ALC3", olAlc3))
								{
									if (!olAlc3.IsEmpty())
									{
										if(olAlc3Tmp.IsEmpty())					
											olAlc3Tmp += CString("'") + olAlc3 + CString("'");
										else
											olAlc3Tmp += CString(",'") + olAlc3 + CString("'");
									}
								}
							}
						}
					}
					if (!olAlc3Tmp.IsEmpty())	
						olSelectText = "AND ALC3 IN (" + olAlc3Tmp + CString(") ");
					else
					{
						SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
						MessageBox(GetString(IDS_STRING919), GetString(IDS_STRING920), MB_OK);
						HideStatus() ;
						return false;
					}
/*
					
					olSelectText.Format(" WHERE HSNA='%s'", pclSelect ) ;
					ogBCD.SetObject("HAI");
					ogBCD.Read("HAI",olSelectText);
					CString olTmpText;
					CString olKeyList;
					for(int i = 0; i < ogBCD.GetDataCount("HAI"); i++)
					{
						olTmpText = ogBCD.GetField("HAI", i, "FLNU");
						if(!olTmpText.IsEmpty())
						{						
							olKeyList   += olTmpText + CString(",");
						}
					}
					olKeyList = olKeyList.Left(olKeyList.GetLength() -1 );

					olSelectText = CString("AND URNO IN (") + olKeyList + CString(")");
*/

				}

				


				// Portionsweise einlesen
				// 2/99 hbe
				PARTIALREAD olPartial ;
				CString olWherePlus ;
				olPartial.Preset();
				
				olWherePlus = olSelectText;
				olPartial.WhereClause += olWherePlus;
				olPartial.StartTime = DBStringToDateTime(oMinStr);
				olPartial.EndTime  = DBStringToDateTime(oMaxStr);
				olPartial.omData = &olData.omData;
 				olPartial.Fields = "RKEY,SKEY,STEV,FLNO,TIFA,TIFD,ORG3,VIA3,ACT3,STOA,STOD,DES3,ALC2,ALC3,SEAS,PSTA,TTYP,PSTD,WRO1,URNO,FLTI,PAXI,PAXF";
			
				SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));

				bool blReadRet = ReadPartialWhere( &olPartial, &m_PR_Read ) ;
				if( ! blReadRet )
				{
					//sprintf(pclErrbuf,"\nKein Suchergebnis bei <%s>.\n", pclWhere);AfxMessageBox(pclErrbuf);
					SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
					MessageBox(GetString(IDS_STRING919), GetString(IDS_STRING920), MB_OK);
				}
				else	
				{
					
					char pclInfo[100];
					oMinStr = olMinDate.Format( "%d.%m.%Y" );
					oMaxStr = olMaxDate.Format( "%d.%m.%Y" );
					sprintf(pclInfo, "%s - %s", oMinStr, oMaxStr);
					olData.omData.Sort(CompareSkey);	// verkn�pfte Fl�ge werden zusammengestellt

					CReportTableDlg olReportTableDlg(this, 22, &(olData.omData), pclInfo, pclSelect,NULL,blChoice?0:1);
					olReportTableDlg.DoModal();
					

				}

				HideStatus() ;
			}
		}
		else
		{
		}
	}
	else if (m_CR_List24.GetCheck() == 1)
	{
		TRACE("CReportSelectDlg::StartTableDlg: List24\n");
 		CTime olDate;
		CTime olMinDate = omLastValues.GetMinTime() ;
		//CTime olMaxDate = omLastValues.GetMaxTime() ;

		// SHA: Statistic according to arr/dep load & pax each day
		if ((ogCustomer == "SHA") && (ogAppName == "FIPS"))
		{
			char pclSelect[128];
			pclSelect[0] = 0;
			ReportSeq1 olReportDlg(this, GetString(IDS_STRING1876), &olMinDate);

			//CReportSeqDlg olReportDlg(this, GetString(IDS_STRING1630), &olMinDate, &olMaxDate);

			if(olReportDlg.DoModal() == IDOK)
			{
				TRACE("CReportSelectDlg::IDOK\n");
				ShowStatus() ;
				m_PR_Read.StepIt() ;

				// R�ckgabewerte festhalten
				omLastValues.SetMinTime( olMinDate ) ;
				omLastValues.SetSelect( pclSelect ) ;

				blOk = true;

				CString oMinStr = olMinDate.Format( "%Y%m%d000000" );
				CString oMaxStr = olMinDate.Format( "%Y%m%d235959" );
				MakeUtc( oMinStr, oMaxStr ) ;
 				
 				// Abfrage, ob Arrival oder Departure
				ReportArrDepSelctDlg olReportArrDepSelctDlg(this);
				if (olReportArrDepSelctDlg.DoModal() == IDOK)
				{
					imArrDep = olReportArrDepSelctDlg.m_ListArrDep;
				}
				else
				{
					return false;
				}

				// Portionsweise einlesen
				// 2/99 hbe
				PARTIALREAD olPartial;
				olPartial.Preset();
				CString olWherePlus;
				olWherePlus.Format(" %s", olFtyp);
				olPartial.WhereClause += olWherePlus;
				olPartial.StartTime = DBStringToDateTime(oMinStr);
				olPartial.EndTime  = DBStringToDateTime(oMaxStr);
				olPartial.omData = &olData.omData;
 
				olPartial.Fields = "RKEY,SKEY,SEAS,TTYP,ACT3,ACT5,STOA,STOD,ORG3,ORG4,DES3,DES4,FLNO,AIRB,LAND,ADID,ALC3,ALC2,FTYP,REGN,GTA1,GTD1,BLT1,PSTD,PSTA,CKIT,URNO,PAX1,PAX2,PAX3,PAXF,PAXI,PAXT,MAIL,CGOT,BAGN,BAGW";
				
				SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));

				bool blReadRet = ReadPartialWhere( &olPartial, &m_PR_Read ) ;
				if( ! blReadRet )
				{
					TRACE("CReportSelectDlg::No data found\n");
					//sprintf(pclErrbuf,"\nKein Suchergebnis bei <%s>.\n", pclWhere);AfxMessageBox(pclErrbuf);
					SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
					MessageBox(GetString(IDS_STRING919), GetString(IDS_STRING920), MB_OK);
				}
				else	
				{
					char pclInfo[100];
					oMinStr = olMinDate.Format( "%d.%m.%Y" );
					sprintf(pclInfo, "%s", oMinStr);
					olData.omData.Sort(CompareSkey);	// verkn�pfte Fl�ge werden zusammengestellt

 					CReportTableDlg olReportTableDlg(this, 23, &(olData.omData), pclInfo, 
						                             pclSelect, NULL, imArrDep, NULL, blUseLoatab);
					olReportTableDlg.DoModal();
				}

				HideStatus() ;
			}
		}
	}
	else if (m_CR_List25.GetCheck() == 1)
	{
		TRACE("CReportSelectDlg::StartTableDlg: List25\n");
 		CTime olDate;
		CTime olMinDate = omLastValues.GetMinTime() ;
		CTime olMaxDate = omLastValues.GetMaxTime() ;

		// SHA: Statistic according to GPU-Usage
		if ((ogCustomer == "SHA") && (ogAppName == "FIPS"))
		{
			char pclSelect[128];
			pclSelect[0] = 0;
			
			CReportSeqFieldDlg olReportDlg(this, GetString(IDS_STRING1888), GetString(IDS_STRING1517), &olMinDate, &olMaxDate, pclSelect, "A|#A|#A|#", 2, 3);
 
			if(olReportDlg.DoModal() == IDOK)
			{
				
				ShowStatus();
				m_PR_Read.StepIt() ;

				// R�ckgabewerte festhalten
				omLastValues.SetMinTime( olMinDate ) ;
				omLastValues.SetMaxTime( olMaxDate ) ;
				omLastValues.SetSelect( pclSelect ) ;
	
				blOk = true;

				CString oMinStr = olMinDate.Format( "%Y%m%d000000" );
				CString oMaxStr = olMaxDate.Format( "%Y%m%d235959" );
				MakeUtc( oMinStr, oMaxStr ) ;

				CString olAlc("") ;
				if(strlen(pclSelect) == 2)
				{
					olAlc.Format(" AND ALC2='%s'", pclSelect ) ;
				}
				else if(strlen(pclSelect) == 3)
				{
					olAlc.Format(" AND ALC3='%s'", pclSelect ) ;
				}
 				else if(strlen(pclSelect) != 0)
				{
					// Airlines nur 2Ltr oder 3Ltr-Code !
					MessageBox("2Ltr/3Ltr-Code only!", GetString(IDS_WARNING), MB_ICONWARNING );
					return false;
				}
 				PARTIALREAD olPartial ;
				olPartial.Preset() ;
				CString olWherePlus ;
				if (strlen(pclSelect) != 0) 
				{
					olWherePlus.Format("%s", olAlc);
					olPartial.WhereClause += olWherePlus;
				}
				olPartial.StartTime = DBStringToDateTime(oMinStr) ;
				olPartial.EndTime  = DBStringToDateTime(oMaxStr) ;
				olPartial.omData = &olData.omData ;	
				olPartial.Fields = "RKEY,ACT3,AIRB,ALC2,ALC3,FLNO,LAND,PSTA,PSTD,REGN,STOA,STOD,DES3,ORG3,URNO";
 				
				SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));

				bool blReadRet = ReadPartialWhere( &olPartial, &m_PR_Read ) ;
				if( ! blReadRet )
				{
					//sprintf(pclErrbuf,"\nKein Suchergebnis bei <%s>.\n", pclWhere);AfxMessageBox(pclErrbuf);
					SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
					MessageBox(GetString(IDS_STRING919), GetString(IDS_STRING920), MB_OK);
				}
				else	
				{
					char pclInfo[100];
					oMinStr = olMinDate.Format( "%d.%m.%Y" );
					oMaxStr = olMaxDate.Format( "%d.%m.%Y" );
					sprintf(pclInfo, "%s - %s", oMinStr, oMaxStr);
					olData.omData.Sort(CompareSkey);	// verkn�pfte Fl�ge werden zusammengestellt

 					CReportTableDlg olReportTableDlg(this, 24, &(olData.omData), pclInfo, pclSelect);
					olReportTableDlg.DoModal();
				}
 				HideStatus() ;
			}
		}
	}
	else if (m_CR_List26.GetCheck() == 1)
	{
		// Baggage belt allocation schedule
		TRACE("CReportSelectDlg::StartTableDlg: List26\n");
 		CTime olMinDate = omLastValues.GetMinTime() ;
 
 		if ((ogCustomer == "SHA") && (ogAppName == "FIPS"))
		{
 			CString olTableName;
			m_CR_List26.GetWindowText(olTableName);

			// Date selection dialog
			ReportSeq1 olReportDlg(this, olTableName, &olMinDate);
			if(olReportDlg.DoModal() == IDOK)
			{
 				ShowStatus() ;
				m_PR_Read.StepIt() ;

				// R�ckgabewerte festhalten
				omLastValues.SetMinTime( olMinDate ) ;
 
				blOk = true;

				// Calculate timespan
				CString olMinStr = olMinDate.Format( "%Y%m%d000000" );
				CString olMaxStr = olMinDate.Format( "%Y%m%d235959" );
				MakeUtc( olMinStr, olMaxStr ) ;
 				
 				
				SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));

				// Load data from Ceda-Database
				CString olWhere;
				olWhere.Format("WHERE (STOA BETWEEN '%s' AND '%s' AND DES3='%s' %s)",
					olMinStr, olMaxStr, pcgHome, olFtyp);
 				bool blReadRet = omDailyCedaFlightData.ReadSpecial(&olData.omData,
						(LPCTSTR) olWhere, "URNO,FLNO,ORG3,STOA,ETAI,BLT1,BLT2,DES3", 
						false, true, RotationDlgCedaFlightData::DefSort);
				
				if( ! blReadRet )
				{
					TRACE("CReportSelectDlg::No data found\n");
					//sprintf(pclErrbuf,"\nKein Suchergebnis bei <%s>.\n", pclWhere);AfxMessageBox(pclErrbuf);
					SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
					MessageBox(GetString(IDS_STRING919), GetString(IDS_STRING920), MB_OK);
				}
				else	
				{
					char pclInfo[100];
					olMinStr = olMinDate.Format( "%d.%m.%Y" );
					sprintf(pclInfo, "%s", olMinStr);
					// Show report dialog	
 					CReportTableDlg olReportTableDlg(this, 25, &(olData.omData), pclInfo);
					olReportTableDlg.DoModal();
				}

				HideStatus() ;
			}
		}
	}
	else if (m_CR_List27.GetCheck() == 1)
	{
		TRACE("CReportSelectDlg::StartTableDlg: List27\n");
 		CTime olDate;
		CTime olMinDate = omLastValues.GetMinTime() ;
		CTime olMaxDate = omLastValues.GetMaxTime() ;

		// Delay codes
		if ((ogCustomer == "SHA") && (ogAppName == "FIPS"))
		{
 
			CString olTableName;
			m_CR_List27.GetWindowText(olTableName);
			olTableName.Remove('&');
			// Ask for the time span to display
			ReportSeqField3DateTimeDlg olReportDlg(this, olTableName, &olMinDate, &olMaxDate,
				GetString(IDS_STRING1517), REPSEQFIELD3_AIRLINECODE, 
				GetString(IDS_STRING1610), REPSEQFIELD3_DELAYCODE, 
				GetString(IDS_STRING1983), REPSEQFIELD3_DELAYTIME);
			if(olReportDlg.DoModal() == IDOK)
			{
				
				ShowStatus();
				m_PR_Read.StepIt() ;

				// R�ckgabewerte festhalten
				omLastValues.SetMinTime( olMinDate ) ;
				omLastValues.SetMaxTime( olMaxDate ) ;
	
				blOk = true;

				CString olMinStr = olMinDate.Format( "%Y%m%d%H%M%S" );
				CString olMaxStr = olMaxDate.Format( "%Y%m%d%H%M%S" );
				MakeUtc( olMinStr, olMaxStr ) ;
 
				PARTIALREAD olPartial;
				olPartial.Preset();
				CString olWherePlus;
				// specify ftyp
				olWherePlus.Format(" %s", comFtyp_SGT);
				olPartial.WhereClause += olWherePlus;
				// specify selected airline if present
				if (!olReportDlg.m_Alc2.IsEmpty())
				{
					olWherePlus.Format(" AND (ALC2 = '%s')", olReportDlg.m_Alc2);
					olPartial.WhereClause += olWherePlus;
				}
				if (!olReportDlg.m_Alc3.IsEmpty())
				{
					olWherePlus.Format(" AND (ALC3 = '%s')", olReportDlg.m_Alc3);
					olPartial.WhereClause += olWherePlus;
				}
				// specify selected delay code if present
				if (!olReportDlg.m_Field2.IsEmpty())
				{
					olWherePlus.Format(" AND (DCD1 = '%s' OR DCD2 = '%s')", olReportDlg.m_Field2, olReportDlg.m_Field2);
					olPartial.WhereClause += olWherePlus;
				}
				// specify selected delay time if present
				if (!olReportDlg.m_Field3.IsEmpty())
				{
//					olWherePlus.Format(" AND (DTD1 >= '%s' OR DTD2 >= '%s')", olReportDlg.m_Field3, olReportDlg.m_Field3);
//					olPartial.WhereClause += olWherePlus;
				}
				// specify selected time interval
 				olPartial.StartTime = DBStringToDateTime(olMinStr);
				olPartial.EndTime  = DBStringToDateTime(olMaxStr);
				olPartial.omData = &olData.omData;	
				olPartial.Fields = "ACT3,DCD1,DCD2,DES3,DTD1,DTD2,FLNO,OFBL,ONBL,ORG3,STOA,STOD,URNO";
  				
				SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
				// Read data
				bool blReadRet = ReadPartialWhere( &olPartial, &m_PR_Read ) ;
 				HideStatus() ;
				if( ! blReadRet )
				{
					//sprintf(pclErrbuf,"\nKein Suchergebnis bei <%s>.\n", pclWhere);AfxMessageBox(pclErrbuf);
					SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
					MessageBox(GetString(IDS_STRING919), GetString(IDS_STRING920), MB_OK);
				}
				else	
				{
					char pclInfo[100];
					olMinStr = olMinDate.Format( "%a, %d.%m.%Y %H:%M" ); 
					olMaxStr = olMaxDate.Format( "%a, %d.%m.%Y %H:%M" );
					sprintf(pclInfo, "%s - %s", olMinStr, olMaxStr);
					olData.omData.Sort(CompareSkey);	// verkn�pfte Fl�ge werden zusammengestellt

					int ipField3 = 1;
					if (!olReportDlg.m_Field3.IsEmpty())
						ipField3 = atoi(olReportDlg.m_Field3);

					char opField1[128];
					char opField2[128];
					sprintf(opField1, olReportDlg.m_Field1);
					sprintf(opField2, olReportDlg.m_Field2);


 					CReportTableDlg olReportTableDlg(this, 27, &(olData.omData), pclInfo, opField1, opField2, ipField3);
					olReportTableDlg.DoModal();
				}
 				HideStatus() ;
			}
		}
	}

	
	else
	{
 		MessageBox( GetString( IDS_STRING1947 ), GetString( IDS_STRING1948 ), MB_ICONWARNING ) ;
	} // 	if (m_CR_List1.GetCheck() == 1) 

	return true;
}



/////////////////////////////////////////////////////////////////////////////
// Portionsweise CeDa Datens�tze einlesen

bool CReportSelectDlg::ReadPartialWhere( PARTIALREAD *prpPartial, CProgressCtrl *prpProgress /* = NULL */ )
{
	
	if( prpPartial->Case < 0 || prpPartial->WhereClause.IsEmpty() ||
		prpPartial->StartTime == TIMENULL || prpPartial->EndTime == TIMENULL )
	{
		// MessageBox("Arguments (struct) incomplete!", "ReadPartialWhere", MB_OK|MB_ICONERROR) ;
		return false ;
	}

	int ilReadFunction = 0 ;	// Lesefunktion entsprechend Reportart festlegen
	switch( prpPartial->Case )
	{
		case 0:		// Sammler ( Standard )
		case 1:		// ABfl�ge BGS
		case 5:		// Messestatistiken
		case 6:		//     "
		case 7:		//     "
		case 9:		// Flugh�fen mit Freq.
			ilReadFunction = 1 ;	// Fl�ge mit ReadSpecial()
			break ;

		case 14:	// POPS Season
			ilReadFunction = 2 ;	// Fl�ge mit ReadDailyFlights() 
			break ;

		default:
			ilReadFunction = 0 ;
	}


	// Zeitraum schrittweise einlesen
	// von Starttag
	COleDateTime olNext( prpPartial->StartTime.GetYear(), prpPartial->StartTime.GetMonth(), prpPartial->StartTime.GetDay(),
						 prpPartial->StartTime.GetHour(), prpPartial->StartTime.GetMinute(), prpPartial->StartTime.GetSecond()) ;
	// bis Entag ( einschl. )
	COleDateTime olEnd( prpPartial->EndTime.GetYear(), prpPartial->EndTime.GetMonth(), prpPartial->EndTime.GetDay(),
						 prpPartial->EndTime.GetHour(), prpPartial->EndTime.GetMinute(), prpPartial->EndTime.GetSecond()) ;
	COleDateTimeSpan olPart( prpPartial->Range,0,0,0 ) ;	// Schrittweite in Tagen
	COleDateTimeSpan olGap( 0,0,0,1 ) ;						// 1 sec L�cke

	bool blRet = true ;
	int ilLoop = 0 ;
	CString olStartTag ;
	CString olEndTag ;
	CString olBetween ;

	while( olNext+olPart < olEnd ) 
	{
		if( prpProgress != NULL ) prpProgress->StepIt() ;
		olStartTag = olNext.Format("%Y%m%d%H%M%S") ; 	
		olEndTag = (olNext+olPart-olGap).Format("%Y%m%d%H%M%S") ; 	
		// TRACE("Part[%d] von [%s] bis [%s]\n", ++ilLoop, olStartTag, olEndTag ) ;
		olBetween.Format( prpPartial->WhereClause,  olStartTag, olEndTag, olStartTag, olEndTag ) ; 
		//TRACE("[%s]\n", olBetween ) ;
		switch( ilReadFunction )
		{
			case 1:		
				if( prpPartial->omData == NULL || prpPartial->Fields.IsEmpty() )
				{
					// MessageBox("Arguments (struct) incomplete!", "ReadPartialWhere",MB_OK|MB_ICONERROR) ;
					blRet = false ;
				}
				else
				{
					omDailyCedaFlightData.ReadSpecial( (CCSPtrArray<ROTATIONDLGFLIGHTDATA> *) prpPartial->omData,
						(LPCTSTR) olBetween, (LPCTSTR) prpPartial->Fields, prpPartial->onlyAFT, prpPartial->Sort, prpPartial->SortArt );
				}
				break ;

			case 2:	
				omDailyCedaFlightData.ReadDailyFlights( (LPCTSTR) olBetween )  ;
				break ;

			default:
				blRet = false ;
		}
		olNext += olPart ;
	}
	if( prpProgress != NULL ) prpProgress->StepIt() ;

	if( blRet )	// restliche Tage einlesen
	{
		olStartTag = olNext.Format("%Y%m%d%H%M%S") ; 	
		olEndTag = olEnd.Format("%Y%m%d%H%M%S") ; 	
		// TRACE("Rest[%d] von [%s] bis [%s]\n", ++ilLoop, olStartTag, olEndTag ) ;
		olBetween.Format( prpPartial->WhereClause,  olStartTag, olEndTag, olStartTag, olEndTag ) ; 
		//TRACE("[%s]\n", olBetween ) ;
		switch( ilReadFunction )
		{
			case 1:	
				omDailyCedaFlightData.ReadSpecial( (CCSPtrArray<ROTATIONDLGFLIGHTDATA> *) prpPartial->omData,
						(LPCTSTR) olBetween, (LPCTSTR) prpPartial->Fields, prpPartial->onlyAFT, prpPartial->Sort, prpPartial->SortArt );
				if( ((CCSPtrArray<ROTATIONDLGFLIGHTDATA> *) prpPartial->omData)->GetSize() < 1 )
				{
					blRet = false ;
				}
				break ;

			case 2:	
				omDailyCedaFlightData.ReadDailyFlights( (LPCTSTR) olBetween ) ;
				if( omDailyCedaFlightData.omData.GetSize() < 1 )
				{
					blRet = false ;
				}
				break ;

			default:
				blRet = false ;
		}
		
		// TRACE( "omDaily... [%d] - olData [%d]\n", omDailyCedaFlightData.omData.GetSize(), ((CCSPtrArray<ROTATIONDLGFLIGHTDATA> *) prpPartial->omData)->GetSize() ) ;
	}

	return( blRet ) ;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
void CReportSelectDlg::SaveToReg()
{
	BOOL ok = WriteDialogToReg((CWnd*) this, m_key);
}


void CReportSelectDlg::OnDestroy() 
{
	SaveToReg();
	CDialog::OnDestroy();
}

void CReportSelectDlg::OnSize(UINT nType, int cx, int cy)  
{
	CDialog::OnSize(nType, cx, cy);
	
	// TODO: Add your message handler code here
	m_resizeHelper.OnSize();
	this->Invalidate();
}

BOOL CReportSelectDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	m_key = "DialogPosition\\ReportsSelection";

	m_PR_Read.SetRange( 0, imProgRange ) ;	
	m_PR_Read.SetStep( 1 ) ;	
	m_PR_Read.SetPos( 0 ) ;	
	m_PR_Read.ShowWindow( SW_HIDE ) ;	// no Progress
	m_ST_Progress.ShowWindow( SW_HIDE ) ;	
	

	if((ogCustomer == "LIS") && (ogAppName == "FIPS"))
	{
		m_CR_List16.ShowWindow(SW_HIDE);
 		m_CR_List24.ShowWindow(SW_HIDE);
		m_CR_List25.ShowWindow(SW_HIDE);
	}
	else if ((ogCustomer == "HAJ") && (ogAppName == "FPMS"))
	{
		m_CR_List16.ShowWindow(SW_HIDE);
		m_CR_List19.ShowWindow(SW_HIDE);
		m_CR_List24.ShowWindow(SW_HIDE);
		m_CR_List25.ShowWindow(SW_HIDE);
	}
	else if((ogCustomer == "SHA") && (ogAppName == "FIPS"))	// Master SHG !!!
	{

		/*CRect olRect;
		GetWindowRect(&olRect);
		MoveWindow(olRect.left - 15, olRect.top, olRect.Width() + 30, olRect.Height());*/

		//m_CR_List7.ShowWindow(SW_HIDE);

		ogPrivList.SetSecStat(m_CR_List20, "REPORTS_RB_DailyFlightLog");
		ogPrivList.SetSecStat(m_CR_List24, "REPORTS_RB_FlightLoadPax");
		ogPrivList.SetSecStat(m_CR_List25, "REPORTS_RB_FlightGpu");
		ogPrivList.SetSecStat(m_CR_List26, "REPORTS_RB_BltAllocSchedule");
		ogPrivList.SetSecStat(m_CR_List21, "REPORTS_RB_FlightOvernight");
		ogPrivList.SetSecStat(m_CR_List22, "REPORTS_RB_FlightDomIntComb");
		ogPrivList.SetSecStat(m_CR_List23, "REPORTS_RB_SeatDomIntComb");
		ogPrivList.SetSecStat(m_CR_List19, "REPORTS_RB_CcaScheduleDaily");
		ogPrivList.SetSecStat(m_CR_List27, "REPORTS_RB_FlightDelay");


		// Shanghai hat eigene Texte im Dialog
		m_CR_List1.SetWindowText(GetString(IDS_STRING1562).GetBuffer(0));
		ogPrivList.SetSecStat(m_CR_List1, "REPORTS_RB_FlightSameAirline");
 		m_CR_List2.SetWindowText(GetString(IDS_STRING1565).GetBuffer(0));
		ogPrivList.SetSecStat(m_CR_List2, "REPORTS_RB_FlightSameOrg");
		m_CR_List3.SetWindowText(GetString(IDS_STRING1589).GetBuffer(0));
		ogPrivList.SetSecStat(m_CR_List3, "REPORTS_RB_FlightSameACReg");
		m_CR_List4.SetWindowText(GetString(IDS_STRING1574).GetBuffer(0));
		ogPrivList.SetSecStat(m_CR_List4, "REPORTS_RB_FlightSameDes");
		m_CR_List5.SetWindowText(GetString(IDS_STRING1580).GetBuffer(0));
		ogPrivList.SetSecStat(m_CR_List5, "REPORTS_RB_FlightSameAC");
		m_CR_List6.SetWindowText(GetString(IDS_STRING1596).GetBuffer(0));
		ogPrivList.SetSecStat(m_CR_List6, "REPORTS_RB_FlightSameNature");
		m_CR_List7.SetWindowText(GetString(IDS_STRING1606).GetBuffer(0));
		ogPrivList.SetSecStat(m_CR_List7, "REPORTS_RB_FlightDelayReason");
		m_CR_List8.SetWindowText(GetString(IDS_STRING1613).GetBuffer(0));
		ogPrivList.SetSecStat(m_CR_List8, "REPORTS_RB_FlightDelayTime");
		m_CR_List9.SetWindowText(GetString(IDS_STRING1623).GetBuffer(0));
		ogPrivList.SetSecStat(m_CR_List9, "REPORTS_RB_FlightCancelledDetailed");
		m_CR_List10.SetWindowText(GetString(IDS_STRING1633).GetBuffer(0));
		ogPrivList.SetSecStat(m_CR_List10, "REPORTS_RB_FlightCancelledSummary");
		m_CR_List11.SetWindowText(GetString(IDS_STRING1630).GetBuffer(0));
		ogPrivList.SetSecStat(m_CR_List11, "REPORTS_RB_FlightArrDepTime");
		
 		m_CR_List12.SetWindowText(GetString(IDS_STRING1634).GetBuffer(0));
		//ogPrivList.SetSecStat(m_CR_List12, "REPORTS_RB_FlightDomIntTransf");
	    m_CR_List12.ShowWindow(SW_HIDE);

		m_CR_List13.SetWindowText(GetString(IDS_STRING1637).GetBuffer(0));
		ogPrivList.SetSecStat(m_CR_List13, "REPORTS_RB_FlightRushHour");

		//m_CR_List14.SetWindowText(GetString(IDS_STRING1644).GetBuffer(0));
		//ogPrivList.SetSecStat(m_CR_List14, "REPORTS_RB_FlightTimeFrame");
		//m_CR_List14.ShowWindow(SW_HIDE);
		ogPrivList.SetSecStat(m_CR_List14, "REPORTS_RB_Invertory");

		m_CR_List15.SetWindowText(GetString(IDS_STRING1750).GetBuffer(0));
		ogPrivList.SetSecStat(m_CR_List15, "REPORTS_RB_CcaRateFailure");
		m_CR_List16.ShowWindow(SW_HIDE);
		m_CR_List18.SetWindowText(GetString(IDS_STRING1751).GetBuffer(0));
		ogPrivList.SetSecStat(m_CR_List18, "REPORTS_RB_SpotRateFailure");

		/*
		WINDOWPLACEMENT olWinPlace;
		CRect olRect;
		// Lege Radio-Button der Liste 24 ueber Radio-Button der Liste 12
		m_CR_List12.GetWindowPlacement(&olWinPlace);				
		m_CR_List24.GetClientRect(olRect);
		m_CR_List24.MoveWindow(olWinPlace.rcNormalPosition.left, olWinPlace.rcNormalPosition.top, olRect.Width(), olRect.Height());
		m_CR_List24.SetWindowText(GetString(IDS_STRING1878).GetBuffer(0));
		m_CR_List24.ShowWindow(SW_SHOW);

		// Lege Radio-Button der Liste 25 ueber Radio-Button der Liste 14
		m_CR_List14.GetWindowPlacement(&olWinPlace);				
		m_CR_List25.GetClientRect(olRect);
		m_CR_List25.MoveWindow(olWinPlace.rcNormalPosition.left, olWinPlace.rcNormalPosition.top, olRect.Width(), olRect.Height());
		m_CR_List25.SetWindowText(GetString(IDS_STRING1888).GetBuffer(0));
		m_CR_List25.ShowWindow(SW_SHOW);
		*/
	}
	else
	{
		; // alle Listungen 
	}


	m_CE_UtcToLocal.SetCheck(TRUE);
	m_CE_Loatab.SetCheck(FALSE);
	// Report Select DAILY nur f�r FAG/POPS ( in utc ! )
	if((ogCustomer == "BVD") && (ogAppName == "POPS"))
	{
		m_CE_UtcToLocal.SetCheck( FALSE ) ;
	}
	
	m_resizeHelper.Init(this->m_hWnd);

	CRect olRect;
	this->GetWindowRect(&olRect);
	GetDialogFromReg(olRect, m_key);
	SetWindowPos(&wndTop, olRect.left, olRect.top, olRect.Width(), olRect.Height(), SWP_HIDEWINDOW);// | SWP_NOMOVE );
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX-Eigenschaftenseiten sollten FALSE zur�ckgeben
}

void CReportSelectDlg::OnCancel() 
{
	// TODO: Zus�tzlichen Bereinigungscode hier einf�gen
	
	ShowWindow(SW_HIDE);
}



// -----   lokale Funktionen
static int CompareToAlc2(const ROTATIONDLGFLIGHTDATA **e1, const ROTATIONDLGFLIGHTDATA **e2)
{

	if((strlen((**e1).Alc2) == 0 ) && (strlen((**e2).Alc2) == 0) )
		return (int)(strcmp((**e1).Alc3,(**e2).Alc3));
	
	else if((strlen((**e1).Alc2) == 0 ) && (strlen((**e2).Alc2) != 0) )
		return (int)(strcmp((**e1).Alc3,(**e2).Alc2));

	else if((strlen((**e1).Alc2) != 0 ) && (strlen((**e2).Alc2) == 0) )
		return (int)(strcmp((**e1).Alc2,(**e2).Alc3));

	else	//if((strlen((**e1).Alc2) != 0 ) && (strlen((**e2).Alc2) != 0) )
		return (int)(strcmp((**e1).Alc2,(**e2).Alc2));

	/*
	if(strcmp((**e1).Alc2,(**e2).Alc2) == 0 )
		return (int)(strcmp((**e1).Alc3,(**e2).Alc3));
	else
		return (int)(strcmp((**e1).Alc2,(**e2).Alc2));
	*/

}

static int CompareToAlc3(const ROTATIONDLGFLIGHTDATA **e1, const ROTATIONDLGFLIGHTDATA **e2)
{
	return (int)(strcmp((**e1).Alc3,(**e2).Alc3));
}

static int CompareToDes3(const ROTATIONDLGFLIGHTDATA **e1, const ROTATIONDLGFLIGHTDATA **e2)
{
	return (int)(strcmp((**e1).Des3,(**e2).Des3));
}

static int CompareToAdid(const ROTATIONDLGFLIGHTDATA **e1, const ROTATIONDLGFLIGHTDATA **e2)
{
	return (int)(strcmp((**e1).Adid,(**e2).Adid));
}

static int CompareToTtypAlc(const ROTATIONDLGFLIGHTDATA **e1, const ROTATIONDLGFLIGHTDATA **e2)
{
	if(strcmp((**e1).Ttyp,(**e2).Ttyp) == 0 )
		return CompareToAlc2(e1,e2);
	else
		return (int)(strcmp((**e1).Ttyp,(**e2).Ttyp));
}

static int CompareToDes3Alc(const ROTATIONDLGFLIGHTDATA **e1, const ROTATIONDLGFLIGHTDATA **e2)
{
	if(strcmp((**e1).Des3,(**e2).Des3) == 0 )
		return CompareToAlc2(e1,e2);
	else
		return (int)(strcmp((**e1).Des3,(**e2).Des3));
}

static int CompareToAlcDes3(const ROTATIONDLGFLIGHTDATA **e1, const ROTATIONDLGFLIGHTDATA **e2)
{
	if(strcmp((**e1).Alc2,(**e2).Alc2) == 0 )
		return CompareToDes3(e1,e2);
	else
		return (int)(strcmp((**e1).Alc2,(**e2).Alc2));
}


static int CompareToTtyp(const ROTATIONDLGFLIGHTDATA **e1, const ROTATIONDLGFLIGHTDATA **e2)
{
		return (int)(strcmp((**e1).Ttyp,(**e2).Ttyp));
}

static int CompareToAlc2Des3(const ROTATIONDLGFLIGHTDATA **e1, const ROTATIONDLGFLIGHTDATA **e2)
{
	int ilRet;

	if((strlen((**e1).Alc2) == 0) && (strlen((**e2).Alc2) == 0))
	{
		ilRet = (int)(strcmp((**e1).Alc3,(**e2).Alc3));
	}
	else
	{
		if(strlen((**e1).Alc2) == 0)
			ilRet = (int)(strcmp((**e1).Alc3,(**e2).Alc2));
	
		if(strlen((**e2).Alc2) == 0)
			ilRet = (int)(strcmp((**e1).Alc2,(**e2).Alc3));
	}

	if((strlen((**e1).Alc2) > 0) && (strlen((**e2).Alc2) > 0))
		ilRet = (int)(strcmp((**e1).Alc2,(**e2).Alc2));



	if(ilRet == 0)
	{
		ilRet = CompareToDes3(e1,e2);
	}
	return ilRet;
}



static int CompareRotation(const ROTATIONDLGFLIGHTDATA **e1, const ROTATIONDLGFLIGHTDATA **e2)
{
	CTime olTime1;
	CTime olTime2;

	if((strcmp((**e1).Des3, pcgHome) == 0) && (strcmp((**e1).Org3, pcgHome) != 0))
		return -1;

	if((strcmp((**e1).Des3, pcgHome) != 0) && (strcmp((**e1).Org3, pcgHome) == 0))
		return 1;

	if((**e1).Tifa == TIMENULL)
		olTime1 = (**e1).Tifd;
	else
		olTime1 = (**e1).Tifa;

	if((**e2).Tifa == TIMENULL)
		olTime2 = (**e2).Tifd;
	else
		olTime2 = (**e2).Tifa;

	return ((olTime1 == olTime2) ? 0 : ((olTime1 > olTime2) ? 1 : -1));
}


static int CompareChrono(const ROTATIONDLGFLIGHTDATA **e1, const ROTATIONDLGFLIGHTDATA **e2)
{
	// vgl. im Zeitlauf: Ankunft - Abflug 
	// hbe am 15/9/98 z.B. f�r Messestatistik !
	CTime olTime1;
	CTime olTime2;

	if( (strcmp((**e1).Adid, "A") == 0)) olTime1 = (**e1).Tifa ;
	else if((strcmp((**e1).Adid, "D") == 0)) olTime1 = (**e1).Tifd ;
	else
	{
		if( (strcmp((**e1).Adid, "B") == 0))
		{
			if((**e1).Tifa != TIMENULL)	olTime1 = (**e1).Tifa;
			else olTime1 = (**e1).Tifd;
		}
	}

	if( (strcmp((**e2).Adid, "A") == 0)) olTime2 = (**e2).Tifa ;
	else if( (strcmp((**e2).Adid, "D") == 0)) olTime2 = (**e2).Tifd ;
	else
	{
		if((strcmp((**e2).Adid, "B") == 0))
		{
			if((**e2).Tifa != TIMENULL)	olTime2 = (**e2).Tifa;
			else olTime2 = (**e2).Tifd;
		}
	}

	
	
	return ((olTime1 == olTime2) ? 0 : ((olTime1 > olTime2) ? 1 : -1));
}


static int CompareSkey(const ROTATIONDLGFLIGHTDATA **e1, const ROTATIONDLGFLIGHTDATA **e2)
{

	if((**e1).Rkey == (**e2).Rkey)
	{
		return CompareRotation(e1,e2);
	}
	else
	{
		return ((**e1).Rkey >  (**e2).Rkey)? 1: -1;
	}

}



