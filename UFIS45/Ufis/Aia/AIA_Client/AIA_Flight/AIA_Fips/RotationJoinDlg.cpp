// RotationJoinDlg.cpp : implementation file
//

#include "stdafx.h"
#include "fpms.h"
#include "RotationJoinDlg.h"
#include "CCSGlobl.h"
#include "AskBox.h"
#include "CedaFlightUtilsData.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


// Local function prototype
static void JoinCf(void *popInstance, int ipDDXType,
    void *vpDataPointer, CString &ropInstanceName);


/////////////////////////////////////////////////////////////////////////////
// RotationJoinDlg dialog


RotationJoinDlg::RotationJoinDlg(CWnd* pParent, ROTATIONDLGFLIGHTDATA *prpFlight, char cpFPart)
	: CDialog(RotationJoinDlg::IDD, pParent)
{
	prmFlight = prpFlight;
	cmFPart = cpFPart;

	//{{AFX_DATA_INIT(RotationJoinDlg)
	m_Alc3 = _T("");
	m_Flns = _T("");
	m_Fltn = _T("");
	m_Sto = _T("");
	//}}AFX_DATA_INIT
	ogDdx.Register(this, APP_EXIT, CString("JOINDLG"), CString("Appli. exit"), JoinCf);

}


RotationJoinDlg::~RotationJoinDlg()
{
	ogDdx.UnRegister(this,NOTUSED);
}


static void JoinCf(void *popInstance, int ipDDXType,
    void *vpDataPointer, CString &ropInstanceName)
{
    RotationJoinDlg *polDlg = (RotationJoinDlg *)popInstance;

    if (ipDDXType == APP_EXIT)
        polDlg->AppExit();
}



void RotationJoinDlg::AppExit()
{
	EndDialog(IDCANCEL);
}


void RotationJoinDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(RotationJoinDlg)
	DDX_Control(pDX, IDC_STO, m_CE_Sto);
	DDX_Control(pDX, IDC_FLTN, m_CE_Fltn);
	DDX_Control(pDX, IDC_FLNS, m_CE_Flns);
	DDX_Control(pDX, IDC_ALC3, m_CE_Alc3);
	DDX_Text(pDX, IDC_ALC3, m_Alc3);
	DDX_Text(pDX, IDC_FLNS, m_Flns);
	DDX_Text(pDX, IDC_FLTN, m_Fltn);
	DDX_Text(pDX, IDC_STO, m_Sto);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(RotationJoinDlg, CDialog)
	//{{AFX_MSG_MAP(RotationJoinDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// RotationJoinDlg message handlers

void RotationJoinDlg::OnCancel() 
{
	
	CDialog::OnCancel();
}


BOOL RotationJoinDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

	m_CE_Sto.SetTypeToDate(true);
	m_CE_Alc3.SetTypeToString("XXX",3,2);
	m_CE_Fltn.SetTypeToString("#####",5,2);
	m_CE_Flns.SetTypeToString("X",1,0);

	m_CE_Sto.SetBKColor(YELLOW);
	m_CE_Alc3.SetBKColor(YELLOW);
	m_CE_Fltn.SetBKColor(YELLOW);

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}



void RotationJoinDlg::OnOK() 
{
	ROTATIONDLGFLIGHTDATA *prlFlight;
	bool blTreffer = false;

	//char clCallWith = 'A';

	char clSearch;

	if (cmFPart == 'A')
		clSearch = 'D';
	else
		clSearch = 'A';

	m_CE_Alc3.GetWindowText(m_Alc3);
	m_CE_Fltn.GetWindowText(m_Fltn);
	m_CE_Flns.GetWindowText(m_Flns);
	m_CE_Sto.GetWindowText(m_Sto);

	if(!m_CE_Alc3.GetStatus() || !m_CE_Fltn.GetStatus() || !m_CE_Sto.GetStatus())
	{
		MessageBox(GetString(IDS_STRING947), GetString(ST_FEHLER));
		return;
	}

	CTime olTimeFrom = DateHourStringToDate( m_Sto , CString("0000")); 
	CTime olTimeTo   = DateHourStringToDate( m_Sto , CString("2359")); 
		
	m_Alc3.TrimRight();
	m_Fltn.TrimRight();
	m_Flns.TrimRight();

	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));

	ROTATIONDLGFLIGHTDATA rlFlight;

	if(!ogRotationDlgFlights.SearchFlight(m_Alc3, m_Fltn, m_Flns, olTimeTo, clSearch, rlFlight))
	{
		SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
		MessageBox(GetString(IDS_STRING948), GetString(ST_FEHLER));
		return;
	}
	else
	{
		prlFlight = &rlFlight;

	}

	ROTATIONDLGFLIGHTDATA *prpAFlight;
	ROTATIONDLGFLIGHTDATA *prpDFlight;


	if(cmFPart == 'A')
	{
		prpAFlight = ogRotationDlgFlights.GetFlightByUrno(prmFlight->Urno);
		prpDFlight = prlFlight;
		
	}
	else
	{
		prpDFlight = ogRotationDlgFlights.GetFlightByUrno(prmFlight->Urno);
		prpAFlight = prlFlight;

	}

	// Is departure before arrival?
//	if(prpAFlight->Stoa > prpDFlight->Stod)
	if(prpAFlight->Tifa > prpDFlight->Tifd)
	{
		MessageBox(GetString(IDS_STRING979), GetString(ST_FEHLER), MB_ICONWARNING);
		return;
	}

	CedaFlightUtilsData olFlightUtilsData;
	olFlightUtilsData.JoinFlight(prpAFlight->Urno, prpAFlight->Rkey, prpDFlight->Urno, prpDFlight->Rkey, false);
	

	CDialog::OnOK();
}
