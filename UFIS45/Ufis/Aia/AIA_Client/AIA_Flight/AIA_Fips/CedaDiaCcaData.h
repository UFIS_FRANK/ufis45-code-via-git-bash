// CedaDiaCcaData.h

#ifndef __CEDADIACCADATA__
#define __CEDADIACCADATA__
 
#include "stdafx.h"
#include "CCSGlobl.h"
#include "CCSPtrArray.h"
#include "CCSCedadata.h"
#include "CCSddx.h"

//---------------------------------------------------------------------------------------------------------

struct CCAFLIGHTDATA;

enum
{
	CCA_CFL_NOCONFLICT=-1,
	CCA_CFL_BADALLOC=0,
	CCA_CFL_OVERLAPPED=2
};

struct DIACCADATA 
{
	CTime 	 Cdat; 		// Erstellungsdatum
	CTime 	 Ckba; 		// Belegung Checkin-Schalter aktueller Beginn
	CTime 	 Ckbs; 		// Belegung Checkin-Schalter geplanter Beginn
	CTime 	 Ckea; 		// Belegung Checkin-Schalter aktuelles Ende
	CTime 	 Ckes; 		// Belegung Checkin-Schalter geplantes Ende
	char 	 Ckic[6]; 	// Checkin-Schalter
	char 	 Ckif[2]; 	// fester Checkin-Schalter
	char 	 Ckit[2]; 	// Terminal Checkin-Schalter
	int 	 Copn	; 	// Schalteröffnungszeit (in Min. vor Abflug)
	char 	 Ctyp[2]; 	// Check-In Typ (Common oder Flight)
	long 	 Flnu; 	// Eindeutige Datensatz-Nr. des zugeteilten Fluges
	CTime 	 Lstu; 		// Datum letzte Änderung
	char 	 Prfl[3]; 	// Protokollierungskennzeichen
	long 	 Urno; 		// Eindeutige Datensatz-Nr.
	char 	 Usec[33]; 	// Anwender (Ersteller)
	char 	 Useu[33]; 	// Anwender (letzte Änderung)
	char	 Rema[61];	// Bemerkung
	char	 Disp[61];	// Bemerkung
	char	 Act3[6];	// Act3 of Flight
	char     Flno[11];	// Flno of Flight
	char	 Stat[11];	// Status of Cca for Conflicts
	long	 Ghpu;		// Urno von Ghp
	long     Gpmu;		// Urno von Gpm
	long     Ghsu;		// Urno von Ghs
	char	 Cgru[258];	// Urnos aus GRN
	CTime	 Stod;
	long	 KKey;	
	bool	 Valid;
	int 	 NotValidBrush;


/*STAT
	Index 0: WRONG Halle/Terminal-Konflik ALLOC,	==> 'J'	or ' '
	Index 1: CONFIRMED,		==> ' ' or 'J'
	Index 2: OVERLAPPED,	==> 'J' or ' '
	Index 3: CONFIRMED,		==> ' ' or 'J'
	Index 4: FLNO changed
	Index 5: Cki whitout demand
	Index 6: Rule changed
	Index 7: Act changed 
	Index 8: Std changed
	Index 9: Error: 0 = No error;   N=No Rules  ; X=More Rules;   W = Rules without leistung
*/

	//DataCreated by this class
	int		 IsChanged;	// Check whether Data has Changed für Relaese

	bool	 IsSelected;
	bool	AlcKonf;

	//bool	Freq;	
	int		FlightFreq;

	int NextC;
	int PrevC;
	int Freq;

	bool bmInDB;

	/// MCU 22.09.98 Brush wird bei Konfliktcheck gesetzt
	//CBrush *Brush;

	DIACCADATA(void)
	{ 
		memset(this,'\0',sizeof(*this));
		AlcKonf		=	false;
		IsSelected  =   false;
		Valid		=	false;
		Urno		=   0;
		Flnu		=   0;
		Ghpu		=	0;
		Gpmu		=	0;
		Ghsu		=	0;
		KKey		=	0;
		IsChanged	=	DATA_UNCHANGED;
		Cdat		=	TIMENULL;
		Ckba		=	TIMENULL;
		Ckbs		=	TIMENULL;
		Ckea		=	TIMENULL;
		Ckes		=	TIMENULL;
		Lstu		=	TIMENULL;
		Stod		=	TIMENULL;
		strcpy(Stat, "0000000000");
		Freq = 0;
		FlightFreq = 0;
		NextC = 0;
		PrevC = 0;

		bmInDB = false;
		NotValidBrush = -1;
	}

	bool IsOpen();
	bool UpdateFIDSMonitor();
	
	const CTime &GetBestStartAlloc();
	const CTime &GetBestEndAlloc();

}; // end DIACCADATAStrukt

//---------------------------------------------------------------------------------------------------------
// Class declaratino

class CedaDiaCcaData: public CCSCedaData
{
// Attributes
public:
    CMapPtrToPtr omUrnoMap;
    CMapStringToPtr omCkicMap;

    CCSPtrArray<DIACCADATA> omData;
	CCSPtrArray<DIACCADATA>  omDelData;

 	char pcmListOfFields[2048];
	CString omSelection;
	bool bmSelection;

	CTime omFrom;
	CTime omTo;

	CUIntArray omKKeyArray;
	CMapPtrToPtr omKKeyMap;

	CMapPtrToPtr omFlnuMap;
 
	bool bmNew ;

	bool bmOffLine;

	const void *pomParent;

// Operations
public:
    CedaDiaCcaData();
	~CedaDiaCcaData();

	bool SetParent(const void *popParent);

	void SetOffLine();
	void ReleaseCca();
	
	
	void AddToKeyMap(DIACCADATA *prpCca);
	void DelFromKeyMap(DIACCADATA *prpCca);

	void RemoveNfromMap();

	bool CompareCompressCca(DIACCADATA *prpCca1, DIACCADATA *prpCca2);

	void Kompress(CCSPtrArray<CCAFLIGHTDATA> &opFLights);
	void KompressFreq(CCSPtrArray<CCAFLIGHTDATA> &opFLights);
	void KompressND();

	void UpdateByKKey(long lpKKey, CString opCnam);
	
	bool CkeckOverlapping(CString opCnam, CTime opStart, CTime opEnd);
	bool GetSelKKeys(CUIntArray &opUrnos);


	void Register(void);
	void UnRegister(void);
	void ClearAll(bool bpWithRegistration = true);
//	bool ReadAll(CString &opUrnos, CTime opFrom, CTime opTo, bool bpClearAll = true);
	bool ReadAll(CString &opUrnos, CTime opFrom, CTime opTo, bool bpClearAll = true, bool bpOnlyFlight = true); 

	bool GetCcasByKKey(CCSPtrArray<DIACCADATA> &ropCcaList, long lpKKey);
	bool GetCcasByFlnu(CCSPtrArray<DIACCADATA> &ropCcaList, long lpFlnu);
	bool GetCcasByCkic(CCSPtrArray<DIACCADATA> &ropCcaList, CString opCkic);
	void GetCcaArray(CCSPtrArray<DIACCADATA> &opCca, CString opTyp, CString opCnam);

	bool GetCcaData(long lpFlnu, CString &opCca, CTime &opStart, CTime &opEnd);    

	bool GetCcaArray(long lpFlnu, CCSPtrArray<DIACCADATA> &opData);
	void GetCcaError(CCSPtrArray<DIACCADATA> &ropAct, long lpFlnu);



	bool GetMinMaxCkic(long lpFlnu, CString &opMin, CString &opMax);

	bool GetMinMaxCkicAndTime(long lpFlnu, CString &opMin, CString &opMax, CTime &opStart, CTime &opEnd);

	bool IsPassFilter(DIACCADATA *prpCca);

	void GetCommonUrnos(CUIntArray &opUrnos, bool bpOnlySel);

	bool GetSel(CUIntArray &opUrnos);

	void Expand(long lpUrno, CTime opFrom , CTime opTo, CTime opRefDay, bool bpAllDays = false);

	bool DeleteOrUpdateTimeFrame(CTime opFrom, CTime opTo, CString opVerkTag);

	void ChangeTimesByUrnos(CUIntArray &opUrnos, CTime opCkbs , CTime opCkes, CString &opMessage, bool bpActual = false);
	void ChangeTimesByKKey(CUIntArray &opUrnos, CTime opCkbs , CTime opCkes);

	bool Read(char *pspWhere = NULL, bool bpClearAll = true, bool bpDdx = false);
    bool ReadInsert(char *pspWhere = NULL);
	bool InsertInternal(DIACCADATA *prpCca, bool bpDdx = true);
	bool Update(DIACCADATA *prpCca);
	bool UpdateInternal(DIACCADATA *prpCca, bool bpDdx = true, bool bpSaveStat = false);
	bool DeleteInternal(DIACCADATA *prpCca, bool bpDdx = true);
	bool ReadSpecial(CCSPtrArray<DIACCADATA> &ropAct,char *pspWhere);
	bool ExistCkic(DIACCADATA *prpCca);
	bool ExistSpecial(char *pspWhere);

	//Save Single Row
	bool Save(DIACCADATA *prpCca);
	void SaveInternal(CCSPtrArray<DIACCADATA> *popCcaList, bool bpDdx =true);
	//Save multi
	void Release(CCSPtrArray<DIACCADATA> *popCcaList = NULL, bool bpDdx = true);

	void ProcessBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);
	DIACCADATA  *GetCcaByUrno(long lpUrno);
	bool UpdateSpecial(CCSPtrArray<DIACCADATA> &ropCca, CCSPtrArray<DIACCADATA> &ropCcaSave, long lpFlightUrno);
	void GetSpecialList(CCSPtrArray<DIACCADATA> &opCca, CString &opFieldDataList);
	bool DeleteSpecial(CString olFlightUrnoList);
	void SetPreSelection(CTime &opFrom, CTime &opTo, CString &opFtyps);
	bool ReadCcas(char *pcpSelection = NULL, bool bpIgnoreViewSel = false);
	bool AddCcaInternal(DIACCADATA *prpCCA);
	CString omFtyps;
	CString omFlnus;

	void SetTableName(CString opTab)
	{
		strcpy(pcmTableName, opTab.GetBuffer(0));
	}
	
	// hbe 11/12/98: um gewünschten Verkehrstag erweitert; def.: => heute
    // hbe 5/99: um Object erweitert BLK / REPBLK 
	CString MakeBlkData( CTime opTrafficDay = TIMENULL , int ipDOO = -1, CString opObject="BLK");
	CString MakeBlkData(RecordSet *popBlkRecord , int ipDOO = -1 );

	CString  GetDataList(DIACCADATA *prpCca);

	void GetBazValues(long lpFlnu, CString opFlti, CString &opBaz1,CString &opBaz4, CTime &opStart, CTime &opEnd);

	bool AllHaveActOpenTime(const CUIntArray &ropUrnos);


	bool CheckDemands( const CCAFLIGHTDATA *prpFlightD);
	bool DelAllDemByFlnuGhpu(long lpFlnu, long lpGhpu);



//-----------------
// MWO neue Funktionen
	bool ChangeTime(long lpUrno,CTime opNewStart,CTime opNewEnd, bool bpWithSave);


	// Private methods
private:
    void PrepareDiaCcaData(DIACCADATA *prpDIACCADATA);

};

//---------------------------------------------------------------------------------------------------------


#endif //__CEDADIACCADATA__
