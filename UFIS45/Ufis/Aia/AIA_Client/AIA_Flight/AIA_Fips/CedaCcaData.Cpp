// CedaCcaData.cpp
 
#include "stdafx.h"
#include <afxwin.h>
#include "BasicData.h"
#include "CCSGlobl.h"
#include "CedaCcaData.h"
#include "CCSBcHandle.h"
#include "CCSCedadata.h"
#include "ButtonListDlg.h"
#include "CcaCedaFlightData.h"
#include "DataSet.h"
#include "Utils.h"
//MWO
#include "BasicData.h"
//END MWO
#include "CcaCommonTableViewer.h"



CCADATA::CCADATA(void)
{ 
	memset(this,'\0',sizeof(*this));
	Urno		=   0;
	Flnu		=   0;
	Ghpu		=	0;
	Gpmu		=	0;
	Ghsu		=	0;
	IsChanged	=	DATA_UNCHANGED;
	Cdat		=	TIMENULL;
	Ckba		=	TIMENULL;
	Ckbs		=	TIMENULL;
	Ckea		=	TIMENULL;
	Ckes		=	TIMENULL;
	Lstu		=	TIMENULL;
	Stod		=	TIMENULL;
	strcpy(Stat, "0000000000");
}


const CCADATA& CCADATA::operator= ( const DIACCADATA& s)
{

	Cdat = s.Cdat; 		
	Ckba = s.Ckba; 		
	Ckbs = s.Ckbs; 		
	Ckea = s.Ckea; 		
	Ckes = s.Ckes; 		
	Lstu = s.Lstu; 		
	Flnu = s.Flnu; 	
	Urno = s.Urno; 		
	Ghpu = s.Ghpu;		
	Gpmu = s.Gpmu;		
	Ghsu = s.Ghsu;		
	Copn = s.Copn;	 	
	Stod = s.Stod;	 	

	 strcpy(Ckic,s.Ckic); 	
	 strcpy(Ckif,s.Ckif); 	
	 strcpy(Ckit,s.Ckit); 	
	 strcpy(Ctyp,s.Ctyp); 	
	 strcpy(Prfl,s.Prfl); 	
	 strcpy(Usec,s.Usec); 	
	 strcpy(Useu,s.Useu); 	
	 strcpy(Rema,s.Rema);	
	 strcpy(Disp,s.Disp);	
	 strcpy(Act3,s.Act3);	
	 strcpy(Flno,s.Flno);	
	 strcpy(Stat,s.Stat);	
	 strcpy(Cgru,s.Cgru);	


	return *this;
}



bool CCADATA::IsOpen(void)
{
	return (Ckba != TIMENULL && Ckea == TIMENULL);
}



bool CCADATA::UpdateFIDSMonitor(void)
{
	if (IsOpen())
	{
		// Open Fids Monitor
		// Message format: "OPEN CKIN: %s AFTU: %s CCAU: %s"
		CString olMessage;
		olMessage.Format("OPEN CKIN: %s AFTU: %ld CCAU: %ld", Ckic, Flnu, Urno);

		return pogButtonList->CallCuteIF(olMessage);
	}
	else
	{
		// close Fids Monitor
		// Message format: "CLOSE CKIN: %s AFTU: %s CCAU: %s"
		CString olMessage;
		olMessage.Format("CLOSE CKIN: %s AFTU: %ld CCAU: %ld", Ckic, Flnu, Urno);

		return pogButtonList->CallCuteIF(olMessage);
	}
}



static void ProcessCcaCf(void *popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName);




static int Compare(const CCADATA **e1, const CCADATA **e2)
{
	return ( (  CString((**e1).Ckic) ==CString((**e2).Ckic)) ? 0 : ( (CString((**e1).Ckic) > CString((**e2).Ckic) ? 1 : -1)));
}



//--CEDADATA-----------------------------------------------------------------------------------------------

CedaCcaData::CedaCcaData(int ipDDXNew /*= DONT_NEED*/, int ipDDXChange /*= DONT_NEED*/, int ipDDXDelete /*= DONT_NEED*/, CWnd *popParent /* = NULL */) : 
	CCSCedaData(&ogCommHandler), imDDXNew(ipDDXNew), imDDXChange(ipDDXChange), imDDXDelete(ipDDXDelete),
		pomParent(popParent)
{
    BEGIN_CEDARECINFO(CCADATA, CcaDataRecInfo)
		CCS_FIELD_DATE		(Cdat,"CDAT","Erstellungsdatum", 1)
		CCS_FIELD_DATE		(Ckba,"CKBA","Belegung Checkin-Schalter aktueller Beginn", 1)
		CCS_FIELD_DATE		(Ckbs,"CKBS","Belegung Checkin-Schalter geplanter Beginn", 1)
		CCS_FIELD_DATE		(Ckea,"CKEA","Belegung Checkin-Schalter aktuelles Ende", 1)
		CCS_FIELD_DATE		(Ckes,"CKES","Belegung Checkin-Schalter geplantes Ende", 1)
		CCS_FIELD_CHAR_TRIM	(Ckic,"CKIC","Checkin-Schalter", 1)
		CCS_FIELD_CHAR_TRIM	(Ckif,"CKIF","fester Checkin-Schalter", 1)
		CCS_FIELD_CHAR_TRIM	(Ckit,"CKIT","Terminal Checkin-Schalter", 1)
		CCS_FIELD_CHAR_TRIM	(Ctyp,"CTYP","Check-In Typ (Common oder Flight)", 1)
		CCS_FIELD_LONG		(Flnu,"FLNU","Eindeutige Datensatz-Nr. des zugeteilten Fluges", 1)
		CCS_FIELD_DATE		(Lstu,"LSTU","Datum letzte �nderung", 1)
		CCS_FIELD_CHAR_TRIM	(Prfl,"PRFL","Protokollierungskennzeichen", 1)
		CCS_FIELD_LONG		(Urno,"URNO","Eindeutige Datensatz-Nr.", 1)
		CCS_FIELD_CHAR_TRIM	(Usec,"USEC","Anwender (Ersteller)", 1)
		CCS_FIELD_CHAR_TRIM	(Useu,"USEU","Anwender (letzte �nderung)", 1)
		CCS_FIELD_CHAR_TRIM	(Rema,"REMA","Bemerkung", 1)
		CCS_FIELD_CHAR_TRIM	(Disp,"DISP","Bemerkung", 1)
		CCS_FIELD_CHAR_TRIM	(Act3,"ACT3", "Aircarfttype 3 Letter", 1)
		CCS_FIELD_CHAR_TRIM	(Flno,"FLNO", "Flightnumber", 1)
		CCS_FIELD_CHAR_TRIM	(Stat,"STAT", "Conflictstatus of Cca", 1)
		CCS_FIELD_LONG		(Ghpu,"Ghpu", "", 1)
		CCS_FIELD_LONG		(Gpmu,"Gpmu", "", 1)
		CCS_FIELD_LONG		(Ghsu,"Ghsu", "", 1)
		CCS_FIELD_CHAR_TRIM	(Cgru,"Cgru", "", 1)
		CCS_FIELD_DATE		(Stod,"STOD","", 1)
		CCS_FIELD_INT		(Copn,"COPN","Schalter�ffnungszeit (in Min. vor Abflug)", 1)
  END_CEDARECINFO

    // Copy the record structure
    for (int i = 0; i < sizeof(CcaDataRecInfo)/sizeof(CcaDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&CcaDataRecInfo[i],sizeof(CEDARECINFO));
        omRecInfo.Add(prpCedaRecInfo);
	}
    // Initialize table names and field names
    strcpy(pcmTableName,"CCA");
    sprintf(pcmListOfFields,"CDAT,CKBA,CKBS,CKEA,CKES,CKIC,CKIF,CKIT,CTYP,FLNU,LSTU,PRFL,URNO,USEC,USEU,REMA,DISP,ACT3,FLNO,STAT,GHPU,GPMU,GHSU,CGRU,STOD,COPN");
	pcmFieldList = pcmListOfFields; //because in CedaData pcmFieldList is just a pointer

	bmCommonOnly = false;
	omFlnuList.Empty();

	omData.RemoveAll();
}

//--REGISTER----------------------------------------------------------------------------------------------

void CedaCcaData::Register(void)
{
	ogDdx.Register((void *)this,BC_CCA_CHANGE,	CString("CCADATA"), CString("Cca-changed"),	ProcessCcaCf);
	ogDdx.Register((void *)this,BC_CCA_NEW,		CString("CCADATA"), CString("Cca-new"),		ProcessCcaCf);
	ogDdx.Register((void *)this,BC_CCA_DELETE,	CString("CCADATA"), CString("Cca-deleted"),	ProcessCcaCf);
}


void CedaCcaData::UnRegister(void)
{
	ogDdx.UnRegister(this,NOTUSED);
}


//--~CEDADATA----------------------------------------------------------------------------------------------

CedaCcaData::~CedaCcaData(void)
{
	ogDdx.UnRegister(this,NOTUSED);
	omRecInfo.DeleteAll();
	ClearAll();
}

//--CLEAR-ALL----------------------------------------------------------------------------------------------

void CedaCcaData::ClearAll(bool bpWithRegistration)
{
    omUrnoMap.RemoveAll();
	omCkicMap.RemoveAll();
    omData.DeleteAll();
	if(bpWithRegistration)
	{
		ogDdx.UnRegister(this,NOTUSED);
	}
	omSelection = "";
	bmSelection = false;
}

//--READ-ALL-----------------------------------------------------------------------------------------------

//-----------------------------------------------------------------------------------------------

bool CedaCcaData::ReadAllFlights(const CString &opWhere)
{
	if(omSelection == opWhere)
		return false; 

	ClearAll(false);

	omSelection = CString("CKBS<>' ' AND CKIC<>' ' AND ") + opWhere;
	CString olSelection = CString("WHERE ") + omSelection;

	char pclSel[1024];
    
	strcpy(pclSel, olSelection );

	return Read(pclSel);

} 



bool CedaCcaData::ReadSpecial(const CString &opWhere)
{
	if(omSelection == opWhere)
		return false; 

	if (opWhere.IsEmpty())
		return false;

	ClearAll(false);

	omSelection = opWhere;
	CString olSelection = CString("WHERE ") + omSelection;

	char pclSel[1024];
    
	strcpy(pclSel, olSelection );


	if (Read(pclSel))
	{
		return true;
	}
	return false;
} 



bool CedaCcaData::Read(char *pspWhere /*NULL*/, bool bpClearAll, bool bpDdx, bool bpNoErrors /* = true */ )
{
    // Select data from the database
	bool ilRc = true;

	if(bpClearAll)
	{
		omUrnoMap.RemoveAll();
		omCkicMap.RemoveAll();
		omData.DeleteAll();
	}


	if(pspWhere == NULL)
	{	
		ilRc = CedaAction("RT");
	}
	else
	{
		ilRc = CedaAction("RT", pspWhere);
	}
	if (ilRc != true)
	{
		return false;
	}
	// Load data from CedaData into the dynamic array of record
	for (int ilLc = 0; ilRc == true; ilLc++)
	{
		CCADATA *prlCca = new CCADATA;
		if ((ilRc = GetBufferRecord(ilLc,prlCca)) == true)
		{
			// CCA mt Errorstatus aussortieren ?
			bool blTrouble = bpNoErrors ? IsErrorCca( prlCca ) : false ;
			if( ! blTrouble )
			{
				if(UpdateInternal(prlCca, bpDdx))
				{
					delete prlCca;
				}
				else
				{
					InsertInternal(prlCca, bpDdx);
				}
			}
			else
			{
				if( prlCca != NULL ) delete prlCca;
			}
		}
		else
		{
			if( prlCca != NULL ) delete prlCca;
		}
	}
    return true;

}


bool CedaCcaData::GetCcasByFlnu(long lpFlnu, CCSPtrArray<CCADATA> &ropFlightCcaList)
{
	for (int ilLc = omData.GetSize()-1; ilLc >= 0; ilLc--)
	{
		CCADATA plCca = omData.ElementAt(ilLc);
		if (plCca.Urno > 0 && omData[ilLc].Flnu == lpFlnu)
//		if (omData[ilLc].Flnu == lpFlnu)
		{
			ropFlightCcaList.Add(&omData[ilLc]);
		}
	}
	
	return true;	
}


bool CedaCcaData::DelAllDemByFlnu(long lpFlnu)
{
	for (int ilLc = omData.GetSize()-1; ilLc >= 0; ilLc--)
	{
		if (omData[ilLc].Flnu == lpFlnu && strlen(omData[ilLc].Ckic) == 0)
		{
			DeleteInternal(&omData[ilLc], false);
		}
	}
	
	return true;	
}




bool CedaCcaData::CheckDemands(const CCAFLIGHTDATA *prpFlight)
{
	if (prpFlight == NULL)
		return false;

	// get ccas for flight
	CCSPtrArray<CCADATA> olFlightCcaList;
	GetCcasByFlnu(prpFlight->Urno, olFlightCcaList);

	CCSPtrArray<CCADATA> olFlightCcaDemList;
	// check ccas with rules
	ogDataSet.CheckDemands(prpFlight, olFlightCcaList, olFlightCcaDemList);

	// delete old demands for flight internaly
	DelAllDemByFlnu(prpFlight->Urno);

	// create cca demands for flight internaly
	for (int k=0; k<olFlightCcaDemList.GetSize(); k++)
	{
		if (strlen(olFlightCcaDemList[k].Ckic) == 0)
		{
			InsertInternal(&olFlightCcaDemList[k], false);
		}
	}
	return true;
}



// hbe : auf Fehlerstatus pr�fen
bool CedaCcaData::IsErrorCca( CCADATA *prpCca )
{
	bool ilRet = false ;
	
	if( prpCca != NULL )
	{
		if( strcmp(prpCca->Ctyp, "E" ) == 0 )
		{
			ilRet = true ;	// Error
		}
		else
		{
			if( prpCca->Stat[9] != '0' && prpCca->Stat[9] != ' ')
			{
				ilRet = true ; // Error
			}
		}
	}
	else
	{
		ilRet = true ;	// Error
	}
	
	return( ilRet ) ;
}


//--INSERT-------------------------------------------------------------------------------------------------

bool CedaCcaData::Insert(CCADATA *prpCca)
{
	prpCca->IsChanged = DATA_NEW;
	Save(prpCca); //Update Database
	InsertInternal(prpCca);
    return true;
}

//--INSERT-INTERNAL--------------------------------------------------------------------------------------

bool CedaCcaData::InsertInternal(CCADATA *prpCca, bool bpDdx)
{
	omData.Add(prpCca);//Update omData
	omUrnoMap.SetAt((void *)prpCca->Urno,prpCca);
	omCkicMap.SetAt(prpCca->Ckic,prpCca);
	if(bpDdx) 
	{	
		ogDdx.DataChanged((void *)this, imDDXNew,(void *)prpCca ); //Update Viewer
	}
    return true;
}

//--DELETE-------------------------------------------------------------------------------------------------

bool CedaCcaData::Delete(long lpUrno)
{
	CCADATA *prlCca = GetCcaByUrno(lpUrno);
	if (prlCca != NULL)
	{
		prlCca->IsChanged = DATA_DELETED;
		Save(prlCca); //Update Database
		//DeleteInternal(prlCca);
	}
    return true;
}

//--DELETE-INTERNAL----------------------------------------------------------------------------------------

bool CedaCcaData::DeleteInternal(CCADATA *prpCca, bool bpDdx /*=true*/)
{
	if (bpDdx)
		ogDdx.DataChanged((void *)this, imDDXDelete, (void *)prpCca); //Update Viewer
	omUrnoMap.RemoveKey((void *)prpCca->Urno);
	omCkicMap.RemoveKey(prpCca->Ckic);
	int ilCcaCount = omData.GetSize();
	for (int ilLc = 0; ilLc < ilCcaCount; ilLc++)
	{
		if (omData[ilLc].Urno == prpCca->Urno)
		{
			omData.DeleteAt(ilLc);//Update omData
			break;
		}
	}
    return true;
}


//--UPDATE-INTERNAL--------------------------------------------------------------------------------------------

bool CedaCcaData::UpdateInternal(CCADATA *prpCca, bool bpDdx)
{
	CCADATA *prlCca = GetCcaByUrno(prpCca->Urno);
	if (prlCca != NULL)
	{
		omCkicMap.RemoveKey(prlCca->Ckic);
		*prlCca = *prpCca; //Update omData
		omCkicMap.SetAt(prlCca->Ckic,prlCca);
		if(bpDdx)
		{
			ogDdx.DataChanged((void *)this, imDDXChange, (void *)prlCca); //Update Viewer
		}
		return true;
	}
    return false;
}

//--GET-BY-URNO--------------------------------------------------------------------------------------------

CCADATA *CedaCcaData::GetCcaByUrno(long lpUrno)
{
	CCADATA  *prlCca;
	if (omUrnoMap.Lookup((void *)lpUrno,(void *& )prlCca) == TRUE)
	{
		return prlCca;
	}
	return NULL;
}

//--READSPECIALCCADATA-------------------------------------------------------------------------------------

bool CedaCcaData::ReadSpecial(CCSPtrArray<CCADATA> &ropCca,char *pspWhere, bool bpNoErrors )
{
	bool ilRc = true;
	if (pspWhere == NULL)
	{
		if (CedaAction("RT") == false)
		{
			return false;
		}
	}
	else
	{
		if (CedaAction("RT",pspWhere) == false)
		{
			return false;
		}
	}
	for (int ilLc = 0; ilRc == true; ilLc++)
	{
		CCADATA *prlCca = new CCADATA;
		if ((ilRc = GetBufferRecord(ilLc,prlCca)) == true)
		{
			
			bool blTrouble = bpNoErrors ? IsErrorCca( prlCca ) : false ;
			if( ! blTrouble )
			{
				ropCca.Add(prlCca);
			}
			else
			{
				if( prlCca != NULL ) delete prlCca;
			}
		}
		else
		{
			delete prlCca;
		}
	}

	ropCca.Sort(Compare);
    return true;
}

//--EXIST------------------------------------------------------------------------------------------------

bool CedaCcaData::ExistCkic(CCADATA *prpCca)
{
	CCADATA  *prlCca;
	if (omCkicMap.Lookup((LPCSTR)prpCca->Ckic,(void *&)prlCca) == TRUE)
	{
		if(prlCca->Urno != prpCca->Urno) return true;
	}
	return false;
}

//--EXIST-SPECIAL--------------------------------------------------------------------------------------------

bool CedaCcaData::ExistSpecial(char *pspWhere)
{
	bool ilRc = false;
	char pclSelection[512];
	sprintf(pclSelection,"WHERE %s",pspWhere);
	ilRc = CedaAction("RT",pclSelection);
	return ilRc;
}

//--SAVE---------------------------------------------------------------------------------------------------






bool CedaCcaData::DeleteSpecial(CString olFlightUrnoList)
{
	char pclSelection[124];
	if(olFlightUrnoList.IsEmpty())
		return false;

	CStringArray olStrArray;
	char pclData[512] = "";
	char pclFieldList[256];

	CString olFieldList = "USEU,LSTU,CKBA,CKBS,CKEA,CKES,CKIC,CKIF,CKIT,COPN,CTYP,PRFL";

	CString olUser = CString(pomCommHandler->pcmUser); 
	CTime olTime = CTime::GetCurrentTime();
	ogBasicData.LocalToUtc(olTime);
	CString olCurrTime = CTimeToDBString(olTime, TIMENULL);

	CString olData = olUser + CString(",") + olCurrTime  + CString(", , , , , , , , , , ");

	for(int i = ExtractItemList(olFlightUrnoList, &olStrArray) -1; i >= 0; i--)
	{
		sprintf(pclSelection, "WHERE FLNU = %s", olStrArray[i]);
		strcpy(pclData, olData);
		strcpy(pclFieldList, olFieldList);
		
		CedaAction("URT",pcmTableName, pclFieldList, pclSelection, "", pclData, "BUF1");
	}
	return true;
}




void CedaCcaData::GetSpecialList(CCSPtrArray<CCADATA> &opCca, CString &opFieldDataList)
{
	CString olFieldList = CString("\rCKIC,CKIT,CKBS,CKES,CKBA,CKEA");
	CString olDataList;

	CString olCkic;
	CString olCkit;
	CString olCkbs;
	CString olCkes;

	opFieldDataList = "";

	for( int i = opCca.GetSize() - 1; i >= 0; i--)
	{
		CString olCkic = CString(opCca[i].Ckic);
		olCkic.TrimRight();
		if((!olCkic.IsEmpty()) || (opCca[i].Ckbs != TIMENULL)  || (opCca[i].Ckes != TIMENULL))
		{

			 olCkic = CString(opCca[i].Ckic);
			 olCkit	= CString(opCca[i].Ckit);
			 olCkbs = CTimeToDBString(opCca[i].Ckbs, TIMENULL);
			 olCkes = CTimeToDBString(opCca[i].Ckes, TIMENULL);
			
			if(olCkic.IsEmpty()) olCkic = " ";
			if(olCkit.IsEmpty()) olCkit = " ";
			if(olCkbs.IsEmpty()) olCkbs = " ";
			if(olCkes.IsEmpty()) olCkes = " ";

			olDataList +=  olCkic + CString(",") + olCkit + 
				CString(",") + olCkbs + CString(",") + olCkes + 
				CString(",") + olCkbs + CString(",") + olCkes + 
				CString("\n"); 
		}
	}
	if(!olDataList.IsEmpty())
	{
		olDataList = olDataList.Left(olDataList.GetLength() - 1);
		opFieldDataList = olFieldList + CString('\n') + olDataList;
	}
	return;
}


//--PROCESS-CF---------------------------------------------------------------------------------------------

static void  ProcessCcaCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
    CedaCcaData *polViewer = (CedaCcaData *)vpInstance;

	polViewer->ProcessBc(ipDDXType,vpDataPointer,ropInstanceName);		//(CCADATA *)vpDataPointer
}






bool CedaCcaData::Save(CCADATA *prpCca)
{
	bool olRc = true;
	CString olListOfData;
	char pclSelection[124];
	char pclData[524];

	CString olUser = CString(pomCommHandler->pcmUser); 
	CTime olTime = CTime::GetCurrentTime();
	ogBasicData.LocalToUtc(olTime);
	CString olCurrTime = CTimeToDBString(olTime, TIMENULL);


	if (prpCca->IsChanged == DATA_UNCHANGED)
	{
		return true; // no change, nothing to do
	}
	switch(prpCca->IsChanged)
	{
	case DATA_NEW:
		prpCca->Cdat = olTime;
		strcpy(prpCca->Usec, olUser);

		MakeCedaData(&omRecInfo,olListOfData,prpCca);
		strcpy(pclData,olListOfData);
		olRc = CedaAction("IRT","","",pclData);
		prpCca->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_CHANGED:
		prpCca->Lstu = olTime;
		strcpy(prpCca->Useu , olUser);

		sprintf(pclSelection, "WHERE URNO = %ld", prpCca->Urno);
		MakeCedaData(&omRecInfo,olListOfData,prpCca);
		strcpy(pclData,olListOfData);
		olRc = CedaAction("URT",pclSelection,"",pclData);
		prpCca->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_DELETED:
		{
			prpCca->Lstu = olTime;
			strcpy(prpCca->Useu , olUser);

			prpCca->Ckba = TIMENULL;
			prpCca->Ckbs = TIMENULL;
			prpCca->Ckea = TIMENULL;
			prpCca->Ckes = TIMENULL;

			strcpy(prpCca->Ckic , "");
			strcpy(prpCca->Ckif , "");
			strcpy(prpCca->Ckit , "");

			sprintf(pclSelection, "WHERE URNO = %ld", prpCca->Urno);
			MakeCedaData(&omRecInfo,olListOfData,prpCca);
			strcpy(pclData,olListOfData);
			olRc = CedaAction("URT",pclSelection,"",pclData);
			prpCca->IsChanged = DATA_UNCHANGED;
			
			
			
			/*
			
			CString olFieldList = "CKBA,CKBS,CKEA,CKES,CKIC,CKIF,CKIT,COPN,CTYP,PRFL";
			char pclFieldList[256];
			CString olData = olUser + CString(",") + olCurrTime  + CString(", , , , , , , , , , ");
			strcpy(pclData, olData);
			strcpy(pclFieldList, olFieldList);
			sprintf(pclSelection, "WHERE URNO = %ld", prpCca->Urno);

			olRc = CedaAction("URT",pcmTableName, pclFieldList, pclSelection, "", pclData, "BUF1");


			CString olFieldList = "";
			char pclFieldList[256];
			CString olData = CString("");
			strcpy(pclData, olData);
			strcpy(pclFieldList, olFieldList);
			sprintf(pclSelection, "WHERE URNO = %ld", prpCca->Urno);

			olRc = CedaAction("DRT",pcmTableName, pclFieldList, pclSelection, "", pclData, "BUF1");
			*/
		}
		break;
	}
    return true;
}





bool CedaCcaData::UpdateSpecial(CCSPtrArray<CCADATA> &opCca, CCSPtrArray<CCADATA> &opCcaSave,long lpFlightUrno, long lpBaseID)
{
	//char buffer[64];
	CString olListOfData;
	char pclSelection[512];
	char pclData[1024];
	CString olFieldList = CString(pcmFieldList);
	char pclFieldList[256];
	CTime olTime = CTime::GetCurrentTime();
	ogBasicData.LocalToUtc(olTime);


	if(lpFlightUrno <= 0)
		return false;

	for(int i =  0; i < opCca.GetSize(); i++)
	{
//		if(opCca[i].Urno != 0 && opCca[i].IsChanged != DATA_NEW && strlen(opCca[i].Ckic) == 0 && 
//			opCca[i].Ckes == TIMENULL && opCca[i].Ckbs == TIMENULL)
		CString olCic = CString(opCca[i].Ckic);
		olCic.TrimLeft();
		if (opCca[i].Urno != 0 && opCca[i].IsChanged != DATA_NEW && olCic.IsEmpty())
		{
			ogDataSet.DeAssignCca(opCca[i].Urno);

			// delete demand record from DB !!
			olFieldList = "";
			olListOfData = "";
			sprintf(pclSelection, "WHERE URNO = %ld", opCca[i].Urno);
			strcpy(pclData,olListOfData);
			strcpy(pclFieldList, olFieldList);

			if (lpBaseID > 0)
				CedaAction("DRT",pcmTableName, pclFieldList, pclSelection, "", pclData, "BUF1", false, lpBaseID);
			else
				CedaAction("DRT",pcmTableName, pclFieldList, pclSelection, "", pclData, "BUF1");


			continue;
		}


		if (!(strlen(opCca[i].Ckic) != 0 || 
			 (i < opCcaSave.GetSize() && strlen(opCcaSave[i].Ckic) != 0) ) )
			continue; // do not save !new! demands !!

		opCca[i].Flnu = lpFlightUrno;
		if((opCca[i].Urno == 0) || ( i >= opCcaSave.GetSize()))
		{
			if((strlen(opCca[i].Ckic) != 0) || (opCca[i].Ckes != TIMENULL)  || (opCca[i].Ckbs != TIMENULL))
			{
				opCca[i].Urno = ogBasicData.GetNextUrno();	
				strcpy(opCca[i].Usec, pcgUser);
				opCca[i].Cdat = olTime;
				MakeCedaData(&omRecInfo,olListOfData, &opCca[i]);
				strcpy(pclData,olListOfData);

				if (lpBaseID > 0)
					CedaAction("IRT","","",pclData, "BUF1", false, lpBaseID);
				else
					CedaAction("IRT","","",pclData);

				if (opCca[i].IsOpen())
					opCca[i].UpdateFIDSMonitor();
			}
		}
		else
		{
			if((strlen(opCca[i].Ckic) != 0) || (opCca[i].Ckes != TIMENULL)  || (opCca[i].Ckbs != TIMENULL))
			{

				if (opCca[i].IsChanged == DATA_NEW)
				{
					strcpy(opCca[i].Usec, pcgUser);
					opCca[i].Cdat = olTime;
					MakeCedaData(&omRecInfo,olListOfData, &opCca[i]);
					strcpy(pclData,olListOfData);
					if (lpBaseID > 0)
						CedaAction("IRT","","",pclData, "BUF1", false, lpBaseID);
					else
						CedaAction("IRT","","",pclData);

					if (opCca[i].IsOpen())
						opCca[i].UpdateFIDSMonitor();

				}
				else
				{
					olFieldList = CString(pcmFieldList);

					MakeCedaData(&omRecInfo, olListOfData, olFieldList, &opCcaSave[i], &opCca[i]);
					if(!olFieldList.IsEmpty())
					{
						strcpy(opCca[i].Useu, pcgUser);
						opCca[i].Lstu = olTime;

						olFieldList = pcmFieldList;
						olListOfData.Empty();

						MakeCedaData(&omRecInfo, olListOfData, olFieldList, &opCcaSave[i], &opCca[i]);

						strcpy(pclFieldList, olFieldList);
						strcpy(pclData,olListOfData);
						sprintf(pclSelection, "WHERE URNO = %ld", opCca[i].Urno);
						if (lpBaseID > 0)
							CedaAction("URT",pcmTableName, pclFieldList, pclSelection,"", pclData, "BUF1", false, lpBaseID); 
						else
							CedaAction("URT",pcmTableName, pclFieldList, pclSelection,"", pclData, "BUF1", false); 

						if (opCca[i].IsOpen() != opCcaSave[i].IsOpen())
							opCca[i].UpdateFIDSMonitor();
					}
				}
			}
			else
			{

				opCca[i].Lstu = olTime;
				strcpy(opCca[i].Useu , pcgUser);

				opCca[i].Ckba = TIMENULL;
				opCca[i].Ckbs = TIMENULL;
				opCca[i].Ckea = TIMENULL;
				opCca[i].Ckes = TIMENULL;

				strcpy(opCca[i].Ckic , "");
				strcpy(opCca[i].Ckif , "");
				strcpy(opCca[i].Ckit , "");

				sprintf(pclSelection, "WHERE URNO = %ld", opCca[i].Urno);
				MakeCedaData(&omRecInfo,olListOfData, &opCca[i]);
				strcpy(pclData,olListOfData);
				CedaAction("URT",pclSelection,"",pclData);
				opCca[i].IsChanged = DATA_UNCHANGED;
	
			}
		}
	}
	return true;
}




void CedaCcaData::SetFilter(bool bpCommonOnly, const CString &ropFlnuList)
{
	bmCommonOnly = bpCommonOnly;
	omFlnuList = ropFlnuList;
}


bool CedaCcaData::IsPassFilter(const CCADATA &ropCca) const
{
	if (bmCommonOnly)
	{
		if (ropCca.Ctyp[0] != 'C')
			return false;
	}

	if (!omFlnuList.IsEmpty())
	{
		CString olFlnu;
		olFlnu.Format("%ld", ropCca.Flnu);
		if (omFlnuList.Find(olFlnu) < 0)
			return false;
	}
		
	return true;
}



//--PROCESS-BC---------------------------------------------------------------------------------------------

void  CedaCcaData::ProcessBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{

	if (omSelection.IsEmpty() || vpDataPointer == NULL)
		return;

	struct BcStruct *prlCcaData;			//if(prlCca->Ctyp == "C")
	long llUrno;
	CString olSelection;
	prlCcaData = (struct BcStruct *) vpDataPointer;
	CString olBcFields(prlCcaData->Fields);
	CString olBcData(prlCcaData->Data);
	CCADATA *prlCca;

	long llBaseID = atol(prlCcaData->Tws);

	//if(llBaseID != IDM_COMMONCCA)
	//	return;

	

		if(ipDDXType == BC_CCA_NEW)
		{
			prlCca = new CCADATA;
			GetRecordFromItemList(prlCca, olBcFields.GetBuffer(0), olBcData.GetBuffer(0));

			if (GetCcaByUrno(prlCca->Urno) != NULL)
			{
				// already exist!! only update necessary!
				UpdateInternal(prlCca, true);
				delete prlCca;
				return;
			}

			if(prlCca->Urno > 0 && IsPassFilter(*prlCca))
			{
				InsertInternal(prlCca);
				return;
				/*
				char pclSelection[512];

 				sprintf(pclSelection, "WHERE URNO=%ld AND %s", prlCca->Urno, omSelection);

				Read(pclSelection, false, true);
				*/
			}
			delete prlCca;
		}

		if(ipDDXType == BC_CCA_CHANGE)
		{
			CString olUrno;
			long llUrno;
			if (GetListItemByField(olBcData, olBcFields, CString("URNO"), olUrno))
			{
				llUrno = atol(olUrno);
			}
			else
			{
				llUrno = MyGetUrnoFromSelection(prlCcaData->Selection);
			}

			CCADATA *prlCca = GetCcaByUrno(llUrno);
			if (prlCca != NULL)
			{
				CString olOldCkic(prlCca->Ckic);
				bool blRead = FillRecord((void *)prlCca, olBcFields, olBcData);
	
				if (blRead)
				{
					omCkicMap.RemoveKey(olOldCkic);
					omCkicMap.SetAt(prlCca->Ckic, prlCca);
					ogDdx.DataChanged((void *)this, imDDXChange, (void *)prlCca); //Update Viewer
				}
				else
				{
					char pclSelection[512];
					sprintf(pclSelection, "WHERE URNO=%ld AND %s", prlCca->Urno, omSelection);
					Read(pclSelection, false, true);
				}
			}
		}
		if(ipDDXType == BC_CCA_DELETE)
		{
			olSelection = (CString)prlCcaData->Selection;
			if (olSelection.Find('\'') != -1)
			{
				llUrno = GetUrnoFromSelection(prlCcaData->Selection);
			}
			else
			{
				int ilFirst = olSelection.Find("=")+1;
				int ilLast  = olSelection.GetLength();
				llUrno = atol(olSelection.Mid(ilFirst,ilLast-ilFirst));
			}
			prlCca = GetCcaByUrno(llUrno);
			if (prlCca != NULL)
			{
				DeleteInternal(prlCca);
			}
		}
	
}

//---------------------------------------------------------------------------------------------------------


