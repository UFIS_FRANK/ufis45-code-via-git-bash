#ifndef AFX_REPORTMOVEMENTS_H__CDB257E2_8BF5_11D1_8127_0000B43C4B01__INCLUDED_
#define AFX_REPORTMOVEMENTS_H__CDB257E2_8BF5_11D1_8127_0000B43C4B01__INCLUDED_

// ReportSortDlg.h : Header-Datei
//
#include "CCSEdit.h"
#include "resrc1.h"

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld CReportMovementsDlg 

class CReportMovementsDlg : public CDialog
{
// Konstruktion
public:
	CReportMovementsDlg(CWnd* pParent = NULL, CString opHeadline = "", CTime* oMinDate = NULL, CTime* oMaxDate = NULL, int *opSelect = 0);

	int *pimSelect;

// Dialogfelddaten
	//{{AFX_DATA(CReportMovementsDlg)
	enum { IDD = IDD_REPORTMOVETIME };
	CCSEdit	m_CE_Datumvon;
	CCSEdit	m_CE_Datumbis;
	CButton	m_CR_List1;
	int		m_List1;
	CString	m_Datumbis;
	CString	m_Datumvon;
	//}}AFX_DATA
	CString omHeadline;
	CTime *pomMinDate;
	CTime *pomMaxDate;

// Überschreibungen
	// Vom Klassen-Assistenten generierte virtuelle Funktionsüberschreibungen
	//{{AFX_VIRTUAL(CReportMovementsDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	//}}AFX_VIRTUAL

// Implementierung
protected:

	// Generierte Nachrichtenzuordnungsfunktionen
	//{{AFX_MSG(CReportMovementsDlg)
	virtual void OnCancel();
	virtual void OnOK();
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio fügt zusätzliche Deklarationen unmittelbar vor der vorhergehenden Zeile ein.

#endif // AFX_REPORTSORTDLG_H__CDB257E2_8BF5_11D1_8127_0000B43C4B01__INCLUDED_
