#if !defined(AFX_ROTATIONTABLEVIEWER_H__32702FA1_573B_11D1_830D_0080AD1DC701__INCLUDED_)
#define AFX_ROTATIONTABLEVIEWER_H__32702FA1_573B_11D1_830D_0080AD1DC701__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// RotationTableViewer.h : header file
//
 
#include "CCS3DStatic.h"
#include "CCSTable.h"
#include "RotationCedaFlightData.h"
#include "CViewer.h"
#include "CCSPrint.h" // BWi
class RotationTableChart;


struct ROTATIONTABLE_LINEDATA
{
	long		Urno;
	CString		Flno;
	CString		Csgn;
	CString		Org4;
	CString		Vial;
	CString		Vian;
	CString		Ifra;
	CString		Ttyp;
	CTime		Stoa;
	CTime		Etai;
	CTime		Etoa;
	CTime		Tmoa;
	CTime		Land;
	CString		Rwya;
	CTime		Onbe;
	CTime		Onbl;
	CString		Psta;
	CString		Gta1;
	CString		Regn;
	CString		Act5;
	CString		Chgi;
	CString		Isre;
	CString		Remp;
	CString		Des4;
	CString		Ifrd;
	CTime		Stod;
	CTime		Etdi;
	CTime		Etod;
	CTime		Ofbl;
	CTime		Slot;
	CString		Rwyd;
	CTime		Airb;
	CString		Pstd;
	CString		Gtd1;
	CString		Gtd2;
	CString		Blt1;
	CString		Ftyp;
	CString		Cic;
	CTime		Etdc;
	// Fields to sort the tablelines
	CTime		Tifa;
	CTime		Tifd;
	CString		Rem1;
	CString		Rem2;
	ROTATIONTABLE_LINEDATA(void)
	{ 
		Urno = 0;
		Stoa = -1;
		Etai = -1;
		Etoa = TIMENULL;
		Tmoa = -1;
		Land = -1;
		Onbe = -1;
		Onbl = -1;
		Stod = -1;
		Etdi = -1;
		Etod = TIMENULL;
		Ofbl = -1;
		Slot = -1;
		Airb = -1;
		Etdc = -1;
	};
};

struct GROUP_DATA
{
	int					GroupID;
	CString				GroupText;
	CCSTable			*Table;
	RotationTableChart	*Chart;
	CCS3DStatic			*TopScaleText;
	CCSPtrArray<ROTATIONTABLE_LINEDATA> Lines;
};


enum 
{
	ADD_LINE,
	INSERT_LINE,
	CHANGE_LINE
};



/////////////////////////////////////////////////////////////////////////////
// RotationTableViewer view

class RotationTableViewer : public CViewer
{
public:
	RotationTableViewer();
	~RotationTableViewer();

// Attributes
public:
	GROUP_DATA rmGroup;
	CCSPtrArray<GROUP_DATA> omGroups;
	CCSPtrArray <TABLE_HEADER_COLUMN> omPrintHeadHeaderArray; // BWi
	CCSPrint *pomPrint; // BWi

	CFont *pomFont;
	CFont *pomDefaultFont;
	CFont *pomUserFont;

	void SetUserFont(LPLOGFONT lpLogFont);

	
	void ChangeViewTo(const char *pcpViewName);

	bool SelectFlightLine(long lpUrno);
	bool ShowFlightLine(long lpUrno);

	void SetTableSort();
	
	void AddGroup(int GroupID, CString opGroupText, CCSTable *popTable, RotationTableChart *popChart, CCS3DStatic *pomTopScaleText);

	bool IsPassFilter(ROTATIONFLIGHTDATA *prpFlight);

	int  GetLinePos(int ipGroupID, ROTATIONTABLE_LINEDATA &rpLine);

	void SetTopScaleText(int ipGroupID);

	GROUP_DATA *GetGroupData(int ipGroupID);

	bool GetGroupAndLineByUrno(ROTATIONFLIGHTDATA *prpFlight, int &ipGroupID1, int &ipLineNo1, int &ipGroupID2, int &ipLineNo2);

	bool GetGroup(ROTATIONFLIGHTDATA *prpFlight, int &ipGroup1, int &ipGroup2);

	int GetNext(int ipGroupID);

	void ShowNextLines();

	void ProcessFlightBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

	void ProcessFlightChange(ROTATIONFLIGHTDATA *prpFlight);
	void ProcessFlightDelete(ROTATIONFLIGHTDATA *prpFlight);

	void MakeLines();
	void MakeLine(ROTATIONFLIGHTDATA *prpFlight, ROTATIONTABLE_LINEDATA &rpLine);
	int  CreateLine(int ipGroupID, ROTATIONTABLE_LINEDATA &rpLine);
	void DeleteLine(int ipGroupID, int ipLineNo);
	void ChangeLine(int ipGroupID, ROTATIONTABLE_LINEDATA &rpLine, int ipLineNo = -1);

	void MakeAndShowLine(ROTATIONFLIGHTDATA *prpFlight);

	void ShowAllTables();
	void ShowTable(int ipGroupID);
	void ShowTableHeader(int ipGroupID);
	void ShowTableLines(int ipGroupID);
	bool ShowTableLine(int ipGroupID, int ipLineNo, int ipModus, bool bpReadAll = false);
	void DeleteAll();



	int CompareFlight(int ipGroup, ROTATIONTABLE_LINEDATA *prpLine1, ROTATIONTABLE_LINEDATA *prpLine2);

	void Repaint();

	//Print
	void PrintAllTableViews(void); // BWi
	void GetHeader(int ipGroupID); // BWi
	void PrintTableView(int ipGroupID); // BWi
	bool PrintTableHeader(int ipGroupID); // BWi
	void PrintTableLine(ROTATIONTABLE_LINEDATA *prpLine,bool bpLastLine,int ipGroupID); // BWi
	bool CreateExcelFile(CString opTrenner);

    CString omFileName;


// Operations
public:
	bool bmAutoNow;

// Overrides

// Implementation
public:
	BOOL CheckPostFlight(const ROTATIONTABLE_LINEDATA *prpTableLine);
	BOOL CheckPostFlight(const ROTATIONFLIGHTDATA *prpAFlight, const ROTATIONFLIGHTDATA *prpDFlight);

private:
	bool GetFieldEditable(ROTATIONTABLE_LINEDATA* prpLine, CString& opStrFieldName);
	bool PrintExcelHeader(ofstream &of, CString opTrenner, int ipGroupID, int ipLines);
	void PrintExcelLine(ofstream &of, ROTATIONTABLE_LINEDATA *prpLine, CString opTrenner, int ipGroupID);
	void PrintExcelLineArr(ofstream &of, ROTATIONTABLE_LINEDATA *prpLine, CString opTrenner);
	void PrintExcelLineArrGround(ofstream &of, ROTATIONTABLE_LINEDATA *prpLine, CString opTrenner);
	void PrintExcelLineDep(ofstream &of, ROTATIONTABLE_LINEDATA *prpLine, CString opTrenner);
	void PrintExcelLineDepGround(ofstream &of, ROTATIONTABLE_LINEDATA *prpLine, CString opTrenner);
	bool PrintExcelHeaderArrGround(ofstream &of, CString opTrenner);
	bool PrintExcelHeaderArr(ofstream &of, CString opTrenner);
	bool PrintExcelHeaderDepGround(ofstream &of, CString opTrenner);
	bool PrintExcelHeaderDep(ofstream &of, CString opTrenner);
	bool CleanSteurzeichen (char *opChar, CString &opString);

    CString omTableName;
    CString omFooterName;

protected:
	CStringArray omSort;

};

/////////////////////////////////////////////////////////////////////////////


#endif // !defined(AFX_ROTATIONTABLEVIEWER_H__32702FA1_573B_11D1_830D_0080AD1DC701__INCLUDED_)
