#ifndef AFX_ROTATIONVIADLG_H__6858C611_6588_11D1_80FD_0000B43C4B01__INCLUDED_
#define AFX_ROTATIONVIADLG_H__6858C611_6588_11D1_80FD_0000B43C4B01__INCLUDED_

// RotationViaDlg.h : Header-Datei
//

#include "CCSTable.h" 
#include "CCSPtrArray.h" 
#include "CCSEdit.h" 
#include "resrc1.h" 
/////////////////////////////////////////////////////////////////////////////
class RotationViaDlg : public CDialog
{
public:
	RotationViaDlg(CWnd* pParent, CString opAdid, CString opFlno, long lpUrnoFlight, CString opList, CTime opRefDat, bool bpLocalTime = true, bool bpEnable = true);
	~RotationViaDlg();

// Dialogfelddaten
	//{{AFX_DATA(RotationViaDlg)
	enum { IDD = IDD_ROTATION_VIA };
	CStatic	m_CS_GMBorder;
	//}}AFX_DATA

	void Enable(bool bpEnable);
	void SetSecState(char opSecState);
	void SetData   (CString opAdid, CString opFlno, long lpUrnoFlight, CString opList, CTime opRefDat, bool bpLocalTime);
	void GetViaList(char *pcpViaList);
	CString GetToolTip();
	CString GetStatus();
	void SetRefDate(CTime opRefDat);

// Überschreibungen
	// Vom Klassen-Assistenten generierte virtuelle Funktionsüberschreibungen
	//{{AFX_VIRTUAL(RotationViaDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	//}}AFX_VIRTUAL

// Implementierung
protected:
	// Generierte Nachrichtenzuordnungsfunktionen
	//{{AFX_MSG(RotationViaDlg)
	virtual BOOL OnInitDialog();
	virtual void OnCancel();
	virtual void OnOK();
    afx_msg LONG OnTableLButtonDown(UINT wParam, LONG lParam);
    afx_msg LONG OnTableLButtonDblclk(UINT wParam, LONG lParam);
	afx_msg LONG OnTableMenuSelect( UINT wParam, LPARAM lParam);
	afx_msg LONG OnTableIPEditKillfocus( UINT wParam, LPARAM lParam);
	afx_msg LONG OnEditChanged( UINT wParam, LPARAM lParam);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

private:
	CWnd* pomParent;
	CCSTable *pomTable;
	bool bmLocal;
	bool bmEnable;
	CString omViaList;
	CTime omRefDat;
	long omUrnoFlight;
	CStringArray omFields;
	CString omCaption;
	CString omAdid;
	CString omFlno;
	char omSecState;

	CString GetField(VIADATA *prpVia, CString opField);
	void GetAttrib(CCSEDIT_ATTRIB &ropAttrib, CString opField);
	void SetField(VIADATA *prpVia, CString opField, CString opValue, CTime opRefDat = TIMENULL);
	CString CreateViaList(CCSPtrArray<VIADATA> *popVias, CTime opRefDat = TIMENULL);
	void SetViaList(CString opAdid, CString opFlno, long lpUrnoFlight, CString opList, CTime opRefDat, bool bpLocalTime);

	void InitTable();
	void DrawHeader();
	void ShowTable();

private:
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio fügt zusätzliche Deklarationen unmittelbar vor der vorhergehenden Zeile ein.

#endif // AFX_RotationViaDlg_H__6858C611_6588_11D1_80FD_0000B43C4B01__INCLUDED_
