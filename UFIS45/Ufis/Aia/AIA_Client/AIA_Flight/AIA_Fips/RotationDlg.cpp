// RotationDlg.cpp : implementation file
//
// Modification History: 
// 16-nov-00	rkr		OnAgent ge�ndert (PRF Athen)
// 17-nov-00	rkr		Aufruf vom Agentdialog unabh�ngig von Datenstruktur
// 22-nov-00	rkr		HandlePostFlight() and CheckPostFlight() 
//						for handling postflights added;
#include "stdafx.h"
#include <process.h>
#include "fpms.h"
#include "RotationDlg.h"
#include "CCSDdx.h"
#include "CCSTime.h"
#include "CCSTable.h"
#include "CCSGlobl.h"
#include "RotationTableViewer.h"
#include "RotationDlgCedaFlightData.h"
#include "RotationJoinDlg.h"
#include "ButtonListDlg.h"
#include "SeasonDlgCedaFlightData.h"
#include "PrivList.h"
#include "RotationRegnDlg.h"
#include "RotationAltDlg.h"
#include "RotationActDlg.h"
#include "RotationAptDlg.h"
#include "RotGroundDlg.h"
#include "RotationHAIDlg.h"
#include "RotationGpuDlg.h"
#include "RotationLoadDlg.h"
#include "RotationLoadDlgATH.h"
#include "SeasonDlg.h"
#include "Utils.h"

#include "CedaBasicData.h"
#include "AwBasicDataDlg.h"
#include "RotationVipDlg.h"
#include "CedaCountData.h"
#include "AskBox.h"
#include "CedaFlightUtilsData.h"
#include "CcaCedaFlightData.H"
#include "ChangeRegistraion.H"
#include "CedaAptLocalUtc.h"
#include "RotationViaDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif



int RotationDlg::CompareCcas(const CCADATA **e1, const CCADATA **e2)
{
	if (strlen((**e1).Ckic) == 0 && (**e1).Ckbs == TIMENULL)
		return 1;
	if (strlen((**e2).Ckic) == 0 && (**e2).Ckbs == TIMENULL)
		return -1;

	if (strlen((**e1).Ckic) == 0)
		return 1;
	if (strlen((**e2).Ckic) == 0)
		return -1;

	if (strcmp((**e1).Ckic, (**e2).Ckic) > 0)
		return 1;
	if (strcmp((**e1).Ckic, (**e2).Ckic) < 0)
		return -1;

	return 0;
}


int RotationDlg::CompareRotationFlight(const ROTATIONDLGFLIGHTDATA **e1, const ROTATIONDLGFLIGHTDATA **e2)
{

	if((**e2).Adid[0] == 'B' && (**e1).Adid[0] == 'B')
	{
		CTime olTime1;
		CTime olTime2;

		if((**e1).Tifa == TIMENULL)
			olTime1 = (**e1).Tifd;
		else
			olTime1 = (**e1).Tifa;

		if((**e2).Tifa == TIMENULL)
			olTime2 = (**e2).Tifd;
		else
			olTime2 = (**e2).Tifa;

		return ((olTime1 == olTime2) ? 0 : ((olTime1 > olTime2) ? 1 : -1));
	}
	else
	{
		return strcmp((**e1).Adid, (**e2).Adid);

	}

}




/////////////////////////////////////////////////////////////////////////////
// RotationDlg dialog


// Local function prototype
static void FlightTableCf(void *popInstance, int ipDDXType,
    void *vpDataPointer, CString &ropInstanceName);


RotationDlg::RotationDlg(CWnd* pParent)
	: omCcaData(CCA_ROTDLG_NEW, CCA_ROTDLG_CHANGE, CCA_ROTDLG_DELETE), CDialog(RotationDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(RotationDlg)
	m_Regn = _T("");
	m_Act3 = _T("");
	m_Act5 = _T("");
	m_Ming = _T("");
	m_Paid = FALSE;
	m_AAirb = _T("");
	m_AFltn = _T("");
	m_AFlns = _T("");
	m_AFlti = _T("");
	m_AB1ba = _T("");
	m_AB1ea = _T("");
	m_AB2ba = _T("");
	m_AB2ea = _T("");
	m_ABlt1 = _T("");
	m_ABlt2 = _T("");
	m_ACxx = FALSE;
	m_ADcd1 = _T("");
	m_ADcd2 = _T("");
	m_ADiverted = FALSE;
	m_ADtd1 = _T("");
	m_ADtd2 = _T("");
	m_AEtai = _T("");
	m_AEtoa = _T("");
	m_AExt1 = _T("");
	m_AExt2 = _T("");
	m_AGa1x = _T("");
	m_AGa2x = _T("");
	m_AGa1y = _T("");
	m_AGa2y = _T("");
	m_AGta1 = _T("");
	m_AGta2 = _T("");
//	m_AHtyp = _T("");
	m_AIfra = _T("");
	m_ALand = _T("");
	m_ALastChange = _T("");
	m_ALastChangeTime = _T("");
	m_ANxti = _T("");
	m_AOfbl = _T("");
	m_AOnbe = _T("");
	m_AOnbl = _T("");
	m_APsta = _T("");
	m_ARem1 = _T("");
	m_ARemp = _T("");
	m_ARerouted = FALSE;
	m_ARwya = _T("");
	m_AStev = _T("");
	m_AStoa = _T("");
	m_AStod = _T("");
	m_AStyp = _T("");
	m_ATga1 = _T("");
	m_ATga2 = _T("");
	m_ATmb1 = _T("");
	m_ATmb2 = _T("");
	m_ATmoa = _T("");
	m_ATtyp = _T("");
	m_DAirb = _T("");
	m_DAlc3 = _T("");
	m_DCxx = FALSE;
	m_DDcd1 = _T("");
	m_DDcd2 = _T("");
	m_DDes3 = _T("");
	m_DDes4 = _T("");
	m_DDiverted = FALSE;
	m_DDtd1 = _T("");
	m_DDtd2 = _T("");
	m_DEtdc = _T("");
	m_DEtdi = _T("");
	m_DEtod = _T("");
	m_DFlns = _T("");
	m_DGd1x = _T("");
	m_DGd1y = _T("");
	m_DGd2x = _T("");
	m_DGd2y = _T("");
	m_DGtd1 = _T("");
	m_DGtd2 = _T("");
//	m_DHtyp = _T("");
	m_DIfrd = _T("");
	m_DIskd = _T("");
	m_DLastChange = _T("");
	m_DLastChangeTime = _T("");
	m_DNxti = _T("");
	m_DOfbl = _T("");
	m_DPdba = _T("");
	m_DPstd = _T("");
	m_DRem1 = _T("");
	m_DRemp = _T("");
	m_DRerouted = FALSE;
	m_DRwyd = _T("");
	m_DSlot = _T("");
	m_DStev = _T("");
	m_DStoa = _T("");
	m_DStod = _T("");
	m_DStyp = _T("");
	m_DTgd1 = _T("");
	m_DTgd2 = _T("");
	m_DTtyp = _T("");
	m_DTwr1 = _T("");
	m_DWro1 = _T("");
	m_ADes3 = _T("");
	m_AOrg3 = _T("");
	m_ATet1 = _T("");
	m_ATet2 = _T("");
	m_DPdea = _T("");
	m_DW1ba = _T("");
	m_DW1ea = _T("");
	m_APaea = _T("");
	m_DOrg3 = _T("");
	m_AAlc3 = _T("");
	m_DFltn = _T("");
	m_DFlti = _T("");
//	m_APaba = _T("");
	m_ACsgn = _T("");
	m_DCsgn = _T("");
	m_ABbaa = _T("");
	m_DBbfa = _T("");
	m_DBaz1 = _T("");
	m_DBaz4 = _T("");
	m_DBao1 = _T("");
	m_DBac1 = _T("");
	m_DBao4 = _T("");
	m_DBac4 = _T("");
	m_DPdba = _T("");
	m_ADivr = _T("");
	m_DDivr = _T("");
	m_AEtdi = _T("");
//first/lastBag
	m_AFBag = _T("");
	m_ALBag = _T("");
	m_GoAround = FALSE;
	//}}AFX_DATA_INIT




	 pomAJfnoTable = NULL;
	 pomDJfnoTable = NULL;
	 pomDCinsTable = NULL;

	polRotationAViaDlg = NULL;
	polRotationDViaDlg = NULL;

	 prmDFlight = NULL;
	 prmAFlight = NULL;
	 prmDFlightSave = NULL;
	 prmAFlightSave = NULL;

	pomParent = pParent;


	bmDlgEnabled = true;

	bmIsAFfnoShown = false;
	bmIsDFfnoShown = false;

	bmDeparture = false;
	bmArrival = false;
	bmIsCheckInNew = true;


	pomAJfnoTable = new CCSTable;
    pomDJfnoTable = new CCSTable;
	pomDCinsTable = new CCSTable;	



	prmAFlight = new ROTATIONDLGFLIGHTDATA;
	prmDFlight = new ROTATIONDLGFLIGHTDATA;
	prmAFlightSave = new ROTATIONDLGFLIGHTDATA;
	prmDFlightSave = new ROTATIONDLGFLIGHTDATA;


    CDialog::Create(RotationDlg::IDD, NULL);

	m_key = "DialogPosition\\DailyDialog";

//	SetWindowPos(&wndTop,0,0,0,0, SWP_NOMOVE | SWP_HIDEWINDOW | SWP_NOSIZE);

	//ModifyStyle();
	
}


bool RotationDlg::ConvertStructUtcTOLocal(ROTATIONDLGFLIGHTDATA *prpAFlight, ROTATIONDLGFLIGHTDATA *prpDFlight)
{
	if (!prpAFlight && !prpDFlight)
		return false;

	if (prpAFlight)
	{
		ogRotationDlgFlights.StructUtcToLocal((void*)prpAFlight, "");
		if (bgRealLocal)
		{
			ConvertLocalTOUtc (prpAFlight, true, true);
			ConvertUtcTOLocal (prpAFlight, true);
		}
	}
	if (prpDFlight)
	{
		ogRotationDlgFlights.StructUtcToLocal((void*)prpDFlight, "");
		if (bgRealLocal)
		{
			ConvertLocalTOUtc (prpDFlight, false, true);
			ConvertUtcTOLocal (prpDFlight, false);
		}
	}

	return true;
}

bool RotationDlg::ConvertStructLocalTOUtc(ROTATIONDLGFLIGHTDATA *prpAFlight, ROTATIONDLGFLIGHTDATA *prpDFlight)
{
	if (!prpAFlight && !prpDFlight)
		return false;

	if (prpAFlight)
	{
		ogRotationDlgFlights.StructLocalToUtc((void*)prpAFlight, "");
		if (bgRealLocal)
		{
			ConvertUtcTOLocal (prpAFlight, true, true);
			ConvertLocalTOUtc (prpAFlight, true);
		}
	}
	if (prpDFlight)
	{
		ogRotationDlgFlights.StructLocalToUtc((void*)prpDFlight, "");
		if (bgRealLocal)
		{
			ConvertUtcTOLocal (prpDFlight, false, true);
			ConvertLocalTOUtc (prpDFlight, false);
		}
	}

	return true;
}


bool RotationDlg::ConvertUtcTOLocal (ROTATIONDLGFLIGHTDATA *prpFlight, bool bpArr, bool bpHopo)
{
	if (!prpFlight)
		return false;

	if (bpArr)
	{
		if (bpHopo)
		{
			CedaAptLocalUtc::AptUtcToLocal (prpFlight->Stod, CString(pcgHome4));
			CedaAptLocalUtc::AptUtcToLocal (prpFlight->Airb, CString(pcgHome4));
			CedaAptLocalUtc::AptUtcToLocal (prpFlight->Airu, CString(pcgHome4));
			CedaAptLocalUtc::AptUtcToLocal (prpFlight->Ofbl, CString(pcgHome4));
			CedaAptLocalUtc::AptUtcToLocal (prpFlight->Ofbu, CString(pcgHome4));
			CedaAptLocalUtc::AptUtcToLocal (prpFlight->Etdi, CString(pcgHome4));
			CedaAptLocalUtc::AptUtcToLocal (prpFlight->Etdu, CString(pcgHome4));
		}
		else
		{
			CedaAptLocalUtc::AptUtcToLocal (prpFlight->Stod, CString(prpFlight->Org4));
			CedaAptLocalUtc::AptUtcToLocal (prpFlight->Airb, CString(prpFlight->Org4));
			CedaAptLocalUtc::AptUtcToLocal (prpFlight->Airu, CString(prpFlight->Org4));
			CedaAptLocalUtc::AptUtcToLocal (prpFlight->Ofbl, CString(prpFlight->Org4));
			CedaAptLocalUtc::AptUtcToLocal (prpFlight->Ofbu, CString(prpFlight->Org4));
			CedaAptLocalUtc::AptUtcToLocal (prpFlight->Etdi, CString(prpFlight->Org4));
			CedaAptLocalUtc::AptUtcToLocal (prpFlight->Etdu, CString(prpFlight->Org4));
		}
	}
	else
	{
		if (bpHopo)
			CedaAptLocalUtc::AptUtcToLocal (prpFlight->Stoa, CString(pcgHome4));
		else
			CedaAptLocalUtc::AptUtcToLocal (prpFlight->Stoa, CString(prpFlight->Des4));
	}


	return true;
}

bool RotationDlg::ConvertLocalTOUtc (ROTATIONDLGFLIGHTDATA *prpFlight, bool bpArr, bool bpHopo)
{
	if (!prpFlight)
		return false;

	if (bpArr)
	{
		if (bpHopo)
		{
			CedaAptLocalUtc::AptLocalToUtc (prpFlight->Stod, CString(pcgHome4));
			CedaAptLocalUtc::AptLocalToUtc (prpFlight->Airb, CString(pcgHome4));
			CedaAptLocalUtc::AptLocalToUtc (prpFlight->Airu, CString(pcgHome4));
			CedaAptLocalUtc::AptLocalToUtc (prpFlight->Ofbl, CString(pcgHome4));
			CedaAptLocalUtc::AptLocalToUtc (prpFlight->Ofbu, CString(pcgHome4));
			CedaAptLocalUtc::AptLocalToUtc (prpFlight->Etdi, CString(pcgHome4));
			CedaAptLocalUtc::AptLocalToUtc (prpFlight->Etdu, CString(pcgHome4));
		}
		else
		{
			CedaAptLocalUtc::AptLocalToUtc (prpFlight->Stod, CString(prpFlight->Org4));
			CedaAptLocalUtc::AptLocalToUtc (prpFlight->Airb, CString(prpFlight->Org4));
			CedaAptLocalUtc::AptLocalToUtc (prpFlight->Airu, CString(prpFlight->Org4));
			CedaAptLocalUtc::AptLocalToUtc (prpFlight->Ofbl, CString(prpFlight->Org4));
			CedaAptLocalUtc::AptLocalToUtc (prpFlight->Ofbu, CString(prpFlight->Org4));
			CedaAptLocalUtc::AptLocalToUtc (prpFlight->Etdi, CString(prpFlight->Org4));
			CedaAptLocalUtc::AptLocalToUtc (prpFlight->Etdu, CString(prpFlight->Org4));
		}
	}
	else
	{
		if (bpHopo)
			CedaAptLocalUtc::AptLocalToUtc (prpFlight->Stoa, CString(pcgHome4));
		else
			CedaAptLocalUtc::AptLocalToUtc (prpFlight->Stoa, CString(prpFlight->Des4));
	}


	return true;
}

void RotationDlg::SetModus(bool bpRotationReadOnly)
{
	if (bmRotationReadOnly != bpRotationReadOnly)
		bmRotationReadOnly = bpRotationReadOnly;
}

bool RotationDlg::NewData(CWnd* pParent, long lpRkey, long lpUrno, CString opAdid, bool bpLocalTime, bool bpRotationReadOnly)
{
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
	
	ClearAll();
	omAdid = opAdid;
	bmLocalTime = bpLocalTime;
	bmRotationReadOnly = bpRotationReadOnly;

	if(lpRkey == 0)
		lpRkey = ogRotationDlgFlights.GetRkey(lpUrno);
	
	pomParent = pParent;
	lmCalledBy = lpUrno;
	lmRkey = lpRkey;

    ogDdx.Register(this, RG_DLG_FLIGHT_CHANGE, CString("FLIGHTDLG"), CString("Flight Update/new"), FlightTableCf);
    ogDdx.Register(this, RG_DLG_ROTATION_CHANGE, CString("FLIGHTDLG"), CString("Flight Update/new"), FlightTableCf);
    ogDdx.Register(this, R_DLG_FLIGHT_CHANGE, CString("FLIGHTDLG"), CString("Flight Update/new"), FlightTableCf);
    ogDdx.Register(this, R_DLG_ROTATION_CHANGE, CString("FLIGHTDLG"), CString("Flight Update/new"), FlightTableCf);
	ogDdx.Register(this, CCA_ROTDLG_NEW,	CString("CCADATA"), CString("Cca-changed"),	FlightTableCf);
	ogDdx.Register(this, CCA_ROTDLG_DELETE,	CString("CCADATA"), CString("Cca-changed"),	FlightTableCf);
	ogDdx.Register(this, CCA_ROTDLG_CHANGE,	CString("CCADATA"), CString("Cca-changed"),	FlightTableCf);
	
	
	ogRotationDlgFlights.ReadRotation( lpRkey, lpUrno, true);

    bool blFlightFound = ProcessRotationChange();
//    bool blFlightFound = true;

	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));

	if (!blFlightFound)
	{
		ogRotationDlgFlights.ClearAll();
	
		ogDdx.UnRegister(this,NOTUSED);
		ClearAll();
 		return false;
	}



	SetWndPos();
	
	bmInit = true;
	
	return true;
}


RotationDlg::~RotationDlg()
{
	ogRotationDlgFlights.ClearAll();
	
	ogDdx.UnRegister(this,NOTUSED);
	ClearAll();

	if(pomAJfnoTable != NULL)
		delete pomAJfnoTable;
	if(pomDJfnoTable != NULL)
		delete pomDJfnoTable;

	if (pomDCinsTable)
		delete pomDCinsTable;


	if(prmDFlight != NULL)
		delete prmDFlight;
	if(prmAFlight != NULL)
		delete prmAFlight;
	if(prmDFlightSave != NULL)
		delete prmDFlightSave;
	if(prmAFlightSave != NULL)
		delete prmAFlightSave;

	if (polRotationAViaDlg)
	{
		polRotationAViaDlg->DestroyWindow();
		delete polRotationAViaDlg;
		polRotationAViaDlg = NULL;
	}

	if (polRotationDViaDlg)
	{
		polRotationDViaDlg->DestroyWindow();
		delete polRotationDViaDlg;
		polRotationDViaDlg = NULL;
	}
}



void RotationDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(RotationDlg)
	DDX_Control(pDX, IDC_AGPU, m_CB_AGpu);
	DDX_Control(pDX, IDC_DGPU, m_CB_DGpu);
	DDX_Control(pDX, IDC_DLOAD, m_CB_DLoad);
	DDX_Control(pDX, IDC_ALOAD, m_CB_ALoad);
	DDX_Control(pDX, IDC_DCICREM4, m_CB_DCicRem4);
	DDX_Control(pDX, IDC_DCICREM3, m_CB_DCicRem3);
	DDX_Control(pDX, IDC_DCICREM2, m_CB_DCicRem2);
	DDX_Control(pDX, IDC_DCICREM1, m_CB_DCicRem1);
	DDX_Control(pDX, IDC_TOWING, m_CB_Towing);
	DDX_Control(pDX, IDC_ADIVR, m_CE_ADivr);
	DDX_Control(pDX, IDC_DDIVR, m_CE_DDivr);
	DDX_Control(pDX, IDC_DRETTAXI, m_CB_DRetTaxi);
	DDX_Control(pDX, IDC_DRETFLIGHT, m_CB_DRetFlight);
	DDX_Control(pDX, IDC_ARETTAXI, m_CB_ARetTaxi);
	DDX_Control(pDX, IDC_ARETFLIGHT, m_CB_ARetFlight);
	DDX_Control(pDX, IDC_DBAZ4, m_CE_DBaz4);
	DDX_Control(pDX, IDC_DBAZ1, m_CE_DBaz1);
	DDX_Control(pDX, IDC_DBAO1, m_CE_DBao1);
	DDX_Control(pDX, IDC_DBAC1, m_CE_DBac1);
	DDX_Control(pDX, IDC_DBAO4, m_CE_DBao4);
	DDX_Control(pDX, IDC_DBAC4, m_CE_DBac4);
	DDX_Control(pDX, IDC_DFLTI, m_CE_DFlti);
	DDX_Control(pDX, IDC_AFLTI, m_CE_AFlti);
	DDX_Control(pDX, IDC_DBBFA, m_CE_DBbfa);
	DDX_Control(pDX, IDC_ABBAA, m_CE_ABbaa);
	DDX_Control(pDX, IDC_DAWI, m_CB_DAwi);
	DDX_Control(pDX, IDC_AAWI, m_CB_AAwi);
	DDX_Control(pDX, IDC_VIP, m_CB_Vip);
	DDX_Control(pDX, IDC_SEASON_MASK, m_CB_SeasonMask);
	DDX_Control(pDX, IDC_DCSGN, m_CE_DCsgn);
	DDX_Control(pDX, IDC_ACSGN, m_CE_ACsgn);
	DDX_Control(pDX, IDC_PAID, m_CB_Paid);
	DDX_Control(pDX, IDC_DTTYPLIST, m_CB_DTtypList);
	DDX_Control(pDX, IDC_DDCD1LIST, m_CB_DDcd1List);
	DDX_Control(pDX, IDC_ATTYPLIST, m_CB_ATtypList);
	DDX_Control(pDX, IDC_DREMP, m_CL_DRemp);
	DDX_Control(pDX, IDC_AREMP, m_CL_ARemp);
	DDX_Control(pDX, IDOK, m_CB_Ok);
	DDX_Control(pDX, IDC_AEXT2LIST, m_CB_AExt2List);
	DDX_Control(pDX, IDC_AEXT1LIST, m_CB_AExt1List);
	DDX_Control(pDX, IDC_ACT35LIST, m_CB_Act35List);
	DDX_Control(pDX, IDC_DPSTDLIST, m_CB_DPstdList);
	DDX_Control(pDX, IDC_DCINSLIST4, m_CB_DCinsList4);
	DDX_Control(pDX, IDC_DCINSLIST3, m_CB_DCinsList3);
	DDX_Control(pDX, IDC_APSTALIST, m_CB_APstaList);
	DDX_Control(pDX, IDC_DWRO1LIST, m_CB_DWro1List);
	DDX_Control(pDX, IDC_DSTODCALC, m_CB_DStodCalc);
	DDX_Control(pDX, IDC_DCINSLIST2, m_CB_DCinsList2);
	DDX_Control(pDX, IDC_DCINSLIST1, m_CB_DCinsList1);
	DDX_Control(pDX, IDC_ASTOACALC, m_CB_AStoaCalc);
	DDX_Control(pDX, IDC_DCINSBORDER, m_CE_DCinsBorder);
	DDX_Control(pDX, IDC_DALC3LIST3, m_CB_DAlc3List3);
	DDX_Control(pDX, IDC_DALC3LIST2, m_CB_DAlc3List2);
	DDX_Control(pDX, IDC_AAlc3LIST3, m_CB_AAlc3List3);
	DDX_Control(pDX, IDC_AAlc3LIST2, m_CB_AAlc3List2);
	DDX_Control(pDX, IDC_STATIC_ANKUNFT, m_CS_Ankunft);
	DDX_Control(pDX, IDC_STATIC_ABFLUG, m_CS_Abflug);
	DDX_Control(pDX, IDC_DGD2X, m_CE_DGd2x);
//	DDX_Control(pDX, IDC_APABA, m_CE_APaba);
	DDX_Control(pDX, IDC_ARWYA, m_CE_ARwya);
	DDX_Control(pDX, IDC_MING, m_CE_Ming);
	DDX_Control(pDX, IDC_DFLTN, m_CE_DFltn);
 	DDX_Control(pDX, IDC_AALC3, m_CE_AAlc3);
	DDX_Control(pDX, IDC_DORG3, m_CE_DOrg3);
	DDX_Control(pDX, IDC_APAEA, m_CE_APaea);
	DDX_Control(pDX, IDC_JOIN, m_CB_Join);
	DDX_Control(pDX, IDC_DW1EA, m_CE_DW1ea);
	DDX_Control(pDX, IDC_DW1BA, m_CE_DW1ba);
	DDX_Control(pDX, IDC_DPDEA, m_CE_DPdea);
	DDX_Control(pDX, IDC_DPDBA, m_CE_DPdba);
	DDX_Control(pDX, IDC_ATET2, m_CE_ATet2);
	DDX_Control(pDX, IDC_ATET1, m_CE_ATet1);
	DDX_Control(pDX, IDC_AORG3, m_CE_AOrg3);
	DDX_Control(pDX, IDC_ADES3, m_CE_ADes3);
	DDX_Control(pDX, IDC_STATUS, m_CB_Status);
	DDX_Control(pDX, IDC_SPLIT, m_CB_Split);
	DDX_Control(pDX, IDC_ROTATION, m_CB_Rotation);
	DDX_Control(pDX, IDC_OBJECT, m_CB_Object);
	DDX_Control(pDX, IDC_DWRO1, m_CE_DWro1);
	DDX_Control(pDX, IDC_DTWR1, m_CE_DTwr1);
	DDX_Control(pDX, IDC_DTTYP, m_CE_DTtyp);
	DDX_Control(pDX, IDC_DTGD2, m_CE_DTgd2);
	DDX_Control(pDX, IDC_DTGD1, m_CE_DTgd1);
	DDX_Control(pDX, IDC_DTelex, m_CB_DTelex);
	DDX_Control(pDX, IDC_DSTYPLIST, m_CB_DStypList);
	DDX_Control(pDX, IDC_DSTYP, m_CE_DStyp);
	DDX_Control(pDX, IDC_DSTOD, m_CE_DStod);
	DDX_Control(pDX, IDC_DSTOA, m_CE_DStoa);
	DDX_Control(pDX, IDC_DSTEV, m_CE_DStev);
	DDX_Control(pDX, IDC_DSLOT, m_CE_DSlot);
	DDX_Control(pDX, IDC_DSHOWJFNO, m_CB_DShowJfno);
	DDX_Control(pDX, IDC_DRWYD, m_CE_DRwyd);
	DDX_Control(pDX, IDC_DREM1, m_CE_DRem1);
	DDX_Control(pDX, IDC_DPSTD, m_CE_DPstd);
	DDX_Control(pDX, IDC_DOFBL, m_CE_DOfbl);
	DDX_Control(pDX, IDC_DNXTI, m_CE_DNxti);
	DDX_Control(pDX, IDC_DLASTCHANGE, m_CE_DLastChange);
	DDX_Control(pDX, IDC_DLASTCHANGETIME, m_CE_DLastChangeTime);
	DDX_Control(pDX, IDC_DJFNOBORDER, m_CE_DJfnoBorder);
	DDX_Control(pDX, IDC_DISKD, m_CE_DIskd);
	DDX_Control(pDX, IDC_DIFRD, m_CE_DIfrd);
//	DDX_Control(pDX, IDC_DHTYPLIST, m_CB_DHtypList);
//	DDX_Control(pDX, IDC_DHTYP, m_CE_DHtyp);
	DDX_Control(pDX, IDC_DGTD2, m_CE_DGtd2);
	DDX_Control(pDX, IDC_DGTD1, m_CE_DGtd1);
	DDX_Control(pDX, IDC_DGTA1LIST, m_CB_DGta1List);
	DDX_Control(pDX, IDC_DGTA2LIST, m_CB_DGta2List);
	DDX_Control(pDX, IDC_DGD2Y, m_CE_DGd2y);
	DDX_Control(pDX, IDC_DGD1Y, m_CE_DGd1y);
	DDX_Control(pDX, IDC_DGD1X, m_CE_DGd1x);
	DDX_Control(pDX, IDC_DFLNS, m_CE_DFlns);
	DDX_Control(pDX, IDC_DETOD, m_CE_DEtod);
	DDX_Control(pDX, IDC_DETDI, m_CE_DEtdi);
	DDX_Control(pDX, IDC_DETDC, m_CE_DEtdc);
	DDX_Control(pDX, IDC_DDTD2, m_CE_DDtd2);
	DDX_Control(pDX, IDC_DDTD1, m_CE_DDtd1);
	DDX_Control(pDX, IDC_DDES3LIST, m_CB_DDes3List);
	DDX_Control(pDX, IDC_DDES3, m_CE_DDes3);
	DDX_Control(pDX, IDC_DDES4, m_CE_DDes4);
	DDX_Control(pDX, IDC_DDCD2LIST, m_CB_DDcd2List);
	DDX_Control(pDX, IDC_DDCD2, m_CE_DDcd2);
	DDX_Control(pDX, IDC_DDCD1, m_CE_DDcd1);
	DDX_Control(pDX, IDC_DCXX, m_CB_DCxx);
	DDX_Control(pDX, IDC_DRerouted, m_CB_DRerouted);
	DDX_Control(pDX, IDC_DDiverted, m_CB_DDiverted);
	DDX_Control(pDX, IDC_DALC3LIST, m_CB_DAlc3List);
	DDX_Control(pDX, IDC_DALC3, m_CE_DAlc3);
	DDX_Control(pDX, IDC_DAIRB, m_CE_DAirb);
	DDX_Control(pDX, IDC_ATTYP, m_CE_ATtyp);
	DDX_Control(pDX, IDC_ATMOA, m_CE_ATmoa);
	DDX_Control(pDX, IDC_ATMB2, m_CE_ATmb2);
	DDX_Control(pDX, IDC_ATMB1, m_CE_ATmb1);
	DDX_Control(pDX, IDC_ATGA2, m_CE_ATga2);
	DDX_Control(pDX, IDC_ATGA1, m_CE_ATga1);
	DDX_Control(pDX, IDC_ATelex, m_CB_ATelex);
	DDX_Control(pDX, IDC_ASTYPLIST, m_CB_AStypList);
	DDX_Control(pDX, IDC_ASTYP, m_CE_AStyp);
	DDX_Control(pDX, IDC_ASTOD, m_CE_AStod);
	DDX_Control(pDX, IDC_ASTOA, m_CE_AStoa);
	DDX_Control(pDX, IDC_ASTEV, m_CE_AStev);
	DDX_Control(pDX, IDC_ASHOWJFNO, m_CB_AShowJfno);
	DDX_Control(pDX, IDC_ARerouted, m_CB_ARerouted);
	DDX_Control(pDX, IDC_AREM1, m_CE_ARem1);
	DDX_Control(pDX, IDC_APSTA, m_CE_APsta);
	DDX_Control(pDX, IDC_AORG3LIST, m_CB_AOrg3List);
	DDX_Control(pDX, IDC_AONBL, m_CE_AOnbl);
	DDX_Control(pDX, IDC_AONBE, m_CE_AOnbe);
	DDX_Control(pDX, IDC_AOFBL, m_CE_AOfbl);
	DDX_Control(pDX, IDC_ANXTI, m_CE_ANxti);
	DDX_Control(pDX, IDC_ALASTCHANGE, m_CE_ALastChange);
	DDX_Control(pDX, IDC_ALASTCHANGETIME, m_CE_ALastChangeTime);
	DDX_Control(pDX, IDC_ALAND, m_CE_ALand);
	DDX_Control(pDX, IDC_AJFNOBORDER, m_CE_AJfnoBorder);
	DDX_Control(pDX, IDC_AIFRA, m_CE_AIfra);
//	DDX_Control(pDX, IDC_AHTYPLIST, m_CB_AHtypList);
//	DDX_Control(pDX, IDC_AHTYP, m_CE_AHtyp);
	DDX_Control(pDX, IDC_AGTA2LIST, m_CB_AGta2List);
	DDX_Control(pDX, IDC_AGTA1LIST, m_CB_AGta1List);
	DDX_Control(pDX, IDC_AGTA2, m_CE_AGta2);
	DDX_Control(pDX, IDC_AGTA1, m_CE_AGta1);
	DDX_Control(pDX, IDC_AGENTS, m_CB_Agents);
	DDX_Control(pDX, IDC_AGA2Y, m_CE_AGa2y);
	DDX_Control(pDX, IDC_AGA1Y, m_CE_AGa1y);
	DDX_Control(pDX, IDC_AGA2X, m_CE_AGa2x);
	DDX_Control(pDX, IDC_AGA1X, m_CE_AGa1x);
	DDX_Control(pDX, IDC_AEXT2, m_CE_AExt2);
	DDX_Control(pDX, IDC_AEXT1, m_CE_AExt1);
	DDX_Control(pDX, IDC_AETOA, m_CE_AEtoa);
	DDX_Control(pDX, IDC_AETAI, m_CE_AEtai);
	DDX_Control(pDX, IDC_ADTD2, m_CE_ADtd2);
	DDX_Control(pDX, IDC_ADTD1, m_CE_ADtd1);
	DDX_Control(pDX, IDC_ADiverted, m_CB_ADiverted);
	DDX_Control(pDX, IDC_ADCD2LIST, m_CB_ADcd2List);
	DDX_Control(pDX, IDC_ADCD2, m_CE_ADcd2);
	DDX_Control(pDX, IDC_ADCD1LIST, m_CB_ADcd1List);
	DDX_Control(pDX, IDC_ADCD1, m_CE_ADcd1);
	DDX_Control(pDX, IDC_ACXX, m_CB_ACxx);
	DDX_Control(pDX, IDC_ABLT2LIST, m_CB_ABlt2List);
	DDX_Control(pDX, IDC_ABLT2, m_CE_ABlt2);
	DDX_Control(pDX, IDC_ABLT1LIST, m_CB_ABlt1List);
	DDX_Control(pDX, IDC_ABLT1, m_CE_ABlt1);
	DDX_Control(pDX, IDC_AB2EA, m_CE_AB2ea);
	DDX_Control(pDX, IDC_AB2BA, m_CE_AB2ba);
	DDX_Control(pDX, IDC_AB1EA, m_CE_AB1ea);
	DDX_Control(pDX, IDC_AB1BA, m_CE_AB1ba);
	DDX_Control(pDX, IDC_AFLNS, m_CE_AFlns);
	DDX_Control(pDX, IDC_AFLTN, m_CE_AFltn);
	DDX_Control(pDX, IDC_AAlc3LIST, m_CB_AAlc3List);
	DDX_Control(pDX, IDC_AAIRB, m_CE_AAirb);
	DDX_Control(pDX, IDC_ACT5, m_CE_Act5);
	DDX_Control(pDX, IDC_ACT3, m_CE_Act3);
	DDX_Control(pDX, IDC_REGN, m_CE_Regn);
	DDX_Text(pDX, IDC_REGN, m_Regn);
	DDX_Text(pDX, IDC_ACT3, m_Act3);
	DDX_Text(pDX, IDC_ACT5, m_Act5);
	DDX_Text(pDX, IDC_MING, m_Ming);
	DDX_Check(pDX, IDC_PAID, m_Paid);
	DDX_Text(pDX, IDC_AAIRB, m_AAirb);
	DDX_Text(pDX, IDC_AFLTN, m_AFltn);
	DDX_Text(pDX, IDC_AFLNS, m_AFlns);
	DDX_Text(pDX, IDC_AFLTI, m_AFlti);
	DDX_Text(pDX, IDC_AB1BA, m_AB1ba);
	DDX_Text(pDX, IDC_AB1EA, m_AB1ea);
	DDX_Text(pDX, IDC_AB2BA, m_AB2ba);
	DDX_Text(pDX, IDC_AB2EA, m_AB2ea);
	DDX_Text(pDX, IDC_ABLT1, m_ABlt1);
	DDX_Text(pDX, IDC_ABLT2, m_ABlt2);
	DDX_Check(pDX, IDC_ACXX, m_ACxx);
	DDX_Text(pDX, IDC_ADCD1, m_ADcd1);
	DDX_Text(pDX, IDC_ADCD2, m_ADcd2);
	DDX_Check(pDX, IDC_ADiverted, m_ADiverted);
	DDX_Check(pDX, IDC_DDiverted, m_DDiverted);
	DDX_Text(pDX, IDC_ADTD1, m_ADtd1);
	DDX_Text(pDX, IDC_ADTD2, m_ADtd2);
	DDX_Text(pDX, IDC_AETAI, m_AEtai);
	DDX_Text(pDX, IDC_AETOA, m_AEtoa);
	DDX_Text(pDX, IDC_AEXT1, m_AExt1);
	DDX_Text(pDX, IDC_AEXT2, m_AExt2);
	DDX_Text(pDX, IDC_AGA1X, m_AGa1x);
	DDX_Text(pDX, IDC_AGA2X, m_AGa2x);
	DDX_Text(pDX, IDC_AGA1Y, m_AGa1y);
	DDX_Text(pDX, IDC_AGA2Y, m_AGa2y);
	DDX_Text(pDX, IDC_AGTA1, m_AGta1);
	DDX_Text(pDX, IDC_AGTA2, m_AGta2);
//	DDX_Text(pDX, IDC_AHTYP, m_AHtyp);
	DDX_Text(pDX, IDC_AIFRA, m_AIfra);
	DDX_Text(pDX, IDC_ALAND, m_ALand);
	DDX_Text(pDX, IDC_ALASTCHANGE, m_ALastChange);
	DDX_Text(pDX, IDC_ALASTCHANGETIME, m_ALastChangeTime);
	DDX_Text(pDX, IDC_ANXTI, m_ANxti);
	DDX_Text(pDX, IDC_AOFBL, m_AOfbl);
	DDX_Text(pDX, IDC_AONBE, m_AOnbe);
	DDX_Text(pDX, IDC_AONBL, m_AOnbl);
	DDX_Text(pDX, IDC_APSTA, m_APsta);
	DDX_Text(pDX, IDC_AREM1, m_ARem1);
	DDX_Check(pDX, IDC_ARerouted, m_ARerouted);
	DDX_Check(pDX, IDC_DRerouted, m_DRerouted);
	DDX_Text(pDX, IDC_ARWYA, m_ARwya);
	DDX_Text(pDX, IDC_ASTEV, m_AStev);
	DDX_Text(pDX, IDC_ASTOA, m_AStoa);
	DDX_Text(pDX, IDC_ASTOD, m_AStod);
	DDX_Text(pDX, IDC_ASTYP, m_AStyp);
	DDX_Text(pDX, IDC_ATGA1, m_ATga1);
	DDX_Text(pDX, IDC_ATGA2, m_ATga2);
	DDX_Text(pDX, IDC_ATMB1, m_ATmb1);
	DDX_Text(pDX, IDC_ATMB2, m_ATmb2);
	DDX_Text(pDX, IDC_ATMOA, m_ATmoa);
	DDX_Text(pDX, IDC_ATTYP, m_ATtyp);
	DDX_Text(pDX, IDC_DAIRB, m_DAirb);
	DDX_Text(pDX, IDC_DALC3, m_DAlc3);
	DDX_Check(pDX, IDC_DCXX, m_DCxx);
	DDX_Text(pDX, IDC_DDCD1, m_DDcd1);
	DDX_Text(pDX, IDC_DDCD2, m_DDcd2);
	DDX_Text(pDX, IDC_DDES3, m_DDes3);
	DDX_Text(pDX, IDC_DDES4, m_DDes4);
	DDX_Text(pDX, IDC_DDTD1, m_DDtd1);
	DDX_Text(pDX, IDC_DDTD2, m_DDtd2);
	DDX_Text(pDX, IDC_DETDC, m_DEtdc);
	DDX_Text(pDX, IDC_DETDI, m_DEtdi);
	DDX_Text(pDX, IDC_DETOD, m_DEtod);
	DDX_Text(pDX, IDC_DFLNS, m_DFlns);
	DDX_Text(pDX, IDC_DGD1X, m_DGd1x);
	DDX_Text(pDX, IDC_DGD1Y, m_DGd1y);
	DDX_Text(pDX, IDC_DGD2X, m_DGd2x);
	DDX_Text(pDX, IDC_DGD2Y, m_DGd2y);
	DDX_Text(pDX, IDC_DGTD1, m_DGtd1);
	DDX_Text(pDX, IDC_DGTD2, m_DGtd2);
//	DDX_Text(pDX, IDC_DHTYP, m_DHtyp);
	DDX_Text(pDX, IDC_DIFRD, m_DIfrd);
	DDX_Text(pDX, IDC_DISKD, m_DIskd);
	DDX_Text(pDX, IDC_DLASTCHANGE, m_DLastChange);
	DDX_Text(pDX, IDC_DLASTCHANGETIME, m_DLastChangeTime);
	DDX_Text(pDX, IDC_DNXTI, m_DNxti);
	DDX_Text(pDX, IDC_DOFBL, m_DOfbl);
	DDX_Text(pDX, IDC_DPDBA, m_DPdba);
	DDX_Text(pDX, IDC_DPSTD, m_DPstd);
	DDX_Text(pDX, IDC_DREM1, m_DRem1);
	DDX_Text(pDX, IDC_DRWYD, m_DRwyd);
	DDX_Text(pDX, IDC_DSLOT, m_DSlot);
	DDX_Text(pDX, IDC_DSTEV, m_DStev);
	DDX_Text(pDX, IDC_DSTOA, m_DStoa);
	DDX_Text(pDX, IDC_DSTOD, m_DStod);
	DDX_Text(pDX, IDC_DSTYP, m_DStyp);
	DDX_Text(pDX, IDC_DTGD1, m_DTgd1);
	DDX_Text(pDX, IDC_DTGD2, m_DTgd2);
	DDX_Text(pDX, IDC_DTTYP, m_DTtyp);
	DDX_Text(pDX, IDC_DTWR1, m_DTwr1);
	DDX_Text(pDX, IDC_DWRO1, m_DWro1);
	DDX_Text(pDX, IDC_ADES3, m_ADes3);
	DDX_Text(pDX, IDC_AORG3, m_AOrg3);
	DDX_Text(pDX, IDC_ATET1, m_ATet1);
	DDX_Text(pDX, IDC_ATET2, m_ATet2);
	DDX_Text(pDX, IDC_DPDEA, m_DPdea);
	DDX_Text(pDX, IDC_DW1BA, m_DW1ba);
	DDX_Text(pDX, IDC_DW1EA, m_DW1ea);
	DDX_Text(pDX, IDC_APAEA, m_APaea);
	DDX_Text(pDX, IDC_DORG3, m_DOrg3);
	DDX_Text(pDX, IDC_AALC3, m_AAlc3);
	DDX_Text(pDX, IDC_DFLTN, m_DFltn);
	DDX_Text(pDX, IDC_DFLTI, m_DFlti);
//	DDX_Text(pDX, IDC_APABA, m_APaba);
	DDX_Text(pDX, IDC_ACSGN, m_ACsgn);
	DDX_Text(pDX, IDC_DCSGN, m_DCsgn);
	DDX_Text(pDX, IDC_ABBAA, m_ABbaa);
	DDX_Text(pDX, IDC_DBBFA, m_DBbfa);
	DDX_Text(pDX, IDC_DBAZ1, m_DBaz1);
	DDX_Text(pDX, IDC_DBAZ4, m_DBaz4);
	DDX_Text(pDX, IDC_DBAO1, m_DBao1);
	DDX_Text(pDX, IDC_DBAC1, m_DBac1);
	DDX_Text(pDX, IDC_DBAO4, m_DBao4);
	DDX_Text(pDX, IDC_DBAC4, m_DBac4);
	DDX_Text(pDX, IDC_ADIVR, m_ADivr);
	DDX_Text(pDX, IDC_DDIVR, m_DDivr);
	DDX_Control(pDX, IDC_AETDI, m_CE_AEtdi);
	DDX_Text(pDX, IDC_AETDI, m_AEtdi);
//first/last Bag
	DDX_Control(pDX, IDC_AFBAG, m_CE_AFBag);
	DDX_Text(pDX, IDC_AFBAG, m_AFBag);
	DDX_Control(pDX, IDC_ALBAG, m_CE_ALBag);
	DDX_Text(pDX, IDC_ALBAG, m_ALBag);
	DDX_Control(pDX, IDC_LOAD_VIEW_ARR, m_CB_Load_View_Arr);
	DDX_Control(pDX, IDC_LOAD_VIEW_DEP, m_CB_Load_View_Dep);
	DDX_Control(pDX, IDC_AVIA, m_CB_AVia);
	DDX_Control(pDX, IDC_DVIA, m_CB_DVia);
	DDX_Control(pDX, IDC_GOAROUND, m_CB_GoAround);
	DDX_Check(pDX, IDC_GOAROUND, m_GoAround);
	DDX_Control(pDX, IDC_COMBO_AHISTORY, m_ComboAHistory);
	DDX_Control(pDX, IDC_COMBO_DHISTORY, m_ComboDHistory);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(RotationDlg, CDialog)
	//{{AFX_MSG_MAP(RotationDlg)
	ON_BN_CLICKED(IDC_VIP, OnVip)
	ON_BN_CLICKED(IDC_SEASON_MASK, OnSeasonMask)
	ON_BN_CLICKED(IDC_ASHOWJFNO, OnAshowjfno)
	ON_BN_CLICKED(IDC_DSHOWJFNO, OnDshowjfno)
	ON_BN_CLICKED(IDC_SPLIT, OnSplit)
	ON_BN_CLICKED(IDC_JOIN, OnJoin)
	ON_BN_CLICKED(IDC_PAID, OnPaid)
	ON_MESSAGE(WM_EDIT_KILLFOCUS, OnEditKillfocus)
	ON_MESSAGE(WM_EDIT_CHANGED, OnEditChanged)
	ON_MESSAGE(WM_TABLE_IPEDIT_KILLFOCUS, OnTableIPEditKillfocus)
	ON_MESSAGE(WM_EDIT_RBUTTONDOWN, OnEditRButtonDown)
	ON_BN_CLICKED(IDC_AAlc3LIST, OnAAlc3LIST)
	ON_BN_CLICKED(IDC_AAlc3LIST2, OnAAlc3LIST2)
	ON_BN_CLICKED(IDC_AAlc3LIST3, OnAAlc3LIST3)
	ON_BN_CLICKED(IDC_ABLT1LIST, OnAblt1list)
	ON_BN_CLICKED(IDC_ABLT2LIST, OnAblt2list)
	ON_BN_CLICKED(IDC_ADCD1LIST, OnAdcd1list)
	ON_BN_CLICKED(IDC_ADCD2LIST, OnAdcd2list)
	ON_BN_CLICKED(IDC_AGTA1LIST, OnAgta1list)
	ON_BN_CLICKED(IDC_AGTA2LIST, OnAgta2list)
//	ON_BN_CLICKED(IDC_AHTYPLIST, OnAhtyplist)
	ON_BN_CLICKED(IDC_AORG3LIST, OnAorg3list)
	ON_BN_CLICKED(IDC_ASTYPLIST, OnAstyplist)
	ON_BN_CLICKED(IDC_ATTYPLIST, OnAttyplist)
	ON_BN_CLICKED(IDC_DALC3LIST, OnDalc3list)
	ON_BN_CLICKED(IDC_DALC3LIST2, OnDalc3list2)
	ON_BN_CLICKED(IDC_DALC3LIST3, OnDalc3list3)
	ON_BN_CLICKED(IDC_DCINSLIST1, OnDcinslist1)
	ON_BN_CLICKED(IDC_DCINSLIST2, OnDcinslist2)
	ON_BN_CLICKED(IDC_DDCD1LIST, OnDdcd1list)
	ON_BN_CLICKED(IDC_DDCD2LIST, OnDdcd2list)
	ON_BN_CLICKED(IDC_DDES3LIST, OnDdes3list)
	ON_BN_CLICKED(IDC_DGTA1LIST, OnDgta1list)
	ON_BN_CLICKED(IDC_DGTA2LIST, OnDgta2list)
//	ON_BN_CLICKED(IDC_DHTYPLIST, OnDhtyplist)
	ON_BN_CLICKED(IDC_DSTYPLIST, OnDstyplist)
	ON_BN_CLICKED(IDC_DTTYPLIST, OnDttyplist)
	ON_BN_CLICKED(IDC_DWRO1LIST, OnDwro1list)
	ON_BN_CLICKED(IDC_DPSTDLIST, OnDpstdlist)
	ON_BN_CLICKED(IDC_APSTALIST, OnApstalist)
	ON_BN_CLICKED(IDC_DCINSLIST3, OnDcinslist3)
	ON_BN_CLICKED(IDC_DCINSLIST4, OnDcinslist4)
	ON_BN_CLICKED(IDC_AEXT1LIST, OnAext1list)
	ON_BN_CLICKED(IDC_AEXT2LIST, OnAext2list)
	ON_BN_CLICKED(IDC_ACT35LIST, OnAct35list)
	ON_BN_CLICKED(IDC_DCXX, OnDcxx)
	ON_BN_CLICKED(IDC_ACXX, OnAcxx)
	ON_BN_CLICKED(IDC_ADiverted, OnADiverted)
	ON_BN_CLICKED(IDC_ARerouted, OnARerouted)
	ON_BN_CLICKED(IDC_DDiverted, OnDDiverted)
	ON_BN_CLICKED(IDC_DRerouted, OnDRerouted)
	ON_CBN_SELCHANGE(IDC_DREMP, OnSelchangeDremp)
	ON_CBN_SELCHANGE(IDC_AREMP, OnSelchangeAremp)
	ON_BN_CLICKED(IDC_ROTATION, OnRotation)
	ON_BN_CLICKED(IDC_DTelex, OnDTelex)
	ON_BN_CLICKED(IDC_ATelex, OnATelex)
	ON_WM_KEYDOWN()
	ON_BN_CLICKED(IDC_STATUS, OnStatus)
	ON_BN_CLICKED(IDC_AGENTS, OnAgent)
	ON_BN_CLICKED(IDC_OBJECT, OnObject)
	ON_BN_CLICKED(IDC_TOWING, OnTowing)
	ON_BN_CLICKED(IDC_AAWI, OnAawi)
	ON_BN_CLICKED(IDC_DAWI, OnDawi)
	ON_BN_CLICKED(IDC_ARETFLIGHT, OnAretflight)
	ON_BN_CLICKED(IDC_ARETTAXI, OnArettaxi)
	ON_BN_CLICKED(IDC_DRETFLIGHT, OnDretflight)
	ON_BN_CLICKED(IDC_DRETTAXI, OnDrettaxi)
	ON_BN_CLICKED(IDC_DCICREM1, OnDcicrem1)
	ON_BN_CLICKED(IDC_DCICREM2, OnDcicrem2)
	ON_BN_CLICKED(IDC_DCICREM3, OnDcicrem3)
	ON_BN_CLICKED(IDC_DCICREM4, OnDcicrem4)
	ON_BN_CLICKED(IDC_AGPU, OnAgpu)
	ON_BN_CLICKED(IDC_DGPU, OnDgpu)
	ON_BN_CLICKED(IDC_ALOAD, OnAload)
	ON_BN_CLICKED(IDC_DLOAD, OnDload)
	ON_MESSAGE(WM_TABLE_IPEDIT, OnTableIPEdit)
	ON_BN_CLICKED(IDC_LOAD_VIEW_ARR, OnAViewLoad)
	ON_BN_CLICKED(IDC_LOAD_VIEW_DEP, OnDViewLoad)
	ON_BN_CLICKED(IDC_AVIA, OnAVia)
	ON_BN_CLICKED(IDC_DVIA, OnDVia)
	ON_NOTIFY_EX( TTN_NEEDTEXT, 0, OnToolTipNotify )
	ON_BN_CLICKED(IDC_GOAROUND, OnGoAround)
	ON_WM_SIZE()
	ON_MESSAGE(WM_EDIT_LBUTTONDBLCLK, OnEditDbClk)
	ON_CBN_SELCHANGE(IDC_COMBO_AHISTORY, OnSelchangeAHistory)
	ON_CBN_SELCHANGE(IDC_COMBO_DHISTORY, OnSelchangeDHistory)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// RotationDlg message handlers


void RotationDlg::ClearAll()
{
	bmInit = false;

	ogRotationDlgFlights.ClearAll();
	omCcaData.ClearAll();
	
	ogDdx.UnRegister(this,NOTUSED);

	ROTATIONDLGFLIGHTDATA *prlFlight = new ROTATIONDLGFLIGHTDATA;	

	*prmAFlight = *prlFlight;
	*prmAFlightSave = *prlFlight;
	*prmDFlight = *prlFlight;
	*prmDFlightSave = *prlFlight;
	
	delete prlFlight;

	omAJfno.DeleteAll();
	omDJfno.DeleteAll();
	omDCins.DeleteAll();
	omDCinsSave.DeleteAll();
}



BOOL RotationDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

	// 20071105 RST PRF 8726 workaround because the dropdown window is not shown (only a )	

    CRect r;
    m_ComboDHistory.GetClientRect(&r);
    m_ComboDHistory.SetWindowPos(NULL, 0, 0, r.right, 200, SWP_NOMOVE | SWP_NOZORDER);

    m_ComboAHistory.GetClientRect(&r);
    m_ComboAHistory.SetWindowPos(NULL, 0, 0, r.right, 200, SWP_NOMOVE | SWP_NOZORDER);

    m_CL_ARemp.GetClientRect(&r);
    m_CL_ARemp.SetWindowPos(NULL, 0, 0, r.right, 200, SWP_NOMOVE | SWP_NOZORDER);

    m_CL_DRemp.GetClientRect(&r);
    m_CL_DRemp.SetWindowPos(NULL, 0, 0, r.right, 200, SWP_NOMOVE | SWP_NOZORDER);


	// 20071105 RST 

    EnableToolTips(TRUE);
	m_key = "DialogPosition\\DailyDialog";
	
	InitTables();	
	SetSecState();
	
	//Init editfields
	
	m_CE_DCinsBorder.SetReadOnly();
	m_CE_ADes3.SetReadOnly();
	m_CE_DOrg3.SetReadOnly();
	m_CE_AOnbe.SetReadOnly();
	m_CE_ALastChange.SetReadOnly();
	m_CE_ALastChangeTime.SetReadOnly();
	m_CE_DLastChange.SetReadOnly();
	m_CE_DLastChangeTime.SetReadOnly();
	m_CE_AJfnoBorder.SetReadOnly();
	m_CE_DJfnoBorder.SetReadOnly();

//first/last Bag
	m_CE_AFBag.SetReadOnly();
	m_CE_ALBag.SetReadOnly();

	m_CE_AStoa.SetBKColor(YELLOW);
	m_CE_AOrg3.SetBKColor(YELLOW);
	//m_CE_ATtyp.SetBKColor(YELLOW);

	m_CE_DStod.SetBKColor(YELLOW);
	m_CE_DDes3.SetBKColor(YELLOW);
	m_CE_DDes4.SetBKColor(YELLOW);
	//m_CE_DTtyp.SetBKColor(YELLOW);




	m_CE_Ming.SetTypeToString("###",3,0);
	m_CE_Act3.SetTypeToString("x|#x|#x|#",3,0);
	m_CE_Act3.SetBKColor(YELLOW);
	m_CE_Act5.SetTypeToString("x|#x|#x|#x|#x|#",5,0);
	m_CE_Act5.SetBKColor(YELLOW);
	
	//Ankunft

//first/last Bag
	m_CE_AFBag.SetTypeToTime(false, true);
	m_CE_ALBag.SetTypeToTime(false, true);

	m_CE_AAlc3.SetTypeToString("X|#X|#X|#",3,2);
	m_CE_AFltn.SetTypeToString("#####",5,2);
	m_CE_AFlns.SetTypeToString("X",1,0);
	m_CE_AFlti.SetTypeToString("A",1,0);

	m_CE_ACsgn.SetTypeToString("X|#X|#X|#X|#X|#X|#X|#X|#",8,0);
	

	m_CE_ATtyp.SetTypeToString("XXXXX",5,0);


//	m_CE_AHtyp.SetTypeToString("XX",2,0);
	m_CE_AStyp.SetTypeToString("XX",2,0);
	m_CE_AStev.SetTypeToString("X",1,0);

	m_CE_AOrg3.SetTypeToString("XXXX",4,0);
	m_CE_AStoa.SetTypeToTime(true, true);
	m_CE_AStod.SetTypeToTime(false, true);

	m_CE_AOfbl.SetTypeToTime(false, true);
	m_CE_AOnbl.SetTypeToTime(false, true);
	m_CE_AAirb.SetTypeToTime(false, true);
	m_CE_ALand.SetTypeToTime(false, true);
	
	m_CE_AEtai.SetTypeToTime(false, true);
	m_CE_AEtoa.SetTypeToTime(false, true);
	//m_CE_AOnbe.SetTypeToTime(false, true);
	m_CE_ANxti.SetTypeToTime(false, true);
	m_CE_ATmoa.SetTypeToTime(false, true);

	m_CE_ARwya.SetTypeToString("XXXX");
	m_CE_AIfra.SetTypeToString("x",1,0);


	m_CE_ADtd1.SetTypeToString("####",4,0);
	m_CE_ADtd2.SetTypeToString("####",4,0);
	m_CE_ADcd1.SetTypeToString("XX",2,0);
	m_CE_ADcd2.SetTypeToString("XX",2,0);

	


	m_CE_APsta.SetTypeToString("x|#x|#x|#x|#x|#",5,0);
//	m_CE_APaba.SetTypeToTime(false, true);
	m_CE_AEtdi.SetTypeToTime(false, true);
	m_CE_APaea.SetTypeToTime(false, true);
	m_CE_ABbaa.SetTypeToTime(false, true);
	
	
	m_CE_AGta1.SetTypeToString("x|#x|#x|#x|#x|#",5,0);
	m_CE_AGta2.SetTypeToString("x|#x|#x|#x|#x|#",5,0);
	m_CE_ATga1.SetTypeToString("x",1,0);
	m_CE_ATga2.SetTypeToString("x",1,0);
	m_CE_AGa1x.SetTypeToTime(false, true);
	m_CE_AGa2x.SetTypeToTime(false, true);
	m_CE_AGa1y.SetTypeToTime(false, true);
	m_CE_AGa2y.SetTypeToTime(false, true);

	
	m_CE_ABlt1.SetTypeToString("x|#x|#x|#x|#x|#",5,0);
	m_CE_ABlt2.SetTypeToString("x|#x|#x|#x|#x|#",5,0);
	m_CE_ATmb1.SetTypeToString("x",1,0);
	m_CE_ATmb2.SetTypeToString("x",1,0);
	m_CE_AB1ba.SetTypeToTime(false, true);
	m_CE_AB2ba.SetTypeToTime(false, true);
	m_CE_AB1ea.SetTypeToTime(false, true);
	m_CE_AB2ea.SetTypeToTime(false, true);

	// Disable the default menu which appear by rightclicking
	//    on the CCSEdit -box
	m_CE_AB1ba.UnsetDefaultMenu();
	m_CE_AB1ea.UnsetDefaultMenu();
	m_CE_AB2ba.UnsetDefaultMenu();
	m_CE_AB2ea.UnsetDefaultMenu();

	m_CE_AExt1.SetTypeToString("x|#x|#x|#x|#x|#",5,0);
	m_CE_AExt2.SetTypeToString("x|#x|#x|#x|#x|#",5,0);
	m_CE_ATet1.SetTypeToString("x",1,0);
	m_CE_ATet2.SetTypeToString("x",1,0);

	m_CE_ARem1.SetTextLimit(0,255,true);

	
	//Abflug
	m_CE_DAlc3.SetTypeToString("X|#X|#X|#",3,2);
	m_CE_DFltn.SetTypeToString("#####",5,1);
	m_CE_DFlns.SetTypeToString("X",1,0);
	m_CE_DFlti.SetTypeToString("A",1,0);

	m_CE_DCsgn.SetTypeToString("X|#X|#X|#X|#X|#X|#X|#X|#",8,0);
	
	m_CE_DTtyp.SetTypeToString("XXXXX",5,0);

//	m_CE_DHtyp.SetTypeToString("XX",2,0);
	m_CE_DStyp.SetTypeToString("XX",2,0);
	m_CE_DStev.SetTypeToString("X",1,0);

	m_CE_DDes3.SetTypeToString("XXX");
	m_CE_DDes4.SetTypeToString("XXXX");
	m_CE_DStod.SetTypeToTime(true, true);
	m_CE_DStoa.SetTypeToTime(false, true);
	
	m_CE_DOfbl.SetTypeToTime(false, true);
	m_CE_DAirb.SetTypeToTime(false, true);
	
	m_CE_DEtdi.SetTypeToTime(false, true);
	m_CE_DEtod.SetTypeToTime(false, true);
	
	m_CE_DEtdc.SetTypeToTime(false, true);
	m_CE_DIskd.SetTypeToTime(false, true);
	m_CE_DNxti.SetTypeToTime(false, true);
	m_CE_DSlot.SetTypeToTime(false, true);

	m_CE_DRwyd.SetTypeToString("XXXX",4,0);
	m_CE_DIfrd.SetTypeToString("x",1,0);


	m_CE_DDtd1.SetTypeToString("####",4,0);
	m_CE_DDtd2.SetTypeToString("####",4,0);
	m_CE_DDcd1.SetTypeToString("XX",2,0);
	m_CE_DDcd2.SetTypeToString("XX",2,0);

	
	
	
	
	m_CE_DPstd.SetTypeToString("x|#x|#x|#x|#x|#",5,0);
	m_CE_DPdba.SetTypeToTime(false, true);
	m_CE_DPdea.SetTypeToTime(false, true);
	m_CE_DBbfa.SetTypeToTime(false, true);
	
	m_CE_DGtd1.SetTypeToString("x|#x|#x|#x|#x|#",5,0);
	m_CE_DGtd2.SetTypeToString("x|#x|#x|#x|#x|#",5,0);
	m_CE_DTgd1.SetTypeToString("x",1,0);
	m_CE_DTgd2.SetTypeToString("x",1,0);
	m_CE_DGd1x.SetTypeToTime(false, true);
	m_CE_DGd2x.SetTypeToTime(false, true);
	m_CE_DGd1y.SetTypeToTime(false, true);
	m_CE_DGd2y.SetTypeToTime(false, true);
	
	m_CE_DWro1.SetTypeToString("x|#x|#x|#x|#x|#",5,0);
	m_CE_DTwr1.SetTypeToString("x",1,0);
	m_CE_DW1ba.SetTypeToTime(false, true);
	m_CE_DW1ea.SetTypeToTime(false, true);

	m_CE_DBao1.SetTypeToTime(false, true);
	m_CE_DBac1.SetTypeToTime(false, true);
	m_CE_DBao4.SetTypeToTime(false, true);
	m_CE_DBac4.SetTypeToTime(false, true);

	m_CE_DRem1.SetTextLimit(0,255,true);
	m_CE_Regn.SetTypeToString("X|#X|#X|#X|#X|#X|#X|#X|#X|#X|#X|#X|#",12,0);

	m_resizeHelper.Init(this->m_hWnd);

	CRect olRect;
	this->GetWindowRect(&olRect);
	GetDialogFromReg(olRect, m_key);
	SetWindowPos(&wndTop, olRect.left, olRect.top, olRect.Width(), olRect.Height(), SWP_HIDEWINDOW);// | SWP_NOMOVE );


	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void RotationDlg::OnSize(UINT nType, int cx, int cy)  
{
	CDialog::OnSize(nType, cx, cy);
	
	// TODO: Add your message handler code here
	m_resizeHelper.OnSize();

	if (this->pomDCinsTable != NULL && ::IsWindow(this->pomDCinsTable->m_hWnd))
	{
		CRect olrectTable;
		m_CE_DCinsBorder.GetWindowRect(&olrectTable);
		ScreenToClient(olrectTable);
		olrectTable.DeflateRect(2,2);     // hiding the CTable window border
		this->pomDCinsTable->SetPosition(olrectTable.left, olrectTable.right, olrectTable.top, olrectTable.bottom);

		this->pomDCinsTable->DisplayTable();
	}

	if (this->pomDJfnoTable != NULL && ::IsWindow(this->pomDJfnoTable->m_hWnd))
	{
		CRect olrectTable;
		m_CE_DJfnoBorder.GetWindowRect(&olrectTable);
		ScreenToClient(olrectTable);
		olrectTable.DeflateRect(2,2);     // hiding the CTable window border
		this->pomDJfnoTable->SetPosition(olrectTable.left, olrectTable.right, olrectTable.top, olrectTable.bottom);

		this->pomDJfnoTable->DisplayTable();
	}

	if (this->pomAJfnoTable != NULL && ::IsWindow(this->pomAJfnoTable->m_hWnd))
	{
		CRect olrectTable;
		m_CE_AJfnoBorder.GetWindowRect(&olrectTable);
		ScreenToClient(olrectTable);
		olrectTable.DeflateRect(2,2);     // hiding the CTable window border
		this->pomAJfnoTable->SetPosition(olrectTable.left, olrectTable.right, olrectTable.top, olrectTable.bottom);

		this->pomAJfnoTable->DisplayTable();
	}

	this->Invalidate();
}




/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
// Fill the fields and show data in the dialog
//
void RotationDlg::InitDialog( bool bpArrival, bool bpDeparture) 
{
	bmChanged = false;

	CCSEDIT_ATTRIB rlAttribC1;
	CCSEDIT_ATTRIB rlAttribC2;
	CCSEDIT_ATTRIB rlAttribC3;
	CCSEDIT_ATTRIB rlAttribC4;

	CCSPtrArray<TABLE_COLUMN> olColList;
	TABLE_COLUMN rlColumnData;

	int ilLc ;




// Ankunft ///////////////////////////////////////////////////////////////////


	if(bpArrival)
	{

// 		if(bmLocalTime) ogRotationDlgFlights.StructUtcToLocal((void*)prmAFlight, "");
 		if(bmLocalTime) ConvertStructUtcTOLocal(prmAFlight, NULL);
	
		if(strcmp(pcgHome, prmAFlight->Org3) == 0)
		{
			m_CE_AStod.SetBKColor(YELLOW);
			m_CE_AStod.SetTypeToTime(true, true);
			m_CE_AStod.SetTextLimit(4,7,false);

			if(strcmp(prmAFlight->Ftyp, "Z") == 0)
				m_CS_Ankunft.SetWindowText(GetString(IDS_STRING1780) + CString(" ")+ prmAFlight->Stoa.Format("%d.%m.%Y"));
			else
			{
				if(strcmp(prmAFlight->Ftyp, "B") == 0)
					m_CS_Ankunft.SetWindowText(GetString(IDS_STRING1778) + CString(" ")+ prmAFlight->Stoa.Format("%d.%m.%Y"));
				else
					m_CS_Ankunft.SetWindowText(GetString(IDS_STRING340) + CString(" ")+ prmAFlight->Stoa.Format("%d.%m.%Y"));
			}
		}
		else
		{
			m_CE_AStod.SetBKColor(WHITE);
			m_CE_AStod.SetTypeToTime(false, true);
			m_CE_AStod.SetTextLimit(0,7,true);
			m_CS_Ankunft.SetWindowText(GetString(IDS_STRING330) + CString(" ") + prmAFlight->Stoa.Format("%d.%m.%Y"));
		}


		if(strcmp(prmAFlight->Ftyp, "X") == 0)
		{
			m_CE_AOnbl.SetReadOnly();
			m_CE_ALand.SetReadOnly();
		}
		else
		{
			m_CE_AOnbl.SetReadOnly(false);
			m_CE_ALand.SetReadOnly(false);
		}


		CString olFlno(prmAFlight->Flno);
		olFlno.TrimRight();
		if(olFlno.IsEmpty())
		{
			m_CE_AOrg3.SetTextLimit(0,4,true);
			m_CE_AOrg3.SetBKColor(WHITE);
		}
		// color Etai
		if(strcmp(prmAFlight->Stea, "U") == 0)
			m_CE_AEtai.SetBKColor(RGB(255,0,0));
		else
		{
			if((strcmp(prmAFlight->Stea, "C") == 0) || (strcmp(prmAFlight->Stea, "E") == 0) || (strcmp(prmAFlight->Stea, "A") == 0))
				m_CE_AEtai.SetBKColor(RGB(16,189,239));
			else
				m_CE_AEtai.SetBKColor(RGB(255,255,255));
		}

		// color Airb
		if(strcmp(prmAFlight->Stab, "U") == 0)
			m_CE_AAirb.SetBKColor(RGB(255,0,0));
		else
		{
			if((strcmp(prmAFlight->Stab, "A") == 0) || (strcmp(prmAFlight->Stab, "D") == 0))
				m_CE_AAirb.SetBKColor(RGB(16,189,239));
			else
				m_CE_AAirb.SetBKColor(RGB(255,255,255));
		}

		// color Land
		if(strcmp(prmAFlight->Stld, "U") == 0)
			m_CE_ALand.SetBKColor(RGB(255,0,0));
		else
		{
			if((strcmp(prmAFlight->Stld, "A") == 0) || (strcmp(prmAFlight->Stld, "D") == 0))
				m_CE_ALand.SetBKColor(RGB(16,189,239));
			else
				m_CE_ALand.SetBKColor(RGB(255,255,255));
		}

		// color Ofbl
		if(strcmp(prmAFlight->Stof, "U") == 0)
			m_CE_AOfbl.SetBKColor(RGB(255,0,0));
		else
		{
			if((strcmp(prmAFlight->Stof, "S") == 0) || (strcmp(prmAFlight->Stof, "D") == 0))
				m_CE_AOfbl.SetBKColor(RGB(16,189,239));
			else
				m_CE_AOfbl.SetBKColor(RGB(255,255,255));
		}


		// color Onbl
		if(strcmp(prmAFlight->Ston, "U") == 0)
			m_CE_AOnbl.SetBKColor(RGB(255,0,0));
		else
		{
			if((strcmp(prmAFlight->Ston, "S") == 0) || (strcmp(prmAFlight->Ston, "D") == 0))
				m_CE_AOnbl.SetBKColor(RGB(16,189,239));
			else
				m_CE_AOnbl.SetBKColor(RGB(255,255,255));
		}



		// color Tmoa
		if(strcmp(prmAFlight->Sttm, "U") == 0)
		{
			if(prmAFlight->Tmoa != TIMENULL)
				m_CE_ATmoa.SetBKColor(RGB(255,0,0));
		}
		else
		{
			if(strcmp(prmAFlight->Sttm, "C") == 0)
				m_CE_ATmoa.SetBKColor(RGB(16,189,239));
			else
				m_CE_ATmoa.SetBKColor(RGB(255,255,255));
		}

		// color Etdi
		if(strcmp(prmAFlight->Sted, "U") == 0)
			m_CE_AEtdi.SetBKColor(RGB(255,0,0));
		else
		{
			if((strcmp(prmAFlight->Sted, "C") == 0) || (strcmp(prmAFlight->Sted, "E") == 0) || (strcmp(prmAFlight->Sted, "A") == 0))
				m_CE_AEtdi.SetBKColor(RGB(16,189,239));
			else
				m_CE_AEtdi.SetBKColor(RGB(255,255,255));
		}

		/*

		if((strcmp(prmAFlight->Org3, pcgHome) == 0) && (strcmp(prmAFlight->Des3, pcgHome) == 0)) 
			m_CS_Ankunft.SetWindowText(GetString(IDS_STRING340) + CString(" ")+ prmAFlight->Stoa.Format("%d.%m.%Y"));
		else
			m_CS_Ankunft.SetWindowText(GetString(IDS_STRING330) + CString(" ") + prmAFlight->Stoa.Format("%d.%m.%Y"));

		*/

		// Weitere Flugnummern
		ogRotationDlgFlights.GetJfnoArray(&omAJfno, prmAFlight);

		if(omAJfno.GetSize() > 0)
			OnAshowjfno();
		else
			AShowJfnoButton();
		

		pomAJfnoTable->ResetContent();
		
		rlAttribC1.Type = KT_STRING;
		rlAttribC1.Format = "XXX";
		rlAttribC1.TextMinLenght = 0;
		rlAttribC1.TextMaxLenght = 3;
		rlAttribC1.Style = ES_UPPERCASE;

		rlAttribC2.Type = KT_STRING;
		rlAttribC2.Format = "#####";
		rlAttribC2.TextMinLenght = 0;
		rlAttribC2.TextMaxLenght = 5;

		rlAttribC3.Type = KT_STRING;
		rlAttribC3.Format = "X";
		rlAttribC3.TextMinLenght = 0;
		rlAttribC3.TextMaxLenght = 1;
		rlAttribC3.Style = ES_UPPERCASE;

		rlColumnData.SeparatorType = SEPA_NONE;
		rlColumnData.Font = &ogMS_Sans_Serif_8;
		rlColumnData.VerticalSeparator = SEPA_NORMAL;
		rlColumnData.HorizontalSeparator = SEPA_NONE;
		rlColumnData.Alignment = COLALIGN_LEFT;


		if(ogPrivList.GetStat("ROTATIONDLG_CB_AShowJfno") != '1')
			rlColumnData.BkColor = RGB(192,192,192);
		else
			rlColumnData.BkColor = RGB(255,255,255);


		for (ilLc = 0; ilLc < omAJfno.GetSize(); ilLc++)
		{
			rlColumnData.Text = omAJfno[ilLc].Alc3;
			rlColumnData.Text.TrimRight();
			rlColumnData.EditAttrib = rlAttribC1;
			olColList.NewAt(olColList.GetSize(), rlColumnData);
			rlColumnData.Text = omAJfno[ilLc].Fltn;
			rlColumnData.Text.TrimRight();
			rlColumnData.EditAttrib = rlAttribC2;
			olColList.NewAt(olColList.GetSize(), rlColumnData);
			rlColumnData.Text = omAJfno[ilLc].Flns;
			rlColumnData.Text.TrimRight();
			rlColumnData.EditAttrib = rlAttribC3;
			olColList.NewAt(olColList.GetSize(), rlColumnData);
			pomAJfnoTable->AddTextLine(olColList, (void*)(NULL));
			olColList.DeleteAll();
		}
		for (ilLc = omAJfno.GetSize(); ilLc < 9; ilLc++)
		{
			rlColumnData.Text = "";
			rlColumnData.EditAttrib = rlAttribC1;
			olColList.NewAt(olColList.GetSize(), rlColumnData);
			rlColumnData.Text = "";
			rlColumnData.EditAttrib = rlAttribC2;
			olColList.NewAt(olColList.GetSize(), rlColumnData);
			rlColumnData.Text = "";
			rlColumnData.EditAttrib = rlAttribC3;
			olColList.NewAt(olColList.GetSize(), rlColumnData);
			pomAJfnoTable->AddTextLine(olColList, (void*)(NULL));
			olColList.DeleteAll();
		}

		pomAJfnoTable->DisplayTable();

		if(omAJfno.GetSize() > 0)
			OnAshowjfno();
		else
			AShowJfnoButton();

		
		// Vias
		if (!polRotationAViaDlg)
		{
			bool blEnable = true;
			if(ogPrivList.GetStat("ROTATIONDLG_CB_AShowVia") != '1')
				blEnable = false;

			polRotationAViaDlg = new RotationViaDlg(this, "A", CString(prmAFlight->Flno), prmAFlight->Urno, prmAFlight->Vial, prmAFlight->Stoa, bmLocalTime, blEnable);
			polRotationAViaDlg->Create(RotationViaDlg::IDD);
			polRotationAViaDlg->SetSecState(ogPrivList.GetStat("ROTATIONDLG_CB_AShowVia"));
		}
		else
			polRotationAViaDlg->SetData("A", CString(prmAFlight->Flno), prmAFlight->Urno, prmAFlight->Vial, prmAFlight->Stoa, bmLocalTime);


		if( CString(prmAFlight->Ftyp)	== "Z" || CString(prmAFlight->Ftyp)	== "B")
		{
			if (polRotationAViaDlg)
				polRotationAViaDlg->Enable(false);
		}
		else
		{
			if (polRotationAViaDlg)
				polRotationAViaDlg->Enable(true);
		}


		m_ACxx = FALSE;
		m_CB_ACxx.SetCheck(FALSE);
		m_ADiverted = FALSE;
		m_CB_ADiverted.SetCheck(FALSE);
		m_ARerouted = FALSE;
		m_CB_ARerouted.SetCheck(FALSE);
		m_CB_ARetFlight.SetCheck(FALSE);
		m_CB_ARetTaxi.SetCheck(FALSE);
		m_GoAround = FALSE;
		m_CB_GoAround.SetCheck(m_GoAround);

		if(strcmp(prmAFlight->Baz6, "G") == 0)
		{
			m_GoAround = TRUE;
			m_CB_GoAround.SetCheck(m_GoAround);
		}

		m_CE_ADivr.EnableWindow(FALSE);

		if(strcmp(prmAFlight->Ftyp, "X") == 0)
		{
			m_ACxx = TRUE;
			m_CB_ACxx.SetCheck(TRUE);
		}
		if(strcmp(prmAFlight->Ftyp, "D") == 0)
		{
			m_ADiverted = TRUE;
			m_CB_ADiverted.SetCheck(TRUE);
			m_CE_ADivr.EnableWindow(TRUE);
		}
		if(strcmp(prmAFlight->Ftyp, "R") == 0)
		{
			m_ARerouted = TRUE;
			m_CB_ARerouted.SetCheck(TRUE);
		}


		if(strcmp(prmAFlight->Ftyp, "Z") == 0)
		{
			m_CB_ARetFlight.SetCheck(TRUE);
		}

		if(strcmp(prmAFlight->Ftyp, "B") == 0)
		{
			m_CB_ARetTaxi.SetCheck(TRUE);
		}



		omAAlc3 = CString(prmAFlight->Alc3);
		omAAlc2 = CString(prmAFlight->Alc2);

		m_AAlc3 = CString(prmAFlight->Flno);
		m_AAlc3 = m_AAlc3.Left(3);
		m_AAlc3.TrimRight();
		m_AFltn = CString(prmAFlight->Fltn);
		m_AFlns = CString(prmAFlight->Flns);
		m_AFlti = prmAFlight->Flti;

		m_CE_AFlti.SetBKColor(RGB(255,255,255));
		if ((int) strlen(prmAFlight->Dssf) > ogDssfFltiId && ogDssfFltiId != -1)
		{
			if (prmAFlight->Dssf[ogDssfFltiId] == 'U')
				m_CE_AFlti.SetBKColor(RGB(255,0,0));
		}

		strcpy(pcmAOrg4, prmAFlight->Org4);
		strcpy(pcmAOrg3, prmAFlight->Org3);

		m_ADes3 = CString(pcgHome);
		m_AOrg3 = CString(prmAFlight->Org4);
		m_ATtyp = CString(prmAFlight->Ttyp);
//		m_AHtyp = CString(prmAFlight->Htyp);
		m_AStyp = CString(prmAFlight->Styp);
		m_AStev = CString(prmAFlight->Stev);
		m_ARem1 = CString(prmAFlight->Rem1);
		m_AStod = DateToHourDivString(prmAFlight->Stod, prmAFlight->Stoa);
		m_AStoa = DateToHourDivString(prmAFlight->Stoa, prmAFlight->Stoa);

		m_AEtai = DateToHourDivString(prmAFlight->Etai, prmAFlight->Stoa);
		m_AEtoa = DateToHourDivString(prmAFlight->Etoa, prmAFlight->Stoa);
		m_AOnbe = DateToHourDivString(prmAFlight->Onbe, prmAFlight->Stoa);
		m_ANxti = DateToHourDivString(prmAFlight->Nxti, prmAFlight->Stoa);

		m_AOnbl = DateToHourDivString(prmAFlight->Onbl, prmAFlight->Stoa);
		m_AOfbl = DateToHourDivString(prmAFlight->Ofbl, prmAFlight->Stoa);
		m_AAirb = DateToHourDivString(prmAFlight->Airb, prmAFlight->Stoa);
		m_ALand = DateToHourDivString(prmAFlight->Land, prmAFlight->Stoa);

		m_ATmoa = DateToHourDivString(prmAFlight->Tmoa, prmAFlight->Stoa);
		m_ADtd1 = CString(prmAFlight->Dtd1);
		m_ADtd2 = CString(prmAFlight->Dtd2);
		m_ADcd1 = CString(prmAFlight->Dcd1);
		m_ADcd2 = CString(prmAFlight->Dcd2);

		m_ARwya = CString(prmAFlight->Rwya);
		m_AIfra = CString(prmAFlight->Ifra);

		m_ADivr = CString(prmAFlight->Divr);

		m_ALastChange = prmAFlight->Lstu .Format("  %d.%m.%Y / %H:%M  ") + CString(prmAFlight->Useu);
		m_ALastChangeTime = CString(prmAFlight->Chgi);

		m_ABbaa = DateToHourDivString(prmAFlight->Bbaa, prmAFlight->Stoa);

//		m_APaba = DateToHourDivString(prmAFlight->Paba, prmAFlight->Stoa);
		m_AEtdi = DateToHourDivString(prmAFlight->Etdi, prmAFlight->Stoa);
		m_APaea = DateToHourDivString(prmAFlight->Paea, prmAFlight->Stoa);
		m_AGa1x = DateToHourDivString(prmAFlight->Ga1x, prmAFlight->Stoa);
		m_AGa2x = DateToHourDivString(prmAFlight->Ga2x, prmAFlight->Stoa);
		m_AGa1y = DateToHourDivString(prmAFlight->Ga1y, prmAFlight->Stoa);
		m_AGa2y = DateToHourDivString(prmAFlight->Ga2y, prmAFlight->Stoa);
		m_AB1ba = DateToHourDivString(prmAFlight->B1ba, prmAFlight->Stoa);
		m_AB2ba = DateToHourDivString(prmAFlight->B2ba, prmAFlight->Stoa);
		m_AB1ea = DateToHourDivString(prmAFlight->B1ea, prmAFlight->Stoa);
		m_AB2ea = DateToHourDivString(prmAFlight->B2ea, prmAFlight->Stoa);
		
		m_APsta = CString(prmAFlight->Psta);
		m_AGta1 = CString(prmAFlight->Gta1);
		m_AGta2 = CString(prmAFlight->Gta2);
		m_ATga1	= CString(prmAFlight->Tga1);
		m_ATga2 = CString(prmAFlight->Tga2);
		m_ABlt1 = CString(prmAFlight->Blt1);
		m_ABlt2 = CString(prmAFlight->Blt2);
		m_ATmb1 = CString(prmAFlight->Tmb1);
		m_ATmb2 = CString(prmAFlight->Tmb2);
		m_AExt1 = CString(prmAFlight->Ext1);
		m_AExt2 = CString(prmAFlight->Ext2);
		m_ATet1 = CString(prmAFlight->Tet1);
		m_ATet2 = CString(prmAFlight->Tet2);

//first/last Bag
		m_AFBag = DateToHourDivString(prmAFlight->Bas1, prmAFlight->Stoa);
		m_ALBag = DateToHourDivString(prmAFlight->Bae1, prmAFlight->Stoa);

		m_CE_ACsgn.SetInitText(prmAFlight->Csgn);

 		m_CE_ADes3.SetInitText(pcgHome4);
		m_CE_AOrg3.SetInitText(m_AOrg3);
		m_CE_ADivr.SetInitText(m_ADivr);
		m_CE_AAlc3.SetInitText(m_AAlc3);
		m_CE_AFltn.SetInitText(m_AFltn);
		m_CE_AFlns.SetInitText(m_AFlns);
		m_CE_AFlti.SetInitText(m_AFlti);
		m_CE_ATtyp.SetInitText(m_ATtyp);
//		m_CE_AHtyp.SetInitText(m_AHtyp);
		m_CE_AStyp.SetInitText(m_AStyp);
		m_CE_AStev.SetInitText(m_AStev);
		m_CE_AOrg3.SetInitText(m_AOrg3);
		m_CE_ARem1.SetInitText(m_ARem1);
		m_CE_AEtai.SetInitText(m_AEtai);
		m_CE_AStod.SetInitText(m_AStod);
		m_CE_AStoa.SetInitText(m_AStoa);
		m_CE_AEtai.SetInitText(m_AEtai);
		m_CE_AEtoa.SetInitText(m_AEtoa);
		m_CE_AOnbe.SetInitText(m_AOnbe);
		m_CE_ANxti.SetInitText(m_ANxti);
		m_CE_AOnbl.SetInitText(m_AOnbl);
		m_CE_AOfbl.SetInitText(m_AOfbl);
		m_CE_AAirb.SetInitText(m_AAirb);
		m_CE_ALand.SetInitText(m_ALand);
		m_CE_ATmoa.SetInitText(m_ATmoa);
		m_CE_ADtd1.SetInitText(m_ADtd1);
		m_CE_ADtd2.SetInitText(m_ADtd2);
		m_CE_ADcd1.SetInitText(m_ADcd1);
		m_CE_ADcd2.SetInitText(m_ADcd2);
		m_CE_ARwya.SetInitText(m_ARwya);
		m_CE_AIfra.SetInitText(m_AIfra);
		m_CE_APsta.SetInitText(m_APsta);
//		m_CE_APaba.SetInitText(m_APaba);
		m_CE_AEtdi.SetInitText(m_AEtdi);
		m_CE_APaea.SetInitText(m_APaea);
		m_CE_AGta1.SetInitText(m_AGta1);
		m_CE_AGta2.SetInitText(m_AGta2);
		m_CE_ATga1.SetInitText(m_ATga1);
		m_CE_ATga2.SetInitText(m_ATga2);
		m_CE_AGa1x.SetInitText(m_AGa1x);
		m_CE_AGa2x.SetInitText(m_AGa2x);
		m_CE_AGa1y.SetInitText(m_AGa1y);
		m_CE_AGa2y.SetInitText(m_AGa2y);
		m_CE_ABlt1.SetInitText(m_ABlt1);
		m_CE_ABlt2.SetInitText(m_ABlt2);
		m_CE_ATmb1.SetInitText(m_ATmb1);
		m_CE_ATmb2.SetInitText(m_ATmb2);
		m_CE_AB1ba.SetInitText(m_AB1ba);
		m_CE_AB2ba.SetInitText(m_AB2ba);
		m_CE_AB1ea.SetInitText(m_AB1ea);
		m_CE_AB2ea.SetInitText(m_AB2ea);
		m_CE_AExt1.SetInitText(m_AExt1);
		m_CE_AExt2.SetInitText(m_AExt2);
		m_CE_ATet1.SetInitText(m_ATet1);
		m_CE_ATet2.SetInitText(m_ATet2);
		m_CE_ABbaa.SetInitText(m_ABbaa);
		m_CE_ALastChange.SetInitText(m_ALastChange);
		m_CE_ALastChangeTime.SetInitText(m_ALastChangeTime);
//first/last Bag
		m_CE_AFBag.SetInitText(m_AFBag);
		m_CE_ALBag.SetInitText(m_ALBag);

	}




// Abflug ///////////////////////////////////////////////////////////////////
	if(bpDeparture)
	{

// 		if(bmLocalTime) ogRotationDlgFlights.StructUtcToLocal((void*)prmDFlight, "");
 		if(bmLocalTime) ConvertStructUtcTOLocal(NULL, prmDFlight);

		if(strcmp(prmDFlight->Des3 ,pcgHome) == 0)
		{
			m_CE_DStoa.SetBKColor(YELLOW);
			m_CE_DStoa.SetTypeToTime(true, true);
			m_CE_DStoa.SetTextLimit(4,7,false);

			
			if(strcmp(prmDFlight->Ftyp, "Z") == 0)
					m_CS_Abflug.SetWindowText(GetString(IDS_STRING1780) + CString(" ")+ prmDFlight->Stod.Format("%d.%m.%Y"));
			else
			{
				if(strcmp(prmDFlight->Ftyp, "B") == 0)
					m_CS_Abflug.SetWindowText(GetString(IDS_STRING1778) + CString(" ")+ prmDFlight->Stod.Format("%d.%m.%Y"));
				else
					m_CS_Abflug.SetWindowText(GetString(IDS_STRING340) + CString(" ")+ prmDFlight->Stod.Format("%d.%m.%Y"));
			}
					
		}
		else
		{
			m_CE_DStoa.SetBKColor(WHITE);
			m_CE_DStoa.SetTypeToTime(false, true);
			m_CE_DStoa.SetTextLimit(0,7,true);
			m_CS_Abflug.SetWindowText(GetString(IDS_STRING338) + CString(" ")+ prmDFlight->Stod.Format("%d.%m.%Y"));
		}


		m_CB_DRetFlight.SetCheck(FALSE);
		m_CB_DRetTaxi.SetCheck(FALSE);
		m_CB_DCxx.SetCheck(FALSE);
		m_DDiverted = FALSE;
		m_CB_DDiverted.SetCheck(FALSE);
		m_DRerouted = FALSE;
		m_CB_DRerouted.SetCheck(FALSE);

		m_CE_DDivr.EnableWindow(FALSE);


		if(strcmp(prmDFlight->Ftyp, "X") == 0)
		{
			m_DCxx = TRUE;
			m_CB_DCxx.SetCheck(TRUE);
			m_CE_DOfbl.SetReadOnly();
			m_CE_DAirb.SetReadOnly();
		}
		else
		{
			if(strcmp(prmDFlight->Ftyp, "O") == 0)
			{
				m_DCxx = FALSE;
				m_CB_DCxx.SetCheck(FALSE);
			}
			m_CE_DOfbl.SetReadOnly(false);
			m_CE_DAirb.SetReadOnly(false);

			if(strcmp(prmDFlight->Ftyp, "D") == 0)
			{
				m_DDiverted = TRUE;
				m_CB_DDiverted.SetCheck(TRUE);
				m_CE_DDivr.EnableWindow(TRUE);
			}
			if(strcmp(prmDFlight->Ftyp, "R") == 0)
			{
				m_DRerouted = TRUE;
				m_CB_DRerouted.SetCheck(TRUE);
			}
			if(strcmp(prmDFlight->Ftyp, "Z") == 0)
			{
				m_CB_DRetFlight.SetCheck(TRUE);
			}

			if(strcmp(prmDFlight->Ftyp, "B") == 0)
			{
				m_CB_DRetTaxi.SetCheck(TRUE);
			}
		}


		CString olFlno(prmDFlight->Flno);
		olFlno.TrimRight();
		if(olFlno.IsEmpty())
		{
			m_CE_DDes3.SetTextLimit(0,3,true);
			m_CE_DDes3.SetBKColor(WHITE);
			m_CE_DDes4.SetTextLimit(0,4,true);
			m_CE_DDes4.SetBKColor(WHITE);
		}
		// color Etdi
		if(strcmp(prmDFlight->Sted, "U") == 0)
			m_CE_DEtdi.SetBKColor(RGB(255,0,0));
		else
		{
			if((strcmp(prmDFlight->Sted, "C") == 0) || (strcmp(prmDFlight->Sted, "E") == 0) || (strcmp(prmDFlight->Sted, "A") == 0))
				m_CE_DEtdi.SetBKColor(RGB(16,189,239));
			else
				m_CE_DEtdi.SetBKColor(RGB(255,255,255));
		}

		// color Airb
		if(strcmp(prmDFlight->Stab, "U") == 0)
			m_CE_DAirb.SetBKColor(RGB(255,0,0));
		else
		{
			if((strcmp(prmDFlight->Stab, "A") == 0) || (strcmp(prmDFlight->Stab, "D") == 0))
				m_CE_DAirb.SetBKColor(RGB(16,189,239));
			else
				m_CE_DAirb.SetBKColor(RGB(255,255,255));
		}


		// color Ofbl
		if(strcmp(prmDFlight->Stof, "U") == 0)
			m_CE_DOfbl.SetBKColor(RGB(255,0,0));
		else
		{
			if((strcmp(prmDFlight->Stof, "S") == 0) || (strcmp(prmDFlight->Stof, "D") == 0))
				m_CE_DOfbl.SetBKColor(RGB(16,189,239));
			else
				m_CE_DOfbl.SetBKColor(RGB(255,255,255));
		}



		// color Slot
		if(strcmp(prmDFlight->Stsl, "U") == 0)
		{
			if(prmDFlight->Slot != TIMENULL)
				m_CE_DSlot.SetBKColor(RGB(255,0,0));
		}
		else
		{
			if(strcmp(prmDFlight->Stsl, "C") == 0)
				m_CE_DSlot.SetBKColor(RGB(16,189,239));
			else
				m_CE_DSlot.SetBKColor(RGB(255,255,255));
		}


		/*
		if((strcmp(prmDFlight->Org3, pcgHome) == 0) && (strcmp(prmDFlight->Des3, pcgHome) == 0)) 
			m_CS_Abflug.SetWindowText(GetString(IDS_STRING340) + CString(" ")+ prmDFlight->Stod.Format("%d.%m.%Y"));
		else
			m_CS_Abflug.SetWindowText(GetString(IDS_STRING338) + CString(" ")+ prmDFlight->Stod.Format("%d.%m.%Y"));
		*/
		

		m_DOrg3 = CString(pcgHome);



		//Weitere Flugnummern

		ogRotationDlgFlights.GetJfnoArray(&omDJfno, prmDFlight);

		pomDJfnoTable->ResetContent();
		

		rlAttribC1.Type = KT_STRING;
		rlAttribC1.Format = "XXXX";
		rlAttribC1.TextMinLenght = 0;
		rlAttribC1.TextMaxLenght = 4;
		rlAttribC1.Style = ES_UPPERCASE;

		rlAttribC2.Type = KT_STRING;
		rlAttribC2.Format = "#####";
		rlAttribC2.TextMinLenght = 0;
		rlAttribC2.TextMaxLenght = 5;

		rlAttribC3.Type = KT_STRING;
		rlAttribC3.Format = "X";
		rlAttribC3.TextMinLenght = 0;
		rlAttribC3.TextMaxLenght = 1;
		rlAttribC3.Style = ES_UPPERCASE;


		rlColumnData.SeparatorType = SEPA_NONE;
		rlColumnData.Font = &ogMS_Sans_Serif_8;
		rlColumnData.VerticalSeparator = SEPA_NORMAL;
		rlColumnData.HorizontalSeparator = SEPA_NONE;
		rlColumnData.Alignment = COLALIGN_LEFT;


		if(ogPrivList.GetStat("ROTATIONDLG_CB_DShowJfno") != '1')
			rlColumnData.BkColor = RGB(192,192,192);
		else
			rlColumnData.BkColor = RGB(255,255,255);


		for ( ilLc = 0; ilLc < omDJfno.GetSize(); ilLc++)
		{
			rlColumnData.Text = omDJfno[ilLc].Alc3;
			rlColumnData.Text.TrimRight();
			rlColumnData.EditAttrib = rlAttribC1;
			olColList.NewAt(olColList.GetSize(), rlColumnData);
			rlColumnData.Text = omDJfno[ilLc].Fltn;
			rlColumnData.Text.TrimRight();
			rlColumnData.EditAttrib = rlAttribC2;
			olColList.NewAt(olColList.GetSize(), rlColumnData);
			rlColumnData.Text = omDJfno[ilLc].Flns;
			rlColumnData.Text.TrimRight();
			rlColumnData.EditAttrib = rlAttribC3;
			olColList.NewAt(olColList.GetSize(), rlColumnData);
			pomDJfnoTable->AddTextLine(olColList, (void*)(NULL));
			olColList.DeleteAll();
		}
		for (ilLc = omDJfno.GetSize(); ilLc < 9; ilLc++)
		{
			rlColumnData.Text = "";
			rlColumnData.EditAttrib = rlAttribC1;
			olColList.NewAt(olColList.GetSize(), rlColumnData);
			rlColumnData.Text = "";
			rlColumnData.EditAttrib = rlAttribC2;
			olColList.NewAt(olColList.GetSize(), rlColumnData);
			rlColumnData.Text = "";
			rlColumnData.EditAttrib = rlAttribC3;
			olColList.NewAt(olColList.GetSize(), rlColumnData);
			pomDJfnoTable->AddTextLine(olColList, (void*)(NULL));
			olColList.DeleteAll();
		}
		pomDJfnoTable->DisplayTable();


		if(omDJfno.GetSize() > 0)
			OnDshowjfno();
		else
			DShowJfnoButton();


		if (!polRotationDViaDlg)
		{
			bool blEnable = true;
			if(ogPrivList.GetStat("ROTATIONDLG_CB_DShowVia") != '1')
				blEnable = false;

			polRotationDViaDlg = new RotationViaDlg(this, "D", CString(prmDFlight->Flno), prmDFlight->Urno, prmDFlight->Vial, prmDFlight->Stod, bmLocalTime, blEnable);
			polRotationDViaDlg->Create(RotationViaDlg::IDD);
			polRotationDViaDlg->SetSecState(ogPrivList.GetStat("ROTATIONDLG_CB_DShowVia"));
		}
		else
			polRotationDViaDlg->SetData("D", CString(prmDFlight->Flno), prmDFlight->Urno, prmDFlight->Vial, prmDFlight->Stod, bmLocalTime);


		omDAlc3 = CString(prmDFlight->Alc3);
		omDAlc2 = CString(prmDFlight->Alc2);

		m_DAlc3 = CString(prmDFlight->Flno);
		m_DAlc3 = m_DAlc3.Left(3);
		m_DAlc3.TrimRight();
		m_DFltn = CString(prmDFlight->Fltn);
		m_DFlns = CString(prmDFlight->Flns);
		m_DFlti = prmDFlight->Flti;

		m_CE_DFlti.SetBKColor(RGB(255,255,255));
		if ((int) strlen(prmDFlight->Dssf) > ogDssfFltiId && ogDssfFltiId != -1)
		{
			if (prmDFlight->Dssf[ogDssfFltiId] == 'U')
 			m_CE_DFlti.SetBKColor(RGB(255,0,0));
		}

		strcpy(pcmDDes4, prmDFlight->Des4);
		strcpy(pcmDDes3, prmDFlight->Des3);

		m_DDivr = CString(prmDFlight->Divr);

		m_DDes3 = CString(prmDFlight->Des3);
		m_DDes4 = CString(prmDFlight->Des4);
		m_DTtyp = CString(prmDFlight->Ttyp);
//		m_DHtyp = CString(prmDFlight->Htyp);
		m_DStyp = CString(prmDFlight->Styp);
		m_DStev = CString(prmDFlight->Stev);
		m_DStod = DateToHourDivString(prmDFlight->Stod, prmDFlight->Stod);
		m_DStoa = DateToHourDivString(prmDFlight->Stoa, prmDFlight->Stod);
		m_DEtdi = DateToHourDivString(prmDFlight->Etdi, prmDFlight->Stod);
		m_DEtod = DateToHourDivString(prmDFlight->Etod, prmDFlight->Stod);
		m_DIskd = DateToHourDivString(prmDFlight->Iskd, prmDFlight->Stod);
		m_DNxti = DateToHourDivString(prmDFlight->Nxti, prmDFlight->Stod);
		m_DEtdc = DateToHourDivString(prmDFlight->Etdc, prmDFlight->Stod);
		m_DOfbl = DateToHourDivString(prmDFlight->Ofbl, prmDFlight->Stod);
		m_DAirb = DateToHourDivString(prmDFlight->Airb, prmDFlight->Stod);
		m_DSlot = DateToHourDivString(prmDFlight->Slot, prmDFlight->Stod);
		m_DDtd1 = CString(prmDFlight->Dtd1);
		m_DDtd2 = CString(prmDFlight->Dtd2);
		m_DDcd1 = CString(prmDFlight->Dcd1);
		m_DDcd2 = CString(prmDFlight->Dcd2);
		m_DRwyd = CString(prmDFlight->Rwyd);
		m_DIfrd = CString(prmDFlight->Ifrd);
		m_DLastChange = prmDFlight->Lstu.Format("  %d.%m.%Y / %H:%M  ") +  CString(prmDFlight->Useu);
		m_DLastChangeTime = CString(prmDFlight->Chgi);
		m_DPdba = DateToHourDivString(prmDFlight->Pdba, prmDFlight->Stod);
		m_DPdea = DateToHourDivString(prmDFlight->Pdea, prmDFlight->Stod);
		m_DBbfa = DateToHourDivString(prmDFlight->Bbfa, prmDFlight->Stod);
		m_DGd1x = DateToHourDivString(prmDFlight->Gd1x, prmDFlight->Stod);
		m_DGd2x = DateToHourDivString(prmDFlight->Gd2x, prmDFlight->Stod);
		m_DGd1y = DateToHourDivString(prmDFlight->Gd1y, prmDFlight->Stod);
		m_DGd2y = DateToHourDivString(prmDFlight->Gd2y, prmDFlight->Stod);
		m_DW1ba = DateToHourDivString(prmDFlight->W1ba, prmDFlight->Stod);
		m_DW1ea = DateToHourDivString(prmDFlight->W1ea, prmDFlight->Stod);
		m_DPstd = CString(prmDFlight->Pstd);
		m_DGtd1 = CString(prmDFlight->Gtd1);
		m_DGtd2 = CString(prmDFlight->Gtd2);
		m_DTgd1 = CString(prmDFlight->Tgd1);
		m_DTgd2 = CString(prmDFlight->Tgd2);
		m_DWro1 = CString(prmDFlight->Wro1);
		m_DTwr1 = CString(prmDFlight->Twr1);
		m_DRem1 = CString(prmDFlight->Rem1);


		m_DBaz1 = CString(prmDFlight->Baz1);
		m_DBaz4 = CString(prmDFlight->Baz4);
		m_DBao1 = DateToHourDivString(prmDFlight->Bao1, prmDFlight->Stod);
		m_DBac1 = DateToHourDivString(prmDFlight->Bac1, prmDFlight->Stod);
		m_DBao4 = DateToHourDivString(prmDFlight->Bao4, prmDFlight->Stod);
		m_DBac4 = DateToHourDivString(prmDFlight->Bac4, prmDFlight->Stod);

		m_CE_DBaz1.SetInitText(m_DBaz1);
		m_CE_DBaz4.SetInitText(m_DBaz4);
		m_CE_DBao1.SetInitText(m_DBao1);
		m_CE_DBac1.SetInitText(m_DBac1);
		m_CE_DBao4.SetInitText(m_DBao4);
		m_CE_DBac4.SetInitText(m_DBac4);

		m_CE_DCsgn.SetInitText(prmDFlight->Csgn);
 
		m_CE_DOrg3.SetInitText(pcgHome4);
		m_CE_DDes3.SetInitText(m_DDes3);
		m_CE_DDes4.SetInitText(m_DDes4);
		m_CE_DDivr.SetInitText(m_DDivr);
		m_CE_DAlc3.SetInitText(m_DAlc3);
		m_CE_DFltn.SetInitText(m_DFltn);
		m_CE_DFlns.SetInitText(m_DFlns);
		m_CE_DFlti.SetInitText(m_DFlti);
		m_CE_DTtyp.SetInitText(m_DTtyp);
//		m_CE_DHtyp.SetInitText(m_DHtyp);
		m_CE_DStyp.SetInitText(m_DStyp);
		m_CE_DStev.SetInitText(m_DStev);
		m_CE_DRem1.SetInitText(m_DRem1);
		m_CE_DEtdi.SetInitText(m_DEtdi);
		m_CE_DStod.SetInitText(m_DStod);
		m_CE_DStoa.SetInitText(m_DStoa);
		m_CE_DOfbl.SetInitText(m_DOfbl);
		m_CE_DAirb.SetInitText(m_DAirb);
		m_CE_DEtdi.SetInitText(m_DEtdi);
		m_CE_DEtod.SetInitText(m_DEtod);
		m_CE_DEtdc.SetInitText(m_DEtdc);
		m_CE_DIskd.SetInitText(m_DIskd);
		m_CE_DSlot.SetInitText(m_DSlot);
		m_CE_DNxti.SetInitText(m_DNxti);
		m_CE_DDtd1.SetInitText(m_DDtd1);
		m_CE_DDtd2.SetInitText(m_DDtd2);
		m_CE_DDcd1.SetInitText(m_DDcd1);
		m_CE_DDcd2.SetInitText(m_DDcd2);
		m_CE_DRwyd.SetInitText(m_DRwyd);
		m_CE_DIfrd.SetInitText(m_DIfrd);
		m_CE_DPstd.SetInitText(m_DPstd);
		m_CE_DPdba.SetInitText(m_DPdba);
		m_CE_DPdea.SetInitText(m_DPdea);
		m_CE_DGtd1.SetInitText(m_DGtd1);
		m_CE_DGtd2.SetInitText(m_DGtd2);
		m_CE_DTgd1.SetInitText(m_DTgd1);
		m_CE_DTgd2.SetInitText(m_DTgd2);
		m_CE_DGd1x.SetInitText(m_DGd1x);
		m_CE_DGd2x.SetInitText(m_DGd2x);
		m_CE_DGd1y.SetInitText(m_DGd1y);
		m_CE_DGd2y.SetInitText(m_DGd2y);
		m_CE_DWro1.SetInitText(m_DWro1);
		m_CE_DTwr1.SetInitText(m_DTwr1);
		m_CE_DW1ba.SetInitText(m_DW1ba);
		m_CE_DW1ea.SetInitText(m_DW1ea);
		m_CE_DBbfa.SetInitText(m_DBbfa);
		m_CE_DLastChange.SetInitText(m_DLastChange);
		m_CE_DLastChangeTime.SetInitText(m_DLastChangeTime);


		if(!bmInit)		
		{
			if(prmDFlight->Urno > 0)
			{
				omCcaData.Register();
				CString olFlnu;
				olFlnu.Format("%ld", prmDFlight->Urno);
				omCcaData.SetFilter(false, olFlnu);

				ReadCcaData();
			}
		}
		DShowCinsTable();	

		bmAutoSetBaz1 = false;
		bmAutoSetBaz4 = false;

		CString olBaz1;
		CString olBaz4;
		CCADATA *prlCca;

		CTime opStart = TIMENULL;
		CTime opEnd	  = TIMENULL;


		for( int i = 0; i < omDCins.GetSize(); i++)
		{
			prlCca = &omDCins[i];

			if(CString(prmDFlight->Flti) == "M")
			{
				if( !CString(prlCca->Ckic).IsEmpty())
				{
					if(ogBCD.GetField("CIC", "CNAM", prlCca->Ckic, "CICR") == "I")
						olBaz1 = ogBCD.GetField("CIC", "CNAM", prlCca->Ckic, "CBAZ");
					else
						olBaz4 = ogBCD.GetField("CIC", "CNAM", prlCca->Ckic, "CBAZ");
				}
			}
			else
			{
				if( !CString(prlCca->Ckic).IsEmpty())
				{
					olBaz1 = ogBCD.GetField("CIC", "CNAM", prlCca->Ckic, "CBAZ");
					if(!olBaz1.IsEmpty())
						break;
				}
			}
			if(prlCca->Ckba != TIMENULL)
			{
				opStart = prlCca->Ckba;
			}
			else
			{
				if(prlCca->Ckbs != TIMENULL)
				{
					opStart = prlCca->Ckbs;
				}
			}

			if(prlCca->Ckea != TIMENULL)
			{
				opEnd = prlCca->Ckea;
			}
			else
			{
				if(prlCca->Ckes != TIMENULL)
				{
					opEnd = prlCca->Ckes;
				}
			}
		}

		if( CString(prmDFlight->Baz1) == olBaz1 && prmDFlight->Bao1 == opStart && prmDFlight->Bac1 == opEnd)
			bmAutoSetBaz1 = true;

		if( CString(prmDFlight->Baz4) == olBaz4 && prmDFlight->Bao4 == opStart && prmDFlight->Bac4 == opEnd)
			bmAutoSetBaz4 = true;


		/*
		if(!CString(prmDFlight->Baz1).IsEmpty())
		{
			prmDFlight->Bao1 = opStart;
			prmDFlight->Bac1 = opEnd;
		}
		else
		{
			prmDFlight->Bao1 = TIMENULL;
			prmDFlight->Bac1 = TIMENULL;
		}

		if(!CString(prmDFlight->Baz4).IsEmpty())
		{
			prmDFlight->Bao4 = opStart;
			prmDFlight->Bac4 = opEnd;
		}
		else
		{
			prmDFlight->Bao4 = TIMENULL;
			prmDFlight->Bac4 = TIMENULL;
		}
		*/

	} // if(bpDeparture)


	if(prmAFlight->Urno != 0)
	{
		m_Regn = CString(prmAFlight->Regn);
		m_Act3 = CString(prmAFlight->Act3);
		m_Act5 = CString(prmAFlight->Act5);
		m_Ming = CString(prmAFlight->Ming);

		m_CE_Ming.SetInitText(m_Ming);
		m_CE_Act3.SetInitText(m_Act3);
		m_CE_Act5.SetInitText(m_Act5);
		m_CE_Regn.SetInitText(m_Regn);
	}

	if(prmDFlight->Urno != 0)
	{
		m_Regn = CString(prmDFlight->Regn);
		m_Act3 = CString(prmDFlight->Act3);
		m_Act5 = CString(prmDFlight->Act5);
		m_Ming = CString(prmDFlight->Ming);

		m_CE_Ming.SetInitText(m_Ming);
		m_CE_Act3.SetInitText(m_Act3);
		m_CE_Act5.SetInitText(m_Act5);
		m_CE_Regn.SetInitText(m_Regn);
	}



	m_CL_ARemp.ResetContent();
	m_CL_DRemp.ResetContent();


	m_CL_ARemp.AddString(" ");
	m_CL_DRemp.AddString(" ");
	
	int ilAIndex = 0;	
	int ilDIndex = 0;	
	CString olCode;
	CString olItem;
	
	int ilCount = ogBCD.GetDataCount("FID");
	ogBCD.SetSort("FID", ogFIDRemarkField+"+", true);


	CString olCodeA = CString(prmAFlight->Remp);
	CString olCodeD = CString(prmDFlight->Remp);

	olCodeA.TrimRight();
	olCodeD.TrimRight();

	for(int i = 0; i < ilCount; i++)
	{
		olItem = ogBCD.GetFields("FID",i , "CODE," + ogFIDRemarkField, "  ", FILLBLANK);
		m_CL_ARemp.AddString(olItem);
		m_CL_DRemp.AddString(olItem);
		olCode = ogBCD.GetField("FID",i , "CODE" );
		
		if((olCode.Find(olCodeA) >= 0) && (!olCodeA.IsEmpty()))
			ilAIndex = i + 1;
		
		if((olCode.Find(olCodeD) >= 0) && (!olCodeD.IsEmpty()))
			ilDIndex = i + 1;
	}
	
	m_CL_ARemp.SetCurSel(ilAIndex);
	m_CL_DRemp.SetCurSel(ilDIndex);


	if(bpArrival && bpDeparture)
	{
		bmChanged = false;
		EnableGlobal();
		EnableArrival();
		EnableDeparture();
	}
	else
	{
		if(bpArrival)
		{
			EnableGlobal();
			EnableArrival();
		}
		else
		{
			EnableGlobal();
			EnableDeparture();
		}
	}

	// Set Towing-Button colour
	CheckTowingButton();
	CheckViaButton();

	//is it a postflight?
	HandlePostFlight(); 

	SetHistoryNamesForEdit();
	InitHistoryCombos();

} 

void RotationDlg::OnEditDbClk( UINT wParam, LPARAM lParam)
{

	CCS_TRY

	CCSEDITNOTIFY *prlNotify = (CCSEDITNOTIFY*)lParam;

	if (prlNotify->Name.IsEmpty()) 
		return;

	CallHistory(prlNotify->Name);

	CCS_CATCH_ALL	

	return;
}

bool RotationDlg::InitHistoryCombos()
{
	if(prmAFlight->Urno > 0) 
	{
		m_ComboAHistory.ResetContent();
		CString olFieldList = GetString(IDS_AHISTORY);

		CStringArray olFieldArray;
		int ilFieldArray = ExtractItemList(olFieldList, &olFieldArray, ',');

		for (int l=0; l<olFieldArray.GetSize(); l++)
		{
			m_ComboAHistory.AddString(olFieldArray.GetAt(l));
		}
	}

	if(prmDFlight->Urno > 0) 
	{
		m_ComboDHistory.ResetContent();
		CString olFieldList = GetString(IDS_DHISTORY);

		CStringArray olFieldArray;
		int ilFieldArray = ExtractItemList(olFieldList, &olFieldArray, ',');

		for (int l=0; l<olFieldArray.GetSize(); l++)
		{
			m_ComboDHistory.AddString(olFieldArray.GetAt(l));
		}
	}

	return true;
}


bool RotationDlg::SetHistoryNamesForEdit()
{
	if(prmAFlight->Urno > 0) 
	{
		m_CE_AAlc3.SetName("AFTAALC3C");
		m_CE_AFltn.SetName("AFTAFLTNC");
		m_CE_AFlns.SetName("AFTAFLNSC");
		m_CE_AFlti.SetName("AFTAFLTIC");
		m_CE_ATtyp.SetName("AFTATTYPC");
		m_CE_AStyp.SetName("AFTASTYPC");
		m_CE_AStev.SetName("AFTASTEVC");
		m_CE_AOrg3.SetName("AFTAORG3C");
//		m_CE_AOrg4.SetName("AFTAORG4");
		m_CE_AStoa.SetName("AFTASTOAD");
		m_CE_AStod.SetName("AFTASTODD");
		m_CE_AEtai.SetName("AFTAETAID");
		m_CE_APsta.SetName("AFTAPSTAC");
		m_CE_AEtdi.SetName("AFTAETDID");
		m_CE_AOfbl.SetName("AFTAOFBLD");
		m_CE_AAirb.SetName("AFTAAIRBD");
		m_CE_AEtoa.SetName("AFTAETOAD");
		m_CE_AOnbe.SetName("AFTAONBED");
		m_CE_ANxti.SetName("AFTANXTID");
		m_CE_ALand.SetName("AFTALANDD");
		m_CE_AOnbl.SetName("AFTAONBLD");
		m_CE_ATmoa.SetName("AFTATMOAD");
		m_CE_ARwya.SetName("AFTARWYAC");
		m_CE_AIfra.SetName("AFTAIFRAC");
		m_CE_ADcd1.SetName("AFTADCD1C");
		m_CE_ADcd2.SetName("AFTADCD2C");
		m_CE_ADtd1.SetName("AFTADTD1C");
		m_CE_ADtd2.SetName("AFTADTD2C");
		m_CE_ABbaa.SetName("AFTABBAAC");
		m_CE_AGta1.SetName("AFTAGTA1C");
		m_CE_AGta2.SetName("AFTAGTA2C");
		m_CE_AGa1x.SetName("AFTAGA1XD");
		m_CE_AGa2x.SetName("AFTAGA2XD");
		m_CE_AGa1y.SetName("AFTAGA1YD");
		m_CE_AGa2y.SetName("AFTAGA2YD");
		m_CE_ABlt1.SetName("AFTABLT1C");
		m_CE_ABlt2.SetName("AFTABLT2C");
		m_CE_AB1ba.SetName("AFTAB1BAD");
		m_CE_AB2ba.SetName("AFTAB2BAD");
		m_CE_AB1ea.SetName("AFTAB1EAD");
		m_CE_AB2ea.SetName("AFTAB2EAD");
		m_CE_AFBag.SetName("AFTABAS1D");
		m_CE_ALBag.SetName("AFTABAE1D");
		m_CE_AExt1.SetName("AFTAEXT1C");
		m_CE_AExt2.SetName("AFTAEXT2C");
		m_CE_ARem1.SetName("AFTAREM1C");
		m_CE_ADivr.SetName("AFTADIVRC");
		m_CE_ACsgn.SetName("AFTACSGNC");
		m_CE_Act3.SetName("AFTRACT3C");
		m_CE_Act5.SetName("AFTRACT5C");
		m_CE_Regn.SetName("AFTRREGNC");
		m_CE_Ming.SetName("AFTRMINGC");
	}
	if(prmAFlight->Urno > 0) 
	{
		m_CE_DAlc3.SetName("AFTDALC3C");
		m_CE_DFltn.SetName("AFTDFLTNC");
		m_CE_DFlns.SetName("AFTDFLNSC");
		m_CE_DFlti.SetName("AFTDFLTIC");
		m_CE_DTtyp.SetName("AFTDTTYPC");
		m_CE_DStyp.SetName("AFTDSTYPC");
		m_CE_DStev.SetName("AFTDSTEVC");
		m_CE_DDes3.SetName("AFTDDES3C");
		m_CE_DDes4.SetName("AFTDDES4C");
		m_CE_DStoa.SetName("AFTDSTOAD");
		m_CE_DStod.SetName("AFTDSTODD");
		m_CE_DEtdi.SetName("AFTDETDID");
		m_CE_DPstd.SetName("AFTDPSTDC");
		m_CE_DOfbl.SetName("AFTDOFBLD");
		m_CE_DAirb.SetName("AFTDAIRBD");
		m_CE_DEtod.SetName("AFTDETODD");
		m_CE_DEtdc.SetName("AFTDETDCD");
		m_CE_DSlot.SetName("AFTDSLOTD");
		m_CE_DRwyd.SetName("AFTDRWYDC");
		m_CE_DIfrd.SetName("AFTDIFRDC");
		m_CE_DIskd.SetName("AFTDISKDC");
		m_CE_DNxti.SetName("AFTDNXTID");
		m_CE_DBbfa.SetName("AFTDBBFAD");
		m_CE_DDcd1.SetName("AFTDDCD1C");
		m_CE_DDcd2.SetName("AFTDDCD2C");
		m_CE_DDtd1.SetName("AFTDDTD1C");
		m_CE_DDtd2.SetName("AFTDDTD2C");
//		m_CE_DGtd1.SetName("AFTDGTD1");
//		m_CE_DGtd2.SetName("AFTDGTD2");
		m_CE_DGtd1.SetName("AFTDGD1PC");
		m_CE_DGtd2.SetName("AFTDGD2PC");
		m_CE_DGd1x.SetName("AFTDGD1XD");
		m_CE_DGd2x.SetName("AFTDGD2XD");
		m_CE_DGd1y.SetName("AFTDGD1YD");
		m_CE_DGd2y.SetName("AFTDGD2YD");
		m_CE_DWro1.SetName("AFTDWRO1C");
		m_CE_DW1ba.SetName("AFTDW1BAD");
		m_CE_DW1ea.SetName("AFTDW1EAD");
		m_CE_DBaz1.SetName("AFTDBAZ1C");
		m_CE_DBao1.SetName("AFTDBAO1D");
		m_CE_DBac1.SetName("AFTDBAC1D");
		m_CE_DBaz4.SetName("AFTDBAZ4C");
		m_CE_DBao4.SetName("AFTDBAO4D");
		m_CE_DBac4.SetName("AFTDBAC4D");
		m_CE_DRem1.SetName("AFTDREM1C");
		m_CE_DCsgn.SetName("AFTDCSGNC");
		m_CE_Act3.SetName("AFTRACT3C");
		m_CE_Act5.SetName("AFTRACT5C");
		m_CE_Regn.SetName("AFTRREGNC");
		m_CE_Ming.SetName("AFTRMINGC");
	}

	return true;
}


void RotationDlg::CallHistory(CString opField)
{
	if (opField.IsEmpty())
		return;

	CString olTable = opField.Left(3);
	CString olField = opField.Mid(4,4);
	CString olAdid = opField.Mid(3,1);
	CString olType = opField.Right(1);

	//----------
	char clStat;
	clStat = ogPrivList.GetStat("ROTATIONDLG_CL_AHistory");

	if(olAdid == "A")
	{
		if(clStat != '1')
			return;
	}

	clStat = ogPrivList.GetStat("ROTATIONDLG_CL_DHistory");

	if(olAdid == "D")
	{
		if(clStat != '1')
			return;
	}
	//----------


	char Abuffer[20];
	char Dbuffer[20];


	char pclConfigPath[256];
	char pclExcelPath[256];

	if (getenv("CEDA") == NULL)
		strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
	else
		strcpy(pclConfigPath, getenv("CEDA"));

	GetPrivateProfileString(ogAppName, "DATA_CHANGES", "DEFAULT",
		pclExcelPath, sizeof pclExcelPath, pclConfigPath);


	if(!strcmp(pclExcelPath, "DEFAULT"))
	{
		MessageBox(GetString(IDS_DATACHANGES), GetString(ST_FEHLER), MB_ICONERROR);
		return;
	}
	char slRunTxt[512] = ""; 

	CString olTimeMode = "U";
	if (bmLocalTime)
		olTimeMode = "L";

	if (olAdid == "A")
		sprintf(slRunTxt,"%s;%s,%s,%s,%s,%s,%s,%s,%s", ltoa(prmAFlight->Urno,Abuffer,10), "0", olTable, olField, "FIELD", olTimeMode, "", olType, CString(pcgUser));
	else if (olAdid == "D")
		sprintf(slRunTxt,"%s;%s,%s,%s,%s,%s,%s,%s,%s", "0", ltoa(prmDFlight->Urno,Dbuffer,10), olTable, olField, "FIELD", olTimeMode, "", olType, CString(pcgUser));
	else
		sprintf(slRunTxt,"%s;%s,%s,%s,%s,%s,%s,%s,%s", ltoa(prmAFlight->Urno,Abuffer,10), ltoa(prmDFlight->Urno,Dbuffer,10), olTable, olField, "FIELD", olTimeMode, "", olType, CString(pcgUser));

	char *args[4];
	args[0] = "child";
	args[1] = slRunTxt;
	args[2] = NULL;
	args[3] = NULL;
	int ilReturn = _spawnv(_P_NOWAIT,pclExcelPath,args);
}

void RotationDlg::OnSelchangeAHistory() 
{
	CString olOperator;
	int ilCur = m_ComboAHistory.GetCurSel();
	m_ComboAHistory.GetLBText(ilCur, olOperator);

	CString olFieldList = GetString(IDS_AHISTORY_FIELD);

	CStringArray olFieldArray;
	int ilFieldArray = ExtractItemList(olFieldList, &olFieldArray, ',');

	if (olFieldArray.GetSize() > ilCur)
	{
		CString olField = olFieldArray.GetAt(ilCur);
		if (olField.IsEmpty())
			return;

		CallHistory(olField);
	}
	else
		return;

}

void RotationDlg::OnSelchangeDHistory() 
{
	CString olOperator;
	int ilCur = m_ComboDHistory.GetCurSel();
	m_ComboDHistory.GetLBText(ilCur, olOperator);

	CString olFieldList = GetString(IDS_DHISTORY_FIELD);

	CStringArray olFieldArray;
	int ilFieldArray = ExtractItemList(olFieldList, &olFieldArray, ',');

	if (olFieldArray.GetSize() > ilCur)
	{
		CString olField = olFieldArray.GetAt(ilCur);
		if (olField.IsEmpty())
			return;

		CallHistory(olField);
	}
	else
		return;

}


void RotationDlg::ArrivalTifd(CTime& opTime) 
{
	opTime = TIMENULL;

	if(!m_AAirb.IsEmpty())
	{
		opTime = HourStringToDate(m_AAirb, prmAFlight->Stoa);
	}
	else
	{
		if(!m_AOfbl.IsEmpty())
		{
			opTime = HourStringToDate(m_AOfbl, prmAFlight->Stoa);
		}
		else
		{
			if(!m_AEtdi.IsEmpty())
			{
				opTime = HourStringToDate(m_AEtdi, prmAFlight->Stoa);
			}
			else
			{
				if(!m_AStod.IsEmpty())
				{
					opTime = HourStringToDate(m_AStod, prmAFlight->Stoa);
				}
			}
		}
	}
}

void RotationDlg::ArrivalTifa(CTime& opTime) 
{
	opTime = TIMENULL;
	if(!m_ALand.IsEmpty())
	{
		opTime = HourStringToDate(m_ALand, prmAFlight->Stoa);
	}
	else
	{
		if(!m_AOnbl.IsEmpty())
		{
			opTime = HourStringToDate(m_AOnbl, prmAFlight->Stoa);
		}
		else
		{
			if(!m_AEtai.IsEmpty())
			{
				opTime = HourStringToDate(m_AEtai, prmAFlight->Stoa);
			}
			else
			{
				if(!m_AStoa.IsEmpty())
				{
					opTime = HourStringToDate(m_AStoa, prmAFlight->Stoa);
				}
			}
		}
	}
}

void RotationDlg::DepartureTifd(CTime& opTime) 
{
	opTime = TIMENULL;
	if(!m_DAirb.IsEmpty())
	{
		opTime = HourStringToDate(m_DAirb, prmDFlight->Stod);
	}
	else
	{
		if(!m_DOfbl.IsEmpty())
		{
			opTime = HourStringToDate(m_DOfbl, prmDFlight->Stod);
		}
		else
		{
			if(!m_DEtdi.IsEmpty())
			{
				opTime = HourStringToDate(m_DEtdi, prmDFlight->Stod);
			}
			else
			{
				if(!m_DStod.IsEmpty())
				{
					opTime = HourStringToDate(m_DStod, prmDFlight->Stod);
				}
			}
		}
	}
}

void RotationDlg::DepartureTifa(CTime& opTime) 
{
	opTime = TIMENULL;
	if(!m_DStoa.IsEmpty())
	{
		opTime = HourStringToDate(m_DStoa, prmDFlight->Stod);
	}
	
}

/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
// Fill the SEASONFLIGHTDATArecords
//
void RotationDlg::FillFlightData() 
{
	CTime olTime;

	//killfocus - check for the curent field
	NextDlgCtrl(); 
	PrevDlgCtrl();

	m_CB_Ok.SetFocus();


	///////////////////////////////////////////
	// globals
	//
	m_CE_Regn.GetWindowText(m_Regn);
	m_CE_Ming.GetWindowText(m_Ming);
	m_CE_Act3.GetWindowText(m_Act3);
	m_CE_Act5.GetWindowText(m_Act5);


	///////////////////////////////////////////
	// Ankunft
	//

	m_GoAround = m_CB_GoAround.GetCheck(); 
	m_ACxx = m_CB_ACxx.GetCheck(); 
	m_ADiverted = m_CB_ADiverted.GetCheck(); 
	m_ARerouted = m_CB_ARerouted.GetCheck(); 

//first/last Bag
//	m_AFBag = m_AB1ba;
	m_CE_AFBag.GetWindowText(m_AFBag);
	m_CE_ALBag.GetWindowText(m_ALBag);

	m_CE_AAlc3.GetWindowText(m_AAlc3);
	m_CE_AFltn.GetWindowText(m_AFltn);
	m_CE_AFlns.GetWindowText(m_AFlns);
	m_CE_AFlti.GetWindowText(m_AFlti);
	m_CE_ACsgn.GetWindowText(m_ACsgn);
	m_CE_ATtyp.GetWindowText(m_ATtyp);
//	m_CE_AHtyp.GetWindowText(m_AHtyp);
	m_CE_AStyp.GetWindowText(m_AStyp);
	m_CE_AStev.GetWindowText(m_AStev);
	m_CE_ADes3.GetWindowText(m_ADes3);
	m_CE_AOrg3.GetWindowText(m_AOrg3);
	m_CE_AEtai.GetWindowText(m_AEtai);
	m_CE_AStod.GetWindowText(m_AStod);
	m_CE_AStoa.GetWindowText(m_AStoa);
	m_CE_AEtai.GetWindowText(m_AEtai);
	m_CE_AEtoa.GetWindowText(m_AEtoa);
	m_CE_AOnbe.GetWindowText(m_AOnbe);
	m_CE_ANxti.GetWindowText(m_ANxti);
	m_CE_AOnbl.GetWindowText(m_AOnbl);
	m_CE_AOfbl.GetWindowText(m_AOfbl);
	m_CE_AAirb.GetWindowText(m_AAirb);
	m_CE_ALand.GetWindowText(m_ALand);
	m_CE_ATmoa.GetWindowText(m_ATmoa);
	m_CE_ADtd1.GetWindowText(m_ADtd1);
	m_CE_ADtd2.GetWindowText(m_ADtd2);
	m_CE_ADcd1.GetWindowText(m_ADcd1);
	m_CE_ADcd2.GetWindowText(m_ADcd2);
	m_CE_ARwya.GetWindowText(m_ARwya);
	m_CE_AIfra.GetWindowText(m_AIfra);
	m_CE_APsta.GetWindowText(m_APsta);
//	m_CE_APaba.GetWindowText(m_APaba);
	m_CE_AEtdi.GetWindowText(m_AEtdi);
	m_CE_APaea.GetWindowText(m_APaea);
	m_CE_AGta1.GetWindowText(m_AGta1);
	m_CE_AGta2.GetWindowText(m_AGta2);
	m_CE_ATga1.GetWindowText(m_ATga1);
	m_CE_ATga2.GetWindowText(m_ATga2);
	m_CE_AGa1x.GetWindowText(m_AGa1x);
	m_CE_AGa2x.GetWindowText(m_AGa2x);
	m_CE_AGa1y.GetWindowText(m_AGa1y);
	m_CE_AGa2y.GetWindowText(m_AGa2y);
	m_CE_ABlt1.GetWindowText(m_ABlt1);
	m_CE_ABlt2.GetWindowText(m_ABlt2);
	m_CE_ATmb1.GetWindowText(m_ATmb1);
	m_CE_ATmb2.GetWindowText(m_ATmb2);
	m_CE_AB1ba.GetWindowText(m_AB1ba);
	m_CE_AB2ba.GetWindowText(m_AB2ba);
	m_CE_AB1ea.GetWindowText(m_AB1ea);
	m_CE_AB2ea.GetWindowText(m_AB2ea);
	m_CE_AExt1.GetWindowText(m_AExt1);
	m_CE_AExt2.GetWindowText(m_AExt2);
	m_CE_ATet1.GetWindowText(m_ATet1);
	m_CE_ATet2.GetWindowText(m_ATet2);
	m_CE_ARem1.GetWindowText(m_ARem1);
	m_CE_ABbaa.GetWindowText(m_ABbaa);
	m_CE_ADivr.GetWindowText(m_ADivr);



	if(m_CE_ATtyp.IsChanged())
		strcpy(prmAFlight->Sttt,"U");


	strcpy(prmAFlight->Regn, m_Regn);
	strcpy(prmAFlight->Ming, m_Ming);
	strcpy(prmAFlight->Act3, m_Act3);
	strcpy(prmAFlight->Act5, m_Act5);


	CString olFlno = ogRotationDlgFlights.CreateFlno(m_AAlc3, m_AFltn, m_AFlns); 
	CString olFlnoT(olFlno);
	olFlnoT.TrimRight();
	if (strcmp(prmAFlight->Flno, olFlnoT) != 0 && strcmp(prmAFlight->Flno, olFlno) != 0)		
		strcpy(prmAFlight->Flno, olFlno);


	strcpy(prmAFlight->Alc3,omAAlc3);
	strcpy(prmAFlight->Alc2,omAAlc2);
	strcpy(prmAFlight->Fltn,m_AFltn);
	strcpy(prmAFlight->Flti,m_AFlti);
	strcpy(prmAFlight->Csgn,m_ACsgn);
	strcpy(prmAFlight->Flns,m_AFlns);
	strcpy(prmAFlight->Ttyp,m_ATtyp);
//	strcpy(prmAFlight->Htyp,m_AHtyp);
	strcpy(prmAFlight->Styp,m_AStyp);
	strcpy(prmAFlight->Stev,m_AStev);
	strcpy(prmAFlight->Divr,m_ADivr);

	strcpy(prmAFlight->Des3,pcgHome);
	strcpy(prmAFlight->Org3,pcmAOrg3);
	strcpy(prmAFlight->Org4,pcmAOrg4);
	strcpy(prmAFlight->Des4,pcgHome4);

	prmAFlight->Stoa = HourStringToDate( m_AStoa, prmAFlight->Stoa);
	prmAFlight->Stod = HourStringToDate(m_AStod, prmAFlight->Stoa);
	prmAFlight->Etoa = HourStringToDate(m_AEtoa, prmAFlight->Stoa);

	// will be set by the fight process !!
	//prmAFlight->Onbe = HourStringToDate(m_AOnbe, prmAFlight->Stoa);
	prmAFlight->Nxti = HourStringToDate(m_ANxti, prmAFlight->Stoa);
	

	if(m_CE_ALand.IsChanged())	
	{
		if(!m_ALand.IsEmpty())
		{
			prmAFlight->Lndu = HourStringToDate(m_ALand, prmAFlight->Stoa);
			prmAFlightSave->Lndu = TIMENULL; // Because this field must be saved in the DB!
		}
		else
		{
			if( strcmp(prmAFlight->Stld, "D") == 0)
			{
				prmAFlight->Lndd = TIMENULL;	

			}else
			if( strcmp(prmAFlight->Stld, "A") == 0)
			{
				prmAFlight->Lnda = TIMENULL;	

			}else
			{
				prmAFlight->Lndu = TIMENULL;	

			}
		}
	}

	if(m_CE_AEtai.IsChanged())	
	{
		if(!m_AEtai.IsEmpty())
		{
			prmAFlight->Etau = HourStringToDate(m_AEtai, prmAFlight->Stoa);
			prmAFlightSave->Etau = TIMENULL; // Because this field must be saved in the DB!
		}
		else
		{
			if( strcmp(prmAFlight->Stea, "A") == 0)
			{
				prmAFlight->Etaa = TIMENULL;	

			}else
			if( strcmp(prmAFlight->Stea, "E") == 0)
			{
				prmAFlight->Etae = TIMENULL;	

			}else
			if( strcmp(prmAFlight->Stea, "C") == 0)
			{
				prmAFlight->Etac = TIMENULL;	

			}else
			{
				prmAFlight->Etau = TIMENULL;	

			}
		}
	}

	if(m_CE_AOnbl.IsChanged())	
	{
		if(!m_AOnbl.IsEmpty())
		{
			prmAFlight->Onbu = HourStringToDate(m_AOnbl, prmAFlight->Stoa);
			prmAFlightSave->Onbu = TIMENULL; // Because this field must be saved in the DB!
		}
		else
		{
			if( strcmp(prmAFlight->Ston, "D") == 0)
			{
				prmAFlight->Onbd = TIMENULL;	

			}else
			if( strcmp(prmAFlight->Ston, "S") == 0)
			{
				prmAFlight->Onbs = TIMENULL;	

			}else
			//if( strcmp(prmAFlight->Ston, "U") == 0)
			{
				prmAFlight->Onbu = TIMENULL;	

			}
		}
	}

	if(m_CE_AOfbl.IsChanged())	
	{
		if(!m_AOfbl.IsEmpty())
		{
			prmAFlight->Ofbu = HourStringToDate(m_AOfbl, prmAFlight->Stoa);
			prmAFlightSave->Ofbu = TIMENULL; // Because this field must be saved in the DB!
		}
		else
		{
			if( strcmp(prmAFlight->Stof, "D") == 0)
			{
				prmAFlight->Ofbd = TIMENULL;	

			}else
			if( strcmp(prmAFlight->Stof, "S") == 0)
			{
				prmAFlight->Ofbs = TIMENULL;	

			}else
			//if( strcmp(prmAFlight->Stof, "U") == 0)
			{
				prmAFlight->Ofbu = TIMENULL;	

			}
		}
	}



	if(m_CE_AAirb.IsChanged())	
	{
		if(!m_AAirb.IsEmpty())
		{
			prmAFlight->Airu = HourStringToDate(m_AAirb, prmAFlight->Stoa);
			prmAFlightSave->Airu = TIMENULL; // Because this field must be saved in the DB!
		}
		else
		{
			if( strcmp(prmAFlight->Stab, "D") == 0)
			{
				prmAFlight->Aird = TIMENULL;	

			}else
			if( strcmp(prmAFlight->Stab, "A") == 0)
			{
				prmAFlight->Aira = TIMENULL;	

			}else
			//if( strcmp(prmAFlight->Stab, "U") == 0)
			{
				prmAFlight->Airu = TIMENULL;	

			}
		}
	}


	if(m_CE_ATmoa.IsChanged())	
	{
		if(!m_ATmoa.IsEmpty())
		{
			prmAFlight->Tmau = HourStringToDate(m_ATmoa, prmAFlight->Stoa);
			prmAFlightSave->Tmau = TIMENULL; // Because this field must be saved in the DB!
		}
		else
		{
			if( strcmp(prmAFlight->Sttm, "A") == 0)
			{
				prmAFlight->Tmaa = TIMENULL;	

			}else
			//if( strcmp(prmAFlight->Sttm, "U") == 0)
			{
				prmAFlight->Tmau = TIMENULL;	

			}
		}
	}

	if(m_CE_AEtdi.IsChanged())	
	{
		if(!m_AEtdi.IsEmpty())
		{
			prmAFlight->Etdu = HourStringToDate(m_AEtdi, prmAFlight->Stoa);
			prmAFlightSave->Etdu = TIMENULL; // Because this field must be saved in the DB!
		}
		else
		{
			if( strcmp(prmAFlight->Sted, "A") == 0)
			{
				prmAFlight->Etda = TIMENULL;	

			}else
			if( strcmp(prmAFlight->Sted, "E") == 0)
			{
				prmAFlight->Etde = TIMENULL;	

			}else
			if( strcmp(prmAFlight->Sted, "C") == 0)
			{
				prmAFlight->Etdc = TIMENULL;	

			}else
			//if( strcmp(prmDFlight->Sted, "U") == 0)
			{
				prmAFlight->Etdu = TIMENULL;	

			}
		}
	}

	if(m_ADtd1.GetLength() > 0)
		FillLeftByChar(m_ADtd1, '0' , 4);

	if(m_ADtd2.GetLength() > 0)
		FillLeftByChar(m_ADtd2, '0' , 4);


	strcpy(prmAFlight->Dtd1,m_ADtd1);
	strcpy(prmAFlight->Dtd2,m_ADtd2);

	strcpy(prmAFlight->Dcd1,m_ADcd1);
	strcpy(prmAFlight->Dcd2,m_ADcd2);
	strcpy(prmAFlight->Rwya,m_ARwya);
	strcpy(prmAFlight->Ifra,m_AIfra);
	strcpy(prmAFlight->Psta,m_APsta);
	strcpy(prmAFlight->Gta1,m_AGta1);
	strcpy(prmAFlight->Gta2,m_AGta2);
	strcpy(prmAFlight->Blt1,m_ABlt1);
	strcpy(prmAFlight->Blt2,m_ABlt2);
	strcpy(prmAFlight->Ext1,m_AExt1);
	strcpy(prmAFlight->Ext2,m_AExt2);
	strcpy(prmAFlight->Rem1,m_ARem1);
	strcpy(prmAFlight->Ming,m_Ming);
	strcpy(prmAFlight->Act3,m_Act3);
	strcpy(prmAFlight->Act5,m_Act5);
	strcpy(prmAFlight->Psta, m_APsta);
	strcpy(prmAFlight->Gta1, m_AGta1);
	strcpy(prmAFlight->Gta2, m_AGta2);
	strcpy(prmAFlight->Tga1, m_ATga1);
	strcpy(prmAFlight->Tga2, m_ATga2);
	strcpy(prmAFlight->Blt1, m_ABlt1);
	strcpy(prmAFlight->Blt2, m_ABlt2);
	strcpy(prmAFlight->Tmb1, m_ATmb1);
	strcpy(prmAFlight->Tmb2, m_ATmb2);
	strcpy(prmAFlight->Ext1, m_AExt1);
	strcpy(prmAFlight->Ext2, m_AExt2);
	strcpy(prmAFlight->Tet1, m_ATet1);
	strcpy(prmAFlight->Tet2, m_ATet2);

	strcpy(prmAFlight->Rem1,m_ARem1);
	if(m_ARem1.IsEmpty())
		strcpy(prmAFlight->Isre,"");
	else
		strcpy(prmAFlight->Isre,"+");

//	GetDayOfWeek(prmAFlight->Stoa, prmAFlight->Dooa);

//	prmAFlight->Paba = HourStringToDate( m_APaba, prmAFlight->Stoa);
	prmAFlight->Paea = HourStringToDate( m_APaea, prmAFlight->Stoa);
	prmAFlight->B1ba = HourStringToDate( m_AB1ba, prmAFlight->Stoa);
	prmAFlight->B2ba = HourStringToDate( m_AB2ba, prmAFlight->Stoa);
	prmAFlight->B1ea = HourStringToDate( m_AB1ea, prmAFlight->Stoa);
	prmAFlight->B2ea = HourStringToDate( m_AB2ea, prmAFlight->Stoa);
	prmAFlight->Ga1x = HourStringToDate( m_AGa1x, prmAFlight->Stoa);
	prmAFlight->Ga2x = HourStringToDate( m_AGa2x, prmAFlight->Stoa);
	prmAFlight->Ga1y = HourStringToDate( m_AGa1y, prmAFlight->Stoa);
	prmAFlight->Ga2y = HourStringToDate( m_AGa2y, prmAFlight->Stoa);
	prmAFlight->Bbaa = HourStringToDate( m_ABbaa, prmAFlight->Stoa);

//first/last Bag
//	prmAFlight->Bas1 = HourStringToDate( m_AFBag, prmAFlight->Stoa);
//	prmAFlight->Bae1 = HourStringToDate( m_ALBag, prmAFlight->Stoa);

	int ilCurSel;
	CString olTmp;


	if((ilCurSel = m_CL_ARemp.GetCurSel()) == CB_ERR)
		strcpy(prmAFlight->Remp, "");
	else
	{
		m_CL_ARemp.GetLBText(ilCurSel, olTmp);
		olTmp = olTmp.Left(4);
		olTmp.TrimRight();
		if(!olTmp.IsEmpty())
			strcpy(prmAFlight->Remp, olTmp);
		else
			strcpy(prmAFlight->Remp, "");
	}


	if (bmChanged)
		strcpy(prmAFlight->Ftyp, "O");
	
	if(m_GoAround == TRUE)
		strcpy(prmAFlight->Baz6, "G");
	else
		strcpy(prmAFlight->Baz6, "");

	if(m_ACxx == TRUE)
		strcpy(prmAFlight->Ftyp, "X");

	if(m_ADiverted == TRUE)
		strcpy(prmAFlight->Ftyp, "D");

	if(m_ARerouted == TRUE)
		strcpy(prmAFlight->Ftyp, "R");


	if(m_CB_ARetFlight.GetCheck())
		strcpy(prmAFlight->Ftyp, "Z");

	if(m_CB_ARetTaxi.GetCheck())
		strcpy(prmAFlight->Ftyp, "B");

	///////////////////////////////////////////////
	// weitere Flugnummern ankunft
	//
	CString olAlc3;
	CString olFltn;
	CString olFlns;

	strcpy(prmAFlight->Jfno, "");
	int ilJcnt = 0;
	CString olJfno;

	int ilLines = pomAJfnoTable->GetLinesCount();
	for(int i = 0; i < ilLines; i++)
	{
		pomAJfnoTable->GetTextFieldValue(i, 0, olAlc3);
		pomAJfnoTable->GetTextFieldValue(i, 1, olFltn);
		pomAJfnoTable->GetTextFieldValue(i, 2, olFlns);
		if(!olAlc3.IsEmpty() || !olFltn.IsEmpty())
		{
			olJfno +=   ogRotationDlgFlights.CreateFlno(olAlc3, olFltn, olFlns);
			ilJcnt++;
		}
	}
	olJfno.TrimRight();
	strcpy(prmAFlight->Jfno, olJfno);
	if (ilJcnt > 0)
		itoa(ilJcnt, prmAFlight->Jcnt, 10);


	///////////////////////////////////////////////
	// vias ankunft
	if (polRotationAViaDlg)
		polRotationAViaDlg->GetViaList(prmAFlight->Vial);
	
	
	///////////////////////////////////////////
	// Abflug
	//

	m_DCxx = m_CB_DCxx.GetCheck(); 
	m_DDiverted = m_CB_DDiverted.GetCheck(); 
	m_DRerouted = m_CB_DRerouted.GetCheck(); 
	
	m_CE_DAlc3.GetWindowText(m_DAlc3);
	m_CE_DFltn.GetWindowText(m_DFltn);
	m_CE_DFlns.GetWindowText(m_DFlns);
	m_CE_DFlti.GetWindowText(m_DFlti);
	m_CE_DCsgn.GetWindowText(m_DCsgn);
	m_CE_DDes3.GetWindowText(m_DDes3);
	m_CE_DDes4.GetWindowText(m_DDes4);
	m_CE_DOrg3.GetWindowText(m_DOrg3);
	m_CE_DTtyp.GetWindowText(m_DTtyp);
//	m_CE_DHtyp.GetWindowText(m_DHtyp);
	m_CE_DStyp.GetWindowText(m_DStyp);
	m_CE_DStev.GetWindowText(m_DStev);
	m_CE_DRem1.GetWindowText(m_DRem1);
	m_CE_DEtdi.GetWindowText(m_DEtdi);
	m_CE_DStod.GetWindowText(m_DStod);
	m_CE_DStoa.GetWindowText(m_DStoa);
	m_CE_DOfbl.GetWindowText(m_DOfbl);
	m_CE_DAirb.GetWindowText(m_DAirb);
	m_CE_DEtdi.GetWindowText(m_DEtdi);
	m_CE_DEtod.GetWindowText(m_DEtod);
	m_CE_DEtdc.GetWindowText(m_DEtdc);
	m_CE_DIskd.GetWindowText(m_DIskd);
	m_CE_DSlot.GetWindowText(m_DSlot);
	m_CE_DNxti.GetWindowText(m_DNxti);
	m_CE_DDtd1.GetWindowText(m_DDtd1);
	m_CE_DDtd2.GetWindowText(m_DDtd2);
	m_CE_DDcd1.GetWindowText(m_DDcd1);
	m_CE_DDcd2.GetWindowText(m_DDcd2);
	m_CE_DRwyd.GetWindowText(m_DRwyd);
	m_CE_DIfrd.GetWindowText(m_DIfrd);
	m_CE_DPstd.GetWindowText(m_DPstd);
	m_CE_DPdba.GetWindowText(m_DPdba);
	m_CE_DPdea.GetWindowText(m_DPdea);
	m_CE_DGtd1.GetWindowText(m_DGtd1);
	m_CE_DGtd2.GetWindowText(m_DGtd2);
	m_CE_DTgd1.GetWindowText(m_DTgd1);
	m_CE_DTgd2.GetWindowText(m_DTgd2);
	m_CE_DGd1x.GetWindowText(m_DGd1x);
	m_CE_DGd2x.GetWindowText(m_DGd2x);
	m_CE_DGd1y.GetWindowText(m_DGd1y);
	m_CE_DGd2y.GetWindowText(m_DGd2y);
	m_CE_DWro1.GetWindowText(m_DWro1);
	m_CE_DTwr1.GetWindowText(m_DTwr1);
	m_CE_DW1ba.GetWindowText(m_DW1ba);
	m_CE_DW1ea.GetWindowText(m_DW1ea);
	m_CE_DBbfa.GetWindowText(m_DBbfa);
	m_CE_DDivr.GetWindowText(m_DDivr);


	m_CE_DBaz1.GetWindowText(m_DBaz1);
	m_CE_DBaz4.GetWindowText(m_DBaz4);
	m_CE_DBao1.GetWindowText(m_DBao1);
	m_CE_DBac1.GetWindowText(m_DBac1);
	m_CE_DBao4.GetWindowText(m_DBao4);
	m_CE_DBac4.GetWindowText(m_DBac4);

	strcpy(prmDFlight->Baz1, m_DBaz1);
	strcpy(prmDFlight->Baz4, m_DBaz4);
	prmDFlight->Bao1 = HourStringToDate(m_DBao1, prmDFlight->Stod);
	prmDFlight->Bac1 = HourStringToDate(m_DBac1, prmDFlight->Stod);
	prmDFlight->Bao4 = HourStringToDate(m_DBao4, prmDFlight->Stod);
	prmDFlight->Bac4 = HourStringToDate(m_DBac4, prmDFlight->Stod);

	if(m_CE_DTtyp.IsChanged())
		strcpy(prmDFlight->Sttt,"U");


	strcpy(prmDFlight->Regn, m_Regn);
	strcpy(prmDFlight->Ming, m_Ming);
	strcpy(prmDFlight->Act3, m_Act3);
	strcpy(prmDFlight->Act5, m_Act5);


	olFlno = ogRotationDlgFlights.CreateFlno(m_DAlc3, m_DFltn, m_DFlns); 
	olFlnoT = olFlno;
	olFlnoT.TrimRight();
	if (strcmp(prmDFlight->Flno, olFlnoT) != 0 && strcmp(prmDFlight->Flno, olFlno) != 0)		
		strcpy(prmDFlight->Flno, olFlno);


	strcpy(prmDFlight->Alc3,omDAlc3);
	strcpy(prmDFlight->Alc2,omDAlc2);
	strcpy(prmDFlight->Fltn,m_DFltn);
	strcpy(prmDFlight->Flti,m_DFlti);
	strcpy(prmDFlight->Csgn,m_DCsgn);
	strcpy(prmDFlight->Flns,m_DFlns);
	strcpy(prmDFlight->Ttyp,m_DTtyp);
//	strcpy(prmDFlight->Htyp,m_DHtyp);
	strcpy(prmDFlight->Styp,m_DStyp);
	strcpy(prmDFlight->Stev,m_DStev);
	strcpy(prmDFlight->Des3,pcmDDes3);
	strcpy(prmDFlight->Org3,pcgHome);
	strcpy(prmDFlight->Des4,pcmDDes4);
	strcpy(prmDFlight->Org4,pcgHome4);
	strcpy(prmDFlight->Divr,m_DDivr);


	prmDFlight->Stod = HourStringToDate( m_DStod, prmDFlight->Stod);
	prmDFlight->Stoa = HourStringToDate( m_DStoa, prmDFlight->Stod);
	prmDFlight->Etod = HourStringToDate(m_DEtod, prmDFlight->Stod);
	prmDFlight->Iskd = HourStringToDate(m_DIskd, prmDFlight->Stod);
	prmDFlight->Nxti = HourStringToDate(m_DNxti, prmDFlight->Stod);
	prmDFlight->Etdc = HourStringToDate(m_DEtdc, prmDFlight->Stod);

//	GetDayOfWeek(prmDFlight->Stod, prmDFlight->Dood);



	if(m_CE_DOfbl.IsChanged())	
	{
		if(!m_DOfbl.IsEmpty())
		{
			prmDFlight->Ofbu = HourStringToDate(m_DOfbl, prmDFlight->Stod);
			prmDFlightSave->Ofbu = TIMENULL; // Because this field must be saved in the DB!
		}
		else
		{
			if( strcmp(prmDFlight->Stof, "D") == 0)
			{
				prmDFlight->Ofbd = TIMENULL;	

			}
 			else if( strcmp(prmDFlight->Stof, "S") == 0)
			{
				prmDFlight->Ofbs = TIMENULL;	

			}else
			//if( strcmp(prmDFlight->Stof, "U") == 0)
			{
				prmDFlight->Ofbu = TIMENULL;	

			}
		}
	}

	
	
	if(m_CE_DAirb.IsChanged())	
	{
		if(!m_DAirb.IsEmpty())
		{
			prmDFlight->Airu = HourStringToDate(m_DAirb, prmDFlight->Stod);
			prmDFlightSave->Airu = TIMENULL; // Because this field must be saved in the DB!
		}
		else
		{
			if( strcmp(prmDFlight->Stab, "D") == 0)
			{
				prmDFlight->Aird = TIMENULL;	

			}else
			if( strcmp(prmDFlight->Stab, "A") == 0)
			{
				prmDFlight->Aira = TIMENULL;	

			}else
			//if( strcmp(prmDFlight->Stab, "U") == 0)
			{
				prmDFlight->Airu = TIMENULL;	

			}
		}
	}
	
	
	
	if(m_CE_DSlot.IsChanged())	
	{
		if(!m_DSlot.IsEmpty())
		{
			prmDFlight->Slou = HourStringToDate(m_DSlot, prmDFlight->Stod);
			prmDFlightSave->Slou = TIMENULL; // Because this field must be saved in the DB!
		}
		else
		{
			if( strcmp(prmDFlight->Stsl, "C") == 0)
			{
				prmDFlight->Ctot = TIMENULL;	

			}else
			//if( strcmp(prmDFlight->Stsl, "U") == 0)
			{
				prmDFlight->Slou = TIMENULL;	

			}
		}
	}
	
	
	
	if(m_CE_DEtdi.IsChanged())	
	{
		if(!m_DEtdi.IsEmpty())
		{
			prmDFlight->Etdu = HourStringToDate(m_DEtdi, prmDFlight->Stod);
			prmDFlightSave->Etdu = TIMENULL; // Because this field must be saved in the DB!
		}
		else
		{
			if( strcmp(prmDFlight->Sted, "A") == 0)
			{
				prmDFlight->Etda = TIMENULL;	

			}else
			if( strcmp(prmDFlight->Sted, "E") == 0)
			{
				prmDFlight->Etde = TIMENULL;	

			}else
			if( strcmp(prmDFlight->Sted, "C") == 0)
			{
				prmDFlight->Etdc = TIMENULL;	

			}else
			//if( strcmp(prmDFlight->Sted, "U") == 0)
			{
				prmDFlight->Etdu = TIMENULL;	

			}
		}
	}

	if(m_DDtd1.GetLength() > 0)
		FillLeftByChar(m_DDtd1, '0' , 4);

	if(m_DDtd1.GetLength() > 0)
		FillLeftByChar(m_DDtd2, '0' , 4);


	strcpy(prmDFlight->Dtd1, m_DDtd1);
	strcpy(prmDFlight->Dtd2, m_DDtd2);

	strcpy(prmDFlight->Dcd1, m_DDcd1);
	strcpy(prmDFlight->Dcd2, m_DDcd2);
	strcpy(prmDFlight->Rwyd, m_DRwyd);
	strcpy(prmDFlight->Ifrd, m_DIfrd);
	strcpy(prmDFlight->Pstd, m_DPstd);
	strcpy(prmDFlight->Gtd1, m_DGtd1);
	strcpy(prmDFlight->Gtd2, m_DGtd2);
	strcpy(prmDFlight->Tgd1, m_DTgd1);
	strcpy(prmDFlight->Tgd2, m_DTgd2);
	strcpy(prmDFlight->Wro1, m_DWro1);
	strcpy(prmDFlight->Twr1, m_DTwr1);
	prmDFlight->Gd1x = HourStringToDate( m_DGd1x, prmDFlight->Stod);
	prmDFlight->Gd2x = HourStringToDate( m_DGd2x, prmDFlight->Stod);
	prmDFlight->Pdba = HourStringToDate( m_DPdba, prmDFlight->Stod);
	prmDFlight->Pdea = HourStringToDate( m_DPdea, prmDFlight->Stod);
	prmDFlight->Gd1y = HourStringToDate( m_DGd1y, prmDFlight->Stod);
	prmDFlight->Gd2y = HourStringToDate( m_DGd2y, prmDFlight->Stod);
	prmDFlight->W1ba = HourStringToDate( m_DW1ba, prmDFlight->Stod);
	prmDFlight->W1ea = HourStringToDate( m_DW1ea, prmDFlight->Stod);
	prmDFlight->Bbfa = HourStringToDate( m_DBbfa, prmDFlight->Stod);
	strcpy(prmDFlight->Rem1,m_DRem1);
	strcpy(prmDFlight->Ming,m_Ming);
	strcpy(prmDFlight->Act3,m_Act3);
	strcpy(prmDFlight->Act5,m_Act5);

	if (bmChanged)
		strcpy(prmDFlight->Ftyp, "O");

	if(m_DCxx == TRUE)
		strcpy(prmDFlight->Ftyp, "X");

	if(m_DDiverted == TRUE)
		strcpy(prmDFlight->Ftyp, "D");

	if(m_DRerouted == TRUE)
		strcpy(prmDFlight->Ftyp, "R");

	if(m_CB_DRetFlight.GetCheck())
		strcpy(prmDFlight->Ftyp, "Z");

	if(m_CB_DRetTaxi.GetCheck())
		strcpy(prmDFlight->Ftyp, "B");




	strcpy(prmDFlight->Rem1,m_DRem1);
	if(m_DRem1.IsEmpty())
		strcpy(prmDFlight->Isre,"");
	else
		strcpy(prmDFlight->Isre,"+");



	if((ilCurSel = m_CL_DRemp.GetCurSel()) == CB_ERR)
		strcpy(prmDFlight->Remp, "");
	else
	{
		m_CL_DRemp.GetLBText(ilCurSel, olTmp);
		olTmp = olTmp.Left(4);
		olTmp.TrimRight();
		if(!olTmp.IsEmpty())
			strcpy(prmDFlight->Remp, olTmp);
		else
			strcpy(prmDFlight->Remp, "");
	}



	///////////////////////////////////////////////
	// weitere Flugnummern abflug
	//

	strcpy(prmDFlight->Jfno, "");
	ilJcnt = 0;
	olJfno = "";

	ilLines = pomDJfnoTable->GetLinesCount();
	for( i = 0; i < ilLines; i++)
	{
		pomDJfnoTable->GetTextFieldValue(i, 0, olAlc3);
		pomDJfnoTable->GetTextFieldValue(i, 1, olFltn);
		pomDJfnoTable->GetTextFieldValue(i, 2, olFlns);
		if(!olAlc3.IsEmpty() || !olFltn.IsEmpty())
		{
			olJfno +=  ogRotationDlgFlights.CreateFlno(olAlc3, olFltn, olFlns);
			ilJcnt++;
		}
	}
	olJfno.TrimRight();
	strcpy(prmDFlight->Jfno, olJfno);
	if (ilJcnt > 0)
		itoa(ilJcnt, prmDFlight->Jcnt, 10);


	///////////////////////////////////////////////
	// vias ankunft
	//
	// Via Abflug

	if (polRotationDViaDlg)
		polRotationDViaDlg->GetViaList(prmDFlight->Vial);

	
	CString olMax = "   ";
	CString olMin = "zzz";

	CString olCkic;
	CString olCkit;
	CString olDisp;
	CTime olCkba;
	CTime olCkea;
	CTime olCkbs;
	CTime olCkes;
	CCADATA *prlCca;
	CString olTmpStr;

	if(pomDCinsTable != NULL)
	{
		ilLines = pomDCinsTable->GetLinesCount();
		for( i = 0; i < ilLines; i++)
		{
			pomDCinsTable->GetTextFieldValue(i, 0, olCkic);
			pomDCinsTable->GetTextFieldValue(i, 1, olCkit);

			pomDCinsTable->GetTextFieldValue(i, 2, olTmp);
			olCkbs = HourStringToDate(olTmp, prmDFlight->Stod);
			if(bmLocalTime) ogBasicData.LocalToUtc(olCkbs);
			
			pomDCinsTable->GetTextFieldValue(i, 3, olTmp);
			olCkes = HourStringToDate(olTmp, prmDFlight->Stod); 
			if(bmLocalTime) ogBasicData.LocalToUtc(olCkes);

			pomDCinsTable->GetTextFieldValue(i, 4, olTmp);
			olCkba = HourStringToDate(olTmp, prmDFlight->Stod); 
			if(bmLocalTime) ogBasicData.LocalToUtc(olCkba);

			pomDCinsTable->GetTextFieldValue(i, 5, olTmp);
			olCkea = HourStringToDate(olTmp, prmDFlight->Stod); 
			if(bmLocalTime) ogBasicData.LocalToUtc(olCkea);

			pomDCinsTable->GetTextFieldValue(i, 6, olDisp);

			if(i >= omDCins.GetSize())
			{
				prlCca = new CCADATA;
				omDCins.Add(prlCca);

				// set cca stod
				prlCca->Stod = prmDFlight->Stod;
				if(bmLocalTime) ogBasicData.LocalToUtc(prlCca->Stod);

				// set cca flno
				olTmpStr = prmDFlight->Flno;
				olTmpStr.TrimRight();
				strcpy(prlCca->Flno, olTmpStr);
			
				// set cca act3
				strcpy(prlCca->Act3, prmDFlight->Act3);			
			}
			else
			{
				prlCca = &omDCins[i];
			}

			strcpy(prlCca->Ckic, olCkic);
			strcpy(prlCca->Ckit, olCkit);
			strcpy(prlCca->Disp, olDisp);
			prlCca->Ckba = olCkba;
 			prlCca->Ckea = olCkea;
			prlCca->Ckbs = olCkbs;
 			prlCca->Ckes = olCkes;

				// set cca flno
				olTmpStr = prmDFlight->Flno;
				olTmpStr.TrimRight();
				strcpy(prlCca->Flno, olTmpStr);


			olCkic.TrimRight();

			if((olCkic < olMin) && !olCkic.IsEmpty())
				olMin = olCkic;

			if((olCkic > olMax) && !olCkic.IsEmpty())
				olMax = olCkic;
		
		
		}

		olMax.TrimRight();
		olMin.TrimRight();

		// in Athens the flight set CKIF and CKIT automatically !!
		if (strcmp(pcgHome, "ATH") != 0)
		{
			if(olMax != "   " && !olMax.IsEmpty())
				strcpy(prmDFlight->Ckit, olMax);
			else
				strcpy(prmDFlight->Ckit, " ");
		
			if(olMin != "zzz" && !olMin.IsEmpty())
				strcpy(prmDFlight->Ckif, olMin);
			else
				strcpy(prmDFlight->Ckif, " ");
		}
	}

	/////////////////////////////////////////////////////////////////////////
	//// Get carousel times
	CString olBaz1;
	CString olBaz4;
	CTime opStart = TIMENULL;
	CTime opEnd	  = TIMENULL;
	for( i = 0; i < omDCins.GetSize(); i++)
	{
		prlCca = &omDCins[i];

		if(CString(prmDFlight->Flti) == "M")
		{
			if( !CString(prlCca->Ckic).IsEmpty())
			{
				if(ogBCD.GetField("CIC", "CNAM", prlCca->Ckic, "CICR") == "I")
					olBaz1 = ogBCD.GetField("CIC", "CNAM", prlCca->Ckic, "CBAZ");
				else
					olBaz4 = ogBCD.GetField("CIC", "CNAM", prlCca->Ckic, "CBAZ");
			}
		}
		else
		{
			if( !CString(prlCca->Ckic).IsEmpty())
			{
				olBaz1 = ogBCD.GetField("CIC", "CNAM", prlCca->Ckic, "CBAZ");
				if(!olBaz1.IsEmpty())
					break;
			}
		}

		if(prlCca->Ckba != TIMENULL)
		{
			opStart = prlCca->Ckba;
		}
		else
		{
			if(prlCca->Ckbs != TIMENULL)
			{
				opStart = prlCca->Ckbs;
			}
		}

		if(prlCca->Ckea != TIMENULL)
		{
			opEnd = prlCca->Ckea;
		}
		else
		{
			if(prlCca->Ckes != TIMENULL)
			{
				opEnd = prlCca->Ckes;
			}
		}
	}
	//// END (get carousel times)
	////////////////////////////////////////////////////////////////////////

	if(CString(prmDFlight->Baz1).IsEmpty() || 
		(bmAutoSetBaz1 && !m_CE_DBaz1.IsChanged() && !m_CE_DBao1.IsChanged() && !m_CE_DBac1.IsChanged()))
	{
		strcpy(prmDFlight->Baz1, olBaz1);
		if (!olBaz1.IsEmpty())
		{
			prmDFlight->Bao1 = opStart;
			prmDFlight->Bac1 = opEnd;
		}
		else
		{
			prmDFlight->Bao1 = TIMENULL;
			prmDFlight->Bac1 = TIMENULL;
		}
	}



	if(CString(prmDFlight->Baz4).IsEmpty() || 
		(bmAutoSetBaz4 && !m_CE_DBaz4.IsChanged() && !m_CE_DBao4.IsChanged() && !m_CE_DBac4.IsChanged()))
	{
		strcpy(prmDFlight->Baz4, olBaz4);
		if (!olBaz4.IsEmpty())
		{
			prmDFlight->Bao4 = opStart;
			prmDFlight->Bac4 = opEnd;
		}
		else
		{
			prmDFlight->Bao4 = TIMENULL;
			prmDFlight->Bac4 = TIMENULL;
		}
	}

}













static void FlightTableCf(void *popInstance, int ipDDXType,
    void *vpDataPointer, CString &ropInstanceName)
{
    RotationDlg *polDlg = (RotationDlg *)popInstance;
 
	if(ipDDXType == RG_DLG_ROTATION_CHANGE || ipDDXType == RG_DLG_FLIGHT_CHANGE || ipDDXType == R_DLG_FLIGHT_CHANGE)
		polDlg->CheckTowingButton();

    if(ipDDXType == R_DLG_FLIGHT_CHANGE)
        polDlg->ProcessFlightChange((ROTATIONDLGFLIGHTDATA *)vpDataPointer);

    if(ipDDXType == R_DLG_ROTATION_CHANGE)
//	    polDlg->ProcessRotationChange((ROTATIONDLGFLIGHTDATA *)vpDataPointer);
	    polDlg->ProcessRotationChange();

 	if (ipDDXType == CCA_ROTDLG_CHANGE || ipDDXType == CCA_ROTDLG_NEW || ipDDXType == CCA_ROTDLG_DELETE)
        polDlg->ProcessCCA((CCADATA *)vpDataPointer, ipDDXType);

}










///////////////////////////////////////////////////////////////////////////
// Cancel
//
void RotationDlg::OnCancel() 
{

	if(bmChanged)
	{
		if (!CheckPostFlight())
		{
			if(MessageBox(GetString(IDS_STRING942), GetString(ST_FRAGE), MB_ICONEXCLAMATION | MB_YESNO) == IDNO)
					return;
		}
	}

	bmChanged = false;
	m_CB_Ok.SetColors(::GetSysColor(COLOR_BTNFACE), ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));

	ogRotationDlgFlights.ClearAll();
	ClearAll();

	if (polRotationAViaDlg)
		polRotationAViaDlg->ShowWindow(SW_HIDE);
	if (polRotationDViaDlg)
		polRotationDViaDlg->ShowWindow(SW_HIDE);

	ShowWindow(SW_HIDE);
}

///////////////////////////////////////////////////////////////////////////
// OK
//
void RotationDlg::OnOK() 
{
	CString olGMessage;
	CString olAMessage;
	CString olDMessage;

	FillFlightData();

	// First check
	if(!CheckAll(olGMessage, olAMessage, olDMessage))
	{
		if(olGMessage.IsEmpty() && olAMessage.IsEmpty() && olDMessage.IsEmpty())
			return;

		if(!olGMessage.IsEmpty())
			olGMessage = GetString(IDS_STRING972) + olGMessage; 

		if(!olAMessage.IsEmpty())
			olAMessage = GetString(IDS_STRING973) + olAMessage; 
		
		if(!olDMessage.IsEmpty())
			olDMessage = GetString(IDS_STRING974) + olDMessage; 
		
//		MessageBox(olGMessage + olAMessage + olDMessage, GetString(ST_FEHLER), MB_ICONWARNING);
		CFPMSApp::MyTopmostMessageBox(this, olGMessage + olAMessage + olDMessage, GetString(ST_FEHLER), MB_ICONWARNING);
		return;
	}

	olGMessage.Empty();
	olAMessage.Empty();
	olDMessage.Empty();
	// Second check
	if(!CheckAll2(olGMessage, olAMessage, olDMessage))
	{
		if(olGMessage.IsEmpty() && olAMessage.IsEmpty() && olDMessage.IsEmpty())
			return;

		if(!olGMessage.IsEmpty())
			olGMessage = GetString(IDS_STRING972) + olGMessage; 

		if(!olAMessage.IsEmpty())
			olAMessage = GetString(IDS_STRING973) + olAMessage; 
		
		if(!olDMessage.IsEmpty())
			olDMessage = GetString(IDS_STRING974) + olDMessage; 

		CString olWarningText;
		olWarningText.Format("%s%s%s\n%s", olGMessage, olAMessage, olDMessage, GetString(IDS_STRING2037));
		if (CFPMSApp::MyTopmostMessageBox(this, olWarningText, GetString(IDS_WARNING), MB_ICONWARNING | MB_YESNO) != IDYES)
		{
			return;
		}
	}


	// additional belt check
	if (!AddBltCheck()) return;


	// Arrival Flight ID changed?
	if (strcmp(prmAFlight->Flti, prmAFlightSave->Flti) != 0)
	{
		m_AFlti.TrimRight();
		CString olMess;
 		if (m_AFlti.IsEmpty())
		{
			olMess.Format(GetString(IDS_STRING1994), prmAFlight->Flno);
		}
		else
		{
			olMess.Format(GetString(IDS_STRING1993), prmAFlight->Flno);
		}
		if (MessageBox(olMess, GetString(ST_FRAGE), MB_ICONQUESTION | MB_YESNO) != IDYES)
			return;
	}

	// Departure Flight ID changed?
	if (strcmp(prmDFlight->Flti, prmDFlightSave->Flti) != 0)
	{
		m_DFlti.TrimRight();
		CString olMess;
 		if (m_DFlti.IsEmpty())
		{
			olMess.Format(GetString(IDS_STRING1994), prmDFlight->Flno);
		}
		else
		{
			olMess.Format(GetString(IDS_STRING1993), prmDFlight->Flno);
		}
		if (MessageBox(olMess, GetString(ST_FRAGE), MB_ICONQUESTION | MB_YESNO) != IDYES)
			return;
	}

 
	m_CB_Ok.SetColors(RGB(0,255, 0), ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
	m_CB_Ok.UpdateWindow();

	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
	if(bmArrival)
	{
		if(	prmAFlight->Urno != 0)
		{
//			if(bmLocalTime) ogRotationDlgFlights.StructLocalToUtc((void*)prmAFlight, "");
	 		if(bmLocalTime) ConvertStructLocalTOUtc(prmAFlight, NULL);

			if(!ogRotationDlgFlights.UpdateFlight(prmAFlight, prmAFlightSave))
			{
				/*

				CString olText;

				olText += CString("\n\nData:     ") + ogRotationDlgFlights.omRetData;
				olText += CString("\n\nFields:   ") + ogRotationDlgFlights.omRetFields;
				olText += CString("\n\nObjekt:   ") + ogRotationDlgFlights.omRetObject;
				olText += CString("\n\nCmd:      ") + ogRotationDlgFlights.omRetCmd;
				olText += CString("\n\nSelection:") + ogRotationDlgFlights.omRetSelection;

				MessageBox(olText, GetString(ST_FEHLER));
				*/

			}

//			if(bmLocalTime) ogRotationDlgFlights.StructUtcToLocal((void*)prmAFlight, "");
	 		if(bmLocalTime) ConvertStructUtcTOLocal(prmAFlight, NULL);

		}
	}
	if(bmDeparture)
	{
		if(	(prmDFlight->Urno != 0))
		{
	 		if(bmLocalTime) ConvertStructLocalTOUtc(NULL, prmDFlight);
// 			if(bmLocalTime) ogRotationDlgFlights.StructLocalToUtc((void*)prmDFlight, "");

			if(ogRotationDlgFlights.UpdateFlight(prmDFlight, prmDFlightSave))
			{
				omCcaData.UpdateSpecial(omDCins, omDCinsSave, prmDFlight->Urno);

				//ACHTUNG MWO das ist nur eine Zwischenl�sung
				/*
				for (int ilLc = 0; ilLc < omDCins.GetSize(); ilLc++)
				{
					if(bmLocalTime)
					{	
						omCcaData.StructUtcToLocal(&omDCins[ilLc]);
					}
				}
				*/
				bmChanged = false;
				//ENDE ACHTUNG MWO das ist nur eine Zwischenl�sung
			
			}
//			if(bmLocalTime) ogRotationDlgFlights.StructUtcToLocal((void*)prmDFlight, "");
	 		if(bmLocalTime) ConvertStructUtcTOLocal(NULL, prmDFlight);
		}
	}
	bmChanged = false;
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
	m_CB_Ok.SetColors(::GetSysColor(COLOR_BTNFACE), ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
}




void RotationDlg::OnSplit() 
{
	if((prmAFlight->Urno != 0) && (prmDFlight->Urno != 0))
	{

		CedaFlightUtilsData omFlightUtilsData;
		omFlightUtilsData.SplitFlight(prmAFlight->Urno, prmAFlight->Rkey, false);
	}
}




void RotationDlg::OnJoin() 
{
	if((prmAFlight->Urno != 0) && (prmDFlight->Urno != 0))
		return;

	if((prmAFlight->Urno == 0) && (prmDFlight->Urno == 0))
		return;

	RotationJoinDlg *pomDlg;
	
	if(prmDFlight->Urno != 0)
		pomDlg = new RotationJoinDlg(this, prmDFlight, 'D');
	else
		pomDlg = new RotationJoinDlg(this, prmAFlight, 'A');

	CFPMSApp::UnsetTopmostWnds();
	pomDlg->DoModal();
	CFPMSApp::SetTopmostWnds();
	
	delete pomDlg;
}




LONG RotationDlg::OnEditKillfocus( UINT wParam, LPARAM lParam)
{
	CCSEDITNOTIFY *prlNotify = (CCSEDITNOTIFY*)lParam;
	CString olTmp;

	if(((UINT)m_CE_DAlc3.imID == wParam) && (!prlNotify->Text.IsEmpty()))
	{
		omDAlc2 = "";
		omDAlc3 = "";
		prlNotify->UserStatus = true;
		if(!(prlNotify->Status = ogBCD.GetField("ALT", "ALC2", "ALC3", prlNotify->Text, omDAlc2, omDAlc3 )))
		{
			omDAlc2 = "";
			omDAlc3 = "";
			RotationAltDlg olDlg(this, prlNotify->Text);
			if(olDlg.DoModal() == IDOK)
			{
				prlNotify->Status = true;
				omDAlc2 = olDlg.m_Alc3;
				omDAlc3 = olDlg.m_Alc2;
			}
			m_CE_DAlc3.SetInitText(omDAlc2, true);
		}
		if(omDAlc2.IsEmpty() && omDAlc3.IsEmpty())
		{
			m_CE_DDes3.SetTextLimit(0,3,true);
			m_CE_DDes3.SetBKColor(WHITE);
			m_CE_DDes4.SetTextLimit(0,4,true);
			m_CE_DDes4.SetBKColor(WHITE);
		}
		else
		{
			m_CE_DDes3.SetTextLimit(3,3,false);
			m_CE_DDes3.SetBKColor(YELLOW);
			m_CE_DDes4.SetTextLimit(4,4,false);
			m_CE_DDes4.SetBKColor(YELLOW);
		}
		return 0L;
	}

	if(((UINT)m_CE_AAlc3.imID == wParam) && (!prlNotify->Text.IsEmpty()))
	{
		omAAlc2 = "";
		omAAlc3 = "";
		prlNotify->UserStatus = true;
		if(!(prlNotify->Status = ogBCD.GetField("ALT", "ALC2", "ALC3", prlNotify->Text, omAAlc2, omAAlc3 )))
		{
			omAAlc2 = "";
			omAAlc3 = "";
			RotationAltDlg olDlg(this, prlNotify->Text);
			if(olDlg.DoModal() == IDOK)
			{
				prlNotify->Status = true;
				omAAlc2 = olDlg.m_Alc3;
				omAAlc3 = olDlg.m_Alc2;
			}
			m_CE_AAlc3.SetInitText(omAAlc2, true);
		}
		if(omAAlc2.IsEmpty() && omAAlc3.IsEmpty())
		{
			m_CE_ADes3.SetTextLimit(0,4,true);
			m_CE_ADes3.SetBKColor(WHITE);
		}
		else
		{
			m_CE_ADes3.SetTextLimit(4,4,false);
			m_CE_ADes3.SetBKColor(YELLOW);
		}
		return 0L;
	}
	
	if(((UINT)m_CE_AOrg3.imID == wParam) && (!prlNotify->Text.IsEmpty()))
	{
		strcpy(pcmAOrg3, "");
		strcpy(pcmAOrg4, "");
		CString olTmp3;
		CString olTmp4;
		
		if(prlNotify->Status = ogBCD.GetField("APT", "APC3", "APC4", prlNotify->Text, olTmp3, olTmp4 ))
		{
			strcpy(pcmAOrg3, olTmp3);
			strcpy(pcmAOrg4, olTmp4);
		}
		else
		{
			RotationAptDlg olDlg(this, prlNotify->Text);
			if(olDlg.DoModal() == IDOK)
			{
				prlNotify->Status = true;
				strcpy(pcmAOrg3, olDlg.m_Apc3);
				strcpy(pcmAOrg4, olDlg.m_Apc4);
			}
		}
		m_CE_AOrg3.SetInitText(pcmAOrg4, true);

		if(strcmp(pcmAOrg3 ,pcgHome) == 0)
		{
			m_CE_AStod.SetBKColor(YELLOW);
			m_CE_AStod.SetTypeToTime(true, true);
			m_CE_AStod.SetTextLimit(4,7,false);
			m_CS_Ankunft.SetWindowText(GetString(IDS_STRING340) + CString(" ")+ prmAFlight->Stoa.Format("%d.%m.%Y"));
		}
		else
		{
			m_CE_AStod.SetBKColor(WHITE);
			m_CE_AStod.SetTypeToTime(false, true);
			m_CE_AStod.SetTextLimit(0,7,true);
			m_CS_Ankunft.SetWindowText(GetString(IDS_STRING330) + CString(" ") + prmAFlight->Stoa.Format("%d.%m.%Y"));
		}
		return 0L;
	}

	if((((UINT)m_CE_DDes3.imID == wParam) || ((UINT)m_CE_DDes4.imID == wParam)) && (!prlNotify->Text.IsEmpty()))
	{
		strcpy(pcmDDes3, "");
		strcpy(pcmDDes4, "");
		CString olTmp3;
		CString olTmp4;
		
		if(prlNotify->Status = ogBCD.GetField("APT", "APC3", "APC4", prlNotify->Text, olTmp3, olTmp4 ))
		{
			strcpy(pcmDDes3, olTmp3);
			strcpy(pcmDDes4, olTmp4);
		}
		else
		{
			RotationAptDlg olDlg(this, prlNotify->Text);
			if(olDlg.DoModal() == IDOK)
			{
				prlNotify->Status = true;
				strcpy(pcmDDes3, olDlg.m_Apc3);
				strcpy(pcmDDes4, olDlg.m_Apc4);
			}
		}
		m_CE_DDes3.SetInitText(pcmDDes3, true);
		m_CE_DDes4.SetInitText(pcmDDes4, true);

		if(strcmp(pcmDDes3 ,pcgHome) == 0)
		{
			m_CE_DStoa.SetBKColor(YELLOW);
			m_CE_DStoa.SetTypeToTime(true, true);
			m_CE_DStoa.SetTextLimit(4,7,false);
			m_CS_Abflug.SetWindowText(GetString(IDS_STRING340) + CString(" ")+ prmDFlight->Stod.Format("%d.%m.%Y"));
		}
		else
		{
			m_CE_DStoa.SetBKColor(WHITE);
			m_CE_DStoa.SetTypeToTime(false, true);
			m_CE_DStoa.SetTextLimit(0,7,true);
			m_CS_Abflug.SetWindowText(GetString(IDS_STRING338) + CString(" ")+ prmDFlight->Stod.Format("%d.%m.%Y"));
		}
		return 0L;
	}
	



	if(((UINT)m_CE_Act3.imID == wParam) && (!prlNotify->Text.IsEmpty()))
	{
		prlNotify->UserStatus = true;
		if(prlNotify->Status = ogBCD.GetField("ACT", "ACT3", prlNotify->Text, "ACT5", olTmp ))
		{
			CString olAct5;
			m_CE_Act5.GetWindowText(olAct5);
			if(olAct5.IsEmpty())
				m_CE_Act5.SetInitText(olTmp);
			else
			{
				CString olUrno = ogBCD.GetFieldExt("ACT", "ACT3", "ACT5", prlNotify->Text, olAct5, "URNO");
				if(olUrno.IsEmpty())
					m_CE_Act5.SetInitText(olTmp);
			}

		}
		return 0L;
	}

	if((UINT)m_CE_Act5.imID == wParam)
	{
		prlNotify->UserStatus = true;
		if(prlNotify->Status = ogBCD.GetField("ACT", "ACT5", prlNotify->Text, "ACT3", olTmp ))
		{
			CString olAct3;
			m_CE_Act3.GetWindowText(olAct3);
			if(olAct3.IsEmpty())
				m_CE_Act3.SetInitText(olTmp);
			else
			{
				CString olUrno = ogBCD.GetFieldExt("ACT", "ACT3", "ACT5", olAct3, prlNotify->Text, "URNO");
				if(olUrno.IsEmpty())
					m_CE_Act3.SetInitText(olTmp);
			}
		}
		return 0L;
	}



	CString olRegn;
	m_CE_Regn.GetWindowText(olRegn);

	if( !olRegn.IsEmpty() && ( ((UINT)m_CE_Act5.imID == wParam) || ((UINT)m_CE_Act3.imID == wParam) ) && prlNotify->Status)
	{
		bool blRet = false;
		CString olAct3;
		CString olAct5;

		CString olWhere;
		olRegn.TrimLeft();
		if(!olRegn.IsEmpty())
		{
			olWhere.Format("WHERE REGN = '%s'", olRegn);
			ogBCD.Read( "ACR", olWhere);
		}
		
		
		if(ogBCD.GetFields("ACR", "REGN", olRegn, "ACT3", "ACT5", olAct3, olAct5))
		{
			if((prlNotify->Text != olAct3) && (prlNotify->Text != olAct5))
			{
				if(MessageBox(GetString(IDS_STRING945), GetString(ST_FRAGE), MB_YESNO) == IDYES)				
				{
					m_CE_Act3.GetWindowText(olAct3);
					m_CE_Act5.GetWindowText(olAct5);

					ogBCD.GetField("ACT", "ACT3","ACT5", prlNotify->Text, olAct3, olAct5);

					ogBCD.SetField("ACR", "REGN", olRegn, "ACT3", olAct3, false);
					ogBCD.SetField("ACR", "REGN", olRegn, "ACT5", olAct5, false);
					ogBCD.Save("ACR");
				}
				else
				{
					m_CE_Act3.SetInitText( olAct3, true);
					m_CE_Act5.SetInitText( olAct5, true);
				}
			}
		}
	}


		if(((UINT)m_CE_Regn.imID == wParam) && (!prlNotify->Text.IsEmpty()))
	{
		CString olAct3;
		CString olAct5;
		prlNotify->Status = true;

		
		CString olWhere;
		prlNotify->Text.TrimLeft();
		if(!prlNotify->Text.IsEmpty())
		{
			olWhere.Format("WHERE REGN = '%s'", prlNotify->Text);
			ogBCD.Read( "ACR", olWhere);
		}
		
		if(prlNotify->Status = ogBCD.GetFields("ACR", "REGN", prlNotify->Text, "ACT3", "ACT5", olAct3, olAct5))
		{
			m_CE_Act3.SetInitText(olAct3, true);
			m_CE_Act5.SetInitText(olAct5, true);
		}
		else
		{
			m_CE_Act3.GetWindowText(olAct3);
			m_CE_Act5.GetWindowText(olAct5);

			RotationRegnDlg olDlg(this, prlNotify->Text, olAct3, olAct5);
			if(olDlg.DoModal() == IDOK)
			{
				m_CE_Act5.SetInitText(olDlg.m_Act5, true);
				m_CE_Act3.SetInitText(olDlg.m_Act3, true);
				prlNotify->Status = true;
			}
			else
				m_CE_Regn.SetInitText("", true);

		}
		return 0L;
	}

	


	if( (UINT)m_CE_AGta1.imID == wParam)
	{
		if(prlNotify->Text.IsEmpty())
		{
			m_CE_ATga1.SetInitText("",true);
		}
		else
		{
			prlNotify->UserStatus = true;
			if(prlNotify->Status = ogBCD.GetField("GAT", "GNAM", prlNotify->Text, "TERM", olTmp ))
			{
				m_CE_ATga1.SetInitText(olTmp,true);
	
				if (prlNotify->IsChanged || prmAFlight->Ga1b == TIMENULL || prmAFlight->Ga1e == TIMENULL)
				{
					prmAFlight->ClearAGat(1);
				
					prmAFlight->Ga1b = prmAFlight->Tifa;

					CTimeSpan olDura;
					GetGatDefAllocDur(prlNotify->Text, olDura);
					prmAFlight->Ga1e = prmAFlight->Tifa + olDura;
				}
			}
		}
		return 0L;
	}


	if( (UINT)m_CE_AGta2.imID == wParam)
	{
		if(prlNotify->Text.IsEmpty())
		{
			m_CE_ATga2.SetInitText("",true);
		}
		else
		{
			prlNotify->UserStatus = true;
			if(prlNotify->Status = ogBCD.GetField("GAT", "GNAM", prlNotify->Text, "TERM", olTmp ))
			{
				m_CE_ATga2.SetInitText(olTmp,true);

				if (prlNotify->IsChanged || prmAFlight->Ga2b == TIMENULL || prmAFlight->Ga2e == TIMENULL)
				{
					prmAFlight->ClearAGat(2);

					prmAFlight->Ga2b = prmAFlight->Tifa;

					CTimeSpan olDura;
					GetGatDefAllocDur(prlNotify->Text, olDura);
					prmAFlight->Ga2e = prmAFlight->Tifa + olDura;		
				}
			}
		}
		return 0L;
	}


	if( (UINT)m_CE_DGtd1.imID == wParam)
	{
		if(prlNotify->Text.IsEmpty())
		{
			m_CE_DTgd1.SetInitText("",true);
		}
		else
		{
			prlNotify->UserStatus = true;
			if(prlNotify->Status = ogBCD.GetField("GAT", "GNAM", prlNotify->Text, "TERM", olTmp ))
			{
				m_CE_DTgd1.SetInitText(olTmp,true);
			
				if (prlNotify->IsChanged || prmDFlight->Gd1b == TIMENULL || prmDFlight->Gd1e == TIMENULL)
				{
					prmDFlight->ClearDGat(1);

					prmDFlight->Gd1e = prmDFlight->Tifd;

					CTimeSpan olDura;
					GetGatDefAllocDur(prlNotify->Text, olDura);
					prmDFlight->Gd1b = prmDFlight->Tifd - olDura;	
				}
			}
		}
		return 0L;
	}



	if( (UINT)m_CE_DGtd2.imID == wParam)
	{
		if(prlNotify->Text.IsEmpty())
		{
			m_CE_DTgd2.SetInitText("",true);
		}
		else
		{
			prlNotify->UserStatus = true;
			if(prlNotify->Status = ogBCD.GetField("GAT", "GNAM", prlNotify->Text, "TERM", olTmp ))
			{
				m_CE_DTgd2.SetInitText(olTmp,true);

				if (prlNotify->IsChanged || prmDFlight->Gd2b == TIMENULL || prmDFlight->Gd2e == TIMENULL)
				{
					prmDFlight->ClearDGat(2);
				
					prmDFlight->Gd2e = prmDFlight->Tifd;

					CTimeSpan olDura;
					GetGatDefAllocDur(prlNotify->Text, olDura);
					prmDFlight->Gd2b = prmDFlight->Tifd - olDura;	
				}
			}
		}
		return 0L;
	}




	if( (UINT)m_CE_ABlt1.imID == wParam)
	{
		if(prlNotify->Text.IsEmpty())
		{
			m_CE_ATmb1.SetInitText("",true);
		}
		else
		{
			prlNotify->UserStatus = true;
			if(prlNotify->Status = ogBCD.GetField("BLT", "BNAM", prlNotify->Text, "TERM", olTmp ))
			{
				m_CE_ATmb1.SetInitText(olTmp,true);

				if (prlNotify->IsChanged || prmAFlight->B1bs == TIMENULL || prmAFlight->B1es == TIMENULL)
				{
					prmAFlight->ClearBlt(1);
				
					prmAFlight->B1bs = prmAFlight->Tifa;

					CTimeSpan olDura;
					GetBltDefAllocDur(prlNotify->Text, olDura);
					prmAFlight->B1es = prmAFlight->Tifa + olDura;		
				}
			}
		}
		return 0L;
	}



	if( (UINT)m_CE_ABlt2.imID == wParam)
	{
		if(prlNotify->Text.IsEmpty())
		{
			m_CE_ATmb2.SetInitText("",true);
		}
		else
		{
			prlNotify->UserStatus = true;
			if(prlNotify->Status = ogBCD.GetField("BLT", "BNAM", prlNotify->Text, "TERM", olTmp ))
			{
				m_CE_ATmb2.SetInitText(olTmp,true);

			
				if (prlNotify->IsChanged || prmAFlight->B2bs == TIMENULL || prmAFlight->B2es == TIMENULL)
				{
					prmAFlight->ClearBlt(2);

					prmAFlight->B2bs = prmAFlight->Tifa;

					CTimeSpan olDura;
					GetBltDefAllocDur(prlNotify->Text, olDura);
					prmAFlight->B2es = prmAFlight->Tifa + olDura;			
				}
			}
		}
		return 0L;
	}



	if(((UINT)m_CE_AExt1.imID == wParam) || ((UINT)m_CE_AExt2.imID == wParam))
	{
		if(prlNotify->Text.IsEmpty())
		{
			if((UINT)m_CE_AExt1.imID == wParam)
				m_CE_ATet1.SetInitText("", true);
			if((UINT)m_CE_AExt2.imID == wParam)
				m_CE_ATet2.SetInitText("", true);
		}
		else
		{
			prlNotify->UserStatus = true;
			if(prlNotify->Status = ogBCD.GetField("EXT", "ENAM", prlNotify->Text, "TERM", olTmp ))
			{
				if((UINT)m_CE_AExt1.imID == wParam)
					m_CE_ATet1.SetInitText(olTmp, true);
				if((UINT)m_CE_AExt2.imID == wParam)
					m_CE_ATet2.SetInitText(olTmp, true);
			}
		}
		return 0L;
	}

	if((UINT)m_CE_DWro1.imID == wParam)
	{
		if(prlNotify->Text.IsEmpty())
		{
			m_CE_DTwr1.SetInitText("", true);
		}
		else
		{
			prlNotify->UserStatus = true;
			if(prlNotify->Status = ogBCD.GetField("WRO", "WNAM", prlNotify->Text, "TERM", olTmp ))
			{
				m_CE_DTwr1.SetInitText(olTmp, true);

				if (prlNotify->IsChanged || prmDFlight->W1bs == TIMENULL || prmDFlight->W1es == TIMENULL)
				{
					prmDFlight->ClearWro();

					prmDFlight->W1es = prmDFlight->Tifd;

					CTimeSpan olDura;
					GetWroDefAllocDur(prlNotify->Text, olDura);
					prmDFlight->W1bs = prmDFlight->Tifd - olDura;
				}
			}
		}
		return 0L;
	}


	//////////////////////////////////////////////////////////
	if(prlNotify->Text.IsEmpty())
		return 0L;


	if(((UINT)m_CE_ATtyp.imID == wParam) || ((UINT)m_CE_DTtyp.imID == wParam))
	{
		prlNotify->Status = ogBCD.GetField("NAT", "TTYP", prlNotify->Text, "URNO", olTmp );
		prlNotify->UserStatus = true;
		return 0L;
	}

/*	if(((UINT)m_CE_AHtyp.imID == wParam) || ((UINT)m_CE_DHtyp.imID == wParam))
	{
		prlNotify->Status = ogBCD.GetField("HTY", "HTYP", prlNotify->Text, "URNO", olTmp );
		prlNotify->UserStatus = true;
		return 0L;
	}
*/
	if(((UINT)m_CE_AStyp.imID == wParam) || ((UINT)m_CE_DStyp.imID == wParam))
	{
		prlNotify->Status = ogBCD.GetField("STY", "STYP", prlNotify->Text, "URNO", olTmp );
		prlNotify->UserStatus = true;
		return 0L;
	}

	if( (UINT)m_CE_APsta.imID == wParam)
	{
		prlNotify->Status = ogBCD.GetField("PST", "PNAM", prlNotify->Text, "URNO", olTmp );
		prlNotify->UserStatus = true;

		if(prlNotify->Status && (prlNotify->IsChanged || prmAFlight->Pabs == TIMENULL || prmAFlight->Paes == TIMENULL))
		{
			prmAFlight->ClearAPos();

			prmAFlight->Pabs = prmAFlight->Tifa;

			CTimeSpan olDura;
			GetPosDefAllocDur(prlNotify->Text, atoi(prmAFlight->Ming), olDura);			
			prmAFlight->Paes = prmAFlight->Tifa + olDura;
		}
	}

	if( (UINT)m_CE_DPstd.imID == wParam)
	{
		prlNotify->Status = ogBCD.GetField("PST", "PNAM", prlNotify->Text, "URNO", olTmp );
		prlNotify->UserStatus = true;

		if(prlNotify->Status && (prlNotify->IsChanged || prmDFlight->Pdbs == TIMENULL || prmDFlight->Pdes == TIMENULL))
		{
			prmDFlight->ClearDPos();

			prmDFlight->Pdes = prmDFlight->Tifd;

			CTimeSpan olDura;
			GetPosDefAllocDur(prlNotify->Text, atoi(prmDFlight->Ming), olDura);						
			prmDFlight->Pdbs = prmDFlight->Tifd - olDura;
		}
	}







	if((UINT)m_CE_APsta.imID == wParam)
	{
		CString olGpuStat = ogBCD.GetField("PST", "PNAM", prlNotify->Text, "GPUS");

		if(olGpuStat != "X")
			m_CB_AGpu.EnableWindow(FALSE);
		else
			m_CB_AGpu.EnableWindow(TRUE);
	}

	if((UINT)m_CE_DPstd.imID == wParam)
	{
		CString olGpuStat = ogBCD.GetField("PST", "PNAM", prlNotify->Text, "GPUS");

		if(olGpuStat != "X")
			m_CB_DGpu.EnableWindow(FALSE);
		else
			m_CB_DGpu.EnableWindow(TRUE);
	}




	if((((UINT)m_CE_ADcd1.imID == wParam) || ((UINT)m_CE_ADcd2.imID == wParam)) ||
	   (((UINT)m_CE_DDcd1.imID == wParam) || ((UINT)m_CE_DDcd2.imID == wParam)))
	{
		prlNotify->UserStatus = true;
		if(!(prlNotify->Status = ogBCD.GetField("DEN", "DECN", prlNotify->Text, "DECA", olTmp )))
		{
			prlNotify->Status = ogBCD.GetField("DEN", "DECA", prlNotify->Text, "DECN", olTmp );
		}
		return 0L;
	}



	if((UINT)m_CE_ARwya.imID == wParam)
	{
		CString olRnam;
		CString olRnum;
		if(prlNotify->Status = ogBCD.GetField("RWY", "RNUM", prlNotify->Text, "RNAM", olRnam ))
		{
			m_CE_ARwya.SetInitText(olRnam, true);
		}
		else
		{
			prlNotify->Status = ogBCD.GetField("RWY", "RNAM", prlNotify->Text, "RNUM", olRnum );
		}
		return 0L;
	}

	if((UINT)m_CE_DRwyd.imID == wParam)
	{
		CString olRnam;
		CString olRnum;

		if(prlNotify->Status = ogBCD.GetField("RWY", "RNUM", prlNotify->Text, "RNAM", olRnam ))
		{
			m_CE_DRwyd.SetInitText(olRnam, true);
		}
		else
		{
			prlNotify->Status = ogBCD.GetField("RWY", "RNAM", prlNotify->Text, "RNUM", olRnum );
		}
		return 0L;
	}
	
	return 0L;
}


LONG RotationDlg::OnEditChanged( UINT wParam, LPARAM lParam)
{
// begin : reject if opening times
	CString olTmp;

//	arr-gate1
	if((UINT)m_CE_AGta1.imID == wParam)
	{
		m_CE_AGta1.GetWindowText(olTmp);

		CString olGate = prmAFlightSave->Gta1;

		if (prmAFlightSave->Ga1x != TIMENULL && !olGate.IsEmpty())
		{
			if (olTmp != olGate)
			{
				m_CE_AGta1.SetInitText(olGate);
//				MessageBox( GetString(IDS_STRING_GATE_ISOPEN), GetString(ST_FEHLER), MB_OK | MB_ICONERROR);								
				CFPMSApp::MyTopmostMessageBox(this, GetString(IDS_STRING_GATE_ISOPEN), GetString(ST_FEHLER), MB_OK | MB_ICONERROR);
			}
		}
	}
//	arr-gate2
	if((UINT)m_CE_AGta2.imID == wParam)
	{
		m_CE_AGta2.GetWindowText(olTmp);

		CString olGate = prmAFlightSave->Gta2;

		if (prmAFlightSave->Ga2x != TIMENULL && !olGate.IsEmpty())
		{
			if (olTmp != olGate)
			{
				m_CE_AGta2.SetInitText(olGate);
//				MessageBox( GetString(IDS_STRING_GATE_ISOPEN), GetString(ST_FEHLER), MB_OK | MB_ICONERROR);								
				CFPMSApp::MyTopmostMessageBox(this, GetString(IDS_STRING_GATE_ISOPEN), GetString(ST_FEHLER), MB_OK | MB_ICONERROR);
			}
		}
	}
//	arr-belt1
	if((UINT)m_CE_ABlt1.imID == wParam)
	{
		m_CE_ABlt1.GetWindowText(olTmp);

		CString olAkt = prmAFlightSave->Blt1;

		if (prmAFlightSave->B1ba != TIMENULL && !olAkt.IsEmpty())
		{
			if (olTmp != olAkt)
			{
				m_CE_ABlt1.SetInitText(olAkt);
//				MessageBox( GetString(IDS_STRING_BELT_ISOPEN), GetString(ST_FEHLER), MB_OK | MB_ICONERROR);								
				CFPMSApp::MyTopmostMessageBox(this, GetString(IDS_STRING_BELT_ISOPEN), GetString(ST_FEHLER), MB_OK | MB_ICONERROR);
			}
		}
	}
//	arr-belt2
	if((UINT)m_CE_ABlt2.imID == wParam)
	{
		m_CE_ABlt2.GetWindowText(olTmp);

		CString olAkt = prmAFlightSave->Blt2;

		if (prmAFlightSave->B2ba != TIMENULL && !olAkt.IsEmpty())
		{
			if (olTmp != olAkt)
			{
				m_CE_ABlt2.SetInitText(olAkt);
//				MessageBox( GetString(IDS_STRING_BELT_ISOPEN), GetString(ST_FEHLER), MB_OK | MB_ICONERROR);								
				CFPMSApp::MyTopmostMessageBox(this, GetString(IDS_STRING_BELT_ISOPEN), GetString(ST_FEHLER), MB_OK | MB_ICONERROR);
			}
		}
	}
//	arr-position
	if((UINT)m_CE_APsta.imID == wParam)
	{
		m_CE_APsta.GetWindowText(olTmp);

		CString olAkt = prmAFlightSave->Psta;

		if (prmAFlightSave->Onbl != TIMENULL && !olAkt.IsEmpty())
		{
			if (olTmp != olAkt)
			{
				m_CE_APsta.SetInitText(olAkt);
//				MessageBox( GetString(IDS_STRING1546), GetString(ST_FEHLER), MB_OK | MB_ICONERROR);								
				CFPMSApp::MyTopmostMessageBox(this, GetString(IDS_STRING1546), GetString(ST_FEHLER), MB_OK | MB_ICONERROR);
			}
		}
	}
//	dep-gate1
	if((UINT)m_CE_DGtd1.imID == wParam)
	{
		m_CE_DGtd1.GetWindowText(olTmp);

		CString olGate = prmDFlightSave->Gtd1;

		if (prmDFlightSave->Gd1x != TIMENULL && !olGate.IsEmpty())
		{
			if (olTmp != olGate)
			{
				m_CE_DGtd1.SetInitText(olGate);
//				MessageBox( GetString(IDS_STRING_GATE_ISOPEN), GetString(ST_FEHLER), MB_OK | MB_ICONERROR);								
				CFPMSApp::MyTopmostMessageBox(this, GetString(IDS_STRING_GATE_ISOPEN), GetString(ST_FEHLER), MB_OK | MB_ICONERROR);
			}
		}
	}
//	dep-gate2
	if((UINT)m_CE_DGtd2.imID == wParam)
	{
		m_CE_DGtd2.GetWindowText(olTmp);

		CString olGate = prmDFlightSave->Gtd2;

		if (prmDFlightSave->Gd2x != TIMENULL && !olGate.IsEmpty())
		{
			if (olTmp != olGate)
			{
				m_CE_DGtd2.SetInitText(olGate);
//				MessageBox( GetString(IDS_STRING_GATE_ISOPEN), GetString(ST_FEHLER), MB_OK | MB_ICONERROR);								
				CFPMSApp::MyTopmostMessageBox(this, GetString(IDS_STRING_GATE_ISOPEN), GetString(ST_FEHLER), MB_OK | MB_ICONERROR);
			}
		}
	}
//	dep-position
	if((UINT)m_CE_DPstd.imID == wParam)
	{
		m_CE_DPstd.GetWindowText(olTmp);

		CString olAkt = prmDFlightSave->Pstd;

		if (prmDFlightSave->Ofbl != TIMENULL && !olAkt.IsEmpty())
		{
			if (olTmp != olAkt)
			{
				m_CE_DPstd.SetInitText(olAkt);
//				MessageBox( GetString(IDS_STRING1546), GetString(ST_FEHLER), MB_OK | MB_ICONERROR);								
				CFPMSApp::MyTopmostMessageBox(this, GetString(IDS_STRING1546), GetString(ST_FEHLER), MB_OK | MB_ICONERROR);
			}
		}
	}
//	dep-wro
	if((UINT)m_CE_DWro1.imID == wParam)
	{
		m_CE_DWro1.GetWindowText(olTmp);

		CString olAkt = prmDFlightSave->Wro1;

		if (prmDFlightSave->W1ba != TIMENULL && !olAkt.IsEmpty())
		{
			if (olTmp != olAkt)
			{
				m_CE_DWro1.SetInitText(olAkt);
//				MessageBox( GetString(IDS_STRING_LOUNGE_ISOPEN), GetString(ST_FEHLER), MB_OK | MB_ICONERROR);								
				CFPMSApp::MyTopmostMessageBox(this, GetString(IDS_STRING_LOUNGE_ISOPEN), GetString(ST_FEHLER), MB_OK | MB_ICONERROR);
			}
		}
	}
// end : reject if opening times


	if(!bmChanged)
	{
		m_CB_Ok.SetColors(RGB(255,0,0), ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
	}
	bmChanged = true;
	return 0L;
}



LONG RotationDlg::OnTableIPEditKillfocus( UINT wParam, LPARAM lParam)
{
	CCSIPEDITNOTIFY *prlNotify = (CCSIPEDITNOTIFY*)lParam;

	if(prlNotify->Text.IsEmpty())
		return 0L;
	CString olTmp;
	CString olTmp2;


	if(((prlNotify->SourceTable == pomAJfnoTable) && (prlNotify->Column == 0)) ||
	   ((prlNotify->SourceTable == pomDJfnoTable) && (prlNotify->Column == 0)))
	{
		if(prlNotify->Text.GetLength() == 3)
		{
			prlNotify->Status = ogBCD.GetField("ALT", "ALC3", prlNotify->Text, "ALC2", olTmp );
		}
		else
		{
			prlNotify->Status = ogBCD.GetField("ALT", "ALC2", prlNotify->Text, "ALC3", olTmp );
		}
		prlNotify->UserStatus = true;
		return 0L;
	}

	if((prlNotify->SourceTable == pomDCinsTable) && (prlNotify->Column == 0))
	{
		prlNotify->UserStatus = true;
		if(prlNotify->Status = ogBCD.GetField("CIC", "CNAM", prlNotify->Text, "TERM", olTmp ))
		{
			pomDCinsTable->SetIPValue(prlNotify->Line, 0, prlNotify->Text);
			pomDCinsTable->SetIPValue(prlNotify->Line, 1, olTmp);
		}
	}
	return 0L;
}

LONG RotationDlg::OnTableIPEdit( UINT wParam, LPARAM lParam)
{
	CCSTABLENOTIFY *prlNotify = (CCSTABLENOTIFY*)lParam;


//	reject a new checkin-counter if counter is already open
	if(prlNotify && (prlNotify->SourceTable == pomDCinsTable) && (prlNotify->Column == 0))
	{
		return CcaHasNoOpentime (prlNotify->Line);
	}


//rkr05042001
	// disable the cic if open (now you can�t hit with TAB)
	if(prlNotify && (prlNotify->SourceTable == pomDCinsTable))
	{
		for (int i = 0; i < omDCinsSave.GetSize(); i++)
		{
			CCADATA *prlCca = &omDCinsSave[i];

			if(prlCca && prlCca->Ckba != TIMENULL)
			{
				bool test = pomDCinsTable->SetColumnEditable(i, 0, false);
			}
			if(prlCca && prlCca->Ckba == TIMENULL)
			{
				bool test = pomDCinsTable->SetColumnEditable(i, 0, true);
			}
		}
	}
//rkr05042001


	return 0L;
}

LONG RotationDlg::CcaHasNoOpentime (int ipLineNo)
{
	if (ipLineNo >= 0 && ipLineNo < omDCinsSave.GetSize())
	{
		CCADATA *prlCca = &omDCinsSave[ipLineNo];

		if(prlCca && prlCca->Ckba != TIMENULL)
		{
//			MessageBox( GetString(IDS_STRING_CCA_ISOPEN), GetString(ST_FEHLER), MB_OK | MB_ICONERROR);								
			CFPMSApp::MyTopmostMessageBox(this, GetString(IDS_STRING_CCA_ISOPEN), GetString(ST_FEHLER), MB_OK | MB_ICONERROR);
			m_CB_Ok.SetFocus();
			return -1L;
		}
	}

	return 0L;
}

void RotationDlg::SaveToReg()
{
	BOOL ok = WriteDialogToReg((CWnd*) this, m_key);
}


BOOL RotationDlg::DestroyWindow() 
{
	SaveToReg();

	BOOL blRet =  CDialog::DestroyWindow();
	if(blRet)
	{
		delete this;
		pogRotationDlg = NULL;
	}
	return blRet;
}


void RotationDlg::CheckTowingButton(void)
{

//    CCSPtrArray<ROTATIONDLGFLIGHTDATA> olRotCopy(ogRotationDlgFlights.omData);
	ogRotationDlgFlights.omData.Sort(CompareRotationFlight);
//	olRotCopy.Sort(CompareRotationFlight);

	ROTATIONDLGFLIGHTDATA *prlFlight;

	int i = 0;

	if (prmAFlight->Urno > 0)
	{
		// at first search arrival flight
		for (i = 0; i < ogRotationDlgFlights.omData.GetSize(); i++)
		{
			prlFlight = &ogRotationDlgFlights.omData[i];
			
			if (prlFlight->Urno == prmAFlight->Urno)
			{
				break; // arr found !! 
			}
		}
	}

	// then count towings to dep
	int ilCount = 0;
	for (; i < ogRotationDlgFlights.omData.GetSize(); i++)
	{
		prlFlight = &ogRotationDlgFlights.omData[i];
			
		if (CString("GT").Find(prlFlight->Ftyp) >= 0)
			ilCount++;

		if (prmDFlight->Urno > 0 && prlFlight->Urno == prmDFlight->Urno)
			break;
	}
		

	CString olButtonText;
	COLORREF olButtonColor;
	if (ilCount > 0)
	{
		olButtonColor = ogColors[19];
		olButtonText.Format(GetString(IDS_STRING1936), ilCount);

	}
	else
	{
		olButtonText.Format(GetString(IDS_STRING1936), 0);
		olButtonColor = ::GetSysColor(COLOR_BTNFACE);
	}

	m_CB_Towing.SetWindowText(olButtonText);
	m_CB_Towing.SetColors(olButtonColor, ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
	m_CB_Towing.UpdateWindow();
	

	//MWO
	//olRotCopy.DeleteAll();
	//END MWO
	return; 
}




void RotationDlg::InitTables()
{
	int ili;
	CRect olRectBorder;
	m_CE_AJfnoBorder.GetWindowRect( olRectBorder );
	ScreenToClient(olRectBorder);
	olRectBorder.DeflateRect(2,2);

	pomAJfnoTable->SetHeaderSpacing(0);
	pomAJfnoTable->SetMiniTable();
	pomAJfnoTable->SetTableEditable(true);

	//rkr04042001
	pomAJfnoTable->SetIPEditModus(true);

    pomAJfnoTable->SetTableData(this, olRectBorder.left, olRectBorder.right, olRectBorder.top, olRectBorder.bottom);
	pomAJfnoTable->SetSelectMode(0);

	CCSPtrArray <TABLE_HEADER_COLUMN> omHeaderDataArray;

	pomAJfnoTable->SetShowSelection(false);
	pomAJfnoTable->ResetContent();
	
	TABLE_HEADER_COLUMN *prlHeader[9];

	prlHeader[0] = new TABLE_HEADER_COLUMN;
	prlHeader[0]->Alignment = COLALIGN_CENTER;
	prlHeader[0]->Length = 36; 
	prlHeader[0]->Font = &ogCourier_Regular_10;
	prlHeader[0]->Text = CString("");

	prlHeader[1] = new TABLE_HEADER_COLUMN;
	prlHeader[1]->Alignment = COLALIGN_CENTER;
	prlHeader[1]->Length = 46; 
	prlHeader[1]->Font = &ogCourier_Regular_10;
	prlHeader[1]->Text = CString("");

	prlHeader[2] = new TABLE_HEADER_COLUMN;
	prlHeader[2]->Alignment = COLALIGN_CENTER;
	prlHeader[2]->Length = 15; 
	prlHeader[2]->Font = &ogCourier_Regular_10;
	prlHeader[2]->Text = CString("");

	for(ili = 0; ili < 3; ili++)
	{
		omHeaderDataArray.Add(prlHeader[ili]);
	}
	pomAJfnoTable->SetHeaderFields(omHeaderDataArray);
	pomAJfnoTable->SetDefaultSeparator();
	pomAJfnoTable->SetTableEditable(true);
	omHeaderDataArray.DeleteAll();
	pomAJfnoTable->DisplayTable();



	m_CE_DJfnoBorder.GetWindowRect( olRectBorder );
	ScreenToClient(olRectBorder);
	olRectBorder.DeflateRect(2,2);

	pomDJfnoTable->SetHeaderSpacing(0);
	pomDJfnoTable->SetMiniTable();
	pomDJfnoTable->SetTableEditable(true);

	//rkr04042001
	pomDJfnoTable->SetIPEditModus(true);

	pomDJfnoTable->SetSelectMode(0);
    pomDJfnoTable->SetTableData(this, olRectBorder.left, olRectBorder.right, olRectBorder.top, olRectBorder.bottom);


	pomDJfnoTable->SetShowSelection(false);
	pomDJfnoTable->ResetContent();
	
	prlHeader[0] = new TABLE_HEADER_COLUMN;
	prlHeader[0]->Alignment = COLALIGN_CENTER;
	prlHeader[0]->Length = 36; 
	prlHeader[0]->Font = &ogCourier_Regular_10;
	prlHeader[0]->Text = CString("");

	prlHeader[1] = new TABLE_HEADER_COLUMN;
	prlHeader[1]->Alignment = COLALIGN_CENTER;
	prlHeader[1]->Length = 46; 
	prlHeader[1]->Font = &ogCourier_Regular_10;
	prlHeader[1]->Text = CString("");

	prlHeader[2] = new TABLE_HEADER_COLUMN;
	prlHeader[2]->Alignment = COLALIGN_CENTER;
	prlHeader[2]->Length = 15; 
	prlHeader[2]->Font = &ogCourier_Regular_10;
	prlHeader[2]->Text = CString("");

	for(ili = 0; ili < 3; ili++)
	{
		omHeaderDataArray.Add(prlHeader[ili]);
	}
	pomDJfnoTable->SetHeaderFields(omHeaderDataArray);
	pomDJfnoTable->SetDefaultSeparator();
	omHeaderDataArray.DeleteAll();
	pomDJfnoTable->DisplayTable();






	m_CE_DCinsBorder.GetWindowRect( olRectBorder );
	pomDCinsTable->ResetContent();
	pomDCinsTable->SetHeaderSpacing(0);
	pomDCinsTable->SetMiniTable();
	pomDCinsTable->SetTableEditable(true);

	//rkr04042001
	pomDCinsTable->SetIPEditModus(true);

	ScreenToClient(olRectBorder);
	olRectBorder.DeflateRect(2,2);
    pomDCinsTable->SetTableData(this, olRectBorder.left, olRectBorder.right, olRectBorder.top, olRectBorder.bottom);
	pomDCinsTable->SetSelectMode(0);


	pomDCinsTable->SetShowSelection(false);
	pomDCinsTable->ResetContent();
	

	prlHeader[0] = new TABLE_HEADER_COLUMN;
	prlHeader[0]->Alignment = COLALIGN_CENTER;
	prlHeader[0]->Length = 40; 
	prlHeader[0]->Font = &ogCourier_Regular_10;
	prlHeader[0]->Text = CString("");

	prlHeader[1] = new TABLE_HEADER_COLUMN;
	prlHeader[1]->Alignment = COLALIGN_CENTER;
	prlHeader[1]->Length = 12; 
	prlHeader[1]->Font = &ogCourier_Regular_10;
	prlHeader[1]->Text = CString("");

	prlHeader[2] = new TABLE_HEADER_COLUMN;
	prlHeader[2]->Alignment = COLALIGN_CENTER;
	prlHeader[2]->Length = 46; 
	prlHeader[2]->Font = &ogCourier_Regular_10;
	prlHeader[2]->Text = CString("");

	prlHeader[3] = new TABLE_HEADER_COLUMN;
	prlHeader[3]->Alignment = COLALIGN_CENTER;
	prlHeader[3]->Length = 46; 
	prlHeader[3]->Font = &ogCourier_Regular_10;
	prlHeader[3]->Text = CString("");


	prlHeader[4] = new TABLE_HEADER_COLUMN;
	prlHeader[4]->Alignment = COLALIGN_CENTER;
	prlHeader[4]->Length = 46; 
	prlHeader[4]->Font = &ogCourier_Regular_10;
	prlHeader[4]->Text = CString("");

	prlHeader[5] = new TABLE_HEADER_COLUMN;
	prlHeader[5]->Alignment = COLALIGN_CENTER;
	prlHeader[5]->Length = 46; 
	prlHeader[5]->Font = &ogCourier_Regular_10;
	prlHeader[5]->Text = CString("");

	prlHeader[6] = new TABLE_HEADER_COLUMN;
	prlHeader[6]->Alignment = COLALIGN_CENTER;
	prlHeader[6]->Length = 70; 
	prlHeader[6]->Font = &ogCourier_Regular_10;
	prlHeader[6]->Text = CString("");


	for(ili = 0; ili < 7; ili++)
	{
		omHeaderDataArray.Add(prlHeader[ili]);
	}
	pomDCinsTable->SetHeaderFields(omHeaderDataArray);
	pomDCinsTable->SetDefaultSeparator();
	omHeaderDataArray.DeleteAll();
	pomDCinsTable->DisplayTable();

}







/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////
// CHECKALL


bool RotationDlg::CheckAll2(CString &opGMess, CString &opAMess, CString &opDMess) 
{
	if((prmAFlight->Urno == 0) && (prmDFlight->Urno == 0))
	{
		return true;
	}


	if (prmAFlight->Urno != 0)
	{
		CTime ATifd = -1;
		CTime ATifa = -1;
		ArrivalTifd(ATifd);
		ArrivalTifa(ATifa);

		if (bmLocalTime && bgRealLocal)
		{
//			CedaAptLocalUtc::AptLocalToUtc (ATifa, "");
//			CedaAptLocalUtc::AptLocalToUtc (ATifd, CString(pcmAOrg4));
		}

		// check if Tifd of origin is smaller than Tifa of Home
		if((ATifa != TIMENULL) && (ATifd != TIMENULL))
		{
			if(ATifa < ATifd)
				opAMess += GetString(IDS_STRING2108) + CString("\n");//"Scheduled time of Arrival (home) before Departure time at origin.";
		}
	}


	if (prmDFlight->Urno != 0)
	{
		CTime DTifa = -1;
		CTime DTifd = -1;

		DepartureTifa(DTifa);
		DepartureTifd(DTifd);

		if (bmLocalTime && bgRealLocal)
		{
//			CedaAptLocalUtc::AptLocalToUtc (DTifd, "");
//			CedaAptLocalUtc::AptLocalToUtc (DTifa, CString(pcmDDes4));
		}

		// check if Tifd of home is smaller than Tifa of Destination
		if((DTifa != TIMENULL) && (DTifd != TIMENULL))
		{
			if(DTifd > DTifa)
				opDMess += GetString(IDS_STRING2109) + CString("\n");//"Scheduled time of Departure (home) after Arrival time at destination.";
		}
	}

//	check for tifa/tifd
	CTime ATifa = -1;
	CTime DTifd = -1;

	ArrivalTifa(ATifa);
	DepartureTifd(DTifd);

	if(bmArrival && bmDeparture)
	{
		if((ATifa != TIMENULL) && (DTifd != TIMENULL))
		{
			if(ATifa > DTifd)
				opGMess += GetString(IDS_STRING2181) + CString("\n");//"STD is before STA!\n";
		}
	}

	if(opGMess.IsEmpty() && opAMess.IsEmpty() && opDMess.IsEmpty())
		return true;
	else
		return false;

}



bool RotationDlg::CheckAll(CString &opGMess, CString &opAMess, CString &opDMess) 
{
	if((prmAFlight->Urno == 0) && (prmDFlight->Urno == 0))
	{
		return true;
	}


	//MWO
	m_CE_Regn.GetWindowText(m_Regn); 
	m_Regn.Remove('\n');
	m_Regn.TrimLeft();
	m_Regn.TrimRight();
	m_CE_Regn.SetWindowText(m_Regn);
	//END MWO

	if(m_AStoa.IsEmpty())
	{
		bmArrival = false;
		if (prmAFlight->Urno != 0)
			opAMess += GetString(IDS_STRING350) + CString("\n");//"Planm��ige Ankunftszeit!\n";
	}
	else
	{
		bmArrival = true;
	}

	if(m_DStod.IsEmpty())
	{
		bmDeparture = false;
		if (prmDFlight->Urno != 0)
			opDMess += GetString(IDS_STRING384) + CString("\n");//"Planm��ige Abflugszeit!\n";
	}
	else
	{
		bmDeparture = true;
	}


	if(!bmArrival && !bmDeparture)
	{
		return false;
	}


	bool blRet = true;
	CString olBuffer;

	CTime olAOnbl;
	CTime olAOfbl;
	CTime olAAirb;
	CTime olALand;

	olALand = HourStringToDate(m_ALand, prmAFlight->Stoa);;
	olAAirb = HourStringToDate(m_AAirb, prmAFlight->Stoa);
	olAOfbl = HourStringToDate(m_AOfbl, prmAFlight->Stoa);
	olAOnbl = HourStringToDate(m_AOnbl, prmAFlight->Stoa);

/*	if (bmLocalTime)
	{
		CedaAptLocalUtc::AptLocalToUtc (olALand, "");
		CedaAptLocalUtc::AptLocalToUtc (olAOnbl, "");
		CedaAptLocalUtc::AptLocalToUtc (olAAirb, CString(pcmAOrg4));
		CedaAptLocalUtc::AptLocalToUtc (olAOfbl, CString(pcmAOrg4));
	}
*/

	CTime olDOnbl;
	CTime olDOfbl; 
	CTime olDAirb;
	CTime olDLand;

	olDAirb = HourStringToDate(m_DAirb, prmDFlight->Stod);
	olDOfbl = HourStringToDate(m_DOfbl, prmDFlight->Stod);
	olDOnbl = HourStringToDate(prmDFlight->Onbl.Format("%H:%M"), prmDFlight->Stod);
	olDLand = HourStringToDate(prmDFlight->Land.Format("%H:%M"), prmDFlight->Stod);

/*	if (bmLocalTime)
	{
		CedaAptLocalUtc::AptLocalToUtc (olDAirb, "");
		CedaAptLocalUtc::AptLocalToUtc (olDOfbl, "");
		CedaAptLocalUtc::AptLocalToUtc (olDOnbl, CString(pcmDDes4));
		CedaAptLocalUtc::AptLocalToUtc (olDLand, CString(pcmDDes4));
	}
*/

	if(!m_CE_Regn.GetStatus() && !m_Regn.IsEmpty())
		opGMess += GetString(IDS_STRING341) + CString("\n");//"LFZ-Kennzeichen"; 

	CString olBuffer2;
	m_CE_Act3.GetWindowText(olBuffer);
	m_CE_Act5.GetWindowText(olBuffer2);
	if(!m_CE_Act3.GetStatus() || !m_CE_Act5.GetStatus() || olBuffer.IsEmpty() || olBuffer2.IsEmpty())
		opGMess += GetString(IDS_STRING342) + CString("\n");//"A/C Typ\n"; 
		
	if(!m_CE_Ming.GetStatus())
		opGMess += GetString(IDS_STRING343) + CString("\n");//"Min.G/T \n"; 

/* in checkall2()
//	check for tifa/tifd
	CTime ATifa = -1;
	CTime DTifd = -1;

	ArrivalTifa(ATifa);
	DepartureTifd(DTifd);

	if(bmArrival && bmDeparture)
	{
		if((ATifa != TIMENULL) && (DTifd != TIMENULL))
		{
			if(ATifa > DTifd)
				opGMess += GetString(IDS_STRING1982) + CString("\n");//"STD is before STA!\n";
		}
	}
*/
	if((olAOnbl != TIMENULL) && (olDOfbl != TIMENULL))
	{
		if(olAOnbl > olDOfbl) 
			opGMess += GetString(IDS_STRING345) + CString("\n");//"Offblock vor Onblock!\n";

	}


	if((olALand != TIMENULL) && (olDAirb != TIMENULL))
	{
		if(olALand > olDAirb) 
			opGMess += GetString(IDS_STRING346) + CString("\n");//"Start ist vor Landung!\n";
	}




	if(bmArrival)
	{

		CString olFlno(prmAFlight->Flno); 

		if(!(m_AAlc3.IsEmpty() && m_AFltn.IsEmpty() && m_AFlns.IsEmpty()) && olFlno.IsEmpty())
			opAMess += GetString(IDS_STRING347) + CString("\n");//"Flugnummer\n"; 

		if (m_CE_AFlti.IsChanged() && 
			(!m_CE_AFlti.GetStatus() || (m_AFlti != "I" && m_AFlti != "D" && !m_AFlti.IsEmpty())))
			opAMess += GetString(IDS_STRING1992) + CString("\n");//"Flight ID\n";

		// weitere Flugnummern

		bool bl0;
		bool bl1;
		bool bl2;
		CString  olAlc3;
		CString  olFltn;
		CString  olFlns;

		int ilLines = pomAJfnoTable->GetLinesCount();
		for(int i = 0; i < ilLines; i++)
		{
			pomDJfnoTable->GetTextFieldValue(i, 0, olAlc3);
			pomDJfnoTable->GetTextFieldValue(i, 1, olFltn);
			pomDJfnoTable->GetTextFieldValue(i, 2, olFlns);
			if(!olAlc3.IsEmpty() && !olFltn.IsEmpty() && !olFlns.IsEmpty())
			{
				bl0 = pomAJfnoTable->GetCellStatus(i, 0); 
				bl1 = pomAJfnoTable->GetCellStatus(i, 1); 
				bl2 = pomAJfnoTable->GetCellStatus(i, 2); 
				if((!bl0) || (!bl1) || (!bl2))
				{
					olBuffer.Format(GetString(IDS_STRING348),i);
					opAMess += olBuffer; 
				}
			}
		}



		if(!m_CE_AOrg3.GetStatus())
			opAMess += GetString(IDS_STRING349) + CString("\n");//"Ausgangsflughafencode!\n"; 

		if(!m_CE_AStoa.GetStatus())
			opAMess += GetString(IDS_STRING384) + CString("\n");//"Planm��ige Ankunftszeit!\n";
		
		if(!m_CE_AStod.GetStatus())
			opAMess += GetString(IDS_STRING351) + CString("\n");//"Planm��ige Abflugszeit!\n";
		
		if(!m_CE_AEtai.GetStatus())
			opAMess += GetString(IDS_STRING352) + CString("\n");//"Ertwartete Ankunftszeit!\n";

		if(!m_CE_AEtdi.GetStatus())
			opAMess += GetString(IDS_STRING385) + CString("\n");//"Ertwartete Abflugsszeit!\n";

		if((olAOnbl != TIMENULL) && (olALand != TIMENULL))
		{
			if(olAOnbl < olALand) 
				opAMess += GetString(IDS_STRING353) + CString("\n");//"Onblock vor Landung!\n";

		}

		if((olAOnbl != TIMENULL) && (olALand == TIMENULL))
		{
				opAMess += GetString(IDS_STRING354) + CString("\n");//"Onblock ohne Landung!\n";

		}
		

		if((olAOfbl != TIMENULL) && (olAAirb != TIMENULL))
		{
			if(olAOfbl > olAAirb) 
				opAMess += GetString(IDS_STRING386) + CString("\n");//"Ofblock nach Start!\n";
		}


		if((olAOfbl == TIMENULL) && (olAAirb != TIMENULL))
		{
			opAMess += GetString(IDS_STRING387) + CString("\n");//"kein Ofblock vor Start!\n";
		}



		// Vias

		if (polRotationAViaDlg)
			opAMess += polRotationAViaDlg->GetStatus();


		if(!m_CE_ATtyp.GetStatus())
			opAMess += GetString(IDS_STRING360) + CString("\n");//"Verkehrsart\n";


		if(!m_CE_APsta.GetStatus())
			opAMess += GetString(IDS_STRING361) + CString("\n");//"Postionsbezeichnung\n";

		//if(!m_CE_APaba.GetStatus())
		//	opAMess += GetString(IDS_STRING362) + CString("\n");//"Beginn der Postionsbelegung\n";

		//if(!m_CE_APaea.GetStatus())
		//	opAMess += GetString(IDS_STRING363) + CString("\n");//"Ende der Postionsbelegung\n";

		if((prmAFlight->Paba != TIMENULL) && (prmAFlight->Paea != TIMENULL))
		{
			if(prmAFlight->Paea <= prmAFlight->Paba)
				opAMess += GetString(IDS_STRING364) + CString("\n");//"Ende der Postionsbelegung ist vor Beginn\n";
		}


		if(!m_CE_AGta1.GetStatus())
			opAMess += GetString(IDS_STRING365) + CString("\n");//"Gatebezeichnung 1\n";

		if(!m_CE_AGa1x.GetStatus())
			opAMess += GetString(IDS_STRING366) + CString("\n");//"Beginn der Gatebelegung 1\n";

		if(!m_CE_AGa1y.GetStatus())
			opAMess += GetString(IDS_STRING367) + CString("\n");//"Ende der Gatebelegung 1\n";

		if((prmAFlight->Ga1x != TIMENULL) && (prmAFlight->Ga1y != TIMENULL))
		{
			if(prmAFlight->Ga1x >= prmAFlight->Ga1y)
				opAMess += GetString(IDS_STRING368) + CString("\n");//"Ende der Gatebelegung 1 ist vor Beginn\n";
		}

		if(!m_CE_AGta2.GetStatus())
			opAMess += GetString(IDS_STRING369) + CString("\n");//"Gatebezeichnung 2\n";

		if(!m_CE_AGa2x.GetStatus())
			opAMess += GetString(IDS_STRING370) + CString("\n");//"Beginn der Gatebelegung 2\n";

		if(!m_CE_AGa2y.GetStatus())
			opAMess += GetString(IDS_STRING371) + CString("\n");//"Ende der Gatebelegung 2\n";

		if((prmAFlight->Ga2x != TIMENULL) && (prmAFlight->Ga1y != TIMENULL))
		{
			if(prmAFlight->Ga2x >= prmAFlight->Ga2y)
				opAMess += GetString(IDS_STRING372) + CString("\n");//"Ende der Gatebelegung 2 ist vor Beginn\n";
		}

		if(!m_CE_ABlt1.GetStatus())
			opAMess += GetString(IDS_STRING373) + CString("\n");//"Bezeichnung des Gep�ckbandes 1\n";

		if(!m_CE_AB1ba.GetStatus())
			opAMess += GetString(IDS_STRING374) + CString("\n");//"Beginn der Belegung des Gep�ckbandes 1\n";

		if(!m_CE_AB1ea.GetStatus())
			opAMess += GetString(IDS_STRING375) + CString("\n");//"Ende der Belegung des Gep�ckbandes 1\n";

		if((prmAFlight->B1ba != TIMENULL) && (prmAFlight->B1ea != TIMENULL))
		{
			if(prmAFlight->B1ba >= prmAFlight->B1ea)
				opAMess += GetString(IDS_STRING376) + CString("\n");//"Ende der Belegung des Gep�ckbandes 1 ist vor Beginn\n";
		}

		if(!m_CE_ABlt2.GetStatus())
			opAMess += GetString(IDS_STRING377) + CString("\n");//"Bezeichnung des Gep�ckbandes 2\n";

		if(!m_CE_AB2ba.GetStatus())
			opAMess += GetString(IDS_STRING378) + CString("\n");//"Beginn der Belegung des Gep�ckbandes 2\n";

		if(!m_CE_AB2ea.GetStatus())
			opAMess += GetString(IDS_STRING379) + CString("\n");//"Ende der Belegung des Gep�ckbandes 2\n";

		if((prmAFlight->B2ba != TIMENULL) && (prmAFlight->B2ea != TIMENULL))
		{
			if(prmAFlight->B2ba >= prmAFlight->B2ea)
				opAMess += GetString(IDS_STRING380) + CString("\n");//"Ende der Belegung des Gep�ckbandes 2 ist vor Beginn\n";
		}


		if(!m_CE_AExt1.GetStatus())
			opAMess += GetString(IDS_STRING381) + CString("\n");//"Bezeichnung des Ausganges 1\n";

		if(!m_CE_AExt2.GetStatus())
			opAMess += GetString(IDS_STRING382) + CString("\n");//"Bezeichnung des Ausganges 2\n";
	}


	//////////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////////
	if(bmDeparture)
	{

		CString olFlno(prmDFlight->Flno); 

		if(!(m_DAlc3.IsEmpty() && m_DFltn.IsEmpty() && m_DFlns.IsEmpty()) && olFlno.IsEmpty())
			opDMess += GetString(IDS_STRING347) + CString("\n");//"Flugnummer\n"; 

		if (m_CE_DFlti.IsChanged() &&
			(!m_CE_DFlti.GetStatus() || (m_DFlti != "I" && m_DFlti !="D" && !m_DFlti.IsEmpty())))
 			opDMess += GetString(IDS_STRING1992) + CString("\n");//"Flight ID\n";

		
		// weitere Flugnummern
		CString  olAlc3;
		CString  olFltn;
		CString  olFlns;

		int ilLines = pomDJfnoTable->GetLinesCount();
		for(int i = 0; i < ilLines; i++)
		{
			pomDJfnoTable->GetTextFieldValue(i, 0, olAlc3);
			pomDJfnoTable->GetTextFieldValue(i, 1, olFltn);
			pomDJfnoTable->GetTextFieldValue(i, 2, olFlns);
			if(!olAlc3.IsEmpty() && !olFltn.IsEmpty() && !olFlns.IsEmpty())
			{
				if((!pomDJfnoTable->GetCellStatus(i, 0)) || (!pomDJfnoTable->GetCellStatus(i, 1)) || (!pomDJfnoTable->GetCellStatus(i, 2)))
				{
					olBuffer.Format(GetString(IDS_STRING348), i);//"Weitere Flugnummer in Zeile %d\n"
					opDMess += olBuffer; 
				}
			}
		}



		if(!m_CE_DDes3.GetStatus())
			opDMess += GetString(IDS_STRING383) + CString("\n");//"Bestimmungsflughafencode!\n"; 

		if(!m_CE_DStod.GetStatus())
			opDMess += GetString(IDS_STRING384) + CString("\n");//"Planm��ige Abflugszeit!\n";
		
		if(!m_CE_DStoa.GetStatus())
			opDMess += GetString(IDS_STRING350) + CString("\n");//"Planm��ige Ankunftszeit!\n";

		if(!m_CE_DEtdi.GetStatus())
			opDMess += GetString(IDS_STRING385) + CString("\n");//"Ertwartete Abflugsszeit!\n";


		if((olDOfbl != TIMENULL) && (olDAirb != TIMENULL))
		{
			if(olDOfbl > olDAirb) 
				opDMess += GetString(IDS_STRING386) + CString("\n");//"Ofblock nach Start!\n";

		}


		if((olDOfbl == TIMENULL) && (olDAirb != TIMENULL))
		{
			opAMess += GetString(IDS_STRING387) + CString("\n");//"kein Ofblock vor Start!\n";
		}


		
		// Vias
		if (polRotationDViaDlg)
			opDMess += polRotationDViaDlg->GetStatus();



		if(!m_CE_DTtyp.GetStatus())
			opDMess += GetString(IDS_STRING360) + CString("\n");//"Verkehrsart\n";


		if(!m_CE_DPstd.GetStatus())
			opDMess += GetString(IDS_STRING361) + CString("\n");//"Postionsbezeichnung\n";

		//if(!m_CE_DPdba.GetStatus())
		//	opDMess += GetString(IDS_STRING362) + CString("\n");//"Beginn der Postionsbelegung\n";

		//if(!m_CE_DPdea.GetStatus())
		//	opDMess += GetString(IDS_STRING363) + CString("\n");//"Ende der Postionsbelegung\n";

		if((prmDFlight->Pdea != TIMENULL) && (prmDFlight->Pdba != TIMENULL))
		{
			if(prmDFlight->Pdba >= prmDFlight->Pdea)
				opDMess += GetString(IDS_STRING364) + CString("\n");//"Ende der Postionsbelegung ist vor Beginn\n";
		}


		if(!m_CE_DGtd1.GetStatus())
			opDMess += GetString(IDS_STRING365) + CString("\n");//"Gatebezeichnung 1\n";

		if(!m_CE_DGd1x.GetStatus())
			opDMess += GetString(IDS_STRING366) + CString("\n");//"Beginn der Gatebelegung 1\n";

		if(!m_CE_DGd1y.GetStatus())
			opDMess += GetString(IDS_STRING367) + CString("\n");//"Ende der Gatebelegung 1\n";

		if((prmDFlight->Gd1x != TIMENULL) && (prmDFlight->Gd1y != TIMENULL))
		{
			if(prmDFlight->Gd1y <= prmDFlight->Gd1x)
				opDMess += GetString(IDS_STRING368) + CString("\n");//"Ende der Gatebelegung 1 ist vor Beginn\n";
		}

		if(!m_CE_DGtd2.GetStatus())
			opDMess += GetString(IDS_STRING369) + CString("\n");//"Gatebezeichnung 2\n";

		if(!m_CE_DGd2x.GetStatus())
			opDMess += GetString(IDS_STRING370) + CString("\n");//"Beginn der Gatebelegung 2\n";

		if(!m_CE_DGd2y.GetStatus())
			opDMess += GetString(IDS_STRING371) + CString("\n");//"Ende der Gatebelegung 2\n";

		if((prmDFlight->Gd2x != TIMENULL) && (prmDFlight->Gd1y != TIMENULL))
		{
			if(prmDFlight->Gd2x >= prmDFlight->Gd2y)
				opDMess += GetString(IDS_STRING372) + CString("\n");//"Ende der Gatebelegung 2 ist vor Beginn\n";
		}

		// check carousel times
		if(!m_CE_DBao1.GetStatus())
			opDMess += GetString(IDS_STRING1988) + CString("\n"); 
		if(!m_CE_DBac1.GetStatus())
			opDMess += GetString(IDS_STRING1989) + CString("\n"); 
		if(!m_CE_DBao4.GetStatus())
			opDMess += GetString(IDS_STRING1990) + CString("\n"); 
		if(!m_CE_DBac4.GetStatus())
			opDMess += GetString(IDS_STRING1991) + CString("\n"); 


		if(!m_CE_DWro1.GetStatus())
			opDMess += GetString(IDS_STRING388) + CString("\n");//"Bezeichnung des Warteraumes\n";

		if(!m_CE_DW1ba.GetStatus())
			opDMess += GetString(IDS_STRING389) + CString("\n");//"Beginn der Belegung des Warteraumes 1\n";

		if(!m_CE_DW1ea.GetStatus())
			opDMess += GetString(IDS_STRING390) + CString("\n");//"Ende der Belegung des Warteraumes \n";

		if((prmDFlight->W1ba != TIMENULL) && (prmDFlight->W1ea != TIMENULL))
		{
			if(prmDFlight->W1ea <= prmDFlight->W1ba)
				opDMess += GetString(IDS_STRING391) + CString("\n");//"Ende der Belegung des Warteraumes ist vor Beginn\n";
		}

		CString olTmp;
		CString olTmp2;

		CTime olCkbs;
		CTime olCkes;
		CTime olCkba;
		CTime olCkea;

		// Checkinschalter
		if(pomDCinsTable != NULL)
		{
			ilLines = pomDCinsTable->GetLinesCount();
			for( i = 0; i < ilLines; i++)
			{

				pomDCinsTable->GetTextFieldValue(i, 2, olTmp);
				pomDCinsTable->GetTextFieldValue(i, 3, olTmp2);
				olCkbs = HourStringToDate(olTmp, prmDFlight->Stod); 
				olCkes = HourStringToDate(olTmp2, prmDFlight->Stod); 
				pomDCinsTable->GetTextFieldValue(i, 4, olTmp);
				pomDCinsTable->GetTextFieldValue(i, 5, olTmp2);
				olCkba = HourStringToDate(olTmp, prmDFlight->Stod); 
				olCkea = HourStringToDate(olTmp2, prmDFlight->Stod); 
					
				if(!pomDCinsTable->GetCellStatus(i, 0))
				{
					olBuffer.Format(GetString(IDS_STRING392),i);//"Bezeichnung Check-In Schalter in Zeile %d\n"
					opDMess += olBuffer; 
				}
				if(!pomDCinsTable->GetCellStatus(i, 2))
				{
					olBuffer.Format(GetString(IDS_STRING393),i); // "Check-In Schalter �ffnungszeit von in Zeile %d\n"
					opDMess += olBuffer; 
				}
				if(!pomDCinsTable->GetCellStatus(i, 3))
				{
					olBuffer.Format(GetString(IDS_STRING394),i); // "Check-In Schalter �ffnungszeit bis in Zeile %d\n"
					opDMess += olBuffer; 
				}


				if(!pomDCinsTable->GetCellStatus(i, 4))
				{
					olBuffer.Format(GetString(IDS_STRING1803), i); // "Check-In Schalter �ffnungszeit von in Zeile %d\n"
					opDMess += olBuffer; 
				}
				if(!pomDCinsTable->GetCellStatus(i, 5))
				{
					olBuffer.Format(GetString(IDS_STRING1804), i); // "Check-In Schalter �ffnungszeit bis in Zeile %d\n"
					opDMess += olBuffer; 
				}



				if(((olCkbs != TIMENULL) && (olCkes != TIMENULL) && olCkbs > olCkes) ||
					((olCkba != TIMENULL) && (olCkea != TIMENULL) && olCkba > olCkea))
				{
					// Allocation of Check-In Counter in line %d: Start time is after end time.\n
					olBuffer.Format(GetString(IDS_STRING395),i);
					opDMess += olBuffer; 
				}
				if ((olCkbs == TIMENULL && olCkes != TIMENULL) ||
					(olCkba == TIMENULL && olCkea != TIMENULL))
				{
					// Allocation of Check-In Counter in line %d: End time without start time!\n
					olBuffer.Format(GetString(IDS_STRING2011), i);
					opDMess += olBuffer; 
				}


			}
		}

	}

	if(opGMess.IsEmpty() && opAMess.IsEmpty() && opDMess.IsEmpty())
		return true;
	else
		return false;
}


void RotationDlg::OnAAlc3LIST() 
{

	CString olText;
	m_CE_AAlc3.GetWindowText(olText);

	AwBasicDataDlg *polDlg = new AwBasicDataDlg(this, "ALT","ALC2,ALC3,ALFN", "ALFN+,ALC2+", olText);
	polDlg->SetSecState("ROTATIONDLG_CE_AAlc3");
	if(polDlg->DoModal() == IDOK)
	{
		omAAlc2 = polDlg->GetField("ALC2");	
		omAAlc3 = polDlg->GetField("ALC3");	
		if(omAAlc2.IsEmpty())
			m_CE_AAlc3.SetInitText(omAAlc3, true);	
		else
			m_CE_AAlc3.SetInitText(omAAlc2, true);	
	}
	delete polDlg;

	CString olTmp;
	m_CE_AAlc3.GetWindowText(olTmp);	
	if(olTmp.IsEmpty())
	{
		m_CE_AOrg3.SetTextLimit(0,4,true);
		m_CE_AOrg3.SetBKColor(WHITE);
	}
	else
	{
		m_CE_AOrg3.SetTextLimit(4,4,false);
		m_CE_AOrg3.SetBKColor(YELLOW);
	}
	m_CE_AAlc3.SetFocus();	
}

void RotationDlg::OnDalc3list() 
{
	CString olText;
	m_CE_DAlc3.GetWindowText(olText);
	
	AwBasicDataDlg *polDlg = new AwBasicDataDlg(this, "ALT","ALC2,ALC3,ALFN", "ALFN+,ALC2+", olText);
	polDlg->SetSecState("ROTATIONDLG_CE_DAlc3");
	if(polDlg->DoModal() == IDOK)
	{
		omDAlc2 = polDlg->GetField("ALC2");	
		omDAlc3 = polDlg->GetField("ALC3");	
		if(omDAlc2.IsEmpty())
			m_CE_DAlc3.SetInitText(omDAlc3, true);	
		else
			m_CE_DAlc3.SetInitText(omDAlc2, true);	
	}
	delete polDlg;

	CString olTmp;
	m_CE_DAlc3.GetWindowText(olTmp);	
	if(olTmp.IsEmpty())
	{
		m_CE_DDes3.SetTextLimit(0,3,true);
		m_CE_DDes3.SetBKColor(WHITE);
		m_CE_DDes4.SetTextLimit(0,4,true);
		m_CE_DDes4.SetBKColor(WHITE);
	}
	else
	{
		m_CE_DDes3.SetTextLimit(3,3,false);
		m_CE_DDes3.SetBKColor(YELLOW);
		m_CE_DDes4.SetTextLimit(4,4,false);
		m_CE_DDes4.SetBKColor(YELLOW);
	}
	m_CE_DAlc3.SetFocus();	


}







void RotationDlg::OnAblt1list() 
{
	CString olText;
	m_CE_ABlt1.GetWindowText(olText);

	AwBasicDataDlg *polDlg = new AwBasicDataDlg(this, "BLT","BNAM,TERM", "BNAM+,TERM+", olText);
	polDlg->SetSecState("ROTATIONDLG_CE_ABlt1");
	if(polDlg->DoModal() == IDOK)
	{
		m_CE_ABlt1.SetInitText(polDlg->GetField("BNAM"), true);	
		m_CE_ATmb1.SetInitText(polDlg->GetField("TERM"), true);	
	}
	delete polDlg;
}


void RotationDlg::OnAblt2list() 
{
	CString olText;
	m_CE_ABlt2.GetWindowText(olText);

	AwBasicDataDlg *polDlg = new AwBasicDataDlg(this, "BLT","BNAM,TERM", "BNAM+,TERM+", olText);
	polDlg->SetSecState("ROTATIONDLG_CE_ABlt2");
	if(polDlg->DoModal() == IDOK)
	{
		m_CE_ABlt2.SetInitText(polDlg->GetField("BNAM"), true);	
		m_CE_ATmb2.SetInitText(polDlg->GetField("TERM"), true);	
		m_CE_ABlt2.SetFocus();
	}
	delete polDlg;
	
}



void RotationDlg::OnAct35list() 
{
	CString olText;
	m_CE_Act3.GetWindowText(olText);

	
	AwBasicDataDlg *polDlg = new AwBasicDataDlg(this, "ACT","ACT3,ACT5,ACFN", "ACFN+,ACT3+", olText);
	polDlg->SetSecState("ROTATIONDLG_CE_Act3");
	if(polDlg->DoModal() == IDOK)
	{
		m_CE_Act3.SetInitText(polDlg->GetField("ACT3"), true);	
		m_CE_Act5.SetInitText(polDlg->GetField("ACT5"), true);	
		m_CE_Act3.SetFocus();	
	}
	delete polDlg;
}



void RotationDlg::OnAext1list() 
{
	CString olText;
	m_CE_AExt1.GetWindowText(olText);

	AwBasicDataDlg *polDlg = new AwBasicDataDlg(this, "EXT","ENAM,TERM", "ENAM+", olText);
	polDlg->SetSecState("ROTATIONDLG_CE_AExt1");
	if(polDlg->DoModal() == IDOK)
	{
		m_CE_AExt1.SetInitText(polDlg->GetField("ENAM"), true);	
		m_CE_ATet1.SetInitText(polDlg->GetField("TERM"), true);	
	}
	delete polDlg;
}


void RotationDlg::OnAext2list() 
{
	CString olText;
	m_CE_AExt2.GetWindowText(olText);

	AwBasicDataDlg *polDlg = new AwBasicDataDlg(this, "EXT","ENAM,TERM", "ENAM+", olText);
	polDlg->SetSecState("ROTATIONDLG_CE_AExt2");
	if(polDlg->DoModal() == IDOK)
	{
		m_CE_AExt2.SetInitText(polDlg->GetField("ENAM"), true);	
		m_CE_ATet2.SetInitText(polDlg->GetField("TERM"), true);	
	}
	delete polDlg;
}

void RotationDlg::OnAgta1list() 
{
	CString olText;
	m_CE_AGta1.GetWindowText(olText);

	AwBasicDataDlg *polDlg = new AwBasicDataDlg(this, "GAT","GNAM,TERM", "GNAM+", olText);
	polDlg->SetSecState("ROTATIONDLG_CE_AGta1");
	if(polDlg->DoModal() == IDOK)
	{
		m_CE_AGta1.SetInitText(polDlg->GetField("GNAM"), true);	
		m_CE_ATga1.SetInitText(polDlg->GetField("TERM"), true);	
		m_CE_AGta1.SetFocus();
	}
	delete polDlg;
}

void RotationDlg::OnAgta2list() 
{
	CString olText;
	m_CE_AGta2.GetWindowText(olText);

	AwBasicDataDlg *polDlg = new AwBasicDataDlg(this, "GAT","GNAM,TERM", "GNAM+", olText);
	polDlg->SetSecState("ROTATIONDLG_CE_AGta2");
	if(polDlg->DoModal() == IDOK)
	{
		m_CE_AGta2.SetInitText(polDlg->GetField("GNAM"), true);	
		m_CE_ATga2.SetInitText(polDlg->GetField("TERM"), true);	
		m_CE_AGta2.SetFocus();
	}
	delete polDlg;
}



void RotationDlg::OnAorg3list() 
{

	CString olText;
	m_CE_AOrg3.GetWindowText(olText);
	
	AwBasicDataDlg *polDlg = new AwBasicDataDlg(this, "APT","APC3,APC4,APFN", "APFN+", olText);
	polDlg->SetSecState("ROTATIONDLG_CE_AOrg3");
	if(polDlg->DoModal() == IDOK)
	{
		m_CE_AOrg3.SetInitText(polDlg->GetField("APC4"), true);	
		m_CE_AOrg3.SetFocus();	
	}
	delete polDlg;
}



void RotationDlg::OnApstalist() 
{
	CString olText;
	m_CE_APsta.GetWindowText(olText);

	AwBasicDataDlg *polDlg = new AwBasicDataDlg(this, "PST","PNAM", "PNAM+", olText);
	polDlg->SetSecState("ROTATIONDLG_CE_APsta");
	if(polDlg->DoModal() == IDOK)
	{
		m_CE_APsta.SetInitText(polDlg->GetField("PNAM"), true);	
		m_CE_APsta.SetFocus();	
		m_CE_APsta.SetFocus();
	}
	delete polDlg;
	
}



void RotationDlg::OnDdes3list() 
{
	CString olText;
	m_CE_DDes3.GetWindowText(olText);

	
	AwBasicDataDlg *polDlg = new AwBasicDataDlg(this, "APT","APC3,APC4,APFN", "APFN+", olText);
	polDlg->SetSecState("ROTATIONDLG_CE_DDes4");
	if(polDlg->DoModal() == IDOK)
	{
		m_CE_DDes3.SetInitText(polDlg->GetField("APC3"), true);	
		m_CE_DDes4.SetInitText(polDlg->GetField("APC4"), true);	
		m_CE_DDes4.SetFocus();	
	}
	delete polDlg;
}


void RotationDlg::OnDgta1list() 
{
	CString olText;
	m_CE_DGtd1.GetWindowText(olText);

	AwBasicDataDlg *polDlg = new AwBasicDataDlg(this, "GAT","GNAM,TERM", "GNAM+", olText);
	polDlg->SetSecState("ROTATIONDLG_CE_DGtd1");
	if(polDlg->DoModal() == IDOK)
	{
		m_CE_DGtd1.SetInitText(polDlg->GetField("GNAM"), true);	
		m_CE_DTgd1.SetInitText(polDlg->GetField("TERM"), true);	
		m_CE_DGtd1.SetFocus();
	}
	delete polDlg;

}

void RotationDlg::OnDgta2list() 
{
	CString olText;
	m_CE_DGtd2.GetWindowText(olText);

	AwBasicDataDlg *polDlg = new AwBasicDataDlg(this, "GAT","GNAM,TERM", "GNAM+", olText);
	polDlg->SetSecState("ROTATIONDLG_CE_DGtd2");
	if(polDlg->DoModal() == IDOK)
	{
		m_CE_DGtd2.SetInitText(polDlg->GetField("GNAM"), true);	
		m_CE_DTgd2.SetInitText(polDlg->GetField("TERM"), true);	
		m_CE_DGtd2.SetFocus();
	}
	delete polDlg;
}


void RotationDlg::OnDpstdlist() 
{
	CString olText;
	m_CE_DPstd.GetWindowText(olText);

	AwBasicDataDlg *polDlg = new AwBasicDataDlg(this, "PST","PNAM", "PNAM+", olText);
	polDlg->SetSecState("ROTATIONDLG_CE_DPstd");
	if(polDlg->DoModal() == IDOK)
	{
		m_CE_DPstd.SetInitText(polDlg->GetField("PNAM"), true);	
		m_CE_DPstd.SetFocus();
	}
	delete polDlg;
}


void RotationDlg::OnDwro1list() 
{
	CString olText;
	m_CE_DWro1.GetWindowText(olText);

	AwBasicDataDlg *polDlg = new AwBasicDataDlg(this, "WRO","WNAM,TERM", "WNAM+", olText);
	polDlg->SetSecState("ROTATIONDLG_CE_DWro1");
	if(polDlg->DoModal() == IDOK)
	{
		m_CE_DWro1.SetInitText(polDlg->GetField("WNAM"), true);	
		m_CE_DTwr1.SetInitText(polDlg->GetField("TERM"), true);	
		m_CE_DWro1.SetFocus();
	}
	delete polDlg;
}





void RotationDlg::OnDttyplist() 
{
	CString olText;
	m_CE_DTtyp.GetWindowText(olText);

	AwBasicDataDlg *polDlg = new AwBasicDataDlg(this, "NAT","TTYP,TNAM", "TNAM+", olText);
	polDlg->SetSecState("ROTATIONDLG_CE_DTtyp");
	if(polDlg->DoModal() == IDOK)
	{
		m_CE_DTtyp.SetInitText(polDlg->GetField("TTYP"), true);	
	}
	delete polDlg;
}

void RotationDlg::OnAttyplist() 
{
	CString olText;
	m_CE_ATtyp.GetWindowText(olText);

	AwBasicDataDlg *polDlg = new AwBasicDataDlg(this, "NAT","TTYP,TNAM", "TNAM+", olText);
	polDlg->SetSecState("ROTATIONDLG_CE_ATtyp");
	if(polDlg->DoModal() == IDOK)
	{
		m_CE_ATtyp.SetInitText(polDlg->GetField("TTYP"), true);	
	}
	delete polDlg;
}




void RotationDlg::OnDstyplist() 
{
	CString olText;
	m_CE_DStyp.GetWindowText(olText);

	AwBasicDataDlg *polDlg = new AwBasicDataDlg(this, "STY","STYP,SNAM", "SNAM+", olText);
	polDlg->SetSecState("ROTATIONDLG_CE_DStyp");
	if(polDlg->DoModal() == IDOK)
	{
		m_CE_DStyp.SetInitText(polDlg->GetField("STYP"), true);	
	}
	delete polDlg;
}



void RotationDlg::OnAstyplist() 
{
	CString olText;
	m_CE_AStyp.GetWindowText(olText);

	AwBasicDataDlg *polDlg = new AwBasicDataDlg(this, "STY","STYP,SNAM", "SNAM+", olText);
	polDlg->SetSecState("ROTATIONDLG_CE_AStyp");
	if(polDlg->DoModal() == IDOK)
	{
		m_CE_AStyp.SetInitText(polDlg->GetField("STYP"), true);	
	}
	delete polDlg;
}


/*
void RotationDlg::OnAhtyplist() 
{
	CString olText;
	m_CE_AHtyp.GetWindowText(olText);

	AwBasicDataDlg *polDlg = new AwBasicDataDlg(this, "HTY","HTYP,HNAM", "HNAM+", olText);
	if(polDlg->DoModal() == IDOK)
	{
		m_CE_AHtyp.SetInitText(polDlg->GetField("HTYP"), true);	
	}
	delete polDlg;
}




void RotationDlg::OnDhtyplist() 
{
	CString olText;
	m_CE_DHtyp.GetWindowText(olText);

	AwBasicDataDlg *polDlg = new AwBasicDataDlg(this, "HTY","HTYP,HNAM", "HNAM+", olText);
	if(polDlg->DoModal() == IDOK)
	{
		m_CE_DHtyp.SetInitText(polDlg->GetField("HTYP"), true);	
	}
	delete polDlg;
}

*/


/////////////////////////////////////////////////////////////////////////////////
//Tables !!



void RotationDlg::OnAAlc3LIST2() 
{
	int ilLine = pomAJfnoTable->pomListBox->GetTopIndex();

	CString olText;
	pomAJfnoTable->GetTextFieldValue(ilLine, 0, olText);

	
	AwBasicDataDlg *polDlg = new AwBasicDataDlg(this, "ALT","ALC2,ALC3,ALFN", "ALFN+", olText);
	polDlg->SetSecState("ROTATIONDLG_CB_AShowJfno");
	if(polDlg->DoModal() == IDOK)
	{
		pomAJfnoTable->SetIPValue(ilLine, 0, polDlg->GetField("ALC2"));
	}
	delete polDlg;
}

void RotationDlg::OnAAlc3LIST3() 
{

	int ilLine = pomAJfnoTable->pomListBox->GetTopIndex()+1;

	CString olText;
	pomAJfnoTable->GetTextFieldValue(ilLine, 0, olText);
	
	AwBasicDataDlg *polDlg = new AwBasicDataDlg(this, "ALT","ALC2,ALC3,ALFN", "ALFN+", olText);
	polDlg->SetSecState("ROTATIONDLG_CB_AShowJfno");
	if(polDlg->DoModal() == IDOK)
	{
		pomAJfnoTable->SetIPValue(ilLine, 0, polDlg->GetField("ALC2"));
	}
	delete polDlg;
}

void RotationDlg::OnDalc3list2() 
{
	int ilLine = pomDJfnoTable->pomListBox->GetTopIndex();

	CString olText;
	pomDJfnoTable->GetTextFieldValue(ilLine, 0, olText);

	
	AwBasicDataDlg *polDlg = new AwBasicDataDlg(this, "ALT","ALC2,ALC3,ALFN", "ALFN+", olText);
	polDlg->SetSecState("ROTATIONDLG_CB_DShowJfno");
	if(polDlg->DoModal() == IDOK)
	{
		pomDJfnoTable->SetIPValue(ilLine, 0, polDlg->GetField("ALC2"));
	}
	delete polDlg;
	
}

void RotationDlg::OnDalc3list3() 
{
	int ilLine = pomDJfnoTable->pomListBox->GetTopIndex()+1;

	CString olText;
	pomDJfnoTable->GetTextFieldValue(ilLine, 0, olText);
	
	AwBasicDataDlg *polDlg = new AwBasicDataDlg(this, "ALT","ALC2,ALC3,ALFN", "ALFN+", olText);
	polDlg->SetSecState("ROTATIONDLG_CB_DShowJfno");
	if(polDlg->DoModal() == IDOK)
	{
		pomDJfnoTable->SetIPValue(ilLine, 0, polDlg->GetField("ALC2"));
	}
	delete polDlg;
}



void RotationDlg::OnDcinslist1() 
{
	int ilLine = pomDCinsTable->pomListBox->GetTopIndex();

	CString olText;
	pomDCinsTable->GetTextFieldValue(ilLine, 0, olText);

	AwBasicDataDlg *polDlg = new AwBasicDataDlg(this, "CIC","CNAM,TERM", "CNAM+", olText);
	polDlg->SetSecState("ROTATIONDLG_CE_DCins");
	if(polDlg->DoModal() == IDOK)
	{
		//	reject a new checkin-counter if counter is already open
		if (CcaHasNoOpentime (ilLine) == 0L)
		{
			pomDCinsTable->SetIPValue(ilLine, 0, polDlg->GetField("CNAM"));
			pomDCinsTable->SetIPValue(ilLine, 1, polDlg->GetField("TERM"));
		}
	}
	delete polDlg;
}

void RotationDlg::OnDcinslist2() 
{
	int ilLine = pomDCinsTable->pomListBox->GetTopIndex() + 1;

	CString olText;
	pomDCinsTable->GetTextFieldValue(ilLine, 0, olText);

	AwBasicDataDlg *polDlg = new AwBasicDataDlg(this, "CIC","CNAM,TERM", "CNAM+", olText);
	polDlg->SetSecState("ROTATIONDLG_CE_DCins");
	if(polDlg->DoModal() == IDOK)
	{
		//	reject a new checkin-counter if counter is already open
		if (CcaHasNoOpentime (ilLine) == 0L)
		{
			pomDCinsTable->SetIPValue(ilLine, 0, polDlg->GetField("CNAM"));
			pomDCinsTable->SetIPValue(ilLine, 1, polDlg->GetField("TERM"));
		}
	}
	delete polDlg;
}

void RotationDlg::OnDcinslist3() 
{
	int ilLine = pomDCinsTable->pomListBox->GetTopIndex() + 2;

	CString olText;
	pomDCinsTable->GetTextFieldValue(ilLine, 0, olText);

	AwBasicDataDlg *polDlg = new AwBasicDataDlg(this, "CIC","CNAM,TERM", "CNAM+", olText);
	polDlg->SetSecState("ROTATIONDLG_CE_DCins");
	if(polDlg->DoModal() == IDOK)
	{
		//	reject a new checkin-counter if counter is already open
		if (CcaHasNoOpentime (ilLine) == 0L)
		{
			pomDCinsTable->SetIPValue(ilLine, 0, polDlg->GetField("CNAM"));
			pomDCinsTable->SetIPValue(ilLine, 1, polDlg->GetField("TERM"));
		}
	}
	delete polDlg;
}

void RotationDlg::OnDcinslist4() 
{
	int ilLine = pomDCinsTable->pomListBox->GetTopIndex() + 3;

	CString olText;
	pomDCinsTable->GetTextFieldValue(ilLine, 0, olText);

	AwBasicDataDlg *polDlg = new AwBasicDataDlg(this, "CIC","CNAM,TERM", "CNAM+", olText);
	polDlg->SetSecState("ROTATIONDLG_CE_DCins");
	if(polDlg->DoModal() == IDOK)
	{
		//	reject a new checkin-counter if counter is already open
		if (CcaHasNoOpentime (ilLine) == 0L)
		{
			pomDCinsTable->SetIPValue(ilLine, 0, polDlg->GetField("CNAM"));
			pomDCinsTable->SetIPValue(ilLine, 1, polDlg->GetField("TERM"));
		}
	}
	delete polDlg;
}


void RotationDlg::OnAdcd1list() 
{
	CString olText;
	m_CE_ADcd1.GetWindowText(olText);

	AwBasicDataDlg *polDlg = new AwBasicDataDlg(this, "DEN","DECA,DECN,ALC3,DENA", "DECN+,ALC3", olText);
	polDlg->SetSecState("ROTATIONDLG_CE_ADcd1");
	if(polDlg->DoModal() == IDOK)
	{
		m_CE_ADcd1.SetInitText(polDlg->GetField("DECN"), true);	
	}
	delete polDlg;
}

void RotationDlg::OnAdcd2list() 
{
	CString olText;
	m_CE_ADcd2.GetWindowText(olText);

	AwBasicDataDlg *polDlg = new AwBasicDataDlg(this, "DEN","DECA,DECN,ALC3,DENA", "DECN+,ALC3", olText);
	polDlg->SetSecState("ROTATIONDLG_CE_ADcd2");
	if(polDlg->DoModal() == IDOK)
	{
		m_CE_ADcd2.SetInitText(polDlg->GetField("DECN"), true);	
	}
	delete polDlg;
	
}


void RotationDlg::OnDdcd1list() 
{
	CString olText;
	m_CE_DDcd1.GetWindowText(olText);

	AwBasicDataDlg *polDlg = new AwBasicDataDlg(this, "DEN","DECA,DECN,ALC3,DENA", "DECN+,ALC3", olText);
	polDlg->SetSecState("ROTATIONDLG_CE_DDcd1");
	if(polDlg->DoModal() == IDOK)
	{
		m_CE_DDcd1.SetInitText(polDlg->GetField("DECN"), true);	
	}
	delete polDlg;
	
}



void RotationDlg::OnDdcd2list() 
{
	CString olText;
	m_CE_DDcd2.GetWindowText(olText);

	AwBasicDataDlg *polDlg = new AwBasicDataDlg(this, "DEN","DECA,DECN,ALC3,DENA", "DECN+,ALC3", olText);
	polDlg->SetSecState("ROTATIONDLG_CE_DDcd2");
	if(polDlg->DoModal() == IDOK)
	{
		m_CE_DDcd2.SetInitText(polDlg->GetField("DECN"), true);	
	}
	delete polDlg;
	
}




void RotationDlg::OnPaid() 
{
	if ((MessageBox( GetString(IDS_STRING1595),GetString(ST_FRAGE),(MB_ICONQUESTION | MB_YESNO | MB_DEFBUTTON2))) == IDYES)
	{
		ogRotationDlgFlights.UpdateFlight(prmAFlight->Urno, "PAID", "P");
		ogRotationDlgFlights.UpdateFlight(prmDFlight->Urno, "PAID", "P");
	}
	else
	{
		ogRotationDlgFlights.UpdateFlight(prmAFlight->Urno, "PAID", "B");
		ogRotationDlgFlights.UpdateFlight(prmDFlight->Urno, "PAID", "B");
	}

}





void RotationDlg::OnSelchangeDremp() 
{
	OnEditChanged(0,0);	
}

void RotationDlg::OnSelchangeAremp() 
{
	OnEditChanged(0,0);	
}

void RotationDlg::OnRotation() 
{
	CString olRegn;
	m_CE_Regn.GetWindowText(olRegn);
	if(olRegn.IsEmpty())
		return;

	CTime olTifa = prmAFlight->Tifa;

	if(olTifa == TIMENULL)
		olTifa = prmDFlight->Tifd;

	if(olTifa == TIMENULL)
		return;

	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
	CString olRetStr = ogRotationFlights.SearchLFZ(olRegn, olTifa);
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
	
	CString olText;
	if(olRetStr.IsEmpty())
		olText.Format(GetString(IDS_STRING1246), olRegn); 
	else
		olText.Format(GetString(IDS_STRING1247), olRegn, olRetStr) ;

	MessageBox(olText, GetString(IDS_STRING946));

}


void RotationDlg::ProcessRotationChange(ROTATIONDLGFLIGHTDATA *prpFlight)
{
	if (prpFlight)
		NewData(this, prpFlight->Rkey, prpFlight->Urno, prpFlight->Adid, bmLocalTime);

	return;
}


void RotationDlg::ProcessFlightChange(ROTATIONDLGFLIGHTDATA *prpFlight)
{
	if(prpFlight == NULL)
	{
		ProcessRotationChange();
		return;
	}

	if(prmAFlight->Urno == prpFlight->Urno)
	{
		*prmAFlight		= *prpFlight;
		*prmAFlightSave = *prpFlight;
		InitDialog(true,false);
	}

	if(prmDFlight->Urno == prpFlight->Urno)
	{
		*prmDFlight		= *prpFlight;
		*prmDFlightSave = *prpFlight;

		InitDialog(false, true);

		ReadCcaData();
		DShowCinsTable();	
	}

	char clPaid;
	CString olAlc3;
	// AFTPAID can be 'B','K','P'
	if(prmDFlight->Urno != 0)
	{
		olAlc3 = prmDFlight->Alc3;
		if(strlen(prmDFlight->Paid) == 0)
			clPaid = ' ';
		else
			clPaid = prmDFlight->Paid[0];
	}
	else
	{
		olAlc3 = prmAFlight->Alc3;
		if(strlen(prmAFlight->Paid) == 0)
			clPaid = ' ';
		else
			clPaid = prmAFlight->Paid[0];
	}

	UpdateCashButton(olAlc3, clPaid);

}



bool RotationDlg::ProcessRotationChange()
{

	ROTATIONDLGFLIGHTDATA *prlFlight;
	ROTATIONDLGFLIGHTDATA *prlFlight2;

	prlFlight = new ROTATIONDLGFLIGHTDATA;

	*prmAFlight		= *prlFlight;
	*prmAFlightSave = *prlFlight;
	*prmDFlight		= *prlFlight;
	*prmDFlightSave = *prlFlight;
	delete prlFlight;

	bmInit = false;

	if((prlFlight = ogRotationDlgFlights.GetFlightByUrno(lmCalledBy)) == NULL)
	{
		return false;
	}
	else
	{
//		if((strcmp(prlFlight->Des4, pcgHome4) == 0) && (!((strcmp(prlFlight->Adid, "B") == 0) && omAdid == "D")))
		if((strcmp(prlFlight->Des3, pcgHome) == 0) && (!((strcmp(prlFlight->Adid, "B") == 0) && omAdid == "D")))
		{
			*prmAFlight		= *prlFlight;
			*prmAFlightSave = *prlFlight;
			if((prlFlight2 = ogRotationDlgFlights.GetDeparture(prlFlight)) != NULL)
			{
				*prmDFlight		= *prlFlight2;
				*prmDFlightSave = *prlFlight2;
			}
		}
		else
		{
			*prmDFlight		= *prlFlight;
			*prmDFlightSave = *prlFlight;
			if((prlFlight2 = ogRotationDlgFlights.GetArrival(prlFlight)) != NULL)
			{
				*prmAFlight		= *prlFlight2;
				*prmAFlightSave = *prlFlight2;
			}
		}
	}
	InitDialog(true, true);

	
	char clPaid;
	CString olAlc3;
	if(prmDFlight->Urno != 0)
	{
		olAlc3 = prmDFlight->Alc3;
		if(strlen(prmDFlight->Paid) == 0)
			clPaid = ' ';
		else
			clPaid = prmDFlight->Paid[0];
	}
	else
	{
		olAlc3 = prmAFlight->Alc3;
		if(strlen(prmAFlight->Paid) == 0)
			clPaid = ' ';
		else
			clPaid = prmAFlight->Paid[0];
	}

	UpdateCashButton(olAlc3, clPaid); 

 
	CString olWhere;

	if(prmAFlight != NULL && prmDFlight == NULL)
	{
		olWhere.Format("WHERE FLNU = %ld", prmAFlight->Urno);
	}
	if(prmAFlight == NULL && prmDFlight != NULL)
	{
		olWhere.Format("WHERE FLNU = %ld", prmDFlight->Urno);
	}
	if(prmAFlight != NULL && prmDFlight != NULL)
	{
		olWhere.Format("WHERE FLNU = %ld OR FLNU = %ld", prmAFlight->Urno, prmDFlight->Urno);
	}

	ogBCD.Read("VIP", olWhere);



	CString olText;

	olText.Format("%s (%d)", GetString(IDS_STRING1556), ogBCD.GetDataCount("VIP"), 10);

	m_CB_Vip.SetWindowText(olText);


	/*
	if( ogBCD.GetDataCount("VIP") > 0)
	{
		m_CB_Vip.SetColors(RGB(255, 0, 0), ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
	}
	else
	{
		m_CB_Vip.EnableWindow(FALSE);
		//m_CB_Agents.EnableWindow(FALSE);
	}
s	*/

	return true;
}

void RotationDlg::OnAViewLoad() 
{
	char pclConfigPath[256];
	char pclExcelPath[256];

	if (getenv("CEDA") == NULL)
		strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
	else
		strcpy(pclConfigPath, getenv("CEDA"));

	GetPrivateProfileString(ogAppName, "LOATABVIEWER", "DEFAULT",
		pclExcelPath, sizeof pclExcelPath, pclConfigPath);


	if(!strcmp(pclExcelPath, "DEFAULT"))
		MessageBox(GetString(IDS_STRING1478), GetString(ST_FEHLER), MB_ICONERROR);


	char *args[4];
	char slRunTxt[256] = "";
	char buffer[64];
	args[0] = "child";
	sprintf(slRunTxt,"%s",ltoa(prmAFlight->Urno,buffer,10));
	args[1] = slRunTxt;
	args[2] = NULL;
	args[3] = NULL;
	int ilReturn = _spawnv(_P_NOWAIT,pclExcelPath,args);
}

void RotationDlg::OnDViewLoad() 
{
	char pclConfigPath[256];
	char pclExcelPath[256];

	if (getenv("CEDA") == NULL)
		strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
	else
		strcpy(pclConfigPath, getenv("CEDA"));

	GetPrivateProfileString(ogAppName, "LOATABVIEWER", "DEFAULT",
		pclExcelPath, sizeof pclExcelPath, pclConfigPath);


	if(!strcmp(pclExcelPath, "DEFAULT"))
		MessageBox(GetString(IDS_STRING1478), GetString(ST_FEHLER), MB_ICONERROR);


	char *args[4];
	char slRunTxt[256] = "";
	char buffer[64];
	args[0] = "child";
	sprintf(slRunTxt,"%s",ltoa(prmDFlight->Urno,buffer,10));
	args[1] = slRunTxt;
	args[2] = NULL;
	args[3] = NULL;
	int ilReturn = _spawnv(_P_NOWAIT,pclExcelPath,args);
}

void RotationDlg::OnDTelex() 
{
	CTimeSpan olSpan(1,0,0,0);

	CTime olTimeStoa = prmDFlight->Stoa;
	CTime olTimeStod = prmDFlight->Stod;

	if (bmLocalTime) ogBasicData.LocalToUtc(olTimeStoa);
	if (bmLocalTime) ogBasicData.LocalToUtc(olTimeStod);

	CTime olTimeFrom = olTimeStod - olSpan;
	CString olFrom   = olTimeFrom.Format("%Y%m%d%H%M00");
	
	CTime olTimeTo = olTimeStod + olSpan;
	CString olTo   = olTimeTo.Format("%Y%m%d%H%M00");

 	CString olTif = olTimeStod.Format("%Y%m%d%H%M00");

	pogButtonList->CallTelexpool(prmDFlight->Urno, CString(prmDFlight->Alc3), CString(prmDFlight->Fltn),
		CString(prmDFlight->Flns), olFrom, olTo, CString(prmDFlight->Regn),
		CString(prmDFlight->Flda), CString(prmDFlight->Adid), olTimeStoa, olTimeStod);
}



void RotationDlg::OnATelex() 
{
	CTimeSpan olSpan(1,0,0,0);

	CTime olTimeStoa = prmAFlight->Stoa;
	CTime olTimeStod = prmAFlight->Stod;

	if (bmLocalTime) ogBasicData.LocalToUtc(olTimeStoa);
	if (bmLocalTime) ogBasicData.LocalToUtc(olTimeStod);

	CTime olTimeFrom = olTimeStoa - olSpan;
	CString olFrom   = olTimeFrom.Format("%Y%m%d%H%M00");
	
	CTime olTimeTo = olTimeStoa + olSpan;
	CString olTo   = olTimeTo.Format("%Y%m%d%H%M00");

 	CString olTif = olTimeStoa.Format("%Y%m%d%H%M00");

	pogButtonList->CallTelexpool(prmAFlight->Urno, CString(prmAFlight->Alc3), CString(prmAFlight->Fltn),
		CString(prmAFlight->Flns), olFrom, olTo, CString(prmAFlight->Regn),
		CString(prmAFlight->Flda), CString(prmAFlight->Adid), olTimeStoa, olTimeStod);

}

void RotationDlg::OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags) 
{
	// TODO: Add your message handler code here and/or call default

	
	if(nChar == 13)
		OnOK();
	else
		CDialog::OnKeyDown(nChar, nRepCnt, nFlags);
}

void RotationDlg::ProcessCCA(CCADATA *prpCca, int ipDDXType)
{
	if(prmDFlight->Urno <= 0) 
		return;
		

	/*
	long llBaseID = atol(prpBc->Tws);

	if(llBaseID == IDM_COMMONCCA)
		return;
	


	int i;
	CCADATA *prlCca;
	CedaCcaData olData;

	prlCca = new CCADATA;
	olData.GetRecordFromItemList(prlCca,prpBc->Fields,prpBc->Data);
	
	if(prlCca->Urno == 0)
	{
		CString olSelection = CString(prpBc->Selection);

		if(olSelection.Find("URNO") >= 0)
		{
			if (olSelection.Find('\'') != -1)
			{
				prlCca->Urno = ogSeasonDlgFlights.GetUrnoFromSelection(prpBc->Selection);
			}
			else
			{
				int ilFirst = olSelection.Find("=")+1;
				int ilLast  = olSelection.GetLength();
				prlCca->Urno = atol(olSelection.Mid(ilFirst,ilLast-ilFirst));
			}
		}
	}	

	if(((prlCca->Urno == 0) || (prlCca->Flnu != prmDFlight->Urno )) && (ipDDXType != BC_CCA_DELETE) )
	{
		delete prlCca;
		return;
	}	

	if(bmLocalTime)	omCcaData.StructUtcToLocal((void*)prlCca);

	omDCinsSave.DeleteAll();

	for (i = omDCins.GetSize() - 1; i >= 0 ; i--)
	{
		if((omDCins[i].Urno == 0) || ( (ipDDXType == BC_CCA_DELETE)  && (omDCins[i].Urno == prlCca->Urno)))
			omDCins.DeleteAt(i);
	}

	if(ipDDXType != BC_CCA_DELETE)
	{
		bool blInsert = false;

		for (i = omDCins.GetSize() - 1; i >= 0 ; i--)
		{
			if(omDCins[i].Urno == prlCca->Urno)
			{
				if((prlCca->Ckbs == TIMENULL) && (strcmp(prlCca->Ckic, "") == 0))
				{
					omDCins.DeleteAt(i);
				}
				else
				{
					*(&omDCins[i]) = *prlCca;
				}
				blInsert = true;
				break;
			}
		}

		if(!blInsert)
		{
			omDCins.New(*prlCca);

		}

	}
	delete prlCca;

	for (i = 0; i < omDCins.GetSize() ; i++)
	{
		omDCinsSave.New(omDCins[i]);
	}

  */

	if (ipDDXType == CCA_ROTDLG_NEW || ipDDXType == CCA_ROTDLG_DELETE)
	{
		if (ipDDXType == CCA_ROTDLG_DELETE)
		{
			// delete the record in omCcaData at first
			omCcaData.DeleteInternal(prpCca, false);

		}

		CCAFLIGHTDATA rlCcaFlight;

//		if(bmLocalTime) ogRotationDlgFlights.StructLocalToUtc((void*)prmDFlight, "");
 		if(bmLocalTime) ConvertStructLocalTOUtc(NULL, prmDFlight);
		rlCcaFlight = *prmDFlight;
// 		if(bmLocalTime) ogRotationDlgFlights.StructUtcToLocal((void*)prmDFlight, "");
 		if(bmLocalTime) ConvertStructUtcTOLocal(NULL, prmDFlight);

		omCcaData.CheckDemands(&rlCcaFlight);
	}

	GetCcas();
	DShowCinsTable();


/*

	CStringArray olData;
	CStringArray olFields;
	bool blReload = false;	

	long llUrno = 0;
	long llFlnu = 0;

	int ilFieldCount = ExtractItemList(CString(prpBc->Fields), &olFields);
	int ilDataCount = ExtractItemList(CString(prpBc->Data), &olData);


	if(ilFieldCount != ilDataCount)
		return;


	for(int i = ilFieldCount - 1; i >= 0; i--)
	{

		if(olFields[i] == "FLNU")
		{
			llFlnu = atol(olData[i]);
			if(llFlnu == prmDFlight->Urno)
				blReload = true;
		}
		if(olFields[i] == "URNO")
		{
			llUrno = atol(olData[i]);
		}
	}



	CString olSelection = CString(prpBc->Selection);

	if(olSelection.Find("URNO") >= 0)
	{
		if (olSelection.Find('\'') != -1)
		{
			llUrno = ogRotationFlights.GetUrnoFromSelection(prpBc->Selection);
		}
		else
		{
			int ilFirst = olSelection.Find("=")+1;
			int ilLast  = olSelection.GetLength();
			llUrno = atol(olSelection.Mid(ilFirst,ilLast-ilFirst));
		}
	}



	if(!blReload && (llUrno > 0))
	{

		for (i = 0; i < omDCins.GetSize(); i++)
		{
			if(omDCins[i].Urno == llUrno)
				blReload = true;
		}
	}


	if(blReload)
	{
		omDCins.DeleteAll();
		omDCinsSave.DeleteAll();
		
		if(prmDFlight->Urno > 0)
		{
			CedaCcaData olData;
			char pclWhere[128];
			sprintf(pclWhere,"WHERE FLNU = %d", prmDFlight->Urno);
			olData.ReadSpecial(omDCins, pclWhere);
			for(int i = 0; i < omDCins.GetSize(); i++)
			{
				omDCinsSave.New(omDCins[i]);
			}
		}

		DShowCinsTable();

	}

*/

}



void RotationDlg::ReadCcaData()
{
	if (prmDFlight->Urno <= 0)
		return;

	CString olWhere;
	olWhere.Format("FLNU = %d", prmDFlight->Urno);
	omCcaData.ReadSpecial(olWhere);

	CCAFLIGHTDATA rlCcaFlight;

//		if(bmLocalTime) ogRotationDlgFlights.StructLocalToUtc((void*)prmDFlight, "");
 	if(bmLocalTime) ConvertStructLocalTOUtc(NULL, prmDFlight);
	rlCcaFlight = *prmDFlight;
// 		if(bmLocalTime) ogRotationDlgFlights.StructUtcToLocal((void*)prmDFlight, "");
 	if(bmLocalTime) ConvertStructUtcTOLocal(NULL, prmDFlight);

	omCcaData.CheckDemands(&rlCcaFlight);

	GetCcas();
}



void RotationDlg::GetCcas()
{

	if(prmDFlight->Urno <= 0)
		return;


	omDCins.DeleteAll();
	for(int i = 0; i < omCcaData.omData.GetSize(); i++)
	{
		omDCins.New(omCcaData.omData[i]);
	}
	omDCins.Sort(CompareCcas);	

	// create 'save'-records
	omDCinsSave.DeleteAll();
	for(i = 0; i < omDCins.GetSize(); i++)
	{
		omDCinsSave.New(omDCins[i]);
	}


}


void RotationDlg::DShowCinsTable()
{
	CCSEDIT_ATTRIB rlAttribC1;
	CCSEDIT_ATTRIB rlAttribC2;
	CCSEDIT_ATTRIB rlAttribC3;
	CCSEDIT_ATTRIB rlAttribC4;

	CCSPtrArray<TABLE_COLUMN> olColList;
	TABLE_COLUMN rlColumnData;

	int ilLc ;
	
	
	pomDCinsTable->ResetContent();


	rlAttribC1.Style = ES_UPPERCASE;


	rlAttribC2.Format = "X";
	rlAttribC2.TextMinLenght = 0;
	rlAttribC2.TextMaxLenght = 1;
	rlAttribC2.Style = ES_UPPERCASE;

	rlAttribC3.Type = KT_TIME;
	rlAttribC3.ChangeDay	= true;
	rlAttribC3.TextMaxLenght = 7;

	rlAttribC4.Type = KT_TIME;
	rlAttribC4.ChangeDay	= true;
	rlAttribC4.TextMaxLenght = 7;


	rlColumnData.Font = &ogMS_Sans_Serif_8;
	rlColumnData.Alignment = COLALIGN_LEFT;

	if(ogPrivList.GetStat("ROTATIONDLG_CE_DCins") != '1')
		rlColumnData.BkColor = RGB(192,192,192);


	CTime olTmpTime;
	for (ilLc = 0; ilLc < omDCins.GetSize(); ilLc++)
	{
		rlColumnData.blIsEditable = true;	
		rlColumnData.Text = omDCins[ilLc].Ckic;
		rlColumnData.EditAttrib = rlAttribC1;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omDCins[ilLc].Ckit;
		rlColumnData.EditAttrib = rlAttribC2;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		
		olTmpTime = omDCins[ilLc].Ckbs;
		if(bmLocalTime) ogBasicData.UtcToLocal(olTmpTime);
		rlColumnData.Text = DateToHourDivString(olTmpTime, prmDFlight->Stod);//omDCins[ilLc].Ckbs.Format("%H:%M");
		rlColumnData.EditAttrib = rlAttribC3;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		
		olTmpTime = omDCins[ilLc].Ckes;
		if(bmLocalTime) ogBasicData.UtcToLocal(olTmpTime);
		rlColumnData.Text = DateToHourDivString(olTmpTime, prmDFlight->Stod);//omDCins[ilLc].Ckes.Format("%H:%M");
		rlColumnData.EditAttrib = rlAttribC4;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		
		olTmpTime = omDCins[ilLc].Ckba;
		if(bmLocalTime) ogBasicData.UtcToLocal(olTmpTime);
		rlColumnData.Text = DateToHourDivString(olTmpTime, prmDFlight->Stod);//omDCins[ilLc].Ckbs.Format("%H:%M");
		rlColumnData.EditAttrib = rlAttribC3;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		
		olTmpTime = omDCins[ilLc].Ckea;
		if(bmLocalTime) ogBasicData.UtcToLocal(olTmpTime);
		rlColumnData.Text = DateToHourDivString(olTmpTime, prmDFlight->Stod);//omDCins[ilLc].Ckes.Format("%H:%M");
		rlColumnData.EditAttrib = rlAttribC4;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		
		rlColumnData.Text = omDCins[ilLc].Disp;//omDCins[ilLc].Ckes.Format("%H:%M");
		rlColumnData.EditAttrib = rlAttribC4;
		rlColumnData.blIsEditable = false;	

		olColList.NewAt(olColList.GetSize(), rlColumnData);

		
		pomDCinsTable->AddTextLine(olColList, (void*)(NULL));
		olColList.DeleteAll();
	}

	for (ilLc = omDCins.GetSize(); ilLc < 30; ilLc++)
	{
		rlColumnData.blIsEditable = true;	
		rlColumnData.Text = "";
		rlColumnData.EditAttrib = rlAttribC1;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = "";
		rlColumnData.EditAttrib = rlAttribC2;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = "";
		rlColumnData.EditAttrib = rlAttribC3;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = "";
		rlColumnData.EditAttrib = rlAttribC4;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = "";
		rlColumnData.EditAttrib = rlAttribC3;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = "";
		rlColumnData.EditAttrib = rlAttribC4;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = "";
		rlColumnData.EditAttrib = rlAttribC4;
		rlColumnData.blIsEditable = false;	
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		pomDCinsTable->AddTextLine(olColList, (void*)(NULL));
		olColList.DeleteAll();
	}
	pomDCinsTable->DisplayTable();
}



///////////////////////////////////////////////////////////////////////////////////////////////////////
//  weiter Flugnummer - Ankunft
//
void RotationDlg::OnAshowjfno() 
{

	if(!bmIsAFfnoShown)
	{
		char clStat = ogPrivList.GetStat("ROTATIONDLG_CB_AShowJfno");
		m_CB_AShowJfno.ShowWindow(SW_HIDE);
		m_CE_AJfnoBorder.ShowWindow(SW_SHOW);
		m_CB_AAlc3List2.ShowWindow(SW_SHOW);
		m_CB_AAlc3List3.ShowWindow(SW_SHOW);
	    pomAJfnoTable->ShowWindow(SW_SHOW);
		SetpWndStatAll(ogPrivList.GetStat("ROTATIONDLG_CB_AShowJfno"),pomAJfnoTable);
		if(clStat == '1')
			pomAJfnoTable->MakeInplaceEdit(0, 0);
	}
	bmIsAFfnoShown = true;



}


void RotationDlg::AShowJfnoButton() 
{
	m_CB_AShowJfno.ShowWindow(SW_SHOW);
	m_CE_AJfnoBorder.ShowWindow(SW_HIDE);
	m_CB_AAlc3List2.ShowWindow(SW_HIDE);
	m_CB_AAlc3List3.ShowWindow(SW_HIDE);
    pomAJfnoTable->ShowWindow(SW_HIDE);
	bmIsAFfnoShown = false;

}



///////////////////////////////////////////////////////////////////////////////////////////////////////
//  weiter Flugnummer - Abflug
//
void RotationDlg::OnDshowjfno() 
{
	if(!bmIsDFfnoShown)
	{

		char clStat = ogPrivList.GetStat("ROTATIONDLG_CB_DShowJfno");

		m_CB_DShowJfno.ShowWindow(SW_HIDE);

		m_CE_DJfnoBorder.ShowWindow(SW_SHOW);
		m_CB_DAlc3List2.ShowWindow(SW_SHOW);
		m_CB_DAlc3List3.ShowWindow(SW_SHOW);
	    pomDJfnoTable->ShowWindow(SW_SHOW);
		SetpWndStatAll(ogPrivList.GetStat("ROTATIONDLG_CB_DShowJfno"),pomDJfnoTable);
		if(clStat == '1')
			pomDJfnoTable->MakeInplaceEdit(0, 0);



	}
	bmIsDFfnoShown = true;
}


void RotationDlg::DShowJfnoButton() 
{
	m_CB_DShowJfno.ShowWindow(SW_SHOW);

	m_CE_DJfnoBorder.ShowWindow(SW_HIDE);
	m_CB_DAlc3List2.ShowWindow(SW_HIDE);
	m_CB_DAlc3List3.ShowWindow(SW_HIDE);

    pomDJfnoTable->ShowWindow(SW_HIDE);
	bmIsDFfnoShown = false;

}




void RotationDlg::SetSecState()
{

	m_CB_DRetTaxi.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CB_DRetTaxi"));	
	m_CB_DRetFlight.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CB_DRetFlight"));	
	m_CB_ARetTaxi.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CB_ARetTaxi"));	
	m_CB_ARetFlight.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CB_ARetFlight"));	

	m_CE_ADivr.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CB_ADiverted"));
	//m_CE_DDivr.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CB_DDiverted"));
	m_CE_DDivr.SetSecState('-');

	m_CB_Ok.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CB_Ok"));			
	m_CB_Rotation.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CB_Rotation"));		
	
	m_CB_Agents.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CB_Agents"));
	m_CB_Status.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CB_Status"));
	m_CB_Object.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CB_Object"));
	
	m_CB_Split.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CB_Teilen"));		
	m_CB_Join.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CB_Verbinden"));	

	m_CB_Vip.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CB_VIP"));
	m_CB_Towing.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CB_Towing"));
	m_CB_SeasonMask.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CB_SEASONMASK"));
			
	m_CB_Paid.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CB_Paid"));	
	m_CE_Ming.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_Ming"));	
	m_CE_Regn.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_Regn"));	
	m_CE_Act3.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_Act3"));	
	m_CE_Act5.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_Act5"));	
	m_CB_Act35List.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CB_Act35List"));	

	SetWndStatAll(ogPrivList.GetStat("ROTATIONDLG_CL_ARemp"), m_CL_ARemp);
	SetWndStatAll(ogPrivList.GetStat("ROTATIONDLG_CL_DRemp"), m_CL_DRemp);		

	m_CE_AAirb.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_AAirb"));
	m_CB_AExt2List.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CB_AExt2List"));	
	m_CB_AExt1List.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CB_AExt1List"));	
	m_CB_APstaList.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CB_APstaList"));	
	m_CB_ATelex.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CB_ATelex"));		
	m_CB_AStypList.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CB_AStypList"));	
	m_CB_ADcd1List.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CB_ADcd1List"));	
	m_CB_ADcd2List.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CB_ADcd2List"));	
	m_CB_ARerouted.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CB_ARerouted"));	
	m_CB_AAlc3List.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CB_AAlc3List"));	
	m_CB_AAlc3List2.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CB_AAlc3List23"));	
	m_CB_AAlc3List3.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CB_AAlc3List23"));	
	m_CB_ADiverted.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CB_ADiverted"));	
	m_CB_ACxx.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CB_ACxx"));
	m_CB_GoAround.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CB_GoAround"));
	m_CB_ABlt1List.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CB_ABlt1List"));	
//	m_CB_AHtypList.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CB_AHtypList"));
	m_CB_ABlt2List.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CB_ABlt2List"));	
	m_CB_AGta2List.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CB_AGta2List"));	
	m_CB_AGta1List.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CB_AGta1List"));	
	m_CB_AOrg3List.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CB_AOrg3List"));	
	m_CB_ATtypList.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CB_ATtypList"));	
	m_CE_AOfbl.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_AOfbl"));		
//	m_CE_APaba.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_APaba"));		
	m_CE_AEtdi.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_AEtdi"));		
	m_CE_ARwya.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_ARwya"));		
	m_CE_AAlc3.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_AAlc3"));		
	m_CE_APaea.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_APaea"));		
	m_CE_ATet2.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_ATet2"));		
	m_CE_ATet1.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_ATet1"));		
	m_CE_AOrg3.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_AOrg3"));		
	m_CE_ADes3.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_ADes3"));		
	m_CE_ATtyp.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_ATtyp"));		
	m_CE_ATmoa.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_ATmoa"));		
	m_CE_ATmb2.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_ATmb2"));		
	m_CE_ATmb1.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_ATmb1"));		
	m_CE_ATga2.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_ATga2"));		
	m_CE_ATga1.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_ATga1"));		
	m_CE_AStyp.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_AStyp"));		
	m_CE_AStod.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_AStod"));		
	m_CE_AStoa.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_AStoa"));		
	m_CE_AStev.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_AStev"));		
	m_CE_ARem1.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_ARem1"));		
	m_CE_APsta.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_APsta"));		
	m_CE_AOnbl.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_AOnbl"));		
	m_CE_AOnbe.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_AOnbe"));		
	m_CE_AOfbl.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_AOfbl"));		
	m_CE_ANxti.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_ANxti"));		
	m_CE_ALand.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_ALand"));		
	m_CE_AIfra.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_AIfra"));		
//	m_CE_AHtyp.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_AHtyp"));		
	m_CE_AGta2.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_AGta2"));		
	m_CE_AGta1.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_AGta1"));		
	m_CE_AGa2y.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_AGa2y"));		
	m_CE_AGa1y.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_AGa1y"));		
	m_CE_AGa2x.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_AGa2x"));		
	m_CE_AGa1x.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_AGa1x"));		
	m_CE_AExt2.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_AExt2"));		
	m_CE_AExt1.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_AExt1"));		
	m_CE_AEtoa.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_AEtoa"));		
	m_CE_AEtai.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_AEtai"));		
	m_CE_ADtd2.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_ADtd2"));		
	m_CE_ADtd1.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_ADtd1"));		
	m_CE_ADcd2.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_ADcd2"));		
	m_CE_ADcd1.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_ADcd1"));		
	m_CE_ABlt2.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_ABlt2"));		
	m_CE_ABlt1.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_ABlt1"));	
	m_CE_AB2ea.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_AB2ea"));		
	m_CE_AB2ba.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_AB2ba"));		
	m_CE_AB1ea.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_AB1ea"));		
	m_CE_AB1ba.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_AB1ba"));		
	m_CE_AFlns.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_AFlns"));		
	m_CE_AFltn.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_AFltn"));		
	m_CE_AFlti.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_AFlti"));		
	m_CE_ACsgn.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_ACsgn"));		
	m_CE_ABbaa.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_ABbaa"));		
	m_CE_DBbfa.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_DBbfa"));		
	m_CE_ALastChange.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_ALastChange"));	
	m_CE_ALastChangeTime.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_ALastChangeTime"));	

	m_CB_Load_View_Arr.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CB_ALoadView"));	
	m_CB_Load_View_Dep.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CB_DLoadView"));	

	m_CB_DTtypList.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CB_DTtypList"));	
	m_CB_DDcd1List.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CB_DDcd1List"));	
	m_CB_DCinsList1.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CB_DCinsList"));	
	m_CB_DCinsList2.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CB_DCinsList"));	
	m_CB_DCinsList3.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CB_DCinsList"));	
	m_CB_DCinsList4.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CB_DCinsList"));	

	m_CB_DCicRem1.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CB_DCicRem"));	
	m_CB_DCicRem2.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CB_DCicRem"));	
	m_CB_DCicRem3.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CB_DCicRem"));	
	m_CB_DCicRem4.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CB_DCicRem"));	
	
			
	m_CB_DPstdList.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CB_DPstdList"));	
	m_CB_DWro1List.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CB_DWro1List"));	
//	m_CB_DHtypList.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CB_DHtypList"));	
	m_CB_DTelex.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CB_DTelex"));	
	m_CB_DStypList.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CB_DStypList"));	
	m_CB_DGta1List.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CB_DGta1List"));	
	m_CB_DGta2List.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CB_DGta2List"));	
	m_CB_DDes3List.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CB_DDes3List"));	
	m_CB_DDcd2List.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CB_DDcd2List"));	
	m_CB_DAlc3List.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CB_DAlc3List"));	
	m_CB_DAlc3List2.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CB_DAlc3List23"));	
	m_CB_DAlc3List3.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CB_DAlc3List23"));	
	m_CB_DCxx.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CB_DCxx"));			
	//m_CB_DDiverted.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CB_DDiverted"));	
	//m_CB_DRerouted.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CB_DRerouted"));	
	m_CB_DDiverted.SetSecState('-');	
	m_CB_DRerouted.SetSecState('-');	
	m_CE_DGd2x.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_DGd2x"));		
	m_CE_DFltn.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_DFltn"));		
	m_CE_DFlti.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_DFlti"));		
	m_CE_DOrg3.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_DOrg3"));		
	m_CE_DW1ea.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_DW1ea"));		
	m_CE_DW1ba.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_DW1ba"));		
	m_CE_DPdea.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_DPdea"));		
	m_CE_DPdba.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_DPdba"));		
	m_CE_DWro1.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_DWro1"));		
	m_CE_DTwr1.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_DTwr1"));		
	m_CE_DTtyp.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_DTtyp"));		
	m_CE_DTgd2.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_DTgd2"));		
	m_CE_DTgd1.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_DTgd1"));		
	m_CE_DStyp.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_DStyp"));		
	m_CE_DStod.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_DStod"));		
	m_CE_DStoa.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_DStoa"));		
	m_CE_DStev.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_DStev"));		
	m_CE_DSlot.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_DSlot"));		
	m_CE_DRwyd.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_DRwyd"));		
	m_CE_DRem1.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_DRem1"));		
	m_CE_DPstd.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_DPstd"));		
	m_CE_DOfbl.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_DOfbl"));		
	m_CE_DNxti.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_DNxti"));		
	m_CE_DIskd.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_DIskd"));		
	m_CE_DIfrd.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_DIfrd"));		
//	m_CE_DHtyp.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_DHtyp"));		
	m_CE_DGtd2.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_DGtd2"));		
	m_CE_DGtd1.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_DGtd1"));		
	m_CE_DGd2y.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_DGd2y"));		
	m_CE_DGd1y.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_DGd1y"));		
	m_CE_DGd1x.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_DGd1x"));		
	m_CE_DFlns.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_DFlns"));		
	m_CE_DEtod.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_DEtod"));		
	m_CE_DEtdi.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_DEtdi"));		
	m_CE_DEtdc.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_DEtdc"));		
	m_CE_DDtd2.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_DDtd2"));		
	m_CE_DDtd1.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_DDtd1"));		
	m_CE_DDes3.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_DDes3"));		
	m_CE_DDes4.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_DDes4"));		
	m_CE_DDcd2.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_DDcd2"));		
	m_CE_DDcd1.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_DDcd1"));		
	m_CE_DAlc3.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_DAlc3"));		
	m_CE_DAirb.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_DAirb"));		
	m_CE_DCsgn.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_DCsgn"));		
	m_CE_DLastChange.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_DLastChange"));	
	m_CE_DLastChangeTime.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_DLastChangeTime"));	


	m_CE_DBaz1.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_DBaz1"));
	m_CE_DBaz4.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_DBaz4"));
	m_CE_DBao1.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_DBao1"));
	m_CE_DBac1.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_DBac1"));
	m_CE_DBao4.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_DBao4"));
	m_CE_DBac4.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_DBac4"));


	if(ogPrivList.GetStat("ROTATIONDLG_CE_Load") == '-')
	{
		m_CB_ALoad.SetSecState('-');
		m_CB_DLoad.SetSecState('-');
	}
	else
	{
		m_CB_ALoad.SetSecState('1');
		m_CB_DLoad.SetSecState('1');
	}


	if(ogPrivList.GetStat("ROTATIONDLG_CE_Gpu") == '-')
	{
		m_CB_AGpu.SetSecState('-');
		m_CB_DGpu.SetSecState('-');
	}
	else
	{
		m_CB_AGpu.SetSecState('1');
		m_CB_DGpu.SetSecState('1');
	}


	if (strcmp(pcgHome, "ATH") != 0)
	{
		// Flti is only editable for Athen AIA
		m_CE_DFlti.SetSecState('0');
		m_CE_AFlti.SetSecState('0');
	}

}





void RotationDlg::EnableArrival()
{
	BOOL blEnable = FALSE;

	if(prmAFlight->Urno > 0)  
		blEnable = TRUE;

	if (bmRotationReadOnly)
		blEnable = FALSE;

	if(!blEnable)
	{
		AShowJfnoButton();
	}



	if( CString(prmAFlight->Ftyp)	== "Z" || CString(prmAFlight->Ftyp)	== "B")
	{
		if (polRotationAViaDlg)
			polRotationAViaDlg->Enable(false);

		m_CE_ARem1.EnableWindow(FALSE);
	}
	else
	{
		if (polRotationAViaDlg)
			polRotationAViaDlg->Enable(blEnable);

		m_CE_ARem1.EnableWindow(blEnable);
	}


	if( blEnable == TRUE && CString(prmAFlight->Ftyp)	== "D")
	{
		m_CE_ADivr.EnableWindow(TRUE);
	}
	else
	{
		m_CE_ADivr.EnableWindow(FALSE);
	}

	char clStat = ogPrivList.GetStat("ROTATIONDLG_CL_ARemp");

	m_CL_ARemp.ShowWindow(SW_SHOW);

	if(clStat == '1')
	{
		m_CL_ARemp.EnableWindow(blEnable);
	}
	else
	{
		if(clStat == '0')
		{
			m_CL_ARemp.EnableWindow(FALSE);
		}
		else
		{
			m_CL_ARemp.ShowWindow(SW_HIDE);
		}

	}

	m_CB_ALoad.EnableWindow(blEnable);
	m_CB_AGpu.EnableWindow(blEnable);


	if(blEnable)
	{
		CString olPst = CString(prmAFlight->Psta);
		if(!olPst.IsEmpty())
		{
			CString olGpuStat = ogBCD.GetField("PST", "PNAM", olPst, "GPUS");

			if(olGpuStat != "X")
				m_CB_AGpu.EnableWindow(FALSE);

		}
		else
		{
			m_CB_AGpu.EnableWindow(FALSE);
		}
	}


	m_CB_ARetFlight.EnableWindow(blEnable);
	m_CB_ARetTaxi.EnableWindow(blEnable);

	m_CB_Load_View_Arr.EnableWindow(blEnable);

	m_CB_AExt1List.EnableWindow(blEnable);
	m_CB_AExt2List.EnableWindow(blEnable);
	m_CB_APstaList.EnableWindow(blEnable);
	m_CB_ATtypList.EnableWindow(blEnable);

	m_CB_DStodCalc.EnableWindow(blEnable);
	m_CB_AAlc3List3.EnableWindow(blEnable);
	m_CB_AAlc3List2.EnableWindow(blEnable);
	m_CS_Ankunft.EnableWindow(blEnable);
	m_CE_APsta.EnableWindow(blEnable);
	m_CE_AAlc3.EnableWindow(blEnable);
	m_CE_ATet2.EnableWindow(blEnable);
	m_CE_ATet1.EnableWindow(blEnable);
	m_CE_AOrg3.EnableWindow(blEnable);
	m_CE_ADes3.EnableWindow(blEnable);
	m_CE_ATtyp.EnableWindow(blEnable);
	m_CE_ATmoa.EnableWindow(blEnable);
	m_CE_ATmb2.EnableWindow(blEnable);
	m_CE_ATmb1.EnableWindow(blEnable);
	m_CE_ATga2.EnableWindow(blEnable);
	m_CE_ATga1.EnableWindow(blEnable);
	m_CB_ATelex.EnableWindow(blEnable);
	m_CB_AStypList.EnableWindow(blEnable);
	m_CE_AStyp.EnableWindow(blEnable);
	m_CE_AStod.EnableWindow(blEnable);
	m_CE_AStoa.EnableWindow(blEnable);
	m_CE_AStev.EnableWindow(blEnable);
	m_CB_AShowJfno.EnableWindow(blEnable);
	m_CB_ARerouted.EnableWindow(blEnable);
	m_CE_APsta.EnableWindow(blEnable);
	m_CB_AOrg3List.EnableWindow(blEnable);
	m_CE_AOnbl.EnableWindow(blEnable);
	m_CE_AOfbl.EnableWindow(blEnable);
	m_CE_ANxti.EnableWindow(blEnable);
	m_CE_ALastChange.EnableWindow(blEnable);
	m_CE_ALastChangeTime.EnableWindow(blEnable);
	m_CE_ALand.EnableWindow(blEnable);
	m_CE_AIfra.EnableWindow(blEnable);
//	m_CB_AHtypList.EnableWindow(blEnable);
//	m_CE_AHtyp.EnableWindow(blEnable);
	m_CB_AGta2List.EnableWindow(blEnable);
	m_CB_AGta1List.EnableWindow(blEnable);
	m_CE_AGta2.EnableWindow(blEnable);
	m_CE_AGta1.EnableWindow(blEnable);
	m_CE_AGa2y.EnableWindow(blEnable);
	m_CE_AGa1y.EnableWindow(blEnable);
	m_CE_ABbaa.EnableWindow(blEnable);
	m_CE_AGa2x.EnableWindow(blEnable);
	m_CE_AGa1x.EnableWindow(blEnable);
	m_CE_AExt2.EnableWindow(blEnable);
	m_CE_AExt1.EnableWindow(blEnable);
	m_CE_AEtoa.EnableWindow(blEnable);
	m_CE_AEtai.EnableWindow(blEnable);
	m_CE_ADtd2.EnableWindow(blEnable);
	m_CE_ADtd1.EnableWindow(blEnable);
	m_CB_ADiverted.EnableWindow(blEnable);
	m_CB_ADcd2List.EnableWindow(blEnable);
	m_CE_ADcd2.EnableWindow(blEnable);
	m_CB_ADcd1List.EnableWindow(blEnable);
	m_CE_ADcd1.EnableWindow(blEnable);
	m_CB_ACxx.EnableWindow(blEnable);
	m_CB_GoAround.EnableWindow(blEnable);
	m_CB_ABlt2List.EnableWindow(blEnable);
	m_CE_ABlt2.EnableWindow(blEnable);
	m_CB_ABlt1List.EnableWindow(blEnable);
	m_CE_ABlt1.EnableWindow(blEnable);
	m_CE_AB2ea.EnableWindow(blEnable);
	m_CE_AB2ba.EnableWindow(blEnable);
	m_CE_AB1ea.EnableWindow(blEnable);
	m_CE_AB1ba.EnableWindow(blEnable);
	m_CE_AFlns.EnableWindow(blEnable);
	m_CE_AFltn.EnableWindow(blEnable);
	m_CE_AFlti.EnableWindow(blEnable);
	m_CB_AAlc3List.EnableWindow(blEnable);
	m_CE_AAirb.EnableWindow(blEnable);
	m_CE_ARwya.EnableWindow(blEnable);
	m_CE_ACsgn.EnableWindow(blEnable);

//	m_CE_APaba.ShowWindow(SW_HIDE);
	m_CE_AEtdi.ShowWindow(SW_SHOW);
	m_CE_AEtdi.EnableWindow(blEnable);
	m_CE_APaea.ShowWindow(SW_HIDE);



	m_CE_AJfnoBorder.EnableWindow(FALSE);

	if(omAJfno.GetSize() > 0)
		OnAshowjfno();
	else
		AShowJfnoButton();

	m_CB_AShowJfno.EnableWindow(blEnable);



	if( CString(prmAFlight->Ftyp)	== "Z" )
	{
		m_CE_AAirb.EnableWindow(FALSE);	
		m_CE_AOfbl.EnableWindow(FALSE);	
	}


	if( CString(prmAFlight->Ftyp)	== "B" )
	{
		m_CE_ALand.ShowWindow(SW_HIDE);	
		m_CE_AAirb.ShowWindow(SW_HIDE);	
		m_CE_AOfbl.EnableWindow(FALSE);	
	}
	else
	{  
		m_CE_ALand.ShowWindow(SW_SHOW);	
		m_CE_AAirb.ShowWindow(SW_SHOW);	
	}


	if(( CString(prmAFlight->Ftyp)	== "Z" ) || (CString(prmAFlight->Ftyp)	== "B" ))
	{
		m_CE_AOrg3.EnableWindow(FALSE);	
		m_CB_AOrg3List.EnableWindow(FALSE);	
	}
	else
	{
		m_CE_AOrg3.EnableWindow(blEnable);	
		m_CB_AOrg3List.EnableWindow(blEnable);	
	}


	if(prmAFlight->Airb == TIMENULL || prmAFlight->Ofbl == TIMENULL)
	{
		m_CB_ARetFlight.EnableWindow(FALSE);
	}
	else
	{
		m_CB_ARetFlight.EnableWindow(blEnable);
	}


	if(prmAFlight->Ofbl == TIMENULL)
	{
		m_CB_ARetTaxi.EnableWindow(FALSE);
	}
	else
	{
		m_CB_ARetTaxi.EnableWindow(blEnable);
		if(prmAFlight->Airb != TIMENULL)
			m_CB_ARetTaxi.EnableWindow(FALSE);
	}


}



void RotationDlg::EnableGlobal()
{
	BOOL blEnable = FALSE;

	if((prmAFlight->Urno > 0) || (prmDFlight->Urno > 0))
		blEnable = TRUE;

	if (bmRotationReadOnly)
		blEnable = FALSE;

	m_CB_SeasonMask.EnableWindow(blEnable);
	m_CB_Paid.EnableWindow(blEnable);
	m_CE_Act5.EnableWindow(blEnable);
	m_CE_Act3.EnableWindow(blEnable);
	m_CE_Regn.EnableWindow(blEnable);
	m_CB_Join.EnableWindow(blEnable);
	m_CE_Ming.EnableWindow(blEnable);
	m_CB_Split.EnableWindow(blEnable);
	m_CB_Rotation.EnableWindow(blEnable);
	m_CB_Ok.EnableWindow(blEnable);
//	m_CB_Towing.EnableWindow(blEnable);
	m_CB_Vip.EnableWindow(blEnable);
	m_CB_Act35List.EnableWindow(blEnable);;

	//m_CB_Paid.EnableWindow(blEnable);
	//m_CB_Object.EnableWindow(blEnable);
	//m_CB_Status.EnableWindow(blEnable);

	// additional Text for caption
	CString olTimes;
	if (bmLocalTime)
		olTimes = GetString(IDS_STRING1921);
	else
		olTimes = GetString(IDS_STRING1920);
	CString olCaption = GetString(IDS_STRING1974);
	olCaption += " ("+olTimes+")";
	SetWindowText(olCaption);

}



void RotationDlg::EnableDeparture()
{
	BOOL blEnable = FALSE;

	if(prmDFlight->Urno > 0) 
		blEnable = TRUE;

	if (bmRotationReadOnly)
		blEnable = FALSE;

	if(blEnable)
	{
		pomDCinsTable->ShowWindow(SW_SHOW);
		SetpWndStatAll(ogPrivList.GetStat("ROTATIONDLG_CE_DCins"),pomDCinsTable);
	}
	else
	{
		pomDCinsTable->ShowWindow(SW_HIDE);
	}



	char clStat = ogPrivList.GetStat("ROTATIONDLG_CL_DRemp");

	m_CL_DRemp.ShowWindow(SW_SHOW);

	if(clStat == '1')
	{
		m_CL_DRemp.EnableWindow(blEnable);
	}
	else
	{
		if(clStat == '0')
		{
			m_CL_DRemp.EnableWindow(FALSE);
		}
		else
		{
			m_CL_DRemp.ShowWindow(SW_HIDE);
		}

	}




	if( CString(prmDFlight->Ftyp)	== "Z" || CString(prmDFlight->Ftyp)	== "B")
	{
		if (polRotationDViaDlg)
			polRotationDViaDlg->Enable(false);

		m_CE_DRem1.EnableWindow(FALSE);
	}
	else
	{
		if (polRotationDViaDlg)
			polRotationDViaDlg->Enable(blEnable);

		m_CE_DRem1.EnableWindow(blEnable);
	}

	if( blEnable == TRUE && CString(prmDFlight->Ftyp)	== "D")
	{
		m_CE_DDivr.EnableWindow(TRUE);
	}
	else
	{
		m_CE_DDivr.EnableWindow(FALSE);
	}


	m_CB_DLoad.EnableWindow(blEnable);
	m_CB_DGpu.EnableWindow(blEnable);

	if(blEnable)
	{
		CString olPst = CString(prmDFlight->Pstd);
		if(!olPst.IsEmpty())
		{
			CString olGpuStat = ogBCD.GetField("PST", "PNAM", olPst, "GPUS");

			if(olGpuStat != "X")
				m_CB_DGpu.EnableWindow(FALSE);
		}
		else
		{
			m_CB_DGpu.EnableWindow(FALSE);
		}
	}




	m_CB_DRetFlight.EnableWindow(blEnable);
	m_CB_DRetTaxi.EnableWindow(blEnable);


	m_CE_DBaz1.EnableWindow(blEnable);
	m_CE_DBaz4.EnableWindow(blEnable);
	m_CE_DBao1.EnableWindow(blEnable);
	m_CE_DBac1.EnableWindow(blEnable);
	m_CE_DBao4.EnableWindow(blEnable);
	m_CE_DBac4.EnableWindow(blEnable);


	m_CB_DWro1List.EnableWindow(blEnable);
	m_CB_DPstdList.EnableWindow(blEnable);

	m_CB_DStodCalc.EnableWindow(blEnable);
	m_CB_DCinsList1.EnableWindow(blEnable);
	m_CB_DCinsList2.EnableWindow(blEnable);
	m_CB_DCinsList3.EnableWindow(blEnable);
	m_CB_DCinsList4.EnableWindow(blEnable);

	m_CB_DCicRem1.EnableWindow(blEnable);
	m_CB_DCicRem2.EnableWindow(blEnable);
	m_CB_DCicRem3.EnableWindow(blEnable);
	m_CB_DCicRem4.EnableWindow(blEnable);

	m_CB_Load_View_Dep.EnableWindow(blEnable);
	
	m_CE_DOrg3.EnableWindow(blEnable);
	m_CE_DFltn.EnableWindow(blEnable);
	m_CE_DFlti.EnableWindow(blEnable);
	m_CE_DW1ea.EnableWindow(blEnable);
	m_CE_DW1ba.EnableWindow(blEnable);
	m_CS_Abflug.EnableWindow(blEnable);
	m_CB_DAlc3List3.EnableWindow(blEnable);
	m_CB_DAlc3List2.EnableWindow(blEnable);
	m_CE_DWro1.EnableWindow(blEnable);
	m_CE_DTwr1.EnableWindow(blEnable);
	m_CB_DTtypList.EnableWindow(blEnable);
	m_CE_DTtyp.EnableWindow(blEnable);
	m_CE_DTgd2.EnableWindow(blEnable);
	m_CE_DTgd1.EnableWindow(blEnable);
	m_CB_DTelex.EnableWindow(blEnable);
	m_CB_DStypList.EnableWindow(blEnable);
	m_CE_DStyp.EnableWindow(blEnable);
	m_CE_DStod.EnableWindow(blEnable);
	m_CE_DStoa.EnableWindow(blEnable);
	m_CE_DStev.EnableWindow(blEnable);
	m_CE_DSlot.EnableWindow(blEnable);
	m_CB_DShowJfno.EnableWindow(blEnable);
	m_CE_DRwyd.EnableWindow(blEnable);
	m_CE_DPstd.EnableWindow(blEnable);
	m_CE_DBbfa.EnableWindow(blEnable);
	m_CE_DOfbl.EnableWindow(blEnable);
	m_CE_DNxti.EnableWindow(blEnable);
	m_CE_DLastChange.EnableWindow(blEnable);
	m_CE_DLastChangeTime.EnableWindow(blEnable);
	m_CE_DIskd.EnableWindow(blEnable);
	m_CE_DIfrd.EnableWindow(blEnable);
//	m_CB_DHtypList.EnableWindow(blEnable);
//	m_CE_DHtyp.EnableWindow(blEnable);
	m_CE_DGtd2.EnableWindow(blEnable);
	m_CE_DGtd1.EnableWindow(blEnable);
	m_CB_DGta1List.EnableWindow(blEnable);
	m_CB_DGta2List.EnableWindow(blEnable);
	m_CE_DGd2y.EnableWindow(blEnable);
	m_CE_DGd1y.EnableWindow(blEnable);
	m_CE_DGd1x.EnableWindow(blEnable);
	m_CE_DFlns.EnableWindow(blEnable);
	m_CE_DEtod.EnableWindow(blEnable);
	m_CE_DEtdi.EnableWindow(blEnable);
	m_CE_DEtdc.EnableWindow(blEnable);
	m_CE_DDtd2.EnableWindow(blEnable);
	m_CE_DDtd1.EnableWindow(blEnable);
	m_CB_DDes3List.EnableWindow(blEnable);
	m_CE_DDes3.EnableWindow(blEnable);
	m_CE_DDes4.EnableWindow(blEnable);
	m_CB_DDcd2List.EnableWindow(blEnable);
	m_CE_DDcd2.EnableWindow(blEnable);
	m_CB_DDcd1List.EnableWindow(blEnable);
	m_CE_DDcd1.EnableWindow(blEnable);
	m_CB_DCxx.EnableWindow(blEnable);
	m_CB_DAlc3List.EnableWindow(blEnable);
	m_CE_DAlc3.EnableWindow(blEnable);
	m_CE_DAirb.EnableWindow(blEnable);
	m_CE_DGd2x.EnableWindow(blEnable);
	m_CE_DCsgn.EnableWindow(blEnable);
	m_CB_DDiverted.EnableWindow(blEnable);
	m_CB_DRerouted.EnableWindow(blEnable);


	m_CE_DPdea.ShowWindow(SW_HIDE);
	m_CE_DPdba.ShowWindow(SW_HIDE);


	m_CE_DJfnoBorder.EnableWindow(FALSE);
	m_CE_DCinsBorder.EnableWindow(FALSE);



	if(omDJfno.GetSize() > 0)
		OnDshowjfno();
	else
		DShowJfnoButton();


	m_CB_DShowJfno.EnableWindow(blEnable);


	if(prmDFlight->Airb == TIMENULL || prmDFlight->Ofbl == TIMENULL)
	{
		m_CB_DRetFlight.EnableWindow(FALSE);
	}
	else
	{
		m_CB_DRetFlight.EnableWindow(blEnable);
	}


	if(prmDFlight->Ofbl == TIMENULL)
	{
		m_CB_DRetTaxi.EnableWindow(FALSE);
	}
	else
	{
		m_CB_DRetTaxi.EnableWindow(blEnable);
		if(prmDFlight->Airb != TIMENULL && CString(prmDFlight->Ftyp) != "B")
			m_CB_DRetTaxi.EnableWindow(FALSE);

		if(prmDFlight->Airb != TIMENULL && CString(prmDFlight->Ftyp) == "B")
			m_CB_DRetFlight.EnableWindow(FALSE);
	}



	if( CString(prmDFlight->Ftyp)	== "Z" )
	{
		m_CE_DAirb.EnableWindow(FALSE);	
		m_CE_DOfbl.EnableWindow(FALSE);	
	}


	if( CString(prmDFlight->Ftyp)	== "B" )
	{
		m_CE_DAirb.ShowWindow(SW_HIDE);	
		m_CE_DOfbl.EnableWindow(FALSE);	
	}
	else
	{
		m_CE_DAirb.ShowWindow(SW_SHOW);	
	}


	if(( CString(prmDFlight->Ftyp)	== "Z" ) || (CString(prmDFlight->Ftyp)	== "B" ))
	{
		m_CE_DDes3.EnableWindow(FALSE);	
		m_CE_DDes4.EnableWindow(FALSE);	
		m_CB_DDes3List.EnableWindow(FALSE);	
	}
	else
	{
		m_CE_DDes3.EnableWindow(blEnable);	
		m_CE_DDes4.EnableWindow(blEnable);	
		m_CB_DDes3List.EnableWindow(blEnable);	
	}


}

void RotationDlg::SetWndPos()
{
	CRect olRect;

	GetWindowRect(&olRect);

	int ilWidth = olRect.right -olRect.left;
	int ilWhichMonitor = ogCfgData.GetMonitorForWindow(CString(MON_DAILYROTDLG_STRING));
	int ilMonitors = ogCfgData.GetMonitorForWindow(CString(MON_COUNT_STRING));
	int left, top, right, bottom;
	int ilCXMonitor;

	bottom = ::GetSystemMetrics(SM_CYSCREEN);

	top = ( (bottom - (olRect.bottom -olRect.top ) ) / 2);  


	if(ilWhichMonitor == 1)
	{
		switch(ilMonitors)
		{
		case 1:
			ilCXMonitor = ::GetSystemMetrics(SM_CXSCREEN);
			left = (int)((ilCXMonitor-ilWidth)/2);
			right = left + ilWidth;//GetSystemMetrics(SM_CXSCREEN);
			break;
		case 2:
			ilCXMonitor = (int)(::GetSystemMetrics(SM_CXSCREEN)/2);
			left = (int)((ilCXMonitor-ilWidth)/2);
			right = left + ilWidth;//GetSystemMetrics(SM_CXSCREEN);
			break;
		case 3:
			ilCXMonitor = (int)(::GetSystemMetrics(SM_CXSCREEN)/3);
			left = (int)((ilCXMonitor-ilWidth)/2);
			right = left + ilWidth;//GetSystemMetrics(SM_CXSCREEN);
			break;
		default:
			ilCXMonitor = ::GetSystemMetrics(SM_CXSCREEN);
			left = (int)((ilCXMonitor-ilWidth)/2);
			right = left + ilWidth;//GetSystemMetrics(SM_CXSCREEN);
			break;
		}
	}
	else if(ilWhichMonitor == 2)
	{
		switch(ilMonitors)
		{
		case 1:
			ilCXMonitor = ::GetSystemMetrics(SM_CXSCREEN);
			left = (int)((ilCXMonitor-ilWidth)/2);
			right = left + ilWidth;//GetSystemMetrics(SM_CXSCREEN);
			break;
		case 2:
			ilCXMonitor = (int)(::GetSystemMetrics(SM_CXSCREEN)/2);
			left = (int)((ilCXMonitor-ilWidth)/2)+(ilCXMonitor);
			right = left + ilWidth;//GetSystemMetrics(SM_CXSCREEN);
			break;
		case 3:
			ilCXMonitor = (int)(::GetSystemMetrics(SM_CXSCREEN)/3);
			left = (int)((ilCXMonitor-ilWidth)/2)+(ilCXMonitor);
			right = left + ilWidth;//GetSystemMetrics(SM_CXSCREEN);
			break;
		default:
			right = GetSystemMetrics(SM_CXSCREEN);
			break;
		}
	}
	else if(ilWhichMonitor == 3)
	{
		switch(ilMonitors)
		{
		case 1:
			ilCXMonitor = ::GetSystemMetrics(SM_CXSCREEN);
			left = (int)((ilCXMonitor-ilWidth)/2);
			right = left + ilWidth;//GetSystemMetrics(SM_CXSCREEN);
			break;
		case 2:
			ilCXMonitor = (int)(::GetSystemMetrics(SM_CXSCREEN)/2);
			left = (int)((ilCXMonitor-ilWidth)/2);
			right = left + ilWidth;//GetSystemMetrics(SM_CXSCREEN);
			break;
		case 3:
			ilCXMonitor = (int)(::GetSystemMetrics(SM_CXSCREEN)/3);
			left = (int)((ilCXMonitor-ilWidth)/2)+(2*ilCXMonitor);
			right = left + ilWidth;//GetSystemMetrics(SM_CXSCREEN);
			break;
		default:
			right = GetSystemMetrics(SM_CXSCREEN);
			break;
		}
	}


//	ShowWindow(SW_SHOW); 

	SetWindowPos(&wndTop, 0,0,0,0, SWP_SHOWWINDOW | SWP_NOSIZE | SWP_NOMOVE);



}



void RotationDlg::OnAgent() 
{
	if(prmAFlight->Urno > 0 || prmDFlight->Urno > 0)
	{
		//alc3 der Fluggesellschaften
		CString opAlc3StrArr ("");
		CString opAlc3StrDep ("");

		if(prmAFlight != NULL)
			opAlc3StrArr.Format("%s", prmAFlight->Alc3);
		if(prmDFlight != NULL)
			opAlc3StrDep.Format("%s", prmDFlight->Alc3);

		if (opAlc3StrArr.GetLength() == 0 && opAlc3StrDep.GetLength() == 0)
			return;

		RotationHAIDlg polRotationHAIDlg(this, opAlc3StrArr, opAlc3StrDep);
		if (polRotationHAIDlg.DoModal() == IDOK)
		{
		}
	}
}

void RotationDlg::OnObject() 
{
	if(prmAFlight->Urno >= 0)
		ogDdx.DataChanged((void *)this, SHOW_FLIGHT, (void*)&(prmAFlight->Urno));
	else
		ogDdx.DataChanged((void *)this, SHOW_FLIGHT, (void*)&(prmDFlight->Urno));

}


void RotationDlg::OnStatus() 
{
/*
	CString olText;
	ROTATIONDLGFLIGHTDATA *prlFlight;	
	int ilCount = ogRotationDlgFlights.omData.GetSize();
	char buffer[256];


	for(int i = 0; i < ilCount; i++)
	{
		prlFlight = &ogRotationDlgFlights.omData[i];

		sprintf(buffer, "URNO:<%ld>  ADID:<%s>  TIFD:<%s>  STOD:<%s>  TIFA:<%s>  STOA:<%s>", prlFlight->Urno, prlFlight->Adid, prlFlight->Tifd.Format("%H:%M"), prlFlight->Stod.Format("%H:%M"), prlFlight->Tifa.Format("%H:%M"), prlFlight->Stoa.Format("%H:%M"));    

		olText += CString("\n\n") + CString(buffer);

	}

	MessageBox(olText, "");

*/
	CString olSelection;
	CString olText;

	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));

	if((prmAFlight->Urno != 0) && (prmDFlight->Urno != 0))
	{
		olSelection.Format("WHERE FKEY = %ld OR FKEY = %ld", prmAFlight->Urno, prmDFlight->Urno);
	}
	else
	{
		if(prmAFlight->Urno != 0)
		{
			olSelection.Format("WHERE FKEY = %ld", prmAFlight->Urno);
		}
		if(prmDFlight->Urno != 0)
		{
			olSelection.Format("WHERE FKEY = %ld", prmDFlight->Urno);
		}
	}


	ogBCD.Read("GHD", olSelection);


	if(ogBCD.GetDataCount("GHD") > 0)
	{

		for(int i = ogBCD.GetDataCount("GHD") - 1; i >= 0; i--)
		{

			olText += ogBCD.GetField("GHS", "URNO", ogBCD.GetField("GHD", i, "GHSU"), "LKNM") + CString("    ");


			if( ogBCD.GetField("GHD", i, "EURN") == "0")
			{
				olText += GetString(IDS_STRING1437);
			}
			else
			{
				CString olDust = ogBCD.GetField("GHD", i, "DUST");
				if(olDust == "1")
				{
					olText += GetString(IDS_STRING1438);
				}
				if(olDust == "2")
				{
					olText += GetString(IDS_STRING1439);
				}
				if(olDust == "3")
				{
					olText += GetString(IDS_STRING1440);
				}

			}
			olText += CString("\n");
		}
	}
	else
	{
			olText += GetString(IDS_STRING1441);
	}

	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));

	MessageBox(olText, GetString(IDS_STRING1436));


}






void RotationDlg::OnTowing() 
{
/*
	if(pogChangeRegistration == NULL)
	{
		pogChangeRegistration = new CChangeRegistraion(this);
		pogChangeRegistration->Create(IDD_CHANGEREGISTRATION);
	}
	pogChangeRegistration->SetWindowPos(&wndTop, 0,0,0,0, SWP_SHOWWINDOW | SWP_NOSIZE | SWP_NOMOVE );
*/
	if(pogRotGroundDlg == NULL)
	{
		pogRotGroundDlg = new RotGroundDlg(this);
		pogRotGroundDlg->Create(IDD_ROTGROUND);
	}

	if (CheckPostFlight() || bmRotationReadOnly)
		pogRotGroundDlg->setStatus(RotGroundDlg::Status::POSTFLIGHT);
	else
		pogRotGroundDlg->setStatus(RotGroundDlg::Status::UNKNOWN);
	pogRotGroundDlg->SetWindowPos(&wndTop, 0,0,0,0, SWP_SHOWWINDOW | SWP_NOSIZE | SWP_NOMOVE );

	if(prmAFlight->Urno > 0)
	{
		pogRotGroundDlg->NewData(this, prmAFlight->Rkey, prmAFlight->Urno, 'A', bmLocalTime, false, !bmRotationReadOnly);
	}
	else
	{
		pogRotGroundDlg->NewData(this, prmDFlight->Rkey, prmDFlight->Urno, 'D', bmLocalTime, false, !bmRotationReadOnly);
	}

}



void RotationDlg::OnVip() 
{
	if(prmAFlight->Urno > 0 || prmDFlight->Urno > 0)
	{
		RotationVipDlg polRotationVipDlg(this, prmAFlight, prmDFlight);
		if (CheckPostFlight())
			polRotationVipDlg.setStatus(RotationVipDlg::Status::POSTFLIGHT);
		else
			polRotationVipDlg.setStatus(RotationVipDlg::Status::UNKNOWN);

		if (polRotationVipDlg.DoModal() == IDOK)
		{
		}
	}


	CString olText;

	olText.Format("%s (%d)", GetString(IDS_STRING1556), ogBCD.GetDataCount("VIP"), 10);

	m_CB_Vip.SetWindowText(olText);




/*

	int ilCount = ogBCD.GetDataCount("VIP");

	CString olText;	


	for(int i = 0; i < ilCount; i++)
	{
		
		if( prmAFlight->Urno == atoi( ogBCD.GetField("VIP", i, "FLNU") ))
		{	
			olText += GetString(IDS_STRING330) + CString("\n\n");
		}
		else
		{	
			olText += GetString(IDS_STRING338) + CString("\n\n");
		}


		olText += GetString(IDS_STRING1693) + ogBCD.GetField("VIP", i, "NOGR") + CString("\n");
		olText += GetString(IDS_STRING1696) + ogBCD.GetField("VIP", i, "GRID") + CString("\n");
		olText += GetString(IDS_STRING1694) + ogBCD.GetField("VIP", i, "PAXN") + CString("\n");
		olText += GetString(IDS_STRING1695) + ogBCD.GetField("VIP", i, "PAXR") + CString("\n");
		olText += GetString(IDS_STRING1681) + ogBCD.GetField("VIP", i, "NOPX") + CString("\n");

		olText += CString("\n");

		if(i < (ilCount - 1))		
			olText += CString("\n");

	}

	MessageBox(olText, GetString(IDS_STRING1556), MB_OK);

*/
}



void RotationDlg::OnSeasonMask() 
{
	if (bmRotationReadOnly)
		pogSeasonDlg->NewData(pomParent, lmRkey, lmCalledBy, DLG_CHANGE_DIADATA, bmLocalTime, omAdid);
	else
		pogSeasonDlg->NewData(pomParent, lmRkey, lmCalledBy, DLG_CHANGE, bmLocalTime, omAdid);
}



void RotationDlg::OnAawi() 
{
	
	CString olApc;
	m_CE_AOrg3.GetWindowText(olApc);

	ShowAwi( olApc); 
}

void RotationDlg::OnDawi() 
{
	CString olApc;
	m_CE_DDes3.GetWindowText(olApc);

	ShowAwi( olApc); 

}


void RotationDlg::ShowAwi( CString olApc) 
{
	
	CString olText;
	CString olWhere;

	CString olApc3;
	CString olApc4;

	ogBCD.GetField("APT", "APC3", "APC4", olApc, olApc3, olApc4);


	if( !olApc3.IsEmpty() || !olApc4.IsEmpty())
	{

		olWhere.Format("WHERE APC3 = '%s' AND APC4 = '%s' AND VPFR < '%s' AND ( VPTO > '%s' OR VPTO = ' ')", olApc3, olApc4, CTime::GetCurrentTime().Format("%Y%m%d%H%M00"), CTime::GetCurrentTime().Format("%Y%m%d%H%M00"));

		ogBCD.Read("AWI", olWhere);

		if( ogBCD.GetDataCount("AWI") == 0)
		{
			MessageBox( GetString(IDS_STRING1682), GetString(ST_HINWEIS), MB_OK );			
		}
		else
		{

			olText = olApc3 + CString("  ") + olApc4 + CString("  "); 
			olText += ogBCD.GetField("APT", "APC4", olApc4, CString("APFN"));
			olText += CString("\n\n"); 

			olText += GetString(IDS_STRING1683) + ogBCD.GetField("AWI", 0, "WCOD") + CString("\n");
			
			olText += GetString(IDS_STRING1684) + ogBCD.GetField("AWI", 0, "TEMP") + CString("\n");
			olText += GetString(IDS_STRING1685) + ogBCD.GetField("AWI", 0, "HUMI") + CString("\n");
			olText += GetString(IDS_STRING1686) + ogBCD.GetField("AWI", 0, "WIND") + CString("\n");
			olText += GetString(IDS_STRING1687) + ogBCD.GetField("AWI", 0, "WDIR") + CString("\n");
			olText += GetString(IDS_STRING1688) + ogBCD.GetField("AWI", 0, "VISI") + CString("\n");
			olText += GetString(IDS_STRING1689) + CString("\n") + ogBCD.GetField("AWI", 0, "MSGT") + CString("\n");

			MessageBox( olText, GetString(IDS_STRING1690), MB_OK );			
		
		}
	}
}

void RotationDlg::OnGoAround() 
{
	OnEditChanged(0,0);
}

void RotationDlg::OnAcxx() 
{
	m_CB_ARetFlight.SetCheck(FALSE);
	m_CB_ARetTaxi.SetCheck(FALSE);
	m_CB_ADiverted.SetCheck(FALSE);
	m_CB_ARerouted.SetCheck(FALSE);
	m_CE_ADivr.EnableWindow(FALSE);
	OnEditChanged(0,0);
}

void RotationDlg::OnADiverted() 
{
	m_CB_ARetFlight.SetCheck(FALSE);
	m_CB_ARetTaxi.SetCheck(FALSE);
	m_CB_ACxx.SetCheck(FALSE);
	m_CB_ARerouted.SetCheck(FALSE);

	if(m_CB_ADiverted.GetCheck() == TRUE)
		m_CE_ADivr.EnableWindow(TRUE);
	else
		m_CE_ADivr.EnableWindow(FALSE);


	OnEditChanged(0,0);	
}

void RotationDlg::OnARerouted() 
{
	m_CB_ARetFlight.SetCheck(FALSE);
	m_CB_ARetTaxi.SetCheck(FALSE);
	m_CB_ADiverted.SetCheck(FALSE);
	m_CB_ACxx.SetCheck(FALSE);
	m_CE_ADivr.EnableWindow(FALSE);
	OnEditChanged(0,0);	
}

void RotationDlg::OnAretflight() 
{
	m_CB_ADiverted.SetCheck(FALSE);
	m_CB_ARerouted.SetCheck(FALSE);
	m_CB_ACxx.SetCheck(FALSE);
	m_CB_ARetTaxi.SetCheck(FALSE);
	m_CE_ADivr.EnableWindow(FALSE);
	OnEditChanged(0,0);
}

void RotationDlg::OnArettaxi() 
{
	m_CB_ADiverted.SetCheck(FALSE);
	m_CB_ARerouted.SetCheck(FALSE);
	m_CB_ACxx.SetCheck(FALSE);
	m_CB_ARetFlight.SetCheck(FALSE);
	m_CE_ADivr.EnableWindow(FALSE);

	OnEditChanged(0,0);
}




void RotationDlg::OnDcxx() 
{
	m_CB_DRetFlight.SetCheck(FALSE);
	m_CB_DRetTaxi.SetCheck(FALSE);
	m_CB_DDiverted.SetCheck(FALSE);
	m_CB_DRerouted.SetCheck(FALSE);
	m_CE_DDivr.EnableWindow(FALSE);
	OnEditChanged(0,0);
}


void RotationDlg::OnDDiverted() 
{
	m_CB_DRetFlight.SetCheck(FALSE);
	m_CB_DRetTaxi.SetCheck(FALSE);
	m_CB_DCxx.SetCheck(FALSE);
	m_CB_DRerouted.SetCheck(FALSE);

	if(m_CB_DDiverted.GetCheck() == TRUE)
		m_CE_DDivr.EnableWindow(TRUE);
	else
		m_CE_DDivr.EnableWindow(FALSE);

	OnEditChanged(0,0);	
}


void RotationDlg::OnDRerouted() 
{
	m_CB_DRetFlight.SetCheck(FALSE);
	m_CB_DRetTaxi.SetCheck(FALSE);
	m_CB_DDiverted.SetCheck(FALSE);
	m_CB_DCxx.SetCheck(FALSE);
	m_CE_DDivr.EnableWindow(FALSE);
	OnEditChanged(0,0);	
}





void RotationDlg::OnDretflight() 
{
	m_CB_DCxx.SetCheck(FALSE);
	m_CB_DRetTaxi.SetCheck(FALSE);
	m_CB_DDiverted.SetCheck(FALSE);
	m_CB_DRerouted.SetCheck(FALSE);
	m_CE_DDivr.EnableWindow(FALSE);
	OnEditChanged(0,0);
}

void RotationDlg::OnDrettaxi() 
{
	m_CB_DRetFlight.SetCheck(FALSE);
	m_CB_DCxx.SetCheck(FALSE);
	m_CB_DDiverted.SetCheck(FALSE);
	m_CB_DRerouted.SetCheck(FALSE);
	m_CE_DDivr.EnableWindow(FALSE);
	OnEditChanged(0,0);
}

void RotationDlg::OnDcicrem1() 
{

	int ilLine = pomDCinsTable->pomListBox->GetTopIndex();

	CString olText;
	pomDCinsTable->GetTextFieldValue(ilLine, 6, olText);
	
	
	AwBasicDataDlg *polDlg = new AwBasicDataDlg(this, "FID_CIC","CODE,"+ogFIDRemarkField, "CODE+", olText);
	polDlg->SetSecState("ROTATIONDLG_CE_DCins");
	if(polDlg->DoModal() == IDOK)
	{
		pomDCinsTable->SetIPValue(ilLine, 6, polDlg->GetField("CODE"));
	}
	delete polDlg;
	
}

void RotationDlg::OnDcicrem2() 
{

	int ilLine = pomDCinsTable->pomListBox->GetTopIndex()+1;

	CString olText;
	pomDCinsTable->GetTextFieldValue(ilLine, 6, olText);
	
	
	AwBasicDataDlg *polDlg = new AwBasicDataDlg(this, "FID_CIC","CODE,"+ogFIDRemarkField, "CODE+", olText);
	polDlg->SetSecState("ROTATIONDLG_CE_DCins");
	if(polDlg->DoModal() == IDOK)
	{
		pomDCinsTable->SetIPValue(ilLine, 6, polDlg->GetField("CODE"));
	}
	delete polDlg;
	
}

void RotationDlg::OnDcicrem3() 
{
	int ilLine = pomDCinsTable->pomListBox->GetTopIndex()+2;

	CString olText;

	pomDCinsTable->GetTextFieldValue(ilLine, 6, olText);
	
	AwBasicDataDlg *polDlg = new AwBasicDataDlg(this, "FID_CIC","CODE,"+ogFIDRemarkField, "CODE+", olText);
	polDlg->SetSecState("ROTATIONDLG_CE_DCins");
	if(polDlg->DoModal() == IDOK)
	{
		pomDCinsTable->SetIPValue(ilLine, 6, polDlg->GetField("CODE"));
	}
	delete polDlg;
}

void RotationDlg::OnDcicrem4() 
{
	int ilLine = pomDCinsTable->pomListBox->GetTopIndex()+3;

	CString olText;
	pomDCinsTable->GetTextFieldValue(ilLine, 6, olText);
	
	AwBasicDataDlg *polDlg = new AwBasicDataDlg(this, "FID_CIC","CODE,"+ogFIDRemarkField, "CODE+", olText);
	polDlg->SetSecState("ROTATIONDLG_CE_DCins");
	if(polDlg->DoModal() == IDOK)
	{
		pomDCinsTable->SetIPValue(ilLine, 6, polDlg->GetField("CODE"));
	}
	delete polDlg;
	
}

void RotationDlg::OnAgpu() 
{
	RotationGpuDlg olDlg(this, prmAFlight);
	olDlg.DoModal();
	
}

void RotationDlg::OnDgpu() 
{
	RotationGpuDlg olDlg(this, prmDFlight);
	olDlg.DoModal();

}

void RotationDlg::OnAload() 
{
	if (strcmp(pcgHome, "ATH") == 0)
	{
		// Athen hat eigenen dialog
		RotationLoadDlgATH olDlg(this, prmAFlight);
		olDlg.DoModal();
	}
	else
	{
		RotationLoadDlg olDlg(this, prmAFlight);
		olDlg.DoModal();
	}

}

void RotationDlg::OnDload() 
{
	if (strcmp(pcgHome, "ATH") == 0)
	{
		// Athen hat eigenen dialog
		RotationLoadDlgATH olDlg(this, prmDFlight);
		olDlg.DoModal();
	}
	else
	{
		RotationLoadDlg olDlg(this, prmDFlight);
		olDlg.DoModal();
	}
}


void RotationDlg::OnEditRButtonDown(UINT wParam, LPARAM lParam) 
{
//	OnEditDbClk(wParam,lParam);
//	return;
	// Insert current time in baggage-belt begin- and end-editbox
 	CCSEDITNOTIFY *prlNotify = (CCSEDITNOTIFY *) lParam;
 	CTime olTime = CTime::GetCurrentTime();
 	if (!bmLocalTime)
	{
		ogBasicData.LocalToUtc(olTime);
	}

	if (prlNotify->SourceControl == &m_CE_AB1ba)
	{
 		m_CE_AB1ba.SetWindowText(DateToHourDivString(olTime, olTime));
	}
	if (prlNotify->SourceControl == &m_CE_AB1ea)
	{
 		m_CE_AB1ea.SetWindowText(DateToHourDivString(olTime, olTime));
	}
	if (prlNotify->SourceControl == &m_CE_AB2ba)
	{
 		m_CE_AB2ba.SetWindowText(DateToHourDivString(olTime, olTime));
	}
	if (prlNotify->SourceControl == &m_CE_AB2ea)
	{
 		m_CE_AB2ea.SetWindowText(DateToHourDivString(olTime, olTime));
	}

	
}



void RotationDlg::UpdateCashButton(const CString &ropAlc3, char cpPaid) 
{
	CString olCash(""); 

	if (bmRotationReadOnly)
		return;


	ogBCD.GetField("ALT", "ALC3", ropAlc3, "CASH", olCash);

	if (olCash == "K")
		m_CB_Paid.ShowWindow(SW_HIDE);
	else
	{
		if(cpPaid == 'B')
		{
			m_CB_Paid.SetWindowText(GetString(IDS_STRING1249));
			m_CB_Paid.SetColors(RGB(255, 0, 0), ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
		}
		else
		{
			m_CB_Paid.SetWindowText(GetString(IDS_STRING1248));
			m_CB_Paid.SetColors(::GetSysColor(COLOR_BTNFACE), ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
		}

		SetWndStatAll(ogPrivList.GetStat("ROTATIONDLG_CB_Paid"),m_CB_Paid);	
		//is it a postflight?
		HandlePostFlight();
	}
} 

BOOL RotationDlg::HandlePostFlight()
{
	// postflight: no changes allowed
	if (CheckPostFlight())
	{
		if (GetDlgItem(IDC_JOIN)) 
			m_CB_Join.EnableWindow(FALSE);

		if (GetDlgItem(IDC_SPLIT)) 
			m_CB_Split.EnableWindow(FALSE);

		if (GetDlgItem(IDC_PAID)) 
			m_CB_Paid.EnableWindow(FALSE);

		if (GetDlgItem(IDOK)) 
			m_CB_Ok.EnableWindow(FALSE);
		
		if (polRotationAViaDlg)
			polRotationAViaDlg->Enable(false);

		if (polRotationDViaDlg)
			polRotationDViaDlg->Enable(false);

		return TRUE;
	}

	return FALSE;
}

BOOL RotationDlg::CheckPostFlight()
{
	BOOL blPost = FALSE;

	if (prmAFlight && prmDFlight)
	{
		if (prmAFlight->Tifa != TIMENULL && prmDFlight->Tifd != TIMENULL)
		{
			if (IsPostFlight(prmAFlight->Tifa) && IsPostFlight(prmDFlight->Tifd))
				blPost = TRUE;
		}
		else if (prmDFlight->Tifd != TIMENULL)
		{
			if (IsPostFlight(prmDFlight->Tifd))
				blPost = TRUE;
		}
		else if (prmAFlight->Tifa != TIMENULL)
		{
			if (IsPostFlight(prmAFlight->Tifa))
				blPost = TRUE;
		}
	}
	else if (!prmAFlight && prmDFlight)
	{
		if (prmDFlight->Tifd != TIMENULL)
		{
			if (IsPostFlight(prmDFlight->Tifd))
				blPost = TRUE;
		}
	}
	else if (prmAFlight && !prmDFlight)
	{
		if (prmAFlight->Tifa != TIMENULL)
		{
			if (IsPostFlight(prmAFlight->Tifa))
				blPost = TRUE;
		}
	}

	if (blPost)
		ModifyWindowText(this, GetString(IDS_STRINGWND_POSTFLIGHT), FALSE);
	else
		ModifyWindowText(this, GetString(IDS_STRINGWND_POSTFLIGHT), TRUE);

	return blPost;
}


bool RotationDlg::AddBltCheck()
{
	if (!bgExtBltCheck)
		return true;

	// only if arrival flight present
	if (prmAFlight->Urno < 1)
		return true;


	//// check the belt type of the two baggage belts
	if (strlen(prmAFlight->Blt1) > 0 && strlen(prmAFlight->Blt2) > 0)
	{
		CString olBlt1Type;
		CString olBlt2Type;

		// get both belt types
		ogBCD.GetField("BLT", "BNAM", prmAFlight->Blt1, "BLTT", olBlt1Type);
		ogBCD.GetField("BLT", "BNAM", prmAFlight->Blt2, "BLTT", olBlt2Type);

		if (!olBlt1Type.IsEmpty() && !olBlt2Type.IsEmpty() &&
			olBlt1Type != olBlt2Type)
		{
			CFPMSApp::MyTopmostMessageBox(this, GetString(IDS_STRING2045) + CString("\n\n") + GetString(IDS_STRING2046), GetString(ST_FEHLER), MB_ICONERROR | MB_OK);
			return false;
		}
	}


	// only proceed if the baggage belt allocation values or STA, ETA has been changed!
	if (!m_CE_ABlt1.IsChanged() && !m_CE_ATmb1.IsChanged() && !m_CE_AB1ba.IsChanged() && !m_CE_AB1ea.IsChanged() &&
		!m_CE_ABlt2.IsChanged() && !m_CE_ATmb2.IsChanged() && !m_CE_AB2ba.IsChanged() && !m_CE_AB2ea.IsChanged() &&
		!m_CE_AStoa.IsChanged() && !m_CE_AEtai.IsChanged() && !m_CE_ATmoa.IsChanged())
		return true;


	CString olWarningText;
	bool blWarning = false;
	CString olAddText;

	//// check opening time of belt 1
	if (!prmAFlight->CheckBltOpeningTime(1, olAddText))
	{
		CString olTmp;
		olTmp.Format(GetString(IDS_STRING2033), olAddText);

		olWarningText += olTmp;
		olWarningText += '\n';
		blWarning = true;
	}
	//// check closing time of belt 1
	if (!prmAFlight->CheckBltClosingTime(1, olAddText))
	{
		CString olTmp;
		olTmp.Format(GetString(IDS_STRING2035), olAddText);

		olWarningText += olTmp;
		olWarningText += '\n';
		blWarning = true;
	}
	//// check opening time of belt 2
	if (!prmAFlight->CheckBltOpeningTime(2, olAddText))
	{
		CString olTmp;
		olTmp.Format(GetString(IDS_STRING2034), olAddText);

		olWarningText += olTmp;
 		olWarningText += '\n';
		blWarning = true;
	}
	//// check closing time of belt 2
	if (!prmAFlight->CheckBltClosingTime(2, olAddText))
	{
		CString olTmp;
		olTmp.Format(GetString(IDS_STRING2036), olAddText);

		olWarningText += olTmp;
		olWarningText += '\n';
		blWarning = true;
	}
	//// check if two belts are allocated
	if (strlen(prmAFlight->Blt1) > 0 && strlen(prmAFlight->Blt2) > 0)
	{
		olWarningText += GetString(IDS_STRING2043);
		olWarningText += '\n';
		blWarning = true;
	}

	// Display message box with warnings
	if (blWarning)
	{
		olWarningText += CString("\n") + GetString(IDS_STRING2037);
		if (CFPMSApp::MyTopmostMessageBox(this, olWarningText, GetString(IDS_WARNING), MB_ICONWARNING | MB_YESNO) == IDYES)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	return true;
}

void RotationDlg::OnAVia() 
{
	if(prmAFlight->Urno > 0)
	{
		if (!polRotationAViaDlg)
		{
			bool blEnable = true;
			if(ogPrivList.GetStat("ROTATIONDLG_CB_AShowVia") != '1')
				blEnable = false;

			polRotationAViaDlg = new RotationViaDlg(this, "A", CString(prmAFlight->Flno), prmAFlight->Urno, prmAFlight->Vial, prmAFlight->Stoa, bmLocalTime, blEnable);
			polRotationAViaDlg->Create(RotationViaDlg::IDD);
			polRotationAViaDlg->SetSecState(ogPrivList.GetStat("ROTATIONDLG_CB_AShowVia"));
		}

		CPoint point;
		::GetCursorPos(&point);
		polRotationAViaDlg->SetWindowPos(&wndTop, point.x, point.y,0,0, SWP_SHOWWINDOW | SWP_NOSIZE);// | SWP_NOMOVE );

	}
}

void RotationDlg::OnDVia() 
{
	if(prmDFlight->Urno > 0)
	{
		if (!polRotationDViaDlg)
		{
			bool blEnable = true;
			if(ogPrivList.GetStat("ROTATIONDLG_CB_DShowVia") != '1')
				blEnable = false;

			polRotationDViaDlg = new RotationViaDlg(this, "D", CString(prmDFlight->Flno), prmDFlight->Urno, prmDFlight->Vial, prmDFlight->Stod, bmLocalTime, blEnable);
			polRotationDViaDlg->Create(RotationViaDlg::IDD);
			polRotationDViaDlg->SetSecState(ogPrivList.GetStat("ROTATIONDLG_CB_DShowVia"));
		}

		CPoint point;
		::GetCursorPos(&point);
		polRotationDViaDlg->SetWindowPos(&wndTop, point.x, point.y,0,0, SWP_SHOWWINDOW | SWP_NOSIZE);// | SWP_NOMOVE );

	}
}


int RotationDlg::OnToolHitTest(CPoint opPoint,TOOLINFO *pTI) const
{
	static int ilLastToolTip = -1;

	CWnd *olpWnd = NULL;
	int ilItemId = -1;
	CRect olRect;


		olpWnd = (CWnd*) GetDlgItem(IDC_ASHOWJFNO);
		olpWnd->GetWindowRect(olRect);
		this->ScreenToClient(olRect);
		if (olRect.PtInRect(opPoint))
		{
			CString olToolTip = "Code Share Flights: ";
			CString olJfno (prmAFlight->Jfno);

			if (!olJfno.IsEmpty())
			{
				while(!olJfno.IsEmpty())
				{
					if (olJfno.GetLength() < 9)
					{
						int anz = 9 - olJfno.GetLength();
						for (int i=0; i<anz; i++) 
							olJfno.Insert(10000, ' ');
					}
					CString olAlc3 = olJfno.Left(3);
					CString olFltn = olJfno.Mid(3,5);
					CString olFlns = olJfno.Mid(8,1);
					olAlc3.TrimLeft();
					olFlns.TrimLeft();
					olFltn.TrimLeft();
					olJfno = olJfno.Right(olJfno.GetLength() - 9);
					CString olTmp = olAlc3 + olFltn + olFlns + " / ";
					olToolTip += olTmp;
				}
			}

			char *olpChar = new char [256];
			// fill tooltip structure
			sprintf(olpChar, olToolTip);
			pTI->cbSize = sizeof(*pTI);
			pTI->uFlags = TTF_IDISHWND;
			pTI->hwnd   = m_hWnd;
			pTI->uId    = (UINT) olpWnd->m_hWnd;
			pTI->lpszText = olpChar;
			return 1;

//			ilItemId = i;
//			break;
		}

		olpWnd = (CWnd*) GetDlgItem(IDC_DSHOWJFNO);
		olpWnd->GetWindowRect(olRect);
		this->ScreenToClient(olRect);
		if (olRect.PtInRect(opPoint))
		{
			CString olToolTip = "Code Share Flights: ";
			CString olJfno (prmDFlight->Jfno);

			if (!olJfno.IsEmpty())
			{
				while(!olJfno.IsEmpty())
				{
					if (olJfno.GetLength() < 9)
					{
						int anz = 9 - olJfno.GetLength();
						for (int i=0; i<anz; i++) 
							olJfno.Insert(10000, ' ');
					}
					CString olAlc3 = olJfno.Left(3);
					CString olFltn = olJfno.Mid(3,5);
					CString olFlns = olJfno.Mid(8,1);
					olAlc3.TrimLeft();
					olFlns.TrimLeft();
					olFltn.TrimLeft();
					olJfno = olJfno.Right(olJfno.GetLength() - 9);
					CString olTmp = olAlc3 + olFltn + olFlns + " / ";
					olToolTip += olTmp;
				}
			}

			char *olpChar = new char [256];
			// fill tooltip structure
			sprintf(olpChar, olToolTip);
			pTI->cbSize = sizeof(*pTI);
			pTI->uFlags = TTF_IDISHWND;
			pTI->hwnd   = m_hWnd;
			pTI->uId    = (UINT) olpWnd->m_hWnd;
			pTI->lpszText = olpChar;
			return 1;
		}


	// cursor over cellNr
//	for (int i = 0; i < omCells.GetSize(); i++)
//	{
/*		olpWnd = (CButton*) m_CB_AVia;
		if (!olpWnd)
			return -1;
*/
		olpWnd = (CWnd*) GetDlgItem(IDC_AVIA);
		olpWnd->GetWindowRect(olRect);
		this->ScreenToClient(olRect);
		if (olRect.PtInRect(opPoint))
		{
			CString olToolTip = "Via Stations";
			CString olVia;
			if (polRotationAViaDlg)
				olVia = polRotationAViaDlg->GetToolTip();

			if (!olVia.IsEmpty())
				olToolTip = olVia;

			char *olpChar = new char [256];
			// fill tooltip structure
			sprintf(olpChar, olToolTip);
			pTI->cbSize = sizeof(*pTI);
			pTI->uFlags = TTF_IDISHWND;
			pTI->hwnd   = m_hWnd;
			pTI->uId    = (UINT) olpWnd->m_hWnd;
			pTI->lpszText = olpChar;
			return 1;

//			ilItemId = i;
//			break;
		}

		olpWnd = (CWnd*) GetDlgItem(IDC_DVIA);
		olpWnd->GetWindowRect(olRect);
		this->ScreenToClient(olRect);
		if (olRect.PtInRect(opPoint))
		{
			CString olToolTip = "Via Stations";
			CString olVia;
			if (polRotationDViaDlg)
				olVia = polRotationDViaDlg->GetToolTip();

			if (!olVia.IsEmpty())
				olToolTip = olVia;

			char *olpChar = new char [256];
			// fill tooltip structure
			sprintf(olpChar, olToolTip);
			pTI->cbSize = sizeof(*pTI);
			pTI->uFlags = TTF_IDISHWND;
			pTI->hwnd   = m_hWnd;
			pTI->uId    = (UINT) olpWnd->m_hWnd;
			pTI->lpszText = olpChar;
			return 1;

//			ilItemId = i;
//			break;
		}
	return -1;
}

BOOL RotationDlg::OnToolTipNotify( UINT id, NMHDR * pNMHDR, LRESULT * pResult )
{
    TOOLTIPTEXT *pTTT = (TOOLTIPTEXT *)pNMHDR;
    UINT nID =pNMHDR->idFrom;
    if (pTTT->uFlags & TTF_IDISHWND)
    {
        // idFrom is actually the HWND of the tool
        nID = ::GetDlgCtrlID((HWND)nID);
        if(nID)
        {
            pTTT->lpszText = MAKEINTRESOURCE(nID);
            pTTT->hinst = AfxGetResourceHandle();
            return(TRUE);
        }
    }
    return(FALSE);
}

void RotationDlg::CheckViaButton(void)
{
	CString olButtonText;
	COLORREF olButtonColor;

	//arr
	int ilCount = atoi(prmAFlight->Vian);
	if (ilCount > 0)
	{
		olButtonColor = ogColors[19];
		olButtonText.Format(GetString(IDS_VIAN), ilCount);

	}
	else
	{
		olButtonText.Format(GetString(IDS_VIAN), 0);
		olButtonColor = ::GetSysColor(COLOR_BTNFACE);
	}

	m_CB_AVia.SetWindowText(olButtonText);
	m_CB_AVia.SetColors(olButtonColor, ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
	m_CB_AVia.UpdateWindow();
	CRect olRect;
	m_CB_AVia.GetWindowRect( olRect );


	//dep
	ilCount = atoi(prmDFlight->Vian);
	if (ilCount > 0)
	{
		olButtonColor = ogColors[19];
		olButtonText.Format(GetString(IDS_VIAN), ilCount);

	}
	else
	{
		olButtonText.Format(GetString(IDS_VIAN), 0);
		olButtonColor = ::GetSysColor(COLOR_BTNFACE);
	}

	m_CB_DVia.SetWindowText(olButtonText);
	m_CB_DVia.SetColors(olButtonColor, ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
	m_CB_DVia.UpdateWindow();
	
	return; 
}







