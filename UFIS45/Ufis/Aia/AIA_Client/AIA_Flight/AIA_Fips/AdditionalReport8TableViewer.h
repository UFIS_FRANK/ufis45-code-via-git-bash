#ifndef __ADDITIONAL_REPORT_8_VIEWER_H__
#define __ADDITIONAL_REPORT_8_VIEWER_H__

#include "stdafx.h"
#include "RotationDlgCedaFlightData.h"
#include "CCSTable.h"
#include "CViewer.h"
#include "CCSPrint.h"


struct ADDITIONAL_REPORT8_LINEDATA
{
	long AUrno;
	long ARkey;
	CString		AAct;
	CString		AAlc2;
	CString		AAlc3;
	CString		ADate;
	CTime 		AEtai; 	
	CString		AFlno;
	CTime 		ALand; 	
	CTime 		AStoa;
	CString		ATtyp;
	CString		ADcd1;
	CString		ADtd1;

	long DUrno;
	long DRkey;
	CString		DAct;
	CString		DAlc2;
	CString		DAlc3;
	CString		DDate;
	CString		DDes3;
	CString		DFlno;
	CString		DTtyp;
	CString		DDcd1;
	CString		DDtd1;

};

/////////////////////////////////////////////////////////////////////////////
// AdditionalReport8TableViewer

class AdditionalReport8TableViewer : public CViewer
{
// Constructions
public:
    AdditionalReport8TableViewer(CCSPtrArray<ROTATIONDLGFLIGHTDATA> *popData, 
		                                 const CStringArray &opShownColumns,
		                                 char *pcpInfo = NULL, char *pcpSelect = NULL);
    ~AdditionalReport8TableViewer();

    void Attach(CCSTable *popAttachWnd);
	void SetParentDlg(CDialog* ppDlg);
    virtual void ChangeViewTo(const char *pcpViewName);
	void PrintPlanToFile(char *pcpDefPath);
// Internal data processing routines
private:
    void MakeLines();
	int  MakeLine(ROTATIONDLGFLIGHTDATA *prpAFlight, ROTATIONDLGFLIGHTDATA *prpDFlight);
	void MakeLineData(ROTATIONDLGFLIGHTDATA *prpAFlight, ROTATIONDLGFLIGHTDATA *prpDFlight, ADDITIONAL_REPORT8_LINEDATA &rpLine);
	void MakeColList(ADDITIONAL_REPORT8_LINEDATA *prlLine, 
		             CCSPtrArray<TABLE_COLUMN> &olColList, CString opCurrentAirline);
	int  CompareGefundenefluege(ADDITIONAL_REPORT8_LINEDATA *prpGefundenefluege1, ADDITIONAL_REPORT8_LINEDATA *prpGefundenefluege2);
	int  CompareFlight(ADDITIONAL_REPORT8_LINEDATA *prpFlight1, ADDITIONAL_REPORT8_LINEDATA *prpFlight2);
	int  GetVia(CCSPtrArray<VIADATA> *opVias, ROTATIONDLGFLIGHTDATA *prpFlight);

// Operations
public:
	void DeleteAll();
	int CreateLine(ADDITIONAL_REPORT8_LINEDATA &prpGefundenefluege);
	void DeleteLine(int ipLineno);
// Window refreshing routines
public:
	void UpdateDisplay();
	void DrawHeader();


// Attributes used for filtering condition
private:
	bool bmIsFromSearch;
	// Array der Header
	CStringArray omShownColumns;
	// Sapltengroese
	CUIntArray omAnzChar;

    CCSTable *pomTable;
	CDialog* pomParentDlg;

	CCSPtrArray<ROTATIONDLGFLIGHTDATA> *pomData;
	// Liste der existierenden Delays
	CStringArray omColumn0;
	CStringArray omColumn1;
	CStringArray omColumn2;
	CStringArray omColumn3;
	CStringArray omColumn4;
	// Auftreten der existierenden Delays
	CUIntArray omCountColumn0;
	CUIntArray omCountColumn1;
	CUIntArray omCountColumn2;
	int imTableLine; // Zeilennumer fuer GetHeaderContent
	

public:
    CCSPtrArray<ADDITIONAL_REPORT8_LINEDATA> omLines;
	CCSPtrArray<TABLE_HEADER_COLUMN> omHeaderDataArray;

	// anzuzeigende Header als Array uebergeben
	void SetColumnsToShow(const CStringArray &opShownColumns);
	int GetFlightCount();

//Print 
	void GetHeader(void);
	CCSPtrArray <TABLE_HEADER_COLUMN> omPrintHeadHeaderArray;
	CCSPrint *pomPrint; 
	void PrintTableView(void);
	bool PrintTableLine(ADDITIONAL_REPORT8_LINEDATA *prpLine,bool bpLastLine, 
		                CString opCurrentACType);
	bool PrintTableHeader(void);
	CString omTableName;
	char *pcmInfo;
	char *pcmSelect;
	CString omFooterName;

protected:
	// Mapping Tabellenfeldname/Spaltenueberschrift
	CString GetHeaderContent(CString opCurrentColumns);
	CString GetFieldContent(ADDITIONAL_REPORT8_LINEDATA* prlLine, 
		                    CString opCurrentColumns, CString opCurrentACType);
	void SetFieldLength(CString opCurrentColumns);
	// Listen zum Zaehlen erstellen
	void MakeCountList(ROTATIONDLGFLIGHTDATA *prpAFlight, ROTATIONDLGFLIGHTDATA *prpDFlight);

};

#endif