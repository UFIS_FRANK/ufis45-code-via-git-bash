// ReportTableDlg.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "fpms.h"
#include "ReportTableDlg.h"
#include "ReportSelectDlg.h"
#include "RotationDlgCedaFlightData.h"
#include "resrc1.h"
#include "Utils.h"
#include <process.h>

#ifdef _DEBUG 
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld CReportTableDlg 

CReportTableDlg::CReportTableDlg(CWnd* pParent, int iTable, 
								 CCSPtrArray<ROTATIONDLGFLIGHTDATA> *popData, char *pcpInfo, 
								 char *pcpSelect, char *pcpInfo2, int ipArrDep,RotationDlgCedaFlightData* popRotDlgData, bool bpUseLoatab)
	: CDialog(CReportTableDlg::IDD, pParent)
{
	pomData = popData;
	pomRotDlgData = popRotDlgData;
	pcmInfo = pcpInfo;
	// FAG BVD :  Daily Reports ( bmDailyRep )  
	// Info2 - wird an Report(x)..... weitergereicht   
	//		   : (R17+18) Verkehrsrechte  
	//           (R19)    Startzeitpunkt, Endzeitpunkt, Terminalauswahl ("0 1 2 12") 
	pcmInfo2 = pcpInfo2;	
	pcmSelect = pcpSelect;
	miTable = iTable;
//   bmDailyRep =  ( miTable >= 17 && miTable <= 19 ) ? true : false ;
    // ab 2/99 DailySeason auf Datei
	// ab 2/2000 DailyCca auch in Datei (TVO)
	bmDailyRep =  ( miTable == 17 ) ? true : false ;
    pomReportTable = NULL;
	pomReportCTable = NULL;

	// SHA-Reports
	pomReportSameORGTableViewer  = NULL;	// case 1
	pomReportSameREGNTableViewer = NULL;	// case 2
	pomReportSameDESTableViewer  = NULL;	// case 3
	pomReportSameACTTableViewer  = NULL;	// case 4
	pomReportSameTTYPTableViewer = NULL;	// case 5
	pomReportSameALTableViewer   = NULL;	// case 0
	pomReportSameDCDTableViewer  = NULL;	// case 6
	pomReportSameDTDTableViewer  = NULL;	// case 7
	pomReportSameFTYPXTableViewer= NULL;	// case 8
	pomReportArrDepTableViewer   = NULL;	// case 10	
	pomReportOvernightTableViewer = NULL; // case 20
	pomReportIntDomMixTableViewer = NULL; // case 21
	pomReportSeatIntDomMixTableViewer = NULL; // case 22
	pomReportArrDepLoadPaxTableViewer   = NULL;	// case 23
	pomReportFlightGPUTableViewer   = NULL;	// case 24
	pomReportBltAllocViewer = NULL; // case 25
	pomReportDailyFlightLogViewer = NULL; // case 26
	pomReportFlightDelayViewer = NULL; // case 27

	imArrDep = ipArrDep;
	bmCommonDlg = false;

	pomGefundenefluegeViewer = NULL;	//	0
	//pomAllefluegeViewer = NULL;		//	1
	pomAbfluegeViewer = NULL;			//	2
	pomFluegezeitraumViewer = NULL;		//	3
	pomMessedaystopViewer = NULL;		//	4
	pomAllemessefluegeViewer = NULL;	//	5
	pomMessefluegeViewer = NULL;		//	6
	pomStatistikMesseViewer = NULL;		//	7
	pomAbfluegelvgViewer = NULL;		//	8
	//pomFluggesellschaftenViewer = NULL;	//	9
	pomSaisonflugplanlvgViewer = NULL;	//	10
	pomFlughaefenViewer = NULL;			//	11
	//pomAbfluegeVerkehrsartViewer = NULL;//	12
	pomArbeitsflugplanViewer = NULL;	//	13
	pomSitzplaetzeViewer = NULL;		//	14
	pomLvgViewer = NULL;		//	15
	pomDesViewer = NULL;		//	16

	pomReport15Viewer = NULL;	// Wochenplan	
	pomReport16Viewer = NULL;	// Inventar	

	pomReport17Viewer = NULL;	// Daily/Seasonal OPS-Plan	
	pomReport19Viewer = NULL;	// Daily Counter-Plan	

	bmUseLoatab = bpUseLoatab;

	m_key = "DialogPosition\\Reports";
}

CReportTableDlg::~CReportTableDlg()
{
	if (pomReportTable != NULL)
	{
		delete pomReportTable;
	}
	if (pomReportCTable != NULL)
	{
		delete pomReportCTable;
	}
	if (pomGefundenefluegeViewer != NULL)
	{
		delete pomGefundenefluegeViewer;
	}
	/*if (pomAllefluegeViewer != NULL)
	{
		delete pomAllefluegeViewer;
	}*/
	if (pomAbfluegeViewer != NULL)
	{
		delete pomAbfluegeViewer;
	}
	if (pomFluegezeitraumViewer != NULL)
	{
		delete pomFluegezeitraumViewer;
	}
	if (pomMessedaystopViewer != NULL)
	{
		delete pomMessedaystopViewer;
	}
	if (pomAllemessefluegeViewer != NULL)
	{
		delete pomAllemessefluegeViewer;
	}
	if (pomMessefluegeViewer != NULL)
	{
		delete pomMessefluegeViewer;
	}
	if (pomStatistikMesseViewer != NULL)
	{
		delete pomStatistikMesseViewer;
	}
	if (pomAbfluegelvgViewer != NULL)
	{
		delete pomAbfluegelvgViewer;
	}
	if (pomSaisonflugplanlvgViewer != NULL)
	{
		delete pomSaisonflugplanlvgViewer;
	}
	if (pomFlughaefenViewer != NULL)
	{
		delete pomFlughaefenViewer;
	}
	if (pomArbeitsflugplanViewer != NULL)
	{
		delete pomArbeitsflugplanViewer;
	}
	if (pomSitzplaetzeViewer != NULL)
	{
		delete pomSitzplaetzeViewer;
	}
	if (pomLvgViewer != NULL)
	{
		delete pomLvgViewer;
	}
	if (pomDesViewer != NULL)
	{
		delete pomDesViewer;
	}
	if (pomReport15Viewer != NULL)
	{
		delete pomReport15Viewer;
	}
	if (pomReport16Viewer != NULL)
	{
		delete pomReport16Viewer;
	}


	if (pomReport17Viewer != NULL)
	{
		delete pomReport17Viewer;
	}
	if (pomReport19Viewer != NULL)
	{
		delete pomReport19Viewer;
	}

	// SHA - Reports
	if (pomReportSameORGTableViewer != NULL)
		delete pomReportSameORGTableViewer;
	if (pomReportSameREGNTableViewer != NULL)
		delete pomReportSameREGNTableViewer;
	if (pomReportSameDESTableViewer != NULL)
		delete pomReportSameDESTableViewer;
	if (pomReportSameACTTableViewer != NULL)
		delete pomReportSameACTTableViewer;
	if (pomReportSameTTYPTableViewer != NULL)
		delete pomReportSameTTYPTableViewer;
	if (pomReportSameALTableViewer != NULL)
		delete pomReportSameALTableViewer;
	if (pomReportSameDCDTableViewer != NULL)
		delete pomReportSameDCDTableViewer;
	if (pomReportSameDTDTableViewer != NULL)
		delete pomReportSameDTDTableViewer;
	if (pomReportSameFTYPXTableViewer != NULL)
		delete pomReportSameFTYPXTableViewer;
	if (pomReportArrDepLoadPaxTableViewer)
		delete pomReportArrDepLoadPaxTableViewer;
	if (pomReportArrDepTableViewer)
		delete pomReportArrDepTableViewer;
	if (pomReportOvernightTableViewer)
		delete pomReportOvernightTableViewer;
	if (pomReportIntDomMixTableViewer)
		delete pomReportIntDomMixTableViewer;
	if (pomReportSeatIntDomMixTableViewer)
		delete pomReportSeatIntDomMixTableViewer;
	if (pomReportFlightGPUTableViewer)
		delete pomReportFlightGPUTableViewer;
	if (pomReportBltAllocViewer)
		delete pomReportBltAllocViewer;
	if (pomReportDailyFlightLogViewer)
		delete pomReportDailyFlightLogViewer;
	if (pomReportFlightDelayViewer)
		delete pomReportFlightDelayViewer;

}

void CReportTableDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CReportTableDlg)
	DDX_Control(pDX, IDC_BEENDEN, m_CB_Beenden);
	DDX_Control(pDX, IDC_PAPIER, m_CB_Papier);
	DDX_Control(pDX, IDC_DRUCKEN, m_CB_Drucken);
	DDX_Control(pDX, IDC_DATEI, m_CB_Datei);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CReportTableDlg, CDialog)
	//{{AFX_MSG_MAP(CReportTableDlg)
	ON_BN_CLICKED(IDC_DATEI, OnDatei)
	ON_BN_CLICKED(IDC_DRUCKEN, OnDrucken)
	ON_BN_CLICKED(IDC_PAPIER, OnPapier)
	ON_BN_CLICKED(IDC_BEENDEN, OnBeenden)
	ON_WM_SIZE()
 	ON_WM_DESTROY()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Behandlungsroutinen f�r Nachrichten CReportTableDlg 

void CReportTableDlg::SaveToReg()
{
	BOOL ok = WriteDialogToReg((CWnd*) this, m_key);
}


void CReportTableDlg::OnDestroy() 
{
	SaveToReg();
	CDialog::OnDestroy();
}

void CReportTableDlg::OnSize(UINT nType, int cx, int cy)  
{
	CDialog::OnSize(nType, cx, cy);
	
	// TODO: Add your message handler code here
	m_resizeHelper.OnSize();

	if(pomReportTable != NULL)
	{
		CRect rect;	
		GetClientRect(&rect);
		rect.InflateRect(1, 1);     // hiding the CTable window border
		pomReportTable->SetPosition(rect.left, rect.right, rect.top+imDialogBarHeight, rect.bottom);
	}
	if(pomReportCTable != NULL)
	{
		CRect rect;	
		GetClientRect(&rect);
		rect.InflateRect(1, 1);     // hiding the CTable window border
		pomReportCTable->SetPosition(rect.left, rect.right, rect.top+imDialogBarHeight, rect.bottom);
	}

	this->Invalidate();
}


BOOL CReportTableDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	m_key = "DialogPosition\\Reports";
	
	// Table Initialisierung 
	CRect olrectTable;

	GetClientRect(&olrectTable);
    imDialogBarHeight = olrectTable.bottom - olrectTable.top;
    
	// extend dialog window to current screen width
    olrectTable.left = 0;
//    olrectTable.top = 65;
    olrectTable.top = 95;
    olrectTable.right = 1024;
    olrectTable.bottom = 768;
    MoveWindow(&olrectTable);
	
	int ilFlightCount;
	bool blValidTable = true ;

	// Default Ausgabe: Drucker
	m_CB_Papier.SetCheck( TRUE ) ;

	if( miTable < 14 || (miTable > 19 && miTable < 28))  // Reports verwenden CCSTable, au�er ...
	{
		pomReportTable = new CCSTable;
		pomReportTable->imSelectMode = 0;

		GetClientRect(&olrectTable);              // !!!!!T E S T !!!!!!!!!!!!!!!!!
		olrectTable.top = olrectTable.top + imDialogBarHeight + 25;

		olrectTable.InflateRect(1,1);     // hiding the CTable window border
		//pomReportTable->SetTableData(this, olrectTable.left+2, olrectTable.right-2, olrectTable.top+28, olrectTable.bottom);
		pomReportTable->SetTableData(this, olrectTable.left, olrectTable.right, olrectTable.top, olrectTable.bottom);
		ilFlightCount = pomData->GetSize();
	}
	else if( miTable <= 19 ) // Wochenplan, Daily-Pl�ne und Inventar verwenden CTable
	{
		pomReportCTable = new CTable;
		pomReportCTable->imSelectMode = 0;

		GetClientRect(&olrectTable);                    
		olrectTable.top = olrectTable.top + imDialogBarHeight;

		olrectTable.InflateRect(1,1);     // hiding the CTable window border
		pomReportCTable->SetTableData(this, olrectTable.left, olrectTable.right, olrectTable.top, (olrectTable.bottom)+28);
	}
	else
	{
		blValidTable = false ;
		Beep( 500, 500 ) ;
		CString olMessage ;
		olMessage.Format( GetString( IDS_STRING1460) , miTable ) ;
		MessageBox( olMessage, "ReportTableDlg", MB_ICONWARNING ) ;
	}


	if( blValidTable )
	{
		switch( miTable )
		{
		case 0:	//	Liste der Gefundenen Flugbewegungen
		{
			if (ogCustomer == "SHA" && ogAppName == "FIPS")
			{
				// wenn neue Spalten fuer SHA --> neuer Viewer
				pomReportSameALTableViewer = new ReportSameALTableViewer(pomData, pcmInfo, pcmSelect);
				pomReportSameALTableViewer->SetParentDlg(this);
				pomReportSameALTableViewer->Attach(pomReportTable);

				SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
				pomReportSameALTableViewer->ChangeViewTo("dummy");
				SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
	
				char pclHeadlineSelect[120];

				ilFlightCount = pomData->GetSize();

				sprintf(pclHeadlineSelect, GetString(IDS_STRING1602), pcmSelect,
						pomReportSameALTableViewer->GetAlfn().GetBuffer(0), pcmInfo,ilFlightCount);
				omHeadline = pclHeadlineSelect;			
			}
			else
			{
				pomGefundenefluegeViewer = new GefundenefluegeTableViewer(pomData, pcmInfo, pcmSelect);
				pomGefundenefluegeViewer->Attach(pomReportTable);

				SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
				pomGefundenefluegeViewer->ChangeViewTo("dummy");
				SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));

				char pclHeadlineSelect[120];
				sprintf(pclHeadlineSelect, GetString(IDS_STRING1060), pcmSelect,pcmInfo,ilFlightCount);
				omHeadline = pclHeadlineSelect;
			}

		}
		break;
		case 1:	//	Liste aller Abfl�ge (BGS)
		{
			// SHA: Statistic of Flights that take off from the same destination Airport
			if ((ogCustomer == "SHA") && (ogAppName == "FIPS"))
			{
				pomReportSameORGTableViewer = new ReportSameORGTableViewer(pomData, pcmInfo, pcmSelect);
				pomReportSameORGTableViewer->SetParentDlg(this);
				pomReportSameORGTableViewer->Attach(pomReportTable);
				
				SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
				pomReportSameORGTableViewer->ChangeViewTo("dummy");
				SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));

				char pclHeadlineSelect[120];
				ilFlightCount = pomData->GetSize();
				sprintf(pclHeadlineSelect, GetString(IDS_STRING1573), pcmSelect, pcmInfo, ilFlightCount);
				omHeadline = pclHeadlineSelect;
			}
			else
			{
				pomAbfluegeViewer = new AbfluegeTableViewer(pomData, pcmInfo);
				pomAbfluegeViewer->Attach(pomReportTable);
				
				SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
				pomAbfluegeViewer->ChangeViewTo("dummy");
				SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));

				char pclHeadlineSelect[120];
				sprintf(pclHeadlineSelect, GetString(IDS_STRING1061), pcmInfo, ilFlightCount);
				omHeadline = pclHeadlineSelect;
			}

			
		}
		break;
		case 2:	//	Liste aller Fl�ge f�r Flugnummer ...
		{
			// SHA: Statistic of Flights that take off from the same destination Airport
			if ((ogCustomer == "SHA") && (ogAppName == "FIPS"))
			{
				pomReportSameREGNTableViewer = new ReportSameREGNTableViewer(pomData, pcmInfo, pcmSelect);
				pomReportSameREGNTableViewer->SetParentDlg(this);
				pomReportSameREGNTableViewer->Attach(pomReportTable);
				
				SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
				pomReportSameREGNTableViewer->ChangeViewTo("dummy");
				SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));

				char pclHeadlineSelect[120];
				ilFlightCount = pomData->GetSize();
				sprintf(pclHeadlineSelect, GetString(IDS_STRING1590), pcmSelect, pcmInfo, ilFlightCount);
				omHeadline = pclHeadlineSelect;
			}
			else
			{
				pomFluegezeitraumViewer = new FluegezeitraumTableViewer(pomData, pcmInfo, pcmSelect);
				pomFluegezeitraumViewer->Attach(pomReportTable);

				SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
				pomFluegezeitraumViewer->ChangeViewTo("dummy");
				SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));

				char pclHeadlineSelect[120];
				sprintf(pclHeadlineSelect, GetString(IDS_STRING1062), pcmSelect, pcmInfo, ilFlightCount);
				omHeadline = pclHeadlineSelect;
				//omHeadline = "Liste aller Fl�ge in einem Zeitraum";	//Dialogbox-�berschrift
			}
		}
		break;
		case 3:	//	Messefl�ge im Daystop
		{
			// SHA: Statistic of Flights that take off from the same destination Airport
			if ((ogCustomer == "SHA") && (ogAppName == "FIPS"))
			{
				pomReportSameDESTableViewer = new ReportSameDESTableViewer(pomData, pcmInfo, pcmSelect);
				pomReportSameDESTableViewer->SetParentDlg(this);
				pomReportSameDESTableViewer->Attach(pomReportTable);
				
				SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
				pomReportSameDESTableViewer->ChangeViewTo("dummy");
				SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));

				char pclHeadlineSelect[120];
				ilFlightCount = pomData->GetSize();
				sprintf(pclHeadlineSelect, GetString(IDS_STRING1578), pcmSelect, pcmInfo, ilFlightCount);
				omHeadline = pclHeadlineSelect;
			}
			else
			{
				pomMessedaystopViewer = new MessedaystopTableViewer(pomData, pcmInfo, pcmSelect);
				pomMessedaystopViewer->Attach(pomReportTable);

				SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
				pomMessedaystopViewer->ChangeViewTo(&ilFlightCount);
				SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));

				char pclHeadlineSelect[120];
				sprintf(pclHeadlineSelect, GetString(IDS_STRING1063), pcmInfo, ilFlightCount);
				omHeadline = pclHeadlineSelect;
			}
		}
		break;
		case 4:	//	Liste aller Messefl�ge 
		{
			
			// SHA: Statistic of Flights that the same AC-Type
			if ((ogCustomer == "SHA") && (ogAppName == "FIPS"))
			{
				pomReportSameACTTableViewer = new ReportSameACTTableViewer(pomData, pcmInfo, pcmSelect);
				pomReportSameACTTableViewer->SetParentDlg(this);
				pomReportSameACTTableViewer->Attach(pomReportTable);
				
				SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
				pomReportSameACTTableViewer->ChangeViewTo("dummy");
				SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));

				char pclHeadlineSelect[120];
				ilFlightCount = pomData->GetSize();
				sprintf(pclHeadlineSelect, GetString(IDS_STRING1583), pcmSelect, 
					    pomReportSameACTTableViewer->GetAcfn(), pcmInfo, ilFlightCount);
				omHeadline = pclHeadlineSelect;
			}
			else
			{
				pomAllemessefluegeViewer = new AllemessefluegeTableViewer(pomData, pcmInfo, pcmSelect);
				pomAllemessefluegeViewer->Attach(pomReportTable);

				SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
				pomAllemessefluegeViewer->ChangeViewTo("dummy");
				SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));

				char pclHeadlineSelect[120];
				if(strlen(pcmSelect))
					sprintf(pclHeadlineSelect, GetString(IDS_STRING1064), pcmSelect, pcmInfo, ilFlightCount);
				else
					sprintf(pclHeadlineSelect, GetString(IDS_STRING1065), pcmInfo, ilFlightCount);

				omHeadline = pclHeadlineSelect;
			}
		}
		break;
		case 5:	//	Statistik Messefl�ge
		{
			// SHA: Statistic of Flights that the same TType
			if ((ogCustomer == "SHA") && (ogAppName == "FIPS"))
			{
				pomReportSameTTYPTableViewer = new ReportSameTTYPTableViewer(pomData, pcmInfo, pcmSelect);
				pomReportSameTTYPTableViewer->SetParentDlg(this);
				pomReportSameTTYPTableViewer->Attach(pomReportTable);
				
				SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
				pomReportSameTTYPTableViewer->ChangeViewTo("dummy");
				SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));

				char pclHeadlineSelect[120];
				ilFlightCount = pomData->GetSize();
				sprintf(pclHeadlineSelect, GetString(IDS_STRING1598), pcmSelect,
					    pomReportSameTTYPTableViewer->GetTname(), pcmInfo, ilFlightCount);
				omHeadline = pclHeadlineSelect;
			}
			else
			{
				pomMessefluegeViewer = new MessefluegeTableViewer(pomData, pcmInfo);
				pomMessefluegeViewer->Attach(pomReportTable);

				SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
				pomMessefluegeViewer->ChangeViewTo("dummy");
				SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));

				char pclHeadlineSelect[120];
				sprintf(pclHeadlineSelect, GetString(IDS_STRING1066), pcmInfo, ilFlightCount);
				omHeadline = pclHeadlineSelect;
			}
		}
		break;
		case 6:	//	Statistik Messefl�ge im Daystop
		{
			// SHA: Flight statistic according to delay reason
			if ((ogCustomer == "SHA") && (ogAppName == "FIPS"))
			{
				pomReportSameDCDTableViewer = new ReportSameDCDTableViewer(pomData, pcmInfo, pcmSelect);
				pomReportSameDCDTableViewer->SetParentDlg(this);
				pomReportSameDCDTableViewer->Attach(pomReportTable);
				
				SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
				pomReportSameDCDTableViewer->ChangeViewTo("dummy");
				SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));

				char pclHeadlineSelect[120];
				ilFlightCount = pomData->GetSize();
				sprintf(pclHeadlineSelect, GetString(IDS_STRING1617), pcmSelect,
					    pomReportSameDCDTableViewer->GetDname(), pcmInfo, ilFlightCount);
				omHeadline = pclHeadlineSelect;
			}
			else
			{
				pomStatistikMesseViewer = new StatistikMesseTableViewer(pomData, pcmInfo, pcmSelect);
				pomStatistikMesseViewer->Attach(pomReportTable);

				SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
				pomStatistikMesseViewer->ChangeViewTo(&ilFlightCount);
				SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));

				char pclHeadlineSelect[120];
				sprintf(pclHeadlineSelect, GetString(IDS_STRING1067), pcmInfo, ilFlightCount);
				omHeadline = pclHeadlineSelect;
			}
		}
		break;
		case 7:	//	Statistik Liste aller Abfl�ge pro LVG
		{
			// SHA: Flight statistic according to delay time
			if ((ogCustomer == "SHA") && (ogAppName == "FIPS"))
			{
				pomReportSameDTDTableViewer = new ReportSameDTDTableViewer(pomData, pcmInfo, pcmSelect);
				pomReportSameDTDTableViewer->SetParentDlg(this);
				pomReportSameDTDTableViewer->Attach(pomReportTable);
				
				SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
				pomReportSameDTDTableViewer->ChangeViewTo("dummy");
				SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));

				char pclHeadlineSelect[120];
				ilFlightCount = pomData->GetSize();
				sprintf(pclHeadlineSelect, GetString(IDS_STRING1622), pcmSelect, pcmInfo, 
					    ilFlightCount);
				omHeadline = pclHeadlineSelect;
			}
			else
			{
				pomAbfluegelvgViewer = new AbfluegelvgTableViewer(pomData, pcmInfo);
				pomAbfluegelvgViewer->Attach(pomReportTable);

				SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
				pomAbfluegelvgViewer->ChangeViewTo("dummy");
				SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));

				char pclHeadlineSelect[120];
				sprintf(pclHeadlineSelect, GetString(IDS_STRING1068), pcmInfo, ilFlightCount);
				omHeadline = pclHeadlineSelect;
			}
		}
		break;
		case 8:	//	Fluggesellschaften eines Saisonflugplanes
		{
			// SHA: Statistic according to flight cancelled
			if ((ogCustomer == "SHA") && (ogAppName == "FIPS"))
			{
				pomReportSameFTYPXTableViewer = new ReportSameFTYPXTableViewer(pomData, pcmInfo, pcmSelect);
				pomReportSameFTYPXTableViewer->SetParentDlg(this);
				pomReportSameFTYPXTableViewer->Attach(pomReportTable);
				
				SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
				pomReportSameFTYPXTableViewer->ChangeViewTo("dummy");
				SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));

				char pclHeadlineSelect[120];
				ilFlightCount = pomData->GetSize();
				sprintf(pclHeadlineSelect, GetString(IDS_STRING1624), pcmInfo, ilFlightCount);
				omHeadline = pclHeadlineSelect;
			}
			else
			{
				pomSaisonflugplanlvgViewer = new SaisonflugplanTableViewer(pomData, pcmInfo);
				pomSaisonflugplanlvgViewer->Attach(pomReportTable);

				SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
				pomSaisonflugplanlvgViewer->ChangeViewTo("dummy");
				SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));

				char pclHeadlineSelect[120];
				sprintf(pclHeadlineSelect, GetString(IDS_STRING1069), pcmInfo, ilFlightCount);
				omHeadline = pclHeadlineSelect;
			}
		}
		break;
		case 9:	//	Liste aller angeflogenen Flugh�fen mit Frequenz
		{
			// flight cancelled nur summary bei SHA 
			if ((ogCustomer == "SHA") && (ogAppName == "FIPS"))
			{
			}
			else
			{
				pomFlughaefenViewer = new FlughaefenTableViewer(pomData, pcmInfo);
				pomFlughaefenViewer->Attach(pomReportTable);

				SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
				pomFlughaefenViewer->ChangeViewTo("dummy");
				SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));

				char pclHeadlineSelect[120];
				sprintf(pclHeadlineSelect, GetString(IDS_STRING1070), pcmInfo, ilFlightCount);
				omHeadline = pclHeadlineSelect;
			}
		}
		break;
		case 10:	//	Arbeitsflugplan
		{
			// arr/dep each day
			if ((ogCustomer == "SHA") && (ogAppName == "FIPS"))
			{
				pomReportArrDepTableViewer = new ReportArrDepTableViewer(pomData, imArrDep, pcmInfo);
				pomReportArrDepTableViewer->SetParentDlg(this);
				pomReportArrDepTableViewer->Attach(pomReportTable);

				SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
				pomReportArrDepTableViewer->ChangeViewTo("dummy");
				SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));

				char pclHeadlineSelect[120];
				sprintf(pclHeadlineSelect, GetString(IDS_STRING1631), pcmInfo, 
					    pomReportArrDepTableViewer->GetFlightCount());
				omHeadline = pclHeadlineSelect;
			}
			else
			{
				pomArbeitsflugplanViewer = new ArbeitsflugplanTableViewer(pomData, pcmInfo);
				pomArbeitsflugplanViewer->Attach(pomReportTable);

				SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
				pomArbeitsflugplanViewer->ChangeViewTo("dummy");
				SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));

				char pclHeadlineSelect[120];
				sprintf(pclHeadlineSelect, GetString(IDS_STRING1071), pcmInfo, ilFlightCount);
				omHeadline = pclHeadlineSelect;
			}
		}
		break;
		case 11:	//	Statistik Sitzplatzangebot nach LVGs und/oder Destinationen
		{
			pomSitzplaetzeViewer = new SitzplaetzeTableViewer(pomData, pcmInfo, pcmSelect);
			pomSitzplaetzeViewer->Attach(pomReportTable);

			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
			pomSitzplaetzeViewer->ChangeViewTo("dummy");
			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));

			char pclHeadlineSelect[120];
			sprintf(pclHeadlineSelect, GetString(IDS_STRING1072), pcmSelect, pcmInfo, ilFlightCount);
			omHeadline = pclHeadlineSelect;
		}
		break;
		case 12:	//	Statistik Sitzplatzangebot nach LVGs
		{
			pomLvgViewer = new LvgTableViewer(pomData, pcmInfo, pcmSelect);
			pomLvgViewer->Attach(pomReportTable);

			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
			pomLvgViewer->ChangeViewTo("dummy");
			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));

			char pclHeadlineSelect[120];
			sprintf(pclHeadlineSelect, GetString(IDS_STRING1072), pcmSelect, pcmInfo, ilFlightCount);
			omHeadline = pclHeadlineSelect;
		}
		break;
		case 13:	//	Statistik Sitzplatzangebot nach Destinationen
		{
			// time frame
			if ((ogCustomer == "SHA") && (ogAppName == "FIPS"))
			{
				CStringArray olColumnsToShow; 

				olColumnsToShow.Add("Date");
				olColumnsToShow.Add("IntlArr");
				olColumnsToShow.Add("IntlDep");
				olColumnsToShow.Add("DomArr");
				olColumnsToShow.Add("DomDep");
				olColumnsToShow.Add("OtherArr");
				olColumnsToShow.Add("OtherDep");
				olColumnsToShow.Add("TotalArr");
				olColumnsToShow.Add("TotalDep");

				pomReportTimeFrameTableViewer = new ReportTimeFrameTableViewer(pomData, olColumnsToShow, pcmInfo, pcmSelect);
				pomReportTimeFrameTableViewer->SetParentDlg(this);
				pomReportTimeFrameTableViewer->Attach(pomReportTable);

				SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
				pomReportTimeFrameTableViewer->ChangeViewTo("dummy");
				SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));

				char pclHeadlineSelect[120];
				sprintf(pclHeadlineSelect, GetString(IDS_STRING1652), pcmInfo, 
					    pomReportTimeFrameTableViewer->GetFlightCount());
				omHeadline = pclHeadlineSelect;
			}
			else
			{
				pomDesViewer = new DesTableViewer(pomData, pcmInfo, pcmSelect);
				pomDesViewer->Attach(pomReportTable);

				SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
				pomDesViewer->ChangeViewTo("dummy");
				SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));

				char pclHeadlineSelect[120];
				sprintf(pclHeadlineSelect, GetString(IDS_STRING1072), pcmSelect, pcmInfo, ilFlightCount);
				omHeadline = pclHeadlineSelect;
			}
		}
		break;
		case 14:	//	Wochenplan
		{
			pomReport15Viewer = new Report15TableViewer(pcmInfo, pcmSelect);
			pomReport15Viewer->Attach(pomReportCTable);

			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
			pomReport15Viewer->ChangeViewTo("dummy");
			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));

			char pclHeadlineSelect[120];
			sprintf(pclHeadlineSelect, GetString(IDS_STRING1075), pcmSelect,pcmInfo);
			omHeadline = pclHeadlineSelect;
		}
		break;


		// Inventory (A/C On Ground)
		case 15:
		{
			pomReport16Viewer = new Report16TableViewer(pomData, pcmInfo);
			pomReport16Viewer->Attach(pomReportCTable);

			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
			pomReport16Viewer->ChangeViewTo("dummy");
			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));

			omHeadline = pomReport16Viewer->omTableName;
		}
		break;

/*
		case 15:	//	Inventar
		{
			pomReport16Viewer = new Report16TableViewer(pomData, pcmInfo);
			pomReport16Viewer->Attach(pomReportCTable);

			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
			pomReport16Viewer->ChangeViewTo("dummy");
			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));

			char pclHeadlineSelect[120];
			sprintf(pclHeadlineSelect, GetString(IDS_STRING1076), pcmInfo);
			omHeadline = pclHeadlineSelect;
		}
		break;
*/
		case 17:	//	Daily OPS-Plan
			m_CB_Datei.EnableWindow( false ) ;
		case 18 :	//  Seasonal OPS-Plan
		{
			pomReport17Viewer = new Report17DailyTableViewer(pcmInfo, pcmSelect, pcmInfo2 );
			pomReport17Viewer->Attach(pomReportCTable);

//			m_CB_Datei.EnableWindow( false ) ;
			m_CB_Papier.SetCheck( TRUE ) ;

			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
			if( miTable == 17 )
			{
				pomReport17Viewer->ChangeViewTo(GetString(IDS_STRING1400));	// Daily
			}
			else
			{
				pomReport17Viewer->ChangeViewTo(GetString(IDS_STRING1411));	// Seasonal
			}
			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));

			char pclHeadlineSelect[120];
			if( miTable == 17 ) 
			{
				sprintf(pclHeadlineSelect, GetString(IDS_STRING1401), pcmSelect,pcmInfo);
			}
			else
			{
				sprintf(pclHeadlineSelect, GetString(IDS_STRING1413), pcmSelect,pcmInfo);
			}
			omHeadline = pclHeadlineSelect;
		}
		break;

		case 19 :	//  Daily Counter-Plan
		{
			CString olDay(pcmInfo2) ;
			olDay.TrimLeft() ;
			CTime olTimeFrom, olTimeTo;
			if (olDay.GetLength() < 14)
			{
				olTimeFrom = CTime::GetCurrentTime();
				olTimeTo = CTime(olTimeFrom.GetYear(), olTimeFrom.GetMonth(), olTimeFrom.GetDay(), 23, 59, 59);
			}
			else if (olDay.GetLength() < 28)
			{
				olTimeFrom = DBStringToDateTime(olDay.Left(14));
				olTimeTo = CTime(olTimeFrom.GetYear(), olTimeFrom.GetMonth(), olTimeFrom.GetDay(), 23, 59, 59);
			}
			else
			{
				olTimeFrom = DBStringToDateTime(olDay.Left(14));
				olTimeTo = DBStringToDateTime(olDay.Mid(15,14));
			}

			//CTime olTrafficDay = olDay.GetLength() >= 14 ? DBStringToDateTime(olDay.Left(14)) : CTime::GetCurrentTime() ;
			
			pomReport19Viewer = new Report19DailyCcaTableViewer(pcmInfo, pcmSelect, pcmInfo2);
			pomReport19Viewer->Attach(pomReportCTable);

			//m_CB_Datei.EnableWindow( false ) ;
			//m_CB_Papier.SetCheck( TRUE ) ;

			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
			pomReport19Viewer->ChangeViewTo(olTimeFrom, olTimeTo);	// Daily Counter
			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));

			char pclHeadlineSelect[120];
			sprintf(pclHeadlineSelect, GetString(IDS_STRING1462), pcmSelect,pcmInfo);
			omHeadline = pclHeadlineSelect;
		}
		break;
		case 20:	//	Overnight stop
		{
			// SHA: Statistic according overnight stops
			if ((ogCustomer == "SHA") && (ogAppName == "FIPS"))
			{
				pomReportOvernightTableViewer = new ReportOvernightTableViewer(pomRotDlgData, pcmInfo, pcmSelect);
				pomReportOvernightTableViewer->SetParentDlg(this);
				pomReportOvernightTableViewer->Attach(pomReportTable);
				
				SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
				pomReportOvernightTableViewer->ChangeViewTo("dummy");
				SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));

				char pclHeadlineSelect[120];
				ilFlightCount = pomReportOvernightTableViewer->imOvernightCount;
//				ilFlightCount = pomData->GetSize();
				sprintf(pclHeadlineSelect, GetString(IDS_STRING1779), pcmInfo, ilFlightCount);
				omHeadline = pclHeadlineSelect;
			}
			else
			{
	
			}
		}
		break;
		case 21:	//	IntDomMixed flights
		{
			// SHA: Statistic according overnight stops
			if ((ogCustomer == "SHA") && (ogAppName == "FIPS"))
			{
				pomReportIntDomMixTableViewer = new ReportFlightDomIntMixTableViewer(pomData, pcmInfo, pcmSelect);
				pomReportIntDomMixTableViewer->SetParentDlg(this);
				pomReportIntDomMixTableViewer->Attach(pomReportTable);
				
				SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
				pomReportIntDomMixTableViewer->ChangeViewTo("dummy");
				SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));

				char pclHeadlineSelect[120];
				ilFlightCount = pomData->GetSize();
				if(imArrDep == 0)
					sprintf(pclHeadlineSelect, GetString(IDS_STRING1798),pcmSelect, pcmInfo, ilFlightCount);
				else
					sprintf(pclHeadlineSelect, GetString(IDS_STRING1799),pcmSelect, pcmInfo, ilFlightCount);

				omHeadline = pclHeadlineSelect;
			}
			else
			{
	
			}
		}
		break;
		case 22:	//	IntDomMixed flights
		{
			// SHA: Statistic according overnight stops
			if ((ogCustomer == "SHA") && (ogAppName == "FIPS"))
			{
				pomReportSeatIntDomMixTableViewer = new ReportSeatDomIntMixTableViewer(pomData, pcmInfo, pcmSelect);
				pomReportSeatIntDomMixTableViewer->SetParentDlg(this);
				pomReportSeatIntDomMixTableViewer->Attach(pomReportTable);
				
				SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
				pomReportSeatIntDomMixTableViewer->ChangeViewTo("dummy");
				SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));

				char pclHeadlineSelect[120];
				ilFlightCount = pomData->GetSize();
				if(imArrDep == 0)
					sprintf(pclHeadlineSelect, GetString(IDS_STRING1800),pcmSelect, pcmInfo, ilFlightCount);
				else
					sprintf(pclHeadlineSelect, GetString(IDS_STRING1801),pcmSelect, pcmInfo, ilFlightCount);

				omHeadline = pclHeadlineSelect;
			}
			else
			{
	
			}
		}
		break;
		case 23:
		{
			// arr/dep load & pax each day
			if ((ogCustomer == "SHA") && (ogAppName == "FIPS"))
			{
				pomReportArrDepLoadPaxTableViewer = new ReportArrDepLoadPaxTableViewer(pomData, imArrDep, pcmInfo, NULL, bmUseLoatab);
				pomReportArrDepLoadPaxTableViewer->SetParentDlg(this);
				pomReportArrDepLoadPaxTableViewer->Attach(pomReportTable);

				SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
				pomReportArrDepLoadPaxTableViewer->ChangeViewTo("dummy");
				SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));

				char pclHeadlineSelect[300];
				CString olLoa;
				if (bmUseLoatab)
					olLoa = GetString(IDS_STRING2185);
				else
					olLoa = GetString(IDS_STRING2186);

				sprintf(pclHeadlineSelect, GetString(IDS_STRING1877), pcmInfo, 
					    pomReportArrDepLoadPaxTableViewer->GetFlightCount(), olLoa);
				omHeadline = pclHeadlineSelect;
			}
		}
		break;
		case 24:
		{
			// Statistic according to GPU-Usage
 			if ((ogCustomer == "SHA") && (ogAppName == "FIPS"))
			{
				pomReportFlightGPUTableViewer = new ReportFlightGPUTableViewer(pomData, pcmInfo, pcmSelect);
				pomReportFlightGPUTableViewer->SetParentDlg(this);
				pomReportFlightGPUTableViewer->Attach(pomReportTable);

				SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
				pomReportFlightGPUTableViewer->ChangeViewTo("dummy");
				SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));

				char pclHeadlineSelect[120];
				if (strlen(pcmSelect) == 0)
				{
					sprintf(pclHeadlineSelect, GetString(IDS_STRING1880), pcmInfo, 
						    pomReportFlightGPUTableViewer->GetFlightCount());
				}
				else
				{
					sprintf(pclHeadlineSelect, GetString(IDS_STRING1887), pcmSelect, pcmInfo, 
						    pomReportFlightGPUTableViewer->GetFlightCount());
				}
				omHeadline = pclHeadlineSelect;
			}
		}
		break;
		case 25:
		{
			// Baggage belt allocation schedule
 			if ((ogCustomer == "SHA") && (ogAppName == "FIPS"))
			{
				// Create viewer
				pomReportBltAllocViewer = new ReportBltAllocViewer();
				pomReportBltAllocViewer->Attach(pomReportTable);
				// Build and show table
				SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
				pomReportBltAllocViewer->ChangeViewTo(*pomData, pcmInfo);
				SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
 				
				omHeadline.Format(GetString(IDS_STRING1901), pcmInfo, 
						    pomReportBltAllocViewer->GetFlightCount());
			}
		}
		break;
		case 26:
		{
			// Daily flight log
 			if ((ogCustomer == "SHA") && (ogAppName == "FIPS"))
			{
				// Create viewer
				pomReportDailyFlightLogViewer = new ReportDailyFlightLogViewer(bmUseLoatab);
//				pomReportDailyFlightLogViewer = new ReportDailyFlightLogViewer();
				pomReportDailyFlightLogViewer->Attach(pomReportTable);
				// Build and show table
				SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
				pomReportDailyFlightLogViewer->ChangeViewTo(*pomData, pcmInfo);
				SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
 				
				CString olLoa;
				if (bmUseLoatab)
					olLoa = GetString(IDS_STRING2185);
				else
					olLoa = GetString(IDS_STRING2186);

				omHeadline.Format(GetString(IDS_STRING1918), pcmInfo, 
						    pomReportDailyFlightLogViewer->GetFlightCount(), olLoa);
			}
		}
		break;	
		case 27:
		{
			// Delay codes
 			if ((ogCustomer == "SHA") && (ogAppName == "FIPS")) 
			{
				// Create viewer
				pomReportFlightDelayViewer = new ReportFlightDelayViewer();
				pomReportFlightDelayViewer->Attach(pomReportTable);
				// Build and show table
				SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
				pomReportFlightDelayViewer->ChangeViewTo(*pomData, pcmInfo, pcmSelect, pcmInfo2, imArrDep);
				SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));

				omHeadline = pomReportFlightDelayViewer->omTableName;
//				omHeadline.Format(GetString(IDS_STRING1971), pcmInfo, 
//						    pomReportFlightDelayViewer->GetFlightCount());
			}
		}
		break;	
		default:
			Beep( 500, 500 ) ;
			MessageBox( GetString(ST_FEHLER)+" serious data damage !" ,"ReportTableDlg", MB_ICONERROR ) ;

		}	// end switch( miTable ...
	}	// end if( ValidTable ...

	CDialog::SetWindowText(omHeadline);	//Dialogbox-�berschrift

	m_resizeHelper.Init(this->m_hWnd);
	m_resizeHelper.Fix(IDC_BEENDEN,DlgResizeHelper::kNoHFix,DlgResizeHelper::kHeight);
	m_resizeHelper.Fix(IDC_DRUCKEN,DlgResizeHelper::kNoHFix,DlgResizeHelper::kHeight);
	m_resizeHelper.Fix(IDC_PAPIER,DlgResizeHelper::kNoHFix,DlgResizeHelper::kHeight);
	m_resizeHelper.Fix(IDC_DATEI,DlgResizeHelper::kNoHFix,DlgResizeHelper::kHeight);
	m_resizeHelper.Fix(IDC_STATIC,DlgResizeHelper::kWidth,DlgResizeHelper::kHeight);

	CRect olRect;
	this->GetWindowRect(&olRect);
	GetDialogFromReg(olRect, m_key);
	SetWindowPos(&wndTop, olRect.left, olRect.top, olRect.Width(), olRect.Height(), SWP_SHOWWINDOW);// | SWP_NOMOVE );
	
	// Test mit Static Text �ber Spalten
/*	RECT olRect;
	olRect.left   = 100;
	olRect.right  = 200;
	olRect.top    = 30;
	olRect.bottom = 30 + 15;
	omStatic = new	CStatic();

	omStatic->Create("Flights", WS_VISIBLE | WS_CHILD | SS_GRAYRECT | SS_CENTER, olRect, this);*/
	// Test Ende


	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX-Eigenschaftenseiten sollten FALSE zur�ckgeben
}



void CReportTableDlg::OnDatei() 
{
	// Daily-Pl�ne nur auf Papier 
	if( bmDailyRep && m_CB_Datei.GetCheck() )
	{
		MessageBox( GetString(IDS_STRING1403), MB_OK ) ;
	}
}

void CReportTableDlg::OnDrucken() 
{
//im FPMS und BDPS-SEC/UIF mu� ein neuer Men�eintrag zum Einrichten von Druckern aufgenommen
//werden. F�r diese Zwecke gibt es einen Common-Dialog, mit dessen Hilfe der/die Drucker eingerichtet
//werden k�nnen. Die Benutzung ist in der C++ Online-Hilfe dokumentiert

	//  Wer geht nur auf Papier raus ? : Daily-Pl�ne 
	bool blAlwaysPaper = bmDailyRep ? true : false ;

	if( ! blAlwaysPaper )
	{
		if((m_CB_Papier.GetCheck() != 1) && (m_CB_Datei.GetCheck() != 1))
		{
			// kein Druck-Medium gew�hlt
			// hbe: vorher /*IDS_STRING935*/
			if( MessageBox(GetString(IDS_STRING1434), GetString(IMFK_PRINT), MB_YESNO|MB_ICONQUESTION ) != IDYES ) return ;
			else blAlwaysPaper = true ;
		}
	}

	// ----------------------------------------------------------- Druckerausgabe 
	if (m_CB_Papier.GetCheck() == 1 || blAlwaysPaper )
	{
		switch(miTable)
		{
			case 0:	//	List der Gefundenen Flugbewegungen
			{
				if ((ogCustomer == "SHA") && (ogAppName == "FIPS"))
					pomReportSameALTableViewer->PrintTableView();
				else
					pomGefundenefluegeViewer->PrintTableView();
			}
			break;
			case 1:	//	Liste aller Abfl�ge
			{
				if ((ogCustomer == "SHA") && (ogAppName == "FIPS"))
					pomReportSameORGTableViewer->PrintTableView();
				else
					pomAbfluegeViewer->PrintTableView();
			}
			break;
			case 2:	//	Liste aller Fl�ge in einem Zeitraum
			{
				if ((ogCustomer == "SHA") && (ogAppName == "FIPS"))
					pomReportSameREGNTableViewer->PrintTableView();
				else
					pomFluegezeitraumViewer->PrintTableView();
			}
			break;
			case 3:	//	Messefl�ge im Daystop
			{
				if ((ogCustomer == "SHA") && (ogAppName == "FIPS"))
					pomReportSameDESTableViewer->PrintTableView();
				else
					pomMessedaystopViewer->PrintTableView();
			}
			break;
			case 4:	//	Liste aller Messefl�ge
			{
				if ((ogCustomer == "SHA") && (ogAppName == "FIPS"))
					pomReportSameACTTableViewer->PrintTableView();
				else
					pomAllemessefluegeViewer->PrintTableView();
			}
			break;
			case 5:	//	Statistik Messefl�ge (Abfl�ge)
			{
				if ((ogCustomer == "SHA") && (ogAppName == "FIPS"))
					pomReportSameTTYPTableViewer->PrintTableView();
				else
					pomMessefluegeViewer->PrintTableView();
			}
			break;
			case 6:	//	Statistik Messefl�ge im Daystop
			{
				if ((ogCustomer == "SHA") && (ogAppName == "FIPS"))
					pomReportSameDCDTableViewer->PrintTableView();
				else
					pomStatistikMesseViewer->PrintTableView();
			}
			break;
			case 7:	//	Statistik Liste aller Abfl�ge pro LVG
			{
				if ((ogCustomer == "SHA") && (ogAppName == "FIPS"))
					pomReportSameDTDTableViewer->PrintTableView();
				else
					pomAbfluegelvgViewer->PrintTableView();
			}
			break;
			case 8:	//	Fluggesellschaften eines Saisonflugplans
			{
				if ((ogCustomer == "SHA") && (ogAppName == "FIPS"))
					pomReportSameFTYPXTableViewer->PrintTableView();
				else
					pomSaisonflugplanlvgViewer->PrintTableView();
			}
			break;
			case 9:	//	Liste aller angeflogenen Flugh�fen mit Frequenz
			{
				pomFlughaefenViewer->PrintTableView();
			}
			break;
			case 10:	//	Arbeitsflugplan
			{
				if ((ogCustomer == "SHA") && (ogAppName == "FIPS"))
					pomReportArrDepTableViewer->PrintTableView();
				else
					pomArbeitsflugplanViewer->PrintTableView();
			}
			break;
			case 11:	//	Sitzplatzangebot nach LVGs und/oder Destinationen
			{
				pomSitzplaetzeViewer->PrintTableView();
			}
			break;
			case 12:	//	Sitzplatzangebot nach LVGs 
			{
				pomLvgViewer->PrintTableView();
			}
			break;
			case 13:	//	Sitzplatzangebot nach Destinationen
			{
				if ((ogCustomer == "SHA") && (ogAppName == "FIPS"))
					pomReportTimeFrameTableViewer->PrintTableView();
				else
					pomDesViewer->PrintTableView();
			}
			break;
			case 14:	//	Wochenplan
			{
				pomReport15Viewer->PrintTableView();
			}
			break;
			case 15:	//	Inventar
			{
				pomReport16Viewer->PrintTableView();
			}
			break;

			case 17:	//	Daily OPS-Plan
			case 18:	// Seasonal OPS-Plan	
			{
				pomReport17Viewer->PrintTableView();
			}
			break;

			case 19:	//	Daily Counter-Plan
			{
				pomReport19Viewer->PrintTableView();
			}
			break;
			case 20:	//	Fluggesellschaften eines Saisonflugplans
			{
				if ((ogCustomer == "SHA") && (ogAppName == "FIPS"))
					pomReportOvernightTableViewer->PrintTableView();
			
			}
			break;
			case 21:	//	Fluggesellschaften eines Saisonflugplans
			{
				if ((ogCustomer == "SHA") && (ogAppName == "FIPS"))
					pomReportIntDomMixTableViewer->PrintTableView();
			
			}
			break;
			case 22:	//	Fluggesellschaften eines Saisonflugplans
			{
				if ((ogCustomer == "SHA") && (ogAppName == "FIPS"))
					pomReportSeatIntDomMixTableViewer->PrintTableView();
			
			}
			break;
			case 23:	//	Arbeitsflugplan mit load & pax
			{
				if ((ogCustomer == "SHA") && (ogAppName == "FIPS"))
					pomReportArrDepLoadPaxTableViewer->PrintTableView();
			}
			break;
			case 24:	//	Flugplan mit GPU-Daten
			{
				if ((ogCustomer == "SHA") && (ogAppName == "FIPS"))
					pomReportFlightGPUTableViewer->PrintTableView();
			}
			break;
			case 25:	//	Baggage belt allocation schedule
			{
				if ((ogCustomer == "SHA") && (ogAppName == "FIPS"))
					pomReportBltAllocViewer->PrintTableView();
			}			
			break;
			case 26:	//	Daily flight log
			{
				if ((ogCustomer == "SHA") && (ogAppName == "FIPS"))
					pomReportDailyFlightLogViewer->PrintTableView();
			}
			break;
			case 27:	//	Delay codes
			{
				if ((ogCustomer == "SHA") && (ogAppName == "FIPS"))
					pomReportFlightDelayViewer->PrintTableView();
			}
			break;
		}
	}


	// -------------------------------------- Drucken in Datei: alle au�er OPS-Plan
	if (m_CB_Datei.GetCheck() == 1 && ! blAlwaysPaper )
	{

		bool blSaveTxt = false;
		if (miTable == 15)
		{
			char pclConfigPath[256];
			char pclExcelPath[256];
			char pclTrenner[64];
			

			if (getenv("CEDA") == NULL)
				strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
			else
				strcpy(pclConfigPath, getenv("CEDA"));

			GetPrivateProfileString(ogAppName, "Excel", "DEFAULT",
				pclExcelPath, sizeof pclExcelPath, pclConfigPath);


			GetPrivateProfileString(ogAppName, "EXCELSEPERATOR", ";",
				pclTrenner, sizeof pclTrenner, pclConfigPath);


			if(!strcmp(pclExcelPath, "DEFAULT"))
				CFPMSApp::MyTopmostMessageBox(this,GetString(IDS_STRING977), GetString(ST_FEHLER), MB_ICONERROR);

			CString olFileName;

			if (pomReport16Viewer->PrintPlanToFile(pclTrenner))
				olFileName = pomReport16Viewer->omFileName;


			bool test = true; //only for testing error
			if (olFileName.IsEmpty() || olFileName.GetLength() > 255 || !test)
			{
				if (olFileName.IsEmpty())
				{
					CString mess = "Filename is empty !" + olFileName;
					CFPMSApp::MyTopmostMessageBox(this, mess, GetString(ST_FEHLER), MB_ICONERROR);
					blSaveTxt = true;
					//return;
				}
				if (olFileName.GetLength() > 255 )
				{
					CString mess = "Length of the Filename > 255: " + olFileName;
					CFPMSApp::MyTopmostMessageBox(this, mess, GetString(ST_FEHLER), MB_ICONERROR);
					blSaveTxt = true;
					//return;
				}

				//CString mess = "Filename invalid: " + olFileName;
				//CFPMSApp::MyTopmostMessageBox(this, mess, GetString(ST_FEHLER), MB_ICONERROR);
				//return;
			}

			if (!blSaveTxt)
			{
				char pclTmp[256];
				strcpy(pclTmp, olFileName); 

			   /* Set up parameters to be sent: */
				char *args[4];
				args[0] = "child";
				args[1] = pclTmp;
				args[2] = NULL;
				args[3] = NULL;

				_spawnv( _P_NOWAIT , pclExcelPath, args );

				return;
			}
			else
			{
				// save as .txt
			}
		}


		LPOPENFILENAME polOfn = new OPENFILENAME;
		char buffer[256] = "";
		char buffer2[256] = "";
		LPSTR lpszStr;
		lpszStr = buffer;
		CString olStr = CString("c:\\tmp\\") + CString("*.txt");
		strcpy(buffer,(LPCTSTR)olStr);

		memset(polOfn, 0, sizeof(*polOfn));
		polOfn->lStructSize = sizeof(*polOfn) ;
		polOfn->lpstrFilter = "Export File(*.txt)\0*.txt\0";
		polOfn->lpstrFile = (LPSTR) buffer;
		polOfn->nMaxFile = 256;
		strcpy(buffer2, GetString(IDS_STRING1077));
		polOfn->lpstrTitle = buffer2;

		bmCommonDlg = true;
		if(GetOpenFileName(polOfn) == TRUE)
		{
			bmCommonDlg = false;
			char pclDefPath[512];
			strcpy(pclDefPath, polOfn->lpstrFile);
			int ilPathlen = strlen(pclDefPath);
			//ilPathlen = ilPathlen -4;
			pclDefPath[(ilPathlen)] = '\0';
			for(int iLen = ilPathlen; iLen > 0; iLen--)
			{
				if(pclDefPath[iLen] == '\\')
				{
/*					if((ilPathlen - iLen) > 8)	// Dateiname zu lang
					{
						pclDefPath[ilPathlen - (ilPathlen - iLen) + 9] = '\0';
					}*/
					break;
				}
			}
			if ((strstr(pclDefPath, ".")) == 0)
				strcat(pclDefPath, ".txt");

			switch(miTable)
			{
			case 0:	//	List der Gefundenen Flugbewegungen
			{
				if ((ogCustomer == "SHA") && (ogAppName == "FIPS"))
 					pomReportSameALTableViewer->PrintPlanToFile(pclDefPath);
				else
					pomGefundenefluegeViewer->PrintPlanToFile(pclDefPath);
			}
			break;
			case 1:	//	Liste aller Abfl�ge
			{	
				if ((ogCustomer == "SHA") && (ogAppName == "FIPS"))
					pomReportSameORGTableViewer->PrintPlanToFile(pclDefPath);
				else
					pomAbfluegeViewer->PrintPlanToFile(pclDefPath);
			}
			break;
			case 2:	//	Liste aller Fl�ge in einem Zeitraum
			{
				if ((ogCustomer == "SHA") && (ogAppName == "FIPS"))
					pomReportSameREGNTableViewer->PrintPlanToFile(pclDefPath);
				else
					pomFluegezeitraumViewer->PrintPlanToFile(pclDefPath);
			}
			break;
			case 3:	//	Messefl�ge im Daystop
			{
				if ((ogCustomer == "SHA") && (ogAppName == "FIPS"))
					pomReportSameDESTableViewer->PrintPlanToFile(pclDefPath);
				else
				{
					pomMessedaystopViewer->PrintPlanToFile(pclDefPath);
				}
			}
			break;
			case 4:	//	Liste aller Messefl�ge
			{
				if ((ogCustomer == "SHA") && (ogAppName == "FIPS"))
					pomReportSameACTTableViewer->PrintPlanToFile(pclDefPath);
				else
					pomAllemessefluegeViewer->PrintPlanToFile(pclDefPath);
			}
			break;
			case 5:	//	Statistik Messefl�ge (Abfl�ge)
			{
				if ((ogCustomer == "SHA") && (ogAppName == "FIPS"))
					pomReportSameTTYPTableViewer->PrintPlanToFile(pclDefPath);
				else
					pomMessefluegeViewer->PrintPlanToFile(pclDefPath);
			}
			break;
			case 6:	//	Statistik Messefl�ge im Daystop
			{
				if ((ogCustomer == "SHA") && (ogAppName == "FIPS"))
					pomReportSameDCDTableViewer->PrintPlanToFile(pclDefPath);
				else
					pomStatistikMesseViewer->PrintPlanToFile(pclDefPath);
			}
			break;
			case 7:	//	Statistik Liste aller Abfl�ge pro LVG
			{
				if ((ogCustomer == "SHA") && (ogAppName == "FIPS"))
					pomReportSameDTDTableViewer->PrintPlanToFile(pclDefPath);
				else
					pomAbfluegelvgViewer->PrintPlanToFile(pclDefPath);
			}
			break;
			case 8:	//	Fluggesellschaften eines Saisonflugplans
			{
				if ((ogCustomer == "SHA") && (ogAppName == "FIPS"))
					pomReportSameFTYPXTableViewer->PrintPlanToFile(pclDefPath);
				else
					pomSaisonflugplanlvgViewer->PrintPlanToFile(pclDefPath);
			}
			break;
			case 9:	//	Liste aller angeflogenen Flugh�fen mit Frequenz
			{
				pomFlughaefenViewer->PrintPlanToFile(pclDefPath);
			}
			break;
			case 10:	//	Arbeitsflugplan
			{
				if ((ogCustomer == "SHA") && (ogAppName == "FIPS"))
					pomReportArrDepTableViewer->PrintPlanToFile(pclDefPath);
				else
					pomArbeitsflugplanViewer->PrintPlanToFile(pclDefPath);
			}
			break;
			case 11:	//	Sitzplatzangebot nach LVGs und/oder Destinationen
			{
				pomSitzplaetzeViewer->PrintPlanToFile(pclDefPath);
			}
			break;
			case 12:	//	Sitzplatzangebot nach LVGs 
			{
				pomLvgViewer->PrintPlanToFile(pclDefPath);
			}
			break;
			case 13:	//	Sitzplatzangebot nach Destinationen
			{
				if ((ogCustomer == "SHA") && (ogAppName == "FIPS"))
					pomReportTimeFrameTableViewer->PrintPlanToFile(pclDefPath);
				else
					pomDesViewer->PrintPlanToFile(pclDefPath);
			}
			break;
			case 14:	//	Wochenplan
			{
				pomReport15Viewer->PrintPlanToFile(pclDefPath);
			}
			break;
/*
			//15-L14-inventory (a/c on ground)
			case 15:
			{
				if (pomReport16Viewer->PrintPlanToFile(pclTrenner))
					olFileName = pomReport16Viewer->omFileName;


				bool test = true; //only for testing error
				if (olFileName.IsEmpty() || olFileName.GetLength() > 255 || !test)
				{
					if (olFileName.IsEmpty())
					{
						CString mess = "Filename is empty !" + olFileName;
						CFPMSApp::MyTopmostMessageBox(this, mess, GetString(ST_FEHLER), MB_ICONERROR);
						return;
					}
					if (olFileName.GetLength() > 255 )
					{
						CString mess = "Length of the Filename > 255: " + olFileName;
						CFPMSApp::MyTopmostMessageBox(this, mess, GetString(ST_FEHLER), MB_ICONERROR);
						return;
					}

					CString mess = "Filename invalid: " + olFileName;
					CFPMSApp::MyTopmostMessageBox(this, mess, GetString(ST_FEHLER), MB_ICONERROR);
					return;
				}

				char pclTmp[256];
				strcpy(pclTmp, olFileName); 

				char *args[4];
				args[0] = "child";
				args[1] = pclTmp;
				args[2] = NULL;
				args[3] = NULL;

				_spawnv( _P_NOWAIT , pclExcelPath, args );

			}
			break;
*/

			case 15:	//	Inventar
			{
				if (blSaveTxt)
					pomReport16Viewer->PrintPlanToFile(pclDefPath);
			}
			break;

			case 18:	// POPS DailySeason
			{
				pomReport17Viewer->PrintPlanToFile(pclDefPath);
			}
			break;

			case 17:	//	Daily Pl�ne: aktuell out of reach !!
			{
				Beep( 600, 100 ) ;
			}
			break;
			case 19:	//  Daily Counter-Plan
			{
				pomReport19Viewer->PrintPlanToFile(pclDefPath);
			}
			break;
			case 20:	//	OvernightStatistic
			{
				pomReportOvernightTableViewer->PrintPlanToFile(pclDefPath);
			}
			break;
			case 21:	//	OvernightStatistic
			{
				pomReportIntDomMixTableViewer->PrintPlanToFile(pclDefPath);
			}
			break;
			case 22:	//	OvernightStatistic
			{
				pomReportSeatIntDomMixTableViewer->PrintPlanToFile(pclDefPath);
			}
			break;
			case 23:	//	Arbeitsflugplan mit load & pax
			{
				pomReportArrDepLoadPaxTableViewer->PrintPlanToFile(pclDefPath);
			}
			break;
			case 24:	//	Flugplan mit GPU-Daten
			{
				pomReportFlightGPUTableViewer->PrintPlanToFile(pclDefPath);
			}
			break;
			case 25:	//	Baggage belt allocation schedule
			{
				pomReportBltAllocViewer->PrintPlanToFile(pclDefPath);
			}
			break;			
			case 26:	//	Daily flight log
			{
				pomReportDailyFlightLogViewer->PrintPlanToFile(pclDefPath);
			}
			break;
			case 27:	//	Delay codes
			{
				pomReportFlightDelayViewer->PrintPlanToFile(pclDefPath);
			}
			break;
			}

		}
		else
			bmCommonDlg = false;

		delete polOfn;

	}

}

void CReportTableDlg::OnPapier() 
{
	;
}

void CReportTableDlg::OnBeenden() 
{

	if (!bmCommonDlg)
		CDialog::OnCancel();

}

void CReportTableDlg::OnCancel() 
{

	if (!bmCommonDlg)
		CDialog::OnCancel();

}
