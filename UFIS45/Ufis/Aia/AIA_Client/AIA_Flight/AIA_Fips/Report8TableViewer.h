#ifndef __ABFLUEGELVGTABLEVIEWER_H__
#define __ABFLUEGELVGTABLEVIEWER_H__

#include "stdafx.h"
#include "RotationDlgCedaFlightData.h"
#include "CCSTable.h"
#include "CViewer.h"
#include "CCSPrint.h"


struct ABFLUEGELVGTABLE_LINEDATA
{
	long		Urno; 	// Eindeutige Datensatz-Nr.
	CString		Cntr;	// Anzahl der Bewegungen
	CString 	Alc2; 	// Fluggesellschaft (Airline 2-Letter Code)
	CString 	Alc3; 	// Fluggesellschaft (Airline 3-Letter Code)
	CString		Des3;	// Bestimmungsflughafen 3-Lettercode
	CString 	Gsmt; 	// Summe Abfl�ge der Verkehrsart

};

/////////////////////////////////////////////////////////////////////////////
// AbfluegelvgTableViewer

class AbfluegelvgTableViewer : public CViewer
{
// Constructions
public:
    AbfluegelvgTableViewer(CCSPtrArray<ROTATIONDLGFLIGHTDATA> *popData, char *pcpInfo = NULL);
    ~AbfluegelvgTableViewer();

    void Attach(CCSTable *popAttachWnd);
    virtual void ChangeViewTo(const char *pcpViewName);
// Internal data processing routines
private:
    void MakeLines();
	void MakeLine(CString opNewstr, CString opNewLvg, int ipCntr);
	void MakeResultLine(CString opNewstr, CString opNewLvg, int ipCntr);
// Operations
public:
	void DeleteAll();
	void CreateLine(ABFLUEGELVGTABLE_LINEDATA *prpAbfluegelvg);
	void DeleteLine(int ipLineno);
// Window refreshing routines
public:
	void UpdateDisplay();
	void PrintPlanToFile(char *pcpDefPath);
// Attributes used for filtering condition
private:
	bool bmIsFromSearch;
	int imLineno;
// Attributes
private:
    CCSTable *pomAbfluegelvgTable;
	CCSPtrArray<ROTATIONDLGFLIGHTDATA> *pomData;
public:
    CCSPtrArray<ABFLUEGELVGTABLE_LINEDATA> omLines;
///////
	CCSPtrArray<TABLE_HEADER_COLUMN> omHeaderDataArray;
//Print 
	void PrintTableView();
	bool PrintTableLine(ABFLUEGELVGTABLE_LINEDATA *prpLine,bool bpLastLine);
	bool PrintTableHeader();
	CCSPrint *pomPrint;
	char *pcmInfo;
	CString omTableName;
	CString omFooterName;

};

#endif //__ABFLUEGELVGTABLEVIEWER_H__
