#ifndef __REPORT_SAME_DES_TABLE_VIEWER_H__
#define __REPORT_SAME_DES_TABLE_VIEWER_H__

#include "stdafx.h"
#include "RotationDlgCedaFlightData.h"
#include "CCSTable.h"
#include "CViewer.h"
#include "CCSPrint.h"


struct SAMEDESTABLE_LINEDATA
{
	long AUrno;
	long ARkey;
	CString		ADate;
	CTime 		AEtai; 	
	CString		AFlno;
	CTime 		ALand; 	
	CString		AOrg3;
	CString		AOrg4;
	CTime		AStoa;
	CString		AVia3;
	CString 	AVian;
	CString		AAct;


	long DUrno;
	long DRkey;
	CString		DAct;
	CTime 		DAirb;
	CString		DDate;
	CString		DDes3;
	CString		DDes4;
	CTime 		DEtdi;
	CString		DFlno;
	CTime		DStod;
	CString		DVia3;
	CString 	DVian;

};

/////////////////////////////////////////////////////////////////////////////
// ReportSameDESTableViewer

class ReportSameDESTableViewer : public CViewer
{
// Constructions
public:
    ReportSameDESTableViewer(CCSPtrArray<ROTATIONDLGFLIGHTDATA> *popData, char *pcpInfo = NULL, char *pcpSelect = NULL);
    ~ReportSameDESTableViewer();

    void Attach(CCSTable *popAttachWnd);
	void SetParentDlg(CDialog* ppDlg);
    virtual void ChangeViewTo(const char *pcpViewName);
	void PrintPlanToFile(char *pcpDefPath);
// Internal data processing routines
private:
    void MakeLines();
	int  MakeLine(ROTATIONDLGFLIGHTDATA *prpAFlight, ROTATIONDLGFLIGHTDATA *prpDFlight);
	void MakeLineData(ROTATIONDLGFLIGHTDATA *prpAFlight, ROTATIONDLGFLIGHTDATA *prpDFlight, SAMEDESTABLE_LINEDATA &rpLine);
	void MakeColList(SAMEDESTABLE_LINEDATA *prlLine, CCSPtrArray<TABLE_COLUMN> &olColList);
	int  CompareGefundenefluege(SAMEDESTABLE_LINEDATA *prpGefundenefluege1, SAMEDESTABLE_LINEDATA *prpGefundenefluege2);
	int  CompareFlight(SAMEDESTABLE_LINEDATA *prpFlight1, SAMEDESTABLE_LINEDATA *prpFlight2);
	int  GetVia(CCSPtrArray<VIADATA> *opVias, ROTATIONDLGFLIGHTDATA *prpFlight);

// Operations
public:
	void DeleteAll();
	int CreateLine(SAMEDESTABLE_LINEDATA &prpGefundenefluege);
	void DeleteLine(int ipLineno);
// Window refreshing routines
public:
	void UpdateDisplay();
	void DrawHeader();


// Attributes used for filtering condition
private:
	bool bmIsFromSearch;
    CCSTable *pomTable;
	CDialog* pomParentDlg;
	CCSPtrArray<ROTATIONDLGFLIGHTDATA> *pomData;
public:
    CCSPtrArray<SAMEDESTABLE_LINEDATA> omLines;
	CCSPtrArray<TABLE_HEADER_COLUMN> omHeaderDataArray;
//Print 
	void GetHeader(void); 
	void PrintTableView(void);
	bool PrintTableLine(SAMEDESTABLE_LINEDATA *prpLine,bool bpLastLine);
	bool PrintTableHeader(void);
	CCSPtrArray <TABLE_HEADER_COLUMN> omPrintHeadHeaderArray;
	CCSPrint *pomPrint;
	CString omTableName;
	char *pcmInfo;
	char *pcmSelect;
	CString omFooterName;
};

#endif //__ReportSameDESTableViewer_H__
