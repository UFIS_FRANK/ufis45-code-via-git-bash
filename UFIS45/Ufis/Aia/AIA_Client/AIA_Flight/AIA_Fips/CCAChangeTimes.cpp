// CCAChangeTimes.cpp : implementation file
//

#include "stdafx.h"
#include "fpms.h"
#include "CCAChangeTimes.h"
#include "CCSGlobl.h"
#include "BasicData.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CCAChangeTimes dialog


CCAChangeTimes::CCAChangeTimes(CWnd* pParent, const CString &ropCaption, bool bpLocalTimes, CTime opStart /* = TIMENULL */, CTime opEnd /* = TIMENULL */)
	: CDialog(CCAChangeTimes::IDD, pParent)
{
	omCkbs = opStart;
	omCkes = opEnd;

	omCaption = ropCaption;
	bmLocalTimes = bpLocalTimes;

	// given times must be in UTC!
	if (bmLocalTimes)
	{
		omCaption += " (LOCAL TIMES)";
		ogBasicData.UtcToLocal(omCkbs);
		ogBasicData.UtcToLocal(omCkes);
	}
	else
	{
		omCaption += " (UTC TIMES)";
	}


	//{{AFX_DATA_INIT(CCAChangeTimes)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void CCAChangeTimes::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CCAChangeTimes)
	DDX_Control(pDX, IDC_CKES, m_CE_Ckes);
	DDX_Control(pDX, IDC_CKBS, m_CE_Ckbs);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CCAChangeTimes, CDialog)
	//{{AFX_MSG_MAP(CCAChangeTimes)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCAChangeTimes message handlers

void CCAChangeTimes::OnOK() 
{
	CString olCkbsStr;	
	CString olCkesStr;	

	m_CE_Ckes.GetWindowText(olCkesStr);
	m_CE_Ckbs.GetWindowText(olCkbsStr);

	omCkes = HourStringToDate(olCkesStr);
	omCkbs = HourStringToDate(olCkbsStr);


	if( !m_CE_Ckes.GetStatus() || !m_CE_Ckbs.GetStatus() || ( omCkes == TIMENULL && omCkbs == TIMENULL) )	
	{
		MessageBox(GetString(IDS_STRING947), GetString(IDS_WARNING), MB_OK);
		return;
	}

	// resulting times are always in UTC!
	if (bmLocalTimes)
	{
		ogBasicData.LocalToUtc(omCkbs);
		ogBasicData.LocalToUtc(omCkes);
	}


	CDialog::OnOK();
}

BOOL CCAChangeTimes::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	m_CE_Ckes.SetTypeToTime();
	m_CE_Ckbs.SetTypeToTime();

	SetWindowText(omCaption);	


	if(omCkes != TIMENULL)
		m_CE_Ckes.SetWindowText(omCkes.Format("%H:%M"));

	if(omCkbs != TIMENULL)
		m_CE_Ckbs.SetWindowText(omCkbs.Format("%H:%M"));



	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}
