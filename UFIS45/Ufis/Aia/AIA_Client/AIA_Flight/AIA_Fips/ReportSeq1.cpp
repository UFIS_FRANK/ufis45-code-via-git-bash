// ReportSeq1.cpp : implementation file
//

#include "stdafx.h"
#include "CCSGlobl.h"
#include "fpms.h"
#include "ReportSeq1.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// ReportSeq1 dialog


ReportSeq1::ReportSeq1(CWnd* pParent, CString opHeadline, CTime* oMinDate, CTime* oMaxDate)
	: CDialog(ReportSeq1::IDD, pParent)
{
	//{{AFX_DATA_INIT(ReportSeq1)
	m_Datumvon = _T("");
	//}}AFX_DATA_INIT
	omHeadline = opHeadline;	//Dialogbox-Überschrift
	pomMinDate = oMinDate;

}
	

void ReportSeq1::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(ReportSeq1)
	DDX_Control(pDX, IDC_DATUMVON, m_CE_Datumvon);
	DDX_Text(pDX, IDC_DATUMVON, m_Datumvon);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(ReportSeq1, CDialog)
	//{{AFX_MSG_MAP(ReportSeq1)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// ReportSeq1 message handlers

BOOL ReportSeq1::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	CDialog::SetWindowText(omHeadline);	//Dialogbox-Überschrift
	m_CE_Datumvon.SetTypeToDate();
	m_CE_Datumvon.SetBKColor(YELLOW);

	m_Datumvon = pomMinDate->Format("%d.%m.%Y");
	m_CE_Datumvon.SetWindowText( m_Datumvon );

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}


void ReportSeq1::OnOK() 
{

	UpdateData(true);

	m_CE_Datumvon.GetWindowText(m_Datumvon);
	// ist nur ein Time Feld gefüllt, übernimmt das andere dessen Inhalt
	if(m_Datumvon.IsEmpty())
		m_CE_Datumvon.GetWindowText(m_Datumvon);

	*pomMinDate = DateStringToDate(m_Datumvon);

	if (m_Datumvon.IsEmpty())
		MessageBox(GetString(IDS_STRING918), GetString(IMFK_REPORT), MB_OK);
	else
	{
		if((m_CE_Datumvon.GetStatus()) && (m_CE_Datumvon.GetStatus()))
		{
			CDialog::OnOK();
		}
		else
			MessageBox(GetString(IDS_STRING934), GetString(IMFK_REPORT), MB_OK);
	}
}


void ReportSeq1::OnCancel() 
{
	// TODO: Zusätzlichen Bereinigungscode hier einfügen
	
	CDialog::OnCancel();
}

