// StatistikMesseTableViewer.cpp 
//
//	Statistik Messefl�ge (Ank�nfte)

#include "stdafx.h"
#include "Report7TableViewer.h"
#include "CcsGlobl.h"

#include <iomanip.h>
#include <iostream.h>
#include <fstream.h>
#include "BasicData.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif


static int CompareToTifa(const ROTATIONDLGFLIGHTDATA **e1, const ROTATIONDLGFLIGHTDATA **e2)
{

	return (((**e1).Tifa == (**e2).Tifa) ? 0 : (((**e1).Tifa > (**e2).Tifa) ? 1 : -1));
}

// StatistikMesseTableViewer
//
StatistikMesseTableViewer::StatistikMesseTableViewer(CCSPtrArray<ROTATIONDLGFLIGHTDATA> *popData, char *pcpInfo, char *pcpSelect)
{
	pomData = popData;
	pcmInfo = pcpInfo;
	pcmSelect = pcpSelect;

	bmIsFromSearch = FALSE;
    pomStatistikMesseTable = NULL;
}

//-----------------------------------------------------------------------------------------------

StatistikMesseTableViewer::~StatistikMesseTableViewer()
{
    DeleteAll();
	omHeaderDataArray.DeleteAll();
}

//-----------------------------------------------------------------------------------------------

void StatistikMesseTableViewer::Attach(CCSTable *popTable)
{
    pomStatistikMesseTable = popTable;
}

//-----------------------------------------------------------------------------------------------
void StatistikMesseTableViewer::ChangeViewTo(int *piCnt)
{
	pimCnt = piCnt;
	DeleteAll();    
	PrepareLines();
	MakeLines();
	UpdateDisplay();
}

//-----------------------------------------------------------------------------------------------
void StatistikMesseTableViewer::PrepareLines()
{
	ROTATIONDLGFLIGHTDATA *prlAFlight;
	ROTATIONDLGFLIGHTDATA *prlDFlight;
	ROTATIONDLGFLIGHTDATA *prlNextFlight = NULL;
	ROTATIONDLGFLIGHTDATA *prlFlight;
    STATISTIKMESSETABLE_LINEDATA rlLine;

	bool blRDeparture = false;
	int ilSpan = atoi(pcmSelect);

	// verkn�pfte Fl�ge werden zusammengestellt

	int ilFlightCount = pomData->GetSize();

	for (int ilLc = 0; ilLc < ilFlightCount; ilLc++)
	{
		prlFlight = &(*pomData)[ilLc];

		if(ilLc + 1 < ilFlightCount)
			prlNextFlight = &(*pomData)[ilLc + 1];	
		else
			prlNextFlight = NULL;	

		//Arrival
		if((strcmp(prlFlight->Org3, pcgHome) != 0) && (strcmp(prlFlight->Des3, pcgHome) == 0))
		{

			prlAFlight = prlFlight;
			prlDFlight = NULL;
			if(prlNextFlight != NULL)
			{
				if((prlNextFlight->Rkey == prlFlight->Rkey) && (prlFlight->Rkey != NULL))
				{
					prlDFlight = prlNextFlight;

					if((strcmp(prlNextFlight->Org3, pcgHome) == 0) && (strcmp(prlNextFlight->Des3, pcgHome) != 0))
					{
						ilLc++;
					}
					else
						blRDeparture = true;
				}
			}
		}
		else
		{
			// Departure
			if((strcmp(prlFlight->Org3, pcgHome) == 0) && (strcmp(prlFlight->Des3, pcgHome) != 0))
			{
				
				prlAFlight = NULL;
				prlDFlight = prlFlight;
				blRDeparture = false;
			}
			else
			{
				//Turnaround
				if((strcmp(prlFlight->Org3, pcgHome) == 0) && (strcmp(prlFlight->Des3, pcgHome) == 0))
				{
					if(blRDeparture)
					{
						blRDeparture = false;
						prlAFlight = prlFlight;
						prlDFlight = NULL;
						if(prlNextFlight != NULL)
						{
							if((prlNextFlight->Rkey == prlFlight->Rkey) && (prlFlight->Rkey != NULL))
							{
								prlDFlight = prlNextFlight;
								if((strcmp(prlNextFlight->Org3, pcgHome) == 0) && (strcmp(prlNextFlight->Des3, pcgHome) != 0))
								{
									ilLc++;
								}
								else
									blRDeparture = true;
							}
						}
					}
					else
					{
						prlAFlight		= prlFlight;
						prlDFlight		= prlFlight;
						if(prlNextFlight != NULL)
						{
							if((prlNextFlight->Rkey == prlFlight->Rkey) && (prlFlight->Rkey != NULL))
							{
								ilLc--;
								prlAFlight	 = NULL;
								blRDeparture = true;
							}
						}
					}
				}
			}
		}
		//ilLineNo = MakeLine(prlAFlight, prlDFlight);
		
		if((prlAFlight != NULL) && (prlDFlight != NULL))
		{
			CTimeSpan olDiv = prlDFlight->Stod - prlAFlight->Stoa;
			int ilTest = olDiv.GetTotalMinutes();

			if ((olDiv.GetTotalMinutes()) < ilSpan)	
			{
				pomData->DeleteAt(ilLc-1);
				pomData->DeleteAt(ilLc-1);
				ilLc = ilLc - 2;
				ilFlightCount = ilFlightCount - 2;
			}
		}

	}

	pomData->Sort(CompareToTifa);	

}

//-----------------------------------------------------------------------------------------------

void StatistikMesseTableViewer::MakeLines()
{
	CTime olTifaold;
	int ilCntr = 0;
	int ilAcnt = 0;
	bool blFirst = TRUE;
	CString olNewstr("");
	CString olOldstr(""); 

	int ilCount = pomData->GetSize();

	for (int i = 0; i < ilCount; i++)
	{
		ROTATIONDLGFLIGHTDATA *prlStatistikMesseData = &pomData->GetAt(i);
		if(!strcmp(prlStatistikMesseData->Adid,"A"))	// nur Ankunftsfl�ge anzeigen
			ilAcnt++;
	}
	MakeResultLine(ilAcnt);
	*pimCnt = ilAcnt;

	for (int ilLc = ilCount - 1; ilLc >= 0 ; ilLc--)
	{
		ROTATIONDLGFLIGHTDATA *prlStatistikMesseData = &pomData->GetAt(ilLc);

		if(!strcmp(prlStatistikMesseData->Adid,"A"))	// nur Ankunftsfl�ge anzeigen
		{
			// ilCntr++;

			if(bgReportLocal)
				ogBasicData.UtcToLocal(prlStatistikMesseData->Tifa);
			olNewstr = prlStatistikMesseData->Tifa.Format( "%Y%m%d" );

			if(blFirst == TRUE)	// der erste
			{
				olOldstr = olNewstr;
				olTifaold = prlStatistikMesseData->Tifa;
				blFirst = FALSE;
			}
			if (olNewstr != olOldstr)	// Datum wechselt
			{
				MakeLine(olTifaold, ilCntr);
				olTifaold = prlStatistikMesseData->Tifa;
				olOldstr = olTifaold.Format( "%Y%m%d" );
				ilCntr = 0;
			}
			
			// hbe am 2/11/98 
			ilCntr++;

		}

		if((ilLc == 0) && (ilCntr > 0))	// nach dem letzten
		{
			MakeLine(prlStatistikMesseData->Tifa, ilCntr);
		}


	}
}

//-----------------------------------------------------------------------------------------------

void StatistikMesseTableViewer::MakeLine(CTime opTifaold, int ipCntr)
{
    // Update viewer data for this shift record
	char clbuffer[10];
    STATISTIKMESSETABLE_LINEDATA rlStatistikMesse;
	sprintf(clbuffer,"%d", ipCntr);
	rlStatistikMesse.Cntr = clbuffer; 
	rlStatistikMesse.Tifa = opTifaold.Format("%d.%m.%Y"); 

	CreateLine(&rlStatistikMesse);
}
//-----------------------------------------------------------------------------------------------

void StatistikMesseTableViewer::MakeResultLine(int ilCount)
{
    // Update viewer data for this shift record
	char clbuffer[10];
    STATISTIKMESSETABLE_LINEDATA rlStatistikMesse;
	sprintf(clbuffer,"%d", ilCount);
	rlStatistikMesse.Cntr = clbuffer; 
	rlStatistikMesse.Tifa = GetString(IDS_STRING1200);//"Gesamt: "; 

	CreateLine(&rlStatistikMesse);
}

//-----------------------------------------------------------------------------------------------

void StatistikMesseTableViewer::CreateLine(STATISTIKMESSETABLE_LINEDATA *prpStatistikMesse)
{

	int ilLineno = 0;
	STATISTIKMESSETABLE_LINEDATA rlStatistikMesse;
	rlStatistikMesse = *prpStatistikMesse;
    omLines.NewAt(ilLineno, rlStatistikMesse);

}

//-----------------------------------------------------------------------------------------------
// UpdateDisplay: Load data selected by filter conditions to the display by using "omTable"

void StatistikMesseTableViewer::UpdateDisplay()
{
	int ilLines = omLines.GetSize();
	const int ilColumns = 2;	//<===== Spaltenanzahl setzen (von 0 ab)
	omHeaderDataArray.DeleteAll();

	pomStatistikMesseTable->SetShowSelection(TRUE);
	pomStatistikMesseTable->ResetContent();

	TABLE_HEADER_COLUMN rlHeader;
	rlHeader.Alignment = COLALIGN_LEFT;
	rlHeader.Font = &ogCourier_Regular_10;
	rlHeader.Length = 120;
	rlHeader.Text = GetString(IDS_STRING332);
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 70; 
	rlHeader.Text = GetString(IDS_STRING1133);
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);

	pomStatistikMesseTable->SetHeaderFields(omHeaderDataArray);

	pomStatistikMesseTable->SetDefaultSeparator();
	pomStatistikMesseTable->SetTableEditable(FALSE);

	TABLE_COLUMN rlColumnData;
	rlColumnData.Font = &ogCourier_Regular_10;
	rlColumnData.BkColor = WHITE;
	rlColumnData.Alignment = COLALIGN_LEFT;
	
    for (int ilLineNo = 0; ilLineNo < ilLines; ilLineNo++)
	{
		CCSPtrArray<TABLE_COLUMN> olColList;

		rlColumnData.Text = omLines[ilLineNo].Tifa;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Cntr;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		pomStatistikMesseTable->AddTextLine(olColList,(void*)NULL);
		olColList.DeleteAll();
	}

	pomStatistikMesseTable->DisplayTable();
}


//-----------------------------------------------------------------------------------------------

void StatistikMesseTableViewer::DeleteLine(int ipLineno)
{
	omLines.DeleteAt(ipLineno);
}

//-----------------------------------------------------------------------------------------------

void StatistikMesseTableViewer::DeleteAll()
{
    while (omLines.GetSize() > 0)
    DeleteLine(0);
}

//-----------------------------------------------------------------------------------------------
// Printing Routines
//-----------------------------------------------------------------------------------------------

void StatistikMesseTableViewer::PrintTableView()
{
	CString olFooter1,olFooter2;
	omTableName = GetString(IDS_STRING1053);
	char pclFooter[100];
	//sprintf(pclInfo, "Statistik Messefl�ge im Daystop (Ank�nfte) %s", pcmInfo);
	sprintf(pclFooter, GetString(IDS_STRING1134), (CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M")));
	omFooterName = pclFooter;
	int ilOrientation = PRINT_PORTRAET;//  PRINT_PORTRAET  PRINT_LANDSCAPE
	pomPrint = new CCSPrint((CWnd*)this,ilOrientation,45);// FirstLine(P=200,L=200),LeftOffset(P=200,L=50)

	if (pomPrint != NULL)
	{
		if (pomPrint->InitializePrinter(ilOrientation) == TRUE)
		{
			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
			//pomPrint->imLineNo = pomPrint->imMaxLines + 1;
			pomPrint->imLineNo = pomPrint->imMaxLines + 1 - 2;
			DOCINFO	rlDocInfo;
			memset(&rlDocInfo, 0, sizeof(DOCINFO));
			rlDocInfo.cbSize = sizeof( DOCINFO );
			rlDocInfo.lpszDocName = omTableName;	
			pomPrint->omCdc.StartDoc( &rlDocInfo );
			pomPrint->imPageNo = 0;
			int ilLines = omLines.GetSize();
			olFooter1.Format("Tage: %d %s",(ilLines-1),omFooterName);
			for(int i = 0; i < ilLines; i++ ) 
			{
				//if(pomPrint->imLineNo >= pomPrint->imMaxLines)
				if(pomPrint->imLineNo >= pomPrint->imMaxLines-2)
				{
					if(pomPrint->imPageNo > 0)
					{
						olFooter2.Format(GetString(IDS_STRING1198),pomPrint->imPageNo);
						pomPrint->PrintUIFFooter(olFooter1,"",olFooter2);
						pomPrint->omCdc.EndPage();
					}
					PrintTableHeader();
				}
				//if(pomPrint->imLineNo == (pomPrint->imMaxLines-1) || i == (ilLines-1))
				if(pomPrint->imLineNo == (pomPrint->imMaxLines-3) || i == (ilLines-1))
				{
					PrintTableLine(&omLines[i],true);
				}
				else
				{
					PrintTableLine(&omLines[i],false);
				}
			}
			olFooter2.Format(GetString(IDS_STRING1198),pomPrint->imPageNo);
			pomPrint->PrintUIFFooter(olFooter1,"",olFooter2);
			pomPrint->omCdc.EndPage();
			pomPrint->omCdc.EndDoc();
		}
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
	delete pomPrint;

	pomPrint = NULL;
	}
}

//-----------------------------------------------------------------------------------------------

bool StatistikMesseTableViewer::PrintTableHeader()
{
	pomPrint->omCdc.StartPage();
	pomPrint->imPageNo++;
	pomPrint->imLineNo = 0;

	char pclHeader[100];
	sprintf(pclHeader, GetString(IDS_STRING1135), pcmInfo);
	//CString olTableName(pclHeader);
	//mPrint->PrintUIFHeader(omTableName,CString(CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M")),pomPrint->imFirstLine-10);
	pomPrint->PrintUIFHeader("",CString(pclHeader),pomPrint->imFirstLine-10);

	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;

	rlElement.Alignment   = PRINT_LEFT;
	rlElement.FrameLeft   = PRINT_NOFRAME;
	rlElement.FrameRight  = PRINT_NOFRAME;
	rlElement.FrameTop    = PRINT_NOFRAME;
	rlElement.FrameBottom = PRINT_FRAMEMEDIUM;
	rlElement.pFont       = &pomPrint->ogCourierNew_Bold_8;

	int ilSize = omHeaderDataArray.GetSize();
	for(int i=0;i<ilSize;i++)
	{
		rlElement.Length = (int)(omHeaderDataArray[i].Length*dgCCSPrintFactor);
		if(rlElement.Length < igCCSPrintMinLength)rlElement.Length+=igCCSPrintMoreLength; 
		rlElement.Text   = omHeaderDataArray[i].Text;
		rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
	}
	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();


	return true;
}

//-----------------------------------------------------------------------------------------------

bool StatistikMesseTableViewer::PrintTableLine(STATISTIKMESSETABLE_LINEDATA *prpLine,bool bpLastLine)
{
	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;
	rlElement.pFont      = &pomPrint->ogCourierNew_Regular_8;
	int ilSize = omHeaderDataArray.GetSize();
	for(int i=0;i<ilSize;i++)
	{
		//if(i!=1&&i!=1)// <= setzen, wenn Spalten nicht angezeigt werden sollen 
		{
			rlElement.Length = (int)(omHeaderDataArray[i].Length*dgCCSPrintFactor);
			if(rlElement.Length < igCCSPrintMinLength)rlElement.Length+=igCCSPrintMoreLength; 
			rlElement.Alignment  = PRINT_LEFT;
			rlElement.FrameTop   = PRINT_FRAMETHIN;
			if(bpLastLine == true)
			{
				rlElement.FrameBottom = PRINT_FRAMETHIN;
			}
			else
			{
				rlElement.FrameBottom = PRINT_NOFRAME;
			}
			rlElement.FrameLeft  = PRINT_NOFRAME;
			rlElement.FrameRight = PRINT_NOFRAME;
			switch(i)
			{
			case 0:
				{
					rlElement.FrameLeft  = PRINT_FRAMETHIN;
					rlElement.Text		= prpLine->Tifa;
				}
				break;
			case 1:
				{
					rlElement.FrameRight = PRINT_FRAMETHIN;
					rlElement.Text		= prpLine->Cntr;
				}
				break;
			}
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
		}
	}
	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();
	return true;
}

//-----------------------------------------------------------------------------------------------


// Drucken in Datei
void StatistikMesseTableViewer::PrintPlanToFile(char *pcpDefPath)
{
	ofstream of;
	of.open( pcpDefPath, ios::out);
	of << GetString(IDS_STRING926) << " " 
		<< pcmInfo << "    "
		<< CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M") << endl << endl;

	of  << GetString(IDS_STRING332) << "       " 
		<< GetString(IDS_STRING1133) << "      " 
		<< endl;
	of << "---------------------------------------------------------------" << endl;
	int ilCount = omLines.GetSize();
	for(int i = 0; i < ilCount; i++)
	{
		STATISTIKMESSETABLE_LINEDATA rlD = omLines[i];
		of.setf(ios::left, ios::adjustfield);
		of   << setw(12) << rlD.Tifa  
			 << setw(12) << rlD.Cntr << endl;
	}
	of.close();
//	MessageBox("Es wurde folgende Datei geschrieben:\nc:\\tmp\\StatistikMesse.txt", "Drucken",(MB_ICONINFORMATION|MB_OK));
}

