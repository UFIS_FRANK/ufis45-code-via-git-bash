#ifndef __REPORT_SAME_TTYPX_TABLE_VIEWER_H__
#define __REPORT_SAME_TTYPX_TABLE_VIEWER_H__

#include "stdafx.h"
#include "RotationDlgCedaFlightData.h"
#include "CCSTable.h"
#include "CViewer.h"
#include "CCSPrint.h"


struct SAMETTYPXTABLE_LINEDATA
{
	long AUrno;
	long ARkey;
	CString		ADate;
	CTime 		AEtai; 	
	CString		AFlno;
	CTime 		ALand; 	
	CString		AOrg3;
	CString		AOrg4;
	CTime		AStoa;
	CString		AVia3;
	CString 	AVian;
	CString		AAct;


	long DUrno;
	long DRkey;
	CString		DAct;
	CTime 		DAirb;
	CString		DDate;
	CString		DDes3;
	CString		DDes4;
	CTime 		DEtdi;
	CString		DFlno;
	CTime		DStod;
	CString		DVia3;
	CString 	DVian;

	CString		Adid;

};

/////////////////////////////////////////////////////////////////////////////
// ReportSameTTYPXTableViewer

class ReportSameTTYPXTableViewer : public CViewer
{
// Constructions
public:
    ReportSameTTYPXTableViewer(CCSPtrArray<ROTATIONDLGFLIGHTDATA> *popData, char *pcpInfo = NULL, char *pcpSelect = NULL);
    ~ReportSameTTYPXTableViewer();

    void Attach(CCSTable *popAttachWnd);
    virtual void ChangeViewTo(const char *pcpViewName);
	void PrintPlanToFile(char *pcpDefPath);
// Internal data processing routines
private:
    void MakeLines();
	int  MakeLine(ROTATIONDLGFLIGHTDATA *prpAFlight, ROTATIONDLGFLIGHTDATA *prpDFlight);
	void MakeLineData(ROTATIONDLGFLIGHTDATA *prpAFlight, ROTATIONDLGFLIGHTDATA *prpDFlight, SAMETTYPXTABLE_LINEDATA &rpLine);
	void MakeColList(SAMETTYPXTABLE_LINEDATA *prlLine, CCSPtrArray<TABLE_COLUMN> &olColList);
	int  CompareGefundenefluege(SAMETTYPXTABLE_LINEDATA *prpGefundenefluege1, SAMETTYPXTABLE_LINEDATA *prpGefundenefluege2);
	int  CompareFlight(SAMETTYPXTABLE_LINEDATA *prpFlight1, SAMETTYPXTABLE_LINEDATA *prpFlight2);
	int  GetVia(CCSPtrArray<VIADATA> *opVias, ROTATIONDLGFLIGHTDATA *prpFlight);

// Operations
public:
	void DeleteAll();
	int CreateLine(SAMETTYPXTABLE_LINEDATA &prpGefundenefluege);
	void DeleteLine(int ipLineno);
// Window refreshing routines
public:
	void UpdateDisplay();
	void DrawHeader();
	char* GetDname();


// Attributes used for filtering condition
private:
	bool bmIsFromSearch;
	CString omDname;
// Attributes
private:
    CCSTable *pomTable;
	CCSPtrArray<ROTATIONDLGFLIGHTDATA> *pomData;

public:
    CCSPtrArray<SAMETTYPXTABLE_LINEDATA> omLines;
	CCSPtrArray<TABLE_HEADER_COLUMN> omHeaderDataArray;
//Print 
	void GetHeader(void); 
	void PrintTableView(void);
	bool PrintTableLine(SAMETTYPXTABLE_LINEDATA *prpLine,bool bpLastLine);
	bool PrintTableHeader(void);
	CCSPtrArray <TABLE_HEADER_COLUMN> omPrintHeadHeaderArray;
	CCSPrint *pomPrint;
	CString omTableName;
	char *pcmInfo;
	char *pcmSelect;
	CString omFooterName;

};

#endif //__ReportSameTTYPXTableViewer_H__
