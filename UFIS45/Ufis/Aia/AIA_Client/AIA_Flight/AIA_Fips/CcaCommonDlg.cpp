// CheckinModifyDlg.cpp: Implementierungsdatei
//

#include "stdafx.h"
//#include "fpms.h"
#include "CcaCommonDlg.h"
#include "PrivList.h"
#include "CedaBasicData.h"
#include  "BasicData.h"
#include "CcaCedaFlightData.h"
#include "AwBasicDataDlg.h"
#include "Utils.h"

#include "resrc1.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld CCcaCommonDlg 


CCcaCommonDlg::CCcaCommonDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CCcaCommonDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CCcaCommonDlg)
	m_Beginn_t = _T("");
	m_Beginn_d = _T("");
	m_ActStartTime = _T("");
	m_ActStartDate = _T("");
	m_Rem = _T("");
	m_Ende_d = _T("");
	m_Ende_t = _T("");
	m_ActEndTime = _T("");
	m_ActEndDate = _T("");
	m_Fluggs = _T("");
	m_Schalter = _T("");
	m_Terminal = _T("");
	m_Disp = _T("");
	//}}AFX_DATA_INIT

    CDialog::Create(CCcaCommonDlg::IDD, pParent);

}

CCcaCommonDlg::~CCcaCommonDlg()
{

}


void CCcaCommonDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CCcaCommonDlg)
	DDX_Control(pDX, IDC_DISP, m_CE_Disp);
	DDX_Control(pDX, IDC_TERMINAL, m_CE_Terminal);
	DDX_Control(pDX, IDC_SCHALTER, m_CE_Schalter);
	DDX_Control(pDX, IDC_FLUGGS, m_CE_Fluggs);
	DDX_Control(pDX, IDC_ENDE_T, m_CE_Ende_t);
	DDX_Control(pDX, IDC_ENDE_D, m_CE_Ende_d);
	DDX_Control(pDX, IDC_ACT_END_TIME, m_CE_ActEndTime);
	DDX_Control(pDX, IDC_ACT_END_DATE, m_CE_ActEndDate);
	DDX_Control(pDX, IDC_BEMERKUNG, m_CE_Rem);
	DDX_Control(pDX, IDC_BEGINN_D, m_CE_Beginn_d);
	DDX_Control(pDX, IDC_BEGINN_T, m_CE_Beginn_t);
	DDX_Control(pDX, IDC_ACT_START_DATE, m_CE_ActStartDate);
	DDX_Control(pDX, IDC_ACT_START_TIME, m_CE_ActStartTime);
	DDX_Control(pDX, IDC_FIDS_REM_LIST, m_CB_FidsRemList);
	DDX_Text(pDX, IDC_BEGINN_T, m_Beginn_t);
	DDX_Text(pDX, IDC_BEGINN_D, m_Beginn_d);
	DDX_Text(pDX, IDC_ACT_START_TIME, m_ActStartTime);
	DDX_Text(pDX, IDC_ACT_START_DATE, m_ActStartDate);
	DDX_Text(pDX, IDC_BEMERKUNG, m_Rem);
	DDX_Text(pDX, IDC_ENDE_D, m_Ende_d);
	DDX_Text(pDX, IDC_ENDE_T, m_Ende_t);
	DDX_Text(pDX, IDC_ACT_END_DATE, m_ActEndDate);
	DDX_Text(pDX, IDC_ACT_END_TIME, m_ActEndTime);
	DDX_Text(pDX, IDC_FLUGGS, m_Fluggs);
	DDX_Text(pDX, IDC_SCHALTER, m_Schalter);
	DDX_Text(pDX, IDC_TERMINAL, m_Terminal);
	DDX_Text(pDX, IDC_DISP, m_Disp);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CCcaCommonDlg, CDialog)
	//{{AFX_MSG_MAP(CCcaCommonDlg)
	ON_BN_CLICKED(IDC_FIDS_REM_LIST, OnFidsRemList)
	ON_WM_CLOSE()
	ON_MESSAGE(WM_EDIT_CHANGED, OnEditChanged)
	ON_WM_SIZE()
 	ON_WM_DESTROY()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Behandlungsroutinen f�r Nachrichten CCcaCommonDlg 
void CCcaCommonDlg::SaveToReg()
{
	BOOL ok = WriteDialogToReg((CWnd*) this, m_key);
}


void CCcaCommonDlg::OnDestroy() 
{
	SaveToReg();
	CDialog::OnDestroy();
}



BOOL CCcaCommonDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	m_key = "DialogPosition\\CommonCheckIn";
	
	m_CE_Schalter.SetTypeToString("X(5)",5,1);
	m_CE_Schalter.SetBKColor(YELLOW);

	m_CE_Terminal.SetBKColor(SILVER);

	/*

	if(omModi == "Datensatz �ndern")
	{
		m_CE_Fluggs.SetTypeToString("X(3)",3,2);//x# 
		m_CE_Fluggs.SetReadOnly();
		char clTmp[20];
		CString olAlc;
		sprintf(clTmp,"%d",omCCA.Flnu);
		bool blRet = ogBCD.GetField("ALT", "URNO", clTmp, "ALC2", olAlc);
		if(blRet == false)
			blRet = ogBCD.GetField("ALT", "URNO", clTmp, "ALC3", olAlc);
		m_CE_Fluggs.SetInitText(olAlc);
	}
	else
	*/
	{
		m_CE_Fluggs.SetTypeToString("X(3)",3,0);//x# 
		//m_CE_Fluggs.SetBKColor(YELLOW);
	}


	m_CE_Beginn_d.SetBKColor(YELLOW);	
	m_CE_Beginn_d.SetTypeToDate(true);
	m_CE_Beginn_t.SetBKColor(YELLOW);
	m_CE_Beginn_t.SetTypeToTime(true);
	m_CE_Ende_d.SetBKColor(YELLOW);			
	m_CE_Ende_d.SetTypeToDate(true);
	m_CE_Ende_t.SetBKColor(YELLOW);
	m_CE_Ende_t.SetTypeToTime(true);
	m_CE_ActStartTime.SetTypeToTime(true);
	m_CE_ActStartDate.SetTypeToDate(true);
	m_CE_ActEndTime.SetTypeToTime(true);
	m_CE_ActEndDate.SetTypeToDate(true);
	
	m_CE_Rem.SetTypeToString("X(60)",60,0);
	m_CE_Disp.SetTypeToString("X(60)",60,0);


	// DXB (Dubai) need other field descriptions at the moment (28.09.2000)
	if (strcmp(pcgHome, "DXB") == 0)
	{
		GetDlgItem(IDC_BEM_TEXT)->SetWindowText(GetString(IDS_STRING2004));
		GetDlgItem(IDC_DISP_TEXT)->SetWindowText(GetString(IDS_STRING2005));
		// and a special button for FidsRemark (02.11.2000)
		GetDlgItem(IDC_FIDS_REM_LIST)->ShowWindow(SW_SHOW);
	}

	m_resizeHelper.Init(this->m_hWnd);

	CRect olRect;
	this->GetWindowRect(&olRect);
	GetDialogFromReg(olRect, m_key);
	SetWindowPos(&wndTop, olRect.left, olRect.top, olRect.Width(), olRect.Height(), SWP_SHOWWINDOW);// | SWP_NOMOVE );


	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX-Eigenschaftenseiten sollten FALSE zur�ckgeben
}

void CCcaCommonDlg::OnSize(UINT nType, int cx, int cy)  
{
	CDialog::OnSize(nType, cx, cy);
	
	// TODO: Add your message handler code here
	m_resizeHelper.OnSize();
}

void CCcaCommonDlg::OnOK() 
{
	CString olErrorText;
	CString olKeineDaten = GetString(ST_NODATA);
	CString olNichtFormat = GetString(ST_BADFORMAT);
	bool ilStatus = true;
	if(m_CE_Schalter.GetStatus() == false)
	{
		ilStatus = false;
		if(m_CE_Schalter.GetWindowTextLength() == 0)
		{
			olErrorText += GetString(IDS_STRING1516) +  olKeineDaten;
		}
		else	
		{
			olErrorText += GetString(IDS_STRING1516) + olNichtFormat;
		}
	}
	if (ilStatus == true)
	{
		if(m_CE_Fluggs.GetStatus() == false)
		{
			ilStatus = false;
			if(m_CE_Fluggs.GetWindowTextLength() == 0)
			{
				olErrorText += GetString(IDS_STRING1517) +  olKeineDaten;
			}
			else
			{
				olErrorText += GetString(IDS_STRING1517) + olNichtFormat;
			}
		}
	}	
	
	// check scheduled times!!!
	if (ilStatus == true)
	{
		if(m_CE_Beginn_d.GetStatus() == false)
		{
			ilStatus = false;
			if(m_CE_Beginn_d.GetWindowTextLength() == 0)
			{
				olErrorText += GetString(IDS_STRING1365) + CString(" ") +  olKeineDaten;
			}
			else
			{
				olErrorText += GetString(IDS_STRING1365) + CString(" ") + olNichtFormat;
			}
		}
	}
	if (ilStatus == true)
	{
		if(m_CE_Beginn_t.GetStatus() == false)
		{
			ilStatus = false;
			if(m_CE_Beginn_t.GetWindowTextLength() == 0)
			{
				olErrorText += GetString(IDS_STRING1365) + CString(" ") +  olKeineDaten;
			}
			else
			{
				olErrorText += GetString(IDS_STRING1365) + CString(" ") + olNichtFormat;
			}
		}
	}
	if (ilStatus == true)
	{
		if(m_CE_Ende_d.GetStatus() == false)
		{
			ilStatus = false;
			if(m_CE_Ende_d.GetWindowTextLength() == 0)
			{
				olErrorText += GetString(IDS_STRING1366) + CString(" ") +  olKeineDaten;
			}
			else
			{
				olErrorText += GetString(IDS_STRING1366) + CString(" ") + olNichtFormat;
			}
		}
	}
	if (ilStatus == true)
	{
		if(m_CE_Ende_t.GetStatus() == false)
		{
			ilStatus = false;
			if(m_CE_Ende_t.GetWindowTextLength() == 0)
			{
				olErrorText += GetString(IDS_STRING1366) + CString(" ") +  olKeineDaten;
			}
			else
			{
				olErrorText += GetString(IDS_STRING1366) + CString(" ") + olNichtFormat;
			}
		}
	}

	
	// check actual times!!!
	if (ilStatus == true)
	{
		if(m_CE_ActStartDate.GetStatus() == false && m_CE_ActStartDate.GetWindowTextLength() != 0)
		{
			ilStatus = false;
			olErrorText += GetString(IDS_STRING2092) + CString(" ") + olNichtFormat;
		}
	}
	if (ilStatus == true)
	{
		if(m_CE_ActStartTime.GetStatus() == false && m_CE_ActStartTime.GetWindowTextLength() != 0)
		{
			ilStatus = false;
			olErrorText += GetString(IDS_STRING2092) + CString(" ") + olNichtFormat;
		}
	}
	if (ilStatus == true)
	{
		if(m_CE_ActEndDate.GetStatus() == false && m_CE_ActEndDate.GetWindowTextLength() != 0)
		{
			ilStatus = false;
			olErrorText += GetString(IDS_STRING2093) + CString(" ") + olNichtFormat;
		}
	}
	if (ilStatus == true)
	{
		if(m_CE_ActEndTime.GetStatus() == false && m_CE_ActEndTime.GetWindowTextLength() != 0)
		{
			ilStatus = false;
			olErrorText += GetString(IDS_STRING2093) + CString(" ") + olNichtFormat;
		}
	}



	if (ilStatus == true)
	{
		char clTmp[20];
		CString olTmp;
		bool blRet;
		m_CE_Beginn_d.GetWindowText(m_Beginn_d);
		m_CE_Beginn_t.GetWindowText(m_Beginn_t);
		m_CE_Ende_d.GetWindowText(m_Ende_d);
		m_CE_Ende_t.GetWindowText(m_Ende_t);
		m_CE_ActStartDate.GetWindowText(m_ActStartDate);
		m_CE_ActStartTime.GetWindowText(m_ActStartTime);
		m_CE_ActEndDate.GetWindowText(m_ActEndDate);
		m_CE_ActEndTime.GetWindowText(m_ActEndTime);
		m_CE_Schalter.GetWindowText(omCCA.Ckic,7);

		// Schaltername
		char clCkic[8];
		CString olCkit;
		strcpy(clCkic, omCCA.Ckic);
		blRet = ogBCD.GetField("CIC", "CNAM", clCkic, "TERM", olCkit); // �ber Checkin-Schalter Terminal holen
		if(blRet == true)
		{
			sprintf(clTmp, "%s", olCkit);
			strcpy(omCCA.Ckit, clTmp);
		}
		else	
		{
			ilStatus = false;
			olErrorText += GetString(IDS_STRING1367);
		}

		CString olAlc;

		// Fluggesellschaft
		m_CE_Fluggs.GetWindowText(olAlc);

		omCCA.Flnu = 0;
		
		if(!olAlc.IsEmpty())
		{
			blRet = ogBCD.GetField("ALT", "ALC2", olAlc, "URNO", olTmp);
			if(blRet == false)
				blRet = ogBCD.GetField("ALT", "ALC3", olAlc, "URNO", olTmp);


			if(!blRet)
			{
				ilStatus = false;
				olErrorText += GetString(IDS_STRING1368);
			}
			else
			{
				omCCA.Flnu = atol(olTmp);
			}
		}


		if (ilStatus == true)
		{

			strcpy(omCCA.Ctyp,"C");

			m_CE_Rem.GetWindowText(omCCA.Rema,61);
			m_CE_Disp.GetWindowText(omCCA.Disp,61);

			if (!strlen(omCCA.Rema))
				strcpy(omCCA.Rema, " ");

			bool blCcaOpen = omCCA.IsOpen();

			omCCA.Ckba = DateHourStringToDate(m_ActStartDate, m_ActStartTime);
			omCCA.Ckea = DateHourStringToDate(m_ActEndDate, m_ActEndTime);
			omCCA.Ckbs = DateHourStringToDate(m_Beginn_d, m_Beginn_t);
			omCCA.Ckes = DateHourStringToDate(m_Ende_d, m_Ende_t);




			if(omCCA.Ckbs < omCCA.Ckes && 
				(omCCA.Ckba < omCCA.Ckea || omCCA.Ckba == TIMENULL || omCCA.Ckea == TIMENULL))
			{
				SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));

				if(bgGatPosLocal) omCcaData.StructLocalToUtc(&omCCA, "");
				
				if(omCCA.Urno <= 0)
				{
					omCCA.Urno = ogBasicData.GetNextUrno();
					CCADATA *prlNewCca = new CCADATA;
					*prlNewCca = omCCA;
					omCcaData.Insert(prlNewCca);
					if (prlNewCca->IsOpen())
						prlNewCca->UpdateFIDSMonitor();
				}
				else
				{
					omCCA.IsChanged = DATA_CHANGED;
					omCcaData.Save(&omCCA);
					if (omCCA.IsOpen() != blCcaOpen)
						omCCA.UpdateFIDSMonitor();
				}

				SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
				ShowWindow(SW_HIDE);
				//CDialog::OnOK();
			}
			else
			{
				Beep(440,70);
				MessageBox(GetString(IDS_STRING1518),GetString(ST_FEHLER),MB_ICONEXCLAMATION);
				m_CE_Schalter.SetFocus();
				m_CE_Schalter.SetSel(0,-1);
			}
		}

	}
	if (ilStatus == false)
	{
		Beep(440,70);
		MessageBox(olErrorText,GetString(ST_FEHLER),MB_ICONEXCLAMATION);
		m_CE_Schalter.SetFocus();
		m_CE_Schalter.SetSel(0,-1);

		//CDialog::OnCancel();
	}
	
}






void CCcaCommonDlg::OnClose() 
{
	ShowWindow(SW_HIDE);
	//CDialog::OnClose();
}
 


void CCcaCommonDlg::NewData(CCADATA *popCCA)
{
	omCCA = *popCCA;


	if(bgGatPosLocal) omCcaData.StructUtcToLocal(&omCCA, "");


	if(omCCA.Urno > 0)
	{
		SetWindowText( GetString(IDS_STRING1519));
	}
	else
	{
		SetWindowText(GetString(IDS_STRING1520));
	}



	m_CE_Terminal.SetInitText(omCCA.Ckit);
	m_CE_Schalter.SetInitText(omCCA.Ckic);

	char clTmp[20];
	CString olAlc;
	sprintf(clTmp,"%d",omCCA.Flnu);
	if(!ogBCD.GetField("ALT", "URNO", clTmp, "ALC2", olAlc))
		ogBCD.GetField("ALT", "URNO", clTmp, "ALC3", olAlc);
	m_CE_Fluggs.SetInitText(olAlc);



	CTime olTime;
	olTime = omCCA.Ckbs;
	m_CE_Beginn_d.SetInitText(olTime.Format("%d.%m.%Y"));
	m_CE_Beginn_t.SetInitText(olTime.Format("%H:%M"));

	olTime = omCCA.Ckes;
	m_CE_Ende_d.SetInitText(olTime.Format("%d.%m.%Y"));
	m_CE_Ende_t.SetInitText(olTime.Format("%H:%M"));

	olTime = omCCA.Ckba;
	m_CE_ActStartDate.SetInitText(olTime.Format("%d.%m.%Y"));
	m_CE_ActStartTime.SetInitText(olTime.Format("%H:%M"));

	olTime = omCCA.Ckea;
	m_CE_ActEndDate.SetInitText(olTime.Format("%d.%m.%Y"));
	m_CE_ActEndTime.SetInitText(olTime.Format("%H:%M"));


	m_CE_Rem.SetInitText(omCCA.Rema);
	m_CE_Disp.SetInitText(omCCA.Disp);


	SetWindowPos(&wndTop, 0,0,0,0, SWP_SHOWWINDOW | SWP_NOSIZE | SWP_NOMOVE );
	ShowWindow(SW_SHOW);
}



void CCcaCommonDlg::OnFidsRemList() 
{
	CString olText;

	m_CE_Disp.GetWindowText(olText);
	AwBasicDataDlg *polDlg = new AwBasicDataDlg(this, "FID_CIC","CODE,"+ogFIDRemarkField, "CODE+", olText);
	if(polDlg->DoModal() == IDOK)
	{
		m_CE_Disp.SetWindowText(polDlg->GetField("CODE"));
	}
	delete polDlg;
}


LONG CCcaCommonDlg::OnEditChanged( UINT wParam, LPARAM lParam)
{
// begin : reject if opening times
	CString olTmp;

	if((UINT)m_CE_Schalter.imID == wParam)
	{
		m_CE_Schalter.GetWindowText(olTmp);

		CString olGate = omCCA.Ckic;

		if (omCCA.Ckba != TIMENULL && !olGate.IsEmpty())
		{
			if (olTmp != olGate)
			{
				m_CE_Schalter.SetInitText(olGate);
				MessageBox( GetString(IDS_STRING_CCA_ISOPEN), GetString(ST_FEHLER), MB_OK | MB_ICONERROR);								
			}
		}
	}
	return 0L;
}
