VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomctl.ocx"
Object = "{64E8E382-05E2-11D2-9B1D-9B0630BC8F12}#1.0#0"; "tab.ocx"
Begin VB.Form TelexPoolHead 
   Caption         =   "Telex Pool"
   ClientHeight    =   8025
   ClientLeft      =   1065
   ClientTop       =   900
   ClientWidth     =   10320
   BeginProperty Font 
      Name            =   "Courier New"
      Size            =   8.25
      Charset         =   161
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "TelexPoolHead.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   8025
   ScaleWidth      =   10320
   ShowInTaskbar   =   0   'False
   WindowState     =   1  'Minimized
   Begin VB.Frame Frame1 
      Caption         =   "Filter"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1065
      Left            =   7860
      TabIndex        =   113
      Top             =   420
      Width           =   1095
      Begin VB.CheckBox chkFilter 
         Caption         =   "SD"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   1
         Left            =   540
         Style           =   1  'Graphical
         TabIndex        =   116
         Top             =   660
         Width           =   435
      End
      Begin VB.CheckBox chkFilter 
         Caption         =   "RC"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   0
         Left            =   120
         Style           =   1  'Graphical
         TabIndex        =   115
         Top             =   660
         Width           =   435
      End
      Begin VB.CheckBox chkFilter 
         Caption         =   "Status"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   2
         Left            =   120
         Style           =   1  'Graphical
         TabIndex        =   114
         Top             =   300
         Width           =   855
      End
   End
   Begin VB.Frame CedaBusyFlag 
      BorderStyle     =   0  'None
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1275
      Left            =   3810
      TabIndex        =   71
      Top             =   6300
      Width           =   1185
      Begin VB.TextBox CedaStatus 
         Alignment       =   2  'Center
         BackColor       =   &H000000FF&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   285
         Index           =   3
         Left            =   0
         TabIndex        =   89
         Text            =   "NO LINK"
         Top             =   900
         Visible         =   0   'False
         Width           =   1125
      End
      Begin VB.TextBox CedaStatus 
         Alignment       =   2  'Center
         BackColor       =   &H000000FF&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   285
         Index           =   2
         Left            =   0
         TabIndex        =   74
         Text            =   "LOG OFF"
         Top             =   600
         Visible         =   0   'False
         Width           =   1125
      End
      Begin VB.TextBox CedaStatus 
         Alignment       =   2  'Center
         BackColor       =   &H00004000&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   285
         Index           =   0
         Left            =   0
         TabIndex        =   72
         Text            =   "LINE BUSY"
         Top             =   0
         Visible         =   0   'False
         Width           =   1125
      End
      Begin VB.TextBox CedaStatus 
         Alignment       =   2  'Center
         BackColor       =   &H00008000&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   285
         Index           =   1
         Left            =   0
         TabIndex        =   73
         Text            =   "LOG ON"
         Top             =   300
         Visible         =   0   'False
         Width           =   1125
      End
   End
   Begin MSComctlLib.StatusBar MyStatusBar 
      Align           =   2  'Align Bottom
      Height          =   315
      Left            =   0
      TabIndex        =   55
      Top             =   7710
      Width           =   10320
      _ExtentX        =   18203
      _ExtentY        =   556
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   4
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   7408
            MinWidth        =   7408
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   7408
            MinWidth        =   7408
         EndProperty
         BeginProperty Panel3 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   864
            MinWidth        =   176
         EndProperty
         BeginProperty Panel4 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   1940
            MinWidth        =   1940
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Frame TopButtons 
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1575
      Left            =   9300
      TabIndex        =   85
      Top             =   -30
      Width           =   915
      Begin VB.CheckBox chkExit 
         Caption         =   "Exit"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   0
         Style           =   1  'Graphical
         TabIndex        =   93
         Top             =   1260
         Width           =   855
      End
      Begin VB.CheckBox FixWin 
         Caption         =   "Arrange"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   0
         Style           =   1  'Graphical
         TabIndex        =   92
         Top             =   660
         Width           =   855
      End
      Begin VB.CheckBox chkClose 
         Caption         =   "Close"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   0
         Style           =   1  'Graphical
         TabIndex        =   88
         Top             =   960
         Width           =   855
      End
      Begin VB.CheckBox OnTop 
         Caption         =   "On Top"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   0
         Style           =   1  'Graphical
         TabIndex        =   87
         Top             =   60
         Width           =   855
      End
      Begin VB.CheckBox OnLine 
         Caption         =   "Online"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   0
         Style           =   1  'Graphical
         TabIndex        =   86
         Top             =   360
         Value           =   1  'Checked
         Width           =   855
      End
   End
   Begin VB.Frame basicButtons 
      BorderStyle     =   0  'None
      Caption         =   "Frame1"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3345
      Left            =   9300
      TabIndex        =   56
      Top             =   1680
      Width           =   915
      Begin VB.CheckBox chkStatus 
         Caption         =   "Status"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   0
         Style           =   1  'Graphical
         TabIndex        =   102
         Top             =   2730
         Width           =   855
      End
      Begin VB.CheckBox chkPrintTxt 
         Caption         =   "Print"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   0
         Style           =   1  'Graphical
         TabIndex        =   83
         Top             =   30
         Width           =   855
      End
      Begin VB.CheckBox chkSend 
         Caption         =   "Send"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   0
         Style           =   1  'Graphical
         TabIndex        =   82
         Top             =   1830
         Width           =   855
      End
      Begin VB.CheckBox chkAddress 
         Caption         =   "Address"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   0
         Style           =   1  'Graphical
         TabIndex        =   81
         Top             =   1230
         Width           =   855
      End
      Begin VB.CheckBox chkOnTime 
         Caption         =   "On Time"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   0
         Style           =   1  'Graphical
         TabIndex        =   80
         Top             =   2130
         Width           =   855
      End
      Begin VB.CheckBox chkAssign 
         Caption         =   "Assign"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   0
         Style           =   1  'Graphical
         TabIndex        =   79
         Top             =   630
         Width           =   855
      End
      Begin VB.CheckBox chkFlight 
         Caption         =   "Flight"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   0
         Style           =   1  'Graphical
         TabIndex        =   78
         Top             =   330
         Width           =   855
      End
      Begin VB.CheckBox chkReject 
         Caption         =   "Reject"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   0
         Style           =   1  'Graphical
         TabIndex        =   77
         Top             =   2430
         Width           =   855
      End
      Begin VB.CheckBox chkNew 
         Caption         =   "New"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   0
         Style           =   1  'Graphical
         TabIndex        =   76
         Top             =   930
         Width           =   855
      End
      Begin VB.CheckBox chkEdit 
         Caption         =   "Edit"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   0
         Style           =   1  'Graphical
         TabIndex        =   75
         Top             =   1530
         Width           =   855
      End
      Begin VB.CheckBox FullText 
         Caption         =   "Fulltext"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   0
         Style           =   1  'Graphical
         TabIndex        =   57
         Top             =   3030
         Width           =   855
      End
   End
   Begin VB.Frame RightCover 
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   555
      Left            =   8520
      TabIndex        =   91
      Top             =   2430
      Width           =   1335
      Begin VB.Line BorderLine 
         BorderColor     =   &H00FFFFFF&
         BorderWidth     =   2
         Index           =   1
         X1              =   45
         X2              =   45
         Y1              =   0
         Y2              =   1140
      End
      Begin VB.Line BorderLine 
         BorderColor     =   &H00000000&
         BorderWidth     =   2
         Index           =   0
         X1              =   15
         X2              =   15
         Y1              =   0
         Y2              =   1140
      End
   End
   Begin VB.Frame MainButtons 
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   30
      TabIndex        =   96
      Top             =   -30
      Width           =   8970
      Begin VB.CheckBox chkHelp 
         Caption         =   "Help"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   6960
         Style           =   1  'Graphical
         TabIndex        =   123
         Top             =   60
         Width           =   855
      End
      Begin VB.CheckBox Airport 
         Caption         =   "Airport"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   4350
         Style           =   1  'Graphical
         TabIndex        =   111
         ToolTipText     =   "UFIS 4.4 / Module Telex Pool / Rel. 1.0 / Upd. 4 / 2000.08.04 "
         Top             =   60
         Width           =   855
      End
      Begin VB.CheckBox chkReset 
         Caption         =   "Reset"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   2610
         Style           =   1  'Graphical
         TabIndex        =   108
         Top             =   60
         Width           =   855
      End
      Begin VB.Frame optMemButtons 
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   870
         TabIndex        =   103
         Tag             =   "3"
         Top             =   60
         Width           =   1725
         Begin VB.OptionButton optMem 
            Caption         =   "MF"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Index           =   1
            Left            =   420
            Style           =   1  'Graphical
            TabIndex        =   106
            Top             =   0
            Width           =   435
         End
         Begin VB.OptionButton optMem 
            BackColor       =   &H0000FF00&
            Caption         =   "MS"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Index           =   3
            Left            =   1290
            Style           =   1  'Graphical
            TabIndex        =   104
            Top             =   0
            Value           =   -1  'True
            Width           =   435
         End
         Begin VB.OptionButton optMem 
            Caption         =   "MU"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Index           =   0
            Left            =   0
            Style           =   1  'Graphical
            TabIndex        =   107
            Top             =   0
            Width           =   435
         End
         Begin VB.OptionButton optMem 
            Caption         =   "MR"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Index           =   2
            Left            =   870
            Style           =   1  'Graphical
            TabIndex        =   105
            Top             =   0
            Width           =   435
         End
      End
      Begin VB.CheckBox chkLoad 
         Caption         =   "Load"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   0
         Style           =   1  'Graphical
         TabIndex        =   101
         Top             =   60
         Width           =   855
      End
      Begin VB.CheckBox chkPrint 
         Caption         =   "Print"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   3480
         Style           =   1  'Graphical
         TabIndex        =   100
         Top             =   60
         Width           =   855
      End
      Begin VB.CheckBox chkSetup 
         Caption         =   "Setup"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   5220
         Style           =   1  'Graphical
         TabIndex        =   99
         Top             =   60
         Width           =   855
      End
      Begin VB.CheckBox chkMail 
         Caption         =   "Mail"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   7830
         Style           =   1  'Graphical
         TabIndex        =   98
         Top             =   60
         Width           =   855
      End
      Begin VB.CheckBox chkAbout 
         Caption         =   "About"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   6090
         Style           =   1  'Graphical
         TabIndex        =   97
         Top             =   60
         Width           =   855
      End
   End
   Begin VB.Frame folderButtons 
      BorderStyle     =   0  'None
      Caption         =   "Frame1"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Index           =   3
      Left            =   1920
      TabIndex        =   33
      Top             =   5790
      Visible         =   0   'False
      Width           =   915
      Begin VB.CheckBox CgoInfo 
         Caption         =   "Loading"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   1
         Left            =   30
         Style           =   1  'Graphical
         TabIndex        =   45
         Tag             =   "CPMCGO"
         Top             =   0
         Width           =   855
      End
   End
   Begin VB.Frame folderButtons 
      BorderStyle     =   0  'None
      Caption         =   "Frame1"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   615
      Index           =   2
      Left            =   990
      TabIndex        =   32
      Top             =   5790
      Visible         =   0   'False
      Width           =   915
      Begin VB.CheckBox PaxInfo 
         Caption         =   "Pax Info"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   1
         Left            =   30
         Style           =   1  'Graphical
         TabIndex        =   84
         Tag             =   "LDMPAX"
         Top             =   300
         Width           =   855
      End
      Begin VB.CheckBox CgoInfo 
         Caption         =   "Loading"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   0
         Left            =   30
         Style           =   1  'Graphical
         TabIndex        =   44
         Tag             =   "LDMCGO"
         Top             =   0
         Width           =   855
      End
   End
   Begin VB.Frame folderButtons 
      BorderStyle     =   0  'None
      Caption         =   "Frame1"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Index           =   1
      Left            =   60
      TabIndex        =   31
      Top             =   5790
      Visible         =   0   'False
      Width           =   915
      Begin VB.CheckBox PaxInfo 
         Caption         =   "Pax Info"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   0
         Left            =   30
         Style           =   1  'Graphical
         TabIndex        =   43
         Tag             =   "MVTPAX"
         Top             =   0
         Width           =   855
      End
   End
   Begin VB.Frame folderButtons 
      BackColor       =   &H000000FF&
      BorderStyle     =   0  'None
      Caption         =   "Frame1"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   135
      Index           =   0
      Left            =   60
      TabIndex        =   30
      Tag             =   "-1"
      Top             =   5580
      Visible         =   0   'False
      Width           =   1995
   End
   Begin VB.Frame folderButtons 
      BorderStyle     =   0  'None
      Caption         =   "Frame1"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Index           =   12
      Left            =   2850
      TabIndex        =   42
      Top             =   6630
      Visible         =   0   'False
      Width           =   915
      Begin VB.CheckBox Check14 
         Caption         =   "12"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   30
         Style           =   1  'Graphical
         TabIndex        =   54
         Top             =   0
         Visible         =   0   'False
         Width           =   855
      End
   End
   Begin VB.Frame folderButtons 
      BorderStyle     =   0  'None
      Caption         =   "Frame1"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Index           =   11
      Left            =   1920
      TabIndex        =   41
      Top             =   6630
      Visible         =   0   'False
      Width           =   915
      Begin VB.CheckBox Check13 
         Caption         =   "11"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   30
         Style           =   1  'Graphical
         TabIndex        =   53
         Top             =   0
         Visible         =   0   'False
         Width           =   855
      End
   End
   Begin VB.Frame folderButtons 
      BorderStyle     =   0  'None
      Caption         =   "Frame1"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Index           =   10
      Left            =   990
      TabIndex        =   40
      Top             =   6630
      Visible         =   0   'False
      Width           =   915
      Begin VB.CheckBox Check12 
         Caption         =   "10"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   30
         Style           =   1  'Graphical
         TabIndex        =   52
         Top             =   0
         Visible         =   0   'False
         Width           =   855
      End
   End
   Begin VB.Frame folderButtons 
      BorderStyle     =   0  'None
      Caption         =   "Frame1"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Index           =   9
      Left            =   60
      TabIndex        =   39
      Top             =   6630
      Visible         =   0   'False
      Width           =   915
      Begin VB.CheckBox Check11 
         Caption         =   "9"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   30
         Style           =   1  'Graphical
         TabIndex        =   51
         Top             =   0
         Visible         =   0   'False
         Width           =   855
      End
   End
   Begin VB.Frame folderButtons 
      BorderStyle     =   0  'None
      Caption         =   "Frame1"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2265
      Index           =   8
      Left            =   6630
      TabIndex        =   38
      Top             =   5010
      Visible         =   0   'False
      Width           =   1335
      Begin VB.CheckBox Check1 
         Caption         =   "Create"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   75
         Style           =   1  'Graphical
         TabIndex        =   121
         Top             =   1950
         Visible         =   0   'False
         Width           =   1155
      End
      Begin VB.CheckBox AtcPreview 
         Caption         =   "Preview"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   75
         Style           =   1  'Graphical
         TabIndex        =   120
         Top             =   360
         Width           =   1155
      End
      Begin VB.TextBox CsgnStatus 
         Alignment       =   2  'Center
         BackColor       =   &H00008000&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   285
         Left            =   75
         TabIndex        =   119
         Text            =   "UPDATE"
         Top             =   1560
         Visible         =   0   'False
         Width           =   1155
      End
      Begin VB.TextBox AtcCSGN 
         Alignment       =   2  'Center
         BackColor       =   &H0000FFFF&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   75
         Locked          =   -1  'True
         TabIndex        =   118
         ToolTipText     =   "Callsign used by ATC"
         Top             =   1260
         Width           =   1155
      End
      Begin VB.TextBox UseCSGN 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   75
         TabIndex        =   112
         ToolTipText     =   "Callsign used by UFIS"
         Top             =   960
         Width           =   1155
      End
      Begin VB.CheckBox AtcTransmit 
         Caption         =   "Transmit"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   75
         Style           =   1  'Graphical
         TabIndex        =   50
         Top             =   60
         Width           =   1155
      End
      Begin VB.Line BorderLine 
         BorderColor     =   &H00000000&
         Index           =   9
         X1              =   30
         X2              =   1275
         Y1              =   1890
         Y2              =   1890
      End
      Begin VB.Line BorderLine 
         BorderColor     =   &H00000000&
         Index           =   8
         X1              =   1260
         X2              =   1260
         Y1              =   15
         Y2              =   1890
      End
      Begin VB.Line BorderLine 
         BorderColor     =   &H00E0E0E0&
         Index           =   7
         X1              =   45
         X2              =   45
         Y1              =   15
         Y2              =   1875
      End
      Begin VB.Line BorderLine 
         BorderColor     =   &H00E0E0E0&
         Index           =   6
         X1              =   45
         X2              =   1260
         Y1              =   15
         Y2              =   15
      End
      Begin VB.Line BorderLine 
         BorderColor     =   &H00000000&
         Index           =   5
         X1              =   1275
         X2              =   1275
         Y1              =   0
         Y2              =   1890
      End
      Begin VB.Line BorderLine 
         BorderColor     =   &H00808080&
         Index           =   4
         X1              =   45
         X2              =   1275
         Y1              =   1875
         Y2              =   1875
      End
      Begin VB.Line BorderLine 
         BorderColor     =   &H00FFFFFF&
         Index           =   3
         X1              =   30
         X2              =   30
         Y1              =   0
         Y2              =   1875
      End
      Begin VB.Line BorderLine 
         BorderColor     =   &H00FFFFFF&
         BorderWidth     =   2
         Index           =   2
         X1              =   30
         X2              =   1260
         Y1              =   0
         Y2              =   0
      End
      Begin VB.Label Label1 
         Caption         =   "Use Callsign"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   120
         TabIndex        =   117
         Top             =   720
         Width           =   1155
      End
   End
   Begin VB.Frame folderButtons 
      BorderStyle     =   0  'None
      Caption         =   "Frame1"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Index           =   7
      Left            =   5640
      TabIndex        =   37
      Top             =   5790
      Visible         =   0   'False
      Width           =   915
      Begin VB.CheckBox Check9 
         Caption         =   "7"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   30
         Style           =   1  'Graphical
         TabIndex        =   49
         Top             =   0
         Visible         =   0   'False
         Width           =   855
      End
   End
   Begin VB.Frame folderButtons 
      BorderStyle     =   0  'None
      Caption         =   "Frame1"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Index           =   6
      Left            =   4710
      TabIndex        =   36
      Top             =   5790
      Visible         =   0   'False
      Width           =   915
      Begin VB.CheckBox Check8 
         Caption         =   "6"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   30
         Style           =   1  'Graphical
         TabIndex        =   48
         Top             =   0
         Visible         =   0   'False
         Width           =   855
      End
   End
   Begin VB.Frame folderButtons 
      BorderStyle     =   0  'None
      Caption         =   "Frame1"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Index           =   5
      Left            =   3780
      TabIndex        =   35
      Top             =   5790
      Visible         =   0   'False
      Width           =   915
      Begin VB.CheckBox Check7 
         Caption         =   "5"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   30
         Style           =   1  'Graphical
         TabIndex        =   47
         Top             =   0
         Visible         =   0   'False
         Width           =   855
      End
   End
   Begin VB.Frame folderButtons 
      BorderStyle     =   0  'None
      Caption         =   "Frame1"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Index           =   4
      Left            =   2850
      TabIndex        =   34
      Top             =   5790
      Visible         =   0   'False
      Width           =   915
      Begin VB.CheckBox PaxInfo 
         Caption         =   "Pax Info"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   2
         Left            =   30
         Style           =   1  'Graphical
         TabIndex        =   46
         Tag             =   "PTMPAX"
         Top             =   0
         Width           =   855
      End
   End
   Begin VB.Frame chkTabsPanel 
      BorderStyle     =   0  'None
      Caption         =   "Frame1"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   135
      TabIndex        =   58
      Top             =   1980
      Width           =   7230
      Begin VB.CheckBox chkTabs 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   225
         Index           =   0
         Left            =   -255
         Style           =   1  'Graphical
         TabIndex        =   70
         Top             =   -75
         Visible         =   0   'False
         Width           =   645
      End
      Begin VB.CheckBox chkTabs 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   225
         Index           =   1
         Left            =   0
         Style           =   1  'Graphical
         TabIndex        =   69
         Top             =   0
         Width           =   615
      End
      Begin VB.CheckBox chkTabs 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   225
         Index           =   2
         Left            =   630
         Style           =   1  'Graphical
         TabIndex        =   68
         Top             =   0
         Width           =   645
      End
      Begin VB.CheckBox chkTabs 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   225
         Index           =   3
         Left            =   1290
         Style           =   1  'Graphical
         TabIndex        =   67
         Top             =   0
         Width           =   645
      End
      Begin VB.CheckBox chkTabs 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   225
         Index           =   4
         Left            =   1950
         Style           =   1  'Graphical
         TabIndex        =   66
         Top             =   0
         Width           =   645
      End
      Begin VB.CheckBox chkTabs 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   225
         Index           =   5
         Left            =   2610
         Style           =   1  'Graphical
         TabIndex        =   65
         Top             =   0
         Width           =   645
      End
      Begin VB.CheckBox chkTabs 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   225
         Index           =   6
         Left            =   3270
         Style           =   1  'Graphical
         TabIndex        =   64
         Top             =   0
         Width           =   645
      End
      Begin VB.CheckBox chkTabs 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   225
         Index           =   7
         Left            =   3930
         Style           =   1  'Graphical
         TabIndex        =   63
         Top             =   0
         Width           =   645
      End
      Begin VB.CheckBox chkTabs 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   225
         Index           =   8
         Left            =   4590
         Style           =   1  'Graphical
         TabIndex        =   62
         Top             =   0
         Width           =   645
      End
      Begin VB.CheckBox chkTabs 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   225
         Index           =   10
         Left            =   5910
         Style           =   1  'Graphical
         TabIndex        =   61
         Top             =   0
         Width           =   645
      End
      Begin VB.CheckBox chkTabs 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   225
         Index           =   11
         Left            =   6570
         Style           =   1  'Graphical
         TabIndex        =   60
         Top             =   0
         Value           =   1  'Checked
         Width           =   645
      End
      Begin VB.CheckBox chkTabs 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   225
         Index           =   9
         Left            =   5250
         Style           =   1  'Graphical
         TabIndex        =   59
         Top             =   0
         Width           =   645
      End
   End
   Begin TABLib.TAB tabTelexList 
      Height          =   1215
      Index           =   1
      Left            =   4830
      TabIndex        =   29
      Top             =   2850
      Width           =   2850
      _Version        =   65536
      _ExtentX        =   5027
      _ExtentY        =   2143
      _StockProps     =   0
      Lines           =   10
      HeaderString    =   "Type,Time,Number,Text Extract"
      HeaderLengthString=   "30,30,30,30"
   End
   Begin VB.CheckBox chkTlxTitleBar 
      BackColor       =   &H80000002&
      Caption         =   "Sent Telexes"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   315
      Index           =   1
      Left            =   4620
      Style           =   1  'Graphical
      TabIndex        =   23
      Top             =   2250
      Value           =   1  'Checked
      Width           =   3825
   End
   Begin VB.CheckBox chkTlxTitleBar 
      BackColor       =   &H80000002&
      Caption         =   "Received Telexes"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   315
      Index           =   0
      Left            =   130
      Style           =   1  'Graphical
      TabIndex        =   22
      Top             =   2250
      Value           =   1  'Checked
      Width           =   4355
   End
   Begin VB.TextBox txtTelexText 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   161
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   615
      Left            =   180
      Locked          =   -1  'True
      MultiLine       =   -1  'True
      ScrollBars      =   3  'Both
      TabIndex        =   12
      Top             =   4560
      Width           =   8175
   End
   Begin TABLib.TAB tabTelexList 
      Height          =   1215
      Index           =   0
      Left            =   1110
      TabIndex        =   24
      Top             =   2880
      Width           =   2850
      _Version        =   65536
      _ExtentX        =   5027
      _ExtentY        =   2143
      _StockProps     =   0
      Lines           =   10
      HeaderString    =   "Type,Time,Number,Text Extract"
      HeaderLengthString=   "30,30,30,30"
   End
   Begin VB.TextBox txtTelexList 
      BackColor       =   &H8000000F&
      CausesValidation=   0   'False
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00C0C0C0&
      Height          =   1875
      Index           =   1
      Left            =   4620
      TabIndex        =   28
      TabStop         =   0   'False
      Top             =   2610
      Width           =   3825
   End
   Begin VB.TextBox txtTelexList 
      BackColor       =   &H8000000F&
      CausesValidation=   0   'False
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00C0C0C0&
      Height          =   1875
      Index           =   0
      Left            =   120
      TabIndex        =   27
      TabStop         =   0   'False
      Top             =   2610
      Width           =   4365
   End
   Begin VB.Frame fraSplitter 
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      ClipControls    =   0   'False
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00C0C0C0&
      Height          =   285
      Index           =   1
      Left            =   8250
      MousePointer    =   15  'Size All
      TabIndex        =   25
      Tag             =   "-1"
      Top             =   5280
      Width           =   315
   End
   Begin VB.Frame fraSplitter 
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      ClipControls    =   0   'False
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00C0C0C0&
      Height          =   2175
      Index           =   0
      Left            =   8580
      MousePointer    =   9  'Size W E
      TabIndex        =   26
      Tag             =   "-1"
      Top             =   3090
      Width           =   195
   End
   Begin VB.Frame fraSplitter 
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      ClipControls    =   0   'False
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00C0C0C0&
      Height          =   195
      Index           =   2
      Left            =   90
      MousePointer    =   7  'Size N S
      TabIndex        =   0
      Tag             =   "-1"
      Top             =   5280
      Width           =   8595
   End
   Begin VB.Frame fraTextFilter 
      Caption         =   "Telex Data Filter"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1065
      Left            =   5250
      TabIndex        =   13
      Top             =   420
      Width           =   2655
      Begin VB.TextBox txtAddr 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   660
         TabIndex        =   20
         Top             =   300
         Width           =   1815
      End
      Begin VB.TextBox txtText 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   660
         TabIndex        =   16
         Top             =   630
         Width           =   1815
      End
      Begin VB.Label Label9 
         Caption         =   "Addr."
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   120
         TabIndex        =   21
         Top             =   360
         Width           =   495
      End
      Begin VB.Label Label7 
         Caption         =   "Text:"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   120
         TabIndex        =   17
         Top             =   690
         Width           =   495
      End
   End
   Begin MSComctlLib.TabStrip tabTlxTypeFolder 
      Height          =   3675
      Left            =   60
      TabIndex        =   14
      Tag             =   "1"
      Top             =   1590
      Width           =   8475
      _ExtentX        =   14949
      _ExtentY        =   6482
      TabWidthStyle   =   2
      TabFixedWidth   =   1161
      TabFixedHeight  =   526
      TabMinWidth     =   1161
      _Version        =   393216
      BeginProperty Tabs {1EFB6598-857C-11D1-B16A-00C0F0283628} 
         NumTabs         =   11
         BeginProperty Tab1 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
            Caption         =   "MVT"
            Object.Tag             =   "'MVT'"
            Object.ToolTipText     =   "Movement Messages"
            ImageVarType    =   2
         EndProperty
         BeginProperty Tab2 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
            Caption         =   "LDM"
            Object.Tag             =   "'LDM'"
            Object.ToolTipText     =   "Load Message"
            ImageVarType    =   2
         EndProperty
         BeginProperty Tab3 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
            Caption         =   "CPM"
            Object.Tag             =   "'CPM'"
            Object.ToolTipText     =   "Container Palette Messages"
            ImageVarType    =   2
         EndProperty
         BeginProperty Tab4 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
            Caption         =   "PTM"
            Object.Tag             =   "'PTM'"
            Object.ToolTipText     =   "Passenger Transfer Messages"
            ImageVarType    =   2
         EndProperty
         BeginProperty Tab5 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
            Caption         =   "CTOT"
            Object.Tag             =   "'CTOT','SAM','SRM','SLC'"
            Object.ToolTipText     =   "Slot Coordination SAM & SRM & SLC"
            ImageVarType    =   2
         EndProperty
         BeginProperty Tab6 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
            Caption         =   "SLOT"
            Object.Tag             =   "'SCR','SMA'"
            Object.ToolTipText     =   "Slot Coordinator SCR & SMA"
            ImageVarType    =   2
         EndProperty
         BeginProperty Tab7 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
            Caption         =   "BSM"
            Object.Tag             =   "'BSM'"
            Object.ToolTipText     =   "Baggage Source Messages"
            ImageVarType    =   2
         EndProperty
         BeginProperty Tab8 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
            Caption         =   "ATC"
            Object.Tag             =   "'ATC'"
            Object.ToolTipText     =   "Air Trafic Control (UFIS internal)"
            ImageVarType    =   2
         EndProperty
         BeginProperty Tab9 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
            Caption         =   "AAD"
            Object.Tag             =   "'AAD'"
            Object.ToolTipText     =   "Ufis Data Interchange Messages"
            ImageVarType    =   2
         EndProperty
         BeginProperty Tab10 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
            Caption         =   "Other"
            ImageVarType    =   2
         EndProperty
         BeginProperty Tab11 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
            Caption         =   "All"
            ImageVarType    =   2
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Frame fraFlightFilter 
      Caption         =   "Flight Data Filter"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1065
      Left            =   2640
      TabIndex        =   8
      Top             =   420
      Width           =   2655
      Begin VB.TextBox txtRegn 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   840
         TabIndex        =   18
         Top             =   630
         Width           =   1635
      End
      Begin VB.TextBox txtFlns 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   2160
         TabIndex        =   11
         ToolTipText     =   "Flight Suffix"
         Top             =   300
         Width           =   315
      End
      Begin VB.TextBox txtFltn 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1590
         TabIndex        =   10
         ToolTipText     =   "Flight Number"
         Top             =   300
         Width           =   555
      End
      Begin VB.TextBox txtFlca 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   840
         TabIndex        =   9
         ToolTipText     =   "Flight Carrier ICAO (3LC)"
         Top             =   300
         Width           =   735
      End
      Begin VB.Label Label8 
         Caption         =   "Registr."
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   120
         TabIndex        =   19
         Top             =   690
         Width           =   645
      End
      Begin VB.Label Label6 
         Caption         =   "Flight:"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   120
         TabIndex        =   15
         Top             =   360
         Width           =   495
      End
   End
   Begin VB.Frame fraTimeFilter 
      Caption         =   "Date/Time Filter"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1065
      Left            =   60
      TabIndex        =   1
      Top             =   420
      Width           =   2625
      Begin VB.CheckBox ResetFilter 
         Caption         =   "R"
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   1605
         Style           =   1  'Graphical
         TabIndex        =   122
         ToolTipText     =   "Reset Time Filter"
         Top             =   15
         Width           =   300
      End
      Begin VB.OptionButton optLocal 
         Caption         =   "L"
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   2205
         Style           =   1  'Graphical
         TabIndex        =   110
         ToolTipText     =   "Times in ATH local (UTC + 180 min.)"
         Top             =   15
         Width           =   300
      End
      Begin VB.OptionButton optUtc 
         BackColor       =   &H0080FF80&
         Caption         =   "U"
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   1905
         Style           =   1  'Graphical
         TabIndex        =   109
         ToolTipText     =   "Times in UTC (ATH Local - 180 min.)"
         Top             =   15
         Value           =   -1  'True
         Width           =   300
      End
      Begin VB.TextBox txtDateFrom 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   570
         TabIndex        =   5
         ToolTipText     =   "Day Date dd.mm.yyyy"
         Top             =   300
         Width           =   1215
      End
      Begin VB.TextBox txtTimeFrom 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1830
         TabIndex        =   4
         ToolTipText     =   "Time hh:mm"
         Top             =   300
         Width           =   615
      End
      Begin VB.TextBox txtTimeTo 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1830
         TabIndex        =   3
         ToolTipText     =   "Time hh:mm"
         Top             =   630
         Width           =   615
      End
      Begin VB.TextBox txtDateTo 
         Alignment       =   2  'Center
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   570
         TabIndex        =   2
         ToolTipText     =   "Day Date dd.mm.yyyy"
         Top             =   630
         Width           =   1215
      End
      Begin VB.Label Label2 
         Caption         =   "from:"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   120
         TabIndex        =   7
         Top             =   360
         Width           =   495
      End
      Begin VB.Label Label4 
         Caption         =   "to:"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   120
         TabIndex        =   6
         Top             =   690
         Width           =   435
      End
   End
   Begin VB.TextBox txtDummy 
      BackColor       =   &H00C0C0C0&
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   270
      TabIndex        =   90
      TabStop         =   0   'False
      Top             =   2670
      Width           =   705
   End
   Begin VB.TextBox CurrentUrno 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   4440
      TabIndex        =   94
      Text            =   "CurrentUrno"
      Top             =   6270
      Visible         =   0   'False
      Width           =   1245
   End
   Begin VB.TextBox CurrentRecord 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   4440
      TabIndex        =   95
      Text            =   "CurrentRecord"
      Top             =   6600
      Visible         =   0   'False
      Width           =   1905
   End
End
Attribute VB_Name = "TelexPoolHead"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private Type AdminMemory
    Folder As String
    ActTab As String
    TabTag As String    'Settings of TypeFolderTabs
    TabCap As String    'Settings of TypeFolderTabs
    ChkVal As String    'TypeFolderButton pushed or not
    ChkTag As String    'Settings of TypeFilterButtons
    ChkCap As String    'Settings of TypeFilterButtons
    FldVal As String    'FieldValues of the FilterDialog Used in The SqlKey
    TlxKey As String    'WhereClause for TLXTAB built
    TfrKey As String    'New WhereClause TFRTAB
    ExtKey As String    'Additional WhereClause from External (not yet used)
End Type
Private ActFilterValues(0 To 4) As AdminMemory
Private NewFilterValues(0 To 4) As AdminMemory
Private Sub StoreFolderValues(MemIdx As Integer)
Dim i As Integer
    NewFilterValues(MemIdx).Folder = tabTlxTypeFolder.Tag & vbTab & folderButtons(0).Tag
    NewFilterValues(MemIdx).ActTab = tabTlxTypeFolder.SelectedItem
    NewFilterValues(MemIdx).TabCap = ""
    NewFilterValues(MemIdx).TabTag = ""
    NewFilterValues(MemIdx).ChkCap = ""
    NewFilterValues(MemIdx).ChkTag = ""
    NewFilterValues(MemIdx).ChkVal = ""
    For i = 0 To chkTabs.count - 2
        NewFilterValues(MemIdx).TabCap = NewFilterValues(MemIdx).TabCap & tabTlxTypeFolder.Tabs(i + 1).Caption & vbTab
        NewFilterValues(MemIdx).TabTag = NewFilterValues(MemIdx).TabTag & tabTlxTypeFolder.Tabs(i + 1).Tag & vbTab
        NewFilterValues(MemIdx).ChkCap = NewFilterValues(MemIdx).ChkCap & chkTabs(i).Caption & vbTab
        NewFilterValues(MemIdx).ChkTag = NewFilterValues(MemIdx).ChkTag & chkTabs(i).Tag & vbTab
        NewFilterValues(MemIdx).ChkVal = NewFilterValues(MemIdx).ChkVal & chkTabs(i).Value & vbTab
    Next
    NewFilterValues(MemIdx).FldVal = ""
    NewFilterValues(MemIdx).FldVal = NewFilterValues(MemIdx).FldVal & txtDateFrom & vbTab
    NewFilterValues(MemIdx).FldVal = NewFilterValues(MemIdx).FldVal & txtTimeFrom & vbTab
    NewFilterValues(MemIdx).FldVal = NewFilterValues(MemIdx).FldVal & txtDateTo & vbTab
    NewFilterValues(MemIdx).FldVal = NewFilterValues(MemIdx).FldVal & txtTimeTo & vbTab
    NewFilterValues(MemIdx).FldVal = NewFilterValues(MemIdx).FldVal & txtFlca & vbTab
    NewFilterValues(MemIdx).FldVal = NewFilterValues(MemIdx).FldVal & txtFltn & vbTab
    NewFilterValues(MemIdx).FldVal = NewFilterValues(MemIdx).FldVal & txtFlns & vbTab
    NewFilterValues(MemIdx).FldVal = NewFilterValues(MemIdx).FldVal & txtRegn & vbTab
    NewFilterValues(MemIdx).FldVal = NewFilterValues(MemIdx).FldVal & txtAddr & vbTab
    NewFilterValues(MemIdx).FldVal = NewFilterValues(MemIdx).FldVal & txtText & vbTab
End Sub

Private Sub RestoreFolderValues(MemIdx As Integer)
Dim i As Integer
Dim Itm As Integer
    For i = 0 To chkTabs.count - 2
        Itm = i + 1
        tabTlxTypeFolder.Tabs(Itm).Tag = GetItem(NewFilterValues(MemIdx).TabTag, Itm, vbTab)
        tabTlxTypeFolder.Tabs(Itm).Caption = GetItem(NewFilterValues(MemIdx).TabCap, Itm, vbTab)
        chkTabs(i).Tag = GetItem(NewFilterValues(MemIdx).ChkTag, Itm, vbTab)
        chkTabs(i).Caption = GetItem(NewFilterValues(MemIdx).ChkCap, Itm, vbTab)
        chkTabs(i).Value = GetItem(NewFilterValues(MemIdx).ChkVal, Itm, vbTab)
    Next
    txtDateFrom = GetItem(NewFilterValues(MemIdx).FldVal, 1, vbTab)
    txtTimeFrom = GetItem(NewFilterValues(MemIdx).FldVal, 2, vbTab)
    txtDateTo = GetItem(NewFilterValues(MemIdx).FldVal, 3, vbTab)
    txtTimeTo = GetItem(NewFilterValues(MemIdx).FldVal, 4, vbTab)
    txtFlca = GetItem(NewFilterValues(MemIdx).FldVal, 5, vbTab)
    txtFltn = GetItem(NewFilterValues(MemIdx).FldVal, 6, vbTab)
    txtFlns = GetItem(NewFilterValues(MemIdx).FldVal, 7, vbTab)
    txtRegn = GetItem(NewFilterValues(MemIdx).FldVal, 8, vbTab)
    txtAddr = GetItem(NewFilterValues(MemIdx).FldVal, 9, vbTab)
    txtText = GetItem(NewFilterValues(MemIdx).FldVal, 10, vbTab)
    tabTlxTypeFolder.Tag = GetItem(NewFilterValues(MemIdx).Folder, 1, vbTab)
    folderButtons(0).Tag = GetItem(NewFilterValues(MemIdx).Folder, 2, vbTab)
    InitFolderByName NewFilterValues(MemIdx).ActTab, False
    tabTlxTypeFolder_Click
End Sub
Private Sub ResetFolderValues(MemIdx As Integer)
Dim i As Integer
Dim Itm As Integer
    For i = 0 To chkTabs.count - 2
        Itm = i + 1
        tabTlxTypeFolder.Tabs(Itm).Tag = GetItem(ActFilterValues(MemIdx).TabTag, Itm, vbTab)
        tabTlxTypeFolder.Tabs(Itm).Caption = GetItem(ActFilterValues(MemIdx).TabCap, Itm, vbTab)
        chkTabs(i).Tag = GetItem(ActFilterValues(MemIdx).ChkTag, Itm, vbTab)
        chkTabs(i).Caption = GetItem(ActFilterValues(MemIdx).ChkCap, Itm, vbTab)
        chkTabs(i).Value = GetItem(ActFilterValues(MemIdx).ChkVal, Itm, vbTab)
    Next
    txtDateFrom = GetItem(ActFilterValues(MemIdx).FldVal, 1, vbTab)
    txtTimeFrom = GetItem(ActFilterValues(MemIdx).FldVal, 2, vbTab)
    txtDateTo = GetItem(ActFilterValues(MemIdx).FldVal, 3, vbTab)
    txtTimeTo = GetItem(ActFilterValues(MemIdx).FldVal, 4, vbTab)
    txtFlca = GetItem(ActFilterValues(MemIdx).FldVal, 5, vbTab)
    txtFltn = GetItem(ActFilterValues(MemIdx).FldVal, 6, vbTab)
    txtFlns = GetItem(ActFilterValues(MemIdx).FldVal, 7, vbTab)
    txtRegn = GetItem(ActFilterValues(MemIdx).FldVal, 8, vbTab)
    txtAddr = GetItem(ActFilterValues(MemIdx).FldVal, 9, vbTab)
    txtText = GetItem(ActFilterValues(MemIdx).FldVal, 10, vbTab)
    tabTlxTypeFolder.Tag = GetItem(ActFilterValues(MemIdx).Folder, 1, vbTab)
    folderButtons(0).Tag = GetItem(ActFilterValues(MemIdx).Folder, 2, vbTab)
    InitFolderByName ActFilterValues(MemIdx).ActTab, False
    tabTlxTypeFolder_Click
End Sub

Private Sub InitFolderByName(cpName As String, bpSetTab As Boolean)
Dim i As Integer
Dim blFound As Boolean
    blFound = False
    i = 0
    While (i < tabTlxTypeFolder.Tabs.count) And (Not blFound)
        i = i + 1
        If (tabTlxTypeFolder.Tabs(i).Caption = cpName) Or (InStr(tabTlxTypeFolder.Tabs(i).Tag, cpName) > 0) Then
            If bpSetTab = True Then chkTabs(i).Value = 1
            tabTlxTypeFolder.Tabs(i).Selected = True
            tabTlxTypeFolder.Tabs(i).HighLighted = True
            blFound = True
        End If
    Wend
End Sub

Public Function GetTabFolderByName(cpName As String) As Integer
    Dim i As Integer
    Dim ilTabNbr As Integer
    Dim clChkNam As String
    clChkNam = "'" & Trim(cpName) & "'"
    ilTabNbr = 0
    i = 0
    While (i < tabTlxTypeFolder.Tabs.count) And (ilTabNbr < 1)
        i = i + 1
        If InStr(tabTlxTypeFolder.Tabs(i).Tag, clChkNam) > 0 Then ilTabNbr = i
    Wend
    GetTabFolderByName = ilTabNbr
End Function

Private Sub AtcCSGN_Change()
    If Trim(UseCSGN.Text) <> Trim(AtcCSGN.Text) Then
        CsgnStatus.Visible = True
    Else
        CsgnStatus.Visible = False
    End If
End Sub

Private Sub CgoInfo_Click(Index As Integer)
    If CgoInfo(Index).Value = 1 Then
        If Trim(CurrentUrno.Text) <> "" Then
            CheckLoaTabViewer CurrentRecord.Text, CgoInfo(Index).Tag
        Else
            MyMsgBox.CallAskUser 0, 0, 0, "Show " & CgoInfo(Index).Caption, "No telex selected.", "hand", "", UserAnswer
        End If
        CgoInfo(Index).Value = 0
    End If
End Sub

Private Sub Check1_Click()
'Dim Txt As String
''PALLAS: ARCID,ADEP,ADEPOLD,ADES,ADESOLD,ETD ,ETDOLD ,ETA ,ATD ,ATA ,RWYID ,ARCTYP,STND
''UFIS:   CSGN ,ORG4,ORG4old,DES4,DES4old,ETDA,ETDAold,ETAA,AIRA,LNDA,RWYA/D,ACT3,------
'    If Check1.Value = 1 Then
'        Txt = ""
'        Txt = Txt & "CSGN: " & vbNewLine
'        Txt = Txt & "ORG4: " & vbNewLine
'        Txt = Txt & "DES4: " & vbNewLine
'        Txt = Txt & "ETOD: " & vbNewLine
'        Txt = Txt & "AIRB: " & vbNewLine
'        Txt = Txt & "RWYD: " & vbNewLine
'        Txt = Txt & "ETOA: " & vbNewLine
'        Txt = Txt & "LAND: " & vbNewLine
'        Txt = Txt & "RWYA: " & vbNewLine
'        Txt = Txt & "ACT3: " & vbNewLine
'        txtTelexText.Text = Txt
'        UseCSGN.Text = ""
'        AtcCSGN.Text = ""
'        CsgnStatus.Visible = False
'        Check1.Value = 0
'    End If
End Sub

Private Sub AtcPreview_Click()
    Dim AskRetVal As Integer
    If AtcPreview.Value = 1 Then
        AtcPreview.BackColor = LightestGreen
        AskRetVal = MyMsgBox.CallAskUser(0, 0, 0, "ATC Interface Preview", "Still under construction.", "hand", "", UserAnswer)
        AtcPreview.Value = 0
    Else
        AtcPreview.BackColor = vbButtonFace
    End If
End Sub

Private Sub AtcTransmit_Click()
    Dim MsgText As String
    Dim OutFldLst As String
    Dim OldFldLst As String
    Dim OutDatLst As String
    Dim OldDatLst As String
    Dim CurLinTxt As String
    Dim CurNewDat As String
    Dim CurOldDat As String
    Dim CurFldNam As String
    Dim UseFldNam As String
    Dim CurFldDat As String
    Dim CedaFields As String
    Dim CedaData As String
    Dim linNbr As Integer
    Dim CedaResult As String
    Dim CedaRetVal As Integer
    Dim AskRetVal As Integer
    
'PALLAS: ARCID,ADEP,ADEPOLD,ADES,ADESOLD,ETD ,ETDOLD ,ETA ,ATD ,ATA ,RWYID ,ARCTYP,STND
'UFIS:   CSGN ,ORG4,ORG4old,DES4,DES4old,ETDA,ETDAold,ETAA,AIRA,LNDA,RWYA/D,ACT3,------

    If AtcTransmit.Value = 1 Then
        AtcTransmit.BackColor = LightestGreen
        AskRetVal = 1
        If Trim(UseCSGN.Text) <> Trim(AtcCSGN.Text) Then
            MsgText = ""
            MsgText = MsgText & "Do you want to change the" & vbNewLine
            MsgText = MsgText & "currently used callsign" & vbNewLine
            MsgText = MsgText & "from " & Trim(UseCSGN.Text) & " to " & Trim(AtcCSGN.Text) & " ?"
            MyMsgBox.InfoApi 0, "Updates the flight", "Hint"
            AskRetVal = MyMsgBox.CallAskUser(0, 0, 0, "ATC Interface Transmit", MsgText, "ask", "Yes,No", UserAnswer)
            If AskRetVal = 1 Then
                AskRetVal = MyMsgBox.CallAskUser(0, 0, 0, "ATC CSGN Update", "Still under construction.", "hand", "", UserAnswer)
                AskRetVal = 1
            End If
        End If
        MsgText = Trim(txtTelexText.Text)
        If MsgText = "" Then AskRetVal = -1
        If AskRetVal = 1 Then
            Screen.MousePointer = 11
            OutFldLst = ""
            OldFldLst = ""
            OutDatLst = ""
            OldDatLst = ""
            linNbr = 0
            Do
                linNbr = linNbr + 1
                CurLinTxt = GetItem(MsgText, linNbr, vbNewLine)
                If CurLinTxt <> "" Then
                    CurNewDat = GetItem(CurLinTxt, 1, "OLD:")
                    CurOldDat = Trim(GetItem(CurLinTxt, 2, "OLD:"))
                    CurFldNam = Trim(GetItem(CurNewDat, 1, ":"))
                    CurFldDat = Trim(GetItem(CurNewDat, 2, ":"))
                    UseFldNam = CurFldNam
                    Select Case CurFldNam
                        Case "CSGN"
                        Case "ORG4"
                            'If CurFldDat = "LGAT" Then CurFldDat = "LGAV"
                            'If CurOldDat = "LGAT" Then CurOldDat = "LGAV"
                        Case "DES4"
                            'If CurFldDat = "LGAT" Then CurFldDat = "LGAV"
                            'If CurOldDat = "LGAT" Then CurOldDat = "LGAV"
                        Case "ETOD"
                            If CurFldDat <> "" Then CurFldDat = Left(CurFldDat & "00", 14)
                            If CurOldDat <> "" Then CurOldDat = Left(CurOldDat & "00", 14)
                            UseFldNam = "ETDA"
                        Case "ETOA"
                            If CurFldDat <> "" Then CurFldDat = Left(CurFldDat & "00", 14)
                            UseFldNam = "ETAA"
                        Case "LAND"
                            If CurFldDat <> "" Then CurFldDat = Left(CurFldDat & "00", 14)
                            UseFldNam = "LNDA"
                        Case "AIRB"
                            If CurFldDat <> "" Then CurFldDat = Left(CurFldDat & "00", 14)
                            UseFldNam = "AIRA"
                        Case "RWYA"
                        Case "RWYD"
                        Case "ACT3"
                        Case "DONE"
                            UseFldNam = ""
                            CurLinTxt = ""
                        Case Else
                            UseFldNam = ""
                    End Select
                    If UseFldNam <> "" Then
                        OutFldLst = OutFldLst & UseFldNam & ","
                        OutDatLst = OutDatLst & CurFldDat & ","
                        If CurOldDat <> "" Then
                            OldFldLst = OldFldLst & UseFldNam & ","
                            OldDatLst = OldDatLst & CurOldDat & ","
                        End If
                    End If
                End If
            Loop While CurLinTxt <> ""
            If OutFldLst <> "" Then
                OutFldLst = Left(OutFldLst, Len(OutFldLst) - 1)
                OutDatLst = Left(OutDatLst, Len(OutDatLst) - 1)
            End If
            If OldFldLst <> "" Then
                OldFldLst = Left(OldFldLst, Len(OldFldLst) - 1)
                OldDatLst = Left(OldDatLst, Len(OldDatLst) - 1)
            End If
            CedaFields = OutFldLst & vbLf & OldFldLst
            CedaData = OutDatLst & vbLf & OldDatLst
            'MsgBox CedaFields & vbNewLine & CedaData
            CedaRetVal = UfisServer.CallCeda(CedaResult, "EXCO", "AFTTAB", CedaFields, CedaData, "ATC", "", 0, True, False)
            Screen.MousePointer = 0
        End If
        AtcTransmit.Value = 0
    Else
        AtcTransmit.BackColor = vbButtonFace
    End If
End Sub

Private Sub chkAbout_Click()
    txtDummy.SetFocus
    If chkAbout.Value = 1 Then
        ShowChildForm Me, chkAbout, MyAboutBox, ""
        chkAbout.Value = 0
    Else
        'Unload frmAbout
    End If
End Sub

Private Sub chkAddress_Click()
    txtDummy.SetFocus
End Sub

Private Sub chkAssign_Click()
    If chkAssign.Value = 1 Then
        'If Trim(CurrentUrno.Text) <> "" Then
            ShowChildForm Me, chkAssign, AssignFlight, CurrentRecord
        'Else
        '    MyMsgBox.CallAskUser 0, 0, 0, "Assign Flight", "No telex selected.", "stop", "", UserAnswer
        '    chkAssign.Value = 0
        'End If
        chkAssign.Value = 0
    Else
        'Unload AssignFlight
    End If
End Sub

Private Sub chkEdit_Click()
    If chkEdit.Value = 1 Then
        If Trim(txtTelexText.Text) <> "" Then
            txtTelexText.Locked = False
            txtTelexText.SetFocus
            chkSend.Enabled = True
        Else
            MyMsgBox.CallAskUser 0, 0, 0, "Edit Telex", "Can't edit empty telexes!", "stop", "", UserAnswer
            chkEdit.Value = 0
        End If
    Else
        txtTelexText.Locked = True
        txtDummy.SetFocus
        chkSend.Enabled = False
    End If
End Sub

Private Sub chkClose_Click()
    If chkClose.Value = 1 Then
        'If CheckPendingEvent(RELEASE) < RC_NEWFOCUS Then
            Me.Hide
        'End If
        chkClose.Value = 0
    End If
End Sub

Private Sub chkExit_Click()
    If chkExit.Value = 1 Then
        'If CheckPendingEvent(RELEASE) < RC_NEWFOCUS Then
            ShutDownApplication
        'End If
        chkExit.Value = 0
    End If
End Sub

Private Sub chkFilter_Click(Index As Integer)
    If chkFilter(Index).Value = 1 Then chkFilter(Index).BackColor = LightestGreen
    Select Case Index
        Case 0  'Received
            If chkFilter(2).Value = 1 Then
                If chkFilter(Index).Value = 1 Then
                    ShowChildForm Me, chkFilter(Index), TlxRcvStat, ""
                    TlxRcvStat.SetMultiSelect True
                Else
                    TlxRcvStat.Hide
                End If
            End If
        Case 1  'Sent
            If chkFilter(2).Value = 1 Then
                If chkFilter(Index).Value = 1 Then
                    ShowChildForm Me, chkFilter(Index), TlxSndStat, ""
                    TlxSndStat.SetMultiSelect True
                Else
                    TlxSndStat.Hide
                End If
            End If
        Case 2  'Status
            chkStatus.Value = 0
            If chkFilter(Index).Value = 1 Then
                If chkFilter(1).Value = 1 Then
                    ShowChildForm Me, chkFilter(Index), TlxSndStat, ""
                    TlxSndStat.SetMultiSelect True
                End If
                If chkFilter(0).Value = 1 Then
                    ShowChildForm Me, chkFilter(Index), TlxRcvStat, ""
                    TlxRcvStat.SetMultiSelect True
                End If
            Else
                TlxRcvStat.Hide
                TlxSndStat.Hide
            End If
    End Select
    CheckLoadButton
    If chkFilter(Index).Value = 0 Then chkFilter(Index).BackColor = vbButtonFace
End Sub

Private Sub chkFlight_Click()
Dim RetVal As Boolean
Dim tmpData As String
Dim tmpVpfr As String
Dim tmpVpto As String
Dim FipsFldLst As String
Dim FipsDatLst As String
Dim FipsMsgTxt As String
Dim CurTelexRecord As String
    If chkFlight.Value = 1 Then
        If Trim(CurrentUrno.Text) <> "" Then
            CurTelexRecord = CurrentRecord.Text
            'VPFR , VPTO, ALC3, Fltn, Flns, Regn, Time, FLNU
            '20001210000000,20001210235900,CSA,420, , , ,27641709
            FipsFldLst = "VPFR,VPTO,ALC3,FLTN,FLNS,REGN,TIME,FLNU"
            FipsDatLst = ""
            RetVal = GetValidTimeFrame(tmpVpfr, tmpVpto, True)
            If RetVal = True Then
                FipsDatLst = FipsDatLst & tmpVpfr & ","
                FipsDatLst = FipsDatLst & tmpVpto & ","
                tmpData = Trim(GetFieldValue("ALC3", CurTelexRecord, DataPool.TlxTabFields))
                If tmpData = "" Then tmpData = Trim(txtFlca.Text)
                FipsDatLst = FipsDatLst & tmpData & ","
                tmpData = Trim(GetFieldValue("FLTN", CurTelexRecord, DataPool.TlxTabFields))
                If tmpData = "" Then tmpData = Trim(txtFltn.Text)
                FipsDatLst = FipsDatLst & tmpData & ","
                tmpData = Trim(GetFieldValue("FLNS", CurTelexRecord, DataPool.TlxTabFields))
                If tmpData = "" Then tmpData = Trim(txtFlns.Text)
                FipsDatLst = FipsDatLst & tmpData & ","
                FipsDatLst = FipsDatLst & Trim(txtRegn.Text) & ","
                tmpData = Trim(GetFieldValue("TIME", CurTelexRecord, DataPool.TlxTabFields))
                FipsDatLst = FipsDatLst & tmpData & ","
                tmpData = Trim(GetFieldValue("FLNU", CurTelexRecord, DataPool.TlxTabFields))
                If Val(tmpData) < 1 Then tmpData = ""
                FipsDatLst = FipsDatLst & tmpData
                FipsMsgTxt = FipsFldLst & vbLf & FipsDatLst
                CheckFormOnTop Me, False
                UfisServer.SendMessageToFips FipsMsgTxt
            Else
                MyMsgBox.CallAskUser 0, 0, 0, "Show Flight Filter", "The actual filter does not match your selection.", "stop", "", UserAnswer
            End If
            chkAssign.Value = 0
        Else
            MyMsgBox.CallAskUser 0, 0, 0, "Show Flight", "No telex selected.", "hand", "", UserAnswer
            chkAssign.Value = 0
        End If
        chkFlight.Value = 0
    Else
        'Unload FlightRotation
    End If
End Sub

Private Sub chkHelp_Click()
    If chkHelp.Value = 1 Then
        DisplayHtmHelpFile "", Me.Name, Me.ActiveControl.Name
        chkHelp.Value = 0
    End If
End Sub

Private Sub chkLoad_Click()
Dim tmpCheck As String
    txtDummy.SetFocus
    If chkLoad.Value = 1 Then
        chkLoad.BackColor = vbGreen
        chkLoad.ForeColor = vbBlack
        tmpCheck = CheckValidFilterFields
        If tmpCheck = "OK" Then
            'MsgBox NewFilterValues(CurMem).TlxKey
            LoadTelexData CurMem, NewFilterValues(CurMem).TlxKey, NewFilterValues(CurMem).TfrKey, "ALL", ""
        Else
            tmpCheck = "Wrong Filter Values!" & tmpCheck
            MyMsgBox.CallAskUser 0, 0, 0, "Load Telexes", tmpCheck, "stop", "", UserAnswer
            chkLoad.Value = 0
        End If
        CheckLoadButton
    End If
End Sub

Private Function CheckValidFilterFields() As String
Dim tmpResult As String
    tmpResult = ""
    CheckValidFilterFields = True
    If txtDateFrom.Tag = "" Then tmpResult = tmpResult & vbNewLine & "Date From"
    If txtDateFrom.Tag = "" Then tmpResult = tmpResult & vbNewLine & "Date To"
    If tmpResult = "" Then tmpResult = "OK"
    CheckValidFilterFields = tmpResult
End Function
Public Sub CheckLoadButton()
    If ApplicationIsStarted Then
        If TlxSqlKeyChanged(CurMem) Then
            chkLoad.BackColor = vbRed
            chkLoad.ForeColor = vbWhite
            chkReset.Enabled = True
        Else
            If optMem(CurMem).Tag > 0 Then
                chkLoad.BackColor = vbButtonFace
                chkLoad.ForeColor = vbBlack
            Else
                chkLoad.BackColor = DarkGreen
                chkLoad.ForeColor = vbWhite
            End If
            chkReset.Enabled = False
        End If
    End If
End Sub

Private Function TlxSqlKeyChanged(MemIdx As Integer) As Boolean
Dim clSqlKey As String
Dim clTimeFilter As String
Dim clFlightFilter As String
Dim clTtypFilter As String
Dim clTextFilter As String
Dim clSereFilter As String
Dim clStatFilter As String
Dim clOrderKey As String
Dim tmpData As String
Dim clDateFrom As String
Dim clDateTo As String
Dim clTimeFrom As String
Dim clTimeTo As String
Dim i As Integer
    If ApplicationIsStarted Then
        'Building the TimeFilter
        GetValidTimeFrame clDateFrom, clDateTo, True
        clTimeFilter = "TIME BETWEEN '" & clDateFrom & "' AND '" & clDateTo & "'"
        clSqlKey = "(" & clTimeFilter & ")"
        
        clFlightFilter = BuildFlightFilter("TLXTAB", clDateFrom, clDateTo, txtFlca, txtFltn, txtFlns, txtRegn)
        If clFlightFilter <> "" Then clSqlKey = clSqlKey & " AND (" & clFlightFilter & ")"
        
        'Building the TTYP-Filter
        clTtypFilter = ""
        If chkTabs(11).Value = 0 Then
            For i = 1 To tabTlxTypeFolder.Tabs.count - 2
                If chkTabs(i).Value = 1 Then
                    'clTtypFilter = clTtypFilter & Replace(tabTlxTypeFolder.Tabs(i).Tag, ";", ",", 1, -1, vbBinaryCompare) & ","
                    clTtypFilter = clTtypFilter & tabTlxTypeFolder.Tabs(i).Tag & ","
                End If
            Next
        End If
        If clTtypFilter <> "" Then
            clSqlKey = clSqlKey & " AND TTYP IN (" & Left(clTtypFilter, Len(clTtypFilter) - 1) & ")"
        End If
        
        'Building the TextFilter
        clTextFilter = ""
        If Trim(txtRegn) <> "" Then
            If clTextFilter <> "" Then clTextFilter = clTextFilter & " AND "
            clTextFilter = clTextFilter & "(TXT1 LIKE '%" & txtRegn & "%' OR TXT2 LIKE '%" & txtRegn & "%')"
        End If
        If Trim(txtAddr) <> "" Then
            If clTextFilter <> "" Then clTextFilter = clTextFilter & " AND "
            clTextFilter = clTextFilter & "(TXT1 LIKE '%" & txtAddr & "%' OR TXT2 LIKE '%" & txtAddr & "%')"
        End If
        If Trim(txtText) <> "" Then
            If clTextFilter <> "" Then clTextFilter = clTextFilter & " AND "
            clTextFilter = clTextFilter & "(TXT1 LIKE '%" & txtText & "%' OR TXT2 LIKE '%" & txtText & "%')"
        End If
        If clTextFilter <> "" Then clSqlKey = clSqlKey & " AND " & clTextFilter
        
        'Building the SERE/STAT-Filter
        clSereFilter = ""
        If chkFilter(0).Value = 1 Then
            clSereFilter = "SERE='R'"
            clStatFilter = ""
            If chkFilter(2).Value = 1 Then clStatFilter = TlxRcvStat.GetStatusFilter
            If clStatFilter <> "" Then clSereFilter = "(" & clSereFilter & " AND " & clStatFilter & ")"
        End If
        If chkFilter(1).Value = 1 Then
            clStatFilter = ""
            If chkFilter(2).Value = 1 Then clStatFilter = TlxSndStat.GetStatusFilter
            If clSereFilter <> "" Then
                If clStatFilter <> "" Then
                    clSereFilter = clSereFilter & " OR (SERE IN ('S','C') AND " & clStatFilter & ")"
                Else
                    If InStr(clSereFilter, "(") > 0 Then
                        clSereFilter = clSereFilter & " OR (SERE IN ('S','C'))"
                    Else
                        clSereFilter = clSereFilter & " OR SERE IN ('S','C')"
                    End If
                End If
            Else
                clSereFilter = "SERE IN ('S','C')"
                If clStatFilter <> "" Then clSereFilter = "(" & clSereFilter & " AND " & clStatFilter & ")"
            End If
        End If
        If clSereFilter <> "" Then clSqlKey = clSqlKey & " AND (" & clSereFilter & ")"
        clOrderKey = MySetUp.OrderKey.Text
        If clOrderKey <> "" Then clSqlKey = clSqlKey & " ORDER BY " & clOrderKey
        NewFilterValues(MemIdx).TlxKey = "WHERE " & clSqlKey
        NewFilterValues(MemIdx).TfrKey = BuildFlightFilter("TFRTAB", clDateFrom, clDateTo, txtFlca, txtFltn, txtFlns, txtRegn)
        If NewFilterValues(MemIdx).TlxKey <> ActFilterValues(MemIdx).TlxKey Then TlxSqlKeyChanged = True Else TlxSqlKeyChanged = False
    End If
End Function

Private Function BuildFlightFilter(ForWhat As String, DateFrom As String, DateTo As String, _
                AftAlc3 As String, AftFltn As String, AftFlns As String, AftRegn As String)
Dim tlxFlightFilter As String
Dim tfrFlightFilter As String
Dim tmpTfrFilter As String
Dim Result As String
Dim tmpDateFrom As String
Dim tmpDateTo As String
Dim tmpAlc3 As String
Dim tmpFltn As String
Dim tmpFlns As String
Dim tmpRegn As String
Dim tmpData As String
    Result = ""
    tmpFltn = Trim(AftFltn)
    tmpAlc3 = Trim(AftAlc3)
    tmpFlns = Trim(AftFlns)
    If ForWhat = "TLXTAB" Then
        tlxFlightFilter = AppendToFilter("", "AND", "FLTN", tmpFltn)
        tlxFlightFilter = AppendToFilter(tlxFlightFilter, "AND", "ALC3", tmpAlc3)
        tlxFlightFilter = AppendToFilter(tlxFlightFilter, "AND", "FLNS", tmpFlns)
        Result = tlxFlightFilter
    ElseIf ForWhat = "TFRTAB" Then
        NewFilterValues(CurMem).TfrKey = ""
        tmpDateFrom = Trim(Left(DateFrom, 8))
        tmpDateTo = Trim(Left(DateTo, 8))
        tmpRegn = Trim(AftRegn)
        tmpData = tmpAlc3 & tmpFltn & tmpFlns & tmpRegn
        If tmpData <> "" Then
            'Filter Combinations for reading the TFRTAB:
            'Filter 1: Full FlightNumber with [empty] FLNS
            'Filter 2: ALC3 and Suffix (FLTN empty)
            'Filter 3: ALC3 only (other values empty)
            'Filter 4: FLNS only (other values empty)
            'Filter 5: REGN only
            tfrFlightFilter = "((VATO>='" & DateFrom & "' AND VAFR<='" & DateTo & "') AND ("
            If tmpAlc3 <> "" And tmpFltn <> "" Then
            'Filter 1: Full FlightNumber with [empty] FLNS
                tmpTfrFilter = AppendToFilter("", "AND", "FLTN", tmpFltn)
                tmpTfrFilter = AppendToFilter(tmpTfrFilter, "AND", "ALC3", tmpAlc3)
                If tmpFlns <> "" Then
                    tmpTfrFilter = AppendToFilter(tmpTfrFilter, "AND", "FLNS", tmpFlns)
                Else
                    tmpTfrFilter = AppendToFilter(tmpTfrFilter, "AND", "FLNS", " ")
                End If
                tfrFlightFilter = tfrFlightFilter & "(" & tmpTfrFilter & ") OR "
            End If
            If tmpAlc3 <> "" And tmpFlns <> "" Then
                'Filter 2: ALC3 and Suffix (FLTN empty)
                tmpTfrFilter = AppendToFilter("", "AND", "FLTN", " ")
                tmpTfrFilter = AppendToFilter(tmpTfrFilter, "AND", "ALC3", tmpAlc3)
                tmpTfrFilter = AppendToFilter(tmpTfrFilter, "AND", "FLNS", tmpFlns)
                tfrFlightFilter = tfrFlightFilter & "(" & tmpTfrFilter & ") OR "
            End If
            If tmpAlc3 <> "" Then
                'Filter 3: ALC3 only (other values empty)
                tmpTfrFilter = AppendToFilter("", "AND", "FLTN", " ")
                tmpTfrFilter = AppendToFilter(tmpTfrFilter, "AND", "ALC3", tmpAlc3)
                tmpTfrFilter = AppendToFilter(tmpTfrFilter, "AND", "FLNS", " ")
                tfrFlightFilter = tfrFlightFilter & "(" & tmpTfrFilter & ") OR "
            End If
            If tmpFlns <> "" Then
                'Filter 4: FLNS only (other values empty)
                tmpTfrFilter = AppendToFilter("", "AND", "FLTN", " ")
                tmpTfrFilter = AppendToFilter(tmpTfrFilter, "AND", "ALC3", " ")
                tmpTfrFilter = AppendToFilter(tmpTfrFilter, "AND", "FLNS", tmpFlns)
                tfrFlightFilter = tfrFlightFilter & "(" & tmpTfrFilter & ") OR "
            End If
            If tmpRegn <> "" Then
                'Filter 5: REGN only
                tmpTfrFilter = AppendToFilter("", "AND", "REGN", tmpRegn)
                tfrFlightFilter = tfrFlightFilter & "(" & tmpTfrFilter & ") OR "
            End If
            'Here the Filter ends with '..OR.' this has to be cut
            tfrFlightFilter = Left(tfrFlightFilter, Len(tfrFlightFilter) - 4) & "))"
            'Remark: ..SELECT TURN.. might be changed to ..SELECT DISTINCT(TURN)..
            'MsgBox tfrFlightFilter
            Result = "WHERE URNO IN (SELECT DISTINCT(TURN) FROM " & "TFRTAB" & " WHERE " & tfrFlightFilter & ")"
        End If
    End If
    BuildFlightFilter = Result
End Function
Private Function AppendToFilter(CurFilter As String, AppOperator As String, _
                                FieldName As String, FieldValue As String)
Dim Result As String
    Result = CurFilter
    If FieldValue <> "" Then
        If Result <> "" Then Result = Result & " " & AppOperator & " "
        If InStr(FieldValue, "%") = 0 Then
            Result = Result & FieldName & "='" & FieldValue & "'"
        Else
            Result = Result & FieldName & " LIKE '" & FieldValue & "'"
        End If
    End If
    AppendToFilter = Result
End Function

Private Sub chkMail_Click()
    txtDummy.SetFocus
    If chkMail.Value = 1 Then
        'VbMailBox.Show
    Else
        'Unload VbMailBox
    End If
End Sub

Private Sub chkNew_Click()
    If chkNew.Value = 1 Then
        txtTelexText.Text = MySetUp.sndTlxTxtDefault.Text
        txtTelexText.Locked = False
        chkSend.Enabled = True
    Else
        txtTelexText.Locked = True
        chkSend.Enabled = False
    End If
    txtDummy.SetFocus
End Sub

Private Sub chkOnTime_Click()
    txtDummy.SetFocus
End Sub

Private Sub chkPrint_Click()
    txtDummy.SetFocus
    If chkPrint.Value = 1 Then
        MyMsgBox.CallAskUser 0, 0, 0, "Printer Manager", "Can't find a printer", "stop", "", UserAnswer
        chkPrint.Value = 0
    End If
End Sub

Private Sub chkPrintTxt_Click()
Dim tmpUrno As String
    txtDummy.SetFocus
    TelexPoolHead.MousePointer = 11
    If chkPrintTxt.Value = 1 Then
        tmpUrno = Trim(CurrentUrno.Text)
        If tmpUrno <> "" Then
            PrintServer.PrintOutput txtTelexText
        Else
            MyMsgBox.CallAskUser 0, 0, 0, "Print Manager", "No data selected for print.", "stop", "", UserAnswer
        End If
        chkPrintTxt.Value = 0
    End If
    TelexPoolHead.MousePointer = 0
End Sub

Private Sub chkReject_Click()
    If chkReject.Value = 1 Then
        UpdateTelexStatus "X"
        chkReject.Value = 0
    End If
    txtDummy.SetFocus
End Sub
Private Sub UpdateTelexStatus(SetStat As String)
Dim tmpUrno As String
Dim tmpSqlKey As String
Dim RetVal As String
Dim RetCode As Integer
    tmpUrno = Trim(CurrentUrno.Text)
    If tmpUrno <> "" Then
        tmpSqlKey = "WHERE URNO=" & tmpUrno
        RetCode = UfisServer.CallCeda(RetVal, "URT", DataPool.TableName, "WSTA", SetStat, tmpSqlKey, "", 0, True, False)
    Else
        MyMsgBox.CallAskUser 0, 0, 0, "Update Manager", "No data selected to update.", "stop", "", UserAnswer
    End If
End Sub
Private Sub chkReset_Click()
    txtDummy.SetFocus
    If chkReset.Value = 1 Then
        ResetFolderValues CurMem
        chkReset.Value = 0
    End If
End Sub

Private Sub chkSend_Click()
Dim TelexText As String
Dim ErrMsg As String
Dim tmpResult As String
Dim retCod As Integer
Dim outData As String
Dim outFields As String
Dim SendIt As Boolean

    txtDummy.SetFocus
    If chkSend.Value = 1 Then
        SendIt = True
        ErrMsg = ""
        TelexText = Trim(txtTelexText.Text)
        If InStr(TelexText, "=TEXT") = 0 Then
            ErrMsg = ErrMsg & "Missing keyword: '=TEXT'" & vbNewLine
            SendIt = False
        End If
        If InStr(TelexText, "=DESTINATION TYPE B") = 0 Then
            ErrMsg = ErrMsg & "Missing keyword: '=DESTINATION TYPE B'" & vbNewLine
            SendIt = False
        End If
        If SendIt Then
            'No longer needed. Done by TlxGen
            'MySetUp.WriteTelexToFile TelexText
            outFields = "SERE,STAT,CDAT,TIME,TXT1"
            outData = "S,S"
            outData = outData & "," & Format(Now, "yyyymmddhhmmss")
            outData = outData & "," & Format(Now, "yyyymmddhhmmss")
            outData = outData & "," & CleanString(TelexText, FOR_SERVER, False)
            retCod = UfisServer.CallCeda(tmpResult, "IBT", "TLXTAB", outFields, outData, "          ", "", 0, True, False)
            'MsgBox tmpResult
            MyMsgBox.CallAskUser 0, 0, 0, "Send Telex", "The telex is on the way now.", "export", "", UserAnswer
            UpdateTelexStatus "S"
            chkEdit.Value = 0
            chkNew.Value = 0
        Else
            If TelexText = "" Then ErrMsg = "Can't send empty telexes!"
            MyMsgBox.CallAskUser 0, 0, 0, "Send Telex", ErrMsg, "stop", "", UserAnswer
        End If
        chkSend.Value = 0
    End If
End Sub

Private Sub chkSetup_Click()
    txtDummy.SetFocus
    If chkSetup.Value = 1 Then
        ShowChildForm Me, chkSetup, PoolConfig, ""
    Else
        PoolConfig.Hide
    End If
End Sub

Private Sub chkStatus_Click()
    Dim SelLine As Long
    Dim tmpUrno As String
    Dim tmpStat As String
    If chkStatus.Value = 1 Then
        tmpUrno = Trim(CurrentUrno.Text)
        If tmpUrno <> "" Then
            If chkTlxTitleBar(1).Value = 0 Then
                ShowChildForm Me, chkStatus, TlxSndStat, ""
            End If
            If chkTlxTitleBar(0).Value = 0 Then
                ShowChildForm Me, chkStatus, TlxRcvStat, ""
                SelLine = tabTelexList(0).GetCurrentSelected
                tmpStat = tabTelexList(0).GetColumnValue(SelLine, 0)
                TlxRcvStat.ExplainStatus "STAT", tmpStat, True
                tmpStat = tabTelexList(0).GetColumnValue(SelLine, 1)
                TlxRcvStat.ExplainStatus "WSTA", tmpStat, True
            End If
        Else
            chkStatus.Value = 0
        End If
    Else
        TlxRcvStat.Hide
        TlxSndStat.Hide
    End If
End Sub

Private Sub chkTabs_Click(Index As Integer)
Static ChkTabsIsBusy As Boolean
Dim MaxIdx As Integer
Dim LastIdx As Integer
Dim i As Integer
Dim j As Integer
Dim TabSet As Boolean
    If Not ChkTabsIsBusy Then
        LastIdx = tabTlxTypeFolder.Tabs.count
        MaxIdx = tabTlxTypeFolder.Tabs.count - 2
        If Index <= MaxIdx Then
            If (chkTabs(Index).Value = 1) And (chkTabs(LastIdx).Value = 1) Then
                chkTabs(Index).Tag = "1"
                chkTabs(LastIdx).Value = 0
            Else
                If chkTabs(Index).Value = 0 Then
                    j = 0
                    i = 0
                    While j = 0 And i < MaxIdx
                        i = i + 1
                        If chkTabs(i).Value = 1 Then
                            j = j + 1
                        End If
                    Wend
                    If j = 0 Then chkTabs(LastIdx).Value = 1
                End If
            End If
        Else
            If chkTabs(LastIdx).Value = 1 Then
                ChkTabsIsBusy = True
                For i = 1 To MaxIdx
                    If chkTabs(i).Value = 1 Then
                        chkTabs(i).Value = 0
                        chkTabs(i).Tag = "1"
                    End If
                Next
                ChkTabsIsBusy = False
            Else
                ChkTabsIsBusy = True
                TabSet = False
                For i = 1 To MaxIdx
                    If chkTabs(i).Tag = "1" Then
                        chkTabs(i).Value = 1
                        chkTabs(i).Tag = ""
                        TabSet = True
                    End If
                Next
                ChkTabsIsBusy = False
                If Index = LastIdx And Not TabSet Then chkTabs(LastIdx).Value = 1
            End If
        End If
        CheckLoadButton
        ChkTabsIsBusy = False
    End If
End Sub

Public Sub SearchFlightTelexes(tmpFldLst As String, tmpDatLst As String)
    Dim tmpData As String
    Dim UseServerTime
    Dim BeginTime
    Dim EndTime
    optMem(1).Value = True
    tmpData = GetFieldValue("ADID", tmpDatLst, tmpFldLst)
    If tmpData = "D" Then
        tmpData = GetFieldValue("STOD", tmpDatLst, tmpFldLst)
    Else
        tmpData = GetFieldValue("STOA", tmpDatLst, tmpFldLst)
    End If
    
    UseServerTime = CedaFullDateToVb(tmpData)
    If optLocal.Value = True Then UseServerTime = DateAdd("n", UtcTimeDiff, UseServerTime)
    BeginTime = DateAdd("h", -Abs(Val(MySetUp.MainFrom)), UseServerTime)
    EndTime = DateAdd("h", Abs(Val(MySetUp.MainTo)), UseServerTime)
    txtDateFrom.Text = Format(BeginTime, MySetUp.DefDateFormat)
    txtDateFrom.Tag = Format(BeginTime, "yyyymmdd")
    txtDateTo.Text = Format(EndTime, MySetUp.DefDateFormat)
    txtDateTo.Tag = Format(EndTime, "yyyymmdd")
    txtTimeFrom.Text = Format(BeginTime, "hh:mm")
    txtTimeTo.Text = Format(EndTime, "hh:mm")
    
    txtFlca = GetFieldValue("ALC3", tmpDatLst, tmpFldLst)
    txtFltn = GetFieldValue("FLTN", tmpDatLst, tmpFldLst)
    txtFlns = GetFieldValue("FLNS", tmpDatLst, tmpFldLst)
    Me.Refresh
    chkLoad.Value = 1
End Sub

Private Sub FixWin_Click()
    On Error Resume Next
    If FixWin.Value = 1 Then
        AutoArrange = True
    Else
        AutoArrange = False
    End If
    ArrangeAllWin
    txtDummy.SetFocus
End Sub
Public Sub ArrangeAllWin()
    On Error Resume Next
    If AutoArrange Then
        If WindowState <> vbNormal Then WindowState = vbNormal
        Refresh
        InitPosSize False, True
        SetFocus
    End If
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyF1 Then DisplayHtmHelpFile "", Me.Name, Me.ActiveControl.Name
End Sub

Private Sub Form_Load()
    InitGrids 8
    InitPosSize False, False
    ReorgLayers
    UfisServer.SetIndicator CedaStatus(0).Left, CedaStatus(0).Top, CedaStatus(0), CedaStatus(3), CedaStatus(1), CedaStatus(2)
    UfisServer.ConnectToCeda
    UfisServer.SetUtcTimeDiff
    optUtc.ToolTipText = "Times in UTC (" & HomeAirport & " Local -" & Str(UtcTimeDiff) & " min.)"
    optLocal.ToolTipText = "Times in " & HomeAirport & " Local (UTC +" & Str(UtcTimeDiff) & " min.)"
End Sub

Public Sub InitPosSize(FreePos As Boolean, CheckOnline As Boolean)
    If FreePos = False Then
        Top = 650
        Left = 500
        Height = 8820
        Width = 10600
    End If
    If CheckOnline = True Then
        If OnLine.Value = 1 Then
            If PoolConfig.OnLineCfg(1).Value = 1 Then
                RcvTelex.Show
                RcvTelex.WindowState = vbNormal
                RcvTelex.Refresh
                RcvTelex.OnTop.Value = OnTop.Value
                RcvTelex.InitPosSize False, False
            End If
            If PoolConfig.OnLineCfg(2).Value = 1 Then
                SndTelex.Show
                SndTelex.WindowState = vbNormal
                SndTelex.Refresh
                SndTelex.OnTop.Value = OnTop.Value
                SndTelex.InitPosSize False, False
            End If
        End If
    End If
End Sub

Public Sub InitMyForm() 'Called by StartModule
    InitHiddenFields
    Load MySetUp
    Load DataPool
    InitDateFields
    InitFolderByName "All", True
End Sub
Public Sub InitMemButtons()
    For CurMem = 0 To 3
        optMem(CurMem).Tag = -2
        optMem(CurMem).ToolTipText = "Ready for use"
        CheckLoadButton
        StoreFolderValues (CurMem)
        ActFilterValues(CurMem) = NewFilterValues(CurMem)
    Next
    optMem(0).Value = True
    CurMem = 0
End Sub
Public Sub InitDateFields()
    Dim UseServerTime
    Dim BeginTime
    Dim EndTime
    optUtc.Value = True
    txtDateFrom.ToolTipText = "Day Date " & MySetUp.DefDateFormat.Text
    txtDateTo.ToolTipText = "Day Date " & MySetUp.DefDateFormat.Text
    UseServerTime = CedaFullDateToVb(MySetUp.ServerTime.Tag)
    If MySetUp.ServerTimeType <> "UTC" Then UseServerTime = DateAdd("n", -UtcTimeDiff, UseServerTime)
    BeginTime = DateAdd("h", -Abs(Val(MySetUp.MainFrom)), UseServerTime)
    EndTime = DateAdd("h", Abs(Val(MySetUp.MainTo)), UseServerTime)
    txtDateFrom.Text = Format(BeginTime, MySetUp.DefDateFormat)
    txtDateFrom.Tag = Format(BeginTime, "yyyymmdd")
    txtDateTo.Text = Format(EndTime, MySetUp.DefDateFormat)
    txtDateTo.Tag = Format(EndTime, "yyyymmdd")
    txtTimeFrom.Text = Format(BeginTime, "hh:mm")
    txtTimeTo.Text = Format(EndTime, "hh:mm")
End Sub
Private Sub InitHiddenFields()
    CurrentUrno = ""
    CurrentRecord = ""
End Sub
Private Sub InitGrids(ipLines As Integer)
    Dim i As Integer
    Dim hdrLenLst As String
    hdrLenLst = ""
    hdrLenLst = hdrLenLst & TextWidth("S") / Screen.TwipsPerPixelX + 5 & ","
    hdrLenLst = hdrLenLst & TextWidth("W") / Screen.TwipsPerPixelX + 5 & ","
    hdrLenLst = hdrLenLst & TextWidth("CTOT") / Screen.TwipsPerPixelX + 5 & ","
    hdrLenLst = hdrLenLst & TextWidth("ST") / Screen.TwipsPerPixelX + 5 & ","
    hdrLenLst = hdrLenLst & TextWidth("00:00") / Screen.TwipsPerPixelX + 5 & ","
    hdrLenLst = hdrLenLst & TextWidth("001") / Screen.TwipsPerPixelX + 5 & ","
    hdrLenLst = hdrLenLst & TextWidth("Very long Text                                                                                ") / Screen.TwipsPerPixelX & ","
    hdrLenLst = hdrLenLst & TextWidth("1234567890") / Screen.TwipsPerPixelX & ","
    hdrLenLst = hdrLenLst & TextWidth("123456") / Screen.TwipsPerPixelX

    For i = 0 To 1
        tabTelexList(i).ResetContent
        tabTelexList(i).FontName = "Courier New"
        tabTelexList(i).HeaderFontSize = 14
        tabTelexList(i).FontSize = 14
        tabTelexList(i).LineHeight = 14
        tabTelexList(i).Top = txtTelexList(i).Top + 1 * Screen.TwipsPerPixelY
        tabTelexList(i).Left = txtTelexList(i).Left + 1 * Screen.TwipsPerPixelX
        tabTelexList(i).Width = txtTelexList(i).Width - 2 * Screen.TwipsPerPixelX
        tabTelexList(i).Height = ipLines * tabTelexList(i).LineHeight * Screen.TwipsPerPixelY
        txtTelexList(i).Height = tabTelexList(i).Height + 2 * Screen.TwipsPerPixelY
        tabTelexList(i).HeaderLengthString = hdrLenLst
        tabTelexList(i).HeaderString = "S,W,Type,ST,Time,No,Text Extract,URNO,Index"
        tabTelexList(i).ShowHorzScroller True
        tabTelexList(i).DateTimeSetColumn 4
        tabTelexList(i).DateTimeSetInputFormatString 4, "YYYYMMDDhhmmss"
        tabTelexList(i).DateTimeSetOutputFormatString 4, "hh':'mm"
    Next i
    fraSplitter(0).Top = chkTlxTitleBar(0).Top
    fraSplitter(0).Left = chkTlxTitleBar(0).Left + chkTlxTitleBar(0).Width
    fraSplitter(0).Height = chkTlxTitleBar(0).Height + txtTelexList(0).Height + 2 * Screen.TwipsPerPixelY
    fraSplitter(2).Left = chkTlxTitleBar(0).Left
    fraSplitter(2).Top = txtTelexList(0).Top + txtTelexList(0).Height
    fraSplitter(2).Width = txtTelexList(1).Left + txtTelexList(1).Width - txtTelexList(0).Left
    fraSplitter(1).Top = fraSplitter(2).Top - 4 * Screen.TwipsPerPixelY
    fraSplitter(1).Left = fraSplitter(0).Left - 4 * Screen.TwipsPerPixelX
    txtTelexText.Top = txtTelexList(0).Top + txtTelexList(0).Height + 9 * Screen.TwipsPerPixelY
    txtTelexText.Left = txtTelexList(0).Left
    txtTelexText.Width = txtTelexList(1).Left + txtTelexList(1).Width - txtTelexList(0).Left
    txtTelexText.Height = tabTlxTypeFolder.Top + tabTlxTypeFolder.Height - txtTelexText.Top - 9 * Screen.TwipsPerPixelY
    RightCover.Top = 0
    folderButtons(0).Top = tabTlxTypeFolder.Top + tabTlxTypeFolder.TabFixedHeight + 2 * Screen.TwipsPerPixelY
    folderButtons(0).Left = RightCover.Left
    folderButtons(0).Tag = tabTlxTypeFolder.SelectedItem.Index
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    If UnloadMode = 0 Then
        Cancel = CheckCancelExit(Me, 3)
    End If
End Sub

Private Sub Form_Resize()
    Dim ilVal As Integer
    Dim newSize As Long
    Dim SizeDiff As Long
    Dim newLeft As Long
    Dim newDist As Long
    Dim newTxtHeight As Long
    Dim newTabHeight As Long
    If Me.WindowState <> vbMinimized Then
        If Me.ScaleHeight > MyStatusBar.Height + 6 Then
            RightCover.Height = Me.ScaleHeight - MyStatusBar.Height
            BorderLine(0).Y2 = RightCover.Height - 5 * Screen.TwipsPerPixelY
            BorderLine(1).Y2 = BorderLine(0).Y2
        End If
        newTabHeight = TelexPoolHead.ScaleHeight - tabTlxTypeFolder.Top - 25 * Screen.TwipsPerPixelY
        newTxtHeight = tabTlxTypeFolder.Top + newTabHeight - txtTelexText.Top - 6 * Screen.TwipsPerPixelY
        If newTxtHeight > 450 Then
            tabTlxTypeFolder.Height = newTabHeight
            txtTelexText.Height = newTxtHeight
        End If
        ilVal = folderButtons(0).Tag
        If ilVal > 0 Then folderButtons(ilVal).Top = folderButtons(0).Top
        basicButtons.Top = TelexPoolHead.ScaleHeight - basicButtons.Height - 23 * Screen.TwipsPerPixelY
        RightCover.Left = TelexPoolHead.ScaleWidth - RightCover.Width
        newLeft = RightCover.Left + RightCover.Width / 2 - OnTop.Width / 2
        TopButtons.Left = newLeft
        basicButtons.Left = newLeft
        folderButtons(0).Left = RightCover.Left + 4 * Screen.TwipsPerPixelX
        ilVal = folderButtons(0).Tag
        folderButtons(ilVal).Left = folderButtons(0).Left
        newLeft = folderButtons(0).Left
        newSize = newLeft - tabTlxTypeFolder.Left - 2 * Screen.TwipsPerPixelX
        If newSize > 7350 Then tabTlxTypeFolder.Width = newSize
        newSize = newLeft - txtTelexList(1).Left - 8 * Screen.TwipsPerPixelX
        If newSize > 850 Then
            SizeDiff = newLeft - txtTelexList(1).Left - 8 * Screen.TwipsPerPixelX - txtTelexList(1).Width
            txtTelexList(1).Width = txtTelexList(1).Width + SizeDiff
            tabTelexList(1).Width = tabTelexList(1).Width + SizeDiff
            chkTlxTitleBar(1).Width = chkTlxTitleBar(1).Width + SizeDiff
            fraSplitter(2).Width = fraSplitter(2).Width + SizeDiff
            txtTelexText.Width = newLeft - txtTelexText.Left - 8 * Screen.TwipsPerPixelX
        End If
        CedaBusyFlag.Top = TelexPoolHead.ScaleHeight - MyStatusBar.Height + 2 * Screen.TwipsPerPixelY
        CedaBusyFlag.Left = TelexPoolHead.ScaleWidth - CedaBusyFlag.Width - 12 * Screen.TwipsPerPixelX
    End If
    CheckArrange
End Sub

Private Sub CheckArrange()
    Refresh
    If AutoArrange And OnLine.Value = 1 Then
        If PoolConfig.OnLineCfg(2).Value = 1 Then
            If SndTelex.Visible = True And SndTelex.WindowState = vbNormal Then
                SndTelex.InitPosSize False, True
            End If
        End If
        If PoolConfig.OnLineCfg(1).Value = 1 Then
            If RcvTelex.Visible = True And RcvTelex.WindowState = vbNormal Then
                RcvTelex.InitPosSize False, True
            End If
        End If
    End If
End Sub

Private Sub fraSplitter_MouseDown(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
    fraSplitter(0).Tag = X
    fraSplitter(2).Tag = Y
End Sub

Private Sub fraSplitter_MouseMove(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
    Dim ilDiffX As Integer
    Dim ilDiffY As Integer
    Dim leftWidth As Integer
    Dim rightWidth As Integer
    Dim topHeight As Integer
    Dim bottomHeight As Integer
    
    If Index <= 1 Then
        If fraSplitter(0).Tag > 0 And X <> fraSplitter(0).Tag Then
            ilDiffX = X - fraSplitter(0).Tag
            leftWidth = chkTlxTitleBar(0).Width + ilDiffX
            rightWidth = chkTlxTitleBar(1).Width - ilDiffX
            If (leftWidth > 690) And (rightWidth > 690) Then
                fraSplitter(0).Left = fraSplitter(0).Left + ilDiffX
                fraSplitter(1).Left = fraSplitter(1).Left + ilDiffX
                chkTlxTitleBar(0).Width = chkTlxTitleBar(0).Width + ilDiffX
                tabTelexList(0).Width = tabTelexList(0).Width + ilDiffX
                txtTelexList(0).Width = txtTelexList(0).Width + ilDiffX
                chkTlxTitleBar(1).Width = chkTlxTitleBar(1).Width - ilDiffX
                chkTlxTitleBar(1).Left = chkTlxTitleBar(1).Left + ilDiffX
                tabTelexList(1).Width = tabTelexList(1).Width - ilDiffX
                tabTelexList(1).Left = tabTelexList(1).Left + ilDiffX
                txtTelexList(1).Width = txtTelexList(1).Width - ilDiffX
                txtTelexList(1).Left = txtTelexList(1).Left + ilDiffX
                TelexPoolHead.Refresh
            End If
        End If
    End If
    If Index >= 1 Then
        If fraSplitter(2).Tag > 0 And Y <> fraSplitter(2).Tag Then
            ilDiffY = Y - fraSplitter(2).Tag
            topHeight = tabTelexList(0).Height + ilDiffY
            bottomHeight = txtTelexText.Height - ilDiffY
            If (topHeight > 465) And (bottomHeight > 465) Then
                fraSplitter(0).Height = fraSplitter(0).Height + ilDiffY
                fraSplitter(1).Top = fraSplitter(1).Top + ilDiffY
                fraSplitter(2).Top = fraSplitter(2).Top + ilDiffY
                tabTelexList(0).Height = tabTelexList(0).Height + ilDiffY
                tabTelexList(1).Height = tabTelexList(1).Height + ilDiffY
                TelexPoolHead.Refresh
                txtTelexList(0).Height = txtTelexList(0).Height + ilDiffY
                txtTelexList(1).Height = txtTelexList(1).Height + ilDiffY
                txtTelexText.Top = txtTelexText.Top + ilDiffY
                txtTelexText.Height = txtTelexText.Height - ilDiffY
                TelexPoolHead.Refresh
            End If
        End If
    End If
End Sub
Private Sub fraSplitter_MouseUp(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
    fraSplitter(0).Tag = -1
    fraSplitter(2).Tag = -1
End Sub
Public Sub LoadTelexData(MemIdx As Integer, tlxSqlKey As String, TfrSqlKey As String, ChooseFolder As String, ClickOnList As String)
Dim ReqLoop As Integer
Dim ActSqlKey As String
Dim tmpCmd As String
Dim tmpTable As String
Dim tmpFields As String
Dim tmpData As String
Dim Line As String, retStr As String
Dim count As Long
Dim TfrCount As Long
Dim strArr() As String
Dim tmpTtyp As String
Dim tmpSere As String
Dim TtypIdx As Integer
Dim SereIdx As Integer
Dim oldTtyp As String
Dim tmpUrno As String
Dim tmpTlxData As String
Dim i As Long
Dim ilTabNbr As Integer
Dim ilLinNbr As Long
Dim clTtypList(25) As String
Dim RetCode As Integer
Dim myForeColor As Long
Dim myBackColor As Long
    'MsgBox "FROM TLXTAB:" & vbNewLine & tlxSqlKey
    'MsgBox "FROM TFRTAB:" & vbNewLine & TfrSqlKey
    If Visible <> True Then Show
    If WindowState = vbMinimized Then WindowState = vbNormal
    Refresh
    TelexPoolHead.MousePointer = 11
    optMem(MemIdx).Tag = -1
    optMem(MemIdx).Value = True     'Changes global CurMem also
    ResetTlxTypeFolder
    tabTelexList(0).ResetContent
    tabTelexList(1).ResetContent
    txtTelexText.Text = ""
    DataPool.TelexData(MemIdx).ResetContent
    DataPool.TtypIdx(MemIdx).ResetContent
    For i = 0 To chkTabs.count - 1
        chkTabs(i).Caption = ""
    Next
    NewFilterValues(MemIdx).TlxKey = tlxSqlKey
    NewFilterValues(MemIdx).TfrKey = TfrSqlKey
    count = -2
    clTtypList(0) = "????,"
    tmpFields = DataPool.TlxTabFields.Text
    tmpData = ""
    tmpCmd = "RTA"
    tmpTable = DataPool.TableName.Text
    ilLinNbr = 0
    oldTtyp = "..."
    TtypIdx = GetItemNo(tmpFields, "TTYP")
    SereIdx = GetItemNo(tmpFields, "SERE")
    ActSqlKey = tlxSqlKey
    myForeColor = vbBlack
    myBackColor = vbWhite
    For ReqLoop = 1 To 2
        If ActSqlKey <> "" Then
            'MsgBox tmpFields
            'MsgBox ActSqlKey
            If UfisServer.CallCeda(retStr, tmpCmd, tmpTable, tmpFields, tmpData, ActSqlKey, "", 0, False, False) = 0 Then
                count = UfisServer.DataBuffer(0).GetLineCount - 1
                If count >= 0 Then
                    For i = 0 To count
                        Line = UfisServer.DataBuffer(0).GetLineValues(i)
                        'MsgBox Line
                        DataPool.TelexData(MemIdx).InsertTextLine Line, False
                        DataPool.TelexData(MemIdx).SetLineColor ilLinNbr, myForeColor, myBackColor
                        tmpSere = GetItem(Line, SereIdx, ",")
                        If tmpSere <> "" Then
                            If InStr("RSC", tmpSere) > 0 Then
                                tmpTtyp = GetItem(Line, TtypIdx, ",")
                                If tmpTtyp <> oldTtyp Then
                                    ilTabNbr = GetTabFolderByName(tmpTtyp)
                                    oldTtyp = tmpTtyp
                                End If
                                If clTtypList(ilTabNbr) = "" Then clTtypList(ilTabNbr) = tmpTtyp & ","
                                clTtypList(ilTabNbr) = clTtypList(ilTabNbr) & Trim(Str(ilLinNbr)) & ";"
                                chkTabs(ilTabNbr).Caption = Str(Val(chkTabs(ilTabNbr).Caption) + 1)
                                ilLinNbr = ilLinNbr + 1
                                chkTabs(11).Caption = Str(ilLinNbr)
                            End If
                        End If
                    Next i
                    If ReqLoop = 2 Then TfrCount = count + 1
                End If
                UfisServer.ResetBuffer 0
            End If
        End If
        ActSqlKey = TfrSqlKey
        myForeColor = LightestGreen
        myBackColor = DarkestGreen
    Next
    chkTabs(10).Caption = chkTabs(0).Caption
    clTtypList(10) = clTtypList(0)
    DataPool.TtypIdx(MemIdx).ResetContent
    For i = 0 To 11
        If clTtypList(i) = "" Then clTtypList(i) = ",,"
        DataPool.TtypIdx(MemIdx).InsertTextLine clTtypList(i), False
    Next
    DataPool.TtypIdx(MemIdx).RedrawTab
    chkLoad.Value = 0
    If ChooseFolder <> "" Then
        InitFolderByName ChooseFolder, True
    End If
    tabTlxTypeFolder_Click
    If ClickOnList <> "" Then
        If ClickOnList = "RCV" Then
            tabTelexList(0).SetCurrentSelection 0
            If chkTlxTitleBar(0).Value = 0 Then FillTelexTextField Else chkTlxTitleBar(0).Value = 0
        ElseIf ClickOnList = "SND" Then
            tabTelexList(1).SetCurrentSelection 0
            If chkTlxTitleBar(1).Value = 0 Then FillTelexTextField Else chkTlxTitleBar(1).Value = 0
        End If
    End If
    optMem(MemIdx).Tag = ilLinNbr
    Select Case optMem(MemIdx).Tag
        Case Is < 0
            optMem(MemIdx).ToolTipText = GetItem(retStr, 1, vbNewLine)
        Case Is = 0
            optMem(MemIdx).ToolTipText = "No data found for that filter."
        Case Else
            optMem(MemIdx).ToolTipText = optMem(MemIdx).Tag & " Telexes "
            If TfrCount > 0 Then optMem(MemIdx).ToolTipText = optMem(MemIdx).ToolTipText & "(" & TfrCount & " Assigned) "
    End Select
    StoreFolderValues MemIdx
    ActFilterValues(MemIdx) = NewFilterValues(MemIdx)
    TelexPoolHead.MousePointer = 0
End Sub

Private Sub chkTlxTitleBar_Click(Index As Integer)
    If chkLoad.Value = 0 Then
        chkTlxTitleBar(1 - Index).Value = 1 - chkTlxTitleBar(Index).Value
        FillTelexTextField
        If chkTlxTitleBar(0).Value = 0 Then
            'chkOnTime.Enabled = False
        Else
            'chkOnTime.Enabled = True
        End If
    End If
End Sub

Private Sub FullText_Click()
    FillTelexTextField
    txtDummy.SetFocus
End Sub

Private Sub OnLine_Click()
    If OnLine.Value = 1 Then
        If PoolConfig.OnLineCfg(1).Value = 1 Then
            RcvTelex.Show
            RcvTelex.WindowState = vbNormal
        Else
            RcvTelex.Hide
        End If
        If PoolConfig.OnLineCfg(2).Value = 1 Then
            SndTelex.Show
            SndTelex.WindowState = vbNormal
        Else
            SndTelex.Hide
        End If
    Else
        RcvTelex.Hide
        SndTelex.Hide
    End If
    txtDummy.SetFocus
End Sub

Private Sub OnTop_Click()
    If OnTop.Value <> 1 Then SetFormOnTop Me, False
    SetAllFormsOnTop True
    txtDummy.SetFocus
End Sub

Private Sub optLocal_Click()
    txtDummy.SetFocus
    Dim Vpfr As String
    Dim Vpto As String
    Dim TimeVal
    optLocal.BackColor = LightestGreen
    optUtc.BackColor = vbButtonFace
    GetValidTimeFrame Vpfr, Vpto, False
    TimeVal = CedaFullDateToVb(Vpfr)
    TimeVal = DateAdd("n", UtcTimeDiff, TimeVal)
    txtDateFrom = Format(TimeVal, MySetUp.DefDateFormat)
    txtTimeFrom = Format(TimeVal, "hh:mm")
    TimeVal = CedaFullDateToVb(Vpto)
    TimeVal = DateAdd("n", UtcTimeDiff, TimeVal)
    txtDateTo = Format(TimeVal, MySetUp.DefDateFormat)
    txtTimeTo = Format(TimeVal, "hh:mm")
End Sub

Private Sub optMem_Click(Index As Integer)
    On Error Resume Next
    txtDummy.SetFocus
    If optMem(Index).Value = True Then
        optMem(optMemButtons.Tag).BackColor = vbButtonFace
        If tabTlxTypeFolder.Tag > 0 Then folderButtons(tabTlxTypeFolder.Tag).Visible = False
        StoreFolderValues CurMem
        CurMem = Index
        RestoreFolderValues CurMem
        optMem(Index).BackColor = vbGreen
        optMemButtons.Tag = Index
        CheckLoadButton
    End If
End Sub

Private Sub optUtc_Click()
    txtDummy.SetFocus
    Dim Vpfr As String
    Dim Vpto As String
    Dim TimeVal
    optUtc.BackColor = LightestGreen
    optLocal.BackColor = vbButtonFace
    GetValidTimeFrame Vpfr, Vpto, False
    TimeVal = CedaFullDateToVb(Vpfr)
    TimeVal = DateAdd("n", -UtcTimeDiff, TimeVal)
    txtDateFrom = Format(TimeVal, MySetUp.DefDateFormat)
    txtTimeFrom = Format(TimeVal, "hh:mm")
    TimeVal = CedaFullDateToVb(Vpto)
    TimeVal = DateAdd("n", -UtcTimeDiff, TimeVal)
    txtDateTo = Format(TimeVal, MySetUp.DefDateFormat)
    txtTimeTo = Format(TimeVal, "hh:mm")
End Sub

Private Sub PaxInfo_Click(Index As Integer)
    If PaxInfo(Index).Value = 1 Then
        If Trim(CurrentUrno.Text) <> "" Then
            CheckLoaTabViewer CurrentRecord.Text, PaxInfo(Index).Tag
        Else
            MyMsgBox.CallAskUser 0, 0, 0, "Show " & PaxInfo(Index).Caption, "No telex selected.", "hand", "", UserAnswer
        End If
        PaxInfo(Index).Value = 0
    End If
End Sub
Private Sub CheckLoaTabViewer(TlxTabRec As String, ShowWhat As String)
    Dim AftUrno As String
    AftUrno = Trim(GetFieldValue("FLNU", TlxTabRec, DataPool.TlxTabFields))
    If AftUrno <> "0" Then
        ShowLoaTabViewer AftUrno & "," & ShowWhat
    Else
        MyMsgBox.CallAskUser 0, 0, 0, "Show Flight Details", "No relation to a flight.", "hand", "", UserAnswer
    End If
End Sub
Private Sub ShowLoaTabViewer(CmdLineParameter As String)
    Static ViewerPath As String
    Static ViewerAppId 'As Variant
    Dim MsgTxt As String
    Dim RetVal As Integer
    On Error Resume Next
    If ViewerAppId > 0 Then
        Err.Description = ""
        AppActivate ViewerAppId, False
        If Err.Description = "" Then
            SendKeys "%{F4}", True
        End If
    End If
    If ViewerPath = "" Then ViewerPath = GetIniEntry(myIniFullName, "MAIN_WINDOW", "", "LOADTAB_VIEWER", "c:\ufis\rel\LoadTabViewer\LoadTabViewer.exe")
    Err.Description = ""
    ViewerAppId = Shell(ViewerPath & " " & CmdLineParameter, vbNormalFocus)
    If Err.Description <> "" Then
        MsgTxt = "Program not found:" & vbNewLine & ViewerPath
        RetVal = MyMsgBox.CallAskUser(0, 0, 0, "UFIS Components Control", MsgTxt, "warn1", "", UserAnswer)
    End If
End Sub

Private Sub ResetFilter_Click()
    If ResetFilter.Value = 1 Then InitDateFields
    ResetFilter.Value = 0
    txtDummy.SetFocus
End Sub

Private Sub tabTelexList_RowSelectionChanged(Index As Integer, ByVal LineNo As Long, ByVal Selected As Boolean)
    If (LineNo >= 0) And (LineNo < tabTelexList(Index).GetLineCount) Then
        If Selected Then
            chkTlxTitleBar(Index).Value = 0
            FillTelexTextField
        End If
    Else
        If Selected Then
            chkTlxTitleBar(Index).Value = 0
            FillTelexTextField
            CurrentRecord = ""
            CurrentUrno = ""
        End If
    End If
End Sub

Private Sub tabTelexList_SendLButtonClick(Index As Integer, ByVal Line As Long, ByVal Col As Long)
    'chkTlxTitleBar(Index).Value = 0
    'FillTelexTextField
End Sub
Private Sub FillTelexTextField()
    Dim Index As Integer
    Dim Line As Long
    Dim clTlxLin As String
    Dim CurTlxRec As String
    Dim tmpTtyp As String
    Dim tmpFldDat As String
    Dim tmpSere As String
    Dim tmpRema As String
    Dim recNbr 'As Variant
    txtTelexText = ""
    CurrentUrno = ""
    CurrentRecord = ""
    AtcCSGN.Text = ""
    UseCSGN.Text = ""
    Index = -1
    If chkTlxTitleBar(0).Value = 0 Then
        Index = 0
    ElseIf chkTlxTitleBar(1).Value = 0 Then
        Index = 1
    End If
    If Index >= 0 Then
        Line = tabTelexList(Index).GetCurrentSelected
        
        If Line >= 0 Then
            clTlxLin = tabTelexList(Index).GetLineValues(Line)
            recNbr = GetItem(clTlxLin, 9, ",")
            If recNbr <> "" Then
                TelexPoolHead.MousePointer = 11
                CurTlxRec = DataPool.TelexData(CurMem).GetLineValues(recNbr)
                'MsgBox CurTlxRec
                CurrentRecord = CurTlxRec
                tmpSere = GetFieldValue("SERE", CurTlxRec, DataPool.TlxTabFields.Text)
                If tmpSere = "C" Then
                    clTlxLin = BuildTelexText(CurTlxRec, "TEXT")
                Else
                    clTlxLin = GetFieldValue("TXT1", CurTlxRec, DataPool.TlxTabFields.Text) & _
                                GetFieldValue("TXT2", CurTlxRec, DataPool.TlxTabFields.Text)
                End If
                txtTelexText.Text = TextString(clTlxLin, FullText.Value)
                CurrentUrno.Text = GetFieldValue("URNO", CurTlxRec, DataPool.TlxTabFields.Text)
                tmpTtyp = Trim(GetFieldValue("TTYP", CurTlxRec, DataPool.TlxTabFields.Text))
                Select Case tmpTtyp
                    Case "ATC"
                        tmpFldDat = GetCsgnFromTelex(txtTelexText.Text)
                        UseCSGN.Text = tmpFldDat
                        AtcCSGN.Text = tmpFldDat
                    Case Else
                End Select
                TelexPoolHead.MousePointer = 0
                tmpRema = GetFieldValue("BEME", CurTlxRec, DataPool.TlxTabFields.Text)
                If InStr(tmpRema, "#") > 0 Then tmpRema = ""
                MyStatusBar.Panels(1).Text = tmpRema
                tmpRema = GetFieldValue("MSTX", CurTlxRec, DataPool.TlxTabFields.Text)
                MyStatusBar.Panels(2).Text = tmpRema
            End If
        End If
    End If
End Sub

Private Function BuildTelexText(cpTlxRec As String, FormatType As String) As String
    Dim Result As String
    Dim tmpTtyp As String
    Dim tmpStyp As String
    Dim tmpStat As String
    Dim tmpSere As String
    Dim tmpTlxCode As String
    Dim tmpFldLst As String
    Dim tmpDatLst As String
    Dim tmpTxt1 As String
    Dim tmpTxt2 As String
    Dim tmpOrg3 As String
    Dim tmpDes3 As String
    Dim tmpAdid As String
    Dim tmpFlda As String
    Dim tmpPlan As String
    Dim tmpAct1 As String
    Dim tmpAct2 As String
    Dim tmpData As String
    Dim FlightLine As String
    Dim TimeLine As String
    Dim DelayLine As String
    Dim TextPart As String
    Dim clFields As String
    Dim tmpTlxAdrLst As String
    Dim tmpPaye As String
    Dim LineSep As String
    Result = ""
    clFields = DataPool.TlxTabFields.Text
    tmpSere = GetFieldValue("SERE", cpTlxRec, clFields)
    tmpTtyp = GetFieldValue("TTYP", cpTlxRec, clFields)
    tmpStyp = GetFieldValue("STYP", cpTlxRec, clFields)
    tmpStat = GetFieldValue("STAT", cpTlxRec, clFields)
    If tmpSere = "C" Then
        If FormatType = "TEXT" Then
            LineSep = vbNewLine
        Else
            LineSep = "|"
        End If
        If tmpTtyp = "" Then tmpTtyp = "MVT"
        If tmpTtyp = "MVT" Then
            tmpTxt1 = GetFieldValue("TXT1", cpTlxRec, clFields)
            tmpFldLst = GetItem(tmpTxt1, 1, Chr(30))
            tmpFldLst = CleanString(tmpFldLst, FOR_CLIENT, False)
            tmpDatLst = GetItem(tmpTxt1, 2, Chr(30))
            tmpDatLst = CleanString(tmpDatLst, FOR_CLIENT, False)
            'MsgBox tmpFldLst & vbNewLine & tmpDatLst
            If Left(tmpDatLst, 1) = "," Then tmpDatLst = Mid(tmpDatLst, 2)
            tmpOrg3 = GetFieldValue("ORG3", tmpDatLst, tmpFldLst)
            tmpDes3 = GetFieldValue("DES3", tmpDatLst, tmpFldLst)
            tmpFlda = GetFieldValue("FLDA", tmpDatLst, tmpFldLst)
            If tmpFlda = "" Then tmpFlda = GetFieldValue("STOD", tmpDatLst, tmpFldLst)
            If tmpStyp = "" Then
                tmpAdid = GetFieldValue("ADID", tmpDatLst, tmpFldLst)
                If tmpAdid = "A" Then
                    If tmpStyp = "" Then tmpStyp = "AA"
                ElseIf tmpAdid = "D" Then
                    If tmpStyp = "" Then tmpStyp = "AD"
                Else
                End If
            End If
            FlightLine = ""
            FlightLine = FlightLine & GetFieldValue("FLNO", tmpDatLst, tmpFldLst) & "/"
            FlightLine = FlightLine & Mid(tmpFlda, 7, 2) & "."
            FlightLine = FlightLine & GetFieldValue("REGN", tmpDatLst, tmpFldLst) & "."
            Select Case tmpStyp
                Case "AA"
                    FlightLine = FlightLine & tmpDes3 & LineSep
                    tmpPlan = GetFieldValue("STOA", tmpDatLst, tmpFldLst)
                    tmpAct1 = GetFieldValue("LAND", tmpDatLst, tmpFldLst)
                    tmpAct2 = GetFieldValue("ONBL", tmpDatLst, tmpFldLst)
                    TimeLine = tmpStyp
                    TimeLine = TimeLine & Mid(tmpAct1, 9, 4) & "/"
                    TimeLine = TimeLine & Mid(tmpAct2, 9, 4) & LineSep
                    TextPart = FlightLine & TimeLine
                Case "AD"
                    FlightLine = FlightLine & tmpOrg3 & LineSep
                    tmpPlan = GetFieldValue("STOD", tmpDatLst, tmpFldLst)
                    tmpAct1 = GetFieldValue("OFBL", tmpDatLst, tmpFldLst)
                    tmpAct2 = GetFieldValue("AIRB", tmpDatLst, tmpFldLst)
                    TimeLine = tmpStyp
                    TimeLine = TimeLine & Mid(tmpAct1, 9, 4) & "/"
                    TimeLine = TimeLine & Mid(tmpAct2, 9, 4)
                    tmpData = GetFieldValue("ETAI", tmpDatLst, tmpFldLst)
                    If tmpData <> "" Then
                        TimeLine = TimeLine & " EA" & Mid(tmpData, 9, 4)
                        tmpData = Left(GetFieldValue("VIAL", tmpDatLst, tmpFldLst), 3)
                        If tmpData = "" Then tmpData = tmpDes3
                        TimeLine = TimeLine & " " & tmpData
                    End If
                    TextPart = FlightLine & TimeLine
                    DelayLine = BuildDelayLine(tmpFldLst, tmpDatLst)
                    If DelayLine <> "" Then TextPart = TextPart & LineSep & DelayLine
                Case "ED"
                    FlightLine = FlightLine & tmpOrg3 & LineSep
                    tmpAct1 = GetFieldValue("ETDI", tmpDatLst, tmpFldLst)
                    TimeLine = tmpStyp & Mid(tmpAct1, 7, 6)
                    TextPart = FlightLine & TimeLine
                    DelayLine = BuildDelayLine(tmpFldLst, tmpDatLst)
                    If DelayLine <> "" Then TextPart = TextPart & LineSep & DelayLine
                Case "NI"
                    FlightLine = FlightLine & tmpOrg3 & LineSep
                    tmpAct1 = GetFieldValue("NXTI", tmpDatLst, tmpFldLst)
                    TimeLine = tmpStyp
                    TextPart = FlightLine & TimeLine
                    DelayLine = BuildDelayLine(tmpFldLst, tmpDatLst)
                    If DelayLine <> "" Then TextPart = TextPart & LineSep & DelayLine
                Case Else
            End Select
            If FormatType = "LINE" Then
                'Browser Sent Telexes
                Result = TextPart
            Else
                'Telex Text Field
                Result = "=PRIORITY" & vbNewLine & "QU" & vbNewLine
                tmpTxt2 = GetFieldValue("TXT2", cpTlxRec, clFields)
                BuildTlxAdressList tmpTxt2, tmpTlxAdrLst, tmpPaye
                Result = Result & tmpTlxAdrLst
                If tmpPaye <> "" Then Result = Result & tmpPaye
                Result = Result & "=TEXT" & vbNewLine & tmpTtyp & vbNewLine
                Result = Result & TextPart & vbNewLine
            End If
        End If
    End If
    BuildTelexText = Result
End Function
Private Function BuildDelayLine(UseFldLst As String, UseDatLst As String) As String
    Dim tmpDtd1 As String
    Dim tmpDtd2 As String
    Dim tmpDcd1 As String
    Dim tmpDcd2 As String
    Dim DelayLine As String
    DelayLine = ""
    tmpDtd1 = GetFieldValue("DTD1", UseDatLst, UseFldLst)
    tmpDtd2 = GetFieldValue("DTD2", UseDatLst, UseFldLst)
    tmpDcd1 = GetFieldValue("DCD1", UseDatLst, UseFldLst)
    tmpDcd2 = GetFieldValue("DCD2", UseDatLst, UseFldLst)
    If Val(tmpDtd1) <= 0 Then tmpDtd1 = ""
    If Val(tmpDtd2) <= 0 Then tmpDtd2 = ""
    If Val(tmpDtd1) > 0 And tmpDcd1 = "" Then tmpDcd1 = ".."
    If Val(tmpDtd2) > 0 And tmpDcd2 = "" Then tmpDcd2 = ".."
    If (tmpDcd1 & tmpDcd2) <> "" Then
        DelayLine = "DL" & tmpDcd1
        If tmpDcd2 <> "" Then DelayLine = DelayLine & "/" & tmpDcd2
        If tmpDtd1 <> "" Then DelayLine = DelayLine & "/" & tmpDtd1
        If tmpDtd2 <> "" Then DelayLine = DelayLine & "/" & tmpDtd2
    End If
    BuildDelayLine = DelayLine
End Function

Private Sub BuildTlxAdressList(FromTxt2 As String, OutAdrLst As String, OutPaye As String)
    Dim tmpAdrLst As String
    Dim ItmCnt As Integer
    Dim CurItm As Integer
    Dim ItmDat As String
    Dim ilLen As Integer
    OutAdrLst = ""
    OutPaye = ""
    tmpAdrLst = CleanString(FromTxt2, FOR_CLIENT, False)
    'MsgBox tmpAdrLst
    ItmCnt = ItemCount(tmpAdrLst, ",")
    'MsgBox ItmCnt
    For CurItm = 1 To ItmCnt
        ItmDat = GetItem(tmpAdrLst, CurItm, ",")
        ilLen = Len(ItmDat)
        If ilLen > 3 Then
            OutAdrLst = OutAdrLst & "STX," & ItmDat & vbNewLine
        Else
            If ilLen > 1 Then OutPaye = OutPaye & ItmDat & ","
        End If
    Next
    If OutAdrLst <> "" Then
        OutAdrLst = "=DESTINATION TYPE B" & vbNewLine & OutAdrLst
    End If
    If OutPaye <> "" Then
        OutPaye = "=DBLSIG" & vbNewLine & GetItem(OutPaye, 1, ",") & vbNewLine
    End If
    'MsgBox OutAdrLst
    'MsgBox OutPaye
End Sub
Private Function GetCsgnFromTelex(TlxTxt As String)
    Dim Result As String
    Result = Trim(GetItem(TlxTxt, 2, "CSGN:"))
    Result = Trim(GetItem(Result, 1, vbNewLine))
    GetCsgnFromTelex = Result
End Function
Private Function TextString(cpText As String, ipMeth As Integer) As String
Dim clTlxLin As String
Dim tmpData As String
Dim ilOffs As Integer
Dim ilTxtBgn As Integer
Dim chkTxtCod As String
    clTlxLin = CleanString(cpText, FOR_CLIENT, True)
    If FullText.Value = 0 Then
        chkTxtCod = vbLf & "."
        ilTxtBgn = 0
        ilOffs = 1
        While ilOffs > 0
            ilOffs = InStr(ilOffs, clTlxLin, chkTxtCod)
            If ilOffs > 0 Then
                ilTxtBgn = ilOffs + 1
                ilOffs = ilOffs + 1
                ilOffs = 0 'we need it only once
            End If
        Wend
        If ilTxtBgn > 0 Then
            clTlxLin = Mid(clTlxLin, ilTxtBgn)
        End If
    End If
    TextString = clTlxLin
End Function

Private Sub tabTelexList_SendLButtonDblClick(Index As Integer, ByVal LineNo As Long, ByVal ColNo As Long)
    If LineNo < 0 Then
        tabTelexList(Index).Sort Str(ColNo), True, True
        tabTelexList(Index).RedrawTab
    End If
End Sub

Private Sub tabTelexList_SendMouseMove(Index As Integer, ByVal Line As Long, ByVal Col As Long, ByVal Flags As String)
Static lastLine(0 To 1) As Long
Dim RecIdx 'as variant
Dim FldIdx As Integer
Dim tmpData As String
    'If (Line + 1) <> lastLine(Index) Then
        RecIdx = tabTelexList(Index).GetColumnValue(Line, 8)
        If RecIdx <> "" Then
            tmpData = ""
            Select Case Col
                Case 0
                    FldIdx = GetItemNo(DataPool.TlxTabFields, "STAT") - 1
                    If FldIdx >= 0 Then
                        tmpData = DataPool.TelexData(CurMem).GetColumnValue(RecIdx, FldIdx)
                        tmpData = TlxRcvStat.ExplainStatus("STAT", tmpData, False)
                    End If
                Case 1
                    FldIdx = GetItemNo(DataPool.TlxTabFields, "WSTA") - 1
                    If FldIdx >= 0 Then
                        tmpData = DataPool.TelexData(CurMem).GetColumnValue(RecIdx, FldIdx)
                        tmpData = TlxRcvStat.ExplainStatus("WSTA", tmpData, False)
                    End If
                Case 4
                    FldIdx = GetItemNo(DataPool.TlxTabFields, "CDAT") - 1
                    If FldIdx >= 0 Then
                        tmpData = DataPool.TelexData(CurMem).GetColumnValue(RecIdx, FldIdx)
                        tmpData = Format(CedaDateToVb(tmpData), MySetUp.DefDateFormat)
                    End If
                Case Else
                    'ShowTip = False
            End Select
            tabTelexList(Index).ToolTipText = tmpData   'Set or Reset the ToolTipText
        End If
        lastLine(Index) = Line + 1  'to force line 0
    'End If
End Sub

Private Sub tabTelexList_SendRButtonClick(Index As Integer, ByVal Line As Long, ByVal Col As Long)
    'MsgBox "R-Click"
End Sub

Private Sub tabTlxTypeFolder_Click()
Dim ListOfTfrLines As String
Dim chkTfrLin As String
    Dim ilCurTab As Integer
    Dim ilOldTab As Integer
    Dim ilMaxRec As Integer
    Dim clIdxLst As String
    Dim ilIdxNbr As Integer
    Dim clTlxRec As String
    Dim clTlxLin As String
    Dim tmpStat As String
    Dim tmpWsta As String
    Dim tmpTtyp As String
    Dim tmpTlxCode As String
    Dim tmpSere As String
    Dim tmpData As String
    Dim myTextColor As Long
    Dim myBackColor As Long
    Dim Idx 'as variant
            
    ilCurTab = tabTlxTypeFolder.SelectedItem.Index
    If chkLoad.Value = 0 Then
        ilOldTab = tabTlxTypeFolder.Tag
        'If ilCurTab <> ilOldTab Then
            TelexPoolHead.MousePointer = 11
            CurrentUrno.Text = ""
            CurrentRecord.Text = ""
            If ilOldTab > 0 Then
                tabTlxTypeFolder.Tabs(ilOldTab).HighLighted = False
                folderButtons(ilOldTab).Visible = False
            End If
            tabTlxTypeFolder.Tabs(ilCurTab).HighLighted = True
            folderButtons(ilCurTab).Top = folderButtons(0).Top
            folderButtons(ilCurTab).Left = folderButtons(0).Left
            folderButtons(ilCurTab).Visible = True
            folderButtons(0).Tag = ilCurTab
            tabTelexList(0).ResetContent
            tabTelexList(0).RedrawTab
            tabTelexList(1).ResetContent
            tabTelexList(1).RedrawTab
            txtTelexText.Text = ""
            AtcCSGN.Text = ""
            UseCSGN.Text = ""
            TelexPoolHead.Refresh
            'TAB Control has no Function GetLineColors yet
            'So I must do a trick: first get a list of all 'TFR' coded lines
            ListOfTfrLines = "," & DataPool.TelexData(CurMem).GetLinesByBackColor(DarkestGreen) & ","
            'MsgBox ListOfTfrLines
            ilMaxRec = DataPool.TelexData(CurMem).GetLineCount
            If ilCurTab <> tabTlxTypeFolder.Tabs.count Then ilMaxRec = -1
            clIdxLst = DataPool.TtypIdx(CurMem).GetColumnValue(ilCurTab, 1)
            ilIdxNbr = 0
            Idx = -1
            While Idx <> ""
                If ilMaxRec < 0 Then
                    ilIdxNbr = ilIdxNbr + 1
                    Idx = GetItem(clIdxLst, ilIdxNbr, ";")
                Else
                    'Folder ALL is activated
                    If ilMaxRec > ilIdxNbr Then
                        Idx = ilIdxNbr
                        ilIdxNbr = ilIdxNbr + 1
                    Else
                        Idx = ""
                    End If
                End If
                If Idx <> "" Then
                    clTlxRec = DataPool.TelexData(CurMem).GetLineValues(Idx)
                    'MsgBox clTlxRec
                    tmpSere = GetFieldValue("SERE", clTlxRec, DataPool.TlxTabFields.Text)
                    tmpStat = GetFieldValue("STAT", clTlxRec, DataPool.TlxTabFields.Text)
                    tmpWsta = GetFieldValue("WSTA", clTlxRec, DataPool.TlxTabFields.Text)
                    If tmpWsta <> "" Then
                        ColorPool.GetStatusColor tmpWsta, "", myTextColor, myBackColor
                    Else
                        ColorPool.GetStatusColor tmpStat, "", myTextColor, myBackColor
                    End If
                    chkTfrLin = "," & Trim(Idx) & ","
                    If InStr(ListOfTfrLines, chkTfrLin) > 0 Then myBackColor = LightestYellow
                    If tmpSere = "R" Then
                        clTlxLin = BuildTlxBrowser("RS", DataPool.TlxTabFields.Text, clTlxRec) & Idx & ","
                        tabTelexList(0).InsertTextLineAt 0, clTlxLin, True
                        tabTelexList(0).SetLineColor 0, myTextColor, myBackColor
                    ElseIf tmpSere = "S" Then
                        clTlxLin = BuildTlxBrowser("RS", DataPool.TlxTabFields.Text, clTlxRec) & Idx & ","
                        tabTelexList(1).InsertTextLineAt 0, clTlxLin, True
                        tabTelexList(1).SetLineColor 0, myTextColor, myBackColor
                    ElseIf tmpSere = "C" Then
                        clTlxLin = BuildTlxBrowser("RS", DataPool.TlxTabFields.Text, clTlxRec) & Idx & ","
                        tabTelexList(1).InsertTextLineAt 0, clTlxLin, True
                        tabTelexList(1).SetLineColor 0, myTextColor, myBackColor
                    End If
                End If
            Wend
            TelexPoolHead.MousePointer = 0
            tabTlxTypeFolder.Tag = ilCurTab
        'End If
    Else
        tabTlxTypeFolder.Tabs(ilCurTab).HighLighted = Not (tabTlxTypeFolder.Tabs(ilCurTab).HighLighted)
    End If
End Sub
Public Function BuildTlxBrowser(FormatType As String, cpFields As String, cpTlxRec As String) As String
Dim clTlxLin As String
Dim tmpTtyp As String
Dim tmpStyp As String
Dim tmpStat As String
Dim tmpWsta As String
Dim tmpSere As String
Dim tmpTlxCode As String
Dim tmpFldLst As String
Dim tmpDatLst As String
Dim tmpTxt1 As String
Dim tmpOrg3 As String
Dim tmpDes3 As String
Dim tmpAdid As String
Dim tmpPlan As String
Dim tmpAct1 As String
Dim tmpAct2 As String
Dim tmpData As String
    tmpSere = GetFieldValue("SERE", cpTlxRec, cpFields)
    tmpTtyp = GetFieldValue("TTYP", cpTlxRec, cpFields)
    tmpStyp = GetFieldValue("STYP", cpTlxRec, cpFields)
    tmpStat = GetFieldValue("STAT", cpTlxRec, cpFields)
    tmpWsta = GetFieldValue("WSTA", cpTlxRec, cpFields)
    If tmpSere = "C" And FormatType = "C" Then
        If tmpStat = "C" Then
            tmpTxt1 = GetFieldValue("TXT1", cpTlxRec, cpFields)
            tmpFldLst = GetItem(tmpTxt1, 1, Chr(30))
            tmpFldLst = CleanString(tmpFldLst, FOR_CLIENT, False)
            tmpDatLst = GetItem(tmpTxt1, 2, Chr(30))
            tmpDatLst = CleanString(tmpDatLst, FOR_CLIENT, False)
            If Left(tmpDatLst, 1) = "," Then tmpDatLst = Mid(tmpDatLst, 2)
            tmpOrg3 = GetFieldValue("ORG3", tmpDatLst, tmpFldLst)
            tmpDes3 = GetFieldValue("DES3", tmpDatLst, tmpFldLst)
            tmpAdid = GetFieldValue("ADID", tmpDatLst, tmpFldLst)
            If tmpAdid = "A" Then
                If tmpStyp = "" Then tmpStyp = "AA"
            ElseIf tmpAdid = "D" Then
                If tmpStyp = "" Then tmpStyp = "AD"
            Else
            End If
            Select Case tmpStyp
                Case "AA"
                    tmpPlan = GetFieldValue("STOA", tmpDatLst, tmpFldLst)
                    tmpAct1 = GetFieldValue("LAND", tmpDatLst, tmpFldLst)
                    tmpAct2 = GetFieldValue("ONBL", tmpDatLst, tmpFldLst)
                Case "AD"
                    tmpPlan = GetFieldValue("STOD", tmpDatLst, tmpFldLst)
                    tmpAct1 = GetFieldValue("OFBL", tmpDatLst, tmpFldLst)
                    tmpAct2 = GetFieldValue("AIRB", tmpDatLst, tmpFldLst)
                Case "ED"
                    tmpPlan = GetFieldValue("STOD", tmpDatLst, tmpFldLst)
                    tmpAct1 = GetFieldValue("ETDI", tmpDatLst, tmpFldLst)
                Case "NI"
                    tmpPlan = GetFieldValue("STOD", tmpDatLst, tmpFldLst)
                    tmpAct1 = GetFieldValue("NXTI", tmpDatLst, tmpFldLst)
                Case Else
            End Select
            clTlxLin = tmpStat & "," & tmpWsta & ","
            clTlxLin = clTlxLin & tmpTtyp & "," & tmpStyp & ","
            clTlxLin = clTlxLin & GetFieldValue("FLNO", tmpDatLst, tmpFldLst) & ","
            clTlxLin = clTlxLin & GetFieldValue("REGN", tmpDatLst, tmpFldLst) & ","
            clTlxLin = clTlxLin & Mid(tmpPlan, 9, 4) & ","
            clTlxLin = clTlxLin & Mid(tmpAct1, 9, 4) & ","
            clTlxLin = clTlxLin & Mid(tmpAct2, 9, 4) & ","
            clTlxLin = clTlxLin & GetFieldValue("URNO", cpTlxRec, cpFields) & ","
        End If
    Else
        clTlxLin = tmpStat & "," & tmpWsta & "," & tmpTtyp & "," & tmpStyp & ","
        tmpData = GetFieldValue("CDAT", cpTlxRec, cpFields)
        clTlxLin = clTlxLin & tmpData & ","
        clTlxLin = clTlxLin & GetFieldValue("TXNO", cpTlxRec, cpFields) & ","
        tmpData = GetFieldValue("TXT1", cpTlxRec, cpFields) & GetFieldValue("TXT2", cpTlxRec, cpFields)
        If tmpSere <> "C" Then
            tmpData = GetTelexExtract(tmpSere, tmpTtyp, tmpData)
        Else
            tmpData = BuildTelexText(cpTlxRec, "LINE")
        End If
        clTlxLin = clTlxLin & tmpData & ","
        clTlxLin = clTlxLin & GetFieldValue("URNO", cpTlxRec, cpFields) & ","
    End If
    clTlxLin = Replace(clTlxLin, Chr(28), "|", 1, -1, vbBinaryCompare)
    BuildTlxBrowser = clTlxLin
End Function
Private Function GetTelexExtract(TlxSere As String, TlxType As String, TlxText As String) As String
    Dim tmpData As String
    Dim tmpTlxCode As String
    Dim NxtLine As String
    Dim DotPos As Integer
    Dim CodPos As Integer
    Dim NxtPos As Integer
    tmpData = TlxText
    tmpTlxCode = "."
    If TlxSere = "R" Then
        If TlxType <> "ATC" Then tmpTlxCode = Chr(28) & TlxType Else tmpTlxCode = "CSGN:"
        DotPos = InStr(TlxText, ".")
        If DotPos > 0 Then
            NxtLine = Mid(TlxText, DotPos)
            NxtLine = GetItem(NxtLine, 1, " ")
        End If
        While (DotPos > 0) And (Len(NxtLine) < 8)
            DotPos = DotPos + 1
            DotPos = InStr(DotPos, TlxText, ".")
            If DotPos > 0 Then
                NxtLine = Mid(TlxText, DotPos)
                NxtLine = GetItem(NxtLine, 1, " ")
            End If
        Wend
        If tmpTlxCode <> Chr(28) Then CodPos = InStr(TlxText, tmpTlxCode) Else CodPos = 0
        If CodPos > DotPos Then
            CodPos = CodPos + Len(tmpTlxCode)
            tmpData = Mid(TlxText, CodPos)
        ElseIf (CodPos > 0) And (DotPos > CodPos) Then
            While (CodPos > 0) And (DotPos > CodPos)
                CodPos = CodPos + Len(tmpTlxCode)
                tmpData = Mid(TlxText, CodPos)
                CodPos = InStr(CodPos, TlxText, tmpTlxCode)
            Wend
            If CodPos > DotPos Then
                CodPos = CodPos + Len(tmpTlxCode)
                tmpData = Mid(TlxText, CodPos)
            End If
        ElseIf (CodPos = 0) And (DotPos > 0) Then
            tmpData = Mid(tmpData, DotPos)
            NxtPos = InStr(tmpData, Chr(28))
            If NxtPos > 0 Then tmpData = Mid(tmpData, NxtPos + 1)
            NxtLine = Replace(GetItem(tmpData, 1, Chr(28)), " ", "", 1, -1, vbBinaryCompare)
            If (Len(TlxType) > 0) And (Left(NxtLine, Len(TlxType)) = TlxType) Then
                NxtPos = InStr(tmpData, Chr(28))
                If NxtPos > 0 Then tmpData = Mid(tmpData, NxtPos + 1)
            Else
                NxtLine = GetItem(tmpData, 1, " ")
                If (Len(NxtLine) = 2) And (Left(NxtLine, 1) = "Q") Then
                    DotPos = InStr(tmpData, ".")
                    If DotPos > 0 Then tmpData = Mid(tmpData, DotPos)
                    NxtPos = InStr(tmpData, Chr(28))
                    If NxtPos > 0 Then tmpData = Mid(tmpData, NxtPos + 1)
                End If
            End If
        End If
    ElseIf TlxSere = "S" Then
        tmpTlxCode = "=TEXT"
        tmpData = GetItem(tmpData, 2, tmpTlxCode)
    End If
    tmpData = LTrim(tmpData)
    While Left(tmpData, 1) = Chr(28)
        tmpData = Mid(tmpData, 2)
        tmpData = LTrim(tmpData)
    Wend
    If tmpData = "" Then tmpData = TlxText
    GetTelexExtract = tmpData
End Function
Private Sub ResetTlxTypeFolder()
    Dim ilOldTab As Integer
    ilOldTab = Val(tabTlxTypeFolder.Tag)
    If ilOldTab > 0 Then
        tabTlxTypeFolder.Tabs(ilOldTab).HighLighted = False
        tabTlxTypeFolder.Tag = 0
    End If
    chkTlxTitleBar(0).Value = 1
    chkTlxTitleBar(1).Value = 1
End Sub

Private Sub txtAddr_Change()
    CheckLoadButton
End Sub

Private Sub txtAddr_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
End Sub


Private Sub txtDateFrom_Change()
    If CheckDateField(txtDateFrom, MySetUp.DefDateFormat, False) <> True Then DoNothing
    CheckLoadButton
End Sub

Private Sub txtDateFrom_Validate(Cancel As Boolean)
    If CheckDateField(txtDateFrom, MySetUp.DefDateFormat, True) <> True Then DoNothing
End Sub

Private Sub txtDateTo_Change()
    If CheckDateField(txtDateTo, MySetUp.DefDateFormat, False) <> True Then DoNothing
    CheckLoadButton
End Sub

Private Sub txtDateTo_Validate(Cancel As Boolean)
    If CheckDateField(txtDateTo, MySetUp.DefDateFormat, True) <> True Then DoNothing
End Sub

Private Sub txtFlca_Change()
    CheckLoadButton
End Sub

Private Sub txtFlca_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
End Sub

Private Sub txtFlns_Change()
    CheckLoadButton
End Sub

Private Sub txtFlns_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
End Sub

Private Sub txtFltn_Change()
    CheckLoadButton
End Sub

Private Sub txtRegn_Change()
    CheckLoadButton
End Sub

Private Sub txtRegn_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
End Sub

Private Sub txtText_Change()
    CheckLoadButton
End Sub

Private Sub txtText_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
End Sub

Private Sub txtTimeFrom_Change()
    CheckLoadButton
End Sub

Private Sub txtTimeTo_Change()
    CheckLoadButton
End Sub

Public Sub DistributeBroadcast(BcNum As String, DestName As String, RecvName As String, cpTable As String, cpCmd As String, cpSelKey As String, cpFields As String, cpData As String)
Dim tmpUrno As String
Dim tmpStat As String
Dim UrnoIdx As Integer
Dim LineIdx 'As Variant
Dim FldIdx As Integer
Dim tmpList As String
Dim CurRecDat As String
Dim tmpFldNam As String
Dim tmpFldVal As String
Dim ItmNbr As Integer
Dim myTextColor As Long
Dim myBackColor As Long
Dim tmpData As String
Dim tmpTxtData As String
    'only called by URT or UBT
    tmpUrno = GetFieldValue("URNO", cpData, cpFields)
    If tmpUrno = "" Then tmpUrno = GetUrnoFromSqlKey(cpSelKey)
    If tmpUrno <> "" Then
        UrnoIdx = GetItemNo(DataPool.TlxTabFields, "URNO") - 1
        tmpList = DataPool.TelexData(CurMem).GetLinesByColumnValue(UrnoIdx, tmpUrno, 1)
        If tmpList <> "" Then
            LineIdx = GetItem(tmpList, 1, ",")
            ItmNbr = 0
            Do
                ItmNbr = ItmNbr + 1
                tmpFldNam = GetItem(cpFields, ItmNbr, ",")
                tmpFldVal = GetItem(cpData, ItmNbr, ",")
                If tmpFldNam <> "" Then
                    FldIdx = GetItemNo(DataPool.TlxTabFields, tmpFldNam) - 1
                    If FldIdx >= 0 Then
                        DataPool.TelexData(CurMem).SetColumnValue LineIdx, FldIdx, tmpFldVal
                    End If
                End If
            Loop While tmpFldNam <> ""
            RefreshListData 0, tmpUrno
            RefreshListData 1, tmpUrno
        End If
        
        tmpList = RcvTelex.tabTelexList.GetLinesByColumnValue(7, tmpUrno, 1)
        If tmpList <> "" Then
            LineIdx = GetItem(tmpList, 1, ",")
            tmpData = GetFieldValue("TTYP", cpData, cpFields)
            If tmpData <> "" Then
                RcvTelex.tabTelexList.SetColumnValue LineIdx, 2, tmpData
                tmpTxtData = RcvTelex.tabTelexList.GetColumnValue(LineIdx, 6)
                If Left(tmpTxtData, Len(tmpData)) = tmpData Then
                    tmpTxtData = Trim(Mid(tmpTxtData, Len(tmpData) + 1))
                    If Left(tmpTxtData, 1) = "|" Then tmpTxtData = Trim(Mid(tmpTxtData, 2))
                    RcvTelex.tabTelexList.SetColumnValue LineIdx, 6, tmpTxtData
                End If
            End If
            tmpData = GetFieldValue("STYP", cpData, cpFields)
            If tmpData <> "" Then RcvTelex.tabTelexList.SetColumnValue LineIdx, 3, tmpData
            tmpData = GetFieldValue("CDAT", cpData, cpFields)
            If tmpData <> "" Then RcvTelex.tabTelexList.SetColumnValue LineIdx, 4, tmpData
            tmpData = GetFieldValue("TXNO", cpData, cpFields)
            If tmpData <> "" Then RcvTelex.tabTelexList.SetColumnValue LineIdx, 5, tmpData
        End If
        
        tmpStat = GetFieldValue("STAT", cpData, cpFields)
        If tmpStat <> "" Then
            ColorPool.GetStatusColor tmpStat, "", myTextColor, myBackColor
            tmpList = RcvTelex.tabTelexList.GetLinesByColumnValue(7, tmpUrno, 1)
            If tmpList <> "" Then
                LineIdx = GetItem(tmpList, 1, ",")
                RcvTelex.tabTelexList.SetColumnValue LineIdx, 0, tmpStat
                RcvTelex.tabTelexList.SetLineColor LineIdx, myTextColor, myBackColor
                RcvTelex.tabTelexList.RedrawTab
            End If
            tmpList = SndTelex.tabTelexList.GetLinesByColumnValue(7, tmpUrno, 1)
            If tmpList <> "" Then
                LineIdx = GetItem(tmpList, 1, ",")
                SndTelex.tabTelexList.SetColumnValue LineIdx, 0, tmpStat
                SndTelex.tabTelexList.SetLineColor LineIdx, myTextColor, myBackColor
                SndTelex.tabTelexList.RedrawTab
            End If
        End If
        tmpStat = GetFieldValue("WSTA", cpData, cpFields)
        If tmpStat <> "" Then
            ColorPool.GetStatusColor tmpStat, "", myTextColor, myBackColor
            tmpList = RcvTelex.tabTelexList.GetLinesByColumnValue(7, tmpUrno, 1)
            If tmpList <> "" Then
                LineIdx = GetItem(tmpList, 1, ",")
                RcvTelex.tabTelexList.SetColumnValue LineIdx, 1, tmpStat
                RcvTelex.tabTelexList.SetLineColor LineIdx, myTextColor, myBackColor
                RcvTelex.tabTelexList.RedrawTab
            End If
            tmpList = SndTelex.tabTelexList.GetLinesByColumnValue(7, tmpUrno, 1)
            If tmpList <> "" Then
                LineIdx = GetItem(tmpList, 1, ",")
                SndTelex.tabTelexList.SetColumnValue LineIdx, 1, tmpStat
                SndTelex.tabTelexList.SetLineColor LineIdx, myTextColor, myBackColor
                SndTelex.tabTelexList.RedrawTab
            End If
        End If
    End If
End Sub

Private Sub RefreshListData(Index As Integer, RecUrno As String)
Dim tmpList As String
Dim LineIdx 'as Variant
Dim DataIdx 'as long
Dim RecData As String
Dim RecLine As String
Dim tmpStat As String
Dim myTextColor As Long
Dim myBackColor As Long
    tmpList = tabTelexList(Index).GetLinesByColumnValue(7, RecUrno, 1)
    If tmpList <> "" Then
        LineIdx = GetItem(tmpList, 1, ",")
        DataIdx = tabTelexList(Index).GetColumnValue(LineIdx, 8)
        If DataIdx <> "" Then
            RecData = DataPool.TelexData(CurMem).GetLineValues(DataIdx)
            RecLine = BuildTlxBrowser("RS", DataPool.TlxTabFields, RecData) & DataIdx
            tabTelexList(Index).UpdateTextLine LineIdx, RecLine, True
            tmpStat = GetFieldValue("STAT", RecData, DataPool.TlxTabFields.Text)
            ColorPool.GetStatusColor tmpStat, "", myTextColor, myBackColor
            tabTelexList(Index).SetLineColor LineIdx, myTextColor, myBackColor
            tabTelexList(Index).RedrawTab
        End If
    End If
End Sub

Public Sub EvaluateBc(BcNum As String, DestName As String, RecvName As String, cpTable As String, cpCmd As String, cpSelKey As String, cpFields As String, cpData As String)
Dim clTlxLin As String
Dim tmpSere As String
Dim tmpStat As String
Dim tmpWsta As String
Dim tmpType As String
Dim FilterLookUp As String
Dim myTextColor As Long
Dim myBackColor As Long
Dim MaxLinCnt As Long
    If cpTable = "TLXTAB" Then
        tmpSere = GetFieldValue("SERE", cpData, cpFields)
        If InStr("IBT,IRT", cpCmd) > 0 Then
            tmpType = GetFieldValue("TTYP", cpData, cpFields)
            tmpStat = GetFieldValue("STAT", cpData, cpFields)
            tmpWsta = GetFieldValue("WSTA", cpData, cpFields)
            If tmpWsta <> "" Then
                ColorPool.GetStatusColor tmpWsta, "", myTextColor, myBackColor
            Else
                ColorPool.GetStatusColor tmpStat, "", myTextColor, myBackColor
            End If
            If tmpSere = "R" Then
                FilterLookUp = MySetUp.CheckOnlineFilter("LOOKUP_RCV", tmpType)
                If FilterLookUp = "OK" Then
                    clTlxLin = TelexPoolHead.BuildTlxBrowser("RS", cpFields, cpData) & ",0"
                    RcvTelex.tabTelexList.InsertTextLineAt 0, clTlxLin, True
                    RcvTelex.tabTelexList.SetLineColor 0, myTextColor, myBackColor
                    MaxLinCnt = RcvTelex.tabTelexList.GetLineCount
                    RcvTelex.Caption = MaxLinCnt & " Received Telexes (Online)"
                    If MaxLinCnt > 500 Then
                        RcvTelex.tabTelexList.DeleteLine MaxLinCnt - 1
                    End If
                    RcvTelex.tabTelexList.RedrawTab
                End If
            ElseIf tmpSere = "C" Then
                clTlxLin = TelexPoolHead.BuildTlxBrowser("C", cpFields, cpData) & ",0"
                SndTelex.tabTelexList.InsertTextLineAt 0, clTlxLin, True
                SndTelex.tabTelexList.SetLineColor 0, myTextColor, myBackColor
                MaxLinCnt = SndTelex.tabTelexList.GetLineCount
                SndTelex.Caption = MaxLinCnt & " Telexes to send (Online)"
                If MaxLinCnt > 500 Then
                    SndTelex.tabTelexList.DeleteLine MaxLinCnt - 1
                End If
            End If
        ElseIf InStr("UBT,URT", cpCmd) > 0 Then
            DistributeBroadcast BcNum, DestName, RecvName, cpTable, cpCmd, cpSelKey, cpFields, cpData
        End If
    ElseIf cpTable = "AFTTAB" Then
        'pruefen auf Create MVT/.AAD (BRS)
        'Zeitrahmen pruefen
        'If cpCmd = "IFR" Then
        'End If
        'If cpCmd = "UFR" Then
        'End If
        'If cpCmd = "DFR" Then
        'End If
    ElseIf cpTable = "TFRTAB" Then
    End If
End Sub

Public Sub RefreshOnlineWindows()
Dim SqlKey As String
Dim RetVal As Integer
Dim tmpVal As String
Dim tmpFields As String
Dim tmpTimeFilter As String
Dim tmpRcvFilter As String
Dim tmpSndFilter As String
Dim count As Long
Dim TlxRec As String
Dim UseServerTime
Dim BeginTime
Dim EndTime
Dim TimeFrom As String
Dim TimeTo As String
Dim i As Long
    count = 0
    RcvTelex.tabTelexList.ResetContent
    SndTelex.tabTelexList.ResetContent
    RcvTelex.Caption = count & " Received Telexes (Online)"
    SndTelex.Caption = count & " Telexes to send (Online)"
    RcvTelex.tabTelexList.RedrawTab
    SndTelex.tabTelexList.RedrawTab
    
    UseServerTime = CedaFullDateToVb(MySetUp.ServerTime.Tag)
    If MySetUp.ServerTimeType <> "UTC" Then UseServerTime = DateAdd("n", -UtcTimeDiff, UseServerTime)
    BeginTime = DateAdd("h", -Abs(Val(MySetUp.OnlineFrom)), UseServerTime)
    TimeFrom = Format(BeginTime, "yyyymmddhhmmss")
    EndTime = DateAdd("h", Abs(Val(MySetUp.OnlineTo)), UseServerTime)
    TimeTo = Format(EndTime, "yyyymmddhhmmss")
    tmpRcvFilter = MySetUp.CheckOnlineFilter("RCV_FILTER", "")
    tmpSndFilter = MySetUp.CheckOnlineFilter("SND_FILTER", "")
    tmpTimeFilter = "(TIME BETWEEN '" & TimeFrom & "' AND '" & TimeTo & "')"
    If tmpRcvFilter <> "" Then tmpRcvFilter = "(SERE='R' AND " & tmpRcvFilter & ")"
    If tmpSndFilter <> "" Then tmpSndFilter = "(SERE='S' AND " & tmpSndFilter & ")"
    If (tmpRcvFilter <> "") And (tmpSndFilter <> "") Then
        SqlKey = "WHERE " & tmpTimeFilter & " AND (" & tmpRcvFilter & " OR " & tmpSndFilter & " OR (SERE='C' AND STAT='C' AND TTYP<>'AAD' AND TTYP<>' ')) AND WSTA<>'X'"
    ElseIf tmpRcvFilter <> "" Then
        SqlKey = "WHERE " & tmpTimeFilter & " AND " & tmpRcvFilter & " AND WSTA<>'X'"
    ElseIf tmpRcvFilter <> "" Then
        SqlKey = "WHERE " & tmpTimeFilter & " AND (" & tmpSndFilter & " OR (SERE='C' AND STAT='C' AND TTYP<>'AAD' AND TTYP<>' ')) AND WSTA<>'X'"
    Else
        SqlKey = ""
    End If
    'MsgBox SqlKey
    If SqlKey <> "" Then
        Screen.MousePointer = 11
        count = -1
        tmpFields = DataPool.TlxTabFields
        RetVal = UfisServer.CallCeda(tmpVal, "RTA", "TLXTAB", tmpFields, "", SqlKey, "CDAT", 0, False, False)
        count = UfisServer.DataBuffer(0).GetLineCount - 1
        If count >= 0 Then
            For i = 0 To count
                TlxRec = UfisServer.DataBuffer(0).GetLineValues(i)
                EvaluateBc "", "", "", "TLXTAB", "IBT", SqlKey, tmpFields, TlxRec
            Next
        End If
        UfisServer.DataBuffer(0).ResetContent
        Screen.MousePointer = 0
    End If
End Sub
Private Sub ReorgLayers()
Dim i As Integer
    MainButtons.ZOrder
    RightCover.ZOrder
    For i = 0 To folderButtons.count - 1
        folderButtons(i).ZOrder
    Next
    basicButtons.ZOrder
    TopButtons.ZOrder
    MyStatusBar.ZOrder
    CedaBusyFlag.ZOrder
End Sub
Private Function GetValidTimeFrame(Vpfr As String, Vpto As String, CheckUtc As Boolean) As Boolean
    Dim RetVal As Boolean
    Dim clDateFrom As String
    Dim clDateTo As String
    Dim clTimeFrom As String
    Dim clTimeTo As String
    Dim TimeVal
    RetVal = False
    clDateFrom = txtDateFrom.Tag
    clDateTo = txtDateTo.Tag
    clTimeFrom = Left(txtTimeFrom.Text, 2) & Right(txtTimeFrom.Text, 2) & "00"
    clTimeTo = Left(txtTimeTo.Text, 2) & Right(txtTimeTo.Text, 2) & "59"
    Vpfr = clDateFrom & clTimeFrom
    Vpto = clDateTo & clTimeTo
    If (CheckUtc) And (optLocal.Value = True) Then
        TimeVal = CedaFullDateToVb(Vpfr)
        TimeVal = DateAdd("n", -UtcTimeDiff, TimeVal)
        Vpfr = Format(TimeVal, "yyyymmddhhmmss")
        TimeVal = CedaFullDateToVb(Vpto)
        TimeVal = DateAdd("n", -UtcTimeDiff, TimeVal)
        Vpto = Format(TimeVal, "yyyymmddhhmmss")
    End If
    RetVal = True
    GetValidTimeFrame = RetVal
End Function

Private Sub UseCSGN_Change()
    If Trim(UseCSGN.Text) <> Trim(AtcCSGN.Text) Then
        CsgnStatus.Visible = True
    Else
        CsgnStatus.Visible = False
    End If
End Sub
