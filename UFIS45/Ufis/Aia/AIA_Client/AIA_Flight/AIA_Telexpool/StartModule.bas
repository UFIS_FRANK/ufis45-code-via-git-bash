Attribute VB_Name = "StartModule"
Option Explicit
Public StartUpMain As Boolean
Public Function GetModuleVersion()
    GetModuleVersion = ""
End Function

Sub Main()
    Dim CmdLine As String
    Screen.MousePointer = 11
    ApplicationIsStarted = False
    AutoArrange = False
    StartUpMain = True
    ShutDownRequested = False
    If App.PrevInstance = True Then
        MyMsgBox.InfoApi 1, "", ""
        With Screen
            If MyMsgBox.AskUser(0, .Width / 2, .Height / 2, App.EXEName & ": Application Control", "This module is already open!", "stop", "", UserAnswer) >= 0 Then ShutDownRequested = True
        End With
        End
    End If
    Screen.MousePointer = 11
    GetFileAndPath DEFAULT_CEDA_INI, myIniFile, myIniPath
    myIniFile = App.EXEName & ".ini"
    myIniFullName = myIniPath & "\" & myIniFile
    If Dir(myIniFullName) = "" Then
        GetFileAndPath DEFAULT_CEDA_INI, myIniFile, myIniPath
        myIniFullName = myIniPath & "\" & myIniFile
    End If
    Load PoolConfig
    CmdLine = Command
    If (InStr(CmdLine, "NOMAIN") > 0) Or (PoolConfig.OnLineCfg(0).Value = 0) Then StartUpMain = False
    HiddenFocus.Show
    HiddenFocus.Refresh
    Screen.MousePointer = 11
    If GetIniEntry(myIniFullName, "MAIN_WINDOW", "", "SHOW_INTRO", "YES") = "YES" Then
        HiddenMain.Show
        HiddenMain.Refresh
    End If
    Load UfisServer
    HiddenMain.CedaStatus(1).Text = "CONNECTING TO '" & UfisServer.HostName & "'"
    Screen.MousePointer = 11
    Load TelexPoolHead
    Screen.MousePointer = 11
    If StartUpMain = True Then
        TelexPoolHead.Show
        TelexPoolHead.Refresh
    End If
    TelexPoolHead.InitMyForm
    Screen.MousePointer = 11
    If PoolConfig.OnLineCfg(1).Value = 1 Then
        RcvTelex.Show
        RcvTelex.Refresh
    End If
    If PoolConfig.OnLineCfg(2).Value = 1 Then
        SndTelex.Show
        SndTelex.Refresh
    End If
    ApplicationIsStarted = True
    TelexPoolHead.InitMemButtons
    TelexPoolHead.FixWin.Value = 1
    HiddenFocus.SetFocus
    TelexPoolHead.FixWin.Value = 0
    With HiddenMain
        .Move .Left, -.Height - 1000
    End With
    Screen.MousePointer = 11
    TelexPoolHead.MyStatusBar.Panels(1).Text = "Starting Broadcast Handler (BcProxy) ..."
    UfisServer.ConnectToBcProxy
    TelexPoolHead.MyStatusBar.Panels(1).Text = "Loading Online Data ..."
    TelexPoolHead.RefreshOnlineWindows
    TelexPoolHead.CheckLoadButton
    SetAllFormsOnTop True
    TelexPoolHead.MyStatusBar.Panels(1).Text = "Starting Ufis Application Manager ..."
    UfisServer.ConnectToApplMgr
    TelexPoolHead.MyStatusBar.Panels(1).Text = ""
    Screen.MousePointer = 0
End Sub

Public Sub RefreshMain()

End Sub

Public Function CheckCancelExit(CurForm As Form, MaxForms As Integer) As Boolean
    If CountVisibleForms <= MaxForms Then
        CheckCancelExit = False
        ShutDownApplication
        'we might come back ... (when the user cancels the request)
    Else
        CurForm.Hide
    End If
    CheckCancelExit = True
End Function
Public Sub ShutDownApplication()
Dim i As Integer
Dim cnt As Integer
    On Error GoTo ErrorHandler
    MyMsgBox.InfoApi 0, "The 'On Line' functions will be disconnected from the server" & vbNewLine & "after leaving the Telex Pool.", "Tip"
    With TelexPoolHead
        If MyMsgBox.CallAskUser(0, 0, 0, "Application Control", "Do you want to exit ?", "ask", "Yes,No;F", UserAnswer) = 1 Then
            ShutDownRequested = True
            UfisServer.aCeda.CleanupCom
            cnt = Forms.count - 1
            For i = cnt To 0 Step -1
                Unload Forms(i)
            Next
            End
        End If
    End With
ErrorHandler:
    Resume Next
End Sub

Public Sub SetAllFormsOnTop(SetValue As Boolean)
    CheckFormOnTop RcvTelex, SetValue
    CheckFormOnTop SndTelex, SetValue
    CheckFormOnTop TelexPoolHead, SetValue
End Sub


Public Sub HandleBroadCast(ReqId As String, DestName As String, RecvName As String, CedaCmd As String, ObjName As String, Seq As String, Tws As String, Twe As String, CedaSqlKey As String, Fields As String, Data As String, BcNum As String)
Dim s As String
Dim clDest As String
Dim clRecv As String
Dim clCmd As String
Dim clTable As String
Dim clSelKey As String
Dim clFields As String
Dim clData As String
    clTable = ObjName
    If InStr("TLXTAB,AFTTAB,TFRTAB", clTable) > 0 Then
        clCmd = CedaCmd
        clSelKey = CedaSqlKey
        clFields = Fields
        clData = Data
        TelexPoolHead.EvaluateBc BcNum, DestName, RecvName, clTable, clCmd, clSelKey, clFields, clData
    End If
End Sub

Public Sub MsgFromUFISAppManager(Orig As Long, Data As String)
Dim tmpFldLst As String
Dim tmpDatLst As String
Dim nlPos As Integer
    'TelexPoolHead.Text1.Text = TelexPoolHead.Text1.Text & vbNewLine & "FROM APP MNG" & vbNewLine & Data
    If ApplicationIsStarted And Not ModalMsgIsOpen Then
        TelexPoolHead.Show
        TelexPoolHead.Refresh
        nlPos = InStr(Data, vbLf)
        If nlPos > 1 Then
            tmpFldLst = Left(Data, nlPos - 1)
            tmpDatLst = Mid(Data, nlPos + 1)
            'TelexPoolHead.Text1.Text = TelexPoolHead.Text1.Text & vbNewLine & tmpFldLst & vbNewLine & tmpDatLst & vbNewLine & "------------------"
            If Left(tmpDatLst, 2) <> "-1" Then
                CheckFormOnTop TelexPoolHead, True
                TelexPoolHead.SearchFlightTelexes tmpFldLst, tmpDatLst
            End If
        Else
            'If MyMsgBox.CallAskUser(0, 0, 0, "Communication Control", "Undefined message received from FIPS", "hand", "", UserAnswer) > 0 Then DoNothing
        End If
    End If
End Sub

Public Sub HandleDisplayChanged()
    With TelexPoolHead
        If .Width > UfisTools.SysInfo.WorkAreaWidth Then
            .Left = UfisTools.SysInfo.WorkAreaLeft
            .Width = UfisTools.SysInfo.WorkAreaWidth
        End If
        If .Height > UfisTools.SysInfo.WorkAreaHeight Then
            .Top = UfisTools.SysInfo.WorkAreaTop
            .Height = UfisTools.SysInfo.WorkAreaHeight
        End If
    End With
End Sub

Public Sub HandleSysColorsChanged()
    TelexPoolHead.ArrangeAllWin
End Sub

Public Sub HandleTimeChanged()
    TelexPoolHead.InitDateFields
End Sub

