VERSION 5.00
Begin VB.Form TlxSndStat 
   Caption         =   "SND Telex Status"
   ClientHeight    =   2385
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   5445
   Icon            =   "TlxSndStat.frx":0000
   LinkTopic       =   "Form3"
   LockControls    =   -1  'True
   ScaleHeight     =   2385
   ScaleWidth      =   5445
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin VB.CheckBox chkWork 
      Caption         =   "Update"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   2
      Left            =   4560
      Style           =   1  'Graphical
      TabIndex        =   36
      Top             =   1470
      Width           =   855
   End
   Begin VB.CheckBox chkWork 
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   1
      Left            =   4560
      Style           =   1  'Graphical
      TabIndex        =   35
      Top             =   1770
      Width           =   855
   End
   Begin VB.CheckBox chkWork 
      Caption         =   "Close"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   0
      Left            =   4560
      Style           =   1  'Graphical
      TabIndex        =   34
      Top             =   2070
      Width           =   855
   End
   Begin VB.Frame fraRcvStat 
      Caption         =   "Work Flow"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2055
      Index           =   1
      Left            =   2280
      TabIndex        =   17
      Top             =   300
      Width           =   2205
      Begin VB.CheckBox chkRecv 
         Caption         =   "5"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   15
         Left            =   120
         TabIndex        =   25
         Top             =   1740
         Width           =   525
      End
      Begin VB.CheckBox chkRecv 
         Caption         =   "4"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   14
         Left            =   120
         TabIndex        =   24
         Top             =   1530
         Width           =   525
      End
      Begin VB.CheckBox chkRecv 
         Caption         =   "3"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   13
         Left            =   120
         TabIndex        =   23
         Top             =   1320
         Width           =   525
      End
      Begin VB.CheckBox chkRecv 
         Caption         =   "2"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   12
         Left            =   120
         TabIndex        =   22
         Top             =   1110
         Width           =   525
      End
      Begin VB.CheckBox chkRecv 
         Caption         =   "1"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   11
         Left            =   120
         TabIndex        =   21
         Top             =   900
         Width           =   525
      End
      Begin VB.CheckBox chkRecv 
         Caption         =   "0"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   10
         Left            =   120
         TabIndex        =   20
         Top             =   690
         Width           =   525
      End
      Begin VB.CheckBox chkRecv 
         Caption         =   "9"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   9
         Left            =   120
         TabIndex        =   19
         Top             =   480
         Width           =   525
      End
      Begin VB.CheckBox chkRecv 
         Caption         =   "8"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   8
         Left            =   120
         TabIndex        =   18
         Top             =   270
         Width           =   525
      End
      Begin VB.Label lblRecv 
         Caption         =   "5"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   15
         Left            =   660
         TabIndex        =   33
         Top             =   1740
         Width           =   1455
      End
      Begin VB.Label lblRecv 
         Caption         =   "9"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   9
         Left            =   660
         TabIndex        =   32
         Top             =   480
         Width           =   1455
      End
      Begin VB.Label lblRecv 
         Caption         =   "0"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   10
         Left            =   660
         TabIndex        =   31
         Top             =   690
         Width           =   1455
      End
      Begin VB.Label lblRecv 
         Caption         =   "1"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   11
         Left            =   660
         TabIndex        =   30
         Top             =   900
         Width           =   1455
      End
      Begin VB.Label lblRecv 
         Caption         =   "2"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   12
         Left            =   660
         TabIndex        =   29
         Top             =   1110
         Width           =   1455
      End
      Begin VB.Label lblRecv 
         Caption         =   "3"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   13
         Left            =   660
         TabIndex        =   28
         Top             =   1320
         Width           =   1455
      End
      Begin VB.Label lblRecv 
         Caption         =   "4"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   14
         Left            =   660
         TabIndex        =   27
         Top             =   1530
         Width           =   1455
      End
      Begin VB.Label lblRecv 
         Caption         =   "8"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   8
         Left            =   660
         TabIndex        =   26
         Top             =   270
         Width           =   1455
      End
   End
   Begin VB.Frame fraRcvStat 
      Caption         =   "System"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2055
      Index           =   0
      Left            =   15
      TabIndex        =   0
      Top             =   300
      Width           =   2205
      Begin VB.CheckBox chkRecv 
         Caption         =   "7"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   7
         Left            =   120
         TabIndex        =   8
         Top             =   1740
         Width           =   525
      End
      Begin VB.CheckBox chkRecv 
         Caption         =   "1"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   1
         Left            =   120
         TabIndex        =   2
         Top             =   480
         Width           =   525
      End
      Begin VB.CheckBox chkRecv 
         Caption         =   "2"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   2
         Left            =   120
         TabIndex        =   3
         Top             =   690
         Width           =   525
      End
      Begin VB.CheckBox chkRecv 
         Caption         =   "3"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   3
         Left            =   120
         TabIndex        =   4
         Top             =   900
         Width           =   525
      End
      Begin VB.CheckBox chkRecv 
         Caption         =   "4"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   4
         Left            =   120
         TabIndex        =   5
         Top             =   1110
         Width           =   525
      End
      Begin VB.CheckBox chkRecv 
         Caption         =   "5"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   5
         Left            =   120
         TabIndex        =   6
         Top             =   1320
         Width           =   525
      End
      Begin VB.CheckBox chkRecv 
         Caption         =   "6"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   6
         Left            =   120
         TabIndex        =   7
         Top             =   1530
         Width           =   525
      End
      Begin VB.CheckBox chkRecv 
         Caption         =   "0"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   0
         Left            =   120
         TabIndex        =   1
         Top             =   270
         Width           =   525
      End
      Begin VB.Label lblRecv 
         Caption         =   "7"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   7
         Left            =   660
         TabIndex        =   16
         Top             =   1740
         Width           =   1455
      End
      Begin VB.Label lblRecv 
         Caption         =   "6"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   6
         Left            =   660
         TabIndex        =   15
         Top             =   1530
         Width           =   1455
      End
      Begin VB.Label lblRecv 
         Caption         =   "5"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   5
         Left            =   660
         TabIndex        =   14
         Top             =   1320
         Width           =   1455
      End
      Begin VB.Label lblRecv 
         Caption         =   "4"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   4
         Left            =   660
         TabIndex        =   13
         Top             =   1110
         Width           =   1455
      End
      Begin VB.Label lblRecv 
         Caption         =   "3"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   3
         Left            =   660
         TabIndex        =   12
         Top             =   900
         Width           =   1455
      End
      Begin VB.Label lblRecv 
         Caption         =   "2"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   2
         Left            =   660
         TabIndex        =   11
         Top             =   690
         Width           =   1455
      End
      Begin VB.Label lblRecv 
         Caption         =   "1"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   1
         Left            =   660
         TabIndex        =   10
         Top             =   480
         Width           =   1455
      End
      Begin VB.Label lblRecv 
         Caption         =   "0"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   0
         Left            =   660
         TabIndex        =   9
         Top             =   270
         Width           =   1455
      End
   End
   Begin VB.Label Label1 
      Caption         =   "Status of outgoing telexes"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   225
      Left            =   30
      TabIndex        =   37
      Top             =   30
      Width           =   2715
   End
End
Attribute VB_Name = "TlxSndStat"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private MyParent As Variant
Private MyCallButton As Control
Private UseMultiSel As Boolean

Public Sub RegisterMyParent(Caller As Form, CallButton As Control, TaskValue As String)
    Set MyParent = Caller
    Set MyCallButton = CallButton
End Sub
Public Sub SetMultiSelect(SetValue As Boolean)
    UseMultiSel = SetValue
    chkWork(2).Enabled = Not SetValue
End Sub
Private Sub InitForm(ForWhat As Integer)
    Dim StatTxt As String
    Dim tmpStat As String
    Dim i As Integer
    Dim j As Integer
    UseMultiSel = False
    Select Case ForWhat
        Case 1
            For i = 0 To 15
                chkRecv(i).Caption = ""
                chkRecv(i).Enabled = False
                lblRecv(i).Caption = ""
            Next
            'System
            StatTxt = ""
            StatTxt = StatTxt & "C,Telex created,Q,Telex on Queue,S,Telex Sent,E,Sita Error,"
            tmpStat = "START"
            i = 0
            j = 0
            While tmpStat <> ""
                j = j + 1
                tmpStat = GetItem(StatTxt, j, ",")
                If tmpStat <> "" Then
                    chkRecv(i).Caption = tmpStat
                    j = j + 1
                    lblRecv(i).Caption = GetItem(StatTxt, j, ",")
                    chkRecv(i).Enabled = True
                End If
                i = i + 1
            Wend
            'Work Flow
            StatTxt = ""
            StatTxt = StatTxt & "V,Already viewed,X,Excluded"
            tmpStat = "START"
            i = 8
            j = 0
            While tmpStat <> ""
                j = j + 1
                tmpStat = GetItem(StatTxt, j, ",")
                If tmpStat <> "" Then
                    chkRecv(i).Caption = tmpStat
                    j = j + 1
                    lblRecv(i).Caption = GetItem(StatTxt, j, ",")
                    chkRecv(i).Enabled = True
                End If
                i = i + 1
            Wend
        Case Else
    End Select
End Sub
Public Function ExplainStatus(tmpFld, tmpVal, CheckIt As Boolean) As String
    Dim tmpResult As String
    Dim i As Integer
    Dim j1 As Integer
    Dim j2 As Integer
    Dim UseIdx As Integer
    SetMultiSelect (Not CheckIt)
    tmpResult = ""
    Select Case tmpFld
        Case "STAT"
            UseIdx = 0
            j1 = 0
            j2 = 7
        Case "WSTA"
            UseIdx = 1
            j1 = 8
            j2 = 15
        Case Else
            j1 = 0
            j2 = -1
    End Select
    For i = j1 To j2
        If CheckIt Then chkRecv(i).Value = 0
        If (chkRecv(i).Enabled) And (chkRecv(i).Caption = tmpVal) Then
            fraRcvStat(UseIdx).Tag = Str(i)
            If CheckIt Then
                chkRecv(i).Value = 1
            Else
                tmpResult = lblRecv(i).Caption
                Exit For
            End If
        End If
    Next
    If tmpResult <> "" Then tmpResult = tmpVal & " = " & tmpResult
    ExplainStatus = tmpResult
End Function

Private Sub chkRecv_Click(Index As Integer)
    Dim LastIdx As Integer
    Dim UseIdx As Integer
    If (Not UseMultiSel) Then
        If Index < 8 Then UseIdx = 0 Else UseIdx = 8
        If chkRecv(UseIdx).Tag <> "" Then LastIdx = Val(chkRecv(UseIdx).Tag) Else LastIdx = -1
        If chkRecv(Index).Value = 1 Then
            If LastIdx >= 0 Then chkRecv(LastIdx).Value = 0
            chkRecv(UseIdx).Tag = Str(Index)
            chkWork(2).Tag = chkRecv(0).Tag & "," & chkRecv(8).Tag
            If chkWork(2).Tag <> (fraRcvStat(0).Tag & "," & fraRcvStat(1).Tag) Then chkWork(2).Enabled = True Else chkWork(2).Enabled = False
        Else
            If LastIdx = Index Then chkRecv(UseIdx).Tag = ""
            chkWork(2).Tag = chkRecv(0).Tag & "," & chkRecv(8).Tag
        End If
    Else
        TelexPoolHead.CheckLoadButton
    End If
End Sub

Private Sub UpdateStatusFields()
    Dim tmpUrno As String
    Dim tmpFldLst As String
    Dim tmpDatLst As String
    Dim tmpSqlKey As String
    Dim RetVal As String
    Dim RetCode As Integer
    Dim UseIdx As Integer
    tmpUrno = Trim(TelexPoolHead.CurrentUrno.Text)
    If tmpUrno <> "" Then
        tmpFldLst = ""
        tmpDatLst = ""
        If chkRecv(0).Tag <> "" Then
            UseIdx = Val(chkRecv(0).Tag)
            tmpFldLst = tmpFldLst & ",STAT"
            tmpDatLst = tmpDatLst & "," & chkRecv(UseIdx).Caption
        End If
        If chkRecv(8).Tag <> "" Then
            UseIdx = Val(chkRecv(8).Tag)
            tmpFldLst = tmpFldLst & ",WSTA"
            tmpDatLst = tmpDatLst & "," & chkRecv(UseIdx).Caption
        End If
        If tmpFldLst <> "" Then
            tmpFldLst = Mid(tmpFldLst, 2)
            tmpDatLst = Mid(tmpDatLst, 2)
            tmpSqlKey = "WHERE URNO=" & tmpUrno
            RetCode = UfisServer.CallCeda(RetVal, "URT", DataPool.TableName, tmpFldLst, tmpDatLst, tmpSqlKey, "", 0, True, False)
        End If
    'Else
    '    MyMsgBox.CallAskUser 0, 0, 0, "Update Manager", "No data selected to update.", "stop", "", UserAnswer
    End If
End Sub

Public Function GetStatusFilter() As String
    Dim Result As String
    Dim tmpList As String
    Dim i As Integer
    Result = ""
    tmpList = ""
    For i = 0 To 7
        If chkRecv(i).Value = 1 Then tmpList = tmpList & ",'" & chkRecv(i).Caption & "'"
    Next
    tmpList = Mid(tmpList, 2)
    If tmpList <> "" Then Result = "STAT IN (" & tmpList & ")"
    tmpList = ""
    For i = 8 To 15
        If chkRecv(i).Value = 1 Then tmpList = tmpList & ",'" & chkRecv(i).Caption & "'"
    Next
    tmpList = Mid(tmpList, 2)
    If tmpList <> "" Then
        If Result <> "" Then Result = Result & " AND "
        Result = Result & "WSTA IN (" & tmpList & ")"
    End If
    GetStatusFilter = Result
End Function

Private Sub chkWork_Click(Index As Integer)
    On Error Resume Next
    If chkWork(Index).Value = 1 Then chkWork(Index).BackColor = LightestGreen
    Select Case Index
        Case 0  'close
            If chkWork(Index).Value = 1 Then
                If IsObject(MyCallButton) Then MyCallButton.Value = 0
                chkWork(Index).Value = 0
            End If
        Case 2
            If chkWork(Index).Value = 1 Then
                UpdateStatusFields
                chkWork(Index).Value = 0
            End If
    End Select
    If chkWork(Index).Value = 0 Then chkWork(Index).BackColor = vbButtonFace
End Sub


Private Sub Form_Load()
    Me.Top = TelexPoolHead.Top + 316 * Screen.TwipsPerPixelY
    Me.Left = TelexPoolHead.Left + 250 * Screen.TwipsPerPixelX
    InitForm 1
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    chkWork(0).Value = 1
    Cancel = True
End Sub


