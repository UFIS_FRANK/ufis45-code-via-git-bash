VERSION 5.00
Begin VB.Form FlightRotation 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Flight Rotation"
   ClientHeight    =   6915
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   10485
   BeginProperty Font 
      Name            =   "MS Sans Serif"
      Size            =   8.25
      Charset         =   161
      Weight          =   700
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "FlightRotation.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6915
   ScaleWidth      =   10485
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Visible         =   0   'False
   Begin VB.Frame AcrSign 
      BorderStyle     =   0  'None
      Enabled         =   0   'False
      Height          =   285
      Index           =   0
      Left            =   1035
      TabIndex        =   139
      Top             =   420
      Visible         =   0   'False
      Width           =   1125
      Begin VB.TextBox AcrFlag 
         Alignment       =   2  'Center
         BackColor       =   &H000000FF&
         ForeColor       =   &H00FFFFFF&
         Height          =   285
         Index           =   1
         Left            =   0
         TabIndex        =   140
         Text            =   "UNKNOWN"
         Top             =   0
         Width           =   1125
      End
   End
   Begin VB.CheckBox chkAssign 
      Caption         =   "Assign"
      Height          =   285
      Index           =   0
      Left            =   4890
      Style           =   1  'Graphical
      TabIndex        =   134
      Top             =   5040
      Width           =   855
   End
   Begin VB.Frame ElseData 
      Caption         =   " Other Information"
      Height          =   1305
      Left            =   7200
      TabIndex        =   123
      Top             =   5580
      Width           =   3255
      Begin VB.Frame ElseSign 
         BorderStyle     =   0  'None
         Enabled         =   0   'False
         Height          =   285
         Index           =   0
         Left            =   90
         TabIndex        =   141
         Top             =   240
         Width           =   3075
         Begin VB.TextBox ElseFlag 
            Alignment       =   2  'Center
            BackColor       =   &H000000FF&
            ForeColor       =   &H00FFFFFF&
            Height          =   285
            Index           =   0
            Left            =   0
            TabIndex        =   142
            Text            =   "Not in the actual flight data view"
            Top             =   0
            Width           =   3075
         End
      End
      Begin VB.TextBox ElseFld 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   1
         Left            =   1710
         TabIndex        =   127
         Text            =   "B23"
         Top             =   570
         Width           =   615
      End
      Begin VB.TextBox ElseFld 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   2
         Left            =   2340
         TabIndex        =   126
         Text            =   "23:55-1"
         Top             =   570
         Width           =   825
      End
      Begin VB.TextBox ElseFld 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   0
         Left            =   870
         TabIndex        =   125
         Text            =   "23:55-1"
         Top             =   570
         Width           =   825
      End
      Begin VB.TextBox ElseFld 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   3
         Left            =   1320
         TabIndex        =   124
         Text            =   "24.07.2000/23:45"
         Top             =   900
         Width           =   1845
      End
      Begin VB.Label ElseLbl 
         Caption         =   "Last Update:"
         Height          =   225
         Index           =   1
         Left            =   120
         TabIndex        =   129
         Top             =   930
         Width           =   1275
      End
      Begin VB.Label ElseLbl 
         Caption         =   "Towing:"
         Height          =   225
         Index           =   0
         Left            =   120
         TabIndex        =   128
         Top             =   600
         Width           =   765
      End
   End
   Begin VB.Frame GeneralData 
      Caption         =   " General "
      Height          =   1305
      Left            =   30
      TabIndex        =   119
      Top             =   5580
      Width           =   7035
      Begin VB.TextBox RemFld 
         Height          =   285
         Index           =   2
         Left            =   1290
         TabIndex        =   122
         Top             =   900
         Width           =   5625
      End
      Begin VB.TextBox RemFld 
         Height          =   285
         Index           =   1
         Left            =   1290
         TabIndex        =   121
         Top             =   570
         Width           =   5625
      End
      Begin VB.TextBox RemFld 
         Height          =   285
         Index           =   0
         Left            =   1290
         TabIndex        =   120
         Top             =   240
         Width           =   5625
      End
      Begin VB.Label RemLbl 
         Caption         =   "SI to send:"
         Height          =   225
         Index           =   2
         Left            =   120
         TabIndex        =   132
         Top             =   930
         Width           =   1125
      End
      Begin VB.Label RemLbl 
         Caption         =   "SI Received:"
         Height          =   225
         Index           =   1
         Left            =   120
         TabIndex        =   131
         Top             =   600
         Width           =   1125
      End
      Begin VB.Label RemLbl 
         Caption         =   "Remark:"
         Height          =   225
         Index           =   0
         Left            =   120
         TabIndex        =   130
         Top             =   270
         Width           =   765
      End
   End
   Begin VB.TextBox RotFlag 
      Alignment       =   2  'Center
      BackColor       =   &H000000FF&
      ForeColor       =   &H00FFFFFF&
      Height          =   285
      Index           =   1
      Left            =   8370
      TabIndex        =   118
      Text            =   "BUS TRANSFER"
      Top             =   1080
      Visible         =   0   'False
      Width           =   2085
   End
   Begin VB.TextBox RotFlag 
      Alignment       =   2  'Center
      BackColor       =   &H000000FF&
      ForeColor       =   &H00FFFFFF&
      Height          =   285
      Index           =   0
      Left            =   2010
      TabIndex        =   115
      Text            =   "ROTATION LOCKED BY USER"
      Top             =   1080
      Visible         =   0   'False
      Width           =   3855
   End
   Begin VB.CheckBox OnTop 
      Caption         =   "On Top"
      Height          =   285
      Left            =   9585
      Style           =   1  'Graphical
      TabIndex        =   114
      Top             =   45
      Width           =   855
   End
   Begin VB.Frame DepartureData 
      Caption         =   " Departure:"
      Height          =   4155
      Left            =   6000
      TabIndex        =   62
      Top             =   1290
      Width           =   4455
      Begin VB.CheckBox chkAssign 
         Caption         =   "Assign"
         Height          =   285
         Index           =   1
         Left            =   3480
         Style           =   1  'Graphical
         TabIndex        =   143
         Top             =   3750
         Width           =   855
      End
      Begin VB.CheckBox DepButton 
         Caption         =   "Transfer"
         Height          =   285
         Index           =   2
         Left            =   2340
         Style           =   1  'Graphical
         TabIndex        =   136
         Top             =   3750
         Width           =   855
      End
      Begin VB.TextBox DepFld 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   5
         Left            =   1440
         TabIndex        =   111
         Text            =   "0045/0060H"
         Top             =   1170
         Width           =   1245
      End
      Begin VB.TextBox DepFld 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   4
         Left            =   1860
         TabIndex        =   110
         Text            =   "19"
         Top             =   3060
         Width           =   615
      End
      Begin VB.TextBox DepFld 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   3
         Left            =   1230
         TabIndex        =   109
         Text            =   "19"
         Top             =   3060
         Width           =   615
      End
      Begin VB.TextBox DepFld 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   2
         Left            =   600
         TabIndex        =   108
         Text            =   "19"
         Top             =   3060
         Width           =   615
      End
      Begin VB.TextBox DepFld 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   1
         Left            =   3540
         TabIndex        =   107
         Text            =   "05:00"
         Top             =   2760
         Width           =   825
      End
      Begin VB.TextBox DepFld 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   61
         Left            =   1800
         TabIndex        =   94
         Text            =   "M"
         Top             =   240
         Width           =   315
      End
      Begin VB.TextBox DepFld 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   60
         Left            =   1230
         TabIndex        =   93
         Text            =   "1234"
         Top             =   240
         Width           =   555
      End
      Begin VB.TextBox DepFld 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   59
         Left            =   600
         TabIndex        =   92
         Text            =   "BMW"
         Top             =   240
         Width           =   615
      End
      Begin VB.TextBox DepFld 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   0
         Left            =   120
         TabIndex        =   91
         Text            =   "SW"
         Top             =   240
         Width           =   420
      End
      Begin VB.TextBox DepFld 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   57
         Left            =   600
         TabIndex        =   90
         Text            =   "06:00"
         Top             =   1470
         Width           =   825
      End
      Begin VB.TextBox DepFld 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   56
         Left            =   600
         TabIndex        =   89
         Text            =   "06:00"
         Top             =   1770
         Width           =   825
      End
      Begin VB.TextBox DepFld 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   55
         Left            =   600
         TabIndex        =   88
         Text            =   "ATH"
         Top             =   1170
         Width           =   825
      End
      Begin VB.TextBox DepFld 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   54
         Left            =   1440
         TabIndex        =   87
         Text            =   "06:05"
         Top             =   1770
         Width           =   825
      End
      Begin VB.TextBox DepFld 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   53
         Left            =   2700
         TabIndex        =   86
         Text            =   "SKG"
         Top             =   1170
         Width           =   825
      End
      Begin VB.TextBox DepFld 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   52
         Left            =   2700
         TabIndex        =   85
         Text            =   "03:50"
         Top             =   1470
         Width           =   825
      End
      Begin VB.TextBox DepFld 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   51
         Left            =   3120
         TabIndex        =   84
         Text            =   "03:50"
         Top             =   1770
         Width           =   825
      End
      Begin VB.TextBox DepFld 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   49
         Left            =   3540
         TabIndex        =   83
         Text            =   "LHR"
         Top             =   1170
         Width           =   825
      End
      Begin VB.TextBox ArrFld 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   48
         Left            =   3540
         TabIndex        =   82
         Text            =   "04:50"
         Top             =   1470
         Width           =   825
      End
      Begin VB.TextBox DepFld 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   45
         Left            =   600
         TabIndex        =   81
         Text            =   "B23"
         Top             =   2070
         Width           =   615
      End
      Begin VB.TextBox DepFld 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   44
         Left            =   1230
         TabIndex        =   80
         Text            =   "19"
         Top             =   2070
         Width           =   615
      End
      Begin VB.TextBox DepFld 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   43
         Left            =   600
         TabIndex        =   79
         Text            =   "KLM"
         Top             =   540
         Width           =   615
      End
      Begin VB.TextBox DepFld 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   42
         Left            =   1230
         TabIndex        =   78
         Text            =   "001"
         Top             =   540
         Width           =   555
      End
      Begin VB.TextBox DepFld 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   41
         Left            =   1800
         TabIndex        =   77
         Text            =   " "
         Top             =   540
         Width           =   315
      End
      Begin VB.TextBox DepFld 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   40
         Left            =   600
         TabIndex        =   76
         Text            =   "04:50"
         Top             =   2460
         Width           =   825
      End
      Begin VB.TextBox DepFld 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   39
         Left            =   2700
         TabIndex        =   75
         Text            =   "04:50"
         Top             =   2460
         Width           =   825
      End
      Begin VB.TextBox DepFld 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   38
         Left            =   3540
         TabIndex        =   74
         Text            =   "05:00"
         Top             =   2460
         Width           =   825
      End
      Begin VB.TextBox DepFld 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   37
         Left            =   1860
         TabIndex        =   73
         Text            =   "04:40"
         Top             =   2460
         Width           =   825
      End
      Begin VB.TextBox DepFld 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   36
         Left            =   600
         TabIndex        =   72
         Text            =   "83"
         Top             =   2760
         Width           =   465
      End
      Begin VB.TextBox DepFld 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   35
         Left            =   1080
         TabIndex        =   71
         Text            =   "0003"
         Top             =   2760
         Width           =   555
      End
      Begin VB.TextBox DepFld 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   34
         Left            =   1650
         TabIndex        =   70
         Text            =   "ES"
         Top             =   2760
         Width           =   465
      End
      Begin VB.TextBox DepFld 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   33
         Left            =   2130
         TabIndex        =   69
         Text            =   "0002"
         Top             =   2760
         Width           =   555
      End
      Begin VB.TextBox DepFld 
         Height          =   285
         Index           =   31
         Left            =   600
         TabIndex        =   68
         Top             =   3360
         Width           =   615
      End
      Begin VB.CheckBox DepButton 
         Caption         =   "Telex"
         Height          =   285
         Index           =   0
         Left            =   600
         Style           =   1  'Graphical
         TabIndex        =   67
         Top             =   3750
         Width           =   855
      End
      Begin VB.CheckBox DepButton 
         Caption         =   "Cancel"
         Height          =   285
         Index           =   1
         Left            =   1470
         Style           =   1  'Graphical
         TabIndex        =   66
         Top             =   3750
         Width           =   855
      End
      Begin VB.TextBox DepFlag 
         Alignment       =   2  'Center
         BackColor       =   &H000000FF&
         ForeColor       =   &H00FFFFFF&
         Height          =   285
         Index           =   0
         Left            =   2130
         TabIndex        =   65
         Text            =   "CANCELED"
         Top             =   240
         Visible         =   0   'False
         Width           =   1125
      End
      Begin VB.Frame Frame2 
         BorderStyle     =   0  'None
         Enabled         =   0   'False
         Height          =   225
         Left            =   1110
         TabIndex        =   63
         Top             =   -15
         Width           =   1245
         Begin VB.TextBox DepDay 
            Alignment       =   2  'Center
            BackColor       =   &H8000000F&
            BorderStyle     =   0  'None
            Height          =   225
            Index           =   0
            Left            =   0
            TabIndex        =   64
            Text            =   "24.07.2000"
            Top             =   15
            Width           =   1155
         End
      End
      Begin VB.Label DepLbl 
         Alignment       =   2  'Center
         Caption         =   "Flight Time"
         Height          =   225
         Index           =   3
         Left            =   1440
         TabIndex        =   117
         Top             =   930
         Width           =   1215
      End
      Begin VB.Label DepLbl 
         Caption         =   "CT: A"
         Height          =   225
         Index           =   2
         Left            =   2970
         TabIndex        =   116
         Top             =   2790
         Width           =   495
      End
      Begin VB.Label DepLbl 
         Alignment       =   2  'Center
         Caption         =   "ISK"
         Height          =   225
         Index           =   1
         Left            =   3630
         TabIndex        =   113
         Top             =   2250
         Width           =   555
      End
      Begin VB.Label DepLbl 
         Caption         =   "EA:"
         Height          =   225
         Index           =   0
         Left            =   2700
         TabIndex        =   112
         Top             =   1800
         Width           =   495
      End
      Begin VB.Label DepLbl 
         Alignment       =   2  'Center
         Caption         =   "Origin"
         Height          =   195
         Index           =   23
         Left            =   720
         TabIndex        =   106
         Top             =   930
         Width           =   585
      End
      Begin VB.Label DepLbl 
         Alignment       =   2  'Center
         Caption         =   "Dest."
         Height          =   195
         Index           =   22
         Left            =   3660
         TabIndex        =   105
         Top             =   930
         Width           =   555
      End
      Begin VB.Label DepLbl 
         Alignment       =   2  'Center
         Caption         =   "Via"
         Height          =   195
         Index           =   21
         Left            =   2820
         TabIndex        =   104
         Top             =   930
         Width           =   585
      End
      Begin VB.Label DepLbl 
         Caption         =   "Plan:"
         Height          =   225
         Index           =   20
         Left            =   120
         TabIndex        =   103
         Top             =   1500
         Width           =   495
      End
      Begin VB.Label DepLbl 
         Caption         =   "Act.:"
         Height          =   225
         Index           =   19
         Left            =   120
         TabIndex        =   102
         Top             =   1800
         Width           =   615
      End
      Begin VB.Label DepLbl 
         Caption         =   "ED:"
         Height          =   225
         Index           =   18
         Left            =   120
         TabIndex        =   101
         Top             =   2490
         Width           =   495
      End
      Begin VB.Label DepLbl 
         Caption         =   "Dly.:"
         Height          =   225
         Index           =   17
         Left            =   120
         TabIndex        =   100
         Top             =   2790
         Width           =   555
      End
      Begin VB.Label DepLbl 
         Caption         =   "P/G:"
         Height          =   225
         Index           =   16
         Left            =   120
         TabIndex        =   99
         Top             =   2100
         Width           =   525
      End
      Begin VB.Label DepLbl 
         Alignment       =   2  'Center
         Caption         =   "ETD"
         Height          =   225
         Index           =   15
         Left            =   2040
         TabIndex        =   98
         Top             =   2250
         Width           =   525
      End
      Begin VB.Label DepLbl 
         Alignment       =   2  'Center
         Caption         =   "NI"
         Height          =   225
         Index           =   14
         Left            =   2820
         TabIndex        =   97
         Top             =   2250
         Width           =   555
      End
      Begin VB.Label DepLbl 
         Caption         =   "Pax:"
         Height          =   225
         Index           =   13
         Left            =   120
         TabIndex        =   96
         Top             =   3090
         Width           =   555
      End
      Begin VB.Label DepLbl 
         Caption         =   "Bag:"
         Height          =   225
         Index           =   12
         Left            =   120
         TabIndex        =   95
         Top             =   3390
         Width           =   555
      End
   End
   Begin VB.Frame ArrivalData 
      Caption         =   " Arrival: "
      Height          =   4155
      Left            =   30
      TabIndex        =   13
      Top             =   1290
      Width           =   5835
      Begin VB.TextBox ArrFld 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   5
         Left            =   600
         TabIndex        =   135
         Text            =   "23:50-1"
         Top             =   1470
         Width           =   825
      End
      Begin VB.Frame Frame1 
         BorderStyle     =   0  'None
         Enabled         =   0   'False
         Height          =   225
         Left            =   810
         TabIndex        =   60
         Top             =   -15
         Width           =   1155
         Begin VB.TextBox ArrDay 
            Alignment       =   2  'Center
            BackColor       =   &H8000000F&
            BorderStyle     =   0  'None
            Height          =   225
            Index           =   0
            Left            =   0
            TabIndex        =   61
            Text            =   "24.07.2000"
            Top             =   15
            Width           =   1125
         End
      End
      Begin VB.TextBox ArrFlag 
         Alignment       =   2  'Center
         BackColor       =   &H000000FF&
         ForeColor       =   &H00FFFFFF&
         Height          =   285
         Index           =   0
         Left            =   2130
         TabIndex        =   59
         Text            =   "CANCELED"
         Top             =   240
         Visible         =   0   'False
         Width           =   1125
      End
      Begin VB.CheckBox ArrButton 
         Caption         =   "Cancel"
         Height          =   285
         Index           =   1
         Left            =   1470
         Style           =   1  'Graphical
         TabIndex        =   58
         Top             =   3750
         Width           =   855
      End
      Begin VB.CheckBox ArrButton 
         Caption         =   "Divert"
         Height          =   285
         Index           =   2
         Left            =   2340
         Style           =   1  'Graphical
         TabIndex        =   57
         Top             =   3750
         Width           =   855
      End
      Begin VB.CheckBox ArrButton 
         Caption         =   "Telex"
         Height          =   285
         Index           =   0
         Left            =   600
         Style           =   1  'Graphical
         TabIndex        =   56
         Top             =   3750
         Width           =   855
      End
      Begin VB.TextBox ArrFld 
         Height          =   285
         Index           =   30
         Left            =   600
         TabIndex        =   54
         Top             =   3360
         Width           =   2085
      End
      Begin VB.TextBox ArrFld 
         Height          =   285
         Index           =   29
         Left            =   600
         TabIndex        =   52
         Text            =   "PX40/50/3/NIL"
         Top             =   3060
         Width           =   2085
      End
      Begin VB.TextBox ArrFld 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   28
         Left            =   2130
         TabIndex        =   51
         Text            =   "0002"
         Top             =   2760
         Width           =   555
      End
      Begin VB.TextBox ArrFld 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   27
         Left            =   1650
         TabIndex        =   50
         Text            =   "ES"
         Top             =   2760
         Width           =   465
      End
      Begin VB.TextBox ArrFld 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   26
         Left            =   1080
         TabIndex        =   49
         Text            =   "0003"
         Top             =   2760
         Width           =   555
      End
      Begin VB.TextBox ArrFld 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   25
         Left            =   600
         TabIndex        =   48
         Text            =   "83"
         Top             =   2760
         Width           =   465
      End
      Begin VB.TextBox ArrFld 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   24
         Left            =   4080
         TabIndex        =   46
         Text            =   "04:40"
         Top             =   2760
         Width           =   825
      End
      Begin VB.TextBox ArrFld 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   23
         Left            =   4920
         TabIndex        =   44
         Text            =   "05:00"
         Top             =   2460
         Width           =   825
      End
      Begin VB.TextBox ArrFld 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   22
         Left            =   4080
         TabIndex        =   43
         Text            =   "04:50"
         Top             =   2460
         Width           =   825
      End
      Begin VB.TextBox ArrFld 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   21
         Left            =   600
         TabIndex        =   42
         Text            =   "04:50"
         Top             =   2460
         Width           =   825
      End
      Begin VB.TextBox ArrFld 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   20
         Left            =   1800
         TabIndex        =   41
         Text            =   " "
         Top             =   540
         Width           =   315
      End
      Begin VB.TextBox ArrFld 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   19
         Left            =   1230
         TabIndex        =   40
         Text            =   "001"
         Top             =   540
         Width           =   555
      End
      Begin VB.TextBox ArrFld 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   18
         Left            =   600
         TabIndex        =   39
         Text            =   "KLM"
         Top             =   540
         Width           =   615
      End
      Begin VB.TextBox ArrFld 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   17
         Left            =   4710
         TabIndex        =   38
         Text            =   "19"
         Top             =   2070
         Width           =   615
      End
      Begin VB.TextBox ArrFld 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   16
         Left            =   4080
         TabIndex        =   37
         Text            =   "B23"
         Top             =   2070
         Width           =   615
      End
      Begin VB.TextBox ArrFld 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   15
         Left            =   4920
         TabIndex        =   31
         Text            =   "05:00"
         Top             =   1770
         Width           =   825
      End
      Begin VB.TextBox ArrFld 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   14
         Left            =   4080
         TabIndex        =   30
         Text            =   "04:50"
         Top             =   1770
         Width           =   825
      End
      Begin VB.TextBox ArrFld 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   13
         Left            =   4080
         TabIndex        =   29
         Text            =   "04:50"
         Top             =   1470
         Width           =   825
      End
      Begin VB.TextBox ArrFld 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   12
         Left            =   4080
         TabIndex        =   27
         Text            =   "ATH"
         Top             =   1170
         Width           =   825
      End
      Begin VB.TextBox ArrFld 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   11
         Left            =   3180
         TabIndex        =   26
         Text            =   "04:00"
         Top             =   1770
         Width           =   825
      End
      Begin VB.TextBox ArrFld 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   10
         Left            =   2340
         TabIndex        =   25
         Text            =   "03:50"
         Top             =   1770
         Width           =   825
      End
      Begin VB.TextBox ArrFld 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   9
         Left            =   2340
         TabIndex        =   24
         Text            =   "03:50"
         Top             =   1470
         Width           =   825
      End
      Begin VB.TextBox ArrFld 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   8
         Left            =   2340
         TabIndex        =   22
         Text            =   "SKG"
         Top             =   1170
         Width           =   825
      End
      Begin VB.TextBox ArrFld 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   7
         Left            =   1440
         TabIndex        =   21
         Text            =   "00:05"
         Top             =   1770
         Width           =   825
      End
      Begin VB.TextBox ArrFld 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   4
         Left            =   600
         TabIndex        =   20
         Text            =   "LHR"
         Top             =   1170
         Width           =   825
      End
      Begin VB.TextBox ArrFld 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   6
         Left            =   600
         TabIndex        =   19
         Text            =   "23:55-1"
         Top             =   1770
         Width           =   825
      End
      Begin VB.TextBox ArrFld 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   0
         Left            =   120
         TabIndex        =   17
         Text            =   "SW"
         Top             =   240
         Width           =   420
      End
      Begin VB.TextBox ArrFld 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   1
         Left            =   600
         TabIndex        =   16
         Text            =   "BMW"
         Top             =   240
         Width           =   615
      End
      Begin VB.TextBox ArrFld 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   2
         Left            =   1230
         TabIndex        =   15
         Text            =   "1234"
         Top             =   240
         Width           =   555
      End
      Begin VB.TextBox ArrFld 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   3
         Left            =   1800
         TabIndex        =   14
         Text            =   "M"
         Top             =   240
         Width           =   315
      End
      Begin VB.Label ArrLbl 
         Caption         =   "Bag:"
         Height          =   225
         Index           =   11
         Left            =   120
         TabIndex        =   55
         Top             =   3390
         Width           =   555
      End
      Begin VB.Label ArrLbl 
         Caption         =   "Pax:"
         Height          =   225
         Index           =   10
         Left            =   120
         TabIndex        =   53
         Top             =   3090
         Width           =   555
      End
      Begin VB.Label ArrLbl 
         Alignment       =   1  'Right Justify
         Caption         =   "TMO:"
         Height          =   225
         Index           =   9
         Left            =   3510
         TabIndex        =   47
         Top             =   2790
         Width           =   525
      End
      Begin VB.Label ArrLbl 
         Alignment       =   1  'Right Justify
         Caption         =   "ETA/NI:"
         Height          =   225
         Index           =   8
         Left            =   3240
         TabIndex        =   45
         Top             =   2490
         Width           =   795
      End
      Begin VB.Label ArrLbl 
         Caption         =   "Pos/Gate:"
         Height          =   225
         Index           =   7
         Left            =   3180
         TabIndex        =   36
         Top             =   2100
         Width           =   945
      End
      Begin VB.Label ArrLbl 
         Caption         =   "Dly.:"
         Height          =   225
         Index           =   6
         Left            =   120
         TabIndex        =   35
         Top             =   2790
         Width           =   555
      End
      Begin VB.Label ArrLbl 
         Caption         =   "EA:"
         Height          =   225
         Index           =   5
         Left            =   120
         TabIndex        =   34
         Top             =   2490
         Width           =   495
      End
      Begin VB.Label ArrLbl 
         Caption         =   "Act.:"
         Height          =   225
         Index           =   4
         Left            =   120
         TabIndex        =   33
         Top             =   1800
         Width           =   615
      End
      Begin VB.Label ArrLbl 
         Caption         =   "Plan:"
         Height          =   225
         Index           =   3
         Left            =   120
         TabIndex        =   32
         Top             =   1500
         Width           =   495
      End
      Begin VB.Label ArrLbl 
         Alignment       =   2  'Center
         Caption         =   "Via"
         Height          =   195
         Index           =   1
         Left            =   2460
         TabIndex        =   28
         Top             =   930
         Width           =   585
      End
      Begin VB.Label ArrLbl 
         Alignment       =   2  'Center
         Caption         =   "Destination"
         Height          =   195
         Index           =   2
         Left            =   4020
         TabIndex        =   23
         Top             =   930
         Width           =   975
      End
      Begin VB.Label ArrLbl 
         Alignment       =   2  'Center
         Caption         =   "Origin"
         Height          =   195
         Index           =   0
         Left            =   720
         TabIndex        =   18
         Top             =   930
         Width           =   585
      End
   End
   Begin VB.Frame AircraftData 
      Caption         =   " Aircraft "
      Height          =   615
      Left            =   30
      TabIndex        =   11
      Top             =   465
      Width           =   10425
      Begin VB.Frame AcrSign 
         BorderStyle     =   0  'None
         Enabled         =   0   'False
         Height          =   285
         Index           =   1
         Left            =   1005
         TabIndex        =   137
         Top             =   240
         Visible         =   0   'False
         Width           =   1125
         Begin VB.TextBox AcrFlag 
            Alignment       =   2  'Center
            BackColor       =   &H000000FF&
            ForeColor       =   &H00FFFFFF&
            Height          =   285
            Index           =   0
            Left            =   0
            TabIndex        =   138
            Text            =   "DATA LINK"
            Top             =   0
            Width           =   1125
         End
      End
      Begin VB.TextBox AcrFld 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   0
         Left            =   120
         TabIndex        =   12
         Text            =   "SXBKF"
         Top             =   240
         Width           =   855
      End
   End
   Begin VB.CheckBox MainButton 
      Caption         =   "Lock"
      Height          =   285
      Index           =   10
      Left            =   8715
      Style           =   1  'Graphical
      TabIndex        =   10
      Top             =   45
      Width           =   855
   End
   Begin VB.CheckBox MainButton 
      Caption         =   "Object"
      Height          =   285
      Index           =   9
      Left            =   7845
      Style           =   1  'Graphical
      TabIndex        =   9
      Top             =   45
      Width           =   855
   End
   Begin VB.CheckBox MainButton 
      Caption         =   "Call"
      Height          =   285
      Index           =   8
      Left            =   6975
      Style           =   1  'Graphical
      TabIndex        =   8
      Top             =   45
      Width           =   855
   End
   Begin VB.CheckBox MainButton 
      Caption         =   "Insert"
      Height          =   285
      Index           =   7
      Left            =   6105
      Style           =   1  'Graphical
      TabIndex        =   7
      Top             =   45
      Width           =   855
   End
   Begin VB.CheckBox MainButton 
      Caption         =   "Agents"
      Height          =   285
      Index           =   6
      Left            =   5235
      Style           =   1  'Graphical
      TabIndex        =   6
      Top             =   45
      Width           =   855
   End
   Begin VB.CheckBox MainButton 
      Caption         =   "Status"
      Height          =   285
      Index           =   5
      Left            =   4365
      Style           =   1  'Graphical
      TabIndex        =   5
      Top             =   45
      Width           =   855
   End
   Begin VB.CheckBox MainButton 
      Caption         =   "Rotation"
      Height          =   285
      Index           =   4
      Left            =   3495
      Style           =   1  'Graphical
      TabIndex        =   4
      Top             =   45
      Width           =   855
   End
   Begin VB.CheckBox MainButton 
      Caption         =   "Join"
      Height          =   285
      Index           =   3
      Left            =   2625
      Style           =   1  'Graphical
      TabIndex        =   3
      Top             =   45
      Width           =   855
   End
   Begin VB.CheckBox MainButton 
      Caption         =   "Split"
      Height          =   285
      Index           =   2
      Left            =   1755
      Style           =   1  'Graphical
      TabIndex        =   2
      Top             =   45
      Width           =   855
   End
   Begin VB.CheckBox MainButton 
      Caption         =   "Close"
      Height          =   285
      Index           =   1
      Left            =   885
      Style           =   1  'Graphical
      TabIndex        =   1
      Top             =   45
      Width           =   855
   End
   Begin VB.CheckBox MainButton 
      Caption         =   "Save"
      Height          =   285
      Index           =   0
      Left            =   15
      Style           =   1  'Graphical
      TabIndex        =   0
      Top             =   45
      Width           =   855
   End
   Begin VB.TextBox txtDummy 
      Appearance      =   0  'Flat
      Height          =   315
      Left            =   180
      TabIndex        =   133
      Top             =   660
      Width           =   255
   End
   Begin VB.Line Line3 
      BorderColor     =   &H80000014&
      X1              =   0
      X2              =   12600
      Y1              =   375
      Y2              =   375
   End
   Begin VB.Line Line2 
      BorderColor     =   &H80000014&
      X1              =   -15
      X2              =   12600
      Y1              =   0
      Y2              =   0
   End
   Begin VB.Line Line1 
      BorderColor     =   &H80000015&
      X1              =   -30
      X2              =   12600
      Y1              =   360
      Y2              =   360
   End
End
Attribute VB_Name = "FlightRotation"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private MyParent As Form
Private MyCallButton As Control
Private CurTelexRecord As String
Private CurFlightRecord As String
Private LastInput As String
Private FormIsUp As Boolean

Public Sub RegisterMyParent(Caller As Form, CallButton As Control, TaskValue As String)
    Set MyParent = Caller
    Set MyCallButton = CallButton
    CurTelexRecord = TaskValue
End Sub
Private Sub InitMyForm()
    ClearAllFields AcrFld
    ClearAllFields ArrDay
    ClearAllFields DepDay
    ClearAllFields ArrFld
    ClearAllFields DepFld
    ClearAllFields RemFld
    ClearAllFields ElseFld
End Sub
Private Sub ClearAllFields(ActFldArray As Variant)
Dim ActFld As Control
    For Each ActFld In ActFldArray
        ActFld.Text = ""
    Next
End Sub

Private Sub ArrButton_Click(Index As Integer)
    txtDummy.SetFocus

End Sub

Private Sub chkAssign_Click(Index As Integer)
    'txtDummy.SetFocus
    If chkAssign(Index).Value = 1 Then
        ShowChildForm Me, chkAssign(0), AssignFlight, ""
    Else
        Unload AssignFlight
    End If
End Sub

Private Sub DepButton_Click(Index As Integer)
    txtDummy.SetFocus

End Sub

Private Sub Form_Activate()
    With MyParent
        OnTop.Value = .OnTop.Value
    End With
    If Not FormIsUp Then
        MainButton(1).SetFocus
        FormIsUp = True
    End If
End Sub

Private Sub Form_Load()
    FormIsUp = False
    With MyParent
        Left = .Left + 50 * Screen.TwipsPerPixelX
        Top = .Top + 50 * Screen.TwipsPerPixelY
    End With
    InitMyForm
    Show
    Refresh
End Sub

Private Sub Form_Unload(Cancel As Integer)
    MyCallButton.Value = 0
End Sub

Private Sub OnTop_Click()
    MyParent.OnTop.Value = OnTop.Value
    Me.SetFocus
    txtDummy.SetFocus
End Sub

Private Sub MainButton_Click(Index As Integer)
    txtDummy.SetFocus
    Select Case Index
        Case 1  'Close
            Unload Me
    End Select
End Sub

