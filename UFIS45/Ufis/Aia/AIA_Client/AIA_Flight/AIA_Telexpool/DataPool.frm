VERSION 5.00
Object = "{64E8E382-05E2-11D2-9B1D-9B0630BC8F12}#1.0#0"; "tab.ocx"
Begin VB.Form DataPool 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "DataPool"
   ClientHeight    =   6345
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   7665
   ControlBox      =   0   'False
   Icon            =   "DataPool.frx":0000
   LinkTopic       =   "Form4"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6345
   ScaleWidth      =   7665
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton btnClose 
      Caption         =   "Close"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   3480
      TabIndex        =   4
      Top             =   6030
      Width           =   855
   End
   Begin TABLib.TAB TtypIdx 
      Height          =   1365
      Index           =   0
      Left            =   3930
      TabIndex        =   3
      ToolTipText     =   "Main DataPool Index Storage (0)"
      Top             =   390
      Width           =   3675
      _Version        =   65536
      _ExtentX        =   6482
      _ExtentY        =   2408
      _StockProps     =   64
   End
   Begin VB.TextBox TableName 
      Alignment       =   2  'Center
      Height          =   285
      Left            =   60
      TabIndex        =   2
      ToolTipText     =   "Full Table Name"
      Top             =   60
      Width           =   765
   End
   Begin VB.TextBox TlxTabFields 
      Height          =   285
      Left            =   900
      TabIndex        =   1
      Tag             =   "32,32,32,32,32,32,32,32,32,32,32,32,32"
      ToolTipText     =   "List of TableFields (read from SYSTAB)"
      Top             =   60
      Width           =   6705
   End
   Begin TABLib.TAB TelexData 
      Height          =   1365
      Index           =   0
      Left            =   60
      TabIndex        =   0
      ToolTipText     =   "Main DataPool (0). Used as filtered Telex Pool 1 (P1)"
      Top             =   390
      Width           =   3825
      _Version        =   65536
      _ExtentX        =   6747
      _ExtentY        =   2408
      _StockProps     =   64
   End
   Begin TABLib.TAB TtypIdx 
      Height          =   1365
      Index           =   1
      Left            =   3930
      TabIndex        =   5
      ToolTipText     =   "Main DataPool Index Storage (1)"
      Top             =   1800
      Width           =   3675
      _Version        =   65536
      _ExtentX        =   6482
      _ExtentY        =   2408
      _StockProps     =   64
   End
   Begin TABLib.TAB TelexData 
      Height          =   1365
      Index           =   1
      Left            =   60
      TabIndex        =   6
      ToolTipText     =   "Main DataPool (1). Used as filtered Telex Pool 2 (P2)"
      Top             =   1800
      Width           =   3825
      _Version        =   65536
      _ExtentX        =   6747
      _ExtentY        =   2408
      _StockProps     =   64
   End
   Begin TABLib.TAB TtypIdx 
      Height          =   1365
      Index           =   2
      Left            =   3930
      TabIndex        =   7
      ToolTipText     =   "Main DataPool Index Storage (2)"
      Top             =   3210
      Width           =   3675
      _Version        =   65536
      _ExtentX        =   6482
      _ExtentY        =   2408
      _StockProps     =   64
   End
   Begin TABLib.TAB TelexData 
      Height          =   1365
      Index           =   2
      Left            =   60
      TabIndex        =   8
      ToolTipText     =   "Main DataPool (2). Used for online received telexes (PR-Pool Received)"
      Top             =   3210
      Width           =   3825
      _Version        =   65536
      _ExtentX        =   6747
      _ExtentY        =   2408
      _StockProps     =   64
   End
   Begin TABLib.TAB TtypIdx 
      Height          =   1365
      Index           =   3
      Left            =   3930
      TabIndex        =   9
      ToolTipText     =   "Main DataPool Index Storage (3)"
      Top             =   4620
      Width           =   3675
      _Version        =   65536
      _ExtentX        =   6482
      _ExtentY        =   2408
      _StockProps     =   64
   End
   Begin TABLib.TAB TelexData 
      Height          =   1365
      Index           =   3
      Left            =   60
      TabIndex        =   10
      ToolTipText     =   "Main DataPool (3). Used for online telexes to be send (PS-Pool Send)"
      Top             =   4620
      Width           =   3825
      _Version        =   65536
      _ExtentX        =   6747
      _ExtentY        =   2408
      _StockProps     =   64
   End
End
Attribute VB_Name = "DataPool"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public Sub InitDataPool()
    Dim clData As String
    Dim clRecDat As String
    Dim clFldLst As String
    Dim count As Integer
    Dim retStr As String
    Dim i As Integer
    clFldLst = ""
    TlxTabFields.Text = "No,Fields,Found,In,SYS,TAB,,,"
    count = -1
    If CedaIsConnected = True Then
        retStr = UfisServer.aCeda.CallServer("RT", "SYSTAB", "FINA", clData, "WHERE TANA='TLX' ORDER BY FINA", "240")
        count = UfisServer.aCeda.GetBufferCount - 1
    End If
    For i = 0 To count
        clRecDat = UfisServer.aCeda.GetBufferLine(i)
        If (Trim(clRecDat) <> "") Then
            clData = GetItem(clRecDat, 1, ",")
            clFldLst = clFldLst & clData & ","
        End If
    Next i
    If clFldLst <> "" Then
        If InStr(clFldLst, "STYP") = 0 Then clFldLst = clFldLst & "STYP,"
        If InStr(clFldLst, "WSTA") = 0 Then clFldLst = clFldLst & "WSTA,"
        If InStr(clFldLst, "FLDA") = 0 Then clFldLst = clFldLst & "FLDA,"
        TlxTabFields.Text = Left(clFldLst, Len(clFldLst) - 1)
    End If
    TableName.Text = "TLX" & UfisServer.TblExt.Text
    For i = 0 To TelexData.count - 1
        TelexData(i).ResetContent
        TelexData(i).FontName = "Courier"
        TelexData(i).HeaderFontSize = 14
        TelexData(i).FontSize = 14
        TelexData(i).LineHeight = 15
        TelexData(i).HeaderString = TlxTabFields.Text
    Next i
    For i = 0 To TtypIdx.count - 1
        TtypIdx(i).ResetContent
        TtypIdx(i).FontName = "Courier"
        TtypIdx(i).HeaderFontSize = 14
        TtypIdx(i).FontSize = 14
        TtypIdx(i).LineHeight = 15
        TtypIdx(i).HeaderString = "TTYP,Index List of Data Pool"
        TtypIdx(i).HeaderLengthString = "30,800"
    Next i
   
End Sub
Private Sub InitCedaConfig()
Dim retStr As String
Dim clData As String
Dim clRecDat As String
Dim count As Integer
Dim i As Integer
    clData = ""
    count = -1
    If CedaIsConnected = True Then
        retStr = UfisServer.aCeda.CallServer("GFRC", "", "", clData, "WHERE [CONFIG]", "240")
        count = UfisServer.aCeda.GetBufferCount - 1
    End If
    For i = 0 To count
        clRecDat = UfisServer.aCeda.GetBufferLine(i)
        If (Trim(clRecDat) <> "") Then
            'MsgBox clRecDat
            'clData = GetItem(clRecDat, 1, ",")
        End If
    Next i
End Sub
Private Sub btnClose_Click()
    Me.Hide
End Sub
Private Sub Form_Load()
    InitDataPool
    InitCedaConfig
End Sub
Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    If UnloadMode = 0 Or UnloadMode = 5 Then
        Cancel = CheckCancelExit(Me, 3)
    End If
End Sub
