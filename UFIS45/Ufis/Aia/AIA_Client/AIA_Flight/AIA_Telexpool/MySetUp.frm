VERSION 5.00
Begin VB.Form MySetUp 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Setup"
   ClientHeight    =   8310
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   7155
   Icon            =   "MySetUp.frx":0000
   LinkTopic       =   "Form3"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   8310
   ScaleWidth      =   7155
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin VB.Timer Timer1 
      Enabled         =   0   'False
      Interval        =   60000
      Left            =   2850
      Top             =   0
   End
   Begin VB.CheckBox OnTop 
      Caption         =   "On Top"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   6120
      Style           =   1  'Graphical
      TabIndex        =   2
      Top             =   1560
      Width           =   855
   End
   Begin VB.CommandButton Reset 
      Caption         =   "Reset"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   6120
      TabIndex        =   1
      Top             =   1860
      Width           =   855
   End
   Begin VB.CommandButton btnClose 
      Cancel          =   -1  'True
      Caption         =   "Close"
      Default         =   -1  'True
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   6120
      TabIndex        =   0
      Top             =   2160
      Width           =   855
   End
   Begin VB.Frame SetupGroup 
      Caption         =   "System"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2475
      Index           =   0
      Left            =   60
      TabIndex        =   3
      Top             =   90
      Visible         =   0   'False
      Width           =   7035
      Begin VB.TextBox sndTlxTxtDefault 
         Height          =   1815
         Left            =   120
         MultiLine       =   -1  'True
         TabIndex        =   7
         ToolTipText     =   "Default Text of (send) Telexes"
         Top             =   540
         Width           =   2565
      End
      Begin VB.TextBox HomeTlxAddr 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   120
         TabIndex        =   6
         Text            =   "WWWWWWW"
         ToolTipText     =   "Own SITA Type B Address"
         Top             =   240
         Width           =   2565
      End
      Begin VB.TextBox OrderKey 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   2760
         TabIndex        =   5
         ToolTipText     =   "SQL ORDER BY ..."
         Top             =   240
         Width           =   2805
      End
      Begin VB.TextBox DefDateFormat 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   5640
         TabIndex        =   4
         ToolTipText     =   "Used Date Format"
         Top             =   240
         Width           =   1260
      End
   End
   Begin VB.Frame SetupGroup 
      Caption         =   "Loader"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2475
      Index           =   1
      Left            =   60
      TabIndex        =   8
      Top             =   2640
      Visible         =   0   'False
      Width           =   7035
      Begin VB.TextBox MyUtcTimeDiff 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   5130
         Locked          =   -1  'True
         TabIndex        =   20
         Text            =   "60"
         ToolTipText     =   "UTC Time Difference"
         Top             =   330
         Width           =   660
      End
      Begin VB.TextBox ServerTimeType 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   4560
         Locked          =   -1  'True
         TabIndex        =   19
         Text            =   "LOC"
         Top             =   330
         Width           =   525
      End
      Begin VB.TextBox OnlineFrom 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   2430
         TabIndex        =   15
         Text            =   "-2"
         Top             =   1020
         Width           =   495
      End
      Begin VB.TextBox OnlineTo 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   2970
         TabIndex        =   14
         Text            =   "+1"
         Top             =   1020
         Width           =   525
      End
      Begin VB.TextBox ServerTime 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   2430
         Locked          =   -1  'True
         TabIndex        =   11
         Tag             =   "200205240800"
         Text            =   "24.05.2002 / 08:00"
         Top             =   330
         Width           =   2070
      End
      Begin VB.TextBox MainTo 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   2970
         TabIndex        =   10
         Text            =   "+1"
         Top             =   690
         Width           =   525
      End
      Begin VB.TextBox MainFrom 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   2430
         TabIndex        =   9
         Text            =   "-2"
         Top             =   690
         Width           =   495
      End
      Begin VB.Label Label5 
         Caption         =   "Server Reference Time"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   150
         TabIndex        =   18
         Top             =   390
         Width           =   2085
      End
      Begin VB.Label Label4 
         Caption         =   "Hours"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   3540
         TabIndex        =   17
         Top             =   1080
         Width           =   555
      End
      Begin VB.Label Label3 
         Caption         =   "Hours"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   3540
         TabIndex        =   16
         Top             =   750
         Width           =   555
      End
      Begin VB.Label Label2 
         Caption         =   "Online Windows Refresh"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   150
         TabIndex        =   13
         Top             =   1080
         Width           =   2145
      End
      Begin VB.Label Label1 
         Caption         =   "Main Data Loader"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   150
         TabIndex        =   12
         Top             =   750
         Width           =   2085
      End
   End
   Begin VB.Frame SetupGroup 
      Caption         =   "Telex Types"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2475
      Index           =   2
      Left            =   60
      TabIndex        =   21
      Top             =   5190
      Visible         =   0   'False
      Width           =   7035
      Begin VB.Frame Frame2 
         Caption         =   "Sender"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   2085
         Left            =   4380
         TabIndex        =   47
         Top             =   270
         Width           =   1125
         Begin VB.CheckBox chkRecv 
            Caption         =   "Else"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Index           =   47
            Left            =   120
            TabIndex        =   55
            Top             =   1770
            Value           =   1  'Checked
            Width           =   765
         End
         Begin VB.CheckBox chkRecv 
            Caption         =   "AAD"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Index           =   41
            Left            =   120
            TabIndex        =   54
            Top             =   510
            Width           =   765
         End
         Begin VB.CheckBox chkRecv 
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Index           =   42
            Left            =   120
            TabIndex        =   53
            Top             =   720
            Width           =   765
         End
         Begin VB.CheckBox chkRecv 
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Index           =   43
            Left            =   120
            TabIndex        =   52
            Top             =   930
            Width           =   765
         End
         Begin VB.CheckBox chkRecv 
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Index           =   44
            Left            =   120
            TabIndex        =   51
            Top             =   1140
            Width           =   765
         End
         Begin VB.CheckBox chkRecv 
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Index           =   45
            Left            =   120
            TabIndex        =   50
            Top             =   1350
            Width           =   765
         End
         Begin VB.CheckBox chkRecv 
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Index           =   46
            Left            =   120
            TabIndex        =   49
            Top             =   1560
            Width           =   765
         End
         Begin VB.CheckBox chkRecv 
            Caption         =   "MVT"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Index           =   40
            Left            =   120
            TabIndex        =   48
            Top             =   300
            Value           =   1  'Checked
            Width           =   765
         End
      End
      Begin VB.Frame Frame1 
         Caption         =   "Receiver"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   2085
         Left            =   120
         TabIndex        =   22
         Top             =   270
         Width           =   4215
         Begin VB.CheckBox chkRecv 
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Index           =   31
            Left            =   2550
            TabIndex        =   71
            Top             =   1770
            Width           =   765
         End
         Begin VB.CheckBox chkRecv 
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Index           =   32
            Left            =   3360
            TabIndex        =   70
            Top             =   300
            Width           =   765
         End
         Begin VB.CheckBox chkRecv 
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Index           =   33
            Left            =   3360
            TabIndex        =   69
            Top             =   510
            Width           =   765
         End
         Begin VB.CheckBox chkRecv 
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Index           =   34
            Left            =   3360
            TabIndex        =   68
            Top             =   720
            Width           =   765
         End
         Begin VB.CheckBox chkRecv 
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Index           =   35
            Left            =   3360
            TabIndex        =   67
            Top             =   930
            Width           =   765
         End
         Begin VB.CheckBox chkRecv 
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Index           =   36
            Left            =   3360
            TabIndex        =   66
            Top             =   1140
            Width           =   765
         End
         Begin VB.CheckBox chkRecv 
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Index           =   37
            Left            =   3360
            TabIndex        =   65
            Top             =   1350
            Width           =   765
         End
         Begin VB.CheckBox chkRecv 
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Index           =   38
            Left            =   3360
            TabIndex        =   64
            Top             =   1560
            Width           =   765
         End
         Begin VB.CheckBox chkRecv 
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Index           =   30
            Left            =   2550
            TabIndex        =   63
            Top             =   1560
            Width           =   735
         End
         Begin VB.CheckBox chkRecv 
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Index           =   24
            Left            =   2550
            TabIndex        =   62
            Top             =   300
            Width           =   765
         End
         Begin VB.CheckBox chkRecv 
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Index           =   25
            Left            =   2550
            TabIndex        =   61
            Top             =   510
            Width           =   765
         End
         Begin VB.CheckBox chkRecv 
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Index           =   26
            Left            =   2550
            TabIndex        =   60
            Top             =   720
            Width           =   765
         End
         Begin VB.CheckBox chkRecv 
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Index           =   27
            Left            =   2550
            TabIndex        =   59
            Top             =   930
            Width           =   765
         End
         Begin VB.CheckBox chkRecv 
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Index           =   28
            Left            =   2550
            TabIndex        =   58
            Top             =   1140
            Width           =   765
         End
         Begin VB.CheckBox chkRecv 
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Index           =   29
            Left            =   2550
            TabIndex        =   57
            Top             =   1350
            Width           =   765
         End
         Begin VB.CheckBox chkRecv 
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Index           =   23
            Left            =   1740
            TabIndex        =   56
            Top             =   1770
            Width           =   765
         End
         Begin VB.CheckBox chkRecv 
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Index           =   22
            Left            =   1740
            TabIndex        =   46
            Top             =   1560
            Width           =   765
         End
         Begin VB.CheckBox chkRecv 
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Index           =   16
            Left            =   1740
            TabIndex        =   45
            Top             =   300
            Width           =   765
         End
         Begin VB.CheckBox chkRecv 
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Index           =   17
            Left            =   1740
            TabIndex        =   44
            Top             =   510
            Width           =   765
         End
         Begin VB.CheckBox chkRecv 
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Index           =   18
            Left            =   1740
            TabIndex        =   43
            Top             =   720
            Width           =   765
         End
         Begin VB.CheckBox chkRecv 
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Index           =   19
            Left            =   1740
            TabIndex        =   42
            Top             =   930
            Width           =   765
         End
         Begin VB.CheckBox chkRecv 
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Index           =   20
            Left            =   1740
            TabIndex        =   41
            Top             =   1140
            Width           =   765
         End
         Begin VB.CheckBox chkRecv 
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Index           =   21
            Left            =   1740
            TabIndex        =   40
            Top             =   1350
            Width           =   765
         End
         Begin VB.CheckBox chkRecv 
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Index           =   15
            Left            =   930
            TabIndex        =   39
            Top             =   1770
            Width           =   765
         End
         Begin VB.CheckBox chkRecv 
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Index           =   14
            Left            =   930
            TabIndex        =   38
            Top             =   1560
            Width           =   765
         End
         Begin VB.CheckBox chkRecv 
            Caption         =   "SMA"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Index           =   9
            Left            =   930
            TabIndex        =   37
            Top             =   510
            Value           =   1  'Checked
            Width           =   765
         End
         Begin VB.CheckBox chkRecv 
            Caption         =   "Else"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Index           =   39
            Left            =   3360
            TabIndex        =   36
            Top             =   1770
            Value           =   1  'Checked
            Width           =   765
         End
         Begin VB.CheckBox chkRecv 
            Caption         =   "AAD"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Index           =   10
            Left            =   930
            TabIndex        =   35
            Top             =   720
            Value           =   1  'Checked
            Width           =   765
         End
         Begin VB.CheckBox chkRecv 
            Caption         =   "RQM"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Index           =   11
            Left            =   930
            TabIndex        =   34
            Top             =   930
            Value           =   1  'Checked
            Width           =   765
         End
         Begin VB.CheckBox chkRecv 
            Caption         =   "BSM"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Index           =   12
            Left            =   930
            TabIndex        =   33
            Top             =   1140
            Value           =   1  'Checked
            Width           =   765
         End
         Begin VB.CheckBox chkRecv 
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Index           =   13
            Left            =   930
            TabIndex        =   32
            Top             =   1350
            Width           =   765
         End
         Begin VB.CheckBox chkRecv 
            Caption         =   "SCR"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Index           =   8
            Left            =   930
            TabIndex        =   31
            Top             =   300
            Value           =   1  'Checked
            Width           =   765
         End
         Begin VB.CheckBox chkRecv 
            Caption         =   "MVT"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Index           =   0
            Left            =   120
            TabIndex        =   30
            Top             =   300
            Value           =   1  'Checked
            Width           =   765
         End
         Begin VB.CheckBox chkRecv 
            Caption         =   "SRM"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Index           =   6
            Left            =   120
            TabIndex        =   29
            Top             =   1560
            Value           =   1  'Checked
            Width           =   765
         End
         Begin VB.CheckBox chkRecv 
            Caption         =   "SAM"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Index           =   5
            Left            =   120
            TabIndex        =   28
            Top             =   1350
            Value           =   1  'Checked
            Width           =   765
         End
         Begin VB.CheckBox chkRecv 
            Caption         =   "ATC"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Index           =   4
            Left            =   120
            TabIndex        =   27
            Top             =   1140
            Value           =   1  'Checked
            Width           =   765
         End
         Begin VB.CheckBox chkRecv 
            Caption         =   "PTM"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Index           =   3
            Left            =   120
            TabIndex        =   26
            Top             =   930
            Value           =   1  'Checked
            Width           =   765
         End
         Begin VB.CheckBox chkRecv 
            Caption         =   "CPM"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Index           =   2
            Left            =   120
            TabIndex        =   25
            Top             =   720
            Value           =   1  'Checked
            Width           =   765
         End
         Begin VB.CheckBox chkRecv 
            Caption         =   "LDM"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Index           =   1
            Left            =   120
            TabIndex        =   24
            Top             =   510
            Value           =   1  'Checked
            Width           =   765
         End
         Begin VB.CheckBox chkRecv 
            Caption         =   "SLC"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Index           =   7
            Left            =   120
            TabIndex        =   23
            Top             =   1770
            Value           =   1  'Checked
            Width           =   765
         End
      End
   End
End
Attribute VB_Name = "MySetUp"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private MyParent As Form
Public Sub RegisterMyParent(Caller As Form, CallButton As Control, TaskValue As String)
    Set MyParent = Caller
    With MyParent
        OnTop.Value = .OnTop.Value
    End With
End Sub

Private Sub btnClose_Click()
    Me.Hide
End Sub

Private Sub Reset_Click()
    Dim UseIdx As Integer
    On Error Resume Next
    With MyParent
        OnTop.Value = .OnTop.Value
    End With
    If SetupGroup(0).Tag <> "" Then
        UseIdx = Val(SetupGroup(0).Tag)
        InitSetupConfig UseIdx
    End If
End Sub

Private Sub Form_Load()
    Me.Height = 3000
    Me.Width = 7260
    'Left = .Left + 50 * Screen.TwipsPerPixelX
    'Top = .Top + 50 * Screen.TwipsPerPixelY
    InitSetupConfig 0
    InitSetupConfig 1
    InitSetupConfig 2
End Sub

Private Sub InitSetupConfig(GroupNo As Integer)
    Dim X As String
    Dim clTxtDef As String
    Dim CliSec As Long
    Dim SrvSec As Long
    Select Case GroupNo
        Case 0
            HomeTlxAddr.Text = GetIniEntry(myIniFullName, "MAIN_WINDOW", "", "HOMETLXADDR", "")
            clTxtDef = "=PRIORITY" & vbNewLine & "QU" & vbNewLine & "=DESTINATION TYPE B" & vbNewLine & "STX," & vbNewLine & "=TEXT" & vbNewLine
            sndTlxTxtDefault.Text = GetIniEntry(myIniFullName, "MAIN_WINDOW", "", "DEFAULTTEXT", clTxtDef)
            OrderKey.Text = GetIniEntry(myIniFullName, "MAIN_WINDOW", "", "SQL_ORDER", "CDAT")
            DefDateFormat.Text = GetIniEntry(myIniFullName, "MAIN_WINDOW", "", "DATE_FORMAT", "dd.mm.yyyy")
        Case 1
            MainFrom.Text = GetIniEntry(myIniFullName, "MAIN_WINDOW", "", "MAIN_PRIOR", "-4")
            MainTo.Text = GetIniEntry(myIniFullName, "MAIN_WINDOW", "", "MAIN_AHEAD", "+1")
            OnlineFrom.Text = GetIniEntry(myIniFullName, "ONLINE_WINDOWS", "", "ONLINE_PRIOR", "-1")
            OnlineTo.Text = GetIniEntry(myIniFullName, "ONLINE_WINDOWS", "", "ONLINE_AHEAD", "+1")
            'X = Format(Now, "yyyymmddhhmmss")
            'CliSec = Val(Right(X, 2))
            X = UfisServer.GetServerConfigItem("SRVT", 1, Format(Now, "yyyymmddhhmmss"))
            SrvSec = Val(Right(X, 2))
            Mid(X, 13, 2) = "00"
            ServerTime.Tag = X
            ServerTimeType.Text = UfisServer.GetServerConfigItem("SRVT", 2, "LOC")
            ServerTime.Text = Format(CedaFullDateToVb(ServerTime.Tag), (DefDateFormat & " / hh:mm"))
            MyUtcTimeDiff.Text = Trim(Str(UtcTimeDiff))
            CliSec = (60 - SrvSec) * 1000
            If CliSec <= 0 Then CliSec = 60000
            Timer1.Interval = CliSec
            Timer1.Enabled = True
        Case 2
            InitOnlineFilter
        Case Else
    End Select
End Sub
Private Sub InitOnlineFilter()
    Dim CodeList As String
    Dim TypeCode As String
    Dim i As Integer
    Dim Itm As Integer
    CodeList = GetIniEntry(myIniFullName, "ONLINE_WINDOWS", "", "RECV_TYPES", "")
    If CodeList <> "" Then
        Itm = 0
        For i = 0 To 38
            chkRecv(i).Caption = ""
            chkRecv(i).Enabled = False
            chkRecv(i).Value = 0
            Itm = Itm + 1
            TypeCode = Trim(GetItem(CodeList, Itm, ","))
            If TypeCode <> "" Then
                chkRecv(i).Caption = TypeCode
                chkRecv(i).Enabled = True
                chkRecv(i).Value = 1
            End If
        Next
        chkRecv(39).Caption = "Else"
        chkRecv(39).Enabled = True
        chkRecv(39).Value = 1
    End If
    CodeList = GetIniEntry(myIniFullName, "ONLINE_WINDOWS", "", "SEND_TYPES", "")
    If CodeList <> "" Then
        Itm = 0
        For i = 40 To 46
            chkRecv(i).Caption = ""
            chkRecv(i).Enabled = False
            chkRecv(i).Value = 0
            Itm = Itm + 1
            TypeCode = Trim(GetItem(CodeList, Itm, ","))
            If TypeCode <> "" Then
                chkRecv(i).Caption = TypeCode
                chkRecv(i).Enabled = True
                chkRecv(i).Value = 1
            End If
        Next
        chkRecv(47).Caption = "Else"
        chkRecv(47).Enabled = True
        chkRecv(47).Value = 1
    End If
    CodeList = GetIniEntry(myIniFullName, "ONLINE_WINDOWS", "", "ONLINE_RECV", "")
    If CodeList <> "" Then
        For i = 0 To 39
            chkRecv(i).Value = 0
        Next
        TypeCode = "START"
        Itm = 0
        While TypeCode <> ""
            Itm = Itm + 1
            TypeCode = Trim(GetItem(CodeList, Itm, ","))
            If TypeCode <> "" Then
                For i = 0 To 39
                    If chkRecv(i).Caption = TypeCode Then
                        chkRecv(i).Value = 1
                        Exit For
                    End If
                Next
            End If
        Wend
    End If
    CodeList = GetIniEntry(myIniFullName, "ONLINE_WINDOWS", "", "ONLINE_SEND", "")
    If CodeList <> "" Then
        For i = 40 To 47
            chkRecv(i).Value = 0
        Next
        TypeCode = "START"
        Itm = 0
        While TypeCode <> ""
            Itm = Itm + 1
            TypeCode = Trim(GetItem(CodeList, Itm, ","))
            If TypeCode <> "" Then
                For i = 40 To 47
                    If chkRecv(i).Caption = TypeCode Then
                        chkRecv(i).Value = 1
                        Exit For
                    End If
                Next
            End If
        Wend
    End If
End Sub
Public Sub ShowSetupGroup(GroupNo As Integer, GroupCaption As String)
    SetupGroup(0).Visible = False
    SetupGroup(1).Visible = False
    SetupGroup(GroupNo).Caption = GroupCaption
    SetupGroup(GroupNo).Top = 90
    SetupGroup(GroupNo).Left = 60
    SetupGroup(GroupNo).Visible = True
    SetupGroup(0).Tag = Str(GroupNo)
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    If UnloadMode = 0 Then
        Cancel = CheckCancelExit(Me, 3)
    End If
End Sub

Private Sub Timer1_Timer()
    Dim CurTime
    CurTime = CedaFullDateToVb(ServerTime.Tag)
    CurTime = DateAdd("n", 1, CurTime)
    ServerTime.Text = Format(CurTime, (DefDateFormat & " / hh:mm"))
    ServerTime.Tag = Format(CurTime, "yyyymmddhhmmss")
    TelexPoolHead.Caption = "Telex Pool " & ServerTime.Text & " " & ServerTimeType
    If Timer1.Interval < 60000 Then Timer1.Interval = 60000
End Sub

Public Function CheckOnlineFilter(Task As String, CheckCode As String) As String
    Dim Result As String
    Dim ActList As String
    Dim NotList As String
    Dim FromIdx As Integer
    Dim ToIdx As Integer
    Dim CurIdx As Integer
    Dim ElseIdx As Integer
    Dim LookOnly As Boolean
    Dim FoundIdx As Integer
    Result = ""
    ActList = ""
    NotList = ""
    Select Case Task
        Case "LOOKUP_RCV", "RCV_FILTER"
            FromIdx = 0
            ToIdx = 38
            ElseIdx = 39
            If Task = "LOOKUP_RCV" Then LookOnly = True Else LookOnly = False
        Case "LOOKUP_SND", "SND_FILTER"
            FromIdx = 40
            ToIdx = 46
            ElseIdx = 47
            If Task = "LOOKUP_SND" Then LookOnly = True Else LookOnly = False
        Case Else
    End Select
    FoundIdx = -1
    For CurIdx = FromIdx To ToIdx
        If chkRecv(CurIdx).Enabled = True Then
            If LookOnly Then
                If chkRecv(CurIdx).Caption = CheckCode Then
                    FoundIdx = CurIdx
                    Exit For
                End If
            Else
                If chkRecv(CurIdx).Value = 1 Then
                    ActList = ActList & ",'" & chkRecv(CurIdx).Caption & "'"
                Else
                    NotList = NotList & ",'" & chkRecv(CurIdx).Caption & "'"
                End If
            End If
        End If
    Next
    If LookOnly Then
        If FoundIdx >= 0 Then
            If chkRecv(FoundIdx).Value = 1 Then Result = "OK" Else Result = "NOK"
        Else
            If chkRecv(ElseIdx).Value = 1 Then Result = "OK" Else Result = "NOK"
        End If
    Else
        ActList = Mid(ActList, 2)
        NotList = Mid(NotList, 2)
        If ActList <> "" Then Result = "TTYP IN (" & ActList & ")"
        If NotList <> "" Then
            If chkRecv(ElseIdx).Value = 1 Then
                If ActList <> "" Then Result = Result & " OR "
                Result = Result & "TTYP NOT IN (" & NotList & ")"
                If ActList <> "" Then Result = "(" & Result & ")"
            End If
        End If
    End If
    CheckOnlineFilter = Result
End Function
