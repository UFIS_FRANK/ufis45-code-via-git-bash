VERSION 5.00
Begin VB.Form PoolConfig 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Telex Pool Configuration"
   ClientHeight    =   6060
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   4725
   Icon            =   "PoolConfig.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6060
   ScaleWidth      =   4725
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin VB.Frame DataPoolConfig 
      Caption         =   "Data Pool"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1365
      Left            =   2010
      TabIndex        =   23
      Top             =   90
      Width           =   1755
      Begin VB.CheckBox DataPoolCfg 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   2
         Left            =   780
         Style           =   1  'Graphical
         TabIndex        =   42
         Top             =   960
         Width           =   855
      End
      Begin VB.CheckBox DataPoolCfg 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   1
         Left            =   780
         Style           =   1  'Graphical
         TabIndex        =   41
         Top             =   660
         Width           =   855
      End
      Begin VB.PictureBox Picture6 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BorderStyle     =   0  'None
         ForeColor       =   &H80000008&
         Height          =   480
         Left            =   150
         Picture         =   "PoolConfig.frx":030A
         ScaleHeight     =   480
         ScaleWidth      =   480
         TabIndex        =   25
         TabStop         =   0   'False
         Top             =   270
         Width           =   480
      End
      Begin VB.CheckBox DataPoolCfg 
         Caption         =   "Memory"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   0
         Left            =   780
         Style           =   1  'Graphical
         TabIndex        =   24
         Top             =   360
         Width           =   855
      End
   End
   Begin VB.Frame FipsLinkConfig 
      Caption         =   "FIPS Module"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1365
      Left            =   2010
      TabIndex        =   20
      Top             =   1590
      Width           =   1755
      Begin VB.CheckBox FipsLinkCfg 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   2
         Left            =   780
         Style           =   1  'Graphical
         TabIndex        =   44
         Top             =   960
         Width           =   855
      End
      Begin VB.CheckBox FipsLinkCfg 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   1
         Left            =   780
         Style           =   1  'Graphical
         TabIndex        =   43
         Top             =   660
         Width           =   855
      End
      Begin VB.CheckBox FipsLinkCfg 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   0
         Left            =   780
         Style           =   1  'Graphical
         TabIndex        =   22
         Top             =   360
         Width           =   855
      End
      Begin VB.PictureBox Picture5 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BorderStyle     =   0  'None
         ForeColor       =   &H80000008&
         Height          =   480
         Left            =   150
         Picture         =   "PoolConfig.frx":0614
         ScaleHeight     =   480
         ScaleWidth      =   480
         TabIndex        =   21
         TabStop         =   0   'False
         Top             =   270
         Width           =   480
      End
   End
   Begin VB.Frame TlxGenConfig 
      Caption         =   "Telex Generator"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2865
      Left            =   2010
      TabIndex        =   14
      Top             =   3090
      Width           =   1755
      Begin VB.CheckBox TlxGenCfg 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   7
         Left            =   780
         Style           =   1  'Graphical
         TabIndex        =   32
         Top             =   2460
         Width           =   855
      End
      Begin VB.CheckBox TlxGenCfg 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   6
         Left            =   780
         Style           =   1  'Graphical
         TabIndex        =   31
         Top             =   2160
         Width           =   855
      End
      Begin VB.CheckBox TlxGenCfg 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   5
         Left            =   780
         Style           =   1  'Graphical
         TabIndex        =   30
         Top             =   1860
         Width           =   855
      End
      Begin VB.CheckBox TlxGenCfg 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   4
         Left            =   780
         Style           =   1  'Graphical
         TabIndex        =   29
         Top             =   1560
         Width           =   855
      End
      Begin VB.CheckBox TlxGenCfg 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   3
         Left            =   780
         Style           =   1  'Graphical
         TabIndex        =   28
         Top             =   1260
         Width           =   855
      End
      Begin VB.CheckBox TlxGenCfg 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   2
         Left            =   780
         Style           =   1  'Graphical
         TabIndex        =   27
         Top             =   960
         Width           =   855
      End
      Begin VB.CheckBox TlxGenCfg 
         Caption         =   "BRS"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   1
         Left            =   780
         Style           =   1  'Graphical
         TabIndex        =   26
         Top             =   660
         Width           =   855
      End
      Begin VB.PictureBox Picture3 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BorderStyle     =   0  'None
         ForeColor       =   &H80000008&
         Height          =   480
         Left            =   150
         Picture         =   "PoolConfig.frx":091E
         ScaleHeight     =   480
         ScaleWidth      =   480
         TabIndex        =   16
         TabStop         =   0   'False
         Top             =   270
         Width           =   480
      End
      Begin VB.CheckBox TlxGenCfg 
         Caption         =   "MVT"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   0
         Left            =   780
         Style           =   1  'Graphical
         TabIndex        =   15
         Top             =   360
         Width           =   855
      End
   End
   Begin VB.Frame EditorConfig 
      Caption         =   "Telex Editor"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1365
      Left            =   120
      TabIndex        =   11
      Top             =   4590
      Width           =   1755
      Begin VB.CheckBox TlxEditCfg 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   2
         Left            =   780
         Style           =   1  'Graphical
         TabIndex        =   38
         Top             =   960
         Width           =   855
      End
      Begin VB.CheckBox TlxEditCfg 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   1
         Left            =   780
         Style           =   1  'Graphical
         TabIndex        =   37
         Top             =   660
         Width           =   855
      End
      Begin VB.CheckBox TlxEditCfg 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   0
         Left            =   780
         Style           =   1  'Graphical
         TabIndex        =   13
         Top             =   360
         Width           =   855
      End
      Begin VB.PictureBox Picture2 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BorderStyle     =   0  'None
         ForeColor       =   &H80000008&
         Height          =   480
         Left            =   150
         Picture         =   "PoolConfig.frx":0C28
         ScaleHeight     =   480
         ScaleWidth      =   480
         TabIndex        =   12
         TabStop         =   0   'False
         Top             =   270
         Width           =   480
      End
   End
   Begin VB.Frame OnlineConfig 
      Caption         =   "Windows"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1365
      Left            =   120
      TabIndex        =   8
      Top             =   1590
      Width           =   1755
      Begin VB.CheckBox OnLineCfg 
         Caption         =   "Send"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   2
         Left            =   780
         Style           =   1  'Graphical
         TabIndex        =   36
         Top             =   960
         Width           =   855
      End
      Begin VB.CheckBox OnLineCfg 
         Caption         =   "Recv"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   1
         Left            =   780
         Style           =   1  'Graphical
         TabIndex        =   35
         Top             =   660
         Width           =   855
      End
      Begin VB.PictureBox Picture1 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BorderStyle     =   0  'None
         ForeColor       =   &H80000008&
         Height          =   480
         Left            =   150
         Picture         =   "PoolConfig.frx":0F32
         ScaleHeight     =   480
         ScaleWidth      =   480
         TabIndex        =   10
         TabStop         =   0   'False
         Top             =   270
         Width           =   480
      End
      Begin VB.CheckBox OnLineCfg 
         Caption         =   "Main"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   0
         Left            =   780
         Style           =   1  'Graphical
         TabIndex        =   9
         Top             =   360
         Width           =   855
      End
   End
   Begin VB.Frame MainConfig 
      Caption         =   "Main Module"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1365
      Left            =   120
      TabIndex        =   5
      Top             =   90
      Width           =   1755
      Begin VB.CheckBox MainCfg 
         Caption         =   "Monitor"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   2
         Left            =   780
         Style           =   1  'Graphical
         TabIndex        =   34
         Top             =   960
         Width           =   855
      End
      Begin VB.CheckBox MainCfg 
         Caption         =   "Loader"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   1
         Left            =   780
         Style           =   1  'Graphical
         TabIndex        =   33
         Top             =   660
         Width           =   855
      End
      Begin VB.CheckBox MainCfg 
         Caption         =   "System"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   0
         Left            =   780
         Style           =   1  'Graphical
         TabIndex        =   7
         Top             =   360
         Width           =   855
      End
      Begin VB.PictureBox IconBox 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BorderStyle     =   0  'None
         ForeColor       =   &H80000008&
         Height          =   480
         Left            =   150
         Picture         =   "PoolConfig.frx":123C
         ScaleHeight     =   480
         ScaleWidth      =   480
         TabIndex        =   6
         TabStop         =   0   'False
         Top             =   270
         Width           =   480
      End
   End
   Begin VB.CheckBox chkSave 
      Caption         =   "Save"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   3840
      Style           =   1  'Graphical
      TabIndex        =   4
      Top             =   5400
      Width           =   855
   End
   Begin VB.CheckBox FixWin 
      Caption         =   "Arrange"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   3840
      Style           =   1  'Graphical
      TabIndex        =   3
      Top             =   330
      Width           =   855
   End
   Begin VB.CheckBox chkReset 
      Caption         =   "Reset"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   3840
      Style           =   1  'Graphical
      TabIndex        =   2
      Top             =   5100
      Width           =   855
   End
   Begin VB.CheckBox OnTop 
      Caption         =   "On Top"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   3840
      Style           =   1  'Graphical
      TabIndex        =   1
      Top             =   30
      Width           =   855
   End
   Begin VB.CommandButton btnClose 
      Cancel          =   -1  'True
      Caption         =   "Close"
      Default         =   -1  'True
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   3840
      TabIndex        =   0
      Top             =   5685
      Width           =   855
   End
   Begin VB.Frame FltHdlConfig 
      Caption         =   "Online Receiver"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1365
      Left            =   120
      TabIndex        =   17
      Top             =   3090
      Width           =   1755
      Begin VB.CheckBox BcHdlCfg 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   2
         Left            =   780
         Style           =   1  'Graphical
         TabIndex        =   40
         Top             =   960
         Width           =   855
      End
      Begin VB.CheckBox BcHdlCfg 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   1
         Left            =   780
         Style           =   1  'Graphical
         TabIndex        =   39
         Top             =   660
         Width           =   855
      End
      Begin VB.CheckBox BcHdlCfg 
         Caption         =   "Filter"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   0
         Left            =   780
         Style           =   1  'Graphical
         TabIndex        =   19
         Top             =   360
         Width           =   855
      End
      Begin VB.PictureBox Picture4 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BorderStyle     =   0  'None
         ForeColor       =   &H80000008&
         Height          =   480
         Left            =   150
         Picture         =   "PoolConfig.frx":167E
         ScaleHeight     =   480
         ScaleWidth      =   480
         TabIndex        =   18
         TabStop         =   0   'False
         Top             =   270
         Width           =   480
      End
   End
End
Attribute VB_Name = "PoolConfig"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private MyParent As Variant
Private MyCallButton As Control

Public Sub RegisterMyParent(Caller As Form, CallButton As Control, TaskValue As String)
    Set MyParent = Caller
    Set MyCallButton = CallButton
End Sub

Private Sub BcHdlCfg_Click(Index As Integer)
    CheckFormOnTop Me, False
    If BcHdlCfg(Index).Value = 1 Then
        Select Case Index
            Case 0
                'BcHdlCfg(1).Value = 0
                MySetUp.ShowSetupGroup 2, "Online Windows Telex Type Filter"
                MySetUp.Show , Me
            Case 1
        End Select
    Else
        MySetUp.Hide
    End If
    CheckFormOnTop Me, True
End Sub

Private Sub btnClose_Click()
    Me.Hide
End Sub

Private Sub DataPoolCfg_Click(Index As Integer)
    CheckFormOnTop Me, False
    If DataPoolCfg(Index).Value = 1 Then
        Select Case Index
            Case 0: DataPool.Show , Me
        End Select
    Else
        Select Case Index
            Case 0: DataPool.Hide
        End Select
    End If
    CheckFormOnTop Me, True
End Sub

Private Sub Form_Activate()
    On Error Resume Next
    With MyParent
        OnTop.Value = .OnTop.Value
    End With
End Sub

Private Sub Form_Load()
    Dim X As String
    Dim clTxtDef As String
    On Error Resume Next
    With MyParent
        Left = .Left + 50 * Screen.TwipsPerPixelX
        Top = .Top + 50 * Screen.TwipsPerPixelY
    End With
    X = GetIniEntry(myIniFullName, "MAIN_WINDOW", "", "SHOW_MAIN", "YES")
    If X = "YES" Then OnLineCfg(0).Value = 1
    X = GetIniEntry(myIniFullName, "MAIN_WINDOW", "", "SHOW_RECV", "YES")
    If X = "YES" Then OnLineCfg(1).Value = 1
    X = GetIniEntry(myIniFullName, "MAIN_WINDOW", "", "SHOW_SEND", "NO")
    If X = "YES" Then OnLineCfg(2).Value = 1
    If OnLineCfg(1).Value = 0 And OnLineCfg(2).Value = 0 Then OnLineCfg(0).Value = 1
End Sub

Private Sub Form_Unload(Cancel As Integer)
    On Error Resume Next
    MyCallButton.Value = 0
End Sub

Private Sub MainCfg_Click(Index As Integer)
    CheckFormOnTop Me, False
    If MainCfg(Index).Value = 1 Then
        Select Case Index
            Case 0
                MainCfg(1).Value = 0
                MySetUp.ShowSetupGroup 0, "General System Configuration"
                MySetUp.Show , Me
            Case 1
                MainCfg(0).Value = 0
                MySetUp.ShowSetupGroup 1, "Data Loader Configuration"
                MySetUp.Show , Me
        End Select
    Else
        MySetUp.Hide
    End If
    CheckFormOnTop Me, True
End Sub

Private Sub OnLineCfg_Click(Index As Integer)
    If OnLineCfg(Index).Value = 1 Then
        OnLineCfg(Index).BackColor = LightGreen
    Else
        OnLineCfg(Index).BackColor = vbButtonFace
    End If
End Sub

Private Sub OnTop_Click()
    MyParent.OnTop.Value = OnTop.Value
    Me.SetFocus
    'txtDummy.SetFocus
End Sub

