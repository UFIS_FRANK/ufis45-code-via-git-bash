VERSION 5.00
Begin VB.Form PrintServer 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Form1"
   ClientHeight    =   1725
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   5595
   BeginProperty Font 
      Name            =   "Courier New"
      Size            =   9.75
      Charset         =   161
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1725
   ScaleWidth      =   5595
   StartUpPosition =   3  'Windows Default
End
Attribute VB_Name = "PrintServer"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub Form_Load()
    Top = 0
    Left = 0
    'PrntBorder.Top = 0
    'PrntBorder.Left = 0
    'MsgBox Printer.ScaleHeight
    'Width = Printer.ScaleWidth
    'Height = Printer.ScaleHeight
    'PrntBorder.Width = ScaleWidth - 2 * Screen.TwipsPerPixelX
    'PrntBorder.Height = ScaleHeight - 2 * Screen.TwipsPerPixelY
End Sub

Private Sub Form_Resize()
    'PrntBorder.Width = ScaleWidth - 2 * Screen.TwipsPerPixelX
    'PrntBorder.Height = ScaleHeight - 2 * Screen.TwipsPerPixelY
End Sub
Public Sub PrintOutput(PrntText As String)
    Dim txtLine() As String
    Dim MsgTxt As String
    Dim RetVal As Integer
    Dim i
    On Error GoTo ErrHandle
    txtLine = Split(PrntText, vbNewLine, -1, vbBinaryCompare)
    Printer.Orientation = vbPRORPortrait
    Printer.PaperSize = vbPRPSA4
    Printer.Font = "Courier New"
    Printer.FontSize = 10
    Printer.FontBold = True
    For i = 1 To 4
        Printer.Print Space(8)
    Next
    For i = 0 To UBound(txtLine)
        Printer.Print Space(8);
        Printer.Print txtLine(i)
    Next
    Printer.EndDoc
    Exit Sub
ErrHandle:
    MsgTxt = "Can't connect to printer."
    RetVal = MyMsgBox.CallAskUser(0, 0, 0, "UFIS Printer Control", MsgTxt, "netfail", "", UserAnswer)
End Sub
