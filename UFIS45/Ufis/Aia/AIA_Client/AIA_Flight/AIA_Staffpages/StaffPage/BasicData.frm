VERSION 5.00
Object = "{64E8E382-05E2-11D2-9B1D-9B0630BC8F12}#1.0#0"; "TAB.ocx"
Begin VB.Form BasicData 
   Caption         =   "Form1"
   ClientHeight    =   4335
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   8805
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   4335
   ScaleWidth      =   8805
   StartUpPosition =   3  'Windows Default
   Begin TABLib.TAB BasTab 
      Height          =   1215
      Index           =   0
      Left            =   60
      TabIndex        =   1
      Top             =   420
      Width           =   3225
      _Version        =   65536
      _ExtentX        =   5689
      _ExtentY        =   2143
      _StockProps     =   64
   End
   Begin VB.CommandButton cmdClose 
      Caption         =   "Close"
      Height          =   375
      Left            =   30
      TabIndex        =   0
      Top             =   0
      Width           =   1335
   End
   Begin TABLib.TAB BasTab 
      Height          =   1215
      Index           =   1
      Left            =   60
      TabIndex        =   2
      Top             =   1680
      Width           =   3225
      _Version        =   65536
      _ExtentX        =   5689
      _ExtentY        =   2143
      _StockProps     =   64
   End
   Begin TABLib.TAB BasTab 
      Height          =   1215
      Index           =   2
      Left            =   60
      TabIndex        =   3
      Top             =   2940
      Width           =   3225
      _Version        =   65536
      _ExtentX        =   5689
      _ExtentY        =   2143
      _StockProps     =   64
   End
   Begin TABLib.TAB CcaTab 
      Height          =   1215
      Index           =   0
      Left            =   3360
      TabIndex        =   4
      Top             =   420
      Width           =   3225
      _Version        =   65536
      _ExtentX        =   5689
      _ExtentY        =   2143
      _StockProps     =   64
   End
   Begin TABLib.TAB CcaTab 
      Height          =   1215
      Index           =   1
      Left            =   3360
      TabIndex        =   5
      Top             =   1680
      Width           =   3225
      _Version        =   65536
      _ExtentX        =   5689
      _ExtentY        =   2143
      _StockProps     =   64
   End
   Begin TABLib.TAB CcaTab 
      Height          =   1215
      Index           =   2
      Left            =   3360
      TabIndex        =   6
      Top             =   2940
      Width           =   3225
      _Version        =   65536
      _ExtentX        =   5689
      _ExtentY        =   2143
      _StockProps     =   64
   End
   Begin TABLib.TAB CicGrp 
      Height          =   2055
      Index           =   0
      Left            =   6660
      TabIndex        =   7
      Top             =   420
      Width           =   2025
      _Version        =   65536
      _ExtentX        =   3572
      _ExtentY        =   3625
      _StockProps     =   64
   End
   Begin TABLib.TAB CicGrp 
      Height          =   1635
      Index           =   1
      Left            =   6660
      TabIndex        =   8
      Top             =   2520
      Width           =   2025
      _Version        =   65536
      _ExtentX        =   3572
      _ExtentY        =   2884
      _StockProps     =   64
   End
End
Attribute VB_Name = "BasicData"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim MaxTab As TABLib.TAB
Dim MaxIdx As Integer
Dim LastCcaBcNum As Long
Dim CurCcaBcNum As Long

Public Sub InitBasics()
    Dim SqlTab As String
    Dim SqlFld As String
    Dim SqlKey As String
    Dim i As Integer
    Screen.MousePointer = 11
    SqlTab = "ALTTAB"
    SqlFld = "URNO,ALC2,ALC3,ALFN"
    SqlKey = ""
    ReadCedaData BasTab(0), SqlTab, SqlFld, SqlKey
    BasTab(0).IndexCreate "ALC2", 1
    BasTab(0).IndexCreate "ALC3", 2
    SqlTab = "APTTAB"
    SqlFld = "URNO,APC3,APC4,APFN"
    SqlKey = ""
    ReadCedaData BasTab(1), SqlTab, SqlFld, SqlKey
    BasTab(1).IndexCreate "APC3", 1
    BasTab(1).IndexCreate "APC4", 2
    SqlTab = "ACTTAB"
    SqlFld = "URNO,ACT3,ACT5,ACFN"
    SqlKey = ""
    ReadCedaData BasTab(2), SqlTab, SqlFld, SqlKey
    BasTab(2).IndexCreate "ACT3", 1
    BasTab(2).IndexCreate "ACT5", 2
    For i = 0 To 2
        CcaTab(i).ResetContent
    Next
    For i = 0 To 1
        CicGrp(i).ResetContent
        CicGrp(i).LogicalFieldList = "GRP,CKIF,CKIT,CTYP"
        CicGrp(i).HeaderString = "GRP,CKIF,CKIT,CTYP"
        CicGrp(i).HeaderLengthString = "10,10,10,10"
        CicGrp(i).ColumnWidthString = "3,5,5,1"
        CicGrp(i).AutoSizeByHeader = True
        CicGrp(i).ShowHorzScroller True
        CicGrp(i).AutoSizeColumns
    Next
    InitCicGroups
    Screen.MousePointer = 0
End Sub
Private Sub InitCicGroups()
    Dim CicSection As String
    Dim GrpList As String
    Dim GrpDesc As String
    Dim GrpName As String
    Dim GrpMemb As String
    Dim GrpCode As String
    Dim GrpLine As String
    Dim NewLine As String
    Dim GrpDflt As String
    Dim GrpNo As Integer
    Dim MaxMemNo As Integer
    Dim CurMemNo As Integer
    CicGrp(0).ResetContent
    CicSection = "CCATAB_CONFIG"
    GrpList = GetIniEntry(StaffPageIniFile, CicSection, "", "CIC_GROUPS", "")
    If GrpList <> "" Then
        GrpNo = 1
        GrpDesc = GetItem(GrpList, GrpNo, "|")
        While GrpDesc <> ""
            GrpName = GetItem(GrpDesc, 1, ",")
            GrpMemb = GetItem(GrpDesc, 2, ",")
            MaxMemNo = Val(GrpMemb)
            For CurMemNo = 1 To MaxMemNo
                GrpCode = GrpName & "_" & CStr(CurMemNo)
                GrpLine = GetIniEntry(StaffPageIniFile, CicSection, "", GrpCode, "")
                GrpLine = Replace(GrpLine, "-", ",", 1, -1, vbBinaryCompare)
                GrpLine = Replace(GrpLine, " ", "", 1, -1, vbBinaryCompare)
                NewLine = GrpName & "," & GrpLine
                CicGrp(0).InsertTextLine NewLine, False
            Next
            GrpNo = GrpNo + 1
            GrpDesc = GetItem(GrpList, GrpNo, "|")
        Wend
    Else
        CicGrp(0).InsertTextLine "MTB,001,054", False
        CicGrp(0).InsertTextLine "MTB,055,076", False
        CicGrp(0).InsertTextLine "MTB,079,106", False
        CicGrp(0).InsertTextLine "MTB,107,157", False
        CicGrp(0).InsertTextLine "ETB,E01,E14", False
        CicGrp(0).InsertTextLine "STB,S01,S08", False
    End If
    CicGrp(0).AutoSizeColumns
    CicGrp(0).RedrawTab
End Sub
Public Function LookUpData(Index As Integer, LookFld As String, LookVal As String, GetFldLst As String) As String
    Dim Result As String
    Dim UseCol As Long
    Dim UseIdx As String
    Dim tmpLen As Integer
    Dim HitList As String
    Dim LineNo As Long
    UseCol = -1
    Result = ""
    tmpLen = Len(LookVal)
    Select Case Index
        Case 0
            If tmpLen = 2 Then
                UseIdx = "ALC2"
                UseCol = 0
            End If
            If tmpLen = 3 Then
                UseIdx = "ALC3"
                UseCol = 0
            End If
        Case 1
            UseIdx = "APC3"
            UseCol = 0
        Case 2
            UseIdx = "ACT3"
            UseCol = 0
        Case Else
    End Select
    If UseCol >= 0 Then
        BasTab(Index).SetInternalLineBuffer True
        HitList = BasTab(Index).GetLinesByIndexValue(UseIdx, LookVal, 0)
        LineNo = Val(HitList)
        If LineNo > 0 Then
            HitList = BasTab(Index).GetNextResultLine
            LineNo = Val(HitList)
            Result = BasTab(Index).GetFieldValues(LineNo, GetFldLst)
        End If
        BasTab(Index).SetInternalLineBuffer False
    End If
    LookUpData = Result
End Function

Private Sub CcaTab_SendLButtonDblClick(Index As Integer, ByVal LineNo As Long, ByVal ColNo As Long)
    Dim tmpTag As String
    If LineNo < 0 Then
        tmpTag = CcaTab(Index).Tag
        If tmpTag = "" Then
            tmpTag = CStr(CcaTab(Index).Top) & "," & CStr(CcaTab(Index).Left) & "," & CStr(CcaTab(Index).Height) & "," & CStr(CcaTab(Index).Width)
            CcaTab(Index).Tag = tmpTag
            CcaTab(Index).Top = BasTab(0).Top
            CcaTab(Index).Left = BasTab(0).Left
            Set MaxTab = CcaTab(Index)
            MaxIdx = Index
            Form_Resize
        Else
            CcaTab(Index).Top = Val(GetItem(tmpTag, 1, ","))
            CcaTab(Index).Left = Val(GetItem(tmpTag, 2, ","))
            CcaTab(Index).Height = Val(GetItem(tmpTag, 3, ","))
            CcaTab(Index).Width = Val(GetItem(tmpTag, 4, ","))
            CcaTab(Index).Tag = ""
            MaxIdx = -1
        End If
        CcaTab(Index).ZOrder
    End If
End Sub

Private Sub cmdClose_Click()
    Me.Hide
End Sub

Private Sub Form_Load()
    MaxIdx = -1
    LastCcaBcNum = 0
    InitBasics
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    If UnloadMode = 0 Then
        Me.Hide
        Cancel = True
    End If
End Sub

Public Sub LoadCcaTab(CcaWhere As String, ReloadData As Boolean)
    Dim SqlTab As String
    Dim SqlFld As String
    Dim SqlKey As String
    Dim tmpData As String
    SqlTab = "CCATAB"
    SqlFld = "URNO,FLNU,CKIC,CTYP,CKBS,CKES,CKBA,CKEA,CKIT"
    If (ReloadData = True) Or (CcaTab(0).myTag = "") Then
        CcaTab(0).IndexDestroy "URNO"
        CcaTab(0).IndexDestroy "FLNU"
        SqlKey = "WHERE (" & CcaWhere & ") AND CTYP<>' '"
        ReadCedaData CcaTab(0), SqlTab, SqlFld, SqlKey
        CcaTab(0).Sort "1,2,4,5", True, True
        CcaTab(0).IndexCreate "URNO", 0
        CcaTab(0).IndexCreate "FLNU", 1
        If CcaTab(0).GetLineCount > 0 Then CcaTab(0).myTag = "OK" Else CcaTab(0).myTag = ""
    End If
    If (ReloadData = True) Or (CcaTab(1).myTag = "") Then
        CcaTab(1).IndexDestroy "URNO"
        CcaTab(1).IndexDestroy "FLNU"
        SqlKey = "WHERE (" & CcaWhere & ") AND CTYP=' '"
        ReadCedaData CcaTab(1), SqlTab, SqlFld, SqlKey
        CcaTab(1).Sort "1,2,4,5", True, True
        CcaTab(1).IndexCreate "URNO", 0
        CcaTab(1).IndexCreate "FLNU", 1
        If CcaTab(1).GetLineCount > 0 Then CcaTab(1).myTag = "OK" Else CcaTab(1).myTag = ""
    End If
    If CcaTab(2).myTag = "" Then
        CcaTab(2).LogicalFieldList = SqlFld
        CcaTab(2).HeaderString = SqlFld
        tmpData = CreateLengthList(SqlFld)
        CcaTab(2).HeaderLengthString = tmpData
        CcaTab(2).ColumnWidthString = tmpData
        CcaTab(2).AutoSizeByHeader = True
        CcaTab(2).ShowHorzScroller True
        CcaTab(2).myTag = "OK"
    End If
End Sub

Public Function LookUpCca(AftUrno As String, AftFlno As String, AftStod As String, AftCkif As String, AftCkit As String, ShowAll As Boolean) As Long
    Dim LineCnt As Long
    Dim HitList As String
    Dim LineNo As Long
    Dim AltUrno As String
    Dim AltCode As String
    Dim CcaRec As String
    Dim CcaCkbs As String
    Dim CcaCkes As String
    Dim CcaCkif As String
    Dim CcaCkit As String
    Dim tmpCkic As String
    Dim GrpName As String
    Dim GrpCkif As String
    Dim GrpCkit As String
    Dim tmpType As String
    Dim CcaTypf As String
    Dim CcaTypt As String
    Dim tmpLen As Integer
    Dim MtbGrpFound As Boolean
    Dim CcaMatch As Boolean
    Dim AftMatch As Boolean
    CcaTab(2).ResetContent
    CicGrp(1).ResetContent
    tmpLen = Len(AftStod)
    If (tmpLen > 0) And (tmpLen <> 14) Then AftStod = Left(AftStod & "00", 14)
    If AftFlno <> "" Then
        AltCode = Trim(Left(AftFlno, 3))
        AltUrno = BasicData.LookUpData(0, "", AltCode, "URNO")
        If AltUrno <> "" Then
            CcaTab(0).SetInternalLineBuffer True
            HitList = CcaTab(0).GetLinesByIndexValue("FLNU", AltUrno, 0)
            LineNo = Val(HitList)
            If LineNo > 0 Then
                While LineNo >= 0
                    LineNo = CcaTab(0).GetNextResultLine
                    If LineNo >= 0 Then
                        CcaMatch = True
                        CcaCkbs = CcaTab(0).GetColumnValue(LineNo, 4)
                        CcaCkes = CcaTab(0).GetColumnValue(LineNo, 5)
                        If (CcaCkes < AftStod) Or (CcaCkbs > AftStod) Then CcaMatch = False
                        If CcaMatch Then
                            CcaRec = CcaTab(0).GetLineValues(LineNo)
                            CcaTab(2).InsertTextLine CcaRec, False
                        End If
                    End If
                Wend
            End If
            CcaTab(0).SetInternalLineBuffer False
        End If
    End If
    CcaTab(1).SetInternalLineBuffer True
    HitList = CcaTab(1).GetLinesByIndexValue("FLNU", AftUrno, 0)
    LineNo = Val(HitList)
    If LineNo > 0 Then
        While LineNo >= 0
            LineNo = CcaTab(1).GetNextResultLine
            If LineNo >= 0 Then
                CcaRec = CcaTab(1).GetLineValues(LineNo)
                CcaTab(2).InsertTextLine CcaRec, False
            End If
        Wend
    End If
    CcaTab(1).SetInternalLineBuffer False
    CcaTab(2).Sort "2,4,5", True, True
    
    AftMatch = GetCicGroup(AftCkif, GrpCkif, GrpCkit, GrpName)
    If GrpName = "MTB" Then
        If (AftCkif >= GrpCkif) And (AftCkit <= GrpCkit) Then AftMatch = True Else AftMatch = False
    End If
    
    CcaCkif = ""
    CcaCkit = ""
    CcaTypf = ""
    CcaTypt = ""
    LineCnt = CcaTab(2).GetLineCount
    For LineNo = 0 To LineCnt
        tmpCkic = CcaTab(2).GetColumnValue(LineNo, 2)
        tmpType = CcaTab(2).GetColumnValue(LineNo, 3)
        If tmpCkic = "" Then tmpCkic = "ZZZZ"
        If tmpType = "" Then tmpType = "D"
        If CcaCkif = "" Then
            CcaMatch = GetCicGroup(tmpCkic, GrpCkif, GrpCkit, GrpName)
            If CcaMatch = True Then
                CcaCkif = tmpCkic
                CcaTypf = tmpType
            End If
        Else
            If tmpCkic > GrpCkit Then
                If CcaCkit = "" Then
                    CcaCkit = CcaCkif
                    CcaTypt = CcaTypf
                End If
                CcaMatch = True
                If Not ShowAll Then
                    If GrpName = "MTB" Then
                        If (AftCkif >= CcaCkif) And (AftCkit <= CcaCkit) Then MtbGrpFound = True
                        If (AftCkif >= GrpCkif) And (AftCkit <= GrpCkit) Then MtbGrpFound = True
                        If MtbGrpFound = True Then CcaMatch = False
                    End If
                    If (AftCkif >= GrpCkif) And (AftCkit <= GrpCkit) Then CcaMatch = False
                End If
                If CcaMatch = True Then
                    If CcaTypf <> CcaTypt Then CcaTypf = "M"
                    CcaRec = GrpName & "," & CcaCkif & "," & CcaCkit & ",(" & CcaTypf & ")"
                    CicGrp(1).InsertTextLine CcaRec, False
                End If
                CcaCkif = ""
                CcaCkit = ""
                CcaTypf = ""
                CcaTypt = ""
                CcaMatch = GetCicGroup(tmpCkic, GrpCkif, GrpCkit, GrpName)
                If CcaMatch = True Then
                    CcaCkif = tmpCkic
                    CcaTypf = tmpType
                End If
            Else
                CcaCkit = tmpCkic
                CcaTypt = tmpType
            End If
        End If
    Next
    If Not ShowAll Then
        LineCnt = CicGrp(1).GetLineCount
        If AftMatch = True Then MtbGrpFound = True
        If (LineCnt > 0) And (MtbGrpFound = True) Then
            LineNo = LineCnt - 1
            While LineNo >= 0
                GrpName = CicGrp(1).GetColumnValue(LineNo, 0)
                If GrpName = "MTB" Then CicGrp(1).DeleteLine LineNo
                LineNo = LineNo - 1
            Wend
        End If
    End If
    CcaTab(2).AutoSizeColumns
    CcaTab(2).RedrawTab
    CicGrp(1).AutoSizeColumns
    CicGrp(1).RedrawTab
    LineCnt = CicGrp(1).GetLineCount
    LookUpCca = LineCnt
End Function
Private Function GetCicGroup(LookCic As String, GrpCkif As String, GrpCkit As String, GrpName As String) As Boolean
    Dim RetVal As Boolean
    Dim CurLine As Long
    Dim MaxLine As Long
    RetVal = False
    MaxLine = CicGrp(0).GetLineCount
    While (CurLine < MaxLine) And (RetVal = False)
        GrpCkif = CicGrp(0).GetColumnValue(CurLine, 1)
        GrpCkit = CicGrp(0).GetColumnValue(CurLine, 2)
        If (LookCic >= GrpCkif) And (LookCic <= GrpCkit) Then
            GrpName = CicGrp(0).GetColumnValue(CurLine, 0)
            RetVal = True
        End If
        CurLine = CurLine + 1
    Wend
    GetCicGroup = RetVal
End Function
Private Sub Form_Resize()
    Dim NewSize As Long
    If MaxIdx >= 0 Then
        NewSize = Me.ScaleHeight - MaxTab.Top - 90
        If NewSize > 600 Then MaxTab.Height = NewSize
        NewSize = Me.ScaleWidth - (MaxTab.Left * 2)
        If NewSize > 600 Then MaxTab.Width = NewSize
    End If
End Sub

Public Sub RefreshCcaOnBc(CurNum As String, CurCmd As String, curSel As String, CurFld As String, CurDat As String)
    Dim tmpUrno As String
    Dim tmpCkbs As String
    Dim tmpCkes As String
    Dim tmpCkea As String
    Dim tmpCtyp As String
    Dim HitList As String
    Dim tmpData As String
    Dim CurIdx As Integer
    Dim LineNo As Long
    CurIdx = -1
    CurCcaBcNum = Val(CurNum)
    If LastCcaBcNum < 1 Then LastCcaBcNum = CurCcaBcNum - 1
    LastCcaBcNum = LastCcaBcNum + 1
    If LastCcaBcNum <> CurCcaBcNum Then
        LastCcaBcNum = CurCcaBcNum
        CcaTab(0).myTag = ""
        CcaTab(1).myTag = ""
    End If
    Select Case CurCmd
        Case "IRT"
            tmpCtyp = GetFieldValue("CTYP", CurDat, CurFld)
            If tmpCtyp = "C" Then CurIdx = 0 Else CurIdx = 1
            CcaTab(CurIdx).InsertTextLine ",,,,,,,,", False
            LineNo = CcaTab(CurIdx).GetLineCount - 1
            CcaTab(CurIdx).SetFieldValues LineNo, CurFld, CurDat
        Case "URT"
            tmpUrno = GetFieldValue("URNO", CurDat, CurFld)
            If tmpUrno = "" Then
                tmpData = GetItem(curSel, 2, "URNO")
                tmpData = Replace(tmpData, "=", "", 1, -1, vbBinaryCompare)
                tmpData = Replace(tmpData, "'", "", 1, -1, vbBinaryCompare)
                tmpData = Replace(tmpData, ")", " ", 1, -1, vbBinaryCompare)
                tmpData = Trim(tmpData)
                tmpUrno = GetItem(tmpData, 1, " ")
            End If
            If tmpUrno <> "" Then
                CurIdx = 0
                HitList = CcaTab(CurIdx).GetLinesByIndexValue("URNO", tmpUrno, 0)
                If HitList = "" Then
                    CurIdx = 1
                    HitList = CcaTab(CurIdx).GetLinesByIndexValue("URNO", tmpUrno, 0)
                End If
                If HitList = "" Then CurIdx = -1
                If CurIdx >= 0 Then
                    LineNo = Val(HitList)
                    CcaTab(CurIdx).SetFieldValues LineNo, CurFld, CurDat
                End If
            End If
        Case "DRT"
            tmpUrno = GetFieldValue("URNO", CurDat, CurFld)
            If tmpUrno = "" Then
                tmpData = GetItem(curSel, 2, "URNO")
                tmpData = Replace(tmpData, "=", "", 1, -1, vbBinaryCompare)
                tmpData = Replace(tmpData, "'", "", 1, -1, vbBinaryCompare)
                tmpData = Replace(tmpData, ")", " ", 1, -1, vbBinaryCompare)
                tmpData = Trim(tmpData)
                tmpUrno = GetItem(tmpData, 1, " ")
            End If
            If tmpUrno <> "" Then
                CurIdx = 0
                HitList = CcaTab(CurIdx).GetLinesByIndexValue("URNO", tmpUrno, 0)
                If HitList = "" Then
                    CurIdx = 1
                    HitList = CcaTab(CurIdx).GetLinesByIndexValue("URNO", tmpUrno, 0)
                End If
                If HitList = "" Then CurIdx = -1
                If CurIdx >= 0 Then
                    LineNo = Val(HitList)
                    CcaTab(CurIdx).DeleteLine LineNo
                End If
            End If
    End Select
    If CurIdx >= 0 Then
        CcaTab(CurIdx).SetLineColor LineNo, vbBlack, vbYellow
        CcaTab(CurIdx).IndexDestroy "URNO"
        CcaTab(CurIdx).IndexDestroy "FLNU"
        CcaTab(CurIdx).Sort "1,2,4,5", True, True
        CcaTab(CurIdx).IndexCreate "URNO", 0
        CcaTab(CurIdx).IndexCreate "FLNU", 1
        CcaTab(CurIdx).AutoSizeColumns
        CcaTab(CurIdx).RedrawTab
    End If
End Sub
