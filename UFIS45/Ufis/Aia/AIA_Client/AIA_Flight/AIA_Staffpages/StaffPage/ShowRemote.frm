VERSION 5.00
Begin VB.Form ShowRemote 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Remote Staff Page Control"
   ClientHeight    =   4995
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   7605
   ControlBox      =   0   'False
   Icon            =   "ShowRemote.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4995
   ScaleWidth      =   7605
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.OptionButton optTool 
      Caption         =   "Unlock"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Index           =   0
      Left            =   1470
      Style           =   1  'Graphical
      TabIndex        =   27
      Top             =   4620
      Width           =   975
   End
   Begin VB.Frame Frame2 
      Caption         =   "Remote StaffPage Buttons"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1140
      Left            =   30
      TabIndex        =   16
      Top             =   2880
      Width           =   4965
      Begin VB.OptionButton optTool 
         Caption         =   "Page"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   5
         Left            =   90
         Style           =   1  'Graphical
         TabIndex        =   26
         Top             =   360
         Width           =   945
      End
      Begin VB.OptionButton optTool 
         Caption         =   "Main"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   6
         Left            =   1050
         Style           =   1  'Graphical
         TabIndex        =   25
         Top             =   360
         Width           =   945
      End
      Begin VB.OptionButton optTool 
         Caption         =   "Time"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   7
         Left            =   2010
         Style           =   1  'Graphical
         TabIndex        =   24
         Top             =   360
         Width           =   945
      End
      Begin VB.OptionButton optTool 
         Caption         =   "UTC"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   8
         Left            =   2970
         Style           =   1  'Graphical
         TabIndex        =   23
         Top             =   360
         Width           =   945
      End
      Begin VB.OptionButton optTool 
         Caption         =   "Status"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   9
         Left            =   3930
         Style           =   1  'Graphical
         TabIndex        =   22
         Top             =   360
         Width           =   945
      End
      Begin VB.OptionButton optTool 
         Caption         =   "Left"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   10
         Left            =   90
         Style           =   1  'Graphical
         TabIndex        =   21
         Top             =   690
         Width           =   945
      End
      Begin VB.OptionButton optTool 
         Caption         =   "Right"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   11
         Left            =   1050
         Style           =   1  'Graphical
         TabIndex        =   20
         Top             =   690
         Width           =   945
      End
      Begin VB.OptionButton optTool 
         Caption         =   "Exit"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   12
         Left            =   2010
         Style           =   1  'Graphical
         TabIndex        =   19
         Top             =   690
         Width           =   945
      End
      Begin VB.OptionButton optTool 
         Caption         =   "Pg Up"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   13
         Left            =   2970
         Style           =   1  'Graphical
         TabIndex        =   18
         Top             =   690
         Width           =   945
      End
      Begin VB.OptionButton optTool 
         Caption         =   "Pg Down"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   14
         Left            =   3930
         Style           =   1  'Graphical
         TabIndex        =   17
         Top             =   690
         Width           =   945
      End
   End
   Begin VB.Timer CcoTimer 
      Enabled         =   0   'False
      Interval        =   2
      Left            =   360
      Top             =   570
   End
   Begin VB.Frame Frame3 
      Height          =   510
      Left            =   0
      TabIndex        =   14
      Top             =   3930
      Width           =   4965
      Begin VB.Label RemoteData 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   90
         TabIndex        =   15
         Top             =   210
         Width           =   4785
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Connected Location"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   975
      Left            =   30
      TabIndex        =   0
      Top             =   90
      Width           =   5895
      Begin VB.CheckBox StartApp 
         Caption         =   "Start Up"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   4560
         Style           =   1  'Graphical
         TabIndex        =   13
         Top             =   585
         Width           =   1245
      End
      Begin VB.CheckBox chkRemoteStart 
         Caption         =   "Remote Port"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   4560
         Style           =   1  'Graphical
         TabIndex        =   12
         Top             =   240
         Width           =   1245
      End
      Begin VB.CheckBox chkCCO 
         Caption         =   "Check"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   3570
         Style           =   1  'Graphical
         TabIndex        =   6
         Top             =   585
         Width           =   945
      End
      Begin VB.OptionButton optConnect 
         Caption         =   "Disconnected"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   1
         Left            =   2190
         Style           =   1  'Graphical
         TabIndex        =   5
         Top             =   585
         Value           =   -1  'True
         Width           =   1350
      End
      Begin VB.OptionButton optConnect 
         Caption         =   "Connect"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   0
         Left            =   810
         Style           =   1  'Graphical
         TabIndex        =   4
         Top             =   585
         Width           =   1350
      End
      Begin VB.ComboBox Location 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   810
         TabIndex        =   3
         Text            =   "172.20.4.160"
         Top             =   240
         Width           =   3705
      End
      Begin VB.CheckBox chkRemote 
         Height          =   660
         Left            =   90
         Picture         =   "ShowRemote.frx":030A
         Style           =   1  'Graphical
         TabIndex        =   2
         TabStop         =   0   'False
         Top             =   240
         Width           =   660
      End
   End
   Begin VB.Frame ToolFrame 
      Caption         =   "Remote Maintenance Tools"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1665
      Left            =   30
      TabIndex        =   1
      Top             =   1140
      Width           =   5895
      Begin VB.OptionButton optTool 
         Caption         =   "Get Data"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   4
         Left            =   2970
         Style           =   1  'Graphical
         TabIndex        =   11
         Top             =   270
         Width           =   945
      End
      Begin VB.OptionButton optTool 
         Caption         =   "Buttons"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   3
         Left            =   2010
         Style           =   1  'Graphical
         TabIndex        =   10
         Top             =   270
         Width           =   945
      End
      Begin VB.OptionButton optTool 
         Caption         =   "Times"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   2
         Left            =   1050
         Style           =   1  'Graphical
         TabIndex        =   9
         Top             =   270
         Width           =   945
      End
      Begin VB.OptionButton optTool 
         Caption         =   "PC Clock"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   1
         Left            =   90
         Style           =   1  'Graphical
         TabIndex        =   8
         Top             =   270
         Width           =   945
      End
   End
   Begin VB.TextBox DummyFocus 
      Height          =   345
      Left            =   960
      Locked          =   -1  'True
      TabIndex        =   7
      Text            =   "Text1"
      Top             =   1710
      Width           =   1815
   End
End
Attribute VB_Name = "ShowRemote"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub CcoTimer_Timer()
    If chkRemote.Value = 1 Then
        chkRemote.Value = 0
    Else
        chkRemote.Value = 1
    End If
End Sub

Private Sub chkCCO_Click()
    If chkCCO.Value = 1 Then
        chkCCO.BackColor = LightGreen
        CcoTimer.Interval = 2000
        CcoTimer.Enabled = True
    Else
        chkCCO.BackColor = vbButtonFace
        CcoTimer.Interval = 0
        CcoTimer.Enabled = False
    End If
    DummyFocus.SetFocus
End Sub

Private Sub chkRemote_Click()
    If chkRemote.Value = 1 Then
        chkRemote.BackColor = NormalYellow
        MainForm.SendRemoteMsg "CCO"
    Else
        chkRemote.BackColor = vbButtonFace
    End If
    DummyFocus.SetFocus
End Sub

Private Sub chkRemoteStart_Click()
    Dim TcpAdr As String
    If chkRemoteStart.Value = 1 Then
        TcpAdr = GetItem(Location.Text, 1, "/")
        'MsgBox TcpAdr
        MainForm.RemotePort(0).RemoteHost = TcpAdr
        MainForm.RemotePort(0).RemotePort = "4396"
        MainForm.RemotePort(0).Connect TcpAdr, "4396"
    Else
        MainForm.RemotePort_Close 0
    End If
End Sub

Private Sub optConnect_Click(Index As Integer)
    Dim TcpAdr As String
    If optConnect(Index).Value = True Then
        If Index = 0 Then
            If Not MainForm.RemoteIsBusy Then
                'MainForm.Text1.Text = ""
                optConnect(0).Caption = "Wait ..."
                optConnect(0).BackColor = NormalYellow
                optConnect(1).Caption = "Disconnect"
                TcpAdr = GetItem(Location.Text, 1, "/")
                MainForm.RemotePort(0).RemoteHost = TcpAdr
                'original
                'MainForm.RemotePort(0).RemotePort = "4397"
                'MainForm.RemotePort(0).Connect TcpAdr, "4397"
                'Test for CDI Connection
                MainForm.RemotePort(0).RemotePort = "3357"
                MainForm.RemotePort(0).Connect TcpAdr, "3357"
            End If
        Else
            CcoTimer.Enabled = False
            MainForm.RemotePort_Close 0
            optConnect(0).Caption = "Connect"
            optConnect(0).BackColor = vbButtonFace
            optConnect(1).Caption = "Disconnected"
            chkCCO.Enabled = False
            ToolFrame.Enabled = False
        End If
        DummyFocus.SetFocus
    End If
End Sub

Private Sub optConnect_GotFocus(Index As Integer)
    DummyFocus.SetFocus
End Sub

Private Sub optTool_Click(Index As Integer)
    Static LastIndex As Integer
    optTool(LastIndex).BackColor = vbButtonFace
    'RemoteData.Caption = ""
    If Index > 0 Then
        If optTool(Index).Value = True Then
            optTool(Index).BackColor = NormalYellow
            Select Case Index
                Case 0
                    'Reset / Undo
                Case 1
                    MainForm.SendRemoteMsg "SEND,CLOCK,1"
                Case 2
                    MainForm.SendRemoteMsg "SEND,TIMES,2"
                Case 3
                    MainForm.SendRemoteMsg "SEND,BUTTONS,3"
                Case 4
                    MainForm.SendRemoteMsg "SEND,DATA,4"
                Case 5
                    'MainForm.SendRemoteMsg "SEND,DATA,5"
                Case 6
                    MainForm.SendRemoteMsg "PUSH,MAIN_TITLE,6"
                Case 10
                    MainForm.SendRemoteMsg "PUSH,LEFT,10"
                Case 11
                    MainForm.SendRemoteMsg "PUSH,RIGHT,11"
                Case 12
                    MainForm.SendRemoteMsg "PUSH,EXIT,12"
                Case 13
                    MainForm.SendRemoteMsg "PUSH,PREV,13"
                Case 14
                    MainForm.SendRemoteMsg "PUSH,NEXT,14"
                Case Else
            End Select
            LastIndex = Index
        End If
    End If
    DummyFocus.SetFocus
End Sub

Private Sub StartApp_Click()
    If StartApp.Value = 1 Then
        MainForm.SendRemoteMsg "START,APPL,-1||c:\ufis\rel\StaffPages\StaffPage.exe"
    End If
End Sub
