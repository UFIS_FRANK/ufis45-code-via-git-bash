VERSION 5.00
Object = "{64E8E382-05E2-11D2-9B1D-9B0630BC8F12}#1.0#0"; "tab.ocx"
Begin VB.Form CdiList 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Connected Clients"
   ClientHeight    =   3000
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   3120
   Icon            =   "CdiList.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3000
   ScaleWidth      =   3120
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin TABLib.TAB TAB1 
      Height          =   2490
      Left            =   60
      TabIndex        =   1
      Top             =   60
      Width           =   3015
      _Version        =   65536
      _ExtentX        =   5318
      _ExtentY        =   4392
      _StockProps     =   0
   End
   Begin VB.CheckBox chkClose 
      Caption         =   "Close"
      Height          =   315
      Left            =   960
      Style           =   1  'Graphical
      TabIndex        =   0
      Top             =   2640
      Width           =   1095
   End
End
Attribute VB_Name = "CdiList"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub chkClose_Click()
    Unload Me
End Sub

Private Sub Form_Load()
    MainForm.btnCdiServer.Value = 1
    InitMyList
End Sub

Private Sub Form_Unload(Cancel As Integer)
    MainForm.btnCdiServer.Value = 0
End Sub

Public Sub InitMyList()
    Dim iCnt As Integer
    Dim tmpLine As String
    Dim cdiObj As Winsock
    TAB1.HeaderString = "NBR,IDX,TCP/IP ADDR.,   NAME,LOGIN"
    TAB1.HeaderLengthString = "36,36,128,90,136"
    TAB1.ColumnAlignmentString = "R,R,L,L,L"
    TAB1.LeftTextOffset = "2"
    TAB1.ResetContent
    iCnt = 0
    For Each cdiObj In MainForm.CdiPort
        If cdiObj.Index > 0 Then
            iCnt = iCnt + 1
            tmpLine = Trim(Str(iCnt)) + ","
            tmpLine = tmpLine + Trim(Str(cdiObj.Index)) + ","
            tmpLine = tmpLine + cdiObj.RemoteHostIP + ",,"
            TAB1.InsertTextLine tmpLine, False
        End If
    Next
    TAB1.RedrawTab
End Sub
