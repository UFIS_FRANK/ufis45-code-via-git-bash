Attribute VB_Name = "MyOwnLib"
Option Explicit
Public StaffPageIniFile As String
Public MyHostName As String
Public MyHostType As String
Public MyApplVersion As String
Public MyUserName As String
Public MyShortName As String
Public MyTblExt As String
Public MyHopo As String
Public MyWksName As String
Public MyCcaVpfr As String
Public MyCcaVpto As String
Public TimesInLocal As Boolean
Public ShowBasics As Boolean
Public ReloadOnDemand As Boolean
Public DarkLineColor As Long
Public LightLineColor As Long
Public LineTextColor As Long

Public Sub ReadCedaData(CurTab As TABLib.TAB, TableName As String, Fields As String, SqlKey As String)
    Dim tmpData As String
    CurTab.ResetContent
    CurTab.LogicalFieldList = Fields
    CurTab.HeaderString = Fields
    tmpData = CreateLengthList(Fields)
    CurTab.HeaderLengthString = tmpData
    CurTab.ColumnWidthString = tmpData
    CurTab.AutoSizeByHeader = True
    CurTab.ShowHorzScroller True
    CurTab.CedaCurrentApplication = MyShortName '& "," & UfisServer.GetApplVersion(True)
    CurTab.CedaHopo = MyHopo
    CurTab.CedaIdentifier = "IDX"
    CurTab.CedaPort = "3357"
    CurTab.CedaReceiveTimeout = "250"
    CurTab.CedaRecordSeparator = vbLf
    CurTab.CedaSendTimeout = "250"
    CurTab.CedaServerName = MyHostName
    CurTab.CedaTabext = MyTblExt
    CurTab.CedaUser = MyUserName
    CurTab.CedaWorkstation = MyWksName
    CurTab.CedaAction "RTA", TableName, Fields, "", SqlKey
    CurTab.AutoSizeColumns
    CurTab.RedrawTab
End Sub

Public Function CreateLengthList(FieldList As String) As String
    Dim ItmCnt As Integer
    Dim ItmDat As String
    Dim Result As String
    ItmDat = "START"
    ItmCnt = 0
    Result = "10"
    While ItmDat <> ""
        ItmCnt = ItmCnt + 1
        ItmDat = GetItem(FieldList, ItmCnt, ",")
        If ItmDat <> "" Then
            Result = Result + ",10"
        End If
    Wend
    CreateLengthList = Result
End Function

Public Sub SetFieldValue(FieldName As String, NewValue As String, DataList As String, FieldList As String)
    Dim ItmNbr As Integer
    ItmNbr = GetItemNo(FieldList, FieldName)
    SetItem DataList, ItmNbr, ",", NewValue
End Sub

Public Sub SetAllFormsOnTop(SetTop As Boolean)

End Sub

Public Sub GetColorValues(LineColors As String)
    Dim strColor As String
    strColor = GetItem(LineColors, 1, ",")
    DarkLineColor = TranslateColor(strColor)
    strColor = GetItem(LineColors, 2, ",")
    LightLineColor = TranslateColor(strColor)
End Sub
Public Function TranslateColor(ColorText As String) As Long
    Dim tmpColor As Long
    Select Case ColorText
        Case "DARKBLUE"
            tmpColor = &HC00000
        Case "NORMALBLUE"
            tmpColor = vbBlue
        Case "LIGHTBLUE"
            tmpColor = &HFF0000
        Case "LIGHTGREY"
            tmpColor = LightGray
        Case "NORMALGREY"
            tmpColor = NormalGray
        Case "DARKGREY"
            tmpColor = DarkGray
        Case Else
            tmpColor = NormalGray
    End Select
    TranslateColor = tmpColor
End Function
