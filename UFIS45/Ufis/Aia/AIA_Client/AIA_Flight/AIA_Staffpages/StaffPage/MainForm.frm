VERSION 5.00
Object = "{64E8E382-05E2-11D2-9B1D-9B0630BC8F12}#1.0#0"; "TAB.ocx"
Object = "{A2F31E92-C74F-11D3-A251-00500437F607}#1.0#0"; "ufiscom.ocx"
Object = "{7DA325C2-D4A4-11D3-9B36-020128B0A0FF}#1.0#0"; "UfisBroadcasts.ocx"
Object = "{248DD890-BB45-11CF-9ABC-0080C7E7B78D}#1.0#0"; "MSWINSCK.ocx"
Begin VB.Form MainForm 
   BorderStyle     =   0  'None
   ClientHeight    =   8835
   ClientLeft      =   -30
   ClientTop       =   -315
   ClientWidth     =   15375
   ControlBox      =   0   'False
   FillColor       =   &H00C0C0C0&
   Icon            =   "MainForm.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   589
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   1025
   ShowInTaskbar   =   0   'False
   Begin TABLib.TAB CdrHdl 
      Height          =   2055
      Left            =   60
      TabIndex        =   18
      Top             =   4650
      Visible         =   0   'False
      Width           =   13035
      _Version        =   65536
      _ExtentX        =   22992
      _ExtentY        =   3625
      _StockProps     =   64
   End
   Begin VB.CheckBox chkHide 
      Caption         =   "HIDE"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   13.5
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   510
      Left            =   3660
      Style           =   1  'Graphical
      TabIndex        =   17
      Top             =   3270
      Visible         =   0   'False
      Width           =   885
   End
   Begin TABLib.TAB DataTab 
      Height          =   1935
      Index           =   1
      Left            =   4470
      TabIndex        =   16
      Top             =   600
      Width           =   3675
      _Version        =   65536
      _ExtentX        =   6482
      _ExtentY        =   3413
      _StockProps     =   64
   End
   Begin VB.CheckBox btnCdiServer 
      Caption         =   "LISTENING"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   13.5
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   510
      Left            =   4590
      Style           =   1  'Graphical
      TabIndex        =   15
      Top             =   3270
      Visible         =   0   'False
      Width           =   2025
   End
   Begin VB.Timer BcSpoolTimer 
      Interval        =   5
      Left            =   600
      Top             =   3960
   End
   Begin MSWinsockLib.Winsock CdiPort 
      Index           =   0
      Left            =   90
      Top             =   3960
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   393216
   End
   Begin VB.Frame Frame1 
      BorderStyle     =   0  'None
      Height          =   525
      Left            =   14160
      TabIndex        =   10
      Top             =   30
      Width           =   1140
      Begin VB.Shape RemoteCon 
         FillColor       =   &H00FFFFFF&
         FillStyle       =   0  'Solid
         Height          =   105
         Index           =   1
         Left            =   960
         Shape           =   3  'Circle
         Top             =   210
         Width           =   165
      End
      Begin VB.Shape RemoteCon 
         FillColor       =   &H00FFFFFF&
         FillStyle       =   0  'Solid
         Height          =   105
         Index           =   0
         Left            =   840
         Shape           =   3  'Circle
         Top             =   210
         Width           =   165
      End
      Begin VB.Shape CedaCon 
         FillColor       =   &H0000FF00&
         FillStyle       =   0  'Solid
         Height          =   345
         Index           =   0
         Left            =   765
         Top             =   90
         Visible         =   0   'False
         Width           =   90
      End
      Begin VB.Label lblRefresh 
         Alignment       =   2  'Center
         BackColor       =   &H00C0C0C0&
         Caption         =   "00:00:00"
         ForeColor       =   &H00000000&
         Height          =   165
         Index           =   0
         Left            =   60
         TabIndex        =   12
         Top             =   60
         Width           =   735
      End
      Begin VB.Label lblRefresh 
         Alignment       =   2  'Center
         BackColor       =   &H00C0C0C0&
         Caption         =   "00:00:00"
         ForeColor       =   &H00000000&
         Height          =   165
         Index           =   1
         Left            =   60
         TabIndex        =   11
         Top             =   270
         Width           =   735
      End
      Begin VB.Shape BcUpdInd 
         FillColor       =   &H00C0C0C0&
         FillStyle       =   0  'Solid
         Height          =   105
         Index           =   0
         Left            =   840
         Shape           =   3  'Circle
         Top             =   90
         Width           =   165
      End
      Begin VB.Shape BcRcvInd 
         FillColor       =   &H00C0C0C0&
         FillStyle       =   0  'Solid
         Height          =   105
         Index           =   0
         Left            =   840
         Shape           =   3  'Circle
         Top             =   330
         Width           =   165
      End
      Begin VB.Shape BcUpdInd 
         FillColor       =   &H00C0C0C0&
         FillStyle       =   0  'Solid
         Height          =   105
         Index           =   1
         Left            =   960
         Shape           =   3  'Circle
         Top             =   90
         Width           =   165
      End
      Begin VB.Shape RefreshInd 
         FillColor       =   &H00C0C0C0&
         FillStyle       =   0  'Solid
         Height          =   105
         Index           =   1
         Left            =   960
         Shape           =   3  'Circle
         Top             =   210
         Width           =   165
      End
      Begin VB.Shape RefreshInd 
         BorderColor     =   &H00000000&
         FillColor       =   &H00C0C0C0&
         FillStyle       =   0  'Solid
         Height          =   105
         Index           =   0
         Left            =   840
         Shape           =   3  'Circle
         Top             =   210
         Width           =   165
      End
      Begin VB.Shape BcRcvInd 
         FillColor       =   &H00C0C0C0&
         FillStyle       =   0  'Solid
         Height          =   105
         Index           =   1
         Left            =   960
         Shape           =   3  'Circle
         Top             =   330
         Width           =   165
      End
   End
   Begin VB.CheckBox lblUTC 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   18
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   585
      Left            =   12390
      Style           =   1  'Graphical
      TabIndex        =   8
      Top             =   15
      Width           =   1725
   End
   Begin VB.CheckBox lblNow 
      Caption         =   "LOGON SERVER"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   18
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   585
      Left            =   8865
      Style           =   1  'Graphical
      TabIndex        =   7
      Top             =   15
      Width           =   3510
   End
   Begin VB.CheckBox MainTitle 
      Caption         =   "Main Title"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   18
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   585
      Left            =   4500
      Style           =   1  'Graphical
      TabIndex        =   6
      Top             =   15
      Width           =   4350
   End
   Begin VB.CheckBox PageTitle 
      Caption         =   "Page Title"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   18
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   585
      Left            =   30
      Style           =   1  'Graphical
      TabIndex        =   5
      Top             =   15
      Width           =   4455
   End
   Begin VB.CheckBox chkRightAction 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   13.5
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   510
      Left            =   1230
      Style           =   1  'Graphical
      TabIndex        =   4
      Top             =   3270
      Width           =   1185
   End
   Begin VB.CheckBox chkLeftAction 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   13.5
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   510
      Left            =   30
      Style           =   1  'Graphical
      TabIndex        =   3
      Top             =   3270
      Width           =   1155
   End
   Begin MSWinsockLib.Winsock RbcBcPort 
      Index           =   0
      Left            =   30
      Top             =   2640
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   393216
   End
   Begin UFISBROADCASTSLib.UfisBroadcasts RcvBc 
      Left            =   2610
      Top             =   2640
      _Version        =   65536
      _ExtentX        =   1402
      _ExtentY        =   1032
      _StockProps     =   0
   End
   Begin VB.CommandButton btnExit 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   13.5
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   510
      Left            =   2460
      TabIndex        =   2
      Top             =   3270
      Width           =   1155
   End
   Begin VB.CommandButton btnPrev 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   13.5
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   510
      Left            =   6660
      TabIndex        =   1
      Top             =   3270
      Width           =   795
   End
   Begin VB.CommandButton btnNext 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   13.5
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   510
      Left            =   7500
      TabIndex        =   0
      Top             =   3270
      Width           =   735
   End
   Begin VB.Timer tmrMinutes 
      Left            =   1470
      Top             =   2640
   End
   Begin VB.Timer tmrSeconds 
      Left            =   990
      Top             =   2640
   End
   Begin UFISCOMLib.UfisCom Ufis 
      Left            =   1950
      Top             =   2640
      _Version        =   65536
      _ExtentX        =   1085
      _ExtentY        =   1032
      _StockProps     =   0
   End
   Begin VB.CheckBox chkAlive 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   18
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   585
      Left            =   14130
      Style           =   1  'Graphical
      TabIndex        =   9
      Top             =   15
      Width           =   1215
   End
   Begin TABLib.TAB DataTab 
      Height          =   1935
      Index           =   0
      Left            =   30
      TabIndex        =   13
      Top             =   600
      Width           =   3675
      _Version        =   65536
      _ExtentX        =   6482
      _ExtentY        =   3413
      _StockProps     =   64
   End
   Begin MSWinsockLib.Winsock RemotePort 
      Index           =   0
      Left            =   480
      Top             =   2640
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   393216
   End
   Begin TABLib.TAB BcSpoolerTab 
      Height          =   1935
      Left            =   10080
      TabIndex        =   14
      Top             =   570
      Visible         =   0   'False
      Width           =   1425
      _Version        =   65536
      _ExtentX        =   2514
      _ExtentY        =   3413
      _StockProps     =   64
   End
   Begin VB.Menu RCPopup 
      Caption         =   "Popup"
      Visible         =   0   'False
      Begin VB.Menu msg1 
         Caption         =   "Info"
      End
      Begin VB.Menu Rest1 
         Caption         =   "Show Me"
      End
      Begin VB.Menu Exit1 
         Caption         =   "Exit"
      End
   End
End
Attribute VB_Name = "MainForm"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

'Application Defines

Public RemoteControl As Boolean
Public RemoteIsOpen As Boolean
Public RemoteIsBusy As Boolean
Public RemoteIdx As Integer
Public CdiIdx As Integer
Public CdiServer As Boolean
Public CdiPacketComplete As Boolean
Public CdiPortsChanged As Boolean
Public CdiSendOnConnex As Boolean
Public CdiActIdx As Integer
Private CdiMsgRest(1000) As String
Private CdiRecSep As String

Public BcPortConnected As Boolean

Public omDisplayMode As String
Dim RemoteColor(2) As Long
Dim GotUtcDiff As Boolean
Dim SupportCCaTab(2) As Boolean

Dim StaffPageSection As String
Dim IniFileFound As String
Dim MainSectionFound As String
Dim PageSectionFound(2) As String
Dim TestMode As Boolean
Dim UpdateOnReload As Boolean
Dim UpdateCount As Long

Dim RefreshCyclus As Integer
Dim RefreshCountDown(2) As Boolean
Dim MinCountDown(2) As Integer
Dim SecCountDown(2) As Integer

Dim CountDownTime As Integer
Dim TabIdx As Integer
Dim currScrollPos(2) As Integer
Dim UTC_Diff As Integer

Dim ApplWatched As String
Dim MyTcpIpAdr As String
Dim HexTcpAdr As String

Dim MainCaption(2) As String
Dim PageCaption(2) As String
Dim UseBcNums(2) As String
Dim LastBcNum(2) As Integer
Dim LostBroadCast(2) As Boolean
Dim ReloadData(2) As Boolean
Dim MyPageFields(2) As String
Dim MyPageFieldCount(2) As Long
Dim MyFieldLen As String
Dim MyTimeFields(2) As String
Dim MyTimeCols(2) As String
Dim MyCedaTitle(2) As String
Dim MyDataTable(2) As String
Dim MyPageSqlKey(2) As String
Dim MyTrigger01(2) As String
Dim MyTrigger02(2) As String
Dim MyTrigger03(2) As String
Dim CdiPageFields(2) As String
Dim CdiKeyFields(2) As String
Dim CdiTriggers(2) As String
Dim CdiFieldLength(2) As String
Dim ToggleButtons As String
Dim MyLoadButton As String
Dim CurLoadButton As String
Dim KeepUpdate As Integer
Dim EmptyLine(2) As String
Dim ListLines(2) As Integer
Dim MyUrnoIdx(2) As Integer
Dim MyFlnoIdx(2) As Integer
Dim MyCkifIdx(2) As Integer
Dim MyCkitIdx(2) As Integer
Dim MyStodIdx(2) As Integer
Dim DefSaveTimer As Integer
Dim ActSaveTimer As Integer
Dim SavePath As String
Dim SendUpdates As Boolean


Private Sub InitApplication()
    Dim CurSection As String
    'Me.Caption = App.EXEName
    If App.PrevInstance = True Then
        MsgBox "Sorry. Application '" + App.EXEName + "' is already running."
        End
    End If
    'LoginDialog.ShowLoginDialog
    MsgLogging UCase(App.EXEName) & " COMING UP"
    GetGlobalConfig
    RemoteColor(0) = &HFF00&
    RemoteColor(1) = &HC000&
    RefreshInd(0).FillColor = RemoteColor(0)
    RefreshInd(1).FillColor = RemoteColor(0)
    lblRefresh(0).Caption = "LOGIN"
    CurSection = StaffPageSection + "_GLOBAL"
    GetApplConfig CurSection, 0
    GetApplConfig CurSection, 1
    DataTab(0).CreateCellObj "UpdCell", vbRed, vbWhite, DataTab(0).FontSize, False, False, True, 0, "Courier New"
    DataTab(1).CreateCellObj "UpdCell", vbRed, vbWhite, DataTab(1).FontSize, False, False, True, 0, "Courier New"
    'MsgBox "RemCtrl is: " & RemoteControl
    UpdateCount = 0
    If Not RemoteControl Then
        OpenRemotePort
        If ApplWatched = "YES" Then
            On Error GoTo ErrorHandle
            Shell "c:\ufis\system\RemoteCtrl.exe"
            On Error GoTo 0
        End If
        GetPageConfig "LEFT", PageCaption(0), 0
        GetPageConfig "RIGHT", PageCaption(1), 1
        AppendLines 0
        AppendLines 1
        ApplicationIsStarted = True
        GotUtcDiff = False
        BcSpoolerTab.ResetContent
        BcSpoolerTab.SetFieldSeparator (Chr(16))
        'MsgBox "BC Activated"
        ReloadData(0) = False
        ReloadData(1) = False
        If MyLoadButton = "LEFT" Then
            MainTitle.Caption = MainCaption(0)
            PageTitle.Caption = PageCaption(0)
            DataTab(0).Visible = True
            DataTab(1).Visible = False
        Else
            MainTitle.Caption = MainCaption(1)
            PageTitle.Caption = PageCaption(1)
            DataTab(1).Visible = True
            DataTab(0).Visible = False
        End If
        LastBcNum(0) = 0
        LastBcNum(1) = 0
        LostBroadCast(0) = False
        LostBroadCast(1) = False
        CurLoadButton = "BOTH"
        MinCountDown(0) = -1
        SecCountDown(0) = 0
        MinCountDown(1) = -1
        SecCountDown(1) = 0
        RefreshCountDown(0) = True
        RefreshCountDown(1) = True
        tmrSeconds.Interval = 500
        tmrMinutes.Interval = 60000
    Else
        tmrSeconds.Interval = 0
        tmrMinutes.Interval = 0
        Me.Caption = "REMOTE STAFF PAGE"
        PageTitle.Caption = "STAFF PAGES"
        MainTitle.Caption = "REMOTE CONTROL"
        lblRefresh(0).Caption = "REMOTE"
        lblRefresh(1).Caption = "00:00:00"
        lblNow.Caption = "00.00.00 / 00:00:00"
        lblUTC.Caption = "00:00:00"
        RefreshCountDown(0) = True
        RefreshCountDown(1) = True
        tmrSeconds.Interval = 500
        tmrMinutes.Interval = 60000
    End If
    'App.TaskVisible = False
    If chkHide.Tag = "AUTO" Then
        Hook Me.hwnd   ' Set up our handler
        AddIconToTray Me.hwnd, Me.Icon, Me.Icon.Handle, App.EXEName + " / " + MainTitle.Caption
        Me.Hide
    End If
    Exit Sub
ErrorHandle:
    Resume Next
End Sub

Private Sub GetGlobalConfig()
    Dim ret As Integer
    Dim blRet As Boolean
    Dim tmpdat As String
    Dim tmpVal As Integer
    Dim ItmNbr As Integer
    Dim CurSection As String
    StaffPageIniFile = Trim(GetItem(Command, 1, "/"))
    If StaffPageIniFile = "" Then StaffPageIniFile = App.EXEName + ".ini"
    StaffPageIniFile = "c:\ufis\system\" & StaffPageIniFile
    ApplWatched = GetIniEntry(StaffPageIniFile, "GLOBAL", "", "APPL_WATCHED", "NO")
    MyHostName = GetIniEntry(StaffPageIniFile, "GLOBAL", "", "HOSTNAME", "LOCAL")
    MyHostType = GetIniEntry(StaffPageIniFile, "GLOBAL", "", "HOSTTYPE", "UNKNOWN")
    MyTblExt = GetIniEntry(StaffPageIniFile, "GLOBAL", "", "TABLEEXTENSION", "TAB")
    MyHopo = GetIniEntry(StaffPageIniFile, "GLOBAL", "", "HOMEAIRPORT", "???")
    MyUserName = GetIniEntry(StaffPageIniFile, "GLOBAL", "", "UFIS_USER", "STAFFPAGE")
    MyShortName = GetIniEntry(StaffPageIniFile, "GLOBAL", "", "SHORT_NAME", "STAFF")
    ToggleButtons = GetIniEntry(StaffPageIniFile, "GLOBAL", "", "TOGGLE_BUTTONS", "NO")
    SavePath = GetIniEntry(StaffPageIniFile, "GLOBAL", "", "SAVE_PATH", "")
    If Len(SavePath) > 0 And Right(SavePath, 1) <> "\" Then SavePath = SavePath + "\"
    tmpdat = GetIniEntry(StaffPageIniFile, "GLOBAL", "", "SAVE_TIMER", "-1")
    DefSaveTimer = Val(tmpdat)
    ActSaveTimer = DefSaveTimer
    RemoteControl = False
    StaffPageSection = Trim(GetItem(Command, 2, "/"))
    If StaffPageSection = "" Then
        StaffPageSection = GetIniEntry(StaffPageIniFile, "GLOBAL", "", "ACTIVE_SECTION", "????")
    End If
    If StaffPageSection = "REMOTE_CTRL" Then
        StaffPageIniFile = "c:\ufis\system\StaffRemote.ini"
        SuspendErrMsg = True
        RemoteControl = True
    End If
    SendUpdates = False
    CdiIdx = -1
    If Not RemoteControl Then
        tmpdat = GetIniEntry(StaffPageIniFile, "GLOBAL", "", "SEND_UPDATES", "NO")
        If tmpdat = "YES" Then
            OpenCdiPort
            SendUpdates = True
            tmpdat = GetIniEntry(StaffPageIniFile, "GLOBAL", "", "CDI_REC_SEP", "LF")
            Select Case tmpdat
                Case "LF"
                    CdiRecSep = vbLf
                Case "CR"
                    CdiRecSep = vbCr
                Case "CRLF"
                    CdiRecSep = vbNewLine
                Case "NONE"
                    CdiRecSep = ""
                Case Else
                    CdiRecSep = ""
            End Select
        End If
    Else
        tmpdat = GetIniEntry(StaffPageIniFile, "GLOBAL", "", "ACCESS_PORT", "0000")
        If tmpdat <> "4397" Then
            MsgBox "Sorry, access denied. No Permission for Remote Control !"
            End
        End If
    End If
    MyApplVersion = "StaffPage," & App.Major & "." & App.Minor & "." & App.Revision & ",11"
    UpdateOnReload = True
    CdiSendOnConnex = True
    
    MyTcpIpAdr = RbcBcPort(0).LocalIP
    HexTcpAdr = ""
    For ItmNbr = 1 To 4
        tmpVal = Val(GetItem(MyTcpIpAdr, ItmNbr, "."))
        tmpdat = Hex(tmpVal)
        HexTcpAdr = HexTcpAdr + Right("00" + tmpdat, 2)
    Next
End Sub

Private Sub GetApplConfig(CurSection As String, CurTabIdx As Integer)
    Dim tmpCfgDat As String
    Dim tmpY As Long
    Dim tmpVal As Long
    DataTab(CurTabIdx).ResetContent
    'MsgBox CurSection
    MainSectionFound = GetIniEntry(StaffPageIniFile, CurSection, "", "?EXIST?", "")
    If MainSectionFound = "NO" Then
        MsgBox "Section '" & CurSection & " not found."
    End If
    tmpCfgDat = GetIniEntry(StaffPageIniFile, CurSection, "", "TEST_MODE", "NO")
    If tmpCfgDat = "YES" Then
        TestMode = True
    Else
        TestMode = False
    End If
    tmpCfgDat = GetIniEntry(StaffPageIniFile, CurSection, "", "APPL_POS", "0,0")
    Me.Left = Val(GetItem(tmpCfgDat, 1, ",")) * Screen.TwipsPerPixelX
    Me.Top = Val(GetItem(tmpCfgDat, 2, ",")) * Screen.TwipsPerPixelY
    tmpCfgDat = GetIniEntry(StaffPageIniFile, CurSection, "", "APPL_SIZE", "FULL")
    If tmpCfgDat = "FULL" Then
        tmpCfgDat = Str(Screen.Width / Screen.TwipsPerPixelX) & "," & Screen.Height / Screen.TwipsPerPixelY
    End If
    Me.Width = Val(GetItem(tmpCfgDat, 1, ",")) * Screen.TwipsPerPixelX
    Me.Height = Val(GetItem(tmpCfgDat, 2, ",")) * Screen.TwipsPerPixelY
    tmpCfgDat = GetIniEntry(StaffPageIniFile, CurSection, "", "TITLE_SIZE", "290")
    MainTitle.Width = Val(tmpCfgDat)
    PageTitle.Width = lblNow.Left - MainTitle.Width - 4
    MainTitle.Left = PageTitle.Left + PageTitle.Width + 1
    tmpCfgDat = GetIniEntry(StaffPageIniFile, CurSection, "", "PAGE_LINES", "29")
    ListLines(CurTabIdx) = Val(tmpCfgDat)
    DataTab(CurTabIdx).FontName = "Courier New"
    tmpCfgDat = GetIniEntry(StaffPageIniFile, CurSection, "", "TEXT_HEIGHT", "24")
    DataTab(CurTabIdx).HeaderFontSize = Val(tmpCfgDat)
    DataTab(CurTabIdx).FontSize = Val(tmpCfgDat)
    tmpCfgDat = GetIniEntry(StaffPageIniFile, CurSection, "", "SCROLLBAR_H", "NO")
    If tmpCfgDat = "YES" Then
        DataTab(CurTabIdx).ShowHorzScroller True
        tmpY = 14
    Else
        DataTab(CurTabIdx).ShowHorzScroller False
        tmpY = 0
    End If
    
    tmpCfgDat = GetIniEntry(StaffPageIniFile, CurSection, "", "TIMES_LOCAL", "NO")
    If tmpCfgDat = "YES" Then TimesInLocal = True Else TimesInLocal = False
    
    tmpCfgDat = GetIniEntry(StaffPageIniFile, CurSection, "", "SHOW_BASICS", "NO")
    If tmpCfgDat = "YES" Then ShowBasics = True Else ShowBasics = False
    
    tmpCfgDat = GetIniEntry(StaffPageIniFile, CurSection, "", "ENABLE_RELOAD", "NO")
    If tmpCfgDat = "YES" Then ReloadOnDemand = True Else ReloadOnDemand = False
    
    tmpCfgDat = GetIniEntry(StaffPageIniFile, CurSection, "", "LINE_COLORS", "DARKBLUE,LIGHTBLUE")
    GetColorValues tmpCfgDat
    
    tmpCfgDat = GetIniEntry(StaffPageIniFile, CurSection, "", "SCROLLBAR_V", "NO")
    If tmpCfgDat = "YES" Then DataTab(CurTabIdx).ShowVertScroller True Else DataTab(CurTabIdx).ShowVertScroller False
    tmpCfgDat = GetIniEntry(StaffPageIniFile, CurSection, "", "LINE_HEIGHT", "24")
    DataTab(CurTabIdx).LineHeight = Val(tmpCfgDat) - 1
    DataTab(CurTabIdx).Height = (ListLines(CurTabIdx) + 1) * (Val(tmpCfgDat) - 1) + tmpY + 1
    DataTab(CurTabIdx).SetTabFontBold True
    
    tmpVal = DataTab(CurTabIdx).Top + DataTab(CurTabIdx).Height
    chkLeftAction.Top = tmpVal
    chkRightAction.Top = tmpVal
    btnExit.Top = tmpVal
    chkHide.Top = tmpVal
    btnCdiServer.Top = tmpVal
    btnPrev.Top = tmpVal
    btnNext.Top = tmpVal
    tmpVal = (Me.Height / Screen.TwipsPerPixelY) - tmpVal
    If tmpVal > 10 Then
        chkLeftAction.Height = tmpVal
        chkRightAction.Height = tmpVal
        btnExit.Height = tmpVal
        chkHide.Height = tmpVal
        btnCdiServer.Height = tmpVal
        btnPrev.Height = tmpVal
        btnNext.Height = tmpVal
    End If
    tmpCfgDat = GetIniEntry(StaffPageIniFile, CurSection, "", "EXIT_WIDTH", "100")
    btnExit.Width = Val(tmpCfgDat)
    tmpVal = ((Me.Width / Screen.TwipsPerPixelX) - btnExit.Width) / 2
    btnExit.Left = tmpVal
    chkLeftAction.Left = 2
    tmpVal = tmpVal - 2
    chkLeftAction.Width = (tmpVal / 2) - 1
    chkRightAction.Left = chkLeftAction.Left + chkLeftAction.Width + 1
    chkRightAction.Width = btnExit.Left - chkRightAction.Left - 1
    chkHide.Left = btnExit.Left + btnExit.Width + 1
    chkHide.Tag = GetIniEntry(StaffPageIniFile, CurSection, "", "HIDE_APP", "NO")
    If chkHide.Tag <> "NO" Then
        chkHide.Visible = True
    End If
    If CdiIdx >= 0 Then
        If chkHide.Tag <> "NO" Then
            btnCdiServer.Left = chkHide.Left + chkHide.Width + 1
        Else
            btnCdiServer.Left = btnExit.Left + btnExit.Width + 1
        End If
        btnPrev.Left = btnCdiServer.Left + btnCdiServer.Width + 1
    Else
        If chkHide.Tag <> "NO" Then
            btnPrev.Left = chkHide.Left + chkHide.Width + 1
        Else
            btnPrev.Left = btnExit.Left + btnExit.Width + 1
        End If
    End If
    tmpVal = ((Me.Width / Screen.TwipsPerPixelX) - btnPrev.Left) / 2
    btnPrev.Width = tmpVal - 1
    btnNext.Left = btnPrev.Left + btnPrev.Width + 1
    btnNext.Width = (Me.Width / Screen.TwipsPerPixelX) - btnNext.Left - 2
    
    MainTitle.Caption = GetIniEntry(StaffPageIniFile, CurSection, "", "MAIN_TITLE", "MAIN_TITLE")
    PageTitle.Caption = GetIniEntry(StaffPageIniFile, CurSection, "", "PAGE_TITLE", "PAGE_TITLE")
    
    ToggleButtons = GetIniEntry(StaffPageIniFile, CurSection, "", "TOGGLE_BUTTONS", ToggleButtons)
    chkLeftAction.Caption = GetIniEntry(StaffPageIniFile, CurSection, "", "LEFT_BUTTON", "LEFT_BUTTON")
    chkRightAction.Caption = GetIniEntry(StaffPageIniFile, CurSection, "", "RIGHT_BUTTON", "RIGHT_BUTTON")
    btnExit.Caption = GetIniEntry(StaffPageIniFile, CurSection, "", "EXIT_BUTTON", "EXIT")
    btnPrev.Caption = GetIniEntry(StaffPageIniFile, CurSection, "", "PREV_BUTTON", "PAGE UP")
    btnNext.Caption = GetIniEntry(StaffPageIniFile, CurSection, "", "NEXT_BUTTON", "PAGE DOWN")
    DataTab(CurTabIdx).SelectBackColor = vbBlack
    DataTab(CurTabIdx).SelectTextColor = vbWhite
    MyLoadButton = GetIniEntry(StaffPageIniFile, CurSection, "", "START_BUTTON", "LEFT")
    MyDataTable(CurTabIdx) = GetIniEntry(StaffPageIniFile, CurSection, "", "TABLE_NAME", "??????")
    tmpCfgDat = GetIniEntry(StaffPageIniFile, CurSection, "", "RELOAD_MIN", "2")
    CountDownTime = Val(tmpCfgDat)
    tmpCfgDat = GetIniEntry(StaffPageIniFile, CurSection, "", "RELOAD_MAX", "5")
    RefreshCyclus = Val(tmpCfgDat)
    tmpCfgDat = GetIniEntry(StaffPageIniFile, CurSection, "", "KEEP_UPDATE", "20")
    KeepUpdate = Val(tmpCfgDat)
    currScrollPos(0) = 0
    currScrollPos(1) = 0
End Sub

Private Sub GetPageConfig(CurAction As String, CurPageTitle As String, CurTabIdx As Integer)
    Dim CurSection As String
    Dim tmpdat As String
    Dim tmpVal As String
    Dim ItmNbr As Integer
    Dim CurPos As Integer
    Dim CurCol As Long
    Dim FldNam As String
    CurSection = StaffPageSection + "_" + CurAction
    PageSectionFound(CurTabIdx) = GetIniEntry(StaffPageIniFile, CurSection, "", "?EXIST?", "")
    If PageSectionFound(CurTabIdx) = "NO" Then
        MsgBox "Page Configuration" & vbNewLine & "Section '" & CurSection & " not found."
    End If
    MainCaption(CurTabIdx) = GetIniEntry(StaffPageIniFile, CurSection, "", "MAIN_TITLE", MainTitle.Caption)
    PageCaption(CurTabIdx) = GetIniEntry(StaffPageIniFile, CurSection, "", "PAGE_TITLE", CurPageTitle)
    tmpVal = Trim(Str((Me.Width / Screen.TwipsPerPixelX) - (DataTab(CurTabIdx).Left * 2))) & ",10"
    MyFieldLen = GetIniEntry(StaffPageIniFile, CurSection, "", "WIDTH_PIXEL", tmpVal)
    DataTab(CurTabIdx).HeaderLengthString = MyFieldLen + ",53,110,53,53"
    MyPageFields(CurTabIdx) = GetIniEntry(StaffPageIniFile, CurSection, "", "FIELD_LIST", "")
    MyTimeFields(CurTabIdx) = GetIniEntry(StaffPageIniFile, CurSection, "", "TIME_FIELDS", "")
    DataTab(CurTabIdx).LogicalFieldList = MyPageFields(CurTabIdx)
    CurPos = 0
    ItmNbr = 0
    CurCol = -1
    MyTimeCols(CurTabIdx) = ""
    EmptyLine(CurTabIdx) = ""
    Do
    
        ItmNbr = ItmNbr + 1
        CurCol = CurCol + 1
        FldNam = GetItem(MyPageFields(CurTabIdx), ItmNbr, ",")
        If FldNam <> "" Then
            tmpVal = GetItem(MyFieldLen, ItmNbr, ",")
            If tmpVal <> "" Then
                CurPos = CurPos + Val(tmpVal)
            End If
            If InStr(MyTimeFields(CurTabIdx), FldNam) > 0 Then
                DataTab(CurTabIdx).DateTimeSetColumn CurCol
                DataTab(CurTabIdx).DateTimeSetInputFormatString CurCol, "YYYYMMDDhhmmss"
                DataTab(CurTabIdx).DateTimeSetOutputFormatString CurCol, "hh':'mm"
                MyTimeCols(CurTabIdx) = MyTimeCols(CurTabIdx) & CStr(CurCol) & ","
            End If
            EmptyLine(CurTabIdx) = EmptyLine(CurTabIdx) & ","
        End If
    Loop While FldNam <> ""
    EmptyLine(CurTabIdx) = Left(EmptyLine(CurTabIdx), Len(EmptyLine(CurTabIdx)) - 1)
    MyPageFieldCount(CurTabIdx) = CurCol
    DataTab(CurTabIdx).Width = CurPos + 1000
    DataTab(CurTabIdx).HeaderString = GetIniEntry(StaffPageIniFile, CurSection, "", "LIST_HEADER", "DATA LINES, ") + ",STA,SRT,MRK,VER"
    DataTab(CurTabIdx).LeftTextOffset = Val(GetIniEntry(StaffPageIniFile, CurSection, "", "LEFT_MARGIN", "0"))
    DataTab(CurTabIdx).Left = 2
    'EmptyLine = GetIniEntry(StaffPageIniFile, CurSection, "", "EMPTY_LINE", EmptyLine)
    MyCedaTitle(CurTabIdx) = GetIniEntry(StaffPageIniFile, CurSection, "", "CEDA_TITLE", "LOAD")
    MyDataTable(CurTabIdx) = GetIniEntry(StaffPageIniFile, CurSection, "", "TABLE_NAME", MyDataTable(CurTabIdx))
    MyPageSqlKey(CurTabIdx) = GetIniEntry(StaffPageIniFile, CurSection, "", "USE_SQL_KEY", "WHERE URNO=0")
    MyTrigger01(CurTabIdx) = GetIniEntry(StaffPageIniFile, CurSection, "", "TRIGGER_01", "-60")
    MyTrigger02(CurTabIdx) = GetIniEntry(StaffPageIniFile, CurSection, "", "TRIGGER_02", "60")
    MyTrigger03(CurTabIdx) = GetIniEntry(StaffPageIniFile, CurSection, "", "TRIGGER_03", "-10")
    UseBcNums(CurTabIdx) = GetIniEntry(StaffPageIniFile, CurSection, "", "BC_NUMBERS", "ALL")
    MyUrnoIdx(CurTabIdx) = GetItemNo(MyPageFields(CurTabIdx), "URNO") - 1
    MyFlnoIdx(CurTabIdx) = GetItemNo(MyPageFields(CurTabIdx), "FLNO") - 1
    MyCkifIdx(CurTabIdx) = GetItemNo(MyPageFields(CurTabIdx), "CKIF") - 1
    MyCkitIdx(CurTabIdx) = GetItemNo(MyPageFields(CurTabIdx), "CKIT") - 1
    MyStodIdx(CurTabIdx) = GetItemNo(MyPageFields(CurTabIdx), "STOD") - 1
    CdiPageFields(CurTabIdx) = GetIniEntry(StaffPageIniFile, CurSection, "", "CDI_FLD_LST", "")
    CdiFieldLength(CurTabIdx) = GetIniEntry(StaffPageIniFile, CurSection, "", "CDI_FLD_LEN", "")
    CdiKeyFields(CurTabIdx) = GetIniEntry(StaffPageIniFile, CurSection, "", "CDI_KEY_FLD", "")
    CdiTriggers(CurTabIdx) = GetIniEntry(StaffPageIniFile, CurSection, "", "CDI_TRIGGER", "")
    DataTab(CurTabIdx).LifeStyle = True
    DataTab(CurTabIdx).CursorLifeStyle = True
    SupportCCaTab(CurTabIdx) = False
    tmpdat = GetIniEntry(StaffPageIniFile, CurSection, "", "SUPPORT_CCATAB", "YES")
    If tmpdat = "YES" Then SupportCCaTab(CurTabIdx) = True
    'DataTab(CurTabIdx).GridLineColor = DarkLineColor
    'DataTab(CurTabIdx).GridLineColor = vbBlack
    
    DataTab(CurTabIdx).CreateDecorationObject "CIC1", "T,L", "2,2", Str(vbWhite) & "," & Str(vbWhite)
    DataTab(CurTabIdx).CreateDecorationObject "CIC2", "T,R", "2,3", Str(vbWhite) & "," & Str(vbBlack)
    DataTab(CurTabIdx).CreateDecorationObject "CIC3", "L", "2", Str(vbWhite)
    DataTab(CurTabIdx).CreateDecorationObject "CIC4", "R", "3", Str(vbBlack)
    DataTab(CurTabIdx).CreateDecorationObject "CIC5", "B,L", "3,2", Str(vbBlack) & "," & Str(vbWhite)
    DataTab(CurTabIdx).CreateDecorationObject "CIC6", "B,R", "3,3", Str(vbBlack) & "," & Str(vbBlack)

    
End Sub

Private Sub QueryFlights(CurTabIdx As Integer)
    Dim ret As Integer
    Dim MaxItm As Integer
    Dim CurItm As Integer
    Dim Where As String
    Dim i As Integer
    Dim Records As Long
    Dim LineCount As Integer
    Dim strRawBuffer As String
    Dim FldNam As String
    Dim FldDat As String
    Dim CurFlno As String
    Dim CurUrno As String
    Dim CurCkif As String
    Dim CurCkit As String
    Dim CurStod As String
    Dim tmpUrno As String
    Dim CurLinNbr As String
    Dim tmpLinNbr As String
    Dim LinCnt As Integer
    Dim newRecDat As String
    Dim CcaRecDat As String
    Dim strTotalBuffer As String
    Dim CurAction As String
    Dim CedaWasConnected As Boolean
    Dim DarkLine As Boolean
    Dim CcatabWhere As String
    Dim CCaCnt As Long
    Dim CurLine As Long
    Dim MaxLine As Long
    Dim UpdLine As Long
    Screen.MousePointer = 11
    tmrMinutes.Interval = 0
    tmrSeconds.Interval = 0
    Where = MakeWhere(CurTabIdx, CcatabWhere)
    CloseConnection False
    OpenConnection False
    If CcatabWhere <> "" Then
        If (MyUrnoIdx(CurTabIdx) < 0) Then SupportCCaTab(CurTabIdx) = False
        If (MyCkifIdx(CurTabIdx) < 0) Then SupportCCaTab(CurTabIdx) = False
        If (MyCkitIdx(CurTabIdx) < 0) Then SupportCCaTab(CurTabIdx) = False
        If (MyFlnoIdx(CurTabIdx) < 0) Then SupportCCaTab(CurTabIdx) = False
        If (MyStodIdx(CurTabIdx) < 0) Then SupportCCaTab(CurTabIdx) = False
        If SupportCCaTab(CurTabIdx) Then BasicData.LoadCcaTab CcatabWhere, False
    End If
    PrepareMessage MyCedaTitle(CurTabIdx)
    CedaWasConnected = False
    If CedaIsConnected = True Then
        CedaWasConnected = True
        If UpdateOnReload = True Then
            MarkTabLines CurTabIdx, -1, MyPageFieldCount(CurTabIdx) + 2, "OLD"
        End If
        strTotalBuffer = ""
        If PageSectionFound(CurTabIdx) = "YES" Then
            CedaCon(0).FillColor = vbGreen
            MsgLogging UCase(App.EXEName) & " LOADING " & omDisplayMode
            ReadCedaData CdrHdl, MyDataTable(CurTabIdx), MyPageFields(CurTabIdx), Where
            LineCount = CdrHdl.GetLineCount - 1
            MaxItm = ItemCount(MyPageFields(CurTabIdx), ",")
            LinCnt = 0
            For i = 0 To LineCount
                newRecDat = CdrHdl.GetLineValues(i)
                FldDat = GetFieldValue("MTOW", newRecDat, MyPageFields(CurTabIdx))
                If FldDat <> "" Then
                    FldDat = Trim(GetItem(FldDat, 1, "."))
                    FldDat = Right("00000" + FldDat, 5)
                    SetFieldValue "MTOW", FldDat, newRecDat, MyPageFields(CurTabIdx)
                End If
                If UpdateOnReload = True Then
                    CurLinNbr = Right("000000" + CStr(LinCnt), 6)
                    tmpLinNbr = CurLinNbr & "_0"
                    newRecDat = newRecDat + ",NEW," + tmpLinNbr + ",NEW,"
                    UpdLine = RefreshOnBc("UFR", MyPageFields(CurTabIdx), newRecDat, CurTabIdx, CurLinNbr, False)
                    If (UpdLine >= 0) And (SupportCCaTab(CurTabIdx) = True) Then
                        CurUrno = GetFieldValue("URNO", newRecDat, MyPageFields(CurTabIdx))
                        CurFlno = GetFieldValue("FLNO", newRecDat, MyPageFields(CurTabIdx))
                        CurCkif = GetFieldValue("CKIF", newRecDat, MyPageFields(CurTabIdx))
                        CurCkit = GetFieldValue("CKIT", newRecDat, MyPageFields(CurTabIdx))
                        CurStod = GetFieldValue("STOD", newRecDat, MyPageFields(CurTabIdx))
                        'Test:
                        'If CurCkit = "S08" Then
                        '    CurCkit = "106"
                        '    DataTab(CurTabIdx).SetColumnValue UpdLine, MyCkitIdx(CurTabIdx), CurCkit
                        'End If
                        CCaCnt = 0
                        If CurCkif <> "" Then CCaCnt = BasicData.LookUpCca(CurUrno, CurFlno, CurStod, CurCkif, CurCkit, False)
                        If CCaCnt > 0 Then
                            DataTab(CurTabIdx).SetDecorationObject UpdLine, MyCkifIdx(CurTabIdx), "CIC1"
                            DataTab(CurTabIdx).SetDecorationObject UpdLine, MyCkitIdx(CurTabIdx), "CIC2"
                            MaxLine = CCaCnt - 1
                            For CurLine = 0 To MaxLine
                                CcaRecDat = EmptyLine(CurTabIdx)
                                tmpUrno = CurUrno & "_" & CStr(CurLine + 1)
                                tmpLinNbr = CurLinNbr & "_" & CStr(CurLine + 1)
                                FldDat = BasicData.CicGrp(1).GetColumnValue(CurLine, 1)
                                SetItem CcaRecDat, (MyCkifIdx(CurTabIdx) + 1), ",", FldDat
                                FldDat = BasicData.CicGrp(1).GetColumnValue(CurLine, 2)
                                SetItem CcaRecDat, (MyCkitIdx(CurTabIdx) + 1), ",", FldDat
                                SetItem CcaRecDat, (MyUrnoIdx(CurTabIdx) + 1), ",", tmpUrno
                                CcaRecDat = CcaRecDat + ",NEW," + tmpLinNbr + ",NEW,"
                                UpdLine = RefreshOnBc("UFR", MyPageFields(CurTabIdx), CcaRecDat, CurTabIdx, CurLinNbr, False)
                                DataTab(CurTabIdx).SetDecorationObject UpdLine, MyCkifIdx(CurTabIdx), "CIC3"
                                DataTab(CurTabIdx).SetDecorationObject UpdLine, MyCkitIdx(CurTabIdx), "CIC4"
                            Next
                            DataTab(CurTabIdx).SetDecorationObject UpdLine, MyCkifIdx(CurTabIdx), "CIC5"
                            DataTab(CurTabIdx).SetDecorationObject UpdLine, MyCkitIdx(CurTabIdx), "CIC6"
                        End If
                    End If
                Else
                    strTotalBuffer = strTotalBuffer + newRecDat + vbLf
                End If
                LinCnt = LinCnt + 1
            Next i
        End If
        If UpdateOnReload = True Then
            DeleteOldTabLines CurTabIdx, MyPageFieldCount(CurTabIdx) + 2, "OLD"
            DataTab(CurTabIdx).Sort MyPageFieldCount(CurTabIdx) + 1, True, True
        Else
            DataTab(CurTabIdx).ResetContent
            Records = DataTab(CurTabIdx).InsertBuffer(strTotalBuffer, vbLf)
        End If
        tmpUrno = ""
        CurUrno = ""
        DarkLine = False
        LineCount = DataTab(CurTabIdx).GetLineCount - 1
        For i = 0 To LineCount
            If MyUrnoIdx(CurTabIdx) >= 0 Then
                tmpUrno = DataTab(CurTabIdx).GetColumnValue(i, MyUrnoIdx(CurTabIdx))
                tmpUrno = GetItem(tmpUrno, 1, "_")
                If tmpUrno <> CurUrno Then DarkLine = Not DarkLine
                If DarkLine Then
                  DataTab(CurTabIdx).SetLineColor i, LineTextColor, DarkLineColor
                  DataTab(CurTabIdx).SetLineTag i, CStr(DarkLineColor)
                Else
                  DataTab(CurTabIdx).SetLineColor i, LineTextColor, LightLineColor
                  DataTab(CurTabIdx).SetLineTag i, CStr(LightLineColor)
                End If
                CurUrno = tmpUrno
            Else
                If (i Mod 2) = 0 Then
                  DataTab(CurTabIdx).SetLineColor i, LineTextColor, DarkLineColor
                  DataTab(CurTabIdx).SetLineTag i, CStr(DarkLineColor)
                Else
                  DataTab(CurTabIdx).SetLineColor i, LineTextColor, LightLineColor
                  DataTab(CurTabIdx).SetLineTag i, CStr(LightLineColor)
                End If
            End If
        Next i
        DataTab(CurTabIdx).OnVScrollTo currScrollPos(CurTabIdx)
    End If
    
    AppendLines CurTabIdx
    CloseConnection False
    
    If CedaWasConnected = True Then
        RefreshInd(0).FillColor = RemoteColor(0)
        RefreshInd(1).FillColor = RemoteColor(0)
        lblRefresh(0) = "LOADED"
        lblRefresh(1) = Format(Now, "hh:mm:ss")
        BcRcvInd(0).FillColor = &HC000&
        BcRcvInd(1).FillColor = &HC000&
        BcUpdInd(0).FillColor = &HC000&
        BcUpdInd(1).FillColor = &HC000&
    Else
        RefreshInd(0).FillColor = vbRed
        RefreshInd(1).FillColor = vbRed
        BcRcvInd(0).FillColor = vbRed
        BcRcvInd(1).FillColor = vbRed
        BcUpdInd(0).FillColor = vbRed
        BcUpdInd(1).FillColor = vbRed
        lblRefresh(0) = "SERVER"
        lblRefresh(1) = MyHostName
    End If
    LostBroadCast(CurTabIdx) = False
    RefreshCountDown(CurTabIdx) = False
    If MinCountDown(CurTabIdx) < 0 Then MinCountDown(CurTabIdx) = RefreshCyclus
    DataTab(CurTabIdx).RedrawTab
    tmrMinutes.Interval = 60000
    tmrSeconds.Interval = 1000
    Screen.MousePointer = 0
End Sub

Private Sub btnCdiServer_Click()
    If btnCdiServer.Value = 1 Then
        CdiList.Show
    Else
        Unload CdiList
    End If
End Sub

Private Sub btnExit_Click()
    Dim ret As Integer
    Screen.MousePointer = 11
    ShutDownRequested = True
    RemotePort_Close 0
    If MyHostName <> "LOCAL" Then
        tmrSeconds.Enabled = False
        tmrMinutes.Enabled = False
        BcSpoolTimer.Enabled = False
        BcPortConnected = False
        If Not RemoteControl Then
            MsgLogging UCase(App.EXEName) & " LOGOFF FROM CEDA"
            OpenConnection True
            'If ServerIsAvailable = True Then
                PrepareMessage "LOGOFF"
                If CedaIsConnected = True Then
                    ret = Ufis.CallServer("SBC", "STAFF/CLOSE", "ALL FIELDS", MyTcpIpAdr, HexTcpAdr, "240")
                End If
            'End If
            CloseConnection True
            MsgLogging UCase(App.EXEName) & " DISCONNECTED"
            MsgLogging UCase(App.EXEName) & " EXIT"
        End If
    End If
    Screen.MousePointer = 0
    End
End Sub

Private Sub QueryLoadData(ByVal UseButton As String, LoadData As Boolean)
    If Not RemoteControl Then
        If GotUtcDiff = False Then
            UTC_Diff = GetUTCDifference()
            If TimesInLocal = True Then lblNow.Value = 1
        End If
        If (UseButton = "LEFT") Or (UseButton = "BOTH") Then
            ReloadData(0) = LoadData
            chkLeftAction.Value = 0
            chkLeftAction.Value = 1
        End If
        If (UseButton = "RIGHT") Or (UseButton = "BOTH") Then
            ReloadData(1) = LoadData
            chkRightAction.Value = 0
            chkRightAction.Value = 1
        End If
    End If
    If Not ApplicationIsStarted Then
        If RemoteControl Then
            RefreshCountDown(0) = False
            RefreshCountDown(1) = False
            ShowRemote.Show , Me
            tmrSeconds.Interval = 0
            tmrMinutes.Interval = 0
            lblRefresh(1).Caption = "00:00:00"
            lblRefresh(0).Caption = "REMOTE"
            lblNow.Caption = "00.00.00 / 00:00:00"
            lblUTC.Caption = "00:00:00"
        End If
        ApplicationIsStarted = True
    End If
    If UseButton = "LEFT" Or (UseButton = "BOTH") Then
        ReloadData(0) = False
    End If
    If UseButton = "RIGHT" Or (UseButton = "BOTH") Then
        ReloadData(1) = False
    End If
End Sub


Private Sub chkAlive_Click()
    If chkAlive.Value = 1 Then
        MyAboutBox.SetLifeStyle = True
        MyAboutBox.VersionTab.LifeStyle = True
        MyAboutBox.Show , Me
        MyAboutBox.VersionTab.InsertTextLine "UfisBroadcasts.ocx,1.0.0.2,Nov 25 2001 - 14:56:22,N.A.", False
        MyAboutBox.VersionTab.AutoSizeColumns
        chkAlive.Value = 0
    End If
End Sub

Private Sub chkHide_Click()
    If chkHide.Value = 1 Then
        Hook Me.hwnd   ' Set up our handler
        AddIconToTray Me.hwnd, Me.Icon, Me.Icon.Handle, App.EXEName + " / " + MainTitle.Caption
        Me.Hide
        chkHide.Value = 0
    End If
End Sub

Private Sub chkLeftAction_Click()
    Me.Refresh
    If chkLeftAction.Value = 1 Then
        chkRightAction.Value = 0
        TabIdx = 0
        omDisplayMode = "LEFT"
        If TestMode Then GetPageConfig "LEFT", PageCaption(0), 0
        If (ReloadData(0) = True) Or (TestMode = True) Then
            CurLoadButton = "RIGHT"
            QueryFlights 0
        End If
        If (ReloadData(0) = False) Or (TestMode = True) Then
            MainTitle.Caption = MainCaption(0)
            PageTitle.Caption = PageCaption(0)
            DataTab(0).Visible = True
            DataTab(1).Visible = False
        End If
        ReloadData(0) = False
        If ToggleButtons = "NO" Then chkLeftAction.Value = 0
    End If
End Sub

Private Sub chkRightAction_Click()
    Me.Refresh
    If chkRightAction.Value = 1 Then
        chkLeftAction.Value = 0
        TabIdx = 1
        omDisplayMode = "RIGHT"
        If TestMode Then GetPageConfig "RIGHT", PageCaption(1), 1
        If (ReloadData(1) = True) Or (TestMode = True) Then
            CurLoadButton = "RIGHT"
            QueryFlights 1
        End If
        If (ReloadData(1) = False) Or (TestMode = True) Then
            MainTitle.Caption = MainCaption(1)
            PageTitle.Caption = PageCaption(1)
            DataTab(1).Visible = True
            DataTab(0).Visible = False
        End If
        ReloadData(1) = False
        If ToggleButtons = "NO" Then chkRightAction.Value = 0
    End If
End Sub

Private Sub btnNext_Click()
    Dim diff As Integer
    Dim tmpUrno As String
    If (currScrollPos(TabIdx) + ListLines(TabIdx)) < DataTab(TabIdx).GetLineCount - ListLines(TabIdx) Then
        currScrollPos(TabIdx) = currScrollPos(TabIdx) + ListLines(TabIdx)
    Else
        diff = ((DataTab(TabIdx).GetLineCount - ListLines(TabIdx)) - currScrollPos(TabIdx))
        currScrollPos(TabIdx) = currScrollPos(TabIdx) + diff
    End If
    tmpUrno = DataTab(TabIdx).GetColumnValue(currScrollPos(TabIdx), MyUrnoIdx(TabIdx))
    While InStr(tmpUrno, "_") > 0
        currScrollPos(TabIdx) = currScrollPos(TabIdx) - 1
        tmpUrno = DataTab(TabIdx).GetColumnValue(currScrollPos(TabIdx), MyUrnoIdx(TabIdx))
    Wend
    DataTab(TabIdx).OnVScrollTo currScrollPos(TabIdx)
End Sub

Private Sub btnPrev_Click()
    Dim tmpUrno As String
    Dim LineNo As Long
    If currScrollPos(TabIdx) >= 0 Then
        currScrollPos(TabIdx) = currScrollPos(TabIdx) - ListLines(TabIdx)
    End If
    If currScrollPos(TabIdx) < 0 Then
        currScrollPos(TabIdx) = 0
    End If
    If currScrollPos(TabIdx) > 0 Then
        LineNo = currScrollPos(TabIdx) + ListLines(TabIdx)
        tmpUrno = DataTab(TabIdx).GetColumnValue(LineNo, MyUrnoIdx(TabIdx))
        While InStr(tmpUrno, "_") > 0
            currScrollPos(TabIdx) = currScrollPos(TabIdx) + 1
            LineNo = currScrollPos(TabIdx) + ListLines(TabIdx)
            tmpUrno = DataTab(TabIdx).GetColumnValue(LineNo, MyUrnoIdx(TabIdx))
        Wend
    End If
    DataTab(TabIdx).OnVScrollTo currScrollPos(TabIdx)
End Sub


Private Sub AppendLines(CurTabIdx As Integer)
    Dim AddLinCnt As Long
    Dim CurLinCnt As Long
    Dim AddLin As Long
    Dim NewLine As String
    CurLinCnt = DataTab(CurTabIdx).GetLineCount
    AddLinCnt = ListLines(CurTabIdx) - CurLinCnt
    If AddLinCnt > 0 Then
        NewLine = EmptyLine(CurTabIdx) + ",,,,"
        CurLinCnt = CurLinCnt - 1
        For AddLin = 1 To AddLinCnt
            DataTab(CurTabIdx).InsertTextLine NewLine, False
            CurLinCnt = CurLinCnt + 1
            If (CurLinCnt Mod 2) = 0 Then
                DataTab(CurTabIdx).SetLineColor CurLinCnt, LineTextColor, DarkLineColor
                DataTab(CurTabIdx).SetLineTag CurLinCnt, CStr(DarkLineColor)
            Else
                DataTab(CurTabIdx).SetLineColor CurLinCnt, LineTextColor, LightLineColor
                DataTab(CurTabIdx).SetLineTag CurLinCnt, CStr(LightLineColor)
            End If
        Next
        DataTab(CurTabIdx).RedrawTab
    End If
End Sub

Private Sub DataTab_SendLButtonClick(Index As Integer, ByVal LineNo As Long, ByVal ColNo As Long)
    DataTab(Index).SetCurrentSelection -1
End Sub

Private Sub DataTab_SendLButtonDblClick(Index As Integer, ByVal LineNo As Long, ByVal ColNo As Long)
    DataTab(Index).SetCurrentSelection -1
End Sub

Private Sub DataTab_SendRButtonClick(Index As Integer, ByVal LineNo As Long, ByVal ColNo As Long)
    Dim tmpFld As String
    Dim tmpdat As String
    Dim tmpGrp As String
    Dim UseIdx As Integer
    Dim GetFldLst As String
    Dim tmpResult As String
    Dim CurUrno As String
    Dim CurFlno As String
    Dim CurStod As String
    Dim CurCkif As String
    Dim CurCkit As String
    Dim tmpCount As Long
    Dim CurLine As Long
    Dim CurBackColor As Long
    If LineNo >= 0 Then
        CurBackColor = Val(DataTab(Index).GetLineTag(LineNo))
        DataTab(Index).SetLineColor LineNo, vbBlack, CurBackColor
        UseIdx = -1
        tmpFld = GetRealItem(MyPageFields(Index), ColNo, ",")
        tmpdat = Trim(DataTab(Index).GetColumnValue(LineNo, ColNo))
        Select Case tmpFld
            Case "FLNO"
                tmpdat = Trim(Left(tmpdat, 3))
                GetFldLst = "ALC2,ALC3,ALFN"
                UseIdx = 0
            Case "ORG3", "DES3", "VIA3"
                GetFldLst = "APC3,APC4,APFN"
                UseIdx = 1
            Case "ACT3", "ACT5", "ACTI"
                GetFldLst = "ACT3,ACT5,ACFN"
                UseIdx = 2
            Case "CKIF", "CKIT"
                tmpdat = DataTab(Index).GetLineValues(LineNo)
                CurUrno = GetFieldValue("URNO", tmpdat, MyPageFields(Index))
                If InStr(CurUrno, "_") > 0 Then CurUrno = ""
                If CurUrno <> "" Then
                    CurFlno = GetFieldValue("FLNO", tmpdat, MyPageFields(Index))
                    CurCkif = GetFieldValue("CKIF", tmpdat, MyPageFields(Index))
                    CurCkit = GetFieldValue("CKIT", tmpdat, MyPageFields(Index))
                    CurStod = GetFieldValue("STOD", tmpdat, MyPageFields(Index))
                    UseIdx = 99
                End If
            Case Else
        End Select
        If (UseIdx >= 0) And (UseIdx < 99) Then
            If tmpdat <> "" Then
                tmpResult = BasicData.LookUpData(UseIdx, tmpFld, tmpdat, GetFldLst)
                If tmpResult <> "" Then
                    tmpResult = Replace(tmpResult, ",", vbNewLine, 1, -1, vbBinaryCompare)
                    tmpResult = tmpResult & "              "
                    MyMsgBox.AskUser 0, 0, 0, "Basic Data Info", tmpResult, "infomsg", "", UserAnswer
                End If
            End If
        End If
        If UseIdx = 99 Then
            tmpCount = BasicData.LookUpCca(CurUrno, CurFlno, CurStod, CurCkif, CurCkit, True)
            tmpResult = ""
            If tmpCount > 0 Then
                For CurLine = 0 To tmpCount - 1
                    tmpGrp = BasicData.CicGrp(1).GetLineValues(CurLine)
                    tmpdat = GetItem(tmpGrp, 1, ",")
                    tmpGrp = Replace(tmpGrp, (tmpdat & ","), (tmpdat & ":  "), 1, -1, vbBinaryCompare)
                    tmpGrp = Replace(tmpGrp, ",(", "  (", 1, -1, vbBinaryCompare)
                    tmpResult = tmpResult & tmpGrp & vbNewLine
                Next
                tmpResult = Left(tmpResult, (Len(tmpResult) - 2))
                tmpResult = Replace(tmpResult, ",", " - ", 1, -1, vbBinaryCompare)
                tmpResult = tmpResult & "        "
            End If
            If tmpResult = "" Then tmpResult = "No counters planned or opened."
            MyMsgBox.AskUser 0, 0, 0, "Full Checkin Counter Info", tmpResult, "infomsg", "", UserAnswer
        End If
        DataTab(Index).SetLineColor LineNo, LineTextColor, CurBackColor
        'DataTab(Index).CursorLifeStyle = False
    End If
    DataTab(Index).SetCurrentSelection -1
End Sub

Private Sub DataTab_TimerExpired(Index As Integer, ByVal LineNo As Long, ByVal LineStatus As Long)
    Dim CurColor As Long
    DataTab(Index).ResetCellProperties LineNo
    CurColor = Val(DataTab(Index).GetLineTag(LineNo))
    DataTab(Index).SetLineColor LineNo, LineTextColor, CurColor
    DataTab(Index).RedrawTab
    If DataTab(Index).GetLineStatusValue(LineNo) > 0 Then UpdateCount = UpdateCount - 1
    DataTab(Index).SetLineStatusValue LineNo, 0
    ToggleUpdLed
    If UpdateCount < 0 Then UpdateCount = 0
End Sub

Private Sub Form_Activate()
    Static IsInit As Boolean
    If Not IsInit Then
        Me.Refresh
        Load BasicData
        IsInit = True
    End If
End Sub

Private Sub Form_Load()
    Set MyMainForm = Me
    MyWksName = CdiPort(0).LocalHostName
    DarkLineColor = &HC00000
    LightLineColor = &HFF0000
    LineTextColor = vbYellow
    InitApplication
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    If UnloadMode = 3 Then
        Cancel = False
        btnExit_Click
    End If
End Sub

Private Sub Form_Resize()
    Dim NewSize As Long
    NewSize = Me.ScaleWidth - (DataTab(0).Left * 2)
    DataTab(0).Width = NewSize
    DataTab(1).Width = NewSize
End Sub

Private Sub Form_Unload(Cancel As Integer)
    If Not ShutDownRequested Then
        btnExit_Click
    End If
End Sub

Private Sub Frame1_Click()
    chkAlive.Value = 1
End Sub

Private Sub lblNow_Click()
    Dim tmpCol As String
    Dim ColNo As Long
    Dim tmpItm As Integer
    Dim i As Integer
    If lblNow.Value = 1 Then
        For i = 0 To 1
            tmpItm = 1
            tmpCol = GetItem(MyTimeCols(i), tmpItm, ",")
            While tmpCol <> ""
                ColNo = Val(tmpCol)
                DataTab(i).DateTimeSetUTCOffsetMinutes ColNo, UTC_Diff
                tmpItm = tmpItm + 1
                tmpCol = GetItem(MyTimeCols(i), tmpItm, ",")
            Wend
            DataTab(i).Refresh
            MainCaption(i) = "TIMES IN LOCAL"
        Next
        MainTitle.Tag = "LOC"
        MainTitle.Caption = MainCaption(TabIdx)
        lblNow.Value = 0
    End If
End Sub

Private Sub lblRefresh_Click(Index As Integer)
    chkAlive.Value = 1
End Sub

Private Sub lblUTC_Click()
    Dim tmpCol As String
    Dim ColNo As Long
    Dim tmpItm As Integer
    Dim i As Integer
    If lblUTC.Value = 1 Then
        For i = 0 To 1
            tmpItm = 1
            tmpCol = GetItem(MyTimeCols(i), tmpItm, ",")
            While tmpCol <> ""
                ColNo = Val(tmpCol)
                DataTab(i).DateTimeSetUTCOffsetMinutes ColNo, 0
                tmpItm = tmpItm + 1
                tmpCol = GetItem(MyTimeCols(i), tmpItm, ",")
            Wend
            DataTab(i).Refresh
            MainCaption(i) = "TIMES IN UTC"
        Next
        MainTitle.Caption = MainCaption(TabIdx)
        MainTitle.Tag = "UTC"
        lblUTC.Value = 0
    End If
End Sub

Private Sub MainTitle_Click()
    If MainTitle.Value = 1 Then
        If MainTitle.Tag = "LOC" Then
            lblUTC.Value = 1
        Else
            lblNow.Value = 1
        End If
        MainTitle.Value = 0
    End If
End Sub

Private Sub PageTitle_Click()
    If PageTitle.Value = 1 Then
        If ShowBasics = True Then
            BasicData.Show
            BasicData.Refresh
        End If
        If ReloadOnDemand = True Then
            BasicData.CcaTab(0).myTag = ""
            BasicData.CcaTab(1).myTag = ""
            QueryFlights 0
            QueryFlights 1
        End If
        PageTitle.Value = 0
    End If
End Sub

Private Sub tmrSeconds_Timer()
    Dim tmpTime As Date
    Dim tmpButton
    Dim CurTabIdx As Integer
    Dim MinSec As Integer
    If ShutDownRequested = True Then
        btnExit_Click
    End If
    lblNow.Caption = Format(Now, "dd.mm.yy / hh:mm:ss")
    If GotUtcDiff = True Then
        tmpTime = DateAdd("n", -UTC_Diff, Now)
        lblUTC.Caption = Format(tmpTime, "hh:mm:ss")
    End If
    If Not CdiServer Then
        If RemoteCon(0).Visible = True Then
            If Val(RemoteCon(0).Tag) <= 0 Then
                RemoteCon(0).Visible = False
                RemoteCon(1).Visible = False
            Else
                RemoteCon(0).Tag = Str(Val(RemoteCon(0).Tag) - 1)
            End If
        End If
        MinSec = 1000
        For CurTabIdx = 0 To 1
            If RefreshCountDown(CurTabIdx) = True Then
                SecCountDown(CurTabIdx) = SecCountDown(CurTabIdx) - 1
                If SecCountDown(CurTabIdx) < MinSec Then MinSec = SecCountDown(CurTabIdx)
                If SecCountDown(CurTabIdx) <= 0 Then
                    tmpButton = CurLoadButton
                    MinSec = 1000
                    If CurTabIdx = 0 Then
                        CurLoadButton = "LEFT"
                        QueryLoadData CurLoadButton, True
                    Else
                        CurLoadButton = "RIGHT"
                        QueryLoadData CurLoadButton, True
                    End If
                    If tmpButton <> CurLoadButton Then
                        CurLoadButton = MyLoadButton
                        omDisplayMode = MyLoadButton
                    End If
                End If
            Else
                If LostBroadCast(CurTabIdx) Then
                    If RefreshInd(CurTabIdx).FillColor = vbRed Then
                        RefreshInd(CurTabIdx).FillColor = vbYellow
                    Else
                        RefreshInd(CurTabIdx).FillColor = vbRed
                    End If
                End If
            End If
            DataTab(CurTabIdx).TimerCheck
        Next
        If MinSec < 1000 Then
            lblRefresh(1).Caption = Trim(Str(MinSec)) & " SEC"
            If RefreshInd(0).FillColor = RemoteColor(0) Then
                RefreshInd(0).FillColor = RemoteColor(1)
                RefreshInd(1).FillColor = RemoteColor(1)
            Else
                RefreshInd(0).FillColor = RemoteColor(0)
                RefreshInd(1).FillColor = RemoteColor(0)
            End If
        End If
    End If
End Sub

Private Sub tmrMinutes_Timer()
    Dim CurTabIdx As Integer
    If Not CdiServer Then
        If DefSaveTimer > 0 Then
            ActSaveTimer = ActSaveTimer - 1
            If ActSaveTimer <= 0 Then
                WriteDataToFile 0
                WriteDataToFile 1
                ActSaveTimer = DefSaveTimer
            End If
        End If
        For CurTabIdx = 0 To 1
            MinCountDown(CurTabIdx) = MinCountDown(CurTabIdx) - 1
            If MinCountDown(CurTabIdx) = 1 Then
                If Not RefreshCountDown(CurTabIdx) Then
                    SecCountDown(CurTabIdx) = 60
                    RefreshCountDown(CurTabIdx) = True
                    lblRefresh(0).Caption = "RELOAD"
                End If
            End If
            If MinCountDown(CurTabIdx) <= 0 Then MinCountDown(CurTabIdx) = RefreshCyclus
        Next
    End If
End Sub

Private Function MakeWhere(CurTabIdx As Integer, CcatabWhere) As String
    Dim MyDate As Date
    Dim x1 As Date
    Dim x2 As Date
    Dim x3 As Date
    Dim strX1 As String
    Dim strX2 As String
    Dim strX3 As String
    Dim strWhere As String
    MyDate = DateAdd("n", -UTC_Diff, Now)
    x1 = DateAdd("n", Val(MyTrigger01(CurTabIdx)), MyDate)
    x2 = DateAdd("n", Val(MyTrigger02(CurTabIdx)), MyDate)
    x3 = DateAdd("n", Val(MyTrigger03(CurTabIdx)), MyDate)
    strX1 = Format(x1, "yyyymmddhhmmss")
    strX2 = Format(x2, "yyyymmddhhmmss")
    strX3 = Format(x3, "yyyymmddhhmmss")
    strWhere = MyPageSqlKey(CurTabIdx)
    strWhere = Replace(strWhere, "[TRIGGER_01]", strX1, 1, -1, vbBinaryCompare)
    strWhere = Replace(strWhere, "[TRIGGER_02]", strX2, 1, -1, vbBinaryCompare)
    strWhere = Replace(strWhere, "[TRIGGER_03]", strX3, 1, -1, vbBinaryCompare)
    CcatabWhere = ""
    If SupportCCaTab(CurTabIdx) = True Then
        x1 = DateAdd("n", -240, x1)
        x2 = DateAdd("n", 240, x2)
        strX1 = Format(x1, "yyyymmddhhmmss")
        strX2 = Format(x2, "yyyymmddhhmmss")
        MyCcaVpfr = strX1
        MyCcaVpto = strX2
        CcatabWhere = CcatabWhere & "((CKBS BETWEEN '[TRIGGER_01]' AND '[TRIGGER_02]')"
        CcatabWhere = CcatabWhere & " OR "
        CcatabWhere = CcatabWhere & "(CKES BETWEEN '[TRIGGER_01]' AND '[TRIGGER_02]'))"
        'CcatabWhere = CcatabWhere & " AND CKEA=' '"
        CcatabWhere = CcatabWhere & " AND CKIC<>' '"
        'CcatabWhere = CcatabWhere & " AND (CKIC LIKE 'E%' OR CKIC LIKE 'S%')"
        CcatabWhere = Replace(CcatabWhere, "[TRIGGER_01]", strX1, 1, -1, vbBinaryCompare)
        CcatabWhere = Replace(CcatabWhere, "[TRIGGER_02]", strX2, 1, -1, vbBinaryCompare)
    End If
    MakeWhere = strWhere
End Function

Private Sub RcvBc_ReceiveBroadcast(ByVal BcNum As String, ByVal DestName As String, ByVal RecvName As String, ByVal Cmd As String, ByVal ObjName As String, ByVal Selection As String, ByVal Fields As String, ByVal Data As String, ByVal twe As String, ByVal tws As String, ByVal OrigName As String)
    Dim BcText As String
    Dim TblBcNum As String
    If BcPortConnected Then
        ' Note: This program acts as server for other staff pages or interfaces.
        ' After the internal BC handling the changes will be send out to all
        ' connected clients by the subroutine "SendCdiMsg".
        ' The application must wait until each message is send completely and
        ' the only way to get the "SendComplete" information is to look for
        ' the related event on the windows message queue with "DoEvents".
        ' In order to avoid troubles in the workflow while DoEvents is invoked,
        ' all incoming BC's should not be processed immediately.
        ' BC's are stored in a BcSpooler and the evaluation is triggered by
        ' a timer of 5 milliseconds, which is disabled during "DoEvents".
        If BcRcvInd(0).FillColor = &HC000& Then
            BcRcvInd(0).FillColor = &HFF00&
            BcRcvInd(1).FillColor = &HC000&
        Else
            BcRcvInd(1).FillColor = &HFF00&
            BcRcvInd(0).FillColor = &HC000&
        End If
        If Trim(OrigName) = Trim(MyHostName) Then
            If Trim(ObjName) = MyDataTable(0) Or Trim(ObjName) = MyDataTable(1) Or Cmd = "CLO" Then
                BcText = BcNum + Chr(16) + DestName + Chr(16) + RecvName + Chr(16) + Cmd + Chr(16) + ObjName + Chr(16) + Selection + Chr(16) + Fields + Chr(16) + Data + Chr(16) + twe + Chr(16) + tws
                BcSpoolerTab.InsertTextLine BcText, False
                ToggleUpdLed
            End If
            If ((SupportCCaTab(0) = True) Or (SupportCCaTab(1) = True)) And (Trim(ObjName) = "CCATAB") Then
                TblBcNum = GetItem(twe, 3, ",")
                BasicData.RefreshCcaOnBc TblBcNum, Cmd, Selection, Fields, Data
            End If
        End If
    End If
End Sub

Private Sub BcSpoolTimer_Timer()
    If (Not CdiServer) And (BcSpoolerTab.GetLineCount > 0) Then
        GetBcFromSpooler
    End If
End Sub

Private Sub GetBcFromSpooler()
    Dim BcEvent As String
    Dim TblBcNum As String
    Dim NewBcNum As Integer
    Dim BcNum As String
    Dim DestName As String
    Dim RecvName As String
    Dim Cmd As String
    Dim Object As String
    Dim Selection As String
    Dim Fields As String
    Dim Data As String
    Dim twe As String
    Dim tws As String
    Dim BcSep As String
    Dim CurTabIdx As Integer
    If BcSpoolerTab.GetLineCount > 0 Then
        BcEvent = BcSpoolerTab.GetLineValues(0)
        BcSpoolerTab.DeleteLine 0
        BcSep = Chr(16)
        BcNum = GetItem(BcEvent, 1, BcSep)
        DestName = GetItem(BcEvent, 2, BcSep)
        RecvName = GetItem(BcEvent, 3, BcSep)
        Cmd = GetItem(BcEvent, 4, BcSep)
        Object = GetItem(BcEvent, 5, BcSep)
        Selection = GetItem(BcEvent, 6, BcSep)
        Fields = GetItem(BcEvent, 7, BcSep)
        Data = GetItem(BcEvent, 8, BcSep)
        twe = GetItem(BcEvent, 9, BcSep)
        tws = GetItem(BcEvent, 10, BcSep)
        'MsgBox BcEvent
        If Cmd = "CLO" Then
            End
        End If
        For CurTabIdx = 0 To 1
            If Trim(Object) = MyDataTable(CurTabIdx) Then
                TblBcNum = ""
                Select Case UseBcNums(CurTabIdx)
                    Case "ARR"
                        TblBcNum = GetItem(twe, 4, ",")
                    Case "DEP"
                        TblBcNum = GetItem(twe, 5, ",")
                    Case "ALL"
                        TblBcNum = GetItem(twe, 3, ",")
                End Select
                NewBcNum = Val(TblBcNum)
                If NewBcNum > 0 Then
                    If NewBcNum <> LastBcNum(CurTabIdx) Then
                        If LastBcNum(CurTabIdx) = 0 Then LastBcNum(CurTabIdx) = NewBcNum - 1
                        LastBcNum(CurTabIdx) = LastBcNum(CurTabIdx) + 1
                        If LastBcNum(CurTabIdx) > 30000 Then LastBcNum(CurTabIdx) = 1
                        If LastBcNum(CurTabIdx) <> NewBcNum Then
                            LostBroadCast(CurTabIdx) = True
                            If MinCountDown(CurTabIdx) > CountDownTime Then
                                MinCountDown(CurTabIdx) = CountDownTime
                            End If
                        End If
                        RefreshOnBc Cmd, Fields, Data, CurTabIdx, "", True
                    End If
                    LastBcNum(CurTabIdx) = NewBcNum
                Else
                    If (LastBcNum(CurTabIdx) > 0) And (NewBcNum <> 0) Then
                        If Abs(NewBcNum) <> LastBcNum(CurTabIdx) Then
                            LastBcNum(CurTabIdx) = Abs(NewBcNum)
                            LostBroadCast(CurTabIdx) = True
                            If MinCountDown(CurTabIdx) > CountDownTime Then
                                MinCountDown(CurTabIdx) = CountDownTime
                            End If
                        End If
                    End If
                End If
                ToggleUpdLed
            End If
        Next
    End If
End Sub

Private Function RefreshOnBc(CurCmd As String, CurFld As String, CurDat As String, CurTabIdx As Integer, TabLineNo As String, CheckCic As Boolean) As Long
    Dim TimeFields As String
    Dim tmpOrig As String
    Dim tmpDest As String
    Dim tmpUrno As String
    Dim MsgTxt As String
    Dim ChgLst As String
    Dim OldLst As String
    Dim NewLst As String
    Dim tmpList As String
    Dim tmpRecIdx As Long
    Dim LineNo As Long
    Dim UpdLineNo As Long
    Dim tmpRecDat As String
    Dim newRecDat As String
    Dim OutRecDat As String
    Dim CurItm As Integer
    Dim MaxItm As Integer
    Dim FldNam As String
    Dim FldIdx As Integer
    Dim FldDat As String
    Dim ColDat As String
    Dim ColLst As String
    Dim OutMsg As String
    Dim CurFlno As String
    Dim CurUrno As String
    Dim CurCkif As String
    Dim CurCkit As String
    Dim CurStod As String
    Dim ChkUrno As String
    Dim tmpLinNbr As String
    Dim CcaRecDat As String
    Dim CurLinNbr As String
    Dim CCaCnt As Long
    Dim CurLine As Long
    Dim MaxLine As Long
    Dim CurBackColor As Long
    Dim IsValidBc As Boolean
    Dim UpdateIt As Boolean
    
    UpdateIt = False
    UpdLineNo = -1
    tmpList = ""
    If MyUrnoIdx(CurTabIdx) >= 0 Then IsValidBc = True
    If IsValidBc Then
        tmpUrno = GetFieldValue("URNO", CurDat, CurFld)
        If tmpUrno <> "" Then
            tmpList = DataTab(CurTabIdx).GetLinesByColumnValue(MyUrnoIdx(CurTabIdx), tmpUrno, 0)
            If tmpList <> "" Then
                'We have it in the list
                LineNo = Val(tmpList)
                tmpRecDat = DataTab(CurTabIdx).GetLineValues(LineNo)
                newRecDat = ""
                'Now look what's to do
                Select Case Trim(CurCmd)
                    Case "UFR"
                        ColLst = ""
                        ChgLst = ""
                        MaxItm = ItemCount(MyPageFields(CurTabIdx), ",")
                        For CurItm = 1 To MaxItm
                            FldNam = GetItem(MyPageFields(CurTabIdx), CurItm, ",")
                            FldIdx = GetItemNo(CurFld, FldNam)
                            If FldIdx > 0 Then
                                FldDat = GetItem(CurDat, FldIdx, ",")
                                If FldNam = "FLNO" Then
                                    'FldDat = Left(FldDat, 7) + Mid(FldDat, 9, 1)
                                ElseIf FldNam = "MTOW" Then
                                    If TabLineNo = "" Then
                                        FldDat = Trim(GetItem(FldDat, 1, "."))
                                        FldDat = Right("00000" + FldDat, 5)
                                    End If
                                ElseIf InStr(MyTimeFields(CurTabIdx), FldNam) > 0 Then
                                    'FldDat = GetTimeFromCedaDate(FldDat)
                                End If
                                ColDat = GetItem(tmpRecDat, CurItm, ",")
                                If ColDat <> FldDat Then
                                    UpdateIt = True
                                    ChgLst = ChgLst & FldNam & ","
                                    OldLst = OldLst & ColDat & ","
                                    NewLst = NewLst & FldDat & ","
                                    ColLst = ColLst & Trim(Str(CurItm - 1)) & ","
                                End If
                            Else
                                FldDat = GetItem(tmpRecDat, CurItm, ",")
                            End If
                            newRecDat = newRecDat & FldDat & ","
                        Next
                    Case "IFR"
                    Case "ISF"
                    Case "DFR"
                        'DataTab(CurTabIdx).DeleteLine LineNo
                        'RefreshColoredLines "", LineNo, "", vbRed, True, CurTabIdx
                        'UpdateIt = True
                    Case "UPS"
                    Case "UPJ"
                    Case Else
                        'Nothing to do
                End Select
                UpdLineNo = LineNo
                If UpdateIt Then
                    lblRefresh(0) = "UPDATE"
                    lblRefresh(1) = Format(Now, "hh:mm:ss")
                    If newRecDat <> "" Then
                        OutRecDat = newRecDat
                        If UpdateOnReload = True Then
                            If TabLineNo <> "" Then
                                newRecDat = newRecDat + "UPD," + TabLineNo + ",UPD"
                            Else
                                newRecDat = newRecDat + "UPD," + GetItem(tmpRecDat, MaxItm + 2, ",")
                            End If
                        End If
                        DataTab(CurTabIdx).UpdateTextLine LineNo, newRecDat, True
                        RefreshColoredLines tmpUrno, LineNo, ColLst, vbGreen, True, CurTabIdx
                        If (SendUpdates) And (CdiIdx > 0) Then
                            OutMsg = MyPageFields(CurTabIdx) & vbNewLine & OutRecDat & vbNewLine & ChgLst & vbNewLine & OldLst & vbNewLine & NewLst
                            SendCdiMsg OutMsg, -1, CurTabIdx
                        End If
                        If (CheckCic = True) And (SupportCCaTab(CurTabIdx)) Then
                            CurUrno = GetFieldValue("URNO", tmpRecDat, MyPageFields(CurTabIdx))
                            CurFlno = GetFieldValue("FLNO", tmpRecDat, MyPageFields(CurTabIdx))
                            CurCkif = GetFieldValue("CKIF", tmpRecDat, MyPageFields(CurTabIdx))
                            CurCkit = GetFieldValue("CKIT", tmpRecDat, MyPageFields(CurTabIdx))
                            CurStod = GetFieldValue("STOD", tmpRecDat, MyPageFields(CurTabIdx))
                            CCaCnt = 0
                            If (CurCkif <> "") Then CCaCnt = BasicData.LookUpCca(CurUrno, CurFlno, CurStod, CurCkif, CurCkit, False)
                            If CCaCnt > 0 Then
                                DataTab(CurTabIdx).SetDecorationObject LineNo, MyCkifIdx(CurTabIdx), "CIC1"
                                DataTab(CurTabIdx).SetDecorationObject LineNo, MyCkitIdx(CurTabIdx), "CIC2"
                                CurBackColor = Val(DataTab(CurTabIdx).GetLineTag(LineNo))
                                CurLinNbr = Right("000000" + CStr(LineNo), 6)
                                MaxLine = CCaCnt - 1
                                For CurLine = 0 To MaxLine
                                    CcaRecDat = EmptyLine(CurTabIdx)
                                    tmpUrno = CurUrno & "_" & CStr(CurLine + 1)
                                    tmpLinNbr = CurLinNbr & "_" & CStr(CurLine + 1)
                                    FldDat = BasicData.CicGrp(1).GetColumnValue(CurLine, 1)
                                    SetItem CcaRecDat, (MyCkifIdx(CurTabIdx) + 1), ",", FldDat
                                    FldDat = BasicData.CicGrp(1).GetColumnValue(CurLine, 2)
                                    SetItem CcaRecDat, (MyCkitIdx(CurTabIdx) + 1), ",", FldDat
                                    SetItem CcaRecDat, (MyUrnoIdx(CurTabIdx) + 1), ",", tmpUrno
                                    CcaRecDat = CcaRecDat + ",UPD," + tmpLinNbr + ",,"
                                    LineNo = LineNo + 1
                                    ChkUrno = DataTab(CurTabIdx).GetColumnValue(LineNo, MyUrnoIdx(CurTabIdx))
                                    ChkUrno = GetItem(ChkUrno, 1, "_")
                                    If ChkUrno = CurUrno Then
                                        DataTab(CurTabIdx).UpdateTextLine LineNo, CcaRecDat, True
                                    Else
                                        DataTab(CurTabIdx).InsertTextLineAt LineNo, CcaRecDat, True
                                        DataTab(CurTabIdx).SetLineColor LineNo, LineTextColor, CurBackColor
                                        DataTab(CurTabIdx).SetLineTag LineNo, CStr(CurBackColor)
                                    End If
                                    DataTab(CurTabIdx).SetDecorationObject LineNo, MyCkifIdx(CurTabIdx), "CIC3"
                                    DataTab(CurTabIdx).SetDecorationObject LineNo, MyCkitIdx(CurTabIdx), "CIC4"
                                Next
                                DataTab(CurTabIdx).SetDecorationObject LineNo, MyCkifIdx(CurTabIdx), "CIC5"
                                DataTab(CurTabIdx).SetDecorationObject LineNo, MyCkitIdx(CurTabIdx), "CIC6"
                                LineNo = LineNo + 1
                                ChkUrno = DataTab(CurTabIdx).GetColumnValue(LineNo, MyUrnoIdx(CurTabIdx))
                                ChkUrno = GetItem(ChkUrno, 1, "_")
                                While ChkUrno = CurUrno
                                    DataTab(CurTabIdx).DeleteLine LineNo
                                    LineNo = LineNo + 1
                                    ChkUrno = DataTab(CurTabIdx).GetColumnValue(LineNo, MyUrnoIdx(CurTabIdx))
                                    ChkUrno = GetItem(ChkUrno, 1, "_")
                                Wend
                            End If
                        End If
                    End If
                Else
                    If TabLineNo <> "" Then
                        'Reload is running
                        'DataTab(CurTabIdx).SetColumnValue LineNo, MyPageFieldCount(CurTabIdx), "REFRESH"
                        DataTab(CurTabIdx).SetColumnValue LineNo, MyPageFieldCount(CurTabIdx) + 1, TabLineNo
                        DataTab(CurTabIdx).SetColumnValue LineNo, MyPageFieldCount(CurTabIdx) + 2, "USE"
                    End If
                End If
            Else
                If TabLineNo <> "" Then
                    DataTab(CurTabIdx).InsertTextLine CurDat, False
                    OutMsg = MyPageFields(CurTabIdx) & vbNewLine & CurDat & vbNewLine & "" & vbNewLine & "" & vbNewLine & ""
                    SendCdiMsg OutMsg, -1, CurTabIdx
                    UpdLineNo = DataTab(CurTabIdx).GetLineCount - 1
                End If
            End If
        End If
    End If
    RefreshOnBc = UpdLineNo
End Function

Private Sub RefreshColoredLines(ActUrno As String, LineNo As Long, ColLst As String, TextColor As Long, DoRefresh As Boolean, CurTabIdx As Integer)
    Dim tmpUrno As String
    Dim tmpVal As String
    Dim tmpCol As String
    Dim NewList As String
    Dim tmpList As String
    Dim TimerData As String
    Dim TimerKey As String
    Dim TimerList As String
    Dim TimerLine As Long
    Dim TimerUrno As String
    Dim UrnoLine As String
    Dim TimerCol As String
    Dim TimerType As String
    Dim ilCnt As Integer
    Dim ItmNbr As Integer
    Dim CurLine As Long
    Dim CurCol As Long
    Dim MaxLine As Long
    Dim UseColor As Long
    Dim CurStat As Long
    Dim CurColor As Long
    Dim IsRefreshed As Boolean
    Dim ActTabIdx As Integer
    If LineNo >= 0 Then
        If ActUrno <> "" Then
            CurColor = Val(DataTab(CurTabIdx).GetLineTag(LineNo))
            DataTab(CurTabIdx).SetLineColor LineNo, TextColor, CurColor
            ItmNbr = 0
            Do
                ItmNbr = ItmNbr + 1
                tmpCol = GetItem(ColLst, ItmNbr, ",")
                If tmpCol <> "" Then
                    CurCol = Val(tmpCol)
                    DataTab(CurTabIdx).SetCellProperty LineNo, CurCol, "UpdCell"
                End If
            Loop While tmpCol <> ""
            CurStat = DataTab(CurTabIdx).GetLineStatusValue(LineNo)
            If CurStat < 1 Then UpdateCount = UpdateCount + 1
            DataTab(CurTabIdx).SetLineStatusValue LineNo, 1
            DataTab(CurTabIdx).TimerSetValue LineNo, KeepUpdate
            DataTab(CurTabIdx).RedrawTab
            ToggleUpdLed
        End If
    End If
End Sub
Private Sub ToggleUpdLed()
    If UpdateCount > 0 Then
        If BcUpdInd(0).FillColor = &HFF00& Then
            BcUpdInd(0).FillColor = vbYellow
            BcUpdInd(1).FillColor = &HFF00&
        Else
            BcUpdInd(1).FillColor = vbYellow
            BcUpdInd(0).FillColor = &HFF00&
        End If
    Else
        If BcUpdInd(0).FillColor = &HC000& Then
            BcUpdInd(0).FillColor = &HFF00&
            BcUpdInd(1).FillColor = &HC000&
        Else
            BcUpdInd(1).FillColor = &HFF00&
            BcUpdInd(0).FillColor = &HC000&
        End If
    End If
End Sub
Private Function GetTimeFromCedaDate(CedaDate As String) As String
    Dim Result As String
    Result = ""
    If Trim(CedaDate) <> "" Then
        Result = Mid(CedaDate, 9, 2) + ":" + Mid(CedaDate, 11, 2)
    End If
    GetTimeFromCedaDate = Result
End Function

Public Function GetUTCDifference() As Integer
    Dim ret As Integer
    Dim buf As String
    Dim strArr() As String
    Dim strUtcArr() As String
    Dim i As Integer, j As Integer
    Dim count As Integer
    Dim istrRet As Integer
    Dim CurrStr As String
    Dim tmpTime As Date
    Dim tmpDiff As Integer
    Screen.MousePointer = 11
    OpenConnection True
    GetUTCDifference = 0
    PrepareMessage "INIT"
    If CedaIsConnected = True Then
        If Ufis.CallServer("GFR", "", "", "", "[CONFIG]", "240") = 0 Then
            buf = Ufis.GetDataBuffer(True)
        End If
        strArr = Split(buf, Chr(10))
        count = UBound(strArr)
        For i = 0 To count
            CurrStr = strArr(i)
            istrRet = InStr(1, CurrStr, "UTCD", 0)
            If istrRet <> 0 Then
                strUtcArr = Split(strArr(i), ",")
                i = count + 1
                tmpDiff = CInt(strUtcArr(1))
            End If
        Next i
    Else
        tmpDiff = 0
    End If
    lblNow.Caption = Format(Now, "dd.mm.yy / hh:mm:ss")
    tmpTime = DateAdd("n", -tmpDiff, Now)
    lblUTC.Caption = Format(tmpTime, "hh:mm:ss")
    ConnectBcPort
    PrepareMessage "LOGIN"
    If CedaIsConnected = True Then
        ret = Ufis.CallServer("SBC", "STAFF/KEEP", "ALL FIELDS", MyTcpIpAdr, HexTcpAdr, "240")
    End If
    GotUtcDiff = True
    GetUTCDifference = tmpDiff
    Screen.MousePointer = 0
End Function

Private Sub OpenConnection(ForNetin As Boolean)
    Dim ret As Boolean
    ret = CedaIsConnected
    CedaCon(0).Visible = True
    CedaCon(0).FillColor = NormalYellow
    If ForNetin Then
        If MyHostName <> "LOCAL" Then
            If CedaIsConnected = False Then
                MsgLogging UCase(App.EXEName) & " CONNECTING TO " & UCase(MyHostName)
                ret = Ufis.InitCom(MyHostName, "CEDA")
            End If
        End If
    Else
        ret = True
    End If
    CedaIsConnected = ret
    If CedaIsConnected Then
        CedaCon(0).FillColor = LightGreen
    Else
        CedaCon(0).FillColor = vbRed
    End If
End Sub
Private Sub PrepareMessage(Context As String)
    Ufis.HomeAirport = MyHopo
    Ufis.TableExt = MyTblExt
    Ufis.Module = MyApplVersion
    Ufis.twe = ""
    Ufis.UserName = MyUserName
    Ufis.tws = MyShortName + " " + Context
End Sub
Private Sub CloseConnection(ForNetin As Boolean)
    If (ForNetin) Or (CedaIsConnected) Then
        If MyHostName <> "LOCAL" Then
            If CedaIsConnected = True Then Ufis.CleanupCom
        End If
    End If
    CedaIsConnected = False
    CedaCon(0).Visible = CedaIsConnected
End Sub
Private Sub ConnectBcPort()
    If MyHostName <> "LOCAL" Then
        RcvBc.InitComm
        BcPortConnected = True
    End If
End Sub

Public Sub RemotePort_Close(Index As Integer)
    If RemotePort(Index).State <> sckClosed Then RemotePort(Index).Close
    If RemotePort(0).State <> sckClosed Then RemotePort(0).Close
    If Index > 0 Then Unload RemotePort(Index)
    'RemoteColor(0) = &HFF00&
    'RemoteColor(1) = &HC000&
    'RefreshInd(0).FillColor = RemoteColor(0)
    'RefreshInd(1).FillColor = RemoteColor(0)
    RemoteIsOpen = False
    RemoteIsBusy = False
    RemoteIdx = -1
    If RemoteControl Then
        If ShowRemote.optConnect(0).Value = True Then
            ShowRemote.optConnect(1).Value = True
        End If
    Else
        If Not ShutDownRequested Then
            OpenRemotePort
        End If
    End If
End Sub

Private Sub RemotePort_Connect(Index As Integer)
    MsgBox "Connected"
    Dim TestString As String
    RemoteIdx = 0
    'RemoteColor(0) = &HFFFF&
    'RemoteColor(1) = &HC0C0&
    'RefreshInd(0).FillColor = RemoteColor(0)
    'RefreshInd(1).FillColor = RemoteColor(0)
    RemoteCon(0).Visible = True
    RemoteCon(1).Visible = True
    RemoteCon(0).Tag = "1"
    RemoteIsBusy = True
    If RemoteControl Then
        ShowRemote.optConnect(0).Caption = "Connected"
        ShowRemote.optConnect(0).BackColor = LightGreen
        ShowRemote.optConnect(1).Enabled = True
        ShowRemote.chkCCO.Enabled = True
        ShowRemote.ToolFrame.Enabled = True
    End If
    TestString = "{=TOT=}      174{=CMD=}RT{=IDN=}ROS_INIT_LOAD{=TBL=}ACTTAB{=EXT=}TAB{=HOPO=}ATH{=FLD=}ACT3,ACT5,ACTI,CDAT{=WHE=}WHERE URNO > 0{=USR=}MWO{=WKS=}wks104{=SEPA=}" & vbLf & "{=APP=}Rostering"
    RemotePort(Index).SendData TestString
End Sub

Private Sub RemotePort_ConnectionRequest(Index As Integer, ByVal requestID As Long)
    Dim OutMsg As String
    RemoteCon(0).Visible = True
    RemoteCon(1).Visible = True
    RemoteCon(0).Tag = "1"
    If RemotePort(0).State <> sckClosed Then RemotePort(0).Close
    RemoteIdx = 1
    Load RemotePort(RemoteIdx)
    RemotePort(RemoteIdx).Accept requestID
    'RemoteColor(0) = &HFFFF&
    'RemoteColor(1) = &HC0C0&
    'RefreshInd(0).FillColor = RemoteColor(0)
    'RefreshInd(1).FillColor = RemoteColor(0)
    OutMsg = "CONNEX,HOST,-1||" & RemotePort(0).LocalIP & " / " & RemotePort(0).LocalHostName
    SendRemoteMsg OutMsg
End Sub

Private Sub RemotePort_DataArrival(Index As Integer, ByVal bytesTotal As Long)
    Static RemMsgRest As String
    Dim RcvData As String
    Dim rcvText As String
    Dim RcvMsg As String
    Dim tmpLen As String
    Dim MsgLen As Integer
    Dim txtLen As Integer
    Dim msgPos As Integer
    Dim gotMsg As Boolean
    RemotePort(Index).GetData RcvData, vbString
    rcvText = RemMsgRest & RcvData
    RemMsgRest = ""
    MsgBox "Received Message:" & vbNewLine & rcvText
    'Text1.Text = RcvData
    txtLen = Len(rcvText)
    gotMsg = True
    msgPos = 1
    While (msgPos < txtLen) And (gotMsg = True)
        gotMsg = False
        If (msgPos + 8) < txtLen Then
            tmpLen = Mid(rcvText, msgPos, 8)
            MsgLen = Val(tmpLen)
        End If
        If (msgPos + 7 + MsgLen) <= txtLen Then
            msgPos = msgPos + 8
            RcvMsg = Mid(rcvText, msgPos, MsgLen)
            msgPos = msgPos + MsgLen
            RunRemoteMsg RcvMsg
            gotMsg = True
        End If
    Wend
    If gotMsg = False Then
        RemMsgRest = Mid(rcvText, msgPos)
    End If
End Sub

Private Sub RemotePort_Error(Index As Integer, ByVal Number As Integer, Description As String, ByVal Scode As Long, ByVal Source As String, ByVal HelpFile As String, ByVal HelpContext As Long, CancelDisplay As Boolean)
    MsgBox "Error: " & Number & vbLf & Description
    RemotePort_Close Index
End Sub

Private Sub RemotePort_SendComplete(Index As Integer)
    'Text1.Text = Text1.Text & vbNewLine & "Ready."
End Sub

Private Sub RemotePort_SendProgress(Index As Integer, ByVal bytesSent As Long, ByVal bytesRemaining As Long)
    'Text1.Text = Str(bytesSent) & " /" & Str(bytesRemaining)
End Sub

Public Sub OpenRemotePort()
    If (Not RemoteIsOpen) And (Not RemoteIsBusy) Then
        RemotePort(0).LocalPort = "4397"
        RemotePort(0).Listen
        RemoteIdx = 0
        'MsgBox "Listening"
        RemoteIsOpen = True
    End If
End Sub

Private Sub RunRemoteMsg(RcvTcpMsg As String)
    Dim RcvTsk As String
    Dim RcvMsg As String
    Dim RcvCmd As String
    Dim RunCmd As String
    Dim RunIdx As String
    Dim OutMsg As String
    Dim tmpdat As String
    RcvTsk = GetItem(RcvTcpMsg, 1, "||")
    RcvMsg = GetItem(RcvTcpMsg, 2, "||")
    RcvCmd = GetItem(RcvTsk, 1, ",")
    RunCmd = GetItem(RcvTsk, 2, ",")
    RunIdx = GetItem(RcvTsk, 3, ",")
    Select Case RcvCmd
        Case "CONNEX"
            Select Case RunCmd
                Case "HOST"
                    ShowRemote.Location.Text = RcvMsg
                Case Else
            End Select
        
        Case "SEND"
            Select Case RunCmd
                Case "CLOCK"
                    OutMsg = "ANSW,CLOCK," & RunIdx & "||" & Now
                    SendRemoteMsg OutMsg
                Case "TIMES"
                    tmpdat = lblNow.Caption & " / UTC=" & lblUTC.Caption & ","
                    tmpdat = tmpdat & lblRefresh(0).Caption & "," & lblRefresh(1).Caption
                    OutMsg = "ANSW,TIMES," & RunIdx & "||" & tmpdat
                    SendRemoteMsg OutMsg
                Case "BUTTONS"
                    tmpdat = ""
                    tmpdat = tmpdat & MainTitle.Caption & vbLf
                    tmpdat = tmpdat & PageTitle.Caption & vbLf
                    tmpdat = tmpdat & lblNow.Caption & vbLf
                    tmpdat = tmpdat & lblUTC.Caption & vbLf
                    tmpdat = tmpdat & lblRefresh(0).Caption & vbLf
                    tmpdat = tmpdat & lblRefresh(1).Caption & vbLf
                    tmpdat = tmpdat & chkLeftAction.Caption & vbLf
                    tmpdat = tmpdat & chkRightAction.Caption & vbLf
                    tmpdat = tmpdat & btnExit.Caption & vbLf
                    tmpdat = tmpdat & btnPrev.Caption & vbLf
                    tmpdat = tmpdat & btnNext.Caption & vbLf
                    tmpdat = tmpdat & MyFieldLen & vbLf
                    tmpdat = tmpdat & DataTab(0).GetHeaderText & vbLf
                    OutMsg = "ANSW,BUTTONS," & RunIdx & "||" & tmpdat
                    SendRemoteMsg OutMsg
                Case "DATA"
                    tmpdat = GetTabDataBuffer()
                    OutMsg = "ANSW,DATA," & RunIdx & "||" & tmpdat
                    SendRemoteMsg OutMsg
                
                Case Else
            End Select
        Case "ANSW"
            Select Case RunCmd
                Case "CLOCK"
                    ShowRemote.RemoteData.Caption = RcvMsg
                Case "TIMES"
                    'ShowRemote.RemoteData.Caption = RcvMsg
                    lblNow.Caption = GetItem(RcvMsg, 1, "/ UTC=")
                    tmpdat = GetItem(RcvMsg, 2, "/ UTC=")
                    lblUTC.Caption = GetItem(tmpdat, 1, ",")
                    lblRefresh(0).Caption = GetItem(tmpdat, 2, ",")
                    lblRefresh(1).Caption = GetItem(tmpdat, 3, ",")
                Case "BUTTONS"
                    MainTitle.Caption = GetItem(RcvMsg, 1, vbLf)
                    PageTitle.Caption = GetItem(RcvMsg, 2, vbLf)
                    lblNow.Caption = GetItem(RcvMsg, 3, vbLf)
                    lblUTC.Caption = GetItem(RcvMsg, 4, vbLf)
                    lblRefresh(0).Caption = GetItem(RcvMsg, 5, vbLf)
                    lblRefresh(1).Caption = GetItem(RcvMsg, 6, vbLf)
                    chkLeftAction.Caption = GetItem(RcvMsg, 7, vbLf)
                    chkRightAction.Caption = GetItem(RcvMsg, 8, vbLf)
                    btnExit.Caption = GetItem(RcvMsg, 9, vbLf)
                    btnPrev.Caption = GetItem(RcvMsg, 10, vbLf)
                    btnNext.Caption = GetItem(RcvMsg, 11, vbLf)
                    MyFieldLen = GetItem(RcvMsg, 12, vbLf)
                    DataTab(0).HeaderLengthString = MyFieldLen
                    DataTab(0).HeaderString = GetItem(RcvMsg, 13, vbLf)
                Case "DATA"
                    DataTab(0).ResetContent
                    DataTab(0).InsertBuffer RcvMsg, vbLf
                Case Else
            End Select
        Case "CCO"
            If Not RemoteControl Then
                If RefreshInd(0).FillColor <> vbRed Then
                    RefreshInd(0).FillColor = vbRed
                    RefreshInd(1).FillColor = RemoteColor(0)
                Else
                    RefreshInd(0).FillColor = RemoteColor(0)
                    RefreshInd(1).FillColor = vbRed
                End If
                SendRemoteMsg "CCA"
                tmpdat = lblNow.Caption & " / UTC=" & lblUTC.Caption & ","
                tmpdat = tmpdat & lblRefresh(0).Caption & "," & lblRefresh(1).Caption
                OutMsg = "ANSW,TIMES,-1||" & tmpdat
                SendRemoteMsg OutMsg
            End If
        Case "CCA"
            If RemoteControl Then
                ShowRemote.chkRemote.BackColor = LightGreen
            End If
        Case "PUSH"
            Select Case RunCmd
                Case "MAIN_TITLE"
                    MainTitle.Value = 1
                Case "LEFT"
                    chkLeftAction.Value = 1
                Case "RIGHT"
                    chkRightAction.Value = 1
                Case "EXIT"
                    btnExit_Click
                Case "PREV"
                    btnPrev_Click
                Case "NEXT"
                    btnNext_Click
                Case Else
            End Select
            OutMsg = "PUSHED," & RunCmd & "," & RunIdx & "||"
            SendRemoteMsg OutMsg
        Case "PUSHED"
            ShowRemote.optTool(0).Value = True
        Case "STARTED"
            ShowRemote.StartApp.Value = 0
            ShowRemote.chkRemoteStart.Value = 0
            ShowRemote.optConnect(0).Value = True
        Case Else
            'MsgBox RcvMsg
    End Select
End Sub

Public Sub SendRemoteMsg(OutText As String)
    Dim tmpLen As String
    Dim totLen As Integer
    totLen = Len(OutText)
    tmpLen = Trim(Str(totLen))
    tmpLen = Right(("00000000") & tmpLen, 8)
    'MsgBox tmpLen & OutText
    RemotePort(RemoteIdx).SendData tmpLen & OutText
End Sub

Private Function GetTabDataBuffer() As String
    Dim CurLine As Long
    Dim MaxLine As Long
    Dim Result As String
    Result = ""
    MaxLine = DataTab(0).GetLineCount - 1
    For CurLine = 0 To MaxLine
        Result = Result & DataTab(0).GetLineValues(CurLine) & vbLf
    Next
    GetTabDataBuffer = Result
End Function

Private Sub WriteDataToFile(CurTabIdx As Integer)
    Dim WrkFileName As String
    Dim CurFileName As String
    Dim CurLin As Long
    Dim MaxLin As Long
    Dim tmpRec As String
    WrkFileName = SavePath + GetTimeStamp(0) + ".txt"
    CurFileName = SavePath + MyCedaTitle(CurTabIdx) + GetTimeStamp(0) + ".txt"
    Open WrkFileName For Output As #1
        tmpRec = "EXPORTED: " + MyCedaTitle(CurTabIdx) + " / " + GetTimeStamp(0)
        Print #1, tmpRec
        Print #1, MyPageFields(CurTabIdx)
        MaxLin = DataTab(CurTabIdx).GetLineCount - 1
        For CurLin = 0 To MaxLin
            tmpRec = DataTab(CurTabIdx).GetLineValues(CurLin)
            Print #1, tmpRec
        Next
    Close #1
    Name WrkFileName As CurFileName
End Sub

Private Sub CdiPort_ConnectionRequest(Index As Integer, ByVal requestID As Long)
    Dim OutMsg As String
    If (Index = 0) And (Not CdiServer) Then
        'CdiIdx = CdiIdx + 1
        CdiIdx = GetFreeCdiIdx()
        Load CdiPort(CdiIdx)
        CdiPort(CdiIdx).LocalPort = 0
        CdiPort(CdiIdx).Accept requestID
        CdiPortsChanged = True
        SetServerCaption
        MsgLogging UCase(App.EXEName) & " CONNECTED: " & CdiPort(Index).RemoteHostIP
        'MsgBox CdiPort(CdiIdx).RemoteHostIP
        'MsgBox CdiPort(CdiIdx).RemoteHost
        If CdiPageFields(0) = "" Then
            OutMsg = "CONNEX,HOST,-1||" & CdiPort(0).LocalIP & " / " & CdiPort(0).LocalHostName & " / IDX" & Str(CdiIdx)
            SendCdiMsg OutMsg, CdiIdx, 0
        End If
        If CdiSendOnConnex = True Then
            CdiSendAllRecs 0, CdiIdx
            CdiSendAllRecs 1, CdiIdx
        End If
    End If
End Sub

Private Sub CdiPort_Close(Index As Integer)
    MsgLogging UCase(App.EXEName) & " CLOSED BY: " & CdiPort(Index).RemoteHostIP
    If CdiPort(Index).State <> sckClosed Then CdiPort(Index).Close
    If Index > 0 Then Unload CdiPort(Index)
    If CdiServer And Index = CdiActIdx Then CdiPacketComplete = True 'Stop sending
    SetServerCaption
    CdiPortsChanged = True
End Sub

Private Sub SetServerCaption()
    Dim iCnt As Integer
    iCnt = CdiPort.count - 1
    If iCnt > 1 Then
        btnCdiServer.Caption = Trim(Str(iCnt)) & " CLIENTS"
    ElseIf iCnt = 1 Then
        btnCdiServer.Caption = "1 CLIENT"
    Else
        btnCdiServer.Caption = "LISTENING"
    End If
    MsgLogging UCase(App.EXEName) & " " & btnCdiServer.Caption
    If btnCdiServer.Value = 1 Then
        CdiList.InitMyList
    End If
End Sub

Private Sub CdiPort_DataArrival(Index As Integer, ByVal bytesTotal As Long)
    Dim RcvData As String
    Dim rcvText As String
    Dim RcvMsg As String
    Dim tmpLen As String
    Dim MsgLen As Integer
    Dim txtLen As Integer
    Dim msgPos As Integer
    Dim gotMsg As Boolean
    CdiPort(Index).GetData RcvData, vbString
    rcvText = CdiMsgRest(Index) & RcvData
    'MsgBox Index & vbNewLine & rcvText
    If Not CdiServer Then
        CdiMsgRest(Index) = ""
        'Text1.Text = RcvData
        txtLen = Len(rcvText)
        gotMsg = True
        msgPos = 1
        While (msgPos < txtLen) And (gotMsg = True)
            gotMsg = False
            If (msgPos + 8) < txtLen Then
                tmpLen = Mid(rcvText, msgPos, 8)
                MsgLen = Val(tmpLen)
            End If
            If (msgPos + 7 + MsgLen) <= txtLen Then
                msgPos = msgPos + 8
                RcvMsg = Mid(rcvText, msgPos, MsgLen)
                msgPos = msgPos + MsgLen
                'Hier muessen the events gespoolt werden !!!
                RunCdiMsg RcvMsg, Index
                gotMsg = True
            End If
        Wend
        If (gotMsg = False) And (msgPos > 0) Then
            CdiMsgRest(Index) = Mid(rcvText, msgPos)
        End If
    Else
        CdiMsgRest(Index) = rcvText
        'Must set a flag that a message was buffered !!
        MsgBox "Message buffered."
    End If
End Sub

Private Sub RunCdiMsg(RcvMsg As String, Index As Integer)
    Dim CdiCmd As String
    Dim CdiMsg As String
    Dim FirstChar As String
    Dim LastChar As String
    Dim tmpData As String
    FirstChar = Left(RcvMsg, 1)
    LastChar = Right(RcvMsg, 1)
    CdiMsg = Mid(RcvMsg, 2, Len(RcvMsg) - 2)
    CdiCmd = GetItem(CdiMsg, 1, ",")
    Select Case CdiCmd
        Case "MYN"  'My Name
            tmpData = GetItem(CdiMsg, 2, ",")
            'CdiPort(Index).Tag = tmpData
            CdiPort(Index).Tag = CdiMsg
            'MsgBox CdiMsg
        Case Else
            'MsgBox "Client sends garbage!"
    End Select
End Sub

Private Sub CdiPort_Error(Index As Integer, ByVal Number As Integer, Description As String, ByVal Scode As Long, ByVal Source As String, ByVal HelpFile As String, ByVal HelpContext As Long, CancelDisplay As Boolean)
    'MsgBox "Error: " & Number & vbLf & Description
    CdiPort_Close Index
    'CdiControl.BorderColor = vbRed
End Sub

Private Sub CdiPort_SendComplete(Index As Integer)
    CdiPacketComplete = True
End Sub

Private Sub CdiPort_SendProgress(Index As Integer, ByVal bytesSent As Long, ByVal bytesRemaining As Long)
    'Text1.Text = Str(bytesSent) & " /" & Str(bytesRemaining)
End Sub

Private Sub OpenCdiPort()
    If Not RemoteControl Then
        CdiPort(0).LocalPort = "4395"
        CdiPort(0).Listen
        CdiIdx = 0
        'CdiControl.Visible = True
        'CdiControl.BorderColor = LightYellow
        btnCdiServer.Visible = True
    End If
End Sub

Public Sub SendCdiMsg(OutText As String, TcpIdx As Integer, CurTabIdx As Integer)
    Dim InternalError As Boolean
    Dim txtLen As String
    Dim totLen As Integer
    Dim MsgTxt(2) As String
    Dim cdiObj As Winsock
    Dim TotalWait As Single
    Dim CurIdx As Integer
    Dim CurMsg As Integer
    Dim ActFldLst As String
    Dim ActDatLst As String
    Dim ChgFldLst As String
    Dim OldDatLst As String
    Dim NewDatLst As String
    Dim FldNam As String
    Dim FldDat As String
    Dim OldDat As String
    Dim FldLen As Integer
    Dim CurItm As Integer
    Dim ItmTxt As String
    Dim OldTxt As String
    Dim ItmLen As Integer
    If CdiPort.count > 1 Then
        MsgTxt(0) = ""
        MsgTxt(1) = ""
        If CdiPageFields(CurTabIdx) <> "" Then
            ActFldLst = GetItem(OutText, 1, vbNewLine)
            ActDatLst = GetItem(OutText, 2, vbNewLine)
            CurItm = 0
            ItmTxt = "START"
            Do
                CurItm = CurItm + 1
                FldNam = GetItem(CdiTriggers(CurTabIdx), CurItm, ",")
                If FldNam <> "" Then
                    ItmTxt = GetFieldValue(FldNam, ActDatLst, ActFldLst)
                End If
            Loop While (FldNam <> "") And (ItmTxt = "")
            If ItmTxt <> "" Then
                ChgFldLst = GetItem(OutText, 3, vbNewLine)
                OldDatLst = GetItem(OutText, 4, vbNewLine)
                NewDatLst = GetItem(OutText, 5, vbNewLine)
                CurItm = 0
                Do
                    CurItm = CurItm + 1
                    FldNam = GetItem(CdiPageFields(CurTabIdx), CurItm, ",")
                    If FldNam <> "" Then
                        FldLen = Val(GetItem(CdiFieldLength(CurTabIdx), CurItm, ","))
                        FldDat = GetFieldValue(FldNam, ActDatLst, ActFldLst)
                        OldDat = GetFieldValue(FldNam, OldDatLst, ChgFldLst)
                        If InStr(CdiKeyFields(CurTabIdx), FldNam) > 0 Then
                            If (OldDat <> "") And (FldDat <> OldDat) Then
                                Mid(MsgTxt(0), 1, 1) = "D"
                                Mid(MsgTxt(1), 1, 1) = "I"
                            End If
                        End If
                        If (InStr(CdiTriggers(CurTabIdx), FldNam) > 0) And (InStr(ChgFldLst, FldNam) > 0) Then
                            If OldDat = "" Then
                                Mid(MsgTxt(0), 1, 1) = "D"
                                Mid(MsgTxt(1), 1, 1) = "I"
                                'OldDat = Space(FldLen)
                            End If
                        End If
                        Select Case FldNam
                            Case "*MT*"
                                If ChgFldLst <> "" Then
                                    OldDat = "U"
                                    FldDat = "U"
                                Else
                                    OldDat = "D"
                                    FldDat = "I"
                                End If
                            Case "ADID"
                                FldDat = GetFieldValue(FldNam, ActDatLst, ActFldLst)
                                'Attention: Validation for NoiseMonitor Interface only !!
                                If FldDat = "B" Then
                                    If InStr(ActFldLst, "ORG4") > 0 Then
                                        FldDat = "A"
                                    ElseIf InStr(ActFldLst, "DES4") > 0 Then
                                        FldDat = "D"
                                    End If
                                End If
                            Case "----"
                                FldDat = ""
                            Case "ORIG"
                                FldDat = GetFieldValue("VIA4", ActDatLst, ActFldLst)
                                If FldDat = "" Then
                                    FldDat = GetFieldValue("ORG4", ActDatLst, ActFldLst)
                                End If
                            Case "DEST"
                                FldDat = GetFieldValue("VIA4", ActDatLst, ActFldLst)
                                If FldDat = "" Then
                                    FldDat = GetFieldValue("DES4", ActDatLst, ActFldLst)
                                End If
                            'Case Else
                            '    FldDat = GetFieldValue(FldNam, ActDatLst, ActFldLst)
                        End Select
                        If OldDat = "" Then OldDat = FldDat
                        OldTxt = Left(OldDat + Space(20), FldLen)
                        ItmTxt = Left(FldDat + Space(20), FldLen)
                        MsgTxt(0) = MsgTxt(0) + OldTxt
                        MsgTxt(1) = MsgTxt(1) + ItmTxt
                    End If
                Loop While FldNam <> ""
                MsgTxt(0) = MsgTxt(0) + CdiRecSep
                MsgTxt(1) = MsgTxt(1) + CdiRecSep
                If Left(MsgTxt(0), 1) = Left(MsgTxt(1), 1) Then
                    MsgTxt(1) = ""
                End If
            End If
        Else
            MsgTxt(0) = Chr(16) + OutText + Chr(17)
            totLen = Len(MsgTxt(0))
            txtLen = Trim(Str(totLen))
            txtLen = Right(("00000000" + txtLen), 8)
            MsgTxt(0) = txtLen + MsgTxt(0)
        End If
        On Error GoTo ErrorHandle
        CurMsg = 0
        Do
            If MsgTxt(CurMsg) <> "" Then
                CdiActIdx = 0
                If TcpIdx > 0 Then
                    CdiPort(TcpIdx).SendData MsgTxt(CurMsg)
                    TotalWait = WaitForComplete(TcpIdx)
                Else
                    CdiServer = True
                    Do
                        CdiPortsChanged = False
                        InternalError = False
                        For Each cdiObj In CdiPort
                            If Not InternalError Then
                                CurIdx = cdiObj.Index
                                If Not InternalError Then
                                    If CurIdx > CdiActIdx Then
                                        cdiObj.SendData MsgTxt(CurMsg)
                                        If Not InternalError Then
                                            TotalWait = WaitForComplete(cdiObj.Index)
                                        End If
                                    End If
                                End If
                            End If
                            If CdiPortsChanged Then Exit For
                            InternalError = False
                        Next
                    Loop While CdiPortsChanged
                    CdiServer = False
                End If
                CdiActIdx = 0
                MsgLogging MsgTxt(CurMsg)
            End If
            CurMsg = CurMsg + 1
        Loop While CurMsg <= 1
    End If
    Exit Sub
ErrorHandle:
    ' The error reason only can be the access to the CdiPort element.
    ' We differ between two cases: single or multiple attach
    'MsgBox "Error: " & Err.Description
    If TcpIdx > 0 Then
        ' Single transmission. The error will be solved by "PortError"
        ' and the port should be closed.
        CdiActIdx = 0
        Exit Sub
    Else
        ' Don't know the error reason. Set a flag and proceed.
        InternalError = True
        Resume Next
    End If
End Sub

Private Function WaitForComplete(Index As Integer) As Single
    Dim StartTime As Single
    CdiPacketComplete = False
    CdiActIdx = Index
    StartTime = Timer
    While Not CdiPacketComplete
        'Waiting for CdiPort_SendComplete-Event
        DoEvents
    Wend
    WaitForComplete = Timer - StartTime
End Function

Private Sub MarkTabLines(CurTabIdx As Integer, UseLine As Long, UseCol As Long, UseText As String)
    Dim MaxLin As Long
    Dim BgnLin As Long
    Dim CurLin As Long
    If UseLine >= 0 Then
        BgnLin = UseLine
        MaxLin = UseLine
    Else
        BgnLin = 0
        MaxLin = DataTab(CurTabIdx).GetLineCount - 1
    End If
    For CurLin = BgnLin To MaxLin
        DataTab(CurTabIdx).SetColumnValue CurLin, UseCol, UseText
    Next
End Sub
Private Sub DeleteOldTabLines(CurTabIdx As Integer, UseCol As Long, UseText As String)
    Dim LineList As String
    Dim ItmCnt As Integer
    Dim CurItm As Integer
    Dim CurLine As Long
    LineList = DataTab(CurTabIdx).GetLinesByColumnValue(UseCol, UseText, 0)
    If LineList <> "" Then
        ItmCnt = ItemCount(LineList, ",")
        For CurItm = ItmCnt To 1 Step -1
            CurLine = Val(GetItem(LineList, CurItm, ","))
            If DataTab(CurTabIdx).GetLineStatusValue(CurLine) > 0 Then UpdateCount = UpdateCount - 1
            DataTab(CurTabIdx).DeleteLine CurLine
        Next
        DataTab(CurTabIdx).RedrawTab
        If UpdateCount < 0 Then UpdateCount = 0
    End If
End Sub

Private Sub CdiSendAllRecs(CurTabIdx As Integer, CurCdiIdx As Integer)
    Dim MaxLin As Long
    Dim BgnLin As Long
    Dim CurLin As Long
    Dim tmpRec As String
    Dim OutMsg As String
    Screen.MousePointer = 11
    BgnLin = 0
    MaxLin = DataTab(CurTabIdx).GetLineCount - 1
    For CurLin = BgnLin To MaxLin
        tmpRec = DataTab(CurTabIdx).GetLineValues(CurLin)
        If GetItem(tmpRec, 1, ",") <> "" Then
            OutMsg = MyPageFields(CurTabIdx) & vbNewLine & tmpRec & vbNewLine & "" & vbNewLine & "" & vbNewLine & ""
            SendCdiMsg OutMsg, CurCdiIdx, CurTabIdx
        End If
    Next
    Screen.MousePointer = 0
End Sub

Private Sub MsgLogging(MsgTxt As String)
    Static MyFileCnt As Integer
    Dim MyLogFile As String
    Dim NewName As String
    Dim ExtName As String
    Dim FileNbr As Integer
    Dim tmpTime As String
    Dim OutTxt As String
    Dim MyLogSize As Long
    FileNbr = FreeFile
    MyLogFile = App.Path + "\" + App.EXEName + ".log"
    tmpTime = Format(Now, "YYYY.MM.DD/hh:mm:ss")
    OutTxt = "[" + tmpTime + "] " + MsgTxt
    Open MyLogFile For Append As #FileNbr
    Print #FileNbr, OutTxt
    Close FileNbr
    MyLogSize = FileLen(MyLogFile)
    If MyLogSize > 1000000 Then
        MyFileCnt = MyFileCnt + 1
        ExtName = Right("0000" & Trim(Str(MyFileCnt)), 4)
        NewName = App.EXEName & "_" & ExtName & ".log"
        On Error GoTo ErrorHandle
        Name MyLogFile As NewName
    End If
    Exit Sub
ErrorHandle:
    MyFileCnt = MyFileCnt + 1
    ExtName = Right("0000" & Trim(Str(MyFileCnt)), 4)
    NewName = App.EXEName & "_" & ExtName & ".log"
    Resume
End Sub

' Handler for mouse events occuring in system tray.
Public Sub SysTrayMouseEventHandler()
    SetForegroundWindow Me.hwnd
    PopupMenu RCPopup, vbPopupMenuRightButton
End Sub

Private Sub msg1_Click()
    CdiList.Show
End Sub

Private Sub Rest1_Click()
    Unhook    ' Return event control to windows
    Me.Show
    RemoveIconFromTray
End Sub

Private Sub Exit1_Click()
    Unhook    ' Return event control to windows
    RemoveIconFromTray
    ShutDownRequested = True
End Sub

Private Function GetFreeCdiIdx()
    Dim iCnt As Integer
    Dim objIdx As Integer
    Dim cdiObj As Winsock
    iCnt = -1
    For Each cdiObj In CdiPort
        iCnt = iCnt + 1
        objIdx = cdiObj.Index
        If objIdx > iCnt Then Exit For
    Next
    If iCnt = objIdx Then iCnt = objIdx + 1
    GetFreeCdiIdx = iCnt
End Function


