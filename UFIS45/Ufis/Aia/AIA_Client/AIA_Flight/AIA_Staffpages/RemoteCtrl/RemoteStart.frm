VERSION 5.00
Object = "{248DD890-BB45-11CF-9ABC-0080C7E7B78D}#1.0#0"; "MSWINSCK.OCX"
Begin VB.Form RemoteStart 
   Caption         =   "Staff Page Remote Starter"
   ClientHeight    =   3195
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   4680
   Icon            =   "RemoteStart.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   3195
   ScaleWidth      =   4680
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton btnHide 
      Caption         =   "Hide"
      Height          =   285
      Left            =   2310
      TabIndex        =   7
      Top             =   2670
      Width           =   705
   End
   Begin VB.CommandButton btnDisconnect 
      Caption         =   "Disconnect"
      Height          =   285
      Left            =   840
      TabIndex        =   5
      Top             =   2670
      Width           =   1425
   End
   Begin MSWinsockLib.Winsock CdiPort 
      Index           =   0
      Left            =   60
      Top             =   2700
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   393216
   End
   Begin VB.CommandButton btnConnect 
      Caption         =   "Connect Remote"
      Height          =   285
      Left            =   840
      TabIndex        =   4
      Top             =   2340
      Width           =   1425
   End
   Begin VB.CommandButton btnStartApp 
      Caption         =   "Start Remote"
      Height          =   285
      Left            =   2310
      TabIndex        =   3
      Top             =   2340
      Width           =   1425
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Exit"
      Height          =   285
      Left            =   3060
      TabIndex        =   0
      Top             =   2670
      Width           =   675
   End
   Begin MSWinsockLib.Winsock RemotePort 
      Index           =   0
      Left            =   60
      Top             =   2250
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   393216
   End
   Begin VB.Timer Timer1 
      Enabled         =   0   'False
      Interval        =   10000
      Left            =   510
      Top             =   2220
   End
   Begin VB.Label Label3 
      Height          =   285
      Left            =   60
      TabIndex        =   6
      Top             =   510
      Width           =   4485
   End
   Begin VB.Label Label2 
      Height          =   1275
      Left            =   60
      TabIndex        =   2
      Top             =   900
      Width           =   4485
   End
   Begin VB.Label Label1 
      Height          =   285
      Left            =   60
      TabIndex        =   1
      Top             =   120
      Width           =   4485
   End
   Begin VB.Menu RCPopup 
      Caption         =   "RCPopup"
      Visible         =   0   'False
      Begin VB.Menu msg1 
         Caption         =   "Message"
      End
      Begin VB.Menu Rest1 
         Caption         =   "Show Me"
      End
      Begin VB.Menu Exit1 
         Caption         =   "Exit"
      End
   End
End
Attribute VB_Name = "RemoteStart"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim RemoteIsOpen As Boolean
Dim RemoteIsBusy As Boolean
Dim RemoteIdx As Integer
Dim CdiPortIsValid As Boolean
Dim MyCtrlPath As String
Dim MyCtrlAppl As String

Private Sub btnConnect_Click()
    Dim TcpAdr As String
    'If Not CdiConnected Then
        'Text1 = ""
        'CdiConnected = True
        'TcpAdr = Text2
        TcpAdr = CdiPort(0).LocalIP
        CdiPort(0).RemoteHost = TcpAdr
        CdiPort(0).RemotePort = "4397"
        CdiPort(0).Connect TcpAdr, "4397"
        CdiPortIsValid = True
    'End If
End Sub

Private Sub btnDisconnect_Click()
    CdiPort_Close 0
End Sub

Private Sub CdiPort_Close(Index As Integer)
    If CdiPort(Index).State <> sckClosed Then CdiPort(Index).Close
    If CdiPort(0).State <> sckClosed Then CdiPort(0).Close
    'If Index > 0 Then Unload CdiPort(Index)
    'MsgBox "Connection Closed"
End Sub

Private Sub CdiPort_Connect(Index As Integer)
    'MsgBox "Connected"
    CdiPortIsValid = True
End Sub

Private Sub CdiPort_DataArrival(Index As Integer, ByVal bytesTotal As Long)
    Static msgRest As String
    Dim RcvData As String
    Dim rcvText As String
    Dim RcvMsg As String
    Dim tmpLen As String
    Dim MsgLen As Integer
    Dim txtLen As Integer
    Dim msgPos As Integer
    Dim gotMsg As Boolean
    CdiPort(Index).GetData RcvData, vbString
    rcvText = msgRest & RcvData
    msgRest = ""
    txtLen = Len(rcvText)
    gotMsg = True
    msgPos = 1
    While (msgPos < txtLen) And (gotMsg = True)
        gotMsg = False
        If (msgPos + 8) < txtLen Then
            tmpLen = Mid(rcvText, msgPos, 8)
            MsgLen = Val(tmpLen)
        End If
        If (msgPos + 7 + MsgLen) <= txtLen Then
            msgPos = msgPos + 8
            RcvMsg = Mid(rcvText, msgPos, MsgLen)
            msgPos = msgPos + MsgLen
            'RunRemoteMsg RcvMsg
            Label2.Caption = RcvMsg
            gotMsg = True
        End If
    Wend
    If gotMsg = False Then
        msgRest = Mid(rcvText, msgPos)
    End If
    CdiPort_Close 0
    CdiPortIsValid = True
End Sub

Private Sub CdiPort_Error(Index As Integer, ByVal Number As Integer, Description As String, ByVal Scode As Long, ByVal Source As String, ByVal HelpFile As String, ByVal HelpContext As Long, CancelDisplay As Boolean)
    Label2.Caption = Description
    CdiPort_Close 0
    CdiPortIsValid = False
End Sub

Private Sub Command1_Click()
    ShutDownRequested = True
    RemotePort_Close 0
    End
End Sub

Private Sub btnStartApp_Click()
    Dim ShellResult As Double
    Dim CtrlFile As String
    CtrlFile = MyCtrlPath + MyCtrlAppl
    If MyCtrlAppl <> "" Then
        On Error GoTo ErrorHandle
        ShellResult = Shell(CtrlFile, vbNormalFocus)
        If ShellResult > 0 Then
            Label2.Caption = "Started: " & ShellResult & vbNewLine & CtrlFile
        Else
            Label2.Caption = "Error Starting: " & vbNewLine & CtrlFile
        End If
    Else
        Label2.Caption = "No Application Configured:" & vbNewLine & vbNewLine & CtrlFile
    End If
    CdiPortIsValid = True
    Exit Sub
ErrorHandle:
    Label2.Caption = "Error Starting: " & vbNewLine & CtrlFile & vbNewLine & "Error: " & Err.Number & " " & Err.Description
End Sub

Private Sub btnHide_Click()
    Hook Me.hwnd   ' Set up our handler
    AddIconToTray Me.hwnd, Me.Icon, Me.Icon.Handle, App.EXEName + " / Monitoring: " + MyCtrlAppl
    Me.Hide
End Sub

Private Sub Form_Load()
    Dim IniFile As String
    Dim TimeCtrl As Long
    If App.PrevInstance Then
        'MsgBox "Already Started"
        End
    End If
    App.TaskVisible = False
    Set MyMainForm = Me
    IniFile = "c:\ufis\system\" + App.EXEName + ".ini"
    MyCtrlPath = GetIniEntry(IniFile, "GLOBAL", "", "CTRL_PATH", "")
    MyCtrlAppl = GetIniEntry(IniFile, "GLOBAL", "", "CTRL_APPL", "")
    Label3.Caption = "Monitoring: " + MyCtrlAppl
    If Right(MyCtrlPath, 1) <> "\" Then MyCtrlPath = MyCtrlPath + "\"
    Timer1.Enabled = False
    TimeCtrl = Val(GetIniEntry(IniFile, "GLOBAL", "", "CTRL_TIME", "10")) * 1000
    If TimeCtrl < 5000 Then TimeCtrl = 5000
    'MsgBox TimeCtrl
    Timer1.Interval = TimeCtrl
    OpenRemotePort
    CdiPortIsValid = True
    Timer1.Enabled = True
    btnHide_Click
End Sub

Public Sub RemotePort_Close(Index As Integer)
    If RemotePort(Index).State <> sckClosed Then RemotePort(Index).Close
    If RemotePort(0).State <> sckClosed Then RemotePort(0).Close
    If Index > 0 Then Unload RemotePort(Index)
    RemoteIsOpen = False
    RemoteIsBusy = False
    RemoteIdx = -1
    Label1.Caption = "Remote Connection Closed"
    If Not ShutDownRequested Then OpenRemotePort
End Sub

Private Sub RemotePort_Connect(Index As Integer)
    MsgBox "Remote Connected"
    RemoteIdx = 0
    RemoteIsBusy = True
End Sub

Private Sub RemotePort_ConnectionRequest(Index As Integer, ByVal requestID As Long)
    Dim OutMsg As String
    If RemotePort(0).State <> sckClosed Then RemotePort(0).Close
    RemoteIdx = 1
    Load RemotePort(RemoteIdx)
    Label1.Caption = "Connected: " & requestID
    RemotePort(RemoteIdx).Accept requestID
    OutMsg = "CONNEX,HOST,-1||" & RemotePort(0).LocalIP & " / " & RemotePort(0).LocalHostName
    SendRemoteMsg OutMsg
End Sub

Private Sub RemotePort_DataArrival(Index As Integer, ByVal bytesTotal As Long)
    Static msgRest As String
    Dim RcvData As String
    Dim rcvText As String
    Dim RcvMsg As String
    Dim tmpLen As String
    Dim MsgLen As Integer
    Dim txtLen As Integer
    Dim msgPos As Integer
    Dim gotMsg As Boolean
    RemotePort(Index).GetData RcvData, vbString
    rcvText = msgRest & RcvData
    msgRest = ""
    txtLen = Len(rcvText)
    gotMsg = True
    msgPos = 1
    While (msgPos < txtLen) And (gotMsg = True)
        gotMsg = False
        If (msgPos + 8) < txtLen Then
            tmpLen = Mid(rcvText, msgPos, 8)
            MsgLen = Val(tmpLen)
        End If
        If (msgPos + 7 + MsgLen) <= txtLen Then
            msgPos = msgPos + 8
            RcvMsg = Mid(rcvText, msgPos, MsgLen)
            msgPos = msgPos + MsgLen
            RunRemoteMsg RcvMsg
            gotMsg = True
        End If
    Wend
    If gotMsg = False Then
        msgRest = Mid(rcvText, msgPos)
    End If
End Sub

Private Sub RemotePort_Error(Index As Integer, ByVal Number As Integer, Description As String, ByVal Scode As Long, ByVal Source As String, ByVal HelpFile As String, ByVal HelpContext As Long, CancelDisplay As Boolean)
    'MsgBox "Error: " & Number & vbLf & Description
    RemotePort_Close Index
End Sub

Private Sub RemotePort_SendComplete(Index As Integer)
'
End Sub

Private Sub RemotePort_SendProgress(Index As Integer, ByVal bytesSent As Long, ByVal bytesRemaining As Long)
'
End Sub

Public Sub OpenRemotePort()
    On Error GoTo ErrorHandle
    If (Not RemoteIsOpen) And (Not RemoteIsBusy) Then
        RemotePort(0).LocalPort = "4396"
        RemotePort(0).Listen
        RemoteIdx = 0
        Label1.Caption = "Listening"
        Label2.Caption = ""
        RemoteIsOpen = True
    End If
    Exit Sub
ErrorHandle:
    End
End Sub

Private Sub RunRemoteMsg(RcvTcpMsg As String)
    Dim RcvTsk As String
    Dim RcvMsg As String
    Dim RcvCmd As String
    Dim RunCmd As String
    Dim RunIdx As String
    Dim OutMsg As String
    Dim tmpDat As String
    Dim ShellResult As Double
    'MsgBox RcvTcpMsg
    RcvTsk = GetItem(RcvTcpMsg, 1, "||")
    RcvMsg = GetItem(RcvTcpMsg, 2, "||")
    RcvCmd = GetItem(RcvTsk, 1, ",")
    RunCmd = GetItem(RcvTsk, 2, ",")
    RunIdx = GetItem(RcvTsk, 3, ",")
    Label2.Caption = RcvMsg
    Select Case RcvCmd
        Case "START"
            ShellResult = Shell(RcvMsg, vbNormalFocus)
            Label2.Caption = Label2.Caption & vbNewLine & Str(ShellResult)
            SendRemoteMsg "STARTED,APPL," & Str(ShellResult) & "||" & RcvMsg
        Case "CCO"
            SendRemoteMsg "CCA"
        Case Else
            'MsgBox RcvTcpMsg
    End Select
End Sub

Public Sub SendRemoteMsg(OutText As String)
    Dim tmpLen As String
    Dim totLen As Integer
    totLen = Len(OutText)
    tmpLen = Trim(Str(totLen))
    tmpLen = Right(("00000000" & tmpLen), 8)
    'MsgBox "Now Sending Answer"
    RemotePort(RemoteIdx).SendData tmpLen & OutText
    'MsgBox "Answer Sent!"
End Sub


Private Sub Timer1_Timer()
    If ShutDownRequested = True Then
        End
    End If
    If CdiPortIsValid = True Then
        btnConnect_Click
    Else
        'MsgBox "Start Application"
        btnStartApp_Click
    End If
End Sub
' Handler for mouse events occuring in system tray.
Public Sub SysTrayMouseEventHandler()
    SetForegroundWindow Me.hwnd
    PopupMenu RCPopup, vbPopupMenuRightButton
End Sub

Private Sub msg1_Click()
    MsgBox "This is a test message", vbOKOnly, "Hello"
End Sub

Private Sub Rest1_Click()
    Unhook    ' Return event control to windows
    Me.Show
    RemoveIconFromTray
End Sub

Private Sub Exit1_Click()
    Unhook    ' Return event control to windows
    RemoveIconFromTray
    ShutDownRequested = True
    Timer1.Enabled = False
    Timer1.Interval = 500
    Timer1.Enabled = True
End Sub

