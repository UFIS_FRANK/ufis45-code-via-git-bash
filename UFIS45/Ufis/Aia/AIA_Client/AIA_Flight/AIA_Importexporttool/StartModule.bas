Attribute VB_Name = "StartModule"
Option Explicit
Public UpdateMode As Boolean
Public StartedAsImportTool As Boolean
Public ShowStrTabStyle As Integer
Public FullMode As Boolean
Public NoopFullMode As Boolean
Public UseGfrCmd As String
Public AlcErrorList As String
Public ChkErrorList As String
Public AutoSwitch As Boolean
Public Initializing As Boolean
Public MinGrdTime As Long
Private Declare Function GetKeyState Lib "user32" (ByVal nVirtKey As Long) As Integer

Sub Main()
    UseGfrCmd = "GFR"
    Load UfisServer
    UfisServer.TwsCode.Text = ".NBC."
    If InStr(Command, "SHOWSTRTAB") > 0 Then
        StartedAsImportTool = False
        ShowStrTabStyle = 1
        Load StandardRotations
        StandardRotations.Show
        StandardRotations.Refresh
    Else
        StartedAsImportTool = True
        ShowStrTabStyle = 0
        MainDialog.Show
        UfisServer.SetIndicator MainDialog.CedaStatus(0).Left, MainDialog.CedaStatus(0).Top, MainDialog.CedaStatus(0), MainDialog.CedaStatus(3), MainDialog.CedaStatus(1), MainDialog.CedaStatus(2)
        MainDialog.Refresh
    End If
    If UfisServer.ConnectToCeda = True Then
        Screen.MousePointer = 11
        SeasonData.LoadSeaTab
        Screen.MousePointer = 0
    Else
    
    End If
    UfisServer.ModName.Text = "ImportTool"
    UfisServer.ConnectToBcProxy
    ApplicationIsStarted = True
End Sub

Public Sub SetAllFormsOnTop(SetValue As Boolean)
'we do nothing here
End Sub

Public Sub HandleBroadCast(ReqId As String, DestName As String, RecvName As String, CedaCmd As String, ObjName As String, Seq As String, Tws As String, Twe As String, CedaSqlKey As String, Fields As String, Data As String, BcNum As String)
Dim tmpCOD As String
Dim tmpWKS As String
Dim tmpCmd As String
    If InStr("AFTTAB", ObjName) > 0 Then
        tmpWKS = GetItem(Tws, 6, ".")
        If tmpWKS = UfisServer.GetMyWorkStationName Then
            'BC from my WKS
            tmpCOD = GetItem(Tws, 1, ".")
            If tmpCOD = "F" Then
                'Progress Message from OAFOS (REGN)
                Select Case CedaCmd
                    Case "RAC"
                        CedaProgress.EvaluateBc CedaCmd, Tws, CedaSqlKey, Fields, Data
                        NoopFlights.EvaluateBc CedaCmd, Tws, CedaSqlKey, Fields, Data
                    Case "RAX"
    'MsgBox "BC Received: " & ObjName & vbNewLine & CedaCmd & vbNewLine & CedaSqlKey & vbNewLine & Fields & vbNewLine & Data
                        CedaProgress.EvaluateBc CedaCmd, Tws, CedaSqlKey, Fields, Data
                        NoopFlights.EvaluateBc CedaCmd, Tws, CedaSqlKey, Fields, Data
                    Case "EXCO"
                        tmpCmd = GetItem(CedaSqlKey, 1, ",")
                        Select Case tmpCmd
                            Case "ACCBGN", "ACCEND"
                                CedaProgress.EvaluateBc tmpCmd, Tws, CedaSqlKey, Fields, Data
                                NoopFlights.EvaluateBc tmpCmd, Tws, CedaSqlKey, Fields, Data
                            Case Else
                        End Select
                    Case Else
                End Select
            End If
        End If
    End If
End Sub

Public Sub MsgFromUFISAppManager(Orig As Long, Data As String)
Dim tmpFldLst As String
Dim tmpDatLst As String
Dim nlPos As Integer
    If ApplicationIsStarted = True Then
        nlPos = InStr(Data, vbLf)
        If nlPos > 1 Then
            tmpFldLst = Left(Data, nlPos - 1)
            tmpDatLst = Mid(Data, nlPos + 1)
            'TelexPoolHead.SearchFlightTelexes tmpFldLst, tmpDatLst
        Else
            'If MyMsgBox.CallAskUser(0, 0, 0, "Communication Control", "Undefined message received from FIPS", "hand", "", UserAnswer) > 0 Then DoNothing
        End If
    End If
End Sub

Public Sub HandleDisplayChanged()
    
End Sub

Public Sub HandleSysColorsChanged()
    
End Sub

Public Sub HandleTimeChanged()
    
End Sub

Public Sub ShutDownApplication()
Dim i As Integer
Dim cnt As Integer
    On Error GoTo ErrorHandler
    If MyMsgBox.CallAskUser(0, 0, 0, "Application Control", "Do you want to exit ?", "ask", "Yes,No;F", UserAnswer) = 1 Then
        ShutDownRequested = True
        UfisServer.aCeda.CleanupCom
        'cnt = Forms.Count - 1
        'For i = cnt To 0 Step -1
        '    Unload Forms(i)
        'Next
        'Here we leave
        End
    End If
    'If not, we come back
    Exit Sub
ErrorHandler:
    Resume Next
End Sub

Public Sub RefreshMain()
    'do nothing
End Sub

Public Function GetKeybdState() As Integer
    Dim KeyFlags As Integer
    KeyFlags = 0
    If Abs(GetKeyState(KeyCodeConstants.vbKeyShift)) > 1 Then KeyFlags = KeyFlags Or 1
    If Abs(GetKeyState(KeyCodeConstants.vbKeyControl)) > 1 Then KeyFlags = KeyFlags Or 2
    GetKeybdState = KeyFlags
End Function

Public Function DrawBackGround(MyPanel As PictureBox, MyColor As Integer, DrawHoriz As Boolean, DrawDown As Boolean) As Long
    Const intBLUESTART% = 255
    Const intBLUEEND% = 0
    Const intBANDHEIGHT% = 15
    Const intSHADOWSTART% = 64
    Const intSHADOWCOLOR% = 0
    Const intTEXTSTART% = 0
    Const intTEXTCOLOR% = 15
    Const intRed% = 1
    Const intGreen% = 2
    Const intBlue% = 4
    Const intBackRed% = 8
    Const intBackGreen% = 16
    Const intBackBlue% = 32
    Dim sngBlueCur As Single
    Dim sngBlueStep As Single
    Dim intFormHeight As Single
    Dim intFormWidth As Single
    Dim intX As Single
    Dim intY As Single
    Dim iColor As Integer
    Dim iRed As Single, iBlue As Single, iGreen As Single
    Dim ReturnColor As Long
    ReturnColor = vbWhite
    If MyColor >= 0 Then
        intFormHeight = MyPanel.ScaleHeight
        intFormWidth = MyPanel.ScaleWidth
    
        iColor = MyColor
        sngBlueCur = intBLUESTART
    
        If DrawDown Then
            If DrawHoriz Then
                sngBlueStep = intBANDHEIGHT * (intBLUEEND - intBLUESTART) / intFormHeight
                For intY = 0 To intFormHeight Step intBANDHEIGHT
                    If iColor And intBlue Then iBlue = sngBlueCur
                    If iColor And intRed Then iRed = sngBlueCur
                    If iColor And intGreen Then iGreen = sngBlueCur
                    If iColor And intBackBlue Then iBlue = 255 - sngBlueCur
                    If iColor And intBackRed Then iRed = 255 - sngBlueCur
                    If iColor And intBackGreen Then iGreen = 255 - sngBlueCur
                    MyPanel.Line (-1, intY - 1)-(intFormWidth, intY + intBANDHEIGHT), RGB(iRed, iGreen, iBlue), BF
                    sngBlueCur = sngBlueCur + sngBlueStep
                    If intY = 0 Then ReturnColor = RGB(iRed, iGreen, iBlue)
                Next intY
            Else
                sngBlueStep = intBANDHEIGHT * (intBLUEEND - intBLUESTART) / intFormWidth
                For intX = 0 To intFormWidth Step intBANDHEIGHT
                    If iColor And intBlue Then iBlue = sngBlueCur
                    If iColor And intRed Then iRed = sngBlueCur
                    If iColor And intGreen Then iGreen = sngBlueCur
                    If iColor And intBackBlue Then iBlue = 255 - sngBlueCur
                    If iColor And intBackRed Then iRed = 255 - sngBlueCur
                    If iColor And intBackGreen Then iGreen = 255 - sngBlueCur
                    MyPanel.Line (intX - 1, -1)-(intX + intBANDHEIGHT, intFormHeight), RGB(iRed, iGreen, iBlue), BF
                    sngBlueCur = sngBlueCur + sngBlueStep
                Next intX
            End If
        Else
            If DrawHoriz Then
                sngBlueStep = intBANDHEIGHT * (intBLUEEND - intBLUESTART) / intFormHeight
                For intY = intFormHeight To 0 Step -intBANDHEIGHT
                    If iColor And intBlue Then iBlue = sngBlueCur
                    If iColor And intRed Then iRed = sngBlueCur
                    If iColor And intGreen Then iGreen = sngBlueCur
                    If iColor And intBackBlue Then iBlue = 255 - sngBlueCur
                    If iColor And intBackRed Then iRed = 255 - sngBlueCur
                    If iColor And intBackGreen Then iGreen = 255 - sngBlueCur
                    MyPanel.Line (-1, intY - 1)-(intFormWidth, intY + intBANDHEIGHT), RGB(iRed, iGreen, iBlue), BF
                    sngBlueCur = sngBlueCur + sngBlueStep
                Next intY
            Else
                sngBlueStep = intBANDHEIGHT * (intBLUEEND - intBLUESTART) / intFormWidth
                For intX = intFormWidth To 0 Step -intBANDHEIGHT
                    If iColor And intBlue Then iBlue = sngBlueCur
                    If iColor And intRed Then iRed = sngBlueCur
                    If iColor And intGreen Then iGreen = sngBlueCur
                    If iColor And intBackBlue Then iBlue = 255 - sngBlueCur
                    If iColor And intBackRed Then iRed = 255 - sngBlueCur
                    If iColor And intBackGreen Then iGreen = 255 - sngBlueCur
                    MyPanel.Line (intX - 1, -1)-(intX + intBANDHEIGHT, intFormHeight), RGB(iRed, iGreen, iBlue), BF
                    sngBlueCur = sngBlueCur + sngBlueStep
                Next intX
            End If
        End If
    
    End If
    DrawBackGround = ReturnColor
End Function


