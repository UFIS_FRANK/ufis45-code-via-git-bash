VERSION 5.00
Object = "{64E8E382-05E2-11D2-9B1D-9B0630BC8F12}#1.0#0"; "TAB.ocx"
Begin VB.Form frmStep1 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Import-Assistant - Step 1/3"
   ClientHeight    =   10155
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   8220
   Icon            =   "frmStep1.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   10155
   ScaleWidth      =   8220
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin VB.Frame Frame7 
      Caption         =   "Import flight type"
      Height          =   735
      Left            =   180
      TabIndex        =   42
      Top             =   4545
      Width           =   3795
      Begin VB.OptionButton optFlightType 
         Caption         =   "Rotation &flights"
         Height          =   285
         Index           =   1
         Left            =   1665
         TabIndex        =   44
         Tag             =   "IMPORT_TYPE1"
         Top             =   315
         Width           =   1635
      End
      Begin VB.OptionButton optFlightType 
         Caption         =   "&Single flights"
         Height          =   285
         Index           =   0
         Left            =   180
         TabIndex        =   43
         Tag             =   "IMPORT_TYPE0"
         Top             =   315
         Value           =   -1  'True
         Width           =   1230
      End
   End
   Begin VB.Frame Frame6 
      Caption         =   "Header information"
      Height          =   915
      Left            =   180
      TabIndex        =   38
      Top             =   3510
      Width           =   3795
      Begin VB.TextBox txtHeaderLine 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00C0C0C0&
         BeginProperty DataFormat 
            Type            =   1
            Format          =   "0"
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1031
            SubFormatType   =   1
         EndProperty
         Enabled         =   0   'False
         Height          =   330
         Left            =   3150
         TabIndex        =   40
         Tag             =   "HEADER_LINE_TEXT_NO"
         Text            =   "1"
         Top             =   405
         Width           =   375
      End
      Begin VB.CheckBox ckbHeaderLine 
         Caption         =   "&Use line as header"
         Height          =   330
         Left            =   180
         TabIndex        =   39
         Tag             =   "HEADER_LINE_NO"
         Top             =   405
         Width           =   1860
      End
      Begin VB.Label Label8 
         Caption         =   "Line no.:"
         Height          =   240
         Left            =   2340
         TabIndex        =   41
         Top             =   450
         Width           =   690
      End
   End
   Begin VB.Frame Frame5 
      Caption         =   "Line filter"
      Height          =   1770
      Left            =   4185
      TabIndex        =   27
      Top             =   3510
      Width           =   3795
      Begin VB.TextBox txtLineFilter 
         BackColor       =   &H00C0C0C0&
         Enabled         =   0   'False
         Height          =   330
         Index           =   2
         Left            =   2475
         TabIndex        =   35
         Tag             =   "LINE_FILTER_TEXT2"
         ToolTipText     =   "Insert the filter-text"
         Top             =   1260
         Width           =   1050
      End
      Begin VB.TextBox txtLineFilter 
         BackColor       =   &H00C0C0C0&
         Enabled         =   0   'False
         Height          =   330
         Index           =   1
         Left            =   2475
         TabIndex        =   34
         Tag             =   "LINE_FILTER_TEXT1"
         ToolTipText     =   "Insert the length of the item"
         Top             =   810
         Width           =   1050
      End
      Begin VB.TextBox txtLineFilter 
         BackColor       =   &H00C0C0C0&
         Enabled         =   0   'False
         Height          =   330
         Index           =   0
         Left            =   2475
         TabIndex        =   33
         Tag             =   "LINE_FILTER_TEXT0"
         ToolTipText     =   "Insert the position / item number, e.g. '0' or '2.1'"
         Top             =   360
         Width           =   1050
      End
      Begin VB.OptionButton optLineFilter 
         Caption         =   "Not"
         Height          =   240
         Index           =   3
         Left            =   270
         TabIndex        =   31
         Tag             =   "LINE_FILTER3"
         Top             =   1395
         Width           =   735
      End
      Begin VB.OptionButton optLineFilter 
         Caption         =   "Month3LC"
         Height          =   240
         Index           =   2
         Left            =   270
         TabIndex        =   30
         Tag             =   "LINE_FILTER2"
         Top             =   1035
         Width           =   1095
      End
      Begin VB.OptionButton optLineFilter 
         Caption         =   "Text"
         Height          =   240
         Index           =   1
         Left            =   270
         TabIndex        =   29
         Tag             =   "LINE_FILTER1"
         Top             =   675
         Width           =   780
      End
      Begin VB.OptionButton optLineFilter 
         Caption         =   "None"
         Height          =   240
         Index           =   0
         Left            =   270
         TabIndex        =   28
         Tag             =   "LINE_FILTER0"
         Top             =   315
         Value           =   -1  'True
         Width           =   870
      End
      Begin VB.Label Label7 
         Caption         =   "Text:"
         Height          =   240
         Left            =   1710
         TabIndex        =   37
         Top             =   1350
         Width           =   465
      End
      Begin VB.Label Label6 
         Caption         =   "Length:"
         Height          =   285
         Left            =   1710
         TabIndex        =   36
         Top             =   885
         Width           =   645
      End
      Begin VB.Label Label5 
         Caption         =   "Position:"
         Height          =   240
         Left            =   1710
         TabIndex        =   32
         Top             =   450
         Width           =   645
      End
   End
   Begin VB.Frame Frame3 
      Caption         =   "Separators as fieldwidths"
      Height          =   2040
      Left            =   3060
      TabIndex        =   21
      Top             =   1305
      Width           =   4920
      Begin VB.CommandButton cmdChange 
         Caption         =   "C&hange"
         Height          =   285
         Left            =   1845
         TabIndex        =   13
         Top             =   1350
         Width           =   915
      End
      Begin VB.CommandButton cmdRemove 
         Caption         =   "&Remove"
         Height          =   285
         Left            =   1845
         TabIndex        =   12
         Top             =   945
         Width           =   915
      End
      Begin VB.CommandButton cmdAdd 
         Caption         =   "&Add"
         Height          =   285
         Left            =   1845
         TabIndex        =   11
         Top             =   540
         Width           =   915
      End
      Begin TABLib.TAB TabFieldWidths 
         Height          =   1365
         Left            =   3195
         TabIndex        =   24
         TabStop         =   0   'False
         Tag             =   "TAB_FIELDWIDTHS"
         Top             =   405
         Width           =   1545
         _Version        =   65536
         _ExtentX        =   2725
         _ExtentY        =   2408
         _StockProps     =   0
      End
      Begin VB.TextBox txtColNo 
         BeginProperty DataFormat 
            Type            =   1
            Format          =   "0"
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1031
            SubFormatType   =   1
         EndProperty
         Height          =   285
         Left            =   225
         TabIndex        =   9
         Top             =   675
         Width           =   1140
      End
      Begin VB.TextBox txtCharNo 
         BeginProperty DataFormat 
            Type            =   1
            Format          =   "0"
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1031
            SubFormatType   =   1
         EndProperty
         Height          =   285
         Left            =   225
         TabIndex        =   10
         Top             =   1440
         Width           =   1140
      End
      Begin VB.Label Label4 
         Caption         =   "In column:"
         Height          =   195
         Left            =   225
         TabIndex        =   23
         Top             =   360
         Width           =   1050
      End
      Begin VB.Label Label3 
         Caption         =   "Behind char:"
         Height          =   195
         Left            =   225
         TabIndex        =   22
         Top             =   1170
         Width           =   1050
      End
   End
   Begin VB.Frame Frame2 
      Caption         =   "Separators as characters"
      Height          =   2040
      Left            =   180
      TabIndex        =   20
      Top             =   1305
      Width           =   2670
      Begin VB.OptionButton optSepaChar 
         Caption         =   "&Others:"
         Height          =   195
         Index           =   4
         Left            =   225
         TabIndex        =   7
         Tag             =   "FORMAT_TYPE4"
         Top             =   1485
         Width           =   1140
      End
      Begin VB.OptionButton optSepaChar 
         Caption         =   "B&lank"
         Height          =   195
         Index           =   3
         Left            =   1575
         TabIndex        =   6
         Tag             =   "FORMAT_TYPE3"
         Top             =   922
         Width           =   915
      End
      Begin VB.OptionButton optSepaChar 
         Caption         =   "&Komma"
         Height          =   195
         Index           =   2
         Left            =   1575
         TabIndex        =   4
         Tag             =   "FORMAT_TYPE2"
         Top             =   360
         Value           =   -1  'True
         Width           =   915
      End
      Begin VB.OptionButton optSepaChar 
         Caption         =   "Se&mikolon"
         Height          =   195
         Index           =   1
         Left            =   225
         TabIndex        =   5
         Tag             =   "FORMAT_TYPE1"
         Top             =   922
         Width           =   1140
      End
      Begin VB.OptionButton optSepaChar 
         Caption         =   "&Tab"
         Height          =   195
         Index           =   0
         Left            =   225
         TabIndex        =   3
         Tag             =   "FORMAT_TYPE0"
         Top             =   360
         Width           =   1140
      End
      Begin VB.TextBox txtSeparator 
         BackColor       =   &H00C0C0C0&
         Enabled         =   0   'False
         Height          =   285
         Left            =   1575
         TabIndex        =   8
         Tag             =   "FORMAT_TYPE_TXT"
         Top             =   1440
         Width           =   690
      End
   End
   Begin VB.CommandButton cmdFinish 
      Caption         =   "F&inish"
      Enabled         =   0   'False
      Height          =   330
      Left            =   6705
      TabIndex        =   16
      Top             =   9675
      Width           =   1275
   End
   Begin VB.CommandButton cmdNext 
      Caption         =   "&Next >"
      Height          =   330
      Left            =   5175
      TabIndex        =   14
      Top             =   9675
      Width           =   1275
   End
   Begin VB.CommandButton cmdBack 
      Caption         =   "< &Back"
      Enabled         =   0   'False
      Height          =   330
      Left            =   3825
      TabIndex        =   15
      Top             =   9675
      Width           =   1275
   End
   Begin VB.CommandButton cmdCancel 
      Caption         =   "&Cancel"
      Height          =   330
      Left            =   2340
      TabIndex        =   17
      Top             =   9675
      Width           =   1275
   End
   Begin VB.Frame Frame1 
      Caption         =   "Input File"
      Height          =   1005
      Left            =   180
      TabIndex        =   0
      Top             =   135
      Width           =   7800
      Begin VB.OptionButton optGeneral 
         Caption         =   "Fi&xed width"
         Height          =   240
         Index           =   1
         Left            =   225
         TabIndex        =   2
         Tag             =   "FORMAT_TYPE_FIXED"
         Top             =   585
         Width           =   1320
      End
      Begin VB.OptionButton optGeneral 
         Caption         =   "Se&parated"
         Height          =   240
         Index           =   0
         Left            =   225
         TabIndex        =   1
         Tag             =   "FORMAT_TYPE_ITEM"
         Top             =   270
         Value           =   -1  'True
         Width           =   1140
      End
      Begin VB.Label Label2 
         Caption         =   "- fields are formatted in columns, filled up with spaces between each field"
         Height          =   240
         Left            =   1530
         TabIndex        =   19
         Top             =   585
         Width           =   5370
      End
      Begin VB.Label Label1 
         Caption         =   "- chars like komma or tabs separate the fields"
         Height          =   240
         Left            =   1530
         TabIndex        =   18
         Top             =   270
         Width           =   5370
      End
   End
   Begin VB.Frame Frame4 
      Caption         =   "Raw data"
      Height          =   4020
      Left            =   180
      TabIndex        =   25
      Top             =   5490
      Width           =   7800
      Begin TABLib.TAB TabRawData 
         Height          =   3390
         Left            =   135
         TabIndex        =   26
         TabStop         =   0   'False
         Top             =   405
         Width           =   7485
         _Version        =   65536
         _ExtentX        =   13203
         _ExtentY        =   5980
         _StockProps     =   0
      End
   End
End
Attribute VB_Name = "frmStep1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private strSepa As String

Private imProcessLines As Integer

Private bOptGeneral As Boolean
Private bOptSepaChar As Boolean
Private bTabFieldWidths As Boolean
Private bCkbHeaderLine As Boolean
Private bOptFlightType As Boolean
Private bOptLineFilter As Boolean

Private bStartByConfig As Boolean

Private Sub ckbHeaderLine_Click()
    HandleTabRawData
    Dim i As Integer
'    For i = 0 To TabRawData.GetLineCount - 1
'        TabRawData.SetLineColor CLng(i), vbBlack, vbWhite
'    Next i

    If ckbHeaderLine.Value = 1 Then
        txtHeaderLine.Enabled = True
        txtHeaderLine.BackColor = vbWhite
        If IsNumeric(txtHeaderLine.Text) = True Then
            TabRawData.SetLineColor CLng(txtHeaderLine.Text) - 1, vbBlack, 12632256
        End If
    Else
        txtHeaderLine.Enabled = False
        txtHeaderLine.BackColor = 12632256
    End If
    TabRawData.RedrawTab
    bCkbHeaderLine = True
End Sub

Private Sub cmdCancel_Click()
    Unload frmHiddenHelper
    Unload frmStep2
    Unload frmStep3
    Unload Me
End Sub

Private Sub cmdNext_Click()
    ' save data and hide form
    'frmHiddenHelper.SaveForm Me, "c:\ufis\test.ini"
    Me.Hide
    
    'load next step and reset if necessary
'    Load frmStep2
'    If bOptGeneral = True Then
'    End If
'    If bOptSepaChar = True Then
'    End If
'    If bTabFieldWidths = True Then
'    End If
'    If bCkbHeaderLine = True Then
'    End If
'    If bOptFlightType = True Then
'    End If
'    If bOptLineFilter = True Then
'    End If
    If bOptGeneral = True Or _
       bOptSepaChar = True Or _
       bTabFieldWidths = True Or _
       bCkbHeaderLine = True Or _
       bOptFlightType = True Or _
       bOptLineFilter = True Then
       frmStep2.ResetContent
    End If

    bOptGeneral = False
    bOptSepaChar = False
    bTabFieldWidths = False
    bCkbHeaderLine = False
    bOptFlightType = False
    bOptLineFilter = False

    If bStartByConfig = True Then
        frmStep2.StartByConfig
    Else
        frmStep2.StartAsNew
    End If
End Sub

Public Sub StartByConfig()
    bStartByConfig = True

    bOptGeneral = False
    bOptSepaChar = False
    bTabFieldWidths = False
    bCkbHeaderLine = False
    bOptFlightType = False
    bOptLineFilter = False

    If frmHiddenHelper.bVisibleMode = True Then
        Me.Show
    End If
End Sub

Public Sub StartAsNew()
    bStartByConfig = False
    HandleTabRawData
    If frmHiddenHelper.bVisibleMode = True Then
        Me.Show
    End If
End Sub

Private Sub Form_Load()
    imProcessLines = 50
    InitTabs

    ' remember user-changes
    bOptGeneral = True
    bOptSepaChar = True
    bTabFieldWidths = True
    bCkbHeaderLine = True
    bOptFlightType = True
    bOptLineFilter = True
End Sub

Private Sub InitTabs()
    InitTabGeneral TabFieldWidths
    TabFieldWidths.HeaderLengthString = "40,50"
    TabFieldWidths.HeaderString = "ColNo,CharNo"
    TabFieldWidths.ColumnAlignmentString = "R,R"
    TabFieldWidths.EmptyAreaBackColor = 12632256

    InitTabGeneral TabRawData
    TabRawData.MainHeader = True
    TabRawData.MainHeaderOnly = True
    TabRawData.ShowHorzScroller True
    TabRawData.EnableHeaderSizing True
    TabRawData.FontSize = 14
End Sub

Private Sub InitTabGeneral(ByRef rTab As TABLib.Tab)
    rTab.ResetContent
    rTab.FontName = "Courier New"
    rTab.FontSize = 12
    rTab.SetTabFontBold True
    'rTab.LeftTextOffset = 2
End Sub

' - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
' - reaction to button-events
' - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
Private Sub cmdAdd_Click()
    Dim blAdd As Boolean
    blAdd = True
    
    If optGeneral(0).Value = True Then
        If Len(txtColNo.Text) = 0 Or Len(txtCharNo.Text) = 0 Then
            blAdd = False
            MsgBox "Please enter a numeric value for the column and the character number!", vbInformation, "No value"
        End If
    Else
        If Len(txtCharNo.Text) = 0 Then
            blAdd = False
            MsgBox "Please enter a numeric value for the column and the character number!", vbInformation, "No value"
        End If
    End If

    If blAdd = True Then
        TabFieldWidths.InsertTextLine txtColNo.Text + "," + txtCharNo.Text, False
        SortTabFieldWidths
        TabFieldWidths.RedrawTab
        txtColNo.Text = ""
        txtCharNo.Text = ""
        HandleTabRawData
        bTabFieldWidths = True
    End If
End Sub

Private Sub cmdChange_Click()
    TabFieldWidths.UpdateTextLine TabFieldWidths.GetCurrentSelected, txtColNo.Text + "," + txtCharNo.Text, False
    SortTabFieldWidths
    TabFieldWidths.RedrawTab
    txtColNo.Text = ""
    txtCharNo.Text = ""
    HandleTabRawData
    bTabFieldWidths = True
End Sub

Private Sub cmdRemove_Click()
    TabFieldWidths.DeleteLine TabFieldWidths.GetCurrentSelected
    TabFieldWidths.RedrawTab
    txtColNo.Text = ""
    txtCharNo.Text = ""
    HandleTabRawData
    bTabFieldWidths = True
End Sub

Private Sub optFlightType_Click(Index As Integer)
    bOptFlightType = True
End Sub

' - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
' - reaction to general radio/option-buttons
' - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
Private Sub optGeneral_Click(Index As Integer)
    Dim i As Integer

    If Index = 1 Then
        For i = 0 To optSepaChar.UBound
            optSepaChar.Item(i).Enabled = False
        Next i
        txtSeparator.Enabled = False
        txtSeparator.BackColor = 12632256 'C0C0C0h
        txtColNo.Enabled = False
        txtColNo.BackColor = 12632256 'C0C0C0h
        TabFieldWidths.CreateCellObj "GREY", 12632256, vbBlack, 12, False, False, True, 0, "Courier New"
        TabFieldWidths.SetColumnProperty 0, "GREY"
    Else
        For i = 0 To optSepaChar.UBound
            optSepaChar.Item(i).Enabled = True
        Next i
        If optSepaChar(4).Value = True Then
            txtSeparator.Enabled = True
            txtSeparator.BackColor = vbWhite
        End If
        txtColNo.Enabled = True
        txtColNo.BackColor = vbWhite
        TabFieldWidths.ResetColumnProperty 0
    End If

    TabFieldWidths.ResetContent
    TabFieldWidths.RedrawTab
    HandleTabRawData
    bOptGeneral = True
End Sub

Private Sub optLineFilter_Click(Index As Integer)
    If Index = 0 Then
        txtLineFilter(0).Enabled = False
        txtLineFilter(0).BackColor = 12632256
        txtLineFilter(1).Enabled = False
        txtLineFilter(1).BackColor = 12632256
        txtLineFilter(2).Enabled = False
        txtLineFilter(2).BackColor = 12632256
    ElseIf Index = 2 Then
        txtLineFilter(0).Enabled = True
        txtLineFilter(0).BackColor = vbWhite
        txtLineFilter(1).Enabled = False
        txtLineFilter(1).BackColor = 12632256
        txtLineFilter(2).Enabled = False
        txtLineFilter(2).BackColor = 12632256
    Else
        txtLineFilter(0).Enabled = True
        txtLineFilter(0).BackColor = vbWhite
        txtLineFilter(1).Enabled = True
        txtLineFilter(1).BackColor = vbWhite
        txtLineFilter(2).Enabled = True
        txtLineFilter(2).BackColor = vbWhite
    End If
    HandleTabRawData
    bOptLineFilter = True
End Sub

Private Sub optSepaChar_Click(Index As Integer)
    If optSepaChar(4).Value = True Then
        txtSeparator.Enabled = True
        txtSeparator.BackColor = vbWhite
        strSepa = txtSeparator.Text
    Else
        txtSeparator.Enabled = False
        txtSeparator.BackColor = 12632256 'C0C0C0h
        If Index = 0 Then
            strSepa = Chr(9)
        ElseIf Index = 1 Then
            strSepa = ";"
        ElseIf Index = 2 Then
            strSepa = ","
        ElseIf Index = 3 Then
            strSepa = " "
        End If
    End If
    HandleTabRawData
    bOptSepaChar = True
End Sub

Private Sub TabFieldWidths_SendLButtonClick(ByVal LineNo As Long, ByVal ColNo As Long)
    If LineNo > -1 Then
        txtColNo.Text = TabFieldWidths.GetColumnValue(LineNo, 0)
        txtCharNo.Text = TabFieldWidths.GetColumnValue(LineNo, 1)
    End If
End Sub

' - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
' - reaction to user-input in text-field
' - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
Private Sub txtCharNo_Change()
    txt_Change txtCharNo
End Sub

Private Sub txtColNo_Change()
    txt_Change txtColNo
End Sub

Private Sub txt_Change(ByRef rTxt As TextBox)
    If IsNumeric(rTxt.Text) = False And Len(rTxt.Text) > 0 Then
        MsgBox "Please enter a numeric value!", vbInformation, "Numeric Value Required"
        If Len(rTxt.Text) > 0 Then
            rTxt.Text = Left(rTxt.Text, Len(rTxt.Text) - 1)
        End If
    End If
    rTxt.SelStart = Len(rTxt.Text)
End Sub

Private Sub txtHeaderLine_Change()
    ckbHeaderLine_Click
    bCkbHeaderLine = True
End Sub

Private Sub txtLineFilter_Change(Index As Integer)
    bOptLineFilter = True
    HandleTabRawData
End Sub

Private Sub txtSeparator_Change()
    strSepa = txtSeparator.Text
    HandleTabRawData
    bOptSepaChar = True
End Sub

' - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
' - handling the preview-tab after each user-action
' - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
Private Sub HandleTabRawData()
    Screen.MousePointer = vbHourglass

    TabRawData.ResetContent
    frmHiddenHelper.TabTmp.SetFieldSeparator Chr(30)

    If optGeneral(0).Value = True Then
        HandleTabRawData_NormalSeparated
    Else
        HandleTabRawData_FixedWidth
    End If

    TabRawData.AutoSizeByHeader = True
    TabRawData.AutoSizeColumns

    ' color the header-line
    If ckbHeaderLine.Value = 1 Then
        If IsNumeric(txtHeaderLine.Text) = True Then
            TabRawData.SetLineColor CLng(txtHeaderLine.Text) - 1, vbBlack, 12632256
        End If
    End If

    TabRawData.AutoSizeColumns
    TabRawData.RedrawTab
    HandleItemColumnTab

    Screen.MousePointer = vbDefault
End Sub

Private Sub HandleTabRawData_NormalSeparated()
    Dim llCnt As Long
    Dim llMaxItems As Long
    Dim llTmpCnt As Long
    Dim strHeaderString As String
    Dim strHeaderLengthString As String
    Dim strMainheaderRanges As String
    Dim strMainHeaderValues As String
    Dim strMainHeaderAlignment As String

    ' getting the max. number of elements in a line for formatting the TAB
    If strSepa = "" Then strSepa = ","
    llMaxItems = 0
    For llCnt = 0 To frmHiddenHelper.TabOrigFile.GetLineCount
        llTmpCnt = ItemCount(frmHiddenHelper.TabOrigFile.GetLineValues(llCnt), strSepa)
        If llTmpCnt > llMaxItems Then
            llMaxItems = llTmpCnt
        End If
    Next llCnt
    llMaxItems = llMaxItems + TabFieldWidths.GetLineCount

    'now we've got the max-number of elements, so let's do the layout of the preview-tab
    TabRawData.SetFieldSeparator strSepa
    Dim ilCountSubColumns As Integer
    Dim ilTotalColumns As Integer
    Dim i As Integer
    Dim strTmp As String
    llCnt = 0
    While llCnt < llMaxItems
    'For llCnt = 0 To llMaxItems - 1
        strTmp = TabFieldWidths.GetLinesByColumnValue(0, CStr(llCnt), 0)
        If strTmp = "" Then
            ilCountSubColumns = 1
        Else
            ilCountSubColumns = ItemCount(strTmp, ",") + 1
            llMaxItems = llMaxItems - ilCountSubColumns + 1
        End If
        For i = 1 To ilCountSubColumns Step 1
            strHeaderString = strHeaderString + Str(ilTotalColumns) + strSepa
            strHeaderLengthString = strHeaderLengthString + "10" + ","
            ilTotalColumns = ilTotalColumns + 1
        Next i
        strMainheaderRanges = strMainheaderRanges + CStr(ilCountSubColumns) + ","
        strMainHeaderValues = strMainHeaderValues + Str(llCnt) + strSepa
        strMainHeaderAlignment = strMainHeaderAlignment + "C" + ","
        llCnt = llCnt + 1
    Wend
    'Next llCnt
    RemoveLastChar strHeaderString
    RemoveLastChar strHeaderLengthString
    RemoveLastChar strMainheaderRanges
    RemoveLastChar strMainHeaderValues
    RemoveLastChar strMainHeaderAlignment

    'setting the layout
    TabRawData.HeaderString = strHeaderString
    TabRawData.HeaderLengthString = strHeaderLengthString
    TabRawData.SetMainHeaderValues strMainheaderRanges, strMainHeaderValues, ""
    TabRawData.HeaderAlignmentString = strMainHeaderAlignment

    ' Getting the values into the TAB
    FillTabRawData 0, imProcessLines, True
End Sub

Private Sub HandleTabRawData_FixedWidth()
    Dim i As Integer
    Dim ilCnt As Integer
    Dim strWidths As String
    Dim strHeaderString As String
    Dim strHeaderLengthString As String
    Dim strMainheaderRanges As String
    Dim strMainHeaderValues As String
    Dim strMainHeaderAlignment As String

    TabRawData.SetFieldSeparator Chr(30)
    strWidths = TabFieldWidths.SelectDistinct(1, "", "", ",", True)
    ilCnt = ItemCount(strWidths, ",")

    For i = 0 To ilCnt
        strHeaderString = strHeaderString + Str(i) + Chr(30)
        strHeaderLengthString = strHeaderLengthString + "10" + ","
        strMainheaderRanges = strMainheaderRanges + CStr(1) + ","
        strMainHeaderValues = strMainHeaderValues + Str(i) + Chr(30)
        strMainHeaderAlignment = strMainHeaderAlignment + "C" + ","
    Next i
    RemoveLastChar strHeaderString
    RemoveLastChar strHeaderLengthString
    RemoveLastChar strMainheaderRanges
    RemoveLastChar strMainHeaderValues
    RemoveLastChar strMainHeaderAlignment

    'setting the layout
    TabRawData.HeaderString = strHeaderString
    TabRawData.HeaderLengthString = strHeaderLengthString
    TabRawData.SetMainHeaderValues strMainheaderRanges, strMainHeaderValues, ""
    TabRawData.HeaderAlignmentString = strMainHeaderAlignment

    ' Getting the values into the TAB
    FillTabRawData 0, imProcessLines, False
End Sub

Private Sub FillTabRawData(ByRef riStart As Integer, riEnd As Integer, pType As Boolean)

    Dim strBuffer As String
    Dim strLine As String
    Dim ilStart As Integer
    Dim ilEnd As Integer
    Dim i As Integer
    Dim blAdd As Boolean

    If riStart = -1 Then
        ilStart = 0
    Else
        ilStart = riStart
    End If

    If riEnd = -1 Then
        ilEnd = imProcessLines
    Else
        ilEnd = riEnd
    End If

    For i = ilStart To ilEnd Step 1
        blAdd = False
        ' patching the line to fit into the RawData-Preview-Tab
        strLine = frmHiddenHelper.TabOrigFile.GetLineValues(i)
        If pType = True Then
            PatchLine_NormalSeparated strLine
        Else
            PatchLine_FixedWidth strLine
        End If

        ' checking the filter-criteria
        If ckbHeaderLine.Value = 1 Then
            If IsNumeric(txtHeaderLine.Text) = True Then
                If i + 1 = txtHeaderLine.Text Then
                    blAdd = True
                End If
            End If
        End If

        If blAdd = True Or FitsLineToFilter(strLine) = True Then
            TabRawData.InsertTextLine strLine, False
        Else
            If ilEnd < 100 Then
                ilEnd = ilEnd + 1
            End If
        End If
    Next i
End Sub

Private Sub PatchLine_FixedWidth(ByRef rStr As String)
    Dim strWidths As String
    Dim ilCnt As Integer
    Dim i As Integer
    Dim strChars As String

    strWidths = TabFieldWidths.SelectDistinct(1, "", "", ",", True)
    frmHiddenHelper.TabTmp.ResetContent
    frmHiddenHelper.TabTmp.InsertBuffer strWidths, ","
    frmHiddenHelper.TabTmp.Sort 0, True, True
    ilCnt = frmHiddenHelper.TabTmp.GetLineCount

    For i = 1 To ilCnt Step 1
        strChars = frmHiddenHelper.TabTmp.GetColumnValue(i - 1, 0)
        If IsNumeric(strChars) = True Then
            If CInt(strChars) < Len(rStr) Then
                rStr = Left(rStr, CInt(strChars) + i - 1) & Chr(30) & Mid(rStr, CInt(strChars) + i)
            End If
        End If
    Next i

End Sub

Private Sub PatchLine_NormalSeparated(ByRef rStr As String)
    Dim i As Integer
    Dim j As Integer
    Dim strItem As String
    Dim strLineNo As String
    Dim ilCnt As Integer
    Dim strChars As String

    frmHiddenHelper.TabTmp.ResetContent
    frmHiddenHelper.TabTmp.InsertBuffer rStr, strSepa
    rStr = ""

    For i = 0 To frmHiddenHelper.TabTmp.GetLineCount Step 1
        strItem = frmHiddenHelper.TabTmp.GetColumnValue(i, 0)
        strLineNo = TabFieldWidths.GetLinesByColumnValue(0, CStr(i), 0)

        If Len(strLineNo) > 0 Then 'we have to split up the item
            ilCnt = ItemCount(strLineNo, ",")
            For j = 1 To ilCnt Step 1
                strChars = TabFieldWidths.GetColumnValue(CLng(GetItem(strLineNo, j, ",")), 1)
                If IsNumeric(strChars) = True Then
                    strItem = Left(strItem, CInt(strChars) + j - 1) & strSepa & Mid(strItem, CInt(strChars) + j)
                End If
            Next j
        End If
        
        rStr = rStr + strItem + strSepa
    Next i
End Sub

Private Sub RemoveLastChar(ByRef rString As String)
    If Len(rString) > 0 Then
        rString = Left(rString, Len(rString) - 1)
    End If
End Sub

Private Sub SortTabFieldWidths()
    If optGeneral(0).Value = True Then
        TabFieldWidths.Sort "0,1", True, True
    Else
        TabFieldWidths.Sort "1", True, True
    End If
End Sub

Public Function GetFieldSeparator() As String
    GetFieldSeparator = strSepa
End Function

Public Function GetRawDataBuffer(ByVal sLineSeparator As String) As String
    GetRawDataBuffer = Me.TabRawData.GetBuffer(0, Me.TabRawData.GetLineCount - 1, sLineSeparator)
End Function

Public Function FitsLineToFilter(ByRef rLine As String) As Boolean
    If optLineFilter(0).Value = True Then
        'Filter "None"
        FitsLineToFilter = True
        Exit Function
    Else
        Dim strItem As String
        Dim strLookup As String
        Dim strLineNo As String
        Dim strCompare As String
        Dim strPatchedColNo As String

        strLookup = Format(txtLineFilter(0).Text, "000.0")
        strLookup = Replace(strLookup, ",", ".")
        strLineNo = frmHiddenHelper.TabItemColumn.GetLinesByColumnValue(1, strLookup, 0)
        If Len(strLineNo) > 0 And IsNumeric(strLineNo) = True Then
            strPatchedColNo = frmHiddenHelper.TabItemColumn.GetColumnValue(CLng(strLineNo), 0)
            strItem = GetItem(rLine, CInt(strPatchedColNo) + 1, strSepa)
            strCompare = txtLineFilter(2).Text

            If optLineFilter(1).Value = True Then
                'Filter "Text"
                If IsNumeric(txtLineFilter(1).Text) = True And Len(strCompare) > 0 Then
                    If Left(strItem, CInt(txtLineFilter(1).Text)) <> strCompare Then
                        FitsLineToFilter = False
                    Else
                        FitsLineToFilter = True
                    End If
                Else
                    FitsLineToFilter = True
                End If
                Exit Function
            ElseIf optLineFilter(2).Value = True Then
                'Filter "Month 3LC"
                If InStr(1, UfisLib.MonthList, "," & strItem & ",") = 0 And Left(UfisLib.MonthList, 3) <> strItem And Right(UfisLib.MonthList, 3) <> strItem Then
                    FitsLineToFilter = False
                Else
                    FitsLineToFilter = True
                End If
                Exit Function
            ElseIf optLineFilter(3).Value = True Then
                'Filter "Not"
                If IsNumeric(txtLineFilter(1).Text) = True Then
                    If Left(strItem, CInt(txtLineFilter(1).Text)) <> strCompare Then
                        FitsLineToFilter = True
                    Else
                        FitsLineToFilter = False
                    End If
                Else
                    FitsLineToFilter = True
                End If

                Exit Function
            End If
        Else
            'no valid format yet
            FitsLineToFilter = True
        End If
    End If
End Function

Private Sub HandleItemColumnTab()
    Dim i As Integer
    Dim ilLen As Integer
    Dim ilCnt As Integer
    Dim ilActColNo As Integer
    Dim ilCountRanges As Integer
    Dim ilCountSubColumns As Integer
    Dim strTmp As String
    Dim strFromChar As String
    Dim strToChar As String

    ilActColNo = 0
    ilCountRanges = ItemCount(TabRawData.GetMainHeaderRanges, ",") - 1

    frmHiddenHelper.TabItemColumn.ResetContent

    If optGeneral(0).Value = True Then 'we have items
        For ilCnt = 0 To ilCountRanges '- 1
            strTmp = TabFieldWidths.GetLinesByColumnValue(0, CStr(ilCnt), 0)
            If strTmp = "" Then
                ilCountSubColumns = 1
            Else
                ilCountSubColumns = ItemCount(strTmp, ",") + 1
            End If

            If ilCountSubColumns = 1 Then
                AddRowToTabItemColumn ilActColNo, ilCnt, 0, CStr(ilCnt), GetMaxColChars(ilActColNo)
                ilActColNo = ilActColNo + 1
            Else
                For i = 1 To ilCountSubColumns Step 1
                    If i = 1 Then
                        strToChar = TabFieldWidths.GetColumnValue(CLng(GetItem(strTmp, i, ",")), 1)
                        AddRowToTabItemColumn ilActColNo, ilCnt, i, CStr(ilCnt) & "L" & strToChar, CInt(strToChar)
                    ElseIf i = ilCountSubColumns Then
                        ilLen = GetMaxColChars(ilActColNo)
                        strFromChar = TabFieldWidths.GetColumnValue(CLng(GetItem(strTmp, i - 1, ",")), 1)
                        AddRowToTabItemColumn ilActColNo, ilCnt, i, CStr(ilCnt) & "R" & CStr(ilLen), ilLen
                    Else
                        strFromChar = CStr(CLng(TabFieldWidths.GetColumnValue(CLng(GetItem(strTmp, i - 1, ",")), 1)) + 1)
                        strToChar = TabFieldWidths.GetColumnValue(CLng(GetItem(strTmp, i, ",")), 1)
                        AddRowToTabItemColumn ilActColNo, ilCnt, i, CStr(ilCnt) & "M" & strFromChar & "|" & strToChar, CInt(strToChar) - CInt(strFromChar)
                    End If
                    ilActColNo = ilActColNo + 1
                Next i
            End If
        Next ilCnt
    Else 'we have fixed widths
        Dim ilLastStartPos As Integer
        Dim ilActStartPos As Integer
        ilLastStartPos = 0
        For ilCnt = 0 To ilCountRanges
            If ilCnt < ilCountRanges Then
                ilActStartPos = TabFieldWidths.GetColumnValue(CLng(ilCnt), 1)
                AddRowToTabItemColumn ilCnt, ilCnt, 0, CStr(ilLastStartPos + 1) & "L" & CStr(ilActStartPos - ilLastStartPos), ilActStartPos - ilLastStartPos
                ilLastStartPos = ilActStartPos
            Else
                AddRowToTabItemColumn ilCnt, ilCnt, 0, CStr(ilActStartPos + 1) & "L" & CStr(GetMaxColChars(ilCnt)), GetMaxColChars(ilCnt)
            End If
        Next ilCnt
    End If
    
End Sub
Private Sub AddRowToTabItemColumn(ByRef rColNo As Integer, ByRef rRangeNo As Integer, ByRef rItemNo As Integer, ByRef rBuildLaw As String, ByRef rLen As Integer)
    Dim strRangeNo As String
    Dim strLine As String

    strRangeNo = Format(rRangeNo, "000")
    strLine = CStr(rColNo) & "," & strRangeNo & "." & CStr(rItemNo) & "," & rBuildLaw & "," & CStr(rLen)
    frmHiddenHelper.TabItemColumn.InsertTextLine strLine, True
End Sub
Private Function GetMaxColChars(ByRef rColNo As Integer) As Integer
    Dim ilMax As Integer
    Dim ilCount As Integer
    Dim llLineCount As Long
    Dim l As Long

    ilMax = 0
    llLineCount = TabRawData.GetLineCount - 1

    For l = 0 To llLineCount Step 1
        If ckbHeaderLine.Value = 0 Or CStr(l + 1) <> txtHeaderLine.Text Then
            ilCount = Len(TabRawData.GetColumnValue(l, rColNo))
            If ilCount > ilMax Then
                ilMax = ilCount
            End If
        End If
    Next l
    GetMaxColChars = ilMax
End Function
