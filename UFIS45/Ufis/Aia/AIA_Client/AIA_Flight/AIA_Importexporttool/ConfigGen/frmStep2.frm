VERSION 5.00
Object = "{64E8E382-05E2-11D2-9B1D-9B0630BC8F12}#1.0#0"; "TAB.ocx"
Begin VB.Form frmStep2 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Import-Assistant - Step 2/3"
   ClientHeight    =   9300
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   8265
   Icon            =   "frmStep2.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   9300
   ScaleWidth      =   8265
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin VB.Frame Frame2 
      Caption         =   "Properties of actual column"
      Height          =   4110
      Left            =   4215
      TabIndex        =   19
      Top             =   225
      Width           =   3810
      Begin VB.OptionButton optFormat 
         Caption         =   "Format ""&Text"", length:"
         Height          =   195
         Index           =   0
         Left            =   315
         TabIndex        =   5
         Top             =   3330
         Width           =   2085
      End
      Begin VB.OptionButton optFormat 
         Caption         =   "Format ""N&umber"", digits:"
         Height          =   195
         Index           =   1
         Left            =   315
         TabIndex        =   7
         Top             =   3690
         Width           =   2085
      End
      Begin VB.TextBox txtNumberOfDigits 
         BackColor       =   &H00C0C0C0&
         Enabled         =   0   'False
         Height          =   285
         Index           =   0
         Left            =   2520
         TabIndex        =   6
         Top             =   3285
         Width           =   870
      End
      Begin VB.TextBox txtNumberOfDigits 
         BackColor       =   &H00C0C0C0&
         Enabled         =   0   'False
         Height          =   285
         Index           =   1
         Left            =   2520
         TabIndex        =   8
         Top             =   3645
         Width           =   870
      End
      Begin VB.TextBox txtDescription 
         BackColor       =   &H00FFFFFF&
         Height          =   285
         Left            =   1980
         TabIndex        =   1
         ToolTipText     =   "Enter a value for the caption of the column"
         Top             =   1620
         Width           =   1410
      End
      Begin VB.TextBox txtColumnName 
         Alignment       =   2  'Center
         BackColor       =   &H00C0C0C0&
         Enabled         =   0   'False
         Height          =   285
         Left            =   1980
         TabIndex        =   27
         TabStop         =   0   'False
         Top             =   915
         Width           =   1050
      End
      Begin VB.TextBox txtDisplayName 
         BackColor       =   &H0000FFFF&
         Height          =   285
         Left            =   1980
         TabIndex        =   0
         ToolTipText     =   "Enter a value for the caption of the column"
         Top             =   1260
         Width           =   1410
      End
      Begin VB.TextBox txtColumnPosition 
         Alignment       =   2  'Center
         BackColor       =   &H00C0C0C0&
         Enabled         =   0   'False
         Height          =   285
         Left            =   1980
         TabIndex        =   26
         TabStop         =   0   'False
         Top             =   570
         Width           =   1410
      End
      Begin VB.TextBox txtRawData 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00C0C0C0&
         Enabled         =   0   'False
         Height          =   285
         Left            =   3105
         TabIndex        =   25
         TabStop         =   0   'False
         Top             =   915
         Width           =   285
      End
      Begin VB.TextBox txtClearByValue 
         Height          =   285
         Index           =   1
         Left            =   2025
         TabIndex        =   4
         ToolTipText     =   "a special value you want to delete, e.g. ""----"""
         Top             =   2745
         Width           =   1410
      End
      Begin VB.TextBox txtClearByValue 
         Height          =   285
         Index           =   0
         Left            =   2025
         TabIndex        =   3
         ToolTipText     =   "a special value you want to delete, e.g. ""----"""
         Top             =   2415
         Width           =   1410
      End
      Begin VB.CheckBox ckbClearZeros 
         Caption         =   "Clear &zeros"
         Height          =   240
         Left            =   270
         TabIndex        =   2
         ToolTipText     =   "deletes appearing zeros in the column"
         Top             =   2145
         Width           =   1365
      End
      Begin VB.Label Label6 
         Caption         =   "Description:"
         Height          =   285
         Left            =   180
         TabIndex        =   28
         Top             =   1620
         Width           =   1590
      End
      Begin VB.Label Label5 
         Caption         =   "Actual column position:"
         Height          =   285
         Left            =   180
         TabIndex        =   24
         Top             =   570
         Width           =   1725
      End
      Begin VB.Line Line2 
         X1              =   225
         X2              =   3600
         Y1              =   3135
         Y2              =   3135
      End
      Begin VB.Label Label4 
         Caption         =   "...delete also the cols"
         Height          =   195
         Left            =   270
         TabIndex        =   23
         Top             =   2790
         Width           =   1860
      End
      Begin VB.Label Label3 
         Caption         =   "Clear by value..."
         Height          =   195
         Left            =   270
         TabIndex        =   22
         Top             =   2460
         Width           =   1185
      End
      Begin VB.Line Line1 
         X1              =   225
         X2              =   3600
         Y1              =   2025
         Y2              =   2025
      End
      Begin VB.Label Label2 
         Caption         =   "Display column name:"
         Height          =   285
         Left            =   180
         TabIndex        =   21
         Top             =   1260
         Width           =   1590
      End
      Begin VB.Label Label1 
         Caption         =   "Actual column name:"
         Height          =   285
         Left            =   180
         TabIndex        =   20
         Top             =   915
         Width           =   1590
      End
   End
   Begin VB.Frame Frame3 
      Caption         =   "Possible columns"
      Height          =   4110
      Left            =   180
      TabIndex        =   17
      Top             =   225
      Width           =   3840
      Begin TABLib.TAB TabPossibleColumns 
         Height          =   3480
         Left            =   180
         TabIndex        =   18
         TabStop         =   0   'False
         Tag             =   "TAB_POSSIBLE_COLUMNS"
         Top             =   360
         Width           =   3435
         _Version        =   65536
         _ExtentX        =   6059
         _ExtentY        =   6138
         _StockProps     =   0
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Preview:"
      Height          =   1860
      Left            =   180
      TabIndex        =   15
      Top             =   4500
      Width           =   7845
      Begin TABLib.TAB TabPreview 
         Height          =   1230
         Left            =   180
         TabIndex        =   16
         TabStop         =   0   'False
         Tag             =   "TAB_PREVIEW"
         Top             =   405
         Width           =   7440
         _Version        =   65536
         _ExtentX        =   13123
         _ExtentY        =   2170
         _StockProps     =   0
      End
   End
   Begin VB.Frame Frame4 
      Caption         =   "Raw data:"
      Height          =   1995
      Left            =   180
      TabIndex        =   13
      Top             =   6525
      Width           =   7845
      Begin TABLib.TAB TabRawData 
         Height          =   1410
         Left            =   135
         TabIndex        =   14
         TabStop         =   0   'False
         Tag             =   "TAB_RAWDATA"
         Top             =   405
         Width           =   7530
         _Version        =   65536
         _ExtentX        =   13282
         _ExtentY        =   2487
         _StockProps     =   0
      End
   End
   Begin VB.CommandButton cmdFinish 
      Caption         =   "F&inish"
      Enabled         =   0   'False
      Height          =   330
      Left            =   6660
      TabIndex        =   11
      Top             =   8730
      Width           =   1275
   End
   Begin VB.CommandButton cmdNext 
      Caption         =   "&Next >"
      Height          =   330
      Left            =   5130
      TabIndex        =   9
      Top             =   8730
      Width           =   1275
   End
   Begin VB.CommandButton cmdBack 
      Caption         =   "< &Back"
      Height          =   330
      Left            =   3780
      TabIndex        =   10
      Top             =   8730
      Width           =   1275
   End
   Begin VB.CommandButton cmdCancel 
      Caption         =   "&Cancel"
      Height          =   330
      Left            =   2295
      TabIndex        =   12
      Top             =   8730
      Width           =   1275
   End
   Begin VB.Menu mnuContext 
      Caption         =   "Properties"
      Visible         =   0   'False
      Begin VB.Menu mnuProperties 
         Caption         =   "Properties"
         Enabled         =   0   'False
      End
      Begin VB.Menu mnuFileBar0 
         Caption         =   "-"
      End
      Begin VB.Menu mnuRemoveColumn 
         Caption         =   "Remove Column From Preview"
      End
   End
End
Attribute VB_Name = "frmStep2"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private strSepPrev As String
Private imLastClickedColumn As Integer ' the column of TabRawData
Private imLastClickedLine As Integer ' is the line of TabPossibleColumns

Private bmFirstShow As Boolean
Private bmChangeInternal As Boolean

Private Sub cmdBack_Click()
    Me.Hide
    frmStep1.Show
End Sub

Public Sub ResetContent()
    strSepPrev = frmStep1.TabRawData.GetFieldSeparator
    Dim i As Integer
    For i = TabPossibleColumns.GetLineCount - 1 To 0 Step -1
        If TabPossibleColumns.GetColumnValue(CLng(i), 10) = "R" Then
            TabPossibleColumns.DeleteLine CLng(i)
        End If
    Next i
    ClearProperties
    InitTabs
    ReadFields
    TabRawData.ColSelectionRemoveAll
    TabPreview.ColSelectionRemoveAll
    frmHiddenHelper.TabConfiguration.ResetContent
    HandlePreviewTab
    'frmHiddenHelper.Show
End Sub

Private Sub InitTabs()
    ' - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    ' - - Preview-tab
    ' - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    InitTabGeneral TabPreview
    InitTabPreview

    ' - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    ' - - RawData-tab
    ' - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    InitTabGeneral TabRawData
    InitTabRawData

    ' - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    ' - - PossibleColumns-tab
    ' - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    InitTabGeneral TabPossibleColumns
    Me.TabPossibleColumns.SetFieldSeparator Chr(30)
    Me.TabPossibleColumns.ShowRowSelection = True
End Sub

Private Sub InitTabRawData()
    With frmStep1
        Me.TabRawData.SetFieldSeparator .TabRawData.GetFieldSeparator
        Me.TabRawData.MainHeader = .TabRawData.MainHeader
        Me.TabRawData.EnableHeaderSizing True
        Me.TabRawData.FontSize = .TabRawData.FontSize
        If .ckbHeaderLine.Value = 1 And IsNumeric(.txtHeaderLine) = True Then
            Dim strHeaderString As String
            strHeaderString = .TabRawData.GetLineValues(CLng(.txtHeaderLine) - 1)
            While ItemCount(strHeaderString, .TabRawData.GetFieldSeparator) <= ItemCount(.TabRawData.HeaderLengthString, .TabRawData.GetFieldSeparator)
                strHeaderString = strHeaderString & .TabRawData.GetFieldSeparator
            Wend
            Me.TabRawData.HeaderString = strHeaderString
        Else
            Me.TabRawData.HeaderString = .TabRawData.HeaderString
        End If
        Me.TabRawData.HeaderLengthString = .TabRawData.HeaderLengthString
        Me.TabRawData.SetMainHeaderValues .TabRawData.GetMainHeaderRanges, .TabRawData.GetMainHeaderValues, ""
        Me.TabRawData.InsertBuffer .GetRawDataBuffer(Chr(10)), Chr(10)
        If .ckbHeaderLine.Value = 1 And IsNumeric(.txtHeaderLine) = True Then
            Me.TabRawData.DeleteLine CLng(.txtHeaderLine) - 1
        End If
    End With
    Me.TabRawData.RedrawTab
End Sub

Private Sub InitTabPreview()
    TabPreview.SetFieldSeparator strSepPrev
    TabPreview.HeaderLengthString = "0"
    TabPreview.HeaderString = "0"
End Sub

Private Sub InitTabGeneral(ByRef rTab As TABLib.Tab)
    rTab.ResetContent
    rTab.FontName = "Courier New"
    rTab.FontSize = 12
    rTab.SetTabFontBold True
    rTab.ShowHorzScroller True
    rTab.ShowRowSelection = False
    rTab.EmptyAreaBackColor = 12632256
    rTab.EmptyAreaRightColor = 12632256
End Sub

Private Sub cmdCancel_Click()
    Unload frmHiddenHelper
    Unload frmStep1
    Unload frmStep3
    Unload Me
End Sub

Private Sub cmdNext_Click()
    Me.Hide
    frmStep3.Start
End Sub

Private Sub Form_Load()
    'strSepPrev = Chr(30)
    strSepPrev = frmStep1.TabRawData.GetFieldSeparator
    Me.TabPossibleColumns.ResetContent
    Me.TabPreview.ResetContent
    Me.TabRawData.ResetContent
    bmFirstShow = True
End Sub

Private Sub HandlePreviewTab()
    Dim ilColCount As Integer
    Dim ilLineCount As Integer
    Dim strColName As String
    Dim strLineNo As String
    Dim strLine As String
    Dim strItem As String
    Dim i As Integer
    Dim j As Integer
    Dim k As Integer
    Dim l As Integer

    TabPreview.ResetContent
    ilColCount = ItemCount(TabPreview.HeaderString, strSepPrev) - 1
    ilLineCount = TabRawData.GetLineCount - 1
    With frmHiddenHelper
        For i = 0 To ilLineCount Step 1
            strLine = ""
            For j = 0 To ilColCount Step 1
                strItem = ""
                strLineNo = TabPossibleColumns.GetLinesByColumnValue(5, CStr(j), 0)
                If Len(strLineNo) > 0 And IsNumeric(GetItem(strLineNo, 1, ",")) = True Then
                    strColName = TabPossibleColumns.GetColumnValue(CLng(GetItem(strLineNo, 1, ",")), 0)
                    strLineNo = .TabConfiguration.GetLinesByMultipleColumnValue("0,1", strColName & ";COLUMNS", 0)
                    If Len(strLineNo) > 0 And IsNumeric(strLineNo) = True Then
                        .TabTmp.ResetContent
                        .TabTmp.InsertBuffer .TabConfiguration.GetColumnValue(CLng(strLineNo), 2), "+"
                        For k = 1 To .TabTmp.GetLineCount Step 1
                            strItem = strItem & TabRawData.GetColumnValue(CLng(i), CLng(.TabTmp.GetColumnValue(k - 1, 0)))
                        Next k
                    End If
                    strLine = strLine & strItem & strSepPrev
                End If
            Next j
            TabPreview.InsertTextLine strLine, False
        Next i
        If TabPreview.HeaderString <> "0" Then
            TabPreview.EnableHeaderSizing True
            TabPreview.AutoSizeByHeader = True
            TabPreview.AutoSizeColumns
        Else
            TabPreview.EnableHeaderSizing False
            TabPreview.AutoSizeByHeader = False
        End If
        TabPreview.RedrawTab
    End With
End Sub



Private Sub mnuRemoveColumn_Click()
    RemoveColumnFromPreview CLng(imLastClickedColumn)
    TabPreview.RedrawTab
    TabPossibleColumns.RedrawTab
End Sub

Private Sub TabPossibleColumns_SendLButtonClick(ByVal LineNo As Long, ByVal ColNo As Long)
    Dim llTextColor As Long
    Dim llBackColor As Long
    Dim ilColNo As Integer

    TabPossibleColumns.GetLineColor LineNo, llTextColor, llBackColor

    If llBackColor = 12632256 Then 'grey
        ilColNo = TabPossibleColumns.GetColumnValue(LineNo, 5)
        If ilColNo > -1 Then
            TabPreview_SendLButtonClick -1, CLng(ilColNo)
        End If
        imLastClickedLine = LineNo
        UpdateProperties
    ElseIf llBackColor = 8438015 Then 'orange
        Dim strColNo As String
        strColNo = TabPossibleColumns.GetColumnValue(LineNo, 5)
        If IsNumeric(strColNo) = True Then
            ilColNo = CInt(strColNo)
            imLastClickedLine = LineNo
            Me.TabPreview.ColSelectionRemoveAll
            Me.TabRawData.ColSelectionRemoveAll
            If ilColNo > -1 Then
                Me.TabRawData.ColSelectionAdd CLng(ilColNo)
                Me.TabRawData.RedrawTab
            End If
            UpdateProperties
        End If
    End If
End Sub

Private Sub TabPossibleColumns_SendLButtonDblClick(ByVal LineNo As Long, ByVal ColNo As Long)
    Dim strColName  As String
    Dim llTextColor As Long
    Dim llBackColor As Long
    Dim ilColNo As Integer

    TabPossibleColumns.GetLineColor LineNo, llTextColor, llBackColor
    If llBackColor <> 12632256 Then
        If TabPossibleColumns.GetColumnValue(LineNo, 10) <> "R" Then
            TabPossibleColumns.SetLineColor LineNo, vbBlack, 12632256
            strColName = TabPossibleColumns.GetColumnValue(LineNo, 0)
            AddColumnToPreview strColName
            TabPossibleColumns.SetColumnValue LineNo, 5, CStr(ItemCount(Me.TabPreview.HeaderString, Me.TabPreview.GetFieldSeparator) - 1)
            TabPossibleColumns.SetColumnValue LineNo, 6, strColName
            TabPossibleColumns.RedrawTab
            ilColNo = ItemCount(TabPreview.HeaderString, TabPreview.GetFieldSeparator) - 1
            TabPreview_SendLButtonClick -1, CLng(ilColNo)
        End If
    End If
    imLastClickedLine = LineNo
    UpdateProperties
End Sub

Private Sub TabPossibleColumns_SendRButtonClick(ByVal LineNo As Long, ByVal ColNo As Long)
    Dim ilPreviewTabColNo As Integer
    Dim strColName As String
    Dim strTmpHeaderString As String

    strColName = TabPossibleColumns.GetColumnValue(LineNo, 0)
    strTmpHeaderString = Replace(TabPreview.HeaderString, strSepPrev, ",")
    ilPreviewTabColNo = GetRealItemNo(strTmpHeaderString, strColName)
    If ilPreviewTabColNo > -1 Then
        imLastClickedColumn = ilPreviewTabColNo
        imLastClickedLine = LineNo
        PopupMenu mnuContext
    End If
End Sub

Private Sub TabPreview_SendLButtonClick(ByVal LineNo As Long, ByVal ColNo As Long)
    TabPreview.ColSelectionRemoveAll
    TabPreview.ColSelectionAdd ColNo

    imLastClickedColumn = CInt(ColNo) + 1

    Dim strLineNo As String
    strLineNo = Me.TabPossibleColumns.GetLinesByColumnValue(5, CStr(ColNo), 0)
    If Len(strLineNo) > 0 And IsNumeric(GetItem(strLineNo, 1, ",")) = True Then
        If IsNumeric(GetItem(strLineNo, 1, ",")) = True Then
            imLastClickedLine = CInt(GetItem(strLineNo, 1, ","))
            TabPossibleColumns.SetCurrentSelection CLng(imLastClickedLine)
        End If
    End If
    UpdateProperties

    PreviewColChanged ColNo

    TabPreview.RedrawTab
    TabRawData.RedrawTab

End Sub

Private Sub TabPreview_SendRButtonClick(ByVal LineNo As Long, ByVal ColNo As Long)
    TabPreview_SendLButtonClick LineNo, ColNo
    'imLastClickedColumn = CInt(ColNo)
    PopupMenu mnuContext
End Sub

Private Sub TabRawData_SendLButtonClick(ByVal LineNo As Long, ByVal ColNo As Long)
    Dim strSelCol As String
    Dim strSelCols As String
    Dim strSearchColText As String
    Dim strLineNo As String
    Dim strTmp As String
    Dim blFound As Boolean
    Dim l As Long
    Dim llPossibleColLine As Long
    Dim ilLength As Integer

    strSelCol = TabPreview.GetSelectedColumns
    If Len(strSelCol) = 0 Or IsNumeric(strSelCol) = False Then
        Exit Sub
    End If

    strLineNo = TabPossibleColumns.GetLinesByColumnValue(5, strSelCol, 0)
    If Len(strLineNo) = 0 Or IsNumeric(GetItem(strLineNo, 1, ",")) = False Then
        Exit Sub
    Else
        llPossibleColLine = CLng(GetItem(strLineNo, 1, ","))
    End If

    strSearchColText = TabPossibleColumns.GetColumnValue(llPossibleColLine, 0)
    strLineNo = frmHiddenHelper.TabConfiguration.GetLinesByMultipleColumnValue("0,1", strSearchColText & ";COLUMNS", 0)

    With frmHiddenHelper
        If Len(strLineNo) > 0 And IsNumeric(strLineNo) = True Then
            ' the line in TabPossibleColumns have been already double-clicked, so that
            ' it is in TabPreview and it has already assigned columns from the TabRawData

            ' look for the assigned RawData-Columns
            .TabTmp.SetFieldSeparator ";"
            .TabTmp.ResetContent
            .TabTmp.InsertBuffer .TabConfiguration.GetColumnValue(CLng(strLineNo), 2), "+"
            strTmp = .TabTmp.GetLinesByColumnValue(0, CStr(ColNo), 0)
            If Len(strTmp) > 0 And IsNumeric(strTmp) = True Then
                ' the column was assigned, so let's remove it
                .TabTmp.DeleteLine CLng(strTmp)
                TabRawData.ColSelectionRemove ColNo
                RemoveLineFromPossibleColTab ColNo
            Else
                ' the column was not assigned, so let's add it
                .TabTmp.InsertTextLine CStr(ColNo), False
                TabRawData.ColSelectionAdd ColNo
                AddLineToPossibleColTab ColNo
            End If
            strTmp = .TabTmp.SelectDistinct("0", "", "", "+", False)
            .TabConfiguration.SetColumnValue CLng(strLineNo), 2, strTmp

            'adding composition-information and length-information
            ilLength = 0
            strTmp = ""
            TabPossibleColumns.SetColumnValue llPossibleColLine, 4, strTmp
            For l = 0 To .TabTmp.GetLineCount - 1 Step 1
                strLineNo = .TabItemColumn.GetLinesByColumnValue(0, .TabTmp.GetColumnValue(l, 0), 0)
                If Len(strLineNo) > 0 And IsNumeric(strLineNo) = True Then
                    strTmp = strTmp & .TabItemColumn.GetColumnValue(CLng(strLineNo), 2) & "+"
                    ilLength = ilLength + .TabItemColumn.GetColumnValue(CLng(strLineNo), 3)
                End If
            Next l
            If Len(strTmp) > 0 Then strTmp = Left(strTmp, Len(strTmp) - 1)
            TabPossibleColumns.SetColumnValue llPossibleColLine, 4, strTmp
            TabPossibleColumns.SetColumnValue llPossibleColLine, 9, CStr(ilLength)
        Else
            strTmp = strSearchColText & ";COLUMNS;" & CStr(ColNo)
            .TabConfiguration.InsertTextLine strTmp, False
            TabRawData.ColSelectionAdd ColNo
            AddLineToPossibleColTab ColNo

            'adding composition-information and length-information
            strLineNo = .TabItemColumn.GetLinesByColumnValue(0, CStr(ColNo), 0)
            If Len(strLineNo) > 0 And IsNumeric(strLineNo) = True Then
                TabPossibleColumns.SetColumnValue llPossibleColLine, 4, .TabItemColumn.GetColumnValue(CLng(strLineNo), 2)
                TabPossibleColumns.SetColumnValue llPossibleColLine, 9, .TabItemColumn.GetColumnValue(CLng(strLineNo), 3)
            End If
        End If
    End With
    TabPossibleColumns.RedrawTab
    TabRawData.RedrawTab
    HandlePreviewTab
    UpdateProperties
End Sub

Private Sub PreviewColChanged(ByRef rlNewCol As Long)
    Dim strSearchColText As String
    Dim strLineNo As String
    Dim strSelCols As String
    Dim l As Long
    Dim llCount As Long
    Dim llLineNo As Long

    TabRawData.ColSelectionRemoveAll

    strSearchColText = TabPossibleColumns.GetColumnValue(CLng(imLastClickedLine), 0)
    strLineNo = frmHiddenHelper.TabConfiguration.GetLinesByMultipleColumnValue("0,1", strSearchColText & ";COLUMNS", 0)

    If Len(strLineNo) > 0 And IsNumeric(strLineNo) = True Then
        strSelCols = frmHiddenHelper.TabConfiguration.GetColumnValue(CLng(strLineNo), 2)
        llCount = ItemCount(strSelCols, "+")
        For l = 1 To llCount Step 1
            TabRawData.ColSelectionAdd CLng(GetItem(strSelCols, CInt(l), "+"))
        Next l
    End If
End Sub

Private Sub ReadFields()
    Dim i As Integer
    Dim sFile As String
    Dim strFileLine As String

    Me.TabPossibleColumns.ResetContent
    Me.TabPossibleColumns.SetFieldSeparator Chr(30)
    Me.TabPossibleColumns.HeaderLengthString = "60,150,0,0,80,20,60,10,80,20,10,0,250"
    Me.TabPossibleColumns.HeaderString = "Fields" & Chr(30) & "Descr." & Chr(30) & Chr(30) & Chr(30) & _
                                        "Composition" & Chr(30) & "Pos" & Chr(30) & "Display" & Chr(30) & _
                                        "CZ" & Chr(30) & "ClearByVal" & Chr(30) & "Len" & Chr(30) & "R" & _
                                        Chr(30) & "Reg." & Chr(30) & Chr(30)

    sFile = "c:\ufis\system\ConfigGen.cfg"
    i = FreeFile
    Open sFile For Input As #i
    Do While Not EOF(i)
        Line Input #i, strFileLine
        Me.TabPossibleColumns.InsertTextLine strFileLine, False
    Loop
    Close #i

    If frmStep1.optFlightType(1).Value = True Then
        ' display rotation flights
        HandleArrivalDeparture
        DeleteSingleFlightEntries
    Else
        ' display only single flights
        Delete_A_and_D
    End If
    HandleMandatory
    'Me.TabPossibleColumns.Sort 0, True, True
    Me.TabPossibleColumns.Sort 1, True, True
    Me.TabPossibleColumns.RedrawTab
End Sub

Private Sub HandleArrivalDeparture()
    'FLNO,Flight Number,M,AD
    Dim strLineNo As String
    Dim strLine As String
    Dim llLineNo As Long
    Dim i As Integer
    strLineNo = Me.TabPossibleColumns.GetLinesByColumnValue(3, "AD", 2)
    For i = 1 To ItemCount(strLineNo, ",") Step 1
        llLineNo = GetItem(strLineNo, i, ",")
        strLine = Me.TabPossibleColumns.GetLineValues(llLineNo)
        Me.TabPossibleColumns.InsertTextLine strLine, False
        Me.TabPossibleColumns.SetColumnValue llLineNo, 0, Me.TabPossibleColumns.GetColumnValue(llLineNo, 0) & "A"
        Me.TabPossibleColumns.SetColumnValue llLineNo, 1, Me.TabPossibleColumns.GetColumnValue(llLineNo, 1) & " Arrival"
        llLineNo = Me.TabPossibleColumns.GetLineCount - 1
        Me.TabPossibleColumns.SetColumnValue llLineNo, 0, Me.TabPossibleColumns.GetColumnValue(llLineNo, 0) & "D"
        Me.TabPossibleColumns.SetColumnValue llLineNo, 1, Me.TabPossibleColumns.GetColumnValue(llLineNo, 1) & " Departure"
    Next i
End Sub

Private Sub DeleteSingleFlightEntries()
    Dim strLineNo As String
    Dim i As Integer

    strLineNo = Me.TabPossibleColumns.GetLinesByColumnValue(3, "S", 0)
    For i = ItemCount(strLineNo, ",") To 1 Step -1
        Me.TabPossibleColumns.DeleteLine CLng(GetItem(strLineNo, i, ","))
    Next i
End Sub

Private Sub Delete_A_and_D()
    Dim strLineNo As String
    Dim i As Integer

    strLineNo = Me.TabPossibleColumns.GetLinesByColumnValue(3, "A", 0)
    For i = ItemCount(strLineNo, ",") To 1 Step -1
        Me.TabPossibleColumns.DeleteLine CLng(GetItem(strLineNo, i, ","))
    Next i

    strLineNo = Me.TabPossibleColumns.GetLinesByColumnValue(3, "D", 0)
    For i = ItemCount(strLineNo, ",") To 1 Step -1
        Me.TabPossibleColumns.DeleteLine CLng(GetItem(strLineNo, i, ","))
    Next i
End Sub

Private Sub HandleMandatory()
    Dim strLineNo As String
    Dim i As Integer
    Dim llLineNo As Long

    strLineNo = Me.TabPossibleColumns.GetLinesByColumnValue(2, "M", 2)
    For i = 1 To ItemCount(strLineNo, ",") Step 1
        llLineNo = GetItem(strLineNo, i, ",")
        Me.TabPossibleColumns.SetLineColor llLineNo, vbBlack, vbYellow
    Next i
    For i = 1 To ItemCount(Me.TabPreview.HeaderString, strSepPrev) Step 1
        strLineNo = Me.TabPossibleColumns.GetLinesByColumnValue(0, GetItem(Me.TabPreview.HeaderString, i, strSepPrev), 0)
        If Len(strLineNo) > 0 And IsNumeric(strLineNo) = True Then
            Me.TabPossibleColumns.SetLineColor CLng(strLineNo), vbBlack, 12632256
        End If
    Next i
    strLineNo = Me.TabPossibleColumns.GetLinesByColumnValue(10, "R", 0)
    For i = 1 To ItemCount(strLineNo, ",") Step 1
        llLineNo = GetItem(strLineNo, i, ",")
        Me.TabPossibleColumns.SetLineColor llLineNo, vbBlack, 8438015
    Next i
End Sub

Private Sub AddColumnToPreview(ByRef rColName As String)
    Dim strTmp As String
    Dim blFirstColumn As Boolean

    blFirstColumn = False
    strTmp = TabPreview.HeaderLengthString
    If strTmp = "0" Then
        blFirstColumn = True
    End If

    ' do the HeaderLengthString first!
    If blFirstColumn = False Then
        strTmp = strTmp & ",40"
    Else
        strTmp = "40"
    End If
    TabPreview.HeaderLengthString = strTmp

    
    ' do the HeaderLengthString first!
    strTmp = TabPreview.HeaderString
    If blFirstColumn = False Then
        strTmp = strTmp & rColName
    Else
        strTmp = rColName
    End If
    TabPreview.HeaderString = strTmp

    HandlePreviewTab
End Sub

Private Sub RemoveColumnFromPreview(ByRef rColNo As Long)
    Dim strTmp As String
    Dim strLineNo As String
    Dim strColName As String
    Dim strHeaderString As String
    Dim strHeaderLengthString As String
    Dim i As Integer
    Dim llLineNo As Long

    strHeaderString = TabPreview.HeaderString
    strHeaderLengthString = TabPreview.HeaderLengthString

    With frmHiddenHelper.TabTmp
        ' handle HeaderString
        .ResetContent
        .SetFieldSeparator ";"
        .InsertBuffer strHeaderString, strSepPrev
        strColName = .GetColumnValue(rColNo - 1, 0)
        .DeleteLine rColNo - 1
        strHeaderString = .SelectDistinct("0", "", "", strSepPrev, False)
        If Len(strHeaderString) = 0 Then
            strHeaderString = "0"
        End If
        TabPreview.HeaderString = strHeaderString
        
        ' handle HeaderLengthString
        .ResetContent
        .InsertBuffer strHeaderLengthString, ","
        .DeleteLine rColNo - 1
        strHeaderLengthString = .SelectDistinct("0", "", "", ",", False)
        If Len(strHeaderLengthString) = 0 Then
            strHeaderLengthString = "0"
        End If

        TabPreview.HeaderLengthString = strHeaderLengthString
    End With

    With frmHiddenHelper
        ' remove from configuration-tab
        strLineNo = .TabConfiguration.GetLinesByMultipleColumnValue("0,1", strColName & ";COLUMNS", 0)
        If Len(strLineNo) > 0 And IsNumeric(strLineNo) = True Then
            .TabTmp.ResetContent
            .TabTmp.InsertBuffer .TabConfiguration.GetColumnValue(CLng(strLineNo), 2), "+"
            For i = .TabTmp.GetLineCount - 1 To 0 Step -1
                'RemoveLineFromPossibleColTab Me.TabRawData.GetColumnValue(CLng(i), CLng(.TabTmp.GetColumnValue(i, 0)))
                RemoveLineFromPossibleColTab CLng(.TabTmp.GetColumnValue(i, 0))
            Next i
        End If

        ' remove from configuration-tab
        strLineNo = .TabConfiguration.GetLinesByColumnValue(0, strColName, 0)
        For i = 1 To ItemCount(strLineNo, ",") Step 1
            .TabConfiguration.DeleteLine CLng(GetItem(strLineNo, i, ","))
        Next i
        .TabConfiguration.RedrawTab
    End With

    ' Handle Color of row (mandatory or not)
    If TabPossibleColumns.GetColumnValue(imLastClickedLine, 10) <> "R" Then
        Dim ilPos As Integer
        Dim ilTmpPos As Integer

        ilPos = TabPossibleColumns.GetColumnValue(imLastClickedLine, 5)
        TabPossibleColumns.SetColumnValue imLastClickedLine, 5, ""
        If TabPossibleColumns.GetColumnValue(CLng(imLastClickedLine), 2) = "M" Then
            TabPossibleColumns.SetLineColor CLng(imLastClickedLine), vbBlack, vbYellow
        Else
            TabPossibleColumns.SetLineColor CLng(imLastClickedLine), vbBlack, vbWhite
        End If

        ' reset attributes
        TabPossibleColumns.SetColumnValue CLng(imLastClickedLine), 4, ""
        TabPossibleColumns.SetColumnValue CLng(imLastClickedLine), 5, ""

        'decrease position of the other columns
        For i = 0 To TabPossibleColumns.GetLineCount - 1 Step 1
            If TabPossibleColumns.GetColumnValue(i, 10) <> "R" Then
                strTmp = TabPossibleColumns.GetColumnValue(i, 5)
                If IsNumeric(strTmp) = True Then
                    ilTmpPos = CInt(strTmp)
                    If ilTmpPos > ilPos Then
                        TabPossibleColumns.SetColumnValue i, 5, CStr(ilTmpPos - 1)
                    End If
                End If
            End If
        Next i
    End If

    HandlePreviewTab

    If TabPreview.HeaderString = "0" And TabPreview.HeaderLengthString = "0" Then
        imLastClickedColumn = -1
        imLastClickedLine = -1
    End If
    TabPreview.ColSelectionRemoveAll
    TabRawData.ColSelectionRemoveAll
    'TabRawData.RedrawTab
End Sub

Public Sub StartByConfig()
    If bmFirstShow = True Then
        bmFirstShow = False
        HandleMandatory
        Me.TabPossibleColumns.ShowHorzScroller True
        Me.TabPossibleColumns.RedrawTab

        
        Me.TabPreview.ShowHorzScroller True
        Me.TabPreview.RedrawTab

        Me.TabRawData.EnableHeaderSizing True
        Me.TabRawData.ShowHorzScroller True
        Me.TabRawData.RedrawTab
    End If
    If frmHiddenHelper.bVisibleMode = True Then
        Me.Show
    End If
End Sub

Public Sub StartAsNew()
    If bmFirstShow = True Then
        bmFirstShow = False
        ReadFields
    End If
    If frmHiddenHelper.bVisibleMode = True Then
        Me.Show
    End If
End Sub

' - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
' - - handling of the properties
' - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
Private Sub ckbClearZeros_Click()
    If bmChangeInternal = False Then
        If imLastClickedLine > -1 Then
            Dim strItem As String
            If ckbClearZeros.Value = 1 Then
                strItem = "1"
            Else
                strItem = ""
            End If
            Me.TabPossibleColumns.SetColumnValue CLng(imLastClickedLine), 7, strItem
            Me.TabPossibleColumns.RedrawTab
        End If
    End If
End Sub

Private Sub txtClearByValue_Change(Index As Integer)
    If bmChangeInternal = False Then
        If imLastClickedLine > -1 Then
            Dim strItem  As String
            strItem = txtClearByValue(0).Text & "," & txtClearByValue(1).Text
            Me.TabPossibleColumns.SetColumnValue CLng(imLastClickedLine), 8, strItem
            Me.TabPossibleColumns.RedrawTab
        End If
    End If
End Sub

Private Sub optFormat_Click(Index As Integer)
    If optFormat(0).Value = True Then
        txtNumberOfDigits(0).Enabled = True
        txtNumberOfDigits(0).BackColor = vbWhite
        txtNumberOfDigits(1).Enabled = False
        txtNumberOfDigits(1).BackColor = 12632256
    Else
        txtNumberOfDigits(1).Enabled = True
        txtNumberOfDigits(1).BackColor = vbWhite
        txtNumberOfDigits(0).Enabled = False
        txtNumberOfDigits(0).BackColor = 12632256
    End If
    If bmChangeInternal = False Then
        SetFormat
    End If
End Sub

Private Sub txtDescription_Change()
    If bmChangeInternal = False Then
        Me.TabPossibleColumns.SetColumnValue imLastClickedLine, 1, txtDescription.Text
        Me.TabPossibleColumns.RedrawTab
    End If
End Sub

Private Sub txtNumberOfDigits_Change(Index As Integer)
    If bmChangeInternal = False Then
        SetFormat
    End If
End Sub

Private Sub SetFormat()
    If imLastClickedLine > -1 Then
        Dim strItem As String
        If optFormat(0).Value = True Then
            strItem = txtNumberOfDigits(0).Text
        ElseIf optFormat(1).Value = True Then
            strItem = "N" & txtNumberOfDigits(1).Text
        End If
        Me.TabPossibleColumns.SetColumnValue imLastClickedLine, 9, strItem
        Me.TabPossibleColumns.RedrawTab
    End If
End Sub

Private Sub txtDisplayName_Change()
    If bmChangeInternal = False Then
        If imLastClickedLine > -1 Then
            Dim strNewHeader As String
            Dim ilColPos As String
            Dim strTmp As String
    
            Me.TabPossibleColumns.SetColumnValue imLastClickedLine, 6, txtDisplayName.Text
            strTmp = Me.TabPossibleColumns.GetColumnValue(imLastClickedLine, 5)
            If IsNumeric(strTmp) = True Then
                ilColPos = CInt(strTmp)
                frmHiddenHelper.TabTmp.ResetContent

                If Me.TabPossibleColumns.GetColumnValue(imLastClickedLine, 10) = "R" Then
                    frmHiddenHelper.TabTmp.SetFieldSeparator Chr(30)
                    frmHiddenHelper.TabTmp.InsertBuffer Me.TabRawData.HeaderString, Me.TabRawData.GetFieldSeparator
                    frmHiddenHelper.TabTmp.UpdateTextLine CLng(ilColPos), txtDisplayName.Text, True
                    strNewHeader = frmHiddenHelper.TabTmp.SelectDistinct("0", "", "", Me.TabRawData.GetFieldSeparator, False)
                    Me.TabRawData.HeaderString = strNewHeader
                    Me.TabRawData.RedrawTab
                Else
                    frmHiddenHelper.TabTmp.SetFieldSeparator Chr(30)
                    frmHiddenHelper.TabTmp.InsertBuffer Me.TabPreview.HeaderString, Me.TabPreview.GetFieldSeparator
                    frmHiddenHelper.TabTmp.UpdateTextLine CLng(ilColPos), txtDisplayName.Text, True
                    strNewHeader = frmHiddenHelper.TabTmp.SelectDistinct("0", "", "", Me.TabPreview.GetFieldSeparator, False)
                    Me.TabPreview.HeaderString = strNewHeader
                    Me.TabPreview.RedrawTab
                End If
                Me.TabPossibleColumns.RedrawTab
            End If
        End If
    End If
End Sub

Private Sub ClearProperties()
    bmChangeInternal = True
    txtColumnPosition.Text = ""
    txtColumnName.Text = ""
    txtRawData.Text = ""
    txtDisplayName.Text = ""
    ckbClearZeros.Value = 0
    txtClearByValue(0).Text = ""
    txtClearByValue(1).Text = ""
    txtNumberOfDigits(0).Text = ""
    txtNumberOfDigits(1).Text = ""
    optFormat(0).Value = True
    bmChangeInternal = False
End Sub

Private Sub AddLineToPossibleColTab(ByRef rColNo As Long)
    Dim strColName As String
    Dim strPos As String
    Dim strComposition As String
    Dim strPosition As String
    Dim strLineNo As String
    Dim strInsert As String
    Dim strNo As String
    Dim strLength As String

    strLineNo = frmHiddenHelper.TabItemColumn.GetLinesByColumnValue(0, CStr(rColNo), 0)
    If Len(strLineNo) > 0 And IsNumeric(strLineNo) = True Then
        strComposition = frmHiddenHelper.TabItemColumn.GetColumnValue(CLng(strLineNo), 2)
        If Len(strComposition) = 0 Then
            strComposition = CInt(Left(frmHiddenHelper.TabItemColumn.GetColumnValue(CLng(strLineNo), 1), 3))
        End If
    Else
        Exit Sub
    End If

    Dim ilCount As Integer
    Dim strTmp As String
    strLineNo = Me.TabPossibleColumns.GetLinesByMultipleColumnValue("5,10", CStr(rColNo) & Chr(30) & "R", 0)
    If Len(strLineNo) > 0 And IsNumeric(strLineNo) = True Then
        strNo = Me.TabPossibleColumns.GetColumnValue(CLng(strLineNo), 11)
        If IsNumeric(strNo) = True Then
            ilCount = CInt(strNo) + 1
            strNo = CStr(ilCount)
            Me.TabPossibleColumns.SetColumnValue CLng(strLineNo), 11, strNo
            Exit Sub
        End If
    Else
        strNo = "1"
    End If
    

    'looking for length-information
    strLineNo = frmHiddenHelper.TabItemColumn.GetLinesByColumnValue(0, CStr(rColNo), 0)
    If Len(strLineNo) > 0 And IsNumeric(strLineNo) = True Then
        strLength = frmHiddenHelper.TabItemColumn.GetColumnValue(CLng(strLineNo), 3)
    End If

    strPosition = CStr(rColNo)

    strColName = GetItem(Me.TabRawData.HeaderString, CInt(rColNo) + 1, strSepPrev)

    'XXX strInsert = strColName & ";;;;" & strComposition & ";" & strPosition & ";" & strColName & ";;;;R;"
    strInsert = CStr(rColNo) & Chr(30) & Chr(30) & Chr(30) & Chr(30) & strComposition & Chr(30) & _
                strPosition & Chr(30) & strColName & Chr(30) & Chr(30) & Chr(30) & strLength & _
                Chr(30) & "R" & Chr(30) & strNo & Chr(30)
    Me.TabPossibleColumns.InsertTextLine strInsert, False
    Me.TabPossibleColumns.SetLineColor TabPossibleColumns.GetLineCount - 1, vbBlack, 8438015
    Me.TabPossibleColumns.RedrawTab
End Sub

Private Sub RemoveLineFromPossibleColTab(ByRef rColNo As Long)
    Dim strLineNo As String
    strLineNo = Me.TabPossibleColumns.GetLinesByMultipleColumnValue("5,10", CStr(rColNo) & Chr(30) & "R", 0)
    If Len(strLineNo) > 0 And IsNumeric(strLineNo) = True Then
        Dim ilCount As Integer
        Dim strNo As String
        strNo = Me.TabPossibleColumns.GetColumnValue(CLng(strLineNo), 11)
        If IsNumeric(strNo) = True Then
            ilCount = CInt(strNo) - 1
            If ilCount = 0 Then
                Me.TabPossibleColumns.DeleteLine CLng(strLineNo)
                Me.TabPossibleColumns.RedrawTab
                If imLastClickedLine = CInt(strLineNo) Then
                    imLastClickedLine = -1
                    imLastClickedColumn = -1
                    ClearProperties
                End If
            Else
                strNo = CStr(ilCount)
                Me.TabPossibleColumns.SetColumnValue CLng(strLineNo), 11, strNo
            End If
        End If
    End If
End Sub

Private Sub UpdateProperties()
    Dim strTmp As String
    Dim ilTmp As Integer

    ClearProperties
    bmChangeInternal = True

    txtColumnPosition.Text = Me.TabPossibleColumns.GetColumnValue(CLng(imLastClickedLine), 5)
    txtColumnName.Text = Me.TabPossibleColumns.GetColumnValue(CLng(imLastClickedLine), 0)
    txtRawData.Text = Me.TabPossibleColumns.GetColumnValue(CLng(imLastClickedLine), 10)
    txtDisplayName.Text = Me.TabPossibleColumns.GetColumnValue(CLng(imLastClickedLine), 6)
    txtDescription.Text = Me.TabPossibleColumns.GetColumnValue(CLng(imLastClickedLine), 1)
    strTmp = Me.TabPossibleColumns.GetColumnValue(CLng(imLastClickedLine), 7)
    If strTmp = "1" Then
        ckbClearZeros.Value = 1
    End If
    strTmp = Me.TabPossibleColumns.GetColumnValue(CLng(imLastClickedLine), 8)
    If Len(strTmp) > 0 Then
        txtClearByValue(0).Text = GetItem(strTmp, 1, ",")
        ilTmp = InStr(1, strTmp, ",")
        If ilTmp > 0 Then
            txtClearByValue(1).Text = Mid(strTmp, ilTmp + 1, Len(strTmp) - ilTmp)
        End If
    End If
    strTmp = Me.TabPossibleColumns.GetColumnValue(CLng(imLastClickedLine), 9)
    If Left(strTmp, 1) = "N" Then
        optFormat(1).Value = True
        txtNumberOfDigits(1).Text = Mid(strTmp, 2, Len(strTmp) - 1)
    Else
        optFormat(0).Value = True
        txtNumberOfDigits(0).Text = strTmp
    End If
    bmChangeInternal = False
End Sub
