VERSION 5.00
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomctl.ocx"
Begin VB.Form frmMain 
   Caption         =   "Import File Configuration Tool"
   ClientHeight    =   5130
   ClientLeft      =   165
   ClientTop       =   735
   ClientWidth     =   7110
   Icon            =   "frmMain.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   5130
   ScaleWidth      =   7110
   StartUpPosition =   3  'Windows Default
   Begin VB.TextBox txtConfigFile 
      Height          =   1545
      Left            =   45
      Locked          =   -1  'True
      MultiLine       =   -1  'True
      ScrollBars      =   3  'Both
      TabIndex        =   2
      Top             =   540
      Width           =   1725
   End
   Begin MSComctlLib.Toolbar tbToolBar 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   1
      Top             =   0
      Width           =   7110
      _ExtentX        =   12541
      _ExtentY        =   741
      ButtonWidth     =   609
      ButtonHeight    =   582
      Appearance      =   1
      ImageList       =   "imlToolbarIcons"
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   5
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "Open"
            Object.ToolTipText     =   "Open a data-file and start the 'Import Assistant'"
            ImageKey        =   "Open"
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "Assistant"
            Object.ToolTipText     =   "Open config-file and start the 'Import Assistant'"
            ImageKey        =   "Assistant"
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "Save"
            Object.ToolTipText     =   "Save generated configuration"
            ImageKey        =   "Save"
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.StatusBar sbStatusBar 
      Align           =   2  'Align Bottom
      Height          =   270
      Left            =   0
      TabIndex        =   0
      Top             =   4860
      Width           =   7110
      _ExtentX        =   12541
      _ExtentY        =   476
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   3
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   6906
            Text            =   "Status"
            TextSave        =   "Status"
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   6
            AutoSize        =   2
            TextSave        =   "8/27/02"
         EndProperty
         BeginProperty Panel3 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   5
            AutoSize        =   2
            TextSave        =   "9:52 AM"
         EndProperty
      EndProperty
   End
   Begin MSComDlg.CommonDialog dlgCommonDialog 
      Left            =   3285
      Top             =   855
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin MSComctlLib.ImageList imlToolbarIcons 
      Left            =   4095
      Top             =   810
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   8
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":030A
            Key             =   "New"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":041C
            Key             =   "Open"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":052E
            Key             =   "Save"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":0640
            Key             =   "Print"
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":0752
            Key             =   "Cut"
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":0864
            Key             =   "Copy"
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":0976
            Key             =   "Paste"
         EndProperty
         BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":0A88
            Key             =   "Assistant"
         EndProperty
      EndProperty
   End
   Begin VB.Menu mnuFile 
      Caption         =   "&File"
      Begin VB.Menu mnuFileOpen 
         Caption         =   "&Open..."
      End
      Begin VB.Menu mnuFileBar0 
         Caption         =   "-"
      End
      Begin VB.Menu mnuAssistant 
         Caption         =   "&Assistant"
      End
      Begin VB.Menu mnuFileBar1 
         Caption         =   "-"
      End
      Begin VB.Menu mnuFileSave 
         Caption         =   "&Save"
      End
      Begin VB.Menu mnuFileSaveAs 
         Caption         =   "Save &As..."
      End
      Begin VB.Menu mnuFileBar2 
         Caption         =   "-"
      End
      Begin VB.Menu mnuFileExit 
         Caption         =   "E&xit"
      End
   End
   Begin VB.Menu mnuView 
      Caption         =   "&View"
      Begin VB.Menu mnuViewToolbar 
         Caption         =   "&Toolbar"
         Checked         =   -1  'True
      End
      Begin VB.Menu mnuViewStatusBar 
         Caption         =   "Status &Bar"
         Checked         =   -1  'True
      End
      Begin VB.Menu mnuViewBar0 
         Caption         =   "-"
      End
      Begin VB.Menu mnuViewRefresh 
         Caption         =   "&Refresh"
         Enabled         =   0   'False
      End
      Begin VB.Menu mnuViewOptions 
         Caption         =   "&Options..."
         Enabled         =   0   'False
      End
      Begin VB.Menu mnuViewWebBrowser 
         Caption         =   "&Web Browser"
         Enabled         =   0   'False
      End
   End
   Begin VB.Menu mnuHelp 
      Caption         =   "&Help"
      Begin VB.Menu mnuHelpContents 
         Caption         =   "&Contents"
         Enabled         =   0   'False
      End
      Begin VB.Menu mnuHelpSearchForHelpOn 
         Caption         =   "&Search For Help On..."
         Enabled         =   0   'False
      End
      Begin VB.Menu mnuHelpBar0 
         Caption         =   "-"
      End
      Begin VB.Menu mnuHelpAbout 
         Caption         =   "&About "
      End
   End
End
Attribute VB_Name = "frmMain"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private Declare Function OSWinHelp% Lib "user32" Alias "WinHelpA" (ByVal hwnd&, ByVal HelpFile$, ByVal wCommand%, dwData As Any)

'Private strFileName As String
Private bmOpenedExisting As Boolean

Private Sub Form_Load()
    Me.WindowState = GetSetting(App.Title, "Settings", "WindowState", 0)
    If Me.WindowState = vbNormal Then
        Me.Left = GetSetting(App.Title, "Settings", "MainLeft", 1000)
        Me.Top = GetSetting(App.Title, "Settings", "MainTop", 1000)
        Me.Width = GetSetting(App.Title, "Settings", "MainWidth", 6500)
        Me.Height = GetSetting(App.Title, "Settings", "MainHeight", 6500)
    End If

    Me.txtConfigFile.Width = Me.Width - 250
    Me.txtConfigFile.Height = Me.Height - 1750
    Me.sbStatusBar.Panels(1).Text = "Ready."
End Sub

Private Sub Form_Resize()
    If Me.Width - 250 > -1 Then
        Me.txtConfigFile.Width = Me.Width - 250
    Else
        Me.txtConfigFile.Width = 0
    End If
    If Me.Height - 1750 > 0 Then
        Me.txtConfigFile.Height = Me.Height - 1750
    Else
        Me.txtConfigFile.Height = 0
    End If
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Dim i As Integer

    'close all sub forms
    For i = Forms.Count - 1 To 1 Step -1
        Unload Forms(i)
    Next

    If Me.WindowState = vbNormal Then
        SaveSetting App.Title, "Settings", "MainLeft", Me.Left
        SaveSetting App.Title, "Settings", "MainTop", Me.Top
        SaveSetting App.Title, "Settings", "MainWidth", Me.Width
        SaveSetting App.Title, "Settings", "MainHeight", Me.Height
    End If
    SaveSetting App.Title, "Settings", "WindowState", Me.WindowState
End Sub

Private Sub tbToolBar_ButtonClick(ByVal Button As MSComCtlLib.Button)
    On Error Resume Next
    Me.Refresh
    Select Case Button.key
        Case "Open"
            mnuFileOpen_Click
        Case "Assistant"
            mnuAssistant_Click
        Case "Save"
            mnuFileSave_Click
    End Select
End Sub

Private Sub mnuHelpAbout_Click()
    MsgBox "Version " & App.Major & "." & App.Minor & "." & App.Revision
End Sub

Private Sub mnuHelpSearchForHelpOn_Click()
    Dim nRet As Integer


    'if there is no helpfile for this project display a message to the user
    'you can set the HelpFile for your application in the
    'Project Properties dialog
    If Len(App.HelpFile) = 0 Then
        MsgBox "Unable to display Help Contents. There is no Help associated with this project.", vbInformation, Me.Caption
    Else
        On Error Resume Next
        nRet = OSWinHelp(Me.hwnd, App.HelpFile, 261, 0)
        If Err Then
            MsgBox Err.Description
        End If
    End If

End Sub

Private Sub mnuHelpContents_Click()
    Dim nRet As Integer


    'if there is no helpfile for this project display a message to the user
    'you can set the HelpFile for your application in the
    'Project Properties dialog
    If Len(App.HelpFile) = 0 Then
        MsgBox "Unable to display Help Contents. There is no Help associated with this project.", vbInformation, Me.Caption
    Else
        On Error Resume Next
        nRet = OSWinHelp(Me.hwnd, App.HelpFile, 3, 0)
        If Err Then
            MsgBox Err.Description
        End If
    End If

End Sub

Private Sub mnuViewWebBrowser_Click()
    'ToDo: Add 'mnuViewWebBrowser_Click' code.
    MsgBox "Add 'mnuViewWebBrowser_Click' code."
End Sub

Private Sub mnuViewOptions_Click()
    'ToDo: Add 'mnuViewOptions_Click' code.
    MsgBox "Add 'mnuViewOptions_Click' code."
End Sub

Private Sub mnuViewRefresh_Click()
    'ToDo: Add 'mnuViewRefresh_Click' code.
    MsgBox "Add 'mnuViewRefresh_Click' code."
End Sub

Private Sub mnuViewStatusBar_Click()
    mnuViewStatusBar.Checked = Not mnuViewStatusBar.Checked
    sbStatusBar.Visible = mnuViewStatusBar.Checked
End Sub

Private Sub mnuViewToolbar_Click()
    mnuViewToolbar.Checked = Not mnuViewToolbar.Checked
    tbToolBar.Visible = mnuViewToolbar.Checked
End Sub

Private Sub mnuFileExit_Click()
    'unload the form
    Unload Me
End Sub

Private Sub mnuFileSaveAs_Click()
    Dim sFile As String

    sFile = GetSaveFileName
    If Len(sFile) > 0 Then
        SaveFile sFile
    End If
End Sub

Private Sub mnuFileSave_Click()
    Dim sFile As String

    sFile = frmHiddenHelper.txtFileName.Text
    If bmOpenedExisting = False Then
        sFile = GetSaveFileName
    End If

    If Len(sFile) > 0 Then
        SaveFile sFile
    End If
End Sub

Private Function GetSaveFileName() As String
    Dim blValidName As Boolean

    GetSaveFileName = ""
    blValidName = False

    While blValidName = False
        With dlgCommonDialog
            .CancelError = False
            .DialogTitle = "Save config-file as ..."
            .Flags = &H1 Or &H4
            .FileName = "c:\ufis\system\*.ini"
            .ShowSave

            If Len(.FileName) = 0 Then
                ' cancel or no selection
                Exit Function
            End If

            If Len(.FileName) > 4 Then
                If UCase(Right(.FileName, 4)) = ".INI" Then
                    ' he entered a valid name
                    GetSaveFileName = .FileName
                    blValidName = True
                End If
            End If

            If blValidName = False Then
                MsgBox "Please enter a valid filename (*.ini)!", vbInformation, "Invalid Filename"
            End If
        End With
    Wend
End Function

Private Sub SaveFile(ByRef rsFileName As String)
    On Error Resume Next
    Dim sFile As String

    sFile = rsFileName

    If ExistFile(sFile) = True Then
        Kill sFile
    End If

    frmHiddenHelper.txtFileName.Text = sFile
    frmHiddenHelper.SaveForm frmHiddenHelper, True
    frmHiddenHelper.SaveForm frmStep1, True
    frmHiddenHelper.SaveForm frmStep2, True
    frmHiddenHelper.SaveForm frmStep3, True
End Sub

Private Sub mnuFileOpen_Click()
    Dim sFile As String
    Dim strBuffer As String
    Dim strFileLine As String
    Dim i As Integer
    Dim llCnt As Long

    ' show the open-file-dialog
    With dlgCommonDialog
        .CancelError = False
        .DialogTitle = "Open data-file for creating the configuration"
        .Flags = &H1 Or &H4 'cdlOFNReadOnly or cdlOFNHideReadOnly
        .FileName = "c:\ufis\system\*.*"
        .Filter = "all files (*.*)|*.*"
        .ShowOpen
        If Len(.FileName) = 0 Or ExistFile(.FileName) = False Then
            Exit Sub
        End If
        sFile = .FileName
    End With

    bmOpenedExisting = False
    If UCase(Right(sFile, 4)) <> ".DBF" Then
        strBuffer = "Displaying the first lines of the data source file:" & vbCrLf & vbCrLf
        llCnt = 0
        i = FreeFile
        Open sFile For Input As #i
        Do While Not EOF(i) And llCnt < 100
            Line Input #i, strFileLine
            strBuffer = strBuffer & strFileLine & vbCrLf
            llCnt = llCnt + 1
        Loop
        txtConfigFile.Text = strBuffer
        Close #i
    Else
        txtConfigFile.Text = "No preview possible in this window for .dbf-files."
    End If

    Load frmHiddenHelper
    Me.Refresh
    frmHiddenHelper.Init
    frmHiddenHelper.txtFileName.Text = sFile
    frmHiddenHelper.bVisibleMode = True
    frmHiddenHelper.Start
End Sub

Private Sub mnuAssistant_Click()
    Dim sFile As String
    Dim i As Integer
    Dim strBuffer As String
    Dim strFileLine As String

    ' show the open-file-dialog
    With dlgCommonDialog
        .DialogTitle = "Open existing configuration file"
        .CancelError = False
        .Flags = &H1 Or &H4 'cdlOFNReadOnly or cdlOFNHideReadOnly
        .FileName = "c:\ufis\system\*.ini"
        .Filter = "Configuration-files (*.ini)|*.ini"
        .ShowOpen
        If Len(.FileName) = 0 Or ExistFile(.FileName) = False Then
            Exit Sub
        ElseIf UCase(Right(.FileName, 4)) <> ".INI" Then
            MsgBox "Please choose a valid config-file!", vbCritical, "Invalid File"
            Exit Sub
        End If
        sFile = .FileName
    End With

    bmOpenedExisting = True
    strBuffer = "Displaying the config-file:" & vbCrLf & vbCrLf
    i = FreeFile
    Open sFile For Input As #i
    Do While Not EOF(i)
        Line Input #i, strFileLine
        strBuffer = strBuffer & strFileLine & vbCrLf
    Loop
    txtConfigFile.Text = strBuffer
    Close #i

    Load frmHiddenHelper
    frmHiddenHelper.Init
    frmHiddenHelper.txtFileName.Text = sFile
    frmHiddenHelper.bVisibleMode = True
    frmHiddenHelper.Start
End Sub

Public Sub SetTextField(ByVal pStr As String)
    Me.txtConfigFile.Text = ""
    txtConfigFile.Text = pStr
End Sub
