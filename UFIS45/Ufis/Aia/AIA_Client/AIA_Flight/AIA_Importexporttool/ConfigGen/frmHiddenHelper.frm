VERSION 5.00
Object = "{64E8E382-05E2-11D2-9B1D-9B0630BC8F12}#1.0#0"; "TAB.ocx"
Begin VB.Form frmHiddenHelper 
   Caption         =   "Form1"
   ClientHeight    =   10635
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   9915
   LinkTopic       =   "Form1"
   ScaleHeight     =   10635
   ScaleWidth      =   9915
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton cmdStart 
      Caption         =   "Start"
      Height          =   285
      Left            =   315
      TabIndex        =   9
      Top             =   180
      Width           =   735
   End
   Begin VB.TextBox txtFileName 
      Height          =   285
      Left            =   1395
      TabIndex        =   8
      Text            =   "c:\ufis\test.ini"
      Top             =   180
      Width           =   1410
   End
   Begin TABLib.TAB TabItemColumn 
      Height          =   1140
      Left            =   90
      TabIndex        =   6
      Tag             =   "TAB_ITEMCOLUMN"
      Top             =   7155
      Width           =   3795
      _Version        =   65536
      _ExtentX        =   6694
      _ExtentY        =   2011
      _StockProps     =   0
   End
   Begin TABLib.TAB TabConfiguration 
      Height          =   1905
      Left            =   135
      TabIndex        =   4
      Tag             =   "TAB_CONFIGURATION"
      Top             =   4500
      Width           =   3390
      _Version        =   65536
      _ExtentX        =   5980
      _ExtentY        =   3360
      _StockProps     =   0
   End
   Begin TABLib.TAB TabTmp 
      Height          =   2175
      Left            =   5805
      TabIndex        =   2
      Top             =   1395
      Width           =   1005
      _Version        =   65536
      _ExtentX        =   1773
      _ExtentY        =   3836
      _StockProps     =   0
   End
   Begin TABLib.TAB TabOrigFile 
      Height          =   2985
      Left            =   135
      TabIndex        =   1
      Tag             =   "TAB_ORIGFILE"
      Top             =   1035
      Width           =   4830
      _Version        =   65536
      _ExtentX        =   8520
      _ExtentY        =   5265
      _StockProps     =   0
   End
   Begin VB.Label Label4 
      Caption         =   "ItemColumnTab:"
      Height          =   285
      Left            =   135
      TabIndex        =   7
      Top             =   6795
      Width           =   2220
   End
   Begin VB.Label Label3 
      Caption         =   "Configuration-Tab:"
      Height          =   285
      Left            =   135
      TabIndex        =   5
      Top             =   4140
      Width           =   1860
   End
   Begin VB.Label Label2 
      Caption         =   "Little Helper:"
      Height          =   240
      Left            =   5805
      TabIndex        =   3
      Top             =   945
      Width           =   1095
   End
   Begin VB.Label Label1 
      Caption         =   "Original File:"
      Height          =   285
      Left            =   180
      TabIndex        =   0
      Top             =   675
      Width           =   3300
   End
End
Attribute VB_Name = "frmHiddenHelper"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public bVisibleMode As Boolean

Public Sub Init()
    TabOrigFile.ResetContent
    TabOrigFile.SetFieldSeparator Chr(30)
    TabOrigFile.FontName = "Courier New"
    TabOrigFile.FontSize = 12
    TabOrigFile.SetTabFontBold True
    TabOrigFile.LeftTextOffset = 2
    TabOrigFile.HeaderLengthString = "1000"
    TabOrigFile.HeaderString = "Col"
    TabOrigFile.ShowHorzScroller True
    TabOrigFile.EnableInlineEdit True

    TabConfiguration.SetFieldSeparator ";"
    TabConfiguration.ResetContent
    TabConfiguration.HeaderLengthString = "60,120,150"
    TabConfiguration.HeaderString = "Item;ItemType;Values"
    TabConfiguration.EnableHeaderSizing True
    TabConfiguration.ShowHorzScroller True

    TabItemColumn.HeaderLengthString = "90,75,75,45"
    TabItemColumn.HeaderString = "Column,Item,Descr.,Len"
    TabItemColumn.EnableHeaderSizing True
    TabItemColumn.ShowHorzScroller True

End Sub

Public Sub SaveForm(frm As Form, pWithTab As Boolean)
    On Error Resume Next

    Dim ctl As Control
    Dim obj As Object

    Dim strTag As String
    Dim strCtlType As String
    Dim strFileName As String
    Dim strSectionName As String

    strFileName = Replace(txtFileName.Text, Right(txtFileName.Text, 4), ".ini")
    strSectionName = frm.Name

    For Each ctl In frm.Controls
        strTag = ctl.Tag
        If Len(strTag) > 0 Then

            strCtlType = TypeName(ctl)
            If strCtlType = "OptionButton" Or strCtlType = "CheckBox" Then
                SetIniEntry strSectionName, strTag, CStr(ctl.Value), strFileName
            ElseIf strCtlType = "TextBox" Then
                SetIniEntry strSectionName, strTag, ctl.Text, strFileName
            ElseIf strCtlType = "TAB" Then
                If pWithTab = True Then
                    SaveTab ctl, strSectionName, strFileName
                End If
            End If
        End If
    Next
End Sub

Public Sub ReadForm(frm As Form, pWithTab As Boolean)
    On Error Resume Next

    Dim ctl As Control
    Dim obj As Object

    Dim strTag As String
    Dim strCtlType As String
    Dim strFileName As String
    Dim strSectionName As String

    strFileName = txtFileName.Text
    strSectionName = frm.Name

    For Each ctl In frm.Controls
        strTag = ctl.Tag
        If Len(strTag) > 0 Then

            strCtlType = TypeName(ctl)
            If strCtlType = "OptionButton" Or strCtlType = "CheckBox" Then
                ctl.Value = GetIniEntry(strSectionName, strTag, ctl.Value, strFileName)
            ElseIf strCtlType = "TextBox" Or strCtlType = "ComboBox" Then
                ctl.Text = GetIniEntry(strSectionName, strTag, ctl.Text, strFileName)
            ElseIf strCtlType = "TAB" Then
                If pWithTab = True Then
                    ReadTab ctl, strSectionName, strFileName
                End If
            End If
        End If
    Next
End Sub

Private Function SaveTab(ByRef rTab As TABLib.Tab, ByVal pSectionName As String, ByVal pFileName As String) As String
    Dim strLineData As String
    Dim strTag As String
    Dim l As Long

    strTag = rTab.Tag

    SetIniEntry pSectionName, strTag & "_FIELDSEPARATOR", Asc(rTab.GetFieldSeparator), pFileName
    SetIniEntry pSectionName, strTag & "_HEADERSTRING", rTab.HeaderString, pFileName
    SetIniEntry pSectionName, strTag & "_HEADERLENGTHSTRING", rTab.HeaderLengthString, pFileName
    SetIniEntry pSectionName, strTag & "_MAINHEADER", rTab.MainHeader, pFileName
    SetIniEntry pSectionName, strTag & "_MAINHEADERONLY", rTab.MainHeaderOnly, pFileName
    SetIniEntry pSectionName, strTag & "_MAINHEADERRANGES", rTab.GetMainHeaderRanges, pFileName
    SetIniEntry pSectionName, strTag & "_MAINHEADERVALUES", rTab.GetMainHeaderValues, pFileName
'    SetIniEntry pSectionName, strTag & "_SHOWHORZSCROLLER", rTab.ShowHorzScroller, pFileName
'    SetIniEntry pSectionName, strTag & "_SHOWVERTSCROLLER", rTab.ShowVertScroller, pFileName
'    SetIniEntry pSectionName, strTag & "_ENABLEHEADERSIZING", rTab.EnableHeaderSizing, pFileName
    SetIniEntry pSectionName, strTag & "_FONTNAME", rTab.FontName, pFileName
    SetIniEntry pSectionName, strTag & "_FONTSIZE", rTab.FontSize, pFileName
'    SetIniEntry pSectionName, strTag & "_SETTABFONTBOLD", rTab.SetTabFontBold, pFileName
    SetIniEntry pSectionName, strTag & "_COLUMNALIGNMENTSTRING", rTab.ColumnAlignmentString, pFileName
    SetIniEntry pSectionName, strTag & "_EMPTYAREABACKCOLOR", rTab.EmptyAreaBackColor, pFileName
    SetIniEntry pSectionName, strTag & "_EMPTYAREARIGHTCOLOR", rTab.EmptyAreaRightColor, pFileName
    SetIniEntry pSectionName, strTag & "_SHOWROWSELECTION", rTab.ShowRowSelection, pFileName
    SetIniEntry pSectionName, strTag & "_AUTOSIZEBYHEADER", rTab.AutoSizeByHeader, pFileName

    For l = 0 To rTab.GetLineCount - 1
        SetIniEntry pSectionName, strTag & "_DATA" & CStr(l), rTab.GetLineValues(l), pFileName
    Next l

End Function

Private Function ReadTab(ByRef rTab As TABLib.Tab, ByVal pSectionName As String, ByVal pFileName As String) As String
    Dim strLineData As String
    Dim blFound As String
    Dim strTag As String
    Dim strTmp As String
    Dim l As Long
    rTab.ResetContent

    strTag = rTab.Tag

    rTab.SetFieldSeparator Chr(GetIniEntry(pSectionName, strTag & "_FIELDSEPARATOR", "", pFileName))
    rTab.HeaderString = GetIniEntry(pSectionName, strTag & "_HEADERSTRING", rTab.HeaderString, pFileName)
    rTab.HeaderLengthString = GetIniEntry(pSectionName, strTag & "_HEADERLENGTHSTRING", rTab.HeaderLengthString, pFileName)
    rTab.MainHeader = GetIniEntry(pSectionName, strTag & "_MAINHEADER", rTab.MainHeader, pFileName)
    rTab.MainHeaderOnly = GetIniEntry(pSectionName, strTag & "_MAINHEADERONLY", rTab.MainHeaderOnly, pFileName)
    rTab.SetMainHeaderValues GetIniEntry(pSectionName, strTag & "_MAINHEADERRANGES", rTab.GetMainHeaderRanges, pFileName), _
                             GetIniEntry(pSectionName, strTag & "_MAINHEADERVALUES", rTab.GetMainHeaderValues, pFileName), _
                             ""
'    rTab.ShowHorzScroller GetIniEntry(pSectionName, strTag & "_SHOWHORZSCROLLER", rTab.ShowHorzScroller, pFileName)
'    rTab.ShowVertScroller GetIniEntry(pSectionName, strTag & "_SHOWVERTSCROLLER", rTab.ShowVertScroller, pFileName)
'    rTab.EnableHeaderSizing GetIniEntry(pSectionName, strTag & "_ENABLEHEADERSIZING", rTab.EnableHeaderSizing, pFileName)
    rTab.FontName = GetIniEntry(pSectionName, strTag & "_FONTNAME", rTab.FontName, pFileName)
    rTab.FontSize = GetIniEntry(pSectionName, strTag & "_FONTSIZE", rTab.FontSize, pFileName)
'    rTab.SetTabFontBold = GetIniEntry(pSectionName, strTag & "_SETTABFONTBOLD", rTab.SetTabFontBold, pFileName)
    rTab.ColumnAlignmentString = GetIniEntry(pSectionName, strTag & "_COLUMNALIGNMENTSTRING", rTab.ColumnAlignmentString, pFileName)
    rTab.EmptyAreaBackColor = GetIniEntry(pSectionName, strTag & "_EMPTYAREABACKCOLOR", rTab.EmptyAreaBackColor, pFileName)
    rTab.EmptyAreaRightColor = GetIniEntry(pSectionName, strTag & "_EMPTYAREARIGHTCOLOR", rTab.EmptyAreaRightColor, pFileName)
    rTab.ShowRowSelection = GetIniEntry(pSectionName, strTag & "_SHOWROWSELECTION", rTab.ShowRowSelection, pFileName)
    rTab.AutoSizeByHeader = GetIniEntry(pSectionName, strTag & "_AUTOSIZEBYHEADER", rTab.AutoSizeByHeader, pFileName)

    blFound = True
    l = 0
    While blFound = True
        strLineData = GetIniEntry(pSectionName, strTag & "_DATA" & CStr(l), "", pFileName)
        If Len(strLineData) > 0 Then
            rTab.InsertTextLine strLineData, False
        Else
            blFound = False
        End If
        l = l + 1
    Wend

End Function

Public Function GetIniEntry(ByVal pSectionName As String, ByVal pKeyName As String, ByVal pDefaultValue As String, ByVal pFileName As String) As String
    Dim ret As Long
    Dim strRet As String
    strRet = Space(500)

    ret = GetPrivateProfileString(pSectionName, pKeyName, pDefaultValue, strRet, Len(strRet), pFileName)

    If ret > 0 Then
        strRet = Left(strRet, ret)
    Else
        strRet = pDefaultValue
    End If
    GetIniEntry = strRet
End Function

Public Function SetIniEntry(ByVal pSectionName As String, ByVal pKeyName As String, ByVal pValue As String, ByVal pFileName As String) As String
    WritePrivateProfileString pSectionName, pKeyName, pValue, pFileName
End Function

Public Sub Start()
    Dim strFileLine As String
    Dim sFile As String
    Dim llCnt As Long
    Dim i As Integer

    sFile = txtFileName.Text
    If ExistFile(sFile) = True Then
        Me.TabConfiguration.ResetContent
        Me.TabItemColumn.ResetContent
        Me.TabOrigFile.ResetContent
        If Len(sFile) > 4 Then
            If UCase(Right(sFile, 4)) = ".INI" Then
                ReadForm Me, True
                Load frmStep1
                ReadForm frmStep1, True
                Load frmStep2
                ReadForm frmStep2, True
                Load frmStep3
                ReadForm frmStep3, True
                frmStep1.StartByConfig
            ElseIf UCase(Right(sFile, 4)) = ".DBF" Then
                Load DbfReader
                DbfReader.Show
                DbfReader.Refresh
                DbfReader.OpenDbfFile sFile, Me, False
                llCnt = 0
                While llCnt < 100 And llCnt < DbfReader.DataTab.GetLineCount - 1
                    strFileLine = DbfReader.DataTab.GetLineValues(llCnt)
                    Me.TabOrigFile.InsertTextLine strFileLine, False
                    llCnt = llCnt + 1
                Wend
                Unload DbfReader
                Me.TabOrigFile.RedrawTab
                frmStep1.StartAsNew
            Else
                llCnt = 0
                i = FreeFile
                Open sFile For Input As #i
                Do While Not EOF(i) And llCnt < 100
                    Line Input #i, strFileLine
                    llCnt = llCnt + 1
                    Me.TabOrigFile.InsertTextLine strFileLine, False
                Loop
                Close #i
                Me.TabOrigFile.RedrawTab
                frmStep1.StartAsNew
            End If
        End If
    Else
        MsgBox "File does not exist! Please enter a valid file path + name!", vbCritical, "No File found"
    End If
    'Me.Show
End Sub

Private Sub cmdStart_Click()
    Start
End Sub

Private Sub Form_Load()
    bVisibleMode = False
End Sub
