VERSION 5.00
Object = "{64E8E382-05E2-11D2-9B1D-9B0630BC8F12}#1.0#0"; "TAB.ocx"
Begin VB.Form frmStep3 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Import-Assistant - Step 3/3"
   ClientHeight    =   9300
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   8265
   Icon            =   "frmStep3.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   9300
   ScaleWidth      =   8265
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin VB.Frame Frame4 
      Caption         =   "General file open filter"
      Height          =   1905
      Left            =   180
      TabIndex        =   23
      Top             =   3420
      Width           =   7845
      Begin TABLib.TAB FILE_FILTER 
         Height          =   1140
         Left            =   225
         TabIndex        =   24
         TabStop         =   0   'False
         Tag             =   "FILE_FILTER"
         Top             =   450
         Width           =   7395
         _Version        =   65536
         _ExtentX        =   13044
         _ExtentY        =   2011
         _StockProps     =   0
      End
   End
   Begin VB.Frame Frame2 
      Caption         =   "General system settings"
      Height          =   1545
      Left            =   180
      TabIndex        =   19
      Top             =   5535
      Width           =   7845
      Begin VB.ComboBox DEFAULT_FTYP 
         BackColor       =   &H0000FFFF&
         Height          =   315
         ItemData        =   "frmStep3.frx":000C
         Left            =   2610
         List            =   "frmStep3.frx":0016
         TabIndex        =   7
         Tag             =   "DEFAULT_FTYP"
         Text            =   "Operational"
         Top             =   1080
         Width           =   1680
      End
      Begin VB.ComboBox DATE_FORMAT 
         BackColor       =   &H0000FFFF&
         Height          =   315
         ItemData        =   "frmStep3.frx":0032
         Left            =   2610
         List            =   "frmStep3.frx":0045
         TabIndex        =   6
         Tag             =   "DATE_FORMAT"
         Text            =   "CEDA"
         Top             =   720
         Width           =   1680
      End
      Begin VB.ComboBox SYSTEM_TYPE 
         BackColor       =   &H0000FFFF&
         Height          =   315
         ItemData        =   "frmStep3.frx":0075
         Left            =   2610
         List            =   "frmStep3.frx":0085
         TabIndex        =   5
         Tag             =   "SYSTEM_TYPE"
         Top             =   360
         Width           =   1680
      End
      Begin VB.Label Label7 
         Caption         =   "Choose the default FTYP"
         Height          =   195
         Left            =   180
         TabIndex        =   22
         Top             =   1140
         Width           =   2265
      End
      Begin VB.Label Label6 
         Caption         =   "Choose the output date-format:"
         Height          =   195
         Left            =   180
         TabIndex        =   21
         Top             =   780
         Width           =   2265
      End
      Begin VB.Label Label5 
         Caption         =   "Choose the system-type:"
         Height          =   195
         Left            =   180
         TabIndex        =   20
         Top             =   420
         Width           =   2265
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "General file properties"
      Height          =   1230
      Left            =   180
      TabIndex        =   17
      Top             =   1980
      Width           =   7845
      Begin VB.TextBox FILE_NAME 
         BackColor       =   &H00C0C0C0&
         Enabled         =   0   'False
         Height          =   330
         Left            =   2655
         TabIndex        =   12
         Tag             =   "FILE_NAME"
         Top             =   675
         Width           =   4875
      End
      Begin VB.CheckBox AUTO_OPEN 
         Caption         =   "&Auto-open of the file:"
         Height          =   330
         Left            =   180
         TabIndex        =   4
         Tag             =   "AUTO_OPEN"
         Top             =   675
         Width           =   1995
      End
      Begin VB.TextBox FILE_PATH 
         BackColor       =   &H0000FFFF&
         Height          =   330
         Left            =   2655
         TabIndex        =   3
         Tag             =   "FILE_PATH"
         Text            =   "c:\Ufis\rel\ImportTool\Data"
         ToolTipText     =   "enter the path where to find the data-files"
         Top             =   315
         Width           =   4875
      End
      Begin VB.Label Label4 
         Caption         =   "File path:"
         Height          =   285
         Left            =   135
         TabIndex        =   18
         Top             =   345
         Width           =   2445
      End
   End
   Begin VB.Frame Frame3 
      Caption         =   "General properties"
      Height          =   1545
      Left            =   180
      TabIndex        =   13
      Top             =   225
      Width           =   7845
      Begin VB.TextBox TOOLTIP_TEXT 
         BackColor       =   &H0000FFFF&
         Height          =   330
         Left            =   2790
         TabIndex        =   2
         Tag             =   "TOOLTIP_TEXT"
         ToolTipText     =   "arbitrary, e.g. ""Olympic Airways FOS 'Next Day Schedule'"
         Top             =   990
         Width           =   4875
      End
      Begin VB.TextBox FORMAT_NAME 
         BackColor       =   &H0000FFFF&
         Height          =   330
         Left            =   2790
         TabIndex        =   0
         Tag             =   "FORMAT_NAME"
         ToolTipText     =   "arbitrary, e.g. ""OA FOS"""
         Top             =   270
         Width           =   4875
      End
      Begin VB.TextBox FORMAT_TITLE 
         BackColor       =   &H0000FFFF&
         Height          =   330
         Left            =   2790
         TabIndex        =   1
         Tag             =   "FORMAT_TITLE"
         ToolTipText     =   "arbitrary, e.g. ""Flight Data Import"""
         Top             =   630
         Width           =   4875
      End
      Begin VB.Label Label3 
         Caption         =   "Tooltip text (over the button):"
         Height          =   285
         Left            =   270
         TabIndex        =   16
         Top             =   1020
         Width           =   2445
      End
      Begin VB.Label Label2 
         Caption         =   "Format name (button caption):"
         Height          =   285
         Left            =   270
         TabIndex        =   15
         Top             =   300
         Width           =   2445
      End
      Begin VB.Label Label1 
         Caption         =   "Format title (window caption):"
         Height          =   285
         Left            =   270
         TabIndex        =   14
         Top             =   660
         Width           =   2445
      End
   End
   Begin VB.CommandButton cmdFinish 
      Caption         =   "F&inish"
      Height          =   330
      Left            =   6660
      TabIndex        =   8
      Top             =   8730
      Width           =   1275
   End
   Begin VB.CommandButton cmdNext 
      Caption         =   "&Next >"
      Enabled         =   0   'False
      Height          =   330
      Left            =   5130
      TabIndex        =   11
      Top             =   8730
      Width           =   1275
   End
   Begin VB.CommandButton cmdBack 
      Caption         =   "< &Back"
      Height          =   330
      Left            =   3780
      TabIndex        =   9
      Top             =   8730
      Width           =   1275
   End
   Begin VB.CommandButton cmdCancel 
      Caption         =   "&Cancel"
      Height          =   330
      Left            =   2295
      TabIndex        =   10
      Top             =   8730
      Width           =   1275
   End
   Begin VB.Menu mnuContext 
      Caption         =   "Properties"
      Visible         =   0   'False
      Begin VB.Menu mnuProperties 
         Caption         =   "Properties"
      End
      Begin VB.Menu mnuFileBar0 
         Caption         =   "-"
      End
      Begin VB.Menu mnuRemoveColumn 
         Caption         =   "Remove Column From Preview"
      End
   End
End
Attribute VB_Name = "frmStep3"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub AUTO_OPEN_Click()
    Dim i As Integer

    If AUTO_OPEN.Value = 1 Then
        Me.FILE_NAME.Enabled = True
        Me.FILE_NAME.BackColor = vbWhite
        For i = 0 To Me.FILE_FILTER.GetLineCount - 1 Step 1
            Me.FILE_FILTER.SetLineColor CLng(i), vbBlack, 12632256
        Next i
        Me.FILE_FILTER.SetCurrentSelection -1
        Me.FILE_FILTER.ShowRowSelection = False
        Me.FILE_FILTER.EnableInlineEdit False
        Me.FILE_FILTER.EnableHeaderSizing False
        Me.FILE_FILTER.RedrawTab
    Else
        FILE_NAME.Enabled = False
        FILE_NAME.BackColor = 12632256
        For i = 0 To Me.FILE_FILTER.GetLineCount - 1 Step 1
            Me.FILE_FILTER.SetLineColor CLng(i), vbBlack, vbWhite
        Next i
        Me.FILE_FILTER.EnableInlineEdit True
        Me.FILE_FILTER.EnableHeaderSizing True
        Me.FILE_FILTER.ShowRowSelection = True
        Me.FILE_FILTER.RedrawTab
    End If
End Sub

Private Sub cmdBack_Click()
    Me.Hide
    frmStep2.Show
End Sub

Private Sub cmdCancel_Click()
    Unload frmHiddenHelper
    Unload frmStep1
    Unload frmStep2
    Unload Me
End Sub

Private Sub cmdFinish_Click()
    Dim strOrigFileName As String
    Dim strTmpFileName As String
    Dim strFileLine As String
    Dim strBuffer As String
    Dim i As Integer

    Screen.MousePointer = vbHourglass
    mdlMain.fMainForm.sbStatusBar.Panels(1).Text = "Generating config-file..."

    ' generating file just for displaying in the frmMain
    strTmpFileName = "c:\tmp\" & Format(Now, "YYYYMMDDhhmmss") & ".ini"
    strOrigFileName = frmHiddenHelper.txtFileName.Text
    frmHiddenHelper.txtFileName.Text = strTmpFileName
    frmHiddenHelper.SaveForm frmHiddenHelper, True
    frmHiddenHelper.SaveForm frmStep1, True
    frmHiddenHelper.SaveForm frmStep2, True
    frmHiddenHelper.SaveForm frmStep3, True

    strBuffer = "Generated config-file:" & vbCrLf & vbCrLf
    i = FreeFile
    Open strTmpFileName For Input As #i
    Do While Not EOF(i)
        Line Input #i, strFileLine
        strBuffer = strBuffer & strFileLine & vbCrLf
    Loop
    Close #i
    Kill strTmpFileName
    'frmMain.txtConfigFile = strBuffer
    mdlMain.fMainForm.SetTextField strBuffer
    mdlMain.fMainForm.sbStatusBar.Panels(1).Text = "Ready."
    frmHiddenHelper.txtFileName.Text = strOrigFileName

    Screen.MousePointer = vbDefault
    Me.Hide
End Sub

Private Sub FILE_FILTER_InplaceEditCell(ByVal LineNo As Long, ByVal ColNo As Long, ByVal NewValue As String, ByVal OldValue As String)
    If Len(NewValue) > 0 Then
        If ColNo = 3 Then
            If UCase(NewValue) <> "W" And UCase(NewValue) <> "S" Then
                MsgBox "Please enter 'W' for 'winter season' or 'S' for 'summer season'!", vbInformation, "Wrong Season"
                FILE_FILTER.SetColumnValue LineNo, ColNo, OldValue
            Else
                FILE_FILTER.SetColumnValue LineNo, ColNo, UCase(NewValue)
            End If
        ElseIf ColNo = 4 Then
            If UCase(NewValue) <> "Y" And UCase(NewValue) <> "N" Then
                MsgBox "Please enter 'Y' for 'yes' or 'N' for 'no'!", vbInformation, "Wrong Multi File Open"
                FILE_FILTER.SetColumnValue LineNo, ColNo, OldValue
            Else
                FILE_FILTER.SetColumnValue LineNo, ColNo, UCase(NewValue)
            End If
        End If
        FILE_FILTER.RedrawTab
    End If
End Sub

Private Sub Form_Load()
    Dim i As Integer
    Me.FILE_FILTER.ResetContent
    Me.FILE_FILTER.HeaderLengthString = "30,200,128,60,60"
    Me.FILE_FILTER.HeaderString = "No.,Filter description,Filter,Season,Multi"
    Me.FILE_FILTER.ColumnAlignmentString = "L,L,L,C,C"
    Me.FILE_FILTER.EnableHeaderSizing True
    Me.FILE_FILTER.EnableInlineEdit True
    Me.FILE_FILTER.InplaceEditUpperCase = False
    Me.FILE_FILTER.NoFocusColumns = "0"
    Me.FILE_FILTER.EmptyAreaBackColor = 12632256
    Me.FILE_FILTER.EmptyAreaRightColor = 12632256
    Me.FILE_FILTER.InsertTextLine "1,all files (*.*),*.*,,Y", False
    For i = 2 To 20 Step 1
        Me.FILE_FILTER.InsertTextLine CStr(i) & ",,,,", False
    Next i
    Me.FILE_FILTER.RedrawTab
End Sub

Public Sub Start()
    If frmHiddenHelper.bVisibleMode = True Then
        Me.Show
    End If
End Sub
