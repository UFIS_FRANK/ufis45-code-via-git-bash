VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomctl.ocx"
Object = "{64E8E382-05E2-11D2-9B1D-9B0630BC8F12}#1.0#0"; "tab.ocx"
Begin VB.Form CfgTool 
   Caption         =   "Embedded Configuration Dialog"
   ClientHeight    =   2130
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   13245
   Icon            =   "CfgTool.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   2130
   ScaleWidth      =   13245
   Visible         =   0   'False
   Begin TABLib.TAB CfgTab 
      Height          =   2685
      Index           =   2
      Left            =   2640
      TabIndex        =   25
      Tag             =   "TAB_POSSIBLE_COLUMNS"
      Top             =   2640
      Visible         =   0   'False
      Width           =   5475
      _Version        =   65536
      _ExtentX        =   9657
      _ExtentY        =   4736
      _StockProps     =   0
   End
   Begin VB.CheckBox chkSave 
      Caption         =   "Save"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   1770
      Style           =   1  'Graphical
      TabIndex        =   24
      Top             =   30
      Width           =   855
   End
   Begin VB.CheckBox chkModify 
      Caption         =   "Modify"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   900
      Style           =   1  'Graphical
      TabIndex        =   23
      Top             =   30
      Width           =   855
   End
   Begin VB.Frame Frame2 
      Caption         =   "Configuration Section"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1365
      Left            =   7770
      TabIndex        =   16
      Top             =   450
      Width           =   5415
      Begin VB.HScrollBar MoveButton 
         Height          =   285
         Left            =   3390
         TabIndex        =   26
         Top             =   300
         Visible         =   0   'False
         Width           =   1905
      End
      Begin VB.TextBox ImpToolTip 
         BackColor       =   &H00E0E0E0&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1380
         Locked          =   -1  'True
         TabIndex        =   21
         Top             =   960
         Width           =   3915
      End
      Begin VB.TextBox WindowTitle 
         BackColor       =   &H00E0E0E0&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1380
         Locked          =   -1  'True
         TabIndex        =   19
         Top             =   630
         Width           =   3915
      End
      Begin VB.TextBox ButtonTitle 
         Alignment       =   2  'Center
         BackColor       =   &H00E0E0E0&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1380
         Locked          =   -1  'True
         TabIndex        =   17
         Top             =   300
         Width           =   945
      End
      Begin VB.Label Label7 
         Caption         =   "Tooltip Text"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   225
         Left            =   150
         TabIndex        =   22
         Top             =   1020
         Width           =   1125
      End
      Begin VB.Label Label6 
         Caption         =   "Window Title"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   225
         Left            =   150
         TabIndex        =   20
         Top             =   690
         Width           =   1155
      End
      Begin VB.Label Label5 
         Caption         =   "Button Title"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   225
         Left            =   150
         TabIndex        =   18
         Top             =   360
         Width           =   1065
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Major Configuration Details"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1365
      Left            =   60
      TabIndex        =   5
      Top             =   450
      Width           =   7635
      Begin VB.TextBox ImpFilePath 
         BackColor       =   &H00E0E0E0&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1830
         Locked          =   -1  'True
         TabIndex        =   11
         Top             =   630
         Width           =   3825
      End
      Begin VB.TextBox ImpFileName 
         BackColor       =   &H00E0E0E0&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   5700
         Locked          =   -1  'True
         TabIndex        =   10
         Top             =   630
         Width           =   1815
      End
      Begin VB.CheckBox AutoOpen 
         Caption         =   "Fixed File Name"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   5700
         TabIndex        =   9
         Top             =   960
         Width           =   1755
      End
      Begin VB.TextBox CfgToolIniFile 
         BackColor       =   &H00E0E0E0&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1830
         TabIndex        =   8
         Top             =   300
         Width           =   5685
      End
      Begin VB.TextBox FileFormatType 
         Alignment       =   2  'Center
         BackColor       =   &H00E0E0E0&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1830
         Locked          =   -1  'True
         TabIndex        =   7
         Top             =   960
         Width           =   945
      End
      Begin VB.TextBox Text1 
         Alignment       =   2  'Center
         BackColor       =   &H00E0E0E0&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   4860
         Locked          =   -1  'True
         TabIndex        =   6
         Top             =   960
         Width           =   555
      End
      Begin VB.Label Label1 
         Caption         =   "Configuration File"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   225
         Left            =   180
         TabIndex        =   15
         Top             =   360
         Width           =   1575
      End
      Begin VB.Label Label2 
         Caption         =   "Import Path/File"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   225
         Left            =   180
         TabIndex        =   14
         Top             =   690
         Width           =   1575
      End
      Begin VB.Label Label3 
         Caption         =   "Import File Type"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   225
         Left            =   180
         TabIndex        =   13
         Top             =   1020
         Width           =   1575
      End
      Begin VB.Label Label4 
         Caption         =   "Item Separator (ASC)"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   225
         Left            =   2940
         TabIndex        =   12
         Top             =   1020
         Width           =   1845
      End
   End
   Begin VB.CheckBox chkNew 
      Caption         =   "New"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   30
      Style           =   1  'Graphical
      TabIndex        =   4
      Top             =   30
      Width           =   855
   End
   Begin MSComctlLib.StatusBar StatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   3
      Top             =   1845
      Width           =   13245
      _ExtentX        =   23363
      _ExtentY        =   503
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   2
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   20294
         EndProperty
      EndProperty
   End
   Begin TABLib.TAB CfgTab 
      Height          =   4485
      Index           =   0
      Left            =   60
      TabIndex        =   1
      Tag             =   "TAB_POSSIBLE_COLUMNS"
      Top             =   1890
      Width           =   6525
      _Version        =   65536
      _ExtentX        =   11509
      _ExtentY        =   7911
      _StockProps     =   0
   End
   Begin VB.CheckBox chkClose 
      Caption         =   "Close"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   2640
      Style           =   1  'Graphical
      TabIndex        =   0
      Top             =   30
      Width           =   855
   End
   Begin TABLib.TAB CfgTab 
      Height          =   4485
      Index           =   1
      Left            =   6660
      TabIndex        =   2
      Tag             =   "TAB_POSSIBLE_COLUMNS"
      Top             =   1890
      Width           =   6525
      _Version        =   65536
      _ExtentX        =   11509
      _ExtentY        =   7911
      _StockProps     =   0
   End
End
Attribute VB_Name = "CfgTool"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub CfgTab_SendLButtonDblClick(Index As Integer, ByVal LineNo As Long, ByVal ColNo As Long)
    Select Case Index
        Case 0
            If LineNo < 0 Then
                'CfgTab(Index).Sort ColNo, True, True
                'CfgTab(Index).RedrawTab
            End If
        Case Else
    End Select
End Sub

Private Sub chkClose_Click()
    If chkClose.Value = 1 Then
        Me.Hide
        chkClose.Value = 0
    End If
End Sub

Private Sub chkNew_Click()
    Dim NewSection As String
    Dim EmptyLine As String
    Dim SectionIdx As Long
    Dim ButtonIdx As Integer
    If chkNew.Value = 1 Then
        chkNew.BackColor = LightGreen
        If MainDialog.CurLoadPath = "" Then MainDialog.CurLoadPath = "c:\ufis\system"
        UfisTools.CommonDialog.DialogTitle = "Open Configuration File"
        UfisTools.CommonDialog.InitDir = MainDialog.CurLoadPath
        UfisTools.CommonDialog.FileName = "*.ini"
        UfisTools.CommonDialog.Filter = "*.ini"
        UfisTools.CommonDialog.FilterIndex = 1
        UfisTools.CommonDialog.Flags = (cdlOFNHideReadOnly)
        UfisTools.CommonDialog.CancelError = True
        On Error GoTo ErrorHandle
        UfisTools.CommonDialog.ShowOpen
        If UfisTools.CommonDialog.FileName <> "" Then
            InitFreeConfig UfisTools.CommonDialog.FileName
            NewSection = Trim(ButtonTitle.Text)
            If NewSection <> "" Then
                NewSection = "SECTION_" & Replace(NewSection, " ", "_", 1, -1, vbBinaryCompare)
                SectionIdx = NewSectionIdx(NewSection)
                ButtonIdx = CInt(SectionIdx)
                If SectionIdx < 0 Then
                    SectionIdx = CfgTab(2).GetLineCount
                    EmptyLine = Chr(30) & Chr(30) & Chr(30) & Chr(30) & Chr(30) & Chr(30)
                    CfgTab(2).InsertTextLine EmptyLine, False
                    CfgTab(2).SetColumnValue SectionIdx, 0, ButtonTitle.Text
                    CfgTab(2).SetColumnValue SectionIdx, 1, NewSection
                    CfgTab(2).SetColumnValue SectionIdx, 2, ImpToolTip.Text
                    CfgTab(2).RedrawTab
                    ButtonTitle.Tag = NewSection
                    MainDialog.InitValidSections GetSectionList
                    ButtonIdx = CInt(SectionIdx)
                    MainDialog.chkImpTyp(ButtonIdx).BackColor = vbYellow
                    MainDialog.chkImpTyp(ButtonIdx).Caption = ButtonTitle.Text
                    MainDialog.chkImpTyp(ButtonIdx).Tag = NewSection
                    MainDialog.chkImpTyp(ButtonIdx).ToolTipText = ImpToolTip.Text
                    MoveButton.Tag = ButtonIdx
                    MoveButton.Max = ButtonIdx
                    MoveButton.Value = ButtonIdx
                    MoveButton.Visible = True
                    chkSave.Enabled = True
                Else
                    
                End If
            End If
        End If
    Else
        chkNew.BackColor = vbButtonFace
        chkSave.Enabled = False
    End If
    Exit Sub
ErrorHandle:
End Sub

Private Function NewSectionIdx(SectionName As String) As Long
    Dim LineList As String
    LineList = CfgTab(2).GetLinesByColumnValue(1, SectionName, 0)
    If LineList <> "" Then
        NewSectionIdx = Val(LineList)
    Else
        NewSectionIdx = -1
    End If
End Function
Private Function GetSectionList() As String
    Dim Result As String
    Dim CurLine As Long
    Dim MaxLine As Long
    MaxLine = CfgTab(2).GetLineCount - 1
    For CurLine = 0 To MaxLine
        Result = Result & "," & CfgTab(2).GetColumnValue(CurLine, 1)
    Next
    GetSectionList = Mid(Result, 2)
End Function

Private Sub chkSave_Click()
    Dim NewSection As String
    If chkSave.Value = 1 Then
        WritePrivateProfileString "MAIN", "IMPORT_TYPES", GetSectionList, myIniFullName
        NewSection = ButtonTitle.Tag
        WritePrivateProfileString NewSection, "SYSTEM_TYPE", "FREE", myIniFullName
        WritePrivateProfileString NewSection, "FORMAT_NAME", ButtonTitle.Text, myIniFullName
        WritePrivateProfileString NewSection, "FORMAT_TITLE", WindowTitle.Text, myIniFullName
        WritePrivateProfileString NewSection, "TOOLTIP_TEXT", ImpToolTip.Text, myIniFullName
        WritePrivateProfileString NewSection, "CONFIG_FILE", CfgToolIniFile.Text, myIniFullName
        WritePrivateProfileString NewSection, "FILE_PATH", ImpFilePath.Text, myIniFullName
        chkSave.Value = 0
        chkNew.Value = 0
        chkSave.Enabled = False
    End If
End Sub

Private Sub Form_Load()
    Dim i As Integer
    Dim LineNo As Long
    Dim EmptyLine As String
    Me.Top = MainDialog.Top + 50 * Screen.TwipsPerPixelY
    Me.Left = MainDialog.Left + 30 * Screen.TwipsPerPixelX
    For i = 0 To 1
        CfgTab(i).ResetContent
        CfgTab(i).EmptyAreaBackColor = vbWhite
        CfgTab(i).EmptyAreaRightColor = vbWhite
        CfgTab(i).HeaderString = "Import File: Configured Specifications"
        CfgTab(i).HeaderLengthString = "1000"
    Next
    CfgTab(2).ResetContent
    CfgTab(2).HeaderString = "Title,Section Name,Tooltip Text"
    CfgTab(2).HeaderLengthString = "100,100,100,100,100"
    CfgTab(2).SetFieldSeparator Chr(30)
    EmptyLine = Chr(30) & Chr(30) & Chr(30) & Chr(30) & Chr(30) & Chr(30)
    LineNo = 0
    For i = 0 To MainDialog.chkImpTyp.Count - 1
        CfgTab(2).InsertTextLine EmptyLine, False
        CfgTab(2).SetColumnValue LineNo, 0, MainDialog.chkImpTyp(i).Caption
        CfgTab(2).SetColumnValue LineNo, 1, MainDialog.chkImpTyp(i).Tag
        CfgTab(2).SetColumnValue LineNo, 2, MainDialog.chkImpTyp(i).ToolTipText
        LineNo = LineNo + 1
    Next
    CfgTab(2).AutoSizeByHeader = True
    CfgTab(2).AutoSizeColumns
    CfgTab(2).RedrawTab
    MoveButton.Visible = False
End Sub

Public Sub InitFreeConfig(CfgFileName As String)
    Dim CurLine As Long
    Dim MaxLine As Long
    Dim ColNo As Long
    Dim LineType As String
    Dim ColNbr As String
    Dim FinalCol As String
    Dim RelaCol As String
    Dim ItmNbr As Integer
    Dim FldItm As Long
    Dim CurFldName As String
    Dim ItmData As String
    Dim ColData As String
    Dim NewData As String
    Dim DummyLine As String
    CfgToolIniFile.Text = CfgFileName
    ImpFilePath.Text = GetIniFileEntry("frmStep3", "FILE_PATH", "c:\ufis", CfgFileName)
    ImpFileName.Text = GetIniFileEntry("frmStep3", "FILE_NAME", "", CfgFileName)
    AutoOpen.Value = Val(GetIniFileEntry("frmStep3", "AUTO_OPEN", "0", CfgFileName))
    ItmData = GetIniFileEntry("frmStep1", "FORMAT_TYPE_ITEM", "????", CfgFileName)
    If ItmData = "True" Then
        FileFormatType.Text = "ITEMS"
    ElseIf ItmData = "False" Then
        FileFormatType.Text = "FIXED"
    Else
        FileFormatType.Text = ItmData
    End If
    ImpToolTip.Text = GetIniFileEntry("frmStep3", "TOOLTIP_TEXT", "", CfgFileName)
    ButtonTitle.Text = GetIniFileEntry("frmStep3", "FORMAT_NAME", "", CfgFileName)
    WindowTitle.Text = GetIniFileEntry("frmStep3", "FORMAT_TITLE", "", CfgFileName)
    CfgTab(0).ResetContent
    ReadTab "TAB_POSSIBLE_COLUMNS", CfgTab(0), "frmStep2", True, CfgFileName
    CfgTab(0).ShowVertScroller True
    CfgTab(0).ShowHorzScroller True
    CfgTab(0).VScrollMaster = False
    CfgTab(0).FontName = "Courier New"
    CfgTab(0).SetTabFontBold True
    CfgTab(0).HeaderFontSize = 17
    CfgTab(0).FontSize = 17
    CfgTab(0).LineHeight = 17
    CfgTab(0).LeftTextOffset = 2
    CfgTab(0).GridLineColor = DarkGray
    CfgTab(0).ColumnAlignmentString = "L,L,L,L,L,R,L,L,L,L,L,L,L,L,L"
    CfgTab(0).ShowRowSelection = True
    CfgTab(0).AutoSizeColumns
    CfgTab(0).Sort "5", True, True
    CfgTab(0).Sort "10", True, True
    CfgTab(0).RedrawTab
    NewData = ""
    For ItmNbr = 1 To 255
        ColData = ColData & "," & Trim(Str(ItmNbr))
        NewData = NewData & ","
        ItmData = ItmData & ",40"
        DummyLine = DummyLine & ",-99"
    Next
    CfgTab(1).ResetContent
    CfgTab(1).HeaderString = "No,Key Code" & ColData
    CfgTab(1).HeaderLengthString = "250" & ItmData
    CfgTab(1).ShowVertScroller True
    CfgTab(1).ShowHorzScroller True
    CfgTab(1).VScrollMaster = False
    CfgTab(1).FontName = "Courier New"
    CfgTab(1).SetTabFontBold True
    CfgTab(1).HeaderFontSize = 17
    CfgTab(1).FontSize = 17
    CfgTab(1).LineHeight = 17
    CfgTab(1).LeftTextOffset = 2
    CfgTab(1).GridLineColor = DarkGray
    CfgTab(1).ColumnAlignmentString = "R,L,L,L,L,L,L,L,L,L,L,L,L,L,L"
    CfgTab(1).ShowRowSelection = True
    CfgTab(1).AutoSizeByHeader = True
    CfgTab(1).InsertTextLine "0,Initial" & NewData, False
    CfgTab(1).InsertTextLine "1,HEAD_LINE" & NewData, False
    CfgTab(1).InsertTextLine "2,FIELD_POS" & NewData, False
    CfgTab(1).InsertTextLine "3,FIELD_LEN" & NewData, False
    CfgTab(1).InsertTextLine "4,FINAL_HEAD" & NewData, False
    CfgTab(1).InsertTextLine "5,FINAL_POS" & NewData, False
    CfgTab(1).InsertTextLine "6,FINAL_LEN" & NewData, False
    CfgTab(1).InsertTextLine "7,FIELD_FRM" & NewData, False
    CfgTab(1).InsertTextLine "8,REPLACE_ALL" & DummyLine, False
    CfgTab(1).InsertTextLine "9,CHECK_BAS" & NewData, False
    CfgTab(1).InsertTextLine "10,DATE_FIELDS" & NewData, False
    CfgTab(1).InsertTextLine "11,TIME_FIELDS" & NewData, False
    CfgTab(1).InsertTextLine "12,TIME_PERIOD" & DummyLine, False
    CfgTab(1).InsertTextLine "13,TIME_CHAIN" & NewData, False
    CfgTab(1).InsertTextLine "14,CHECK_VIAS" & NewData, False
    CfgTab(1).InsertTextLine "15,ROTATIONS" & NewData, False
    CfgTab(1).InsertTextLine "16,CLEAR_VALUE" & NewData, False
    CfgTab(1).InsertTextLine "17,CLEAR_BYVAL" & NewData, False
    CfgTab(1).InsertTextLine "18,CLEAR_ZEROS" & NewData, False
    CfgTab(1).InsertTextLine "19,FILTER1,Airline:ALC:,,,:,1,3" & NewData, False
    CfgTab(1).InsertTextLine "20,FILTER2,Flights:FLNO:,,,,,:,1+,2,3+,4" & NewData, False
    CfgTab(1).InsertTextLine "21,FILTER3,Airports:APC:,,,,,:,10,11,15,16" & NewData, False
    CfgTab(1).InsertTextLine "22,FILTER4,Aircraft:ACT:,,8" & NewData, False
    CfgTab(1).InsertTextLine "23,N.N." & NewData, False
    CfgTab(1).InsertTextLine "24,N.N." & NewData, False
    CfgTab(1).InsertTextLine "25,EXTRACT_DAT,DL1,DL2,DL3" & DummyLine, False
    
    If FileFormatType.Text = "ITEMS" Then
        MaxLine = CfgTab(0).GetLineCount - 1
        For CurLine = 0 To MaxLine
            ColData = Trim(CfgTab(0).GetColumnValue(CurLine, 4))
            If ColData <> "" Then
                If InStr(ColData, "+") > 0 Then
                    NewData = ""
                    ItmNbr = 0
                    Do
                        ItmNbr = ItmNbr + 1
                        ItmData = GetItem(ColData, ItmNbr, "+")
                        If ItmData <> "" Then
                            NewData = "+" & Trim(Str(Val(ItmData) + 1))
                        End If
                    Loop While ItmData <> ""
                    NewData = Mid(NewData, 2)
                ElseIf InStr(ColData, "L") > 0 Then
                    FldItm = Val(GetItem(ColData, 1, "L")) + 1
                    NewData = Trim(Str(FldItm)) & "L" & GetItem(ColData, 2, "L")
                ElseIf InStr(ColData, "R") > 0 Then
                    FldItm = Val(GetItem(ColData, 1, "R")) + 1
                    NewData = Trim(Str(FldItm)) & "R" & GetItem(ColData, 2, "R")
                ElseIf InStr(ColData, "M") > 0 Then
                    FldItm = Val(GetItem(ColData, 1, "M")) + 1
                    NewData = Trim(Str(FldItm)) & "M" & GetItem(ColData, 2, "M")
                    NewData = Replace(NewData, "|", "/", 1, -1, vbBinaryCompare)
                Else
                    NewData = Trim(Str(Val(ColData) + 1))
                End If
                CfgTab(0).SetColumnValue CurLine, 4, NewData
            End If
        Next
    End If
    MaxLine = CfgTab(0).GetLineCount - 1
    For CurLine = 0 To MaxLine
        ColNbr = Trim(CfgTab(0).GetColumnValue(CurLine, 5))
        If ColNbr <> "" Then
            ColNo = Val(ColNbr) + 2
            ColNbr = Trim(Str(ColNo - 1))
            FinalCol = Trim(Str(ColNo + 2))
            RelaCol = Trim(Str(ColNo - 1))
            LineType = Trim(CfgTab(0).GetColumnValue(CurLine, 10))
            If LineType <> "R" Then
                CurFldName = Trim(CfgTab(0).GetColumnValue(CurLine, 0))
                CfgTab(1).SetColumnValue 4, ColNo, CfgTab(0).GetColumnValue(CurLine, 6)
                CfgTab(1).SetColumnValue 5, ColNo, CfgTab(0).GetColumnValue(CurLine, 4)
                CfgTab(1).SetColumnValue 6, ColNo, CfgTab(0).GetColumnValue(CurLine, 9)
                Select Case CurFldName
                    Case "OPFR"
                        CfgTab(1).SetColumnValue 10, ColNo, ColNbr
                        CfgTab(1).SetColumnValue 12, 2, ColNbr
                        CfgTab(1).SetColumnValue 13, 2, ColNbr
                        CfgTab(1).SetColumnValue 25, 9, "DL" & FinalCol
                    Case "OPTO"
                        CfgTab(1).SetColumnValue 10, ColNo, ColNbr
                        CfgTab(1).SetColumnValue 12, 3, ColNbr
                        CfgTab(1).SetColumnValue 25, 10, "DL" & FinalCol
                    Case "TIME", "TIMEA"
                        CfgTab(1).SetColumnValue 11, ColNo, ColNbr
                        CfgTab(1).SetColumnValue 12, 6, ColNbr
                        CfgTab(1).SetColumnValue 13, 3, ColNbr
                        CfgTab(1).SetColumnValue 25, 16, "DL" & FinalCol
                    Case "TIMED"
                        CfgTab(1).SetColumnValue 11, ColNo, ColNbr
                        CfgTab(1).SetColumnValue 12, 7, ColNbr
                        CfgTab(1).SetColumnValue 13, 4, ColNbr
                        CfgTab(1).SetColumnValue 25, 17, "DL" & FinalCol
                    Case "FRED"
                        CfgTab(1).SetColumnValue 12, 4, ColNbr
                        CfgTab(1).SetColumnValue 25, 11, "DL" & FinalCol
                    Case "FREW"
                        CfgTab(1).SetColumnValue 12, 5, ColNbr
                        CfgTab(1).SetColumnValue 25, 23, "DL" & FinalCol
                    Case "DAOF"
                        CfgTab(1).SetColumnValue 13, 5, ColNbr
                        CfgTab(1).SetColumnValue 25, 18, "DL" & FinalCol
                    Case "ACT3"
                        CfgTab(1).SetColumnValue 8, ColNo, "ACT3"
                        CfgTab(1).SetColumnValue 22, 3, RelaCol
                        CfgTab(1).SetColumnValue 25, 12, "DL" & FinalCol
                    Case "ALC3"
                        CfgTab(1).SetColumnValue 8, ColNo, "ALC3"
                        CfgTab(1).SetColumnValue 9, ColNo, CreateCheckBas("ALTTAB", "ALC2/2", "", ColNbr) & "|" & CreateCheckBas("ALTTAB", "ALC3/3", "", ColNbr)
                    Case "ALC3A"
                        CfgTab(1).SetColumnValue 8, ColNo, "FLCA"
                        CfgTab(1).SetColumnValue 9, ColNo, CreateCheckBas("ALTTAB", "ALC2/2", "", ColNbr) & "|" & CreateCheckBas("ALTTAB", "ALC3/3", "", ColNbr)
                        CfgTab(1).SetColumnValue 19, 3, RelaCol
                        CfgTab(1).SetColumnValue 20, 3, RelaCol & "+"
                        CfgTab(1).SetColumnValue 25, 5, "DL" & FinalCol
                    Case "ALC3D"
                        CfgTab(1).SetColumnValue 8, ColNo, "FLCD"
                        CfgTab(1).SetColumnValue 9, ColNo, CreateCheckBas("ALTTAB", "ALC2/2", "", ColNbr) & "|" & CreateCheckBas("ALTTAB", "ALC3/3", "", ColNbr)
                        CfgTab(1).SetColumnValue 19, 4, RelaCol
                        CfgTab(1).SetColumnValue 20, 5, RelaCol & "+"
                        CfgTab(1).SetColumnValue 25, 7, "DL" & FinalCol
                    Case "APC3"
                        CfgTab(1).SetColumnValue 8, ColNo, "APC3"
                        CfgTab(1).SetColumnValue 14, 2, ColNbr
                        CfgTab(1).SetColumnValue 9, ColNo, CreateCheckBas("APTTAB", "APC3", "'" & HomeAirport & "'", ColNbr)
                    Case "APC3A"
                        CfgTab(1).SetColumnValue 8, ColNo, "ORG3"
                        CfgTab(1).SetColumnValue 14, 2, ColNbr
                        CfgTab(1).SetColumnValue 9, ColNo, CreateCheckBas("APTTAB", "APC3", "'" & HomeAirport & "'", ColNbr)
                        CfgTab(1).SetColumnValue 21, 3, RelaCol
                        CfgTab(1).SetColumnValue 25, 14, "DL" & FinalCol
                    Case "VIA3A"
                        CfgTab(1).SetColumnValue 8, ColNo, "VIA3"
                        CfgTab(1).SetColumnValue 14, 3, ColNbr
                        CfgTab(1).SetColumnValue 14, 4, ColNbr
                        CfgTab(1).SetColumnValue 9, ColNo, CreateCheckBas("APTTAB", "APC3", "'" & HomeAirport & "'", ColNbr)
                        CfgTab(1).SetColumnValue 21, 4, RelaCol
                        CfgTab(1).SetColumnValue 25, 15, "DL" & FinalCol
                    Case "VIA3"
                        CfgTab(1).SetColumnValue 8, ColNo, "VIAS"
                        CfgTab(1).SetColumnValue 14, 3, ColNbr
                        CfgTab(1).SetColumnValue 14, 4, ColNbr
                        CfgTab(1).SetColumnValue 9, ColNo, CreateCheckBas("APTTAB", "APC3", "'" & HomeAirport & "'", ColNbr)
                    Case "APC3D"
                        CfgTab(1).SetColumnValue 8, ColNo, "DES3"
                        CfgTab(1).SetColumnValue 14, 5, ColNbr
                        CfgTab(1).SetColumnValue 9, ColNo, CreateCheckBas("APTTAB", "APC3", "'" & HomeAirport & "'", ColNbr)
                        CfgTab(1).SetColumnValue 21, 6, RelaCol
                        CfgTab(1).SetColumnValue 25, 20, "DL" & FinalCol
                    Case "VIA3D"
                        CfgTab(1).SetColumnValue 8, ColNo, "VID3"
                        CfgTab(1).SetColumnValue 14, 6, ColNbr
                        CfgTab(1).SetColumnValue 14, 7, ColNbr
                        CfgTab(1).SetColumnValue 9, ColNo, CreateCheckBas("APTTAB", "APC3", "'" & HomeAirport & "'", ColNbr)
                        CfgTab(1).SetColumnValue 21, 5, RelaCol
                        CfgTab(1).SetColumnValue 25, 19, "DL" & FinalCol
                    Case "FLTNA"
                        CfgTab(1).SetColumnValue 15, 2, ColNbr
                        CfgTab(1).SetColumnValue 20, 4, RelaCol
                        CfgTab(1).SetColumnValue 25, 6, "DL" & FinalCol
                    Case "FLTND"
                        CfgTab(1).SetColumnValue 15, 3, ColNbr
                        CfgTab(1).SetColumnValue 20, 6, RelaCol
                        CfgTab(1).SetColumnValue 25, 8, "DL" & FinalCol
                    Case "STYPA"
                        CfgTab(1).SetColumnValue 25, 21, "DL" & FinalCol
                    Case "STYPD"
                        CfgTab(1).SetColumnValue 25, 22, "DL" & FinalCol
                    Case Else
                End Select
            Else
                CfgTab(1).SetColumnValue 1, ColNo, CfgTab(0).GetColumnValue(CurLine, 6)
                CfgTab(1).SetColumnValue 2, ColNo, CfgTab(0).GetColumnValue(CurLine, 4)
                CfgTab(1).SetColumnValue 3, ColNo, CfgTab(0).GetColumnValue(CurLine, 9)
            End If
        End If
    Next
    CfgTab(1).AutoSizeColumns
    CfgTab(0).RedrawTab
End Sub
Private Function CreateCheckBas(TblName As String, FldName As String, ErrorValues As String, ColNbr As String) As String
    CreateCheckBas = FldName & "|" & ErrorValues & "|" & TblName & "|" & ColNbr
End Function

Private Sub Form_Resize()
    Dim NewHeight As Long
    NewHeight = Me.ScaleHeight - StatusBar1.Height - CfgTab(0).Top - 2 * Screen.TwipsPerPixelY
    If NewHeight > 150 Then
        CfgTab(0).Height = NewHeight
        CfgTab(1).Height = NewHeight
    End If
End Sub

Private Sub MoveButton_Change()
    Dim OldPos As Long
    Dim NewPos As Long
    Dim BtnIdx As Integer
    Dim CurData As String
    If MoveButton.Value <> MoveButton.Tag Then
        BtnIdx = MoveButton.Tag
        MainDialog.chkImpTyp(BtnIdx).BackColor = vbButtonFace
        NewPos = CLng(MoveButton.Value)
        OldPos = CLng(BtnIdx)
        CurData = CfgTab(2).GetLineValues(OldPos)
        CfgTab(2).DeleteLine OldPos
        CfgTab(2).InsertTextLineAt NewPos, CurData, False
        CfgTab(2).RedrawTab
        MoveButton.Tag = MoveButton.Value
        MainDialog.InitValidSections GetSectionList
        BtnIdx = MoveButton.Tag
        MainDialog.chkImpTyp(BtnIdx).BackColor = vbYellow
        MainDialog.chkImpTyp(BtnIdx).Caption = ButtonTitle.Text
        MainDialog.chkImpTyp(BtnIdx).Tag = ButtonTitle.Tag
        MainDialog.chkImpTyp(BtnIdx).ToolTipText = ImpToolTip.Text
    End If
End Sub
