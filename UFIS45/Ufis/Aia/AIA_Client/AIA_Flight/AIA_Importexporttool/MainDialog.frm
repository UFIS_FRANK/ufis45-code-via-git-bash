VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.OCX"
Object = "{64E8E382-05E2-11D2-9B1D-9B0630BC8F12}#1.0#0"; "TAB.ocx"
Begin VB.Form MainDialog 
   ClientHeight    =   5805
   ClientLeft      =   165
   ClientTop       =   555
   ClientWidth     =   14310
   BeginProperty Font 
      Name            =   "Courier New"
      Size            =   8.25
      Charset         =   161
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "MainDialog.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MouseIcon       =   "MainDialog.frx":030A
   OLEDropMode     =   1  'Manual
   ScaleHeight     =   5805
   ScaleWidth      =   14310
   StartUpPosition =   3  'Windows Default
   Begin VB.CheckBox chkAppl 
      Caption         =   "Help"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   9
      Left            =   13110
      Style           =   1  'Graphical
      TabIndex        =   56
      Tag             =   "HELP"
      Top             =   390
      Width           =   855
   End
   Begin VB.CheckBox chkBackup 
      Caption         =   "Backup"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   60
      Style           =   1  'Graphical
      TabIndex        =   53
      Top             =   60
      Width           =   855
   End
   Begin VB.CheckBox chkRestore 
      Caption         =   "Restore"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   930
      Style           =   1  'Graphical
      TabIndex        =   52
      Top             =   60
      Width           =   855
   End
   Begin VB.Timer Timer1 
      Enabled         =   0   'False
      Interval        =   10
      Left            =   8430
      Top             =   1560
   End
   Begin VB.CheckBox chkAppl 
      Caption         =   "?"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   8
      Left            =   7650
      Style           =   1  'Graphical
      TabIndex        =   48
      Tag             =   "ASK"
      ToolTipText     =   "Asks for Drop confirmation"
      Top             =   390
      Width           =   225
   End
   Begin VB.HScrollBar ImpScroll 
      Height          =   300
      Left            =   2055
      TabIndex        =   47
      Top             =   45
      Width           =   870
   End
   Begin VB.CheckBox chkAppl 
      Caption         =   "Config"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   7
      Left            =   11370
      Style           =   1  'Graphical
      TabIndex        =   46
      Tag             =   "CONFIG"
      Top             =   390
      Width           =   855
   End
   Begin VB.CheckBox chkEdit 
      Caption         =   "Edit"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   3960
      Style           =   1  'Graphical
      TabIndex        =   6
      Top             =   690
      Width           =   435
   End
   Begin VB.CheckBox chkDontClear 
      Caption         =   "=>"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   3240
      Style           =   1  'Graphical
      TabIndex        =   45
      ToolTipText     =   "Bypass the Clear Function"
      Top             =   390
      Width           =   285
   End
   Begin VB.CheckBox chkSpare 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   7890
      Style           =   1  'Graphical
      TabIndex        =   44
      ToolTipText     =   "A Spare Part Only (Don't touch)"
      Top             =   390
      Width           =   855
   End
   Begin VB.CheckBox chkInline 
      Caption         =   "Cell"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   3540
      Style           =   1  'Graphical
      TabIndex        =   43
      Top             =   690
      Width           =   435
   End
   Begin VB.TextBox ActSeason 
      Alignment       =   2  'Center
      BackColor       =   &H00C0C0C0&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   3
      Left            =   2670
      Locked          =   -1  'True
      TabIndex        =   42
      Top             =   690
      Width           =   855
   End
   Begin VB.CheckBox chkLineWork 
      Caption         =   "Batch"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   6
      Left            =   12240
      Style           =   1  'Graphical
      TabIndex        =   41
      Top             =   690
      Width           =   855
   End
   Begin VB.CheckBox chkLineWork 
      Caption         =   "Join"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   5
      Left            =   8760
      Style           =   1  'Graphical
      TabIndex        =   40
      Top             =   690
      Width           =   855
   End
   Begin VB.CheckBox chkLineWork 
      Caption         =   "Split"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   4
      Left            =   7890
      Style           =   1  'Graphical
      TabIndex        =   39
      Top             =   690
      Width           =   855
   End
   Begin VB.Frame Frame1 
      BorderStyle     =   0  'None
      Height          =   315
      Left            =   60
      TabIndex        =   35
      Top             =   690
      Width           =   2625
      Begin VB.TextBox ActSeason 
         Alignment       =   2  'Center
         BackColor       =   &H00C0C0C0&
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   2
         Left            =   1740
         Locked          =   -1  'True
         TabIndex        =   38
         Top             =   0
         Width           =   855
      End
      Begin VB.TextBox ActSeason 
         Alignment       =   2  'Center
         BackColor       =   &H00C0C0C0&
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   1
         Left            =   0
         Locked          =   -1  'True
         TabIndex        =   37
         Top             =   0
         Width           =   855
      End
      Begin VB.TextBox ActSeason 
         Alignment       =   2  'Center
         BackColor       =   &H00C0C0C0&
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   0
         Left            =   870
         Locked          =   -1  'True
         TabIndex        =   36
         Top             =   0
         Width           =   855
      End
   End
   Begin VB.CheckBox chkClear 
      Caption         =   "Clear"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   2670
      Style           =   1  'Graphical
      TabIndex        =   34
      ToolTipText     =   "Clear out all flights prior tomorrow"
      Top             =   390
      Width           =   585
   End
   Begin VB.Frame CedaBusyFlag 
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   161
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1275
      Left            =   6840
      TabIndex        =   29
      Top             =   1170
      Width           =   1215
      Begin VB.TextBox CedaStatus 
         Alignment       =   2  'Center
         BackColor       =   &H0000FFFF&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   285
         Index           =   1
         Left            =   30
         Locked          =   -1  'True
         TabIndex        =   33
         Text            =   "LOG ON"
         Top             =   300
         Visible         =   0   'False
         Width           =   1125
      End
      Begin VB.TextBox CedaStatus 
         Alignment       =   2  'Center
         BackColor       =   &H00004000&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   285
         Index           =   0
         Left            =   30
         Locked          =   -1  'True
         TabIndex        =   32
         Text            =   "LINE BUSY"
         Top             =   0
         Visible         =   0   'False
         Width           =   1125
      End
      Begin VB.TextBox CedaStatus 
         Alignment       =   2  'Center
         BackColor       =   &H0000FFFF&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   285
         Index           =   2
         Left            =   30
         Locked          =   -1  'True
         TabIndex        =   31
         Text            =   "LOG OFF"
         Top             =   600
         Visible         =   0   'False
         Width           =   1125
      End
      Begin VB.TextBox CedaStatus 
         Alignment       =   2  'Center
         BackColor       =   &H00C00000&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   285
         Index           =   3
         Left            =   30
         Locked          =   -1  'True
         TabIndex        =   30
         Text            =   "OFFLINE"
         ToolTipText     =   "Server:"
         Top             =   900
         Visible         =   0   'False
         Width           =   1125
      End
      Begin VB.Line Line10 
         BorderColor     =   &H00FFFFFF&
         X1              =   1140
         X2              =   1140
         Y1              =   0
         Y2              =   1590
      End
      Begin VB.Line Line9 
         BorderColor     =   &H00FFFFFF&
         X1              =   45
         X2              =   1155
         Y1              =   255
         Y2              =   255
      End
      Begin VB.Line Line8 
         BorderColor     =   &H00FFFFFF&
         X1              =   0
         X2              =   0
         Y1              =   0
         Y2              =   1590
      End
      Begin VB.Line Line7 
         BorderColor     =   &H00808080&
         X1              =   45
         X2              =   45
         Y1              =   0
         Y2              =   1590
      End
      Begin VB.Line Line6 
         BorderColor     =   &H00808080&
         X1              =   60
         X2              =   1155
         Y1              =   0
         Y2              =   0
      End
   End
   Begin VB.CheckBox chkExtract 
      Caption         =   "Extract"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   5280
      Style           =   1  'Graphical
      TabIndex        =   28
      ToolTipText     =   "Opens the 'import workbench' for data filtering"
      Top             =   390
      Width           =   855
   End
   Begin VB.CheckBox chkLineWork 
      Caption         =   "Ignore"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   3
      Left            =   7020
      Style           =   1  'Graphical
      TabIndex        =   27
      Top             =   690
      Width           =   855
   End
   Begin VB.CheckBox chkLineWork 
      Caption         =   "Delete"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   2
      Left            =   11370
      Style           =   1  'Graphical
      TabIndex        =   26
      Top             =   690
      Width           =   855
   End
   Begin VB.CheckBox chkLineWork 
      Caption         =   "Update"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   1
      Left            =   10500
      Style           =   1  'Graphical
      TabIndex        =   25
      Top             =   690
      Width           =   855
   End
   Begin VB.CheckBox chkLineWork 
      Caption         =   "Insert"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   0
      Left            =   9630
      Style           =   1  'Graphical
      TabIndex        =   24
      Top             =   690
      Width           =   855
   End
   Begin VB.CheckBox chkDetails 
      Caption         =   "Daily"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   5370
      Style           =   1  'Graphical
      TabIndex        =   23
      Top             =   1350
      Visible         =   0   'False
      Width           =   855
   End
   Begin MSComctlLib.StatusBar StatusBar 
      Align           =   2  'Align Bottom
      Height          =   300
      Left            =   0
      TabIndex        =   13
      Top             =   5505
      Width           =   14310
      _ExtentX        =   25241
      _ExtentY        =   529
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   7
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            Object.Width           =   1323
            MinWidth        =   1323
            Object.ToolTipText     =   "Total Lines"
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            Object.Width           =   1323
            MinWidth        =   1323
            Object.ToolTipText     =   "Valid Lines (Filtered)"
         EndProperty
         BeginProperty Panel3 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   1323
            MinWidth        =   1323
            Object.ToolTipText     =   "Total Flights"
         EndProperty
         BeginProperty Panel4 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   1323
            MinWidth        =   1323
            Object.ToolTipText     =   "Arrivals"
         EndProperty
         BeginProperty Panel5 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   1323
            MinWidth        =   1323
            Object.ToolTipText     =   "Departures"
         EndProperty
         BeginProperty Panel6 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   1323
            MinWidth        =   1323
            Object.ToolTipText     =   "Rotations"
         EndProperty
         BeginProperty Panel7 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   16624
            MinWidth        =   8819
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.CheckBox chkAppl 
      Caption         =   "Exit"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   6
      Left            =   13980
      Style           =   1  'Graphical
      TabIndex        =   22
      Tag             =   "EXIT"
      Top             =   390
      Width           =   855
   End
   Begin VB.CheckBox chkAppl 
      Caption         =   "About"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   5
      Left            =   12240
      Style           =   1  'Graphical
      TabIndex        =   21
      Tag             =   "ABOUT"
      Top             =   390
      Width           =   855
   End
   Begin VB.CheckBox chkShrink 
      Caption         =   "Shrink"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   1800
      Style           =   1  'Graphical
      TabIndex        =   20
      ToolTipText     =   "Merge overnight rotations into one single line"
      Top             =   390
      Width           =   855
   End
   Begin VB.CheckBox chkAppl 
      Caption         =   "Drag"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   3
      Left            =   7020
      Style           =   1  'Graphical
      TabIndex        =   18
      Tag             =   "DRAG"
      ToolTipText     =   "Enables manual Drag&Drop"
      Top             =   390
      Width           =   645
   End
   Begin VB.CheckBox chkAppl 
      Caption         =   "Setup"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   4
      Left            =   10500
      Style           =   1  'Graphical
      TabIndex        =   1
      Tag             =   "SETUP"
      Top             =   390
      Width           =   855
   End
   Begin VB.CheckBox chkRequest 
      Caption         =   "Select"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   60
      Style           =   1  'Graphical
      TabIndex        =   19
      Top             =   390
      Width           =   855
   End
   Begin VB.CheckBox chkAppl 
      Caption         =   "Save As"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   0
      Left            =   9630
      Style           =   1  'Graphical
      TabIndex        =   17
      Tag             =   "SAVEAS"
      Top             =   390
      Width           =   855
   End
   Begin VB.CheckBox chkAppl 
      Caption         =   "Print"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   2
      Left            =   8760
      Style           =   1  'Graphical
      TabIndex        =   16
      Top             =   390
      Width           =   855
   End
   Begin VB.CheckBox chkAppl 
      Caption         =   "Report"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   1
      Left            =   6150
      Style           =   1  'Graphical
      TabIndex        =   15
      Top             =   390
      Width           =   855
   End
   Begin VB.CheckBox chkAction 
      Caption         =   "Preview"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   5370
      Style           =   1  'Graphical
      TabIndex        =   14
      Top             =   1650
      Visible         =   0   'False
      Width           =   855
   End
   Begin VB.CheckBox chkAll 
      Caption         =   "All"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   6585
      Style           =   1  'Graphical
      TabIndex        =   9
      ToolTipText     =   "Global update (permanent for this file)"
      Top             =   690
      Width           =   420
   End
   Begin VB.CheckBox chkImport 
      Caption         =   "Import"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   5370
      Style           =   1  'Graphical
      TabIndex        =   12
      Top             =   1950
      Visible         =   0   'False
      Width           =   855
   End
   Begin VB.CheckBox chkIdentify 
      Caption         =   "Identify"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   4410
      Style           =   1  'Graphical
      TabIndex        =   11
      ToolTipText     =   "Determine single arrivals, departures or complete rotations"
      Top             =   390
      Width           =   855
   End
   Begin VB.CheckBox chkImpTyp 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   0
      Left            =   660
      Style           =   1  'Graphical
      TabIndex        =   10
      Top             =   3030
      Visible         =   0   'False
      Width           =   855
   End
   Begin VB.CheckBox chkReplace 
      Caption         =   "Upd"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   6150
      Style           =   1  'Graphical
      TabIndex        =   8
      ToolTipText     =   "Single update "
      Top             =   690
      Width           =   450
   End
   Begin VB.TextBox txtEdit 
      BackColor       =   &H8000000F&
      Enabled         =   0   'False
      Height          =   285
      Left            =   4410
      Locked          =   -1  'True
      TabIndex        =   7
      Top             =   690
      Width           =   1725
   End
   Begin VB.CheckBox chkCheck 
      Caption         =   "Check"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   3540
      Style           =   1  'Graphical
      TabIndex        =   5
      ToolTipText     =   "Full check and basic data validation (Try Right Mouse Button also)"
      Top             =   390
      Width           =   855
   End
   Begin VB.CheckBox chkExpand 
      Caption         =   "Open"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   930
      Style           =   1  'Graphical
      TabIndex        =   4
      ToolTipText     =   "Load file in pre-interpretation mode"
      Top             =   390
      Width           =   855
   End
   Begin VB.CheckBox chkSplit 
      Caption         =   "Split"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   4410
      Style           =   1  'Graphical
      TabIndex        =   3
      Top             =   1950
      Visible         =   0   'False
      Width           =   855
   End
   Begin VB.CheckBox chkFilter 
      Caption         =   "Filter"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   4410
      Style           =   1  'Graphical
      TabIndex        =   2
      Top             =   1650
      Visible         =   0   'False
      Width           =   855
   End
   Begin VB.CheckBox chkLoad 
      Caption         =   "Load"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   4410
      Style           =   1  'Graphical
      TabIndex        =   0
      Top             =   1350
      Visible         =   0   'False
      Width           =   855
   End
   Begin VB.Frame TabFrame 
      BorderStyle     =   0  'None
      Enabled         =   0   'False
      Height          =   1605
      Left            =   60
      TabIndex        =   50
      Top             =   1050
      Width           =   3885
      Begin TABLib.TAB FileData 
         Height          =   1485
         Left            =   0
         TabIndex        =   51
         Top             =   0
         Width           =   3525
         _Version        =   65536
         _ExtentX        =   6218
         _ExtentY        =   2619
         _StockProps     =   64
         Columns         =   10
         FontName        =   "Courier New"
      End
   End
   Begin VB.CheckBox chkSort 
      Caption         =   "Sort"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   1800
      Style           =   1  'Graphical
      TabIndex        =   49
      ToolTipText     =   "Single update "
      Top             =   390
      Visible         =   0   'False
      Width           =   855
   End
   Begin VB.Label lblSize 
      Alignment       =   2  'Center
      AutoSize        =   -1  'True
      Caption         =   "1024 / 768"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   210
      Left            =   13995
      TabIndex        =   57
      Top             =   120
      Width           =   795
   End
   Begin VB.Label ErrCnt 
      Alignment       =   2  'Center
      BackColor       =   &H00FFC0FF&
      BorderStyle     =   1  'Fixed Single
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   13110
      TabIndex        =   55
      ToolTipText     =   "Error Counter"
      Top             =   690
      Width           =   855
   End
   Begin VB.Label WarnCnt 
      Alignment       =   2  'Center
      BorderStyle     =   1  'Fixed Single
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   13980
      TabIndex        =   54
      ToolTipText     =   "Warning Counter"
      Top             =   690
      Width           =   855
   End
End
Attribute VB_Name = "MainDialog"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public PushedImpButton As Integer
Public DataSystemType As String
Public DefaultFtyp As String
Public ExtractSystemType As Integer
Public ActColLst As String
Public TotalReset As Boolean
Public ExtFldCnt As Long
Public UseStypField As String
Public UseVialFlag As String
Dim UseDefaultSeason As Boolean
Dim DefaultSeasonRec As String
Dim SeasonActYear As String
Dim SeasonNxtYear As String
Dim SeasonBegin As String
Dim SeasonEnd As String
Dim SeasonFirstMonth As Integer
Dim SeasonLastMonth As Integer
Dim ErrorColor As Long
Dim ShowMaxButtons As Integer
Dim MainPeriodCols As String
Dim MainVpfrCol As Long
Dim MainVptoCol As Long
Dim MainFredCol As Long
Dim MainFrewCol As Long
Dim MainStoaCol As Long
Dim MainStodCol As Long
Dim MainChkFred As String
Dim InputBuffer As String
Dim CurHeaderLength As String
Dim MainTitle As String
Dim FormatType As String
Dim ItemSep As String
Dim StopRecursion As Boolean
Dim GridPrepared As Boolean
Dim AllLinCnt As Integer
Dim DatLinCnt As Integer
Dim ArrFltCnt As Integer
Dim DepFltCnt As Integer
Dim myLoadPath As String
Dim myLoadName As String
Public CurLoadPath As String
Dim CurLoadName As String
Dim CurFileFilter As String
Dim CurMultiFiles As String
Dim CurFilterIndex As Integer
Dim NormalHeader As String
Dim ExpandHeader As String
Dim CurrentHeader As String
Dim FldPosLst As String
Dim ExpPosLst As String
Dim CurFldPosLst As String
Dim FldLenLst As String
Dim FldWidLst As String
Dim FldFrmLst As String
Dim FldAddLst As String
Dim LineFilterPos As Integer
Dim LineFilterLen As Integer
Dim LineFilterTyp As String
Dim LineFilterText As String
Dim LineWrap As Boolean
Dim IgnoreEmptyLine As Boolean
Dim SetColSelection As Boolean
Dim ColSelectList As String
Dim ChkBasDat As String
Dim AutoCheck As String
Dim LegColNbr As Long
Dim OrgColNbr As Long
Dim DesColNbr As Long
Dim DateFields As String
Dim TimeFields As String
Dim DateFieldFormat As String
Dim ClearZeros As String
Dim ClearValues As String
Dim ClearByValue As String
Dim TimeChainInfo As String
Dim CheckViaList As String
Dim IgnoreEmpty As String
Dim ShrinkList As String
Dim AftFldLst As String
Dim ExtFldLst As String
Dim OutputType As String
Dim ActionType As String
Dim CheckRotation As String
Dim ExtractDepartures As String
Dim ExtractArrivals As String
Dim ExtractRotationArr As String
Dim ExtractRotationDep As String
Dim ExtractSlotLine As String
Dim AutoReplaceAll As String
Dim DragLine As Long
Dim DropLine As Long
Dim MoveLine As Long
Dim DragForeColor As Long
Dim DragBackcolor As Long
Dim TestMode As Boolean
Dim ActiveTimeFields As String
Dim FlnoField As Integer
Dim WinZipPath As String
Dim WinZipId 'as variant

Private Sub ActSeason_DblClick(Index As Integer)
    If MySetUp.MonitorSetting(3).Value = 1 Then
        SeasonData.Show , Me
    Else
        SeasonData.Show
    End If
End Sub

Private Sub chkAction_Click()
    If Not RestoreAction Then
        If Not TotalReset Then
            If chkAction.Value = 1 Then
                OaFosPreview
            Else
                chkAction.BackColor = vbButtonFace
                chkImport.Enabled = False
            End If
        End If
    End If
End Sub

Public Sub OaFosPreview()
Dim CurLine As Long
Dim MaxLine As Long
Dim Alc3Row As Long
Dim FltnRow As Long
Dim FlnsRow As Long
Dim DateRow As Long
Dim AdidRow As Long
Dim tmpAlc3 As String
Dim tmpFltn As String
Dim tmpFlns As String
Dim tmpDate As String
Dim tmpAdid As String
Dim AftFkey As String
Dim AftFkeyList As String
Dim tmpSqlKey As String
Dim tmpResult As String
Dim i As Long
Dim llCount As Long
Dim RetCode As Integer
Dim PacketCount As Integer
Dim ChkMainLine As String
Dim ChkExtrLine As String
Dim ExtrMaxLine As Long
Dim ListOfOaFosLines As String
    ListOfOaFosLines = ""
    'hardcode
    Alc3Row = 3
    FltnRow = 4
    FlnsRow = -1
    DateRow = 18
    AdidRow = 1
    ClickButtonChain 9
    Me.MousePointer = 11
    AftFkeyList = ""
    MaxLine = FileData.GetLineCount - 1
    ExtrMaxLine = FlightExtract.ExtractData.GetLineCount - 1
    PacketCount = 0
    For CurLine = 0 To MaxLine
        tmpAlc3 = "OAL"
        tmpFltn = FileData.GetColumnValue(CurLine, FltnRow)
        tmpFlns = " "
        tmpAdid = Trim(FileData.GetColumnValue(CurLine, AdidRow))
        If tmpAdid = "A" Then
            tmpDate = FileData.GetColumnValue(CurLine, 18)
        ElseIf tmpAdid = "D" Then
            tmpDate = FileData.GetColumnValue(CurLine, 16)
        ElseIf tmpAdid = "B" Then
            tmpDate = FileData.GetColumnValue(CurLine, 16)
        Else
            tmpDate = ""
        End If
        If tmpDate <> "" Then
            ChkMainLine = FileData.GetColumnValue(CurLine, 0)
            ChkExtrLine = FlightExtract.ExtractData.GetLinesByColumnValue(0, ChkMainLine, 0)
            'MsgBox ChkMainLine & " / " & ChkExtrLine
            If Val(ChkExtrLine) > ExtrMaxLine Then ChkExtrLine = ""
            If ChkExtrLine = "DONE" Then ChkExtrLine = ""
            If ChkExtrLine = "OK" Then ChkExtrLine = ""
            If ChkExtrLine <> "" Then
                AftFkey = BuildAftFkey(tmpAlc3, tmpFltn, tmpFlns, tmpDate, tmpAdid)
                FileData.SetColumnValue CurLine, ExtFldCnt, AftFkey
                ListOfOaFosLines = ListOfOaFosLines & Str(CurLine) & ","
                AftFkey = "'" & AftFkey & "',"
                AftFkeyList = AftFkeyList & AftFkey
                PacketCount = PacketCount + 1
                If PacketCount >= 30 Then
                    AftFkeyList = Left(AftFkeyList, Len(AftFkeyList) - 1)
                    CheckExistingFlights AftFkeyList
                    AftFkeyList = ""
                    PacketCount = 0
                End If
            End If
        End If
    Next
    If AftFkeyList <> "" Then
        AftFkeyList = Left(AftFkeyList, Len(AftFkeyList) - 1)
        CheckExistingFlights AftFkeyList
    End If
    CheckInsertFlights
    chkImport.Enabled = True
    PrecheckCedaAction 1, ErrorColor
    PrecheckCedaAction 2, ErrorColor
    Me.MousePointer = 0
    'MsgBox ListOfOaFosLines
End Sub
Private Sub CheckExistingFlights(AftFkeyList As String)
Dim tmpSqlKey As String
Dim tmpResult As String
Dim i As Long
Dim llCount As Long
Dim RetCode As Integer
Dim tmpUrno As String
Dim tmpFkey As String
Dim tmpColLst As String
Dim tmpLine As Long
Dim tmpData As String
    If AftFkeyList <> "" Then
        tmpSqlKey = "WHERE FKEY IN (" & AftFkeyList & ")"
        RetCode = UfisServer.CallCeda(tmpResult, "RTA", "AFTTAB", "URNO,FKEY", " ", tmpSqlKey, "", 1, False, False)
        llCount = UfisServer.DataBuffer(1).GetLineCount - 1
        For i = 0 To llCount
            tmpUrno = UfisServer.DataBuffer(1).GetColumnValue(i, 0)
            tmpFkey = UfisServer.DataBuffer(1).GetColumnValue(i, 1)
            tmpColLst = FileData.GetLinesByColumnValue(ExtFldCnt, tmpFkey, 1)
            If tmpColLst <> "" Then
                tmpLine = Val(GetItem(tmpColLst, 1, ","))
                If tmpLine >= 0 Then
                    FileData.SetColumnValue tmpLine, ExtFldCnt, tmpUrno
                    FileData.SetColumnValue tmpLine, 2, "U"
                End If
            End If
        Next
        FileData.RedrawTab
    End If
End Sub
Private Sub CheckInsertFlights()
Dim MaxLine As Long
Dim CurLine As Long
Dim tmpAdid As String
Dim tmpAction As String
Dim ChkMainLine As String
Dim ChkExtrLine As String
Dim ExtrMaxLine As Long

    MaxLine = FileData.GetLineCount - 1
    ExtrMaxLine = FlightExtract.ExtractData.GetLineCount - 1
    For CurLine = 0 To MaxLine
        tmpAdid = FileData.GetColumnValue(CurLine, 1)
        tmpAction = Trim(FileData.GetColumnValue(CurLine, 2))
        If tmpAction = "" Then
            Select Case tmpAdid
                Case "A", "D", "B"
                    ChkMainLine = FileData.GetColumnValue(CurLine, 0)
                    ChkExtrLine = FlightExtract.ExtractData.GetLinesByColumnValue(0, ChkMainLine, 0)
                    'MsgBox ChkMainLine & " / " & ChkExtrLine
                    If Val(ChkExtrLine) > ExtrMaxLine Then ChkExtrLine = ""
                    If ChkExtrLine <> "" Then
                        FileData.SetColumnValue CurLine, 2, "I"
                    End If
                Case Else
            End Select
        End If
    Next
End Sub

Private Sub chkAll_Click()
    If chkAll.Value = 1 Then
        chkAll.BackColor = LightestGreen
        chkReplace.Value = 1
    Else
        chkAll.BackColor = vbButtonFace
    End If
End Sub

Private Sub chkAppl_Click(Index As Integer)
    If Index <> 4 Then
        If chkAppl(Index).Value = 1 Then chkAppl(Index).BackColor = LightestGreen Else chkAppl(Index).BackColor = vbButtonFace
    End If
    Select Case chkAppl(Index).Tag
        Case "EXIT"
            If chkAppl(Index).Value = 1 Then
                ShutDownApplication
                chkAppl(Index).Value = 0
                If TabFrame.Enabled = True Then FileData.SetFocus
            End If
        Case "RESET"
            If chkAppl(Index).Value = 1 Then
                TotalReset = True
                ResetMain 0, False
                TotalReset = False
                MySetUp.InitForm True
                InitApplication
                chkAppl(Index).Value = 0
            End If
        Case "SETUP"
            If chkAppl(Index).Value = 1 Then
                If MySetUp.MonitorSetting(3).Value = 1 Then
                    MySetUp.Show , Me
                Else
                    MySetUp.Show
                End If
            Else
                MySetUp.Hide
                If TabFrame.Enabled = True Then FileData.SetFocus
            End If
        Case "ABOUT"
            If chkAppl(Index).Value = 1 Then
                If MySetUp.MonitorSetting(3).Value = 1 Then
                    MyAboutBox.Show , Me
                Else
                    MyAboutBox.Show
                End If
                chkAppl(Index).Value = 0
            Else
                'Unload MyAboutBox
                'If TabFrame.Enabled = True Then FileData.SetFocus
            End If
        Case "SAVEAS"
            If chkAppl(Index).Value = 1 Then
                chkAppl(Index).BackColor = LightestGreen
                SaveLoadedFile Index, MainDialog.FileData
                chkAppl(Index).Value = 0
            Else
                chkAppl(Index).BackColor = vbButtonFace
            End If
            If TabFrame.Enabled = True Then FileData.SetFocus
        Case "SPARE"
            DontPlay
            If TabFrame.Enabled = True Then FileData.SetFocus
        Case "DRAG"
            If chkAppl(Index).Value = 0 Then chkAppl(8).Value = 0
        Case "ASK"
            If chkAppl(Index).Value = 1 Then chkAppl(3).Value = 1
        Case "HELP"
            If chkAppl(Index).Value = 1 Then DisplayHtmHelpFile "", Me.Name, Me.ActiveControl.Name
            chkAppl(Index).Value = 0
        Case "CONFIG"
            If chkAppl(Index).Value = 1 Then
                CfgTool.Show , Me
                chkAppl(Index).Value = 0
            End If
        Case Else
            If TabFrame.Enabled = True Then FileData.SetFocus
    End Select
End Sub

Private Sub DontPlay()
    If chkSpare.Value = 1 Then
        If MyMsgBox.CallAskUser(0, 0, 0, "Function Button Control", "Don't play with" & vbNewLine & "my spare parts!", "grobi", "", UserAnswer) = 2 Then
            '
        End If
        chkSpare.Value = 0
    End If
End Sub
Public Sub SaveLoadedFile(Index As Integer, myObject As Object)
    Dim FileCount As Integer
    Dim ActFile As String
    Dim ActPath As String
    Dim tmpAnsw As Integer
    Dim FileNo As Integer
    Dim tmpRec As String
    Dim CurLine As Long
    Dim MaxLine As Long
    If (chkAppl(0).Value = 1) Or (Index >= 0) Then
        If myObject.GetLineCount > 0 Then
            FileCount = GetFileAndPath(chkRequest.Tag, myLoadName, myLoadPath)
            ActFile = GetItem(myLoadName, 1, ".") & "_sve.txt"
            UfisTools.CommonDialog.DialogTitle = "Save data as: " & DataSystemType
            UfisTools.CommonDialog.InitDir = myLoadPath
            UfisTools.CommonDialog.FileName = ActFile
            UfisTools.CommonDialog.Filter = "Saved Files|*_sve.txt|Excel Export|*.csv|ASCII Files|*.txt|All|*.*"
            UfisTools.CommonDialog.CancelError = True
            On Error GoTo ErrorHandle
            UfisTools.CommonDialog.ShowSave
            If UfisTools.CommonDialog.FileName <> "" Then
                tmpAnsw = 1
                FileNo = FreeFile
                Open UfisTools.CommonDialog.FileName For Output As #FileNo
                MaxLine = myObject.GetLineCount - 1
                For CurLine = 0 To MaxLine
                    tmpRec = myObject.GetLineValues(CurLine)
                    Print #FileNo, tmpRec
                Next
                Close #FileNo
            End If
        Else
            If MyMsgBox.CallAskUser(0, 0, 0, "Save Data Control", "There's nothing to save!", "export", "", UserAnswer) >= 0 Then DoNothing
        End If
    End If
    Exit Sub
ErrorHandle:
    myLoadName = ""
    myLoadPath = ""
    chkAppl(0).Value = 0
    Exit Sub
End Sub

Private Sub chkBackup_Click()
    Dim UsePath As String
    Dim UseName As String
    Dim PathAndFile As String
    If Not RestoreAction Then
        If chkBackup.Value = 1 Then
            UsePath = App.Path
            UseName = Replace(chkImpTyp(PushedImpButton).Caption, " ", "_", 1, -1, vbBinaryCompare) & "_"
            If FullMode Then
                UseName = UseName & "FULL_" & Format(Now, "yyyymmdd") & ".ite"
            Else
                UseName = UseName & "UPDT_" & Format(Now, "yyyymmdd") & ".ite"
            End If
            If UsePath <> "" Then UsePath = UsePath & "\"
            PathAndFile = UsePath & UseName
            Screen.MousePointer = 11
            MySetUp.Show
            MySetUp.Refresh
            SaveForm PathAndFile, MySetUp, True
            MySetUp.Hide
            Me.Refresh
            SetIniFileEntry Me.Name, Me.Name & "_IMP_BUTTON", Str(PushedImpButton), PathAndFile
            SetIniFileEntry Me.Name, Me.Name & "_COL_COUNT", Str(ExtFldCnt), PathAndFile
            SetIniFileEntry Me.Name, Me.Name & "_UPDT_MODE", Str(CInt(UpdateMode)), PathAndFile
            SetIniFileEntry Me.Name, Me.Name & "_FULL_MODE", Str(CInt(FullMode)), PathAndFile
            StoreLineStatus
            SaveForm PathAndFile, Me, True
            chkBackup.Value = 0
            Screen.MousePointer = 0
        End If
    Else
        chkBackup.Value = 0
    End If
End Sub
Private Sub WriteObjProps(curCtrl)
    Dim ObjType As String
    Print #1, curCtrl.Name
    Print #1, curCtrl.Visible
    Print #1, curCtrl.Tag
    ObjType = TypeName(curCtrl)
    Select Case ObjType
        Case "CheckBox"
            Print #1, curCtrl.Value
        Case "TextBox"
            Print #1, curCtrl.Value
        Case Else
            MsgBox "Undefined Type: " & ObjType
    End Select
End Sub

Private Sub chkRestore_Click()
    Dim UsePath As String
    Dim UseName As String
    Dim PathAndFile As String
    Dim CurCfgTag As String
    Dim CurCfgText As String
    If chkRestore.Value = 1 Then
        If PushedImpButton >= 0 Then chkImpTyp(PushedImpButton).Value = 0
        UsePath = App.Path
        PathAndFile = UsePath & UseName
        UfisTools.CommonDialog.DialogTitle = "Open Backup file"
        UfisTools.CommonDialog.InitDir = UsePath
        UfisTools.CommonDialog.FileName = "*.ite"
        UfisTools.CommonDialog.Filter = "Import Tool Backup Files|*.ite"
        UfisTools.CommonDialog.FilterIndex = 1
        UfisTools.CommonDialog.Flags = (cdlOFNHideReadOnly)
        UfisTools.CommonDialog.CancelError = True
        On Error GoTo ErrorHandle
        UfisTools.CommonDialog.ShowOpen
        If UfisTools.CommonDialog.FileName <> "" Then
            Screen.MousePointer = 11
            RestoreAction = True
            Initializing = True
            PathAndFile = UfisTools.CommonDialog.FileName
            CurCfgTag = MySetUp.ServerTime.Tag
            CurCfgText = MySetUp.ServerTime.Text
            ReadForm PathAndFile, MySetUp, True
            MySetUp.ServerTime.Tag = CurCfgTag
            MySetUp.ServerTime.Text = CurCfgText
            RestoreAction = True
            PushedImpButton = Val(GetIniFileEntry(Me.Name, Me.Name & "_IMP_BUTTON", "-1", PathAndFile))
            If PushedImpButton >= 0 Then
                chkImpTyp(PushedImpButton).Value = 1
            End If
            Initializing = False
            ReadForm PathAndFile, Me, True
            RestoreAction = True
            ExtFldCnt = Val(GetIniFileEntry(Me.Name, Me.Name & "_COL_COUNT", Str(ExtFldCnt), PathAndFile))
            UpdateMode = Val(GetIniFileEntry(Me.Name, Me.Name & "_UPDT_MODE", Str(CInt(UpdateMode)), PathAndFile))
            FullMode = Val(GetIniFileEntry(Me.Name, Me.Name & "_FULL_MODE", Str(CInt(FullMode)), PathAndFile))
            GridPrepared = True
            InitDataList 10, False
            If MySetUp.MonitorSetting(1).Value = 1 Then ToggleDateFormat True Else ToggleDateFormat False
            RestoreLineStatus
            RestoreCellObjectInfo
            RestoreAction = False
            chkRestore.Value = 0
            Screen.MousePointer = 0
        End If
        Exit Sub
    End If
ErrorHandle:
    chkRestore.Value = 0
End Sub


Private Sub chkCheck_Click()
    If Not RestoreAction Then
        If Not TotalReset Then
            If chkCheck.Value = 1 Then
                chkClear.Value = 1
                PerformChecks 0, -1
                chkCheck.BackColor = LightestGreen
            Else
                MainDialog.MousePointer = 11
                chkIdentify.Value = 0
                ClearErrorStatus 0, -1
                chkCheck.BackColor = vbButtonFace
                MainDialog.MousePointer = 0
            End If
        End If
        If TabFrame.Enabled = True Then FileData.SetFocus
    End If
End Sub

Private Sub PerformChecks(LoopStart As Long, LoopEnd As Long)
Dim tmpRowNum As Long
Dim tmpFldNam As String
Dim tmpTblNam As String
Dim DontCheck As String
Dim FieldList As String
Dim ErrLinLst As String
Dim ErrLinNbr As Integer
Dim Columns As String
Dim i As Integer
Dim DataLen As Integer
    MainDialog.MousePointer = 11
    'must be checked again in case of manual changes
    CheckVpfrVptoFrame LoopStart, LoopEnd
    CheckEmptyLines LoopStart, LoopEnd
    If CedaIsConnected Then
        If AutoCheck <> "" Then
            i = 0
            Do
                tmpFldNam = GetItem(AutoCheck, i + 1, ",")
                'cfg rule syntax will be changed soon
                DataLen = Val(GetItem(tmpFldNam, 2, "/"))
                tmpFldNam = GetItem(tmpFldNam, 1, "/")
                DontCheck = GetItem(AutoCheck, i + 2, ",")
                tmpTblNam = GetItem(AutoCheck, i + 3, ",")
                tmpRowNum = Val(GetItem(AutoCheck, i + 4, ",")) + 2
                FieldList = GetItem(AutoCheck, i + 5, ",")
                Columns = GetItem(AutoCheck, i + 6, ",")
                If tmpFldNam <> "" Then CheckBasicData LoopStart, LoopEnd, tmpRowNum, tmpFldNam, tmpTblNam, DataLen, FieldList, Columns, DontCheck
                i = i + 6
            Loop While tmpFldNam <> ""
        End If
        If ChkBasDat <> "" Then
            i = 0
            Do
                tmpFldNam = GetItem(ChkBasDat, i + 1, ",")
                'cfg rule syntax will be changed soon
                DataLen = Val(GetItem(tmpFldNam, 2, "/"))
                tmpFldNam = GetItem(tmpFldNam, 1, "/")
                DontCheck = GetItem(ChkBasDat, i + 2, ",")
                tmpTblNam = GetItem(ChkBasDat, i + 3, ",")
                tmpRowNum = Val(GetItem(ChkBasDat, i + 4, ",")) + 2
                If tmpFldNam <> "" Then CheckBasicData LoopStart, LoopEnd, tmpRowNum, tmpFldNam, tmpTblNam, DataLen, "", "", DontCheck
                i = i + 4
            Loop While tmpFldNam <> ""
        End If
    Else
        'If MyMsgBox.CallAskUser(0, 0, 0, "Check Basic Data", "You are not connected to a server.", "netfail", "", UserAnswer) >= 0 Then DoNothing
    End If
    FileData.RedrawTab
    ResetErrorCounter
    MainDialog.MousePointer = 0
End Sub

Public Sub ClearErrorStatus(LoopStart As Long, LoopEnd As Long)
    ClearColumnValue LoopStart, LoopEnd, ExtFldCnt, 0, True
    ClearColumnValue LoopStart, LoopEnd, 1, 0, True
    ClearColumnValue LoopStart, LoopEnd, 2, 0, True
End Sub

Private Sub CheckEmptyLines(LoopBegin As Long, LoopEnd As Long)
Dim CurLine As Long
Dim MaxLine As Long
Dim tmpFldVal As String
Dim ColIdx As Long
Dim ItmNbr As Integer
Dim InvalidLine As Boolean
    If IgnoreEmpty <> "" Then
        If LoopEnd >= 0 Then
            MaxLine = LoopEnd
        Else
            MaxLine = FileData.GetLineCount - 1
        End If
        For CurLine = LoopBegin To MaxLine
            InvalidLine = True
            ItmNbr = 0
            tmpFldVal = FileData.GetColumnValue(CurLine, 1)
            If tmpFldVal = "" Then
                Do
                    ItmNbr = ItmNbr + 1
                    ColIdx = Val(GetItem(IgnoreEmpty, ItmNbr, ",")) + 2
                    If ColIdx > 2 Then tmpFldVal = FileData.GetColumnValue(CurLine, ColIdx)
                    If tmpFldVal <> "" Then
                        InvalidLine = False
                        ColIdx = 0
                    End If
                Loop While ColIdx > 2
                If InvalidLine Then
                    FileData.SetColumnValue CurLine, 1, "-"
                    FileData.SetColumnValue CurLine, 2, "-"
                    FileData.SetColumnValue CurLine, ExtFldCnt, "IGNORED"
                    FileData.SetLineColor CurLine, vbBlack, LightGray
                End If
            End If
        Next
    End If
End Sub

Private Sub ShowEditGroup(bpFlag)
    txtEdit.Enabled = bpFlag
    If bpFlag = False Then
        chkEdit.Tag = ""
        txtEdit.Text = ""
        txtEdit.Tag = ""
        chkReplace.ToolTipText = ""
        chkAll.ToolTipText = ""
        chkAll.Tag = ""
        txtEdit.BackColor = vbButtonFace
        txtEdit.Locked = True
    Else
        txtEdit.BackColor = vbWhite
        txtEdit.Locked = False
    End If
    chkReplace.Enabled = bpFlag
    chkAll.Enabled = False
    FileData.EnableHeaderSelection bpFlag
End Sub

Private Sub ClearColumnValue(LoopStart As Long, LoopEnd As Long, lpCol As Long, lpLen As Long, ClearErrorLines As Boolean)
    Dim MyForeColor As Long
    Dim MyBackColor As Long
    Dim llCount As Long
    Dim i As Long
    Dim StatCol As Long
    Dim clData As String
    Dim tmpData As String
    MousePointer = 11
    If lpLen > 0 Then clData = Space(lpLen) Else clData = ""
    If LoopEnd >= 0 Then
        llCount = LoopEnd
    Else
        llCount = FileData.GetLineCount - 1
    End If
    StatCol = ExtFldCnt + 1
    For i = LoopStart To llCount
        If ClearErrorLines Then
            FileData.SetColumnValue i, lpCol, clData
        Else
            If lpCol = 1 Then
                tmpData = FileData.GetColumnValue(i, lpCol)
                tmpData = Trim(tmpData)
                If tmpData <> "E" Then
                    FileData.SetColumnValue i, lpCol, clData
                End If
            End If
        End If
        If ClearErrorLines Then
            FileData.SetLineColor i, vbBlack, vbWhite
            FileData.ResetLineDecorations i
            FileData.SetColumnValue i, StatCol, ""
        Else
            FileData.GetLineColor i, MyForeColor, MyBackColor
            If MyBackColor <> ErrorColor Then FileData.SetLineColor i, vbBlack, vbWhite
        End If
    Next
    MousePointer = 0
End Sub
Private Sub CheckBasicData(LoopStart As Long, LoopEnd As Long, tmpCol As Long, tmpFldNam As String, tmpTblNam As String, DataLen As Integer, FieldList As String, Columns As String, DontCheck As String)
Dim ChkList As String
Dim llCount As Long
Dim i As Long
Dim ItmNbr As Integer
Dim tmpData As String
Dim chkData As String
Dim RetCod
Dim tmpSqlKey As String
Dim tmpResult As String
Dim tmpColLst As String
Dim tmpFldLst As String
Dim tmpFldVal As String
Dim llLine As Long
Dim CurColIdx As Long
Dim clLineIdx As String
Dim LinItm As Integer
Dim Col0Val As String
Dim Col1Val As String
Dim Col2Val As String
Dim CedaData As String
    If CedaIsConnected Then
        StatusBar.Panels(7).Text = "Check Basic Data: " & tmpFldNam
        If LoopEnd >= 0 Then
            llCount = LoopEnd
        Else
            llCount = FileData.GetLineCount - 1
        End If
        ChkList = ""
        For i = LoopStart To llCount
            Col0Val = FileData.GetColumnValue(i, 0)
            Col1Val = FileData.GetColumnValue(i, 1)
            Col2Val = FileData.GetColumnValue(i, 2)
            If Not ((Col1Val = "-") And (Col2Val = "-")) Then
                tmpData = FileData.GetColumnValue(i, tmpCol)
                If DataLen > 0 Then
                    If Len(tmpData) <> DataLen Then tmpData = ""
                End If
                If (DontCheck <> "") And (tmpData <> "") Then
                    If InStr(DontCheck, tmpData) > 0 Then
                        'MsgBox "Not Checked: " & Col0Val & vbNewLine & DontCheck & vbNewLine & tmpData
                        tmpData = ""
                    End If
                End If
                If tmpData <> "" Then
                    tmpData = "'" & tmpData & "'"
                    If InStr(ChkList, tmpData) = 0 Then ChkList = ChkList & tmpData & ","
                End If
            End If
        Next
        'MsgBox chkList
        If ChkList <> "" Then
            If Len(ChkList) > 0 Then ChkList = Left(ChkList, Len(ChkList) - 1)
            tmpSqlKey = "WHERE " & tmpFldNam & " IN (" & ChkList & ")"
            tmpFldLst = tmpFldNam
            If FieldList <> "" Then
                tmpFldLst = tmpFldLst & "," & Replace(FieldList, ";", ",", 1, -1, vbBinaryCompare)
            End If
            'MsgBox "Proceed ?"
            'dbgTrace = chkList & vbNewLine & vbNewLine
            RetCod = UfisServer.CallCeda(tmpResult, "RTA", tmpTblNam, tmpFldLst, "", tmpSqlKey, "", 1, False, False)
            'dbgTrace = dbgTrace & vbNewLine & vbNewLine
            If DontCheck <> "" Then
                ChkList = ChkList & "," & Replace(DontCheck, ";", ",", 1, -1, vbBinaryCompare)
            End If
            llCount = UfisServer.DataBuffer(1).GetLineCount - 1
            For i = 0 To llCount
                CedaData = UfisServer.DataBuffer(1).GetLineValues(i)
                tmpFldVal = GetItem(CedaData, 1, ",")
                'dbgTrace = dbgTrace & tmpFldVal & ","
                tmpData = "'" & tmpFldVal & "'"
                ChkList = Replace(ChkList, tmpData, "", 1, -1, vbBinaryCompare)
                If FieldList <> "" Then
                    tmpColLst = FileData.GetLinesByColumnValue(tmpCol, tmpFldVal, 1)
                    clLineIdx = "START"
                    LinItm = 0
                    While clLineIdx <> ""
                        LinItm = LinItm + 1
                        clLineIdx = GetItem(tmpColLst, LinItm, ",")
                        If clLineIdx <> "" Then
                            llLine = Val(clLineIdx)
                            Col1Val = FileData.GetColumnValue(llLine, 1)
                            Col2Val = FileData.GetColumnValue(llLine, 2)
                            If Not ((Col1Val = "-") And (Col2Val = "-")) Then
                                tmpResult = tmpFldNam & ":" & tmpData
                                tmpFldVal = GetItem(CedaData, 2, ",")
                                CurColIdx = Val(GetItem(Columns, 1, ";")) + 2
                                If CurColIdx > 2 Then
                                    FileData.SetColumnValue llLine, CurColIdx, tmpFldVal
                                End If
                            End If
                        End If
                    Wend
                End If
            Next
            ChkList = Replace(ChkList, "'", "", 1, -1, vbBinaryCompare)
            ChkList = ChkList & ",*END*"
            ItmNbr = 0
            tmpData = "START"
            While ItmNbr >= 0
                ItmNbr = ItmNbr + 1
                tmpData = GetItem(ChkList, ItmNbr, ",")
                If (tmpData <> "") Then
                    If (tmpData <> "*END*") Then
                        tmpColLst = FileData.GetLinesByColumnValue(tmpCol, tmpData, 1)
                        clLineIdx = "START"
                        LinItm = 0
                        While clLineIdx <> ""
                            LinItm = LinItm + 1
                            clLineIdx = GetItem(tmpColLst, LinItm, ",")
                            If clLineIdx <> "" Then
                                llLine = Val(clLineIdx)
                                Col1Val = FileData.GetColumnValue(llLine, 1)
                                Col2Val = FileData.GetColumnValue(llLine, 2)
                                If Not ((Col1Val = "-") And (Col2Val = "-")) Then
                                    tmpResult = tmpFldNam & ":" & tmpData
                                    SetErrorText Str(tmpCol), llLine, ExtFldCnt, tmpResult, True, 0
                                End If
                            End If
                        Wend
                    Else
                        ItmNbr = -1
                    End If
                End If
            Wend
        End If
        StatusBar.Panels(7).Text = "Ready"
        Refresh
    Else
        If MyMsgBox.CallAskUser(0, 0, 0, "Check Basic Data", "You are not connected to a server.", "stop", "", UserAnswer) >= 0 Then DoNothing
    End If
End Sub

Private Sub SetErrorRange(ColIdx As Long, FirstLine As Long, LastLine As Long, ErrTxt As String)
    Dim CurLine As Long
    For CurLine = FirstLine To LastLine
        SetErrorText Str(ColIdx), CurLine, ExtFldCnt, ErrTxt, True, 1
    Next
End Sub
Private Sub SetErrorText(ErrColList As String, tmpLine As Long, tmpCol As Long, tmpText As String, Markit As Boolean, ErrPrio As Integer)
Dim ErrorCol As Long
Dim ItmNbr As Integer
Dim ItmTxt As String
Dim tmpResult As String
Dim tmpdat As String
    Select Case ErrPrio
        Case 1
            FileData.SetLineColor tmpLine, vbBlack, LightestRed
            If Markit Then FileData.SetColumnValue tmpLine, 1, "W"
        Case Else
            FileData.SetLineColor tmpLine, vbWhite, ErrorColor
            If Markit Then FileData.SetColumnValue tmpLine, 1, "E"
    End Select
    tmpResult = FileData.GetColumnValue(tmpLine, tmpCol)
    If InStr(tmpResult, tmpText) = 0 Then tmpResult = tmpResult & tmpText & " "
    FileData.SetColumnValue tmpLine, tmpCol, tmpResult
    If Markit Then FileData.SetColumnValue tmpLine, 2, "-"
    If ErrColList <> "" Then
        ItmTxt = "START"
        ItmNbr = 0
        While ItmTxt <> ""
            ItmNbr = ItmNbr + 1
            ItmTxt = GetItem(ErrColList, ItmNbr, ",")
            If ItmTxt <> "" Then
                ErrorCol = Val(ItmTxt)
                If ErrorCol > 2 Then
                    FileData.SetDecorationObject tmpLine, ErrorCol, "CellError"
                    StoreCellObjectInfo tmpLine, ErrorCol
                End If
            End If
        Wend
    End If
End Sub

Private Sub StoreCellObjectInfo(LineNo As Long, ColNo As Long)
    Dim StatCol As Long
    Dim CurList As String
    Dim ChkList As String
    Dim AddVal As String
    Dim ChkCol As String
    StatCol = ExtFldCnt + 1
    AddVal = Trim(Str(ColNo))
    CurList = FileData.GetColumnValue(LineNo, StatCol)
    If CurList <> "" Then
        ChkList = "/" & CurList & "/"
        ChkCol = "/" & AddVal & "/"
        If InStr(ChkList, ChkCol) = 0 Then CurList = CurList & "/" & AddVal
    Else
        CurList = AddVal
    End If
    FileData.SetColumnValue LineNo, StatCol, CurList
End Sub
Private Sub RestoreCellObjectInfo()
    Dim CurLine As Long
    Dim MaxLine As Long
    Dim ColNo As Long
    Dim ItmNbr As Integer
    Dim StatCol As Long
    Dim CurList As String
    Dim ChkCol As String
    StatCol = ExtFldCnt + 1
    MaxLine = FileData.GetLineCount - 1
    For CurLine = 0 To MaxLine
        CurList = FileData.GetColumnValue(CurLine, StatCol)
        ChkCol = CurList
        ItmNbr = 0
        While ChkCol <> ""
            ItmNbr = ItmNbr + 1
            ChkCol = GetItem(CurList, ItmNbr, "/")
            If ChkCol <> "" Then
                ColNo = Val(ChkCol)
                FileData.SetDecorationObject CurLine, ColNo, "CellError"
            End If
        Wend
    Next
End Sub

Private Sub ResetErrorCounter()
    Dim ErrLinLst As String
    Dim ErrLinNbr As Integer
    ErrLinLst = FileData.GetLinesByBackColor(ErrorColor)
    ErrLinNbr = ItemCount(ErrLinLst, ",")
    If ErrLinNbr > 0 Then
        ErrCnt.Caption = Str(Trim(ErrLinNbr))
        ErrCnt.BackColor = vbRed
        ErrCnt.ForeColor = vbWhite
    Else
        ErrCnt.Caption = ""
        ErrCnt.BackColor = vbButtonFace
        ErrCnt.ForeColor = vbBlack
    End If
    ErrCnt.Refresh
    ErrLinLst = FileData.GetLinesByBackColor(LightestRed)
    ErrLinNbr = ItemCount(ErrLinLst, ",")
    If ErrLinNbr > 0 Then
        WarnCnt.Caption = Str(Trim(ErrLinNbr))
        WarnCnt.BackColor = LightestRed
        WarnCnt.ForeColor = vbBlack
    Else
        WarnCnt.Caption = ""
        WarnCnt.BackColor = vbButtonFace
        WarnCnt.ForeColor = vbBlack
    End If
    WarnCnt.Refresh
End Sub

Private Sub chkCheck_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
    Dim MsgTxt As String
    Dim LineNo As Long
    If (Button = 2) And (chkCheck.Value = 1) Then
        LineNo = FileData.GetCurrentSelected
        If LineNo >= 0 Then
            MsgTxt = ""
            MsgTxt = MsgTxt & "Do you want to check" & vbNewLine
            MsgTxt = MsgTxt & "the highlighted line?"
            If MyMsgBox.CallAskUser(0, 0, 0, "Data Validation", MsgTxt, "ask", "Yes,No", UserAnswer) = 1 Then
                ClearErrorStatus LineNo, LineNo
                PerformChecks LineNo, LineNo
                PerformIdentify
            End If
        End If
    End If
End Sub

Private Sub chkClear_Click()
    Dim FilterVpfr As String
    Dim FilterVpto As String
    Dim IsValidLine As Boolean
    Dim CurLine As Long
    Dim MaxLine As Long
    Dim RecData As String
    Dim OldRecData As String
    Dim Col1Txt As String
    Dim Col2Txt As String
    Dim MsgTxt As String
    If Not RestoreAction Then
        If chkClear.Value = 1 Then
            chkShrink.Value = 1
            If (MySetUp.chkWork(1).Value = 1) And (chkDontClear.Value = 0) Then
                chkClear.BackColor = LightestGreen
                StatusBar.Panels(7).Text = "Clearing out data of the past ..."
                Refresh
                If (DataSystemType = "SLOT") Or (DataSystemType = "FHK") Or (DataSystemType = "FREE") Then
                    Screen.MousePointer = 11
                    InitPeriodCheck MainPeriodCols, False
                    FilterVpfr = ActSeason(3).Tag
                    FilterVpto = ActSeason(2).Tag
                    MaxLine = FileData.GetLineCount - 1
                    CurLine = 0
                    While CurLine <= MaxLine
                        IsValidLine = True
                        RecData = FileData.GetLineValues(CurLine)
                        Col1Txt = GetItem(RecData, 2, ",")
                        Col2Txt = GetItem(RecData, 3, ",")
                        If (Col1Txt <> "E" And Col1Txt <> "-") And Col2Txt <> "-" Then
                            OldRecData = RecData
                            IsValidLine = CheckFlightPeriod(FilterVpfr, FilterVpto, RecData, IsValidLine, True, False, "")
                            If IsValidLine Then
                                If OldRecData <> RecData Then
                                    FileData.UpdateTextLine CurLine, RecData, False
                                End If
                            Else
                                FileData.DeleteLine CurLine
                                CurLine = CurLine - 1
                                MaxLine = MaxLine - 1
                            End If
                        End If
                        CurLine = CurLine + 1
                    Wend
                    FileData.RedrawTab
                    MaxLine = FileData.GetLineCount
                    StatusBar.Panels(2).Text = Str(MaxLine)
                    chkClear.Enabled = False
                    StatusBar.Panels(7).Text = "Ready"
                    Refresh
                    Screen.MousePointer = 0
                    ResetErrorCounter
                Else
                    MsgTxt = ""
                    MsgTxt = MsgTxt & "The function 'Clear Out' is" & vbNewLine
                    MsgTxt = MsgTxt & "not yet available for '" & DataSystemType & "'"
                    If MyMsgBox.CallAskUser(0, 0, 0, "Configuration Control", MsgTxt, "hand", "", UserAnswer) >= 0 Then DoNothing
                    StatusBar.Panels(7).Text = ""
                    Refresh
                End If
            Else
                '
            End If
            If chkDontClear.Value = 0 Then
                chkDontClear.Value = 1
                chkDontClear.BackColor = LightestGreen
                chkDontClear.Caption = ""
                chkDontClear.Enabled = False
            End If
        Else
            chkClear.BackColor = vbButtonFace
        End If
        If TabFrame.Enabled = True Then FileData.SetFocus
    End If
End Sub

Private Sub chkDontClear_Click()
    If Not RestoreAction Then
        If chkDontClear.Value = 1 Then
            chkDontClear.BackColor = vbRed
            chkDontClear.ForeColor = vbWhite
            chkDontClear.Caption = "X"
            chkClear.Value = 1
        Else
            chkDontClear.BackColor = vbButtonFace
            chkDontClear.ForeColor = vbBlack
            chkDontClear.Caption = "=>"
        End If
        If TabFrame.Enabled = True Then FileData.SetFocus
    End If
End Sub

Private Sub chkEdit_Click()
    If chkEdit.Value = 1 Then
        ShowEditGroup True
        chkEdit.BackColor = LightestGreen
        chkInline.Value = 0
    Else
        chkEdit.BackColor = vbButtonFace
        ShowEditGroup False
    End If
    If TabFrame.Enabled = True Then FileData.SetFocus
End Sub

Private Sub chkExpand_Click()
    If Not RestoreAction Then
        If (Not TotalReset) Then
            ResetTimeFields
            If chkExpand.Value = 1 Then
                StopRecursion = True
                chkSplit.Value = 1
                StopRecursion = False
                CurrentHeader = ExpandHeader
                CurFldPosLst = ExpPosLst
                chkExpand.Enabled = True
            Else
                chkCheck.Value = 0
                chkShrink.Value = 0
                chkClear.Value = 0
                chkDontClear.Value = 0
                chkClear.Enabled = True
                chkDontClear.Enabled = True
                chkExpand.BackColor = vbButtonFace
                CurrentHeader = NormalHeader
                CurFldPosLst = FldPosLst
            End If
            If chkLoad.Value = 1 Then
                If Not StopRecursion Then LoadFileData "(for preparation)"
            Else
                chkLoad.Value = 1
            End If
            FileData.RedrawTab
            TabFrame.Enabled = True
            If chkExpand.Value = 1 Then
                chkExpand.BackColor = LightestGreen
                MySetUp.MonitorSetting(0).Enabled = True
                MySetUp.MonitorSetting(1).Enabled = True
            Else
                MySetUp.MonitorSetting(0).Enabled = False
                MySetUp.MonitorSetting(1).Enabled = False
            End If
            If TabFrame.Enabled = True Then FileData.SetFocus
        End If
    End If
End Sub

Private Sub chkExtract_Click()
    Dim Answ As Integer
    Dim MsgTxt As String
    If Not RestoreAction Then
        If chkExtract.Value = 1 Then
            chkEdit.Value = 0
            chkInline.Value = 0
            chkIdentify.Value = 1
            Answ = 1
            MsgTxt = ""
            If Val(ErrCnt.Caption) > 0 Then
                MsgTxt = MsgTxt & ErrCnt.Caption & " errors"
            End If
            If Val(WarnCnt.Caption) > 0 Then
                If MsgTxt <> "" Then MsgTxt = MsgTxt & " and "
                MsgTxt = MsgTxt & WarnCnt.Caption & " warnings"
            End If
            If MsgTxt <> "" Then
                MsgTxt = "This file contains still" & MsgTxt & vbNewLine & "Do you want to proceed?"
                Answ = MyMsgBox.CallAskUser(0, 0, 0, "File Import Control", MsgTxt, "query", "Yes,No;F", UserAnswer)
            End If
            If Answ = 1 Then
                chkSort.Value = 1
                MousePointer = 11
                chkSort.Enabled = False
                If MySetUp.MonitorSetting(3).Value = 1 Then
                    FlightExtract.Show , Me
                Else
                    FlightExtract.Show
                End If
                MousePointer = 0
                chkExtract.BackColor = LightestGreen
            Else
                chkExtract.Value = 0
            End If
        Else
            If FormIsVisible("RecFilter") Then RecFilter.Hide
            If FormIsVisible("FlightExtract") Then FlightExtract.Hide
            If DataSystemType = "FHK" Then chkSort.Enabled = True
            chkExtract.BackColor = vbButtonFace
            If TabFrame.Enabled = True Then FileData.SetFocus
        End If
    End If
End Sub

Private Sub chkFilter_Click()
    If Not RestoreAction Then
        If Not TotalReset Then
            MySetUp.chkFilter.Value = chkFilter.Value
            If chkFilter.Value = 0 Then
                chkFilter.BackColor = vbButtonFace
                StopRecursion = True
                chkSplit.Value = 0
                StopRecursion = False
            End If
            If chkLoad.Value = 1 Then LoadFileData "(For Filter)" Else chkLoad.Value = 1
            If chkFilter.Value = 1 Then chkFilter.BackColor = LightestGreen
        End If
    End If
End Sub

Private Sub chkIdentify_Click()
    If Not RestoreAction Then
        If Not TotalReset Then
            ArrFltCnt = 0
            DepFltCnt = 0
            If chkIdentify.Value = 1 Then
                chkCheck.Value = 1
                PerformIdentify
                chkIdentify.BackColor = LightestGreen
            Else
                chkExtract.Value = 0
                Unload FlightExtract
                chkIdentify.BackColor = vbButtonFace
                ClearColumnValue 0, -1, 1, 0, False
            End If
            FileData.RedrawTab
        End If
        If TabFrame.Enabled = True Then FileData.SetFocus
    End If
End Sub
Private Sub PerformIdentify()
    If chkIdentify.Value = 1 Then
        If LegColNbr > 2 And OrgColNbr > 2 And DesColNbr > 2 Then
            IdentifyFlights 0, LegColNbr, OrgColNbr, DesColNbr
        ElseIf CheckRotation <> "" Then
            IdentifyRotations CheckRotation
        'ElseIf DataSystemType = "FHK" Then
        '    IdentifyRotations "ADID,5"
        End If
        ResetErrorCounter
    End If
End Sub
Private Sub IdentifyRotations(FltnList As String)
Dim CurLine As Long
Dim MaxLine As Long
Dim FlnaCol As Long
Dim FlndCol As Long
Dim AdidCol As Long
Dim tmpFlna As String
Dim tmpFlnd As String
Dim tmpAdid As String
Dim Col1Val As String
Dim Col2Val As String
    If FltnList <> "" Then
        MousePointer = 11
        If InStr(FltnList, "ADID") > 0 Then
            AdidCol = Val(GetItem(FltnList, 2, ",")) + 2
        Else
            FlnaCol = Val(GetItem(FltnList, 1, ",")) + 2
            FlndCol = Val(GetItem(FltnList, 2, ",")) + 2
        End If
        If ((FlnaCol > 2) And (FlndCol > 2)) Or (AdidCol > 2) Then
            MaxLine = FileData.GetLineCount - 1
            For CurLine = 0 To MaxLine
                Col1Val = FileData.GetColumnValue(CurLine, 1)
                Col2Val = FileData.GetColumnValue(CurLine, 2)
                If (Col1Val <> "E") And (Col2Val <> "-") Then
                    If AdidCol > 2 Then
                        tmpAdid = FileData.GetColumnValue(CurLine, AdidCol)
                        If tmpAdid = "A" Then
                            FileData.SetLineColor CurLine, vbBlack, vbYellow
                        ElseIf tmpAdid = "D" Then
                            FileData.SetLineColor CurLine, vbBlack, vbGreen
                        Else
                            'empty line ??
                            SetErrorText "", CurLine, ExtFldCnt, "ADID:???", True, 0
                        End If
                    Else
                        tmpFlna = FileData.GetColumnValue(CurLine, FlnaCol)
                        tmpFlnd = FileData.GetColumnValue(CurLine, FlndCol)
                        If (tmpFlna <> "") And (tmpFlnd <> "") Then
                            'rotation
                        ElseIf tmpFlna <> "" Then
                            FileData.SetLineColor CurLine, vbBlack, vbYellow
                        ElseIf tmpFlnd <> "" Then
                            FileData.SetLineColor CurLine, vbBlack, vbGreen
                        Else
                            'empty line ??
                            SetErrorText "", CurLine, ExtFldCnt, "FLTN:???", True, 0
                        End If
                    End If
                End If
            Next
        End If
        MousePointer = 0
    End If
End Sub
Private Sub chkImport_Click()
    If Not RestoreAction Then
        If chkImport.Value = 1 Then
            OaFosImport
        Else
            chkImport.BackColor = vbButtonFace
        End If
    End If
End Sub

Public Sub OaFosImport()
Dim ErrMsgTxt As String
        ErrMsgTxt = ""
        If AftFldLst = "" Then
            ErrMsgTxt = ErrMsgTxt & "Missing AFT field list for output" & vbNewLine
        End If
        If ExtFldLst = "" Then
            ErrMsgTxt = ErrMsgTxt & "Missing EXT field list for output" & vbNewLine
        End If
        If ErrMsgTxt = "" Then
            Select Case ActionType
                Case "CEDA_CMD"
                    PrecheckCedaAction 3, LightGray
                Case Else
            End Select
            chkImport.BackColor = LightestGreen
        Else
            MyMsgBox.InfoApi 0, "The keywords AFT_FIELDS or EXT_FIELDS" & vbNewLine & "are not included in ImportTool.ini for this session.", "More info"
            If MyMsgBox.CallAskUser(0, 0, 0, "Configuration Control", ErrMsgTxt, "stop", "", UserAnswer) >= 0 Then DoNothing
        End If
End Sub

Private Sub PrecheckCedaAction(doRelease As Integer, CheckColor As Long)
Dim pclFullName As String
Dim CurLine As Long
Dim MaxLine As Long
Dim FirstLine As Long
Dim LastLine As Long
Dim tmpCol1Val As String
Dim tmpCol2Val As String
Dim tmpFldNam As String
Dim tmpFldVal As String
Dim CurFlno As String
Dim OldFlno As String
Dim CurAdid As String
Dim OldAdid As String
Dim tmpFlcaVal As String
Dim tmpFltnVal As String
Dim ErrorTrip As Boolean
Dim ArrivalTrip As Boolean
Dim DepartureTrip As Boolean
Dim ViaTrip As Boolean
Dim FldColIdx As Long
Dim FlcaCol As Long
Dim FltnCol As Long
    Me.MousePointer = 11
    'MsgBox "Precheck " & doRelease
    If doRelease = 1 Then WarnCnt.Caption = 0
    If OutputType = "LOCAL_FILE" Then
        pclFullName = myLoadPath & "\CMD_" & myLoadName
        Open pclFullName For Output As #2
    End If
    'hardcode
    FlcaCol = 1 + 2
    FltnCol = 2 + 2
    OldFlno = ""
    CurAdid = "?"
    OldAdid = "?"
    FirstLine = -1
    LastLine = -1
    ErrorTrip = False
    ArrivalTrip = False
    DepartureTrip = False
    ViaTrip = False
    MaxLine = FileData.GetLineCount
    For CurLine = 0 To MaxLine
        tmpFlcaVal = FileData.GetColumnValue(CurLine, FlcaCol)
        tmpFltnVal = FileData.GetColumnValue(CurLine, FltnCol)
        CurFlno = tmpFlcaVal & tmpFltnVal
        tmpCol1Val = FileData.GetColumnValue(CurLine, 1)
        tmpCol2Val = FileData.GetColumnValue(CurLine, 2)
        If CurFlno <> OldFlno Then
            If OldFlno <> "" Then
                'Run through the flight lines
                'MsgBox OldFlno & " / " & FirstLine + 1 & " / " & LastLine + 1 & " / " & ErrorTrip
                If ErrorTrip Then
                    SetFlightLineErrors FirstLine, LastLine, CheckColor
                Else
                    GetFlightRecInfo FirstLine, LastLine, CurAdid, doRelease
                End If
            End If
            FirstLine = CurLine
            ErrorTrip = False
            ArrivalTrip = False
            DepartureTrip = False
            CurAdid = "?"
            OldFlno = CurFlno
        End If
        If (tmpCol1Val = "A") Or (CurAdid = "?" And tmpCol1Val = "1") Then
            CurAdid = "A"
            ArrivalTrip = True
        ElseIf tmpCol1Val = "D" Then
            CurAdid = tmpCol1Val
            DepartureTrip = True
        Else
        End If
        If tmpCol2Val = "-" Then ErrorTrip = True
        OldFlno = CurFlno
        OldAdid = CurAdid
        LastLine = CurLine
            ErrorTrip = False
            ArrivalTrip = False
            DepartureTrip = False
    Next
    If OutputType = "LOCAL_FILE" Then
        Close #2
    End If
    Me.MousePointer = 0
End Sub

Private Sub SetFlightLineErrors(FirstLine As Long, LastLine As Long, BackColor As Long)
    Dim CurLine As Long
    Dim CurForeColor As Long
    Dim CurBackColor As Long
    For CurLine = FirstLine To LastLine
        FileData.SetColumnValue CurLine, 2, "-"
        FileData.GetLineColor CurLine, CurForeColor, CurBackColor
        If CurBackColor <> ErrorColor Then
            FileData.SetLineColor CurLine, vbBlack, BackColor
        End If
    Next
    FileData.RedrawTab
End Sub

Private Sub GetFlightRecInfo(FirstLine As Long, LastLine As Long, CurAdid As String, doRelease As Integer)
Dim ColStatus As String
Dim tmpTblNam As String
Dim CurLine As Long
Dim ItmNbr As Integer
Dim FldColIdx As Long
Dim tmpFldNam As String
Dim tmpFldVal As String
Dim TblFldVal As String
Dim tmpOutFldLst As String
Dim tmpOutDatLst As String
Dim ActOutFldLst As String
Dim ActOutDatLst As String
Dim tmpCmdVal As String
Dim tmpActCmd As String
Dim tmpSqlKey As String
Dim tmpUrno As String
Dim RetVal As Integer
Dim ResultBuffer As String
Dim ActRem As String
Dim tmpAlc2 As String
Dim tmpFlno As String
Dim tmpStoaDay As String
Dim tmpStodDay As String
Dim tmpViaList As String
Dim ActViaList As String
Dim FltCnt

    'this function is called 3 times
    'by value of doRelease:
    '1 = step through the list and increase the total number of ceda transactions (flights)
    '2 = for update transactions: read the existing data,
    '    decrease the total number and set the flight line to OK when nothing has to be changed
    '3 = perform the inserts and remaining updates,
    '    decrease the total number and set the flight line to DONE
    
    FltCnt = WarnCnt.Caption
    If doRelease = 1 Then FltCnt = FltCnt + 1
    
    'The first line always contains data of the departure airport (left part)
    'The last line always contains data of the destination airport (right part)
    'All the lines contain data of the aircraft
    'if there is more than one line, the via information must be handled separately
    
    Select Case CurAdid
        Case "A"
            tmpCmdVal = FileData.GetColumnValue(LastLine, 2)
            tmpUrno = FileData.GetColumnValue(LastLine, ExtFldCnt)
            ColStatus = FileData.GetColumnValue(LastLine, 0)
        Case "B"
            tmpCmdVal = FileData.GetColumnValue(LastLine, 2)
            tmpUrno = FileData.GetColumnValue(LastLine, ExtFldCnt)
            ColStatus = FileData.GetColumnValue(LastLine, 0)
        Case "D"
            tmpCmdVal = FileData.GetColumnValue(FirstLine, 2)
            tmpUrno = FileData.GetColumnValue(FirstLine, ExtFldCnt)
            ColStatus = FileData.GetColumnValue(FirstLine, 0)
        Case Else
    End Select
    
    If ColStatus <> "OK" Then
        tmpOutFldLst = ""
        tmpOutDatLst = ""
        
        '1.Get Departure Part
        ItmNbr = 0
        Do
            ItmNbr = ItmNbr + 1
            tmpFldNam = GetItem(AftFldLst, ItmNbr, ",")
            If tmpFldNam <> "" Then
                If InStr(("FLDA,ORG3,STOD,DTD1,ACT3,REGN,FTYP,STYP" & MainDialog.UseStypField), tmpFldNam) > 0 Then
                    FldColIdx = Val(GetItem(ExtFldLst, ItmNbr, ",")) + 2
                    If FldColIdx > 2 Then
                        tmpFldVal = FileData.GetColumnValue(FirstLine, FldColIdx)
                        If tmpFldNam = "FTYP" And tmpFldVal = "" Then
                            Select Case tmpFldVal
                                Case "C"
                                    tmpFldVal = "X"
                                Case "N"
                                    tmpFldVal = "N"
                                Case Else
                                    tmpFldVal = "O"
                            End Select
                        End If
                        If tmpFldVal <> "" Then
                            If tmpFldNam = "STOD" Then
                                tmpFldVal = tmpFldVal & "00"
                                tmpStoaDay = Left(tmpFldVal, 8)
                            End If
                            tmpOutFldLst = tmpOutFldLst & tmpFldNam & ","
                            tmpOutDatLst = tmpOutDatLst & tmpFldVal & ","
                        End If
                    End If
                End If
            End If
        Loop While tmpFldNam <> ""
        
        '2.Get Destination Part
        ItmNbr = 0
        Do
            ItmNbr = ItmNbr + 1
            tmpFldNam = GetItem(AftFldLst, ItmNbr, ",")
            If tmpFldNam <> "" Then
                If InStr("DES3,STOA", tmpFldNam) > 0 Then
                    FldColIdx = Val(GetItem(ExtFldLst, ItmNbr, ",")) + 2
                    If FldColIdx > 2 Then
                        tmpFldVal = FileData.GetColumnValue(LastLine, FldColIdx)
                        If tmpFldVal <> "" Then
                            If tmpFldNam = "STOA" Then
                                tmpFldVal = tmpFldVal & "00"
                                tmpStodDay = Left(tmpFldVal, 8)
                            End If
                            tmpOutFldLst = tmpOutFldLst & tmpFldNam & ","
                            tmpOutDatLst = tmpOutDatLst & tmpFldVal & ","
                        End If
                    End If
                End If
            End If
        Loop While tmpFldNam <> ""
      
        tmpOutFldLst = tmpOutFldLst & "VIAL,"
        tmpOutDatLst = tmpOutDatLst & ","
        tmpOutFldLst = Left(tmpOutFldLst, Len(tmpOutFldLst) - 1)
        tmpOutDatLst = Left(tmpOutDatLst, Len(tmpOutDatLst) - 1)
        
        Select Case tmpCmdVal
            Case "I"
                'Extract of aftdata.doc ('flight' documentation)
                'Command ISF: Insert Seasonal Flight
                'Selection bei einem Flug
                '1. Date (YYYYMMDD) Begin of the period
                '2. Date (YYYYMMDD) End of the period
                '3. Operatioanl days (1-7) of the flight
                '4. Weekly frequency (1..2..) of the flight
                tmpActCmd = "ISF"
                Select Case CurAdid
                    Case "A"
                        tmpSqlKey = tmpStoaDay & "," & tmpStoaDay & ",1234567,1"
                    Case "B"
                        tmpSqlKey = tmpStodDay & "," & tmpStodDay & ",1234567,1"
                    Case "D"
                        tmpSqlKey = tmpStodDay & "," & tmpStodDay & ",1234567,1"
                    Case Else
                End Select
            Case "U"
                tmpActCmd = "UFR"
                tmpSqlKey = "WHERE URNO=" & tmpUrno
            Case "D"
                'Actually there is no Delete Statement in Data Import Files
            Case Else
        End Select
        
        tmpTblNam = "AFTTAB"
        
        ActRem = ""
        If tmpActCmd = "UFR" Then
            If doRelease > 1 Then
                RetVal = UfisServer.CallCeda(ResultBuffer, "GFR", tmpTblNam, tmpOutFldLst, " ", tmpSqlKey, "", 0, True, False)
                'MsgBox ResultBuffer
                ActOutFldLst = ""
                ActOutDatLst = ""
                ItmNbr = 0
                Do
                    ItmNbr = ItmNbr + 1
                    tmpFldNam = GetItem(tmpOutFldLst, ItmNbr, ",")
                    If tmpFldNam <> "" Then
                        tmpFldVal = GetItem(tmpOutDatLst, ItmNbr, ",")
                        TblFldVal = GetItem(ResultBuffer, ItmNbr, ",")
                        If tmpFldNam = "VIAL" Then
                            tmpFldVal = CheckViaInformation(FirstLine, LastLine, TblFldVal, CurAdid)
                            'MsgBox "VIAL:" & vbNewLine & "DBS <" & TblFldVal & ">" & vbNewLine & "NEW <" & tmpFldVal & ">"
                        End If
                        If tmpFldVal <> TblFldVal Then
                            ActOutFldLst = ActOutFldLst & tmpFldNam & ","
                            ActOutDatLst = ActOutDatLst & tmpFldVal & ","
                        End If
                    End If
                Loop While tmpFldNam <> ""
                If ActOutFldLst <> "" Then
                    ActOutFldLst = Left(ActOutFldLst, Len(ActOutFldLst) - 1)
                    ActOutDatLst = Left(ActOutDatLst, Len(ActOutDatLst) - 1)
                    If doRelease = 3 Then
                        'MsgBox "FOR UPDATE" & vbNewLine & tmpSqlKey & vbNewLine & ActOutFldLst & vbNewLine & ActOutDatLst
                        If OutputType = "LOCAL_FILE" Then
                            Print #2, "CMD:" & tmpActCmd
                            Print #2, "TBL:" & tmpTblNam
                            Print #2, "KEY:" & tmpSqlKey
                            Print #2, "FLD:" & ActOutFldLst
                            Print #2, "DAT:" & ActOutDatLst
                            Print #2, "----------------------------------------------"
                        End If
                        RetVal = UfisServer.CallCeda(ResultBuffer, tmpActCmd, tmpTblNam, ActOutFldLst, ActOutDatLst, tmpSqlKey, "", 0, False, False)
                        ActRem = "DONE"
                    End If
                Else
                    ActRem = "OK"
                End If
            End If
        End If
        If tmpActCmd = "ISF" Then
            If doRelease = 3 Then
                tmpOutFldLst = "FLNO,FLTN,ALC2," & tmpOutFldLst
                FldColIdx = 1 + 2
                tmpAlc2 = FileData.GetColumnValue(FirstLine, FldColIdx)
                tmpOutDatLst = tmpAlc2 & "," & tmpOutDatLst
                FldColIdx = 2 + 2
                tmpFldVal = FileData.GetColumnValue(FirstLine, FldColIdx)
                tmpOutDatLst = tmpFldVal & "," & tmpOutDatLst 'FLTN
                tmpFlno = Left(tmpAlc2 & Space(3), 3) & tmpFldVal
                tmpOutDatLst = tmpFlno & "," & tmpOutDatLst
                tmpViaList = CheckViaInformation(FirstLine, LastLine, "", CurAdid)
                If tmpViaList <> "" Then
                    tmpOutDatLst = tmpOutDatLst & tmpViaList
                End If
                'MsgBox tmpActCmd & vbNewLine & tmpSqlKey & vbNewLine & tmpOutFldLst & vbNewLine & tmpOutDatLst
                If OutputType = "LOCAL_FILE" Then
                    Print #2, "CMD:" & tmpActCmd
                    Print #2, "TBL:" & tmpTblNam
                    Print #2, "KEY:" & tmpSqlKey
                    Print #2, "FLD:" & tmpOutFldLst
                    Print #2, "DAT:" & tmpOutDatLst
                    Print #2, "----------------------------------------------"
                End If
                RetVal = UfisServer.CallCeda(ResultBuffer, tmpActCmd, tmpTblNam, tmpOutFldLst, tmpOutDatLst, tmpSqlKey, "", 0, False, False)
                ActRem = "DONE"
            End If
        End If
            
        If ActRem <> "" Then
            For CurLine = FirstLine To LastLine
                FileData.SetColumnValue CurLine, 0, ActRem
            Next
            FltCnt = FltCnt - 1
        End If
   
    End If
    WarnCnt.Caption = FltCnt
    WarnCnt.Refresh
End Sub

Private Function CheckViaInformation(FirstLine As Long, LastLine As Long, ActViaList As String, FltAdid As String)
Dim Result As String
Dim CurLine As Long
Dim PrvLine As Long
Dim FldColIdx As Long
Dim tmpVal As String
Dim ViaData As String
Dim NewViaList As String

'Extract of 'Flight' Documentation aftdata.doc
'Field VIAL
'Position  2.Via  Bedeutung   Inhalt
'1,      121,     ... Flughafen wird auf den Anzeigen dargestellt Blank, 1 oder 2
'2-4,    122-124, *** 3-Lettercode des Flughafens                 FRA, MUC, ...
'5-8,    125-128, ... 4-Lettercode des Flughafens                 EDDF, EDDM, ...
'9-22,   129-142, *** Geplante Ankunftzeit am Flughafen (STOA)    19971010120000
'23-36,  143-156, ... Erwartete Ankunftzeit am Flughafen (ETOA)   19971010120000
'37-50,  157-170, ... Landezeit am Flughafen (LAND)               19971010120000
'51-64,  171-184, ... onBlock-Zeit am Flughafen (ONBL)            19971010120000
'65-78,  185-198, *** Geplante Abflugzeit am Flughafen (STOD)     19971010123000
'79-92,  199-212, ... Erwartete Abflugzeit am Flughafen (ETOD)    19971010123000
'93-106, 213-226, ... offBlock-Zeit am Flughafen (OFBL)           19971010123000
'107-120,227-240, ... Startzeit am Flughafen (AIRB)               19971010123000

'the Import Data file contains only the *** marked information
'we must take care not to overwrite already existing data of the other parts
'so it is necessary to read VIAL from the database and mix it with the imported data
'than first we can compare old/new and decide not to update if it is equal
    
    Result = ""
    If LastLine > FirstLine Then
        For CurLine = FirstLine + 1 To LastLine
            FldColIdx = Val(GetFieldValue("DES3", ExtFldLst, AftFldLst)) + 2
            If FldColIdx > 2 Then
                tmpVal = FileData.GetColumnValue(CurLine - 1, FldColIdx)
                ViaData = StripViaFromList(tmpVal, ActViaList)
                'here we have a valid Via record for this airport.
                'the string might be empty, so we patch it
                Mid(ViaData, 2, 3) = tmpVal
            End If
            FldColIdx = Val(GetFieldValue("STOA", ExtFldLst, AftFldLst)) + 2
            If FldColIdx > 2 Then
                tmpVal = FileData.GetColumnValue(CurLine - 1, FldColIdx) & "00"
                Mid(ViaData, 9, 14) = tmpVal
            End If
            FldColIdx = Val(GetFieldValue("STOD", ExtFldLst, AftFldLst)) + 2
            If FldColIdx > 2 Then
                tmpVal = FileData.GetColumnValue(CurLine, FldColIdx) & "00"
                Mid(ViaData, 65, 14) = tmpVal
            End If
            Result = Result & ViaData
        Next
    Else
        'No vias, so return empty VIAL
        Result = ""
    End If
    If (Result <> "") And (UseVialFlag = "YES") Then
        Mid(Result, 1) = "1"
    End If
    CheckViaInformation = RTrim(Result)
End Function
Private Function StripViaFromList(Apc3Code As String, ActViaList As String)
Dim Result As String
Dim ilLen As Integer
Dim i As Integer
    Result = ""
    ilLen = Len(ActViaList)
    For i = 1 To ilLen Step 120
        If Mid(ActViaList, i + 1, 3) = Apc3Code Then
            Result = Mid(ActViaList, i, 120)
            Exit For
        End If
    Next
    Result = Left(Result & Space(120), 120)
    StripViaFromList = Result
End Function

Private Sub chkInline_Click()
    If chkInline.Value = 1 Then
        chkEdit.Value = 0
        FileData.EnableInlineEdit True
        chkInline.BackColor = LightestGreen
    Else
        FileData.EnableInlineEdit False
        chkInline.BackColor = vbButtonFace
    End If
End Sub

Private Sub chkLoad_Click()
    If Not RestoreAction Then
        If Not TotalReset Then
            MySetUp.chkLoad.Value = chkLoad.Value
            If chkLoad.Value = 1 Then
                If chkExpand.Value = 0 Then LoadFileData "(For Load)"
            Else
                ResetMain 0, False
                chkLoad.Enabled = True
                MySetUp.chkLoad.Enabled = True
            End If
        End If
    End If
End Sub

Public Sub ResetMain(SetValue As Integer, SetEnable As Boolean)
    Dim i As Integer
    Dim tmpData As String
    Dim tmpItmDat As String
    If Not RestoreAction Then
        TotalReset = True
        
        InitGrids True
        
        chkAppl(0).BackColor = vbButtonFace
        chkAppl(0).Value = SetValue
        chkAppl(0).Enabled = SetEnable
        
        chkImport.BackColor = vbButtonFace
        chkImport.Value = SetValue
        chkImport.Enabled = SetEnable
        
        chkAction.BackColor = vbButtonFace
        chkAction.Value = SetValue
        chkAction.Enabled = SetEnable
        
        chkExtract.BackColor = vbButtonFace
        chkExtract.Value = SetValue
        chkExtract.Enabled = SetEnable
        
        chkIdentify.BackColor = vbButtonFace
        chkIdentify.Value = SetValue
        chkIdentify.Enabled = SetEnable
        
        chkSort.BackColor = vbButtonFace
        chkSort.Value = SetValue
        chkSort.Enabled = SetEnable
        
        chkShrink.BackColor = vbButtonFace
        chkShrink.Value = SetValue
        chkShrink.Enabled = SetEnable
        
        chkClear.BackColor = vbButtonFace
        chkClear.Value = SetValue
        chkClear.Enabled = SetEnable
        
        chkInline.BackColor = vbButtonFace
        chkInline.Value = SetValue
        chkInline.Enabled = SetEnable
        
        chkEdit.BackColor = vbButtonFace
        chkEdit.Value = SetValue
        chkEdit.Enabled = SetEnable
        
        chkCheck.BackColor = vbButtonFace
        chkCheck.Value = SetValue
        chkCheck.Enabled = SetEnable
        
        chkDontClear.BackColor = vbButtonFace
        chkDontClear = SetValue
        chkDontClear.Enabled = SetEnable
        
        chkExpand.BackColor = vbButtonFace
        chkExpand.Value = SetValue
        chkExpand.Enabled = SetEnable
        
        chkSplit.BackColor = vbButtonFace
        chkSplit.Value = SetValue
        chkSplit.Enabled = True
        'MySetUp.chkSplit.BackColor = vbButtonFace
        'MySetUp.chkSplit.Value = SetValue
        
        chkFilter.BackColor = vbButtonFace
        chkFilter.Value = SetValue
        chkFilter.Enabled = True
        'MySetUp.chkFilter.BackColor = vbButtonFace
        'MySetUp.chkFilter.Value = SetValue
        
        chkLoad.BackColor = vbButtonFace
        chkLoad.Value = SetValue
        chkLoad.Enabled = True
        
        'MySetUp.chkLoad.BackColor = vbButtonFace
        'MySetUp.chkLoad.Value = SetValue
        
        For i = 0 To 3
            ActSeason(i).Text = ""
            ActSeason(i).Tag = ""
            ActSeason(0).ToolTipText = ""
            ActSeason(i).ForeColor = vbBlack
            ActSeason(i).BackColor = NormalGray
        Next
            
        If SetEnable = True Then
            tmpData = GetIniEntry(myIniFullName, myIniSection, "", "AUTO_CLEAR", "NO,0")
            tmpItmDat = GetItem(tmpData, 1, ",")
            If tmpItmDat = "YES" Then
                MySetUp.chkWork(1).Value = 0
                MySetUp.chkWork(1).Value = 1
            Else
                MySetUp.chkWork(1).Value = 1
                MySetUp.chkWork(1).Value = 0
            End If
            If Not UpdateMode Then MySetUp.chkWork(1).Enabled = True
            tmpItmDat = GetItem(tmpData, 2, ",")
            MySetUp.AutoClear.Caption = Trim(Str(Abs(Val(tmpItmDat))))
            tmpData = GetIniEntry(myIniFullName, myIniSection, "", "TIME_OFFSET", "4")
            MySetUp.OpTimeOffset.Caption = Trim(Str(Abs(Val(tmpData))))
        End If
        
        Refresh
        
        TotalReset = False
    End If
End Sub

Private Sub ClickButtonChain(myButton As Integer)
    Dim SetValue As Integer
    If Not RestoreAction Then
        SetValue = 1
        If myButton > 2 Then chkLoad.Value = SetValue: Refresh
        If myButton > 3 Then chkFilter.Value = SetValue: Refresh
        If myButton > 4 Then chkSplit.Value = SetValue: Refresh
        If myButton > 5 Then chkExpand.Value = SetValue: Refresh
        If myButton > 6 Then chkShrink.Value = SetValue: Refresh
        If myButton > 7 Then chkCheck.Value = SetValue: Refresh
        If myButton > 8 Then chkIdentify.Value = SetValue: Refresh
        If myButton > 9 Then chkAction.Value = SetValue: Refresh
        If myButton > 10 Then chkImport.Value = SetValue: Refresh
    End If
End Sub

Private Sub LoadFileData(tmpCaption As String)
    Dim CurFileName As String
    Dim FullFileName As String
    Dim inpLine As String
    Dim wrapLine As String
    Dim SetUpAnsw As String
    Dim SetUpCmd As String
    Dim pclText As String
    Dim FileCount As Integer
    Dim DoIt As Boolean
    Dim NxtFile As Integer
    Dim LoopLinCnt As Long
    Dim MaxLine As Long
    Dim CurLine As Long
    Dim IsFirstLine As Boolean
    Dim StartTime
        FileCount = GetFileAndPath(chkRequest.Tag, myLoadName, myLoadPath)
        MainDialog.MousePointer = 11
        On Error GoTo ErrorHandle
        InitGrids True
        FileData.ShowVertScroller False
        Me.Refresh
        ErrCnt.Caption = ""
        WarnCnt.Caption = ""
        AllLinCnt = 0
        DatLinCnt = 0
        For NxtFile = 1 To FileCount
            CurFileName = GetItem(myLoadName, NxtFile, " ")
            FullFileName = myLoadPath & "\" & CurFileName
            StatusBar.Panels(1).Text = AllLinCnt
            StatusBar.Panels(2).Text = ""
            StatusBar.Panels(7).Text = "Reading file data " & tmpCaption & " - " & CurFileName & " ..."
            StatusBar.Refresh
            MySetUp.DetermineFileType "RESET"
            'StatusBar.Panels(2).Text = DatLinCnt
            MySetUp.MyFileInfo.Caption = CurFileName + " Import File Information"
            MySetUp.CurFileInfo(0).Caption = FullFileName
            MySetUp.Refresh
            If UCase(Right(FullFileName, 4)) = ".DBF" Then
                MySetUp.chkReadDbf.Enabled = True
                DoIt = True
                DbfReader.OpenDbfFile FullFileName, Me, True
                MaxLine = DbfReader.DataTab.GetLineCount - 1
                IsFirstLine = True
                For CurLine = 0 To MaxLine
                    If CurLine Mod 50 = 0 Then
                        DbfReader.DataTab.OnVScrollTo CurLine
                        DbfReader.DataTab.RedrawTab
                        DbfReader.Refresh
                    End If
                    inpLine = DbfReader.DataTab.GetLineValues(CurLine)
                    InsertDataLine inpLine
                    If IsFirstLine Then
                        IsFirstLine = False
                        MySetUp.CurFileInfo(1).Caption = inpLine
                        SetUpAnsw = MySetUp.DetermineFileType("INIT")
                        SetUpCmd = GetItem(SetUpAnsw, 1, ",")
                        MySetUp.Refresh
                        If SetUpCmd <> "OK" Then
                            chkAppl(4).Value = 1
                            pclText = ""
                            pclText = pclText & "File Data Content Check:" & vbNewLine
                            pclText = pclText & MySetUp.CurFileInfo(11).Caption
                            If MyMsgBox.CallAskUser(0, 0, 0, "File Control", pclText, "stop", "", UserAnswer) >= 0 Then DoNothing
                            DoIt = False
                        End If
                    End If
                Next
                DbfReader.Hide
            Else
                MySetUp.chkReadDbf.Enabled = False
                InputBuffer = ""
                Open FullFileName For Input As #1
                    DoIt = True
                    LoopLinCnt = 0
                    IsFirstLine = True
                    While (Not EOF(1)) And (DoIt)
                        Line Input #1, inpLine
                        If InsertDataLine(inpLine) Then LoopLinCnt = LoopLinCnt + 1
                        If IsFirstLine Then
                            IsFirstLine = False
                            MySetUp.CurFileInfo(1).Caption = inpLine
                            SetUpAnsw = MySetUp.DetermineFileType("INIT")
                            SetUpCmd = GetItem(SetUpAnsw, 1, ",")
                            MySetUp.Refresh
                            If SetUpCmd <> "OK" Then
                                chkAppl(4).Value = 1
                                pclText = ""
                                pclText = pclText & "File Data Content Check:" & vbNewLine
                                pclText = pclText & MySetUp.CurFileInfo(11).Caption
                                If MyMsgBox.CallAskUser(0, 0, 0, "File Control", pclText, "stop", "", UserAnswer) >= 0 Then DoNothing
                                DoIt = False
                            End If
                        End If
                        If (FileCount > 1) And (LoopLinCnt = 1) Then FileData.SetColumnValue DatLinCnt - 1, ExtFldCnt, CurFileName
                        If DatLinCnt Mod 50 = 0 Then
                            FileData.OnVScrollTo DatLinCnt - 50
                            FileData.RedrawTab
                            Me.Refresh
                        End If
                        'decreases performance dramatically: so don't use it !!
                        'StatusBar.Panels(1).Text = Str(AllLinCnt)
                    Wend
                Close #1
            End If
            If DoIt Then
                MySetUp.CurFileInfo(2).Caption = inpLine
                MySetUp.Refresh
            End If
            If InputBuffer <> "" Then
                'FileData.InsertBuffer InputBuffer, vbLf
                InputBuffer = ""
            End If
            StatusBar.Panels(2).Text = Str(LoopLinCnt)
            StatusBar.Refresh
            MySetUp.DetermineFileType "INIT"
        Next
        If (UpdateMode) And (DataSystemType = "FHK") Then
            chkSort.Top = chkShrink.Top
            chkSort.Visible = True
            chkShrink.Visible = False
            MySetUp.chkWork(1).Value = 0
        Else
            chkShrink.Visible = True
            chkSort.Visible = False
        End If
        StatusBar.Panels(1).Text = AllLinCnt
        StatusBar.Panels(2).Text = DatLinCnt
        StatusBar.Panels(7).Text = "Ready"
        FileData.OnVScrollTo 0
        FileData.ShowVertScroller True
        FileData.RedrawTab
        Refresh
        If DoIt And chkExpand.Enabled = True And chkExpand.Value = 1 Then
            GetTimeChains
            CheckVpfrVptoFrame 0, -1
            ClearDoubleVias
            ClearFieldsByFieldValue
            PrepareOverNightShrink
            CheckAutoReplace
            ToggleDateFormat True
        End If
        ResetErrorCounter
        Me.Caption = chkImpTyp(PushedImpButton).Caption & " " & MainTitle & " (" & MySetUp.CurFileInfo(5).Caption & " Mode) " & GetItem(MySetUp.MyFileInfo.Caption, 1, " ")
        If DataSystemType = "FREE" Then
            FileData.AutoSizeByHeader = True
            FileData.AutoSizeColumns
        End If
        FileData.RedrawTab
        Refresh
        MainDialog.MousePointer = 0
    Exit Sub
ErrorHandle:
    MainDialog.MousePointer = 0
    pclText = Err.Description & vbNewLine & FullFileName
    Select Case Err.Number   ' Evaluate error number.
        Case 53   ' "File does not exist" error.
            MyMsgBox.InfoApi 0, " " & vbNewLine & " ", "Hint"
            If MyMsgBox.CallAskUser(0, 0, 0, "File Control", pclText, "stop", "", UserAnswer) >= 0 Then DoNothing
            'Exit Sub
        Case 55   ' "File already open" error.
            Close #1   ' Close open file.
            Resume 'try again
        Case Else
            ' Handle other situations here...
        MainDialog.MousePointer = 0
    End Select
    MySetUp.DetermineFileType "INIT"
End Sub
Private Function InsertDataLine(CurLine As String) As Boolean
Dim tmpLine As String
Dim datLine As String
Dim UseLine As String
Dim LineIsValid As Boolean
    UseLine = Replace(CurLine, ",", ";", 1, -1, vbBinaryCompare)
    LineIsValid = CheckValidLines(UseLine)
    If LineIsValid And LineWrap Then
        Line Input #1, tmpLine
        tmpLine = Replace(tmpLine, ",", ";", 1, -1, vbBinaryCompare)
        UseLine = UseLine & tmpLine
    End If
    AllLinCnt = AllLinCnt + 1
    If chkFilter.Value = 1 Then
        If IgnoreEmptyLine Then
            If Trim(UseLine) = "" Then LineIsValid = False
        End If
        If LineIsValid Then
            DatLinCnt = DatLinCnt + 1
            datLine = SplitLineData(UseLine)
            tmpLine = Right("    " & Str(DatLinCnt), 4) & "," & datLine & ",,"
            FileData.InsertTextLine tmpLine, False
            'InputBuffer = InputBuffer & tmpLine & vbLf
        End If
    Else
        DatLinCnt = DatLinCnt + 1
        datLine = SplitLineData(UseLine)
        tmpLine = Right("    " & Str(DatLinCnt), 4) & "," & datLine & ",,"
        FileData.InsertTextLine tmpLine, False
        If LineIsValid = False Then
            FileData.SetLineColor DatLinCnt - 1, vbWhite, vbMagenta
        End If
    End If
    InsertDataLine = LineIsValid
End Function

Private Function SplitLineData(tmpLine As String) As String
Dim Result As String
Dim tmpItem As String
Dim tmpFldVal As String
Dim tmpAddVal As String
Dim tmpData As String
Dim FldNbr As Integer
Dim FldPos As Integer
Dim FldLen As Integer

    If chkSplit.Value = 1 Then
        FldNbr = 0
        ExtFldCnt = 0
        Result = ",,"
        'MsgBox FormatType
        Do
            FldNbr = FldNbr + 1
            ExtFldCnt = ExtFldCnt + 1
            tmpItem = GetItem(CurFldPosLst, FldNbr, ",")
            tmpFldVal = GetCombiValue(FldNbr, tmpLine, tmpItem)
            FldPos = Val(tmpItem)
            FldLen = Val(GetItem(FldLenLst, FldNbr, ","))
            If (FldPos = 0) And (FormatType = "ITEMS") Then
                If CurFldPosLst = "ALL" Then
                    tmpData = GetItem(CurrentHeader, FldNbr, ",")
                    If tmpData <> "" Then FldPos = FldNbr
                End If
            End If
            If (FldPos > 0) Or (tmpItem = "-") Then
                If (tmpFldVal = "") And (tmpItem <> "-") Then
                    If FormatType = "FIXED" Then
                        tmpFldVal = Trim(Mid(tmpLine, FldPos, FldLen))
                    Else
                        tmpFldVal = GetItem(tmpLine, FldPos, ItemSep)
                    End If
                End If
                If (tmpFldVal <> "") Or (tmpItem = "-") Then
                    If chkExpand.Value = 1 Then
                        tmpAddVal = GetItem(FldAddLst, FldNbr, ",")
                        If tmpAddVal <> "" Then Result = Result & Replace(tmpAddVal, "#", " ", 1, -1, vbBinaryCompare)
                        tmpFldVal = CheckDateFieldFormat(FldNbr, tmpFldVal)
                        tmpFldVal = ClearZeroFieldFiller(FldNbr, tmpFldVal)
                        tmpFldVal = ClearFieldValues(FldNbr, tmpFldVal)
                        tmpFldVal = CheckFrequencyFormat(FldNbr, tmpFldVal)
                    End If
                End If
                Result = Result & tmpFldVal & ","
            End If
        Loop While (FldPos > 0) Or (tmpItem = "-")

        ExtFldCnt = ExtFldCnt + 2
    Else
        Result = tmpLine
    End If
    SplitLineData = Result
End Function
Private Function GetCombiValue(tmpFldNbr As Integer, tmpLine As String, tmpList As String) As String
Dim Result As String
Dim FldItm As Integer
Dim ItmNbr As Integer
Dim ItmLen As Integer
Dim ItmPos As Integer
Dim FldFrm As String
Dim ItmDat As String
Dim tmpFlno As String
Dim FltAlc As String
Dim FltNum As String
Dim FltSfx As String
Dim ItmText As String
    Result = ""
    If Left(tmpList, 1) = "#" Then
        If FlnoField > 0 Then
            tmpFlno = GetItem(tmpLine, FlnoField, ItemSep)
            StripAftFlno tmpFlno, FltAlc, FltNum, FltSfx
        End If
        Select Case tmpList
            Case "#FLC"
                Result = FltAlc
            Case "#FLN"
                Result = FltNum
            Case "#FLS"
                Result = FltSfx
            Case "#FLT"
                Result = RTrim(Left(FltNum + "     ", 5) + FltSfx)
            Case Else
        End Select
        tmpList = "-"
    ElseIf InStr(tmpList, "+") > 0 Then
        ItmNbr = 0
        Do
            ItmNbr = ItmNbr + 1
            FldItm = Val(GetItem(tmpList, ItmNbr, "+"))
            If FldItm > 0 Then
                ItmDat = GetItem(tmpLine, FldItm, ItemSep)
                FldFrm = GetItem(FldFrmLst, FldItm, ",")
                ItmDat = CheckFieldFormat(ItmDat, FldFrm)
                Result = Result & ItmDat
            End If
        Loop While FldItm > 0
    ElseIf InStr(tmpList, "L") > 0 Then
        FldItm = Val(GetItem(tmpList, 1, "L"))
        ItmLen = Val(GetItem(tmpList, 2, "L"))
        If (FldItm > 0) And (ItmLen > 0) Then Result = Result & Left(GetItem(tmpLine, FldItm, ItemSep), ItmLen)
    ElseIf InStr(tmpList, "R") > 0 Then
        FldItm = Val(GetItem(tmpList, 1, "R"))
        ItmLen = Val(GetItem(tmpList, 2, "R"))
        If (FldItm > 0) And (ItmLen > 0) Then Result = Result & Right(GetItem(tmpLine, FldItm, ItemSep), ItmLen)
    ElseIf InStr(tmpList, "M") > 0 Then
        FldItm = Val(GetItem(tmpList, 1, "M"))
        ItmPos = Val(GetItem(tmpList, 2, "M"))
        ItmLen = Val(GetItem(tmpList, 2, "/"))
        If (FldItm > 0) And (ItmLen > 0) And (ItmPos > 0) Then Result = Result & Mid(GetItem(tmpLine, FldItm, ItemSep), ItmPos, ItmLen)
    End If
    GetCombiValue = Result
End Function
Private Function CheckFieldFormat(FieldData As String, FieldFormat As String)
    Dim Result As String
    Dim FrmCod As String
    Dim FrmCtl As String
    Dim tmpVal As Integer
    Result = FieldData
    FrmCod = Left(FieldFormat, 1)
    FrmCtl = Mid(FieldFormat, 2)
    Select Case FrmCod
        Case "N"
            'Numerical fixed length leading zeros
            If Trim(FieldData) <> "" Then
                tmpVal = Val(FrmCtl)
                Result = Right(String(tmpVal, "0") & Trim(FieldData), tmpVal)
            End If
        Case Else
    End Select
    'MsgBox FieldData & " / " & FieldFormat & " / " & Result
    CheckFieldFormat = Result
End Function

Private Sub GetTimeChains()
Dim CurLine As Long
Dim MaxLine As Long
Dim tmpStodDay As String
Dim tmpStod As String
Dim tmpStoaDay As String
Dim tmpStoa As String
Dim FldaColIdx As Long
Dim StodColIdx As Long
Dim StoaColIdx As Long
    If TimeChainInfo <> "" Then
        StatusBar.Panels(7).Text = "Formatting Time Fields ..."
        FldaColIdx = Val(GetItem(TimeChainInfo, 1, ",")) + 2
        StodColIdx = Val(GetItem(TimeChainInfo, 2, ",")) + 2
        StoaColIdx = Val(GetItem(TimeChainInfo, 3, ",")) + 2
        If (FldaColIdx > 2) And (StodColIdx > 2) And (StoaColIdx > 2) Then
            MaxLine = FileData.GetLineCount - 1
            For CurLine = 0 To MaxLine
                tmpStodDay = FileData.GetColumnValue(CurLine, FldaColIdx)
                tmpStoaDay = tmpStodDay
                tmpStod = FileData.GetColumnValue(CurLine, StodColIdx)
                tmpStoa = FileData.GetColumnValue(CurLine, StoaColIdx)
                If tmpStoa < tmpStod Then
                    tmpStoaDay = ShiftDate(tmpStoaDay, 1)
                End If
                If tmpStod <> "" Then
                    tmpStod = tmpStodDay & tmpStod
                    FileData.SetColumnValue CurLine, StodColIdx, tmpStod
                End If
                If tmpStoa <> "" Then
                    tmpStoa = tmpStoaDay & tmpStoa
                    FileData.SetColumnValue CurLine, StoaColIdx, tmpStoa
                End If
            Next
            FileData.RedrawTab
            StatusBar.Panels(7).Text = "Ready"
            Refresh
        End If
    End If
End Sub

Private Sub ClearDoubleVias()
Dim i As Integer
Dim CurLine As Long
Dim MaxLine As Long
Dim tmpVia1 As String
Dim tmpVia2 As String
Dim Via1ColIdx As Long
Dim Via2ColIdx As Long
Dim ClrColIdx As Long
    If CheckViaList <> "" Then
        StatusBar.Panels(7).Text = "Clearing Duplicated VIA's ..."
        For i = 0 To 3 Step 3
            Via1ColIdx = Val(GetItem(CheckViaList, i + 1, ",")) + 2
            Via2ColIdx = Val(GetItem(CheckViaList, i + 2, ",")) + 2
            ClrColIdx = Val(GetItem(CheckViaList, i + 3, ",")) + 2
            If (Via1ColIdx > 2) And (Via2ColIdx > 2) And (ClrColIdx > 2) Then
                MaxLine = FileData.GetLineCount - 1
                For CurLine = 0 To MaxLine
                    tmpVia1 = Trim(FileData.GetColumnValue(CurLine, Via1ColIdx))  'Always VIA
                    tmpVia2 = Trim(FileData.GetColumnValue(CurLine, Via2ColIdx))  'ORIG or DEST
                    If tmpVia1 = tmpVia2 Then
                        FileData.SetColumnValue CurLine, ClrColIdx, ""
                    End If
                    If (tmpVia2 = "") And (tmpVia1 <> "") Then
                        FileData.SetColumnValue CurLine, Via2ColIdx, tmpVia1
                        FileData.SetColumnValue CurLine, Via1ColIdx, ""
                    End If
                Next
                FileData.RedrawTab
                Refresh
            End If
        Next
        StatusBar.Panels(7).Text = "Ready"
    End If
End Sub
Private Sub ClearFieldsByFieldValue()
Dim LstItm As Integer
Dim ClrItm As Integer
Dim CurLine As Long
Dim MaxLine As Long
Dim tmpFldVal As String
Dim tmpChkVal As String
Dim ChkColIdx As Long
Dim ClrColIdx As Long
Dim ClrList As String
    If ClearByValue <> "" Then
        StatusBar.Panels(7).Text = "Erasing Unneeded Data ..."
        FileData.RedrawTab
        MaxLine = FileData.GetLineCount - 1
        For CurLine = 0 To MaxLine
            LstItm = 0
            Do
                LstItm = LstItm + 1
                ClrList = GetItem(ClearByValue, LstItm, "|")
                ChkColIdx = Val(GetItem(ClrList, 1, ",")) + 2
                If ChkColIdx > 2 Then
                    tmpFldVal = FileData.GetColumnValue(CurLine, ChkColIdx)
                    tmpChkVal = GetItem(ClrList, 2, ",")
                    If Trim(tmpFldVal) = Trim(tmpChkVal) Then
                        ClrItm = 2
                        Do
                            ClrItm = ClrItm + 1
                            ClrColIdx = Val(GetItem(ClrList, ClrItm, ",")) + 2
                            If ClrColIdx > 2 Then FileData.SetColumnValue CurLine, ClrColIdx, ""
                        Loop While ClrColIdx > 2
                    End If
                End If
            Loop While ChkColIdx > 2
        Next
        FileData.RedrawTab
        StatusBar.Panels(7).Text = "Ready"
        Refresh
    End If
End Sub
Private Function ShiftDate(tmpDay As String, tmpVal As Integer) As String
Dim Result As String
Dim tmpVbDate
    'Result = tmpDay
    tmpVbDate = UfisLib.CedaDateToVb(tmpDay)
    tmpVbDate = DateAdd("d", tmpVal, tmpVbDate)
    Result = Format(tmpVbDate, "yyyymmdd")
    ShiftDate = Result
End Function

Private Function CheckDateFieldFormat(tmpFldNbr As Integer, tmpDate As String) As String
    Dim Result As String
    Dim tmpVal As String
    Dim ItmData As String
    Dim itmChr As String
    Result = tmpDate
    tmpVal = "," & Trim(Str(tmpFldNbr)) & ","
    If InStr(DateFields, tmpVal) > 0 Then
        Select Case DateFieldFormat
            Case "DDMMYYYY"
                Result = Right(tmpDate, 4) & Mid(tmpDate, 3, 2) & Left(tmpDate, 2)
            Case "DDMMMYY", "SSIM2"
                ItmData = tmpDate
                If Len(ItmData) <> 7 Then
                    itmChr = Mid(tmpDate, 2, 1)
                    If (itmChr < "0") Or (itmChr > "9") Then ItmData = "0" & ItmData
                    If Len(ItmData) <> 7 Then
                        If Not UseDefaultSeason Then FindOutDefSeason
                        If UseDefaultSeason Then ItmData = BuildCleanSsimDateFormat(ItmData)
                    End If
                End If
                Result = DecodeSsimDayFormat(ItmData, "SSIM2", "CEDA")
            Case "DD/MM/YY"
                Result = DecodeSlotDayFormat(tmpDate, 1, 2, 3)
            Case "MM/DD/YY"
                Result = DecodeSlotDayFormat(tmpDate, 2, 1, 3)
            Case Else
        End Select
    End If
    If InStr(TimeFields, tmpVal) > 0 Then
        Result = Replace(tmpDate, ":", "", 1, -1, vbBinaryCompare)
    End If
    CheckDateFieldFormat = Result
End Function
Private Function BuildCleanSsimDateFormat(ActDate As String)
    Dim Result As String
    Dim tmpDay As String
    Dim tmpMonth As String
    Dim CurMonth As Integer
    Result = ActDate
    tmpDay = Left(ActDate, 2)
    tmpMonth = Mid(ActDate, 3, 3)
    CurMonth = GetItemNo(MonthList, tmpMonth)
    If (CurMonth >= SeasonFirstMonth) And (CurMonth <= 12) Then
        Result = tmpDay & tmpMonth & Mid(SeasonActYear, 3, 2)
    Else
        Result = tmpDay & tmpMonth & Mid(SeasonNxtYear, 3, 2)
    End If
    BuildCleanSsimDateFormat = Result
End Function
Private Sub FindOutDefSeason()
    DefaultSeasonRec = SeasonData.GetSeasonByName(UCase(Left(myLoadName, 3)))
    If DefaultSeasonRec = "" Then DefaultSeasonRec = SeasonData.GetSeasonByUser(myLoadName)
    If DefaultSeasonRec <> "" Then
        SeasonBegin = GetItem(DefaultSeasonRec, 5, ",")
        SeasonEnd = GetItem(DefaultSeasonRec, 6, ",")
        SeasonActYear = Left(SeasonBegin, 4)
        SeasonNxtYear = Left(SeasonEnd, 4)
        SeasonFirstMonth = Val(Mid(SeasonBegin, 5, 2))
        SeasonLastMonth = Val(Mid(SeasonEnd, 5, 2))
        UseDefaultSeason = True
    End If
End Sub
Private Function DecodeSlotDayFormat(tmpDate As String, DayItm As Integer, MonthItm As Integer, YearItm As Integer) As String
Dim Result As String
Dim tmpDay As String
Dim tmpMonth As String
Dim tmpYear As String
    tmpDay = Trim(GetItem(tmpDate, DayItm, "/"))
    tmpDay = Right("00" & tmpDay, 2)
    tmpMonth = Trim(GetItem(tmpDate, MonthItm, "/"))
    tmpMonth = Right("00" & tmpMonth, 2)
    tmpYear = Trim(GetItem(tmpDate, YearItm, "/"))
    If Len(tmpYear) < 2 Then tmpYear = Right("00" & tmpYear, 2)
    Result = tmpYear & tmpMonth & tmpDay
    If Left(Result, 2) = 99 Then Result = "19" & Result Else Result = "20" & Result
    DecodeSlotDayFormat = Result
End Function

Private Function ClearZeroFieldFiller(tmpFldNbr As Integer, tmpDate As String) As String
Dim Result As String
Dim tmpVal As String
    Result = tmpDate
    tmpVal = "," & Trim(Str(tmpFldNbr)) & ","
    If InStr(ClearZeros, tmpVal) > 0 Then
        If tmpDate = Left("00000000000000000000", Len(tmpDate)) Then Result = ""
    End If
    ClearZeroFieldFiller = Result
End Function
Private Function ClearFieldValues(tmpFldNbr As Integer, tmpValue As String) As String
Dim Result As String
Dim tmpChkFld As String
Dim tmpClrVal As String
Dim tmpClrLst As String
Dim LstItm As Integer
    Result = tmpValue
    LstItm = 0
    tmpChkFld = "," & Trim(Str(tmpFldNbr)) & ","
    If InStr(2, ClearValues, tmpChkFld) > 0 Then
        Do
            LstItm = LstItm + 1
            tmpClrLst = GetItem(ClearValues, LstItm, "|")
            tmpClrVal = GetItem(tmpClrLst, 2, ",")
            If tmpClrVal = tmpValue Then Result = ""
        Loop While tmpClrLst <> ""
    End If
    ClearFieldValues = Result
End Function

Private Function CheckFrequencyFormat(tmpFldNbr As Integer, tmpFreq As String) As String
Dim Result As String
Dim tmpPos As Integer
Dim tmpChr As String
Dim tmpVal As String
Dim i As Integer
Dim l As Integer
    Result = tmpFreq
    tmpVal = "," & Trim(Str(tmpFldNbr)) & ","
    If InStr(MainChkFred, tmpVal) > 0 Then
        Result = "......."
        l = Len(tmpFreq)
        For i = 1 To l
            tmpChr = Mid(tmpFreq, i, 1)
            tmpPos = Val(tmpChr)
            If tmpPos > 0 Then Mid(Result, tmpPos, 1) = tmpChr
        Next
    End If
    CheckFrequencyFormat = Result
End Function

Private Function CheckValidLines(tmpLine As String) As Boolean
Dim tmpData As String
    tmpData = "..."
    CheckValidLines = True
    If LineFilterPos > 0 Then
        If FormatType = "FIXED" Then
            tmpData = Mid(tmpLine, LineFilterPos, LineFilterLen)
        ElseIf FormatType = "ITEMS" Then
            tmpData = GetItem(tmpLine, LineFilterPos, ItemSep)
        End If
    End If
    Select Case LineFilterTyp
        Case "TEXT"
            If tmpData <> LineFilterText Then CheckValidLines = False
        Case "TIME"
            CheckValidLines = CheckValidTime(tmpData, "2400")
        Case "NUMBER"
            CheckValidLines = IsNumeric(tmpData)
        Case "MONTH3LC"
            If tmpData = "" Then tmpData = "..."
            If InStr(MonthList, tmpData) = 0 Then CheckValidLines = False
        Case "NOT"
            If tmpData = LineFilterText Then CheckValidLines = False
        Case Else
    End Select
End Function

Private Sub chkImpTyp_Click(Index As Integer)
    Dim tmpData As String
    Dim tmpItmDat As String
    Dim tmpCurDate As String
    If chkImpTyp(Index).Value = 1 Then
        If Not RestoreAction Then MySetUp.DetermineFileType "RESET"
        If (PushedImpButton >= 0) And (PushedImpButton <> Index) Then chkImpTyp(PushedImpButton).Value = 0
        PushedImpButton = Index
        ResetTimeFields
        ResetChkImpTyp Index
        If (Initializing) Or (Not RestoreAction) Then
            TabFrame.Enabled = False
            ResetMain 0, False
        End If
        Unload FlightDetails
        Unload RecFilter
        Unload FlightExtract
        myIniSection = chkImpTyp(Index).Tag
        DataSystemType = UCase(GetIniEntry(myIniFullName, myIniSection, "", "SYSTEM_TYPE", ""))
        UseStypField = UCase(GetIniEntry(myIniFullName, "MAIN", "", "SERVICE_TYPE", "STYP"))
        WinZipPath = GetIniEntry(myIniFullName, "MAIN", "", "WINZIP", "")
        myLoadPath = GetIniEntry(myIniFullName, myIniSection, "", "FILE_PATH", "")
        myLoadName = GetIniEntry(myIniFullName, myIniSection, "", "FILE_NAME", "")
        If Not RestoreAction Then
            chkRequest.Tag = myLoadPath & "\" & myLoadName
            chkRequest.ToolTipText = chkRequest.Tag
        End If
        If DataSystemType = "FREE" Then
            Load CfgTool
            'CfgTool.Show
            'CfgTool.Refresh
            Screen.MousePointer = 11
            tmpData = GetIniEntry(myIniFullName, myIniSection, "", "CONFIG_FILE", "???????")
            CfgTool.InitFreeConfig tmpData
            InitFreeConfigData
            Screen.MousePointer = 0
        Else
            CurFileFilter = GetIniEntry(myIniFullName, myIniSection, "", "FILE_FILTER", "ASCII Files|*.txt|Excel Export|*.csv|All|*.*")
            tmpItmDat = GetIniEntry(myIniFullName, myIniSection, "", "FILTER_INDEX", "1,1")
            tmpCurDate = Format(Now, "yyyymmdd")
            tmpData = SeasonData.GetValidSeason(tmpCurDate)
            CurFilterIndex = 0
            If Left(tmpData, 1) = "W" Then CurFilterIndex = Val(GetItem(tmpItmDat, 1, ","))
            If Left(tmpData, 1) = "S" Then CurFilterIndex = Val(GetItem(tmpItmDat, 2, ","))
            If CurFilterIndex < 1 Then CurFilterIndex = 1
            CurMultiFiles = GetIniEntry(myIniFullName, myIniSection, "", "MULTI_FILES", "")
            tmpData = GetIniEntry(myIniFullName, myIniSection, "", "FORMAT_TITLE", MainTitle)
            If (Not RestoreAction) And (tmpData <> "") Then MainTitle = tmpData
            FormatType = GetIniEntry(myIniFullName, myIniSection, "", "FORMAT_TYPE", "FIXED")
            ItemSep = GetItem(FormatType, 2, ",")
            If ItemSep = "SEMICOLON" Then ItemSep = ";" 'we don't use commas inside this appl
            If ItemSep = "COMMA" Then ItemSep = ";" 'we don't use commas inside this appl
            FormatType = GetItem(FormatType, 1, ",")
            If Not RestoreAction Then Me.Caption = chkImpTyp(Index).Caption & " " & MainTitle
            NormalHeader = GetIniEntry(myIniFullName, myIniSection, "", "HEAD_LINE", "")
            ExpandHeader = GetIniEntry(myIniFullName, myIniSection, "", "FINAL_HEAD", NormalHeader)
            FldPosLst = GetIniEntry(myIniFullName, myIniSection, "", "FIELD_POS", "ALL")
            ExpPosLst = GetIniEntry(myIniFullName, myIniSection, "", "FINAL_POS", FldPosLst)
            FldLenLst = GetIniEntry(myIniFullName, myIniSection, "", "FIELD_LEN", "")
            FldWidLst = GetIniEntry(myIniFullName, myIniSection, "", "FINAL_LEN", FldLenLst)
            FldFrmLst = GetIniEntry(myIniFullName, myIniSection, "", "FIELD_FRM", "")
            FldAddLst = GetIniEntry(myIniFullName, myIniSection, "", "FIELD_ADD", "")
            FlnoField = Val(GetIniEntry(myIniFullName, myIniSection, "", "FLNO_FIELD", "0"))
            tmpData = GetIniEntry(myIniFullName, myIniSection, "", "LINE_FILTER", "0,0,,NONE")
            LineFilterTyp = GetItem(tmpData, 1, ",")
            LineFilterPos = Val(GetItem(tmpData, 2, ","))
            LineFilterLen = Val(GetItem(tmpData, 3, ","))
            LineFilterText = GetItem(tmpData, 4, ",")
            tmpData = GetIniEntry(myIniFullName, myIniSection, "", "LINE_WRAP", "")
            If tmpData = "YES" Then LineWrap = True Else LineWrap = False
            ChkBasDat = GetIniEntry(myIniFullName, myIniSection, "", "CHECK_BAS", "")
            AutoCheck = GetIniEntry(myIniFullName, myIniSection, "", "AUTO_CHECK", "")
            AutoReplaceAll = GetIniEntry(myIniFullName, myIniSection, "", "REPLACE_ALL", "")
            tmpData = GetIniEntry(myIniFullName, myIniSection, "", "CHECK_LEG", "0,0,0")
            LegColNbr = Val(GetItem(tmpData, 1, ",")) + 2
            OrgColNbr = Val(GetItem(tmpData, 2, ",")) + 2
            DesColNbr = Val(GetItem(tmpData, 3, ",")) + 2
            DateFields = "," & GetIniEntry(myIniFullName, myIniSection, "", "DATE_FIELDS", "") & ","
            DateFields = Replace(DateFields, " ", "", 1, -1, vbBinaryCompare)
            DateFieldFormat = GetIniEntry(myIniFullName, myIniSection, "", "DATE_FORMAT", "")
            TimeFields = "," & GetIniEntry(myIniFullName, myIniSection, "", "TIME_FIELDS", "") & ","
            TimeFields = Replace(TimeFields, " ", "", 1, -1, vbBinaryCompare)
            ClearZeros = "," & GetIniEntry(myIniFullName, myIniSection, "", "CLEAR_ZEROS", "") & ","
            ClearZeros = Replace(ClearZeros, " ", "", 1, -1, vbBinaryCompare)
            ClearValues = "," & GetIniEntry(myIniFullName, myIniSection, "", "CLEAR_VALUE", "") & ","
            ClearValues = Replace(ClearValues, " ", "", 1, -1, vbBinaryCompare)
            ClearValues = Replace(ClearValues, "|", ",|,", 1, -1, vbBinaryCompare)
            ClearByValue = GetIniEntry(myIniFullName, myIniSection, "", "CLEAR_BYVAL", "")
            ClearByValue = Replace(ClearByValue, " ", "", 1, -1, vbBinaryCompare)
            TimeChainInfo = GetIniEntry(myIniFullName, myIniSection, "", "TIME_CHAIN", "")
            MainPeriodCols = GetIniEntry(myIniFullName, myIniSection, "", "TIME_PERIOD", "0,0,0,0")
            InitPeriodCheck MainPeriodCols, True
            CheckViaList = GetIniEntry(myIniFullName, myIniSection, "", "CHECK_VIAS", "")
            IgnoreEmpty = GetIniEntry(myIniFullName, myIniSection, "", "IGNORE_EMP", "")
            ShrinkList = GetIniEntry(myIniFullName, myIniSection, "", "LINE_SHRINK", "")
            AftFldLst = GetIniEntry(myIniFullName, myIniSection, "", "AFT_FIELDS", "")
            ExtFldLst = GetIniEntry(myIniFullName, myIniSection, "", "EXT_FIELDS", "")
            DefaultFtyp = GetIniEntry(myIniFullName, myIniSection, "", "DEFAULT_FTYP", "S")
            UseVialFlag = GetIniEntry(myIniFullName, myIniSection, "", "SET_VIA_FLAG", "NO")
            OutputType = GetIniEntry(myIniFullName, myIniSection, "", "OUTPUT_TYPE", "LOCAL_FILE")
            ActionType = GetIniEntry(myIniFullName, myIniSection, "", "ACTION_TYPE", "CEDA_IMP")
            CheckRotation = GetIniEntry(myIniFullName, myIniSection, "", "ROTATIONS", "")
            tmpData = GetIniEntry(myIniFullName, myIniSection, "", "IGNORE_EMPTY", "")
            If tmpData = "YES" Then IgnoreEmptyLine = True Else IgnoreEmptyLine = False
            tmpData = GetIniEntry(myIniFullName, myIniSection, "", "COL_SELECT", "")
            If tmpData = "YES" Then SetColSelection = True Else SetColSelection = False
            ColSelectList = GetIniEntry(myIniFullName, myIniSection, "", "SELECT_COL", "")
            If ColSelectList <> "" Then SetColSelection = True
            ExtractRotationArr = GetIniEntry(myIniFullName, myIniSection, "", "EXTRACT_RFA", "")
            ExtractRotationDep = GetIniEntry(myIniFullName, myIniSection, "", "EXTRACT_RFD", "")
            ExtractArrivals = GetIniEntry(myIniFullName, myIniSection, "", "EXTRACT_ARR", "")
            ExtractDepartures = GetIniEntry(myIniFullName, myIniSection, "", "EXTRACT_DEP", "")
            ExtractSlotLine = GetIniEntry(myIniFullName, myIniSection, "", "EXTRACT_DAT", "")
            If ExtractSlotLine <> "" Then ExtractSystemType = 1
            If ExtractArrivals <> "" Then ExtractSystemType = 2
            If ExtractRotationArr <> "" Then ExtractSystemType = 3
        End If
        If Not RestoreAction Then
            tmpData = GetIniEntry(myIniFullName, myIniSection, "", "AUTO_OPEN", "NO")
            If tmpData = "NO" Then
                chkRequest.Enabled = True
                chkRequest.Value = 1
            Else
                ResetMain 0, True
                chkRequest.Enabled = False
                chkExpand.Value = 1
            End If
            chkBackup.Enabled = True
            chkImpTyp(Index).BackColor = LightestGreen
        End If
    Else
        PushedImpButton = -1
        Me.Caption = MainTitle
        chkLoad.Value = 0
        chkBackup.Enabled = False
        chkImpTyp(Index).BackColor = vbButtonFace
    End If
    Me.Refresh
End Sub
Private Sub ResetChkImpTyp(WithOut As Integer)
Dim i As Integer
Dim n As Integer
    n = chkImpTyp.UBound
    For i = 0 To n
        If i <> WithOut Then chkImpTyp(i).Value = 0
    Next
End Sub

Private Sub chkReplace_Click()
Dim tmpLine
Dim tmpCol
Dim RefCol As Long
Dim RefTag As String
Dim RefTxt As String
Dim CodeTxt As String
Dim ColList As String
Dim DataList As String
Dim tmpColLst As String
Dim clLineIdx As String
Dim llLine As Long
Dim LinItm As Integer
Dim tmpData As String
Dim tmpOldData As String
Dim tmpLval As String
Dim tmpRval As String
Dim ReqMsg As String
Dim UsrAnsw As Integer
Dim RefAnsw As Integer
Dim RetVal As Integer
Dim CedaData As String
Dim tmpResult As String
Dim tmpSqlKey As String
Dim RefContext As String

    If chkReplace.Value = 1 Then
        chkReplace.BackColor = LightestGreen
        chkReplace.Refresh
        Me.Refresh
        MainDialog.MousePointer = 11
        'MsgBox chkEdit.Tag
        tmpLine = GetItem(chkEdit.Tag, 1, ",")
        tmpCol = GetItem(chkEdit.Tag, 2, ",")
        If tmpLine <> "" And tmpCol <> "" And Trim(txtEdit.Text) <> Trim(txtEdit.Tag) Then
            llLine = Val(tmpLine)
            FileData.SetColumnValue llLine, tmpCol, Trim(txtEdit.Text)
            ClearErrorStatus llLine, llLine
            PerformChecks llLine, llLine
            If chkAll.Value = 1 Then
                RefTag = chkAll.Tag
                CodeTxt = GetItem(RefTag, 1, "/")
                RefAnsw = 0
                If InStr(RefTag, "/") > 0 Then
                    RefCol = Val(GetItem(RefTag, 2, "/")) + 2
                    ColList = Trim(Str(RefCol)) & "," & Trim(Str(tmpCol))
                    RefTxt = Trim(FileData.GetColumnValue(tmpLine, RefCol))
                    DataList = RefTxt & "," & txtEdit.Tag
                    RefContext = GetItem(RefTag, 3, "/")
                    If RefContext = "" Then RefContext = "Airline"
                    ReqMsg = ""
                    ReqMsg = ReqMsg & "Do you want to perform this " & vbNewLine
                    ReqMsg = ReqMsg & "global change based on" & vbNewLine & vbNewLine
                    ReqMsg = ReqMsg & RefContext & " " & RefTxt & " only ?" & vbNewLine
                    RefAnsw = MyMsgBox.CallAskUser(0, 0, 0, "Perform Auto Replace All", ReqMsg, "ask", "Yes;F,No", UserAnswer)
                End If
                If RefAnsw = 1 Then
                    tmpColLst = FileData.GetLinesByMultipleColumnValue(ColList, DataList, 0)
                Else
                    tmpColLst = FileData.GetLinesByColumnValue(tmpCol, txtEdit.Tag, 0)
                End If
                'MsgBox tmpColLst
                clLineIdx = "START"
                LinItm = 0
                While clLineIdx <> ""
                    LinItm = LinItm + 1
                    clLineIdx = GetItem(tmpColLst, LinItm, ",")
                    If clLineIdx <> "" Then
                        llLine = Val(clLineIdx)
                        FileData.SetColumnValue llLine, tmpCol, Trim(txtEdit.Text)
                        'ClearErrorStatus llLine, llLine
                        'PerformChecks llLine, llLine
                    End If
                Wend
                ReqMsg = ""
                ReqMsg = ReqMsg & "Do you want to make this " & vbNewLine
                ReqMsg = ReqMsg & "global change permanent ?" & vbNewLine & vbNewLine
                ReqMsg = ReqMsg & "TYPE:" & vbTab & DataSystemType & vbNewLine
                ReqMsg = ReqMsg & "CODE:" & vbTab & CodeTxt & vbNewLine & vbNewLine
                ReqMsg = ReqMsg & "TEXT:" & vbTab & txtEdit.Tag & vbNewLine
                ReqMsg = ReqMsg & "REPL:" & vbTab & txtEdit.Text & vbNewLine
                If RefAnsw = 1 Then ReqMsg = ReqMsg & vbNewLine & "(" & RefContext & " " & RefTxt & " only.)" & vbNewLine
                If MyMsgBox.CallAskUser(0, 0, 0, "Check Auto Replace All", ReqMsg, "ask", "Yes;F,No", UserAnswer) = 1 Then
                    If RefAnsw = 1 Then
                        tmpLval = Trim(txtEdit.Tag) & "/" & RefTxt
                        tmpRval = Trim(txtEdit.Text) & "/" & RefTxt
                    Else
                        tmpLval = Trim(txtEdit.Tag)
                        tmpRval = Trim(txtEdit.Text)
                    End If
                    tmpSqlKey = "WHERE TYPE='" & DataSystemType & "' AND LKEY='" & CodeTxt & "' AND (LVAL='" & tmpLval & "' OR RVAL='" & tmpRval & "')"
                    RetVal = UfisServer.CallCeda(tmpResult, "DRT", "GDLTAB", "", "", tmpSqlKey, "", 1, False, False)
                    tmpSqlKey = "            "
                    CedaData = ""
                    CedaData = CedaData & DataSystemType & "," & CodeTxt
                    CedaData = CedaData & "," & tmpLval & "," & tmpRval
                    RetVal = UfisServer.CallCeda(tmpResult, "IBT", "GDLTAB", "TYPE,LKEY,LVAL,RVAL", CedaData, tmpSqlKey, "", 1, False, False)
                    If MyMsgBox.CallAskUser(0, 0, 0, "Check Auto Replace All", "Made permanent!", "hand", "", UserAnswer) = 1 Then DoNothing
                End If
            End If
            tmpOldData = "'" & txtEdit.Tag & "'"
            txtEdit.Tag = Trim(txtEdit.Text)
            txtEdit.BackColor = vbWhite
            tmpData = "'" & txtEdit.Tag & "'"
            chkAll.ToolTipText = Replace(chkAll.ToolTipText, tmpOldData, tmpData, 1, 1, vbBinaryCompare)
            PerformIdentify
        End If
        FileData.RedrawTab
        MainDialog.MousePointer = 0
        chkAll.Value = 0
        chkReplace.Value = 0
    Else
        chkReplace.BackColor = vbButtonFace
    End If
End Sub

Private Sub chkRequest_Click()
    Dim ListOfFiles As String
    Dim ActFile As String
    Dim ActPath As String
    Dim tmpAnsw As Integer
    Dim SelFilterItem As Integer
    Dim SelFilterIndex As Integer
    Dim SelFilterText As String
    Dim FileCount As Integer
    If (Not Initializing) And (Not RestoreAction) Then
        If chkRequest.Value = 1 Then
            TabFrame.Enabled = False
            chkRequest.BackColor = LightestGreen
            If chkRequest.Tag = "" Then chkRequest.Tag = CurLoadPath & "\" & CurLoadName
            FileCount = GetFileAndPath(chkRequest.Tag, myLoadName, myLoadPath)
            UfisTools.CommonDialog.DialogTitle = "Open import file of: " & DataSystemType
            UfisTools.CommonDialog.InitDir = myLoadPath
            UfisTools.CommonDialog.FileName = myLoadName
            UfisTools.CommonDialog.Filter = CurFileFilter
            UfisTools.CommonDialog.FilterIndex = CurFilterIndex
            If InStr(CurMultiFiles, "Y") > 0 Then
                UfisTools.CommonDialog.Flags = (cdlOFNAllowMultiselect Or cdlOFNHideReadOnly)
            Else
                UfisTools.CommonDialog.Flags = (cdlOFNHideReadOnly)
            End If
            UfisTools.CommonDialog.CancelError = True
            On Error GoTo ErrorHandle
            UfisTools.CommonDialog.ShowOpen
            If UfisTools.CommonDialog.FileName <> "" Then
                ListOfFiles = Replace(UfisTools.CommonDialog.FileName, Chr(34), "", 1, -1, vbBinaryCompare)
                SelFilterIndex = UfisTools.CommonDialog.FilterIndex
                SelFilterItem = ((SelFilterIndex - 1) * 2) + 1
                SelFilterText = GetItem(CurFileFilter, SelFilterItem, "|")
                tmpAnsw = 1
                If (ListOfFiles = chkRequest.Tag) And (chkExpand.Value = 1) Then
                    tmpAnsw = MyMsgBox.CallAskUser(0, 0, 0, "Import File Control", "Do you want to reopen the file?", "ask", "Yes,No;F", UserAnswer)
                End If
                FileCount = GetFileAndPath(ListOfFiles, ActFile, ActPath)
                If FileCount > 1 Then
                    If GetItem(CurMultiFiles, SelFilterIndex, ",") <> "Y" Then
                        tmpAnsw = MyMsgBox.CallAskUser(0, 0, 0, (SelFilterText & " File Control"), "Too many files! Please select one file only!", "stop", "", UserAnswer)
                        tmpAnsw = -1
                    End If
                End If
                If tmpAnsw = 1 Then
                    chkRequest.Tag = ListOfFiles
                    chkRequest.ToolTipText = chkRequest.Tag
                    If UCase(GetItem(ActFile, 2, ".")) = "ZIP" Then
                        If WinZipPath <> "" Then
                            chkRequest.Value = 0
                            WinZipId = Shell(WinZipPath & " " & ListOfFiles, vbNormalFocus)
                            AppActivate "WinZip", True
                        End If
                    Else
                        chkRequest.Value = 0
                        TabFrame.Enabled = True
                        Refresh
                        ResetMain 0, False
                        ResetMain 0, True
                        If Not TestMode Then
                            chkExpand.Value = 1
                        Else
                            MySetUp.Show , Me
                            chkLoad.Value = 1
                        End If
                    End If
                Else
                    chkRequest.Value = 0
                    chkRequest.Tag = ""
                    chkRequest.ToolTipText = "No file selected."
                End If
            End If
        Else
            chkRequest.BackColor = vbButtonFace
        End If
    End If
    Exit Sub
ErrorHandle:
    myLoadName = ""
    myLoadPath = ""
    chkRequest.Value = 0
    Exit Sub
End Sub

Private Sub chkShrink_Click()
    If Not RestoreAction Then
        If chkShrink.Value = 1 Then
            ClickButtonChain 6
            If (UpdateMode) And (DataSystemType = "FHK") Then
                chkSort.Value = 1
            Else
                ShrinkOverNightLines
            End If
            ResetErrorCounter
            chkShrink.BackColor = LightestGreen
            chkShrink.Enabled = False
        Else
            chkShrink.Enabled = True
            chkShrink.BackColor = vbButtonFace
        End If
        If TabFrame.Enabled = True Then FileData.SetFocus
    End If
End Sub
Private Sub PrepareOverNightShrink()
Dim ShrinkRecsFound
Dim CurLine As Long
Dim MaxLine As Long
Dim ArrLine As Long
Dim DepLine As Long
'Dim tmpline As Long
Dim StoaColIdx As Long
Dim StodColIdx As Long
Dim DaofColIdx As Long
Dim FltnArrCol As Long
Dim FltnDepCol As Long
Dim DaofVal As Integer
Dim GroundDays As Integer
Dim tmpDaof As String
Dim tmpStoa As String
Dim tmpStod As String
Dim CurFlna As String
Dim CurFlnd As String
Dim tmpFlna As String
Dim tmpFlnd As String
Dim tmpData As String
Dim varDepDay As Variant
Dim strDepDay As String
Dim tmpColStat As String
    ShrinkRecsFound = False
    chkShrink.Enabled = ShrinkRecsFound
    chkShrink.Value = 0
    FileData.RedrawTab
    If ShrinkList <> "" Then
        StatusBar.Panels(7).Text = "Searching Overnight Rotations ..."
        'hardcoded
        FltnArrCol = 2 + 2
        FltnDepCol = 4 + 2
        StoaColIdx = 12 + 2
        StodColIdx = 13 + 2
        DaofColIdx = 14 + 2
        GroundDays = -1
        DepLine = -1
        MaxLine = FileData.GetLineCount - 1
        For CurLine = 0 To MaxLine
            tmpColStat = FileData.GetColumnValue(CurLine, 1)
            'If tmpColStat <> "E" Then
                tmpDaof = FileData.GetColumnValue(CurLine, DaofColIdx)
                DaofVal = Val(tmpDaof)
                tmpStoa = GetTimePart(FileData.GetColumnValue(CurLine, StoaColIdx))
                tmpStod = GetTimePart(FileData.GetColumnValue(CurLine, StodColIdx))
                If DaofVal > 0 Then
                    'found a new nightstop arrival
                    If GroundDays >= 0 Then
                        'can't be. Here we have an error
                        SetErrorRange DaofColIdx, ArrLine, CurLine - 1, "OVN?.ALL"
                    End If
                    GroundDays = DaofVal
                    tmpData = Left(FileData.GetColumnValue(CurLine, StoaColIdx), 8)
                    varDepDay = CedaDateToVb(tmpData)
                    varDepDay = DateAdd("d", DaofVal, varDepDay)
                    strDepDay = Format(varDepDay, "yyyymmdd")
                    CurFlna = FileData.GetColumnValue(CurLine, FltnArrCol)
                    CurFlnd = FileData.GetColumnValue(CurLine, FltnDepCol)
                    DepLine = -1
                Else
                    'Here only special values are allowed
                    If Trim(tmpDaof) <> "" Then
                        If (tmpDaof = "X") And (DepLine >= 0) Then
                            SetErrorText Str(DaofColIdx), CurLine, ExtFldCnt, "OVN?.ARR", True, 0
                        End If
                    End If
                End If
                If (GroundDays = DaofVal) And (GroundDays > 0) Then
                    'this is the arrival record
                    FileData.SetColumnValue CurLine, 1, "A"
                    FileData.SetColumnValue CurLine, 2, "N"
                    'FileData.SetColumnValue CurLine, StodColIdx, ""
                    FileData.SetLineColor CurLine, LightYellow, vbBlack
                    ArrLine = CurLine
                    ShrinkRecsFound = True
                ElseIf GroundDays > 0 Then
                    'these records will be ignored
                    tmpFlna = FileData.GetColumnValue(CurLine, FltnArrCol)
                    tmpFlnd = FileData.GetColumnValue(CurLine, FltnDepCol)
                    If (tmpFlna = CurFlna) And (tmpFlnd = CurFlnd) Then
                        FileData.SetColumnValue CurLine, 1, "-"
                        FileData.SetColumnValue CurLine, 2, "N"
                        FileData.SetLineColor CurLine, vbBlack, LightGray
                    Else
                        SetErrorRange DaofColIdx, ArrLine, CurLine - 1, "OVN?.NXT"
                    End If
                ElseIf GroundDays = 0 Then
                    'must be nightstop departure record
                    tmpFlna = FileData.GetColumnValue(CurLine, FltnArrCol)
                    tmpFlnd = FileData.GetColumnValue(CurLine, FltnDepCol)
                    If (tmpFlna = CurFlna) And (tmpFlnd = CurFlnd) Then
                        FileData.SetColumnValue CurLine, 1, "D"
                        FileData.SetColumnValue CurLine, 2, "N"
                        tmpData = Left(FileData.GetColumnValue(CurLine, StodColIdx), 8)
                        If tmpData = strDepDay Then
                            FileData.SetLineColor CurLine, LightGreen, vbBlack
                        Else
                            tmpData = strDepDay & tmpStod
                            FileData.SetColumnValue CurLine, StodColIdx, tmpData
                            SetErrorText "7", CurLine, ExtFldCnt, "P.BEGIN", False, 1
                        End If
                        DepLine = CurLine
                        ShrinkRecsFound = True
                    Else
                        SetErrorRange DaofColIdx, ArrLine, CurLine - 1, "OVN?.DEP"
                    End If
                End If
            'End If
            GroundDays = GroundDays - 1
        Next
        FileData.RedrawTab
        StatusBar.Panels(7).Text = "Ready"
        Refresh
    End If
    chkShrink.Enabled = ShrinkRecsFound
End Sub
Private Function GetTimePart(tmpDate As String) As String
    Dim Result As String
    Result = Right(tmpDate, 4)
    GetTimePart = Result
End Function
Private Sub ShrinkOverNightLines()
Dim CurLine As Long
Dim MaxLine As Long
Dim Col1Val As String
Dim Col2Val As String
Dim ArrLine As Long
Dim ColNbr As Long
Dim ItmNbr As Integer
Dim ItmVal As String
Dim DelLine As Boolean
Dim ForeColor As Long
Dim BackColor As Long
    If ShrinkList <> "" Then
        StatusBar.Panels(2).Text = ""
        StatusBar.Panels(7).Text = "Shrinking Overnight Rotations ..."
        Refresh
        MousePointer = 11
        MaxLine = FileData.GetLineCount
        CurLine = 0
        ArrLine = -1
        Do
            Col1Val = FileData.GetColumnValue(CurLine, 1)
            Col2Val = FileData.GetColumnValue(CurLine, 2)
            DelLine = False
            If (Col1Val = "-") And (Col2Val = "-") Then
                DelLine = True
            ElseIf (Col1Val = "-") And (Col2Val = "N") Then
                DelLine = True
            ElseIf (Col1Val = "A") And (Col2Val = "N") Then
                If ArrLine >= 0 Then
                    'missing departure
                    SetErrorText "", ArrLine, ExtFldCnt, "DEP?", False, 0
                End If
                ArrLine = CurLine
            ElseIf (Col1Val = "D") And (Col2Val = "N") Then
                If ArrLine >= 0 Then
                    ItmNbr = 0
                    Do
                        ItmNbr = ItmNbr + 1
                        ColNbr = Val(GetItem(ShrinkList, ItmNbr, ",")) + 2
                        If ColNbr > 2 Then
                            ItmVal = FileData.GetColumnValue(CurLine, ColNbr)
                            FileData.SetColumnValue ArrLine, ColNbr, ItmVal
                        End If
                    Loop While ColNbr > 2
                    FileData.SetColumnValue ArrLine, 1, ""
                    FileData.SetColumnValue ArrLine, 2, ""
                    FileData.SetLineColor ArrLine, vbBlack, LightGray
                    DelLine = True
                    ArrLine = -1
                Else
                    'missing arrival
                    SetErrorText "", ArrLine, ExtFldCnt, "ARR?", False, 0
                End If
            End If
            If DelLine Then
                FileData.GetLineColor CurLine, ForeColor, BackColor
                'If BackColor = LightestRed Then ResetErrorCounter
                FileData.DeleteLine CurLine
                CurLine = CurLine - 1
                MaxLine = MaxLine - 1
                DelLine = False
            End If
            CurLine = CurLine + 1
        Loop While CurLine < MaxLine
        MaxLine = FileData.GetLineCount
        StatusBar.Panels(2).Text = Trim(Str(MaxLine))
        StatusBar.Panels(7).Text = "Ready"
        FileData.RedrawTab
        Me.Refresh
        MousePointer = 0
    End If
End Sub

Private Sub chkSort_Click()
    'For FHK Update Format only
    If Not RestoreAction Then
        If (UpdateMode) And (DataSystemType = "FHK") Then
            Screen.MousePointer = 11
            If chkSort.Value = 1 Then
                chkSort.BackColor = LightestGreen
                FileData.Sort "10", True, True
                FileData.Sort "9", True, True
                FileData.Sort "7", True, True
                FileData.Sort "6", True, True
                FileData.Sort "5", True, True
            Else
                FileData.Sort "0", True, True
                chkSort.BackColor = vbButtonFace
            End If
            FileData.RedrawTab
            If TabFrame.Enabled = True Then FileData.SetFocus
            Screen.MousePointer = 0
        End If
    End If
End Sub

Private Sub chkSpare_Click()
    If chkSpare.Value = 1 Then
        chkSpare.BackColor = LightestGreen
        DontPlay
    Else
        chkSpare.BackColor = vbButtonFace
    End If
End Sub

Private Sub chkSplit_Click()
    If Not RestoreAction Then
        If Not TotalReset Then
            MySetUp.chkSplit.Value = chkSplit.Value
            If chkSplit.Value = 1 Then
                CurrentHeader = NormalHeader
                CurFldPosLst = FldPosLst
                If chkFilter.Value = 0 Then
                    chkFilter.Value = 1
                Else
                    If Not StopRecursion Then LoadFileData "(For Split)"
                End If
                chkSplit.BackColor = LightestGreen
            Else
                chkSplit.BackColor = vbButtonFace
                If Not StopRecursion Then LoadFileData "(For Filter)"
                StopRecursion = True
                chkExpand.Value = 0
                StopRecursion = False
            End If
        End If
    End If
End Sub

Private Sub ErrCnt_Click()
    If Val(ErrCnt.Caption) > 0 Then JumpToErrorLine ErrorColor
End Sub

Private Sub ErrCnt_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    If Val(ErrCnt.Caption) > 0 Then ErrCnt.MousePointer = 14 Else ErrCnt.MousePointer = 0
End Sub

Private Sub FileData_RowSelectionChanged(ByVal LineNo As Long, ByVal Selected As Boolean)
    Dim TopLine As Long
    Dim VisLine As Long
    If Selected And LineNo >= 0 Then
        TopLine = FileData.GetVScrollPos
        VisLine = TopLine + ((FileData.Height / Screen.TwipsPerPixelY) / FileData.LineHeight) - 2
        If (LineNo < TopLine) Or (LineNo > VisLine) Then FileData.OnVScrollTo LineNo - 1
    End If
End Sub

Private Sub FileData_SendMouseMove(ByVal Line As Long, ByVal Col As Long, ByVal Flags As String)
    'Dim RecData As String
    'If Flags = "LBUTTON" Then
    '    MousePointer = 99
    '    If DragLine < 0 Then
    '        DragLine = Line
    '        MoveLine = Line
    '        FileData.GetLineColor Line, DragForeColor, DragBackcolor
    '        FileData.SetLineColor Line, vbBlack, vbCyan
    '        FileData.SelectBackColor = vbCyan
    '        FileData.SelectTextColor = vbBlack
    '    Else
    '        If MoveLine <> Line Then
    '            MoveLine = Line
    '            FileData.SetCurrentSelection Line
    '        End If
    '    End If
    'Else
    '    If DragLine >= 0 Then
    '        If DragLine <> Line Then
    '            RecData = FileData.GetLineValues(DragLine)
    '            If DragLine > Line Then
    '                FileData.DeleteLine DragLine
    '                FileData.InsertTextLineAt Line, RecData, False
    '                FileData.SetLineColor Line, DragForeColor, DragBackcolor
    '                FileData.SetCurrentSelection Line
    '            Else
    '                FileData.InsertTextLineAt Line, RecData, False
    '                FileData.SetLineColor Line, DragForeColor, DragBackcolor
    '                FileData.SetCurrentSelection Line
    '                FileData.DeleteLine DragLine
    '            End If
    '        Else
    '            FileData.SetLineColor DragLine, DragForeColor, DragBackcolor
    '        End If
    '        FileData.SelectBackColor = NormalBlue
    '        FileData.SelectTextColor = vbWhite
    '        FileData.RedrawTab
    '        MousePointer = 0
    '        DragLine = -1
    '        DropLine = -1
    '        MoveLine = -1
    '    End If
    'End If
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyF1 Then DisplayHtmHelpFile "", Me.Name, Me.ActiveControl.Name
End Sub

Private Sub Form_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    'If DragLine >= 0 Then
    '    MousePointer = 12
    'End If
End Sub

Private Sub Form_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
    'If DragLine >= 0 Then
    '    FileData.SetLineColor DragLine, DragForeColor, DragBackcolor
    '    FileData.SelectBackColor = NormalBlue
    '    FileData.SelectTextColor = vbWhite
    '    FileData.RedrawTab
    '    DragLine = -1
    '    DropLine = -1
    '    MousePointer = 0
    'End If
End Sub


Private Sub Form_OLEDragDrop(Data As DataObject, Effect As Long, Button As Integer, Shift As Integer, X As Single, Y As Single)
    Dim tmpFile As String
    Dim RetVal As Integer
    On Error GoTo ErrHdl
    If PushedImpButton >= 0 Then
        If (Effect And vbDropEffectCopy) = vbDropEffectCopy Then
            If Data.Files.Count > 0 Then
                tmpFile = Data.Files(1)
                RetVal = 1
                If chkExpand.Value = 1 Then
                    RetVal = MyMsgBox.CallAskUser(0, 0, 0, "Import File Control", "Do you want to replace the current file?", "export", "Yes,No", UserAnswer)
                End If
                If RetVal = 1 Then
                    chkRequest.Tag = tmpFile
                    TabFrame.Enabled = True
                    ResetMain 0, False
                    ResetMain 0, True
                    StopRecursion = True
                    chkExpand.Value = 0
                    StopRecursion = False
                    chkExpand.Value = 1
                End If
            End If
        End If
    End If
ErrHdl:
End Sub

Private Sub Form_OLEDragOver(Data As DataObject, Effect As Long, Button As Integer, Shift As Integer, X As Single, Y As Single, State As Integer)
    If PushedImpButton >= 0 Then
        If State = 1 Then TabFrame.Enabled = True Else TabFrame.Enabled = False
        Me.SetFocus
    Else
        '
    End If
End Sub

Private Sub ImpScroll_Change()
    HandleImpScroll
End Sub
Private Sub ImpScroll_Scroll()
    HandleImpScroll
End Sub
Private Sub HandleImpScroll()
    Dim Idx As Integer
    Dim i As Integer
    Dim LeftPos As Long
    Idx = ImpScroll.Value
    LeftPos = chkRestore.Left + chkRestore.Width + Screen.TwipsPerPixelX
    For i = 0 To ShowMaxButtons
        If Idx <= chkImpTyp.UBound Then
            chkImpTyp(Idx).Left = LeftPos
            chkImpTyp(Idx).Visible = True
            chkImpTyp(Idx).ZOrder
            LeftPos = LeftPos + chkImpTyp(Idx).Width + Screen.TwipsPerPixelX
            Idx = Idx + 1
        End If
    Next
End Sub

Private Sub Timer1_Timer()
    Timer1.Enabled = False
    CheckCmdLineParams
End Sub

Private Sub CheckCmdLineParams()
    Dim FileCount As Integer
    Dim CmdLine As String
    Dim ListOfFiles As String
    Dim ActFile As String
    Dim ActPath As String
    CmdLine = Command
    ListOfFiles = GetItem(CmdLine, 1, "{=")
    If ListOfFiles <> "" Then
        'MsgBox ListOfFiles
        'FileCount = GetFileAndPath(ListOfFiles, ActFile, ActPath)
        'MsgBox ActFile
    End If
End Sub

Private Sub txtEdit_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
End Sub

Private Sub WarnCnt_Click()
    If Val(WarnCnt.Caption) > 0 Then JumpToErrorLine LightestRed
End Sub

Private Sub JumpToErrorLine(ErrColor As Long)
    Dim CurScroll As Long
    Dim CurLine As Long
    Dim MaxLine As Long
    Dim ForeColor As Long
    Dim BackColor As Long
    CurLine = FileData.GetCurrentSelected
    If CurLine < 0 Then CurLine = -1
    MaxLine = FileData.GetLineCount - 1
    While CurLine <= MaxLine
        CurLine = CurLine + 1
        FileData.GetLineColor CurLine, ForeColor, BackColor
        If BackColor = ErrColor Then
            If CurLine > 10 Then CurScroll = CurLine - 10 Else CurScroll = 0
            FileData.OnVScrollTo CurScroll
            FileData.SetCurrentSelection CurLine
            CurLine = MaxLine + 1
        End If
    Wend
End Sub

Private Sub FileData_SendLButtonDblClick(ByVal Line As Long, ByVal Col As Long)
Dim tmpData As String
Dim tmpLinNbr As String
Dim tmpHead As String
Dim LineNo As Long
    If Line >= 0 Then
        If chkEdit.Value = 1 Then
            tmpData = FileData.GetColumnValue(Line, Col)
            txtEdit.Tag = tmpData
            txtEdit.Text = tmpData
            chkEdit.Tag = Line & "," & Col
            tmpLinNbr = FileData.GetColumnValue(Line, 0)
            tmpHead = FileData.GetLineValues(-1)
            tmpData = GetItem(tmpHead, Col + 1, ",")
            chkReplace.ToolTipText = "Replace '" & Trim(tmpData) & "' in line " & Trim(tmpLinNbr) & " "
            chkAll.ToolTipText = "Replace all '" & Trim(txtEdit.Tag) & "' in column '" & Trim(tmpData) & "' "
            chkAll.Tag = GetItem(AutoReplaceAll, Col - 2, ",")
            FileData.ColSelectionRemoveAll
            FileData.SelectColumnBackColor = DarkBlue
            FileData.SelectColumnTextColor = vbWhite
            FileData.ColSelectionAdd Col
            txtEdit.SetFocus
        ElseIf chkExtract.Value = 1 Then
            If (chkInline.Value = 0) And (FlightExtract.chkWork(5).Value = 1) Then
                LineNo = FileData.GetCurrentSelected
                FlightExtract.CopyAllFlights LineNo, LineNo
                If TabFrame.Enabled = True Then FileData.SetFocus
            End If
        End If
    Else
        'Sort
        If FullMode Then
            MousePointer = 11
            chkEdit.Value = 0
            chkExtract.Value = 0
            FileData.Sort Str(Col), True, True
            FileData.RedrawTab
            MousePointer = 0
        End If
    End If
End Sub

Private Sub Form_Load()
    InitApplication
    PushedImpButton = -1
    'ErrorColor = &HFF80FF
    ErrorColor = vbRed
    If Command <> "" Then Timer1.Enabled = True
End Sub

Public Sub InitApplication()
    WinZipId = -1
    DragLine = -1
    DropLine = -1
    myIniPath = "c:\ufis\system"
    myIniFile = "ImportTool.ini"
    myIniFullName = myIniPath & "\" & myIniFile
    MainTitle = GetIniEntry(myIniFullName, "MAIN", "", "IMPORT_TITLE", "Data Import Tool")
    Me.Caption = MainTitle
    UseGfrCmd = GetIniEntry(myIniFullName, "MAIN", "", "ROUTER_CMD", "GFR")
    Load MySetUp
    InitGrids True
    ShowMaxButtons = Abs(Val(GetIniEntry(myIniFullName, "MAIN", "", "MAX_BUTTONS", "12"))) - 1
    If ShowMaxButtons < 1 Then ShowMaxButtons = 1
    If ShowMaxButtons > 12 Then ShowMaxButtons = 12
    InitValidSections ""
    MySetUp.DetermineFileType "RESET"
    Load SeasonData
End Sub

Public Sub InitValidSections(CfgSections As String)
Dim ValidSections As String
Dim ItmNbr As Integer
Dim tmpData As String
Dim ButtonIndex As Integer
Dim BreakOut As Boolean
Dim LeftPos As Long
Dim TopPos As Long
    ValidSections = CfgSections
    LeftPos = chkRestore.Left + chkRestore.Width + Screen.TwipsPerPixelX
    TopPos = chkRestore.Top
    chkLoad.Value = 0
    For ButtonIndex = 0 To chkImpTyp.UBound
        chkImpTyp(ButtonIndex).Visible = False
        chkImpTyp(ButtonIndex).Value = 0
    Next
    tmpData = GetIniEntry(myIniFullName, "MAIN", "", "TESTMODE", "NO")
    If tmpData = "YES" Then TestMode = True Else TestMode = False
    If ValidSections = "" Then ValidSections = GetIniEntry(myIniFullName, "MAIN", "", "IMPORT_TYPES", "")
    ButtonIndex = -1
    ItmNbr = 0
    BreakOut = False
    Do
        ItmNbr = ItmNbr + 1
        tmpData = Trim(GetItem(ValidSections, ItmNbr, ","))
        If tmpData <> "" Then
            ButtonIndex = ButtonIndex + 1
            If ButtonIndex > chkImpTyp.UBound Then Load chkImpTyp(ButtonIndex)
            If ButtonIndex <= chkImpTyp.UBound Then
                chkImpTyp(ButtonIndex).Top = TopPos
                chkImpTyp(ButtonIndex).Left = LeftPos
                chkImpTyp(ButtonIndex).Tag = tmpData
                If ButtonIndex <= ShowMaxButtons Then
                    chkImpTyp(ButtonIndex).Visible = True
                    LeftPos = LeftPos + chkImpTyp(ButtonIndex).Width + Screen.TwipsPerPixelX
                End If
                tmpData = Trim(GetIniEntry(myIniFullName, chkImpTyp(ButtonIndex).Tag, "", "FORMAT_NAME", ""))
                If tmpData <> "" Then
                    chkImpTyp(ButtonIndex).Caption = tmpData
                    chkImpTyp(ButtonIndex).ToolTipText = LTrim(Trim(GetIniEntry(myIniFullName, chkImpTyp(ButtonIndex).Tag, "", "TOOLTIP_TEXT", "")) & " ")
                Else
                    chkImpTyp(ButtonIndex).Caption = "???" & Str(ItmNbr)
                    chkImpTyp(ButtonIndex).ToolTipText = "Missing configuration key: 'FORMAT_NAME' "
                    chkImpTyp(ButtonIndex).Tag = "NOTHING"
                End If
            End If
        Else
            BreakOut = True
        End If
    Loop While Not BreakOut
    ImpScroll.Visible = False
    If ButtonIndex < 0 Then
        MsgBox "No valid configuration found."
    Else
        If ButtonIndex > ShowMaxButtons Then
            ImpScroll.Tag = Trim(Str(ShowMaxButtons))
            ImpScroll.Max = ButtonIndex - ShowMaxButtons
            ImpScroll.Left = LeftPos - Screen.TwipsPerPixelX
            ImpScroll.Visible = True
            LeftPos = LeftPos + ImpScroll.Width + Screen.TwipsPerPixelX - Screen.TwipsPerPixelX
        End If
    End If
End Sub

Public Sub InitGrids(ipFirstCall As Boolean)
    ResetLabels
    If WindowState = vbNormal Then
        Me.Top = 975
        Me.Left = 0
        Me.Width = Screen.Width / Val(MySetUp.MaxMon.Caption)
    End If
    If chkSplit.Value = 0 Or chkSplit.Enabled = False Then
        PrepareFileData 8, ipFirstCall
    Else
        InitDataList 8, ipFirstCall
    End If
    If WindowState = vbNormal Then
        Me.Height = Screen.Height - 1380
    End If
    Me.Refresh
End Sub

Private Sub PrepareFileData(ipLines As Integer, ipFirstCall As Boolean)
Dim HdrLenLst As String
    Me.FontSize = MySetUp.FontSlider.Value
    HdrLenLst = ""
    HdrLenLst = HdrLenLst & TextWidth("9999") / Screen.TwipsPerPixelX + 6 & ","
    HdrLenLst = HdrLenLst & "1000"
    If ipFirstCall Then FileData.ResetContent
    FileData.FontName = "Courier New"
    FileData.SetTabFontBold True
    FileData.ShowHorzScroller True
    FileData.HeaderFontSize = MySetUp.FontSlider.Value + 6
    FileData.FontSize = MySetUp.FontSlider.Value + 6
    FileData.LineHeight = MySetUp.FontSlider.Value + 6
    FileData.LifeStyle = True
    'FileData.CursorLifeStyle = True
    FileData.CreateDecorationObject "CellError", "R,T,B,L", "2,2,2,2", Str(vbBlack) & "," & Str(LightGray) & "," & Str(vbBlack) & "," & Str(LightGray)
    FileData.HeaderLengthString = HdrLenLst
    FileData.HeaderString = "Line,Data Lines"
    If ipFirstCall And Not GridPrepared Then
        'FileData.Top = FileFrame.Top + 1 * Screen.TwipsPerPixelY
        'FileData.Left = FileFrame.Left + 1 * Screen.TwipsPerPixelX
        'FileData.Height = ipLines * FileData.LineHeight * Screen.TwipsPerPixelY
        'Me.Height = FileData.Top + FileData.Height + StatusBar.Height + 3 * Screen.TwipsPerPixelY
        'FileData.GridLineColor = vbBlack
        GridPrepared = True
    End If
End Sub

Private Sub InitDataList(ipLines As Integer, ipFirstCall As Boolean)
Dim i As Integer
Dim tmpLen
Dim HdrLenLst As String
Dim tmpHeader As String
Dim tmpAlign As String
Dim ColWid As String
    Me.FontSize = MySetUp.FontSlider.Value
    FileData.ShowHorzScroller False
    If chkExpand.Value = 1 Then
        ActColLst = FldWidLst
        tmpHeader = "LINE,I,A," & ExpandHeader & ",Remark,System Cells,System Status"
    Else
        ActColLst = FldLenLst
        tmpHeader = "LINE,I,A," & NormalHeader & ",Remark,System Cells,System Status"
    End If
    HdrLenLst = ""
    ColWid = ""
    HdrLenLst = HdrLenLst & TextWidth("9999") / Screen.TwipsPerPixelX + 6 & ","
    HdrLenLst = HdrLenLst & TextWidth("A") / Screen.TwipsPerPixelX + 6 & ","
    HdrLenLst = HdrLenLst & TextWidth("U") / Screen.TwipsPerPixelX + 6 & ","
    tmpAlign = "L,L,L,"
    ColWid = "4,1,1,"
    i = 0
    Do
        i = i + 1
        tmpLen = GetItem(ActColLst, i, ",")
        If tmpLen <> "" Then
            ColWid = ColWid & tmpLen & ","
            HdrLenLst = HdrLenLst & TextWidth(Space(tmpLen)) / Screen.TwipsPerPixelX + 6 & ","
            tmpAlign = tmpAlign & "L,"
        End If
    Loop While tmpLen <> ""
    HdrLenLst = HdrLenLst & "500,500,500"
    ColWid = ColWid & "160,160,160"
    
    tmpAlign = tmpAlign & "L,L,L"
    If ipFirstCall Then FileData.ResetContent
    FileData.FontName = "Courier New"
    FileData.SetTabFontBold True
    FileData.ShowHorzScroller True
    FileData.HeaderFontSize = MySetUp.FontSlider.Value + 6
    FileData.FontSize = MySetUp.FontSlider.Value + 6
    FileData.LineHeight = MySetUp.FontSlider.Value + 6
    'FileData.CreateCellObj "ErrorCell", vbMagenta, vbWhite, FileData.FontSize, False, False, True, 0, "Courier New"
    FileData.CreateDecorationObject "CellError", "R,T,B,L", "2,2,2,2", Str(vbBlack) & "," & Str(LightGray) & "," & Str(vbBlack) & "," & Str(LightGray)
    FileData.HeaderLengthString = HdrLenLst
    FileData.ColumnAlignmentString = tmpAlign
    FileData.HeaderString = tmpHeader
    FileData.ColumnWidthString = ColWid
    CurHeaderLength = HdrLenLst
    'Text1.Text = Text1.Text & "HL:" & HdrLenLst & vbNewLine
    'Text1.Text = Text1.Text & "CA:" & tmpAlign & vbNewLine
    'Text1.Text = Text1.Text & "HS:" & tmpHeader & vbNewLine
    'Text1.Text = Text1.Text & "CW:" & ColWid & vbNewLine
    If ipFirstCall And Not GridPrepared Then
        'FileData.Height = ipLines * FileData.LineHeight * Screen.TwipsPerPixelY
    End If
    'MsgBox DataSystemType
    If SetColSelection Then
        FileData.EnableHeaderSelection True
        'FileData.SetHeaderSelection ColSelectList
    End If
 'FileData.MonthNames = "JAN,FEB,MAR,APR,MAY,JUN,JUL,AUG,SEP,OCT,NOV,DEC"
 'FileData.ColumnDateFormats = ",,,,,,,YYYYMMDD,DDMMYYYY,,,,,,,,,,,,,,,,,,,,,,,"
End Sub
Private Sub ResetLabels()
Dim i As Integer
    ErrCnt.Caption = ""
    ErrCnt.BackColor = vbButtonFace
    ErrCnt.ForeColor = vbBlack
    WarnCnt.Caption = ""
    WarnCnt.BackColor = vbButtonFace
    WarnCnt.ForeColor = vbBlack
    For i = 1 To StatusBar.Panels.Count
        StatusBar.Panels(i).Text = ""
    Next
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
'MsgBox UnloadMode
    Select Case UnloadMode
        Case 0
            chkAppl(6).Value = 1
            chkAppl(6).Value = 0
            Cancel = True
        Case Else
            Unload Me
    End Select
End Sub

Private Sub Form_Resize()
Dim NewVal As Long
    NewVal = Me.ScaleHeight - TabFrame.Top - StatusBar.Height '- 4 * Screen.TwipsPerPixelY
    If NewVal > 50 Then
        TabFrame.Height = NewVal
        FileData.Height = NewVal
    End If
    NewVal = Me.ScaleWidth - 2 * TabFrame.Left '- 5 * Screen.TwipsPerPixelX
    If NewVal > 50 Then
        TabFrame.Width = NewVal
        FileData.Width = NewVal '- 1 * Screen.TwipsPerPixelY
    End If
    CedaBusyFlag.Top = MainDialog.ScaleHeight - StatusBar.Height + 2 * Screen.TwipsPerPixelY
    CedaBusyFlag.Left = MainDialog.ScaleWidth - CedaBusyFlag.Width - 14 * Screen.TwipsPerPixelX
    lblSize.Caption = CStr(Me.Width / 15) & " / " & CStr(Me.Height / 15)
 End Sub

Private Sub txtEdit_Change()
    If txtEdit.Text <> txtEdit.Tag Then
        txtEdit.BackColor = vbYellow
        chkReplace.Enabled = True
        If chkAll.Tag <> "" Then chkAll.Enabled = True Else chkAll.Enabled = False
    Else
        txtEdit.BackColor = vbWhite
        chkReplace.Enabled = False
        chkAll.Enabled = False
    End If
End Sub

Private Sub IdentifyFlights(lpFlno As Long, lpSector As Long, lpOrig As Long, lpDest As Long)
Dim llCount As Long
Dim i As Long
Dim ilSectorNbr As Integer
Dim ilLastSectorNbr As Integer
Dim clFlno As String
Dim clOrg3 As String
Dim clDes3 As String
Dim clLastOrg3 As String
Dim clLastDes3 As String
Dim StodColIdx As Long
Dim StoaColIdx As Long
Dim clStod As String
Dim clStoa As String
Dim clLastStod As String
Dim clLastStoa As String
Dim MyForeColor As Long
Dim MyBackColor As Long
Dim FirstLine As Long
Dim LastLine As Long
Dim clAdid As String
Dim clCntVal As String
Dim FlightClosed As Boolean
Dim ApproachCnt As Integer
Dim DepartureCnt As Integer
Dim clRegn As String
Dim clLastRegn As String

    ArrFltCnt = 0
    DepFltCnt = 0
    If LegColNbr > 2 And OrgColNbr > 2 And DesColNbr > 2 Then
        MousePointer = 11
        
        If TimeChainInfo <> "" Then
            StodColIdx = Val(GetItem(TimeChainInfo, 2, ",")) + 2
            StoaColIdx = Val(GetItem(TimeChainInfo, 3, ",")) + 2
        End If
        
        llCount = FileData.GetLineCount - 1
        For i = 0 To llCount
            FileData.GetLineColor i, MyForeColor, MyBackColor
            If MyBackColor <> ErrorColor Then
                ilSectorNbr = Val(FileData.GetColumnValue(i, lpSector))
                clOrg3 = FileData.GetColumnValue(i, lpOrig)
                clDes3 = FileData.GetColumnValue(i, lpDest)
                'UFIS-1205
                clRegn = FileData.GetColumnValue(i, 13)
                
                If (StodColIdx > 2) And (StoaColIdx > 2) Then
                    clStod = FileData.GetColumnValue(i, StodColIdx)
                    clStoa = FileData.GetColumnValue(i, StoaColIdx)
                End If

                If ilSectorNbr = 1 Then
                    ' Moved to End of IF
                    ' UFIS-1205
                    If (clAdid = "?") And (Not FlightClosed) Then
                      'BADFLT doesn't connect HomeAirport
                      SetErrorText Str(lpSector) & "," & Str(lpOrig) & "," & Str(lpDest), LastLine, ExtFldCnt, HomeAirport & "?", True, 1
                    End If
                    'Begin of a new flight (First Sector)
                    If clOrg3 = HomeAirport And clDes3 = HomeAirport Then
                        'Circulation Flight
                        clAdid = "B"
                        FlightClosed = True
                        ApproachCnt = 0
                        FileData.SetLineColor i, vbBlack, vbCyan
                        FileData.SetColumnValue i, 1, clAdid
                        ArrFltCnt = ArrFltCnt + 1
                        DepFltCnt = DepFltCnt + 1
                    ElseIf clOrg3 = HomeAirport Then
                        'Departure Flight
                        clAdid = "D"
                        ApproachCnt = 0
                        DepartureCnt = 1
                        FlightClosed = False
                        FileData.SetLineColor i, vbBlack, vbGreen
                        FileData.SetColumnValue i, 1, clAdid
                        DepFltCnt = DepFltCnt + 1
                    ElseIf clDes3 = HomeAirport Then
                        'Arrival Flight
                        clAdid = "A"
                        FlightClosed = True
                        ApproachCnt = 0
                        DepartureCnt = 0
                        FileData.SetLineColor i, vbBlack, vbYellow
                        FileData.SetColumnValue i, 1, clAdid
                        ArrFltCnt = ArrFltCnt + 1
                    Else
                        'Must be an approach to us
                        clAdid = "?"
                        DepartureCnt = 0
                        FlightClosed = False
                        FileData.SetLineColor i, vbBlack, LightestYellow
                        ApproachCnt = ApproachCnt + 1
                        clCntVal = Trim(Str(ApproachCnt))
                        FileData.SetColumnValue i, 1, clCntVal
                    End If
                    
                    
                    
                    FirstLine = i
                Else
                    ilLastSectorNbr = ilLastSectorNbr + 1
                    If ilSectorNbr = ilLastSectorNbr Then
                        If clOrg3 = HomeAirport And clDes3 = HomeAirport Then
                            'Circulation Flight must be an Error
                            SetErrorText "", i, ExtFldCnt, "CIRCUL", True, 0
                        ElseIf clOrg3 = HomeAirport Then
                            'Departure Flight is an error
                            SetErrorText "", i, ExtFldCnt, "BADDEP", True, 0
                        ElseIf clDes3 = HomeAirport Then
                            'Arrival Flight
                            clAdid = "A"
                            ApproachCnt = 0
                            If FlightClosed = False Then
                                FileData.SetLineColor i, vbBlack, vbYellow
                                FlightClosed = True
                                FileData.SetColumnValue i, 1, clAdid
                                If clLastDes3 <> clOrg3 Then
                                    SetErrorText "", i, ExtFldCnt, "ORIGIN", True, 0
                                End If
                                If (StodColIdx > 2) And (StoaColIdx > 2) Then
                                    If clLastStoa > clStod Then
                                        SetErrorText "", i, ExtFldCnt, "STOD", True, 0
                                    End If
                                End If
                                ArrFltCnt = ArrFltCnt + 1
                            Else
                                SetErrorText "", i, ExtFldCnt, "BADARR", True, 0
                            End If
                        Else
                            If FlightClosed Then
                                SetErrorText "", i, ExtFldCnt, "CLOSED", True, 0
                            Else
                                If clAdid = "?" Then
                                    ApproachCnt = ApproachCnt + 1
                                    clCntVal = Trim(Str(ApproachCnt))
                                    FileData.SetColumnValue i, 1, clCntVal
                                    FileData.SetLineColor i, vbBlack, LightestYellow
                                Else
                                    DepartureCnt = DepartureCnt + 1
                                    clCntVal = Trim(Str(DepartureCnt))
                                    FileData.SetColumnValue i, 1, clCntVal
                                    FileData.SetLineColor i, vbBlack, LightestGreen
                                End If
                                If clLastDes3 <> clOrg3 Then
                                    SetErrorText "", i, ExtFldCnt, "ORIGIN", True, 0
                                End If
                                If (StodColIdx > 2) And (StoaColIdx > 2) Then
                                    If clLastStoa > clStod Then
                                        SetErrorText "", i, ExtFldCnt, "STOD", True, 0
                                    End If
                                End If
                            End If
                        End If
                    Else
                        SetErrorText "", i, ExtFldCnt, "SECTOR", True, 0
                    End If
                End If
                
                ' UFIS-1205
                ' Reset ApproachCnt  if the REGN is changed
                If (clLastRegn <> clRegn And DataSystemType = "OAFOS") Then
                    ApproachCnt = 0
                End If

                ilLastSectorNbr = ilSectorNbr
                clLastOrg3 = clOrg3
                clLastDes3 = clDes3
                clLastStod = clStod
                clLastStoa = clStoa
                ' UFIS-1205 ATH-98
                clLastRegn = clRegn
                
                
                LastLine = i
                
                ' UFIS-1205
                If (clAdid = "?") And (Not FlightClosed) And (ApproachCnt = 0) Then
                  'BADFLT doesn't connect HomeAirport
                  SetErrorText Str(lpSector) & "," & Str(lpOrig) & "," & Str(lpDest), LastLine, ExtFldCnt, HomeAirport & "?" & "--" & ApproachCnt, True, 1
                End If
            End If
        Next
        'Merged from SandBox AIA
        'If (clAdid = "?") And (Not FlightClosed) Then
        '    'BADFLT doesn't connect HomeAirport
        '    SetErrorText Str(lpSector) & "," & Str(lpOrig) & "," & Str(lpDest), LastLine, ExtFldCnt, HomeAirport & "?", True, 1
        'End If
        MousePointer = 0
    End If
End Sub

Private Sub CheckVpfrVptoFrame(LoopBegin As Long, LoopEnd As Long)
Dim CurLine As Long
Dim MaxLine As Long
Dim FldaColIdx As Long
Dim StoaColIdx As Long
Dim StodColIdx As Long
Dim DaofColIdx As Long
Dim tmpVpfr As String
Dim tmpVpto As String
Dim tmpFred As String
Dim tmpFrew As String
Dim tmpStoa As String
Dim tmpStod As String
Dim tmpStoaDay As String
Dim tmpStodDay As String
Dim tmpDaof As String
Dim DayOffset As Integer
Dim RetVal As Integer
Dim FirstDay As String
Dim MinVpfrDay As String
Dim MaxVptoDay As String
Dim MinLinNbr As String
Dim MaxLinNbr As String
    If (MainVpfrCol > 2) And (MainVptoCol > 2) Then
        MinVpfrDay = "99999999"
        MaxVptoDay = "00000000"
        If TimeChainInfo <> "" Then
            FldaColIdx = Val(GetItem(TimeChainInfo, 1, ",")) + 2
            StodColIdx = Val(GetItem(TimeChainInfo, 2, ",")) + 2
            StoaColIdx = Val(GetItem(TimeChainInfo, 3, ",")) + 2
            DaofColIdx = Val(GetItem(TimeChainInfo, 4, ",")) + 2
        End If
        StatusBar.Panels(7).Text = "Checking period time frames and frequency"
        If LoopEnd >= 0 Then
            MaxLine = LoopEnd
        Else
            MaxLine = FileData.GetLineCount - 1
        End If
        'MsgBox "Starting Loop from " & LoopBegin & " to" & MaxLine
        For CurLine = LoopBegin To MaxLine
            tmpVpfr = FileData.GetColumnValue(CurLine, MainVpfrCol)
            tmpVpto = FileData.GetColumnValue(CurLine, MainVptoCol)
            If (MainFredCol > 2) Then
                tmpFred = FileData.GetColumnValue(CurLine, MainFredCol)
                If MainFrewCol > 2 Then tmpFrew = FileData.GetColumnValue(CurLine, MainFrewCol) Else tmpFrew = "1"
                FirstDay = ""
                RetVal = CheckPeriodData(tmpVpfr, tmpVpto, tmpFred, tmpFrew, FirstDay, False)
            Else
                FirstDay = tmpVpfr
                RetVal = 0
            End If
            If RetVal < 0 Then
                Select Case RetVal
                    Case -1
                        SetErrorText Str(MainFredCol), CurLine, ExtFldCnt, "NODAYS", True, 0
                    Case -2
                        SetErrorText Str(MainVpfrCol), CurLine, ExtFldCnt, "P.DATES", True, 0
                    Case -3
                        SetErrorText Str(MainFredCol), CurLine, ExtFldCnt, "OPDAYS", True, 0
                    Case Else
                        SetErrorText Str(MainVptoCol), CurLine, ExtFldCnt, "PERIOD", True, 0
                End Select
            Else
                If (StodColIdx > 2) And (StoaColIdx > 2) Then
                    If FirstDay = "" Then
                        SetErrorText Str(MainVpfrCol), CurLine, ExtFldCnt, "DAY1", True, 0
                        FirstDay = tmpVpfr
                    End If
                    tmpStodDay = FirstDay
                    tmpStoaDay = tmpStodDay
                    tmpStod = FileData.GetColumnValue(CurLine, StodColIdx)
                    tmpStod = Right(tmpStod, 4)
                    tmpStoa = FileData.GetColumnValue(CurLine, StoaColIdx)
                    tmpStoa = Right(tmpStoa, 4)
                    DayOffset = 0
                    If DaofColIdx > 2 Then
                        tmpDaof = FileData.GetColumnValue(CurLine, DaofColIdx)
                        DayOffset = Val(tmpDaof)
                    End If
                    If (tmpStoa < tmpStod) Or (DayOffset > 0) Then
                        If DayOffset <= 0 Then DayOffset = 1
                        tmpStoaDay = ShiftDate(tmpStoaDay, DayOffset)
                    End If
                    If tmpStod <> "" Then
                        tmpStod = tmpStodDay & tmpStod
                        FileData.SetColumnValue CurLine, StodColIdx, tmpStod
                        If DataSystemType = "OAFOS" Then tmpVpfr = Left(tmpStod, 8)
                    End If
                    If tmpStoa <> "" Then
                        tmpStoa = tmpStoaDay & tmpStoa
                        FileData.SetColumnValue CurLine, StoaColIdx, tmpStoa
                        If DataSystemType = "OAFOS" Then tmpVpto = Left(tmpStoa, 8)
                    End If
                End If
                If tmpVpto > MaxVptoDay Then
                    MaxVptoDay = tmpVpto
                    MaxLinNbr = FileData.GetColumnValue(CurLine, 0)
                End If
                If tmpVpfr < MinVpfrDay Then
                    MinVpfrDay = tmpVpfr
                    MinLinNbr = FileData.GetColumnValue(CurLine, 0)
                End If
            End If
        Next
        FileData.RedrawTab
        'MsgBox "MIN/MAX Days = " & MinVpfrDay & " / " & MaxVptoDay
        If Trim(ActSeason(0).Text) = "" Then
            DetermineValidSeason MinVpfrDay, MaxVptoDay
            ActSeason(1).ToolTipText = "In line " & MinLinNbr
            ActSeason(2).ToolTipText = "In line " & MaxLinNbr
        End If
        'MsgBox "'ActSeason' now contains '" & ActSeason(0).Text & "'" & vbNewLine & vbNewLine & "SEATAB Data <" & ActSeason(0).Tag & ">"
        StatusBar.Panels(7).Text = "Ready"
        Refresh
    End If
End Sub

Public Sub DetermineValidSeason(Vpfr As String, Vpto As String)
Dim varVpfr
Dim varVpto
Dim varNxtDay
Dim varDiff
Dim varMidDay
Dim MidDay As String
Dim NxtDay As String
Dim tmpSeaDat As String
Dim tmpdat As String
    'MsgBox "Entered 'DetermineValidSeason'"
    ActSeason(1).Text = DecodeSsimDayFormat(Vpfr, "CEDA", "SSIM2")
    ActSeason(1).Tag = Vpfr
    ActSeason(1).BackColor = LightYellow
    ActSeason(2).Text = DecodeSsimDayFormat(Vpto, "CEDA", "SSIM2")
    ActSeason(2).Tag = Vpto
    ActSeason(2).BackColor = LightYellow
    varVpfr = CedaDateToVb(Vpfr)
    varVpto = CedaDateToVb(Vpto)
    varDiff = Round(DateDiff("d", varVpto, varVpfr) / 2)
    varMidDay = DateAdd("d", varDiff, varVpto)
    MidDay = Format(varMidDay, "yyyymmdd")
    tmpSeaDat = SeasonData.GetValidSeason(MidDay)
    If tmpSeaDat <> "" Then
        ActSeason(0).Text = GetItem(tmpSeaDat, 1, ",")
        ActSeason(0).Tag = tmpSeaDat
        tmpdat = GetItem(tmpSeaDat, 4, ",")
        If tmpdat = "" Then tmpdat = GetItem(tmpSeaDat, 1, ",")
        ActSeason(0).ToolTipText = "Season: " & tmpdat
        ActSeason(0).ForeColor = vbBlack
        ActSeason(0).BackColor = LightYellow
    Else
        'ActSeason(0).Text = DecodeSsimDayFormat(MidDay, "CEDA", "SSIM2")
        ActSeason(0).Text = "???"
        ActSeason(0).Tag = "ERROR," & MidDay
        ActSeason(0).ToolTipText = "No valid season found."
        ActSeason(0).ForeColor = vbWhite
        ActSeason(0).BackColor = vbRed
    End If
    varNxtDay = CedaFullDateToVb(MySetUp.ServerTime.Tag)
    If MySetUp.ServerTimeType <> "UTC" Then varNxtDay = DateAdd("n", -UtcTimeDiff, varNxtDay)
    varNxtDay = DateAdd("d", Val(MySetUp.AutoClear.Caption), varNxtDay)
    NxtDay = Format(varNxtDay, "yyyymmdd")
    ActSeason(3).Text = DecodeSsimDayFormat(NxtDay, "CEDA", "SSIM2")
    ActSeason(3).Tag = NxtDay
    ActSeason(3).ForeColor = vbBlack
    ActSeason(3).BackColor = LightestGreen
    chkClear.ToolTipText = "Clear out all flights prior " & ActSeason(3).Text
End Sub

Public Function CheckPeriodData(Vpfr As String, Vpto As String, Frqd As String, Frqw As String, FirstValidDay As String, RetFrqDays As Boolean) As Integer
Dim Result As Integer
Dim varVpfr As Variant
Dim varVpto As Variant
Dim varFday As Variant
Dim strWkdy As String
Dim intDayCnt As Integer
Dim tmpFrqd As String
Dim ChrPos As Integer
Dim FirstDay As String
'Dim tmpdata As String used for: check if the date is valid
    Result = 0
    FirstValidDay = ""
    tmpFrqd = Trim(Frqd)
    tmpFrqd = Replace(tmpFrqd, "0", ".", 1, -1, vbBinaryCompare)
    If Len(tmpFrqd) <> 7 Or (tmpFrqd = ".......") Then Result = -1
    If Result = 0 Then
        If Vpto < Vpfr Then Result = -2
    End If
    If Result = 0 Then
        varVpfr = CedaDateToVb(Vpfr)
        varVpto = CedaDateToVb(Vpto)
        If varVpto < varVpfr Then Result = -2
        If Not CheckValidDate(Vpfr) Then Result = -2
        If Not CheckValidDate(Vpto) Then Result = -2
    End If
    If Result = 0 Then
        varFday = varVpfr
        'to check the operational days
        'we run only one week (7 days) through the period frame
        intDayCnt = 0
        Do
            intDayCnt = intDayCnt + 1
            strWkdy = Trim(Str(Weekday(varFday, vbMonday)))
            If InStr(tmpFrqd, strWkdy) > 0 Then
                tmpFrqd = Replace(tmpFrqd, strWkdy, ".", 1, 1, vbBinaryCompare)
                If FirstValidDay = "" Then FirstValidDay = Format(varFday, "yyyymmdd")
            End If
            varFday = DateAdd("d", 1, varFday)
        Loop While (varFday <= varVpto) And (intDayCnt < 7)
        If tmpFrqd <> "......." Then
            Result = -3
            If RetFrqDays Then
                Result = 0
                For intDayCnt = 1 To 7
                    strWkdy = Mid(tmpFrqd, intDayCnt, 1)
                    If strWkdy <> "." Then Frqd = Replace(Frqd, strWkdy, ".", 1, -1, vbBinaryCompare)
                Next
                If Frqd = "......." Then Result = -4
            End If
        End If
    End If
    CheckPeriodData = Result
End Function

Public Function BuildSlotRecord(CurLine As Long, FlightLine As Long) As String
Dim ResultRecord As String
Dim StopSearch As Boolean
Dim LegRec As String
Dim pclFullName As String
Dim MaxLine As Long
Dim FirstLine As Long
Dim LastLine As Long
Dim ErrorLine As Long
Dim tmpCol1Val As String
Dim tmpCol2Val As String
Dim tmpFldNam As String
Dim tmpFldVal As String
Dim tmpOrig As String
Dim tmpDest As String
Dim errAdid As String
Dim CurFlno As String
Dim OldFlno As String
Dim CurAdid As String
Dim OldAdid As String
Dim tmpFlcaVal As String
Dim tmpFltnVal As String
Dim tmpFltvVal As String
Dim ErrorTrip As Boolean
Dim ArrivalTrip As Boolean
Dim DepartureTrip As Boolean
Dim ViaTrip As Boolean
Dim FldColIdx As Long
Dim FlcaCol As Long
Dim FltnCol As Long
Dim FltvCol As Long
    Select Case DataSystemType
        Case "SSIM"
            FlcaCol = 2 + 2
            FltnCol = 3 + 2
            FltvCol = 4 + 2
        Case "OAFOS"
            FlcaCol = 1 + 2
            FltnCol = 2 + 2
            FltvCol = 3 + 2
        Case Else
            'hardcode for SSIM
            FlcaCol = 2 + 2
            FltnCol = 3 + 2
            FltvCol = 4 + 2
    End Select
    OldFlno = ""
    CurAdid = "?"
    OldAdid = "?"
    FirstLine = -1
    LastLine = -1
    ErrorLine = -1
    ErrorTrip = False
    ArrivalTrip = False
    DepartureTrip = False
    ViaTrip = False
    MaxLine = FileData.GetLineCount + 1
    StopSearch = False
    ResultRecord = ""
    While (CurLine <= MaxLine) And (StopSearch = False)
        tmpFlcaVal = FileData.GetColumnValue(CurLine, FlcaCol)
        tmpFltnVal = FileData.GetColumnValue(CurLine, FltnCol)
        tmpFltvVal = FileData.GetColumnValue(CurLine, FltvCol)
        CurFlno = tmpFlcaVal & tmpFltnVal & "V" & tmpFltvVal
        tmpCol1Val = FileData.GetColumnValue(CurLine, 1)
        tmpCol2Val = FileData.GetColumnValue(CurLine, 2)
        If (tmpCol1Val = "E") And (ErrorLine < 0) Then
            tmpOrig = FileData.GetColumnValue(CurLine, OrgColNbr)
            tmpDest = FileData.GetColumnValue(CurLine, DesColNbr)
            If tmpOrig = HomeAirport Then
                'MsgBox CurLine & "/" & tmpOrig & "/" & tmpDest
                errAdid = "D"
                ErrorLine = CurLine
            ElseIf tmpDest = HomeAirport Then
                'MsgBox CurLine & "/" & tmpOrig & "/" & tmpDest
                errAdid = "A"
                ErrorLine = CurLine
            End If
        End If
        
        ' Added for UFIS-1205
        ' to handle Full rotations with the same FLNO
        If CurFlno <> OldFlno Then
            If OldFlno <> "" Then
                'Run through the flight lines
                'MsgBox OldFlno & " / " & FirstLine + 1 & " / " & LastLine + 1 & " / " & ErrorTrip
                If ErrorTrip Then
                    'MsgBox OldFlno & "/" & FirstLine + 1 & "/" & LastLine + 1 & "/ERROR"
                    SetFlightLineErrors FirstLine, LastLine, LightestRed
                    GetSlotRecInfo ResultRecord, FirstLine, LastLine, FlightLine, CurAdid, False
                    StopSearch = True
                Else
                    GetSlotRecInfo ResultRecord, FirstLine, LastLine, FlightLine, CurAdid, True
                    StopSearch = True
                End If
            End If
            FirstLine = CurLine
            ErrorTrip = False
            ArrivalTrip = False
            DepartureTrip = False
            CurAdid = "?"
            OldFlno = CurFlno
        
        End If

        
        If (tmpCol1Val = "A") Or (CurAdid = "?" And tmpCol1Val = "1") Then
            CurAdid = "A"
            ArrivalTrip = True
        ElseIf tmpCol1Val = "D" Then
            CurAdid = tmpCol1Val
            DepartureTrip = True
        ElseIf tmpCol1Val = "B" Then
            'Adid=B
        Else
        End If
        If (tmpCol1Val = "E") Or (tmpCol2Val = "-") Then ErrorTrip = True
        OldFlno = CurFlno
        OldAdid = CurAdid
        LastLine = CurLine
        CurLine = CurLine + 1
    Wend
    If ErrorLine >= 0 Then
        If ErrorLine < FirstLine Then
            'MsgBox "ERROR FROM " & FirstLine & " to " & LastLine & " (" & ErrorLine & ")"
            GetSlotRecInfo ResultRecord, ErrorLine, ErrorLine, ErrorLine, errAdid, True
        End If
    End If
    CurLine = CurLine - 1
    BuildSlotRecord = ResultRecord
End Function

Public Sub GetSlotRecInfo(ResultRecord As String, FirstLine As Long, LastLine As Long, FlightLine As Long, CurAdid As String, ValidFlight As Boolean)
Dim BreakOut As Boolean
Dim FirstRec As String
Dim LastRec As String
Dim FormatList As String
Dim ItmTxt As String
Dim ItmIdx As String
Dim LineCtrl As String
Dim TaskCtrl As String
Dim ColTxt As String
Dim CurItm As Integer
Dim ColIdx As Integer

    'MsgBox FirstLine + 1 & " / " & LastLine + 1 & " / " & CurAdid
    BreakOut = False
    Select Case CurAdid
        Case "A"
            'ExtractArrivals = "LL1,LL2,LL3,LL5,LL6,'','',LL10,LL11,LL12,'',LL21,FL13,F?17,LL18,'','','','',LL9,'','1',FL22"
            FormatList = ExtractArrivals
            FlightLine = LastLine
        Case "D"
            'ExtractDepartures = "FL1,FL2,FL3,'','',FL5,FL6,FL10,FL11,FL12,'',FL21,'','','',FL14,'',L?13,LL17,'',FL9,'1',FL22"
            FormatList = ExtractDepartures
            FlightLine = FirstLine
        Case "B"
            BreakOut = True
            FlightLine = FirstLine
        Case "X"
            'Slot Coord line
            FormatList = ExtractSlotLine
            FlightLine = FirstLine
        Case "F"
            'FHK Format line
            'HardCode
            If GetItem(ResultRecord, 8, ",") = "A" Then
                FormatList = ExtractRotationArr
            Else
                FormatList = ExtractRotationDep
            End If
            FlightLine = FirstLine
        Case Else
            'MsgBox "ADID:" & CurAdid
            BreakOut = True
            FlightLine = FirstLine
    End Select
    ResultRecord = ""
    If Not BreakOut Then
        FirstRec = FileData.GetLineValues(FirstLine)
        If LastLine >= 0 Then LastRec = FileData.GetLineValues(LastLine)
        CurItm = 0
        Do
            CurItm = CurItm + 1
            ItmTxt = GetItem(FormatList, CurItm, ",")
            If ItmTxt <> "" Then
                LineCtrl = Left(ItmTxt, 1)
                TaskCtrl = Mid(ItmTxt, 2, 1)
                If LineCtrl <> "'" Then
                    ColIdx = Val(Mid(ItmTxt, 3))
                    If ColIdx <= 0 Then
                        MsgBox "Syntax Error 'ColIdx': " & ItmTxt
                    End If
                    ColTxt = ""
                    Select Case TaskCtrl
                        Case "L"
                            'Line Data required
                        Case "?"
                            If FirstLine = LastLine Then LineCtrl = "X"
                        Case Else
                            MsgBox "Syntax Error 'TaskCtrl': " & ItmTxt
                    End Select
                    Select Case LineCtrl
                        Case "F"
                            'First Line
                            ColTxt = GetItem(FirstRec, ColIdx, ",")
                        Case "L"
                            'Last Line
                            ColTxt = GetItem(LastRec, ColIdx, ",")
                        Case "X"
                            'No Line Data required. Use item 2 in 'ItmTxt' as text.
                            ColTxt = GetItem(ItmTxt, 2, ";")
                            ColTxt = Replace(ColTxt, "'", "", 1, -1, vbBinaryCompare)
                        Case "D"
                            'Slot Coord
                            ColTxt = GetItem(FirstRec, ColIdx, ",")
                        Case Else
                            MsgBox "Syntax Error 'LineCtrl': " & ItmTxt
                    End Select
                Else
                    'Direct Text Input
                    ColTxt = Replace(ItmTxt, "'", "", 1, -1, vbBinaryCompare)
                End If
                ResultRecord = ResultRecord & ColTxt & ","
            End If
        Loop While ItmTxt <> ""
        ResultRecord = ResultRecord & Trim(Str(FirstLine)) & ","
        ResultRecord = ResultRecord & Trim(Str(LastLine)) & ","
        ResultRecord = ResultRecord & Trim(Str(FlightLine)) & ","
        If CurAdid = "A" Then
            Select Case DataSystemType
                Case "SSIM"
                    CheckSsimArrFrqd LastRec, ResultRecord
                Case "OAFOS"
                    CheckOaFosArrFrqd LastRec, ResultRecord
                Case Else
            End Select
        End If
    End If
End Sub
Private Sub CheckSsimArrFrqd(SsimRec As String, SlotRec As String)
    Dim tmpStodDay As String
    Dim tmpStoaDay As String
    Dim tmpFrqd As String
    tmpStodDay = Left(GetItem(SsimRec, 14, ","), 8)
    tmpStoaDay = Left(GetItem(SsimRec, 18, ","), 8)
    If tmpStoaDay > tmpStodDay Then
        tmpFrqd = GetItem(SlotRec, 10, ",")
        tmpFrqd = ShiftFrqd(tmpFrqd, 1)
        SetItem SlotRec, 10, ",", tmpFrqd
        tmpStodDay = GetItem(SlotRec, 8, ",")
        tmpStodDay = CedaDateAdd(tmpStodDay, 1)
        SetItem SlotRec, 8, ",", tmpStodDay
        tmpStoaDay = GetItem(SlotRec, 9, ",")
        tmpStoaDay = CedaDateAdd(tmpStoaDay, 1)
        SetItem SlotRec, 9, ",", tmpStoaDay
    End If
End Sub
Private Sub CheckOaFosArrFrqd(OaFosRec As String, SlotRec As String)
    Dim tmpStodDay As String
    Dim tmpStoaDay As String
    Dim tmpFrqd As String
    tmpStodDay = Left(GetItem(OaFosRec, 17, ","), 8)
    tmpStoaDay = Left(GetItem(OaFosRec, 19, ","), 8)
    If tmpStoaDay > tmpStodDay Then
        SetItem SlotRec, 8, ",", tmpStoaDay
        SetItem SlotRec, 9, ",", tmpStoaDay
    End If
End Sub
Private Function ShiftFrqd(UseFrqd As String, ShiftValue As Integer) As String
    Dim tmpFrqd As String
    Dim iVal As Integer
    Dim i As Integer
    tmpFrqd = "......."
    For i = 1 To 7
        If Mid(UseFrqd, i, 1) <> "." Then
            iVal = i + ShiftValue
            If iVal > 7 Then iVal = iVal - 7
            Mid(tmpFrqd, iVal, 1) = Trim(Str(iVal))
        End If
    Next
    ShiftFrqd = tmpFrqd
End Function
Private Sub CheckAutoReplace()
    Dim CurCol As Long
    Dim RefCol As Long
    Dim CurItm As Integer
    Dim ColCode As String
    Dim OldTxt As String
    Dim NewTxt As String
    Dim RefTxt As String
    Dim tmpResult As String
    Dim tmpSqlKey As String
    Dim CedaData As String
    Dim llCount As Long
    Dim i As Long
    Dim RetVal As Integer
    Dim tmpColLst As String
    Dim clLineIdx As String
    Dim LinItm As Integer
    Dim llLine As Long
    Dim UsedKeys As String
    Dim ColList As String
    Dim DataList As String
    MySetUp.UsedGldTabKeys = ""
    If (MySetUp.chkWork(2).Value = 1) And (AutoReplaceAll <> "") And (CedaIsConnected) Then
        CurItm = 0
        For CurItm = 1 To ExtFldCnt
            ColCode = GetItem(AutoReplaceAll, CurItm, ",")
            If ColCode <> "" Then
                If InStr(ColCode, "/") > 0 Then
                    RefCol = Val(GetItem(ColCode, 2, "/")) + 2
                    ColCode = GetItem(ColCode, 1, "/")
                Else
                    RefCol = -1
                End If
                tmpSqlKey = "WHERE TYPE='" & DataSystemType & "' AND LKEY='" & ColCode & "' ORDER BY LVAL DESC"
                RetVal = UfisServer.CallCeda(tmpResult, "RTA", "GDLTAB", "LVAL,RVAL", "", tmpSqlKey, "", 1, False, False)
                CurCol = CurItm + 2
                'MsgBox CurCol & vbNewLine & CedaData
                llCount = UfisServer.DataBuffer(1).GetLineCount - 1
                For i = 0 To llCount
                    CedaData = UfisServer.DataBuffer(1).GetLineValues(i)
                    OldTxt = GetItem(CedaData, 1, ",")
                    OldTxt = GetItem(OldTxt, 1, "/")
                    NewTxt = GetItem(CedaData, 2, ",")
                    RefTxt = GetItem(NewTxt, 2, "/")
                    NewTxt = GetItem(NewTxt, 1, "/")
                    If OldTxt <> NewTxt Then
                        If RefTxt = "" Then
                            tmpColLst = FileData.GetLinesByColumnValue(CurCol, OldTxt, 0)
                        Else
                            ColList = Trim(Str(RefCol)) & "," & Trim(Str(CurCol))
                            DataList = RefTxt & "," & OldTxt
                            tmpColLst = FileData.GetLinesByMultipleColumnValue(ColList, DataList, 0)
                        End If
                        If tmpColLst <> "" Then
                            clLineIdx = "START"
                            LinItm = 0
                            While clLineIdx <> ""
                                LinItm = LinItm + 1
                                clLineIdx = GetItem(tmpColLst, LinItm, ",")
                                If clLineIdx <> "" Then
                                    llLine = Val(clLineIdx)
                                    FileData.SetColumnValue llLine, CurCol, NewTxt
                                End If
                            Wend
                            UsedKeys = DataSystemType & "," & ColCode & "," & GetItem(CedaData, 1, ",") & "," & GetItem(CedaData, 2, ",") & ",CNT: " & Trim(Str(LinItm - 1))
                            MySetUp.UsedGldTabKeys = MySetUp.UsedGldTabKeys & UsedKeys & vbLf
                        End If
                    End If
                Next
            End If
        Next
    End If
    MySetUp.chkWork(4).Value = 0
    MySetUp.chkWork(4).Value = 1
End Sub

Public Sub ToggleDateFormat(bpWhat As Boolean)
    Dim CurItm As Integer
    Dim ColNbr As Integer
    Dim ItmTxt As String
    Dim newLen As String
    On Error Resume Next
    If chkExpand.Value = 1 Then
        CurItm = 2
        Do
            ItmTxt = GetItem(DateFields, CurItm, ",")
            If ItmTxt <> "" Then
                ColNbr = Val(ItmTxt) + 2
                If bpWhat = True Then   'User Format
                    FileData.DateTimeSetColumn ColNbr
                    FileData.DateTimeSetInputFormatString ColNbr, "YYYYMMDD"
                    FileData.DateTimeSetOutputFormatString ColNbr, "DDMMMYY"
                    ItmTxt = Right("000" + Trim(Str(ColNbr)), 3)
                    If InStr(ActiveTimeFields, ItmTxt) = 0 Then ActiveTimeFields = ActiveTimeFields + ItmTxt + ","
                    ColNbr = ColNbr + 1
                    newLen = TextWidth("01OCT00") / Screen.TwipsPerPixelX + 6
                    SetItem CurHeaderLength, ColNbr, ",", newLen
                Else
                    FileData.DateTimeResetColumn ColNbr
                    ColNbr = ColNbr + 1
                    newLen = TextWidth("20001001") / Screen.TwipsPerPixelX + 6
                    SetItem CurHeaderLength, ColNbr, ",", newLen
                End If
            End If
            CurItm = CurItm + 1
        Loop While ItmTxt <> ""
        CurItm = 2
        Do
            ItmTxt = GetItem(TimeFields, CurItm, ",")
            If ItmTxt <> "" Then
                ColNbr = Val(ItmTxt) + 2
                If bpWhat = True Then
                    FileData.DateTimeSetColumn ColNbr
                    FileData.DateTimeSetInputFormatString ColNbr, "YYYYMMDDhhmm"
                    FileData.DateTimeSetOutputFormatString ColNbr, "DDMMMYY'/'hhmm"
                    ItmTxt = Right("000" + Trim(Str(ColNbr)), 3)
                    If InStr(ActiveTimeFields, ItmTxt) = 0 Then ActiveTimeFields = ActiveTimeFields + ItmTxt + ","
                    ColNbr = ColNbr + 1
                    newLen = TextWidth("01OCT00/1200") / Screen.TwipsPerPixelX + 6
                    SetItem CurHeaderLength, ColNbr, ",", newLen
                Else
                    FileData.DateTimeResetColumn ColNbr
                    ColNbr = ColNbr + 1
                    newLen = TextWidth("200010011200") / Screen.TwipsPerPixelX + 6
                    SetItem CurHeaderLength, ColNbr, ",", newLen
                End If
            End If
            CurItm = CurItm + 1
        Loop While ItmTxt <> ""
        FileData.HeaderLengthString = CurHeaderLength
        FileData.RedrawTab
    End If
End Sub

Private Sub ResetTimeFields()
    Dim CurCol As Long
    Dim ItmTxt As String
    Dim ItmNbr As Integer
    ItmNbr = 0
    ItmTxt = "START"
    While ItmTxt <> ""
        ItmNbr = ItmNbr + 1
        ItmTxt = GetItem(ActiveTimeFields, ItmNbr, ",")
        If ItmTxt <> "" Then
            CurCol = Val(ItmTxt)
            FileData.DateTimeResetColumn CurCol
        End If
    Wend
    ActiveTimeFields = ""
End Sub

Public Sub InitPeriodCheck(TimePeriodColumns As String, InitMain As Boolean)
    If InitMain Then
        MainVpfrCol = Val(GetItem(TimePeriodColumns, 1, ",")) + 2
        MainVptoCol = Val(GetItem(TimePeriodColumns, 2, ",")) + 2
        MainFredCol = Val(GetItem(TimePeriodColumns, 3, ",")) + 2
        MainFrewCol = Val(GetItem(TimePeriodColumns, 4, ",")) + 2
        MainStoaCol = Val(GetItem(TimePeriodColumns, 5, ",")) + 2
        MainStodCol = Val(GetItem(TimePeriodColumns, 6, ",")) + 2
        MainChkFred = "," & GetItem(MainPeriodCols, 3, ",") & ","
    End If
    CheckFlightPeriod TimePeriodColumns, "", "", True, False, False, ""
End Sub

Public Function CheckFlightPeriod(Spfr As String, Spto As String, FltRecord As String, RetVal As Boolean, ForCheck As Boolean, CutOut As Boolean, FredFilter As String) As Boolean
    Static VpfrItm As Integer
    Static VptoItm As Integer
    Static FrqdItm As Integer
    Static FrqwItm As Integer
    Static StoaItm As Integer
    Static StodItm As Integer
    Dim Result As Boolean
    Dim Vpfr As String
    Dim Vpto As String
    Dim Frqd As String
    Dim OldFrqd As String
    Dim Frqw As String
    Dim FirstDay As String
    Dim Stoa As String
    Dim Stod As String
    Result = RetVal
    If ForCheck = True Then
        If Result = True Then
            FirstDay = ""
            Vpfr = GetItem(FltRecord, VpfrItm, ",")
            Vpto = GetItem(FltRecord, VptoItm, ",")
            Frqd = GetItem(FltRecord, FrqdItm, ",")
            OldFrqd = Frqd
            Frqw = GetItem(FltRecord, FrqwItm, ",")
            If Frqw = "" Then Frqw = "1"
            If (Vpfr > Spto) Or (Vpto < Spfr) Then
                Result = False
            Else
                If (CutOut = True) And (Vpfr < Spfr) Then
                    Vpfr = Spfr
                    SetItem FltRecord, VpfrItm, ",", Vpfr
                End If
                If (CutOut = True) And (Vpto > Spto) Then
                    Vpto = Spto
                    SetItem FltRecord, VptoItm, ",", Vpto
                End If
                If FredFilter <> "" Then Frqd = PatchFrqDays(Frqd, FredFilter)
                If CheckPeriodData(Vpfr, Vpto, Frqd, Frqw, FirstDay, True) <> 0 Then Result = False
                If Frqd <> OldFrqd Then SetItem FltRecord, FrqdItm, ",", Frqd
                If Result = True Then
                    If FirstDay = "" Then
                        'Here we have a problem
                        'MsgBox "No FirstDay found!" & vbNewLine & FltRecord
                        FirstDay = Vpfr
                    End If
                    If StoaItm > 3 Then Stoa = GetItem(FltRecord, StoaItm, ",") Else Stoa = ""
                    If StodItm > 3 Then Stod = GetItem(FltRecord, StodItm, ",") Else Stod = ""
                    PatchTimeChain FirstDay, Stoa, Stod
                    If StoaItm > 3 Then SetItem FltRecord, StoaItm, ",", Stoa
                    If StodItm > 3 Then SetItem FltRecord, StodItm, ",", Stod
                    If DataSystemType = "OAFOS" Then
                        If Trim(Stoa) <> "" Then
                            If Stoa < FlightExtract.txtVpfr(3).Tag Then Result = False
                        End If
                        If Trim(Stod) <> "" Then
                            If Stod < FlightExtract.txtVpfr(3).Tag Then Result = False
                        End If
                    End If
                End If
            End If
        End If
    Else
        'For Init
        VpfrItm = Val(GetItem(Spfr, 1, ",")) + 3
        VptoItm = Val(GetItem(Spfr, 2, ",")) + 3
        FrqdItm = Val(GetItem(Spfr, 3, ",")) + 3
        FrqwItm = Val(GetItem(Spfr, 4, ",")) + 3
        StoaItm = Val(GetItem(Spfr, 5, ",")) + 3
        StodItm = Val(GetItem(Spfr, 6, ",")) + 3
    End If
    CheckFlightPeriod = Result
End Function

Private Function PatchFrqDays(CurFred As String, FilterFred As String) As String
    Dim Result As String
    Dim i As Integer
    Dim tmpChr As String
    Result = CurFred
    If FilterFred <> "......." Then
        For i = 1 To 7
            tmpChr = Mid(FilterFred, i, 1)
            If tmpChr = "." Then Mid(Result, i, 1) = "."
        Next
    End If
    PatchFrqDays = Result
End Function
Private Sub PatchTimeChain(FirstDay As String, Stoa As String, Stod As String)
    Dim StoaDay As String
    Dim StodDay As String
    Dim varFirstDay
    Dim varStoaDay
    Dim varStodDay
    Dim DayOffset As Long
    If Len(RTrim(Stoa)) = 4 Then Stoa = FirstDay + Stoa
    If Len(RTrim(Stod)) = 4 Then Stod = FirstDay + Stod
    If (RTrim(Stoa) <> "") And (RTrim(Stod) <> "") Then
        StoaDay = Left(Stoa, 8)
        StodDay = Left(Stod, 8)
        varFirstDay = CedaDateToVb(FirstDay)
        varStoaDay = CedaDateToVb(StoaDay)
        varStodDay = CedaDateToVb(StodDay)
        DayOffset = DateDiff("d", varStoaDay, varStodDay)
        varStodDay = DateAdd("d", DayOffset, varFirstDay)
        StodDay = Format(varStodDay, "yyyymmdd")
        StoaDay = Format(varFirstDay, "yyyymmdd")
        Mid(Stoa, 1, 8) = StoaDay
        Mid(Stod, 1, 8) = StodDay
    ElseIf RTrim(Stoa) <> "" Then
        Mid(Stoa, 1, 8) = FirstDay
    ElseIf RTrim(Stod) <> "" Then
        Mid(Stod, 1, 8) = FirstDay
    End If
End Sub

Private Sub WarnCnt_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    If Val(WarnCnt.Caption) > 0 Then WarnCnt.MousePointer = 14 Else WarnCnt.MousePointer = 0
End Sub

Private Sub StoreLineStatus()
    Dim CurLine As Long
    Dim MaxLine As Long
    Dim CurForeColor As Long
    Dim CurBackColor As Long
    Dim StatCol As Long
    Dim CurStat As String
    StatCol = ExtFldCnt + 2
    MaxLine = FileData.GetLineCount - 1
    For CurLine = 0 To MaxLine
        FileData.GetLineColor CurLine, CurForeColor, CurBackColor
        If CurBackColor <> vbWhite Then
            CurStat = Trim(Str(CurForeColor)) & "/" & Trim(Str(CurBackColor))
            FileData.SetColumnValue CurLine, StatCol, CurStat
        End If
    Next
End Sub
Private Sub RestoreLineStatus()
    Dim CurLine As Long
    Dim MaxLine As Long
    Dim CurForeColor As Long
    Dim CurBackColor As Long
    Dim StatCol As Long
    Dim CurStat As String
    StatCol = ExtFldCnt + 2
    MaxLine = FileData.GetLineCount - 1
    For CurLine = 0 To MaxLine
        CurStat = FileData.GetColumnValue(CurLine, StatCol)
        If CurStat <> "" Then
            CurForeColor = Val(GetItem(CurStat, 1, "/"))
            CurBackColor = Val(GetItem(CurStat, 2, "/"))
            FileData.SetLineColor CurLine, CurForeColor, CurBackColor
            FileData.SetColumnValue CurLine, StatCol, ""
        End If
    Next
End Sub

Private Sub InitFreeConfigData()
    Dim CurLine As Long
    Dim MaxLine As Long
    Dim MaxItem As Long
    Dim Linedata As String
    FormatType = CfgTool.FileFormatType.Text
    ItemSep = ";"
    ExpandHeader = BuildCleanItems(CfgTool.CfgTab(1).GetLineValues(4), 2, -1, ",")
    MaxItem = ItemCount(ExpandHeader, ",") + 2
    ExpPosLst = BuildCleanItems(CfgTool.CfgTab(1).GetLineValues(5), 2, -1, ",")
    FldWidLst = BuildCleanItems(CfgTool.CfgTab(1).GetLineValues(6), 2, -1, ",")
    NormalHeader = BuildCleanItems(CfgTool.CfgTab(1).GetLineValues(1), 2, -1, ",")
    FldPosLst = BuildCleanItems(CfgTool.CfgTab(1).GetLineValues(2), 2, -1, ",")
    FldLenLst = BuildCleanItems(CfgTool.CfgTab(1).GetLineValues(3), 2, -1, ",")
    DateFields = "," & BuildCleanItems(CfgTool.CfgTab(1).GetLineValues(10), 2, MaxItem, ",") & ","
    TimeFields = "," & BuildCleanItems(CfgTool.CfgTab(1).GetLineValues(11), 2, MaxItem, ",") & ","
    TimeChainInfo = BuildCleanItems(CfgTool.CfgTab(1).GetLineValues(13), 2, -1, ",")
    MainPeriodCols = BuildCleanItems(CfgTool.CfgTab(1).GetLineValues(12), 2, 7, ",")
    CheckViaList = BuildCleanItems(CfgTool.CfgTab(1).GetLineValues(14), 2, 7, ",")
    AutoReplaceAll = BuildCleanItems(CfgTool.CfgTab(1).GetLineValues(8), 2, MaxItem, ",")
    AutoReplaceAll = Replace(AutoReplaceAll, "-99", "", 1, -1, vbBinaryCompare)
    CheckRotation = BuildCleanItems(CfgTool.CfgTab(1).GetLineValues(15), 2, 3, ",")
    ChkBasDat = BuildCleanItems(CfgTool.CfgTab(1).GetLineValues(9), 2, MaxItem, ",")
    ChkBasDat = Replace(ChkBasDat, "|", ",", 1, -1, vbBinaryCompare)
    ExtractSlotLine = BuildCleanItems(CfgTool.CfgTab(1).GetLineValues(25), 2, 25, ",")
    ExtractSlotLine = Replace(ExtractSlotLine, "-99", "''", 1, -1, vbBinaryCompare)
    InitPeriodCheck MainPeriodCols, True
    
    'STILL HARD CODED PARTS
    '======================
    DateFieldFormat = "SSIM2"
    ExtractSystemType = 1
    DefaultFtyp = "S"
End Sub

Public Function BuildCleanItems(RecData As String, FirstItem As Long, LastItem As Long, RecDeli As String) As String
    Dim Result As String
    Dim CurItm As Long
    Dim ItmData As String
    Result = ""
    CurItm = FirstItem
    ItmData = GetRealItem(RecData, CurItm, RecDeli)
    While ((ItmData <> "") And (ItmData <> "-99")) Or (CurItm <= LastItem)
        If ItmData <> "" Then Result = Result & RecDeli & ItmData
        CurItm = CurItm + 1
        ItmData = GetRealItem(RecData, CurItm, RecDeli)
    Wend
    Result = Mid(Result, 2)
    BuildCleanItems = Result
End Function
