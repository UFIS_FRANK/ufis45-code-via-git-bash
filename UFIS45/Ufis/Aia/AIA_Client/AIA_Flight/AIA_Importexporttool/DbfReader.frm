VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomctl.ocx"
Object = "{64E8E382-05E2-11D2-9B1D-9B0630BC8F12}#1.0#0"; "tab.ocx"
Begin VB.Form DbfReader 
   Caption         =   "UFIS DBF Reader"
   ClientHeight    =   5625
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   10380
   Icon            =   "DbfReader.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   5625
   ScaleWidth      =   10380
   StartUpPosition =   3  'Windows Default
   Begin MSComctlLib.StatusBar StatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   4
      Top             =   5340
      Width           =   10380
      _ExtentX        =   18309
      _ExtentY        =   503
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   2
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   15240
         EndProperty
      EndProperty
   End
   Begin VB.CheckBox chkWork 
      Caption         =   "Close"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   1
      Left            =   1770
      Style           =   1  'Graphical
      TabIndex        =   3
      Top             =   30
      Width           =   855
   End
   Begin VB.CheckBox chkWork 
      Caption         =   "Load"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   0
      Left            =   30
      Style           =   1  'Graphical
      TabIndex        =   2
      Top             =   30
      Width           =   855
   End
   Begin VB.TextBox UseFileName 
      BackColor       =   &H00C0C0C0&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   30
      TabIndex        =   1
      Top             =   360
      Width           =   9135
   End
   Begin TABLib.TAB DataTab 
      Height          =   4515
      Left            =   30
      TabIndex        =   0
      Top             =   720
      Width           =   10155
      _Version        =   65536
      _ExtentX        =   17912
      _ExtentY        =   7964
      _StockProps     =   0
   End
   Begin VB.CheckBox chkWork 
      Caption         =   "All"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   2
      Left            =   900
      Style           =   1  'Graphical
      TabIndex        =   5
      Top             =   30
      Width           =   855
   End
End
Attribute VB_Name = "DbfReader"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private Type DBF_HEADER
    DbfVers As String * 4
    DbfRecCnt As Integer
    DbfRecCntx As Integer
    DbfHdrLen As Integer
    DbfRecLen As Integer
    DbfEtc As String * 20
End Type
Private Type DBF_FIELD
    DbfFldName As String * 11
    DbfFldType As String * 1
    DbfFldEtc1 As String * 4
    DbfFldLen As String * 1
    DbfFldDig As String * 1
    DbfFldEtc2 As String * 14
End Type
Private Type DAT_FIELD
    FldNam As String * 16
    FldTyp As String * 4
    FldDig As Integer
    FldLen As Integer
    FldPos As Integer
    FldEnd As Integer
End Type
Public Sub OpenDbfFile(DbfFileName As String, MyParent As Form, ShowMe As Boolean)
    UseFileName.Text = DbfFileName
    If ShowMe Then
        Me.Top = MyParent.Top + 150 * Screen.TwipsPerPixelY
        Me.Left = MyParent.Left + 150 * Screen.TwipsPerPixelX
        Me.Show , MyParent
    End If
    chkWork(2).Value = 0
    chkWork(0).Value = 1
End Sub
Private Sub ReadDbfFile()
    Dim CurFileName As String
    Dim DbfFile As Integer
    Dim FldCnt As Integer
    Dim CurFld As Integer
    Dim ChrPos As Integer
    Dim FilPos As Long
    Dim CurRec As Long
    Dim LinCnt As Long
    Dim FldList As String
    Dim LenList As String
    Dim tmpFldNam As String
    Dim tmpFldLen As Integer
    Dim tmpFldPos As Integer
    Dim tmpDbfRec As String
    Dim tmpFldDat As String
    Dim tmpDatRec As String
    Dim DbfHeaderLine As DBF_HEADER
    Dim DbfFieldLine As DBF_FIELD
    Dim DbfDataField(256) As DAT_FIELD
    CurFileName = UseFileName.Text
    If UCase(Right(Dir(CurFileName), 4)) = ".DBF" Then
        Screen.MousePointer = 11
        FldList = ""
        LenList = ""
        DataTab.ResetContent
        DataTab.SetFieldSeparator ","
        DbfFile = FreeFile
        Open CurFileName For Binary As #DbfFile
        Get #DbfFile, 1, DbfHeaderLine
        FldCnt = (DbfHeaderLine.DbfHdrLen / 32) - 1
        tmpFldPos = 2
        For CurFld = 1 To FldCnt
            Get #DbfFile, , DbfFieldLine
            tmpFldNam = Left(DbfFieldLine.DbfFldName, 11)
            ChrPos = InStr(tmpFldNam, Chr(0))
            If ChrPos > 1 Then tmpFldNam = Left(tmpFldNam, ChrPos - 1)
            tmpFldNam = Trim(tmpFldNam)
            FldList = FldList & tmpFldNam & ","
            DbfDataField(CurFld).FldNam = tmpFldNam
            tmpFldLen = Asc(DbfFieldLine.DbfFldLen)
            DbfDataField(CurFld).FldLen = tmpFldLen
            LenList = LenList & Trim(Str(tmpFldLen * Screen.TwipsPerPixelX)) & ","
            DbfDataField(CurFld).FldPos = tmpFldPos
            tmpFldPos = tmpFldPos + tmpFldLen - 1
            DbfDataField(CurFld).FldEnd = tmpFldPos
            tmpFldPos = tmpFldPos + 1
        Next
        DataTab.HeaderString = FldList
        DataTab.HeaderLengthString = LenList
        DataTab.SetFieldSeparator ";"
        DataTab.AutoSizeByHeader = True
        DataTab.ShowVertScroller False
        DataTab.ShowHorzScroller True
        DataTab.HeaderFontSize = 17
        DataTab.FontSize = 17
        DataTab.LineHeight = 17
        DataTab.SetTabFontBold True
        DataTab.RedrawTab
        Me.Refresh
        LinCnt = 0
        FilPos = DbfHeaderLine.DbfHdrLen + 1 - DbfHeaderLine.DbfRecLen
        For CurRec = 1 To DbfHeaderLine.DbfRecCnt
            FilPos = FilPos + DbfHeaderLine.DbfRecLen
            tmpDbfRec = Space(DbfHeaderLine.DbfRecLen)
            Get #DbfFile, FilPos, tmpDbfRec
            If (Left(tmpDbfRec, 1) <> "*") Or (chkWork(2).Value = 1) Then
                tmpDatRec = ""
                For CurFld = 1 To FldCnt
                    tmpFldDat = Mid(tmpDbfRec, DbfDataField(CurFld).FldPos, DbfDataField(CurFld).FldLen)
                    tmpFldDat = Replace(tmpFldDat, ";", ",", 1, -1, vbBinaryCompare)
                    tmpDatRec = tmpDatRec & tmpFldDat & ";"
                Next
                DataTab.InsertTextLine tmpDatRec, False
                If Left(tmpDbfRec, 1) = "*" Then
                    DataTab.SetLineColor LinCnt, vbBlack, LightRed
                    DataTab.SetLineStatusValue LinCnt, 1
                End If
                LinCnt = LinCnt + 1
                If LinCnt Mod 30 = 0 Then
                    DataTab.OnVScrollTo LinCnt - 30
                    DataTab.RedrawTab
                    Me.Refresh
                End If
            End If
        Next
        Close DbfFile
        DataTab.AutoSizeColumns
        DataTab.ShowVertScroller True
        DataTab.OnVScrollTo 0
        DataTab.RedrawTab
        Me.Refresh
        Screen.MousePointer = 0
    Else
        MsgBox "File Not Found." & vbNewLine & CurFileName
    End If
End Sub

Private Sub chkWork_Click(Index As Integer)
    If chkWork(Index).Value = 1 Then
        chkWork(Index).BackColor = LightestGreen
        Select Case Index
            Case 0
                ReadDbfFile
                chkWork(Index).Value = 0
                Me.Refresh
            Case 1
                chkWork(Index).Value = 0
                Me.Hide
            Case Else
        End Select
    Else
        chkWork(Index).BackColor = vbButtonFace
    End If
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyF1 Then DisplayHtmHelpFile "", Me.Name, Me.ActiveControl.Name
End Sub

Private Sub Form_Load()
    DataTab.HeaderString = "File Content"
    DataTab.HeaderLengthString = "1000"
    DataTab.ResetContent
    DataTab.RedrawTab
End Sub

Private Sub Form_Resize()
    Dim NewVal As Long
    NewVal = Me.ScaleHeight - DataTab.Top - StatusBar1.Height - 3 * Screen.TwipsPerPixelY
    If NewVal > 250 Then DataTab.Height = NewVal
    NewVal = Me.ScaleWidth - (DataTab.Left * 2)
    If NewVal > 250 Then
        DataTab.Width = NewVal
        UseFileName.Width = NewVal
    End If
End Sub

