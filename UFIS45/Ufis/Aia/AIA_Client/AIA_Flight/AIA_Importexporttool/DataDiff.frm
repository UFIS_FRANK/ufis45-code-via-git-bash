VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.ocx"
Object = "{64E8E382-05E2-11D2-9B1D-9B0630BC8F12}#1.0#0"; "TAB.ocx"
Begin VB.Form DataDiff 
   Caption         =   "Preview/Import Analizer"
   ClientHeight    =   1905
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   7950
   BeginProperty Font 
      Name            =   "Courier New"
      Size            =   8.25
      Charset         =   161
      Weight          =   700
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "DataDiff.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   1905
   ScaleWidth      =   7950
   ShowInTaskbar   =   0   'False
   Begin MSComctlLib.StatusBar StatusBar 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   4
      Top             =   1620
      Width           =   7950
      _ExtentX        =   14023
      _ExtentY        =   503
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   2
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   10954
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
         EndProperty
      EndProperty
   End
   Begin VB.CheckBox chkWork 
      Caption         =   "Save As"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   2
      Left            =   900
      Style           =   1  'Graphical
      TabIndex        =   3
      Top             =   60
      Width           =   855
   End
   Begin VB.CheckBox chkWork 
      Caption         =   "Compare"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   1
      Left            =   30
      Style           =   1  'Graphical
      TabIndex        =   2
      Top             =   60
      Width           =   855
   End
   Begin VB.CheckBox chkWork 
      Caption         =   "Close"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   0
      Left            =   1770
      Style           =   1  'Graphical
      TabIndex        =   1
      Top             =   60
      Width           =   855
   End
   Begin TABLib.TAB FlightList 
      Height          =   945
      Left            =   30
      TabIndex        =   0
      Top             =   390
      Width           =   4605
      _Version        =   65536
      _ExtentX        =   8123
      _ExtentY        =   1667
      _StockProps     =   64
   End
End
Attribute VB_Name = "DataDiff"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private CurHeadLength As String
Private IsInitialized As Boolean
Dim lastLineNo As Long
Dim lastBackColor As Long
Dim lastForeColor As Long

Public Sub InitFlightList(ipLines As Integer, ipFirstCall As Boolean)
Dim i As Integer
Dim tmpLen
Dim HdrLenLst As String
Dim tmpHeader As String
Dim ActColLst As String
Dim FldTypLst As String

    Me.FontSize = MySetUp.FontSlider.Value
    '"xxxx,xxxx,xxxx,xxxx,FTYP,FLNO,ORG3,STOD,VIA3,DES3,STOA,ACT3,STYP,"
    tmpHeader = "LINE,FLIGHT KEY,OP DAY,I,F,FLNO,ORG,STOD,VIA,DES,STOA,ACT,N,WKSN,CDAT,TRA.CODE,REMARK"
    ActColLst = "4   ,10        ,7     ,1,1,9   ,3  ,12  ,3  ,3  ,12  ,3  ,1,10  ,12  ,10"
    HdrLenLst = ""
    FldTypLst = ""
    i = 0
    Do
        i = i + 1
        tmpLen = GetItem(ActColLst, i, ",")
        If tmpLen <> "" Then
            If tmpLen > 0 Then
                HdrLenLst = HdrLenLst & TextWidth(Space(tmpLen)) / Screen.TwipsPerPixelX + 6 & ","
            Else
                HdrLenLst = HdrLenLst & "1,"
            End If
            FldTypLst = FldTypLst & "L,"
        End If
    Loop While tmpLen <> ""
    HdrLenLst = HdrLenLst & "1000"
    FldTypLst = FldTypLst & "L"
    SetItem FldTypLst, 1, ",", "R"
    If ipFirstCall Then FlightList.ResetContent
    FlightList.FontName = "Courier New"
    FlightList.SetTabFontBold True
    FlightList.ShowHorzScroller True
    FlightList.HeaderFontSize = MySetUp.FontSlider.Value + 6
    FlightList.FontSize = MySetUp.FontSlider.Value + 6
    FlightList.LineHeight = MySetUp.FontSlider.Value + 7
    FlightList.HeaderLengthString = HdrLenLst
    FlightList.HeaderString = tmpHeader
    FlightList.ColumnAlignmentString = FldTypLst
    FlightList.GridLineColor = DarkGray
    'HardCoded
    FlightList.DateTimeSetColumn 2
    FlightList.DateTimeSetInputFormatString 2, "YYYYMMDD"
    FlightList.DateTimeSetOutputFormatString 2, "DDMMMYY"
    FlightList.DateTimeSetColumn 7
    FlightList.DateTimeSetInputFormatString 7, "YYYYMMDDhhmm"
    FlightList.DateTimeSetOutputFormatString 7, "DDMMMYY'/'hhmm"
    FlightList.DateTimeSetColumn 10
    FlightList.DateTimeSetInputFormatString 10, "YYYYMMDDhhmm"
    FlightList.DateTimeSetOutputFormatString 10, "DDMMMYY'/'hhmm"
    FlightList.DateTimeSetColumn 14
    FlightList.DateTimeSetInputFormatString 14, "YYYYMMDDhhmm"
    FlightList.DateTimeSetOutputFormatString 14, "DDMMMYY'/'hhmm"
    If ipFirstCall And Not IsInitialized Then
        FlightList.Height = ipLines * FlightList.LineHeight * Screen.TwipsPerPixelY
        Me.Height = FlightList.Top + FlightList.Height + StatusBar.Height + 9 * Screen.TwipsPerPixelY
        'IsInitialized = True
    End If
    FlightList.SetMainHeaderValues "1,2,1,9,2,1,1", ",RELATED FLIGHTS,P,AFFECTED FLIGHT DATA FIELDS,AFFECTED BY,ACTION,BLACKBOX,REMARK", "12632256,12632256,12632256,12632256,12632256,12632256,12632256"
    FlightList.MainHeader = True
CurHeadLength = ActColLst
    Me.Refresh
End Sub

Private Sub chkWork_Click(Index As Integer)
    Select Case Index
        Case 0
            'Close
            If chkWork(Index) = 1 Then Unload Me
        Case 1
            If chkWork(Index) = 1 Then
                chkWork(Index) = 0
            End If
        Case 2
            'Save As
            If chkWork(Index).Value = 1 Then
                MainDialog.SaveLoadedFile Index, DataDiff.FlightList
                chkWork(Index).Value = 0
            End If
    End Select
End Sub

Public Sub LoadDiffData()
'    Dim CurLine As Long
'    Dim MaxLine As Long
'    Dim NewLine As Long
'    Dim tmpRec As String
'    Dim tmpFldNam As String
'    Dim OldFldVal As String
'    Dim NewFldVal As String
'    Dim tmpOldDat As String
'    Dim tmpNewDat As String
'    Dim CurLineKey As String
'    Dim OldLineKey As String
'    Dim ValidFields As String
'    Dim tmpImpKey As String
'    Dim FldItemNo As Integer
'    Dim ListLine As Long
'    Dim ForeColor As Long
'    Dim BackColor As Long
'
'    Screen.MousePointer = 11
'    FlightList.ResetContent
'    ValidFields = "xxxx,xxxx,xxxx,xxxx,FTYP,FLNO,ORG3,STOD,VIA3,DES3,STOA,ACT3,STYP,"
'    ValidFields = Replace(ValidFields, "STYP", MainDialog.UseStypField, 1, -1, vbBinaryCompare)
'
'    MaxLine = PreView.PreviewList.GetLineCount '- 1
'    NewLine = 0
'    ListLine = 0
'    OldLineKey = ""
'    For CurLine = 0 To MaxLine
'        tmpRec = PreView.PreviewList.GetLineValues(CurLine)
'        CurLineKey = GetItem(tmpRec, 1, ",") & "/" & GetItem(tmpRec, 2, ",")
'        If CurLineKey <> OldLineKey Then
'            If OldLineKey <> "" Then
'                If NewLine Mod 2 = 0 Then
'                    If tmpImpKey = "P" Then
'                        ForeColor = vbBlack
'                        BackColor = LightGray
'                    Else
'                        ForeColor = vbBlack
'                        BackColor = LightYellow
'                    End If
'                Else
'                    If tmpImpKey = "P" Then
'                        ForeColor = vbBlack
'                        BackColor = vbWhite
'                    Else
'                        ForeColor = vbBlack
'                        BackColor = LightestYellow
'                    End If
'                End If
'                FlightList.InsertTextLine tmpNewDat, False
'                FlightList.SetLineColor ListLine, ForeColor, BackColor
'                ListLine = ListLine + 1
'                FlightList.InsertTextLine tmpOldDat, False
'                FlightList.SetLineColor ListLine, ForeColor, BackColor
'                ListLine = ListLine + 1
'            End If
'            NewLine = NewLine + 1
'            tmpImpKey = GetItem(tmpRec, 3, ",")
'            tmpNewDat = Trim(Str(NewLine)) & ","
'            tmpNewDat = tmpNewDat & Replace(GetItem(tmpRec, 5, ","), "#", " ", 1, 1, vbBinaryCompare) & ","
'            tmpNewDat = tmpNewDat & GetItem(tmpRec, 4, ",") & ","
'            tmpNewDat = tmpNewDat & tmpImpKey & ","
'            NewFldVal = GetItem(tmpRec, 9, ",")
'            SetItem tmpNewDat, 14, ",", NewFldVal
'            NewFldVal = GetItem(tmpRec, 10, ",")
'            SetItem tmpNewDat, 15, ",", NewFldVal
'            NewFldVal = GetItem(tmpRec, 1, ",")
'            SetItem tmpNewDat, 16, ",", NewFldVal
'            NewFldVal = GetItem(tmpRec, 2, ",")
'            SetItem tmpNewDat, 17, ",", NewFldVal
'            tmpOldDat = ","
'            SetItem tmpOldDat, 16, ",", ""
'            OldLineKey = CurLineKey
'        End If
'        tmpFldNam = GetItem(tmpRec, 6, ",")
'        If tmpFldNam <> "" Then
'            OldFldVal = GetItem(tmpRec, 7, ",")
'            NewFldVal = GetItem(tmpRec, 8, ",")
'            FldItemNo = GetItemNo(ValidFields, tmpFldNam)
'            If FldItemNo <= 0 Then
'                If tmpFldNam = "VIAL" Then
'                    OldFldVal = Mid(OldFldVal, 2, 3)
'                    NewFldVal = Mid(NewFldVal, 2, 3)
'                    FldItemNo = 9
'                End If
'            End If
'            If FldItemNo > 0 Then
'                SetItem tmpNewDat, FldItemNo, ",", NewFldVal
'                SetItem tmpOldDat, FldItemNo, ",", OldFldVal
'            End If
'
'        End If
'    Next
'    FlightList.RedrawTab
'    Screen.MousePointer = 0
End Sub

Private Sub FlightList_RowSelectionChanged(ByVal LineNo As Long, ByVal Selected As Boolean)
    'If Selected = True Then
    '    If lastLineNo >= 0 Then FlightList.SetLineColor lastLineNo, lastForeColor, lastBackColor
    '    If LineNo Mod 2 = 0 Then lastLineNo = LineNo + 1 Else lastLineNo = LineNo - 1
    '    FlightList.GetLineColor lastLineNo, lastForeColor, lastBackColor
    '    FlightList.SetLineColor lastLineNo, vbWhite, NormalBlue
    'End If
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyF1 Then DisplayHtmHelpFile "", Me.Name, Me.ActiveControl.Name
End Sub

Private Sub Form_Load()
    Me.Left = MainDialog.Left
    Me.Top = MainDialog.Top
    Me.Width = MainDialog.Width
    InitFlightList 25, True
    lastLineNo = -1
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    PreView.chkWork(3).Value = 0
End Sub

Private Sub Form_Resize()
Dim NewVal As Long
    NewVal = Me.ScaleHeight - FlightList.Top - StatusBar.Height - 3 * Screen.TwipsPerPixelY
    If NewVal > 50 Then
        FlightList.Height = NewVal
    End If
    NewVal = Me.ScaleWidth - 2 * FlightList.Left
    If NewVal > 50 Then FlightList.Width = NewVal
End Sub

