VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{64E8E382-05E2-11D2-9B1D-9B0630BC8F12}#1.0#0"; "tab.ocx"
Begin VB.Form RecFilter 
   Caption         =   "Record Filter Preset"
   ClientHeight    =   5640
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   9300
   BeginProperty Font 
      Name            =   "Courier New"
      Size            =   11.25
      Charset         =   161
      Weight          =   700
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "RecFilter.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   5640
   ScaleWidth      =   9300
   ShowInTaskbar   =   0   'False
   Begin VB.CheckBox chkTool 
      Caption         =   "Preview"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   6
      Left            =   2640
      Style           =   1  'Graphical
      TabIndex        =   62
      Top             =   30
      Width           =   855
   End
   Begin VB.CheckBox chkTool 
      Caption         =   "-Vias-"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   5
      Left            =   3510
      Style           =   1  'Graphical
      TabIndex        =   61
      Top             =   360
      Width           =   855
   End
   Begin TABLib.TAB HelperTab 
      Height          =   825
      Index           =   0
      Left            =   1140
      TabIndex        =   58
      Top             =   4470
      Visible         =   0   'False
      Width           =   1245
      _Version        =   65536
      _ExtentX        =   2196
      _ExtentY        =   1455
      _StockProps     =   64
   End
   Begin VB.Frame Frame1 
      BorderStyle     =   0  'None
      Height          =   285
      Left            =   30
      TabIndex        =   41
      Top             =   360
      Width           =   3585
      Begin VB.CheckBox chkWkDay 
         Caption         =   "All"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   8
         Left            =   0
         Style           =   1  'Graphical
         TabIndex        =   50
         ToolTipText     =   "Select all frequency days"
         Top             =   0
         Width           =   765
      End
      Begin VB.CheckBox chkWkDay 
         Caption         =   "7"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   7
         Left            =   2400
         Style           =   1  'Graphical
         TabIndex        =   49
         ToolTipText     =   "Frequency filter for Sunday"
         Top             =   0
         Width           =   255
      End
      Begin VB.CheckBox chkWkDay 
         Caption         =   "6"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   6
         Left            =   2130
         Style           =   1  'Graphical
         TabIndex        =   48
         ToolTipText     =   "Frequency filter for Saturday"
         Top             =   0
         Width           =   255
      End
      Begin VB.CheckBox chkWkDay 
         Caption         =   "5"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   5
         Left            =   1860
         Style           =   1  'Graphical
         TabIndex        =   47
         ToolTipText     =   "Frequency filter for Friday"
         Top             =   0
         Width           =   255
      End
      Begin VB.CheckBox chkWkDay 
         Caption         =   "4"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   4
         Left            =   1590
         Style           =   1  'Graphical
         TabIndex        =   46
         ToolTipText     =   "Frequency filter for Thursday"
         Top             =   0
         Width           =   255
      End
      Begin VB.CheckBox chkWkDay 
         Caption         =   "3"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   3
         Left            =   1320
         Style           =   1  'Graphical
         TabIndex        =   45
         ToolTipText     =   "Frequency filter for Wednesday"
         Top             =   0
         Width           =   255
      End
      Begin VB.CheckBox chkWkDay 
         Caption         =   "2"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   2
         Left            =   1050
         Style           =   1  'Graphical
         TabIndex        =   44
         ToolTipText     =   "Frequency filter for Thuesday"
         Top             =   0
         Width           =   255
      End
      Begin VB.CheckBox chkWkDay 
         Caption         =   "1"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   1
         Left            =   780
         Style           =   1  'Graphical
         TabIndex        =   43
         ToolTipText     =   "Frequency filter for Monday"
         Top             =   0
         Width           =   255
      End
      Begin VB.CheckBox chkWkDay 
         Caption         =   "None"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   0
         Left            =   2670
         Style           =   1  'Graphical
         TabIndex        =   42
         Tag             =   "......."
         ToolTipText     =   "Unselect all frequency days"
         Top             =   0
         Width           =   795
      End
   End
   Begin VB.CheckBox chkTool 
      Caption         =   "Compare"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   4
      Left            =   900
      Style           =   1  'Graphical
      TabIndex        =   40
      Top             =   30
      Width           =   855
   End
   Begin MSComctlLib.StatusBar StatusBar 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   22
      Top             =   5355
      Width           =   9300
      _ExtentX        =   16404
      _ExtentY        =   503
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   3
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            Object.Width           =   1632
            MinWidth        =   1632
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            Object.Width           =   1623
            MinWidth        =   1623
         EndProperty
         BeginProperty Panel3 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   12568
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.CheckBox chkTool 
      Caption         =   "Load"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   2
      Left            =   30
      Style           =   1  'Graphical
      TabIndex        =   0
      Top             =   30
      Width           =   855
   End
   Begin VB.CheckBox chkTool 
      Caption         =   "Refresh"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   1
      Left            =   1770
      Style           =   1  'Graphical
      TabIndex        =   1
      Top             =   30
      Width           =   855
   End
   Begin VB.CheckBox chkTool 
      Caption         =   "Close"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   0
      Left            =   4380
      Style           =   1  'Graphical
      TabIndex        =   3
      Top             =   30
      Width           =   855
   End
   Begin VB.CheckBox chkWork 
      Caption         =   "OP Days"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   4
      Left            =   9450
      Style           =   1  'Graphical
      TabIndex        =   21
      ToolTipText     =   "Activate an date filter "
      Top             =   750
      Width           =   1275
   End
   Begin VB.Frame FilterPanel 
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3795
      Index           =   4
      Left            =   9390
      TabIndex        =   20
      Top             =   600
      Width           =   2685
      Begin VB.TextBox txtFilterVal 
         Alignment       =   2  'Center
         BackColor       =   &H00E0E0E0&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   8
         Left            =   1350
         TabIndex        =   55
         Top             =   150
         Width           =   1275
      End
      Begin TABLib.TAB FilterTab 
         Height          =   2925
         Index           =   8
         Left            =   60
         TabIndex        =   12
         Top             =   480
         Width           =   1275
         _Version        =   65536
         _ExtentX        =   2249
         _ExtentY        =   5159
         _StockProps     =   64
         Columns         =   10
      End
      Begin TABLib.TAB FilterTab 
         Height          =   2925
         Index           =   9
         Left            =   1350
         TabIndex        =   13
         Top             =   480
         Width           =   1275
         _Version        =   65536
         _ExtentX        =   2249
         _ExtentY        =   5159
         _StockProps     =   64
         Columns         =   10
      End
      Begin VB.Frame ButtonPanel 
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   4
         Left            =   150
         TabIndex        =   27
         Top             =   3450
         Width           =   2385
         Begin VB.CheckBox chkFilterSet 
            Caption         =   "0"
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Index           =   9
            Left            =   1200
            Style           =   1  'Graphical
            TabIndex        =   37
            Top             =   0
            Width           =   855
         End
         Begin VB.CheckBox chkFilterSet 
            Caption         =   "0"
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Index           =   8
            Left            =   330
            Style           =   1  'Graphical
            TabIndex        =   36
            Top             =   0
            Width           =   855
         End
      End
   End
   Begin VB.CheckBox chkWork 
      Caption         =   "Aircraft"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   3
      Left            =   7440
      Style           =   1  'Graphical
      TabIndex        =   7
      ToolTipText     =   "Activate an aircraft type filter "
      Top             =   750
      Width           =   855
   End
   Begin VB.CheckBox chkWork 
      Caption         =   "Airport"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   2
      Left            =   5430
      Style           =   1  'Graphical
      TabIndex        =   6
      ToolTipText     =   "Activate an airport filter "
      Top             =   750
      Width           =   855
   End
   Begin VB.CheckBox chkWork 
      Caption         =   "Flight"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   1
      Left            =   2100
      Style           =   1  'Graphical
      TabIndex        =   5
      ToolTipText     =   "Activate a flight filter "
      Top             =   750
      Width           =   1515
   End
   Begin VB.CheckBox chkWork 
      Caption         =   "Airline"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   0
      Left            =   90
      Style           =   1  'Graphical
      TabIndex        =   4
      ToolTipText     =   "Activate an airline filter "
      Top             =   750
      Width           =   855
   End
   Begin VB.Frame FilterPanel 
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3795
      Index           =   3
      Left            =   7380
      TabIndex        =   18
      Top             =   600
      Width           =   1845
      Begin VB.TextBox txtFilterVal 
         Alignment       =   2  'Center
         BackColor       =   &H00E0E0E0&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   6
         Left            =   930
         TabIndex        =   54
         Top             =   150
         Width           =   855
      End
      Begin TABLib.TAB FilterTab 
         Height          =   2925
         Index           =   7
         Left            =   930
         TabIndex        =   19
         Top             =   480
         Width           =   855
         _Version        =   65536
         _ExtentX        =   1508
         _ExtentY        =   5159
         _StockProps     =   64
         Columns         =   10
      End
      Begin VB.Frame ButtonPanel 
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   3
         Left            =   60
         TabIndex        =   26
         Top             =   3450
         Width           =   1725
         Begin VB.CheckBox chkFilterSet 
            Caption         =   "0"
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Index           =   7
            Left            =   870
            Style           =   1  'Graphical
            TabIndex        =   35
            Top             =   0
            Width           =   855
         End
         Begin VB.CheckBox chkFilterSet 
            Caption         =   "0"
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Index           =   6
            Left            =   0
            Style           =   1  'Graphical
            TabIndex        =   34
            Top             =   0
            Width           =   855
         End
      End
      Begin TABLib.TAB FilterTab 
         Height          =   2925
         Index           =   6
         Left            =   60
         TabIndex        =   39
         Top             =   480
         Width           =   855
         _Version        =   65536
         _ExtentX        =   1508
         _ExtentY        =   5159
         _StockProps     =   64
         Columns         =   10
      End
   End
   Begin VB.Frame FilterPanel 
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3795
      Index           =   2
      Left            =   5370
      TabIndex        =   16
      Top             =   600
      Width           =   1845
      Begin VB.TextBox txtFilterVal 
         Alignment       =   2  'Center
         BackColor       =   &H00E0E0E0&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   4
         Left            =   930
         TabIndex        =   53
         Top             =   150
         Width           =   855
      End
      Begin TABLib.TAB FilterTab 
         Height          =   2925
         Index           =   4
         Left            =   60
         TabIndex        =   11
         Top             =   480
         Width           =   855
         _Version        =   65536
         _ExtentX        =   1508
         _ExtentY        =   5159
         _StockProps     =   64
         Columns         =   10
      End
      Begin TABLib.TAB FilterTab 
         Height          =   2925
         Index           =   5
         Left            =   930
         TabIndex        =   17
         Top             =   480
         Width           =   855
         _Version        =   65536
         _ExtentX        =   1508
         _ExtentY        =   5159
         _StockProps     =   64
         Columns         =   10
      End
      Begin VB.Frame ButtonPanel 
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   2
         Left            =   60
         TabIndex        =   25
         Top             =   3450
         Width           =   1725
         Begin VB.CheckBox chkFilterSet 
            Caption         =   "0"
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Index           =   5
            Left            =   870
            Style           =   1  'Graphical
            TabIndex        =   33
            Top             =   0
            Width           =   855
         End
         Begin VB.CheckBox chkFilterSet 
            Caption         =   "0"
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Index           =   4
            Left            =   0
            Style           =   1  'Graphical
            TabIndex        =   32
            Top             =   0
            Width           =   855
         End
      End
   End
   Begin VB.CheckBox chkTool 
      Caption         =   "Adjust"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   3
      Left            =   3510
      Style           =   1  'Graphical
      TabIndex        =   2
      Top             =   30
      Width           =   855
   End
   Begin VB.Frame FilterPanel 
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3795
      Index           =   1
      Left            =   2040
      TabIndex        =   15
      Top             =   600
      Width           =   3165
      Begin VB.TextBox txtFilterVal 
         Alignment       =   2  'Center
         BackColor       =   &H00E0E0E0&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   2
         Left            =   1590
         TabIndex        =   52
         Top             =   150
         Width           =   1515
      End
      Begin TABLib.TAB FilterTab 
         Height          =   2925
         Index           =   2
         Left            =   60
         TabIndex        =   9
         Top             =   480
         Width           =   1500
         _Version        =   65536
         _ExtentX        =   2646
         _ExtentY        =   5159
         _StockProps     =   64
         Columns         =   10
      End
      Begin TABLib.TAB FilterTab 
         Height          =   2925
         Index           =   3
         Left            =   1590
         TabIndex        =   10
         Top             =   480
         Width           =   1500
         _Version        =   65536
         _ExtentX        =   2646
         _ExtentY        =   5159
         _StockProps     =   64
         Columns         =   10
      End
      Begin VB.Frame ButtonPanel 
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   1
         Left            =   60
         TabIndex        =   24
         Top             =   3450
         Width           =   2385
         Begin VB.CheckBox chkFilterSet 
            Caption         =   "0"
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Index           =   3
            Left            =   1320
            Style           =   1  'Graphical
            TabIndex        =   31
            Top             =   0
            Width           =   855
         End
         Begin VB.CheckBox chkFilterSet 
            Caption         =   "0"
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Index           =   2
            Left            =   450
            Style           =   1  'Graphical
            TabIndex        =   30
            Top             =   0
            Width           =   855
         End
      End
   End
   Begin VB.Frame FilterPanel 
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3795
      Index           =   0
      Left            =   30
      TabIndex        =   14
      Top             =   600
      Width           =   1845
      Begin VB.TextBox txtFilterVal 
         Alignment       =   2  'Center
         BackColor       =   &H00E0E0E0&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   0
         Left            =   930
         TabIndex        =   51
         Top             =   150
         Width           =   855
      End
      Begin TABLib.TAB FilterTab 
         Height          =   2925
         Index           =   0
         Left            =   60
         TabIndex        =   8
         Top             =   480
         Width           =   855
         _Version        =   65536
         _ExtentX        =   1508
         _ExtentY        =   5159
         _StockProps     =   64
         Columns         =   10
      End
      Begin VB.Frame ButtonPanel 
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   0
         Left            =   60
         TabIndex        =   23
         Top             =   3450
         Width           =   1725
         Begin VB.CheckBox chkFilterSet 
            Caption         =   "0"
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Index           =   1
            Left            =   870
            Style           =   1  'Graphical
            TabIndex        =   29
            Top             =   0
            Width           =   855
         End
         Begin VB.CheckBox chkFilterSet 
            Caption         =   "0"
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Index           =   0
            Left            =   0
            Style           =   1  'Graphical
            TabIndex        =   28
            Top             =   0
            Width           =   855
         End
      End
      Begin TABLib.TAB FilterTab 
         Height          =   2925
         Index           =   1
         Left            =   930
         TabIndex        =   38
         Top             =   480
         Width           =   855
         _Version        =   65536
         _ExtentX        =   1508
         _ExtentY        =   5159
         _StockProps     =   64
         Columns         =   10
      End
   End
   Begin TABLib.TAB FullFilter 
      Height          =   825
      Index           =   0
      Left            =   4530
      TabIndex        =   56
      Top             =   4470
      Visible         =   0   'False
      Width           =   1215
      _Version        =   65536
      _ExtentX        =   2143
      _ExtentY        =   1455
      _StockProps     =   64
   End
   Begin TABLib.TAB FullFilter 
      Height          =   825
      Index           =   1
      Left            =   5910
      TabIndex        =   57
      Top             =   4470
      Visible         =   0   'False
      Width           =   1425
      _Version        =   65536
      _ExtentX        =   2514
      _ExtentY        =   1455
      _StockProps     =   64
   End
   Begin TABLib.TAB FullFilter 
      Height          =   795
      Index           =   2
      Left            =   7560
      TabIndex        =   59
      Top             =   4470
      Visible         =   0   'False
      Width           =   1485
      _Version        =   65536
      _ExtentX        =   2619
      _ExtentY        =   1402
      _StockProps     =   64
   End
   Begin TABLib.TAB HelperTab 
      Height          =   795
      Index           =   1
      Left            =   2520
      TabIndex        =   60
      Top             =   4500
      Visible         =   0   'False
      Width           =   1245
      _Version        =   65536
      _ExtentX        =   2196
      _ExtentY        =   1402
      _StockProps     =   64
   End
End
Attribute VB_Name = "RecFilter"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim FilterItems(4) As String
Dim CheckItems(4) As String
Dim LastLoadFilter As String
Dim InitFilterCall As Boolean

Public Sub InitFilterConfig()
    Dim i As Integer
    Dim KeyCode As String
    Dim FilterCfg As String
    Dim CurTab As Integer
    Dim itemlist As String
    For i = 0 To 4
        If MainDialog.DataSystemType <> "FREE" Then
            KeyCode = "FILTER" & Trim(Str(i + 1))
            FilterCfg = GetIniEntry(myIniFullName, myIniSection, "", KeyCode, "")
        Else
            FilterCfg = ""
            Select Case i
                Case 0  'ALC
                    FilterCfg = MainDialog.BuildCleanItems(CfgTool.CfgTab(1).GetLineValues(19), 2, 25, ",")
                    FilterCfg = Replace(FilterCfg, ":,", ":", 1, -1, vbBinaryCompare)
                    FilterCfg = Replace(FilterCfg, ",", ":", 1, -1, vbBinaryCompare)
                Case 1  'FLNO
                    FilterCfg = MainDialog.BuildCleanItems(CfgTool.CfgTab(1).GetLineValues(20), 2, 25, ",")
                    FilterCfg = Replace(FilterCfg, ":,", ":", 1, -1, vbBinaryCompare)
                    FilterCfg = Replace(FilterCfg, ",:", ":", 1, -1, vbBinaryCompare)
                    FilterCfg = Replace(FilterCfg, "+,", "+", 1, -1, vbBinaryCompare)
                Case 2  'APC
                    FilterCfg = MainDialog.BuildCleanItems(CfgTool.CfgTab(1).GetLineValues(21), 2, 25, ",")
                    FilterCfg = Replace(FilterCfg, ":,", ":", 1, -1, vbBinaryCompare)
                    FilterCfg = Replace(FilterCfg, ",:", ":", 1, -1, vbBinaryCompare)
                Case 3 'ACT
                    FilterCfg = MainDialog.BuildCleanItems(CfgTool.CfgTab(1).GetLineValues(22), 2, 25, ",")
                    FilterCfg = Replace(FilterCfg, ":,", ":", 1, -1, vbBinaryCompare)
                    FilterCfg = Replace(FilterCfg, ",", ":", 1, -1, vbBinaryCompare)
                Case 4
                Case Else
            End Select
        End If
        chkWork(i).Caption = GetItem(FilterCfg, 1, ":")
        CurTab = i * 2
        FilterTab(CurTab).SetHeaderText GetItem(FilterCfg, 2, ":")
        FilterTab(CurTab).ResetContent
        FilterTab(CurTab + 1).SetHeaderText "Filter"
        FilterTab(CurTab + 1).ResetContent
        'we must increase all coloumn numbers by 2
        itemlist = GetItem(FilterCfg, 3, ":")
        FilterItems(i) = ShiftItemValues(itemlist, 2)
        itemlist = GetItem(FilterCfg, 4, ":")
        If itemlist <> "" Then
            CheckItems(i) = ShiftItemValues(itemlist, 2)
        Else
            CheckItems(i) = FilterItems(i)
        End If
    Next
End Sub

Private Function ShiftItemValues(itemlist As String, ShiftVal As Integer) As String
    Dim Result As String
    Dim CurItm As Integer
    Dim ColItm As Integer
    Dim ColNbr As Integer
    Dim tmpItm As String
    Dim tmpCol As String
    Dim NewItm As String
    Dim tmpLen As Integer
    Result = ""
    CurItm = 1
    Do
        'separate items
        tmpItm = GetItem(itemlist, CurItm, ",")
        If tmpItm <> "" Then
            NewItm = ""
            ColItm = 1
            Do
                'separate coloumns
                tmpCol = GetItem(tmpItm, ColItm, "+")
                If tmpCol <> "" Then
                    ColNbr = Val(tmpCol) + ShiftVal
                    NewItm = NewItm & Trim(Str(ColNbr)) & "+"
                End If
                ColItm = ColItm + 1
            Loop While tmpCol <> ""
            If ColItm > 2 Then NewItm = Left(NewItm, Len(NewItm) - 1)
            Result = Result & NewItm & ","
        End If
        CurItm = CurItm + 1
    Loop While tmpItm <> ""
    If CurItm > 2 Then Result = Left(Result, Len(Result) - 1)
    ShiftItemValues = Result
End Function

Private Sub chkFilterSet_Click(Index As Integer)
Dim FromCol As Integer
Dim ToCol As Integer
    If chkFilterSet(Index).Value = 1 Then
        FromCol = Index
        If FromCol Mod 2 = 0 Then
            ToCol = FromCol + 1
        Else
            ToCol = FromCol - 1
        End If
        ShiftAllData FromCol, ToCol
        If chkTool(3).Value = 1 Then AdjustFilterView
        chkFilterSet(Index).Value = 0
    End If
End Sub

Private Sub ShiftAllData(FromCol As Integer, ToCol As Integer)
    Dim ColTxt As String
    MousePointer = 11
    ColTxt = FilterTab(FromCol).SelectDistinct("0", "", "", vbLf, False)
    FilterTab(ToCol).InsertBuffer ColTxt, vbLf
    FilterTab(ToCol).Sort "0", True, True
    FilterTab(ToCol).RedrawTab
    FilterTab(FromCol).ResetContent
    FilterTab(FromCol).RedrawTab
    SetFilterCount FromCol
    SetFilterCount ToCol
    MousePointer = 0
    CheckRecLoadFilter False
End Sub
Private Sub SetFilterCount(ColIdx As Integer)
    Dim tmpCount As Long
    Dim MaxLin As Long
    tmpCount = FilterTab(ColIdx).GetLineCount
    chkFilterSet(ColIdx).Caption = Trim(Str(tmpCount))
    If FilterTab(ColIdx).GetVScrollPos = 0 Then
        MaxLin = FilterTab(ColIdx).Height / (FilterTab(ColIdx).LineHeight * Screen.TwipsPerPixelY) - 1
        If tmpCount > MaxLin Then
            FilterTab(ColIdx).ShowVertScroller True
        Else
            FilterTab(ColIdx).ShowVertScroller False
        End If
    End If
End Sub
Private Sub ShiftData(FromCol As Integer, ToCol As Integer, ActSelLine As Long)
Dim SelLine As Long
Dim TgtLine As Long
Dim Linedata As String
Dim NewData As String
Dim ItmDat As String
    SelLine = ActSelLine
    If SelLine < 0 Then SelLine = FilterTab(FromCol).GetCurrentSelected
    If SelLine >= 0 Then
        Linedata = FilterTab(FromCol).GetLineValues(SelLine)
        ItmDat = GetItem(Linedata, 1, ",")
        If ItmDat <> "" Then
            InsertFilterValue Linedata, ToCol
            FilterTab(FromCol).DeleteLine SelLine
        End If
        SetFilterCount FromCol
        SetFilterCount ToCol
        FilterTab(FromCol).RedrawTab
        FilterTab(ToCol).RedrawTab
    Else
        MyMsgBox.InfoApi 0, "Click on the desired (valid) line and try it again.", "More info"
        If MyMsgBox.CallAskUser(0, 0, 0, "Action Control", "No data selected", "hand", "", UserAnswer) >= 0 Then DoNothing
    End If
End Sub
Private Sub chkTool_Click(Index As Integer)
    If chkTool(Index).Value = 0 Then chkTool(Index).BackColor = vbButtonFace
    Select Case Index
        Case 0
            'Close
            If chkTool(Index).Value = 1 Then
                Me.Tag = ""
                Me.Hide
                PreView.Hide
                FlightExtract.SetFocus
                FlightExtract.chkWork(0).BackColor = vbYellow
                chkTool(Index).Value = 0
            End If
        Case 1
            'Refresh
            If chkTool(Index).Value = 1 Then
                chkTool(3).Value = 0
                If Not StopNestedCalls Then chkTool(5).Value = 0
                InitLists
                chkTool(Index).Value = 0
            End If
        Case 2
            'load
            If chkTool(Index).Value = 1 Then
                CheckRecLoadFilter True
                If Me.Visible = True Then Me.Refresh
                FlightExtract.chkWork(4).Value = 1
                If Me.Visible = True Then Me.Refresh
            End If
        Case 3
            'Adjust
            If chkTool(Index).Value = 1 Then
                chkTool(6).Enabled = True
                chkTool(6).Value = 1
                Me.Refresh
                InitAdjustFilter
                chkWork(4).Enabled = True
            Else
                chkTool(6).Value = 0
                chkTool(6).Enabled = False
                chkTool(1).Value = 1
            End If
        Case 4
            'Compare NOOP
            If chkTool(Index).Value = 1 Then
                FlightExtract.chkWork(13).Value = 1
            Else
                FlightExtract.chkWork(13).Value = 0
            End If
        Case 5
            'Vias
            If (chkTool(Index).Value = 1) Or (chkTool(3).Value = 1) Then
                StopNestedCalls = True
                FullFilter(0).ResetContent
                chkTool(3).Value = 0
                chkTool(3).Value = 1
                StopNestedCalls = False
            End If
        Case 6
            'Preview
            If chkTool(Index).Value = 1 Then
                PreView.Show , FlightExtract
                PreView.Refresh
                If Me.Visible Then Me.SetFocus
            Else
                PreView.Hide
                FlightExtract.Refresh
                If Me.Visible Then Me.SetFocus
            End If
        Case Else
    End Select
    If chkTool(Index).Value = 1 Then chkTool(Index).BackColor = LightestGreen
End Sub

Private Sub BuildFilters()
    Dim ColTxt As String
    Dim TgtCol As Integer
    Dim ItmNbr As Integer
    Dim CurCol As String
    Dim i As Integer
    MousePointer = 11
    For i = 0 To 4
        UfisTools.BlackBox(0).ResetContent
        TgtCol = i * 2
        FilterTab(TgtCol).ResetContent
        FilterTab(TgtCol + 1).ResetContent
        ItmNbr = 1
        Do
            CurCol = GetItem(FilterItems(i), ItmNbr, ",")
            If CurCol <> "" Then
                CurCol = Replace(CurCol, "+", ",", 1, -1, vbBinaryCompare)
                ColTxt = MainDialog.FileData.SelectDistinct(CurCol, "", "", vbLf, True)
                UfisTools.BlackBox(0).InsertBuffer ColTxt, vbLf
            End If
            ItmNbr = ItmNbr + 1
        Loop While CurCol <> ""
        ColTxt = UfisTools.BlackBox(0).SelectDistinct("0", "", "", vbLf, True)
        UfisTools.BlackBox(0).ResetContent
        ColTxt = Replace(ColTxt, (vbLf & vbLf), vbLf, 1, -1, vbBinaryCompare)
        If Left(ColTxt, 1) = vbLf Then ColTxt = Mid(ColTxt, 2)
        If Right(ColTxt, 1) = vbLf Then
            ColTxt = Left(ColTxt, Len(ColTxt) - 1)
        End If
        FilterTab(TgtCol).InsertBuffer ColTxt, vbLf
        FilterTab(TgtCol).Sort "0", True, True
        FilterTab(TgtCol).RedrawTab
        SetFilterCount TgtCol
    Next
    MousePointer = 0
End Sub

Private Sub chkWkDay_Click(Index As Integer)
    Dim i As Integer
    Dim tmpTag As String
    If chkWkDay(Index).Value = 1 Then chkWkDay(Index).BackColor = LightGreen Else chkWkDay(Index).BackColor = vbButtonFace
    Select Case Index
        Case 8
            If chkWkDay(Index).Value = 1 Then
                StopNestedCalls = True
                For i = 1 To 7
                    chkWkDay(i).Value = 1
                Next
                chkWkDay(8).Value = 0
                StopNestedCalls = False
                If chkTool(3).Value = 1 Then AdjustFilterView
            End If
        Case 0
            If chkWkDay(Index).Value = 1 Then
                StopNestedCalls = True
                For i = 1 To 7
                    chkWkDay(i).Value = 0
                Next
                chkWkDay(Index).Tag = "......."
                chkWkDay(0).Value = 0
                StopNestedCalls = False
                If chkTool(3).Value = 1 Then AdjustFilterView
            End If
        Case Else
            tmpTag = chkWkDay(0).Tag
            If chkWkDay(Index).Value = 1 Then
                Mid(tmpTag, Index, 1) = Trim(Str(Index))
                chkWkDay(0).Tag = tmpTag
            Else
                Mid(tmpTag, Index, 1) = "."
                chkWkDay(0).Tag = tmpTag
                'If tmpTag = "......." Then chkWkDay(0).Value = 1
            End If
            If Not StopNestedCalls Then
                If chkTool(3).Value = 1 Then AdjustFilterView
            End If
    End Select
End Sub

Private Sub chkWork_Click(Index As Integer)
Dim FirstButton As Integer
Dim LastButton As Integer
Dim CurButton As Integer
Dim ColIdx As Integer
    If chkWork(Index).Value = 0 Then chkWork(Index).BackColor = vbButtonFace
    If chkWork(Index).Value = 1 Then
        FilterPanel(Index).Enabled = True
    Else
        FilterPanel(Index).Enabled = False
    End If
    FirstButton = Index * 2
    LastButton = FirstButton + 1
    For CurButton = FirstButton To LastButton
        chkFilterSet(CurButton).Enabled = FilterPanel(Index).Enabled
        If FilterPanel(Index).Enabled Then
            If CurButton Mod 2 = 0 Then
                FilterTab(CurButton).DisplayBackColor = LightYellow
                FilterTab(CurButton).DisplayTextColor = vbBlack
                FilterTab(CurButton).SelectBackColor = DarkestYellow
                FilterTab(CurButton).SelectTextColor = vbWhite
                txtFilterVal(CurButton).BackColor = vbWhite
            Else
                FilterTab(CurButton).DisplayBackColor = NormalBlue
                FilterTab(CurButton).DisplayTextColor = vbWhite
                FilterTab(CurButton).SelectBackColor = DarkBlue
                FilterTab(CurButton).SelectTextColor = vbWhite
            End If
        Else
            FilterTab(CurButton).DisplayBackColor = LightGray
            FilterTab(CurButton).DisplayTextColor = vbBlack
            FilterTab(CurButton).SelectBackColor = LightGray
            FilterTab(CurButton).SelectTextColor = vbBlack
            If CurButton Mod 2 = 0 Then txtFilterVal(CurButton).BackColor = LightGray
        End If
        FilterTab(CurButton).RedrawTab
    Next
    CheckRecLoadFilter False
    If (Not InitFilterCall) And (chkTool(3).Value = 1) Then AdjustFilterView
    If chkWork(Index).Value = 1 Then chkWork(Index).BackColor = LightestGreen
End Sub

Private Sub FilterTab_GotFocus(Index As Integer)
    'FilterTab(Index).SelectTextColor = vbWhite
    'FilterTab(Index).RedrawTab
    'StatusBar.Panels(5).Text = FilterTab(Index).Tag
End Sub
Private Sub FilterTab_LostFocus(Index As Integer)
    'FilterTab(Index).SelectTextColor = NormalGray
    'FilterTab(Index).RedrawTab
    'StatusBar.Panels(5).Text = ""
End Sub

Private Sub FilterTab_RowSelectionChanged(Index As Integer, ByVal LineNo As Long, ByVal Selected As Boolean)
    'If Trim(FilterTab(Index).GetColumnValue(LineNo, 0)) <> "" Then
        'SetStatusInfo Index, FilterTab(Index).GetColumnValue(LineNo, 0), "HighLighted"
    'End If
End Sub

Private Sub FilterTab_SendLButtonClick(Index As Integer, ByVal Line As Long, ByVal Col As Long)
    If Line < 0 Then
        FilterTab(Index).SetFocus
    Else
        'FilterTab(Index).SetDecorationObject Line, 0, "Invalid"
        'FilterTab(Index).Refresh
    End If
End Sub

Private Sub FilterTab_SendLButtonDblClick(Index As Integer, ByVal Line As Long, ByVal Col As Long)
    If Line < 0 Then
        FilterTab(Index).SetFocus
    Else
        ToggleFilterValues Index, Line, Col
    End If
End Sub

Private Sub ToggleFilterValues(Index As Integer, Line As Long, Col As Long)
    Dim FromCol As Integer
    Dim ToCol As Integer
    Dim SelCol As Integer
    If Line >= 0 Then
        If Trim(FilterTab(Index).GetColumnValue(Line, 0)) <> "" Then
            FromCol = Index
            If Index Mod 2 = 0 Then
                ToCol = FromCol + 1
                SelCol = ToCol
            Else
                ToCol = FromCol - 1
                SelCol = FromCol
            End If
            'SetStatusInfo Index, FilterTab(Index).GetColumnValue(Line, 0), "Moved"
            ShiftData FromCol, ToCol, Line
            FilterTab(Index).SetCurrentSelection Line
            FilterTab(ToCol).RedrawTab
            Me.Refresh
            If FromCol = 0 Then
                If FilterTab(ToCol).GetLineCount = 1 Then
                    'chkTool(2).Value = 1
                End If
            ElseIf FromCol = 1 Then
                If FilterTab(FromCol).GetLineCount = 0 Then
                    FlightExtract.ResetList
                End If
                If FilterTab(FromCol).GetLineCount = 1 Then
                    'chkTool(2).Value = 1
                End If
            End If
            If chkTool(3).Value = 1 Then AdjustFilterView
            CheckRecLoadFilter False
        End If
    End If
End Sub

Private Sub SetStatusInfo(ColIdx As Integer, ColTxt As String, ActTxt As String)
    Dim tmpTxt As String
    Dim tmpWork As Integer
    'tmpWork = ((ColIdx + 2) \ 2) - 1
    'StatusBar.Panels(1).Text = chkWork(tmpWork).Caption
    'StatusBar.Panels(2).Text = FilterTab(ColIdx).GetHeaderText
    'StatusBar.Panels(3).Text = Replace(ColTxt, ",", "", 1, -1, vbBinaryCompare) & ": "
    'StatusBar.Panels(4).Text = ActTxt
End Sub

Private Sub FilterTab_SendRButtonClick(Index As Integer, ByVal Line As Long, ByVal Col As Long)
    Dim tmpText As String
    tmpText = Trim(FilterTab(Index).GetColumnValue(Line, Col))
    BasicDetails Index, tmpText
End Sub

Public Sub BasicDetails(Index As Integer, tmpText As String)
    Dim tmpSqlKey As String
    Dim tmpTable As String
    Dim tmpFields As String
    Dim RetVal As Integer
    Dim CurItm As Integer
    Dim tmpFldNam As String
    Dim tmpFldVal As String
    Dim MsgTxt As String
    Dim tmpFlno As String
    Dim tmpFlca As String
    Dim tmpFltn As String
    Dim tmpFlns As String
    Dim ReqButtons As String
    
    MousePointer = 11
    If tmpText <> "" Then
        ReqButtons = ""
        tmpTable = ""
        Select Case Index
            Case 0 To 1
                tmpTable = "ALTTAB"
                tmpFields = "ALC2,ALC3,ALFN,CTRY,CASH,ADD1,ADD2,ADD3,ADD4,PHON,TFAX,SITA,TELX,WEBS,BASE"
                If Len(tmpText) = 2 Then
                    tmpSqlKey = "WHERE ALC2='" & tmpText & "'"
                End If
                If Len(tmpText) = 3 Then
                    tmpSqlKey = "WHERE ALC3='" & tmpText & "'"
                End If
                'ReqButtons = "OK,Flights"
            Case 2 To 3
                tmpFlno = StripAftFlno(tmpText, tmpFlca, tmpFltn, tmpFlns)
                ShowFlights.Show , MainDialog
                ShowFlights.LoadFlightData FlightExtract.txtVpfr(3).Tag, FlightExtract.txtVpto(3).Tag, tmpFlca, tmpFltn, tmpFlns
            Case 4 To 5
                tmpTable = "APTTAB"
                tmpFields = "APC3,APC4,APSN,APFN,APN2,APN3,APN4,LAND,APTT"
                If Len(tmpText) = 3 Then
                    tmpSqlKey = "WHERE APC3='" & tmpText & "'"
                End If
                If Len(tmpText) = 4 Then
                    tmpSqlKey = "WHERE APC4='" & tmpText & "'"
                End If
            Case 6 To 7
                tmpTable = "ACTTAB"
                tmpFields = "ACT3,ACT5,ACTI,ACFN,SEAF,SEAB,SEAE,SEAT,ENTY"
                If Len(tmpText) = 3 Then
                    tmpSqlKey = "WHERE ACT3='" & tmpText & "'"
                Else
                    tmpSqlKey = "WHERE ACT5='" & tmpText & "'"
                End If
            Case Else
        End Select
        If tmpTable <> "" Then
            RetVal = UfisServer.CallCeda(UserAnswer, "RT", tmpTable, tmpFields, "", tmpSqlKey, "", 0, True, False)
            MsgTxt = ""
            CurItm = 0
            Do
                CurItm = CurItm + 1
                tmpFldNam = GetItem(tmpFields, CurItm, ",")
                If tmpFldNam <> "" Then
                    tmpFldVal = GetItem(UserAnswer, CurItm, ",")
                    If tmpFldVal <> "" Then
                        MsgTxt = MsgTxt & tmpFldNam & ": " & tmpFldVal & vbNewLine
                    End If
                End If
            Loop While tmpFldNam <> ""
            If MsgTxt <> "" Then
                MsgTxt = Left(MsgTxt, Len(MsgTxt) - 2)
            Else
                MsgTxt = "'" & tmpText & "' is not in your basic data."
            End If
            If MyMsgBox.CallAskUser(0, 0, 0, "Basic Data Information", MsgTxt, "infomsg", ReqButtons, UserAnswer) = 2 Then
                Me.Refresh
                UserAnswer = Replace(UserAnswer, "&", "", 1, -1, vbBinaryCompare)
                Select Case UserAnswer
                    Case "Flights"
                        ShowFlights.Show , MainDialog
                        ShowFlights.LoadFlightData FlightExtract.txtVpfr(3).Tag, FlightExtract.txtVpto(3).Tag, tmpText, tmpFltn, tmpFlns
                    Case Else
                End Select
            End If
        End If
    End If
    MousePointer = 0
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyF1 Then DisplayHtmHelpFile "", Me.Name, Me.ActiveControl.Name
End Sub

Private Sub Form_Load()
    Dim i As Integer
    StatusBar.ZOrder
    Me.Top = FlightExtract.Top + FlightExtract.Height - FlightExtract.StatusBar.Height - Me.Height - 22 * Screen.TwipsPerPixelY
    If UpdateMode Then
        Me.Width = FilterPanel(1).Left + FilterPanel(1).Width + 10 * Screen.TwipsPerPixelX
    End If
    Me.Left = FlightExtract.Left + FlightExtract.Width / 2 - Me.Width / 2
    Me.Caption = Me.Caption & " (" & MySetUp.CurFileInfo(5).Caption & " Mode) " & GetItem(MySetUp.MyFileInfo.Caption, 1, " ")
    InitLists
    For i = 0 To 2
        FullFilter(i).ResetContent
        FullFilter(i).FontName = "Courier New"
        FullFilter(i).SetTabFontBold True
        FullFilter(i).HeaderFontSize = 17
        FullFilter(i).FontSize = 17
        FullFilter(i).LineHeight = 17
        FullFilter(i).HeaderLengthString = "200,200,200,200,200,200,200,200,200,200,200,200,200,200"
        FullFilter(i).HeaderString = "LINE,T,R,ALC,FLNO,TYP,APC,VIA,DATE,W,TIME,N,Remark,MainIdx"
        FullFilter(i).ColumnWidthString = "5,1,1,3,9,3,3,3,8,1,4,1,20,5"
        FullFilter(i).ColumnAlignmentString = "R,L,L,L,L,L,L,L,L,L,L,L,L,R"
        FullFilter(i).Tag = ""
        'FullFilter(i).AutoSizeByHeader = True
        'FullFilter(i).AutoSizeColumns
    Next
    HelperTab(0).ResetContent
    HelperTab(0).HeaderString = "VALUE,T"
    HelperTab(0).HeaderLengthString = "60,60"
    HelperTab(0).ColumnWidthString = "5,1"
    HelperTab(0).ColumnAlignmentString = "R,L"
    HelperTab(1).ResetContent
    HelperTab(1).HeaderString = "VALUE,T"
    HelperTab(1).HeaderLengthString = "60,60"
    HelperTab(1).ColumnWidthString = "16,1"
    HelperTab(1).ColumnAlignmentString = "L,L"
    If MainDialog.DataSystemType = "FHK" Then
        chkTool(3).Enabled = True
        chkTool(5).Enabled = True
    Else
        chkTool(3).Enabled = False
        chkTool(5).Enabled = False
    End If
End Sub

Private Sub InitLists()
Dim myHeaderList As String
Dim HdrLenLst As String
Dim i As Integer
Dim j As Integer
Dim tmpLen As Integer
    For i = 0 To 9
        FilterTab(i).ShowHorzScroller False
        FilterTab(i).ResetContent
        FilterTab(i).FontName = "Courier New"
        FilterTab(i).SetTabFontBold True
        FilterTab(i).HeaderFontSize = 17
        FilterTab(i).FontSize = 17
        FilterTab(i).LineHeight = 17
        FilterTab(i).HeaderLengthString = "200"
        FilterTab(i).HeaderString = " "
        FilterTab(i).MainHeaderOnly = True
        FilterTab(i).Tag = ""
        If i Mod 2 = 0 Then
            FilterTab(i).DisplayBackColor = LightYellow
            FilterTab(i).DisplayTextColor = vbBlack
            FilterTab(i).SelectBackColor = DarkestYellow
            FilterTab(i).SelectTextColor = vbBlack
        Else
            FilterTab(i).DisplayBackColor = NormalBlue
            FilterTab(i).DisplayTextColor = vbWhite
            FilterTab(i).SelectBackColor = DarkestBlue
            FilterTab(i).SelectTextColor = vbWhite
        End If
        FilterTab(i).EmptyAreaBackColor = LightGray
        FilterTab(i).GridLineColor = DarkGray
        FilterTab(i).CreateDecorationObject "Invalid", "L", "-1", CStr(vbRed)
    Next
    Me.Refresh
    InitFilterConfig
    BuildFilters
    For i = 0 To 4
        chkWork(i).Value = 1
        If Val(chkFilterSet(i * 2).Caption) = 0 Then chkWork(i).Value = 0
    Next
    If UpdateMode Then
        For i = 2 To 4
            chkWork(i).Value = 0
            chkWork(i).Enabled = False
        Next
    End If
End Sub

Public Function CheckFilterKeys(RecData As String, IsValidLine As Boolean) As Boolean
    Dim RetVal As Boolean
    Dim i As Integer
    Dim CurTab As Integer
    Dim CurColLst As String
    Dim ItmNbr As Integer
    Dim ColNbr As Integer
    Dim tmpCol As String
    Dim ColDat As String
    Dim ChkVal As String
    Dim HitLst As String
    Dim ColLen As Integer
    Dim CurCol As Integer
    Dim SelCnt As Integer
    Dim HitCnt As Integer
    RetVal = IsValidLine
    If RetVal = True Then
        SelCnt = 0
        HitCnt = 0
        i = 0
        Do
            If FilterPanel(i).Enabled = True Then
                If chkWork(i).Value = 1 Then
                    CurTab = i * 2 + 1
                    If FilterTab(CurTab).GetLineCount > 0 Then
                        SelCnt = SelCnt + 1
                        'Run through Activated FilterValues
                        ItmNbr = 1
                        Do
                            CurColLst = GetItem(CheckItems(i), ItmNbr, ",")
                            If CurColLst <> "" Then
                                CurColLst = Replace(CurColLst, "+", ",", 1, -1, vbBinaryCompare)
                                ChkVal = ""
                                ColNbr = 1
                                Do
                                    tmpCol = GetItem(CurColLst, ColNbr, ",")
                                    If tmpCol <> "" Then
                                        CurCol = Val(tmpCol) + 1
                                        ColLen = Val(GetItem(FlightExtract.ExtractData.ColumnWidthString, CurCol, ","))
                                        ColDat = GetItem(RecData, CurCol, ",")
                                        ChkVal = ChkVal & Left(ColDat & Space(ColLen), ColLen)
                                    End If
                                    ColNbr = ColNbr + 1
                                Loop While tmpCol <> ""
                                ChkVal = RTrim(ChkVal)
                                If ChkVal <> "" Then
                                    HitLst = FilterTab(CurTab).GetLinesByColumnValue(0, ChkVal, 0)
                                    If HitLst <> "" Then HitCnt = HitCnt + 1
                                End If
                            End If
                            ItmNbr = ItmNbr + 1
                        Loop While (CurColLst <> "") And (HitCnt < SelCnt)
                    End If
                End If
            End If
            i = i + 1
        Loop While (i <= 4) And (SelCnt = HitCnt)
        If SelCnt <> HitCnt Then RetVal = False
    End If
    CheckFilterKeys = RetVal
End Function

Public Sub InsertFilterValue(FilterData As String, ColIdx As Integer)
Dim MaxLin As Long
Dim CurLin As Long
Dim FndLin As Long
Dim tmpData As String
    MaxLin = FilterTab(ColIdx).GetLineCount - 1
    FndLin = -1
    CurLin = 0
    While (CurLin <= MaxLin) And (FndLin < 0)
        tmpData = FilterTab(ColIdx).GetLineValues(CurLin)
        If FilterData <= tmpData Then FndLin = CurLin
        CurLin = CurLin + 1
    Wend
    If FndLin < 0 Then
        If CurLin > MaxLin Then FndLin = CurLin Else FndLin = 0
    End If
    FilterTab(ColIdx).InsertTextLineAt FndLin, FilterData, True
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    If UnloadMode = 0 Then
        Me.Tag = ""
        Me.Hide
        FlightExtract.Show
        Cancel = True
    End If
End Sub

Private Sub Form_Resize()
    Dim i As Integer
    Dim NewWidth As Long
    Dim NewHeight As Long
    Dim NewTop As Long
    Dim MaxLin As Long
    NewHeight = Me.ScaleHeight - StatusBar.Height - FilterPanel(0).Top - Screen.TwipsPerPixelY
    If NewHeight > 1500 Then
        NewTop = NewHeight - ButtonPanel(0).Height - 2 * Screen.TwipsPerPixelY
        For i = 0 To 4
            FilterPanel(i).Height = NewHeight
            ButtonPanel(i).Top = NewTop
        Next
        NewHeight = ButtonPanel(0).Top - FilterTab(0).Top - 2 * Screen.TwipsPerPixelY
        MaxLin = (NewHeight - (FilterTab(0).LineHeight * Screen.TwipsPerPixelY) + (10 * Screen.TwipsPerPixelY)) / (FilterTab(0).LineHeight * Screen.TwipsPerPixelY)
        For i = 0 To 9
            FilterTab(i).Height = NewHeight
            If FilterTab(i).GetVScrollPos = 0 Then
                If MaxLin > FilterTab(i).GetLineCount Then
                    FilterTab(i).ShowVertScroller False
                Else
                    FilterTab(i).ShowVertScroller True
                End If
            End If
        Next
    End If
    NewHeight = Me.ScaleHeight - StatusBar.Height - FullFilter(1).Top - Screen.TwipsPerPixelY
    If NewHeight > 1500 Then FullFilter(1).Height = NewHeight
    NewWidth = Me.ScaleWidth - FullFilter(1).Left - 60
    If NewWidth > 1500 Then FullFilter(1).Width = NewWidth
    Me.Refresh
End Sub
Private Sub CheckRecLoadFilter(ForLoad As Boolean)
    Dim Result As String
    Dim i As Integer
    Dim MaxLine As Long
    Result = ""
    For i = 1 To 9 Step 2
        If chkFilterSet(i).Enabled = True Then
            MaxLine = FilterTab(i).GetLineCount - 1
            If MaxLine >= 0 Then
                Result = Result & FilterTab(i).GetBuffer(0, MaxLine, ",")
            End If
        End If
    Next
    If ForLoad = True Then LastLoadFilter = Result
    If LastLoadFilter = Result Then
        chkTool(2).ForeColor = vbBlack
        chkTool(2).BackColor = vbButtonFace
        chkTool(4).Enabled = True
        chkTool(2).Tag = "OK"
    Else
        chkTool(2).ForeColor = vbWhite
        chkTool(2).BackColor = vbRed
        chkTool(4).Enabled = False
        chkTool(2).Tag = "NOTOK"
    End If
    If Not ForLoad Then FlightExtract.CheckLoadFilter False
End Sub

Public Function CheckFullAirlineFilter() As Boolean
    Dim Result As Boolean
    Dim i As Integer
    Dim FilterCnt As Integer
    Result = True
    FilterCnt = 0
    For i = 3 To 9 Step 2
        If chkFilterSet(i).Enabled = True Then FilterCnt = FilterCnt + Val(chkFilterSet(i).Caption)
    Next
    If (chkWkDay(0).Tag <> ".......") And (chkWkDay(0).Tag <> "1234567") Then FilterCnt = FilterCnt + 1
    If FilterCnt > 0 Then Result = False
    CheckFullAirlineFilter = Result
End Function

Public Sub PrepareNoopFlights(SetEnable As Boolean)
    Dim i As Integer
    For i = 1 To 4
        If SetEnable = True Then chkWork(i) = 1 Else chkWork(i) = 0
        chkWork(i).Enabled = SetEnable
    Next
    If UpdateMode Then
        For i = 2 To 4
            chkWork(i).Value = 0
            chkWork(i).Enabled = False
        Next
        chkWork(4).Value = chkTool(3).Value
    End If
    If SetEnable = True Then
        Caption = "Record Filter Preset" & " (" & MySetUp.CurFileInfo(5).Caption & " Mode) " & GetItem(MySetUp.MyFileInfo.Caption, 1, " ")
    Else
        Caption = "Airline Compare Flights Filter" & " (" & MySetUp.CurFileInfo(5).Caption & " Mode) " & GetItem(MySetUp.MyFileInfo.Caption, 1, " ")
    End If
End Sub

Private Sub txtFilterVal_Change(Index As Integer)
    If Index = 2 Then
        SearchInList Index, 1
    Else
        SearchInList Index, 2
    End If
End Sub

Private Sub txtFilterVal_KeyPress(Index As Integer, KeyAscii As Integer)
    Dim LineNo As Long
    Dim HitText As String
    If KeyAscii = 13 Then
        LineNo = FilterTab(Index).GetCurrentSelected
        If LineNo >= 0 Then
            HitText = FilterTab(Index).GetColumnValue(LineNo, 0)
            If InStr(HitText, txtFilterVal(Index).Text) > 0 Then
                FilterTab_SendLButtonDblClick Index, LineNo, 0
                chkTool(2).Value = 1
            End If
        End If
        KeyAscii = 0
    Else
        KeyAscii = Asc(UCase(Chr(KeyAscii)))
    End If
End Sub

Private Sub SearchInList(UseIndex As Integer, Method As Integer)
    Static LastIndex As Integer
    Dim HitLst As String
    Dim HitRow As Long
    Dim NxtRow As Long
    Dim NewScroll As Long
    Dim LoopCount As Integer
    Dim tmpText As String
    Dim ColNbr As String
    Dim Index As Integer
    Dim UseCol As Long
    Index = UseIndex
    If Index < 0 Then Index = LastIndex
    LastIndex = Index
    tmpText = Trim(txtFilterVal(Index).Text)
    UseCol = 0
    If tmpText <> "" Then
        LoopCount = 0
        Do
            HitLst = FilterTab(Index).GetNextLineByColumnValue(UseCol, tmpText, Method)
            HitRow = Val(HitLst)
            If HitLst <> "" Then
                If HitRow > 5 Then NewScroll = HitRow - 5 Else NewScroll = 0
                FilterTab(Index).OnVScrollTo NewScroll
            Else
                FilterTab(Index).SetCurrentSelection -1
                FilterTab(Index).OnVScrollTo 0
            End If
            LoopCount = LoopCount + 1
        Loop While LoopCount < 2 And HitLst = ""
        FilterTab(Index).SetCurrentSelection HitRow
    End If
    txtFilterVal(Index).SetFocus
End Sub

Private Sub InitAdjustFilter()
    Dim CurLine As Long
    Dim MaxLine As Long
    Dim tmpRec As String
    Dim NewRec As String
    Dim tmpErr As String
    Dim tmpAdid As String
    Dim tmpAlc2 As String
    Dim tmpFltn As String
    Dim tmpFlno As String
    Dim tmpVia3 As String
    Dim tmpApc3 As String
    Dim tmpFred As String
    Dim tmpTtyp As String
    Dim tmpTime As String
    Dim tmpWkdy As String
    Dim tmpVpfr As String
    Dim tmpVpto As String
    Dim tmpTifr As String
    Dim tmpTito As String
    Dim tmpDate As String
    Dim tmpMainIdx As String
    Dim IsValid As Boolean
    Dim varVpfr
    Dim varVpto
    Dim varTifr
    Dim varTito
    Dim varDay
    If MainDialog.DataSystemType = "FHK" Then
        InitFilterCall = True
        Screen.MousePointer = 11
        If FullFilter(0).GetLineCount < 1 Then
            StatusBar.Panels(3).Text = "Initialising Filter Data ..."
            tmpTifr = FlightExtract.txtVpfr(0).Tag
            tmpTito = FlightExtract.txtVpto(0).Tag
            varTifr = CedaDateToVb(tmpTifr)
            varTito = CedaDateToVb(tmpTito)
            MaxLine = MainDialog.FileData.GetLineCount - 1
            For CurLine = 0 To MaxLine
                IsValid = True
                tmpErr = Trim(MainDialog.FileData.GetColumnValue(CurLine, 1))
                tmpErr = tmpErr & Trim(MainDialog.FileData.GetColumnValue(CurLine, 2))
                If (InStr(tmpErr, "E") > 0) Or (InStr(tmpErr, "-") > 0) Then IsValid = False
                If IsValid Then
                    tmpMainIdx = CStr(CurLine)
                    NewRec = MainDialog.FileData.GetColumnValue(CurLine, 0) & ","
                    NewRec = NewRec & MainDialog.FileData.GetColumnValue(CurLine, 3) & ","
                    NewRec = NewRec & MainDialog.FileData.GetColumnValue(CurLine, 7) & ","
                    tmpAlc2 = MainDialog.FileData.GetColumnValue(CurLine, 5)
                    tmpFltn = MainDialog.FileData.GetColumnValue(CurLine, 6)
                    tmpFlno = Left(tmpAlc2 & "   ", 3) & tmpFltn
                    NewRec = NewRec & tmpAlc2 & ","
                    NewRec = NewRec & tmpFlno & ","
                    NewRec = NewRec & MainDialog.FileData.GetColumnValue(CurLine, 14) & ","
                    tmpApc3 = MainDialog.FileData.GetColumnValue(CurLine, 13)
                    tmpVia3 = Trim(MainDialog.FileData.GetColumnValue(CurLine, 12))
                    tmpTime = MainDialog.FileData.GetColumnValue(CurLine, 8)
                    tmpTtyp = MainDialog.FileData.GetColumnValue(CurLine, 16)
                    tmpVpfr = MainDialog.FileData.GetColumnValue(CurLine, 9)
                    tmpVpto = MainDialog.FileData.GetColumnValue(CurLine, 10)
                    tmpFred = MainDialog.FileData.GetColumnValue(CurLine, 11)
                    varVpfr = CedaDateToVb(tmpVpfr)
                    varVpto = CedaDateToVb(tmpVpto)
                    If (varVpfr <= varTito) And (varVpto >= varTifr) Then
                        varDay = varVpfr
                        While varDay <= varVpto
                            If (varDay >= varTifr) And (varDay <= varTito) Then
                                tmpWkdy = CStr(Weekday(varDay, vbMonday))
                                If InStr(tmpFred, tmpWkdy) > 0 Then
                                    tmpDate = Format(varDay, "yyyymmdd")
                                    tmpRec = NewRec & tmpApc3 & "," & tmpVia3 & "," & tmpDate & "," & tmpWkdy & "," & tmpTime & "," & tmpTtyp & ",," & tmpMainIdx
                                    FullFilter(0).InsertTextLine tmpRec, False
                                    If chkTool(5).Value = 1 Then
                                        If tmpVia3 <> "" Then
                                            tmpRec = NewRec & tmpVia3 & "," & "!" & "," & tmpDate & "," & tmpWkdy & "," & tmpTime & "," & tmpTtyp & ",," & tmpMainIdx
                                            FullFilter(0).InsertTextLine tmpRec, False
                                        End If
                                    End If
                                End If
                            End If
                            varDay = DateAdd("d", 1, varDay)
                        Wend
                    End If
                End If
            Next
            'FullFilter(0).AutoSizeColumns
            'FullFilter(0).RedrawTab
            StatusBar.Panels(3).Text = "Creating internal indexes ..."
            FullFilter(0).IndexCreate "ALC", 3
            FullFilter(0).IndexCreate "FLNO", 4
            FullFilter(0).IndexCreate "ACT", 5
            FullFilter(0).IndexCreate "APC", 6
            FullFilter(0).IndexCreate "DAY", 8
            FullFilter(0).IndexCreate "WKD", 9
        End If
        StatusBar.Panels(3).Text = "Publishing Filters ..."
        Screen.MousePointer = 0
        If chkWork(4).Caption = "" Then
            chkWork(4).Caption = "OP Days"
            chkWork(4).Value = 1
            FilterTab(8).DateTimeSetColumn 0
            FilterTab(8).DateTimeSetInputFormatString 0, "YYYYMMDD"
            FilterTab(8).DateTimeSetOutputFormatString 0, "DDMMMYY"
            FilterTab(9).DateTimeSetColumn 0
            FilterTab(9).DateTimeSetInputFormatString 0, "YYYYMMDD"
            FilterTab(9).DateTimeSetOutputFormatString 0, "DDMMMYY"
        End If
        Me.Width = FilterPanel(4).Left + FilterPanel(4).Width + 10 * Screen.TwipsPerPixelX
        Me.Left = FlightExtract.Left + FlightExtract.Width / 2 - Me.Width / 2
        FlightExtract.Refresh
        Me.Refresh
        If chkTool(3).Value = 1 Then AdjustFilterView
        StatusBar.Panels(3).Text = CStr(FullFilter(0).GetLineCount) & " Filter Lines"
        InitFilterCall = False
    End If
End Sub

Private Sub AdjustFilterView()
    Dim MaxLine As Long
    Dim CurLine As Long
    Dim MaxLine2 As Long
    Dim CurLine2 As Long
    Dim LineNo As Long
    Dim ColNo As Long
    Dim CurIdx As Integer
    Dim SrcIndex As Integer
    Dim DstIndex As Integer
    Dim ColIdx As Integer
    Dim tmpData As String
    Dim tmpIdxName As String
    Dim tmpRec As String
    Dim HitList As String
    Dim IsFilter As Boolean
    Dim tmpTag As String
    
    Screen.MousePointer = 11
    FullFilter(1).ResetContent
    IsFilter = False
    
    SrcIndex = 0
    ColIdx = 0
    tmpTag = chkWkDay(0).Tag
    If (tmpTag <> ".......") And (tmpTag <> "1234567") Then
        For CurIdx = 1 To 7
            If chkWkDay(CurIdx).Value = 1 Then
                tmpData = CStr(CurIdx)
                HitList = FullFilter(0).GetLinesByIndexValue("WKD", tmpData, 0)
                HelperTab(0).ResetContent
                HelperTab(0).InsertBuffer HitList, ","
                HelperTab(0).Sort "0", True, True
                MaxLine = HelperTab(0).GetLineCount - 1
                For CurLine = 0 To MaxLine
                    LineNo = Val(HelperTab(0).GetColumnValue(CurLine, 0))
                    tmpRec = FullFilter(0).GetLineValues(LineNo)
                    FullFilter(1).InsertTextLine tmpRec, False
                Next
                IsFilter = True
                SrcIndex = 1
            End If
        Next
    End If
            
    For CurIdx = 1 To 9 Step 2
        If chkWork(ColIdx).Value = 1 Then
            If FilterTab(CurIdx).GetLineCount > 0 Then
                FullFilter(1).IndexDestroy tmpIdxName
                Select Case CurIdx
                    Case 1
                        tmpIdxName = "ALC"
                        ColNo = 3
                    Case 3
                        tmpIdxName = "FLNO"
                        ColNo = 4
                    Case 5
                        tmpIdxName = "APC"
                        ColNo = 6
                    Case 7
                        tmpIdxName = "ACT"
                        ColNo = 5
                    Case 9
                        tmpIdxName = "DAY"
                        ColNo = 8
                    Case Else
                End Select
                If (FullFilter(1).GetLineCount > 0) Or (IsFilter) Then
                    SrcIndex = 1
                    DstIndex = 2
                    FullFilter(1).IndexCreate tmpIdxName, ColNo
                Else
                    SrcIndex = 0
                    DstIndex = 1
                End If
                CheckAdjustFilter SrcIndex, CurIdx - 1, ColNo
                IsFilter = True
                FullFilter(DstIndex).ResetContent
                MaxLine2 = FilterTab(CurIdx).GetLineCount - 1
                For CurLine2 = 0 To MaxLine2
                    tmpData = Trim(FilterTab(CurIdx).GetColumnValue(CurLine2, 0))
                    HitList = FullFilter(SrcIndex).GetLinesByIndexValue(tmpIdxName, tmpData, 0)
                    HelperTab(0).ResetContent
                    HelperTab(0).InsertBuffer HitList, ","
                    HelperTab(0).Sort "0", True, True
                    MaxLine = HelperTab(0).GetLineCount - 1
                    For CurLine = 0 To MaxLine
                        LineNo = Val(HelperTab(0).GetColumnValue(CurLine, 0))
                        tmpRec = FullFilter(SrcIndex).GetLineValues(LineNo)
                        FullFilter(DstIndex).InsertTextLine tmpRec, False
                    Next
                Next
                HelperTab(0).ResetContent
                If DstIndex = 2 Then
                    FullFilter(1).ResetContent
                    MaxLine2 = FullFilter(2).GetLineCount - 1
                    tmpRec = FullFilter(2).GetBuffer(0, MaxLine2, vbLf)
                    FullFilter(1).InsertBuffer tmpRec, vbLf
                End If
            End If
        End If
        ColIdx = ColIdx + 1
    Next
    If (FullFilter(1).GetLineCount > 0) Or (IsFilter) Then SrcIndex = 1 Else SrcIndex = 0
    
    For CurIdx = 1 To 9 Step 2
        If FilterTab(CurIdx).GetLineCount = 0 Then
            Select Case CurIdx
                Case 1
                    FillAdjustFilter SrcIndex, 0, 3
                Case 3
                    FillAdjustFilter SrcIndex, 2, 4
                Case 5
                    FillAdjustFilter SrcIndex, 4, 6
                Case 7
                    FillAdjustFilter SrcIndex, 6, 5
                Case 9
                    FillAdjustFilter SrcIndex, 8, 8
                Case Else
            End Select
        Else
            Select Case CurIdx
                Case 1
                    CheckAdjustFilter SrcIndex, CurIdx, 3
                Case 3
                    CheckAdjustFilter SrcIndex, CurIdx, 4
                Case 5
                    CheckAdjustFilter SrcIndex, CurIdx, 6
                Case 7
                    CheckAdjustFilter SrcIndex, CurIdx, 5
                Case 9
                    CheckAdjustFilter SrcIndex, CurIdx, 8
                Case Else
            End Select
        End If
    Next
    
    Screen.MousePointer = 0
    Screen.MousePointer = 11
    Me.Refresh
    PreView.FullFilter(0).ResetContent
    PreView.FullFilter(0).ShowVertScroller False
    PreView.FullFilter(0).Refresh
    MaxLine2 = FullFilter(SrcIndex).GetLineCount - 1
    tmpRec = FullFilter(SrcIndex).GetBuffer(0, MaxLine2, vbLf)
    PreView.FullFilter(0).InsertBuffer tmpRec, vbLf
    PreView.FullFilter(0).AutoSizeColumns
    PreView.FullFilter(0).ShowVertScroller True
    PreView.FullFilter(0).Refresh
    PreView.SortDefault
    Screen.MousePointer = 0
    Screen.MousePointer = 11
    PreView.IdentifyFlights
    PreView.lblCount.Caption = CStr(PreView.FullFilter(0).GetLineCount)
    Screen.MousePointer = 0
    
End Sub
Private Sub FillAdjustFilter(SrcIndex As Integer, DstIndex As Integer, ColNo As Long)
    Dim tmpData As String
    tmpData = FullFilter(SrcIndex).SelectDistinct(CStr(ColNo), "", "", vbLf, True)
    FilterTab(DstIndex).ResetContent
    FilterTab(DstIndex).InsertBuffer tmpData, vbLf
    FilterTab(DstIndex).Sort "0", True, True
    FilterTab(DstIndex).ShowVertScroller True
    FilterTab(DstIndex).RedrawTab
    chkFilterSet(DstIndex).Caption = CStr(FilterTab(DstIndex).GetLineCount)
    chkFilterSet(DstIndex).Refresh
End Sub
Private Sub CheckAdjustFilter(SrcIndex As Integer, DstIndex As Integer, ColNo As Long)
    Dim tmpData As String
    tmpData = FullFilter(SrcIndex).SelectDistinct(CStr(ColNo), "", "", vbLf, True)
    HelperTab(1).ResetContent
    HelperTab(1).IndexDestroy "VAL"
    HelperTab(1).InsertBuffer tmpData, vbLf
    HelperTab(1).IndexCreate "VAL", 0
    CheckValidFilter DstIndex
End Sub
Private Sub CheckValidFilter(Index As Integer)
    Dim tmpData As String
    Dim tmpHit As String
    Dim MaxLine As Long
    Dim CurLine As Long
    MaxLine = FilterTab(Index).GetLineCount - 1
    For CurLine = 0 To MaxLine
        tmpData = FilterTab(Index).GetColumnValue(CurLine, 0)
        tmpHit = HelperTab(1).GetLinesByIndexValue("VAL", tmpData, 0)
        If tmpHit <> "" Then
            FilterTab(Index).ResetLineDecorations CurLine
        Else
            FilterTab(Index).SetDecorationObject CurLine, 0, "Invalid"
        End If
    Next
    FilterTab(Index).RedrawTab
End Sub
