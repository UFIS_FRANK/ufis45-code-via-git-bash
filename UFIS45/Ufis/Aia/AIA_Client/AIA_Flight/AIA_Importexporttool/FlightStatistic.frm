VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{64E8E382-05E2-11D2-9B1D-9B0630BC8F12}#1.0#0"; "tab.ocx"
Begin VB.Form FlightStatistic 
   Caption         =   "Flight Statistics"
   ClientHeight    =   3765
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   14160
   BeginProperty Font 
      Name            =   "Courier New"
      Size            =   11.25
      Charset         =   161
      Weight          =   700
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "FlightStatistic.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   3765
   ScaleWidth      =   14160
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.CheckBox chkWork 
      Caption         =   "Chart"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   15
      Left            =   12240
      Style           =   1  'Graphical
      TabIndex        =   18
      Top             =   30
      Width           =   855
   End
   Begin VB.CheckBox chkWork 
      Caption         =   "Gantt"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   14
      Left            =   11370
      Style           =   1  'Graphical
      TabIndex        =   17
      Top             =   30
      Width           =   855
   End
   Begin VB.CheckBox chkWork 
      Caption         =   "Details"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   13
      Left            =   10500
      Style           =   1  'Graphical
      TabIndex        =   16
      Top             =   30
      Width           =   855
   End
   Begin VB.CheckBox chkWork 
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   12
      Left            =   8760
      Style           =   1  'Graphical
      TabIndex        =   15
      Top             =   30
      Width           =   855
   End
   Begin VB.CheckBox chkWork 
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   11
      Left            =   7890
      Style           =   1  'Graphical
      TabIndex        =   14
      Top             =   30
      Width           =   855
   End
   Begin VB.CheckBox chkWork 
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   10
      Left            =   7020
      Style           =   1  'Graphical
      TabIndex        =   13
      Top             =   30
      Width           =   855
   End
   Begin VB.CheckBox chkWork 
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   9
      Left            =   6150
      Style           =   1  'Graphical
      TabIndex        =   12
      Top             =   30
      Width           =   855
   End
   Begin VB.CheckBox chkWork 
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   8
      Left            =   5280
      Style           =   1  'Graphical
      TabIndex        =   11
      Top             =   30
      Width           =   855
   End
   Begin VB.CheckBox chkWork 
      Caption         =   "Round"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   7
      Left            =   4410
      Style           =   1  'Graphical
      TabIndex        =   10
      Top             =   30
      Width           =   855
   End
   Begin VB.CheckBox chkWork 
      Caption         =   "On Dep."
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   6
      Left            =   3540
      Style           =   1  'Graphical
      TabIndex        =   9
      Top             =   30
      Width           =   855
   End
   Begin VB.CheckBox chkWork 
      Caption         =   "On Arr."
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   5
      Left            =   2670
      Style           =   1  'Graphical
      TabIndex        =   8
      Top             =   30
      Width           =   855
   End
   Begin VB.CheckBox chkWork 
      Caption         =   "Distribut"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   4
      Left            =   1800
      Style           =   1  'Graphical
      TabIndex        =   7
      Top             =   30
      Width           =   855
   End
   Begin MSComctlLib.StatusBar StatusBar 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   5
      Top             =   3480
      Width           =   14160
      _ExtentX        =   24977
      _ExtentY        =   503
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   2
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   21881
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
         EndProperty
      EndProperty
   End
   Begin VB.CheckBox chkWork 
      Caption         =   "Close"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   3
      Left            =   13110
      Style           =   1  'Graphical
      TabIndex        =   4
      Top             =   30
      Width           =   855
   End
   Begin VB.CheckBox chkWork 
      Caption         =   "Save As"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   2
      Left            =   9630
      Style           =   1  'Graphical
      TabIndex        =   3
      Top             =   30
      Width           =   855
   End
   Begin VB.CheckBox chkWork 
      Caption         =   "Shrink"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   1
      Left            =   930
      Style           =   1  'Graphical
      TabIndex        =   2
      Top             =   30
      Width           =   855
   End
   Begin VB.CheckBox chkWork 
      Caption         =   "Season"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   0
      Left            =   60
      Style           =   1  'Graphical
      TabIndex        =   1
      Top             =   30
      Width           =   855
   End
   Begin TABLib.TAB StatisticData 
      Height          =   2055
      Index           =   0
      Left            =   60
      TabIndex        =   0
      Top             =   360
      Width           =   4875
      _Version        =   65536
      _ExtentX        =   8599
      _ExtentY        =   3625
      _StockProps     =   64
      Columns         =   10
   End
   Begin TABLib.TAB StatisticData 
      Height          =   645
      Index           =   1
      Left            =   60
      TabIndex        =   6
      Top             =   2580
      Width           =   4875
      _Version        =   65536
      _ExtentX        =   8599
      _ExtentY        =   1138
      _StockProps     =   64
      Columns         =   10
   End
End
Attribute VB_Name = "FlightStatistic"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private CurHeadLength As String
Private IsInitialized As Boolean
Dim StatVal(400, 20) As Long
Dim FirstDay As Variant
Dim LastDay As Variant
Dim PeriodRange As Long

Public Sub InitStatisticList(ipLines As Integer, ipFirstCall As Boolean)
Dim i As Integer
Dim tmpLen
Dim HdrLenLst As String
Dim ColAlign As String
Dim tmpHeader As String
Dim ActColLst As String
  
    tmpHeader = "Date,W,INTER,SHENG,TOTAL,INTER,SHENG,TOTAL,INTER,SHENG,MIXED,TOTAL,TRANS,  WIDE, NORMAL, SMALL,Remark"
    ActColLst = "7   ,1,5    ,5    ,5    ,5    ,5    ,5    ,5    ,5    ,5    ,5    ,5    ,7     ,7      ,7     ,50"
    ColAlign = ""
    HdrLenLst = ""
    i = 0
    Do
        i = i + 1
        tmpLen = GetItem(ActColLst, i, ",")
        If tmpLen <> "" Then
            If tmpLen > 0 Then
                HdrLenLst = HdrLenLst & TextWidth(Space(tmpLen)) / Screen.TwipsPerPixelX + 6 & ","
            Else
                HdrLenLst = HdrLenLst & "1,"
            End If
            ColAlign = ColAlign & "R,"
        End If
    Loop While tmpLen <> ""
    HdrLenLst = HdrLenLst & "1000"
    SetItem ColAlign, 1, ",", "L"
    SetItem ColAlign, 2, ",", "L"
    If ipFirstCall Then
        StatisticData(0).ResetContent
        StatisticData(1).ResetContent
    End If
    For i = 0 To 1
        StatisticData(i).FontName = "Courier New"
        StatisticData(i).SetTabFontBold True
        StatisticData(i).HeaderFontSize = MySetUp.FontSlider.Value + 6
        StatisticData(i).FontSize = MySetUp.FontSlider.Value + 6
        StatisticData(i).LineHeight = MySetUp.FontSlider.Value + 7
        StatisticData(i).LifeStyle = True
        StatisticData(i).CursorLifeStyle = True
        StatisticData(i).HeaderLengthString = HdrLenLst
        StatisticData(i).HeaderString = tmpHeader
        StatisticData(i).ColumnAlignmentString = ColAlign
        StatisticData(i).GridLineColor = vbBlack
    Next
    'HardCoded
    StatisticData(0).DateTimeSetColumn 0
    StatisticData(0).DateTimeSetInputFormatString 0, "YYYYMMDD"
    StatisticData(0).DateTimeSetOutputFormatString 0, "DDMMMYY"
    If ipFirstCall And Not IsInitialized Then
        StatisticData(0).Height = ipLines * StatisticData(0).LineHeight * Screen.TwipsPerPixelY
        StatisticData(1).Height = ((2 * StatisticData(1).LineHeight) + 1) * Screen.TwipsPerPixelY
        Me.Height = StatisticData(0).Top + StatisticData(0).Height + StatisticData(1).Height + StatusBar.Height + 30 * Screen.TwipsPerPixelY
        Me.Left = MainDialog.Left
        Me.Width = MainDialog.Width
        'IsInitialized = True
    End If
    CurHeadLength = ActColLst
    StatisticData(0).SetMainHeaderValues "2,3,3,5,3,1", "OP DAYS,ARRIVALS,DEPARTURES,LINKED ROTATIONS,AC MINUTES ON GROUND,REMARK", "12632256,12632256,12632256,12632256,12632256,12632256"
    StatisticData(0).MainHeader = True
    BuildFlightStatistic
    StatisticData(1).SelectBackColor = vbWhite
    StatisticData(1).SelectTextColor = vbBlack
End Sub

Public Sub ChangeMyDataFont(myFontSize As Integer)
Dim i As Integer
Dim tmpLen
Dim HdrLenLst As String
    FontSize = myFontSize
    HdrLenLst = ""
    i = 0
    Do
        i = i + 1
        tmpLen = GetItem(CurHeadLength, i, ",")
        If tmpLen <> "" Then
            HdrLenLst = HdrLenLst & TextWidth(Space(tmpLen)) / Screen.TwipsPerPixelX + 6 & ","
        End If
    Loop While tmpLen <> ""
    HdrLenLst = HdrLenLst & "1000"
    For i = 0 To 1
        StatisticData(i).HeaderLengthString = HdrLenLst
        StatisticData(i).HeaderFontSize = myFontSize + 6
        StatisticData(i).FontSize = myFontSize + 6
        StatisticData(i).LineHeight = myFontSize + 7
    Next
    Refresh
End Sub

Public Sub BuildFlightStatistic()
Dim CurLine As Long
Dim MaxLine As Long
Dim DataLine As String
Dim strFrqd As String
Dim WeekFreq As Integer
Dim DayOffset As Integer

Dim ArrFltn As String
Dim DepFltn As String

Dim varVpfr As Variant
Dim varVpto As Variant
Dim varFday As Variant
Dim strWkdy As String
Dim WkDayCnt As Integer
Dim tmpData As String
Dim varStoa As Variant
Dim varStod As Variant
Dim ArrGrti As Long
Dim DepGrti As Long

Dim IsValidWeek As Boolean
Dim InvalidDays As Integer
Dim TurnFlag As Integer
Dim ArrFlt As Boolean
Dim DepFlt As Boolean

Dim SeasVpfr As String
Dim SeasVpto As String
Dim ArrIdx As Integer
Dim DepIdx As Integer
Dim OutRange As Long

Dim i As Integer
Dim j As Integer

    MaxLine = FlightExtract.ExtractData.GetLineCount - 1
    'MsgBox "Statistic for " & MaxLine & " Lines"
    For i = 0 To 400
        For j = 0 To 20
            StatVal(i, j) = 0
        Next
    Next
    
    SeasVpfr = FlightExtract.txtVpfr(0).Tag
    SeasVpto = FlightExtract.txtVpto(0).Tag
    FirstDay = CedaDateToVb(SeasVpfr)   'Goes to StatVal(1,n)
    LastDay = CedaDateToVb(SeasVpto)
    PeriodRange = DateDiff("d", FirstDay, LastDay)
    
    If (PeriodRange > 0) And (PeriodRange < 400) Then
        StatisticData(0).ResetContent
        Screen.MousePointer = 11
        Refresh
        
        For CurLine = 0 To MaxLine
            DataLine = FlightExtract.ExtractData.GetLineValues(CurLine)
    
            varVpfr = CedaDateToVb(GetItem(DataLine, 8, ","))
            varVpto = CedaDateToVb(GetItem(DataLine, 9, ","))
            strFrqd = GetItem(DataLine, 10, ",")
            WeekFreq = Val(GetItem(DataLine, 22, ","))
            DayOffset = Val(GetItem(DataLine, 17, ",")) 'Defines the departure
            
            'Determine what type of rotation we got
            ArrFltn = GetItem(DataLine, 5, ",")
            DepFltn = GetItem(DataLine, 7, ",")
            If ArrFltn <> "" Then ArrFlt = True Else ArrFlt = False
            If DepFltn <> "" Then DepFlt = True Else DepFlt = False
            TurnFlag = 0
            If ArrFlt Then TurnFlag = TurnFlag + 1
            If DepFlt Then TurnFlag = TurnFlag + 2
            
            varStoa = CedaFullDateToVb(GetItem(DataLine, 15, ","))
            varStod = CedaFullDateToVb(GetItem(DataLine, 16, ","))
            Select Case TurnFlag
                Case 1
                    ArrGrti = 45
                    DepGrti = 0
                Case 2
                    ArrGrti = 0
                    DepGrti = 15
                Case 3
                    ArrGrti = DateDiff("n", varStoa, varStod)
                    DepGrti = 0
                Case Else
            End Select
            
            ArrIdx = DateDiff("d", FirstDay, varVpfr)
        
            WkDayCnt = 0
            IsValidWeek = True
            InvalidDays = 0
            OutRange = 0
            
            varFday = varVpfr
            Do
                ArrIdx = ArrIdx + 1
                strWkdy = Trim(Str(Weekday(varFday, vbMonday)))
                If IsValidWeek And (InStr(strFrqd, strWkdy) > 0) Then
                    'Hit. Always valid for Arrivals or single Departures
                    DepIdx = ArrIdx + DayOffset
                    If (ArrIdx > 0) And (DepIdx < 400) Then
                        Select Case TurnFlag
                            Case 1  'Single arrival
                                StatVal(ArrIdx, 3) = StatVal(ArrIdx, 3) + 1
                                StatVal(ArrIdx, 13) = StatVal(ArrIdx, 13) + ArrGrti
                            Case 2  'Single departure
                                StatVal(DepIdx, 6) = StatVal(DepIdx, 6) + 1
                                StatVal(DepIdx, 13) = StatVal(DepIdx, 13) + DepGrti
                            Case 3  'Rotation
                                StatVal(ArrIdx, 3) = StatVal(ArrIdx, 3) + 1
                                StatVal(DepIdx, 6) = StatVal(DepIdx, 6) + 1
                                StatVal(ArrIdx, 10) = StatVal(ArrIdx, 10) + 1
                                If ArrFltn = DepFltn Then StatVal(ArrIdx, 11) = StatVal(ArrIdx, 11) + 1
                                StatVal(ArrIdx, 13) = StatVal(ArrIdx, 13) + ArrGrti
                                StatVal(DepIdx, 13) = StatVal(DepIdx, 13) + DepGrti
                            Case Else
                        End Select
                    Else
                        OutRange = OutRange + 1
                    End If
                End If
                varFday = DateAdd("d", 1, varFday)
                If Not IsValidWeek Then InvalidDays = InvalidDays - 1
                WkDayCnt = WkDayCnt + 1
                If WkDayCnt = 7 Then
                    If IsValidWeek Then
                        InvalidDays = (WeekFreq - 1) * 7
                        If InvalidDays > 0 Then IsValidWeek = False
                    Else
                        If InvalidDays = 0 Then
                            IsValidWeek = True
                        Else
                            'here we have a problem
                            'MsgBox "Please tell Berni that here is a problem with the weekly frequency."
                        End If
                    End If
                    WkDayCnt = 0
                End If
            Loop While varFday <= varVpto
        Next
        DisplayStatistic
        Screen.MousePointer = 0
        If OutRange > 0 Then
            MsgBox OutRange & " flights out of period"
        End If
    Else
        MsgBox "Period range out of limits"
    End If
End Sub

Private Sub DisplayStatistic()
    Dim i As Integer
    Dim j As Integer
    Dim LineNo As Long
    Dim CurDay As Variant
    Dim strWkdy As String
    Dim strDate As String
    Dim tmpRec As String
    Dim tmpVal As String
    Dim LineTotal As Long
    StatisticData(0).ResetContent
    For j = 1 To 15
        StatVal(0, j) = 0
    Next
    CurDay = FirstDay
    LineNo = 0
    For i = 1 To 400
        strWkdy = Trim(Str(Weekday(CurDay, vbMonday)))
        strDate = Format(CurDay, "yyyymmdd")
        tmpRec = strDate & "," & strWkdy & ","
        LineTotal = 0
        For j = 1 To 15
            tmpVal = Trim(Str(StatVal(i, j)))
            If tmpVal = "0" Then tmpVal = ""
            tmpRec = tmpRec & tmpVal & ","
            StatVal(0, j) = StatVal(0, j) + StatVal(i, j)   'Build totals
            LineTotal = LineTotal + StatVal(i, j)
        Next
        If chkWork(1).Value = 0 Then LineTotal = 1
        If LineTotal > 0 Then
            StatisticData(0).InsertTextLine tmpRec, False
            If strWkdy > "5" Then StatisticData(0).SetLineColor LineNo, vbBlack, LightestYellow
            LineNo = LineNo + 1
        End If
        CurDay = DateAdd("d", 1, CurDay)
        If LineNo > PeriodRange Then Exit For
    Next
    tmpRec = ",,"
    For j = 1 To 15
        tmpVal = Trim(Str(StatVal(0, j)))
        If tmpVal = "0" Then tmpVal = ""
        tmpRec = tmpRec & tmpVal & ","
    Next
    StatisticData(1).InsertTextLine tmpRec, False
    StatisticData(0).RedrawTab
    StatisticData(1).RedrawTab
End Sub
Private Sub chkWork_Click(Index As Integer)
    Select Case Index
        Case 0
            'Season
            If chkWork(0).Value = 1 Then
            Else
            End If
        Case 1
            'Shrink
            DisplayStatistic
        Case 2
            If chkWork(Index).Value = 1 Then
                MainDialog.SaveLoadedFile Index, FlightStatistic.StatisticData(0)
                chkWork(Index).Value = 0
            End If
        Case 3
            Me.Hide
            Unload Me
            Exit Sub
        Case Else
    End Select
    If chkWork(Index).Value = 1 Then chkWork(Index).BackColor = LightestGreen Else chkWork(Index).BackColor = vbButtonFace
End Sub


Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyF1 Then DisplayHtmHelpFile "", Me.Name, Me.ActiveControl.Name
End Sub

Private Sub Form_Load()
    InitStatisticList 20, True
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    FlightExtract.chkWork(15).Value = 0
End Sub

Private Sub Form_Resize()
Dim NewVal As Long
    NewVal = Me.ScaleHeight - StatisticData(1).Height - StatusBar.Height - 2 * Screen.TwipsPerPixelY
    If NewVal > 50 Then
        StatisticData(1).Top = NewVal
    End If
    NewVal = StatisticData(1).Top - StatisticData(0).Top '- 4 * Screen.TwipsPerPixelY
    If NewVal > 50 Then
        StatisticData(0).Height = NewVal
    End If
    NewVal = Me.ScaleWidth - 2 * StatisticData(0).Left
    If NewVal > 50 Then
        StatisticData(0).Width = NewVal
        StatisticData(1).Width = NewVal
    End If
End Sub

Private Sub StatisticData_SendLButtonDblClick(Index As Integer, ByVal LineNo As Long, ByVal ColNo As Long)
    If LineNo = -1 Then
        If Index = 0 Then
            MousePointer = 11
            StatisticData(Index).Sort Str(ColNo), True, True
            StatisticData(Index).RedrawTab
            MousePointer = 0
        End If
    End If
End Sub
