VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.ocx"
Object = "{64E8E382-05E2-11D2-9B1D-9B0630BC8F12}#1.0#0"; "TAB.ocx"
Begin VB.Form PreView 
   Caption         =   "Import Filter Result Preview"
   ClientHeight    =   3585
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   7155
   BeginProperty Font 
      Name            =   "Courier New"
      Size            =   8.25
      Charset         =   161
      Weight          =   700
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "Preview.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   3585
   ScaleWidth      =   7155
   ShowInTaskbar   =   0   'False
   Begin VB.PictureBox PreviewPanel 
      Height          =   1815
      Left            =   0
      ScaleHeight     =   1755
      ScaleWidth      =   4455
      TabIndex        =   6
      Top             =   420
      Width           =   4515
      Begin TABLib.TAB FullFilter 
         Height          =   1125
         Index           =   0
         Left            =   0
         TabIndex        =   7
         Top             =   0
         Width           =   4095
         _Version        =   65536
         _ExtentX        =   7223
         _ExtentY        =   1984
         _StockProps     =   64
      End
   End
   Begin VB.CheckBox chkWork 
      Caption         =   "Identify"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   3
      Left            =   930
      Style           =   1  'Graphical
      TabIndex        =   5
      Top             =   60
      Width           =   855
   End
   Begin MSComctlLib.StatusBar StatusBar 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   4
      Top             =   3300
      Width           =   7155
      _ExtentX        =   12621
      _ExtentY        =   503
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   2
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   9551
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
         EndProperty
      EndProperty
   End
   Begin VB.CheckBox chkWork 
      Caption         =   "Save As"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   2
      Left            =   1800
      Style           =   1  'Graphical
      TabIndex        =   3
      Top             =   60
      Width           =   855
   End
   Begin VB.CheckBox chkWork 
      Caption         =   "Compare"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   1
      Left            =   60
      Style           =   1  'Graphical
      TabIndex        =   2
      Top             =   60
      Width           =   855
   End
   Begin VB.CheckBox chkWork 
      Caption         =   "Close"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   0
      Left            =   2670
      Style           =   1  'Graphical
      TabIndex        =   0
      Top             =   60
      Width           =   855
   End
   Begin VB.Label lblCount 
      Alignment       =   2  'Center
      BorderStyle     =   1  'Fixed Single
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   3570
      TabIndex        =   1
      Top             =   60
      Width           =   945
   End
End
Attribute VB_Name = "PreView"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub chkWork_Click(Index As Integer)
    If chkWork(Index).Value = 0 Then chkWork(Index).BackColor = vbButtonFace
    Select Case Index
        Case 0
            'Close
            If chkWork(Index) = 1 Then Me.Hide
            chkWork(Index) = 0
        Case 1
            If chkWork(Index) = 1 Then
                'CompareFlights
                chkWork(Index) = 0
            End If
        Case 2
            'Save As
            If chkWork(Index).Value = 1 Then
                MainDialog.SaveLoadedFile Index, PreView.FullFilter(0)
                chkWork(Index).Value = 0
            End If
        Case 3
            'Identify
            IdentifyFlights
    End Select
    If chkWork(Index).Value = 1 Then chkWork(Index).BackColor = LightGreen
End Sub
Public Sub IdentifyFlights()
    Dim CurLine As Long
    Dim MaxLine As Long
    Dim ArrBackColor As Long
    Dim DepBackColor As Long
    Dim ErrBackColor As Long
    Dim ArrForeColor As Long
    Dim DepForeColor As Long
    Dim ErrForeColor As Long
    Dim tmpAdid
    Screen.MousePointer = 11
    If chkWork(3).Value = 1 Then
        ArrBackColor = vbYellow
        DepBackColor = vbGreen
        ErrBackColor = vbRed
        ArrForeColor = vbBlack
        DepForeColor = vbBlack
        ErrForeColor = vbWhite
    Else
        ArrBackColor = vbWhite
        DepBackColor = vbWhite
        ErrBackColor = vbRed
        ArrForeColor = vbBlack
        DepForeColor = vbBlack
        ErrForeColor = vbWhite
    End If
    MaxLine = FullFilter(0).GetLineCount - 1
    For CurLine = 0 To MaxLine
        tmpAdid = Trim(FullFilter(0).GetColumnValue(CurLine, 2))
        Select Case tmpAdid
            Case "A"
                FullFilter(0).SetLineColor CurLine, ArrForeColor, ArrBackColor
            Case "D"
                FullFilter(0).SetLineColor CurLine, DepForeColor, DepBackColor
            Case Else
                FullFilter(0).SetLineColor CurLine, ErrForeColor, ErrBackColor
        End Select
    Next
    FullFilter(0).Refresh
    Screen.MousePointer = 0
End Sub
Private Sub Form_Load()
    FullFilter(0).ResetContent
    FullFilter(0).FontName = "Courier New"
    FullFilter(0).SetTabFontBold True
    FullFilter(0).HeaderFontSize = 17
    FullFilter(0).FontSize = 17
    FullFilter(0).LineHeight = 17
    FullFilter(0).HeaderLengthString = "60,60,60,60,60,60,60,60,60,60,60,60,60,60"
    FullFilter(0).HeaderString = "LINE,T,R,ALC,FLNO,TYP,APC,VIA,DATE,W,TIME,N,Remark,MainIdx"
    FullFilter(0).ColumnWidthString = "5,1,1,3,9,3,3,3,8,1,4,1,20,5"
    FullFilter(0).ColumnAlignmentString = "R,L,L,L,L,L,L,C,C,L,L,L,L,R"
    FullFilter(0).DateTimeSetColumn 8
    FullFilter(0).DateTimeSetInputFormatString 8, "YYYYMMDD"
    FullFilter(0).DateTimeSetOutputFormatString 8, "DDMMMYY"
    FullFilter(0).ShowHorzScroller True
    FullFilter(0).Tag = ""
    FullFilter(0).AutoSizeByHeader = True
    FullFilter(0).AutoSizeColumns
    FullFilter(0).LifeStyle = True
    FullFilter(0).CursorLifeStyle = True
    chkWork(3).Value = 1
    Me.Left = FlightExtract.Left
    Me.Top = FlightExtract.Top
    Me.Height = FlightExtract.Height
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    Me.Hide
    Cancel = True
End Sub

Private Sub Form_Resize()
    Dim NewVal As Long
    NewVal = Me.ScaleHeight - StatusBar.Height - PreviewPanel.Top - 30
    If NewVal > 600 Then
        PreviewPanel.Height = NewVal
        FullFilter(0).Height = NewVal - 60
    End If
    NewVal = Me.ScaleWidth - 30
    If NewVal > 600 Then
        PreviewPanel.Width = NewVal
        FullFilter(0).Width = NewVal - 60
    End If
End Sub

Private Sub FullFilter_SendLButtonDblClick(Index As Integer, ByVal LineNo As Long, ByVal ColNo As Long)
    If LineNo < 0 Then
        Screen.MousePointer = 11
        FullFilter(Index).Sort CStr(ColNo), True, True
        FullFilter(0).AutoSizeColumns
        FullFilter(Index).Refresh
        Screen.MousePointer = 0
    End If
End Sub

Public Sub SortDefault()
    Screen.MousePointer = 11
    FullFilter(0).Sort "0", True, True
    FullFilter(0).Sort "8", True, True
    FullFilter(0).Sort "4", True, True
    FullFilter(0).AutoSizeColumns
    FullFilter(0).Refresh
    Screen.MousePointer = 0
End Sub
