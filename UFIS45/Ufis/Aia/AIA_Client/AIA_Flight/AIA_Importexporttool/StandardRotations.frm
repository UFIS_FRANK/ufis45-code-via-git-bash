VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{64E8E382-05E2-11D2-9B1D-9B0630BC8F12}#1.0#0"; "TAB.ocx"
Begin VB.Form StandardRotations 
   Caption         =   "Standard Rotations"
   ClientHeight    =   8040
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   15225
   Icon            =   "StandardRotations.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   8040
   ScaleWidth      =   15225
   Begin TABLib.TAB AftTabData 
      Height          =   1125
      Index           =   0
      Left            =   10470
      TabIndex        =   134
      Top             =   5250
      Visible         =   0   'False
      Width           =   2805
      _Version        =   65536
      _ExtentX        =   4948
      _ExtentY        =   1984
      _StockProps     =   64
   End
   Begin MSComctlLib.StatusBar StatusBar 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   8
      Top             =   7755
      Width           =   15225
      _ExtentX        =   26855
      _ExtentY        =   503
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   2
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   23760
         EndProperty
      EndProperty
   End
   Begin VB.Frame TimeScalePanel 
      Caption         =   "Time Schedule Info"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1305
      Left            =   2040
      TabIndex        =   56
      Top             =   390
      Width           =   13125
      Begin VB.HScrollBar TimeScroll 
         Height          =   285
         Left            =   90
         TabIndex        =   100
         Top             =   270
         Width           =   1845
      End
      Begin VB.Frame CalendarFrame 
         Height          =   1290
         Left            =   2070
         TabIndex        =   98
         Top             =   15
         Width           =   10965
         Begin VB.Frame CalendarPanel 
            BorderStyle     =   0  'None
            Height          =   1275
            Index           =   0
            Left            =   0
            TabIndex        =   99
            Top             =   0
            Width           =   3015
            Begin VB.PictureBox MonthCover 
               AutoRedraw      =   -1  'True
               BackColor       =   &H00E0E0E0&
               Height          =   375
               Index           =   0
               Left            =   0
               ScaleHeight     =   315
               ScaleWidth      =   495
               TabIndex        =   136
               Top             =   240
               Visible         =   0   'False
               Width           =   555
               Begin VB.Label ArrDays 
                  Alignment       =   2  'Center
                  Appearance      =   0  'Flat
                  BackColor       =   &H00C0C0C0&
                  BorderStyle     =   1  'Fixed Single
                  ForeColor       =   &H80000008&
                  Height          =   90
                  Index           =   0
                  Left            =   0
                  TabIndex        =   140
                  Top             =   0
                  Visible         =   0   'False
                  Width           =   135
               End
               Begin VB.Label DepDays 
                  Alignment       =   2  'Center
                  Appearance      =   0  'Flat
                  BackColor       =   &H00C0C0C0&
                  BorderStyle     =   1  'Fixed Single
                  ForeColor       =   &H80000008&
                  Height          =   90
                  Index           =   0
                  Left            =   120
                  TabIndex        =   139
                  Top             =   0
                  Visible         =   0   'False
                  Width           =   135
               End
               Begin VB.Label ArrDisp 
                  Alignment       =   2  'Center
                  Appearance      =   0  'Flat
                  BackColor       =   &H00C0C0C0&
                  BorderStyle     =   1  'Fixed Single
                  ForeColor       =   &H80000008&
                  Height          =   90
                  Index           =   0
                  Left            =   0
                  TabIndex        =   138
                  Top             =   75
                  Visible         =   0   'False
                  Width           =   135
               End
               Begin VB.Label DepDisp 
                  Alignment       =   2  'Center
                  Appearance      =   0  'Flat
                  BackColor       =   &H00C0C0C0&
                  BorderStyle     =   1  'Fixed Single
                  ForeColor       =   &H80000008&
                  Height          =   90
                  Index           =   0
                  Left            =   120
                  TabIndex        =   137
                  Top             =   75
                  Visible         =   0   'False
                  Width           =   135
               End
            End
            Begin VB.Label MonthLabel 
               Alignment       =   2  'Center
               Appearance      =   0  'Flat
               BackColor       =   &H00C0C0C0&
               BorderStyle     =   1  'Fixed Single
               Caption         =   "12"
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   6
                  Charset         =   161
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H80000008&
               Height          =   180
               Index           =   0
               Left            =   480
               TabIndex        =   101
               Top             =   0
               Visible         =   0   'False
               Width           =   555
            End
         End
      End
      Begin VB.CheckBox chkTime 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   2
         Left            =   1080
         Style           =   1  'Graphical
         TabIndex        =   96
         Top             =   270
         Visible         =   0   'False
         Width           =   855
      End
      Begin VB.CheckBox chkTime 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   1
         Left            =   960
         Style           =   1  'Graphical
         TabIndex        =   95
         Top             =   270
         Visible         =   0   'False
         Width           =   120
      End
      Begin VB.CheckBox chkTime 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   0
         Left            =   90
         Style           =   1  'Graphical
         TabIndex        =   94
         Top             =   270
         Visible         =   0   'False
         Width           =   855
      End
      Begin VB.TextBox TimeScaleInfo 
         Alignment       =   2  'Center
         BackColor       =   &H00E0E0E0&
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   4
         Left            =   1470
         Locked          =   -1  'True
         TabIndex        =   61
         Top             =   930
         Width           =   465
      End
      Begin VB.TextBox TimeScaleInfo 
         Alignment       =   2  'Center
         BackColor       =   &H00E0E0E0&
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   3
         Left            =   1020
         Locked          =   -1  'True
         TabIndex        =   60
         Top             =   630
         Width           =   915
      End
      Begin VB.TextBox TimeScaleInfo 
         Alignment       =   2  'Center
         BackColor       =   &H00E0E0E0&
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   2
         Left            =   90
         Locked          =   -1  'True
         TabIndex        =   59
         Top             =   630
         Width           =   915
      End
      Begin VB.TextBox TimeScaleInfo 
         Alignment       =   2  'Center
         BackColor       =   &H00E0E0E0&
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   1
         Left            =   1020
         Locked          =   -1  'True
         TabIndex        =   58
         Top             =   930
         Width           =   435
      End
      Begin VB.TextBox TimeScaleInfo 
         Alignment       =   2  'Center
         BackColor       =   &H00E0E0E0&
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   0
         Left            =   90
         Locked          =   -1  'True
         TabIndex        =   57
         Top             =   930
         Width           =   915
      End
   End
   Begin VB.Frame SplitPanel 
      Caption         =   "Action Parameter"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1305
      Left            =   2040
      TabIndex        =   45
      Top             =   390
      Width           =   2775
      Begin VB.CheckBox chkSplit 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   4
         Left            =   1830
         Style           =   1  'Graphical
         TabIndex        =   91
         Top             =   270
         Width           =   855
      End
      Begin VB.CheckBox chkSplit 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   3
         Left            =   960
         Style           =   1  'Graphical
         TabIndex        =   90
         Top             =   270
         Width           =   855
      End
      Begin VB.CheckBox chkSplit 
         Caption         =   "Rotation"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   2
         Left            =   90
         Style           =   1  'Graphical
         TabIndex        =   89
         ToolTipText     =   "Perform action by flight rotation"
         Top             =   930
         Width           =   855
      End
      Begin VB.CheckBox chkSplit 
         Caption         =   "Period"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   1
         Left            =   90
         Style           =   1  'Graphical
         TabIndex        =   55
         ToolTipText     =   "Perform action by period dates"
         Top             =   600
         Width           =   855
      End
      Begin VB.CheckBox chkSplit 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   0
         Left            =   90
         Style           =   1  'Graphical
         TabIndex        =   54
         Top             =   270
         Width           =   855
      End
      Begin VB.TextBox txtSplitFr 
         Alignment       =   2  'Center
         BackColor       =   &H00E0E0E0&
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   0
         Left            =   1380
         TabIndex        =   51
         Top             =   630
         Width           =   585
      End
      Begin VB.TextBox txtSplitTo 
         Alignment       =   2  'Center
         BackColor       =   &H00E0E0E0&
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   0
         Left            =   1380
         TabIndex        =   50
         Top             =   930
         Width           =   585
      End
      Begin VB.TextBox txtSplitFr 
         Alignment       =   2  'Center
         BackColor       =   &H00E0E0E0&
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   1
         Left            =   1980
         TabIndex        =   49
         Top             =   630
         Width           =   345
      End
      Begin VB.TextBox txtSplitFr 
         Alignment       =   2  'Center
         BackColor       =   &H00E0E0E0&
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   2
         Left            =   2340
         TabIndex        =   48
         Top             =   630
         Width           =   345
      End
      Begin VB.TextBox txtSplitTo 
         Alignment       =   2  'Center
         BackColor       =   &H00E0E0E0&
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   1
         Left            =   1980
         TabIndex        =   47
         Top             =   930
         Width           =   345
      End
      Begin VB.TextBox txtSplitTo 
         Alignment       =   2  'Center
         BackColor       =   &H00E0E0E0&
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   2
         Left            =   2340
         TabIndex        =   46
         Top             =   930
         Width           =   345
      End
      Begin VB.Label lblSplit 
         Caption         =   "Beg."
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   225
         Index           =   0
         Left            =   990
         TabIndex        =   53
         Top             =   660
         Width           =   495
      End
      Begin VB.Label lblSplit 
         Caption         =   "End"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   1
         Left            =   990
         TabIndex        =   52
         Top             =   1005
         Width           =   345
      End
   End
   Begin VB.Frame PeriodPanel 
      Caption         =   "Period Filter"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1305
      Left            =   30
      TabIndex        =   11
      Top             =   390
      Width           =   1935
      Begin VB.CheckBox chkLoad 
         Caption         =   "File"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   2
         Left            =   1290
         Style           =   1  'Graphical
         TabIndex        =   107
         Top             =   270
         Width           =   570
      End
      Begin VB.CheckBox chkLoad 
         Caption         =   "AFT"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   0
         Left            =   120
         Style           =   1  'Graphical
         TabIndex        =   93
         Top             =   270
         Width           =   570
      End
      Begin VB.CheckBox chkLoad 
         Caption         =   "SSI"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   1
         Left            =   705
         Style           =   1  'Graphical
         TabIndex        =   92
         Top             =   270
         Width           =   570
      End
      Begin VB.TextBox txtVpto 
         Alignment       =   2  'Center
         BackColor       =   &H0080FFFF&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   2
         Left            =   1500
         TabIndex        =   5
         Top             =   930
         Width           =   345
      End
      Begin VB.TextBox txtVpto 
         Alignment       =   2  'Center
         BackColor       =   &H0080FFFF&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   1
         Left            =   1140
         TabIndex        =   4
         Top             =   930
         Width           =   345
      End
      Begin VB.TextBox txtVpfr 
         Alignment       =   2  'Center
         BackColor       =   &H0080FFFF&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   2
         Left            =   1500
         TabIndex        =   2
         Top             =   630
         Width           =   345
      End
      Begin VB.TextBox txtVpfr 
         Alignment       =   2  'Center
         BackColor       =   &H0080FFFF&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   1
         Left            =   1140
         TabIndex        =   1
         Top             =   630
         Width           =   345
      End
      Begin VB.TextBox txtVpto 
         Alignment       =   2  'Center
         BackColor       =   &H0080FFFF&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   0
         Left            =   540
         TabIndex        =   3
         Top             =   930
         Width           =   585
      End
      Begin VB.TextBox txtVpfr 
         Alignment       =   2  'Center
         BackColor       =   &H0080FFFF&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   0
         Left            =   540
         TabIndex        =   0
         Top             =   630
         Width           =   585
      End
      Begin VB.Label lblPeriod 
         Caption         =   "End"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   1
         Left            =   120
         TabIndex        =   13
         Top             =   975
         Width           =   345
      End
      Begin VB.Label lblPeriod 
         Caption         =   "Beg."
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   225
         Index           =   0
         Left            =   120
         TabIndex        =   12
         Top             =   660
         Width           =   495
      End
   End
   Begin VB.Frame TopButtonPanel 
      BorderStyle     =   0  'None
      Height          =   315
      Index           =   0
      Left            =   30
      TabIndex        =   30
      Top             =   30
      Width           =   15165
      Begin VB.CheckBox chkWork 
         Caption         =   " Shrink"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   14
         Left            =   8610
         Style           =   1  'Graphical
         TabIndex        =   135
         Top             =   0
         Visible         =   0   'False
         Width           =   855
      End
      Begin VB.CheckBox chkWork 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   16
         Left            =   5610
         Style           =   1  'Graphical
         TabIndex        =   103
         Top             =   0
         Width           =   375
      End
      Begin VB.CheckBox chkWork 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   15
         Left            =   2610
         Style           =   1  'Graphical
         TabIndex        =   102
         Top             =   0
         Width           =   375
      End
      Begin VB.CheckBox chkSwitch 
         Caption         =   "Switch"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   0
         Left            =   0
         Style           =   1  'Graphical
         TabIndex        =   88
         Top             =   0
         Width           =   855
      End
      Begin VB.CheckBox chkWork 
         Caption         =   "Close"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   17
         Left            =   10740
         Style           =   1  'Graphical
         TabIndex        =   44
         Top             =   0
         Width           =   855
      End
      Begin VB.CheckBox chkWork 
         Caption         =   "Server"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   13
         Left            =   6870
         Style           =   1  'Graphical
         TabIndex        =   43
         Top             =   0
         Visible         =   0   'False
         Width           =   855
      End
      Begin VB.CheckBox chkWork 
         Caption         =   "Transfer"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   12
         Left            =   9480
         Style           =   1  'Graphical
         TabIndex        =   42
         Top             =   0
         Width           =   855
      End
      Begin VB.CheckBox chkWork 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   11
         Left            =   10350
         Style           =   1  'Graphical
         TabIndex        =   41
         Top             =   0
         Visible         =   0   'False
         Width           =   375
      End
      Begin VB.CheckBox chkWork 
         Caption         =   "Create"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   10
         Left            =   7740
         Style           =   1  'Graphical
         TabIndex        =   40
         Top             =   0
         Width           =   855
      End
      Begin VB.CheckBox chkWork 
         Caption         =   "Extract"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   9
         Left            =   6000
         Style           =   1  'Graphical
         TabIndex        =   39
         Top             =   0
         Width           =   855
      End
      Begin VB.CheckBox chkWork 
         Caption         =   "Batch"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   8
         Left            =   14220
         Style           =   1  'Graphical
         TabIndex        =   38
         Top             =   0
         Visible         =   0   'False
         Width           =   855
      End
      Begin VB.CheckBox chkWork 
         Caption         =   "Join"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   7
         Left            =   12870
         Style           =   1  'Graphical
         TabIndex        =   37
         Top             =   0
         Visible         =   0   'False
         Width           =   855
      End
      Begin VB.CheckBox chkWork 
         Caption         =   "Split"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   6
         Left            =   12000
         Style           =   1  'Graphical
         TabIndex        =   36
         Top             =   0
         Visible         =   0   'False
         Width           =   855
      End
      Begin VB.CheckBox chkWork 
         Caption         =   "Delete"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   5
         Left            =   4740
         Style           =   1  'Graphical
         TabIndex        =   35
         Top             =   0
         Width           =   855
      End
      Begin VB.CheckBox chkWork 
         Caption         =   " "
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   2
         Left            =   13770
         Style           =   1  'Graphical
         TabIndex        =   34
         Top             =   0
         Visible         =   0   'False
         Width           =   375
      End
      Begin VB.CheckBox chkWork 
         Caption         =   "Save"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   1
         Left            =   1740
         Style           =   1  'Graphical
         TabIndex        =   33
         Top             =   0
         Width           =   855
      End
      Begin VB.CheckBox chkWork 
         Caption         =   "Update"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   4
         Left            =   3870
         Style           =   1  'Graphical
         TabIndex        =   32
         Top             =   0
         Width           =   855
      End
      Begin VB.CheckBox chkWork 
         Caption         =   "Insert"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   3
         Left            =   3000
         Style           =   1  'Graphical
         TabIndex        =   31
         Top             =   0
         Width           =   855
      End
      Begin VB.CheckBox chkWork 
         Caption         =   "Load"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   0
         Left            =   870
         Style           =   1  'Graphical
         TabIndex        =   6
         Top             =   0
         Width           =   855
      End
   End
   Begin VB.Frame FilterFrame 
      BorderStyle     =   0  'None
      Height          =   3975
      Index           =   0
      Left            =   30
      TabIndex        =   14
      Top             =   2640
      Visible         =   0   'False
      Width           =   1995
      Begin VB.CheckBox chkFilter 
         Caption         =   "Airl"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   0
         Left            =   90
         Style           =   1  'Graphical
         TabIndex        =   21
         ToolTipText     =   "Activate an airline filter "
         Top             =   210
         Width           =   405
      End
      Begin VB.Frame FilterPanel 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   3885
         Index           =   0
         Left            =   0
         TabIndex        =   15
         Top             =   0
         Width           =   1935
         Begin VB.CheckBox chkAlc3 
            Caption         =   "3"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Index           =   1
            Left            =   720
            Style           =   1  'Graphical
            TabIndex        =   123
            ToolTipText     =   "Display Airline 3LC"
            Top             =   210
            Width           =   225
         End
         Begin VB.CheckBox chkAlc3 
            Caption         =   "2"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Index           =   0
            Left            =   510
            Style           =   1  'Graphical
            TabIndex        =   122
            ToolTipText     =   "Display Airline 2LC"
            Top             =   210
            Width           =   225
         End
         Begin VB.TextBox txtFilterVal 
            Alignment       =   2  'Center
            BackColor       =   &H00E0E0E0&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   0
            Left            =   990
            TabIndex        =   62
            Top             =   180
            Width           =   855
         End
         Begin VB.Frame ButtonPanel 
            BorderStyle     =   0  'None
            BeginProperty Font 
               Name            =   "Courier New"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Index           =   0
            Left            =   90
            TabIndex        =   16
            Top             =   3510
            Width           =   1785
            Begin VB.CheckBox chkFilterSet 
               Caption         =   "0"
               Enabled         =   0   'False
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   161
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Index           =   1
               Left            =   900
               Style           =   1  'Graphical
               TabIndex        =   18
               Top             =   0
               Width           =   855
            End
            Begin VB.CheckBox chkFilterSet 
               Caption         =   "0"
               Enabled         =   0   'False
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   161
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Index           =   0
               Left            =   0
               Style           =   1  'Graphical
               TabIndex        =   17
               Top             =   0
               Width           =   855
            End
         End
         Begin TABLib.TAB FilterTab 
            Height          =   2925
            Index           =   0
            Left            =   90
            TabIndex        =   19
            Top             =   540
            Width           =   855
            _Version        =   65536
            _ExtentX        =   1508
            _ExtentY        =   5159
            _StockProps     =   64
            Columns         =   10
         End
         Begin TABLib.TAB FilterTab 
            Height          =   2925
            Index           =   1
            Left            =   990
            TabIndex        =   20
            Top             =   540
            Width           =   855
            _Version        =   65536
            _ExtentX        =   1508
            _ExtentY        =   5159
            _StockProps     =   64
            Columns         =   10
         End
      End
   End
   Begin VB.Frame FilterFrame 
      BorderStyle     =   0  'None
      Height          =   3975
      Index           =   2
      Left            =   2010
      TabIndex        =   22
      Top             =   2640
      Visible         =   0   'False
      Width           =   1995
      Begin VB.CheckBox chkFilter 
         Caption         =   "Aircraft"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   2
         Left            =   90
         Style           =   1  'Graphical
         TabIndex        =   29
         ToolTipText     =   "Activate an aircraft type filter "
         Top             =   210
         Width           =   855
      End
      Begin VB.Frame FilterPanel 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   3885
         Index           =   2
         Left            =   0
         TabIndex        =   23
         Top             =   0
         Width           =   1935
         Begin VB.TextBox txtFilterVal 
            Alignment       =   2  'Center
            BackColor       =   &H00E0E0E0&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   2
            Left            =   990
            TabIndex        =   63
            Top             =   180
            Width           =   855
         End
         Begin VB.Frame ButtonPanel 
            BorderStyle     =   0  'None
            BeginProperty Font 
               Name            =   "Courier New"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Index           =   2
            Left            =   90
            TabIndex        =   24
            Top             =   3510
            Width           =   1785
            Begin VB.CheckBox chkFilterSet 
               Caption         =   "0"
               Enabled         =   0   'False
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   161
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Index           =   3
               Left            =   900
               Style           =   1  'Graphical
               TabIndex        =   26
               Top             =   0
               Width           =   855
            End
            Begin VB.CheckBox chkFilterSet 
               Caption         =   "0"
               Enabled         =   0   'False
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   161
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Index           =   2
               Left            =   0
               Style           =   1  'Graphical
               TabIndex        =   25
               Top             =   0
               Width           =   855
            End
         End
         Begin TABLib.TAB FilterTab 
            Height          =   2925
            Index           =   3
            Left            =   990
            TabIndex        =   27
            Top             =   540
            Width           =   855
            _Version        =   65536
            _ExtentX        =   1508
            _ExtentY        =   5159
            _StockProps     =   64
            Columns         =   10
         End
         Begin TABLib.TAB FilterTab 
            Height          =   2925
            Index           =   2
            Left            =   90
            TabIndex        =   28
            Top             =   540
            Width           =   855
            _Version        =   65536
            _ExtentX        =   1508
            _ExtentY        =   5159
            _StockProps     =   64
            Columns         =   10
         End
      End
   End
   Begin VB.Frame FilterFrame 
      BorderStyle     =   0  'None
      Height          =   3975
      Index           =   4
      Left            =   3990
      TabIndex        =   64
      Top             =   2640
      Visible         =   0   'False
      Width           =   3165
      Begin VB.CheckBox chkFilter 
         Caption         =   "Groups"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   4
         Left            =   90
         Style           =   1  'Graphical
         TabIndex        =   65
         ToolTipText     =   "Activate an aircraft group filter "
         Top             =   210
         Width           =   1440
      End
      Begin VB.Frame FilterPanel 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   3885
         Index           =   4
         Left            =   0
         TabIndex        =   66
         Top             =   0
         Width           =   3105
         Begin VB.Frame ButtonPanel 
            BorderStyle     =   0  'None
            BeginProperty Font 
               Name            =   "Courier New"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Index           =   4
            Left            =   90
            TabIndex        =   68
            Top             =   3510
            Width           =   2955
            Begin VB.CheckBox chkFilterSet 
               Caption         =   "0"
               Enabled         =   0   'False
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   161
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Index           =   4
               Left            =   0
               Style           =   1  'Graphical
               TabIndex        =   70
               Top             =   0
               Width           =   1440
            End
            Begin VB.CheckBox chkFilterSet 
               Caption         =   "0"
               Enabled         =   0   'False
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   161
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Index           =   5
               Left            =   1500
               Style           =   1  'Graphical
               TabIndex        =   69
               Top             =   0
               Width           =   1440
            End
         End
         Begin VB.TextBox txtFilterVal 
            Alignment       =   2  'Center
            BackColor       =   &H00E0E0E0&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   4
            Left            =   1575
            TabIndex        =   67
            Top             =   180
            Width           =   1440
         End
         Begin TABLib.TAB FilterTab 
            Height          =   2925
            Index           =   5
            Left            =   1575
            TabIndex        =   71
            Top             =   540
            Width           =   1440
            _Version        =   65536
            _ExtentX        =   2540
            _ExtentY        =   5159
            _StockProps     =   64
            Columns         =   10
         End
         Begin TABLib.TAB FilterTab 
            Height          =   2925
            Index           =   4
            Left            =   90
            TabIndex        =   72
            Top             =   540
            Width           =   1440
            _Version        =   65536
            _ExtentX        =   2540
            _ExtentY        =   5159
            _StockProps     =   64
            Columns         =   10
         End
      End
   End
   Begin VB.Frame FilterFrame 
      BorderStyle     =   0  'None
      Height          =   3975
      Index           =   6
      Left            =   7260
      TabIndex        =   73
      Top             =   2640
      Visible         =   0   'False
      Width           =   3165
      Begin VB.CheckBox chkGroupMaster 
         Caption         =   "Group"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   6
         Left            =   1020
         Style           =   1  'Graphical
         TabIndex        =   74
         ToolTipText     =   "Show the selected aircraft group"
         Top             =   210
         Width           =   945
      End
      Begin VB.Frame FilterPanel 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   3885
         Index           =   6
         Left            =   0
         TabIndex        =   75
         Top             =   0
         Width           =   3105
         Begin VB.TextBox GrnUrno 
            Height          =   315
            Left            =   30
            TabIndex        =   121
            Text            =   "GRN.URNO"
            Top             =   2220
            Visible         =   0   'False
            Width           =   975
         End
         Begin VB.TextBox txtFilterVal 
            Alignment       =   2  'Center
            BackColor       =   &H00E0E0E0&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   6
            Left            =   2040
            TabIndex        =   97
            Top             =   180
            Width           =   960
         End
         Begin VB.CheckBox chkGroup 
            Caption         =   "Delete"
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Index           =   2
            Left            =   90
            Style           =   1  'Graphical
            TabIndex        =   85
            ToolTipText     =   "Delete the selected aircraft group"
            Top             =   1590
            Width           =   855
         End
         Begin VB.CheckBox chkGroup 
            Caption         =   "Update"
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Index           =   1
            Left            =   90
            Style           =   1  'Graphical
            TabIndex        =   84
            ToolTipText     =   "Update the selected aircraft group"
            Top             =   1260
            Width           =   855
         End
         Begin VB.CheckBox chkGroup 
            Caption         =   "New"
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Index           =   0
            Left            =   90
            Style           =   1  'Graphical
            TabIndex        =   83
            ToolTipText     =   "Define a new aircraft group"
            Top             =   930
            Width           =   855
         End
         Begin VB.TextBox ActGrpName 
            Alignment       =   2  'Center
            BackColor       =   &H00E0E0E0&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   0
            Left            =   90
            TabIndex        =   82
            Top             =   540
            Width           =   2910
         End
         Begin VB.TextBox ActGrpName 
            Alignment       =   2  'Center
            BackColor       =   &H00E0E0E0&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   1
            Left            =   90
            TabIndex        =   79
            Top             =   180
            Width           =   855
         End
         Begin VB.Frame ButtonPanel 
            BorderStyle     =   0  'None
            BeginProperty Font 
               Name            =   "Courier New"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Index           =   6
            Left            =   90
            TabIndex        =   76
            Top             =   3510
            Width           =   2955
            Begin VB.CheckBox chkGroup 
               Caption         =   "Save"
               Enabled         =   0   'False
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   161
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Index           =   3
               Left            =   0
               Style           =   1  'Graphical
               TabIndex        =   86
               ToolTipText     =   "Save the changes"
               Top             =   0
               Width           =   855
            End
            Begin VB.CheckBox chkFilterSet 
               Caption         =   "0"
               Enabled         =   0   'False
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   161
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Index           =   7
               Left            =   1950
               Style           =   1  'Graphical
               TabIndex        =   78
               Top             =   0
               Width           =   975
            End
            Begin VB.CheckBox chkFilterSet 
               Caption         =   "0"
               Enabled         =   0   'False
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   161
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Index           =   6
               Left            =   930
               Style           =   1  'Graphical
               TabIndex        =   77
               Top             =   0
               Width           =   975
            End
         End
         Begin TABLib.TAB FilterTab 
            Height          =   2505
            Index           =   7
            Left            =   2040
            TabIndex        =   80
            Top             =   930
            Width           =   975
            _Version        =   65536
            _ExtentX        =   1720
            _ExtentY        =   4419
            _StockProps     =   64
            Columns         =   10
         End
         Begin TABLib.TAB FilterTab 
            Height          =   2505
            Index           =   6
            Left            =   1020
            TabIndex        =   81
            Top             =   930
            Width           =   975
            _Version        =   65536
            _ExtentX        =   1720
            _ExtentY        =   4419
            _StockProps     =   64
            Columns         =   10
         End
      End
   End
   Begin TABLib.TAB StrTabData 
      Height          =   3135
      Index           =   0
      Left            =   30
      TabIndex        =   7
      Top             =   1800
      Width           =   9945
      _Version        =   65536
      _ExtentX        =   17542
      _ExtentY        =   5530
      _StockProps     =   64
   End
   Begin TABLib.TAB StrTabData 
      Height          =   2415
      Index           =   1
      Left            =   30
      TabIndex        =   87
      Top             =   4530
      Width           =   9945
      _Version        =   65536
      _ExtentX        =   17542
      _ExtentY        =   4260
      _StockProps     =   64
   End
   Begin TABLib.TAB Flights 
      Height          =   1875
      Index           =   1
      Left            =   9975
      TabIndex        =   10
      Top             =   2790
      Width           =   5205
      _Version        =   65536
      _ExtentX        =   9181
      _ExtentY        =   3307
      _StockProps     =   64
   End
   Begin TABLib.TAB Flights 
      Height          =   1125
      Index           =   0
      Left            =   9975
      TabIndex        =   9
      Top             =   1800
      Width           =   5205
      _Version        =   65536
      _ExtentX        =   9181
      _ExtentY        =   1984
      _StockProps     =   64
   End
   Begin VB.Frame Splitter 
      Height          =   465
      Index           =   1
      Left            =   9960
      TabIndex        =   106
      Top             =   4710
      Width           =   5205
      Begin VB.PictureBox SplitAnchor 
         AutoRedraw      =   -1  'True
         Height          =   285
         Index           =   1
         Left            =   2520
         MousePointer    =   7  'Size N S
         ScaleHeight     =   225
         ScaleWidth      =   105
         TabIndex        =   142
         Tag             =   "-1"
         Top             =   135
         Width           =   165
      End
      Begin VB.Label DepLineCnt 
         Alignment       =   1  'Right Justify
         BackColor       =   &H000000FF&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   285
         Index           =   3
         Left            =   4680
         TabIndex        =   115
         Top             =   135
         Width           =   465
      End
      Begin VB.Label DepLineCnt 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   2
         Left            =   4020
         TabIndex        =   114
         Top             =   135
         Width           =   645
      End
      Begin VB.Label DepLineCnt 
         Alignment       =   1  'Right Justify
         BackColor       =   &H0000FF00&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   1
         Left            =   3360
         TabIndex        =   113
         Top             =   135
         Width           =   645
      End
      Begin VB.Label DepLineCnt 
         Alignment       =   1  'Right Justify
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   0
         Left            =   2700
         TabIndex        =   112
         Top             =   135
         Width           =   645
      End
      Begin VB.Label ArrLineCnt 
         Alignment       =   1  'Right Justify
         BackColor       =   &H000000FF&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   285
         Index           =   3
         Left            =   2040
         TabIndex        =   111
         Top             =   135
         Width           =   465
      End
      Begin VB.Label ArrLineCnt 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   2
         Left            =   1380
         TabIndex        =   110
         Top             =   135
         Width           =   645
      End
      Begin VB.Label ArrLineCnt 
         Alignment       =   1  'Right Justify
         BackColor       =   &H0000FFFF&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   1
         Left            =   720
         TabIndex        =   109
         Top             =   135
         Width           =   645
      End
      Begin VB.Label ArrLineCnt 
         Alignment       =   1  'Right Justify
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   0
         Left            =   60
         TabIndex        =   108
         Top             =   135
         Width           =   645
      End
   End
   Begin VB.Frame Splitter 
      Height          =   465
      Index           =   2
      Left            =   8010
      TabIndex        =   119
      Top             =   6960
      Width           =   1965
      Begin VB.Label RotLineCnt 
         Alignment       =   1  'Right Justify
         BackColor       =   &H0000FF00&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   2
         Left            =   1350
         TabIndex        =   133
         Top             =   135
         Width           =   555
      End
      Begin VB.Label RotLineCnt 
         Alignment       =   1  'Right Justify
         BackColor       =   &H0000FFFF&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   1
         Left            =   750
         TabIndex        =   132
         Top             =   135
         Width           =   585
      End
      Begin VB.Label RotLineCnt 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   0
         Left            =   60
         TabIndex        =   120
         Top             =   135
         Width           =   675
      End
   End
   Begin VB.Frame Splitter 
      Height          =   465
      Index           =   0
      Left            =   30
      TabIndex        =   104
      Top             =   6960
      Width           =   7995
      Begin VB.PictureBox SplitAnchor 
         AutoRedraw      =   -1  'True
         Height          =   285
         Index           =   0
         Left            =   7770
         MousePointer    =   7  'Size N S
         ScaleHeight     =   225
         ScaleWidth      =   105
         TabIndex        =   141
         Tag             =   "-1"
         Top             =   135
         Width           =   165
      End
      Begin VB.CheckBox chkStrSplitter 
         Caption         =   " "
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   8
         Left            =   3420
         Style           =   1  'Graphical
         TabIndex        =   131
         Top             =   135
         Width           =   675
      End
      Begin VB.CheckBox chkStrSplitter 
         Caption         =   " "
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   7
         Left            =   2730
         Style           =   1  'Graphical
         TabIndex        =   130
         Top             =   135
         Width           =   675
      End
      Begin VB.CheckBox chkStrSplitter 
         Caption         =   "Days"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   1
         Left            =   7050
         Style           =   1  'Graphical
         TabIndex        =   129
         Top             =   135
         Width           =   705
      End
      Begin VB.CheckBox chkStrSplitter 
         Caption         =   "Undo"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   6
         Left            =   4110
         Style           =   1  'Graphical
         TabIndex        =   128
         Top             =   135
         Width           =   645
      End
      Begin VB.CheckBox chkStrSplitter 
         Caption         =   "Cursor"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   5
         Left            =   6300
         Style           =   1  'Graphical
         TabIndex        =   127
         Top             =   135
         Width           =   735
      End
      Begin VB.CheckBox chkStrSplitter 
         Caption         =   "R"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   4
         Left            =   5970
         Style           =   1  'Graphical
         TabIndex        =   126
         Top             =   135
         Width           =   315
      End
      Begin VB.CheckBox chkStrSplitter 
         Caption         =   "S"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   3
         Left            =   4770
         Style           =   1  'Graphical
         TabIndex        =   125
         Top             =   135
         Width           =   315
      End
      Begin VB.CheckBox chkStrSplitter 
         Caption         =   "Manual"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   2
         Left            =   5100
         Style           =   1  'Graphical
         TabIndex        =   124
         Top             =   135
         Width           =   855
      End
      Begin VB.CheckBox chkStrSplitter 
         Caption         =   " "
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   0
         Left            =   2040
         Style           =   1  'Graphical
         TabIndex        =   105
         Top             =   135
         Width           =   675
      End
      Begin VB.Label RulLineCnt 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00C0C0FF&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   2
         Left            =   1380
         TabIndex        =   118
         Top             =   135
         Width           =   645
      End
      Begin VB.Label RulLineCnt 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00FFC0C0&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   1
         Left            =   720
         TabIndex        =   117
         Top             =   135
         Width           =   645
      End
      Begin VB.Label RulLineCnt 
         Alignment       =   1  'Right Justify
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   0
         Left            =   60
         TabIndex        =   116
         Top             =   135
         Width           =   645
      End
   End
End
Attribute VB_Name = "StandardRotations"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public NoEcho As Boolean
Dim CreateIsBusy As Boolean
Dim RulesFromFile As Boolean
Dim CurRotaFile As String
Public CurRotaUrno As Long

Dim DropMarkerIsSet As Boolean
Dim DragIndex As Integer
Dim DragLine As Long
Dim DropIndex As Integer
Dim DropLine As Long
Dim StrTabLoaded As Boolean

Dim CountArr(0 To 3) As Long
Dim CountDep(0 To 3) As Long
Dim CountRul(0 To 3) As Long
Dim CountRot(0 To 3) As Long

Dim StrTabNxtUrno As Long
Dim StrTabCntUrno As Integer
Dim WeeksPerMonth As Integer
Dim FirstRangeDate As String
Dim LastRangeDate As String
Dim LastArrDayIndex As Integer
Dim LastArrDayColor As Long
Dim LastArrDispColor As Long
Dim LastDepDayColor As Long
Dim LastDepDispColor As Long

Dim LastAutoDelete As Long
Dim LastAutoInsert As Long
Dim IsInplaceEdit As Boolean
Dim CurEditLine As Long
Dim CurEditColNo As Long
Dim CurDelCount As Long
Dim CurInsCount As Long
Dim CurUpdCount As Long
Dim CurTtlCount As Long

Dim StrTabFina As String
Dim StrTabFele As String
Dim StrTabType As String
Dim StrTabFity As String
Dim StrTabList As String

Dim StrVpfrCol As Long
Dim StrVptoCol As Long
Dim StrFredCol As Long
Dim StrFrqwCol As Long
Dim StrFlcaCol As Long
Dim StrFlnaCol As Long
Dim StrFlsaCol As Long
Dim StrAct3Col As Long
Dim StrAcgnCol As Long
Dim StrFlcdCol As Long
Dim StrFlndCol As Long
Dim StrFlsdCol As Long
Dim StrDaofCol As Long
Dim StrWkstCol As Long
Dim StrStatCol As Long
Dim StrRemaCol As Long
Dim StrGrnuCol As Long
Dim StrFca3Col As Long
Dim StrFcd3Col As Long
Public StrUrnoCol As Long
Dim StrFkyaCol As Long
Dim StrFkydCol As Long
Dim StrRulaCol As Long
Dim StrRuldCol As Long
Dim StrRulrCol As Long

Private Sub ActGrpName_Change(Index As Integer)
    chkGroup(3).Enabled = True
End Sub

Private Sub ArrDisp_MouseMove(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
    AdjustTimeScaleColors Index, ArrDisp(Index)
End Sub

Private Sub ArrLineCnt_Click(Index As Integer)
    Dim CurColor As Long
    Select Case Index
        Case 1
            CurColor = vbYellow
        Case 2
            CurColor = vbWhite
        Case 3
            CurColor = vbRed
        Case Else
        CurColor = -1
    End Select
    If CurColor > 0 Then JumpToColoredLine Flights(0), CurColor
End Sub

Private Sub CalendarFrame_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    AdjustTimeScaleColors -1, ArrDisp(0)
End Sub

Private Sub CalendarPanel_MouseMove(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
    AdjustTimeScaleColors -1, ArrDisp(0)
End Sub

Private Sub chkAlc3_Click(Index As Integer)
    If chkAlc3(Index).Value = 1 Then
        chkAlc3(Index).BackColor = LightGreen
        If Index = 0 Then chkAlc3(1).Value = 0 Else chkAlc3(0).Value = 0
        ReorganizeAlc3Filter
    Else
        chkAlc3(Index).BackColor = vbButtonFace
        If Index = 0 Then chkAlc3(1).Value = 1 Else chkAlc3(0).Value = 1
    End If
End Sub
Private Sub ReorganizeAlc3Filter()
    Dim CurLine As Long
    Dim MaxLine As Long
    Dim Col1 As Long
    Dim Col2 As Long
    Dim ColTxt As String
    Dim i As Integer
    Screen.MousePointer = 11
    If chkAlc3(0).Value = 1 Then
        Col1 = 2
        Col2 = 1
    Else
        Col1 = 1
        Col2 = 2
    End If
    For i = 0 To 1
        MaxLine = FilterTab(i).GetLineCount - 1
        For CurLine = 0 To MaxLine
            ColTxt = Trim(FilterTab(i).GetColumnValue(CurLine, Col1))
            If ColTxt = "" Then ColTxt = FilterTab(i).GetColumnValue(CurLine, Col2)
            FilterTab(i).SetColumnValue CurLine, 0, ColTxt
        Next
        FilterTab(i).Sort 0, True, True
        FilterTab(i).Refresh
    Next
    Screen.MousePointer = 0
End Sub


Private Sub chkFilter_Click(Index As Integer)
    Dim NxtIdx As Integer
    NxtIdx = Index + 1
    If chkFilter(Index).Value = 1 Then
        FilterPanel(Index).Enabled = True
        chkFilterSet(Index).Enabled = True
        chkFilterSet(NxtIdx).Enabled = True
        txtFilterVal(Index).Enabled = True
        txtFilterVal(Index).BackColor = vbWhite
        chkFilter(Index).BackColor = LightGreen
    Else
        FilterPanel(Index).Enabled = False
        chkFilterSet(Index).Enabled = False
        chkFilterSet(NxtIdx).Enabled = False
        txtFilterVal(Index).Enabled = False
        txtFilterVal(Index).BackColor = LightGray
        chkFilter(Index).BackColor = vbButtonFace
    End If
End Sub

Private Sub chkFilterSet_Click(Index As Integer)
    Dim CurLine As Long
    Dim MaxLine As Long
    Dim LineNo As Long
    Dim CurForeColor As Long
    Dim CurBackColor As Long
    Dim NewForeColor As Long
    Dim NewBackColor As Long
    Dim NxtIdx As Integer
    Dim tmpBuff As String
    If chkFilterSet(Index).Value = 1 Then
        If Index Mod 2 = 0 Then NxtIdx = Index + 1 Else NxtIdx = Index - 1
        If Index < 6 Then
            tmpBuff = FilterTab(Index).GetBuffer(0, FilterTab(Index).GetLineCount - 1, vbLf)
            FilterTab(NxtIdx).InsertBuffer tmpBuff, vbLf
        Else
            MaxLine = FilterTab(Index).GetLineCount - 1
            LineNo = FilterTab(NxtIdx).GetLineCount
            For CurLine = 0 To MaxLine
                tmpBuff = FilterTab(Index).GetLineValues(CurLine)
                FilterTab(Index).GetLineColor CurLine, CurForeColor, CurBackColor
                FilterTab(NxtIdx).InsertTextLine tmpBuff, False
                Select Case CurBackColor
                    Case vbBlue
                        NewForeColor = vbWhite
                        NewBackColor = vbRed
                        chkFilterSet(6).Tag = chkFilterSet(6).Tag + 1
                    Case vbYellow
                        NewForeColor = vbBlack
                        NewBackColor = vbGreen
                        chkFilterSet(7).Tag = chkFilterSet(7).Tag + 1
                    Case vbRed
                        NewForeColor = vbWhite
                        NewBackColor = vbBlue
                        chkFilterSet(6).Tag = chkFilterSet(6).Tag - 1
                    Case vbGreen
                        NewForeColor = vbBlack
                        NewBackColor = vbYellow
                        chkFilterSet(7).Tag = chkFilterSet(7).Tag - 1
                    Case Else
                End Select
                FilterTab(NxtIdx).SetLineColor LineNo, NewForeColor, NewBackColor
                LineNo = LineNo + 1
            Next
            CheckGrpSaveButton
        End If
        FilterTab(NxtIdx).Sort 0, True, True
        FilterTab(Index).ResetContent
        FilterTab(Index).Refresh
        FilterTab(NxtIdx).Refresh
        chkFilterSet(Index).Caption = "0"
        chkFilterSet(NxtIdx).Caption = Trim(Str(FilterTab(NxtIdx).GetLineCount))
        chkFilterSet(Index).Value = 0
        CheckFilterCounter
    End If
End Sub
Private Sub CheckFilterCounter()
    Dim CurTtl As Long
    CurTtl = 0
    CurTtl = CurTtl + Val(chkFilterSet(1).Caption)
    CurTtl = CurTtl + Val(chkFilterSet(3).Caption)
    CurTtl = CurTtl + Val(chkFilterSet(5).Caption)
    If CurTtl > 0 Then
        chkSwitch(0).ForeColor = vbBlue
        chkWork(0).ForeColor = vbBlue
    Else
        chkSwitch(0).ForeColor = vbBlack
        chkWork(0).ForeColor = vbBlack
    End If
End Sub
Private Sub chkGroup_Click(Index As Integer)
    If chkGroup(Index).Value = 1 Then chkGroup(Index).BackColor = LightGreen Else chkGroup(Index).BackColor = vbButtonFace
    Select Case Index
        Case 0  'New
            If chkGroup(Index).Value = 1 Then
                chkGroup(1).Value = 0
                AdjustActGroup False
                chkFilterSet(6).Enabled = True
                chkFilterSet(7).Enabled = True
            Else
                If GrnUrno.Text <> "" Then AdjustActGroup True
                chkFilterSet(6).Enabled = False
                chkFilterSet(7).Enabled = False
            End If
        Case 1  'Update
            If chkGroup(Index).Value = 1 Then
                If chkGroup(0).Value = 1 Then
                    chkGroup(0).Value = 0
                Else
                    If GrnUrno.Text <> "" Then AdjustActGroup True
                End If
                chkFilterSet(6).Enabled = True
                chkFilterSet(7).Enabled = True
            Else
                chkGroup(3).Enabled = False
                chkFilterSet(6).Enabled = False
                chkFilterSet(7).Enabled = False
            End If
        Case 3  'Save
            If chkGroup(Index).Value = 1 Then
                SaveGroupData
                chkGroup(Index).Value = 0
            End If
        Case Else
    End Select
End Sub
Private Sub SaveGroupData()
    Dim SaveIt As Boolean
    Dim MsgTxt As String
    Dim RetVal As Integer
    Dim CedaAction As String
    Dim GrnTabUrno As String
    Dim NewUrno As String
    Dim GrnTabFields As String
    Dim GrmTabFields As String
    Dim NewRec As String
    Dim SqlKey As String
    Dim LineNo As Long
    Dim LineList As String
    Dim LineItem As String
    Dim ItmNbr As Long
    Dim NewMembers As Long
    SaveIt = True
    MsgTxt = ""
    If Trim(ActGrpName(1).Text) = "" Then
        MsgTxt = MsgTxt & "Group Short Name missing." & vbNewLine
        SaveIt = False
    End If
    If Len(Trim(ActGrpName(1).Text)) > 3 Then
        MsgTxt = MsgTxt & "Group Short Name too long (max. 3)" & vbNewLine
        SaveIt = False
    End If
    If Trim(ActGrpName(0).Text) = "" Then
        MsgTxt = MsgTxt & "Group Name missing." & vbNewLine
        SaveIt = False
    End If
    If Len(Trim(ActGrpName(0).Text)) > 12 Then
        MsgTxt = MsgTxt & "Group Name too long (max. 12)" & vbNewLine
        SaveIt = False
    End If
    NewMembers = FilterTab(7).GetLineCount
    If NewMembers < 1 Then
        MsgTxt = MsgTxt & "No members selected." & vbNewLine
        SaveIt = False
    End If
    If NewMembers = 1 Then
        MsgTxt = MsgTxt & "One single member doesn't make sense." & vbNewLine
        SaveIt = False
    End If
    If SaveIt Then
        Screen.MousePointer = 11
        CedaAction = ""
        If chkGroup(0).Value = 1 Then
            'New Group
            CedaAction = "IRT"
            GrnTabFields = "URNO,APPL,TABN,GRSN,GRPN,FLDN,GRDS,PRFL,CDAT,USEC,LSTU,USEU"
            GrnTabUrno = UfisServer.UrnoPoolGetNext
            GrnUrno.Text = GrnTabUrno
            NewRec = GrnTabUrno & ","
            NewRec = NewRec & "DIET,ACTTAB,"
            NewRec = NewRec & Trim(ActGrpName(1).Text) & ","
            NewRec = NewRec & Trim(ActGrpName(0).Text) & ", , , , , , , "
        Else
            'Update
            CedaAction = "URT"
            GrnTabUrno = GrnUrno.Text
            GrnTabFields = "GRSN,GRPN,LSTU,USEU"
            NewRec = NewRec & Trim(ActGrpName(1).Text) & ","
            NewRec = NewRec & Trim(ActGrpName(0).Text) & ", , "
            SqlKey = "WHERE URNO=" & GrnTabUrno
        End If
        UfisServer.CallCeda MsgTxt, CedaAction, "GRNTAB", GrnTabFields, NewRec, SqlKey, "", 0, False, False
        'Check for GroupMembers to delete
        LineList = FilterTab(6).GetLinesByBackColor(vbRed)
        If LineList <> "" Then
            ItmNbr = 0
            LineItem = GetRealItem(LineList, ItmNbr, ",")
            While LineItem <> ""
                LineNo = Val(LineItem)
                SqlKey = "WHERE URNO=" & FilterTab(6).GetColumnValue(LineNo, 3)
                UfisServer.CallCeda MsgTxt, "DRT", "GRMTAB", "", "", SqlKey, "", 0, False, False
                ItmNbr = ItmNbr + 1
                LineItem = GetRealItem(LineList, ItmNbr, ",")
            Wend
        End If
        'Check For GroupMembers to insert
        LineList = FilterTab(7).GetLinesByBackColor(vbGreen)
        If LineList <> "" Then
            NewMembers = ItemCount(LineList, ",")
            UfisServer.UrnoPoolPrepare NewMembers
            GrmTabFields = "URNO,GURN,VALU,CDAT,USEC,LSTU,USEU,PRFL"
            ItmNbr = 0
            LineItem = GetRealItem(LineList, ItmNbr, ",")
            While LineItem <> ""
                LineNo = Val(LineItem)
                NewRec = UfisServer.UrnoPoolGetNext & ","
                NewRec = NewRec & GrnTabUrno & ","
                NewRec = NewRec & FilterTab(7).GetColumnValue(LineNo, 1) & ", , , , , "
                UfisServer.CallCeda MsgTxt, "IRT", "GRMTAB", GrmTabFields, NewRec, "", "", 0, False, False
                ItmNbr = ItmNbr + 1
                LineItem = GetRealItem(LineList, ItmNbr, ",")
            Wend
        End If
        Screen.MousePointer = 0
        LoadGroupData
        chkGroup(0).Value = 0
        chkGroup(1).Value = 0
        chkGroupMaster(6).Value = 0
    End If
    If Not SaveIt Then
        RetVal = MyMsgBox.CallAskUser(0, 0, 0, "Transaction Control", MsgTxt, "stop", "", UserAnswer)
    End If
End Sub
Private Sub chkGroupMaster_Click(Index As Integer)
    Dim NxtIdx As Integer
    If Index = 6 Then
        NxtIdx = Index + 1
        If chkGroupMaster(Index).Value = 1 Then
            FilterPanel(Index).Enabled = True
            txtFilterVal(Index).Enabled = True
            txtFilterVal(Index).BackColor = vbWhite
            ActGrpName(0).Enabled = True
            ActGrpName(0).BackColor = vbYellow
            ActGrpName(1).Enabled = True
            ActGrpName(1).BackColor = vbYellow
            For NxtIdx = 0 To chkGroup.Count - 1
                chkGroup(NxtIdx).Enabled = True
            Next
            If GrnUrno.Text <> "" Then AdjustActGroup True
            chkGroupMaster(Index).BackColor = LightGreen
        Else
            FilterPanel(Index).Enabled = False
            chkFilterSet(Index).Enabled = False
            chkFilterSet(NxtIdx).Enabled = False
            txtFilterVal(Index).Enabled = False
            txtFilterVal(Index).BackColor = LightGray
            ActGrpName(0).Enabled = False
            ActGrpName(0).BackColor = LightGray
            ActGrpName(1).Enabled = False
            ActGrpName(1).BackColor = LightGray
            For NxtIdx = 0 To chkGroup.Count - 1
                chkGroup(NxtIdx).Enabled = False
            Next
            FilterTab(6).ResetContent
            FilterTab(6).Refresh
            chkFilterSet(6).Caption = "0"
            chkGroupMaster(Index).BackColor = vbButtonFace
        End If
    End If
End Sub
Private Sub AdjustActGroup(TotalReset As Boolean)
    Dim CurLine As Long
    Dim MaxLine As Long
    Dim LineList As String
    Dim LineItem As String
    Dim LineNo As Long
    Dim ItmNbr As Long
    Dim CurGrnUrno As String
    Dim CurActUrno As String
    Dim ActActUrno As String
    FilterTab(7).ResetContent
    If TotalReset Then FilterTab(7).InsertBuffer GetLineValuesByColumnValue(5, 2, GrnUrno.Text), vbLf
    FilterTab(6).ResetContent
    MaxLine = UfisServer.BasicData(5).GetLineCount - 1
    LineList = UfisServer.BasicData(5).GetBuffer(0, MaxLine, vbLf)
    FilterTab(6).InsertBuffer LineList, vbLf
    FilterTab(6).Sort 2, True, True
    MaxLine = FilterTab(7).GetLineCount - 1
    For CurLine = 0 To MaxLine
        CurGrnUrno = FilterTab(7).GetColumnValue(CurLine, 2)
        LineList = FilterTab(6).GetLinesByColumnValue(2, CurGrnUrno, 0)
        ItmNbr = 0
        LineItem = GetRealItem(LineList, ItmNbr, ",")
        While LineItem <> ""
            LineNo = Val(LineItem)
            FilterTab(6).SetLineStatusValue LineNo, 1
            ItmNbr = ItmNbr + 1
            LineItem = GetRealItem(LineList, ItmNbr, ",")
        Wend
        FilterTab(7).SetLineColor CurLine, vbWhite, vbBlue
    Next
    FilterTab(6).Sort 1, True, True
    MaxLine = FilterTab(7).GetLineCount - 1
    For CurLine = 0 To MaxLine
        CurActUrno = FilterTab(7).GetColumnValue(CurLine, 1)
        LineList = FilterTab(6).GetLinesByColumnValue(1, CurActUrno, 0)
        ItmNbr = 0
        LineItem = GetRealItem(LineList, ItmNbr, ",")
        While LineItem <> ""
            LineNo = Val(LineItem)
            FilterTab(6).SetLineStatusValue LineNo, 1
            ItmNbr = ItmNbr + 1
            LineItem = GetRealItem(LineList, ItmNbr, ",")
        Wend
    Next
    FilterTab(6).Sort 1, True, True
    MaxLine = FilterTab(6).GetLineCount - 1
    CurLine = 0
    While CurLine <= MaxLine
        CurActUrno = FilterTab(6).GetColumnValue(CurLine, 1)
        If (CurActUrno = ActActUrno) Or (FilterTab(6).GetLineStatusValue(CurLine) = 1) Then
            FilterTab(6).DeleteLine CurLine
            MaxLine = MaxLine - 1
            CurLine = CurLine - 1
        Else
            FilterTab(6).SetLineColor CurLine, vbBlack, vbYellow
            FilterTab(6).SetColumnValue CurLine, 2, "0"
            FilterTab(6).SetColumnValue CurLine, 3, "0"
            ActActUrno = CurActUrno
        End If
        CurLine = CurLine + 1
    Wend
    FilterTab(6).Sort 0, True, True
    FilterTab(7).Sort 0, True, True
    FilterTab(6).Refresh
    FilterTab(7).Refresh
    chkFilterSet(6).Tag = 0
    chkFilterSet(7).Tag = 0
    chkFilterSet(6).Caption = Trim(Str(FilterTab(6).GetLineCount))
    chkFilterSet(7).Caption = Trim(Str(FilterTab(7).GetLineCount))
    CheckGrpSaveButton
End Sub
Private Sub CheckGrpSaveButton()
    If chkFilterSet(6).Tag + chkFilterSet(7).Tag > 0 Then
        chkGroup(3).Enabled = True
    Else
        chkGroup(3).Enabled = False
    End If
End Sub
Public Function GetLineValuesByColumnValue(Index As Integer, ColNo As Integer, ColValue As String) As String
    Dim Result As String
    Dim LineList As String
    Dim LineItem As String
    Dim LineNo As Long
    Dim ItmNbr As Long
    ItmNbr = 0
    Result = ""
    LineList = UfisServer.BasicData(Index).GetLinesByColumnValue(ColNo, ColValue, 0)
    LineItem = GetRealItem(LineList, ItmNbr, ",")
    While LineItem <> ""
        LineNo = Val(LineItem)
        Result = Result & UfisServer.BasicData(Index).GetLineValues(LineNo) & vbLf
        ItmNbr = ItmNbr + 1
        LineItem = GetRealItem(LineList, ItmNbr, ",")
    Wend
    GetLineValuesByColumnValue = Result
End Function
Private Sub chkLoad_Click(Index As Integer)
    Dim i As Integer
    If chkLoad(Index).Value = 1 Then
        chkLoad(Index).BackColor = LightGreen
        chkWork(3).Enabled = False
        chkWork(4).Enabled = False
        chkWork(5).Enabled = False
        StrTabData(0).SetBoolPropertyReadOnly 13, False
        For i = 0 To chkLoad.Count - 1
            If i <> Index Then chkLoad(i) = 0
        Next
        Select Case Index
            Case 0
                'AFT
            Case 1
                'SSI
            Case 2
                'File
                LinkFile.Show , Me
            Case Else
        End Select
    Else
        Select Case Index
            Case 0
                'AFT
            Case 1
                'SSI
            Case 2
                'File
                LinkFile.Hide
                Me.Refresh
            Case Else
        End Select
        chkWork(3).Enabled = True
        chkWork(4).Enabled = True
        chkWork(5).Enabled = True
        StrTabData(0).SetBoolPropertyReadOnly 13, True
        chkLoad(Index).BackColor = vbButtonFace
    End If
End Sub

Private Sub ReadRotationFile()
    Dim CurLine As Long
    Dim MaxLine As Long
    Dim StrTabRec As String
    Dim tmpAlc3Arr As String
    Dim tmpFltnArr As String
    Dim tmpFlnsArr As String
    Dim tmpFkeyArr As String
    Dim tmpAlc3Dep As String
    Dim tmpFltnDep As String
    Dim tmpFlnsDep As String
    Dim tmpFkeyDep As String
    Dim tmpStoa As String
    Dim tmpStod As String
    Dim tmpFreq As String
    Dim tmpDaof As String
    Dim tmpSked As String
    Dim tmpSsimVpfr As String
    Dim tmpSsimVpto As String
    Dim tmpCedaVpfr As String
    Dim tmpCedaVpto As String
    Dim CurUrno As Long
    Dim varStoa
    Dim varStod
    
    RulesFromFile = True
    StrTabData(0).ResetContent
    StrTabData(0).Refresh
    LinkFile.chkUtc.Value = 1
    If CurRotaFile <> LinkFile.txtCurFile.Text Then CurRotaUrno = CurRotaUrno + 1000000
    CurRotaFile = LinkFile.txtCurFile.Text
    tmpCedaVpfr = txtVpfr(0).Tag
    tmpCedaVpto = txtVpto(0).Tag
    tmpSsimVpfr = DecodeSsimDayFormat(tmpCedaVpfr, "CEDA", "SSIM2")
    tmpSsimVpto = DecodeSsimDayFormat(tmpCedaVpto, "CEDA", "SSIM2")
    MaxLine = LinkFile.FileData.GetLineCount - 1
    For CurLine = 0 To MaxLine
        If LinkFile.FileData.GetLineStatusValue(CurLine) = 0 Then
            tmpAlc3Arr = LinkFile.FileData.GetColumnValue(CurLine, 0)
            tmpFltnArr = LinkFile.FileData.GetColumnValue(CurLine, 6)
            tmpFlnsArr = ""
            tmpAlc3Dep = LinkFile.FileData.GetColumnValue(CurLine, 0)
            tmpFltnDep = LinkFile.FileData.GetColumnValue(CurLine, 7)
            tmpFlnsDep = ""
            tmpAlc3Arr = Alc2ToAlc3LookUp(tmpAlc3Arr)
            tmpAlc3Dep = Alc2ToAlc3LookUp(tmpAlc3Dep)
            tmpStoa = LinkFile.FileData.GetColumnValue(CurLine, 4)
            tmpStod = LinkFile.FileData.GetColumnValue(CurLine, 5)
            CurUrno = CurLine + CurRotaUrno
            tmpFreq = FormatFrequency(LinkFile.FileData.GetColumnValue(CurLine, 9))
            tmpStoa = tmpCedaVpfr & tmpStoa
            tmpStod = tmpCedaVpfr & tmpStod
            varStoa = CedaFullDateToVb(tmpStoa)
            varStod = CedaFullDateToVb(tmpStod)
            If varStod < varStoa Then varStod = DateAdd("d", 1, varStod)
            tmpDaof = CStr(DateDiff("d", varStoa, varStod))
            If tmpDaof = "0" Then
                tmpDaof = ""
            Else
                tmpDaof = "+" & tmpDaof
            End If
            tmpFkeyArr = Left(TurnAftFkey(BuildAftFkey(tmpAlc3Arr, tmpFltnArr, tmpFlnsArr, "", "")), 9)
            tmpFkeyDep = Left(TurnAftFkey(BuildAftFkey(tmpAlc3Dep, tmpFltnDep, tmpFlnsDep, "", "")), 9)
            StrTabRec = ""
            StrTabRec = StrTabRec & tmpSsimVpfr & ","
            StrTabRec = StrTabRec & tmpSsimVpto & ","
            StrTabRec = StrTabRec & tmpFreq & ","
            StrTabRec = StrTabRec & ","
            StrTabRec = StrTabRec & tmpAlc3Arr & ","
            StrTabRec = StrTabRec & tmpFltnArr & ","
            StrTabRec = StrTabRec & tmpFlnsArr & ","
            StrTabRec = StrTabRec & LinkFile.FileData.GetColumnValue(CurLine, 8) & ","  'Act3
            StrTabRec = StrTabRec & ","
            StrTabRec = StrTabRec & tmpAlc3Dep & ","
            StrTabRec = StrTabRec & tmpFltnDep & ","
            StrTabRec = StrTabRec & tmpFlnsDep & ","
            StrTabRec = StrTabRec & tmpDaof & ","
            StrTabRec = StrTabRec & "0,0,,"
            StrTabRec = StrTabRec & tmpAlc3Arr & ","
            StrTabRec = StrTabRec & tmpAlc3Dep & ","
            StrTabRec = StrTabRec & "0,"
            StrTabRec = StrTabRec & CStr(CurUrno) & ","
            StrTabRec = StrTabRec & tmpFkeyArr & ","
            StrTabRec = StrTabRec & tmpFkeyDep & ","
            StrTabRec = StrTabRec & "-1,-1,-1"
            StrTabData(0).InsertTextLine StrTabRec, False
        End If
    Next
    StrTabData(0).Refresh
End Sub

Private Sub chkStrSplitter_Click(Index As Integer)
    Dim MaxLine As Long
    Dim NewRec As String
    Select Case Index
        Case 1  'Days
            If chkStrSplitter(Index).Value = 1 Then
                chkStrSplitter(Index).BackColor = LightGreen
                AdjustLineMarkers StrTabData(0), 1, StrTabData(0).GetCurrentSelected, True
            Else
                ResetRangeInfoObjects False, ArrDays, DepDays, True, True
                ResetRangeInfoObjects False, ArrDisp, DepDisp, True, True
                chkStrSplitter(Index).BackColor = vbButtonFace
            End If
        Case 2  'Manual (DragDrop)
            If chkStrSplitter(Index).Value = 1 Then
                If chkStrSplitter(6).Value = 0 Then
                    If chkStrSplitter(3).Value = 0 Then
                        chkStrSplitter(4).Value = 1
                        DropIndex = 1
                    Else
                        DropIndex = 0
                    End If
                    MaxLine = StrTabData(DropIndex).GetLineCount
                    NewRec = ",,,,,,,,,,,,,0,0,,,,0,0,,,-1,-1,-1"
                    StrTabData(DropIndex).InsertTextLine NewRec, False
                    StrTabData(DropIndex).SetLineColor MaxLine, vbBlack, vbCyan
                    StrTabData(DropIndex).SetLineStatusValue MaxLine, 9
                    StrTabData(DropIndex).OnVScrollTo MaxLine
                    'StrTabData(DropIndex).Refresh
                    DropLine = MaxLine
                    Flights(0).ShowRowSelection = False
                    Flights(1).ShowRowSelection = False
                Else
                    Flights(0).ShowRowSelection = True
                    Flights(1).ShowRowSelection = True
                End If
                chkStrSplitter(5).Value = 0
                chkStrSplitter(5).Enabled = False
                chkStrSplitter(1).Value = 0
                chkStrSplitter(1).Enabled = False
                chkStrSplitter(3).Enabled = False
                chkStrSplitter(4).Enabled = False
            Else
                If DropIndex >= 0 Then
                    MaxLine = StrTabData(DropIndex).GetLineCount - 1
                    StrTabData(DropIndex).DeleteLine MaxLine
                    StrTabData(DropIndex).Refresh
                End If
                chkStrSplitter(3).Value = 0
                chkStrSplitter(4).Value = 0
                chkStrSplitter(6).Value = 0
                chkStrSplitter(1).Enabled = True
                'chkStrSplitter(3).Enabled = True
                chkStrSplitter(4).Enabled = True
                chkStrSplitter(5).Enabled = True
                chkStrSplitter(Index).BackColor = vbButtonFace
                DropIndex = -1
                DropLine = -1
                DropMarkerIsSet = False
                If (DragIndex >= 0) And (DragLine >= 0) Then AdjustDropMarker Flights(DragIndex), DragLine, 1, 5, "", False, False
                Flights(0).ShowRowSelection = True
                Flights(1).ShowRowSelection = True
            End If
            DragIndex = -1
            DragLine = -1
        Case 3  '(S)
            If chkStrSplitter(Index).Value = 1 Then
                chkStrSplitter(2).Value = 1
                chkStrSplitter(4).Value = 0
                chkStrSplitter(Index).BackColor = LightGreen
            Else
                If chkStrSplitter(4).Value = 0 Then chkStrSplitter(2).Value = 0
                chkStrSplitter(Index).BackColor = vbButtonFace
            End If
        Case 4  '(R)
            If chkStrSplitter(Index).Value = 1 Then
                chkStrSplitter(2).Value = 1
                chkStrSplitter(3).Value = 0
                chkStrSplitter(Index).BackColor = LightGreen
            Else
                If chkStrSplitter(3).Value = 0 Then chkStrSplitter(2).Value = 0
                chkStrSplitter(Index).BackColor = vbButtonFace
            End If
        Case 5 'Cursor
            If chkStrSplitter(Index).Value = 1 Then
                chkStrSplitter(Index).BackColor = LightGreen
                chkWork(3).Value = 0
                chkWork(4).Value = 0
                AdjustLineMarkers StrTabData(0), 1, StrTabData(0).GetCurrentSelected, True
            Else
                chkStrSplitter(Index).BackColor = vbButtonFace
                ResetLineMarkers StrTabData(0)
                ResetLineMarkers StrTabData(1)
                ResetLineMarkers Flights(0)
                ResetLineMarkers Flights(1)
            End If
        Case 6  'Undo
            If chkStrSplitter(Index).Value = 1 Then
                If (DragIndex >= 0) And (DragLine >= 0) Then AdjustDropMarker Flights(DragIndex), DragLine, 1, 5, "", False, False
                If DropIndex >= 0 Then
                    MaxLine = StrTabData(DropIndex).GetLineCount - 1
                    StrTabData(DropIndex).DeleteLine MaxLine
                    StrTabData(DropIndex).Refresh
                End If
                chkStrSplitter(2).Value = 1
                DropIndex = -1
                DropLine = -1
                DragIndex = -1
                DragLine = -1
                chkStrSplitter(Index).BackColor = LightGreen
                chkStrSplitter(2).BackColor = vbRed
            Else
                chkStrSplitter(Index).BackColor = vbButtonFace
                chkStrSplitter(2).Value = 0
            End If
            Flights(0).ShowRowSelection = True
            Flights(1).ShowRowSelection = True
        Case Else
    End Select
End Sub

Private Sub chkSwitch_Click(Index As Integer)
    Dim NewStyle As Integer
    Select Case Index
        Case 0  'Switch
            If chkSwitch(Index).Value = 1 Then
                NewStyle = -1
                Select Case ShowStrTabStyle
                    Case 0
                        NewStyle = 1
                    Case 1
                        NewStyle = 0
                    Case Else
                End Select
                ShowStrTabStyle = NewStyle
                InitForm False
                Form_Resize
                Me.Refresh
                chkSwitch(Index).Value = 0
            End If
        Case Else
    End Select
End Sub

Private Sub DepDays_MouseMove(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
    AdjustTimeScaleColors Index, DepDays(Index)
End Sub

Private Sub DepDisp_MouseMove(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
    AdjustTimeScaleColors Index, DepDisp(Index)
End Sub

Private Sub DepLineCnt_Click(Index As Integer)
    Dim CurColor As Long
    Select Case Index
        Case 1
            CurColor = vbGreen
        Case 2
            CurColor = vbWhite
        Case 3
            CurColor = vbRed
        Case Else
            CurColor = -1
    End Select
    If CurColor > 0 Then JumpToColoredLine Flights(1), CurColor
End Sub

Private Sub FilterTab_RowSelectionChanged(Index As Integer, ByVal LineNo As Long, ByVal Selected As Boolean)
    Select Case Index
        Case 4, 5
            If LineNo >= 0 Then
                If Selected Then
                    chkGroupMaster(6).Value = 0
                    ActGrpName(0) = FilterTab(Index).GetColumnValue(LineNo, 0)
                    ActGrpName(1) = FilterTab(Index).GetColumnValue(LineNo, 1)
                    GrnUrno.Text = FilterTab(Index).GetColumnValue(LineNo, 2)
                    FilterTab(7).ResetContent
                    FilterTab(7).InsertBuffer GetLineValuesByColumnValue(5, 2, GrnUrno.Text), vbLf
                    FilterTab(7).Sort 0, True, True
                    FilterTab(7).Refresh
                    chkFilterSet(7).Caption = Trim(Str(FilterTab(7).GetLineCount))
                End If
            Else
                ActGrpName(0).Text = ""
                ActGrpName(1).Text = ""
                GrnUrno.Text = ""
            End If
        Case Else
    End Select
End Sub

Private Sub FilterTab_SendLButtonDblClick(Index As Integer, ByVal LineNo As Long, ByVal ColNo As Long)
    Dim NxtLineNo As Long
    Dim CurForeColor As Long
    Dim CurBackColor As Long
    Dim NewForeColor As Long
    Dim NewBackColor As Long
    Dim NxtIdx As Integer
    Dim tmpBuff As String
    If LineNo >= 0 Then
        If (Index < 6) Or (chkGroup(0).Value = 1) Or (chkGroup(1).Value = 1) Then
            If Index Mod 2 = 0 Then NxtIdx = Index + 1 Else NxtIdx = Index - 1
            tmpBuff = FilterTab(Index).GetLineValues(LineNo)
            FilterTab(Index).GetLineColor LineNo, CurForeColor, CurBackColor
            FilterTab(Index).DeleteLine LineNo
            FilterTab(Index).Refresh
            chkFilterSet(Index).Caption = Trim(Str(FilterTab(Index).GetLineCount))
            NxtLineNo = FilterTab(NxtIdx).GetLineCount
            FilterTab(NxtIdx).InsertTextLine tmpBuff, False
            If (NxtIdx = 6) Or (NxtIdx = 7) Then
                Select Case CurBackColor
                    Case vbBlue
                        NewForeColor = vbWhite
                        NewBackColor = vbRed
                        chkFilterSet(6).Tag = chkFilterSet(6).Tag + 1
                    Case vbYellow
                        NewForeColor = vbBlack
                        NewBackColor = vbGreen
                        chkFilterSet(7).Tag = chkFilterSet(7).Tag + 1
                    Case vbRed
                        NewForeColor = vbWhite
                        NewBackColor = vbBlue
                        chkFilterSet(6).Tag = chkFilterSet(6).Tag - 1
                    Case vbGreen
                        NewForeColor = vbBlack
                        NewBackColor = vbYellow
                        chkFilterSet(7).Tag = chkFilterSet(7).Tag - 1
                    Case Else
                End Select
                FilterTab(NxtIdx).SetLineColor NxtLineNo, NewForeColor, NewBackColor
                CheckGrpSaveButton
            End If
            FilterTab(NxtIdx).Sort 0, True, True
            FilterTab(NxtIdx).Refresh
            chkFilterSet(NxtIdx).Caption = Trim(Str(FilterTab(NxtIdx).GetLineCount))
            CheckFilterCounter
        End If
    End If
End Sub

Private Sub Flights_RowSelectionChanged(Index As Integer, ByVal LineNo As Long, ByVal Selected As Boolean)
    Dim tmpStat As String
    Dim StrRec As String
    If (Not StopNestedCalls) And (Not CreateIsBusy) Then
        If (chkStrSplitter(6).Value = 0) And (chkStrSplitter(2).Value = 1) Then
            If (Selected) And (LineNo >= 0) Then
                tmpStat = Flights(Index).GetColumnValue(LineNo, 6) & Flights(Index).GetColumnValue(LineNo, 7)
                If tmpStat = "00" Then
                    If (DragIndex >= 0) And (DragLine >= 0) Then AdjustDropMarker Flights(DragIndex), DragLine, 1, 5, "", False, False
                    Flights(Index).ShowRowSelection = False
                    If Index = 0 Then
                        chkStrSplitter(2).BackColor = vbYellow
                        AdjustDropMarker Flights(0), LineNo, 1, 5, "ArrMarker", True, False
                    Else
                        chkStrSplitter(2).BackColor = LightGreen
                        AdjustDropMarker Flights(1), LineNo, 1, 5, "DepMarker", True, False
                    End If
                    DragIndex = Index
                    DragLine = LineNo
                Else
                    Flights(Index).ShowRowSelection = True
                    If (DragIndex >= 0) And (DragLine >= 0) Then AdjustDropMarker Flights(DragIndex), DragLine, 1, 5, "", False, False
                    chkStrSplitter(2).BackColor = vbButtonFace
                    DragIndex = -1
                    DragLine = -1
                End If
                If DropMarkerIsSet Then AdjustDropMarker StrTabData(DropIndex), DropLine, 4, 12, "", False, True
            End If
        Else
            If (Selected) And (LineNo >= 0) Then
                Select Case Index
                    Case 0
                        AdjustLineMarkers Flights(0), 2, LineNo, False
                    Case 1
                        AdjustLineMarkers Flights(1), 3, LineNo, False
                    Case Else
                End Select
            End If
        End If
    End If
End Sub

Private Function BuildStrFormatFromFlight(Index As Integer, LineNo As Long) As String
    Dim NewRec As String
    Dim tmpData As String
    NewRec = ""
    '"A,Flight,ACT,Day,W,Time,R,S,Remark,FKEY,RULE,ROTA,LINE"
    '"VPFR,VPTO,FRED,FRQW,FLCA,FLNA,FLSA,ACT3,ACGN,FLCD,FLND,FLSD,DAOF,STAT,WKST,REMA,FCA3,FCD3,GRNU,URNO"
    tmpData = DecodeSsimDayFormat(Flights(Index).GetColumnValue(LineNo, 3), "CEDA", "SSIM2")
    NewRec = NewRec & tmpData & ","
    NewRec = NewRec & tmpData & ",1234567,,"
    tmpData = Flights(Index).GetColumnValue(LineNo, 1)
    If Index = 0 Then
        NewRec = NewRec & Left(tmpData, 3) & ","
        NewRec = NewRec & Mid(tmpData, 4) & ",,,,,,,,,,,,,,,"
    Else
        NewRec = NewRec & ",,,,," & Left(tmpData, 3) & ","
        NewRec = NewRec & Mid(tmpData, 4) & ",,,,,,,,,,,,,,,"
    End If
    BuildStrFormatFromFlight = NewRec
End Function

Private Function BuildStrRotationFromFlights(ArrRec As String, DepRec As String) As String
    Dim NewRec As String
    Dim ArrDate
    Dim ArrVpfr As String
    Dim ArrVpto As String
    Dim ArrFlno As String
    Dim ArrFlca As String
    Dim ArrFltn As String
    Dim ArrFlns As String
    Dim ArrFred As String
    Dim ArrFrew As String
    Dim ArrAct3 As String
    Dim ArrFkey As String
    Dim DepDate
    Dim DepVpfr As String
    Dim DepVpto As String
    Dim DepFlno As String
    Dim DepFlca As String
    Dim DepFltn As String
    Dim DepFlns As String
    Dim DepFred As String
    Dim DepFrew As String
    Dim DepDaof As String
    Dim DepAct3 As String
    Dim DepFkey As String
    
    NewRec = ""
    ' 0   1     2   3  4   5  6 7   8     9    10   11   12
    '"A,Flight,ACT,Day,W,Time,R,S,Remark,FKEY,RULE,ROTA,LINE"
    '"VPFR,VPTO,FRED,FRQW,FLCA,FLNA,FLSA,ACT3,ACGN,FLCD,FLND,FLSD,DAOF,STAT,WKST,REMA,FCA3,FCD3,GRNU,URNO"
    ArrVpfr = DecodeSsimDayFormat(GetRealItem(ArrRec, 3, ","), "CEDA", "SSIM2")
    ArrVpto = ArrVpfr
    ArrFred = GetRealItem(ArrRec, 4, ",")
    ArrFrew = ""
    ArrFlno = GetRealItem(ArrRec, 1, ",")
    ArrFlca = Trim(Left(ArrFlno, 3))
    ArrFltn = Mid(ArrFlno, 4)
    ArrFlns = ""
    ArrAct3 = GetRealItem(ArrRec, 2, ",")
    ArrFkey = GetRealItem(ArrRec, 9, ",")
    DepVpfr = DecodeSsimDayFormat(GetRealItem(DepRec, 3, ","), "CEDA", "SSIM2")
    DepVpto = DepVpfr
    DepFred = GetRealItem(DepRec, 4, ",")
    DepFrew = ""
    DepFlno = GetRealItem(DepRec, 1, ",")
    DepFlca = Trim(Left(DepFlno, 3))
    DepFltn = Mid(DepFlno, 4)
    DepFlns = ""
    DepAct3 = GetRealItem(DepRec, 2, ",")
    DepFkey = GetRealItem(DepRec, 9, ",")
    DepDaof = ""
    If ArrVpfr = "" Then ArrVpfr = DepVpfr
    If ArrVpto = "" Then ArrVpto = DepVpto
    If ArrFred = "" Then ArrFred = DepFred
    If ArrFrew = "" Then ArrFrew = DepFrew
    If ArrAct3 = "" Then ArrAct3 = DepAct3
    If DepVpfr <> "" Then
        ArrDate = CedaDateToVb(DecodeSsimDayFormat(ArrVpfr, "SSIM2", "CEDA"))
        DepDate = CedaDateToVb(DecodeSsimDayFormat(DepVpfr, "SSIM2", "CEDA"))
        DepDaof = Trim(Str(DateDiff("d", ArrDate, DepDate)))
        If DepDaof = "0" Then DepDaof = ""
    End If
    
    NewRec = NewRec & ArrVpfr & ","
    NewRec = NewRec & ArrVpto & ","
    NewRec = NewRec & ArrFred & ","
    NewRec = NewRec & ArrFrew & ","
    NewRec = NewRec & ArrFlca & ","
    NewRec = NewRec & ArrFltn & ","
    NewRec = NewRec & ArrFlns & ","
    NewRec = NewRec & ArrAct3 & ",,"
    '"VPFR,VPTO,FRED,FRQW,FLCA,FLNA,FLSA,ACT3,ACGN,FLCD,FLND,FLSD,DAOF,STAT,WKST,REMA,FCA3,FCD3,GRNU,URNO,FKYA,FKYD"
    NewRec = NewRec & DepFlca & ","
    NewRec = NewRec & DepFltn & ","
    NewRec = NewRec & DepFlns & ","
    NewRec = NewRec & DepDaof & ","
    NewRec = NewRec & "0,0,,"
    NewRec = NewRec & Left(ArrFkey, 3) & ","
    NewRec = NewRec & Left(DepFkey, 3) & ","
    NewRec = NewRec & "0,0,"
    NewRec = NewRec & Left(ArrFkey, 9) & ","
    NewRec = NewRec & Left(DepFkey, 9) & ","
    NewRec = NewRec & "-1,-1,-1"
    '" BEGIN,  END,OP.DAYS,W,FCA,FLTN,S,ACT,GRP,FCD,FLTN,S,+D,I,H,Remark,FCA3,FCD3,GRNU,URNO,FKYA,FKYD,RULA,RULD,RULR"
    
    BuildStrRotationFromFlights = NewRec
End Function

Private Sub Form_Activate()
    Static IsActivated As Boolean
    Dim NewStyle As Integer
    If Not IsActivated Then
        Me.Refresh
        chkLoad(2).Enabled = False
        If StartedAsImportTool Then
            Select Case MainDialog.DataSystemType
                Case "SLOT", "SSIM"
                    chkLoad(2).Enabled = True
                Case Else
            End Select
        End If
        InitBasicData
        chkWork(2).Value = 1
        Me.Refresh
        txtVpfr(1).SetFocus
        IsActivated = True
    End If
    If AutoSwitch Then
        chkSwitch(0).Value = 1
        AutoSwitch = False
    End If
End Sub

Private Sub InitBasicData()
    Dim UrnoList As String
    Dim ValueList As String
    Dim ItemText As String
    Dim CurLine As Long
    Dim MaxLine As Long
    Dim ColValue As String
    Screen.MousePointer = 11
    If UfisServer.BasicData(0).Tag <> "OK" Then
        LoadBasics 0, "ALTTAB", "ALC3,ALC3,ALC2,ALFN,URNO", "WHERE (ALC3<>' ' OR ALC2<>' ')"
        UfisServer.BasicDataSort 0, 1
        chkAlc3(0).Value = 0
        chkAlc3(0).Value = 1
    End If
    If UfisServer.BasicData(2).Tag <> "OK" Then LoadBasics 2, "ACTTAB", "ACT3,ACFN,URNO", "WHERE ACT3<>' '"
    LoadGroupData
    Screen.MousePointer = 0
End Sub

Private Sub LoadGroupData()
    Dim UrnoList As String
    Dim ValueList As String
    Dim ItemText As String
    Dim CurLine As Long
    Dim MaxLine As Long
    Dim ColValue As String
    Screen.MousePointer = 11
    'Load defined Aircraft Groups (Names)
    LoadBasics 4, "GRNTAB", "GRPN,GRSN,URNO", "WHERE TABN='ACTTAB' AND APPL='DIET'"
    UrnoList = UfisServer.BasicData(4).SelectDistinct("2", "", "", ",", True) 'Get URNO-List
    'Load Aircraft Group Members
    UfisServer.BasicDataInit 5, 1, "GRMTAB", "PRFL,VALU,GURN,URNO", "WHERE GURN IN (" & UrnoList & ")"
    
    'Sort ACTTAB by URNO
    UfisServer.BasicDataSort 2, 2
    MaxLine = UfisServer.BasicData(5).GetLineCount - 1
    For CurLine = 0 To MaxLine
        
        ColValue = UfisServer.BasicData(5).GetColumnValue(CurLine, 1)
        ValueList = GetLineValuesByColumnValue(2, 2, ColValue)
        If ValueList = "" Then ValueList = "N.N."
        UfisServer.BasicData(5).SetColumnValue CurLine, 0, GetItem(ValueList, 1, ",")
    Next
    UfisServer.BasicDataSort 5, 1
    MaxLine = UfisServer.BasicData(2).GetLineCount - 1
    For CurLine = 0 To MaxLine
        ColValue = UfisServer.BasicData(2).GetColumnValue(CurLine, 2)
        ValueList = UfisServer.BasicData(5).GetLinesByColumnValue(1, ColValue, 0)
        If ValueList = "" Then
            ValueList = UfisServer.BasicData(2).GetColumnValue(CurLine, 0) & "," & ColValue & ",0,0,"
            UfisServer.BasicData(5).InsertTextLine ValueList, False
        End If
    Next
    UfisServer.BasicData(5).HeaderString = "ACT.ACT3,ACT.URNO,GRN.URNO,GRM.URNO,??????"
    UfisServer.BasicData(5).AutoSizeByHeader = True
    UfisServer.BasicData(5).AutoSizeColumns
    UfisServer.BasicDataSort 5, 2
    GrnUrno.Text = ""
    Screen.MousePointer = 0
End Sub

Private Sub Form_Load()
    InitForm True
End Sub

Public Sub InitForm(FirstCall As Boolean)
    Dim i As Integer
    Dim CurTab As Integer
    Dim HdrTxtStr As String
    Dim HdrLenStr As String
    Dim tmpDate As String
    If FirstCall Then
        WeeksPerMonth = 6
        Flights(0).Top = StrTabData(0).Top
        CurEditLine = -1
        LastAutoDelete = -1
        LastAutoInsert = -1
        DragIndex = -1
        DragLine = -1
        DropIndex = -1
        DropLine = -1
        StrVpfrCol = 0
        StrVptoCol = 1
        StrFredCol = 2
        StrFrqwCol = 3
        StrFlcaCol = 4
        StrFlnaCol = 5
        StrFlsaCol = 6
        StrAct3Col = 7
        StrAcgnCol = 8
        StrFlcdCol = 9
        StrFlndCol = 10
        StrFlsdCol = 11
        StrDaofCol = 12
        StrStatCol = 13
        StrWkstCol = 14
        StrRemaCol = 15
        StrFca3Col = 16
        StrFcd3Col = 17
        StrGrnuCol = 18
        StrUrnoCol = 19
        StrFkyaCol = 20
        StrFkydCol = 21
        StrRulaCol = 22
        StrRuldCol = 23
        StrRulrCol = 24
        For i = 0 To 1
            Flights(i).ResetContent
            Flights(i).ShowHorzScroller True
            Flights(i).ShowVertScroller True
            Flights(i).VScrollMaster = False
            Flights(i).FontName = "Courier New"
            Flights(i).SetTabFontBold True
            Flights(i).HeaderFontSize = 17
            Flights(i).FontSize = 17
            Flights(i).LineHeight = 17
            Flights(i).LeftTextOffset = 2
            Flights(i).HeaderString = "A,Flight,ACT,  Day,O,Time,R,H,S,Remark,FKEY,RULE,ROTA,LINE,ALC3"
            Flights(i).ColumnWidthString = "1,9,3,8,1,4,1,1,1,30,20,10,10,10,3"
            Flights(i).HeaderLengthString = "16,90,33,70,16,42,18,18,18,62,180,100,200,100,33"
            Flights(i).GridLineColor = DarkGray
            Flights(i).LifeStyle = True
            'Flights(i).CursorLifeStyle = True
            Flights(i).DateTimeSetColumn 3
            Flights(i).DateTimeSetInputFormatString 3, "YYYYMMDD"
            Flights(i).DateTimeSetOutputFormatString 3, "DDMMMYY"
            Flights(i).SetColumnBoolProperty 6, "1", "0"
            Flights(i).SetColumnBoolProperty 7, "1", "0"
            Flights(i).SetColumnBoolProperty 8, "1", "0"
            Flights(i).SetBoolPropertyReadOnly 6, True
            Flights(i).SetBoolPropertyReadOnly 7, True
            Flights(i).SetBoolPropertyReadOnly 8, True
            Flights(i).CreateDecorationObject "Marker1", "L,L,T,B", "-1,2,2,2", Str(LightBlue) & "," & Str(LightestBlue) & "," & Str(LightestBlue) & "," & Str(DarkBlue)
            Flights(i).CreateDecorationObject "Marker2", "L,B,T", "-1,2,2", Str(LightBlue) & "," & Str(DarkBlue) & "," & Str(LightestBlue)
            Flights(i).CreateDecorationObject "Marker3", "L,R,T,B", "-1,2,2,2", Str(LightBlue) & "," & Str(DarkBlue) & "," & Str(LightestBlue) & "," & Str(DarkBlue)
            Flights(i).CreateDecorationObject "ArrMarker1", "BTL,L,L,T,B", "-15,-1,2,2,2", Str(NormalGray) & "," & Str(vbYellow) & "," & Str(LightestBlue) & "," & Str(LightestBlue) & "," & Str(DarkBlue)
            Flights(i).CreateDecorationObject "ArrMarker2", "L,B,T", "-1,2,2", Str(vbYellow) & "," & Str(DarkBlue) & "," & Str(LightestBlue)
            Flights(i).CreateDecorationObject "ArrMarker3", "L,R,T,B", "-1,2,2,2", Str(vbYellow) & "," & Str(DarkBlue) & "," & Str(LightestBlue) & "," & Str(DarkBlue)
            Flights(i).CreateDecorationObject "DepMarker1", "BTL,L,L,T,B", "-15,-1,2,2,2", Str(NormalGray) & "," & Str(vbGreen) & "," & Str(LightestBlue) & "," & Str(LightestBlue) & "," & Str(DarkBlue)
            Flights(i).CreateDecorationObject "DepMarker2", "L,B,T", "-1,2,2", Str(vbGreen) & "," & Str(DarkBlue) & "," & Str(LightestBlue)
            Flights(i).CreateDecorationObject "DepMarker3", "L,R,T,B", "-1,2,2,2", Str(vbGreen) & "," & Str(DarkBlue) & "," & Str(LightestBlue) & "," & Str(DarkBlue)
        Next
        Flights(0).ShowHorzScroller False
        Flights(0).SetMainHeaderValues "6,3,6", "Import Arrival Flights,Status,System", "12632256,12632256,12632256"
        Flights(0).MainHeader = True
        Flights(1).SetMainHeaderValues "6,3,6", "Import Departure Flights,Status,System", "12632256,12632256,12632256"
        Flights(1).MainHeader = True
        Splitter(1).Left = Flights(0).Left
        Splitter(1).Width = Flights(0).Width
        StrTabFina = "VPFR,VPTO,FRED,FRQW,FLCA,FLNA,FLSA,ACT3,ACGN,FLCD,FLND,FLSD,DAOF,STAT,WKST,REMA,FCA3,FCD3,GRNU,URNO"
        StrTabFele = "8,8,7,1,3,5,1,3,3,3,5,1,2,1,1,64,3,3,10,10"
        StrTabList = "0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19"
        HdrTxtStr = " BEGIN,  END,OP.DAYS,W,FCA,FLTN,S,ACT,GRP,FCD,FLTN,S,+D,I,H,Remark,FCA3,FCD3,GRNU,URNO,FKYA,FKYD,RULA,RULD,RULR"
        HdrLenStr = "70,70,70,18,34,45,18,34,50,34,45,18,25,18,18,500,34,34,90,90,100,100,200,200,100"
        For i = 0 To 1
            StrTabData(i).ResetContent
            StrTabData(i).MainHeader = True
            StrTabData(i).ShowHorzScroller True
            StrTabData(i).ShowVertScroller True
            StrTabData(i).VScrollMaster = False
            StrTabData(i).FontName = "Courier New"
            StrTabData(i).SetTabFontBold True
            StrTabData(i).HeaderFontSize = 17
            StrTabData(i).FontSize = 17
            StrTabData(i).LineHeight = 17
            StrTabData(i).LeftTextOffset = 2
            StrTabData(i).LifeStyle = True
            StrTabData(i).CursorLifeStyle = True
            StrTabData(i).HeaderString = HdrTxtStr
            StrTabData(i).HeaderLengthString = HdrLenStr
            StrTabData(i).ColumnWidthString = StrTabFele & ",10,10,100,100,100"
            StrTabData(i).SetColumnBoolProperty 13, "1", "0"
            StrTabData(i).SetColumnBoolProperty 14, "1", "0"
            StrTabData(i).SetBoolPropertyReadOnly 13, True
            StrTabData(i).SetBoolPropertyReadOnly 14, True
            StrTabData(i).GridLineColor = DarkGray
            StrTabData(i).NoFocusColumns = "8,13,14,16,17,18,19,20,21,22,23,24"
            StrTabData(i).InplaceEditUpperCase = True
            StrTabData(i).InplaceEditSendKeyEvents = False
            StrTabData(i).EnableInlineEdit False
            StrTabData(i).CreateDecorationObject "CellDiff", "R,T,B,L", "2,2,2,2", Str(vbBlack) & "," & Str(LightGray) & "," & Str(vbBlack) & "," & Str(LightGray)
            StrTabData(i).CreateDecorationObject "Marker1", "L,L,T,B", "-1,2,2,2", Str(LightBlue) & "," & Str(LightestBlue) & "," & Str(LightestBlue) & "," & Str(DarkBlue)
            StrTabData(i).CreateDecorationObject "Marker2", "L,B,T", "-1,2,2", Str(LightBlue) & "," & Str(DarkBlue) & "," & Str(LightestBlue)
            StrTabData(i).CreateDecorationObject "Marker3", "L,R,T,B", "-1,2,2,2", Str(LightBlue) & "," & Str(DarkBlue) & "," & Str(LightestBlue) & "," & Str(DarkBlue)
            StrTabData(i).CreateDecorationObject "ArrMarker1", "L,L,T,B", "-1,2,2,2", Str(vbYellow) & "," & Str(LightestBlue) & "," & Str(LightestBlue) & "," & Str(DarkBlue)
            StrTabData(i).CreateDecorationObject "ArrMarker2", "L,B,T", "-1,2,2", Str(vbYellow) & "," & Str(DarkBlue) & "," & Str(LightestBlue)
            StrTabData(i).CreateDecorationObject "ArrMarker3", "L,R,T,B", "-1,2,2,2", Str(vbYellow) & "," & Str(DarkBlue) & "," & Str(LightestBlue) & "," & Str(DarkBlue)
            StrTabData(i).CreateDecorationObject "DepMarker1", "L,L,T,B", "-1,2,2,2", Str(vbGreen) & "," & Str(LightestBlue) & "," & Str(LightestBlue) & "," & Str(DarkBlue)
            StrTabData(i).CreateDecorationObject "DepMarker2", "L,B,T", "-1,2,2", Str(vbGreen) & "," & Str(DarkBlue) & "," & Str(LightestBlue)
            StrTabData(i).CreateDecorationObject "DepMarker3", "L,R,T,B", "-1,2,2,2", Str(vbGreen) & "," & Str(DarkBlue) & "," & Str(LightestBlue) & "," & Str(DarkBlue)
        Next
        StrTabData(0).SetMainHeaderValues "4,3,2,4,3,6", "Standard Validity Period,Arrival,Aircraft,Departure,Status,System", "12632256,12632256,12632256,12632256,12632256,12632256"
        StrTabData(1).SetMainHeaderValues "4,3,2,4,3,6", "Import Validity Period,Arrival,Aircraft,Departure,Status,System", "12632256,12632256,12632256,12632256,12632256,12632256"
        HdrTxtStr = " BEGIN,  END,OP.DAYS,W,FCA,FLTN,S,ACT,GRP,FCD,FLTN,S,+D,M,T,Remark,FCA3,FCD3,GRNU,URNO,FKYA,FKYD,RULA,RULD,RULR"
        StrTabData(1).HeaderString = HdrTxtStr
        UfisServer.UrnoPoolInit 500
        ArrangeArrDays
        For i = 0 To 7
            If FilterTab(i).Tag <> "OK" Then
                FilterTab(i).ShowHorzScroller False
                'FilterTab(i).ShowHorzScroller True
                FilterTab(i).ResetContent
                FilterTab(i).FontName = "Courier New"
                FilterTab(i).SetTabFontBold True
                FilterTab(i).HeaderFontSize = 17
                FilterTab(i).FontSize = 17
                FilterTab(i).LineHeight = 17
                FilterTab(i).LifeStyle = True
                'FilterTab(i).CursorLifeStyle = True
                FilterTab(i).HeaderLengthString = "200,200,200,200,200"
                FilterTab(i).HeaderString = ".,.,.,.,."
                FilterTab(i).Tag = "OK"
                If i < 6 Then
                    If i Mod 2 = 0 Then
                        FilterTab(i).DisplayBackColor = LightYellow
                        FilterTab(i).DisplayTextColor = vbBlack
                        FilterTab(i).SelectBackColor = DarkestYellow
                        FilterTab(i).SelectTextColor = vbWhite
                    Else
                        FilterTab(i).DisplayBackColor = NormalBlue
                        FilterTab(i).DisplayTextColor = vbWhite
                        FilterTab(i).SelectBackColor = DarkestBlue
                        FilterTab(i).SelectTextColor = vbWhite
                    End If
                Else
                    FilterTab(i).ShowRowSelection = False
                End If
                FilterTab(i).EmptyAreaBackColor = LightGray
                FilterTab(i).GridLineColor = DarkGray
                FilterTab(i).MainHeaderOnly = True
            End If
        Next
        'Splitter(0).Left = StrTabData(0).Left
        'Splitter(2).Left = StrTabData(0).Left + Splitter(0).Width + 1 * Screen.TwipsPerPixelX
        'Splitter(0).Width = StrTabData(0).Width
        CurEditLine = -1
        CurDelCount = 0
        CurInsCount = 0
        CurUpdCount = 0
        CurTtlCount = 0
        LastAutoDelete = -1
        LastAutoInsert = -1
        chkWork(1).Enabled = False
        If Not StartedAsImportTool Then
            chkWork(17).Caption = "Exit"
            chkSwitch(0).Enabled = False
            tmpDate = Format(DateAdd("d", -10, Now), "yyyymmdd", 1)
            txtVpfr(0).Tag = tmpDate
            txtVpfr(0).Text = Left(tmpDate, 4)
            txtVpfr(1).Text = Mid(tmpDate, 5, 2)
            txtVpfr(2).Text = Right(tmpDate, 2)
            tmpDate = Format(DateAdd("d", 10, Now), "yyyymmdd", 1)
            txtVpto(0).Tag = tmpDate
            txtVpto(0).Text = Left(tmpDate, 4)
            txtVpto(1).Text = Mid(tmpDate, 5, 2)
            txtVpto(2).Text = Right(tmpDate, 2)
            chkStrSplitter(1).Value = 1
        End If
        Me.Left = 0
        Me.Top = 0
        Me.Height = Screen.Height - 425
        Me.Width = 1024 * Screen.TwipsPerPixelX
    End If
    If ShowStrTabStyle = 0 Then
        'ImportTool Style
        StrTabData(0).ShowHorzScroller False
        For i = 9 To 14
            chkWork(i).Visible = True
        Next
        chkWork(17).Left = chkWork(11).Left + chkWork(11).Width + 1 * Screen.TwipsPerPixelX
        chkWork(17).Caption = "Close"
        StrTabData(0).Left = PeriodPanel.Left
        StrTabData(1).Left = PeriodPanel.Left
        StrTabData(1).Visible = True
        Flights(0).Visible = True
        Flights(1).Visible = True
        For i = 0 To 6 Step 2
            FilterFrame(i).Visible = False
        Next
        Splitter(0).Visible = True
        Splitter(1).Visible = True
        Splitter(2).Visible = True
    End If
    If ShowStrTabStyle = 1 Then
        'Show StrTab only
        StrTabData(0).ShowHorzScroller True
        Flights(0).Visible = False
        Flights(1).Visible = False
        For i = 9 To 14
            chkWork(i).Visible = False
        Next
        chkWork(17).Left = chkWork(9).Left
        FilterFrame(0).Top = StrTabData(0).Top - 7 * Screen.TwipsPerPixelX
        FilterFrame(0).Left = PeriodPanel.Left
        FilterFrame(2).Left = FilterFrame(0).Left
        FilterFrame(2).Top = FilterFrame(0).Top + FilterFrame(0).Height + 4 * Screen.TwipsPerPixelX
        StrTabData(0).Left = SplitPanel.Left
        StrTabData(1).Left = StrTabData(0).Left
        StrTabData(1).Visible = False
        FilterFrame(4).Top = FilterFrame(0).Top
        FilterFrame(4).Left = StrTabData(0).Left + StrTabData(0).Width + 5 * Screen.TwipsPerPixelY
        FilterFrame(6).Left = FilterFrame(4).Left
        FilterFrame(6).Top = FilterFrame(4).Top + FilterFrame(4).Height + 4 * Screen.TwipsPerPixelX
        FilterFrame(0).Visible = True
        FilterFrame(2).Visible = True
        FilterFrame(4).Visible = True
        FilterFrame(6).Visible = True
        Splitter(0).Visible = False
        Splitter(1).Visible = False
        Splitter(2).Visible = False
    End If
    If MainDialog.DataSystemType = "FHK" Then chkWork(13).Enabled = True Else chkWork(13).Enabled = False
    DrawBackGround SplitAnchor(0), 7, True, True
    DrawBackGround SplitAnchor(1), 7, True, True
End Sub
Public Sub SetExtractTimeframe()
    Dim i As Integer
    If StartedAsImportTool Then
        For i = 0 To 2
            Me.txtVpfr(i).Tag = FlightExtract.txtVpfr(i).Tag
            Me.txtVpfr(i).Text = FlightExtract.txtVpfr(i).Text
            Me.txtVpto(i).Tag = FlightExtract.txtVpto(i).Tag
            Me.txtVpto(i).Text = FlightExtract.txtVpto(i).Text
        Next
    End If
End Sub

Public Sub LoadStrTabData(TimeFrom As String, TimeTo As String, FlcaList As String, Act3List As String, GrnuList As String)
    Dim tmpAnsw As Boolean
    Dim StrSqlKey As String
    Dim tmpData As String
    Dim tmpAlc3 As String
    Dim tmpFltn As String
    Dim tmpFlns As String
    Dim tmpFkey As String
    Dim CurLine As Long
    Dim MaxLine As Long
    StrTabData(0).CedaCurrentApplication = UfisServer.ModName
    StrTabData(0).CedaHopo = UfisServer.HOPO
    StrTabData(0).CedaIdentifier = "IDX"
    StrTabData(0).CedaPort = "3357"
    StrTabData(0).CedaReceiveTimeout = "250"
    StrTabData(0).CedaRecordSeparator = vbLf
    StrTabData(0).CedaSendTimeout = "250"
    StrTabData(0).CedaServerName = UfisServer.HostName
    StrTabData(0).CedaTabext = UfisServer.TblExt
    StrTabData(0).CedaUser = UfisServer.ModName
    StrTabData(0).CedaWorkstation = UfisServer.GetMyWorkStationName
    StrSqlKey = "WHERE "
    StrSqlKey = StrSqlKey & "(VPTO>='" & TimeFrom & "' AND VPFR<='" & TimeTo & "') "
    If FlcaList <> "" Then StrSqlKey = StrSqlKey & "AND (FCA3 IN (" & FlcaList & ") OR FCD3 IN (" & FlcaList & ")) "
    If (Act3List <> "") And (GrnuList <> "") Then
        StrSqlKey = StrSqlKey & "AND ((ACT3 IN (" & Act3List & ")) OR (GRNU IN (" & GrnuList & ")))"
    ElseIf Act3List <> "" Then
        StrSqlKey = StrSqlKey & "AND (ACT3 IN (" & Act3List & "))"
    ElseIf GrnuList <> "" Then
        StrSqlKey = StrSqlKey & "AND (GRNU IN (" & GrnuList & "))"
    End If
    If CedaIsConnected Then
        StrTabData(0).CedaAction "RTA", "STRTAB", StrTabFina, "", StrSqlKey
    Else
        'StrTabData(0).ReadFromFile "c:\ufis\strtabdata.txt"
    End If
    StrTabData(0).Refresh
    MaxLine = StrTabData(0).GetLineCount - 1
    For CurLine = 0 To MaxLine
        StrTabData(0).SetColumnValue CurLine, StrFredCol, FormatFrequency(Trim(StrTabData(0).GetColumnValue(CurLine, StrFredCol)))
        tmpData = Trim(StrTabData(0).GetColumnValue(CurLine, StrGrnuCol))
        If tmpData = "0" Then
            StrTabData(0).SetColumnValue CurLine, StrAcgnCol, ""
        Else
            'Show Group
        End If
        StrTabData(0).SetColumnValue CurLine, StrWkstCol, "0"
        tmpAlc3 = StrTabData(0).GetColumnValue(CurLine, StrFca3Col)
        tmpFltn = StrTabData(0).GetColumnValue(CurLine, StrFlnaCol)
        tmpFlns = StrTabData(0).GetColumnValue(CurLine, StrFlsaCol)
        tmpFkey = Left(TurnAftFkey(BuildAftFkey(tmpAlc3, tmpFltn, tmpFlns, "", "")), 9)
        StrTabData(0).SetColumnValue CurLine, StrFkyaCol, tmpFkey
        tmpAlc3 = StrTabData(0).GetColumnValue(CurLine, StrFcd3Col)
        tmpFltn = StrTabData(0).GetColumnValue(CurLine, StrFlndCol)
        tmpFlns = StrTabData(0).GetColumnValue(CurLine, StrFlsdCol)
        tmpFkey = Left(TurnAftFkey(BuildAftFkey(tmpAlc3, tmpFltn, tmpFlns, "", "")), 9)
        StrTabData(0).SetColumnValue CurLine, StrFkydCol, tmpFkey
        StrTabData(0).SetColumnValue CurLine, StrRulaCol, "-1"
        StrTabData(0).SetColumnValue CurLine, StrRuldCol, "-1"
        StrTabData(0).SetColumnValue CurLine, StrRulrCol, "-1"
        StrTabData(0).SetColumnValue CurLine, StrVpfrCol, DecodeSsimDayFormat(Left(StrTabData(0).GetColumnValue(CurLine, StrVpfrCol), 8), "CEDA", "SSIM2")
        StrTabData(0).SetColumnValue CurLine, StrVptoCol, DecodeSsimDayFormat(Left(StrTabData(0).GetColumnValue(CurLine, StrVptoCol), 8), "CEDA", "SSIM2")
        StrTabData(0).SetLineTag CurLine, StrTabData(0).GetLineValues(CurLine)
        StrTabData(0).SetLineColor CurLine, vbBlack, LightGray
    Next
End Sub

Private Sub CheckAftTabLoader()
    Dim MaxLine As Long
    Dim CurLine As Long
    Dim tmpGrnUrno As String
    Dim TmpBegin As String
    Dim TmpEnd As String
    Dim tmpAlc3Filter As String
    Dim tmpAct3Filter As String
    Dim RetVal As Integer
    Dim MsgTxt As String
    Dim i As Integer
    If CheckLoadPeriod = True Then
        If (Flights(0).GetLineCount = 0) Or (Flights(1).GetLineCount = 0) Then chkWork(9).Value = 1
        Screen.MousePointer = 11
        If chkFilter(0).Value = 1 Then tmpAlc3Filter = FilterTab(1).SelectDistinct(1, "", "", vbLf, True)
        If tmpAlc3Filter = "" Then tmpAlc3Filter = Flights(0).SelectDistinct(14, "", "", vbLf, True) & vbLf & Flights(1).SelectDistinct(14, "", "", vbLf, True)
        If (chkFilter(2).Value = 1) Or (chkFilter(4).Value = 1) Then
            If chkFilter(2).Value = 1 Then tmpAct3Filter = FilterTab(3).SelectDistinct(0, "", "", vbLf, True)
            If chkFilter(4).Value = 1 Then
                MaxLine = FilterTab(5).GetLineCount - 1
                For CurLine = 0 To MaxLine
                    tmpGrnUrno = FilterTab(5).GetColumnValue(CurLine, 2)
                    tmpAct3Filter = tmpAct3Filter & vbLf & GetLineValuesByColumnValue(5, 2, tmpGrnUrno)
                Next
            End If
        End If
        If tmpAct3Filter = "" Then tmpAct3Filter = Flights(0).SelectDistinct(2, "", "", vbLf, True) & vbLf & Flights(1).SelectDistinct(2, "", "", vbLf, True)
        AftTabData(0).ResetContent
        AftTabData(0).InsertBuffer tmpAlc3Filter, vbLf
        tmpAlc3Filter = AftTabData(0).SelectDistinct(0, "'", "'", ",", True)
        AftTabData(0).ResetContent
        AftTabData(0).InsertBuffer tmpAct3Filter, vbLf
        tmpAct3Filter = AftTabData(0).SelectDistinct(0, "'", "'", ",", True)
        If tmpAlc3Filter = "''" Then tmpAlc3Filter = ""
        If tmpAct3Filter = "''" Then tmpAct3Filter = ""
        RetVal = 1
        If (tmpAlc3Filter = "") And (tmpAct3Filter = "") Then
            MsgTxt = "Do you really want to load all flights" & vbNewLine
            MsgTxt = MsgTxt & "without any particular filter ?"
            RetVal = MyMsgBox.CallAskUser(0, 0, 0, "Transaction Control", MsgTxt, "ask", "Yes,No;F", UserAnswer)
        End If
        If RetVal = 1 Then LoadAftTabData txtVpfr(0).Tag, txtVpto(0).Tag, tmpAlc3Filter, tmpAct3Filter
        Screen.MousePointer = 0
    Else
        
    End If
End Sub

Public Sub LoadAftTabData(TimeFrom As String, TimeTo As String, FlcaList As String, Act3List As String)
    Dim AftSqlKey As String
    Dim AftTabFina As String
    Dim tmpFtyp As String
    Dim tmpAlc3 As String
    Dim tmpFlno As String
    Dim tmpFltn As String
    Dim tmpFlns As String
    Dim tmpFkey As String
    Dim tmpFred As String
    Dim tmpAct3 As String
    Dim tmpApc3 As String
    Dim tmpVia3 As String
    Dim tmpSked As String
    Dim tmpStyp As String
    Dim tmpRegn As String
    Dim CurFkeyList As String
    Dim NewRec As String
    Dim NewTag As String
    Dim CurLine As Long
    Dim MaxLine As Long
    Dim LineNo As Long
    Dim OldMax As Long
    AftTabData(0).ResetContent
    AftTabData(0).CedaCurrentApplication = UfisServer.ModName
    AftTabData(0).CedaHopo = UfisServer.HOPO
    AftTabData(0).CedaIdentifier = "IDX"
    AftTabData(0).CedaPort = "3357"
    AftTabData(0).CedaReceiveTimeout = "250"
    AftTabData(0).CedaRecordSeparator = vbLf
    AftTabData(0).CedaSendTimeout = "250"
    AftTabData(0).CedaServerName = UfisServer.HostName
    AftTabData(0).CedaTabext = UfisServer.TblExt
    AftTabData(0).CedaUser = UfisServer.ModName
    AftTabData(0).CedaWorkstation = UfisServer.GetMyWorkStationName
    AftTabFina = "FKEY,FTYP,FLNO,STOA,DOOA,ACT3,ALC3,ORG3,VIA3,REGN," & MainDialog.UseStypField
    AftSqlKey = "WHERE "
    AftSqlKey = AftSqlKey & "STOA BETWEEN '" & TimeFrom & "000000' AND '" & TimeTo & "235959'  AND ADID='A' AND FLNO<>' ' "
    If FlcaList <> "" Then AftSqlKey = AftSqlKey & "AND ALC3 IN (" & FlcaList & ") "
    If Act3List <> "" Then AftSqlKey = AftSqlKey & "AND ACT3 IN (" & Act3List & ") "
    If CedaIsConnected Then
        AftTabData(0).CedaAction "RTA", "AFTTAB", AftTabFina, "", AftSqlKey
    Else
        'AftTabData(0).ReadFromFile "c:\ufis\AftTabData.txt"
    End If
    CurFkeyList = Flights(0).SelectDistinct(10, "", "", ",", True)
    MaxLine = AftTabData(0).GetLineCount - 1
    LineNo = Flights(0).GetLineCount
    OldMax = LineNo
    For CurLine = 0 To MaxLine
        tmpFkey = TurnAftFkey(AftTabData(0).GetColumnValue(CurLine, 0))
        If InStr(CurFkeyList, tmpFkey) = 0 Then
            '"FKEY,FTYP,FLNO,STOA,DOOA,ACT3,ALC3,ORG3,VIA3,REGN," & MainDialog.UseStypField
            tmpFtyp = AftTabData(0).GetColumnValue(CurLine, 1)
            tmpFlno = AftTabData(0).GetColumnValue(CurLine, 2)
            tmpSked = AftTabData(0).GetColumnValue(CurLine, 3)
            tmpFred = AftTabData(0).GetColumnValue(CurLine, 4)
            tmpAct3 = AftTabData(0).GetColumnValue(CurLine, 5)
            tmpAlc3 = AftTabData(0).GetColumnValue(CurLine, 6)
            tmpApc3 = AftTabData(0).GetColumnValue(CurLine, 7)
            tmpVia3 = AftTabData(0).GetColumnValue(CurLine, 8)
            tmpRegn = AftTabData(0).GetColumnValue(CurLine, 9)
            tmpStyp = AftTabData(0).GetColumnValue(CurLine, 10)
            NewRec = ","
            NewRec = NewRec & tmpFlno & ","
            NewRec = NewRec & tmpAct3 & ","
            NewRec = NewRec & Left(tmpSked, 8) & ","
            NewRec = NewRec & tmpFred & ","
            NewRec = NewRec & Mid(tmpSked, 9, 4) & ","
            NewRec = NewRec & "0,0,1,,"
            NewRec = NewRec & tmpFkey & ","
            NewRec = NewRec & "-1,-1,-1,"
            NewRec = NewRec & tmpAlc3
            Flights(0).InsertTextLine NewRec, False
            Flights(0).SetLineColor LineNo, vbBlack, vbYellow
            NewTag = "-1,,,"
            NewTag = NewTag & Trim(Left(tmpFlno, 3)) & ","
            NewTag = NewTag & Trim(Mid(tmpFlno, 4)) & ","
            NewTag = NewTag & ",,"
            NewTag = NewTag & Left(tmpSked, 8) & ","
            NewTag = NewTag & Left(tmpSked, 8) & ","
            NewTag = NewTag & FormatFrequency(tmpFred) & ","
            NewTag = NewTag & tmpAct3 & ","
            NewTag = NewTag & ","
            NewTag = NewTag & tmpApc3 & ","
            NewTag = NewTag & tmpVia3 & ","
            NewTag = NewTag & tmpSked & ","
            NewTag = NewTag & ",,,,"
            NewTag = NewTag & tmpStyp & ","
            NewTag = NewTag & ",,"
            NewTag = NewTag & tmpRegn & ","
            NewTag = NewTag & tmpFtyp & ","
            NewTag = NewTag & ",-1"
            Flights(0).SetLineTag LineNo, NewTag
            LineNo = LineNo + 1
        End If
    Next
    Flights(0).Sort 10, True, True
    Flights(0).Refresh
    CountArr(0) = CountArr(0) + (LineNo - OldMax)
    CountArr(1) = CountArr(1) + (LineNo - OldMax)
    PublishCounters
    AftTabData(0).ResetContent
    Me.Refresh
    AftTabFina = "FKEY,FTYP,FLNO,STOD,DOOD,ACT3,ALC3,DES3,VIA3,REGN," & MainDialog.UseStypField
    AftSqlKey = "WHERE "
    AftSqlKey = AftSqlKey & "STOD BETWEEN '" & TimeFrom & "000000' AND '" & TimeTo & "235959'  AND ADID='D' AND FLNO<>' ' "
    If FlcaList <> "" Then AftSqlKey = AftSqlKey & "AND ALC3 IN (" & FlcaList & ") "
    If Act3List <> "" Then AftSqlKey = AftSqlKey & "AND ACT3 IN (" & Act3List & ") "
    If CedaIsConnected Then
        AftTabData(0).CedaAction "RTA", "AFTTAB", AftTabFina, "", AftSqlKey
    Else
        'AftTabData(0).ReadFromFile "c:\ufis\AftTabData.txt"
    End If
    CurFkeyList = Flights(1).SelectDistinct(10, "", "", ",", True)
    MaxLine = AftTabData(0).GetLineCount - 1
    LineNo = Flights(1).GetLineCount
    OldMax = LineNo
    For CurLine = 0 To MaxLine
        tmpFkey = TurnAftFkey(AftTabData(0).GetColumnValue(CurLine, 0))
        If InStr(CurFkeyList, tmpFkey) = 0 Then
            '"FKEY,FTYP,FLNO,STOA,DOOA,ACT3,ALC3,ORG3,VIA3,REGN," & MainDialog.UseStypField
            tmpFtyp = AftTabData(0).GetColumnValue(CurLine, 1)
            tmpFlno = AftTabData(0).GetColumnValue(CurLine, 2)
            tmpSked = AftTabData(0).GetColumnValue(CurLine, 3)
            tmpFred = AftTabData(0).GetColumnValue(CurLine, 4)
            tmpAct3 = AftTabData(0).GetColumnValue(CurLine, 5)
            tmpAlc3 = AftTabData(0).GetColumnValue(CurLine, 6)
            tmpApc3 = AftTabData(0).GetColumnValue(CurLine, 7)
            tmpVia3 = AftTabData(0).GetColumnValue(CurLine, 8)
            tmpRegn = AftTabData(0).GetColumnValue(CurLine, 9)
            tmpStyp = AftTabData(0).GetColumnValue(CurLine, 10)
            NewRec = ","
            NewRec = NewRec & tmpFlno & ","
            NewRec = NewRec & tmpAct3 & ","
            NewRec = NewRec & Left(tmpSked, 8) & ","
            NewRec = NewRec & tmpFred & ","
            NewRec = NewRec & Mid(tmpSked, 9, 4) & ","
            NewRec = NewRec & "0,0,1,,"
            NewRec = NewRec & tmpFkey & ","
            NewRec = NewRec & "-1,-1,-1,"
            NewRec = NewRec & tmpAlc3
            Flights(1).InsertTextLine NewRec, False
            Flights(1).SetLineColor LineNo, vbBlack, vbGreen
            NewTag = "-1,,,"
            NewTag = NewTag & ",,"
            NewTag = NewTag & Trim(Left(tmpFlno, 3)) & ","
            NewTag = NewTag & Trim(Mid(tmpFlno, 4)) & ","
            NewTag = NewTag & Left(tmpSked, 8) & ","
            NewTag = NewTag & Left(tmpSked, 8) & ","
            NewTag = NewTag & FormatFrequency(tmpFred) & ","
            NewTag = NewTag & tmpAct3 & ","
            NewTag = NewTag & ","
            NewTag = NewTag & ",,,"
            NewTag = NewTag & tmpSked & ","
            NewTag = NewTag & ","
            NewTag = NewTag & tmpVia3 & ","
            NewTag = NewTag & tmpApc3 & ","
            NewTag = NewTag & ","
            NewTag = NewTag & tmpStyp & ","
            NewTag = NewTag & ","
            NewTag = NewTag & tmpRegn & ","
            NewTag = NewTag & tmpFtyp & ","
            NewTag = NewTag & ",-1"
            Flights(1).SetLineTag LineNo, NewTag
            LineNo = LineNo + 1
        End If
    Next
    Flights(1).Sort 10, True, True
    Flights(1).Refresh
    CountDep(0) = CountDep(0) + (LineNo - OldMax)
    CountDep(1) = CountDep(1) + (LineNo - OldMax)
    PublishCounters
    AftTabData(0).ResetContent
    Me.Refresh
End Sub

Private Sub LoadBasics(Index As Integer, TableName As String, Fields As String, SqlKey As String)
    Dim tmpBuff As String
    Dim CurLine As Long
    Dim MaxLine As Long
    FilterTab(Index).ResetContent
    FilterTab(Index + 1).ResetContent
    UfisServer.BasicDataInit Index, 0, TableName, Fields, SqlKey
    MaxLine = UfisServer.BasicData(Index).GetLineCount - 1
    For CurLine = 0 To MaxLine
        tmpBuff = UfisServer.BasicData(Index).GetLineValues(CurLine)
        FilterTab(Index).InsertTextLine tmpBuff, False
    Next
    FilterTab(Index).Refresh
    FilterTab(Index + 1).Refresh
    chkFilterSet(Index).Caption = Trim(Str(MaxLine + 1))
    chkFilterSet(Index + 1).Caption = "0"
    chkFilter(Index).Value = 1
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    If UnloadMode = 0 Then
        If StartedAsImportTool Then
            Me.Hide
            FlightExtract.Show
            Cancel = True
        Else
            If Not ShutDownRequested Then
                chkWork(17).Value = 1
                Cancel = True
            End If
        End If
    End If
End Sub

Private Sub chkWork_Click(Index As Integer)
    Dim CurSelLine As Long
    Dim CurMaxLine As Long
    Dim tmpBuff As String
    If chkWork(Index).Value = 1 Then
        chkWork(Index).BackColor = LightGreen
        chkWork(Index).Refresh
    End If
    Select Case Index
        Case 0  'Load
            If chkWork(Index).Value = 1 Then
                chkWork(3).Value = 0
                chkWork(4).Value = 0
                chkWork(5).Value = 0
                CheckStrTabLoader
                chkWork(Index).Value = 0
            End If
        Case 1  'Save
            If chkWork(Index).Value = 1 Then
                chkWork(3).Value = 0
                chkWork(4).Value = 0
                chkWork(5).Value = 0
                SaveStrTabData
                chkWork(Index).Value = 0
            End If
        Case 2  'Reset
            If chkWork(Index).Value = 1 Then
                'chkWork(0).Value = 1
                'Me.Refresh
                'If StartedAsImportTool Then chkWork(9).Value = 1
                chkWork(Index).Value = 0
            End If
        Case 3  'Insert
            If chkWork(Index).Value = 1 Then
                If Not RulesFromFile Then
                    chkStrSplitter(5).Value = 0
                    CheckInsert
                Else
                    chkWork(Index).Value = 0
                End If
            Else
                If Not RulesFromFile Then
                    LastAutoInsert = -1
                    If chkWork(4).Value = 0 Then
                        StrTabData(0).EnableInlineEdit False
                        IsInplaceEdit = False
                        HandleComboBox -1, -1, False
                        StrTabData(0).SetBoolPropertyReadOnly 13, True
                    End If
                End If
            End If
        Case 4  'Update
            If chkWork(Index).Value = 1 Then
                If Not RulesFromFile Then
                    chkStrSplitter(5).Value = 0
                    CurSelLine = StrTabData(0).GetCurrentSelected
                    StrTabData(0).ResetLineDecorations CurSelLine
                    CheckUpdateChanges
                    StrTabData(0).SetBoolPropertyReadOnly 13, False
                    StrTabData(0).EnableInlineEdit True
                    IsInplaceEdit = True
                    HandleComboBox CurSelLine, 1, True
                    StrTabData(0).Refresh
                Else
                    chkWork(Index) = 0
                End If
            Else
                If Not RulesFromFile Then
                    If chkWork(3).Value = 0 Then
                        StrTabData(0).EnableInlineEdit False
                        IsInplaceEdit = False
                        HandleComboBox -1, -1, False
                        StrTabData(0).SetBoolPropertyReadOnly 13, True
                    End If
                End If
            End If
        Case 5  'Delete
            If chkWork(Index).Value = 1 Then
                If Not RulesFromFile Then CheckDelete
                chkWork(Index).Value = 0
            End If
        Case 6  'Split
            If chkWork(Index).Value = 1 Then
                chkWork(7).Value = 0
                CheckSplitPanel Index, True
            Else
                CheckSplitPanel Index, False
            End If
        Case 7  'Join
            If chkWork(Index).Value = 1 Then
                chkWork(6).Value = 0
                CheckSplitPanel Index, True
            Else
                CheckSplitPanel Index, False
            End If
        Case 8  'Batch
            If chkWork(Index).Value = 1 Then
                chkWork(Index).Value = 0
            End If
        Case 9  'Explode
            If chkWork(Index).Value = 1 Then
                LoadClientFlights
                chkWork(Index).Value = 0
            End If
        Case 10  'Create
            If chkWork(Index).Value = 1 Then
                BuildUpRotations
                chkWork(Index).Value = 0
            End If
        Case 11  'Dispo
            If chkWork(Index).Value = 1 Then
                chkWork(Index).Value = 0
            End If
        Case 12  'Transfer
            If chkWork(Index).Value = 1 Then
                TransferNewRotations
                chkWork(Index).Value = 0
            End If
        Case 13  'Server
            If chkWork(Index).Value = 1 Then
                CheckAftTabLoader
                chkWork(Index).Value = 0
            End If
        Case 17  'Close
            If chkWork(Index).Value = 1 Then
                If StartedAsImportTool Then
                    Me.Hide
                Else
                    ShutDownApplication
                End If
                chkWork(Index).Value = 0
            End If
        Case Else
    End Select
    If chkWork(Index).Value = 0 Then chkWork(Index).BackColor = vbButtonFace
End Sub
Private Sub HandleComboBox(LineNo As Long, ForWhat As Integer, ShowIt As Boolean)
    Dim MaxLine As Long
    Dim CurLine As Long
    Dim tmpBuff As String
    If ShowIt Then
        MaxLine = UfisServer.BasicData(4).GetLineCount - 1
        tmpBuff = ",Nothing,0" & vbLf
        For CurLine = 0 To MaxLine
            tmpBuff = tmpBuff & UfisServer.BasicData(4).GetColumnValue(CurLine, 1) & ","
            tmpBuff = tmpBuff & UfisServer.BasicData(4).GetColumnValue(CurLine, 0) & ","
            tmpBuff = tmpBuff & UfisServer.BasicData(4).GetColumnValue(CurLine, 2) & vbLf
        Next
        If MaxLine >= 0 Then
            StrTabData(0).Visible = False
            StrTabData(0).ComboResetObject "ACTM"
            StrTabData(0).CreateComboObject "ACTM", 8, 2, "EDIT", CLng(170)
            StrTabData(0).ComboSetColumnLengthString "ACTM", "31,120"
            StrTabData(0).SetComboColumn "ACTM", 8
            StrTabData(0).ComboResetContent "ACTM"
            StrTabData(0).ComboAddTextLines "ACTM", tmpBuff, vbLf
            StrTabData(0).ComboMinWidth = 10
            StrTabData(0).Visible = True
            StrTabData(0).SetCurrentSelection LineNo
        End If
    Else
        StrTabData(0).ComboShow "ACTM", False
        StrTabData(0).ComboResetObject "ACTM"
    End If
    StrTabData(0).Refresh
End Sub

Private Sub SaveStrTabData()
    Dim CurLine As Long
    Dim MaxLine As Long
    Dim StrTabLine As String
    Dim DataItem As String
    Dim LineStatus As Long
    Dim CurForeColor As Long
    Dim CurBackColor As Long
    Screen.MousePointer = 11
    MaxLine = StrTabData(0).GetLineCount - 1
    If MaxLine >= 0 Then
        For CurLine = 0 To MaxLine
            LineStatus = StrTabData(0).GetLineStatusValue(CurLine)
            StrTabData(0).GetLineColor CurLine, CurForeColor, CurBackColor
            If CedaIsConnected Then
                If LineStatus = 2 Then
                    'Inserts
                    DataItem = DecodeSsimDayFormat(StrTabData(0).GetColumnValue(CurLine, StrVpfrCol), "SSIM2", "CEDA")
                    StrTabData(0).SetColumnValue CurLine, StrVpfrCol, DataItem
                    DataItem = DecodeSsimDayFormat(StrTabData(0).GetColumnValue(CurLine, StrVptoCol), "SSIM2", "CEDA")
                    StrTabData(0).SetColumnValue CurLine, StrVptoCol, DataItem
                    StrTabData(0).SetColumnValue CurLine, StrFca3Col, Alc2ToAlc3LookUp(StrTabData(0).GetColumnValue(CurLine, StrFlcaCol))
                    StrTabData(0).SetColumnValue CurLine, StrFcd3Col, Alc2ToAlc3LookUp(StrTabData(0).GetColumnValue(CurLine, StrFlcdCol))
                    StrTabData(0).SetColumnValue CurLine, StrUrnoCol, UfisServer.UrnoPoolGetNext
                    'StrTabLine = StrTabData(0).GetLineValues(CurLine)
                    StrTabLine = StrTabData(0).GetColumnValues(CurLine, StrTabList)
                    HandleStrTabInsert StrTabLine
                    DataItem = DecodeSsimDayFormat(StrTabData(0).GetColumnValue(CurLine, StrVpfrCol), "CEDA", "SSIM2")
                    StrTabData(0).SetColumnValue CurLine, StrVpfrCol, DataItem
                    DataItem = DecodeSsimDayFormat(StrTabData(0).GetColumnValue(CurLine, StrVptoCol), "CEDA", "SSIM2")
                    StrTabData(0).SetColumnValue CurLine, StrVptoCol, DataItem
                ElseIf LineStatus = 1 Then
                    'Updates
                    DataItem = DecodeSsimDayFormat(StrTabData(0).GetColumnValue(CurLine, StrVpfrCol), "SSIM2", "CEDA")
                    StrTabData(0).SetColumnValue CurLine, StrVpfrCol, DataItem
                    DataItem = DecodeSsimDayFormat(StrTabData(0).GetColumnValue(CurLine, StrVptoCol), "SSIM2", "CEDA")
                    StrTabData(0).SetColumnValue CurLine, StrVptoCol, DataItem
                    StrTabData(0).SetColumnValue CurLine, StrFca3Col, Alc2ToAlc3LookUp(StrTabData(0).GetColumnValue(CurLine, StrFlcaCol))
                    StrTabData(0).SetColumnValue CurLine, StrFcd3Col, Alc2ToAlc3LookUp(StrTabData(0).GetColumnValue(CurLine, StrFlcdCol))
                    'StrTabLine = StrTabData(0).GetLineValues(CurLine)
                    StrTabLine = StrTabData(0).GetColumnValues(CurLine, StrTabList)
                    HandleStrTabUpdate StrTabLine, StrTabData(0).GetLineTag(CurLine)
                    DataItem = DecodeSsimDayFormat(StrTabData(0).GetColumnValue(CurLine, StrVpfrCol), "CEDA", "SSIM2")
                    StrTabData(0).SetColumnValue CurLine, StrVpfrCol, DataItem
                    DataItem = DecodeSsimDayFormat(StrTabData(0).GetColumnValue(CurLine, StrVptoCol), "CEDA", "SSIM2")
                    StrTabData(0).SetColumnValue CurLine, StrVptoCol, DataItem
                ElseIf CurBackColor = vbRed Then
                    'Deletes
                    'StrTabLine = StrTabData(0).GetLineValues(CurLine)
                    StrTabLine = StrTabData(0).GetColumnValues(CurLine, StrTabList)
                    HandleStrTabDelete StrTabLine
                End If
            Else
                'StrTabData(0).WriteToFile "c:\ufis\strtabdata.txt", False
            End If
        Next
    End If
    chkWork(0).Value = 1
    Screen.MousePointer = 0
End Sub
Private Sub HandleStrTabInsert(RecordData As String)
    UfisServer.CallCeda UserAnswer, "IRT", "STRTAB", StrTabFina, RecordData, "", "", 0, False, False
End Sub
Private Sub HandleStrTabUpdate(RecordData As String, OldValues As String)
    Dim SqlKey As String
    Dim SqlFields As String
    Dim SqlData As String
    Dim ItemData As String
    Dim CurItem As Long
    Dim ItmCnt As Long
    ItmCnt = ItemCount(StrTabFina, ",") - 1
    SqlData = ""
    SqlFields = ""
    'MsgBox RecordData & vbNewLine & OldValues
    For CurItem = 0 To ItmCnt
        ItemData = GetRealItem(RecordData, CurItem, ",")
        If ItemData <> GetRealItem(OldValues, CurItem, ",") Then
            SqlData = SqlData & "," & ItemData
            SqlFields = SqlFields & "," & GetRealItem(StrTabFina, CurItem, ",")
        End If
    Next
    If SqlFields <> "" Then
        SqlFields = Mid(SqlFields, 2)
        SqlData = Mid(SqlData, 2)
        ItemData = GetRealItem(RecordData, StrUrnoCol, ",")
        SqlKey = "WHERE URNO=" & ItemData
        UfisServer.CallCeda UserAnswer, "URT", "STRTAB", SqlFields, SqlData, SqlKey, "", 0, False, False
    End If
End Sub
Private Sub HandleStrTabDelete(RecordData As String)
    Dim ItemData As String
    Dim SqlKey As String
    ItemData = GetRealItem(RecordData, StrUrnoCol, ",")
    SqlKey = "WHERE URNO=" & ItemData
    UfisServer.CallCeda UserAnswer, "DRT", "STRTAB", "", "", SqlKey, "", 0, False, False
End Sub
Private Sub CheckSplitPanel(Index As Integer, SetEnable As Boolean)
    Dim i As Integer
    SplitPanel.Enabled = SetEnable
    For i = 1 To 2
        chkSplit(i).Enabled = SetEnable
    Next
    For i = 0 To 1
        lblSplit(i).Enabled = SetEnable
    Next
    For i = 0 To 2
        txtSplitFr(i).Enabled = SetEnable
        txtSplitTo(i).Enabled = SetEnable
        If SetEnable Then txtSplitFr(i).BackColor = vbWhite Else txtSplitFr(i).BackColor = LightGray
        If SetEnable Then txtSplitTo(i).BackColor = vbWhite Else txtSplitTo(i).BackColor = LightGray
    Next
    If SetEnable Then SplitPanel.Caption = chkWork(Index).Caption & " Parameter" Else SplitPanel.Caption = "Action Parameter"
End Sub
Private Sub CheckInsert()
    Dim CurLine As Long
    Dim BotLine As Long
    Dim NewRec As String
    Dim ScrollLine As Long
    CurLine = StrTabData(0).GetCurrentSelected
    If CurLine < 0 Then
        CurLine = StrTabData(0).GetLineCount - 1
    End If
    If CurLine >= 0 Then
        NewRec = StrTabData(0).GetLineValues(CurLine)
    Else
        CurLine = -1
        NewRec = ",,,,,,,,,,,,,0,0,,,,0,0,,,-1,-1,-1"
    End If
    CurLine = CurLine + 1
    LastAutoInsert = CurLine
    StrTabData(0).SetCurrentSelection -1
    StrTabData(0).InsertTextLineAt CurLine, NewRec, False
    StrTabData(0).SetLineStatusValue CurLine, 2
    StrTabData(0).SetLineColor CurLine, vbBlack, LightGreen
    BotLine = StrTabData(0).GetVScrollPos + (((StrTabData(0).Height / Screen.TwipsPerPixelY) / StrTabData(0).LineHeight) - 3)
    If CurLine > BotLine Then
        ScrollLine = CurLine - (BotLine - StrTabData(0).GetVScrollPos)
        StrTabData(0).OnVScrollTo ScrollLine
    End If
    StrTabData(0).Refresh
    CurInsCount = CurInsCount + 1
    CurEditLine = -1
    CheckUpdateChanges
    StrTabData(0).SetFocus
    StrTabData(0).EnableInlineEdit True
    IsInplaceEdit = True
    StrTabData(0).SetCurrentSelection CurLine
    HandleComboBox CurLine, 1, True
    StrTabData(0).SetBoolPropertyReadOnly 13, False
    StrTabData(0).SetInplaceEdit CurLine, 0, False
End Sub
Private Sub CheckDelete()
    Dim CurLine As Long
    Dim CurForeColor As Long
    Dim CurBackColor As Long
    Dim MsgTxt As String
    Dim RetVal As Integer
    Dim CurStat As Long
    CurLine = StrTabData(0).GetCurrentSelected
    If CurLine >= 0 Then
        StrTabData(0).GetLineColor CurLine, CurForeColor, CurBackColor
        If CurBackColor <> vbRed Then
            MsgTxt = "Do you want to delete this record?"
        Else
            MsgTxt = "Do you want to re-activate this record?"
        End If
        RetVal = MyMsgBox.CallAskUser(0, 0, 0, "Transaction Control", MsgTxt, "ask", "Yes;F,No", UserAnswer)
        If RetVal = 1 Then
            CurStat = StrTabData(0).GetLineStatusValue(CurLine)
            If CurBackColor <> vbRed Then
                StrTabData(0).SetColumnValue CurLine, StrRemaCol, "Deleted"
                StrTabData(0).SetLineColor CurLine, vbWhite, vbRed
                Select Case CurStat
                    Case 1  'Updates
                        CurUpdCount = CurUpdCount - 1
                    Case 2  'Inserts
                        CurInsCount = CurInsCount - 1
                    Case Else
                End Select
                CurDelCount = CurDelCount + 1
                chkWork(5).Caption = "Undo"
            Else
                StrTabData(0).SetColumnValue CurLine, StrRemaCol, GetRealItem(StrTabData(0).GetLineTag(CurLine), StrRemaCol, ",")
                Select Case CurStat
                    Case 1  'Updates
                        StrTabData(0).SetLineColor CurLine, vbBlack, vbWhite
                        CurUpdCount = CurUpdCount + 1
                    Case 2  'Inserts
                        StrTabData(0).SetLineColor CurLine, vbBlack, LightGreen
                        CurInsCount = CurInsCount + 1
                    Case Else
                        StrTabData(0).SetLineColor CurLine, vbBlack, LightGray
                End Select
                CurDelCount = CurDelCount - 1
                chkWork(5).Caption = "Delete"
            End If
            StrTabData(0).Refresh
            StrTabData(0).SetFocus
            CheckUpdateChanges
        End If
    End If
End Sub
Private Function CheckLoadPeriod() As Boolean
    Dim Result As Boolean
    Dim IsOk As Boolean
    
    Result = True
    IsOk = CheckValidDate(txtVpfr(0).Tag)
    If Not IsOk Then
        txtVpfr(0).BackColor = vbRed
        txtVpfr(0).ForeColor = vbWhite
        txtVpfr(1).BackColor = vbRed
        txtVpfr(1).ForeColor = vbWhite
        txtVpfr(2).BackColor = vbRed
        txtVpfr(2).ForeColor = vbWhite
        Result = False
    Else
        txtVpfr(0).BackColor = vbYellow
        txtVpfr(0).ForeColor = vbBlack
        txtVpfr(1).BackColor = vbYellow
        txtVpfr(1).ForeColor = vbBlack
        txtVpfr(2).BackColor = vbYellow
        txtVpfr(2).ForeColor = vbBlack
    End If
    IsOk = CheckValidDate(txtVpto(0).Tag)
    If Not IsOk Then
        txtVpto(0).BackColor = vbRed
        txtVpto(0).ForeColor = vbWhite
        txtVpto(1).BackColor = vbRed
        txtVpto(1).ForeColor = vbWhite
        txtVpto(2).BackColor = vbRed
        txtVpto(2).ForeColor = vbWhite
        Result = False
    Else
        txtVpto(0).BackColor = vbYellow
        txtVpto(0).ForeColor = vbBlack
        txtVpto(1).BackColor = vbYellow
        txtVpto(1).ForeColor = vbBlack
        txtVpto(2).BackColor = vbYellow
        txtVpto(2).ForeColor = vbBlack
    End If
    CheckLoadPeriod = Result
End Function

Private Function CheckSplitPeriod() As Boolean
    Dim Result As Boolean
    Dim IsOk As Boolean
    Result = True
    IsOk = CheckValidDate(txtSplitFr(0).Tag)
    If Not IsOk Then
        txtSplitFr(0).BackColor = vbRed
        txtSplitFr(0).ForeColor = vbWhite
        txtSplitFr(1).BackColor = vbRed
        txtSplitFr(1).ForeColor = vbWhite
        txtSplitFr(2).BackColor = vbRed
        txtSplitFr(2).ForeColor = vbWhite
        Result = False
    Else
        txtSplitFr(0).BackColor = vbWhite
        txtSplitFr(0).ForeColor = vbBlack
        txtSplitFr(1).BackColor = vbWhite
        txtSplitFr(1).ForeColor = vbBlack
        txtSplitFr(2).BackColor = vbWhite
        txtSplitFr(2).ForeColor = vbBlack
    End If
    IsOk = CheckValidDate(txtSplitTo(0).Tag)
    If Not IsOk Then
        txtSplitTo(0).BackColor = vbRed
        txtSplitTo(0).ForeColor = vbWhite
        txtSplitTo(1).BackColor = vbRed
        txtSplitTo(1).ForeColor = vbWhite
        txtSplitTo(2).BackColor = vbRed
        txtSplitTo(2).ForeColor = vbWhite
        Result = False
    Else
        txtSplitTo(0).BackColor = vbWhite
        txtSplitTo(0).ForeColor = vbBlack
        txtSplitTo(1).BackColor = vbWhite
        txtSplitTo(1).ForeColor = vbBlack
        txtSplitTo(2).BackColor = vbWhite
        txtSplitTo(2).ForeColor = vbBlack
    End If
    CheckSplitPeriod = Result
End Function

Private Sub CheckStrTabLoader()
    Dim TmpBegin As String
    Dim TmpEnd As String
    Dim tmpAlc3Filter As String
    Dim tmpAct3Filter As String
    Dim tmpGrnuFilter As String
    Dim i As Integer
    If CheckLoadPeriod = True Then
        Screen.MousePointer = 11
        StrTabData(0).ResetContent
        StrTabData(0).Refresh
        RulesFromFile = False
        If chkLoad(0).Value = 1 Then
            '
        ElseIf chkLoad(1).Value = 1 Then
            '
        ElseIf chkLoad(2).Value = 1 Then
            ReadRotationFile
        Else
            CurRotaFile = ""
            tmpAlc3Filter = ""
            tmpAct3Filter = ""
            tmpGrnuFilter = ""
            If chkFilter(0).Value = 1 Then tmpAlc3Filter = FilterTab(1).SelectDistinct(1, "'", "'", ",", True)
            If chkFilter(2).Value = 1 Then tmpAct3Filter = FilterTab(3).SelectDistinct(0, "'", "'", ",", True)
            If chkFilter(4).Value = 1 Then tmpGrnuFilter = FilterTab(5).SelectDistinct(2, "", "", ",", True)
            LoadStrTabData txtVpfr(0).Tag, txtVpto(0).Tag, tmpAlc3Filter, tmpAct3Filter, tmpGrnuFilter
        End If
        SetArrDaysRange txtVpfr(0).Tag, txtVpto(0).Tag, NormalGray, "R"
        StrTabData(0).Sort StrFredCol, False, True
        StrTabData(0).Sort StrFkyaCol, True, True
        StrTabData(0).Refresh
        CurEditLine = -1
        CurDelCount = 0
        CurInsCount = 0
        CurUpdCount = 0
        CurTtlCount = 0
        CountRul(0) = StrTabData(0).GetLineCount
        CountRul(1) = 0
        CountRul(2) = 0
        PublishCounters
        LastAutoDelete = -1
        LastAutoInsert = -1
        chkWork(0).ForeColor = vbBlack
        chkWork(0).BackColor = vbButtonFace
        chkWork(1).Enabled = False
        Screen.MousePointer = 0
    Else
        
    End If
End Sub

Private Sub LoadClientFlights()
    Dim StartTime
    Dim CurLine As Long
    Dim MaxLine As Long
    Dim ArrLineCount As Long
    Dim DepLineCount As Long
    Dim TtlLineCount As Long
    Dim DataLine As String
    Dim strFrqd As String
    Dim WeekFreq As Integer
    Dim CurCode As String
    
    Dim ArrDate As String
    Dim ArrStoa As String
    Dim ArrFlno As String
    Dim ArrAlc3 As String
    Dim ArrFlca As String
    Dim ArrFltn As String
    Dim ArrFlns As String
    Dim ArrFkey As String
    
    Dim DepDate As String
    Dim DepStod As String
    Dim DepFlno As String
    Dim DepAlc3 As String
    Dim DepFlcd As String
    Dim DepFltn As String
    Dim DepFlns As String
    Dim DepFkey As String
    
    Dim CurFtyp As String
    Dim CurAct3 As String
    
    Dim varVpfr As Variant
    Dim varVpto As Variant
    Dim varFday As Variant
    Dim strWkdy As String
    Dim ArrWkdy As String
    Dim DepWkdy As String
    Dim WkDayCnt As Integer
    Dim tmpData As String
    
    Dim varStoa As Variant
    Dim varStod As Variant
    Dim varCurStoa As Variant
    Dim varCurStod As Variant
    
    Dim IsValidWeek As Boolean
    Dim InvalidDays As Integer
    Dim TurnFlag As Integer
    Dim ArrFlt As Boolean
    Dim DepFlt As Boolean
    
    Dim SeasVpfr As String
    Dim SeasVpto As String
    Dim ArrIdx As Integer
    Dim DepIdx As Integer
    Dim OutRange As Long
    
    Dim FirstDay
    Dim LastDay
    Dim PeriodRange As Long
    Dim i As Integer
    
    Dim tmpArrFlight As String
    Dim tmpDepFlight As String
    Dim LineType As String
    Dim LineNbr As String
    
    StartTime = Timer
    Screen.MousePointer = 11
    MaxLine = FlightExtract.ExtractData.GetLineCount - 1
    
    SeasVpfr = txtVpfr(0).Tag
    SeasVpto = txtVpto(0).Tag
    If (SeasVpfr <> "") And (SeasVpto <> "") Then
        FirstDay = CedaDateToVb(SeasVpfr)
        LastDay = CedaDateToVb(SeasVpto)
        PeriodRange = DateDiff("d", FirstDay, LastDay)
    Else
        PeriodRange = -1
    End If
    If PeriodRange >= 0 Then
        Screen.MousePointer = 11
        Flights(0).ResetContent
        Flights(1).ResetContent
        Flights(0).Refresh
        Flights(1).Refresh
        StrTabData(1).ResetContent
        StrTabData(1).Refresh
        For i = 0 To 3
            CountArr(i) = 0
            CountDep(i) = 0
            ArrLineCnt(i).Caption = ""
            DepLineCnt(i).Caption = ""
        Next
        For i = 0 To 2
            CountRot(i) = 0
            RotLineCnt(i).Caption = ""
        Next
        Me.Refresh
        'SetProgressBar 0, 1, MaxLine, "Creating Flights from File ..."
        ArrLineCount = 0
        DepLineCount = 0
        TtlLineCount = 0
        
        For CurLine = 0 To MaxLine
            'If CurLine Mod 10 = 0 Then SetProgressBar 0, 2, CurLine, ""
            LineNbr = Trim(Str(CurLine))
            DataLine = FlightExtract.ExtractData.GetLineValues(CurLine)
            
            LineType = Trim(GetItem(DataLine, 2, ","))
            If (LineType <> "-") And (LineType <> "E") Then
                'No Ignore and no Error Line
                CurCode = GetItem(DataLine, 3, ",")
                
                varVpfr = CedaDateToVb(GetItem(DataLine, 8, ","))
                varVpto = CedaDateToVb(GetItem(DataLine, 9, ","))
                strFrqd = GetItem(DataLine, 10, ",")
                WeekFreq = Val(GetItem(DataLine, 22, ","))
                
                'Determine what type of rotation we got
                ArrFltn = GetItem(DataLine, 5, ",")
                If ArrFltn <> "" Then
                    ArrAlc3 = Alc2ToAlc3LookUp(GetItem(DataLine, 4, ","))
                    ArrFlt = True
                Else
                    ArrAlc3 = ""
                    ArrFlt = False
                End If
                DepFltn = GetItem(DataLine, 7, ",")
                If DepFltn <> "" Then
                    DepAlc3 = Alc2ToAlc3LookUp(GetItem(DataLine, 6, ","))
                    DepFlt = True
                Else
                    DepAlc3 = ""
                    DepFlt = False
                End If
                TurnFlag = 0
                If ArrFlt Then TurnFlag = TurnFlag + 1
                If DepFlt Then TurnFlag = TurnFlag + 2
                
                'CurRegn = GetItem(DataLine, 23, ",")
                CurFtyp = GetItem(DataLine, 24, ",")
                If CurFtyp = "" Then CurFtyp = MainDialog.DefaultFtyp
                If CurFtyp = "C" Then CurFtyp = "X"
                varStoa = CedaFullDateToVb(GetItem(DataLine, 15, ","))
                varStod = CedaFullDateToVb(GetItem(DataLine, 16, ","))
                ArrStoa = Mid(GetItem(DataLine, 15, ","), 9)
                DepStod = Mid(GetItem(DataLine, 16, ","), 9)
                ArrFlca = GetItem(DataLine, 4, ",")
                DepFlcd = GetItem(DataLine, 6, ",")
                CurAct3 = GetItem(DataLine, 11, ",")
                ArrFlns = ""
                DepFlns = ""
                ArrFlno = BuildProperAftFlno(ArrFlca, ArrFltn, ArrFlns)
                DepFlno = BuildProperAftFlno(DepFlcd, DepFltn, DepFlns)
                
                WkDayCnt = 0
                IsValidWeek = True
                InvalidDays = 0
                OutRange = 0
                
                ArrIdx = -1
                If TurnFlag = 3 Then
                    DepIdx = DateDiff("d", varStoa, varStod)
                Else
                    DepIdx = 0
                End If
                varFday = varVpfr
                Do
                    ArrIdx = ArrIdx + 1
                    strWkdy = Trim(Str(Weekday(varFday, vbMonday)))
                    If IsValidWeek And (InStr(strFrqd, strWkdy) > 0) Then
                        'Hit. Always valid for Arrivals or single Departures
                        tmpData = Trim(Str(CurLine))
                        If (CurFtyp <> "O") And (CurFtyp <> "S") Then CurCode = CurFtyp
                        
                        Select Case TurnFlag
                            Case 1  'Single arrival
                                varCurStoa = varFday
                                ArrDate = Format(varCurStoa, "yyyymmdd")
                                If (ArrDate >= SeasVpfr) And (ArrDate <= SeasVpto) Then
                                    ArrFkey = TurnAftFkey(BuildAftFkey(ArrAlc3, ArrFltn, ArrFlns, ArrDate, "A"))
                                    tmpData = CurCode + "," + ArrFlno + "," + CurAct3 + "," + ArrDate + "," + strWkdy + "," + ArrStoa + ",0,0,0,," + ArrFkey + ",-1,-1," + LineNbr + "," + ArrAlc3
                                    Flights(0).InsertTextLine tmpData, False
                                    Flights(0).SetLineTag ArrLineCount, DataLine
                                    Flights(0).SetLineColor ArrLineCount, vbBlack, vbYellow
                                    ArrLineCount = ArrLineCount + 1
                                    TtlLineCount = TtlLineCount + 1
                                    CountArr(1) = CountArr(1) + 1
                                End If
                            Case 2  'Single departure
                                varCurStod = DateAdd("d", DepIdx, varFday)
                                DepDate = Format(varCurStod, "yyyymmdd")
                                If (DepDate >= SeasVpfr) And (DepDate <= SeasVpto) Then
                                    strWkdy = Trim(Str(Weekday(varCurStod, vbMonday)))
                                    DepFkey = TurnAftFkey(BuildAftFkey(DepAlc3, DepFltn, DepFlns, DepDate, "D"))
                                    tmpData = CurCode + "," + DepFlno + "," + CurAct3 + "," + DepDate + "," + strWkdy + "," + DepStod + ",0,0,0,," + DepFkey + ",-1,-1," + LineNbr + "," + DepAlc3
                                    Flights(1).InsertTextLine tmpData, False
                                    Flights(1).SetLineTag DepLineCount, DataLine
                                    Flights(1).SetLineColor DepLineCount, vbBlack, vbGreen
                                    DepLineCount = DepLineCount + 1
                                    TtlLineCount = TtlLineCount + 1
                                    CountDep(1) = CountDep(1) + 1
                                End If
                            Case 3  'Rotation
                                ArrFkey = ""
                                DepFkey = ""
                                varCurStoa = varFday
                                ArrDate = Format(varCurStoa, "yyyymmdd")
                                If (ArrDate >= SeasVpfr) And (ArrDate <= SeasVpto) Then
                                    ArrWkdy = strWkdy
                                    ArrFkey = TurnAftFkey(BuildAftFkey(ArrAlc3, ArrFltn, ArrFlns, ArrDate, "A"))
                                End If
                                varCurStod = DateAdd("d", DepIdx, varFday)
                                DepDate = Format(varCurStod, "yyyymmdd")
                                If (DepDate >= SeasVpfr) And (DepDate <= SeasVpto) Then
                                    DepWkdy = Trim(Str(Weekday(varCurStod, vbMonday)))
                                    DepFkey = TurnAftFkey(BuildAftFkey(DepAlc3, DepFltn, DepFlns, DepDate, "D"))
                                End If
                                If (ArrFkey <> "") And (DepFkey <> "") Then
                                    tmpArrFlight = CurCode + "," + ArrFlno + "," + CurAct3 + "," + ArrDate + "," + ArrWkdy + "," + ArrStoa + ",1,0,0,," + ArrFkey + ",-1,-1," + LineNbr + "," + ArrAlc3
                                    Flights(0).InsertTextLine tmpArrFlight, False
                                    Flights(0).SetLineTag ArrLineCount, DataLine
                                    ArrLineCount = ArrLineCount + 1
                                    'TtlLineCount = TtlLineCount + 1 dont' activate
                                    CountArr(2) = CountArr(2) + 1
                                    tmpDepFlight = CurCode + "," + DepFlno + "," + CurAct3 + "," + DepDate + "," + DepWkdy + "," + DepStod + ",1,0,0,," + DepFkey + ",-1,-1," + LineNbr + "," + DepAlc3
                                    Flights(1).InsertTextLine tmpDepFlight, False
                                    Flights(1).SetLineTag DepLineCount, DataLine
                                    DepLineCount = DepLineCount + 1
                                    CountDep(2) = CountDep(2) + 1
                                    TtlLineCount = TtlLineCount + 1
                                ElseIf ArrFkey <> "" Then
                                    tmpArrFlight = CurCode + "," + ArrFlno + "," + CurAct3 + "," + ArrDate + "," + ArrWkdy + "," + ArrStoa + ",0,0,0,," + ArrFkey + ",-1,-1," + LineNbr + "," + ArrAlc3
                                    Flights(0).InsertTextLine tmpArrFlight, False
                                    Flights(0).SetLineTag ArrLineCount, DataLine
                                    Flights(0).SetLineColor ArrLineCount, vbBlack, vbYellow
                                    ArrLineCount = ArrLineCount + 1
                                    TtlLineCount = TtlLineCount + 1
                                    CountArr(1) = CountArr(1) + 1
                                ElseIf DepFkey <> "" Then
                                    tmpDepFlight = CurCode + "," + DepFlno + "," + CurAct3 + "," + DepDate + "," + DepWkdy + "," + DepStod + ",0,0,0,," + DepFkey + ",-1,-1," + LineNbr + "," + DepAlc3
                                    Flights(1).InsertTextLine tmpDepFlight, False
                                    Flights(1).SetLineTag DepLineCount, DataLine
                                    Flights(1).SetLineColor DepLineCount, vbBlack, vbYellow
                                    DepLineCount = DepLineCount + 1
                                    TtlLineCount = TtlLineCount + 1
                                    CountDep(1) = CountDep(1) + 1
                                End If
                            Case Else
                        End Select
                    End If
                    varFday = DateAdd("d", 1, varFday)
                    If Not IsValidWeek Then InvalidDays = InvalidDays - 1
                    WkDayCnt = WkDayCnt + 1
                    If WkDayCnt = 7 Then
                        If IsValidWeek Then
                            InvalidDays = (WeekFreq - 1) * 7
                            If InvalidDays > 0 Then IsValidWeek = False
                        Else
                            If InvalidDays = 0 Then
                                IsValidWeek = True
                            Else
                                'here we have a problem
                                'MsgBox "Please tell Berni that here is a problem with the weekly frequency."
                            End If
                        End If
                        WkDayCnt = 0
                    End If
                    If (IsValidWeek) And (TtlLineCount Mod 100 = 0) Then PublishCounters
                Loop While varFday <= varVpto
            End If
        Next
        CountArr(0) = ArrLineCount
        CountDep(0) = DepLineCount
        For i = 0 To 2
            ArrLineCnt(i).Caption = Trim(Str(CountArr(i)))
            DepLineCnt(i).Caption = Trim(Str(CountDep(i)))
        Next
        Flights(0).Sort "10", True, True
        Flights(0).Refresh
        Flights(1).Sort "10", True, True
        Flights(1).Refresh
        Screen.MousePointer = 0
        If OutRange > 0 Then
            MsgBox OutRange & " flights out of period"
        End If
    Else
        MsgBox "Period range out of limits"
    End If
    'CliFkeyCnt(0).Caption = Str(Flights(0).GetLineCount)
    'SetProgressBar 0, 3, 0, ""
    Screen.MousePointer = 0
    'Text1 = Timer - StartTime
    Me.Refresh
    Me.SetFocus
End Sub

Private Function TurnAftFkey(AftFkey As String) As String
    TurnAftFkey = Mid(AftFkey, 6, 3) + Left(AftFkey, 5) + Mid(AftFkey, 9)
End Function

Private Function FormatFrequency(tmpFreq As String) As String
    Dim Result As String
    Dim tmpPos As Integer
    Dim tmpChr As String
    Dim tmpVal As String
    Dim FreqType As Integer
    Dim i As Integer
    Dim l As Integer
    FreqType = 0
    'To be completed
    If InStr(tmpFreq, "0") > 0 Then
        FreqType = 1
    End If
    Result = "......."
    Select Case FreqType
        Case 0  'Type 1234567
            l = Len(tmpFreq)
            For i = 1 To l
                tmpChr = Mid(tmpFreq, i, 1)
                tmpPos = Val(tmpChr)
                If (tmpPos > 0) And (tmpPos < 8) Then Mid(Result, tmpPos, 1) = tmpChr
            Next
        Case 1  'Type 0011001
            l = Len(tmpFreq)
            If l > 7 Then l = 7
            For i = 1 To l
                tmpChr = Mid(tmpFreq, i, 1)
                If tmpChr = "1" Then Mid(Result, i, 1) = Trim(Str(i))
            Next
        Case Else
            Result = tmpFreq
    End Select
    FormatFrequency = Result
End Function

Private Sub Form_Resize()
    Dim NewHeight As Long
    Dim FullHeight As Long
    Dim HalfHeight As Long
    Dim FrameHeight As Long
    Dim VisLines As Long
    Dim NewTop As Long
    Dim i As Integer
    Dim Idx As Integer
    NewHeight = Me.ScaleHeight - StrTabData(0).Top - StatusBar.Height - 4 * Screen.TwipsPerPixelY
    If NewHeight > 3260 Then
        FullHeight = NewHeight
        If ShowStrTabStyle = 0 Then
            'Like ImportTool
            VisLines = FullHeight / (17 * Screen.TwipsPerPixelY) / 3
            HalfHeight = (VisLines * 17 * Screen.TwipsPerPixelY) + 1 * Screen.TwipsPerPixelY
            StrTabData(0).Height = HalfHeight
            HalfHeight = FullHeight - HalfHeight
            NewTop = (StrTabData(0).Top + FullHeight) - HalfHeight - 5 * Screen.TwipsPerPixelY
            Splitter(0).Top = NewTop
            Splitter(2).Top = NewTop
            NewTop = NewTop + Splitter(0).Height
            HalfHeight = HalfHeight - Splitter(0).Height + 4 * Screen.TwipsPerPixelY
            StrTabData(1).Top = NewTop
            StrTabData(1).Height = HalfHeight + Screen.TwipsPerPixelY
            VisLines = FullHeight / (17 * Screen.TwipsPerPixelY) / 2
            HalfHeight = VisLines * 17 * Screen.TwipsPerPixelY + 1 * Screen.TwipsPerPixelY
            Flights(0).Height = HalfHeight
            HalfHeight = FullHeight - HalfHeight
            NewTop = (StrTabData(0).Top + FullHeight) - HalfHeight - 5 * Screen.TwipsPerPixelY
            Splitter(1).Top = NewTop
            NewTop = NewTop + Splitter(1).Height
            HalfHeight = HalfHeight - Splitter(1).Height + 5 * Screen.TwipsPerPixelY
            Flights(1).Top = NewTop
            Flights(1).Height = HalfHeight
        End If
        If ShowStrTabStyle = 1 Then
            'Like StrtabBrowser
            StrTabData(0).Height = FullHeight
            NewHeight = NewHeight + 8 * Screen.TwipsPerPixelY
            FrameHeight = NewHeight / 2
            NewHeight = NewHeight - FrameHeight
            For i = 0 To 6 Step 2
                FilterFrame(i).Height = FrameHeight
                FilterPanel(i).Height = FrameHeight
                ButtonPanel(i).Top = FrameHeight - ButtonPanel(i).Height - 7 * Screen.TwipsPerPixelY
                FilterTab(i).Height = FrameHeight - FilterTab(i).Top - ButtonPanel(i).Height - 12 * Screen.TwipsPerPixelY
                Idx = i + 1
                FilterTab(Idx).Height = FrameHeight - FilterTab(Idx).Top - ButtonPanel(i).Height - 12 * Screen.TwipsPerPixelY
                FrameHeight = NewHeight
            Next
            NewTop = (StrTabData(0).Top + StrTabData(0).Height) - FrameHeight
            FilterFrame(2).Top = NewTop
            FilterFrame(6).Top = NewTop
        End If
    End If
End Sub

Private Sub MonthCover_MouseMove(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
    AdjustTimeScaleColors -1, ArrDays(0)
End Sub

Private Sub MonthLabel_MouseMove(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
    TimeScaleInfo(0).Text = GetItem(MonthLabel(Index).Tag, 2, ",")
    TimeScaleInfo(1).Text = GetItem(MonthLabel(Index).Tag, 3, ",")
    TimeScaleInfo(4).Text = GetItem(MonthLabel(Index).Tag, 4, ",")
End Sub

Private Sub SplitAnchor_MouseDown(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
    SplitAnchor(Index).Tag = Y
End Sub

Private Sub SplitAnchor_MouseMove(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
    Dim DiffY As Long
    Dim TopHeight As Long
    Dim BottomHeight As Long
    If (SplitAnchor(Index).Tag > 0) And (Y <> SplitAnchor(Index).Tag) Then
        DiffY = Y - SplitAnchor(Index).Tag
        If Index = 0 Then
            TopHeight = StrTabData(0).Height + DiffY
            BottomHeight = StrTabData(1).Height - DiffY
            If (TopHeight > 15) And (BottomHeight > 15) Then
                StrTabData(0).Height = StrTabData(0).Height + DiffY
                Splitter(0).Top = Splitter(0).Top + DiffY
                Splitter(2).Top = Splitter(2).Top + DiffY
                StrTabData(1).Top = StrTabData(1).Top + DiffY
                StrTabData(1).Height = StrTabData(1).Height - DiffY
            End If
        Else
            TopHeight = Flights(0).Height + DiffY
            BottomHeight = Flights(1).Height - DiffY
            If (TopHeight > 15) And (BottomHeight > 15) Then
                Flights(0).Height = Flights(0).Height + DiffY
                Splitter(1).Top = Splitter(1).Top + DiffY
                Flights(1).Top = Flights(1).Top + DiffY
                Flights(1).Height = Flights(1).Height - DiffY
            End If
        End If
        Me.Refresh
    End If
End Sub

Private Sub SplitAnchor_MouseUp(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
    SplitAnchor(Index).Tag = -1
End Sub

Private Sub StrTabData_BoolPropertyChanged(Index As Integer, ByVal LineNo As Long, ByVal ColNo As Long, ByVal NewValue As Boolean)
    If CurEditLine <> LineNo Then CheckUpdateChanges
    CurEditLine = LineNo
    CurEditColNo = ColNo
    CheckUpdateChanges
    CurEditLine = LineNo
    CurEditColNo = ColNo
End Sub

Private Sub StrTabData_CloseInplaceEdit(Index As Integer, ByVal LineNo As Long, ByVal ColNo As Long)
    If Index = 0 Then
        CheckUpdateChanges
        chkWork(3).Value = 0
        chkWork(4).Value = 0
    End If
End Sub

Private Sub StrTabData_ComboSelChanged(Index As Integer, ByVal LineNo As Long, ByVal ColNo As Long, ByVal OldValue As String, ByVal NewValues As String)
    If CurEditLine <> LineNo Then
        StrTabData(0).SetColumnValue LineNo, 18, GetRealItem(NewValues, 2, ",")
        CheckUpdateChanges
    End If
    CurEditLine = LineNo
    CurEditColNo = ColNo
    CheckUpdateChanges
    CurEditLine = LineNo
    CurEditColNo = ColNo
End Sub

Private Sub StrTabData_EditPositionChanged(Index As Integer, ByVal LineNo As Long, ByVal ColNo As Long, ByVal Value As String)
    If CurEditLine <> LineNo Then CheckUpdateChanges
    CurEditLine = LineNo
    CurEditColNo = ColNo
End Sub

Private Sub StrTabData_InplaceEditCell(Index As Integer, ByVal LineNo As Long, ByVal ColNo As Long, ByVal NewValue As String, ByVal OldValue As String)
    Select Case ColNo
        Case StrFredCol
            If OldValue <> NewValue Then StrTabData(0).SetColumnValue LineNo, StrFredCol, FormatFrequency(NewValue)
        Case Else
    End Select
    If OldValue <> NewValue Then CheckUpdateChanges
    CurEditLine = LineNo
End Sub

Private Sub chkSplit_Click(Index As Integer)
    If chkSplit(Index).Value = 1 Then chkSplit(Index).BackColor = LightGreen
    Select Case Index
        Case 1
            If chkSplit(Index).Value = 1 Then
                chkSplit(1).Value = 0
                chkSplit(Index).Value = 0
            End If
        Case 2
            If chkSplit(Index).Value = 1 Then
                chkSplit(0).Value = 0
                chkSplit(Index).Value = 0
            End If
        Case Else
            chkSplit(Index).Value = 0
    End Select
    If chkSplit(Index).Value = 0 Then chkSplit(Index).BackColor = vbButtonFace
End Sub

Private Sub Flights_OnHScroll(Index As Integer, ByVal ColNo As Long)
    If Index = 1 Then Flights(0).OnHScrollTo ColNo
End Sub

Private Sub StrTabData_LostFocus(Index As Integer)
'    Dim LineNo As Long
'    Dim ColNo As Long
'    If Index = 0 Then
'        If (CurEditLine >= 0) And (CurEditColNo >= 0) Then
'            CheckUpdateChanges
'        End If
'    End If
'    If CurEditLine >= 0 Then
'        StrTabData(0).SetCurrentSelection CurEditLine
'        StrTabData(0).Refresh
'    End If
End Sub

Private Sub StrTabData_OnHScroll(Index As Integer, ByVal ColNo As Long)
    If Index = 1 Then StrTabData(0).OnHScrollTo ColNo
End Sub

Private Sub StrTabData_RowSelectionChanged(Index As Integer, ByVal LineNo As Long, ByVal Selected As Boolean)
    Static lastLineNo As Long
    Dim CurLine As Long
    Dim tmpData As String
    Dim tmpVpfr As String
    Dim tmpVpto As String
    Dim CurForeColor As Long
    Dim CurBackColor As Long
    If Not CreateIsBusy Then
        If LineNo >= 0 Then
            If Selected Then
                If (LastAutoDelete >= 0) And (LineNo > LastAutoDelete) Then
                    StrTabData(Index).SetCurrentSelection LastAutoDelete
                    If IsInplaceEdit Then StrTabData(Index).SetInplaceEdit LastAutoDelete, CurEditColNo, False
                    LastAutoDelete = -1
                    LastAutoInsert = -1
                Else
                    If Index = 0 Then
                        If (chkWork(3).Value = 1) And (LastAutoInsert >= 0) Then
                            If LineNo = (LastAutoInsert + 1) Then
                                LastAutoInsert = -1
                                CheckInsert
                            End If
                        Else
                            If (LineNo <> lastLineNo) Or (LineNo = 0) Then
                                'Set TimeScale Info
                                tmpData = StrTabData(Index).GetColumnValue(LineNo, StrFlcaCol)
                                tmpData = tmpData & StrTabData(Index).GetColumnValue(LineNo, StrFlnaCol)
                                tmpData = tmpData & StrTabData(Index).GetColumnValue(LineNo, StrFlsaCol)
                                TimeScaleInfo(2).Text = tmpData
                                tmpData = StrTabData(Index).GetColumnValue(LineNo, StrFlcdCol)
                                tmpData = tmpData & StrTabData(Index).GetColumnValue(LineNo, StrFlndCol)
                                tmpData = tmpData & StrTabData(Index).GetColumnValue(LineNo, StrFlsdCol)
                                TimeScaleInfo(3).Text = tmpData
                                'Set Split/Join Action Range
                                tmpVpfr = StrTabData(Index).GetColumnValue(LineNo, StrVpfrCol)
                                tmpVpfr = DecodeSsimDayFormat(tmpVpfr, "SSIM2", "CEDA")
                                txtSplitFr(0) = Left(tmpVpfr, 4)
                                txtSplitFr(1) = Mid(tmpVpfr, 5, 2)
                                txtSplitFr(2) = Right(tmpVpfr, 2)
                                tmpVpto = StrTabData(Index).GetColumnValue(LineNo, StrVptoCol)
                                tmpVpto = DecodeSsimDayFormat(tmpVpto, "SSIM2", "CEDA")
                                txtSplitTo(0) = Left(tmpVpto, 4)
                                txtSplitTo(1) = Mid(tmpVpto, 5, 2)
                                txtSplitTo(2) = Right(tmpVpto, 2)
                                'Check Edit Status
                                StrTabData(Index).GetLineColor LineNo, CurForeColor, CurBackColor
                                If (chkWork(3).Value = 1) Or (chkWork(4).Value = 1) Then
                                    If CurBackColor = vbRed Then
                                        StrTabData(Index).EnableInlineEdit False
                                    Else
                                        StrTabData(Index).EnableInlineEdit True
                                    End If
                                End If
                                'Check Undo Caption
                                If CurBackColor = vbRed Then chkWork(5).Caption = "Undo" Else chkWork(5).Caption = "Delete"
                                'Adjust ACT Group Combo
                                'If Trim(StrTabData(Index).GetColumnValue(LineNo, StrAcgnCol)) = "" Then
                                '    StrTabData(Index).ComboShow "ACTM", False
                                'Else
                                '    StrTabData(Index).ComboShow "ACTM", True
                                'End If
                                lastLineNo = LineNo
                            End If
                            If Not StopNestedCalls Then
                                'Show Calendar
                                'ResetRangeInfoObjects False, ArrDisp, DepDisp
                                'SetFlightRange StrTabData(Index).GetLineValues(LineNo), ArrDays, DepDays, vbYellow, vbGreen, True
                                'Show Valid Rule Flights and Rotations
                                AdjustLineMarkers StrTabData(0), 1, LineNo, False
                                If chkLoad(2).Value = 1 Then
                                    If Not NoEcho Then
                                        tmpData = StrTabData(0).GetColumnValue(LineNo, StrUrnoCol)
                                        CurLine = Val(tmpData) - CurRotaUrno
                                        LinkFile.NoEcho = True
                                        LinkFile.FileData.OnVScrollTo CurLine - 3
                                        LinkFile.FileData.SetCurrentSelection CurLine
                                        LinkFile.NoEcho = False
                                    End If
                                End If
                            End If
                        End If
                    Else
                        If Not StopNestedCalls Then
                            AdjustLineMarkers StrTabData(1), 4, LineNo, False
                        End If
                    End If
                End If
            Else
                If CurEditLine >= 0 Then CheckUpdateChanges
                CheckDeleteEmptyLine Index, LineNo
            End If
        End If
    End If
End Sub

Private Sub CheckDeleteEmptyLine(Index As Integer, LineNo As Long)
    Dim CurLineStat As Long
    If IsEmptyItemList(StrTabData(Index).GetLineValues(LineNo), ",", "0000") Then
        StrTabData(Index).EnableInlineEdit False
        CurLineStat = StrTabData(Index).GetLineStatusValue(LineNo)
        StrTabData(Index).DeleteLine LineNo
        StrTabData(Index).Refresh
        LastAutoDelete = LineNo
        CurEditLine = -1
        Select Case CurLineStat
            Case 1
                CurUpdCount = CurUpdCount - 1
            Case 2
                CurInsCount = CurInsCount - 1
            Case Else
        End Select
        CheckUpdateChanges
        If IsInplaceEdit Then StrTabData(Index).EnableInlineEdit True
    End If
End Sub
Private Sub CheckUpdateChanges()
    Dim TagItems As String
    Dim IsChanged As Boolean
    Dim CurItm As Long
    Dim CurForeColor As Long
    Dim CurBackColor As Long
    Dim CurStat As Long
    If Not RulesFromFile Then
        If CurEditLine >= 0 Then
            CurStat = StrTabData(0).GetLineStatusValue(CurEditLine)
            If CurStat <> 2 Then
                TagItems = StrTabData(0).GetLineTag(CurEditLine)
                IsChanged = False
                StrTabData(0).ResetLineDecorations CurEditLine
                For CurItm = 0 To 15
                    If StrTabData(0).GetColumnValue(CurEditLine, CurItm) <> GetRealItem(TagItems, CurItm, ",") Then
                        StrTabData(0).SetDecorationObject CurEditLine, CurItm, "CellDiff"
                        IsChanged = True
                    End If
                Next
                StrTabData(0).GetLineColor CurEditLine, CurForeColor, CurBackColor
                If IsChanged Then
                    If CurStat <> 1 Then
                        StrTabData(0).SetLineStatusValue CurEditLine, 1
                        If CurBackColor <> vbRed Then
                            CurUpdCount = CurUpdCount + 1
                            StrTabData(0).SetLineColor CurEditLine, vbBlack, vbWhite
                        End If
                    End If
                Else
                    If CurStat = 1 Then
                        StrTabData(0).SetLineStatusValue CurEditLine, 0
                        If CurBackColor <> vbRed Then
                            CurUpdCount = CurUpdCount - 1
                            StrTabData(0).SetLineColor CurEditLine, vbBlack, LightGray
                        End If
                    End If
                End If
            End If
            CurEditLine = -1
            StrTabData(0).Refresh
        End If
        CurTtlCount = CurInsCount + CurUpdCount + CurDelCount
        'Text1.Text = Str(CurTtlCount)
        If CurTtlCount > 0 Then
            chkWork(1).Enabled = True
        Else
            chkWork(1).Enabled = False
        End If
    End If
End Sub

Private Sub StrTabData_SendLButtonClick(Index As Integer, ByVal LineNo As Long, ByVal ColNo As Long)
    Dim ArrRec As String
    Dim DepRec As String
    Dim NewRec As String
    Dim ArrLineNo As Long
    Dim DepLineNo As Long
    Dim ArrCedaDate As String
    Dim ArrTagItems As String
    Dim DepCedaDate As String
    Dim DepTagItems As String
    Dim CurAct3List As String
    Dim CurLineStat As Long
    Dim NewLineStat As Long
    If chkStrSplitter(2).Value = 1 Then
        If chkStrSplitter(6).Value = 0 Then
            Select Case Index
                Case 0  'Standard Rotation
                    If chkStrSplitter(3).Value = 1 Then
                    End If
                Case 1  'New Rotation
                    If chkStrSplitter(4).Value = 1 Then
                        If (DragLine >= 0) And (DragIndex = 0) Then
                            'Dragged Arrival
                            If (ColNo >= 4) And (ColNo <= 6) Then
                                If LineNo = DropLine Then
                                    ArrLineNo = DragLine
                                    ArrTagItems = Flights(0).GetLineTag(ArrLineNo)
                                    DepRec = ""
                                    DepTagItems = ""
                                    DepLineNo = -1
                                    CurLineStat = StrTabData(Index).GetLineStatusValue(LineNo)
                                    Select Case CurLineStat
                                        Case 9  'Empty Dropline
                                            NewLineStat = 1
                                        Case 1  'Single Arrival
                                            NewLineStat = -1
                                        Case 2  'Single Departure
                                            NewLineStat = -1
                                            DepLineNo = Val(StrTabData(Index).GetColumnValue(LineNo, 23))
                                            If DepLineNo >= 0 Then
                                                DepRec = Flights(1).GetLineValues(DepLineNo)
                                                ArrCedaDate = Flights(0).GetColumnValue(ArrLineNo, 3)
                                                DepCedaDate = Flights(1).GetColumnValue(DepLineNo, 3)
                                                CurAct3List = Flights(0).GetColumnValue(ArrLineNo, 2)
                                                DepTagItems = Flights(1).GetLineTag(DepLineNo)
                                                If IsValidFlightPair(ArrCedaDate, ArrTagItems, DepCedaDate, DepTagItems, CurAct3List) Then NewLineStat = 3
                                            End If
                                        Case 3  'Manual Rotation
                                            NewLineStat = -1
                                        Case Else
                                            NewLineStat = -1
                                    End Select
                                    If NewLineStat > 0 Then
                                        ArrRec = Flights(DragIndex).GetLineValues(DragLine)
                                        NewRec = BuildStrRotationFromFlights(ArrRec, DepRec)
                                        AdjustDropMarker StrTabData(Index), LineNo, 4, 6, "", False, True
                                        Select Case NewLineStat
                                            Case 1
                                                StrTabData(Index).InsertTextLineAt LineNo, NewRec, False
                                                StrTabData(Index).SetLineColor LineNo, vbBlack, vbYellow
                                                CountRot(1) = CountRot(1) + 1
                                            Case 2
                                                StrTabData(Index).InsertTextLineAt LineNo, NewRec, False
                                                StrTabData(Index).SetLineColor LineNo, vbBlack, vbGreen
                                                CountRot(2) = CountRot(2) + 1
                                            Case 3
                                                StrTabData(Index).UpdateTextLine LineNo, NewRec, False
                                                StrTabData(Index).SetLineColor LineNo, vbBlack, vbWhite
                                                Flights(0).SetColumnValue ArrLineNo, 7, "1"
                                                Flights(1).SetColumnValue DepLineNo, 7, "1"
                                                Flights(0).SetLineColor ArrLineNo, vbBlack, vbWhite
                                                Flights(1).SetLineColor DepLineNo, vbBlack, vbWhite
                                                CountArr(1) = CountArr(1) - 1
                                                CountArr(2) = CountArr(2) + 1
                                                CountDep(1) = CountDep(1) - 1
                                                CountDep(2) = CountDep(2) + 1
                                                CountRot(0) = CountRot(0) + 1
                                                CountRot(2) = CountRot(2) - 1
                                                Flights(0).Refresh
                                                Flights(1).Refresh
                                            Case Else
                                        End Select
                                        StrTabData(Index).SetColumnValue LineNo, 13, "1"
                                        StrTabData(Index).SetLineTag LineNo, ArrTagItems & vbLf & DepTagItems
                                        StrTabData(Index).SetLineStatusValue LineNo, NewLineStat
                                        StrTabData(Index).SetColumnValue LineNo, 22, Trim(Str(ArrLineNo))
                                        StrTabData(Index).SetColumnValue LineNo, 23, Trim(Str(DepLineNo))
                                        Flights(DragIndex).SetColumnValue DragLine, 7, "1"
                                        Flights(DragIndex).SetColumnValue DragLine, 12, "M" & Trim(Str(LineNo))
                                        Flights(DragIndex).Refresh
                                        chkStrSplitter(2).BackColor = vbButtonFace
                                        AdjustDropMarker Flights(DragIndex), DragLine, 1, 5, "", False, False
                                        DragLine = -1
                                        DropLine = StrTabData(Index).GetLineCount - 1
                                        StrTabData(Index).Refresh
                                        PublishCounters
                                    End If
                                End If
                            End If
                        End If
                        If (DragLine >= 0) And (DragIndex = 1) Then
                            'Dragged Departure
                            If (ColNo >= 9) And (ColNo <= 12) Then
                                If LineNo = DropLine Then
                                    ArrRec = ""
                                    ArrTagItems = ""
                                    ArrLineNo = -1
                                    DepLineNo = DragLine
                                    DepTagItems = Flights(1).GetLineTag(DepLineNo)
                                    CurLineStat = StrTabData(Index).GetLineStatusValue(LineNo)
                                    Select Case CurLineStat
                                        Case 9  'Empty Dropline
                                            NewLineStat = 2
                                        Case 1  'Single Arrival
                                            NewLineStat = -1
                                            ArrLineNo = Val(StrTabData(Index).GetColumnValue(LineNo, 22))
                                            If ArrLineNo >= 0 Then
                                                ArrRec = Flights(0).GetLineValues(ArrLineNo)
                                                ArrCedaDate = Flights(0).GetColumnValue(ArrLineNo, 3)
                                                DepCedaDate = Flights(1).GetColumnValue(DepLineNo, 3)
                                                CurAct3List = Flights(0).GetColumnValue(ArrLineNo, 2)
                                                ArrTagItems = Flights(0).GetLineTag(ArrLineNo)
                                                If IsValidFlightPair(ArrCedaDate, ArrTagItems, DepCedaDate, DepTagItems, CurAct3List) Then NewLineStat = 3
                                            End If
                                        Case 2  'Single Departure
                                            NewLineStat = -1
                                        Case 3  'Manual Rotation
                                            NewLineStat = -1
                                        Case Else
                                            NewLineStat = -1
                                    End Select
                                    If NewLineStat > 0 Then
                                        DepRec = Flights(DragIndex).GetLineValues(DragLine)
                                        NewRec = BuildStrRotationFromFlights(ArrRec, DepRec)
                                        AdjustDropMarker StrTabData(Index), LineNo, 9, 12, "", False, True
                                        Select Case NewLineStat
                                            Case 1
                                                StrTabData(Index).InsertTextLineAt LineNo, NewRec, False
                                                StrTabData(Index).SetLineColor LineNo, vbBlack, vbYellow
                                                CountRot(1) = CountRot(1) + 1
                                            Case 2
                                                StrTabData(Index).InsertTextLineAt LineNo, NewRec, False
                                                StrTabData(Index).SetLineColor LineNo, vbBlack, vbGreen
                                                CountRot(2) = CountRot(2) + 1
                                            Case 3
                                                StrTabData(Index).UpdateTextLine LineNo, NewRec, False
                                                StrTabData(Index).SetLineColor LineNo, vbBlack, vbWhite
                                                Flights(0).SetColumnValue ArrLineNo, 7, "1"
                                                Flights(1).SetColumnValue DepLineNo, 7, "1"
                                                Flights(0).SetLineColor ArrLineNo, vbBlack, vbWhite
                                                Flights(1).SetLineColor DepLineNo, vbBlack, vbWhite
                                                CountArr(1) = CountArr(1) - 1
                                                CountArr(2) = CountArr(2) + 1
                                                CountDep(1) = CountDep(1) - 1
                                                CountDep(2) = CountDep(2) + 1
                                                CountRot(0) = CountRot(0) + 1
                                                CountRot(1) = CountRot(1) - 1
                                                Flights(0).Refresh
                                                Flights(1).Refresh
                                            Case Else
                                        End Select
                                        StrTabData(Index).SetLineTag LineNo, ArrTagItems & vbLf & DepTagItems
                                        StrTabData(Index).SetLineStatusValue LineNo, NewLineStat
                                        StrTabData(Index).SetColumnValue LineNo, 22, Trim(Str(ArrLineNo))
                                        StrTabData(Index).SetColumnValue LineNo, 23, Trim(Str(DepLineNo))
                                        StrTabData(Index).SetColumnValue LineNo, 13, "1"
                                        Flights(DragIndex).SetColumnValue DragLine, 7, "1"
                                        Flights(DragIndex).SetColumnValue DragLine, 12, "M" & Trim(Str(LineNo))
                                        Flights(DragIndex).Refresh
                                        chkStrSplitter(2).BackColor = vbButtonFace
                                        AdjustDropMarker Flights(DragIndex), DragLine, 1, 5, "", False, False
                                        DragLine = -1
                                        DropLine = StrTabData(Index).GetLineCount - 1
                                        StrTabData(Index).Refresh
                                        PublishCounters
                                    End If
                                End If
                            End If
                        End If
                    End If
                Case Else
            End Select
        End If
    End If
End Sub

Private Sub StrTabData_SendLButtonDblClick(Index As Integer, ByVal LineNo As Long, ByVal ColNo As Long)
    Dim ArrLineNo As Long
    Dim DepLineNo As Long
    Dim CurLineStat As Long
    If LineNo < 0 Then
        'StrTabData(0).Sort ColNo, True, True
        'StrTabData(1).Sort ColNo, True, True
        'StrTabData(0).Refresh
        'StrTabData(1).Refresh
    Else
        If chkStrSplitter(6).Value = 1 Then
            If Index = 1 Then
                If StrTabData(1).GetColumnValue(LineNo, 14) = "0" Then
                    If StrTabData(1).GetColumnValue(LineNo, 13) = "1" Then
                        'Undo of manually built rotations
                        ArrLineNo = Val(StrTabData(Index).GetColumnValue(LineNo, 22))
                        If ArrLineNo >= 0 Then
                            Flights(0).SetLineColor ArrLineNo, vbBlack, vbYellow
                            Flights(0).SetColumnValue ArrLineNo, 7, "0"
                            Flights(0).Refresh
                        End If
                        DepLineNo = Val(StrTabData(Index).GetColumnValue(LineNo, 23))
                        If DepLineNo >= 0 Then
                            Flights(1).SetLineColor DepLineNo, vbBlack, vbGreen
                            Flights(1).SetColumnValue DepLineNo, 7, "0"
                            Flights(1).Refresh
                        End If
                        CurLineStat = StrTabData(Index).GetLineStatusValue(LineNo)
                        Select Case CurLineStat
                            Case 1
                                CountRot(1) = CountRot(1) - 1
                            Case 2
                                CountRot(2) = CountRot(2) - 1
                            Case 3
                                CountArr(1) = CountArr(1) + 1
                                CountArr(2) = CountArr(2) - 1
                                CountDep(1) = CountDep(1) + 1
                                CountDep(2) = CountDep(2) - 1
                                CountRot(0) = CountRot(0) - 1
                            Case Else
                        End Select
                        If CurLineStat > 0 Then PublishCounters
                        StrTabData(1).DeleteLine LineNo
                        StrTabData(1).Refresh
                    End If
                End If
            End If
        End If
    End If
End Sub

Private Sub StrTabData_SendMouseMove(Index As Integer, ByVal LineNo As Long, ByVal ColNo As Long, ByVal Flags As String)
    Dim CurLineStat As Long
    Dim ArrLineNo As Long
    Dim DepLineNo As Long
    Dim ArrCedaDate As String
    Dim ArrTagItems As String
    Dim DepCedaDate As String
    Dim DepTagItems As String
    Dim CurAct3List As String
    If (DropIndex >= 0) And (DragIndex >= 0) And (DragLine >= 0) Then
        If DropIndex = Index Then
            If DragIndex = 0 Then
                'Arrival
                If LineNo = DropLine Then
                    If Not DropMarkerIsSet Then AdjustDropMarker StrTabData(Index), LineNo, 4, 6, "ArrMarker", True, True
                Else
                    If DropMarkerIsSet Then AdjustDropMarker StrTabData(Index), DropLine, 4, 6, "", False, True
                    CurLineStat = StrTabData(Index).GetLineStatusValue(LineNo)
                    Select Case CurLineStat
                        Case 9  'Free DropLine
                            DropLine = LineNo
                            AdjustDropMarker StrTabData(Index), LineNo, 4, 6, "ArrMarker", True, True
                        Case 1  'Single Arrival
                        Case 2  'Single Departure
                            DepLineNo = Val(StrTabData(Index).GetColumnValue(LineNo, 23))
                            If DepLineNo >= 0 Then
                                ArrCedaDate = Flights(0).GetColumnValue(DragLine, 3)
                                DepCedaDate = Flights(1).GetColumnValue(DepLineNo, 3)
                                CurAct3List = Flights(0).GetColumnValue(DragLine, 2)
                                ArrTagItems = Flights(0).GetLineTag(DragLine)
                                DepTagItems = Flights(1).GetLineTag(DepLineNo)
                                If IsValidFlightPair(ArrCedaDate, ArrTagItems, DepCedaDate, DepTagItems, CurAct3List) Then
                                    DropLine = LineNo
                                    AdjustDropMarker StrTabData(Index), LineNo, 4, 6, "ArrMarker", True, True
                                End If
                            End If
                        Case 3  'Manual Rotation
                        Case Else
                    End Select
                End If
            End If
            If DragIndex = 1 Then
                'Departure
                If LineNo = DropLine Then
                    If Not DropMarkerIsSet Then AdjustDropMarker StrTabData(Index), LineNo, 9, 12, "DepMarker", True, True
                Else
                    If DropMarkerIsSet Then AdjustDropMarker StrTabData(Index), DropLine, 9, 12, "", False, True
                    CurLineStat = StrTabData(Index).GetLineStatusValue(LineNo)
                    Select Case CurLineStat
                        Case 9  'Free DropLine
                            DropLine = LineNo
                            AdjustDropMarker StrTabData(Index), LineNo, 9, 12, "DepMarker", True, True
                        Case 1  'Single Arrival
                            ArrLineNo = Val(StrTabData(Index).GetColumnValue(LineNo, 22))
                            If ArrLineNo >= 0 Then
                                ArrCedaDate = Flights(0).GetColumnValue(ArrLineNo, 3)
                                DepCedaDate = Flights(1).GetColumnValue(DragLine, 3)
                                CurAct3List = Flights(0).GetColumnValue(ArrLineNo, 2)
                                ArrTagItems = Flights(0).GetLineTag(ArrLineNo)
                                DepTagItems = Flights(1).GetLineTag(DragLine)
                                If IsValidFlightPair(ArrCedaDate, ArrTagItems, DepCedaDate, DepTagItems, CurAct3List) Then
                                    DropLine = LineNo
                                    AdjustDropMarker StrTabData(Index), LineNo, 9, 12, "DepMarker", True, True
                                End If
                            End If
                        Case 2  'Single Departure
                        Case 3  'Manual Rotation
                        Case Else
                    End Select
                End If
            End If
        End If
    End If
End Sub
Private Sub AdjustDropMarker(CurTab As TABLib.Tab, LineNo As Long, FirstCol As Long, LastCol As Long, UseMarker As String, ShowIt As Boolean, KeepMarker As Boolean)
    Dim CurCol As Long
    Dim FromCol As Long
    Dim ToCol As Long
    FromCol = FirstCol + 1
    ToCol = LastCol - 1
    If ShowIt Then
        If LineNo >= 0 Then
            CurTab.SetDecorationObject LineNo, FirstCol, UseMarker & "1"
            For CurCol = FromCol To ToCol
                CurTab.SetDecorationObject LineNo, CurCol, UseMarker & "2"
            Next
            CurTab.SetDecorationObject LineNo, LastCol, UseMarker & "3"
            CurTab.Refresh
        End If
    Else
        If LineNo >= 0 Then
            CurTab.ResetCellDecoration LineNo, FirstCol
            For CurCol = FromCol To ToCol
                CurTab.ResetCellDecoration LineNo, CurCol
            Next
            CurTab.ResetCellDecoration LineNo, LastCol
            CurTab.Refresh
        End If
    End If
    If KeepMarker Then DropMarkerIsSet = ShowIt
End Sub
Private Sub StrTabData_SendRButtonClick(Index As Integer, ByVal LineNo As Long, ByVal ColNo As Long)
    Dim CurOldValue As String
    Dim CurNewValue As String
    Dim MsgTxt As String
    Dim RetVal As Integer
    If LineNo >= 0 Then
        If Index = 0 Then
            If Not RulesFromFile Then
                CurOldValue = GetRealItem(StrTabData(Index).GetLineTag(LineNo), ColNo, ",")
                CurNewValue = StrTabData(Index).GetColumnValue(LineNo, ColNo)
                If CurOldValue <> CurNewValue Then
                    MsgTxt = "Do you want to undo your changes?"
                    RetVal = MyMsgBox.CallAskUser(0, 0, 0, "Transaction Control", MsgTxt, "ask", "Yes;F,All,No", UserAnswer)
                    If RetVal = 1 Then
                        StrTabData(Index).SetColumnValue LineNo, ColNo, CurOldValue
                        CurEditLine = LineNo
                        CheckUpdateChanges
                    ElseIf RetVal = 2 Then
                        StrTabData(Index).UpdateTextLine LineNo, StrTabData(Index).GetLineTag(LineNo), False
                        CurEditLine = LineNo
                        CheckUpdateChanges
                    End If
                End If
            End If
        End If
    End If
End Sub

Private Sub TimeScalePanel_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    AdjustTimeScaleColors -1, ArrDays(0)
End Sub

Private Sub ArrDays_MouseMove(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
    AdjustTimeScaleColors Index, ArrDays(Index)
End Sub
Private Sub AdjustTimeScaleColors(Index As Integer, CurLabel)
    If Index <> LastArrDayIndex Then
        If LastArrDayIndex >= 0 Then
            ArrDays(LastArrDayIndex).BackColor = LastArrDayColor
            DepDays(LastArrDayIndex).BackColor = LastDepDayColor
            ArrDisp(LastArrDayIndex).BackColor = LastArrDispColor
            DepDisp(LastArrDayIndex).BackColor = LastDepDispColor
            If Index < 0 Then
                TimeScaleInfo(0).Text = ""
                TimeScaleInfo(1).Text = ""
                TimeScaleInfo(4).Text = ""
            End If
        End If
        If Index >= 0 Then
            If ArrDays(Index).BackColor <> vbMagenta Then LastArrDayColor = ArrDays(Index).BackColor
            If DepDays(Index).BackColor <> vbMagenta Then LastDepDayColor = DepDays(Index).BackColor
            If ArrDisp(Index).BackColor <> vbMagenta Then LastArrDispColor = ArrDisp(Index).BackColor
            If DepDisp(Index).BackColor <> vbMagenta Then LastDepDispColor = DepDisp(Index).BackColor
            ArrDays(Index).BackColor = vbMagenta
            DepDays(Index).BackColor = vbMagenta
            ArrDisp(Index).BackColor = vbMagenta
            DepDisp(Index).BackColor = vbMagenta
        End If
        LastArrDayIndex = Index
    End If
    If Index >= 0 Then
        TimeScaleInfo(0).Text = GetItem(CurLabel.Tag, 2, ",")
        TimeScaleInfo(1).Text = GetItem(CurLabel.Tag, 3, ",")
        TimeScaleInfo(4).Text = GetItem(CurLabel.Tag, 4, ",")
    End If
    If TimeScaleInfo(1).Text = "" Then TimeScaleInfo(0).Text = ""
End Sub
Private Sub TimeScroll_Change()
    Dim NewLeft As Long
    NewLeft = TimeScroll.Value * ((((MonthLabel(0).Width + 1 * Screen.TwipsPerPixelX) / 3) * 7) + 5 * Screen.TwipsPerPixelX)
    CalendarPanel(0).Left = -NewLeft
End Sub

Private Sub TimeScroll_Scroll()
    Dim NewLeft As Long
    NewLeft = TimeScroll.Value * ((((MonthLabel(0).Width + 1 * Screen.TwipsPerPixelX) / 3) * 7) + 5 * Screen.TwipsPerPixelX)
    CalendarPanel(0).Left = -NewLeft
End Sub

Private Sub txtFilterVal_Change(Index As Integer)
    SearchInList Index
End Sub

Private Sub txtFilterVal_KeyPress(Index As Integer, KeyAscii As Integer)
    Dim LineNo As Long
    Dim HitText As String
    If KeyAscii = 13 Then
        LineNo = FilterTab(Index).GetCurrentSelected
        If LineNo >= 0 Then
            HitText = FilterTab(Index).GetColumnValue(LineNo, 0)
            If InStr(HitText, txtFilterVal(Index).Text) > 0 Then FilterTab_SendLButtonDblClick Index, LineNo, 0
            SearchInList Index
        End If
        KeyAscii = 0
    Else
        KeyAscii = Asc(UCase(Chr(KeyAscii)))
    End If
End Sub

Private Sub txtSplitFr_Change(Index As Integer)
    Dim tmpCedaDate As String
    tmpCedaDate = ""
    tmpCedaDate = tmpCedaDate & Trim(txtSplitFr(0))
    tmpCedaDate = tmpCedaDate & Trim(txtSplitFr(1))
    tmpCedaDate = tmpCedaDate & Trim(txtSplitFr(2))
    txtSplitFr(0).Tag = tmpCedaDate
End Sub

Private Sub txtSplitFr_GotFocus(Index As Integer)
    SetTextSelected
End Sub

Private Sub txtSplitFr_LostFocus(Index As Integer)
    CheckSplitPeriod
End Sub

Private Sub txtSplitTo_Change(Index As Integer)
    Dim tmpCedaDate As String
    tmpCedaDate = ""
    tmpCedaDate = tmpCedaDate & Trim(txtSplitTo(0))
    tmpCedaDate = tmpCedaDate & Trim(txtSplitTo(1))
    tmpCedaDate = tmpCedaDate & Trim(txtSplitTo(2))
    txtSplitTo(0).Tag = tmpCedaDate
End Sub

Private Sub txtSplitTo_GotFocus(Index As Integer)
    SetTextSelected
End Sub

Private Sub txtSplitTo_LostFocus(Index As Integer)
    CheckSplitPeriod
End Sub

Private Sub txtVpfr_Change(Index As Integer)
    Dim tmpCedaDate As String
    tmpCedaDate = ""
    tmpCedaDate = tmpCedaDate & Trim(txtVpfr(0))
    tmpCedaDate = tmpCedaDate & Trim(txtVpfr(1))
    tmpCedaDate = tmpCedaDate & Trim(txtVpfr(2))
    txtVpfr(0).Tag = tmpCedaDate
End Sub

Private Sub txtVpfr_GotFocus(Index As Integer)
    SetTextSelected
End Sub

Private Sub txtVpfr_LostFocus(Index As Integer)
    CheckLoadPeriod
End Sub

Private Sub txtVpto_GotFocus(Index As Integer)
    SetTextSelected
End Sub

Private Sub txtVpto_LostFocus(Index As Integer)
    CheckLoadPeriod
End Sub

Private Sub txtVpto_Change(Index As Integer)
    Dim tmpCedaDate As String
    tmpCedaDate = ""
    tmpCedaDate = tmpCedaDate & Trim(txtVpto(0))
    tmpCedaDate = tmpCedaDate & Trim(txtVpto(1))
    tmpCedaDate = tmpCedaDate & Trim(txtVpto(2))
    txtVpto(0).Tag = tmpCedaDate
End Sub

Private Sub txtVpfr_KeyPress(Index As Integer, KeyAscii As Integer)
    Select Case KeyAscii
        Case 8
        Case 48 To 57
        Case Else
            'MsgBox KeyAscii
            KeyAscii = 0
    End Select
End Sub

Private Sub txtVpto_KeyPress(Index As Integer, KeyAscii As Integer)
    Select Case KeyAscii
        Case 8
        Case 48 To 57
        Case Else
            'MsgBox KeyAscii
            KeyAscii = 0
    End Select
End Sub
Private Sub ArrangeArrDays()
    Dim i As Integer
    MonthLabel(0).Width = ((ArrDays(0).Width + DepDays(0).Width) * 3) - 1 * Screen.TwipsPerPixelX
    ArrangeCalendar ArrDays, True
    ArrangeCalendar ArrDisp, False
    ArrangeCalendar DepDays, False
    ArrangeCalendar DepDisp, False
    i = ArrDays.Count
    TimeScroll.Max = (i / (WeeksPerMonth * 7)) - 1
    i = i - 1
    CalendarPanel(0).Width = MonthCover(MonthCover.UBound).Left + MonthCover(MonthCover.UBound).Width + 45
End Sub

Private Sub ArrangeCalendar(curCtrl, SetMonth As Boolean)
    Dim Idx As Integer
    Dim iMonth As Integer
    Dim iWeek As Integer
    Dim iDays As Integer
    Dim wkIdx As Integer
    Dim moIdx As Integer
    Dim lstIdx As Integer
    Dim nMonth As Integer
    Dim CurTop As Long
    Dim CurLeft As Long
    Dim CoverTop As Long
    Dim CoverLeft As Long
    Dim MoDays As String
    Dim NewTag As String
    Dim ValidDays As Integer
    Dim DayCount As Integer
    MoDays = "31,28,31,30,31,30,31,31,30,31,30,31"
    Idx = 0
    nMonth = 0
    curCtrl(Idx).BackColor = NormalGray
    CurTop = curCtrl(Idx).Top
    CoverTop = 180
    CoverLeft = 30
    For iMonth = 1 To 24
        nMonth = nMonth + 1
        If SetMonth Then
            Load MonthCover(iMonth)
            MonthCover(iMonth).Top = CoverTop
            MonthCover(iMonth).Left = CoverLeft
            MonthCover(iMonth).Height = ((curCtrl(0).Height * 2) * 6) + 60
            MonthCover(iMonth).Width = ((curCtrl(0).Width * 2) * 7) + 45
            MonthCover(iMonth).Visible = True
            'DrawBackGround MonthCover(iMonth), 7, True, True
            'If iMonth Mod 2 = 0 Then
            '    DrawBackGround MonthCover(iMonth), 7, False, True
            'Else
            '    DrawBackGround MonthCover(iMonth), 7, False, False
            'End If
            CoverLeft = CoverLeft + MonthCover(iMonth).Width + 30
        End If
        If nMonth > 12 Then nMonth = 1
        ValidDays = Val(GetItem(MoDays, nMonth, ","))
        DayCount = 0
        moIdx = Idx + 1
        For iWeek = 1 To WeeksPerMonth
            CurLeft = curCtrl(0).Left
            wkIdx = Idx + 1
            For iDays = 1 To 7
                DayCount = DayCount + 1
                Idx = Idx + 1
                Load curCtrl(Idx)
                Set curCtrl(Idx).Container = MonthCover(iMonth)
                curCtrl(Idx).Top = CurTop
                curCtrl(Idx).Left = CurLeft
                CurLeft = CurLeft + (curCtrl(Idx).Width * 2)
                NewTag = "II,,"
                curCtrl(Idx).Tag = NewTag
                curCtrl(Idx).ZOrder
                curCtrl(Idx).Visible = False
                'curCtrl(Idx).Visible = True
            Next
            lstIdx = Idx
            CurTop = CurTop + (curCtrl(Idx).Height * 2)
            CurLeft = curCtrl(wkIdx).Left
        Next
        CurTop = curCtrl(moIdx).Top
        If SetMonth Then
            Load MonthLabel(iMonth)
            MonthLabel(iMonth).Left = MonthCover(iMonth).Left
            MonthLabel(iMonth).Caption = GetItem(MonthList, nMonth, ",")
            MonthLabel(iMonth).Tag = "," & GetItem(MonthList, nMonth, ",")
            MonthLabel(iMonth).Visible = True
            MonthLabel(iMonth).ZOrder
        End If
    Next
End Sub

Private Sub SetArrDaysRange(SetFirstDay As String, SetLastDay As String, UseColor As Long, Flag As String)
    Dim FirstDay
    Dim LastDay
    Dim Fday
    Dim PeriodRange As Long
    Dim DayIdx As Integer
    Dim MonthIdx As Integer
    Dim MonthNxt As Integer
    Dim MonthCnt As Integer
    Dim TagItems As String
    Dim strWkdy As String
    Dim OpDate As String
    ResetRangeInfoObjects True, ArrDays, DepDays, True, True
    ResetRangeInfoObjects True, ArrDisp, DepDisp, True, True
    FirstRangeDate = ""
    LastRangeDate = ""
    FirstDay = CedaDateToVb(SetFirstDay)
    LastDay = CedaDateToVb(SetLastDay)
    PeriodRange = DateDiff("d", FirstDay, LastDay)
    If PeriodRange >= 0 Then
        MonthIdx = Val(Mid(SetFirstDay, 5, 2)) - 1
        MonthCnt = MonthIdx
        Fday = FirstDay
        While (Fday <= LastDay) And (DayIdx < ArrDays.Count)
            strWkdy = Trim(Str(Weekday(Fday, vbMonday)))
            OpDate = Format(Fday, "yyyymmdd")
            MonthNxt = Val(Mid(OpDate, 5, 2))
            If MonthNxt <> MonthIdx Then
                MonthIdx = MonthNxt
                MonthCnt = MonthCnt + 1
                DayIdx = InitMonthRange(OpDate, FirstRangeDate)
                OpDate = DecodeSsimDayFormat(OpDate, "CEDA", "SSIM2")
                TagItems = Str(DayIdx) & "," & Mid(OpDate, 3) & ",,,"
                MonthLabel(MonthCnt).Tag = TagItems
                MonthLabel(MonthCnt).Caption = Mid(OpDate, 3, 3) & " " & Mid(OpDate, 6, 2)
            Else
                OpDate = DecodeSsimDayFormat(OpDate, "CEDA", "SSIM2")
            End If
            If (DayIdx > 0) And (DayIdx < ArrDays.Count) Then
                ArrDays(DayIdx).Tag = "RR," & OpDate & "," & strWkdy & ",,,"
                ArrDays(DayIdx).BackColor = UseColor
                ArrDays(DayIdx).Visible = True
                ArrDisp(DayIdx).Tag = ArrDays(DayIdx).Tag
                ArrDisp(DayIdx).BackColor = UseColor
                ArrDisp(DayIdx).Visible = True
                DepDays(DayIdx).Tag = ArrDays(DayIdx).Tag
                DepDays(DayIdx).BackColor = UseColor
                DepDays(DayIdx).Visible = True
                DepDisp(DayIdx).Tag = ArrDays(DayIdx).Tag
                DepDisp(DayIdx).BackColor = UseColor
                DepDisp(DayIdx).Visible = True
                DayIdx = DayIdx + 1
            End If
            Fday = DateAdd("d", 1, Fday)
        Wend
        LastRangeDate = SetLastDay
        AdjustRangeScroll SetFirstDay, SetLastDay
    End If
End Sub

Private Sub AdjustRangeScroll(SetFirstDate As String, SetLastDate As String)
    Dim FirstIdx As Integer
    Dim LastIdx As Integer
    Dim MidIdx As Integer
    FirstIdx = InitMonthRange(SetFirstDate, FirstRangeDate)
    LastIdx = InitMonthRange(SetLastDate, FirstRangeDate)
    MidIdx = FirstIdx + ((LastIdx - FirstIdx) / 2)
    MidIdx = MidIdx / (WeeksPerMonth * 7) - 3
    If MidIdx < 0 Then MidIdx = 0
    If MidIdx > TimeScroll.Max Then MidIdx = TimeScroll.Max
    TimeScroll.Value = MidIdx
End Sub

Private Sub ResetRangeInfoObjects(FullReset As Boolean, ArrObj, DepObj, ResetArr As Boolean, ResetDep As Boolean)
    Dim i As Integer
    Dim TagItems As String
    Dim NewTag As String
    AdjustTimeScaleColors -1, ArrObj(0)
    AdjustTimeScaleColors -1, DepObj(0)
    For i = 1 To ArrObj.Count - 1
        If ResetArr Then
            TagItems = ArrObj(i).Tag
            If Left(TagItems, 1) <> "I" Then
                If (FullReset) Or (Left(TagItems, 1) <> "R") Then
                    Mid(TagItems, 1, 1) = "I"
                    ArrObj(i).Tag = TagItems
                    ArrObj(i).BackColor = DarkGray
                    If FullReset Then ArrObj(i).Visible = False
                End If
            End If
        End If
        If ResetDep Then
            TagItems = DepObj(i).Tag
            If Left(TagItems, 1) <> "I" Then
                If (FullReset) Or (Left(TagItems, 1) <> "R") Then
                    Mid(TagItems, 1, 1) = "I"
                    DepObj(i).Tag = TagItems
                    DepObj(i).BackColor = DarkGray
                    If FullReset Then DepObj(i).Visible = False
                End If
            End If
        End If
    Next
    If FullReset Then
        For i = 1 To MonthLabel.Count - 1
            MonthLabel(i).Caption = Left(MonthLabel(i).Caption, 3)
        Next
    End If
End Sub

Private Sub SetFlightRange(FlightRecord As String, ArrObj, DepObj, ArrColor As Long, DepColor As Long, ResetRange As Boolean)
    Dim FirstDay
    Dim LastDay
    Dim ArrDay
    Dim DepDay
    Dim SetFirstDay As String
    Dim SetLastDay As String
    Dim FirstValidDate As String
    Dim LastValidDate As String
    Dim PeriodRange As Long
    Dim DayDiff As Long
    Dim ArrFlno As String
    Dim ArrVpfr As String
    Dim ArrVpto As String
    Dim ArrIdx As Integer
    Dim ArrOpDate As String
    Dim ArrFrqd As String
    Dim ArrWkDay As String
    Dim ArrDaof As String
    Dim ArrMonthIdx As Integer
    Dim ArrMonthNxt As Integer
    Dim ArrTagItems As String
    Dim DepIdx As Integer
    Dim DepOpDate As String
    Dim DepWkDay As String
    Dim DepDaof As String
    Dim DepFlno As String
    Dim DepMonthIdx As Integer
    Dim DepMonthNxt As Integer
    Dim DepTagItems As String
    Dim IsValidWeek As Boolean
    Dim IsRotation As Boolean
    Dim DaofVal As Integer
    Dim InvalidDays As Integer
    Dim WkDayCnt As Integer
    Dim WeekFreq As Integer
    If Not StopNestedCalls Then
    
        If ResetRange Then ResetRangeInfoObjects False, ArrObj, DepObj, True, True
        
        ArrVpfr = GetRealItem(FlightRecord, StrVpfrCol, ",")
        ArrVpto = GetRealItem(FlightRecord, StrVptoCol, ",")
        If ArrVpfr <> "" And ArrVpto <> "" Then
            SetFirstDay = DecodeSsimDayFormat(ArrVpfr, "SSIM2", "CEDA")
            SetLastDay = DecodeSsimDayFormat(ArrVpto, "SSIM2", "CEDA")
            ArrFrqd = GetRealItem(FlightRecord, StrFredCol, ",")
            DepDaof = GetRealItem(FlightRecord, StrDaofCol, ",")
            DaofVal = Val(DepDaof)
            If DaofVal <> 0 Then
                ArrDaof = "+" & Trim(Str(DaofVal))
                DepDaof = "-" & Trim(Str(DaofVal))
            Else
                ArrDaof = "--"
                DepDaof = "--"
            End If
            WeekFreq = Val(GetRealItem(FlightRecord, StrFrqwCol, ","))
            If WeekFreq < 1 Then WeekFreq = 1
            ArrFlno = GetRealItem(FlightRecord, StrFlnaCol, ",")
            DepFlno = GetRealItem(FlightRecord, StrFlndCol, ",")
            IsRotation = True
            If (ArrFlno = "") Or (DepFlno = "") Then IsRotation = False
            FirstDay = CedaDateToVb(SetFirstDay)
            LastDay = CedaDateToVb(SetLastDay)
            PeriodRange = DateDiff("d", FirstDay, LastDay)
            If PeriodRange >= 0 Then
                IsValidWeek = True
                InvalidDays = 0
                WkDayCnt = 0
                ArrMonthIdx = Val(Mid(SetFirstDay, 5, 2)) - 1
                ArrDay = FirstDay
                ArrIdx = -1
                While ArrDay <= LastDay
                    ArrWkDay = Trim(Str(Weekday(ArrDay, vbMonday)))
                    ArrOpDate = Format(ArrDay, "yyyymmdd")
                    ArrMonthNxt = Val(Mid(ArrOpDate, 5, 2))
                    If (ArrMonthNxt <> ArrMonthIdx) Or (ArrIdx < 1) Then
                        ArrMonthIdx = ArrMonthNxt
                        ArrIdx = InitMonthRange(ArrOpDate, FirstRangeDate)
                    End If
                    ArrOpDate = DecodeSsimDayFormat(ArrOpDate, "CEDA", "SSIM2")
                    If DaofVal > 0 Then
                        DepDay = DateAdd("d", DaofVal, ArrDay)
                        DepWkDay = Trim(Str(Weekday(DepDay, vbMonday)))
                        DepOpDate = Format(DepDay, "yyyymmdd")
                        DepMonthNxt = Val(Mid(DepOpDate, 5, 2))
                        If (DepMonthNxt <> DepMonthIdx) Or (DepIdx < 1) Then
                            DepMonthIdx = DepMonthNxt
                            DepIdx = InitMonthRange(DepOpDate, FirstRangeDate)
                        End If
                        DepOpDate = DecodeSsimDayFormat(DepOpDate, "CEDA", "SSIM2")
                    Else
                        DepDay = ArrDay
                        DepWkDay = ArrWkDay
                        DepOpDate = ArrOpDate
                        DepMonthNxt = ArrMonthNxt
                        DepMonthIdx = ArrMonthIdx
                        DepIdx = ArrIdx
                    End If
                    If IsValidWeek And (InStr(ArrFrqd, ArrWkDay) > 0) Then
                        If (ArrIdx >= 0) Then
                            If (ArrObj(ArrIdx).Visible) And (FirstValidDate = "") Then FirstValidDate = ArrOpDate
                        End If
                        If (DepIdx >= 0) Then
                            If (DepObj(DepIdx).Visible) Then LastValidDate = DepOpDate
                        End If
                        ArrTagItems = "FR," & ArrOpDate & "," & ArrWkDay & "," & ArrDaof & ",,"
                        DepTagItems = "FR," & DepOpDate & "," & DepWkDay & "," & DepDaof & ",,"
                        If ArrFlno <> "" Then
                            If IsRotation Then
                                If DaofVal = 0 Then
                                    If (ArrIdx > 0) And (ArrIdx < ArrObj.Count) Then
                                        ArrObj(ArrIdx).Tag = ArrTagItems
                                        ArrObj(ArrIdx).BackColor = ArrColor
                                    End If
                                    If (DepIdx > 0) And (DepIdx < DepObj.Count) Then
                                        DepObj(DepIdx).Tag = DepTagItems
                                        DepObj(DepIdx).BackColor = DepColor
                                    End If
                                Else
                                    If (ArrIdx > 0) And (ArrIdx < ArrObj.Count) Then
                                        DepObj(ArrIdx).Tag = ArrTagItems
                                        DepObj(ArrIdx).BackColor = ArrColor
                                    End If
                                    If (DepIdx > 0) And (DepIdx < DepObj.Count) Then
                                        ArrObj(DepIdx).Tag = DepTagItems
                                        ArrObj(DepIdx).BackColor = DepColor
                                    End If
                                End If
                            Else
                                If (ArrIdx > 0) And (ArrIdx < ArrObj.Count) Then
                                    ArrObj(ArrIdx).Tag = ArrTagItems
                                    ArrObj(ArrIdx).BackColor = ArrColor
                                End If
                            End If
                        End If
                        If (ArrFlno = "") And (DepFlno <> "") And (DepIdx > 0) And (DepIdx < ArrObj.Count) Then
                            DepObj(DepIdx).Tag = DepTagItems
                            DepObj(DepIdx).BackColor = DepColor
                        End If
                    End If
                    If Not IsValidWeek Then InvalidDays = InvalidDays - 1
                    WkDayCnt = WkDayCnt + 1
                    If WkDayCnt = 7 Then
                        If IsValidWeek Then
                            InvalidDays = (WeekFreq - 1) * 7
                            If InvalidDays > 0 Then IsValidWeek = False
                        Else
                            If InvalidDays = 0 Then
                                IsValidWeek = True
                            Else
                                'here we have a problem
                                'MsgBox "Please tell Berni that here is a problem with the weekly frequency."
                            End If
                        End If
                        WkDayCnt = 0
                    End If
                    ArrDay = DateAdd("d", 1, ArrDay)
                    If ArrIdx > 0 Then ArrIdx = ArrIdx + 1
                    If DepIdx > 0 Then DepIdx = DepIdx + 1
                Wend
                Me.Refresh
            End If
        End If
    End If
End Sub

Private Function InitMonthRange(CedaDate As String, FirstMonthDate As String) As Integer
    Dim DayIdx As Integer
    Dim FirstDay
    Dim FirstDayDate As String
    Dim WkDay As Integer
    Dim FirstMonthIdx As Integer
    Dim CedaMonthIdx As Integer
    Dim FirstYear As Integer
    Dim CedaYear As Integer
    Dim MonthOffset As Integer
    DayIdx = -1
    If CedaDate <> "" Then
        If FirstMonthDate = "" Then FirstMonthDate = CedaDate
        FirstMonthIdx = Val(Mid(FirstMonthDate, 5, 2))
        FirstYear = Val(Left(FirstMonthDate, 4))
        CedaYear = Val(Left(CedaDate, 4))
        MonthOffset = (CedaYear - FirstYear) * 12
        CedaMonthIdx = Val(Mid(CedaDate, 5, 2)) + MonthOffset
        If CedaMonthIdx > 0 Then
            FirstDayDate = Left(CedaDate, 6) & "01"
            FirstDay = CedaDateToVb(FirstDayDate)
            WkDay = Weekday(FirstDay, vbMonday)
            DayIdx = ((CedaMonthIdx - 1) * WeeksPerMonth * 7) + WkDay + Val(Mid(CedaDate, 7, 2)) - 1
        End If
    End If
    InitMonthRange = DayIdx
End Function

Private Sub SearchInList(UseIndex As Integer)
    Static LastIndex As Integer
    Dim HitLst As String
    Dim HitRow As Long
    Dim NxtRow As Long
    Dim NewScroll As Long
    Dim LoopCount As Integer
    Dim tmpText As String
    Dim ColNbr As String
    Dim Index As Integer
    Dim UseCol As Long
    Index = UseIndex
    If Index < 0 Then Index = LastIndex
    LastIndex = Index
    tmpText = Trim(txtFilterVal(Index).Text)
    UseCol = 0
    If tmpText <> "" Then
        LoopCount = 0
        Do
            HitLst = FilterTab(Index).GetNextLineByColumnValue(UseCol, tmpText, 2)
            HitRow = Val(HitLst)
            If HitLst <> "" Then
                If HitRow > 5 Then NewScroll = HitRow - 5 Else NewScroll = 0
                FilterTab(Index).OnVScrollTo NewScroll
            Else
                FilterTab(Index).SetCurrentSelection -1
                FilterTab(Index).OnVScrollTo 0
            End If
            LoopCount = LoopCount + 1
        Loop While LoopCount < 2 And HitLst = ""
        FilterTab(Index).SetCurrentSelection HitRow
    End If
    txtFilterVal(Index).SetFocus
End Sub

Private Function IsEmptyItemList(itemlist As String, ItemSep As String, Defaults As String) As Boolean
    Dim tmpData As String
    tmpData = Replace(itemlist, ItemSep, "", 1, -1, vbBinaryCompare)
    tmpData = Replace(tmpData, " ", "", 1, -1, vbBinaryCompare)
    If tmpData = Defaults Then IsEmptyItemList = True Else IsEmptyItemList = False
End Function

Private Function Alc2ToAlc3LookUp(CurAlc2 As String) As String
    Dim Alc2LineNoList As String
    Dim CurAlc As String
    Dim CurLine As Long
    CurAlc = Trim(CurAlc2)
    If Len(CurAlc) = 2 Then
        CurAlc = UfisServer.BasicLookUp(0, 2, 1, CurAlc, 0, True)
    End If
    Alc2ToAlc3LookUp = CurAlc
End Function

Private Sub BuildUpRotations()
    Dim CurLine As Long
    Dim ArrMaxLine As Long
    Dim DepMaxLine As Long
    Dim CurCode As String
    Dim CurStat As String
    Dim CurWkst As String
    Dim CurFkya As String
    Dim OldFkya As String
    Dim CurFkyd As String
    Dim OldFkyd As String
    Dim StrStat As String
    Dim StrWkst As String
    Dim FkyaHitList As String
    Dim CurLineNbr As String
    Dim ItemNo As Long
    Dim LineNo As Long
    If StrTabData(0).GetLineCount > 0 Then
        StopNestedCalls = True
        CreateIsBusy = True
        Screen.MousePointer = 11
        StrTabData(0).Sort StrFkyaCol, True, True
        StrTabData(0).Refresh
        Flights(0).Sort 10, True, True
        Flights(1).Sort 10, True, True
        Flights(1).Visible = False
        Splitter(1).Top = StrTabData(1).Top + StrTabData(1).Height - Splitter(1).Height
        Flights(0).Height = Splitter(1).Top - Flights(0).Top + 5 * Screen.TwipsPerPixelX
        ArrMaxLine = Flights(0).GetLineCount - 1
        DepMaxLine = Flights(1).GetLineCount - 1
        For CurLine = 0 To ArrMaxLine
            If CurLine Mod 5 = 0 Then
                Flights(0).SetCurrentSelection CurLine
                Flights(0).OnVScrollTo CurLine - 3
                Flights(0).Refresh
            End If
            If CurLine Mod 100 = 0 Then
                PublishCounters
                Me.Refresh
            End If
            CurFkya = Flights(0).GetColumnValue(CurLine, 10)
            If CurFkya = OldFkya Then
                DisableDoubleFlights 0, ("," & Str(CurLine)), False
            Else
                OldFkya = CurFkya
            End If
            CurFkya = Left(CurFkya, 9)
            If CurLine <= DepMaxLine Then
                CurFkyd = Flights(1).GetColumnValue(CurLine, 10)
                If CurFkyd = OldFkyd Then
                    DisableDoubleFlights 1, ("," & Str(CurLine)), False
                Else
                    OldFkyd = CurFkyd
                End If
            End If
            CurCode = Left(Trim(Flights(0).GetColumnValue(CurLine, 0)) & "N", 1)
            If CurCode = "N" Then
                CurStat = Flights(0).GetColumnValue(CurLine, 6)
                CurWkst = Flights(0).GetColumnValue(CurLine, 7)
                If (CurStat = "0") And (CurWkst = "0") Then
                    FkyaHitList = StrTabData(0).GetLinesByColumnValue(StrFkyaCol, CurFkya, 0)
                    If FkyaHitList <> "" Then
                        CurLineNbr = "START"
                        ItemNo = 0
                        While CurLineNbr <> ""
                            CurLineNbr = GetRealItem(FkyaHitList, ItemNo, ",")
                            If CurLineNbr <> "" Then
                                LineNo = Val(CurLineNbr)
                                StrStat = StrTabData(0).GetColumnValue(LineNo, StrStatCol)
                                StrWkst = StrTabData(0).GetColumnValue(LineNo, StrWkstCol)
                                If (StrStat = "0") And (StrWkst = "0") Then CollectRotationFlights LineNo, ArrDisp, DepDisp
                                ItemNo = ItemNo + 1
                            End If
                        Wend
                    End If
                End If
            End If
        Next
        If DepMaxLine > ArrMaxLine Then
            For CurLine = ArrMaxLine + 1 To DepMaxLine
                CurFkyd = Flights(1).GetColumnValue(CurLine, 10)
                If CurFkyd = OldFkyd Then
                    DisableDoubleFlights 0, ("," & Str(CurLine)), False
                Else
                    OldFkyd = CurFkyd
                End If
            Next
        End If
        PublishCounters
        Flights(1).Visible = True
        Form_Resize
        Flights(0).OnVScrollTo 0
        Flights(1).OnVScrollTo 0
        Flights(0).SetCurrentSelection 0
        Flights(1).SetCurrentSelection 0
        StrTabData(1).SetCurrentSelection 0
        StopNestedCalls = False
        CreateIsBusy = False
        StrTabData(0).IndexDestroy "StrUrnoIdx"
        StrTabData(0).IndexCreate "StrUrnoIdx", StrUrnoCol
        StrTabData(1).IndexDestroy "StrUrnoIdx"
        StrTabData(1).IndexCreate "StrUrnoIdx", StrUrnoCol
        StrTabData(1).IndexDestroy "RotaUrnoIdx"
        StrTabData(1).IndexCreate "RotaUrnoIdx", StrRulaCol
        Flights(0).IndexDestroy "StrUrnoIdx"
        Flights(0).IndexCreate "StrUrnoIdx", 11
        Flights(0).IndexDestroy "RotaUrnoIdx"
        Flights(0).IndexCreate "RotaUrnoIdx", 12
        Flights(1).IndexDestroy "StrUrnoIdx"
        Flights(1).IndexCreate "StrUrnoIdx", 11
        Flights(1).IndexDestroy "RotaUrnoIdx"
        Flights(1).IndexCreate "RotaUrnoIdx", 12
        StrTabData(0).OnVScrollTo 0
        StrTabData(0).SetCurrentSelection 0
        Screen.MousePointer = 0
        Me.Refresh
    Else
        chkWork(0).BackColor = vbRed
        chkWork(0).ForeColor = vbWhite
    End If
End Sub
Private Sub PublishCounters()
    Dim i As Integer
    For i = 0 To 3
        ArrLineCnt(i).Caption = Trim(Str(CountArr(i)))
        DepLineCnt(i).Caption = Trim(Str(CountDep(i)))
        ArrLineCnt(i).Refresh
        DepLineCnt(i).Refresh
    Next
    For i = 0 To 2
        RotLineCnt(i).Caption = Trim(Str(CountRot(i)))
        RulLineCnt(i).Caption = Trim(Str(CountRul(i)))
        RotLineCnt(i).Refresh
        RulLineCnt(i).Refresh
    Next
End Sub
Private Sub CollectRotationFlights(LineNo As Long, ArrObj, DepObj)
    Dim FlightRecord As String
    Dim FirstDay
    Dim LastDay
    Dim ArrDay
    Dim DepDay
    Dim SetFirstDay As String
    Dim SetLastDay As String
    Dim PeriodRange As Long
    Dim ArrFlno As String
    Dim ArrVpfr As String
    Dim ArrVpto As String
    Dim ArrOpDate As String
    Dim ArrFrqd As String
    Dim ArrWkDay As String
    Dim DepOpDate As String
    Dim DepWkDay As String
    Dim DepFlno As String
    Dim IsValidWeek As Boolean
    Dim IsRotation As Boolean
    Dim DaofVal As Integer
    Dim InvalidDays As Integer
    Dim WkDayCnt As Integer
    Dim WeekFreq As Integer
    Dim FkyaHitList As String
    Dim FkydHitList As String
    Dim ArrFkey As String
    Dim DepFkey As String
    Dim StrFkya As String
    Dim StrFkyd As String
    Dim StrUrno As String
    Dim StrWkday As String
    Dim RotaUrno As String
    Dim NewDaof As String
    Dim ArrCedaDate As String
    Dim DepCedaDate As String
    Dim ArrTagItems As String
    Dim DepTagItems As String
    Dim NewLineNo As Long
    Dim ArrLineNo As Long
    Dim DepLineNo As Long
    Dim ArrStat As String
    Dim DepStat As String
    Dim ArrAct3 As String
    Dim DepAct3 As String
    Dim ArrStoa As String
    Dim DepStod As String
    Dim CurAct3List As String
    Dim FlightPairFound As Boolean
    Dim CurForeColor As Long
    Dim CurBackColor As Long
    
    FlightRecord = StrTabData(0).GetLineValues(LineNo)
    CurAct3List = BuildAct3List(FlightRecord)
    FlightPairFound = False
    StrTabData(0).GetLineColor LineNo, CurForeColor, CurBackColor
    
    ArrVpfr = GetRealItem(FlightRecord, StrVpfrCol, ",")
    ArrVpto = GetRealItem(FlightRecord, StrVptoCol, ",")
    If ArrVpfr <> "" And ArrVpto <> "" Then
        ArrFlno = GetRealItem(FlightRecord, StrFlnaCol, ",")
        DepFlno = GetRealItem(FlightRecord, StrFlndCol, ",")
        StrUrno = GetRealItem(FlightRecord, StrUrnoCol, ",")
        IsRotation = True
        If (ArrFlno = "") Or (DepFlno = "") Then IsRotation = False
        If IsRotation Then
            SetFirstDay = DecodeSsimDayFormat(ArrVpfr, "SSIM2", "CEDA")
            SetLastDay = DecodeSsimDayFormat(ArrVpto, "SSIM2", "CEDA")
            ArrFrqd = GetRealItem(FlightRecord, StrFredCol, ",")
            NewDaof = GetRealItem(FlightRecord, StrDaofCol, ",")
            DaofVal = Val(NewDaof)
            WeekFreq = Val(GetRealItem(FlightRecord, StrFrqwCol, ","))
            If WeekFreq < 1 Then WeekFreq = 1
            FirstDay = CedaDateToVb(SetFirstDay)
            LastDay = CedaDateToVb(SetLastDay)
            PeriodRange = DateDiff("d", FirstDay, LastDay)
            If PeriodRange >= 0 Then
                StrFkya = GetRealItem(FlightRecord, StrFkyaCol, ",")
                StrFkyd = GetRealItem(FlightRecord, StrFkydCol, ",")
                IsValidWeek = True
                InvalidDays = 0
                WkDayCnt = 0
                ArrDay = FirstDay
                NewLineNo = -1
                While ArrDay <= LastDay
                    ArrWkDay = Trim(Str(Weekday(ArrDay, vbMonday)))
                    ArrCedaDate = Format(ArrDay, "yyyymmdd")
                    ArrFkey = StrFkya & ArrCedaDate & "A"
                    ArrOpDate = DecodeSsimDayFormat(ArrCedaDate, "CEDA", "SSIM2")
                    If DaofVal > 0 Then
                        DepDay = DateAdd("d", DaofVal, ArrDay)
                        DepWkDay = Trim(Str(Weekday(DepDay, vbMonday)))
                        DepCedaDate = Format(DepDay, "yyyymmdd")
                        DepFkey = StrFkyd & DepCedaDate & "D"
                        DepOpDate = DecodeSsimDayFormat(DepCedaDate, "CEDA", "SSIM2")
                    Else
                        DepDay = ArrDay
                        DepCedaDate = ArrCedaDate
                        DepFkey = StrFkyd & DepCedaDate & "D"
                        DepWkDay = ArrWkDay
                        DepOpDate = ArrOpDate
                    End If
                    If IsValidWeek And (InStr(ArrFrqd, ArrWkDay) > 0) Then
                        'Suchen ArrFlight
                        FkyaHitList = Flights(0).GetLinesByColumnValue(10, ArrFkey, 0)
                        'Suchen DepFlight
                        FkydHitList = Flights(1).GetLinesByColumnValue(10, DepFkey, 0)
                        If (FkyaHitList <> "") And (FkydHitList <> "") Then
                            ArrLineNo = Val(GetRealItem(FkyaHitList, 0, ","))
                            DepLineNo = Val(GetRealItem(FkydHitList, 0, ","))
                            DisableDoubleFlights 0, FkyaHitList, True
                            DisableDoubleFlights 1, FkydHitList, True
                            ArrStat = Flights(0).GetColumnValue(ArrLineNo, 6)
                            ArrStat = ArrStat & Flights(0).GetColumnValue(ArrLineNo, 7)
                            DepStat = Flights(1).GetColumnValue(DepLineNo, 6)
                            DepStat = DepStat & Flights(1).GetColumnValue(DepLineNo, 7)
                            If (ArrStat = "00") And (DepStat = "00") Then
                                ArrTagItems = Flights(0).GetLineTag(ArrLineNo)
                                DepTagItems = Flights(1).GetLineTag(DepLineNo)
                                If IsValidFlightPair(ArrCedaDate, ArrTagItems, DepCedaDate, DepTagItems, CurAct3List) Then
                                    'Found Valid Rotation
                                    RotaUrno = StrUrno & ArrOpDate
                                    NewLineNo = StrTabData(1).GetLineCount
                                    StrTabData(1).InsertTextLine StrTabData(0).GetLineValues(LineNo), False
                                    StrTabData(1).SetColumnValue NewLineNo, StrVpfrCol, ArrOpDate
                                    StrTabData(1).SetColumnValue NewLineNo, StrVptoCol, DepOpDate
                                    StrWkday = FormatFrequency(ArrWkDay)
                                    StrTabData(1).SetColumnValue NewLineNo, StrFredCol, StrWkday
                                    StrTabData(1).SetColumnValue NewLineNo, StrStatCol, "0"
                                    StrTabData(1).SetColumnValue NewLineNo, StrWkstCol, "0"
                                    StrTabData(1).SetColumnValue NewLineNo, StrRemaCol, BuildBasicInfo(ArrTagItems, DepTagItems)
                                    StrTabData(1).SetColumnValue NewLineNo, StrRulaCol, RotaUrno
                                    StrTabData(1).SetLineTag NewLineNo, ArrTagItems & vbLf & DepTagItems
                                    If Not FlightPairFound Then
                                        'StrTabData(1).SetCurrentSelection NewLineNo
                                        StrTabData(0).SetColumnValue LineNo, StrWkstCol, "1"
                                        StrTabData(0).SetLineColor LineNo, vbBlack, LightestBlue
                                        CountRul(1) = CountRul(1) + 1
                                        If CurBackColor = LightestRed Then CountRul(2) = CountRul(2) - 1
                                        StrTabData(0).SetCurrentSelection LineNo
                                        StrTabData(0).OnVScrollTo LineNo - 3
                                        FlightPairFound = True
                                    End If
                                    Flights(0).SetColumnValue ArrLineNo, 7, "1"
                                    Flights(0).SetColumnValue ArrLineNo, 11, StrUrno
                                    Flights(0).SetColumnValue ArrLineNo, 12, RotaUrno
                                    Flights(1).SetColumnValue DepLineNo, 7, "1"
                                    Flights(1).SetColumnValue DepLineNo, 11, StrUrno
                                    Flights(1).SetColumnValue DepLineNo, 12, RotaUrno
                                    Flights(0).SetLineColor ArrLineNo, vbBlack, vbWhite
                                    Flights(1).SetLineColor DepLineNo, vbBlack, vbWhite
                                    CountArr(1) = CountArr(1) - 1
                                    CountArr(2) = CountArr(2) + 1
                                    CountDep(1) = CountDep(1) - 1
                                    CountDep(2) = CountDep(2) + 1
                                    CountRot(0) = CountRot(0) + 1
                                    NewLineNo = -1
                                End If
                            End If
                        End If
                    End If
                    If Not IsValidWeek Then InvalidDays = InvalidDays - 1
                    WkDayCnt = WkDayCnt + 1
                    If WkDayCnt = 7 Then
                        If IsValidWeek Then
                            InvalidDays = (WeekFreq - 1) * 7
                            If InvalidDays > 0 Then IsValidWeek = False
                        Else
                            If InvalidDays = 0 Then
                                IsValidWeek = True
                            Else
                                'here we have a problem
                                'MsgBox "Please tell Berni that here is a problem with the weekly frequency."
                            End If
                        End If
                        WkDayCnt = 0
                    End If
                    ArrDay = DateAdd("d", 1, ArrDay)
                Wend
                If FlightPairFound Then
                    StrTabData(1).OnVScrollTo StrTabData(1).GetLineCount - 2
                    StrTabData(1).Refresh
                    StrTabData(0).Refresh
                Else
                    If CurBackColor <> LightestRed Then
                        StrTabData(0).SetLineColor LineNo, vbBlack, LightestRed
                        CountRul(2) = CountRul(2) + 1
                    End If
                End If
            End If
        End If
    End If
End Sub

Private Function IsValidFlightPair(ArrCedaDate As String, ArrRec As String, DepCedaDate As String, DepRec As String, Act3List As String) As Boolean
    Dim ChkValidPair As Boolean
    Dim ArrAct3 As String
    Dim DepAct3 As String
    Dim ArrStoa As String
    Dim DepStod As String
    Dim CurAct3List As String
    ArrAct3 = GetRealItem(ArrRec, 10, ",")
    DepAct3 = GetRealItem(DepRec, 10, ",")
    ArrStoa = ArrCedaDate & Mid(GetRealItem(ArrRec, 14, ","), 9, 4)
    DepStod = DepCedaDate & Mid(GetRealItem(DepRec, 15, ","), 9, 4)
    If Act3List <> "" Then CurAct3List = Act3List Else CurAct3List = ArrAct3
    ChkValidPair = True
    If (ArrAct3 <> DepAct3) Or (InStr(CurAct3List, ArrAct3) = 0) Or (ArrStoa > DepStod) Then ChkValidPair = False
    IsValidFlightPair = ChkValidPair
End Function

Private Sub TransferNewRotations()
    Dim CurLine As Long
    Dim MaxLine As Long
    Dim CurForeColor As Long
    Dim CurBackColor As Long
    Dim FlightLine As Long
    Dim StrTabRec As String
    Dim FlightRec As String
    Dim FlightTagItems As String
    Dim ArrRecord As String
    Dim DepRecord As String
    Dim OldRecord As String
    Dim RotaRecord As String
    Dim CurStat As String
    Dim CurWkst As String
    Dim LineCount As Long
    Dim i As Integer
    Screen.MousePointer = 11
    chkStrSplitter(2).Value = 0
    chkStrSplitter(6).Value = 0
    FlightExtract.ClearExtractList
    MaxLine = StrTabData(1).GetLineCount - 1
    CurLine = 0
    While CurLine <= MaxLine
        If StrTabData(1).GetColumnValue(CurLine, StrStatCol) = "1" Then
            StrTabData(1).GetLineColor CurLine, CurForeColor, CurBackColor
            Select Case CurBackColor
                Case vbYellow
                    FlightLine = Val(StrTabData(1).GetColumnValue(CurLine, StrRulaCol))
                    Flights(0).SetColumnValue FlightLine, 7, "0"
                    StrTabData(1).DeleteLine CurLine
                    CurLine = CurLine - 1
                    MaxLine = MaxLine - 1
                Case vbGreen
                    FlightLine = Val(StrTabData(1).GetColumnValue(CurLine, StrRuldCol))
                    Flights(1).SetColumnValue FlightLine, 7, "0"
                    StrTabData(1).DeleteLine CurLine
                    CurLine = CurLine - 1
                    MaxLine = MaxLine - 1
                Case vbWhite
                Case Else
            End Select
        End If
        CurLine = CurLine + 1
    Wend
    StrTabData(1).Refresh
    Flights(0).Refresh
    Flights(1).Refresh
    LineCount = FlightExtract.ExtractData.GetLineCount
    MaxLine = StrTabData(1).GetLineCount - 1
    For CurLine = 0 To MaxLine
        CurWkst = StrTabData(1).GetColumnValue(CurLine, StrWkstCol)
        If CurWkst <> "1" Then
            StrTabRec = StrTabData(1).GetLineValues(CurLine)
            FlightTagItems = StrTabData(1).GetLineTag(CurLine)
            ArrRecord = GetItem(FlightTagItems, 1, vbLf)
            DepRecord = GetItem(FlightTagItems, 2, vbLf)
            RotaRecord = BuildNewRotaRecord(StrTabRec, ArrRecord, DepRecord)
            FlightExtract.ExtractData.InsertTextLine RotaRecord, False
            StrTabData(1).SetColumnValue CurLine, StrWkstCol, "1"
            LineCount = LineCount + 1
        End If
    Next
    For i = 0 To 1
        MaxLine = Flights(i).GetLineCount - 1
        For CurLine = 0 To MaxLine
            CurStat = Flights(i).GetColumnValue(CurLine, 6)
            If CurStat = "0" Then
                CurWkst = Flights(i).GetColumnValue(CurLine, 7)
                If CurWkst = "0" Then
                    FlightRec = Flights(i).GetLineValues(CurLine)
                    OldRecord = Flights(i).GetLineTag(CurLine)
                    RotaRecord = BuildRotaRecFromFlight(i, FlightRec, OldRecord)
                    FlightExtract.ExtractData.InsertTextLine RotaRecord, False
                    If i = 0 Then
                        FlightExtract.ExtractData.SetLineColor LineCount, vbBlack, NormalYellow
                    Else
                        FlightExtract.ExtractData.SetLineColor LineCount, vbBlack, vbGreen
                    End If
                    LineCount = LineCount + 1
                End If
            End If
        Next
    Next
    'FlightExtract.ExtractData.Sort 0, True, True
    FlightExtract.ExtractData.Refresh
    Screen.MousePointer = 0
End Sub

Private Function BuildNewRotaRecord(StrTabRec As String, ArrRec As String, DepRec As String) As String
    Dim NewRec As String
    Dim tmpData As String
    Dim tmpDaofData As String
    Dim DaofVal As Integer
    Dim Fday
    NewRec = ""
    NewRec = NewRec & GetRealItem(ArrRec, 0, ",") & "," 'line
    NewRec = NewRec & "" & ","  'I
    NewRec = NewRec & "" & ","  'A
    NewRec = NewRec & GetRealItem(ArrRec, 3, ",") & "," 'FLCA
    NewRec = NewRec & GetRealItem(ArrRec, 4, ",") & "," 'FLNA
    NewRec = NewRec & GetRealItem(DepRec, 5, ",") & "," 'FLCD
    NewRec = NewRec & GetRealItem(DepRec, 6, ",") & "," 'FLND
    NewRec = NewRec & DecodeSsimDayFormat(GetRealItem(StrTabRec, StrVpfrCol, ","), "SSIM2", "CEDA") & ","   'VPFR
    NewRec = NewRec & DecodeSsimDayFormat(GetRealItem(StrTabRec, StrVptoCol, ","), "SSIM2", "CEDA") & ","   'VPTO
    NewRec = NewRec & FormatFrequency(GetRealItem(StrTabRec, StrFredCol, ",")) & "," 'FRED
    NewRec = NewRec & GetRealItem(ArrRec, 10, ",") & ","    'ACT3
    NewRec = NewRec & GetRealItem(ArrRec, 11, ",") & ","    'SEAT
    NewRec = NewRec & GetRealItem(ArrRec, 12, ",") & ","    'ORG3
    NewRec = NewRec & GetRealItem(ArrRec, 13, ",") & ","    'VIA3
    tmpData = DecodeSsimDayFormat(GetRealItem(StrTabRec, StrVpfrCol, ","), "SSIM2", "CEDA")
    tmpData = tmpData & Mid(GetRealItem(ArrRec, 14, ","), 9, 4)
    NewRec = NewRec & tmpData & "," 'STOA
    tmpDaofData = GetRealItem(StrTabRec, StrDaofCol, ",")
    DaofVal = Val(tmpDaofData)
    tmpDaofData = Trim(Str(DaofVal))
    If tmpDaofData = "0" Then tmpDaofData = ""
    tmpData = DecodeSsimDayFormat(GetRealItem(StrTabRec, StrVpfrCol, ","), "SSIM2", "CEDA")
    If DaofVal > 0 Then
        Fday = CedaDateToVb(tmpData)
        Fday = DateAdd("d", DaofVal, Fday)
        tmpData = Format(Fday, "yyyymmdd")
    End If
    tmpData = tmpData & Mid(GetRealItem(DepRec, 15, ","), 9, 4)
    NewRec = NewRec & tmpData & "," 'STOD
    NewRec = NewRec & tmpDaofData & "," 'DAOF
    NewRec = NewRec & GetRealItem(DepRec, 17, ",") & ","    'VIA3
    NewRec = NewRec & GetRealItem(DepRec, 18, ",") & ","    'DES3
    NewRec = NewRec & GetRealItem(ArrRec, 19, ",") & ","    'NATA
    NewRec = NewRec & GetRealItem(DepRec, 20, ",") & ","    'NATD
    NewRec = NewRec & GetRealItem(StrTabRec, StrFrqwCol, ",") & "," 'FRQW
    NewRec = NewRec & GetRealItem(ArrRec, 22, ",") & ","    'REGN
    NewRec = NewRec & GetRealItem(ArrRec, 23, ",") & ","    'FTYP
    NewRec = NewRec & GetRealItem(ArrRec, 24, ",") & ","    'REMA
    NewRec = NewRec & GetRealItem(ArrRec, 25, ",") & ","    'INDEX
    BuildNewRotaRecord = NewRec
End Function

Private Function BuildRotaRecFromFlight(Index As Integer, FlightRec As String, OldRec As String) As String
    Dim NewRec As String
    Dim tmpData As String
    NewRec = ""
    If Index = 0 Then
        'Arrivals
        NewRec = NewRec & GetRealItem(OldRec, 0, ",") & "," 'line
        NewRec = NewRec & "A," 'I
        NewRec = NewRec & GetRealItem(FlightRec, 0, ",") & ","   'New/Delete (FHK)
        NewRec = NewRec & GetRealItem(OldRec, 3, ",") & "," 'FLCA
        NewRec = NewRec & GetRealItem(OldRec, 4, ",") & "," 'FLNA
        NewRec = NewRec & ","  'FLCD
        NewRec = NewRec & ","  'FLND
        NewRec = NewRec & GetRealItem(FlightRec, 3, ",") & ","   'VPFR
        NewRec = NewRec & GetRealItem(FlightRec, 3, ",") & ","   'VPTO
        NewRec = NewRec & FormatFrequency(GetRealItem(FlightRec, 4, ",")) & "," 'FRED
        NewRec = NewRec & GetRealItem(OldRec, 10, ",") & ","    'ACT3
        NewRec = NewRec & GetRealItem(OldRec, 11, ",") & ","    'SEAT
        NewRec = NewRec & GetRealItem(OldRec, 12, ",") & ","    'ORG3
        NewRec = NewRec & GetRealItem(OldRec, 13, ",") & ","    'VIA3
        tmpData = GetRealItem(FlightRec, 3, ",")
        tmpData = tmpData & GetRealItem(FlightRec, 5, ",")
        NewRec = NewRec & tmpData & "," 'STOA
        NewRec = NewRec & ","  'STOD
        NewRec = NewRec & ","  'DAOF
        NewRec = NewRec & ","     'VIA3
        NewRec = NewRec & ","     'DES3
        NewRec = NewRec & GetRealItem(OldRec, 19, ",") & ","    'NATA
        NewRec = NewRec & ","     'NATD
        NewRec = NewRec & ","  'FRQW
        NewRec = NewRec & GetRealItem(OldRec, 22, ",") & ","    'REGN
        NewRec = NewRec & GetRealItem(OldRec, 23, ",") & ","    'FTYP
        NewRec = NewRec & GetRealItem(OldRec, 24, ",") & ","    'REMA
        NewRec = NewRec & GetRealItem(OldRec, 25, ",") & ","    'INDEX
    Else
        'Departures
        NewRec = NewRec & GetRealItem(OldRec, 0, ",") & "," 'line
        NewRec = NewRec & "D," 'I
        NewRec = NewRec & GetRealItem(FlightRec, 0, ",") & ","   'New/Delete (FHK)
        NewRec = NewRec & ","  'FLCA
        NewRec = NewRec & ","  'FLNA
        NewRec = NewRec & GetRealItem(OldRec, 5, ",") & "," 'FLCD
        NewRec = NewRec & GetRealItem(OldRec, 6, ",") & "," 'FLND
        NewRec = NewRec & GetRealItem(FlightRec, 3, ",") & ","   'VPFR
        NewRec = NewRec & GetRealItem(FlightRec, 3, ",") & ","   'VPTO
        NewRec = NewRec & FormatFrequency(GetRealItem(FlightRec, 4, ",")) & "," 'FRED
        NewRec = NewRec & GetRealItem(OldRec, 10, ",") & ","    'ACT3
        NewRec = NewRec & GetRealItem(OldRec, 11, ",") & ","    'SEAT
        NewRec = NewRec & ","     'ORG3
        NewRec = NewRec & ","     'VIA3
        NewRec = NewRec & "," 'STOA
        tmpData = GetRealItem(FlightRec, 3, ",")
        tmpData = tmpData & GetRealItem(FlightRec, 5, ",")
        NewRec = NewRec & tmpData & "," 'STOD
        NewRec = NewRec & ","  'DAOF
        NewRec = NewRec & GetRealItem(OldRec, 17, ",") & ","    'VIA3
        NewRec = NewRec & GetRealItem(OldRec, 18, ",") & ","    'DES3
        NewRec = NewRec & ","     'NATA
        NewRec = NewRec & GetRealItem(OldRec, 20, ",") & ","    'NATD
        NewRec = NewRec & ","  'FRQW
        NewRec = NewRec & GetRealItem(OldRec, 22, ",") & ","    'REGN
        NewRec = NewRec & GetRealItem(OldRec, 23, ",") & ","    'FTYP
        NewRec = NewRec & GetRealItem(OldRec, 24, ",") & ","    'REMA
        NewRec = NewRec & GetRealItem(OldRec, 25, ",") & ","    'INDEX
    End If
    BuildRotaRecFromFlight = NewRec
End Function

Private Function BuildBasicInfo(ArrRec As String, DepRec As String) As String
    Dim BasInfo As String
    Dim tmpData As String
    BasInfo = "ARR "
    BasInfo = BasInfo & Mid(GetRealItem(ArrRec, 14, ","), 9, 4) & ": "
    BasInfo = BasInfo & GetRealItem(ArrRec, 12, ",")
    tmpData = Trim(GetRealItem(ArrRec, 13, ","))
    If tmpData = "" Then tmpData = "   "
    BasInfo = BasInfo & "(" & tmpData & ") "
    BasInfo = BasInfo & GetRealItem(ArrRec, 19, ",")
    BasInfo = BasInfo & " / DEP "
    BasInfo = BasInfo & Mid(GetRealItem(DepRec, 15, ","), 9, 4) & ": "
    BasInfo = BasInfo & GetRealItem(DepRec, 18, ",")
    tmpData = Trim(GetRealItem(DepRec, 17, ","))
    If tmpData = "" Then tmpData = "   "
    BasInfo = BasInfo & "(" & tmpData & ") "
    BasInfo = BasInfo & GetRealItem(DepRec, 20, ",")
    BuildBasicInfo = BasInfo
End Function

Private Sub DisableDoubleFlights(Index As Integer, LineList As String, SetChecked As Boolean)
    Dim CurLine As Long
    Dim CurItem As Long
    Dim CurForeColor As Long
    Dim CurBackColor As Long
    Dim ItemData As String
    CurItem = 0
    ItemData = "START"
    While ItemData <> ""
        CurItem = CurItem + 1
        ItemData = GetRealItem(LineList, CurItem, ",")
        If ItemData <> "" Then
            CurLine = Val(ItemData)
            Flights(Index).GetLineColor CurLine, CurForeColor, CurBackColor
            If CurBackColor <> vbRed Then
                Flights(Index).SetLineColor CurLine, vbWhite, vbRed
                Flights(Index).SetColumnValue CurLine, 9, "Double"
                If SetChecked Then Flights(Index).SetColumnValue CurLine, 7, "1"
                If Index = 0 Then
                    CountArr(3) = CountArr(3) + 1
                    If CurBackColor = vbWhite Then
                        CountArr(2) = CountArr(2) - 1
                    ElseIf CurBackColor = vbYellow Then
                        CountArr(1) = CountArr(1) - 1
                    End If
                Else
                    CountDep(3) = CountDep(3) + 1
                    If CurBackColor = vbWhite Then
                        CountDep(2) = CountDep(2) - 1
                    ElseIf CurBackColor = vbGreen Then
                        CountDep(1) = CountDep(1) - 1
                    End If
                End If
                PublishCounters
            End If
        End If
    Wend
End Sub

Private Function BuildAct3List(StrTabRec As String)
    Dim CurAct3List As String
    Dim GrnUrno As String
    Dim LineList As String
    Dim LineItem As String
    Dim ItmNbr As Long
    Dim LineNo As String
    GrnUrno = GetRealItem(StrTabRec, StrGrnuCol, ",")
    If GrnUrno <> "0" Then
        LineList = UfisServer.BasicData(5).GetLinesByColumnValue(2, GrnUrno, 0)
        CurAct3List = ""
        ItmNbr = 0
        LineItem = GetRealItem(LineList, ItmNbr, ",")
        While LineItem <> ""
            LineNo = Val(LineItem)
            CurAct3List = CurAct3List & "," & UfisServer.BasicData(5).GetColumnValue(LineNo, 0)
            ItmNbr = ItmNbr + 1
            LineItem = GetRealItem(LineList, ItmNbr, ",")
        Wend
        CurAct3List = Mid(CurAct3List, 2)
    Else
        CurAct3List = GetRealItem(StrTabRec, StrAct3Col, ",")
    End If
    BuildAct3List = CurAct3List
End Function

Private Sub JumpToColoredLine(CurTab, JmpColor As Long)
    Dim CurScroll As Long
    Dim CurLine As Long
    Dim MaxLine As Long
    Dim UseLine As Long
    Dim ForeColor As Long
    Dim BackColor As Long
    Dim LineFound As Boolean
    Dim LoopCount As Integer
    CurLine = CurTab.GetCurrentSelected
    If CurLine < 0 Then CurLine = -1
    MaxLine = CurTab.GetLineCount - 1
    LineFound = False
    LoopCount = 0
    Do
        While CurLine <= MaxLine
            CurLine = CurLine + 1
            CurTab.GetLineColor CurLine, ForeColor, BackColor
            If BackColor = JmpColor Then
                UseLine = CurLine
                LineFound = True
                CurLine = MaxLine + 1
            End If
        Wend
        LoopCount = LoopCount + 1
        CurLine = -1
    Loop While (LineFound = False) And (LoopCount < 2)
    If LineFound Then
        If UseLine > 5 Then CurScroll = UseLine - 5 Else CurScroll = 0
        CurTab.OnVScrollTo CurScroll
        CurTab.SetCurrentSelection UseLine
    End If
End Sub

Private Function SetLineMarkers(CurTab As TABLib.Tab, LineList As String, FirstColNo As Long, LastColNo As Long, IgnoreRotation As Boolean, CheckOpDays As String) As Long
    Dim LineNo As Long
    Dim CurBackColor As Long
    Dim CurForeColor As Long
    Dim FirstLine As Long
    Dim ColNo As Long
    Dim ToCol As Long
    Dim CurItem As Long
    Dim CurText As String
    Dim tmpDay As String
    Dim DoIt As Boolean
    FirstLine = Val(GetRealItem(LineList, 0, ","))
    If chkStrSplitter(5).Value = 1 Then
        If LineList <> "-1" Then
            CurText = "START"
            CurItem = 0
            While CurText <> ""
                CurText = GetRealItem(LineList, CurItem, ",")
                If CurText <> "" Then
                    LineNo = Val(CurText)
                    If LineNo >= 0 Then
                        DoIt = True
                        If IgnoreRotation Then
                            CurTab.GetLineColor LineNo, CurForeColor, CurBackColor
                            If CurBackColor = vbWhite Then DoIt = False
                        End If
                        If CheckOpDays <> "" Then
                            tmpDay = CurTab.GetColumnValue(LineNo, 4)
                            If InStr(CheckOpDays, tmpDay) = 0 Then DoIt = False
                        End If
                        If DoIt Then
                            CurTab.SetDecorationObject LineNo, FirstColNo, "Marker1"
                            ToCol = LastColNo - 1
                            For ColNo = FirstColNo + 1 To ToCol
                                CurTab.SetDecorationObject LineNo, ColNo, "Marker2"
                            Next
                            CurTab.SetDecorationObject LineNo, LastColNo, "Marker3"
                            If LineNo < FirstLine Then FirstLine = LineNo
                        End If
                    End If
                End If
                CurItem = CurItem + 1
            Wend
            CurTab.Refresh
        End If
    End If
    SetLineMarkers = FirstLine
End Function

Private Sub ResetLineMarkers(CurTab As TABLib.Tab)
    Dim LineNo As Long
    Dim MaxLine As Long
    MaxLine = CurTab.GetLineCount - 1
    For LineNo = 0 To MaxLine
        CurTab.ResetLineDecorations LineNo
    Next
    CurTab.Refresh
End Sub

Private Sub AdjustLineMarkers(CurTab, Caller As Integer, LineNo As Long, ForceRefresh As Boolean)
    Dim StrUrno As String
    Dim StrLines As String
    Dim ArrLines As String
    Dim DepLines As String
    Dim RotLines As String
    Dim FirstLineNo As Long
    Dim tmpFkey As String
    Dim tmpLine As String
    Dim tmpFreq As String
    Dim ArrDispChanged As Boolean
    Dim DepDispChanged As Boolean
    Dim RotDispChanged As Boolean
    Dim CheckValidRota As Boolean
    If (Not StopNestedCalls) And (Not IsInplaceEdit) Then
        CheckValidRota = False
        tmpFreq = ""
        If (chkStrSplitter(1).Value = 1) Or (chkStrSplitter(5).Value = 1) Then
            FirstLineNo = -1
            If LineNo >= 0 Then
                Select Case Caller
                    Case 1  'StrTab Rules
                        StrLines = Str(LineNo)
                        StrUrno = Trim(StrTabData(0).GetColumnValue(LineNo, StrUrnoCol))
                        ArrLines = Flights(0).GetLinesByIndexValue("StrUrnoIdx", StrUrno, 0)
                        If ArrLines = "" Then
                            tmpFkey = StrTabData(0).GetColumnValue(LineNo, StrFkyaCol)
                            ArrLines = Flights(0).GetLinesByColumnValue(10, tmpFkey, 2)
                            If ArrLines = "" Then ArrLines = "-1"
                            tmpFreq = Trim(StrTabData(0).GetColumnValue(LineNo, StrFredCol))
                            CheckValidRota = True
                        End If
                        DepLines = Flights(1).GetLinesByIndexValue("StrUrnoIdx", StrUrno, 0)
                        If DepLines = "" Then
                            tmpFkey = StrTabData(0).GetColumnValue(LineNo, StrFkydCol)
                            DepLines = Flights(1).GetLinesByColumnValue(10, tmpFkey, 2)
                            If DepLines = "" Then DepLines = "-1"
                            tmpFreq = Trim(StrTabData(0).GetColumnValue(LineNo, StrFredCol))
                            CheckValidRota = True
                        End If
                        RotLines = StrTabData(1).GetLinesByIndexValue("StrUrnoIdx", StrUrno, 0)
                        If RotLines = "" Then RotLines = "-1"
                    Case 2  'Arrival Flights
                        ArrLines = Str(LineNo)
                        tmpLine = Flights(0).GetColumnValue(LineNo, 12)
                        If Left(tmpLine, 1) = "M" Then
                            RotLines = Mid(tmpLine, 2)
                            DepLines = "-1"
                            StrLines = "-1"
                        Else
                            StrUrno = Flights(0).GetColumnValue(LineNo, 11)
                            StrLines = StrTabData(0).GetLinesByIndexValue("StrUrnoIdx", StrUrno, 0)
                            DepLines = "-1"
                            StrUrno = Flights(0).GetColumnValue(LineNo, 12)
                            RotLines = StrTabData(1).GetLinesByIndexValue("RotaUrnoIdx", StrUrno, 0)
                        End If
                    Case 3  'DepFlights
                        DepLines = Str(LineNo)
                        tmpLine = Flights(1).GetColumnValue(LineNo, 12)
                        If Left(tmpLine, 1) = "M" Then
                            RotLines = Mid(tmpLine, 2)
                            ArrLines = "-1"
                            StrLines = "-1"
                        Else
                            StrUrno = Flights(1).GetColumnValue(LineNo, 11)
                            StrLines = StrTabData(0).GetLinesByIndexValue("StrUrnoIdx", StrUrno, 0)
                            ArrLines = "-1"
                            StrUrno = Flights(1).GetColumnValue(LineNo, 12)
                            RotLines = StrTabData(1).GetLinesByIndexValue("RotaUrnoIdx", StrUrno, 0)
                        End If
                    Case 4  'New Rotations
                        RotLines = Str(LineNo)
                        If StrTabData(1).GetColumnValue(LineNo, StrStatCol) = "0" Then
                            StrUrno = StrTabData(1).GetColumnValue(LineNo, StrUrnoCol)
                            StrLines = StrTabData(0).GetLinesByIndexValue("StrUrnoIdx", StrUrno, 0)
                            StrUrno = StrUrno & StrTabData(1).GetColumnValue(LineNo, StrVpfrCol)
                            ArrLines = Flights(0).GetLinesByIndexValue("RotaUrnoIdx", StrUrno, 0)
                            DepLines = Flights(1).GetLinesByIndexValue("RotaUrnoIdx", StrUrno, 0)
                        Else
                            StrLines = "-1"
                            ArrLines = StrTabData(1).GetColumnValue(LineNo, StrRulaCol)
                            DepLines = StrTabData(1).GetColumnValue(LineNo, StrRuldCol)
                        End If
                    Case Else
                    StrLines = "-1"
                    ArrLines = "-1"
                    DepLines = "-1"
                    RotLines = "-1"
                End Select
            End If
        End If
        If chkStrSplitter(5).Value = 1 Then
            ResetLineMarkers StrTabData(0)
            ResetLineMarkers StrTabData(1)
            ResetLineMarkers Flights(0)
            ResetLineMarkers Flights(1)
            FirstLineNo = SetLineMarkers(StrTabData(0), StrLines, 0, 3, False, "")
            FirstLineNo = SetLineMarkers(StrTabData(0), StrLines, 4, 6, False, "")
            FirstLineNo = SetLineMarkers(StrTabData(0), StrLines, 7, 8, False, "")
            FirstLineNo = SetLineMarkers(StrTabData(0), StrLines, 9, 12, False, "")
            If (Caller <> 1) And (FirstLineNo >= 0) Then StrTabData(0).OnVScrollTo FirstLineNo - 1
            FirstLineNo = SetLineMarkers(Flights(0), ArrLines, 1, 5, CheckValidRota, tmpFreq)
            If (Caller <> 2) And (FirstLineNo >= 0) Then Flights(0).OnVScrollTo FirstLineNo - 1
            FirstLineNo = SetLineMarkers(Flights(1), DepLines, 1, 5, CheckValidRota, tmpFreq)
            If (Caller <> 3) And (FirstLineNo >= 0) Then Flights(1).OnVScrollTo FirstLineNo - 1
            FirstLineNo = SetLineMarkers(StrTabData(1), RotLines, 0, 3, False, "")
            FirstLineNo = SetLineMarkers(StrTabData(1), RotLines, 4, 6, False, "")
            FirstLineNo = SetLineMarkers(StrTabData(1), RotLines, 7, 8, False, "")
            FirstLineNo = SetLineMarkers(StrTabData(1), RotLines, 9, 12, False, "")
            If (Caller <> 4) And (FirstLineNo >= 0) Then StrTabData(1).OnVScrollTo FirstLineNo - 1
        End If
        If chkStrSplitter(1).Value = 1 Then
            ResetRangeInfoObjects False, ArrDays, DepDays, True, True
            ResetRangeInfoObjects False, ArrDisp, DepDisp, True, True
            MixRangeDisplay 1, StrLines
            Select Case Caller
                Case 1
                    MixRangeDisplay 4, RotLines
                Case 2
                    MixRangeDisplay 2, ArrLines
                Case 3
                    MixRangeDisplay 3, DepLines
                Case 4
                    MixRangeDisplay 4, RotLines
                Case Else
            End Select
        End If
        If (Me.Visible) And (Not NoEcho) Then CurTab.SetFocus
    End If
End Sub

Private Sub MixRangeDisplay(Caller As Integer, LineList As String)
    Dim LineNo As Long
    Dim CurItem As Long
    Dim CurText As String
    If LineList <> "-1" Then
        CurText = "START"
        CurItem = 0
        While CurText <> ""
            CurText = GetRealItem(LineList, CurItem, ",")
            If CurText <> "" Then
                LineNo = Val(CurText)
                If LineNo >= 0 Then
                    Select Case Caller
                        Case 1
                            SetFlightRange StrTabData(0).GetLineValues(LineNo), ArrDays, DepDays, vbYellow, vbGreen, False
                        Case 2
                            SetFlightRange (BuildStrFormatFromFlight(0, LineNo)), ArrDisp, DepDisp, vbWhite, vbCyan, False
                        Case 3
                            SetFlightRange (BuildStrFormatFromFlight(1, LineNo)), ArrDisp, DepDisp, vbWhite, vbCyan, False
                        Case 4
                            SetFlightRange StrTabData(1).GetLineValues(LineNo), ArrDisp, DepDisp, vbWhite, vbCyan, False
                    End Select
                End If
            End If
            CurItem = CurItem + 1
        Wend
    End If
End Sub
