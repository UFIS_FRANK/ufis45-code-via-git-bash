VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{64E8E382-05E2-11D2-9B1D-9B0630BC8F12}#1.0#0"; "TAB.ocx"
Begin VB.Form NoopFlights 
   Caption         =   "Flight Schedule Analizer"
   ClientHeight    =   7995
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   15240
   BeginProperty Font 
      Name            =   "Courier New"
      Size            =   11.25
      Charset         =   161
      Weight          =   700
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "NoopFlights.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   7995
   ScaleWidth      =   15240
   ShowInTaskbar   =   0   'False
   Begin VB.CheckBox chkWork 
      Caption         =   "Full"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   15
      Left            =   7620
      Style           =   1  'Graphical
      TabIndex        =   108
      ToolTipText     =   "Full Mode Import of All Records"
      Top             =   30
      Width           =   855
   End
   Begin VB.CheckBox chkWork 
      Caption         =   "Single"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   14
      Left            =   6600
      Style           =   1  'Graphical
      TabIndex        =   107
      ToolTipText     =   "Import Records of the Selected Aircraft(s)"
      Top             =   30
      Width           =   855
   End
   Begin VB.TextBox ServerTime 
      Alignment       =   2  'Center
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   13320
      Locked          =   -1  'True
      TabIndex        =   105
      Tag             =   "200205240800"
      Text            =   "24.05.2002 / 08:00"
      Top             =   30
      Width           =   1800
   End
   Begin VB.CheckBox chkWork 
      Caption         =   "Details"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   13
      Left            =   1770
      Style           =   1  'Graphical
      TabIndex        =   100
      Top             =   30
      Width           =   855
   End
   Begin TABLib.TAB RotaLink 
      Height          =   705
      Index           =   0
      Left            =   0
      TabIndex        =   89
      Top             =   3780
      Width           =   2355
      _Version        =   65536
      _ExtentX        =   4154
      _ExtentY        =   1244
      _StockProps     =   64
   End
   Begin TABLib.TAB RotaLink 
      Height          =   705
      Index           =   2
      Left            =   2400
      TabIndex        =   90
      Top             =   3780
      Width           =   885
      _Version        =   65536
      _ExtentX        =   1561
      _ExtentY        =   1244
      _StockProps     =   64
   End
   Begin TABLib.TAB RotaLink 
      Height          =   705
      Index           =   1
      Left            =   3420
      TabIndex        =   91
      Top             =   3780
      Width           =   2445
      _Version        =   65536
      _ExtentX        =   4313
      _ExtentY        =   1244
      _StockProps     =   64
   End
   Begin TABLib.TAB RotaList 
      Height          =   2535
      Index           =   0
      Left            =   5370
      TabIndex        =   86
      Top             =   1740
      Visible         =   0   'False
      Width           =   2355
      _Version        =   65536
      _ExtentX        =   4154
      _ExtentY        =   4471
      _StockProps     =   64
   End
   Begin TABLib.TAB RotaList 
      Height          =   2535
      Index           =   1
      Left            =   7830
      TabIndex        =   87
      Top             =   1740
      Visible         =   0   'False
      Width           =   2355
      _Version        =   65536
      _ExtentX        =   4154
      _ExtentY        =   4471
      _StockProps     =   64
   End
   Begin TABLib.TAB RotaList 
      Height          =   2475
      Index           =   2
      Left            =   8520
      TabIndex        =   88
      Top             =   2070
      Visible         =   0   'False
      Width           =   2355
      _Version        =   65536
      _ExtentX        =   4154
      _ExtentY        =   4366
      _StockProps     =   64
   End
   Begin VB.CheckBox chkWork 
      Caption         =   "Keep"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   12
      Left            =   8730
      Style           =   1  'Graphical
      TabIndex        =   59
      ToolTipText     =   "Don't clear selected lines"
      Top             =   30
      Width           =   855
   End
   Begin VB.CheckBox chkWork 
      Caption         =   "Delete"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   11
      Left            =   7860
      Style           =   1  'Graphical
      TabIndex        =   58
      ToolTipText     =   "Activate DoubleClick for Deletes"
      Top             =   30
      Width           =   855
   End
   Begin VB.CheckBox chkWork 
      Caption         =   "Update"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   10
      Left            =   6990
      Style           =   1  'Graphical
      TabIndex        =   57
      ToolTipText     =   "Activate DoubleClick for Updates"
      Top             =   30
      Width           =   855
   End
   Begin VB.CheckBox chkWork 
      Caption         =   "Insert"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   9
      Left            =   6120
      Style           =   1  'Graphical
      TabIndex        =   56
      ToolTipText     =   "Activate DoubleClick for Inserts"
      Top             =   30
      Width           =   855
   End
   Begin VB.CheckBox chkWork 
      Caption         =   "Jump"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   8
      Left            =   9600
      Style           =   1  'Graphical
      TabIndex        =   55
      ToolTipText     =   "AutoJump after each Transaction"
      Top             =   30
      Value           =   1  'Checked
      Width           =   855
   End
   Begin VB.CheckBox chkWork 
      Caption         =   "Ask"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   7
      Left            =   5250
      Style           =   1  'Graphical
      TabIndex        =   54
      Top             =   30
      Value           =   1  'Checked
      Width           =   855
   End
   Begin VB.CheckBox chkWork 
      Caption         =   "Activate"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   6
      Left            =   4380
      Style           =   1  'Graphical
      TabIndex        =   53
      ToolTipText     =   "Activate the Transaction Buttons"
      Top             =   30
      Width           =   855
   End
   Begin VB.CheckBox chkWork 
      Caption         =   "Doubles"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   5
      Left            =   30
      Style           =   1  'Graphical
      TabIndex        =   23
      Top             =   30
      Width           =   855
   End
   Begin VB.CheckBox chkWork 
      Caption         =   "Rotation"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   4
      Left            =   2640
      Style           =   1  'Graphical
      TabIndex        =   22
      Top             =   30
      Width           =   855
   End
   Begin VB.CheckBox chkWork 
      Caption         =   "AODB"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   2
      Left            =   900
      Style           =   1  'Graphical
      TabIndex        =   11
      Top             =   30
      Width           =   855
   End
   Begin VB.Frame TopLabels 
      Caption         =   "Airline Data"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   645
      Left            =   30
      TabIndex        =   6
      Top             =   390
      Width           =   14445
      Begin VB.Frame fraMinGrd 
         Caption         =   "Min.Grd.Time"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   645
         Left            =   6390
         TabIndex        =   112
         Top             =   0
         Width           =   1425
         Begin VB.TextBox txtMinGrdTime 
            Alignment       =   2  'Center
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   360
            TabIndex        =   113
            Top             =   240
            Width           =   675
         End
         Begin VB.Shape SignMinGrd 
            BackColor       =   &H000000FF&
            BackStyle       =   1  'Opaque
            FillColor       =   &H000000FF&
            FillStyle       =   0  'Solid
            Height          =   165
            Index           =   1
            Left            =   1110
            Shape           =   3  'Circle
            Top             =   330
            Visible         =   0   'False
            Width           =   195
         End
         Begin VB.Shape SignMinGrd 
            BackColor       =   &H000000FF&
            BackStyle       =   1  'Opaque
            FillColor       =   &H000000FF&
            FillStyle       =   0  'Solid
            Height          =   165
            Index           =   0
            Left            =   120
            Shape           =   3  'Circle
            Top             =   330
            Visible         =   0   'False
            Width           =   195
         End
      End
      Begin TABLib.TAB FlnoTabCombo 
         Height          =   375
         Index           =   0
         Left            =   4650
         TabIndex        =   111
         Top             =   270
         Width           =   1365
         _Version        =   65536
         _ExtentX        =   2408
         _ExtentY        =   661
         _StockProps     =   64
      End
      Begin TABLib.TAB RegnTabCombo 
         Height          =   375
         Index           =   0
         Left            =   4650
         TabIndex        =   110
         Top             =   270
         Width           =   1365
         _Version        =   65536
         _ExtentX        =   2408
         _ExtentY        =   661
         _StockProps     =   64
      End
      Begin TABLib.TAB AlcTabCombo 
         Height          =   375
         Index           =   0
         Left            =   90
         TabIndex        =   109
         Top             =   270
         Width           =   4515
         _Version        =   65536
         _ExtentX        =   7964
         _ExtentY        =   661
         _StockProps     =   64
      End
      Begin VB.Frame DspShrink 
         Height          =   675
         Left            =   6060
         TabIndex        =   71
         Top             =   -30
         Visible         =   0   'False
         Width           =   2775
         Begin VB.CheckBox chkShrink 
            Caption         =   "OK"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Index           =   0
            Left            =   2325
            Style           =   1  'Graphical
            TabIndex        =   72
            Top             =   270
            Width           =   360
         End
         Begin VB.Label lblShrink 
            Alignment       =   1  'Right Justify
            BorderStyle     =   1  'Fixed Single
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Index           =   5
            Left            =   1890
            TabIndex        =   78
            Tag             =   "C4"
            Top             =   270
            Width           =   330
         End
         Begin VB.Label lblShrink 
            Alignment       =   1  'Right Justify
            BorderStyle     =   1  'Fixed Single
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Index           =   4
            Left            =   1530
            TabIndex        =   77
            Tag             =   "S3"
            Top             =   270
            Width           =   330
         End
         Begin VB.Label lblShrink 
            Alignment       =   1  'Right Justify
            BorderStyle     =   1  'Fixed Single
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Index           =   3
            Left            =   1170
            TabIndex        =   76
            Tag             =   "C3"
            Top             =   270
            Width           =   330
         End
         Begin VB.Label lblShrink 
            Alignment       =   1  'Right Justify
            BorderStyle     =   1  'Fixed Single
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Index           =   2
            Left            =   810
            TabIndex        =   75
            Tag             =   "S2"
            Top             =   270
            Width           =   330
         End
         Begin VB.Label lblShrink 
            Alignment       =   1  'Right Justify
            BorderStyle     =   1  'Fixed Single
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Index           =   1
            Left            =   450
            TabIndex        =   74
            Tag             =   "C1"
            Top             =   270
            Width           =   330
         End
         Begin VB.Label lblShrink 
            Alignment       =   1  'Right Justify
            BorderStyle     =   1  'Fixed Single
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Index           =   0
            Left            =   90
            TabIndex        =   73
            Tag             =   "C2"
            Top             =   270
            Width           =   330
         End
         Begin VB.Label lblShrink 
            Alignment       =   1  'Right Justify
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            BorderStyle     =   1  'Fixed Single
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   285
            Index           =   6
            Left            =   90
            TabIndex        =   79
            Tag             =   "C2"
            Top             =   270
            Width           =   330
         End
         Begin VB.Label lblShrink 
            Alignment       =   1  'Right Justify
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            BorderStyle     =   1  'Fixed Single
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   285
            Index           =   7
            Left            =   450
            TabIndex        =   80
            Tag             =   "C2"
            Top             =   270
            Width           =   330
         End
         Begin VB.Label lblShrink 
            Alignment       =   1  'Right Justify
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            BorderStyle     =   1  'Fixed Single
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   285
            Index           =   8
            Left            =   810
            TabIndex        =   81
            Tag             =   "C2"
            Top             =   270
            Width           =   330
         End
         Begin VB.Label lblShrink 
            Alignment       =   1  'Right Justify
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            BorderStyle     =   1  'Fixed Single
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   285
            Index           =   9
            Left            =   1170
            TabIndex        =   82
            Tag             =   "C2"
            Top             =   270
            Width           =   330
         End
         Begin VB.Label lblShrink 
            Alignment       =   1  'Right Justify
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            BorderStyle     =   1  'Fixed Single
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   285
            Index           =   10
            Left            =   1530
            TabIndex        =   83
            Tag             =   "C2"
            Top             =   270
            Width           =   330
         End
         Begin VB.Label lblShrink 
            Alignment       =   1  'Right Justify
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            BorderStyle     =   1  'Fixed Single
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   285
            Index           =   11
            Left            =   1890
            TabIndex        =   84
            Tag             =   "C2"
            Top             =   270
            Width           =   330
         End
      End
      Begin VB.Frame DspLoaded 
         BorderStyle     =   0  'None
         Height          =   315
         Left            =   9270
         TabIndex        =   62
         Top             =   240
         Width           =   5085
         Begin VB.Label TotalSecs 
            Alignment       =   2  'Center
            BorderStyle     =   1  'Fixed Single
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   3975
            TabIndex        =   85
            Top             =   30
            Width           =   1005
         End
         Begin VB.Label Label1 
            Caption         =   "Loaded"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Left            =   210
            TabIndex        =   66
            Top             =   60
            Width           =   675
         End
         Begin VB.Label CurEnd 
            Alignment       =   2  'Center
            BorderStyle     =   1  'Fixed Single
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   1980
            TabIndex        =   65
            Top             =   30
            Width           =   945
         End
         Begin VB.Label CurBegin 
            Alignment       =   2  'Center
            BorderStyle     =   1  'Fixed Single
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   990
            TabIndex        =   64
            Top             =   30
            Width           =   945
         End
         Begin VB.Label UsedSec 
            Alignment       =   2  'Center
            BorderStyle     =   1  'Fixed Single
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   2940
            TabIndex        =   63
            Top             =   30
            Width           =   1005
         End
      End
      Begin VB.ComboBox FlnoList 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   4410
         Sorted          =   -1  'True
         TabIndex        =   29
         Top             =   90
         Visible         =   0   'False
         Width           =   1635
      End
      Begin VB.Frame DspLoading 
         BorderStyle     =   0  'None
         Height          =   315
         Left            =   6030
         TabIndex        =   18
         Top             =   270
         Visible         =   0   'False
         Width           =   2775
         Begin VB.Label Label6 
            Caption         =   "Loading"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Left            =   90
            TabIndex        =   21
            Top             =   30
            Width           =   735
         End
         Begin VB.Label lblVpto 
            Alignment       =   2  'Center
            BorderStyle     =   1  'Fixed Single
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   1830
            TabIndex        =   20
            Top             =   0
            Width           =   945
         End
         Begin VB.Label lblVpfr 
            Alignment       =   2  'Center
            BorderStyle     =   1  'Fixed Single
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   840
            TabIndex        =   19
            Top             =   0
            Width           =   945
         End
      End
   End
   Begin VB.CheckBox chkWork 
      Caption         =   "Shrink"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   1
      Left            =   3510
      Style           =   1  'Graphical
      TabIndex        =   5
      ToolTipText     =   "Show Differences Only"
      Top             =   30
      Width           =   855
   End
   Begin MSComctlLib.StatusBar StatusBar 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   4
      Top             =   7710
      Width           =   15240
      _ExtentX        =   26882
      _ExtentY        =   503
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   4
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   7056
            MinWidth        =   7056
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   11466
            MinWidth        =   6174
         EndProperty
         BeginProperty Panel3 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   6703
            MinWidth        =   1411
         EndProperty
         BeginProperty Panel4 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   2
            Object.Width           =   1058
            MinWidth        =   1058
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.CheckBox chkWork 
      Caption         =   "Close"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   0
      Left            =   10470
      Style           =   1  'Graphical
      TabIndex        =   3
      Top             =   30
      Width           =   855
   End
   Begin TABLib.TAB NoopList 
      Height          =   2475
      Index           =   0
      Left            =   0
      TabIndex        =   0
      Top             =   1140
      Width           =   2355
      _Version        =   65536
      _ExtentX        =   4154
      _ExtentY        =   4366
      _StockProps     =   64
   End
   Begin TABLib.TAB NoopList 
      Height          =   2475
      Index           =   1
      Left            =   3420
      TabIndex        =   2
      Top             =   1140
      Width           =   2475
      _Version        =   65536
      _ExtentX        =   4366
      _ExtentY        =   4366
      _StockProps     =   64
   End
   Begin VB.Frame BottomLabels 
      Height          =   855
      Index           =   0
      Left            =   30
      TabIndex        =   7
      Top             =   4980
      Width           =   6015
      Begin VB.TextBox txtSearch 
         Appearance      =   0  'Flat
         BackColor       =   &H8000000F&
         BorderStyle     =   0  'None
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Index           =   1
         Left            =   150
         MaxLength       =   9
         TabIndex        =   47
         Top             =   525
         Width           =   975
      End
      Begin VB.TextBox txtSearch 
         Appearance      =   0  'Flat
         BackColor       =   &H8000000F&
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Index           =   0
         Left            =   150
         MaxLength       =   9
         TabIndex        =   45
         Top             =   225
         Width           =   975
      End
      Begin VB.CheckBox chkSearch 
         Caption         =   "Look"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   0
         Left            =   1170
         Style           =   1  'Graphical
         TabIndex        =   46
         Top             =   195
         Width           =   780
      End
      Begin VB.CheckBox chkSearch 
         Caption         =   "Look"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   1
         Left            =   1170
         Style           =   1  'Graphical
         TabIndex        =   48
         Top             =   495
         Width           =   780
      End
      Begin VB.CheckBox chkSearch 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   4
         Left            =   90
         Style           =   1  'Graphical
         TabIndex        =   67
         Top             =   195
         Width           =   1080
      End
      Begin VB.CheckBox chkSearch 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   5
         Left            =   90
         Style           =   1  'Graphical
         TabIndex        =   68
         Top             =   495
         Width           =   1080
      End
      Begin VB.Label CliFkeyCnt 
         Alignment       =   1  'Right Justify
         BackColor       =   &H008080FF&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   11
         Left            =   2640
         TabIndex        =   102
         Top             =   495
         Width           =   645
      End
      Begin VB.Label CliFkeyCnt 
         Alignment       =   1  'Right Justify
         BackColor       =   &H008080FF&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   10
         Left            =   2640
         TabIndex        =   101
         Top             =   195
         Width           =   645
      End
      Begin VB.Label CliFkeyCnt 
         Alignment       =   1  'Right Justify
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   9
         Left            =   1980
         TabIndex        =   37
         Top             =   495
         Width           =   645
      End
      Begin VB.Label CliFkeyCnt 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   285
         Index           =   8
         Left            =   3300
         TabIndex        =   36
         ToolTipText     =   "Different to AODB"
         Top             =   495
         Width           =   645
      End
      Begin VB.Label CliFkeyCnt 
         Alignment       =   1  'Right Justify
         BackColor       =   &H0000FF00&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   285
         Index           =   7
         Left            =   3960
         TabIndex        =   35
         ToolTipText     =   "Must be inserted."
         Top             =   495
         Width           =   645
      End
      Begin VB.Label CliFkeyCnt 
         Alignment       =   1  'Right Justify
         BackColor       =   &H0000FFFF&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   285
         Index           =   6
         Left            =   4620
         TabIndex        =   34
         ToolTipText     =   "Doubles in File"
         Top             =   495
         Width           =   645
      End
      Begin VB.Label CliFkeyCnt 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00E0E0E0&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   285
         Index           =   5
         Left            =   5280
         TabIndex        =   33
         ToolTipText     =   "Identical in File and AODB"
         Top             =   495
         Width           =   645
      End
      Begin VB.Label CliFkeyCnt 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00E0E0E0&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   285
         Index           =   4
         Left            =   5280
         TabIndex        =   31
         ToolTipText     =   "Identical in File and AODB"
         Top             =   180
         Width           =   645
      End
      Begin VB.Label CliFkeyCnt 
         Alignment       =   1  'Right Justify
         BackColor       =   &H0000FFFF&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   285
         Index           =   3
         Left            =   4620
         TabIndex        =   25
         ToolTipText     =   "Doubles in File"
         Top             =   195
         Width           =   645
      End
      Begin VB.Label CliFkeyCnt 
         Alignment       =   1  'Right Justify
         BackColor       =   &H0000FF00&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   285
         Index           =   2
         Left            =   3960
         TabIndex        =   15
         ToolTipText     =   "Must be inserted."
         Top             =   195
         Width           =   645
      End
      Begin VB.Label CliFkeyCnt 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   285
         Index           =   1
         Left            =   3300
         TabIndex        =   14
         ToolTipText     =   "Different to AODB"
         Top             =   195
         Width           =   645
      End
      Begin VB.Label CliFkeyCnt 
         Alignment       =   1  'Right Justify
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   0
         Left            =   1980
         TabIndex        =   8
         Top             =   195
         Width           =   645
      End
   End
   Begin VB.Frame BottomLabels 
      Height          =   855
      Index           =   1
      Left            =   30
      TabIndex        =   9
      Top             =   6840
      Width           =   6015
      Begin VB.TextBox txtSearch 
         Appearance      =   0  'Flat
         BackColor       =   &H8000000F&
         BorderStyle     =   0  'None
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Index           =   3
         Left            =   150
         MaxLength       =   9
         TabIndex        =   51
         Top             =   525
         Width           =   975
      End
      Begin VB.TextBox txtSearch 
         Appearance      =   0  'Flat
         BackColor       =   &H8000000F&
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Index           =   2
         Left            =   150
         MaxLength       =   9
         TabIndex        =   49
         Top             =   225
         Width           =   975
      End
      Begin VB.CheckBox chkSearch 
         Caption         =   "Look"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   2
         Left            =   1170
         Style           =   1  'Graphical
         TabIndex        =   50
         Top             =   195
         Width           =   780
      End
      Begin VB.CheckBox chkSearch 
         Caption         =   "Look"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   3
         Left            =   1170
         Style           =   1  'Graphical
         TabIndex        =   52
         Top             =   495
         Width           =   780
      End
      Begin VB.CheckBox chkSearch 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   6
         Left            =   90
         Style           =   1  'Graphical
         TabIndex        =   69
         Top             =   195
         Width           =   1080
      End
      Begin VB.CheckBox chkSearch 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   7
         Left            =   90
         Style           =   1  'Graphical
         TabIndex        =   70
         Top             =   495
         Width           =   1080
      End
      Begin VB.Label SrvFkeyCnt 
         Alignment       =   1  'Right Justify
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   0
         Left            =   1980
         TabIndex        =   10
         Top             =   195
         Width           =   645
      End
      Begin VB.Label SrvFkeyCnt 
         Alignment       =   1  'Right Justify
         BackColor       =   &H008080FF&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   11
         Left            =   2640
         TabIndex        =   104
         Top             =   495
         Width           =   645
      End
      Begin VB.Label SrvFkeyCnt 
         Alignment       =   1  'Right Justify
         BackColor       =   &H008080FF&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   10
         Left            =   2640
         TabIndex        =   103
         Top             =   195
         Width           =   645
      End
      Begin VB.Label SrvFkeyCnt 
         Alignment       =   1  'Right Justify
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   9
         Left            =   1980
         TabIndex        =   42
         Top             =   495
         Width           =   645
      End
      Begin VB.Label SrvFkeyCnt 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   285
         Index           =   8
         Left            =   3300
         TabIndex        =   41
         ToolTipText     =   "Different to File"
         Top             =   495
         Width           =   645
      End
      Begin VB.Label SrvFkeyCnt 
         Alignment       =   1  'Right Justify
         BackColor       =   &H000000FF&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   285
         Index           =   7
         Left            =   3960
         TabIndex        =   40
         ToolTipText     =   "In AODB but not in File"
         Top             =   495
         Width           =   645
      End
      Begin VB.Label SrvFkeyCnt 
         Alignment       =   1  'Right Justify
         BackColor       =   &H0000C0C0&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   285
         Index           =   6
         Left            =   4620
         TabIndex        =   39
         ToolTipText     =   "Doubles in AODB"
         Top             =   495
         Width           =   645
      End
      Begin VB.Label SrvFkeyCnt 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00E0E0E0&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   285
         Index           =   5
         Left            =   5280
         TabIndex        =   38
         Top             =   495
         Width           =   645
      End
      Begin VB.Label SrvFkeyCnt 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00E0E0E0&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   285
         Index           =   4
         Left            =   5280
         TabIndex        =   32
         Top             =   195
         Width           =   645
      End
      Begin VB.Label SrvFkeyCnt 
         Alignment       =   1  'Right Justify
         BackColor       =   &H0000C0C0&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   285
         Index           =   3
         Left            =   4620
         TabIndex        =   26
         ToolTipText     =   "Doubles in AODB"
         Top             =   195
         Width           =   645
      End
      Begin VB.Label SrvFkeyCnt 
         Alignment       =   1  'Right Justify
         BackColor       =   &H000000FF&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   285
         Index           =   2
         Left            =   3960
         TabIndex        =   17
         ToolTipText     =   "In AODB but not in File"
         Top             =   195
         Width           =   645
      End
      Begin VB.Label SrvFkeyCnt 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   285
         Index           =   1
         Left            =   3300
         TabIndex        =   16
         ToolTipText     =   "Different to File"
         Top             =   195
         Width           =   645
      End
   End
   Begin TABLib.TAB NoopList 
      Height          =   2475
      Index           =   2
      Left            =   2430
      TabIndex        =   1
      Top             =   1140
      Width           =   855
      _Version        =   65536
      _ExtentX        =   1508
      _ExtentY        =   4366
      _StockProps     =   64
   End
   Begin VB.Frame BottomLabels 
      Height          =   915
      Index           =   3
      Left            =   30
      TabIndex        =   92
      Top             =   5910
      Width           =   3045
      Begin VB.CheckBox chkRota 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   2
         Left            =   2010
         Style           =   1  'Graphical
         TabIndex        =   99
         Top             =   210
         Width           =   945
      End
      Begin VB.CheckBox chkRota 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   1
         Left            =   1050
         Style           =   1  'Graphical
         TabIndex        =   98
         Top             =   210
         Width           =   945
      End
      Begin VB.CheckBox chkRota 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   0
         Left            =   90
         Style           =   1  'Graphical
         TabIndex        =   97
         Top             =   210
         Width           =   945
      End
      Begin VB.Label RotaKeyCnt 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00E0E0E0&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   285
         Index           =   3
         Left            =   2250
         TabIndex        =   96
         Top             =   540
         Width           =   705
      End
      Begin VB.Label RotaKeyCnt 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00E0E0E0&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   285
         Index           =   2
         Left            =   1530
         TabIndex        =   95
         Top             =   540
         Width           =   705
      End
      Begin VB.Label RotaKeyCnt 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00E0E0E0&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   285
         Index           =   1
         Left            =   810
         TabIndex        =   94
         Top             =   540
         Width           =   705
      End
      Begin VB.Label RotaKeyCnt 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00E0E0E0&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   285
         Index           =   0
         Left            =   90
         TabIndex        =   93
         Top             =   540
         Width           =   705
      End
   End
   Begin VB.Frame BottomLabels 
      Height          =   855
      Index           =   2
      Left            =   3150
      TabIndex        =   24
      Top             =   5910
      Width           =   3045
      Begin MSComctlLib.ProgressBar MyProgressBar 
         Height          =   120
         Index           =   1
         Left            =   120
         TabIndex        =   61
         Top             =   630
         Visible         =   0   'False
         Width           =   2805
         _ExtentX        =   4948
         _ExtentY        =   212
         _Version        =   393216
         Appearance      =   0
         Scrolling       =   1
      End
      Begin MSComctlLib.ProgressBar MyProgressBar 
         Height          =   240
         Index           =   0
         Left            =   120
         TabIndex        =   43
         Top             =   510
         Visible         =   0   'False
         Width           =   2805
         _ExtentX        =   4948
         _ExtentY        =   423
         _Version        =   393216
         Appearance      =   0
         Scrolling       =   1
      End
      Begin VB.Shape CedaLed 
         BackColor       =   &H00C0C0C0&
         BorderColor     =   &H00000000&
         FillStyle       =   0  'Solid
         Height          =   165
         Index           =   1
         Left            =   1665
         Top             =   255
         Visible         =   0   'False
         Width           =   465
      End
      Begin VB.Shape CedaLed 
         BackColor       =   &H00C0C0C0&
         BorderColor     =   &H00000000&
         FillStyle       =   0  'Solid
         Height          =   165
         Index           =   0
         Left            =   1155
         Top             =   255
         Visible         =   0   'False
         Width           =   465
      End
      Begin VB.Label CurPos 
         Alignment       =   1  'Right Justify
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   285
         Index           =   4
         Left            =   2205
         TabIndex        =   60
         Top             =   195
         Visible         =   0   'False
         Width           =   750
      End
      Begin VB.Label CurPos 
         Alignment       =   2  'Center
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   3
         Left            =   90
         TabIndex        =   44
         Top             =   495
         Width           =   2865
      End
      Begin VB.Label CurPos 
         Alignment       =   1  'Right Justify
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   2
         Left            =   2205
         TabIndex        =   30
         Top             =   195
         Width           =   750
      End
      Begin VB.Label CurPos 
         Alignment       =   2  'Center
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   1
         Left            =   1080
         TabIndex        =   28
         Top             =   195
         Width           =   1110
      End
      Begin VB.Label CurPos 
         Alignment       =   2  'Center
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   0
         Left            =   90
         TabIndex        =   27
         Top             =   195
         Width           =   975
      End
   End
   Begin VB.CheckBox chkWork 
      Caption         =   "Flights"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   3
      Left            =   900
      Style           =   1  'Graphical
      TabIndex        =   12
      Top             =   30
      Visible         =   0   'False
      Width           =   855
   End
   Begin VB.Label Label2 
      Caption         =   "Time Limit"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   225
      Left            =   12360
      TabIndex        =   106
      Top             =   90
      Width           =   915
   End
   Begin VB.Line BLine 
      BorderColor     =   &H00808080&
      BorderWidth     =   2
      Index           =   0
      X1              =   30
      X2              =   3600
      Y1              =   1125
      Y2              =   1125
   End
   Begin VB.Line RLine 
      BorderWidth     =   2
      Index           =   5
      X1              =   6660
      X2              =   6660
      Y1              =   1140
      Y2              =   2590
   End
   Begin VB.Line RLine 
      BorderColor     =   &H00808080&
      BorderWidth     =   2
      Index           =   4
      X1              =   6510
      X2              =   6510
      Y1              =   1140
      Y2              =   2590
   End
   Begin VB.Line RLine 
      BorderWidth     =   2
      Index           =   3
      X1              =   6450
      X2              =   6450
      Y1              =   1140
      Y2              =   2590
   End
   Begin VB.Line RLine 
      BorderColor     =   &H00808080&
      BorderWidth     =   2
      Index           =   2
      X1              =   6300
      X2              =   6300
      Y1              =   1140
      Y2              =   2590
   End
   Begin VB.Line RLine 
      BorderColor     =   &H00808080&
      BorderWidth     =   2
      Index           =   0
      X1              =   6120
      X2              =   6120
      Y1              =   1140
      Y2              =   2590
   End
   Begin VB.Line RLine 
      BorderWidth     =   2
      Index           =   1
      X1              =   6240
      X2              =   6240
      Y1              =   1140
      Y2              =   2590
   End
   Begin VB.Label Label3 
      Caption         =   "<< A little bit oversized, isn't it ? >>"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   16110
      TabIndex        =   13
      Top             =   1710
      Width           =   2595
   End
   Begin VB.Line BLine 
      BorderColor     =   &H00000000&
      BorderWidth     =   2
      Index           =   1
      X1              =   30
      X2              =   3570
      Y1              =   3690
      Y2              =   3690
   End
End
Attribute VB_Name = "NoopFlights"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim TotalStartZeit
Dim TotalLines As Long
Dim MyFullMode As Boolean
Dim MyUpdateMode As Boolean
Dim FormIsBusy As Boolean
Dim ImportIsBusy As Boolean
Dim SeriousError As Boolean
Dim ArrFlightLineNo(2) As Long
Dim DepFlightLineNo(2) As Long
Dim SelRegnList As String
Dim LimitColor As Long
Dim PrflCol As Long
Dim FtypCol As Long
Dim FlnoCol As Long
Dim AdidCol As Long
Dim TimeCol As Long
Dim DoopCol As Long
Dim Act3Col As Long
Dim Apc3Col As Long
Dim Via3Col As Long
Dim StypCol As Long
Dim FltnCol As Long
Dim TifdCol As Long
Dim RtypCol As Long
Dim RegnCol As Long
Dim FkeyCol As Long
Dim RkeyCol As Long
Dim UrnoCol As Long
Dim RotaCol As Long
Dim RemaCol As Long


Private Sub AlcTabCombo_ComboSelChanged(Index As Integer, ByVal LineNo As Long, ByVal ColNo As Long, ByVal OldValue As String, ByVal NewValues As String)
    Dim NewAlc As String
    NewAlc = Trim(GetItem(NewValues, 1, ","))
    If NewAlc = "" Then NewAlc = Trim(GetItem(NewValues, 2, ","))
    If NewAlc = "***" Then NewAlc = "-ALL-"
    If (Not FormIsBusy) And (NewAlc <> CurPos(0).Caption) Then
        CurPos(0).Caption = NewAlc
        If NewAlc <> "-ALL-" Then
            AdjustScrollMaster FlnoCol, 3, NewAlc
        Else
            AdjustScrollMaster -1, -1, ""
        End If
        CreateDistinctFlno NewAlc
    End If
End Sub

Private Sub chkSearch_Click(Index As Integer)
    If Index < 3 Then
        If chkSearch(Index).Value = 1 Then
            NoopList(2).SetFocus
            chkSearch(Index + 4).Value = 1
            chkSearch(Index + 4).BackColor = LightGreen
            txtSearch(Index).BackColor = LightestGreen
            chkSearch(Index).BackColor = LightGreen
            Me.Refresh
            SearchInList Index
            chkSearch(Index).Value = 0
        Else
            chkSearch(Index + 4).Value = 0
            chkSearch(Index + 4).BackColor = vbButtonFace
            txtSearch(Index).BackColor = vbButtonFace
            chkSearch(Index).BackColor = vbButtonFace
            Me.Refresh
        End If
    End If
End Sub

Private Sub chkShrink_Click(Index As Integer)
    If chkShrink(Index).Value = 1 Then
        FormIsBusy = True
        AdjustScrollMaster -2, -1, ""
        FormIsBusy = False
        AdjustShrinkButton False
        chkShrink(Index).Value = 0
    End If
End Sub

Private Sub CurPos_MouseDown(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
    Dim CallIndex As Integer
    Dim ForWhat As Integer
    Dim CurAction As Integer
    If Index = 4 Then
        If Button = 2 Then
            CallIndex = DefineColorAction(CurPos(Index).BackColor, ForWhat, CurAction)
            CallServerAction CallIndex, Val(CurPos(Index).Caption), ForWhat, True, ""
        Else
            If Val(CurPos(Index).Caption) > 0 Then JumpToColoredLine CurPos(Index).BackColor, 1, -1
        End If
    End If
End Sub

Private Sub FlnoTabCombo_ComboSelChanged(Index As Integer, ByVal LineNo As Long, ByVal ColNo As Long, ByVal OldValue As String, ByVal NewValues As String)
    Dim NewFlno As String
    Dim CurAlc As String
    NewFlno = GetItem(NewValues, 1, ",")
    If (Not FormIsBusy) And (NewFlno <> CurPos(1).Caption) Then
        CurPos(1).Caption = NewFlno
        If NewFlno <> "-ALL-" Then
            AdjustScrollMaster FlnoCol, 9, NewFlno
        Else
            CurAlc = CurPos(0).Caption
            CurPos(0).Caption = ""
            AlcTabCombo_ComboSelChanged 0, 0, 0, "***", CurAlc
        End If
    End If

End Sub

Private Sub Form_Activate()
    If MyUpdateMode Then chkWork(2).Value = 1
    CheckKeyboardState GetKeybdState
End Sub

Private Sub Form_GotFocus()
    CheckKeyboardState GetKeybdState
End Sub

Private Sub Form_Load()
    MyFullMode = NoopFullMode
    MyUpdateMode = Not NoopFullMode
    If MyFullMode Then
        Me.Caption = Me.Caption & " (For Full Mode) " & GetItem(MySetUp.MyFileInfo.Caption, 1, " ")
    Else
        Me.Caption = Me.Caption & " (For Update Mode) " & GetItem(MySetUp.MyFileInfo.Caption, 1, " ")
    End If
    InitLists
    InitShrinkDsp
    If MyUpdateMode Then
        chkWork(5).Enabled = False
    End If
    If MainDialog.DataSystemType <> "OAFOS" Then
        FlnoTabCombo(0).Visible = True
        RegnTabCombo(0).Visible = False
    Else
        FlnoTabCombo(0).Visible = False
        RegnTabCombo(0).Visible = True
    End If
    ShowSecondList True, "5,2,0"
    MySetUp.NoopListIsOpen = True
    MySetUp.SynchronizeTimeDisplays
    SelRegnList = ""
    LimitColor = vbBlue
    txtMinGrdTime.Text = CStr(MinGrdTime)
    txtMinGrdTime.Tag = CStr(MinGrdTime)
    txtMinGrdTime.BackColor = vbWhite
    txtMinGrdTime.ForeColor = vbBlack
End Sub
Private Sub InitShrinkDsp()
    lblShrink(0).BackColor = FindFkeyColor(lblShrink(0).Tag)
    lblShrink(1).BackColor = FindFkeyColor(lblShrink(1).Tag)
    lblShrink(2).BackColor = FindFkeyColor(lblShrink(2).Tag)
    lblShrink(3).BackColor = FindFkeyColor(lblShrink(3).Tag)
    lblShrink(4).BackColor = FindFkeyColor(lblShrink(4).Tag)
    lblShrink(5).BackColor = SrvFkeyCnt(10).BackColor
End Sub
Private Sub AdjustShrinkLabels()
    Dim i As Integer
    Dim FkeyCount As Long
    Dim ObjIdx As Integer
    Dim ObjKey As String
    For i = 0 To 4
        ObjKey = Left(lblShrink(i).Tag, 1)
        ObjIdx = Val(Mid(lblShrink(i).Tag, 2))
        Select Case ObjKey
            Case "S"
                FkeyCount = Val(SrvFkeyCnt(ObjIdx).Caption)
            Case "C"
                FkeyCount = Val(CliFkeyCnt(ObjIdx).Caption)
            Case Else
                FkeyCount = 0
        End Select
        If FkeyCount > 0 Then
            lblShrink(i).BorderStyle = 1
            lblShrink(i).ZOrder
        Else
            lblShrink(i).BorderStyle = 0
            lblShrink(i + 6).ZOrder
        End If
    Next
    FkeyCount = Val(SrvFkeyCnt(10).Caption) + Val(CliFkeyCnt(ObjIdx).Caption)
    If FkeyCount > 0 Then
        lblShrink(5).BorderStyle = 1
        lblShrink(5).ZOrder
    Else
        lblShrink(5).BorderStyle = 0
        lblShrink(11).ZOrder
    End If
    AdjustShrinkButton False
End Sub
Private Sub AdjustShrinkButton(ShowDiff As Boolean)
    Dim i As Integer
    Dim Result As String
    Result = ""
    For i = 0 To 5
        Result = Result & Trim(Str(lblShrink(i).BorderStyle)) & ","
    Next
    If (ShowDiff) And (Result <> chkShrink(0).Tag) Then
        chkShrink(0).BackColor = vbRed
        chkShrink(0).ForeColor = vbWhite
    Else
        If Not ShowDiff Then chkShrink(0).Tag = Result
        chkShrink(0).BackColor = vbButtonFace
        chkShrink(0).ForeColor = vbBlack
    End If
End Sub

Private Sub Form_Unload(Cancel As Integer)
    MySetUp.NoopListIsOpen = False
End Sub

Private Sub lblShrink_Click(Index As Integer)
    Dim CurIdx As Integer
    If Index < 6 Then CurIdx = Index Else CurIdx = Index - 6
    If lblShrink(CurIdx).BorderStyle = 0 Then
        lblShrink(CurIdx).BorderStyle = 1
        lblShrink(CurIdx).ZOrder
    Else
        lblShrink(CurIdx).BorderStyle = 0
        lblShrink(CurIdx + 6).ZOrder
    End If
    AdjustShrinkButton True
End Sub
Private Function FindFkeyColor(ObjTag As String) As Long
    Dim ObjIdx As Integer
    Dim ObjKey As String
    ObjKey = Left(ObjTag, 1)
    ObjIdx = Val(Mid(ObjTag, 2))
    Select Case ObjKey
        Case "S"
            FindFkeyColor = SrvFkeyCnt(ObjIdx).BackColor
        Case "C"
            FindFkeyColor = CliFkeyCnt(ObjIdx).BackColor
        Case Else
            FindFkeyColor = 0
    End Select
End Function
Private Sub InitLists()
    Dim TmpTest As String
    Dim myHeaderList As String
    Dim HdrLenLst As String
    Dim ActColLst As String
    Dim ColAlign As String
    Dim i As Integer
    Dim j As Integer
    Dim tmpHeader As String
    Dim tmpHedLen As String
    Dim tmpLen As Integer
    Dim tmpdat As String
    Dim TotalSize As Long
    Dim tmpSize As Long
    Dim tmpVal As Long
    Dim tmpFkeyCol As Integer
    Dim tmpColor As String
    PrflCol = 0
    FtypCol = 1
    FlnoCol = 2
    AdidCol = 3
    TimeCol = 4
    DoopCol = 5
    Act3Col = 6
    Apc3Col = 7
    Via3Col = 8
    StypCol = 9
    RegnCol = 10
    FltnCol = 11
    TifdCol = 12
    RtypCol = 13
    FkeyCol = 14
    RkeyCol = 15
    UrnoCol = 16
    RotaCol = 17
    RemaCol = 18
    tmpFkeyCol = CInt(RegnCol + 2)
    tmpColor = Str(vbBlack) & "," & Str(LightGray)
    tmpHeader = "A,S, Flights,I,Time,D,ACT,APC,VIA,S,REGN,Linked, Date   Time,I,  FKEY,RKEY,URNO,ROTAKEY,Remark"
    ActColLst = "1,1,9,1,5,1,3,3,3,1,6,9,12,1,18,10,10,10,"
    ColAlign = ""
    HdrLenLst = ""
    TotalSize = 0
    i = 0
    Do
        i = i + 1
        tmpdat = GetItem(ActColLst, i, ",")
        If tmpdat <> "" Then
            tmpLen = Val(tmpdat)
            If tmpLen > 0 Then
                tmpSize = (TextWidth(Space(tmpLen)) / Screen.TwipsPerPixelX) + 7
            Else
                tmpSize = 0
            End If
            HdrLenLst = HdrLenLst & Str(tmpSize) & ","
            If i < tmpFkeyCol Then TotalSize = TotalSize + tmpSize
            ColAlign = ColAlign & "L,"
        End If
    Loop While tmpdat <> ""
    HdrLenLst = HdrLenLst & "1000"
    ColAlign = ColAlign & "L"
    ActColLst = ActColLst & "40"
    For i = 0 To 2
        NoopList(i).ResetContent
        NoopList(i).ShowVertScroller True
        NoopList(i).ShowHorzScroller True
        NoopList(i).VScrollMaster = False
        NoopList(i).FontName = "Courier New"
        NoopList(i).SetTabFontBold True
        NoopList(i).HeaderFontSize = 17
        NoopList(i).FontSize = 17
        NoopList(i).LineHeight = 17
        NoopList(i).LeftTextOffset = 2
        NoopList(i).LifeStyle = True
        'NoopList(i).CursorLifeStyle = True
        NoopList(i).GridLineColor = DarkGray
        NoopList(i).HeaderLengthString = HdrLenLst
        NoopList(i).HeaderString = tmpHeader
        NoopList(i).ColumnAlignmentString = ColAlign
        NoopList(i).ColumnWidthString = ActColLst
        NoopList(i).Height = 24 * NoopList(i).LineHeight * Screen.TwipsPerPixelY
        NoopList(i).CreateDecorationObject "CellDiff", "R,T,B,L", "2,2,2,2", tmpColor & "," & tmpColor
        NoopList(i).CreateDecorationObject "SrvRota", "BTR", "8", Trim(Str(LightRed))
        NoopList(i).CreateDecorationObject "CliRota", "BTL", "8", Trim(Str(LightRed))
        NoopList(i).CreateDecorationObject "RotaDiff", "L,R,T,B,L", "-1,2,2,2,2", Trim(Str(LightRed)) & "," & tmpColor & "," & tmpColor
        NoopList(i).CreateDecorationObject "RotaDeny", "L", "-1", Trim(Str(LightRed))
        NoopList(i).CreateDecorationObject "SplitArr", "TL", "16", Trim(Str(LightRed))
        NoopList(i).CreateDecorationObject "SplitDep", "BR", "16", Trim(Str(LightRed))
        NoopList(i).CreateCellObj "MultiSel", vbBlue, vbWhite, 17, False, False, True, 0, "Courier New"
        BottomLabels(i).Top = NoopList(i).Top + NoopList(i).Height - 7 * Screen.TwipsPerPixelY
        RotaList(i).ResetContent
        RotaList(i).ShowVertScroller True
        RotaList(i).FontName = "Courier New"
        RotaList(i).SetTabFontBold True
        RotaList(i).HeaderFontSize = 17
        RotaList(i).FontSize = 17
        RotaList(i).LineHeight = 17
        RotaList(i).LifeStyle = True
        'RotaList(i).CursorLifeStyle = True
        RotaList(i).GridLineColor = DarkGray
        RotaList(i).HeaderLengthString = "60,60,60"
        RotaList(i).HeaderString = "Line,RotaLine,xx"
        RotaList(i).ColumnAlignmentString = "R,R,R"
        RotaList(i).ColumnWidthString = "6,6,6"
        RotaLink(i).ResetContent
        RotaLink(i).ShowVertScroller False
        RotaLink(i).ShowHorzScroller False
        RotaLink(i).FontName = "Courier New"
        RotaLink(i).SetTabFontBold True
        RotaLink(i).HeaderFontSize = 17
        RotaLink(i).FontSize = 17
        RotaLink(i).LineHeight = 17
        RotaLink(i).LeftTextOffset = 2
        RotaLink(i).LifeStyle = True
        'RotaLink(i).CursorLifeStyle = True
        RotaLink(i).GridLineColor = DarkGray
        RotaLink(i).HeaderLengthString = HdrLenLst
        RotaLink(i).HeaderString = tmpHeader
        RotaLink(i).ColumnAlignmentString = ColAlign
        RotaLink(i).ColumnWidthString = ActColLst
        RotaLink(i).Height = 3 * RotaLink(i).LineHeight * Screen.TwipsPerPixelY
        RotaLink(i).CreateDecorationObject "CellDiff", "R,T,B,L", "2,2,2,2", tmpColor & "," & tmpColor
        RotaLink(i).CreateDecorationObject "RotaDiff", "L,R,T,B,L", "-1,2,2,2,2", Trim(Str(LightRed)) & "," & tmpColor & "," & tmpColor
        RotaLink(i).CreateDecorationObject "RotaDeny", "L", "-1", Trim(Str(LightRed))
        RotaLink(i).CreateDecorationObject "SplitArr", "TL", "16", Trim(Str(LightRed))
        RotaLink(i).CreateDecorationObject "SplitDep", "BR", "16", Trim(Str(LightRed))
        RotaLink(i).CreateCellObj "MultiSel", vbBlue, vbWhite, 17, False, False, True, 0, "Courier New"
        RotaLink(i).MainHeaderOnly = True
        If i < 2 Then
            NoopList(i).DateTimeSetColumn TimeCol
            NoopList(i).DateTimeSetInputFormatString TimeCol, "YYYYMMDDhhmm"
            NoopList(i).DateTimeSetOutputFormatString TimeCol, "hh':'mm"
            NoopList(i).DateTimeSetColumn TifdCol
            NoopList(i).DateTimeSetInputFormatString TifdCol, "YYYYMMDDhhmm"
            NoopList(i).DateTimeSetOutputFormatString TifdCol, "DDMMMYY'/'hhmm"
            RotaLink(i).DateTimeSetColumn TimeCol
            RotaLink(i).DateTimeSetInputFormatString TimeCol, "YYYYMMDDhhmm"
            RotaLink(i).DateTimeSetOutputFormatString TimeCol, "hh':'mm"
            RotaLink(i).DateTimeSetColumn TifdCol
            RotaLink(i).DateTimeSetInputFormatString TifdCol, "YYYYMMDDhhmm"
            RotaLink(i).DateTimeSetOutputFormatString TifdCol, "DDMMMYY'/'hhmm"
        End If
    Next
    RotaLink(0).SetMainHeaderValues "10,4,4,1", "Import File Rotation Data,Linked Flight,System Data,Remark", "12632256,12632256,12632256,12632256"
    RotaLink(0).MainHeader = True
    RotaLink(1).SetMainHeaderValues "10,4,4,1", "UFIS AODB Rotation Data,Linked Flight,System Data,Remark", "12632256,12632256,12632256,12632256"
    RotaLink(1).MainHeader = True
    TotalSize = TotalSize + 16  'Plus VertScrollBar
    RLine(0).X1 = TopLabels.Left '- Screen.TwipsPerPixelX
    RLine(0).X2 = RLine(0).X1
    NoopList(0).Left = RLine(0).X1 + 1 * Screen.TwipsPerPixelX
    NoopList(0).Width = TotalSize * Screen.TwipsPerPixelX
    NoopList(0).SetMainHeaderValues "10,4,4,1", "Import File Flight Data,Rotation Data,System Data,Remark", "12632256,12632256,12632256,12632256"
    NoopList(0).MainHeader = True
    BottomLabels(0).Left = NoopList(0).Left
    BottomLabels(0).Width = NoopList(0).Width
    'HardCoded
    RLine(1).X1 = NoopList(0).Left + NoopList(0).Width + Screen.TwipsPerPixelX
    RLine(1).X2 = RLine(1).X1
    RLine(2).X1 = RLine(1).X1 '+ 2 * Screen.TwipsPerPixelX
    RLine(2).X2 = RLine(2).X1
    NoopList(2).Left = RLine(2).X1 + Screen.TwipsPerPixelX
    tmpSize = TextWidth("ABC1234 S") / Screen.TwipsPerPixelX + 6
    tmpHedLen = Trim(Str(tmpSize))
    tmpVal = TextWidth("31DEC02") / Screen.TwipsPerPixelX + 6
    tmpHedLen = tmpHedLen & "," & Trim(Str(tmpVal))
    tmpSize = tmpSize + tmpVal
    tmpVal = TextWidth("A") / Screen.TwipsPerPixelX + 6
    tmpHedLen = tmpHedLen & "," & Trim(Str(tmpVal))
    tmpSize = tmpSize + tmpVal
    tmpVal = TextWidth("1") / Screen.TwipsPerPixelX + 6
    tmpHedLen = tmpHedLen & "," & Trim(Str(tmpVal))
    tmpVal = TextWidth("12345678") / Screen.TwipsPerPixelX + 6
    tmpHedLen = tmpHedLen & "," & Trim(Str(tmpVal))
    NoopList(2).HeaderLengthString = tmpHedLen
    NoopList(2).HeaderString = " Flights,  Day,I,D,  LineNo"
    NoopList(2).ColumnAlignmentString = "L,L,L,L,R"
    NoopList(2).ColumnWidthString = "9,7,1,1,8"
    NoopList(2).SetMainHeaderValues "4,1", "SYNCHRONIZED SCROLL,  ", "12632256,12632256"
    NoopList(2).MainHeader = True
    NoopList(2).Width = (tmpSize + 32) * Screen.TwipsPerPixelX
    NoopList(2).VScrollMaster = True
    NoopList(2).ShowVertScroller True
    NoopList(2).DateTimeSetColumn 1
    NoopList(2).DateTimeSetInputFormatString 1, "YYYYMMDD"
    NoopList(2).DateTimeSetOutputFormatString 1, "DDMMMYY"
    NoopList(2).ShowVertScroller True
    
    RotaLink(2).HeaderLengthString = tmpHedLen
    RotaLink(2).HeaderString = " Flights,  Day,I,D,  LineNo"
    RotaLink(2).ColumnAlignmentString = "L,L,L,L,R"
    RotaLink(2).ColumnWidthString = "9,7,1,1,8"
    RotaLink(2).SetMainHeaderValues "4,1", "Selected Flight,  ", "12632256,12632256"
    RotaLink(2).MainHeader = True
    RotaLink(2).DateTimeSetColumn 1
    RotaLink(2).DateTimeSetInputFormatString 1, "YYYYMMDD"
    RotaLink(2).DateTimeSetOutputFormatString 1, "DDMMMYY"
    BottomLabels(2).Left = NoopList(2).Left
    BottomLabels(2).Width = NoopList(2).Width
    RLine(3).X1 = NoopList(2).Left + NoopList(2).Width '+ Screen.TwipsPerPixelX
    RLine(3).X2 = RLine(3).X1
    RLine(4).X1 = RLine(3).X1 + Screen.TwipsPerPixelX
    RLine(4).X2 = RLine(4).X1
    NoopList(1).Left = RLine(4).X1 + Screen.TwipsPerPixelX
    NoopList(1).Width = TotalSize * Screen.TwipsPerPixelX
    NoopList(1).SetMainHeaderValues "10,4,4,1", "UFIS AODB Flight Data,Rotation Data,System Data,Remark", "12632256,12632256,12632256,12632256"
    NoopList(1).MainHeader = True
    BottomLabels(1).Left = NoopList(1).Left
    BottomLabels(1).Width = NoopList(1).Width
    'HardCoded
    RLine(5).X1 = NoopList(1).Left + NoopList(1).Width + 2 * Screen.TwipsPerPixelX
    RLine(5).X2 = RLine(5).X1
    TopLabels.Width = RLine(5).X1 - RLine(0).X1 + 2 * Screen.TwipsPerPixelX
    BLine(0).X2 = TopLabels.Width - Screen.TwipsPerPixelX
    BLine(1).X1 = BLine(0).X1
    BLine(1).X2 = BLine(0).X2
    RotaLink(0).Left = NoopList(0).Left
    RotaLink(0).Width = NoopList(0).Width
    RotaLink(1).Left = NoopList(1).Left
    RotaLink(1).Width = NoopList(1).Width
    RotaLink(2).Left = NoopList(2).Left
    RotaLink(2).Width = NoopList(2).Width
    
    AlcTabCombo(0).ResetContent
    AlcTabCombo(0).ShowVertScroller False
    AlcTabCombo(0).ShowHorzScroller False
    AlcTabCombo(0).VScrollMaster = False
    AlcTabCombo(0).FontName = "Courier New"
    AlcTabCombo(0).SetTabFontBold True
    AlcTabCombo(0).HeaderFontSize = 17
    AlcTabCombo(0).FontSize = 17
    AlcTabCombo(0).LineHeight = 19
    AlcTabCombo(0).GridLineColor = vbBlack
    AlcTabCombo(0).HeaderLengthString = Str(AlcTabCombo(0).Width / Screen.TwipsPerPixelX)
    AlcTabCombo(0).HeaderString = "ALC"
    AlcTabCombo(0).ColumnAlignmentString = "L"
    AlcTabCombo(0).ColumnWidthString = "500"
    AlcTabCombo(0).Height = 1 * AlcTabCombo(0).LineHeight * Screen.TwipsPerPixelY
    AlcTabCombo(0).MainHeaderOnly = True
    AlcTabCombo(0).InsertTextLine ",,", False
    AlcTabCombo(0).SetCurrentSelection 0
    AlcTabCombo(0).CreateComboObject "AirlCodes", 0, 3, "EDIT", 400
    AlcTabCombo(0).SetComboColumn "AirlCodes", 0
    AlcTabCombo(0).ComboMinWidth = 30
    AlcTabCombo(0).EnableInlineEdit True
    AlcTabCombo(0).NoFocusColumns = "0"
    AlcTabCombo(0).ComboSetColumnLengthString "AirlCodes", "32,32,350"
    TmpTest = ",,,"
    AlcTabCombo(0).ComboAddTextLines "AirlCodes", TmpTest, vbLf
    AlcTabCombo(0).RedrawTab
    Load AlcTabCombo(1)
    AlcTabCombo(1).Visible = False
    
    RegnTabCombo(0).ResetContent
    RegnTabCombo(0).ShowVertScroller False
    RegnTabCombo(0).ShowHorzScroller False
    RegnTabCombo(0).VScrollMaster = False
    RegnTabCombo(0).FontName = "Courier New"
    RegnTabCombo(0).SetTabFontBold True
    RegnTabCombo(0).HeaderFontSize = 17
    RegnTabCombo(0).FontSize = 17
    RegnTabCombo(0).LineHeight = 19
    RegnTabCombo(0).GridLineColor = vbBlack
    RegnTabCombo(0).HeaderLengthString = Str(RegnTabCombo(0).Width / Screen.TwipsPerPixelX)
    RegnTabCombo(0).HeaderString = "ALC"
    RegnTabCombo(0).ColumnAlignmentString = "L"
    RegnTabCombo(0).ColumnWidthString = "12"
    RegnTabCombo(0).Height = 1 * RegnTabCombo(0).LineHeight * Screen.TwipsPerPixelY
    RegnTabCombo(0).MainHeaderOnly = True
    RegnTabCombo(0).InsertTextLine ",,", False
    RegnTabCombo(0).SetCurrentSelection 0
    RegnTabCombo(0).CreateComboObject "RegnCodes", 0, 1, "EDIT", 105
    RegnTabCombo(0).SetComboColumn "RegnCodes", 0
    RegnTabCombo(0).ComboMinWidth = 30
    RegnTabCombo(0).EnableInlineEdit True
    RegnTabCombo(0).NoFocusColumns = "0"
    RegnTabCombo(0).ComboSetColumnLengthString "RegnCodes", "105"
    TmpTest = ","
    RegnTabCombo(0).ComboAddTextLines "RegnCodes", TmpTest, vbLf
    RegnTabCombo(0).RedrawTab
    Load RegnTabCombo(1)
    RegnTabCombo(1).Visible = False
    
    FlnoTabCombo(0).ResetContent
    FlnoTabCombo(0).ShowVertScroller False
    FlnoTabCombo(0).ShowHorzScroller False
    FlnoTabCombo(0).VScrollMaster = False
    FlnoTabCombo(0).FontName = "Courier New"
    FlnoTabCombo(0).SetTabFontBold True
    FlnoTabCombo(0).HeaderFontSize = 17
    FlnoTabCombo(0).FontSize = 17
    FlnoTabCombo(0).LineHeight = 19
    FlnoTabCombo(0).GridLineColor = vbBlack
    FlnoTabCombo(0).HeaderLengthString = Str(FlnoTabCombo(0).Width / Screen.TwipsPerPixelX)
    FlnoTabCombo(0).HeaderString = "ALC"
    FlnoTabCombo(0).ColumnAlignmentString = "L"
    FlnoTabCombo(0).ColumnWidthString = "12"
    FlnoTabCombo(0).Height = 1 * FlnoTabCombo(0).LineHeight * Screen.TwipsPerPixelY
    FlnoTabCombo(0).MainHeaderOnly = True
    FlnoTabCombo(0).InsertTextLine ",,", False
    FlnoTabCombo(0).SetCurrentSelection 0
    FlnoTabCombo(0).CreateComboObject "FlnoCodes", 0, 1, "EDIT", 105
    FlnoTabCombo(0).SetComboColumn "FlnoCodes", 0
    FlnoTabCombo(0).ComboMinWidth = 30
    FlnoTabCombo(0).EnableInlineEdit True
    FlnoTabCombo(0).NoFocusColumns = "0"
    FlnoTabCombo(0).ComboSetColumnLengthString "FlnoCodes", "105"
    TmpTest = ","
    FlnoTabCombo(0).ComboAddTextLines "FlnoCodes", TmpTest, vbLf
    FlnoTabCombo(0).RedrawTab
    Load FlnoTabCombo(1)
    FlnoTabCombo(1).Visible = False
    
    Me.Width = TopLabels.Width + 2 * TopLabels.Left + 6 * Screen.TwipsPerPixelX
    Me.Left = 0
    Me.Top = FlightExtract.Top
    Me.Height = FlightExtract.Height
End Sub
Private Sub ToggleRotationView(ShowRotation As Boolean)
    Dim HdrLenLst As String
    Dim ActColLst As String
    Dim ColAlign As String
    Dim tmpHeader As String
    Dim tmpHedLen As String
    Dim tmpLen As Integer
    Dim tmpdat As String
    Dim TotalSize As Long
    Dim tmpSize As Long
    Dim tmpFkeyCol As Integer
    Dim i As Integer
    If ShowRotation Then
        tmpFkeyCol = CInt(RegnCol + 12)
        tmpHeader = "A,S, Flights,I,Time,D,ACT,APC,VIA,S,REGN,Linked, Date   Time,I,  FKEY,RKEY,URNO,ROTAKEY,Remark"
        ActColLst = "1,1,0,0,5,0,3,3,3,1,6,9,12,1,18,10,10,10,"
    Else
        tmpFkeyCol = CInt(RegnCol + 12)
        tmpHeader = "A,S, Flights,I,Time,D,ACT,APC,VIA,S,REGN,Linked, Date   Time,I,  FKEY,RKEY,URNO,ROTAKEY,Remark"
        ActColLst = "1,1,9,1,5,1,3,3,3,1,6,9,12,1,18,10,10,10,"
    End If
    ColAlign = ""
    HdrLenLst = ""
    TotalSize = 0
    i = 0
    Do
        i = i + 1
        tmpdat = GetItem(ActColLst, i, ",")
        If tmpdat <> "" Then
            tmpLen = Val(tmpdat)
            If tmpLen > 0 Then
                tmpSize = (TextWidth(Space(tmpLen)) / Screen.TwipsPerPixelX) + 7
            Else
                tmpSize = 0
            End If
            HdrLenLst = HdrLenLst & Str(tmpSize) & ","
            If i < tmpFkeyCol Then TotalSize = TotalSize + tmpSize
            ColAlign = ColAlign & "L,"
        End If
    Loop While tmpdat <> ""
    HdrLenLst = HdrLenLst & "1000"
    ColAlign = ColAlign & "L"
    ActColLst = ActColLst & "40"
    If ShowRotation Then
        For i = 0 To 1
            NoopList(i).HeaderLengthString = HdrLenLst
            NoopList(i).HeaderString = tmpHeader
            NoopList(i).ColumnAlignmentString = ColAlign
            NoopList(i).ColumnWidthString = ActColLst
            'NoopList(i).DateTimeSetOutputFormatString TimeCol, "hhmm"
            NoopList(i).ShowVertScroller False
            NoopList(i).RedrawTab
            RotaLink(i).HeaderLengthString = HdrLenLst
            RotaLink(i).HeaderString = tmpHeader
            RotaLink(i).ColumnAlignmentString = ColAlign
            RotaLink(i).ColumnWidthString = ActColLst
            'RotaLink(i).DateTimeSetOutputFormatString TimeCol, "hhmm"
            RotaLink(i).ShowVertScroller False
            RotaLink(i).RedrawTab
        Next
    Else
        For i = 0 To 1
            NoopList(i).HeaderLengthString = HdrLenLst
            NoopList(i).HeaderString = tmpHeader
            NoopList(i).ColumnAlignmentString = ColAlign
            NoopList(i).ColumnWidthString = ActColLst
            'NoopList(i).DateTimeSetOutputFormatString TimeCol, "DDMMMYY'/'hhmm"
            NoopList(i).ShowVertScroller True
            NoopList(i).RedrawTab
            RotaLink(i).HeaderLengthString = HdrLenLst
            RotaLink(i).HeaderString = tmpHeader
            RotaLink(i).ColumnAlignmentString = ColAlign
            RotaLink(i).ColumnWidthString = ActColLst
            'RotaLink(i).DateTimeSetOutputFormatString TimeCol, "DDMMMYY'/'hhmm"
            RotaLink(i).ShowVertScroller True
            RotaLink(i).RedrawTab
        Next
    End If
    NoopList(0).SetMainHeaderValues "10,4,4,1", "Import File Flight Data,Rotation Data,System Data,Remark", "12632256,12632256,12632256,12632256"
    NoopList(0).MainHeader = True
    NoopList(1).SetMainHeaderValues "10,4,4,1", "UFIS AODB Flight Data,Rotation Data,System Data,Remark", "12632256,12632256,12632256,12632256"
    NoopList(1).MainHeader = True
    RotaLink(0).SetMainHeaderValues "10,4,4,1", "Import File Rotation Data,Linked Flight,System Data,Remark", "12632256,12632256,12632256,12632256"
    RotaLink(0).MainHeader = True
    RotaLink(1).SetMainHeaderValues "10,4,4,1", "UFIS AODB Rotation Data,Linked Flight,System Data,Remark", "12632256,12632256,12632256,12632256"
    RotaLink(1).MainHeader = True
    If ShowRotation Then
        NoopList(0).OnHScrollTo 1
        NoopList(1).OnHScrollTo 1
    Else
        NoopList(0).OnHScrollTo 0
        NoopList(1).OnHScrollTo 0
    End If
    
End Sub

Private Sub chkWork_Click(Index As Integer)
    Dim i As Integer
    Dim tmpResult As String
    On Error Resume Next
    If chkWork(Index).Value = 0 Then chkWork(Index).BackColor = vbButtonFace
    Select Case Index
        Case 0
            'Close
            If chkWork(Index).Value = 1 Then
                RecFilter.chkTool(4).Value = 0
                Unload Me
                Exit Sub
            End If
        Case 1
            'Shrink
            If chkWork(Index).Value = 1 Then
                fraMinGrd.Visible = False
                Me.Refresh
                AdjustShrinkLabels
                DspShrink.Visible = True
            Else
                DspShrink.Visible = False
                fraMinGrd.Visible = True
                Me.Refresh
            End If
            FormIsBusy = True
            AdjustScrollMaster -2, -1, ""
            FormIsBusy = False
        Case 2
            'AODB
            chkWork(6).Value = 0
            chkWork(13).Value = 0
            If chkWork(Index).Value = 1 Then
                fraMinGrd.Visible = False
                TotalStartZeit = Timer
                FormIsBusy = True
                chkWork(5).Value = 0
                chkWork(1).Value = 0
                chkWork(3).Value = 0
                chkWork(4).Value = 0
                ShowSecondList True, "2,0"
                CreateFlightLists
                tmpResult = Trim(Str(Timer - TotalStartZeit))
                If Left(tmpResult, 1) = "." Then tmpResult = "0" & tmpResult
                tmpResult = GetItem(tmpResult, 1, ".") & "." & Left(GetItem(tmpResult, 2, "."), 3) & "s"
                TotalSecs.Caption = tmpResult
                chkWork(1).Enabled = False
                chkWork(3).Enabled = True
                chkWork(6).Enabled = False
                chkWork(3).Value = 1
                chkWork(6).Enabled = True
                FormIsBusy = False
                txtMinGrdTime.BackColor = vbWhite
                txtMinGrdTime.ForeColor = vbBlack
                txtMinGrdTime.Tag = CStr(MinGrdTime)
                fraMinGrd.Visible = True
            Else
                chkWork(1).Enabled = False
                chkWork(3).Enabled = False
                chkWork(4).Enabled = False
                chkWork(6).Enabled = False
            End If
        Case 3
            'Flights
            If chkWork(Index).Value = 1 Then
                FormIsBusy = True
                chkWork(4).Value = 0
                chkWork(Index).Enabled = False
                If chkWork(5).Value = 1 Then
                    'Doubles
                    CompareFlightLists 3, FkeyCol, FkeyCol
                Else
                    'AODB
                    CompareFlightLists 2, FkeyCol, FkeyCol
                    If MainDialog.DataSystemType <> "OAFOS" Then
                        ShowSecondList True, "2,13,4,1,6,0"
                    Else
                        ShowSecondList True, "2,13,4,6,0"
                    End If
                End If
                tmpResult = Trim(Str(Timer - TotalStartZeit))
                If Left(tmpResult, 1) = "." Then tmpResult = "0" & tmpResult
                tmpResult = GetItem(tmpResult, 1, ".") & "." & Left(GetItem(tmpResult, 2, "."), 3) & "s"
                TotalSecs.Caption = tmpResult
                chkWork(1).Enabled = True
                chkWork(4).Enabled = True
                FormIsBusy = False
            End If
        Case 4
            'Rotation
            If chkWork(Index).Value = 1 Then
                ToggleRotationView True
            Else
                ToggleRotationView False
            End If
        Case 5            'Doubles
            If chkWork(Index).Value = 1 Then
                TotalStartZeit = Timer
                FormIsBusy = True
                chkWork(2).Value = 0
                chkWork(1).Value = 0
                chkWork(3).Value = 0
                ShowSecondList False, "5,13,4,1,0"
                CreateFlightLists
                'chkWork(1).Enabled = True
                chkWork(3).Enabled = False
                chkWork(4).Enabled = False
                FormIsBusy = False
                tmpResult = Trim(Str(Timer - TotalStartZeit))
                If Left(tmpResult, 1) = "." Then tmpResult = "0" & tmpResult
                tmpResult = GetItem(tmpResult, 1, ".") & "." & Left(GetItem(tmpResult, 2, "."), 3) & "s"
                TotalSecs.Caption = tmpResult
            Else
                chkWork(1).Enabled = False
                chkWork(3).Enabled = False
                chkWork(4).Enabled = False
            End If
        Case 6  'Activate
            If chkWork(Index).Value = 1 Then
                If MainDialog.DataSystemType <> "OAFOS" Then
                    ShowSecondList True, "2,13,4,1,6,7,9,10,11,12,8,0"
                Else
                    ShowSecondList True, "2,13,4,6,7,14,15,12,0"
                End If
                For i = 7 To 12
                    chkWork(i).Enabled = True
                    If chkWork(i).Value = 1 Then chkWork(i).BackColor = LightGreen
                Next
                NoopList(2).SetCurrentSelection NoopList(2).GetCurrentSelected
            Else
                If MainDialog.DataSystemType <> "OAFOS" Then
                    ShowSecondList True, "2,13,4,1,6,0"
                Else
                    ShowSecondList True, "2,13,4,6,0"
                End If
                For i = 7 To 12
                    chkWork(i).Enabled = False
                    chkWork(i).BackColor = vbButtonFace
                Next
                ClearLineSelection
            End If
            NoopList(2).SetFocus
        Case 9      'Insert
            HandleActionButton FOR_INSERT
            NoopList(2).SetFocus
        Case 10     'Update
            HandleActionButton FOR_UPDATE
            NoopList(2).SetFocus
        Case 11     'Delete
            HandleActionButton FOR_DELETE
            NoopList(2).SetFocus
        Case 12 'Keep
            If chkWork(Index).Value = 0 Then
                ClearLineSelection
                chkWork(Index).Tag = ""
            Else
                chkWork(Index).Tag = "INIT"
            End If
            NoopList(2).SetCurrentSelection NoopList(2).GetCurrentSelected
            NoopList(2).SetFocus
        Case 13 'Details
            Form_Resize
            ShowMinGrdIndicator False
            If chkWork(Index).Value = 1 Then
                NoopList(2).SetCurrentSelection NoopList(2).GetCurrentSelected
            End If
            NoopList(2).SetFocus
        Case 14 'OAFOS single Import
            If chkWork(Index).Value = 1 Then
                chkWork(Index).BackColor = LightGreen
                OaFosRegnImport True, "OAL"
                chkWork(Index).Value = 0
            End If
        Case 15 'OAFOS full Import
            If chkWork(Index).Value = 1 Then
                chkWork(Index).BackColor = LightGreen
                OaFosRegnImport False, "OAL"
                chkWork(Index).Value = 0
            End If
        Case Else
    End Select
    If chkWork(Index).Value = 1 Then chkWork(Index).BackColor = LightGreen
    Screen.MousePointer = 0
End Sub

Private Sub OaFosRegnImport(SingleMode As Boolean, Alc3List As String)
    Dim MsgTxt As String
    Dim RetVal As Integer
    Dim LedIdx As Integer
    Dim CurLine As Long
    Dim MaxLine As Long
    Dim FirstRegnLine As Long
    Dim SelList As String
    Dim LinNbr As String
    Dim ItmNbr As Long
    Dim CurRegn As String
    Dim chkTime As String
    Dim CurPrfl As String
    Dim CurFtyp As String
    Dim tmpTime As String
    Dim tmpRota As String
    Dim RefRegn As String
    Dim CurAdid As String
    Dim tmpLine As String
    Dim ArrLine As String
    Dim DepLine As String
    Dim MinTime As String
    Dim MaxTime As String
    Dim ArrFldLst As String
    Dim DepFldLst As String
    Dim tmpFldLst As String
    Dim tmpCmd As String
    Dim tmpKey As String
    Dim OutResult As String
    Dim RefLine As Long
    Dim MidLine As Long
    Dim CurForeColor As Long
    Dim CurBackColor As Long
    Dim CurLineTag As String
    Dim SomeThingChanged As Boolean
    Dim SomeThingSent As Boolean
    Dim LineCode As String
    Dim StartZeit
    SomeThingSent = False
    RetVal = 1
    If (SingleMode = False) Or (chkWork(7).Value = 1) Then
        If SingleMode Then
            If SelRegnList <> "" Then
                If InStr(SelRegnList, ",") = 0 Then
                    MsgTxt = "Do you want to import the turnarounds of " & SelRegnList & "?"
                Else
                    MsgTxt = "Do you want to import the turnarounds of the selected aircrafts" & vbNewLine & SelRegnList & "?"
                End If
            Else
                OaFosCancel -1, True
                RetVal = -1
            End If
        Else
            MsgTxt = "Do you want to import the full list ?"
        End If
        If RetVal = 1 Then RetVal = MyMsgBox.CallAskUser(0, 0, 0, "Transaction Control", MsgTxt, "ask", "Yes;F,No", UserAnswer)
        Me.Refresh
    End If
    If RetVal = 1 Then
        Screen.MousePointer = 11
        StartZeit = Timer
        UsedSec.Caption = ""
        CedaProgress.Show , Me
        CedaProgress.Top = Me.Top + NoopList(1).Top + 15 * Screen.TwipsPerPixelY
        CedaProgress.Left = Me.Left + NoopList(1).Left + 4 * Screen.TwipsPerPixelX
        CedaProgress.Caption = "OA FOS Import Progress"
        If SingleMode = False Then CedaProgress.MsgList(0).ResetContent
        CedaProgress.Refresh
        UfisServer.BcSpool True
        CedaLed(0).FillColor = NormalGreen
        CedaLed(0).Visible = True
        CedaLed(0).Refresh
        CedaLed(1).FillColor = NormalGreen
        CedaLed(1).Visible = True
        CedaLed(1).Refresh
        LedIdx = 0
        ImportIsBusy = True
        MidLine = (NoopList(2).Height / (NoopList(2).LineHeight * Screen.TwipsPerPixelY) / 2) - 2
        'ArrFldLst = "REGN,ADID,FKEY,FTYP,STOA,ORG3,VIAL," & MainDialog.UseStypField
        'DepFldLst = "REGN,ADID,FKEY,FTYP,STOD,DES3,VIAL," & MainDialog.UseStypField
        ArrFldLst = ""
        DepFldLst = ""
        If SingleMode Then SelList = NoopList(2).GetLinesByStatusValue(1, 0) Else SelList = ""
        chkWork(12).Value = 0
        ClearLineSelection
        RefRegn = ""
        ArrLine = ""
        DepLine = ""
        OutResult = ""
        ItmNbr = -1
        CurLine = -1
        SomeThingChanged = False
        MaxLine = NoopList(2).GetLineCount - 1
        While CurLine < MaxLine
            If SingleMode Then
                ItmNbr = ItmNbr + 1
                LinNbr = GetRealItem(SelList, ItmNbr, ",")
                If LinNbr <> "" Then CurLine = Val(LinNbr) Else MaxLine = -1
            Else
                CurLine = CurLine + 1
                If CurLine > MaxLine Then MaxLine = -1
            End If
            If CurLine <= MaxLine Then
                If CurLine > MidLine Then
                    NoopList(2).OnVScrollTo CurLine - MidLine
                ElseIf CurLine < NoopList(2).GetVScrollPos Then
                    NoopList(2).OnVScrollTo CurLine
                End If
                NoopList(2).SetCurrentSelection CurLine
                Screen.MousePointer = 11
                RefLine = Val(NoopList(2).GetColumnValue(CurLine, 4))
                CurRegn = NoopList(0).GetColumnValue(RefLine, RegnCol)
                chkTime = GetImportCheckTime
                tmpTime = NoopList(0).GetColumnValue(RefLine, TimeCol)
                CurFtyp = NoopList(0).GetColumnValue(RefLine, FtypCol)
                If (CurFtyp = "X") Or (CurFtyp = "N") Then CurRegn = ""
                If CurRegn <> "" Then
                    CurPrfl = NoopList(0).GetColumnValue(RefLine, PrflCol)
                    If CurPrfl <> "!" Then
                        If tmpTime >= chkTime Then
                            If RefRegn = "" Then
                                RefRegn = CurRegn
                                MinTime = tmpTime
                                MaxTime = tmpTime
                                CurPos(3).Caption = "Turnarounds of " & RefRegn
                                CurPos(3).Refresh
                                FirstRegnLine = CurLine
                            End If
                            CurAdid = NoopList(0).GetColumnValue(RefLine, AdidCol)
                            CurLineTag = Trim(NoopList(2).GetLineTag(CurLine))
                            If CurLineTag = "/" Then CurLineTag = ""
                            PrepareAftInsert RefLine, tmpCmd, tmpKey, tmpFldLst, tmpLine, "REGN"
                            tmpLine = CurRegn & "," & CurAdid & "," & TurnImpFkeyToAftFkey(NoopList(0).GetColumnValue(RefLine, FkeyCol)) & "," & tmpLine
                            NoopList(0).ResetLineDecorations RefLine
                            NoopList(1).ResetLineDecorations RefLine
                            If NoopList(0).GetLineTag(RefLine) <> "" Then
                                CliFkeyCnt(10).Caption = Trim(Str(Val(CliFkeyCnt(10).Caption) - 1))
                                NoopList(0).SetLineTag RefLine, ""
                            End If
                            If NoopList(1).GetLineTag(RefLine) <> "" Then
                                SrvFkeyCnt(10).Caption = Trim(Str(Val(SrvFkeyCnt(10).Caption) - 1))
                                NoopList(1).SetLineTag RefLine, ""
                            End If
                            NoopList(2).SetLineTag CurLine, ""
                            NoopList(0).GetLineColor RefLine, CurForeColor, CurBackColor
                            If CurBackColor = CliFkeyCnt(1).BackColor Then
                                CliFkeyCnt(1).Caption = Trim(Str(Val(CliFkeyCnt(1).Caption) - 1))
                                SrvFkeyCnt(1).Caption = Trim(Str(Val(SrvFkeyCnt(1).Caption) - 1))
                            ElseIf CurBackColor = CliFkeyCnt(2).BackColor Then
                                CliFkeyCnt(2).Caption = Trim(Str(Val(CliFkeyCnt(2).Caption) - 1))
                            End If
                            tmpRota = NoopList(1).GetColumnValue(RefLine, RotaCol)
                            NoopList(1).UpdateTextLine RefLine, NoopList(0).GetLineValues(RefLine), False
                            NoopList(0).SetColumnValue RefLine, 0, "I"
                            NoopList(1).SetColumnValue RefLine, 0, ">"
                            NoopList(1).SetColumnValue RefLine, RotaCol, tmpRota
                            'NoopList(1).SetLineColor RefLine, SrvFkeyCnt(4).ForeColor, SrvFkeyCnt(4).BackColor
                            NoopList(0).SetLineColor RefLine, CliFkeyCnt(4).ForeColor, CliFkeyCnt(4).BackColor
                            NoopList(2).SetLineColor RefLine, CliFkeyCnt(4).ForeColor, CliFkeyCnt(4).BackColor
                            CliFkeyCnt(4).Caption = Trim(Str(Val(CliFkeyCnt(4).Caption) + 1))
                            'SrvFkeyCnt(4).Caption = Trim(Str(Val(SrvFkeyCnt(4).Caption) + 1))
                            If CurRegn = RefRegn Then
                                Select Case CurAdid
                                    Case "A"
                                        If ArrLine <> "" Then
                                            If DepLine = "" Then DepLine = RefRegn & ",D,-"
                                            OutResult = OutResult & ArrLine & vbLf & DepLine & vbLf
                                        End If
                                        ArrLine = tmpLine
                                        If ArrFldLst = "" Then ArrFldLst = "REGN,ADID,FKEY," & tmpFldLst
                                        DepLine = ""
                                    Case "D"
                                        DepLine = tmpLine
                                        If DepFldLst = "" Then DepFldLst = "REGN,ADID,FKEY," & tmpFldLst
                                        If ArrLine = "" Then ArrLine = RefRegn & ",A,-"
                                        OutResult = OutResult & ArrLine & vbLf & DepLine & vbLf
                                        ArrLine = ""
                                        DepLine = ""
                                    Case Else
                                End Select
                                If tmpTime > MaxTime Then MaxTime = tmpTime
                                If (CurBackColor = CliFkeyCnt(1).BackColor) Or (CurBackColor = CliFkeyCnt(2).BackColor) Or (CurLineTag <> "") Then
                                    SomeThingChanged = True
                                End If
                            Else
                                If ArrLine <> "" Then
                                    DepLine = RefRegn & ",D,-"
                                    OutResult = OutResult & ArrLine & vbLf & DepLine & vbLf
                                    ArrLine = ""
                                    DepLine = ""
                                End If
'SomeThingChanged = True
                                CedaProgress.HandleProgress 1, CurLine, MaxLine
                                If (SomeThingChanged) And (OutResult <> "") Then
                                    NoopList(0).RedrawTab
                                    NoopList(1).RedrawTab
                                    NoopList(2).RedrawTab
                                    OutResult = "*CMD*,ACC," & RefRegn & "," & MinTime & "00," & MaxTime & "59," & Alc3List & vbLf & OutResult
                                    OutResult = Left(OutResult, Len(OutResult) - 1)
                                    CedaLed(LedIdx).FillColor = NormalYellow
                                    CedaLed(LedIdx).Refresh
                                    Me.Refresh
                                    'MsgBox ArrFldLst & vbLf & DepFldLst
                                    LineCode = CedaProgress.AppendToMyList(RefRegn & "," & MinTime & "," & MaxTime & ",,,,")
                                    UfisServer.TwsCode.Text = "F.NBC." & LineCode & "." & Trim(Str(FirstRegnLine)) & "." & Trim(Str(MaxLine)) & "." & UfisServer.GetMyWorkStationName
                                    If Not SomeThingSent Then
                                        UfisServer.CallCeda UserAnswer, "SEAS", "AFTTAB", "", "", "ACCBGN,OAFOS", "", 0, False, False
                                        SomeThingSent = True
                                    End If
                                    UfisServer.CallCeda UserAnswer, "SEAS", "AFTTAB", (ArrFldLst & vbLf & DepFldLst), OutResult, "ACC,OAFOS", "", 0, False, False
                                    CedaProgress.UpdateMyList LineCode, RefRegn & "," & MinTime & "," & MaxTime & ",X,,,"
                                    CedaLed(LedIdx).FillColor = NormalGreen
                                    CedaLed(LedIdx).Refresh
                                    If LedIdx = 0 Then LedIdx = 1 Else LedIdx = 0
                                    Me.Refresh
                                End If
                                SomeThingChanged = False
                                RefRegn = CurRegn
                                MinTime = tmpTime
                                MaxTime = tmpTime
                                FirstRegnLine = CurLine
                                OutResult = ""
                                Select Case CurAdid
                                    Case "A"
                                        ArrLine = tmpLine
                                    Case "D"
                                        DepLine = tmpLine
                                        ArrLine = RefRegn & ",A,-"
                                        OutResult = OutResult & ArrLine & vbLf & DepLine & vbLf
                                        ArrLine = ""
                                        DepLine = ""
                                    Case Else
                                End Select
                                If (CurBackColor = CliFkeyCnt(1).BackColor) Or (CurBackColor = CliFkeyCnt(2).BackColor) Or (CurLineTag <> "") Then
                                    SomeThingChanged = True
                                End If
                                CurPos(3).Caption = "Turnarounds of " & RefRegn
                                CurPos(3).Refresh
                            End If
                        End If
                    End If
                Else
                    If (CurFtyp = "X") Or (CurFtyp = "N") Then
                        OaFosCancel CurLine, False
                    End If
                End If
            End If
            MsgTxt = Trim(Str(Timer - StartZeit))
            If Left(MsgTxt, 1) = "." Then MsgTxt = "0" & MsgTxt
            MsgTxt = GetItem(MsgTxt, 1, ".") & "s"
            UsedSec.Caption = MsgTxt
            UsedSec.Refresh
            UfisServer.GetNextBcFromSpool True
        Wend
        If ArrLine <> "" Then
            DepLine = RefRegn & ",D,-"
            OutResult = OutResult & ArrLine & vbLf & DepLine & vbLf
            ArrLine = ""
            DepLine = ""
        End If
'SomeThingChanged = True
        CedaProgress.HandleProgress 1, CurLine, MaxLine
        If (SomeThingChanged) And (OutResult <> "") Then
            OutResult = "*CMD*,ACC," & RefRegn & "," & MinTime & "00," & MaxTime & "59," & Alc3List & vbLf & OutResult
            OutResult = Left(OutResult, Len(OutResult) - 1)
            CedaLed(LedIdx).FillColor = NormalYellow
            CedaLed(LedIdx).Refresh
            LineCode = CedaProgress.AppendToMyList(RefRegn & "," & MinTime & "," & MaxTime & ",,,,")
            UfisServer.TwsCode.Text = "F.NBC." & LineCode & "." & Trim(Str(FirstRegnLine)) & "." & Trim(Str(MaxLine)) & "." & UfisServer.GetMyWorkStationName
            If Not SomeThingSent Then
                UfisServer.CallCeda UserAnswer, "SEAS", "AFTTAB", "", "", "ACCBGN,OAFOS", "", 0, False, False
                SomeThingSent = True
            End If
            UfisServer.CallCeda UserAnswer, "SEAS", "AFTTAB", (ArrFldLst & vbLf & DepFldLst), OutResult, "ACC,OAFOS", "", 0, False, False
            CedaProgress.UpdateMyList LineCode, RefRegn & "," & MinTime & "," & MaxTime & ",X,,,"
            CedaLed(LedIdx).FillColor = NormalGreen
            CedaLed(LedIdx).Refresh
        End If
        CedaProgress.HandleProgress 1, -1, -1
        NoopList(2).RedrawTab
        NoopList(1).RedrawTab
        NoopList(0).RedrawTab
        CedaLed(0).Visible = False
        CedaLed(1).Visible = False
        CurPos(3).Caption = ""
        CurPos(3).Refresh
        UfisServer.BcSpool False
        If SomeThingSent Then
            UfisServer.TwsCode.Text = "F.NBC.-1.-1.-1." & UfisServer.GetMyWorkStationName
            UfisServer.CallCeda UserAnswer, "SEAS", "AFTTAB", "", "", "ACCEND,OAFOS", "", 0, False, False
        End If
        UfisServer.TwsCode.Text = ".NBC."
        MsgTxt = Trim(Str(Timer - StartZeit))
        If Left(MsgTxt, 1) = "." Then MsgTxt = "0" & MsgTxt
        MsgTxt = GetItem(MsgTxt, 1, ".") & "s"
        UsedSec.Caption = MsgTxt
        ImportIsBusy = False
        Screen.MousePointer = 0
    End If
End Sub
Private Sub OaFosCancel(SelLine As Long, AskBack As Boolean)
    Dim RetVal As Integer
    Dim SqlRet As Integer
    Dim MsgTxt As String
    Dim CurLine As Long
    Dim RefLine As Long
    Dim CurRegn As String
    Dim chkTime As String
    Dim tmpTime As String
    Dim CurFtyp As String
    Dim tmpRota As String
    Dim CurCmd As String
    Dim SqlKey As String
    Dim FldLst As String
    Dim DatLst As String
    Dim TblNam As String
    Dim CurForeColor As Long
    Dim CurBackColor As Long
    If SelLine >= 0 Then CurLine = SelLine Else CurLine = NoopList(2).GetCurrentSelected
    RefLine = Val(NoopList(2).GetColumnValue(CurLine, 4))
    chkTime = GetImportCheckTime
    tmpTime = NoopList(0).GetColumnValue(RefLine, TimeCol)
    If tmpTime >= chkTime Then
        CurFtyp = NoopList(0).GetColumnValue(RefLine, FtypCol)
        If (CurFtyp = "X") Or (CurFtyp = "N") Then
            RetVal = 0
            NoopList(0).GetLineColor RefLine, CurForeColor, CurBackColor
            If CurBackColor = CliFkeyCnt(1).BackColor Then
                MsgTxt = "Do you want to cancel this flight?"
                CurCmd = "UFR"
                RetVal = 1
            End If
            If CurBackColor = CliFkeyCnt(2).BackColor Then
                MsgTxt = "Do you want to insert this cancelled flight?"
                CurCmd = "ISF"
                RetVal = 1
            End If
            If (AskBack) And (RetVal = 1) Then RetVal = MyMsgBox.CallAskUser(0, 0, 0, "Transaction Control", MsgTxt, "ask", "Yes;F,No", UserAnswer)
            If RetVal = 1 Then
                Select Case CurCmd
                    Case "UFR"
                        PrepareAftUpdate RefLine, CurCmd, SqlKey, FldLst, DatLst, "REGN"
                    Case "ISF"
                        PrepareAftInsert RefLine, CurCmd, SqlKey, FldLst, DatLst, "REGN"
                    Case Else
                End Select
                If CurCmd <> "" Then
                    TblNam = "AFTTAB"
                    SqlRet = UfisServer.CallCeda(UserAnswer, CurCmd, TblNam, FldLst, DatLst, SqlKey, "", 0, False, False)
                    NoopList(0).ResetLineDecorations RefLine
                    NoopList(1).ResetLineDecorations RefLine
                    If NoopList(0).GetLineTag(RefLine) <> "" Then
                        CliFkeyCnt(10).Caption = Trim(Str(Val(CliFkeyCnt(10).Caption) - 1))
                        NoopList(0).SetLineTag RefLine, ""
                    End If
                    If NoopList(1).GetLineTag(RefLine) <> "" Then
                        SrvFkeyCnt(10).Caption = Trim(Str(Val(SrvFkeyCnt(10).Caption) - 1))
                        NoopList(1).SetLineTag RefLine, ""
                    End If
                    NoopList(2).SetLineTag CurLine, ""
                    If CurBackColor = CliFkeyCnt(1).BackColor Then
                        CliFkeyCnt(1).Caption = Trim(Str(Val(CliFkeyCnt(1).Caption) - 1))
                        SrvFkeyCnt(1).Caption = Trim(Str(Val(SrvFkeyCnt(1).Caption) - 1))
                    ElseIf CurBackColor = CliFkeyCnt(2).BackColor Then
                        CliFkeyCnt(2).Caption = Trim(Str(Val(CliFkeyCnt(2).Caption) - 1))
                    End If
                    tmpRota = NoopList(1).GetColumnValue(RefLine, RotaCol)
                    NoopList(1).UpdateTextLine RefLine, NoopList(0).GetLineValues(RefLine), False
                    NoopList(0).SetColumnValue RefLine, 0, "I"
                    NoopList(1).SetColumnValue RefLine, 0, ">"
                    NoopList(1).SetColumnValue RefLine, RotaCol, tmpRota
                    'NoopList(1).SetLineColor RefLine, SrvFkeyCnt(4).ForeColor, SrvFkeyCnt(4).BackColor
                    NoopList(0).SetLineColor RefLine, CliFkeyCnt(4).ForeColor, CliFkeyCnt(4).BackColor
                    NoopList(2).SetLineColor RefLine, CliFkeyCnt(4).ForeColor, CliFkeyCnt(4).BackColor
                    CliFkeyCnt(4).Caption = Trim(Str(Val(CliFkeyCnt(4).Caption) + 1))
                    'SrvFkeyCnt(4).Caption = Trim(Str(Val(SrvFkeyCnt(4).Caption) + 1))
                End If
            End If
        End If
    End If
End Sub
Private Sub HandleActionButton(ActionCode As Integer)
    Dim CurLine As Long
    Dim TextColor As Long
    Dim BackColor As Long
    Dim CallIndex As Integer
    Dim ForWhat As Integer
    Dim CurAction As Integer
    Dim DoIt As Boolean
    CurLine = NoopList(2).GetCurrentSelected
    DoIt = CheckValidAction(CurLine)
    If DoIt Then
        NoopList(2).GetLineColor CurLine, TextColor, BackColor
        CallIndex = DefineColorAction(BackColor, ForWhat, CurAction)
        DoIt = False
        Select Case CurAction
            Case FOR_INSERT
                If chkWork(9).Value = 1 Then DoIt = True
            Case FOR_UPDATE
                If chkWork(10).Value = 1 Then DoIt = True
            Case FOR_DELETE
                If chkWork(11).Value = 1 Then DoIt = True
            Case Else
        End Select
        If ActionCode >= 0 Then
            If CurAction <> ActionCode Then DoIt = False
        End If
        If DoIt Then CallServerAction CallIndex, 1, ForWhat, False, ""
    End If
End Sub

Private Sub ShowSecondList(SetVisible As Boolean, Buttons As String)
    Dim i As Integer
    Dim CurBtn As Integer
    Dim CurPos As Long
    Dim tmpItm As String
    NoopList(1).Visible = SetVisible
    BottomLabels(1).Visible = SetVisible
    For i = 0 To 15
        chkWork(i).Visible = False
    Next
    If SetVisible = True Then
        TopLabels.Width = RLine(5).X1 - RLine(0).X1 - 2 * Screen.TwipsPerPixelX
        Me.Width = TopLabels.Width + 2 * TopLabels.Left + 10 * Screen.TwipsPerPixelX
    Else
        TopLabels.Width = NoopList(0).Width + NoopList(2).Width + 10 * Screen.TwipsPerPixelX
        Me.Width = TopLabels.Width + 2 * TopLabels.Left + 6 * Screen.TwipsPerPixelX
    End If
    tmpItm = "START"
    i = 0
    CurPos = 30
    While tmpItm <> ""
        i = i + 1
        tmpItm = GetItem(Buttons, i, ",")
        If tmpItm <> "" Then
            CurBtn = Val(tmpItm)
            chkWork(CurBtn).Left = CurPos
            CurPos = CurPos + chkWork(CurBtn).Width + Screen.TwipsPerPixelX
            chkWork(CurBtn).Visible = True
        End If
    Wend
    Me.Refresh
    DoEvents
End Sub

Private Sub CreateServerRotations()
    Dim AppendFlights As Boolean
    Dim MaxLine As Long
    Dim CurLine As Long
    Dim RotaCount As Long
    Dim CurRkey As String
    Dim NxtRkey As String
    Dim tmpFkey As String
    Dim tmpAdid As String
    Dim tmpStoa As String
    Dim tmpStod As String
    Dim ArrRkeys As String
    Dim DepRkeys As String
    Dim SrvRtyp As String
    Dim NxtLine As Long
    Dim LoopCount As Integer
    AppendFlights = False
    LoopCount = 0
    Do
        Screen.MousePointer = 11
        RotaList(1).ResetContent
        LoopCount = LoopCount + 1
        ArrRkeys = ""
        DepRkeys = ""
        NoopList(1).Sort Str(RkeyCol), True, True
        MaxLine = NoopList(1).GetLineCount - 1
        SetProgressBar 0, 1, MaxLine, "Building Flight Keys and Merging AODB Rotations ..."
        CurLine = 0
        NxtLine = 0
        RotaCount = 0
        AppendFlights = False
        While (CurLine <= MaxLine)
            If (CurLine Mod 100 = 0) Or (NxtLine Mod 100 = 0) Then SetProgressBar 0, 2, CurLine, ""
            CurRkey = NoopList(1).GetColumnValue(CurLine, RkeyCol)
            If (CurRkey <> "") And (Left(CurRkey, 1) <> ".") Then
                NxtLine = CurLine + 1
                NxtRkey = NoopList(1).GetColumnValue(NxtLine, RkeyCol)
                If CurRkey = NxtRkey Then
                    RotaList(1).InsertTextLine Trim(Str(RotaCount)) & ",-1,-1", False
                    If Trim(NoopList(1).GetColumnValue(CurLine, FltnCol)) <> "-" Then
                        NoopList(1).SetColumnValue CurLine, FkeyCol, TurnAftFkey(NoopList(1).GetColumnValue(CurLine, FkeyCol))
                        NoopList(1).SetColumnValue CurLine, TimeCol, Left(NoopList(1).GetColumnValue(CurLine, TimeCol), 12)
                        If Left(NoopList(1).GetColumnValue(CurLine, RemaCol), 2) <> "Li" Then
                            NoopList(1).SetColumnValue CurLine, PrflCol, ""
                        End If
                    End If
                    If Trim(NoopList(1).GetColumnValue(NxtLine, FltnCol)) <> "-" Then
                        NoopList(1).SetColumnValue NxtLine, FkeyCol, TurnAftFkey(NoopList(1).GetColumnValue(NxtLine, FkeyCol))
                        NoopList(1).SetColumnValue NxtLine, TimeCol, Left(NoopList(1).GetColumnValue(NxtLine, TimeCol), 12)
                        If Left(NoopList(1).GetColumnValue(NxtLine, RemaCol), 2) <> "Li" Then
                            NoopList(1).SetColumnValue NxtLine, PrflCol, ""
                        End If
                    End If
                    NoopList(1).SetColumnValue CurLine, FltnCol, NoopList(1).GetColumnValue(NxtLine, FlnoCol)
                    NoopList(1).SetColumnValue CurLine, TifdCol, NoopList(1).GetColumnValue(NxtLine, TimeCol)
                    NoopList(1).SetColumnValue CurLine, RtypCol, NoopList(1).GetColumnValue(NxtLine, AdidCol)
                    NoopList(1).SetColumnValue CurLine, RotaCol, Trim(Str(RotaCount + 1)) & "/" & Trim(Str(RotaCount))
                    NoopList(1).SetColumnValue CurLine, RkeyCol, ("." & CurRkey)
                    RotaCount = RotaCount + 1
                    RotaList(1).InsertTextLine Trim(Str(RotaCount)) & ",-1,-1", False
                    NoopList(1).SetColumnValue NxtLine, FltnCol, NoopList(1).GetColumnValue(CurLine, FlnoCol)
                    NoopList(1).SetColumnValue NxtLine, TifdCol, NoopList(1).GetColumnValue(CurLine, TimeCol)
                    NoopList(1).SetColumnValue NxtLine, RtypCol, NoopList(1).GetColumnValue(CurLine, AdidCol)
                    NoopList(1).SetColumnValue NxtLine, RotaCol, Trim(Str(RotaCount - 1)) & "/" & Trim(Str(RotaCount))
                    NoopList(1).SetColumnValue NxtLine, RkeyCol, ("." & CurRkey)
                    RotaCount = RotaCount + 1
                    CurLine = CurLine + 1
                Else
                    If Trim(NoopList(1).GetColumnValue(CurLine, FltnCol)) <> "-" Then
                        NoopList(1).SetColumnValue CurLine, FkeyCol, TurnAftFkey(NoopList(1).GetColumnValue(CurLine, FkeyCol))
                        NoopList(1).SetColumnValue CurLine, TimeCol, Left(NoopList(1).GetColumnValue(CurLine, TimeCol), 12)
                    End If
                    NoopList(1).SetColumnValue CurLine, FltnCol, "    -"
                    NoopList(1).SetColumnValue CurLine, TifdCol, ""
                    NoopList(1).SetColumnValue CurLine, RotaCol, "-1/-1"
                    SrvRtyp = NoopList(1).GetColumnValue(CurLine, RtypCol)
                    If (MyUpdateMode) Or ((MyFullMode) And (SrvRtyp = "J")) Then
                        tmpAdid = NoopList(1).GetColumnValue(CurLine, AdidCol)
                        Select Case tmpAdid
                            Case "D"
                                ArrRkeys = ArrRkeys & CurRkey & ","
                            Case "A"
                                DepRkeys = DepRkeys & CurRkey & ","
                            Case Else
                        End Select
                        If MyFullMode Then AppendFlights = True
                    End If
                    NoopList(1).SetColumnValue CurLine, RtypCol, ""
                    If Left(NoopList(1).GetColumnValue(CurLine, RemaCol), 2) <> "Li" Then
                        NoopList(1).SetColumnValue CurLine, PrflCol, ""
                    End If
                End If
            Else
                If CurRkey <> "" Then
                    NxtLine = CurLine + 1
                    NxtRkey = NoopList(1).GetColumnValue(NxtLine, RkeyCol)
                    If CurRkey = NxtRkey Then
                        RotaList(1).InsertTextLine Trim(Str(RotaCount)) & ",-1,-1", False
                        NoopList(1).SetColumnValue CurLine, RotaCol, Trim(Str(RotaCount + 1)) & "/" & Trim(Str(RotaCount))
                        RotaCount = RotaCount + 1
                        RotaList(1).InsertTextLine Trim(Str(NxtLine)) & ",-1,-1", False
                        NoopList(1).SetColumnValue NxtLine, RotaCol, Trim(Str(RotaCount - 1)) & "/" & Trim(Str(RotaCount))
                        RotaCount = RotaCount + 1
                        CurLine = CurLine + 1
                    End If
                End If
            End If
            CurLine = CurLine + 1
        Wend
        NoopList(1).RedrawTab
        SetProgressBar 0, 3, -1, ""
        If ArrRkeys <> "" Then
            ArrRkeys = Left(ArrRkeys, Len(ArrRkeys) - 1)
            AppendFlights = True
        End If
        If DepRkeys <> "" Then
            DepRkeys = Left(DepRkeys, Len(DepRkeys) - 1)
            AppendFlights = True
        End If
        If (LoopCount = 1) And (AppendFlights) Then
            LoadFlightsByFkey ArrRkeys, DepRkeys
        End If
        Screen.MousePointer = 0
    Loop While (LoopCount < 2) And (AppendFlights = True)
    RotaList(1).RedrawTab
End Sub

Private Sub ClearOutEmptyLines(Index As Integer, ClearLabels As Boolean)
    Dim CurLine As Long
    Dim MaxLine As Long
    Dim CntLine As Long
    Dim tmpData As String
    Dim i As Integer
    StatusBar.Panels(2).Text = "Clear empty lines. Loop" & Str(Index)
    MaxLine = NoopList(Index).GetLineCount - 1
    CurLine = 0
    CntLine = 0
    While CurLine <= MaxLine
        CntLine = CntLine + 1
        If CntLine Mod 100 = 0 Then
            StatusBar.Panels(2).Text = "Clear empty lines. Loop" & Str(Index) & " Line" & Str(CntLine)
        End If
        'Adid must be filled
        tmpData = Trim(NoopList(Index).GetColumnValue(CurLine, AdidCol))
        If tmpData = "" Then
            NoopList(Index).DeleteLine CurLine
            CurLine = CurLine - 1
            MaxLine = MaxLine - 1
        Else
            'NoopList(Index).SetLineColor CurLine, vbBlack, vbWhite
        End If
        CurLine = CurLine + 1
    Wend
    NoopList(Index).RedrawTab
    If ClearLabels Then
        For i = 1 To 4
            If Index = 0 Then
                CliFkeyCnt(i).Caption = "0"
            Else
                SrvFkeyCnt(i).Caption = "0"
            End If
        Next
    End If
    StatusBar.Panels(2).Text = "Ready."
End Sub

Private Sub CompareFlightLists(UseColorIdx As Integer, SortCol As Long, CheckCol As Long)
    Dim LeftLine As Long
    Dim RightLine As Long
    Dim RotaLine As Long
    Dim FtypVal As String
    Dim RotaItem As String
    Dim tmpFtyp As String
    Dim LeftMax As Long
    Dim RightMax As Long
    Dim useMax As Long
    Dim LeftVal As String
    Dim RightVal As String
    Dim LeftValues As String
    Dim RightValues As String
    Dim OldLeftVal(8) As String
    Dim OldLeftLin(8) As Long
    Dim OldRightVal(8) As String
    Dim OldRightLin(8) As Long
    Dim OldLine As Long
    Dim OldLeftIdx As Integer
    Dim OldRightIdx As Integer
    Dim CompValues As Boolean
    Dim GreenCnt As Long
    Dim GreenForeColor As Long
    Dim GreenBackColor As Long
    Dim RedCnt As Long
    Dim RedForeColor As Long
    Dim RedBackColor As Long
    Dim LeftYelCnt As Long
    Dim LeftYelForeColor As Long
    Dim LeftYelBackColor As Long
    Dim RightYelCnt As Long
    Dim RightYelForeColor As Long
    Dim RightYelBackColor As Long
    Dim WhiteCnt As Long
    Dim WhiteForeColor As Long
    Dim WhiteBackColor As Long
    Dim GrayCnt As Long
    Dim GrayForeColor As Long
    Dim GrayBackColor As Long
    Dim CurLeftAdid As String
    Dim CurRightAdid As String
    Dim DoubledLeft As Boolean
    Dim DoubledRight As Boolean
    Dim IsInList As Boolean
    Dim NotInList As String
    Dim MsgTxt As String
    
    Screen.MousePointer = 11
    NotInList = ""
    ClearOutEmptyLines 0, True
    ClearOutEmptyLines 1, True
    StatusBar.Panels(2).Text = "Sorting the Flight Lists."
    NoopList(0).Sort Trim(Str(SortCol)), True, True
    NoopList(1).Sort Trim(Str(SortCol)), True, True
    NoopList(0).RedrawTab
    NoopList(1).RedrawTab
    
    GreenCnt = 0
    GreenBackColor = CliFkeyCnt(2).BackColor
    GreenForeColor = CliFkeyCnt(2).ForeColor
    RedCnt = 0
    RedBackColor = SrvFkeyCnt(2).BackColor
    RedForeColor = SrvFkeyCnt(2).ForeColor
    LeftYelCnt = 0
    LeftYelBackColor = CliFkeyCnt(3).BackColor
    LeftYelForeColor = CliFkeyCnt(3).ForeColor
    RightYelCnt = 0
    RightYelBackColor = SrvFkeyCnt(3).BackColor
    RightYelForeColor = SrvFkeyCnt(3).ForeColor
    WhiteCnt = 0
    WhiteForeColor = CliFkeyCnt(1).ForeColor
    WhiteBackColor = CliFkeyCnt(1).BackColor
    GrayCnt = 0
    GrayForeColor = CliFkeyCnt(4).ForeColor
    GrayBackColor = CliFkeyCnt(4).BackColor
    LeftMax = NoopList(0).GetLineCount - 1
    RightMax = NoopList(1).GetLineCount - 1
    LeftLine = 0
    RightLine = 0
    If RightMax > LeftMax Then useMax = RightMax Else useMax = LeftMax
    For OldLeftIdx = 0 To 8
        OldLeftVal(OldLeftIdx) = ""
        OldRightVal(OldLeftIdx) = ""
        OldLeftLin(OldLeftIdx) = -1
        OldRightLin(OldLeftIdx) = -1
    Next
    StatusBar.Panels(2).Text = "Comparing the Flight Lists ..."
    While LeftLine <= useMax
        If LeftLine Mod 100 = 0 Then SetProgressBar 0, 0, useMax, Str(LeftLine)
        
        LeftVal = Trim(NoopList(0).GetColumnValue(LeftLine, CheckCol))
        RightVal = Trim(NoopList(1).GetColumnValue(RightLine, CheckCol))
        CurLeftAdid = Trim(NoopList(0).GetColumnValue(LeftLine, AdidCol))
        Select Case CurLeftAdid
            Case "A"
                OldLeftIdx = 0
            Case "D"
                OldLeftIdx = 1
            Case Else
                OldLeftIdx = -1
        End Select
        CurRightAdid = Trim(NoopList(1).GetColumnValue(RightLine, AdidCol))
        Select Case CurRightAdid
            Case "A"
                OldRightIdx = 0
            Case "D"
                OldRightIdx = 1
            Case Else
                OldRightIdx = -1
        End Select
        If (LeftVal <> "") And (RightVal <> "") Then
            If LeftVal <> RightVal Then
                If RightVal > LeftVal Then
                    'Flight in File but not on Server (Green Color)
                    NoopList(0).SetLineColor LeftLine, GreenForeColor, GreenBackColor
                    NoopList(1).InsertTextLineAt LeftLine, ",,,,,,,,,,,,,,", False
                    NoopList(1).SetLineColor LeftLine, GreenForeColor, GreenBackColor
                    RightMax = RightMax + 1
                    GreenCnt = GreenCnt + 1
                    RightVal = ""
                ElseIf LeftVal > RightVal Then
                    'Flight on Server but not in File (Red Color)
                    FtypVal = NoopList(1).GetColumnValue(LeftLine, FtypCol)
                    IsInList = CheckAlc3InList(RightVal)
                    NoopList(0).InsertTextLineAt LeftLine, ",,,,,,,,,,,,,,", False
                    If (IsInList) And (FtypVal <> "N") And (FtypVal <> "X") Then
                        NoopList(0).SetLineColor LeftLine, RedForeColor, RedBackColor
                        NoopList(1).SetLineColor LeftLine, RedForeColor, RedBackColor
                        RedCnt = RedCnt + 1
                    Else
                        If IsInList Then
                            NoopList(0).SetLineColor LeftLine, RedForeColor, RedBackColor
                        Else
                            NoopList(0).SetLineColor LeftLine, GrayForeColor, GrayBackColor
                        End If
                        NoopList(1).SetLineColor LeftLine, GrayForeColor, GrayBackColor
                        GrayCnt = GrayCnt + 1
                    End If
                    If (Not IsInList) And (InStr(NotInList, Left(RightVal, 3)) = 0) Then
                        NotInList = NotInList & Left(RightVal, 3) & vbNewLine
                    End If
                    LeftMax = LeftMax + 1
                    LeftVal = ""
                End If
                If RightMax > LeftMax Then useMax = RightMax Else useMax = LeftMax
            End If
            DoubledLeft = False
            If LeftVal <> "" Then
                If OldLeftIdx >= 0 Then
                    If OldLeftVal(OldLeftIdx) = LeftVal Then
                        NoopList(0).SetLineColor LeftLine, LeftYelForeColor, LeftYelBackColor
                        NoopList(1).SetLineColor LeftLine, LeftYelForeColor, LeftYelBackColor
                        NoopList(0).SetColumnValue LeftLine, 0, "!"
                        LeftYelCnt = LeftYelCnt + 1
                        DoubledLeft = True
                    End If
                    OldLeftVal(OldLeftIdx) = LeftVal
                    OldLeftLin(OldLeftIdx) = LeftLine
                End If
            End If
            DoubledRight = False
            If RightVal <> "" Then
                If OldRightIdx >= 0 Then
                    If OldRightVal(OldRightIdx) = RightVal Then
                        If Not DoubledLeft Then
                            NoopList(0).SetLineColor LeftLine, RightYelForeColor, RightYelBackColor
                        End If
                        NoopList(1).SetLineColor LeftLine, RightYelForeColor, RightYelBackColor
                        RightYelCnt = RightYelCnt + 1
                        DoubledRight = True
                    End If
                    OldRightVal(OldRightIdx) = RightVal
                    OldRightLin(OldRightIdx) = LeftLine
                End If
            End If
            If (LeftLine = RightLine) And (LeftVal = RightVal) Then
                If (Not DoubledLeft) And (Not DoubledRight) Then
                    CompValues = CompareValues(LeftLine, TimeCol, False, True, "")
                    CompValues = CompareValues(LeftLine, Act3Col, False, CompValues, "")
                    CompValues = CompareValues(LeftLine, Apc3Col, False, CompValues, "")
                    CompValues = CompareValues(LeftLine, Via3Col, False, CompValues, "")
                    CompValues = CompareValues(LeftLine, StypCol, False, CompValues, "")
                    If MainDialog.DataSystemType = "OAFOS" Then
                        tmpFtyp = NoopList(0).GetColumnValue(LeftLine, FtypCol)
                        If (tmpFtyp <> "X") And (tmpFtyp <> "N") Then
                            CompValues = CompareValues(LeftLine, RegnCol, False, CompValues, "")
                        End If
                    End If
                    CompValues = CompareValues(LeftLine, FtypCol, True, CompValues, "S,O")
                    If CompValues = False Then
                        WhiteCnt = WhiteCnt + 1
                        NoopList(0).SetLineColor LeftLine, WhiteForeColor, WhiteBackColor
                        NoopList(1).SetLineColor LeftLine, WhiteForeColor, WhiteBackColor
                    Else
                        GrayCnt = GrayCnt + 1
                        NoopList(0).SetLineColor LeftLine, GrayForeColor, GrayBackColor
                        NoopList(1).SetLineColor LeftLine, GrayForeColor, GrayBackColor
                    End If
                End If
            End If
        Else
            If LeftVal <> "" Then
                DoubledLeft = False
                NoopList(0).SetLineColor LeftLine, GreenForeColor, GreenBackColor
                NoopList(1).InsertTextLineAt LeftLine, ",,,,,,,,,,,,,,", False
                NoopList(1).SetLineColor LeftLine, GreenForeColor, GreenBackColor
                RightMax = RightMax + 1
                GreenCnt = GreenCnt + 1
                If OldLeftIdx >= 0 Then
                    If OldLeftVal(OldLeftIdx) = LeftVal Then
                        NoopList(0).SetLineColor LeftLine, LeftYelForeColor, LeftYelBackColor
                        NoopList(1).SetLineColor LeftLine, LeftYelForeColor, LeftYelBackColor
                        LeftYelCnt = LeftYelCnt + 1
                        DoubledLeft = True
                    End If
                    OldLeftVal(OldLeftIdx) = LeftVal
                    OldLeftLin(OldLeftIdx) = LeftLine
                End If
            End If
            If RightVal <> "" Then
                IsInList = CheckAlc3InList(RightVal)
                DoubledRight = False
                FtypVal = NoopList(1).GetColumnValue(LeftLine, FtypCol)
                NoopList(0).InsertTextLineAt LeftLine, ",,,,,,,,,,,,,,", False
                If (IsInList) And (FtypVal <> "N") And (FtypVal <> "X") Then
                    NoopList(0).SetLineColor LeftLine, RedForeColor, RedBackColor
                    NoopList(1).SetLineColor LeftLine, RedForeColor, RedBackColor
                    RedCnt = RedCnt + 1
                Else
                    If IsInList Then
                        NoopList(0).SetLineColor LeftLine, RedForeColor, RedBackColor
                    Else
                        NoopList(0).SetLineColor LeftLine, GrayForeColor, GrayBackColor
                    End If
                    NoopList(1).SetLineColor LeftLine, GrayForeColor, GrayBackColor
                    GrayCnt = GrayCnt + 1
                End If
                If (Not IsInList) And (InStr(NotInList, Left(RightVal, 3)) = 0) Then
                    NotInList = NotInList & Left(RightVal, 3) & vbNewLine
                End If
                LeftMax = LeftMax + 1
                If OldRightIdx >= 0 Then
                    If OldRightVal(OldRightIdx) = RightVal Then
                        NoopList(0).SetLineColor LeftLine, RightYelForeColor, RightYelBackColor
                        NoopList(1).SetLineColor LeftLine, RightYelForeColor, RightYelBackColor
                        RightYelCnt = RightYelCnt + 1
                        DoubledRight = True
                    End If
                    OldRightVal(OldRightIdx) = RightVal
                    OldRightLin(OldRightIdx) = LeftLine
                End If
            End If
        End If
        RightLine = RightLine + 1
        LeftLine = LeftLine + 1
        If RightMax > LeftMax Then useMax = RightMax Else useMax = LeftMax
    Wend
    CliFkeyCnt(1).Caption = Str(WhiteCnt)
    SrvFkeyCnt(1).Caption = Str(WhiteCnt)
    CliFkeyCnt(4).Caption = Str(GrayCnt)
    SrvFkeyCnt(4).Caption = Str(GrayCnt)
    CliFkeyCnt(3).Caption = Str(LeftYelCnt)
    SrvFkeyCnt(3).Caption = Str(RightYelCnt)
    CliFkeyCnt(2).Caption = Str(GreenCnt)
    SrvFkeyCnt(2).Caption = Str(RedCnt)
    NoopList(0).RedrawTab
    NoopList(1).RedrawTab
    Me.Refresh
    SetProgressBar 0, 3, 0, ""
    
    If NoopList(0).GetLineCount <> NoopList(1).GetLineCount Then
        MsgBox "Lists not snychronized!!" & vbNewLine & "Please stop the application" & vbNewLine & "and start up again."
        SeriousError = True
        NoopList(0).ResetContent
        NoopList(1).ResetContent
        NoopList(2).ResetContent
    Else
        If MyUpdateMode Then AdjustUpdateColors
        SeriousError = False
    End If
    If MainDialog.DataSystemType = "OAFOS" Then
        CreateRegnView
    End If
    CheckRotationDetails
    Screen.MousePointer = 0
    AdjustScrollMaster -1, -1, ""
    If MainDialog.DataSystemType <> "OAFOS" Then
        CreateDistinctFlno "-ALL-"
    Else
        CreateDistinctRegn "-ALL-"
    End If
    For useMax = 1 To 32
        NoopList(0).InsertTextLineAt LeftLine, "-,,,,,,,,,,,,,,", False
        NoopList(1).InsertTextLineAt LeftLine, "-,,,,,,,,,,,,,,", False
    Next
    Me.Refresh
    If NotInList <> "" Then
        MsgTxt = "Attention:" & vbNewLine & "Some of the co-operating airlines" & vbNewLine
        MsgTxt = MsgTxt & "could not be found in the left side." & vbNewLine
        MsgTxt = MsgTxt & "This concerns:" & vbNewLine & NotInList
        If FullMode Then
            MsgTxt = MsgTxt & "It's recommended to load these" & vbNewLine
            MsgTxt = MsgTxt & "airlines into your filter and run" & vbNewLine
            MsgTxt = MsgTxt & "the full validation again." & vbNewLine
        End If
        If MyMsgBox.CallAskUser(0, 0, 0, "Combined Airlines", MsgTxt, "infomsg", "", UserAnswer) > 0 Then DoNothing
        Me.Refresh
    End If
End Sub
Private Function CheckAlc3InList(UsedFkey As String) As Boolean
    Dim Max As Long
    Dim i As Long
    Dim tmpAlc3 As String
    Dim IsInList As Boolean
    IsInList = True
    tmpAlc3 = Trim(Left(UsedFkey, 3))
    If Len(tmpAlc3) = 3 Then
        IsInList = False
        Max = AlcTabCombo(1).GetLineCount - 1
        For i = 0 To Max
            If (Trim(AlcTabCombo(1).GetColumnValue(i, 0)) = tmpAlc3) Or (Trim(AlcTabCombo(1).GetColumnValue(i, 1)) = tmpAlc3) Then
                IsInList = True
                Exit For
            End If
        Next
    End If
    CheckAlc3InList = IsInList
End Function
Private Sub CreateRegnView()
    Dim CurLine As Long
    Dim MaxLine As Long
    Dim RegnKey As String
    Dim CurFtyp As String
    MaxLine = NoopList(0).GetLineCount - 1
    For CurLine = 0 To MaxLine
        CurFtyp = NoopList(0).GetColumnValue(CurLine, FtypCol)
        If (CurFtyp <> "X") And (CurFtyp <> "N") Then
            RegnKey = NoopList(0).GetColumnValue(CurLine, RegnCol)
            RegnKey = Left(RegnKey & "ZZZZZZZZZZZZ", 12)
            RegnKey = RegnKey & NoopList(0).GetColumnValue(CurLine, TimeCol)
            RegnKey = RegnKey & "," & Right("000000" & Trim(Str(CurLine)), 6)
        Else
            RegnKey = ""
            'NoopList(0).SetColumnValue CurLine, RegnCol, ""
        End If
        NoopList(0).SetColumnValue CurLine, RemaCol, RegnKey
        NoopList(1).SetColumnValue CurLine, RemaCol, RegnKey
    Next
    NoopList(0).Sort RemaCol, True, True
    NoopList(1).Sort RemaCol, True, True
End Sub

Private Sub AdjustUpdateColors()
    Dim CurLine As Long
    Dim MaxLine As Long
    Dim i As Integer
    Dim tmpCode As String
    Dim tmpFtyp As String
    Dim tmpFkey As String
    Dim CliTime As String
    Dim chkTime As String
    Dim SrvTime As String
    Dim CliCnt(4) As Long
    Dim SrvCnt(4) As Long
    Dim CurForeColor As Long
    Dim CurBackColor As Long
    chkTime = GetImportCheckTime
    MaxLine = NoopList(0).GetLineCount - 1
    For CurLine = 0 To MaxLine
        tmpCode = Trim(NoopList(0).GetColumnValue(CurLine, PrflCol))
        If tmpCode = "" Then
            tmpCode = NoopList(1).GetColumnValue(CurLine, PrflCol)
        End If
        tmpFtyp = NoopList(1).GetColumnValue(CurLine, FtypCol)
        tmpFkey = NoopList(1).GetColumnValue(CurLine, FkeyCol)
        CliTime = NoopList(0).GetColumnValue(CurLine, TimeCol)
        'SrvTime = NoopList(1).GetColumnValue(CurLine, TimeCol)
        NoopList(0).GetLineColor CurLine, CurForeColor, CurBackColor
        If (CurBackColor <> SrvFkeyCnt(3).BackColor) And (CurBackColor <> CliFkeyCnt(3).BackColor) Then
            Select Case tmpCode
                Case "D"
                    If tmpFkey <> "" Then
                        NoopList(0).SetLineColor CurLine, SrvFkeyCnt(2).ForeColor, SrvFkeyCnt(2).BackColor
                        NoopList(1).SetLineColor CurLine, SrvFkeyCnt(2).ForeColor, SrvFkeyCnt(2).BackColor
                        NoopList(1).SetColumnValue CurLine, PrflCol, "D"
                        SrvCnt(2) = SrvCnt(2) + 1
                    Else
                        NoopList(0).SetLineColor CurLine, SrvFkeyCnt(4).ForeColor, SrvFkeyCnt(4).BackColor
                        NoopList(1).SetLineColor CurLine, SrvFkeyCnt(4).ForeColor, SrvFkeyCnt(4).BackColor
                        NoopList(1).SetColumnValue CurLine, PrflCol, "-"
                        CliCnt(4) = CliCnt(4) + 1
                        SrvCnt(4) = SrvCnt(4) + 1
                    End If
                Case "N"
                    If tmpFkey <> "" Then
                        If CurBackColor = CliFkeyCnt(1).BackColor Then
                            NoopList(0).SetLineColor CurLine, CliFkeyCnt(1).ForeColor, CliFkeyCnt(1).BackColor
                            NoopList(1).SetLineColor CurLine, SrvFkeyCnt(1).ForeColor, SrvFkeyCnt(1).BackColor
                            NoopList(1).SetColumnValue CurLine, PrflCol, "U"
                            CliCnt(1) = CliCnt(1) + 1
                            SrvCnt(1) = SrvCnt(1) + 1
                        Else
                            NoopList(0).SetLineColor CurLine, CliFkeyCnt(4).ForeColor, CliFkeyCnt(4).BackColor
                            NoopList(1).SetLineColor CurLine, SrvFkeyCnt(4).ForeColor, SrvFkeyCnt(4).BackColor
                            NoopList(1).SetColumnValue CurLine, PrflCol, "-"
                            CliCnt(4) = CliCnt(4) + 1
                            SrvCnt(4) = SrvCnt(4) + 1
                        End If
                    Else
                        NoopList(0).SetLineColor CurLine, CliFkeyCnt(2).ForeColor, CliFkeyCnt(2).BackColor
                        NoopList(1).SetLineColor CurLine, CliFkeyCnt(2).ForeColor, CliFkeyCnt(2).BackColor
                        NoopList(1).SetColumnValue CurLine, PrflCol, "I"
                        CliCnt(2) = CliCnt(2) + 1
                    End If
                Case "L"    'Loaded Rotation Link
                    NoopList(0).SetLineColor CurLine, SrvFkeyCnt(4).ForeColor, SrvFkeyCnt(4).BackColor
                    NoopList(1).SetLineColor CurLine, SrvFkeyCnt(4).ForeColor, SrvFkeyCnt(4).BackColor
                    NoopList(0).SetColumnValue CurLine, PrflCol, "-"
                    CliCnt(4) = CliCnt(4) + 1
                    SrvCnt(4) = SrvCnt(4) + 1
                Case Else
            End Select
            If (CliTime <> "") And (CliTime < chkTime) Then
                NoopList(0).GetLineColor CurLine, CurForeColor, CurBackColor
                If CurBackColor <> SrvFkeyCnt(4).BackColor Then
                    NoopList(0).SetLineColor CurLine, LimitColor, CurBackColor
                    NoopList(0).SetColumnValue CurLine, PrflCol, "<"
                End If
            End If
        End If
    Next
    For i = 1 To 4
        If i <> 3 Then
            SrvFkeyCnt(i).Caption = Trim(Str(SrvCnt(i)))
            CliFkeyCnt(i).Caption = Trim(Str(CliCnt(i)))
        End If
    Next
    NoopList(0).RedrawTab
    NoopList(1).RedrawTab
End Sub
Private Function CompareValues(LineNo As Long, ColNo As Long, ShowDiff As Boolean, RetVal As Boolean, IgnoreData As String) As Boolean
    Dim ShowCellObj As Boolean
    Dim LeftItem As String
    Dim RightItem As String
    If NoopList(0).GetColumnValue(LineNo, ColNo) <> NoopList(1).GetColumnValue(LineNo, ColNo) Then
        CompareValues = False
        ShowCellObj = ShowDiff
        If IgnoreData <> "" Then
            LeftItem = NoopList(0).GetColumnValue(LineNo, ColNo)
            RightItem = NoopList(1).GetColumnValue(LineNo, ColNo)
            If LeftItem = GetItem(IgnoreData, 1, ",") And RightItem = GetItem(IgnoreData, 2, ",") Then
                ShowCellObj = False
                CompareValues = RetVal
            End If
        End If
        If ShowCellObj Then
            NoopList(0).SetDecorationObject LineNo, ColNo, "CellDiff"
            NoopList(1).SetDecorationObject LineNo, ColNo, "CellDiff"
        End If
    Else
        CompareValues = RetVal
    End If
End Function

Public Sub CreateFlightLists()
    Dim FilterCnt As Long
    Dim MsgTxt As String
    Dim tmpdat As String
    Dim tmpAlc As String
    Dim tmpAlc3 As String
    Dim ChkAlc As String
    Dim tmpBeginDay As String
    Dim tmpEndDay As String
    Dim iLen As Integer
    Dim ReqVal As Integer
    Dim ErrCnt As Integer
    Dim LineNo As Long
    Dim UsedAlc3Filter As String
    AlcTabCombo(0).ComboResetContent "AirlCodes"
    AlcTabCombo(1).ResetContent
    RegnTabCombo(0).ComboResetContent "RegnCodes"
    RegnTabCombo(1).ResetContent
    FlnoTabCombo(0).ComboResetContent "RegnCodes"
    FlnoTabCombo(1).ResetContent
    UfisTools.BlackBox(0).ResetContent
    NoopList(0).ResetContent
    NoopList(1).ResetContent
    NoopList(2).ResetContent
    NoopList(0).RedrawTab
    NoopList(1).RedrawTab
    NoopList(2).RedrawTab
    Me.Refresh
    StatusBar.Panels(2).Text = "Preparing the Airline Filters"
    ErrCnt = 0
    UsedAlc3Filter = ""
    FilterCnt = Val(DistinctBasicData("INIT", "3,5"))
    If FilterCnt > 0 Then
        Screen.MousePointer = 11
        For LineNo = 1 To FilterCnt
            tmpAlc3 = DistinctBasicData("NEXT", tmpAlc)
            iLen = Len(tmpAlc3)
            If iLen = 3 Then
                AlcTabCombo(1).InsertTextLine tmpAlc, False
                UsedAlc3Filter = UsedAlc3Filter + "'" + tmpAlc3 + "',"
            Else
                MsgTxt = "Can't determine the airline's 2LC/3LC of: " & tmpAlc
                If MyMsgBox.CallAskUser(0, 0, 0, "Record Filter", MsgTxt, "infomsg", "", UserAnswer) > 0 Then DoNothing
                Me.Refresh
                ErrCnt = ErrCnt + 1
            End If
        Next
        FilterCnt = Val(DistinctBasicData("RESET", "3,5"))
        AlcTabCombo(1).Sort 1, True, True
        AlcTabCombo(1).Sort 0, True, True
        FilterCnt = AlcTabCombo(1).GetLineCount
        If FilterCnt > 1 Then AlcTabCombo(1).InsertTextLineAt 0, "***,,All Airlines,", False
        FilterCnt = AlcTabCombo(1).GetLineCount
        AlcTabCombo(0).SetColumnValue 0, 0, AlcTabCombo(1).GetColumnValue(0, 0)
        CurPos(0).Caption = AlcTabCombo(1).GetColumnValue(0, 0)
        If CurPos(0).Caption = "***" Then CurPos(0).Caption = "-ALL-"
        For LineNo = 0 To FilterCnt
            tmpAlc = AlcTabCombo(1).GetLineValues(LineNo)
            AlcTabCombo(0).ComboAddTextLines "AirlCodes", tmpAlc, vbLf
            ChkAlc = "," + GetItem(tmpAlc, 1, ",") + ","
            If InStr(ChkErrorList, ChkAlc) > 0 Then
                AlcTabCombo(0).ComboSetLineColors "AirlCodes", LineNo, vbWhite, vbRed
            End If
        Next
        AlcTabCombo(0).SetCurrentSelection 0
        AlcTabCombo(0).RedrawTab
        Screen.MousePointer = 0
        If UsedAlc3Filter <> "" Then UsedAlc3Filter = Left(UsedAlc3Filter, Len(UsedAlc3Filter) - 1)
        If Len(UsedAlc3Filter) = 0 Then ErrCnt = 1
        ReqVal = 1
        If ErrCnt > 0 Then
            MsgTxt = ErrCnt & " Error(s) detected." & vbNewLine & "Do you want to proceed?"
            ReqVal = MyMsgBox.CallAskUser(0, 0, 0, "Record Filter", MsgTxt, "infomsg", "Yes,No", UserAnswer)
            Me.Refresh
        End If
        If ReqVal = 1 Then
            tmpBeginDay = FlightExtract.txtVpfr(0).Tag
            tmpEndDay = FlightExtract.txtVpto(0).Tag
            CurBegin.Caption = DecodeSsimDayFormat(tmpBeginDay, "CEDA", "SSIM2")
            CurBegin.Tag = tmpBeginDay
            CurEnd.Caption = DecodeSsimDayFormat(tmpEndDay, "CEDA", "SSIM2")
            CurEnd.Tag = tmpEndDay
            LoadClientFlights
            If chkWork(2).Value = 1 Then
                If MyFullMode = True Then LoadFlightsByAlc3 UsedAlc3Filter, tmpBeginDay, tmpEndDay
                If MyUpdateMode = True Then LoadFlightsByFkey "", ""
            Else
                CreateDistinctList
            End If
        End If
    Else
        MsgTxt = "Please load flight data first."
        If MyMsgBox.CallAskUser(0, 0, 0, "Record Filter", MsgTxt, "infomsg", "", UserAnswer) > 0 Then DoNothing
    End If
    StatusBar.Panels(2).Text = ""
End Sub

Private Sub CreateDistinctList()
    Dim CurLine As Long
    Dim MaxLine As Long
    Dim tmpBuffer As String
    Dim tmpRec As String
    Dim OldRec As String
    Screen.MousePointer = 11
    StatusBar.Panels(2).Text = "Sorting buffer."
    NoopList(0).Sort Trim(Str(FkeyCol)), True, True
    NoopList(0).RedrawTab
    MaxLine = NoopList(0).GetLineCount - 1
    OldRec = ""
    SetProgressBar 0, 1, MaxLine, "Comparing Flights ..."
    For CurLine = 0 To MaxLine
        If CurLine Mod 100 = 0 Then SetProgressBar 0, 2, CurLine, ""
        tmpRec = NoopList(0).GetColumnValue(CurLine, FkeyCol)
        If OldRec = tmpRec Then
            NoopList(0).SetLineColor CurLine, vbBlack, CliFkeyCnt(3).BackColor
        Else
            NoopList(0).SetLineColor CurLine, vbBlack, LightGray
        End If
        OldRec = tmpRec
    Next
    NoopList(0).RedrawTab
    Screen.MousePointer = 0
    SetProgressBar 0, 3, 0, ""
    AdjustScrollMaster -1, -1, ""
    Me.Refresh
End Sub

Private Function Alc3LookUp(AirlCode As String) As String
    Dim i As Long
    Dim Max As Long
    Dim Result As String
    Dim tmpAlc As String
    tmpAlc = Trim(AirlCode)
    Result = tmpAlc
    If Len(tmpAlc) = 2 Then
        Max = AlcTabCombo(1).GetLineCount - 1
        For i = 0 To Max
            If Trim(AlcTabCombo(1).GetColumnValue(i, 0)) = tmpAlc Then
                Result = Trim(AlcTabCombo(1).GetColumnValue(i, 1))
                Exit For
            End If
        Next
    End If
    Alc3LookUp = Result
End Function

Private Sub LoadFlightsByAlc3(UseAlc3 As String, BeginDay As String, EndDay As String)
    Dim FlightsFound As Boolean
    Dim CedaError As Boolean
    Dim RetCode As Integer
    Dim CdrAnsw As Boolean
    Dim tmpResult As String
    Dim tmpArrSqlKey As String
    Dim tmpArrFldLst As String
    Dim tmpDepSqlKey As String
    Dim tmpDepFldLst As String
    Dim tmpKeyItems As String
    Dim tmpVpfr As String
    Dim tmpVpto As String
    Dim tmpFkey As String
    Dim tmpTime As String
    Dim MaxLine As Long
    Dim CurLine As Long
    Dim FromLine As Long
    Dim ToLine As Long
    Dim BufferSteps As Long
    Dim ReadDays As Long
    Dim FirstDay
    Dim LastDay
    Dim ReadFrom
    Dim ReadTo
    Dim ReadSteps As Long
    Dim LoopCount As Long
    Dim LastLineCount As Long
    Dim i As Long
    
    Dim StartZeit

    Screen.MousePointer = 11
    NoopList(1).ResetContent
    BufferSteps = 50

    DspLoading.Visible = True
    DspLoaded.Visible = False
    Me.Refresh
    
    CedaLed(0).Visible = True
    CedaLed(1).Visible = True
    FirstDay = CedaDateToVb(BeginDay)
    LastDay = CedaDateToVb(EndDay)
    ReadDays = 30
    ReadSteps = ((DateDiff("d", FirstDay, LastDay) - 1) / ReadDays) + 1
    If ReadSteps < 1 Then ReadSteps = 1
    SetProgressBar 0, 1, ReadSteps, "Reading from the database ..."

    NoopList(1).CedaCurrentApplication = UfisServer.ModName
    NoopList(1).CedaHopo = UfisServer.HOPO
    NoopList(1).CedaIdentifier = ""
    NoopList(1).CedaPort = "3357"
    NoopList(1).CedaReceiveTimeout = "250"
    NoopList(1).CedaRecordSeparator = vbLf
    NoopList(1).CedaSendTimeout = "250"
    NoopList(1).CedaServerName = UfisServer.HostName
    NoopList(1).CedaTabext = UfisServer.TblExt
    NoopList(1).CedaUser = UfisServer.ModName
    NoopList(1).CedaWorkstation = UfisServer.GetMyWorkStationName
    
    tmpArrFldLst = "PRFL,FTYP,FLNO,ADID,STOA,DOOA,ACT3,ORG3,VIA3,STYP,REGN,FLTN,TIFA,RTYP,FKEY,RKEY,URNO"
    tmpDepFldLst = "PRFL,FTYP,FLNO,ADID,STOD,DOOD,ACT3,DES3,VIA3,STYP,REGN,FLTN,TIFD,RTYP,FKEY,RKEY,URNO"
    tmpArrFldLst = Replace(tmpArrFldLst, "STYP", MainDialog.UseStypField, 1, -1, vbBinaryCompare)
    tmpDepFldLst = Replace(tmpDepFldLst, "STYP", MainDialog.UseStypField, 1, -1, vbBinaryCompare)
    
    StartZeit = Timer
    LoopCount = 0
    CedaError = False
    ReadFrom = FirstDay
    Do
        FlightsFound = False
        ReadTo = DateAdd("d", (ReadDays - 1), ReadFrom)
        If ReadTo > LastDay Then ReadTo = LastDay
        tmpVpfr = Format(ReadFrom, "yyyymmdd")
        tmpVpto = Format(ReadTo, "yyyymmdd")
        lblVpfr.Caption = DecodeSsimDayFormat(tmpVpfr, "CEDA", "SSIM2")
        lblVpto.Caption = DecodeSsimDayFormat(tmpVpto, "CEDA", "SSIM2")
        DspLoading.Refresh
        LoopCount = LoopCount + 1
        SetProgressBar 0, 2, LoopCount, ""
        tmpArrSqlKey = "WHERE STOA BETWEEN '" & tmpVpfr & "000000' AND '" & tmpVpto & "235959' AND ALC3 IN (" & UseAlc3 & ") AND ADID='A' AND FLNO<>' '"
        tmpDepSqlKey = "WHERE STOD BETWEEN '" & tmpVpfr & "000000' AND '" & tmpVpto & "235959' AND ALC3 IN (" & UseAlc3 & ") AND ADID='D' AND FLNO<>' '"
        If MySetUp.chkServer(0).Value = 1 Then
            tmpKeyItems = "{=CMD=}" & UseGfrCmd & "{=TBL=}AFTTAB{=EVT=}2"
            tmpKeyItems = tmpKeyItems & "{=EVT_1=}{=FLD=}" & tmpArrFldLst & "{=WHE=}" & tmpArrSqlKey
            tmpKeyItems = tmpKeyItems & "{=EVT_2=}{=FLD=}" & tmpDepFldLst & "{=WHE=}" & tmpDepSqlKey
            LastLineCount = NoopList(1).GetLineCount
            CedaLed(0).FillColor = NormalYellow
            CedaLed(0).Refresh
            CedaLed(1).FillColor = NormalYellow
            CedaLed(1).Refresh
            CdrAnsw = NoopList(1).CedaFreeCommand(tmpKeyItems)
            If CdrAnsw = False Then
                CedaLed(0).FillColor = vbRed
                CedaLed(0).Refresh
                CedaLed(1).FillColor = vbRed
                CedaLed(1).Refresh
                MsgBox NoopList(1).GetLastCedaError
                CedaError = True
            Else
                CedaLed(0).FillColor = NormalGreen
                CedaLed(0).Refresh
                CedaLed(1).FillColor = NormalGreen
                CedaLed(1).Refresh
                If NoopList(1).GetLineCount > LastLineCount Then FlightsFound = True
            End If
        Else
            CedaLed(0).FillColor = NormalYellow
            CedaLed(0).Refresh
            RetCode = UfisServer.CallCeda(tmpResult, UseGfrCmd, "AFTTAB", tmpArrFldLst, "", tmpArrSqlKey, "", 0, False, False)
            CedaLed(0).FillColor = NormalGreen
            CedaLed(0).Refresh
            MaxLine = UfisServer.DataBuffer(0).GetLineCount - 1
            If MaxLine >= 0 Then
                FlightsFound = True
                If MaxLine = 0 Then
                    tmpResult = TrimRecordData(UfisServer.DataBuffer(0).GetLineValues(0))
                    If Left(tmpResult, 3) <> ",,," Then
                        NoopList(1).InsertTextLine tmpResult, False
                    End If
                    MaxLine = -1
                End If
                For CurLine = 0 To MaxLine
                    tmpResult = TrimRecordData(UfisServer.DataBuffer(0).GetLineValues(CurLine))
                    NoopList(1).InsertTextLine tmpResult, False
                Next
            End If
            MaxLine = NoopList(1).GetLineCount
            SrvFkeyCnt(0).Caption = Str(MaxLine)
            SrvFkeyCnt(0).Refresh
            CedaLed(1).FillColor = NormalYellow
            CedaLed(1).Refresh
            RetCode = UfisServer.CallCeda(tmpResult, UseGfrCmd, "AFTTAB", tmpDepFldLst, "", tmpDepSqlKey, "", 0, False, False)
            CedaLed(1).FillColor = NormalGreen
            CedaLed(1).Refresh
            MaxLine = UfisServer.DataBuffer(0).GetLineCount - 1
            If MaxLine >= 0 Then
                FlightsFound = True
                If MaxLine = 0 Then
                    tmpResult = TrimRecordData(UfisServer.DataBuffer(0).GetLineValues(0))
                    If Left(tmpResult, 3) <> ",,," Then
                        NoopList(1).InsertTextLine tmpResult, False
                    End If
                    MaxLine = -1
                End If
                For CurLine = 0 To MaxLine
                    tmpResult = TrimRecordData(UfisServer.DataBuffer(0).GetLineValues(CurLine))
                    NoopList(1).InsertTextLine tmpResult, False
                Next
            End If
            MaxLine = NoopList(1).GetLineCount
            SrvFkeyCnt(0).Caption = Str(MaxLine)
            SrvFkeyCnt(0).Refresh
        End If
        If (FlightsFound) And ((MaxLine < 32) Or (LoopCount = 1)) Then
            NoopList(1).RedrawTab
            Me.Refresh
        End If
        ReadFrom = DateAdd("d", 1, ReadTo)
    Loop While (ReadFrom <= LastDay) And (Not CedaError)
    If CedaError Then
        NoopList(0).ResetContent
        NoopList(0).RedrawTab
        NoopList(1).ResetContent
    End If
    NoopList(1).RedrawTab
    
    DspLoading.Visible = False
    tmpResult = Trim(Str(Timer - StartZeit))
    If Left(tmpResult, 1) = "." Then tmpResult = "0" & tmpResult
    tmpResult = GetItem(tmpResult, 1, ".") & "." & Left(GetItem(tmpResult, 2, "."), 3) & "s"
    UsedSec.Caption = tmpResult
    DspLoaded.Visible = True
    SetProgressBar 0, 3, 0, ""
    Me.Refresh
    CedaLed(0).Visible = False
    CedaLed(1).Visible = False
    
    Screen.MousePointer = 0
    SrvFkeyCnt(0).Caption = Str(NoopList(1).GetLineCount)
    Me.Refresh
    CreateServerRotations
End Sub

Private Sub LoadFlightsByFkey(UseArrRkeys As String, UseDepRkeys As String)
    Dim AppendFlights As Boolean
    Dim FlightsFound As Boolean
    Dim RetCode As Integer
    Dim CdrAnsw As Boolean
    Dim CedaError As Boolean
    Dim tmpResult As String
    Dim ArrFkeys As String
    Dim DepFkeys As String
    Dim tmpArrSqlKey As String
    Dim tmpArrFldLst As String
    Dim tmpDepSqlKey As String
    Dim tmpDepFldLst As String
    Dim tmpKeyItems As String
    Dim tmpFkey As String
    Dim tmpTime As String
    Dim MaxLine As Long
    Dim CurLine As Long
    Dim ReadMaxLine As Long
    Dim FromLine As Long
    Dim ToLine As Long
    Dim LinCnt As Long
    Dim LastLineCount As Long
    Dim AppLine As Long
    Dim MaxAppLine As Long
    Dim BufferSteps As Long
    Dim i As Long
    
    Dim StartZeit
    
    Screen.MousePointer = 11
    DspLoaded.Visible = False
    CedaLed(0).Visible = True
    CedaLed(1).Visible = True
    If (UseArrRkeys <> "") Or (UseDepRkeys <> "") Then AppendFlights = True Else AppendFlights = False
    If Not AppendFlights Then NoopList(1).ResetContent

    If MySetUp.chkServer(0).Value = 1 Then
        NoopList(1).CedaCurrentApplication = UfisServer.ModName
        NoopList(1).CedaHopo = UfisServer.HOPO
        NoopList(1).CedaIdentifier = "ID"
        NoopList(1).CedaPort = "3357"
        NoopList(1).CedaReceiveTimeout = "250"
        NoopList(1).CedaRecordSeparator = vbLf
        NoopList(1).CedaSendTimeout = "250"
        NoopList(1).CedaServerName = UfisServer.HostName
        NoopList(1).CedaTabext = UfisServer.TblExt
        NoopList(1).CedaUser = UfisServer.ModName
        NoopList(1).CedaWorkstation = UfisServer.GetMyWorkStationName
    End If
    
    tmpArrFldLst = "PRFL,FTYP,FLNO,ADID,STOA,DOOA,ACT3,ORG3,VIA3,STYP,REGN,FLTN,TIFA,RTYP,FKEY,RKEY,URNO"
    tmpDepFldLst = "PRFL,FTYP,FLNO,ADID,STOD,DOOD,ACT3,DES3,VIA3,STYP,REGN,FLTN,TIFD,RTYP,FKEY,RKEY,URNO"
    tmpArrFldLst = Replace(tmpArrFldLst, "STYP", MainDialog.UseStypField, 1, -1, vbBinaryCompare)
    tmpDepFldLst = Replace(tmpDepFldLst, "STYP", MainDialog.UseStypField, 1, -1, vbBinaryCompare)
    
    StartZeit = Timer
    BufferSteps = 500
    ReadMaxLine = NoopList(0).GetLineCount - 1
    SetProgressBar 0, 1, ReadMaxLine, "Reading from the database ..."
    If AppendFlights Then FromLine = ReadMaxLine Else FromLine = 0
    While FromLine <= ReadMaxLine
        ToLine = FromLine + BufferSteps
        If ToLine > ReadMaxLine Then ToLine = ReadMaxLine
        SetProgressBar 0, 2, ToLine, ""
        FlightsFound = False
        If Not AppendFlights Then
            CreateFkeyFilters FromLine, ToLine, ArrFkeys, DepFkeys
            tmpArrSqlKey = "WHERE FKEY IN (" & ArrFkeys & ") AND ADID='A' AND FTYP NOT IN ('T','G')"
            tmpDepSqlKey = "WHERE FKEY IN (" & DepFkeys & ") AND ADID='D' AND FTYP NOT IN ('T','G')"
        Else
            ArrFkeys = UseArrRkeys
            tmpArrSqlKey = "WHERE RKEY IN (" & ArrFkeys & ") AND ADID='A' AND FTYP NOT IN ('T','G')"
            DepFkeys = UseDepRkeys
            tmpDepSqlKey = "WHERE RKEY IN (" & DepFkeys & ") AND ADID='D' AND FTYP NOT IN ('T','G')"
        End If
        If ((ArrFkeys <> "") And (DepFkeys <> "")) And (MySetUp.chkServer(0).Value = 1) Then
            tmpKeyItems = "{=CMD=}" & UseGfrCmd & "{=TBL=}AFTTAB{=EVT=}2"
            tmpKeyItems = tmpKeyItems & "{=EVT_1=}{=FLD=}" & tmpArrFldLst & "{=WHE=}" & tmpArrSqlKey
            tmpKeyItems = tmpKeyItems & "{=EVT_2=}{=FLD=}" & tmpDepFldLst & "{=WHE=}" & tmpDepSqlKey
            LastLineCount = NoopList(1).GetLineCount
            CedaLed(0).FillColor = NormalYellow
            CedaLed(0).Refresh
            CedaLed(1).FillColor = NormalYellow
            CedaLed(1).Refresh
            CdrAnsw = NoopList(1).CedaFreeCommand(tmpKeyItems)
            If CdrAnsw = False Then
                CedaLed(0).FillColor = vbRed
                CedaLed(0).Refresh
                CedaLed(1).FillColor = vbRed
                CedaLed(1).Refresh
                MsgBox NoopList(1).GetLastCedaError
                CedaError = True
            Else
                CedaLed(0).FillColor = NormalGreen
                CedaLed(0).Refresh
                CedaLed(1).FillColor = NormalGreen
                CedaLed(1).Refresh
                If NoopList(1).GetLineCount > LastLineCount Then
                    If AppendFlights Then
                        MaxAppLine = NoopList(1).GetLineCount - 1
                        For AppLine = LastLineCount To MaxAppLine
                            NoopList(1).SetColumnValue AppLine, RemaCol, "Linked Server Rotation"
                            NoopList(1).SetColumnValue AppLine, 0, "L"
                        Next
                    End If
                    FlightsFound = True
                End If
            End If
            ArrFkeys = ""
            DepFkeys = ""
        End If
        If ArrFkeys <> "" Then
            CedaLed(0).FillColor = NormalYellow
            CedaLed(0).Refresh
            If MySetUp.chkServer(0).Value = 1 Then
                LastLineCount = NoopList(1).GetLineCount
                CdrAnsw = NoopList(1).CedaAction(UseGfrCmd, "AFTTAB", tmpArrFldLst, "", tmpArrSqlKey)
                If CdrAnsw = False Then
                    CedaLed(0).FillColor = vbRed
                    CedaLed(0).Refresh
                    MsgBox NoopList(1).GetLastCedaError
                    CedaError = True
                Else
                    CedaLed(0).FillColor = NormalGreen
                    CedaLed(0).Refresh
                    If NoopList(1).GetLineCount > LastLineCount Then
                        If AppendFlights Then
                            MaxAppLine = NoopList(1).GetLineCount - 1
                            For AppLine = LastLineCount To MaxAppLine
                                NoopList(1).SetColumnValue AppLine, RemaCol, "Linked Server Rotation"
                                NoopList(1).SetColumnValue AppLine, 0, "L"
                            Next
                        End If
                        FlightsFound = True
                    End If
                End If
            Else
                RetCode = UfisServer.CallCeda(tmpResult, UseGfrCmd, "AFTTAB", tmpArrFldLst, "", tmpArrSqlKey, "", 0, False, False)
                CedaLed(0).FillColor = NormalGreen
                CedaLed(0).Refresh
                MaxLine = UfisServer.DataBuffer(0).GetLineCount - 1
                If MaxLine >= 0 Then
                    FlightsFound = True
                    AppLine = NoopList(1).GetLineCount
                    If MaxLine = 0 Then
                        tmpResult = TrimRecordData(UfisServer.DataBuffer(0).GetLineValues(0))
                        If Left(tmpResult, 3) <> ",,," Then
                            NoopList(1).InsertTextLine tmpResult, False
                            If AppendFlights Then
                                NoopList(1).SetColumnValue AppLine, RemaCol, "Linked Server Rotation"
                                NoopList(1).SetColumnValue AppLine, 0, "L"
                            End If
                        End If
                        MaxLine = -1
                    End If
                    For CurLine = 0 To MaxLine
                        tmpResult = TrimRecordData(UfisServer.DataBuffer(0).GetLineValues(CurLine))
                        NoopList(1).InsertTextLine tmpResult, False
                        If AppendFlights Then
                            NoopList(1).SetColumnValue AppLine, RemaCol, "Linked Server Rotation"
                            NoopList(1).SetColumnValue AppLine, 0, "L"
                            AppLine = AppLine + 1
                        End If
                    Next
                End If
            End If
            LinCnt = NoopList(1).GetLineCount
            SrvFkeyCnt(0).Caption = Str(LinCnt)
            SrvFkeyCnt(0).Refresh
        End If
        
        If DepFkeys <> "" Then
            CedaLed(1).FillColor = NormalYellow
            CedaLed(1).Refresh
            If MySetUp.chkServer(0).Value = 1 Then
                LastLineCount = NoopList(1).GetLineCount
                CdrAnsw = NoopList(1).CedaAction(UseGfrCmd, "AFTTAB", tmpDepFldLst, "", tmpDepSqlKey)
                If CdrAnsw = False Then
                    CedaLed(1).FillColor = vbRed
                    CedaLed(1).Refresh
                    MsgBox NoopList(1).GetLastCedaError
                    CedaError = True
                Else
                    CedaLed(1).FillColor = NormalGreen
                    CedaLed(1).Refresh
                    If NoopList(1).GetLineCount > LastLineCount Then
                        If AppendFlights Then
                            MaxAppLine = NoopList(1).GetLineCount - 1
                            For AppLine = LastLineCount To MaxAppLine
                                NoopList(1).SetColumnValue AppLine, RemaCol, "Linked Server Rotation"
                                NoopList(1).SetColumnValue AppLine, 0, "L"
                            Next
                        End If
                        FlightsFound = True
                    End If
                End If
            Else
                RetCode = UfisServer.CallCeda(tmpResult, UseGfrCmd, "AFTTAB", tmpDepFldLst, "", tmpDepSqlKey, "", 0, False, False)
                CedaLed(1).FillColor = NormalGreen
                CedaLed(1).Refresh
                MaxLine = UfisServer.DataBuffer(0).GetLineCount - 1
                If MaxLine >= 0 Then
                    FlightsFound = True
                    AppLine = NoopList(1).GetLineCount
                    If MaxLine = 0 Then
                        tmpResult = TrimRecordData(UfisServer.DataBuffer(0).GetLineValues(0))
                        If Left(tmpResult, 3) <> ",,," Then
                            NoopList(1).InsertTextLine tmpResult, False
                            If AppendFlights Then
                                NoopList(1).SetColumnValue AppLine, RemaCol, "Linked Server Rotation"
                                NoopList(1).SetColumnValue AppLine, 0, "L"
                            End If
                        End If
                        MaxLine = -1
                    End If
                    For CurLine = 0 To MaxLine
                        tmpResult = TrimRecordData(UfisServer.DataBuffer(0).GetLineValues(CurLine))
                        NoopList(1).InsertTextLine tmpResult, False
                        If AppendFlights Then
                            NoopList(1).SetColumnValue AppLine, RemaCol, "Linked Server Rotation"
                            NoopList(1).SetColumnValue AppLine, 0, "L"
                            AppLine = AppLine + 1
                        End If
                    Next
                End If
            End If
            LinCnt = NoopList(1).GetLineCount
            SrvFkeyCnt(0).Caption = Str(LinCnt)
            SrvFkeyCnt(0).Refresh
        End If
        If (FlightsFound) And ((LinCnt < 32) Or (CurLine = 0)) Then
            NoopList(1).RedrawTab
            Me.Refresh
        End If
        FromLine = ToLine + 1
    Wend
    NoopList(1).RedrawTab
    
    If Not AppendFlights Then
        tmpResult = Trim(Str(Timer - StartZeit))
        If Left(tmpResult, 1) = "." Then tmpResult = "0" & tmpResult
        tmpResult = GetItem(tmpResult, 1, ".") & "." & Left(GetItem(tmpResult, 2, "."), 3) & "s"
        UsedSec.Caption = tmpResult
    End If
    DspLoaded.Visible = True
    SetProgressBar 0, 3, 0, ""
    CedaLed(0).Visible = False
    CedaLed(1).Visible = False
    
    Screen.MousePointer = 0
    SrvFkeyCnt(0).Caption = Str(NoopList(1).GetLineCount)
    Me.Refresh
    If Not AppendFlights Then CreateServerRotations
End Sub

Private Sub CreateFkeyFilters(FromLine As Long, ToLine As Long, ArrFkeys As String, DepFkeys As String)
    Dim tmpAdid As String
    Dim tmpFkey As String
    Dim CurLine As Long
    ArrFkeys = ""
    DepFkeys = ""
    For CurLine = FromLine To ToLine
        tmpFkey = NoopList(0).GetColumnValue(CurLine, FkeyCol)
        tmpAdid = Mid(tmpFkey, 18, 1)
        tmpFkey = Mid(tmpFkey, 4, 5) & Left(tmpFkey, 3) & Mid(tmpFkey, 9)
        Select Case tmpAdid
            Case "A"
                ArrFkeys = ArrFkeys & "'" & tmpFkey & "',"
            Case "D"
                DepFkeys = DepFkeys & "'" & tmpFkey & "',"
            Case Else
        End Select
    Next
    If ArrFkeys <> "" Then ArrFkeys = Left(ArrFkeys, Len(ArrFkeys) - 1)
    If DepFkeys <> "" Then DepFkeys = Left(DepFkeys, Len(DepFkeys) - 1)
End Sub
Private Function TurnImpFkeyToAftFkey(CurFkey As String) As String
    TurnImpFkeyToAftFkey = Mid(CurFkey, 4, 5) & Left(CurFkey, 3) & Mid(CurFkey, 9)
End Function
Private Sub Form_Resize()
    Dim NewHeight As Long
    Dim NewTop As Long
    Dim i As Integer
    If chkWork(13).Value = 1 Then
        RotaLink(0).Visible = True
        RotaLink(1).Visible = True
        RotaLink(2).Visible = True
        'BottomLabels(3).Visible = True
    Else
        RotaLink(0).Visible = False
        RotaLink(1).Visible = False
        RotaLink(2).Visible = False
        'BottomLabels(3).Visible = False
    End If
    NewTop = Me.ScaleHeight - StatusBar.Height - BottomLabels(0).Height - 4 * Screen.TwipsPerPixelY
    If NewTop > 150 Then
        BottomLabels(0).Top = NewTop
        BottomLabels(1).Top = NewTop
        BottomLabels(2).Top = NewTop
        NewHeight = NewTop + BottomLabels(0).Height + 2 * Screen.TwipsPerPixelY
        For i = 0 To 5
            RLine(i).Y2 = NewHeight
        Next
    End If
    If chkWork(13).Value = 1 Then
        NewTop = NewTop - RotaLink(0).Height + 8 * Screen.TwipsPerPixelY
        If NewTop > 150 Then
            RotaLink(0).Top = NewTop
            RotaLink(1).Top = NewTop
            RotaLink(2).Top = NewTop
            'BottomLabels(3).Top = NewTop - 8 * Screen.TwipsPerPixelY
        End If
    Else
        NewTop = NewTop + 8 * Screen.TwipsPerPixelY
    End If
    NewHeight = NewTop - NoopList(0).Top - 1 * Screen.TwipsPerPixelY
    If NewHeight > 100 Then
        NoopList(0).Height = NewHeight
        NoopList(2).Height = NewHeight
        NoopList(1).Height = NewHeight
    End If
    BLine(1).Y1 = RLine(0).Y2
    BLine(1).Y2 = BLine(1).Y1
End Sub

Private Sub LoadClientFlights()
    Dim CurLine As Long
    Dim MaxLine As Long
    Dim LineCount As Long
    Dim RotaCount As Long
    Dim BufferCount As Long
    Dim CurDateDiff As Long
    Dim CurDateCount As Long
    Dim DataLine As String
    Dim LineBuff As String
    Dim strFrqd As String
    Dim WeekFreq As Integer
    Dim DayOffset As Integer
    
    Dim CurCode As String
    
    Dim ArrDate As String
    Dim ArrStoa As String
    Dim ArrFlno As String
    Dim ArrAlc3 As String
    Dim ArrFlca As String
    Dim ArrFltn As String
    Dim ArrFlns As String
    Dim ArrFkey As String
    Dim ArrVia3 As String
    Dim ArrTtyp As String
    
    Dim DepDate As String
    Dim DepStod As String
    Dim DepFlno As String
    Dim DepAlc3 As String
    Dim DepFlcd As String
    Dim DepFltn As String
    Dim DepFlns As String
    Dim DepFkey As String
    Dim DepVia3 As String
    Dim DepTtyp As String
    
    Dim CurFtyp As String
    Dim CurRegn As String
    Dim CurAct3 As String
    Dim CurOrg3 As String
    Dim CurDes3 As String
    Dim CurRkey As String
    
    Dim varVpfr As Variant
    Dim varVpto As Variant
    Dim varFday As Variant
    Dim strWkdy As String
    Dim ArrWkdy As String
    Dim DepWkdy As String
    Dim WkDayCnt As Integer
    Dim tmpData As String
    
    Dim varStoa As Variant
    Dim varStod As Variant
    Dim varCurStoa As Variant
    Dim varCurStod As Variant
    
    Dim ArrGrti As Long
    Dim DepGrti As Long
    
    Dim IsValidWeek As Boolean
    Dim InvalidDays As Integer
    Dim TurnFlag As Integer
    Dim ArrFlt As Boolean
    Dim DepFlt As Boolean
    
    Dim SeasVpfr As String
    Dim SeasVpto As String
    Dim UseAlc3 As String
    Dim ArrIdx As Integer
    Dim DepIdx As Integer
    Dim OutRange As Long
    
    Dim FirstDay
    Dim LastDay
    Dim PeriodRange As Long
    Dim i As Integer
    Dim j As Integer
    
    Dim OrigLine As Long
    Dim tmpLinDat As String
    Dim tmpArrFlight As String
    Dim tmpDepFlight As String
    Dim ArrRota As String
    Dim DepRota As String
    Dim LineType As String
    
    Screen.MousePointer = 11
    DspLoading.Visible = False
    DspLoaded.Visible = False
    Me.Refresh
    MaxLine = FlightExtract.ExtractData.GetLineCount - 1
    
    SeasVpfr = CurBegin.Tag
    SeasVpto = CurEnd.Tag
    
    FirstDay = CedaDateToVb(SeasVpfr)
    LastDay = CedaDateToVb(SeasVpto)
    PeriodRange = DateDiff("d", FirstDay, LastDay)
    If PeriodRange >= 0 Then
        NoopList(0).ResetContent
        NoopList(1).ResetContent
        Screen.MousePointer = 11
        Refresh
        SetProgressBar 0, 1, MaxLine, "Creating Flights from File ..."
        LineBuff = ""
        LineCount = 0
        BufferCount = 0
        RotaCount = 0
        
        For CurLine = 0 To MaxLine
            If CurLine Mod 10 = 0 Then SetProgressBar 0, 2, CurLine, ""
            DataLine = FlightExtract.ExtractData.GetLineValues(CurLine)
            
            'Attention:
            'The Extract Datalist contains a Period Begin (VPFR) cut to the first day of the filter.
            'In order to create all flights at the beginning of the filtered timeframe correctly,
            'we need the original period begin that is still kept in the lines of the main datalist.
            'Unfortunately it still works only with SLOT file formats
            'If MainDialog.DataSystemType = "SLOT" Then
            '    tmpLinDat = GetItem(DataLine, 28, ",")
            '    OrigLine = Val(tmpLinDat)
            '    DataLine = MainDialog.FileData.GetLineValues(OrigLine)
            'End If
            LineType = Trim(GetItem(DataLine, 2, ","))
            If (LineType <> "-") And (LineType <> "E") Then
                'No Ignore and no Error Line
                CurCode = GetItem(DataLine, 3, ",")
                
                varVpfr = CedaDateToVb(GetItem(DataLine, 8, ","))
                varVpto = CedaDateToVb(GetItem(DataLine, 9, ","))
                CurDateDiff = DateDiff("d", varVpfr, varVpto)
                strFrqd = GetItem(DataLine, 10, ",")
                WeekFreq = Val(GetItem(DataLine, 22, ","))
                DayOffset = Val(GetItem(DataLine, 17, ",")) 'Defines the departure
                
                'Determine what type of rotation we got
                ArrFltn = GetItem(DataLine, 5, ",")
                If ArrFltn <> "" Then
                    ArrAlc3 = Alc3LookUp(GetItem(DataLine, 4, ","))
                    ArrFlt = True
                Else
                    ArrAlc3 = ""
                    ArrFlt = False
                End If
                DepFltn = GetItem(DataLine, 7, ",")
                If DepFltn <> "" Then
                    DepAlc3 = Alc3LookUp(GetItem(DataLine, 6, ","))
                    DepFlt = True
                Else
                    DepAlc3 = ""
                    DepFlt = False
                End If
                TurnFlag = 0
                If ArrFlt Then TurnFlag = TurnFlag + 1
                If DepFlt Then TurnFlag = TurnFlag + 2
                
                CurRegn = GetItem(DataLine, 23, ",")
                CurFtyp = GetItem(DataLine, 24, ",")
                If CurFtyp = "" Then CurFtyp = MainDialog.DefaultFtyp
                If CurFtyp = "C" Then CurFtyp = "X"
                varStoa = CedaFullDateToVb(GetItem(DataLine, 15, ","))
                varStod = CedaFullDateToVb(GetItem(DataLine, 16, ","))
                ArrStoa = Mid(GetItem(DataLine, 15, ","), 9)
                DepStod = Mid(GetItem(DataLine, 16, ","), 9)
                ArrFlca = GetItem(DataLine, 4, ",")
                DepFlcd = GetItem(DataLine, 6, ",")
                CurAct3 = GetItem(DataLine, 11, ",")
                CurOrg3 = GetItem(DataLine, 13, ",")
                ArrVia3 = GetItem(DataLine, 14, ",")
                DepVia3 = GetItem(DataLine, 18, ",")
                CurDes3 = GetItem(DataLine, 19, ",")
                ArrTtyp = GetItem(DataLine, 20, ",")
                DepTtyp = GetItem(DataLine, 21, ",")
                ArrFlns = ""
                DepFlns = ""
                ArrFlno = BuildProperAftFlno(ArrFlca, ArrFltn, ArrFlns)
                DepFlno = BuildProperAftFlno(DepFlcd, DepFltn, DepFlns)
                
                WkDayCnt = 0
                IsValidWeek = True
                InvalidDays = 0
                OutRange = 0
                
                ArrIdx = -1
                If TurnFlag = 3 Then
                    DepIdx = DateDiff("d", varStoa, varStod)
                Else
                    DepIdx = 0
                End If
                If CurDateDiff > 190 Then SetProgressBar 1, 1, CurDateDiff, ""
                CurDateCount = 0
                varFday = varVpfr
                Do
                    If CurDateDiff > 190 Then CurDateCount = CurDateCount + 1
                    ArrIdx = ArrIdx + 1
                    strWkdy = Trim(Str(Weekday(varFday, vbMonday)))
                    If IsValidWeek And (InStr(strFrqd, strWkdy) > 0) Then
                        'Hit. Always valid for Arrivals or single Departures
                        If CurDateDiff > 190 Then SetProgressBar 1, 2, CurDateCount, ""
                        tmpData = Trim(Str(CurLine))
                        CurRkey = Right("00000" & tmpData, 5)
                        tmpData = Trim(Str(ArrIdx))
                        CurRkey = CurRkey & Right("00000" & tmpData, 5)
                        
                        Select Case TurnFlag
                            Case 1  'Single arrival
                                varCurStoa = varFday
                                ArrDate = Format(varCurStoa, "yyyymmdd")
                                If (ArrDate >= SeasVpfr) And (ArrDate <= SeasVpto) Then
                                    ArrFkey = TurnAftFkey(BuildAftFkey(ArrAlc3, ArrFltn, ArrFlns, ArrDate, "A"))
                                    tmpData = CurCode + "," + CurFtyp + "," + ArrFlno + ",A," + ArrDate + ArrStoa + "," + strWkdy + "," + CurAct3 + "," + CurOrg3 + "," + ArrVia3 + "," + ArrTtyp + "," + CurRegn + ",    -,,," + ArrFkey + "," + CurRkey + "," + Str(CurLine) + ",-1/-1,"
                                    LineBuff = LineBuff + tmpData + vbLf
                                    BufferCount = BufferCount + 1
                                    LineCount = LineCount + 1
                                End If
                            Case 2  'Single departure
                                varCurStod = DateAdd("d", DepIdx, varFday)
                                DepDate = Format(varCurStod, "yyyymmdd")
                                If (DepDate >= SeasVpfr) And (DepDate <= SeasVpto) Then
                                    strWkdy = Trim(Str(Weekday(varCurStod, vbMonday)))
                                    DepFkey = TurnAftFkey(BuildAftFkey(DepAlc3, DepFltn, DepFlns, DepDate, "D"))
                                    tmpData = CurCode + "," + CurFtyp + "," + DepFlno + ",D," + DepDate + DepStod + "," + strWkdy + "," + CurAct3 + "," + CurDes3 + "," + DepVia3 + "," + DepTtyp + "," + CurRegn + ",    -,,," + DepFkey + "," + CurRkey + "," + Str(CurLine) + ",-1/-1,"
                                    LineBuff = LineBuff + tmpData + vbLf
                                    BufferCount = BufferCount + 1
                                    LineCount = LineCount + 1
                                End If
                            Case 3  'Rotation
                                ArrFkey = ""
                                DepFkey = ""
                                varCurStoa = varFday
                                ArrDate = Format(varCurStoa, "yyyymmdd")
                                If (ArrDate >= SeasVpfr) And (ArrDate <= SeasVpto) Then
                                    ArrWkdy = strWkdy
                                    ArrFkey = TurnAftFkey(BuildAftFkey(ArrAlc3, ArrFltn, ArrFlns, ArrDate, "A"))
                                    RotaList(0).InsertTextLine Trim(Str(RotaCount)) & ",-1,-1", False
                                    ArrRota = Trim(Str(RotaCount + 1)) + "/" + Trim(Str(RotaCount))
                                    BufferCount = BufferCount + 1
                                    LineCount = LineCount + 1
                                    RotaCount = RotaCount + 1
                                End If
                                varCurStod = DateAdd("d", DepIdx, varFday)
                                DepDate = Format(varCurStod, "yyyymmdd")
                                If (DepDate >= SeasVpfr) And (DepDate <= SeasVpto) Then
                                    DepWkdy = Trim(Str(Weekday(varCurStod, vbMonday)))
                                    DepFkey = TurnAftFkey(BuildAftFkey(DepAlc3, DepFltn, DepFlns, DepDate, "D"))
                                    RotaList(0).InsertTextLine Trim(Str(RotaCount)) & ",-1,-1", False
                                    DepRota = Trim(Str(RotaCount - 1)) + "/" + Trim(Str(RotaCount))
                                    BufferCount = BufferCount + 1
                                    LineCount = LineCount + 1
                                    RotaCount = RotaCount + 1
                                End If
                                If (ArrFkey <> "") And (DepFkey <> "") Then
                                    tmpArrFlight = CurCode + "," + CurFtyp + "," + ArrFlno + ",A," + ArrDate + ArrStoa + "," + ArrWkdy + "," + CurAct3 + "," + CurOrg3 + "," + ArrVia3 + "," + ArrTtyp + "," + CurRegn + "," + DepFlno + "," + DepDate + DepStod + ",D," + ArrFkey + "," + CurRkey + "," + Str(CurLine) + "," + ArrRota + ","
                                    LineBuff = LineBuff + tmpArrFlight + vbLf
                                    tmpDepFlight = CurCode + "," + CurFtyp + "," + DepFlno + ",D," + DepDate + DepStod + "," + DepWkdy + "," + CurAct3 + "," + CurDes3 + "," + DepVia3 + "," + DepTtyp + "," + CurRegn + "," + ArrFlno + "," + ArrDate + ArrStoa + ",A," + DepFkey + "," + CurRkey + "," + Str(CurLine) + "," + DepRota + ","
                                    LineBuff = LineBuff + tmpDepFlight + vbLf
                                ElseIf ArrFkey <> "" Then
                                    tmpArrFlight = CurCode + "," + CurFtyp + "," + ArrFlno + ",A," + ArrDate + ArrStoa + "," + ArrWkdy + "," + CurAct3 + "," + CurOrg3 + "," + ArrVia3 + "," + ArrTtyp + "," + CurRegn + ",,,," + ArrFkey + "," + CurRkey + "," + Str(CurLine) + ",,"
                                    LineBuff = LineBuff + tmpArrFlight + vbLf
                                ElseIf DepFkey <> "" Then
                                    tmpDepFlight = CurCode + "," + CurFtyp + "," + DepFlno + ",D," + DepDate + DepStod + "," + DepWkdy + "," + CurAct3 + "," + CurDes3 + "," + DepVia3 + "," + DepTtyp + "," + CurRegn + ",,,," + DepFkey + "," + CurRkey + "," + Str(CurLine) + ",,"
                                    LineBuff = LineBuff + tmpDepFlight + vbLf
                                End If
                            Case Else
                        End Select
                    End If
                    varFday = DateAdd("d", 1, varFday)
                    If Not IsValidWeek Then InvalidDays = InvalidDays - 1
                    WkDayCnt = WkDayCnt + 1
                    If WkDayCnt = 7 Then
                        If IsValidWeek Then
                            InvalidDays = (WeekFreq - 1) * 7
                            If InvalidDays > 0 Then IsValidWeek = False
                        Else
                            If InvalidDays = 0 Then
                                IsValidWeek = True
                            Else
                                'here we have a problem
                                'MsgBox "Please tell Berni that here is a problem with the weekly frequency."
                            End If
                        End If
                        WkDayCnt = 0
                    End If
                    If BufferCount >= 150 Then
                        NoopList(0).InsertBuffer LineBuff, vbLf
                        LineBuff = ""
                        BufferCount = 0
                    End If
                Loop While varFday <= varVpto
                If CurDateDiff > 190 Then SetProgressBar 1, 3, 0, ""
            End If
        Next
        If LineBuff <> "" Then
            NoopList(0).InsertBuffer LineBuff, vbLf
            LineBuff = ""
        End If
        NoopList(0).RedrawTab
        Screen.MousePointer = 0
        If OutRange > 0 Then
            MsgBox OutRange & " flights out of period"
        End If
    Else
        MsgBox "Period range out of limits"
    End If
    CliFkeyCnt(0).Caption = Str(NoopList(0).GetLineCount)
    SrvFkeyCnt(0).Caption = Str(NoopList(1).GetLineCount)
    SetProgressBar 0, 3, 0, ""
    Screen.MousePointer = 0
    Me.Refresh
End Sub


Private Sub SearchInList(UseIndex As Integer)
    Static LastIndex As Integer
    Dim Index As Integer
    Dim HitLst As String
    Dim HitRow As Long
    Dim NxtRow As Long
    Dim NewScroll As Long
    Dim LoopCount As Integer
    Dim tmpText As String
    Dim ColList As String
    Dim ColNbr As String
    Dim UseIdx As Integer
    Dim UseCol As Long
    Index = UseIndex
    If Index < 0 Then Index = LastIndex
    LastIndex = Index
    tmpText = txtSearch(Index).Text
    Select Case Index
        Case 0
            UseIdx = 0
            ColList = Trim(Str(FlnoCol))
            'UseCol = FlnoCol
        Case 2
            UseIdx = 1
            ColList = Trim(Str(FlnoCol))
            'UseCol = FlnoCol
        Case Else
            tmpText = ""
    End Select
    If tmpText <> "" Then
        LoopCount = 0
        Do
            ColNbr = GetItem(ColList, 1, ",")
            If ColNbr <> "" Then
                UseCol = Val(ColNbr)
                HitLst = NoopList(UseIdx).GetNextLineByColumnValue(UseCol, tmpText, 1)
                HitRow = Val(HitLst)
                If HitLst <> "" Then
                    If HitRow > 5 Then NewScroll = HitRow - 5 Else NewScroll = 0
                    NoopList(2).OnVScrollTo NewScroll
                Else
                    NoopList(UseIdx).SetCurrentSelection -1
                    NoopList(2).OnVScrollTo 0
                End If
            End If
            LoopCount = LoopCount + 1
        Loop While LoopCount < 2 And HitLst = ""
        NoopList(2).SetCurrentSelection HitRow
        NoopList(UseIdx).SetCurrentSelection HitRow
    End If
End Sub

Private Sub NoopList_GotFocus(Index As Integer)
    If Index = 2 Then CheckKeyboardState GetKeybdState
End Sub

Private Sub NoopList_OnHScroll(Index As Integer, ByVal ColNo As Long)
    Static LastColNo(3) As Long
    'If Index <> 2 Then RotaLink(Index).OnHScrollTo ColNo
    RotaLink(Index).OnHScrollTo ColNo
    If Val(GetRealItem(NoopList(Index).HeaderLengthString, ColNo, ",")) <= 0 Then
        If ColNo > LastColNo(Index) Then NoopList(Index).OnHScrollTo ColNo + 1 Else NoopList(Index).OnHScrollTo ColNo - 1
    End If
    LastColNo(Index) = ColNo
End Sub

Private Sub NoopList_OnVScroll(Index As Integer, ByVal LineNo As Long)
    Dim ExtLineNo As Long
    Dim SyncLineOffs As Long
    Dim SlaveLineOffs As Long
    Dim VisLine As Long
    Dim SelLine As Long
    If Index = 2 Then
        ExtLineNo = Val(NoopList(2).GetColumnValue(LineNo, 4))
        If ExtLineNo >= 0 Then
            NoopList(0).OnVScrollTo ExtLineNo
            NoopList(1).OnVScrollTo ExtLineNo
            SelLine = NoopList(2).GetCurrentSelected
            VisLine = LineNo + ((NoopList(2).Height / Screen.TwipsPerPixelY) / NoopList(2).LineHeight) - 3
            StopNestedCalls = True
            If (SelLine >= LineNo) And (SelLine <= VisLine) Then NoopList(2).SetCurrentSelection SelLine
            StopNestedCalls = False
        Else
            NoopList(0).SetCurrentSelection -1
            NoopList(1).SetCurrentSelection -1
        End If
    End If
End Sub

Private Sub NoopList_RowSelectionChanged(Index As Integer, ByVal LineNo As Long, ByVal Selected As Boolean)
    Dim ExtLineNo As Long
    Dim SyncLineOffs As Long
    Dim SlaveLineOffs As Long
    Dim tmpdat As String
    Dim tmpLineNo As Long
    Dim tmpArrLineNo As Long
    Dim tmpDepLineNo As Long
    
    Dim IsValidRotation As Boolean
    Select Case Index
        Case 0
            If (Not ImportIsBusy) And (Selected) And (LineNo >= 0) Then
                tmpdat = NoopList(0).GetColumnValue(LineNo, UrnoCol)
                If tmpdat = "" Then tmpdat = "-1"
                ExtLineNo = Val(tmpdat)
                If ExtLineNo <> FlightExtract.ExtractData.GetCurrentSelected Then
                    FlightExtract.ExtractData.SetCurrentSelection ExtLineNo
                End If
            End If
        Case 1
            'If Selected And LineNo >= 0 Then SrvFkeyCnt(4).Caption = Trim(Str(LineNo + 1))
        Case 2
            If Selected = True Then
                ExtLineNo = Val(NoopList(2).GetColumnValue(LineNo, 4))
                If ExtLineNo >= 0 Then
                    SyncLineOffs = LineNo - NoopList(2).GetVScrollPos
                    SlaveLineOffs = ExtLineNo - NoopList(0).GetVScrollPos
                    If SyncLineOffs <> SlaveLineOffs Then NoopList(0).OnVScrollTo (ExtLineNo - SyncLineOffs)
                    SlaveLineOffs = ExtLineNo - NoopList(1).GetVScrollPos
                    If SyncLineOffs <> SlaveLineOffs Then NoopList(1).OnVScrollTo (ExtLineNo - SyncLineOffs)
                    NoopList(0).SetCurrentSelection ExtLineNo
                    NoopList(1).SetCurrentSelection ExtLineNo
                    CurPos(2).Caption = CStr(LineNo + 1)
                    CurPos(3).Caption = CStr(LineNo + 1) & " / " & CStr(TotalLines)
                    CurPos(2).Refresh
                    CurPos(3).Refresh
                    If chkWork(13).Value = 1 Then ShowRotationDetails LineNo, ExtLineNo, True, tmpArrLineNo, tmpDepLineNo, IsValidRotation
                    'Me.Refresh
                    NoopList(2).SetFocus
                    CheckKeyboardState GetKeybdState
                    HandleMultiSelect LineNo
                Else
                    NoopList(0).SetCurrentSelection -1
                    NoopList(1).SetCurrentSelection -1
                End If
            End If
        Case Else
    End Select
End Sub
Private Sub BindRotations(FirstCall As Boolean)
    Dim CurLine As Long
    Dim MaxLine As Long
    Dim i As Integer
    Dim RotaLine(2) As Long
    Dim RotaItem(2) As String
    Dim RotaCount(2) As Long
    For i = 0 To 1
        MaxLine = NoopList(i).GetLineCount - 1
        If i = 0 Then
            SetProgressBar 0, 1, MaxLine, "Binding Import Rotations ..."
        Else
            SetProgressBar 0, 1, MaxLine, "Binding Server Rotations ..."
        End If
        For CurLine = 0 To MaxLine
            If CurLine Mod 200 = 0 Then SetProgressBar 0, 2, CurLine, ""
            RotaLine(i) = -1
            RotaItem(i) = GetItem(NoopList(i).GetColumnValue(CurLine, RotaCol), 2, "/")
            If RotaItem(i) <> "" Then RotaLine(i) = Val(RotaItem(i))
            If RotaLine(i) >= 0 Then RotaList(i).SetColumnValue RotaLine(i), 1, Trim(Str(CurLine))
        Next
        RotaCount(i) = RotaList(i).GetLineCount - 1
        SetProgressBar 0, 3, -1, ""
    Next
End Sub

Private Sub CheckRotationDetails()
    Dim CurLine As Long
    Dim MaxLine As Long
    Dim NewLine As Long
    Dim RotaLine(2) As Long
    Dim RotaItem(2) As String
    Dim RotaRegn(2) As String
    Dim RotaCount(2) As Long
    Dim CurCol As Long
    Dim tmpdat As String
    Dim Result As String
    Dim i As Integer
    Dim ChgFlag As Integer
    Dim CheckIt As Boolean
    Dim ArrFlight As Boolean
    Dim ArrLineNo As Long
    Dim DepLineNo As Long
    Dim IsValidRotation As Boolean
    Dim CurForeColor As Long
    Dim CurBackColor As Long
    Dim LineStatus As Long
    Dim LoopCnt As Integer
    
    BindRotations True
    
    For i = 0 To 1
        RotaCount(i) = RotaList(i).GetLineCount - 1
    Next
    
For LoopCnt = 1 To 2
    MaxLine = NoopList(0).GetLineCount - 1
    SetProgressBar 0, 1, MaxLine, "CrossCheck of Rotation Links (Step " & CStr(LoopCnt) & ") ..."
    For CurLine = 0 To MaxLine
        LineStatus = NoopList(0).GetLineStatusValue(CurLine)
        If LineStatus < 1 Then NoopList(0).SetLineStatusValue CurLine, 1
        If CurLine Mod 200 = 0 Then SetProgressBar 0, 2, CurLine, ""
        CheckIt = False
        ArrLineNo = -1
        DepLineNo = -1
        If LoopCnt = 1 Then
            If (NoopList(0).GetColumnValue(CurLine, AdidCol) = "A") Or (NoopList(1).GetColumnValue(CurLine, AdidCol) = "A") Then
                ArrFlight = True
                For i = 0 To 1
                    RotaLine(i) = -1
                    RotaItem(i) = Trim(GetItem(NoopList(i).GetColumnValue(CurLine, RotaCol), 1, "/"))
                    If RotaItem(i) <> "" Then
                        NewLine = Val(RotaItem(i))
                        If NewLine >= 0 Then RotaLine(i) = Val(RotaList(i).GetColumnValue(NewLine, 1))
                    End If
                    RotaRegn(i) = Trim(NoopList(i).GetColumnValue(CurLine, RegnCol))
                Next
                If (MainDialog.DataSystemType <> "OAFOS") And (RotaRegn(0) = "") Then
                    If (RotaLine(0) < 0) And (RotaLine(1) >= 0) Then
                        NewLine = RotaLine(1)
                        RotaItem(0) = GetItem(NoopList(0).GetColumnValue(NewLine, RotaCol), 1, "/")
                        If (RotaItem(0) = "") Or (Val(RotaItem(0)) < 0) Then
                            RotaCount(0) = RotaCount(0) + 1
                            RotaItem(0) = Trim(Str(RotaCount(0) + 1)) + "/" + Trim(Str(RotaCount(0)))
                            NoopList(0).SetColumnValue CurLine, RotaCol, RotaItem(0)
                            RotaList(0).InsertTextLine Trim(Str(RotaCount(0))) & "," & Trim(Str(CurLine)) & ",-1", False
                            NoopList(0).SetColumnValue CurLine, FltnCol, NoopList(1).GetColumnValue(CurLine, FltnCol)
                            NoopList(0).SetColumnValue CurLine, TifdCol, NoopList(1).GetColumnValue(CurLine, TifdCol)
                            NoopList(0).SetColumnValue CurLine, RtypCol, NoopList(1).GetColumnValue(CurLine, RtypCol)
                            NoopList(0).SetDecorationObject CurLine, FltnCol, "SrvRota"
                            NoopList(1).SetDecorationObject CurLine, FltnCol, "SrvRota"
                            If Trim(NoopList(0).GetColumnValue(CurLine, FlnoCol)) = "" Then
                                NoopList(0).SetColumnValue CurLine, AdidCol, NoopList(1).GetColumnValue(CurLine, AdidCol)
                                NoopList(0).SetColumnValue CurLine, TimeCol, NoopList(1).GetColumnValue(CurLine, TimeCol)
                                NoopList(0).SetColumnValue CurLine, DoopCol, NoopList(1).GetColumnValue(CurLine, DoopCol)
                            End If
                            RotaCount(0) = RotaCount(0) + 1
                            RotaItem(0) = Trim(Str(RotaCount(0) - 1)) + "/" + Trim(Str(RotaCount(0)))
                            NoopList(0).SetColumnValue NewLine, RotaCol, RotaItem(0)
                            RotaList(0).InsertTextLine Trim(Str(RotaCount(0))) & "," & Trim(Str(NewLine)) & ",-1", False
                            NoopList(0).SetColumnValue NewLine, FltnCol, NoopList(1).GetColumnValue(NewLine, FltnCol)
                            NoopList(0).SetColumnValue NewLine, TifdCol, NoopList(1).GetColumnValue(NewLine, TifdCol)
                            NoopList(0).SetColumnValue NewLine, RtypCol, NoopList(1).GetColumnValue(NewLine, RtypCol)
                            NoopList(0).SetDecorationObject NewLine, FltnCol, "SrvRota"
                            NoopList(1).SetDecorationObject NewLine, FltnCol, "SrvRota"
                            If Trim(NoopList(0).GetColumnValue(NewLine, FlnoCol)) = "" Then
                                NoopList(0).SetColumnValue NewLine, AdidCol, NoopList(1).GetColumnValue(NewLine, AdidCol)
                                NoopList(0).SetColumnValue NewLine, TimeCol, NoopList(1).GetColumnValue(NewLine, TimeCol)
                                NoopList(0).SetColumnValue NewLine, DoopCol, NoopList(1).GetColumnValue(NewLine, DoopCol)
                            End If
                        End If
                    End If
                End If
                If (MainDialog.DataSystemType <> "OAFOS") And (RotaRegn(0) = "") Then
                    If (RotaLine(1) < 0) And (RotaLine(0) >= 0) Then
                        NewLine = RotaLine(0)
                        RotaItem(1) = GetItem(NoopList(1).GetColumnValue(NewLine, RotaCol), 1, "/")
                        If (RotaItem(1) = "") Or (Val(RotaItem(1)) < 0) Then
                            RotaCount(1) = RotaCount(1) + 1
                            RotaItem(1) = Trim(Str(RotaCount(1) + 1)) + "/" + Trim(Str(RotaCount(1)))
                            NoopList(1).SetColumnValue CurLine, RotaCol, RotaItem(1)
                            RotaList(1).InsertTextLine Trim(Str(RotaCount(1))) & "," & Trim(Str(CurLine)) & ",-1", False
                            NoopList(1).SetColumnValue CurLine, FltnCol, NoopList(0).GetColumnValue(CurLine, FltnCol)
                            NoopList(1).SetColumnValue CurLine, TifdCol, NoopList(0).GetColumnValue(CurLine, TifdCol)
                            NoopList(1).SetColumnValue CurLine, RtypCol, NoopList(0).GetColumnValue(CurLine, RtypCol)
                            NoopList(1).SetDecorationObject CurLine, FltnCol, "CliRota"
                            NoopList(0).SetDecorationObject CurLine, FltnCol, "CliRota"
                            If Trim(NoopList(1).GetColumnValue(CurLine, FlnoCol)) = "" Then
                                NoopList(1).SetColumnValue CurLine, AdidCol, NoopList(0).GetColumnValue(CurLine, AdidCol)
                                NoopList(1).SetColumnValue CurLine, TimeCol, NoopList(0).GetColumnValue(CurLine, TimeCol)
                                NoopList(1).SetColumnValue CurLine, DoopCol, NoopList(0).GetColumnValue(CurLine, DoopCol)
                            End If
                            RotaCount(1) = RotaCount(1) + 1
                            RotaItem(1) = Trim(Str(RotaCount(1) - 1)) + "/" + Trim(Str(RotaCount(1)))
                            NoopList(1).SetColumnValue NewLine, RotaCol, RotaItem(1)
                            RotaList(1).InsertTextLine Trim(Str(RotaCount(1))) & "," & Trim(Str(NewLine)) & ",-1", False
                            NoopList(1).SetColumnValue NewLine, FltnCol, NoopList(0).GetColumnValue(NewLine, FltnCol)
                            NoopList(1).SetColumnValue NewLine, TifdCol, NoopList(0).GetColumnValue(NewLine, TifdCol)
                            NoopList(1).SetColumnValue NewLine, RtypCol, NoopList(0).GetColumnValue(NewLine, RtypCol)
                            NoopList(1).SetDecorationObject NewLine, FltnCol, "CliRota"
                            NoopList(0).SetDecorationObject NewLine, FltnCol, "CliRota"
                            If Trim(NoopList(1).GetColumnValue(NewLine, FlnoCol)) = "" Then
                                NoopList(1).SetColumnValue NewLine, AdidCol, NoopList(0).GetColumnValue(NewLine, AdidCol)
                                NoopList(1).SetColumnValue NewLine, TimeCol, NoopList(0).GetColumnValue(NewLine, TimeCol)
                                NoopList(1).SetColumnValue NewLine, DoopCol, NoopList(0).GetColumnValue(NewLine, DoopCol)
                            End If
                        End If
                    End If
                End If
                CheckIt = True
            End If
        ElseIf LineStatus < 2 Then
            If (NoopList(0).GetColumnValue(CurLine, AdidCol) = "D") Or (NoopList(1).GetColumnValue(CurLine, AdidCol) = "D") Then
                CheckIt = True
    '            ArrFlight = False
    '            For i = 0 To 1
    '                RotaLine(i) = -1
    '                RotaItem(i) = Trim(GetItem(NoopList(i).GetColumnValue(CurLine, RotaCol), 1, "/"))
    '                If RotaItem(i) <> "" Then
    '                    NewLine = Val(RotaItem(i))
    '                    If NewLine >= 0 Then RotaLine(i) = Val(RotaList(i).GetColumnValue(NewLine, 1))
    '                End If
    '            Next
    '            'If (RotaLine(0) < 0) Or (RotaLine(1) < 0) Then
    '                'MsgBox "Check Curline " & CurLine
    '                ArrFlight = False
    '                CheckIt = True
    '            'End If
            End If
        End If
        If CheckIt Then
            Result = ShowRotationDetails(CurLine, CurLine, False, ArrLineNo, DepLineNo, IsValidRotation)
            If ArrLineNo >= 0 Then
                NoopList(0).SetLineStatusValue ArrLineNo, 2
                tmpdat = "START"
                i = 0
                While tmpdat <> ""
                    i = i + 1
                    tmpdat = GetItem(Result, i, ",")
                    If tmpdat <> "" Then
                        CurCol = Val(tmpdat)
                        i = i + 1
                        ChgFlag = Val(GetItem(Result, i, ","))
                        SetRotaDiff 0, ArrLineNo, CurCol, ChgFlag, False, False
                        i = i + 1
                        'ChgFlag = Val(GetItem(Result, i, ","))
                        'SetRotaDiff 0, RotaLine(0), CurCol, ChgFlag, False, False
                        i = i + 1
                        ChgFlag = Val(GetItem(Result, i, ","))
                        SetRotaDiff 1, ArrLineNo, CurCol, ChgFlag, False, False
                        i = i + 1
                        'ChgFlag = Val(GetItem(Result, i, ","))
                        'SetRotaDiff 1, RotaLine(1), CurCol, ChgFlag, False, False
                    End If
                Wend
            End If
            If DepLineNo >= 0 Then
                NoopList(0).SetLineStatusValue DepLineNo, 2
                tmpdat = "START"
                i = 0
                While tmpdat <> ""
                    i = i + 1
                    tmpdat = GetItem(Result, i, ",")
                    If tmpdat <> "" Then
                        CurCol = Val(tmpdat)
                        i = i + 1
                        'ChgFlag = Val(GetItem(Result, i, ","))
                        'SetRotaDiff 0, RotaLine(0), CurCol, ChgFlag, False, False
                        i = i + 1
                        ChgFlag = Val(GetItem(Result, i, ","))
                        SetRotaDiff 0, DepLineNo, CurCol, ChgFlag, False, False
                        i = i + 1
                        'ChgFlag = Val(GetItem(Result, i, ","))
                        'SetRotaDiff 1, RotaLine(1), CurCol, ChgFlag, False, False
                        i = i + 1
                        ChgFlag = Val(GetItem(Result, i, ","))
                        SetRotaDiff 1, DepLineNo, CurCol, ChgFlag, False, False
                    End If
                Wend
            End If
            If Not IsValidRotation Then
                NoopList(0).GetLineColor CurLine, CurForeColor, CurBackColor
                If CurBackColor = CliFkeyCnt(4).BackColor Then
                    NoopList(0).SetLineColor CurLine, CliFkeyCnt(1).ForeColor, CliFkeyCnt(1).BackColor
                End If
            End If
        End If
    Next
    SetProgressBar 0, 3, -1, ""
Next
End Sub

Private Function ShowRotationDetails(MasterLineNo As Long, FlightLineNo As Long, ShowDetails As Boolean, ArrLineNo As Long, DepLineNo As Long, IsValidRotation As Boolean) As String
    Dim Result As String
    Dim RotaFlightLineNo As Long
    Dim LineForeColor As Long
    Dim LineBackColor As Long
    Dim tmpRotaRec1 As String
    Dim tmpRotaRec2 As String
    Dim ArrData(2) As String
    Dim DepData(2) As String
    Dim SelFlight As String
    Dim tmpdat As String
    Dim i As Integer
    For i = 0 To 1
        If ShowDetails Then RotaLink(i).ResetContent
        ArrData(i) = ""
        DepData(i) = ""
        tmpRotaRec1 = ""
        tmpRotaRec2 = ""
        ArrFlightLineNo(i) = -1
        DepFlightLineNo(i) = -1
        tmpRotaRec1 = NoopList(i).GetLineValues(FlightLineNo)
        tmpdat = NoopList(i).GetColumnValue(FlightLineNo, RotaCol)
        If tmpdat <> "" Then
            RotaFlightLineNo = Val(tmpdat)
            If RotaFlightLineNo >= 0 Then
                RotaFlightLineNo = Val(RotaList(i).GetColumnValue(RotaFlightLineNo, 1))
                If RotaFlightLineNo >= 0 Then tmpRotaRec2 = NoopList(i).GetLineValues(RotaFlightLineNo)
            End If
        End If
        tmpdat = GetRealItem(tmpRotaRec1, AdidCol, ",")
        Select Case tmpdat
            Case "A"
                ArrData(i) = tmpRotaRec1
                DepData(i) = tmpRotaRec2
                ArrFlightLineNo(i) = FlightLineNo
                DepFlightLineNo(i) = RotaFlightLineNo
            Case "D"
                ArrData(i) = tmpRotaRec2
                DepData(i) = tmpRotaRec1
                ArrFlightLineNo(i) = RotaFlightLineNo
                DepFlightLineNo(i) = FlightLineNo
            Case Else
        End Select
    Next
    If ShowDetails Then
        For i = 0 To 1
            RotaLink(i).InsertTextLine ArrData(i), False
            RotaLink(i).InsertTextLine DepData(i), False
            If ArrFlightLineNo(i) >= 0 Then
                NoopList(i).GetLineColor ArrFlightLineNo(i), LineForeColor, LineBackColor
                RotaLink(i).SetLineColor 0, LineForeColor, LineBackColor
            End If
            If DepFlightLineNo(i) >= 0 Then
                NoopList(i).GetLineColor DepFlightLineNo(i), LineForeColor, LineBackColor
                RotaLink(i).SetLineColor 1, LineForeColor, LineBackColor
            End If
        Next
        RotaLink(2).ResetContent
        SelFlight = NoopList(2).GetLineValues(MasterLineNo)
        tmpdat = GetRealItem(SelFlight, 2, ",")
        NoopList(2).GetLineColor MasterLineNo, LineForeColor, LineBackColor
        Select Case tmpdat
            Case "A"
                RotaLink(2).InsertTextLine SelFlight, False
                RotaLink(2).InsertTextLine ",,,,", False
                RotaLink(2).SetLineColor 0, LineForeColor, LineBackColor
            Case "D"
                RotaLink(2).InsertTextLine ",,,,", False
                RotaLink(2).InsertTextLine SelFlight, False
                RotaLink(2).SetLineColor 1, LineForeColor, LineBackColor
            Case Else
        End Select
    End If
    Result = CrossCheckRotation(ArrData(0), DepData(0), ArrData(1), DepData(1), ShowDetails, IsValidRotation)
    ArrLineNo = ArrFlightLineNo(0)
    DepLineNo = DepFlightLineNo(0)
    If ShowDetails Then
        RotaLink(0).RedrawTab
        RotaLink(1).RedrawTab
        RotaLink(2).RedrawTab
    End If
    ShowRotationDetails = Result
End Function

Private Function CrossCheckRotation(ImpArr As String, ImpDep As String, SrvArr As String, SrvDep As String, ShowDetails As Boolean, IsValidRotation As Boolean) As String
    Dim Result As String
    Dim ImpArrFlno As String
    Dim ImpDepFlno As String
    Dim SrvArrFlno As String
    Dim SrvDepFlno As String
    Dim ImpArrFkey As String
    Dim ImpDepFkey As String
    Dim SrvArrFkey As String
    Dim SrvDepFkey As String
    Dim ImpArrData As String
    Dim SrvArrData As String
    Dim ImpDepData As String
    Dim SrvDepData As String
    Dim ImpArrFtyp As String
    Dim ImpDepFtyp As String
    Dim ImpArrDataChg As Integer
    Dim SrvArrDataChg As Integer
    Dim ImpDepDataChg As Integer
    Dim SrvDepDataChg As Integer
    Dim ImpSplit As Boolean
    Dim SrvSplit As Boolean
    Dim i As Integer
    Dim tmpArr
    Dim tmpDep
    Dim tmpDiff As Long
    
    IsValidRotation = True
    ImpSplit = False
    SrvSplit = False
    Result = ""
    ImpArrFtyp = GetRealItem(ImpArr, FtypCol, ",")
    ImpDepFtyp = GetRealItem(ImpDep, FtypCol, ",")
    
    ImpArrFlno = GetRealItem(ImpArr, FlnoCol, ",")
    ImpDepFlno = GetRealItem(ImpDep, FlnoCol, ",")
    SrvArrFlno = GetRealItem(SrvArr, FlnoCol, ",")
    SrvDepFlno = GetRealItem(SrvDep, FlnoCol, ",")
    
    ImpArrFkey = GetRealItem(ImpArr, FkeyCol, ",")
    ImpDepFkey = GetRealItem(ImpDep, FkeyCol, ",")
    SrvArrFkey = GetRealItem(SrvArr, FkeyCol, ",")
    SrvDepFkey = GetRealItem(SrvDep, FkeyCol, ",")
    
    ImpArrData = GetRealItem(ImpArr, TimeCol, ",")
    SrvArrData = GetRealItem(SrvArr, TimeCol, ",")
    ImpDepData = GetRealItem(ImpDep, TimeCol, ",")
    SrvDepData = GetRealItem(SrvDep, TimeCol, ",")
    ImpArrDataChg = 0
    SrvArrDataChg = 0
    ImpDepDataChg = 0
    SrvDepDataChg = 0
    If ImpArrFkey = SrvArrFkey Then
        If (ImpArrData <> SrvArrData) Then
            ImpArrDataChg = 1
            SrvArrDataChg = 1
        End If
    End If
    If ImpDepFkey = SrvDepFkey Then
        If (ImpDepData <> SrvDepData) Then
            ImpDepDataChg = 1
            SrvDepDataChg = 1
        End If
    End If
    ShowMinGrdIndicator False
    If ImpArrData = "" Then ImpArrData = SrvArrData
    If ImpDepData = "" Then ImpDepData = SrvDepData
    If (ImpArrData <> "") And (ImpDepData <> "") Then
        tmpArr = CedaFullDateToVb(ImpArrData)
        tmpDep = CedaFullDateToVb(ImpDepData)
        tmpDiff = CLng(DateDiff("n", tmpArr, tmpDep))
        If tmpDiff < MinGrdTime Then
            ImpArrDataChg = ImpArrDataChg + 2
            ImpDepDataChg = ImpDepDataChg + 2
            ImpSplit = True
            SrvSplit = True
            IsValidRotation = False
            ShowMinGrdIndicator True
        End If
    End If
    If (SrvArrData <> "") And (SrvDepData <> "") Then
        tmpArr = CedaFullDateToVb(SrvArrData)
        tmpDep = CedaFullDateToVb(SrvDepData)
        tmpDiff = CLng(DateDiff("n", tmpArr, tmpDep))
        If tmpDiff < MinGrdTime Then
            SrvArrDataChg = SrvArrDataChg + 2
            SrvDepDataChg = SrvDepDataChg + 2
            SrvSplit = True
            IsValidRotation = False
            ShowMinGrdIndicator True
        End If
    End If
    If ShowDetails Then
        SetRotaDiff 0, 0, TimeCol, ImpArrDataChg, ShowDetails, ImpSplit
        SetRotaDiff 0, 1, TimeCol, ImpDepDataChg, ShowDetails, ImpSplit
        SetRotaDiff 1, 0, TimeCol, SrvArrDataChg, ShowDetails, SrvSplit
        SetRotaDiff 1, 1, TimeCol, SrvDepDataChg, ShowDetails, SrvSplit
    Else
        Result = Result + Str(TimeCol) + ","
        Result = Result + Str(ImpArrDataChg) + ","
        Result = Result + Str(ImpDepDataChg) + ","
        Result = Result + Str(SrvArrDataChg) + ","
        Result = Result + Str(SrvDepDataChg) + ","
    End If
    If MainDialog.DataSystemType = "OAFOS" Then
        ImpArrData = GetRealItem(ImpArr, RegnCol, ",")
        SrvArrData = GetRealItem(SrvArr, RegnCol, ",")
        ImpDepData = GetRealItem(ImpDep, RegnCol, ",")
        SrvDepData = GetRealItem(SrvDep, RegnCol, ",")
        ImpArrDataChg = 0
        SrvArrDataChg = 0
        ImpDepDataChg = 0
        SrvDepDataChg = 0
        If ImpArrFkey = SrvArrFkey Then
            If (ImpArrFtyp <> "X") And (ImpArrFtyp <> "N") And (ImpArrData <> SrvArrData) Then
                ImpArrDataChg = 1
                SrvArrDataChg = 1
            End If
        End If
        If ImpDepFkey = SrvDepFkey Then
            If (ImpDepFtyp <> "X") And (ImpDepFtyp <> "N") And (ImpDepData <> SrvDepData) Then
                ImpDepDataChg = 1
                SrvDepDataChg = 1
            End If
        End If
        If (ImpArrFkey <> "") And (ImpDepFkey <> "") And (ImpArrData <> ImpDepData) Then
            ImpArrDataChg = ImpArrDataChg + 2
            ImpDepDataChg = ImpDepDataChg + 2
        End If
        If (SrvArrFkey <> "") And (SrvDepFkey <> "") And (SrvArrData <> SrvDepData) Then
            SrvArrDataChg = SrvArrDataChg + 2
            SrvDepDataChg = SrvDepDataChg + 2
        End If
        If ShowDetails Then
            SetRotaDiff 0, 0, RegnCol, ImpArrDataChg, ShowDetails, ImpSplit
            SetRotaDiff 0, 1, RegnCol, ImpDepDataChg, ShowDetails, ImpSplit
            SetRotaDiff 1, 0, RegnCol, SrvArrDataChg, ShowDetails, SrvSplit
            SetRotaDiff 1, 1, RegnCol, SrvDepDataChg, ShowDetails, SrvSplit
        Else
            Result = Result + Str(RegnCol) + ","
            Result = Result + Str(ImpArrDataChg) + ","
            Result = Result + Str(ImpDepDataChg) + ","
            Result = Result + Str(SrvArrDataChg) + ","
            Result = Result + Str(SrvDepDataChg) + ","
        End If
    End If
    
    ImpArrData = GetRealItem(ImpArr, Act3Col, ",")
    SrvArrData = GetRealItem(SrvArr, Act3Col, ",")
    ImpDepData = GetRealItem(ImpDep, Act3Col, ",")
    SrvDepData = GetRealItem(SrvDep, Act3Col, ",")
    ImpArrDataChg = 0
    SrvArrDataChg = 0
    ImpDepDataChg = 0
    SrvDepDataChg = 0
    If ImpArrFkey = SrvArrFkey Then
        If (ImpArrData <> SrvArrData) Then
            ImpArrDataChg = 1
            SrvArrDataChg = 1
        End If
    End If
    If ImpDepFkey = SrvDepFkey Then
        If (ImpDepData <> SrvDepData) Then
            ImpDepDataChg = 1
            SrvDepDataChg = 1
        End If
    End If
    If (ImpArrData <> "") And (ImpDepData <> "") And (ImpArrData <> ImpDepData) Then
        ImpArrDataChg = ImpArrDataChg + 2
        ImpDepDataChg = ImpDepDataChg + 2
        ImpSplit = True
        SrvSplit = True
        IsValidRotation = False
    End If
    If (ImpArrData = "") And (SrvArrData <> "") And (SrvDepData <> "") And (ImpDepData <> "") And (ImpDepData <> SrvDepData) Then
        ImpArrDataChg = ImpArrDataChg + 2
        ImpDepDataChg = ImpDepDataChg + 2
        ImpSplit = True
        SrvSplit = True
        IsValidRotation = False
    End If
    If (SrvArrData <> "") And (SrvDepData <> "") And (SrvArrData <> SrvDepData) Then
        SrvArrDataChg = SrvArrDataChg + 2
        SrvDepDataChg = SrvDepDataChg + 2
        SrvSplit = True
        IsValidRotation = False
    End If
    If (ImpDepData = "") And (SrvDepData <> "") And (SrvArrData <> "") And (ImpArrData <> "") And (ImpArrData <> SrvArrData) Then
        ImpArrDataChg = ImpArrDataChg + 2
        ImpDepDataChg = ImpDepDataChg + 2
        ImpSplit = True
        SrvSplit = True
        IsValidRotation = False
    End If
    If (ImpArrData <> "") And (SrvDepData <> "") And (ImpArrData <> SrvDepData) Then
        IsValidRotation = False
        If (ImpArrFkey <> SrvArrFkey) Or (ImpDepFkey <> SrvDepFkey) Then
            SrvSplit = True
        End If
        If (ImpArrData <> ImpDepData) Then
            ImpSplit = True
        End If
    End If
    If ShowDetails Then
        SetRotaDiff 0, 0, Act3Col, ImpArrDataChg, ShowDetails, ImpSplit
        SetRotaDiff 0, 1, Act3Col, ImpDepDataChg, ShowDetails, ImpSplit
        SetRotaDiff 1, 0, Act3Col, SrvArrDataChg, ShowDetails, SrvSplit
        SetRotaDiff 1, 1, Act3Col, SrvDepDataChg, ShowDetails, SrvSplit
    Else
        Result = Result + Str(Act3Col) + ","
        Result = Result + Str(ImpArrDataChg) + ","
        Result = Result + Str(ImpDepDataChg) + ","
        Result = Result + Str(SrvArrDataChg) + ","
        Result = Result + Str(SrvDepDataChg) + ","
    End If

    ImpArrData = GetRealItem(ImpArr, Apc3Col, ",")
    SrvArrData = GetRealItem(SrvArr, Apc3Col, ",")
    ImpDepData = GetRealItem(ImpDep, Apc3Col, ",")
    SrvDepData = GetRealItem(SrvDep, Apc3Col, ",")
    ImpArrDataChg = 0
    SrvArrDataChg = 0
    ImpDepDataChg = 0
    SrvDepDataChg = 0
    If ImpArrFkey = SrvArrFkey Then
        If (ImpArrData <> SrvArrData) Then
            ImpArrDataChg = 1
            SrvArrDataChg = 1
        End If
    End If
    If ImpDepFkey = SrvDepFkey Then
        If (ImpDepData <> SrvDepData) Then
            ImpDepDataChg = 1
            SrvDepDataChg = 1
        End If
    End If
    If ShowDetails Then
        SetRotaDiff 0, 0, Apc3Col, ImpArrDataChg, ShowDetails, ImpSplit
        SetRotaDiff 0, 1, Apc3Col, ImpDepDataChg, ShowDetails, ImpSplit
        SetRotaDiff 1, 0, Apc3Col, SrvArrDataChg, ShowDetails, SrvSplit
        SetRotaDiff 1, 1, Apc3Col, SrvDepDataChg, ShowDetails, SrvSplit
    Else
        Result = Result + Str(Apc3Col) + ","
        Result = Result + Str(ImpArrDataChg) + ","
        Result = Result + Str(ImpDepDataChg) + ","
        Result = Result + Str(SrvArrDataChg) + ","
        Result = Result + Str(SrvDepDataChg) + ","
    End If

    ImpArrData = GetRealItem(ImpArr, Via3Col, ",")
    SrvArrData = GetRealItem(SrvArr, Via3Col, ",")
    ImpDepData = GetRealItem(ImpDep, Via3Col, ",")
    SrvDepData = GetRealItem(SrvDep, Via3Col, ",")
    ImpArrDataChg = 0
    SrvArrDataChg = 0
    ImpDepDataChg = 0
    SrvDepDataChg = 0
    If ImpArrFkey = SrvArrFkey Then
        If (ImpArrData <> SrvArrData) Then
            ImpArrDataChg = 1
            SrvArrDataChg = 1
        End If
    End If
    If ImpDepFkey = SrvDepFkey Then
        If (ImpDepData <> SrvDepData) Then
            ImpDepDataChg = 1
            SrvDepDataChg = 1
        End If
    End If
    If ShowDetails Then
        SetRotaDiff 0, 0, Via3Col, ImpArrDataChg, ShowDetails, ImpSplit
        SetRotaDiff 0, 1, Via3Col, ImpDepDataChg, ShowDetails, ImpSplit
        SetRotaDiff 1, 0, Via3Col, SrvArrDataChg, ShowDetails, SrvSplit
        SetRotaDiff 1, 1, Via3Col, SrvDepDataChg, ShowDetails, SrvSplit
    Else
        Result = Result + Str(Via3Col) + ","
        Result = Result + Str(ImpArrDataChg) + ","
        Result = Result + Str(ImpDepDataChg) + ","
        Result = Result + Str(SrvArrDataChg) + ","
        Result = Result + Str(SrvDepDataChg) + ","
    End If

    ImpArrData = GetRealItem(ImpArr, StypCol, ",")
    SrvArrData = GetRealItem(SrvArr, StypCol, ",")
    ImpDepData = GetRealItem(ImpDep, StypCol, ",")
    SrvDepData = GetRealItem(SrvDep, StypCol, ",")
    ImpArrDataChg = -1
    SrvArrDataChg = -1
    ImpDepDataChg = -1
    SrvDepDataChg = -1
    If ImpArrFkey = SrvArrFkey Then
        ImpArrDataChg = 0
        SrvArrDataChg = 0
        If (ImpArrData <> SrvArrData) Then
            ImpArrDataChg = 1
            SrvArrDataChg = 1
        End If
    End If
    If ImpDepFkey = SrvDepFkey Then
        ImpDepDataChg = 0
        SrvDepDataChg = 0
        If (ImpDepData <> SrvDepData) Then
            ImpDepDataChg = 1
            SrvDepDataChg = 1
        End If
    End If
    If ShowDetails Then
        SetRotaDiff 0, 0, StypCol, ImpArrDataChg, ShowDetails, ImpSplit
        SetRotaDiff 0, 1, StypCol, ImpDepDataChg, ShowDetails, ImpSplit
        SetRotaDiff 1, 0, StypCol, SrvArrDataChg, ShowDetails, SrvSplit
        SetRotaDiff 1, 1, StypCol, SrvDepDataChg, ShowDetails, SrvSplit
    Else
        Result = Result + Str(StypCol) + ","
        Result = Result + Str(ImpArrDataChg) + ","
        Result = Result + Str(ImpDepDataChg) + ","
        Result = Result + Str(SrvArrDataChg) + ","
        Result = Result + Str(SrvDepDataChg) + ","
    End If
    
    ImpArrData = ImpArrFkey
    SrvArrData = SrvArrFkey
    ImpDepData = ImpDepFkey
    SrvDepData = SrvDepFkey
    ImpArrDataChg = 0
    SrvArrDataChg = 0
    ImpDepDataChg = 0
    SrvDepDataChg = 0
    If (ImpArrData <> "") And (SrvArrData <> "") And (ImpArrData <> SrvArrData) Then
        ImpArrDataChg = 2
        SrvArrDataChg = 2
        ImpDepDataChg = 2
        SrvDepDataChg = 2
        SrvSplit = True
        IsValidRotation = False
    End If
    If (ImpDepData <> "") And (SrvDepData <> "") And (ImpDepData <> SrvDepData) Then
        ImpArrDataChg = 2
        SrvArrDataChg = 2
        ImpDepDataChg = 2
        SrvDepDataChg = 2
        SrvSplit = True
        IsValidRotation = False
    End If
    If ShowDetails Then
        SetRotaDiff 0, 0, FlnoCol, ImpArrDataChg, ShowDetails, ImpSplit
        SetRotaDiff 0, 1, FlnoCol, ImpDepDataChg, ShowDetails, ImpSplit
        SetRotaDiff 1, 0, FlnoCol, SrvArrDataChg, ShowDetails, SrvSplit
        SetRotaDiff 1, 1, FlnoCol, SrvDepDataChg, ShowDetails, SrvSplit
    Else
        Result = Result + Str(FlnoCol) + ","
        Result = Result + Str(ImpArrDataChg) + ","
        Result = Result + Str(ImpDepDataChg) + ","
        Result = Result + Str(SrvArrDataChg) + ","
        Result = Result + Str(SrvDepDataChg) + ","
    End If
    
    ImpArrData = Trim(GetRealItem(ImpArr, FltnCol, ","))
    SrvArrData = Trim(GetRealItem(SrvArr, FltnCol, ","))
    ImpDepData = Trim(GetRealItem(ImpDep, FltnCol, ","))
    SrvDepData = Trim(GetRealItem(SrvDep, FltnCol, ","))
    ImpArrDataChg = 0
    SrvArrDataChg = 0
    ImpDepDataChg = 0
    SrvDepDataChg = 0
    If (ImpArrData <> "") And (ImpArrData <> "-") And (SrvArrData <> "") And (ImpArrData <> SrvArrData) Then
        ImpArrDataChg = 2
        SrvArrDataChg = 2
        ImpDepDataChg = 2
        SrvDepDataChg = 2
        SrvSplit = True
        IsValidRotation = False
    End If
    If (ImpDepData <> "") And (ImpDepData <> "-") And (SrvDepData <> "") And (ImpDepData <> SrvDepData) Then
        ImpArrDataChg = 2
        SrvArrDataChg = 2
        ImpDepDataChg = 2
        SrvDepDataChg = 2
        SrvSplit = True
        IsValidRotation = False
    End If
    If ShowDetails Then
        SetRotaDiff 0, 0, FltnCol, ImpArrDataChg, ShowDetails, ImpSplit
        SetRotaDiff 0, 1, FltnCol, ImpDepDataChg, ShowDetails, ImpSplit
        SetRotaDiff 1, 0, FltnCol, SrvArrDataChg, ShowDetails, SrvSplit
        SetRotaDiff 1, 1, FltnCol, SrvDepDataChg, ShowDetails, SrvSplit
    Else
        Result = Result + Str(FltnCol) + ","
        Result = Result + Str(ImpArrDataChg) + ","
        Result = Result + Str(ImpDepDataChg) + ","
        Result = Result + Str(SrvArrDataChg) + ","
        Result = Result + Str(SrvDepDataChg) + ","
    End If
    
    If IsValidRotation Then
        ImpArrData = Trim(GetRealItem(ImpArr, RkeyCol, ","))
        SrvArrData = Trim(GetRealItem(SrvArr, RkeyCol, ","))
        ImpDepData = Trim(GetRealItem(ImpDep, RkeyCol, ","))
        SrvDepData = Trim(GetRealItem(SrvDep, RkeyCol, ","))
        ImpArrDataChg = 0
        SrvArrDataChg = 0
        ImpDepDataChg = 0
        SrvDepDataChg = 0
        If (ImpArrData <> "") And (ImpDepData <> "") Then
            If (SrvArrData <> SrvDepData) Then
                SrvSplit = True
                IsValidRotation = False
            End If
            If (SrvArrData = "") And (SrvDepData = "") Then
                SrvSplit = True
                IsValidRotation = False
            End If
            If Not IsValidRotation Then
                ImpArrDataChg = 2
                SrvArrDataChg = 2
                ImpDepDataChg = 2
                SrvDepDataChg = 2
                If ShowDetails Then
                    SetRotaDiff 0, 0, RkeyCol, ImpArrDataChg, ShowDetails, ImpSplit
                    SetRotaDiff 0, 1, RkeyCol, ImpDepDataChg, ShowDetails, ImpSplit
                    SetRotaDiff 1, 0, RkeyCol, SrvArrDataChg, ShowDetails, SrvSplit
                    SetRotaDiff 1, 1, RkeyCol, SrvDepDataChg, ShowDetails, SrvSplit
                Else
                    Result = Result + Str(RkeyCol) + ","
                    Result = Result + Str(ImpArrDataChg) + ","
                    Result = Result + Str(ImpDepDataChg) + ","
                    Result = Result + Str(SrvArrDataChg) + ","
                    Result = Result + Str(SrvDepDataChg) + ","
                End If
            End If
        End If
    End If
    If Not IsValidRotation Then
        If ShowDetails Then
            SetRotaDiff 0, 0, 0, 4, ShowDetails, ImpSplit
            SetRotaDiff 0, 1, 0, 4, ShowDetails, ImpSplit
            SetRotaDiff 1, 0, 0, 4, ShowDetails, SrvSplit
            SetRotaDiff 1, 1, 0, 4, ShowDetails, SrvSplit
        Else
            Result = Result + Str(0) + ","
            Result = Result + Str(ImpArrDataChg) + ","
            Result = Result + Str(ImpDepDataChg) + ","
            Result = Result + Str(SrvArrDataChg) + ","
            Result = Result + Str(SrvDepDataChg) + ","
            Result = Result + "0,"
            If ImpSplit Then
                Result = Result + "4,"
                Result = Result + "5,"
            Else
                Result = Result + "2,"
                Result = Result + "2,"
            End If
            If SrvSplit Then
                Result = Result + "4,"
                Result = Result + "5,"
            Else
                Result = Result + "2,"
                Result = Result + "2,"
            End If
        End If
    End If
    
    CrossCheckRotation = Result
End Function
Private Sub ShowMinGrdIndicator(IsVisible As Boolean)
    If fraMinGrd.Visible Then
        SignMinGrd(0).Visible = IsVisible
        SignMinGrd(1).Visible = IsVisible
    End If
End Sub
Private Sub SetRotaDiff(Index As Integer, LineNo As Long, ColNo As Long, DiffStatus As Integer, ShowDetails As Boolean, ShowSplit As Boolean)
    If LineNo >= 0 Then
        If ShowDetails Then
            If RotaLink(Index).GetColumnValue(LineNo, FlnoCol) <> "" Then
                Select Case DiffStatus
                    Case 1  'Update
                        RotaLink(Index).SetDecorationObject LineNo, ColNo, "CellDiff"
                    Case 2  'Invalid Rotation
                        RotaLink(Index).SetDecorationObject LineNo, ColNo, "RotaDeny"
                    Case 3  'Both
                        RotaLink(Index).SetDecorationObject LineNo, ColNo, "RotaDiff"
                    Case 4  'Split Or Join
                        If ShowSplit Then
                            If LineNo = 0 Then
                                RotaLink(Index).SetDecorationObject LineNo, ColNo, "SplitArr"
                            Else
                                RotaLink(Index).SetDecorationObject LineNo, ColNo, "SplitDep"
                            End If
                        Else
                            RotaLink(Index).SetDecorationObject LineNo, ColNo, "RotaDeny"
                        End If
                    Case Else
                End Select
            End If
        Else
            If NoopList(Index).GetColumnValue(LineNo, FlnoCol) <> "" Then
                Select Case DiffStatus
                    Case 1  'Update
                        NoopList(Index).SetDecorationObject LineNo, ColNo, "CellDiff"
                    Case 2  'Invalid Rotation
                        NoopList(Index).SetDecorationObject LineNo, ColNo, "RotaDeny"
                        If NoopList(Index).GetLineTag(LineNo) = "" Then
                            If Index = 0 Then
                                NoopList(Index).SetLineTag LineNo, "L"
                            Else
                                NoopList(Index).SetLineTag LineNo, "R"
                            End If
                        End If
                    Case 3  'Both
                        NoopList(Index).SetDecorationObject LineNo, ColNo, "RotaDiff"
                        If NoopList(Index).GetLineTag(LineNo) = "" Then
                            If Index = 0 Then
                                NoopList(Index).SetLineTag LineNo, "L"
                            Else
                                NoopList(Index).SetLineTag LineNo, "R"
                            End If
                        End If
                    Case 4  'Split Arrival
                        NoopList(Index).SetDecorationObject LineNo, ColNo, "SplitArr"
                        If NoopList(Index).GetLineTag(LineNo) = "" Then
                            If Index = 0 Then
                                NoopList(Index).SetLineTag LineNo, "L"
                            Else
                                NoopList(Index).SetLineTag LineNo, "R"
                            End If
                        End If
                    Case 5  'Split Departure
                        NoopList(Index).SetDecorationObject LineNo, ColNo, "SplitDep"
                        If NoopList(Index).GetLineTag(LineNo) = "" Then
                            If Index = 0 Then
                                NoopList(Index).SetLineTag LineNo, "L"
                            Else
                                NoopList(Index).SetLineTag LineNo, "R"
                            End If
                        End If
                    Case Else
                End Select
            End If
        End If
    End If
End Sub

Private Sub HandleMultiSelect(LineNo As Long)
    Static FirstLine As Long
    Static LastLine As Long
    Static FirstTextColor As Long
    Static FirstBackColor As Long
    Static MultiSelection As Boolean
    Static SelCount As Long
    Dim FromLine As Long
    Dim ToLine As Long
    Dim CurLine As Long
    Dim DmmColor As Long
    Dim CurBackColor As Long
    Dim SelList As String
    Dim LinNbr As String
    Dim ItmNbr As Long
    If MainDialog.DataSystemType = "OAFOS" Then
        SelectRegnLines LineNo
    Else
        If (chkWork(6).Value = 1) And (Not StopNestedCalls) And (LineNo >= 0) Then
            FromLine = -1
            ToLine = -1
            If (KeybdFlags = 0) And (chkWork(12).Tag = "INIT") Then
                KeybdFlags = 2
                chkWork(12).Tag = ""
            End If
            Select Case KeybdFlags
                Case 1  'Shift
                    If LineNo <= FirstLine Then
                        FromLine = LineNo
                        ToLine = FirstLine
                        FirstLine = LineNo
                    End If
                    If LineNo >= LastLine Then
                        FromLine = LastLine
                        ToLine = LineNo
                        LastLine = LineNo
                    End If
                Case 2  'Ctrl
                    NoopList(2).GetLineColor LineNo, DmmColor, CurBackColor
                    If MultiSelection Then
                        If CurBackColor = FirstBackColor Then
                            If NoopList(2).GetLineStatusValue(LineNo) <> 1 Then
                                NoopList(2).SetCellProperty LineNo, 0, "MultiSel"
                                NoopList(2).SetCellProperty LineNo, 1, "MultiSel"
                                NoopList(2).SetCellProperty LineNo, 2, "MultiSel"
                                NoopList(2).SetLineStatusValue LineNo, 1
                                SelCount = SelCount + 1
                            Else
                                NoopList(2).ResetCellProperties LineNo
                                NoopList(2).SetLineStatusValue LineNo, -1
                                SelCount = SelCount - 1
                            End If
                            If LineNo < FirstLine Then FirstLine = LineNo
                            If LineNo > LastLine Then LastLine = LineNo
                        End If
                    Else
                        If (CurBackColor <> CliFkeyCnt(4).BackColor) And (CurBackColor <> CliFkeyCnt(3).BackColor) Then
                            NoopList(2).SetCellProperty LineNo, 0, "MultiSel"
                            NoopList(2).SetCellProperty LineNo, 1, "MultiSel"
                            NoopList(2).SetCellProperty LineNo, 2, "MultiSel"
                            NoopList(2).SetLineStatusValue LineNo, 1
                            FirstLine = LineNo
                            LastLine = LineNo
                            FirstBackColor = CurBackColor
                            FirstTextColor = DmmColor
                            MultiSelection = True
                            SelCount = 1
                        End If
                    End If
                    NoopList(2).RedrawTab
                Case Else
                If chkWork(12).Value = 0 Then
                    If MultiSelection Then
                        SelList = NoopList(2).GetLinesByStatusValue(1, 0)
                        If SelList <> "" Then
                            Screen.MousePointer = 11
                            LinNbr = "START"
                            ItmNbr = -1
                            While LinNbr <> ""
                                ItmNbr = ItmNbr + 1
                                LinNbr = GetRealItem(SelList, ItmNbr, ",")
                                If LinNbr <> "" Then
                                    CurLine = Val(LinNbr)
                                    NoopList(2).ResetCellProperties CurLine
                                    NoopList(2).SetLineStatusValue CurLine, -1
                                End If
                            Wend
                            Screen.MousePointer = 0
                            NoopList(2).RedrawTab
                        End If
                    End If
                    NoopList(2).GetLineColor LineNo, FirstTextColor, FirstBackColor
                    FirstLine = LineNo
                    LastLine = LineNo
                    SelCount = 0
                    MultiSelection = False
                Else
                    If Not MultiSelection Then
                        NoopList(2).GetLineColor LineNo, FirstTextColor, FirstBackColor
                        FirstLine = LineNo
                        LastLine = LineNo
                        SelCount = 0
                    End If
                End If
            End Select
            If SelCount = 0 Then
                NoopList(2).GetLineColor LineNo, FirstTextColor, FirstBackColor
                FirstLine = LineNo
                LastLine = LineNo
                FromLine = LineNo
                ToLine = LineNo
            End If
            If (FirstBackColor = CliFkeyCnt(4).BackColor) Or (FirstBackColor = CliFkeyCnt(3).BackColor) Then FromLine = -1
            If FromLine >= 0 Then
                For CurLine = FromLine To ToLine
                    If NoopList(2).GetLineStatusValue(CurLine) <> 1 Then
                        NoopList(2).GetLineColor CurLine, DmmColor, CurBackColor
                        If CurBackColor = FirstBackColor Then
                            NoopList(2).SetCellProperty CurLine, 0, "MultiSel"
                            NoopList(2).SetCellProperty CurLine, 1, "MultiSel"
                            NoopList(2).SetCellProperty CurLine, 2, "MultiSel"
                            NoopList(2).SetLineStatusValue CurLine, 1
                            SelCount = SelCount + 1
                        End If
                    End If
                Next
                NoopList(2).RedrawTab
                MultiSelection = True
            End If
            If SelCount > 0 Then
                CurPos(4).Caption = Str(SelCount)
                CurPos(4).ForeColor = FirstTextColor
                CurPos(4).BackColor = FirstBackColor
                CurPos(4).Visible = True
            Else
                CurPos(4).Visible = False
            End If
        End If
    End If
End Sub

Private Sub SelectRegnLines(LineNo As Long)
    Dim RefFtyp As String
    Dim RefRegn As String
    Dim CurRegn As String
    Dim CurPrfl As String
    Dim CurLine As Long
    Dim MaxLine As Long
    Dim RefLine As Long
    If chkWork(6).Value = 1 Then
        If NoopList(2).GetLineStatusValue(LineNo) <> 1 Then
            If chkWork(12).Value = 0 Then ClearLineSelection
            RefLine = Val(NoopList(2).GetColumnValue(LineNo, 4))
            RefRegn = NoopList(0).GetColumnValue(RefLine, RegnCol)
            RefFtyp = NoopList(0).GetColumnValue(RefLine, FtypCol)
            If (RefFtyp = "X") Or (RefFtyp = "N") Then RefRegn = ""
            If RefRegn <> "" Then
                If InStr(SelRegnList, RefRegn) = 0 Then
                    If SelRegnList <> "" Then SelRegnList = SelRegnList & ", "
                    SelRegnList = SelRegnList & RefRegn
                End If
                CurPrfl = NoopList(0).GetColumnValue(RefLine, PrflCol)
                If CurPrfl <> "!" Then
                    NoopList(2).SetCellProperty LineNo, 0, "MultiSel"
                    NoopList(2).SetCellProperty LineNo, 1, "MultiSel"
                    NoopList(2).SetCellProperty LineNo, 2, "MultiSel"
                    NoopList(2).SetLineStatusValue LineNo, 1
                End If
                CurLine = LineNo
                CurRegn = RefRegn
                While (CurLine > 0) And (CurRegn = RefRegn)
                    CurLine = CurLine - 1
                    RefLine = Val(NoopList(2).GetColumnValue(CurLine, 4))
                    CurRegn = NoopList(0).GetColumnValue(RefLine, RegnCol)
                    If CurRegn = RefRegn Then
                        CurPrfl = NoopList(0).GetColumnValue(RefLine, PrflCol)
                        If CurPrfl <> "!" Then
                            NoopList(2).SetCellProperty CurLine, 0, "MultiSel"
                            NoopList(2).SetCellProperty CurLine, 1, "MultiSel"
                            NoopList(2).SetCellProperty CurLine, 2, "MultiSel"
                            NoopList(2).SetLineStatusValue CurLine, 1
                        End If
                    End If
                Wend
                CurLine = LineNo
                CurRegn = RefRegn
                MaxLine = NoopList(2).GetLineCount - 1
                While (CurLine < MaxLine) And (CurRegn = RefRegn)
                    CurLine = CurLine + 1
                    RefLine = Val(NoopList(2).GetColumnValue(CurLine, 4))
                    CurRegn = NoopList(0).GetColumnValue(RefLine, RegnCol)
                    If CurRegn = RefRegn Then
                        CurPrfl = NoopList(0).GetColumnValue(RefLine, PrflCol)
                        If CurPrfl <> "!" Then
                            NoopList(2).SetCellProperty CurLine, 0, "MultiSel"
                            NoopList(2).SetCellProperty CurLine, 1, "MultiSel"
                            NoopList(2).SetCellProperty CurLine, 2, "MultiSel"
                            NoopList(2).SetLineStatusValue CurLine, 1
                        End If
                    End If
                Wend
            End If
            NoopList(2).RedrawTab
        End If
    End If
End Sub
Private Sub ClearLineSelection()
    Dim CurLine As Long
    Dim SelList As String
    Dim LinNbr As String
    Dim ItmNbr As Long
    SelList = NoopList(2).GetLinesByStatusValue(1, 0)
    If SelList <> "" Then
        LinNbr = "START"
        ItmNbr = -1
        While LinNbr <> ""
            ItmNbr = ItmNbr + 1
            LinNbr = GetRealItem(SelList, ItmNbr, ",")
            If LinNbr <> "" Then
                CurLine = Val(LinNbr)
                NoopList(2).ResetCellProperties CurLine
                NoopList(2).SetLineStatusValue CurLine, -1
            End If
        Wend
        NoopList(2).RedrawTab
    End If
    SelRegnList = ""
End Sub
Private Sub NoopList_SendLButtonDblClick(Index As Integer, ByVal LineNo As Long, ByVal ColNo As Long)
    If (Index = 2) And (LineNo >= 0) Then
        HandleActionButton -1
    End If
End Sub

Private Sub NoopList_SendRButtonClick(Index As Integer, ByVal LineNo As Long, ByVal ColNo As Long)
    Dim tmpText As String
    Dim CallIndex As Integer
    Dim ForWhat As Integer
    Dim CurAction As Integer
    Dim TextColor As Long
    Dim CurBackColor As Long
    Dim DoIt As Boolean
    CallIndex = -1
    tmpText = Trim(NoopList(Index).GetColumnValue(LineNo, ColNo))
    If Index <> 2 Then
        Select Case ColNo
            Case FlnoCol  'Flights
                CallIndex = 2
            'Case 1
            'Case 2
            'Case 3
            Case Act3Col  'AirCraft Type
                CallIndex = 6
            Case Apc3Col  'AirportCode
                CallIndex = 4
            Case Via3Col  'AirportCode
                CallIndex = 4
            'Case 7
            'Case 8
            Case Else
        End Select
        RecFilter.BasicDetails CallIndex, tmpText
    ElseIf chkWork(6).Value = 0 Then
        Select Case ColNo
            Case 0
                tmpText = Trim(Left(tmpText, 3))
                CallIndex = 0   'AirlineCode
            'Case 1
            'Case 2
            Case Else
        End Select
        RecFilter.BasicDetails CallIndex, tmpText
    Else
        DoIt = CheckValidAction(LineNo)
        If DoIt Then
            If MainDialog.DataSystemType = "OAFOS" Then
                OaFosRegnImport True, "OAL"
            Else
                NoopList(2).GetLineColor LineNo, TextColor, CurBackColor
                CallIndex = DefineColorAction(CurBackColor, ForWhat, CurAction)
                CallServerAction CallIndex, 1, ForWhat, False, ""
            End If
        End If
    End If
End Sub
Private Function CheckValidAction(LineNo As Long) As Boolean
    Dim DoIt As Boolean
    DoIt = True
    Dim TextColor As Long
    Dim CurBackColor As Long
    NoopList(2).GetLineColor LineNo, TextColor, CurBackColor
    If KeybdFlags <> 0 Then DoIt = False
    If CurPos(4).Visible = True Then
        If CurBackColor <> CurPos(4).BackColor Then DoIt = False
    End If
    CheckValidAction = DoIt
End Function
Private Sub NoopList_HitKeyOnLine(Index As Integer, ByVal Key As Integer, ByVal LineNo As Long)
    Dim tmpText As String
    Dim CallIndex As Integer
    Dim ForWhat As Integer
    Dim CurAction As Integer
    Dim CurKey As Integer
    Dim TextColor As Long
    Dim BackColor As Long
    Dim DoIt As Boolean
    If chkWork(6).Value = 1 Then
        If Index = 2 Then
            CurKey = Key
            Select Case KeybdFlags
                Case 0  'Nothing
                    Select Case CurKey
                        Case 13, 32, 45, 46, 73, 68, 85
                            NoopList(2).GetLineColor LineNo, TextColor, BackColor
                            CallIndex = DefineColorAction(BackColor, ForWhat, CurAction)
                            DoIt = False
                            Select Case Key
                                Case 46, 68
                                    If CurAction = FOR_DELETE Then DoIt = True
                                Case 32, 85
                                    If CurAction = FOR_UPDATE Then DoIt = True
                                Case 45, 73
                                    If CurAction = FOR_INSERT Then DoIt = True
                                Case 13
                                    DoIt = True
                                Case Else
                            End Select
                            If DoIt Then CallServerAction CallIndex, 1, ForWhat, False, ""
                        Case Else
                    End Select
                Case 1, 2 'Shift or Ctrl
                    Select Case CurKey
                        Case 13, 32
                            HandleMultiSelect LineNo
                        Case Else
                    End Select
                Case Else
            End Select
        End If
    End If
End Sub

Private Function DefineColorAction(BackColor As Long, ForWhat As Integer, ActionCode As Integer) As Integer
    Dim UseIdx As Integer
    Dim CurIdx As Integer
    UseIdx = -1
    For CurIdx = 1 To 3
        If SrvFkeyCnt(CurIdx).BackColor = BackColor Then
            UseIdx = CurIdx
            ForWhat = FOR_SERVER
            Select Case UseIdx
                Case 1
                    ActionCode = FOR_UPDATE
                Case 2
                    ActionCode = FOR_DELETE
                Case 3
                    ActionCode = FOR_DELETE
                Case Else
                    ActionCode = -1
            End Select
            Exit For
        End If
    Next
    If UseIdx < 0 Then
        For CurIdx = 1 To 3
            If CliFkeyCnt(CurIdx).BackColor = BackColor Then
                UseIdx = CurIdx
                ForWhat = FOR_CLIENT
                Select Case UseIdx
                    Case 1
                        ActionCode = FOR_UPDATE
                    Case 2
                        ActionCode = FOR_INSERT
                    Case 3
                        ActionCode = -1
                    Case Else
                        ActionCode = -1
                End Select
                Exit For
            End If
        Next
    End If
    If UseIdx < 0 Then
        If CliFkeyCnt(10).BackColor = BackColor Then UseIdx = 10
    End If
    DefineColorAction = UseIdx
End Function

Private Sub JumpToColoredLine(ErrColor As Long, LineStatus As Long, JumpToLine As Long)
    Dim CurScroll As Long
    Dim CurLine As Long
    Dim MaxLine As Long
    Dim UseLine As Long
    Dim ForeColor As Long
    Dim BackColor As Long
    Dim LineFound As Boolean
    Dim LoopCount As Integer
    If JumpToLine < 0 Then
        CurLine = NoopList(2).GetCurrentSelected
        If CurLine < 0 Then CurLine = -1
        MaxLine = NoopList(2).GetLineCount - 1
        LineFound = False
        LoopCount = 0
        Do
            While CurLine <= MaxLine
                CurLine = CurLine + 1
                NoopList(2).GetLineColor CurLine, ForeColor, BackColor
                If (LineStatus > 0) And (LineStatus <> NoopList(2).GetLineStatusValue(CurLine)) Then BackColor = -1
                If BackColor = ErrColor Then
                    UseLine = CurLine
                    LineFound = True
                    CurLine = MaxLine + 1
                End If
            Wend
            LoopCount = LoopCount + 1
            CurLine = -1
        Loop While (LineFound = False) And (LoopCount < 2)
    Else
        UseLine = JumpToLine
        LineFound = True
    End If
    If LineFound Then
        If UseLine > 10 Then CurScroll = UseLine - 10 Else CurScroll = 0
        NoopList(2).OnVScrollTo CurScroll
        If LineStatus > 0 Then StopNestedCalls = True
        NoopList(2).SetCurrentSelection UseLine
        StopNestedCalls = False
    End If
End Sub

Private Sub JumpToTaggedLine(TagText As String, JumpToLine As Long)
    Dim CurScroll As Long
    Dim CurLine As Long
    Dim MaxLine As Long
    Dim UseLine As Long
    Dim LineFound As Boolean
    Dim LoopCount As Integer
    If JumpToLine < 0 Then
        CurLine = NoopList(2).GetCurrentSelected
        If CurLine < 0 Then CurLine = -1
        MaxLine = NoopList(2).GetLineCount - 1
        LineFound = False
        LoopCount = 0
        Do
            While CurLine <= MaxLine
                CurLine = CurLine + 1
                If InStr(NoopList(2).GetLineTag(CurLine), TagText) > 0 Then
                    UseLine = CurLine
                    LineFound = True
                    CurLine = MaxLine + 1
                End If
            Wend
            LoopCount = LoopCount + 1
            CurLine = -1
        Loop While (LineFound = False) And (LoopCount < 2)
    Else
        UseLine = JumpToLine
        LineFound = True
    End If
    If LineFound Then
        If UseLine > 10 Then CurScroll = UseLine - 10 Else CurScroll = 0
        NoopList(2).OnVScrollTo CurScroll
        StopNestedCalls = True
        NoopList(2).SetCurrentSelection UseLine
        StopNestedCalls = False
    End If
End Sub

Private Sub AdjustScrollMaster(UseCheckCol As Long, UseCheckLen As Integer, UseCheckData As String)
    Static LastCheckCol As Long
    Static LastCheckLen As Integer
    Static LastCheckData As String
    Dim CheckCol As Long
    Dim CheckLen As Integer
    Dim CheckData As String
    Dim MaxLine As Long
    Dim CurLine As Long
    Dim RecLine As Long
    Dim LeftTextColor As Long
    Dim LeftBackColor As Long
    Dim RightTextColor As Long
    Dim RightBackColor As Long
    Dim UseTextColor As Long
    Dim UseBackColor As Long
    Dim CliCnt1 As Long
    Dim CliCnt2 As Long
    Dim CliCnt3 As Long
    Dim CliCnt4 As Long
    Dim CliCnt5 As Long
    Dim SrvCnt1 As Long
    Dim SrvCnt2 As Long
    Dim SrvCnt3 As Long
    Dim SrvCnt4 As Long
    Dim SrvCnt5 As Long
    Dim tmpFkey As String
    Dim tmpFlno As String
    Dim tmpDoop As String
    Dim tmpLine As String
    Dim LeftTag As String
    Dim RightTag As String
    Dim CurLeftDat As String
    Dim CurRightDat As String
    Dim LeftColorIdx As Integer
    Dim RightColorIdx As Integer
    Dim UseColorIdx As Integer
    Dim tmpBuff As String
    Dim StartZeit
    Screen.MousePointer = 11
    If UseCheckCol < -1 Then
        CheckCol = LastCheckCol
        CheckLen = LastCheckLen
        CheckData = LastCheckData
    Else
        CheckCol = UseCheckCol
        CheckLen = UseCheckLen
        CheckData = UseCheckData
    End If
    NoopList(2).ResetContent
    MaxLine = NoopList(0).GetLineCount - 1
    SetProgressBar 0, 1, MaxLine, "Adjusting Scroll Master Lines ..."
    tmpLine = ""
    StartZeit = Timer
    For CurLine = 0 To MaxLine
        If CheckCol >= 0 Then
            CurLeftDat = Trim(Left(NoopList(0).GetColumnValue(CurLine, CheckCol), CheckLen))
            CurRightDat = Trim(Left(NoopList(1).GetColumnValue(CurLine, CheckCol), CheckLen))
        Else
            CurLeftDat = CheckData
        End If
        If (CurLeftDat = CheckData) Or (CurRightDat = CheckData) Then
            tmpFkey = Trim(NoopList(0).GetColumnValue(CurLine, FkeyCol))
            If tmpFkey <> "" Then
                tmpFlno = Trim(NoopList(0).GetColumnValue(CurLine, FlnoCol))
                tmpDoop = Trim(NoopList(0).GetColumnValue(CurLine, DoopCol))
            Else
                tmpFkey = Trim(NoopList(1).GetColumnValue(CurLine, FkeyCol))
                tmpFlno = Trim(NoopList(1).GetColumnValue(CurLine, FlnoCol))
                tmpDoop = Trim(NoopList(1).GetColumnValue(CurLine, DoopCol))
            End If
            If chkWork(1).Value = 1 Then
                'Shrink
                If chkWork(5).Value = 0 Then
                    NoopList(1).GetLineColor CurLine, RightTextColor, RightBackColor
                Else
                    NoopList(0).GetLineColor CurLine, RightTextColor, RightBackColor
                End If
                If Not FindShrinkColor(RightBackColor) Then tmpFkey = ""
            End If
            If tmpFkey <> "" Then
                tmpLine = tmpLine + FormatFkeyToFlno(tmpFkey, tmpFlno, True) + "," + tmpDoop + "," + Trim(Str(CurLine)) + "," + vbLf
            End If
        End If
        If CurLine Mod 150 = 0 Then
            If tmpLine <> "" Then
                NoopList(2).InsertBuffer tmpLine, vbLf
                tmpLine = ""
            End If
            SetProgressBar 0, 2, CurLine, ""
        End If
    Next
    If tmpLine <> "" Then
        NoopList(2).InsertBuffer tmpLine, vbLf
        tmpLine = ""
    End If
    SetProgressBar 0, 3, 0, ""
    MaxLine = NoopList(2).GetLineCount - 1
    TotalLines = NoopList(2).GetLineCount
    If MaxLine = 0 Then NoopList(2).InsertTextLine "-,,,,-1,", False
    SetProgressBar 0, 1, MaxLine, "Adjusting Scroll Master Colors ..."
    For CurLine = 0 To MaxLine
        If CurLine Mod 150 = 0 Then SetProgressBar 0, 2, CurLine, ""
        RecLine = Val(NoopList(2).GetColumnValue(CurLine, 4))
        NoopList(0).GetLineColor RecLine, LeftTextColor, LeftBackColor
        NoopList(1).GetLineColor RecLine, RightTextColor, RightBackColor
        If RightBackColor = CliFkeyCnt(4).BackColor Then
            RightTextColor = LeftTextColor
            RightBackColor = LeftBackColor
        End If
        If chkWork(5).Value = 0 Then
            UseTextColor = RightTextColor
            UseBackColor = RightBackColor
        Else
            UseTextColor = LeftTextColor
            UseBackColor = LeftBackColor
        End If
        Select Case LeftBackColor
            Case CliFkeyCnt(1).BackColor
                CliCnt1 = CliCnt1 + 1
            Case CliFkeyCnt(2).BackColor
                CliCnt2 = CliCnt2 + 1
            Case CliFkeyCnt(3).BackColor
                CliCnt3 = CliCnt3 + 1
            Case CliFkeyCnt(4).BackColor
                CliCnt4 = CliCnt4 + 1
            Case Else
        End Select
        If chkWork(5).Value = 0 Then
            Select Case RightBackColor
                Case SrvFkeyCnt(1).BackColor
                    SrvCnt1 = SrvCnt1 + 1
                Case SrvFkeyCnt(2).BackColor
                    SrvCnt2 = SrvCnt2 + 1
                Case SrvFkeyCnt(3).BackColor
                    SrvCnt3 = SrvCnt3 + 1
                Case SrvFkeyCnt(4).BackColor
                    SrvCnt4 = SrvCnt4 + 1
                Case Else
                UseTextColor = LeftTextColor
                UseBackColor = LeftBackColor
            End Select
        End If
        NoopList(2).SetLineColor CurLine, UseTextColor, UseBackColor
        LeftTag = NoopList(0).GetLineTag(RecLine)
        RightTag = NoopList(1).GetLineTag(RecLine)
        NoopList(2).SetLineTag CurLine, LeftTag + "/" + RightTag
        If LeftTag <> "" Then CliCnt5 = CliCnt5 + 1
        If RightTag <> "" Then SrvCnt5 = SrvCnt5 + 1
    Next
    If CheckCol >= 0 Then
        SrvFkeyCnt(5).Caption = Str(SrvCnt4)
        SrvFkeyCnt(6).Caption = Str(SrvCnt3)
        SrvFkeyCnt(7).Caption = Str(SrvCnt2)
        SrvFkeyCnt(8).Caption = Str(SrvCnt1)
        SrvFkeyCnt(11).Caption = Str(SrvCnt5)
        CliFkeyCnt(5).Caption = Str(CliCnt4)
        CliFkeyCnt(6).Caption = Str(CliCnt3)
        CliFkeyCnt(7).Caption = Str(CliCnt2)
        CliFkeyCnt(8).Caption = Str(CliCnt1)
        CliFkeyCnt(11).Caption = Str(CliCnt5)
    Else
        If chkWork(1).Value = 0 Then
            SrvFkeyCnt(10).Caption = Str(SrvCnt5)
            SrvFkeyCnt(4).Caption = Str(SrvCnt4)
            SrvFkeyCnt(3).Caption = Str(SrvCnt3)
            SrvFkeyCnt(2).Caption = Str(SrvCnt2)
            SrvFkeyCnt(1).Caption = Str(SrvCnt1)
            CliFkeyCnt(10).Caption = Str(CliCnt5)
            CliFkeyCnt(4).Caption = Str(CliCnt4)
            CliFkeyCnt(3).Caption = Str(CliCnt3)
            CliFkeyCnt(2).Caption = Str(CliCnt2)
            CliFkeyCnt(1).Caption = Str(CliCnt1)
        End If
        SrvFkeyCnt(5).Caption = ""
        SrvFkeyCnt(6).Caption = ""
        SrvFkeyCnt(7).Caption = ""
        SrvFkeyCnt(8).Caption = ""
        SrvFkeyCnt(11).Caption = ""
        CliFkeyCnt(5).Caption = ""
        CliFkeyCnt(6).Caption = ""
        CliFkeyCnt(7).Caption = ""
        CliFkeyCnt(8).Caption = ""
        CliFkeyCnt(11).Caption = ""
    End If
    'MsgBox Timer - StartZeit
    NoopList(2).IndexDestroy "LineIdx"
    NoopList(2).IndexCreate "LineIdx", 4
    SetProgressBar 0, 3, 0, ""
    LastCheckCol = CheckCol
    LastCheckLen = CheckLen
    LastCheckData = CheckData
    NoopList(2).RedrawTab
    
    NoopList(2).SetCurrentSelection 0
    NoopList(2).SetFocus
    Screen.MousePointer = 0
End Sub
Private Function FindShrinkColor(LookUpColor As Long) As Boolean
    Dim i As Integer
    Dim Result As Boolean
    Result = False
    For i = 0 To 5
        If lblShrink(i).BorderStyle = 1 Then
            If LookUpColor = lblShrink(i).BackColor Then
                Result = True
                Exit For
            End If
        End If
    Next
    FindShrinkColor = Result
End Function

Private Function DistinctBasicData(ForWhat As String, itemlist As String) As String
    Static CurLine As Long
    Static MaxLine As Long
    Dim ItmNbr As Integer
    Dim CurItm As String
    Dim ColTxt As String
    Select Case ForWhat
        Case "INIT"
            UfisTools.BlackBox(0).ResetContent
            ItmNbr = 1
            Do
                CurItm = GetItem(itemlist, ItmNbr, ",")
                If CurItm <> "" Then
                    ColTxt = FlightExtract.ExtractData.SelectDistinct(CurItm, "", "", vbLf, True)
                    UfisTools.BlackBox(0).InsertBuffer ColTxt, vbLf
                End If
                ItmNbr = ItmNbr + 1
            Loop While CurItm <> ""
            ColTxt = UfisTools.BlackBox(0).SelectDistinct("0", "", "", vbLf, True)
            UfisTools.BlackBox(0).ResetContent
            ColTxt = Replace(ColTxt, (vbLf & vbLf), vbLf, 1, -1, vbBinaryCompare)
            If Left(ColTxt, 1) = vbLf Then ColTxt = Mid(ColTxt, 2)
            If Right(ColTxt, 1) = vbLf Then
                ColTxt = Left(ColTxt, Len(ColTxt) - 1)
            End If
            UfisTools.BlackBox(0).InsertBuffer ColTxt, vbLf
            InitAlcBasicData
            CurLine = -1
            MaxLine = UfisTools.BlackBox(0).GetLineCount
            DistinctBasicData = Str(MaxLine)
        Case "NEXT"
            CurLine = CurLine + 1
            If CurLine < MaxLine Then
                'itemlist = Replace(UfisTools.BlackBox(0).GetLineValues(CurLine), ",", "", 1, -1, vbBinaryCompare)
                itemlist = UfisTools.BlackBox(0).GetLineValues(CurLine)
                ColTxt = Trim(GetItem(itemlist, 2, ","))
                If Len(ColTxt) <> 3 Then ColTxt = Trim(GetItem(itemlist, 1, ","))
                DistinctBasicData = ColTxt
            Else
                DistinctBasicData = "{=END=}"
            End If
        Case "RESET"
            UfisTools.BlackBox(0).ResetContent
            CurLine = -1
            MaxLine = 0
            DistinctBasicData = "0"
        Case Else
    End Select
End Function

Private Sub InitAlcBasicData()
    Dim LineNo As Long
    Dim LineCnt As Long
    Dim iLen As Integer
    Dim tmpdat As String
    Dim MyAlc3Filter As String
    Dim MyAlc2Filter As String
    LineCnt = UfisTools.BlackBox(0).GetLineCount - 1
    For LineNo = 0 To LineCnt
        tmpdat = UfisTools.BlackBox(0).GetColumnValue(LineNo, 0)
        iLen = Len(tmpdat)
        If iLen = 3 Then MyAlc3Filter = MyAlc3Filter + "'" + tmpdat + "',"
        If iLen = 2 Then MyAlc2Filter = MyAlc2Filter + "'" + tmpdat + "',"
    Next
    UfisTools.BlackBox(0).ResetContent
    If MyAlc3Filter <> "" Then
        MyAlc3Filter = Left(MyAlc3Filter, Len(MyAlc3Filter) - 1)
        ReadAlcData "ALC3", MyAlc3Filter
    End If
    If MyAlc2Filter <> "" Then
        MyAlc2Filter = Left(MyAlc2Filter, Len(MyAlc2Filter) - 1)
        ReadAlcData "ALC2", MyAlc2Filter
    End If
End Sub
Private Sub ReadAlcData(FilterField As String, FilterData As String)
    Dim RetCode As Integer
    Dim tmpSqlKey As String
    Dim tmpAlc2 As String
    Dim tmpAlc3 As String
    Dim tmpAlfn As String
    Dim RecData As String
    Dim tmpLine As String
    Dim CurLine As Long
    Dim MaxLine As Long
    tmpSqlKey = "WHERE " & FilterField & " IN (" & FilterData & ")"
    RetCode = UfisServer.CallCeda(RecData, "RT", "ALTTAB", "ALC2,ALC3,ALFN", "", tmpSqlKey, "", 0, False, False)
    If RetCode >= 0 Then
        MaxLine = UfisServer.DataBuffer(0).GetLineCount - 1
        For CurLine = 0 To MaxLine
            RecData = UfisServer.DataBuffer(0).GetLineValues(CurLine)
            tmpAlc2 = GetItem(RecData, 1, ",")
            tmpAlc3 = GetItem(RecData, 2, ",")
            tmpAlfn = GetItem(RecData, 3, ",")
            If tmpAlc2 = "" Then tmpAlc2 = tmpAlc3
            If tmpAlc3 = "" Then tmpAlc3 = tmpAlc2
            If tmpAlc3 <> "" Then
                If FilterField = "ALC2" Then
                    tmpLine = tmpAlc2 & "," & tmpAlc3 & "," & tmpAlfn
                Else
                    tmpLine = tmpAlc3 & "," & tmpAlc2 & "," & tmpAlfn
                End If
                UfisTools.BlackBox(0).InsertTextLine tmpLine, False
            End If
        Next
    End If
End Sub

Private Function FormatFkeyToFlno(AftFkey As String, AftFlno As String, TurnedKey As Boolean) As String
    Dim Result As String
    Dim tmpdat As String
    Dim tmpVal As Integer
    If AftFlno = "" Then
        If TurnedKey Then
            Result = Left(AftFkey, 3) & Mid(AftFkey, 5, 5) & ","
        Else
            Result = Mid(AftFkey, 6, 3) & Mid(AftFkey, 2, 4) & Mid(AftFkey, 9, 1) & ","
        End If
    Else
        Result = AftFlno & ","
    End If
    Result = Result & Mid(AftFkey, 10, 8) & ","
    Result = Result & Right(AftFkey, 1)
    If Mid(Result, 8, 1) = "#" Then Mid(Result, 8, 1) = " "
    FormatFkeyToFlno = Result
End Function

Private Sub CreateDistinctFlno(AirlCode As String)
    Dim tmpBuff As String
    Dim CurLine As Long
    Dim MaxLine As Long
    FormIsBusy = True
    Screen.MousePointer = 11
    FlnoTabCombo(0).ComboResetContent "FlnoCodes"
    FlnoTabCombo(1).ResetContent
    MaxLine = UfisTools.BlackBox(0).GetLineCount - 1
    If MaxLine < 0 Then
        StatusBar.Panels(2).Text = "Creating Flight Filter Lines ..."
        UfisTools.BlackBox(0).ResetContent
        tmpBuff = NoopList(0).SelectDistinct(Trim(Str(FlnoCol)), "", "", vbLf, True)
        UfisTools.BlackBox(0).InsertBuffer tmpBuff, vbLf
        tmpBuff = NoopList(1).SelectDistinct(Trim(Str(FlnoCol)), "", "", vbLf, True)
        UfisTools.BlackBox(0).InsertBuffer tmpBuff, vbLf
        tmpBuff = UfisTools.BlackBox(0).SelectDistinct("0", "", "", vbLf, True)
        UfisTools.BlackBox(0).ResetContent
        tmpBuff = Replace(tmpBuff, (vbLf & vbLf), vbLf, 1, -1, vbBinaryCompare)
        If Left(tmpBuff, 1) = vbLf Then tmpBuff = Mid(tmpBuff, 2)
        If Right(tmpBuff, 1) = vbLf Then tmpBuff = Left(tmpBuff, Len(tmpBuff) - 1)
        UfisTools.BlackBox(0).InsertBuffer tmpBuff, vbLf
        UfisTools.BlackBox(0).Sort 0, True, True
    End If
    MaxLine = UfisTools.BlackBox(0).GetLineCount - 1
    SetProgressBar 0, 1, MaxLine, "Adjusting Flight Filters ..."
    For CurLine = 0 To MaxLine
        If CurLine Mod 50 = 0 Then
            SetProgressBar 0, 2, CurLine, ""
        End If
        tmpBuff = UfisTools.BlackBox(0).GetLineValues(CurLine)
        If (AirlCode = "-ALL-") Or (RTrim(Left(tmpBuff, 3)) = AirlCode) Then FlnoTabCombo(1).InsertTextLine tmpBuff, False
    Next
    If FlnoTabCombo(1).GetLineCount > 1 Then FlnoTabCombo(1).InsertTextLineAt 0, "-ALL-,", False
    FlnoTabCombo(0).SetColumnValue 0, 0, FlnoTabCombo(1).GetColumnValue(0, 0)
    MaxLine = FlnoTabCombo(1).GetLineCount - 1
    For CurLine = 0 To MaxLine
        FlnoTabCombo(0).ComboAddTextLines "FlnoCodes", FlnoTabCombo(1).GetLineValues(CurLine), vbLf
    Next
    FlnoTabCombo(0).SetCurrentSelection 0
    CurPos(1).Caption = FlnoTabCombo(1).GetColumnValue(0, 0)
    SetProgressBar 0, 3, 0, ""
    Screen.MousePointer = 0
    FormIsBusy = False
End Sub

Private Sub CreateDistinctRegn(AirlCode As String)
    Dim tmpBuff As String
    Dim CurLine As Long
    Dim MaxLine As Long
    FormIsBusy = True
    On Error Resume Next
    Screen.MousePointer = 11
    RegnTabCombo(0).ComboResetContent "RegnCodes"
    RegnTabCombo(1).ResetContent
    tmpBuff = NoopList(0).SelectDistinct(Trim(Str(RegnCol)), "", "", vbLf, True)
    RegnTabCombo(1).InsertBuffer tmpBuff, vbLf
    tmpBuff = NoopList(1).SelectDistinct(Trim(Str(RegnCol)), "", "", vbLf, True)
    RegnTabCombo(1).InsertBuffer tmpBuff, vbLf
    tmpBuff = RegnTabCombo(1).SelectDistinct("0", "", "", vbLf, True)
    RegnTabCombo(1).ResetContent
    tmpBuff = Replace(tmpBuff, (vbLf & vbLf), vbLf, 1, -1, vbBinaryCompare)
    If Left(tmpBuff, 1) = vbLf Then tmpBuff = Mid(tmpBuff, 2)
    If Right(tmpBuff, 1) = vbLf Then tmpBuff = Left(tmpBuff, Len(tmpBuff) - 1)
    RegnTabCombo(1).InsertBuffer tmpBuff, vbLf
    RegnTabCombo(1).Sort 0, True, True
    MaxLine = RegnTabCombo(1).GetLineCount
    If MaxLine > 1 Then RegnTabCombo(1).InsertTextLineAt 0, "-ALL-,", False
    MaxLine = RegnTabCombo(1).GetLineCount - 1
    RegnTabCombo(0).SetColumnValue 0, 0, RegnTabCombo(1).GetColumnValue(0, 0)
    For CurLine = 0 To MaxLine
        tmpBuff = RegnTabCombo(1).GetLineValues(CurLine)
        RegnTabCombo(0).ComboAddTextLines "RegnCodes", tmpBuff, vbLf
    Next
    RegnTabCombo(0).SetCurrentSelection 0
    CurPos(1).Caption = RegnTabCombo(1).GetColumnValue(0, 0)
    Screen.MousePointer = 0
    FormIsBusy = False
End Sub

Private Sub FlnoList_Click()
    Dim NewFlno As String
    Dim CurLeftFlno As String
    Dim CurRightFlno As String
    Dim tmpLine As String
    Dim tmpFkey As String
    Dim CurLine As Long
    Dim MaxLine As Long
    Dim NewLine As Long
    Dim Dummy As Long
    Dim LeftBackColor As Long
    Dim RightBackColor As Long
    NewFlno = GetItem(FlnoList.Text, 1, "/")
    If NewFlno = "" Then NewFlno = GetItem(FlnoList.Text, 2, "/")
    If (Not FormIsBusy) And (FlnoList.ListCount > 2) And (NewFlno <> CurPos(1).Caption) Then
        CurPos(1).Caption = NewFlno
        If NewFlno <> "-ALL-" Then
            AdjustScrollMaster FlnoCol, 9, NewFlno
        Else
            CurPos(0).Caption = ""
            'Alc3List_Click
        End If
    End If
End Sub

Private Function TurnAftFkey(AftFkey As String) As String
    TurnAftFkey = Mid(AftFkey, 6, 3) + Left(AftFkey, 5) + Mid(AftFkey, 9)
End Function

Private Sub SetProgressBar(Index As Integer, ForWhat As Integer, CurValue As Long, Context As String)
    Static ProgFact(0 To 2) As Variant
    Dim tmpVal As Long
    Dim NewVal As Integer
    On Error Resume Next
    Select Case ForWhat
        Case 0
            ProgFact(Index) = -1
            If CurValue > 0 Then
                MyProgressBar(Index).Max = 30000
                MyProgressBar(Index).Value = 0
                ProgFact(Index) = MyProgressBar(Index).Max / CurValue
            End If
            If ProgFact(Index) > 0 Then
                NewVal = CInt(Val(Context) * ProgFact(Index))
                If NewVal <= MyProgressBar(Index).Max Then MyProgressBar(Index).Value = NewVal
                MyProgressBar(Index).Visible = True
                If Index = 0 Then
                    tmpVal = CLng(MyProgressBar(Index).Value) * 100 / CLng(MyProgressBar(Index).Max)
                    If tmpVal > 100 Then tmpVal = 100
                    CurPos(2).Caption = Str(tmpVal) & "%"
                    CurPos(2).Refresh
                End If
            End If
        Case 1
            ProgFact(Index) = -1
            If CurValue > 0 Then
                MyProgressBar(Index).Max = 30000
                ProgFact(Index) = MyProgressBar(Index).Max / CurValue
                MyProgressBar(Index).Value = 0
                MyProgressBar(Index).Visible = True
            End If
            If Index = 0 Then StatusBar.Panels(2).Text = Context
        Case 2
            If ProgFact(Index) > 0 Then
                NewVal = CInt(CurValue * ProgFact(Index))
                If NewVal <= MyProgressBar(Index).Max Then MyProgressBar(Index).Value = NewVal
                If Index = 0 Then
                    tmpVal = CLng(MyProgressBar(Index).Value) * 100 / CLng(MyProgressBar(Index).Max)
                    If tmpVal > 100 Then tmpVal = 100
                    CurPos(2).Caption = Str(tmpVal) & "%"
                    CurPos(2).Refresh
                End If
            End If
            If Context <> "" Then StatusBar.Panels(2).Text = Context
        Case 3
            ProgFact(Index) = -1
            MyProgressBar(Index).Visible = False
            If Index = 0 Then
                StatusBar.Panels(2).Text = Context
                CurPos(2).Caption = ""
            End If
    End Select
End Sub

Private Sub RegnTabCombo_ComboSelChanged(Index As Integer, ByVal LineNo As Long, ByVal ColNo As Long, ByVal OldValue As String, ByVal NewValues As String)
    Dim NewRegn As String
    Dim CurAlc As String
    NewRegn = GetItem(NewValues, 1, ",")
    If (Not FormIsBusy) And (NewRegn <> CurPos(1).Caption) Then
        CurPos(1).Caption = NewRegn
        If NewRegn <> "-ALL-" Then
            AdjustScrollMaster RegnCol, 9, NewRegn
        Else
            CurAlc = CurPos(0).Caption
            CurPos(0).Caption = ""
            AlcTabCombo_ComboSelChanged 0, 0, 0, "***", CurAlc
        End If
    End If
End Sub

Private Sub RotaLink_SendLButtonDblClick(Index As Integer, ByVal LineNo As Long, ByVal ColNo As Long)
    Dim ShowLineNo As Long
    Dim LookLine As String
    Dim SetLine As String
    If Index <> 2 Then
        ShowLineNo = -1
        If LineNo = 0 Then
            ShowLineNo = ArrFlightLineNo(Index)
        ElseIf LineNo = 1 Then
            ShowLineNo = DepFlightLineNo(Index)
        End If
        If ShowLineNo >= 0 Then
            LookLine = Trim(Str(ShowLineNo))
            'SetLine = NoopList(2).GetLinesByColumnValue(4, LookLine, 0)
            SetLine = NoopList(2).GetLinesByIndexValue("LineIdx", LookLine, 0)
            If SetLine <> "" Then
                ShowLineNo = Val(SetLine)
                NoopList(2).OnVScrollTo ShowLineNo
                NoopList(2).SetCurrentSelection ShowLineNo
            End If
        End If
    End If
End Sub
Private Function GetMainLineNo(CurLineNo As Long) As Long
    Dim MainLine As Long
    Dim LookLine As String
    Dim SetLine As String
    MainLine = -1
    If CurLineNo >= 0 Then
        LookLine = Trim(Str(CurLineNo))
        'SetLine = NoopList(2).GetLinesByColumnValue(4, LookLine, 0)
        SetLine = NoopList(2).GetLinesByIndexValue("LineIdx", LookLine, 0)
        If SetLine <> "" Then MainLine = Val(SetLine)
    End If
    GetMainLineNo = MainLine
End Function

Private Sub txtMinGrdTime_Change()
    MinGrdTime = Abs(Val(txtMinGrdTime.Text))
    If CStr(MinGrdTime) <> txtMinGrdTime.Tag Then
        txtMinGrdTime.BackColor = vbRed
        txtMinGrdTime.ForeColor = vbWhite
    Else
        txtMinGrdTime.BackColor = vbWhite
        txtMinGrdTime.ForeColor = vbBlack
    End If
End Sub

Private Sub txtMinGrdTime_GotFocus()
    SetTextSelected
End Sub

Private Sub txtMinGrdTime_LostFocus()
    MinGrdTime = Abs(Val(txtMinGrdTime.Text))
    txtMinGrdTime.Text = CStr(MinGrdTime)
End Sub

Private Sub txtSearch_Change(Index As Integer)
    SearchInList Index
    txtSearch(Index).SetFocus
End Sub

Private Sub txtSearch_KeyPress(Index As Integer, KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
    'KeyAscii = Asc(UCase(Chr(KeyAscii)))
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    CheckKeyboardState Shift
    If KeyCode = 114 Then
        If Shift = 0 Then
            SearchInList -1
        ElseIf Shift = 1 Then
            'Shift
        End If
    End If
    If KeyCode = vbKeyF1 Then DisplayHtmHelpFile "", Me.Name, Me.ActiveControl.Name
End Sub
Private Sub Form_KeyUp(KeyCode As Integer, Shift As Integer)
    CheckKeyboardState Shift
End Sub

Private Sub CliFkeyCnt_MouseDown(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
    Dim UseSelList As String
    If Button = 2 Then
        If MainDialog.DataSystemType <> "OAFOS" Then
            UseSelList = NoopList(2).GetLinesByBackColor(CliFkeyCnt(Index).BackColor)
            CallServerAction Index, Val(CliFkeyCnt(Index).Caption), FOR_CLIENT, True, UseSelList
        End If
    Else
        If Val(CliFkeyCnt(Index).Caption) > 0 Then
            If Index < 10 Then
                JumpToColoredLine CliFkeyCnt(Index).BackColor, -1, -1
            Else
                JumpToTaggedLine "L", -1
            End If
        End If
    End If
End Sub

Private Sub SrvFkeyCnt_MouseDown(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
    Dim UseSelList As String
    If Button = 2 Then
        If MainDialog.DataSystemType <> "OAFOS" Then
            UseSelList = NoopList(2).GetLinesByBackColor(SrvFkeyCnt(Index).BackColor)
            CallServerAction Index, Val(SrvFkeyCnt(Index).Caption), FOR_SERVER, True, UseSelList
        End If
    Else
        If Val(SrvFkeyCnt(Index).Caption) > 0 Then
            If Index < 10 Then
                JumpToColoredLine SrvFkeyCnt(Index).BackColor, -1, -1
            Else
                JumpToTaggedLine "R", -1
            End If
        End If
    End If
End Sub

Private Sub CallServerAction(ActionIndex As Integer, UseCount As Long, CalledFor As Integer, MyFullMode As Boolean, UseSelList As String)
    Dim MsgTxt As String
    Dim ActionCode As Integer
    Dim RetVal As Integer
    Dim SqlRet As Integer
    Dim LinCnt As Long
    Dim SelLine As Long
    Dim LineNo As Long
    Dim TblNam As String
    Dim SqlCmd As String
    Dim SqlKey As String
    Dim FldLst As String
    Dim DatLst As String
    Dim SelLst As String
    Dim SelItm As Long
    Dim SelDat As String
    Dim tmpAdid As String
    Dim chkAdid As String
    Dim LoopCnt As Integer
    Dim iLoop As Integer
    Dim JumpValue As Integer
    Dim CurForeColor As Long
    Dim CurBackColor As Long
    Dim DiffList As String
    Dim ArrLineNo As Long
    Dim DepLineNo As Long
    Dim IsValidOldRotation As Boolean
    Dim IsValidNewRotation As Boolean
    If (chkWork(6).Value = 1) Then
        If (MainDialog.DataSystemType <> "OAFOS") Then
            Screen.MousePointer = 11
            ActionCode = -1
            LinCnt = UseCount
            Select Case ActionIndex
                Case 1, 8
                    MsgTxt = "Do you want to update"
                    ActionCode = FOR_UPDATE
                Case 2, 7
                    If CalledFor = FOR_SERVER Then
                        MsgTxt = "Do you want to delete"
                        ActionCode = FOR_DELETE
                    Else
                        MsgTxt = "Do you want to insert"
                        ActionCode = FOR_INSERT
                    End If
                Case 3, 6
                    If CalledFor = FOR_SERVER Then
                        MsgTxt = "Do you want to remove"
                        ActionCode = FOR_DELETE
                    End If
                Case Else
                    ActionCode = -1
            End Select
            JumpValue = chkWork(8).Value
            If (ActionCode >= 0) And (LinCnt > 0) Then
                RetVal = 1
                If (MyFullMode) Or (LinCnt > 1) Then
                    If LinCnt > 1 Then MsgTxt = MsgTxt & Str(LinCnt) & " records?" Else MsgTxt = MsgTxt & " this record?"
                    RetVal = MyMsgBox.CallAskUser(0, 0, 0, "Transaction Control", MsgTxt, "ask", "Yes;F,No", UserAnswer)
                Else
                    SelLine = NoopList(2).GetCurrentSelected
                    LineNo = Val(NoopList(2).GetColumnValue(SelLine, 4))
                    NoopList(0).GetLineColor LineNo, CurForeColor, CurBackColor
                    If CurForeColor = LimitColor Then
                        MsgTxt = "Sorry. Won't touch operational flights!"
                        RetVal = MyMsgBox.CallAskUser(0, 0, 0, "Transaction Control", MsgTxt, "stop", "", UserAnswer)
                        RetVal = -1
                    Else
                        If chkWork(7).Value = 1 Then
                            MsgTxt = MsgTxt & " this record?"
                            RetVal = MyMsgBox.CallAskUser(0, 0, 0, "Transaction Control", MsgTxt, "ask", "Yes;F,No", UserAnswer)
                        End If
                    End If
                End If
                Me.Refresh
                If RetVal = 1 Then
                    If LinCnt > 1 Then chkWork(8).Value = 0
                    TblNam = "AFTTAB"
                    SelLst = UseSelList
                    If UseSelList <> "" Then UseSelList = ""
                    If SelLst = "" Then
                        SelLst = NoopList(2).GetLinesByStatusValue(1, 0)
                        SelLine = NoopList(2).GetCurrentSelected
                        If SelLst = "" Then SelLst = Trim(Str(SelLine))
                    End If
                    If ActionCode = FOR_UPDATE Then LoopCnt = 2 Else LoopCnt = 1
                    SelDat = "START"
                    SelItm = 0
                    While SelDat <> ""
                        SelDat = GetRealItem(SelLst, SelItm, ",")
                        If SelDat <> "" Then
                            SelLine = Val(SelDat)
                            JumpToColoredLine -1, 1, SelLine
                            LineNo = Val(NoopList(2).GetColumnValue(SelLine, 4))
                            If NoopList(0).GetLineTag(LineNo) = "DONE" Then LineNo = -1
                            If LineNo >= 0 Then
                                SqlCmd = ""
                                IsValidOldRotation = True
                                If ActionCode <> FOR_DELETE Then
                                    DiffList = ShowRotationDetails(LineNo, LineNo, False, ArrLineNo, DepLineNo, IsValidOldRotation)
                                    If MainDialog.DataSystemType <> "OAFOS" Then CheckSplitBeforeUpdate IsValidOldRotation, IsValidNewRotation, ArrLineNo, DepLineNo
                                End If
                                If (IsValidOldRotation) Then
                                    Select Case ActionCode
                                        Case FOR_INSERT
                                            PrepareAftInsert LineNo, SqlCmd, SqlKey, FldLst, DatLst, ""
                                        Case FOR_UPDATE
                                                PrepareAftUpdate LineNo, SqlCmd, SqlKey, FldLst, DatLst, ""
                                        Case FOR_DELETE
                                            PrepareAftDelete LineNo, SqlCmd, SqlKey, FldLst, DatLst
                                        Case Else
                                    End Select
                                    If SqlCmd <> "" Then
                                        SqlRet = UfisServer.CallCeda(UserAnswer, SqlCmd, TblNam, FldLst, DatLst, SqlKey, "", 0, False, False)
                                        Select Case ActionCode
                                            Case FOR_INSERT
                                                FinishAftInsert SelLine, True
                                            Case FOR_UPDATE
                                                    FinishAftUpdate SelLine, True
                                            Case FOR_DELETE
                                                FinishAftDelete SelLine
                                            Case Else
                                        End Select
                                    End If
                                    NoopList(0).SetLineTag LineNo, "DONE"
                                Else
                                    SendDataToImpMgr SelLine, ArrLineNo, DepLineNo, IsValidOldRotation, IsValidNewRotation
                                End If
                            End If
                        End If
                        SelItm = SelItm + 1
                    Wend
                End If
            End If
            chkWork(8).Value = JumpValue
            Screen.MousePointer = 0
        End If
    End If
End Sub

Private Sub CheckSplitBeforeUpdate(IsValidOldRotation As Boolean, IsValidNewRotation As Boolean, ArrLineNo As Long, DepLineNo As Long)
    Dim CliArrFkey As String
    Dim CliDepFkey As String
    Dim SvrArrFkey As String
    Dim SvrDepFkey As String
    Dim CliArrAct3 As String
    Dim CliDepAct3 As String
    Dim SvrArrAct3 As String
    Dim SvrDepAct3 As String
    Dim SvrArrRkey As String
    Dim SvrDepRkey As String
    Dim CliArrTime As String
    Dim CliDepTime As String
    Dim SvrArrTime As String
    Dim SvrDepTime As String
    Dim MustSplit As Boolean
    Dim SqlRet As Integer
    Dim SqlKey As String
    Dim tmpArr
    Dim tmpDep
    Dim tmpDiff As Long
    IsValidNewRotation = True
    If (ArrLineNo >= 0) And (DepLineNo >= 0) Then
        SvrArrRkey = Trim(NoopList(1).GetColumnValue(ArrLineNo, RkeyCol))
        SvrDepRkey = Trim(NoopList(1).GetColumnValue(DepLineNo, RkeyCol))
        If SvrArrRkey = SvrDepRkey Then
            MustSplit = False
            CliArrAct3 = Trim(NoopList(0).GetColumnValue(ArrLineNo, Act3Col))
            CliDepAct3 = Trim(NoopList(0).GetColumnValue(DepLineNo, Act3Col))
            SvrArrAct3 = Trim(NoopList(1).GetColumnValue(ArrLineNo, Act3Col))
            SvrDepAct3 = Trim(NoopList(1).GetColumnValue(DepLineNo, Act3Col))
            
            If (CliArrAct3 <> "") And (CliDepAct3 <> "") Then
                If CliArrAct3 <> CliDepAct3 Then IsValidNewRotation = False
                If (SvrArrAct3 <> "") And (SvrDepAct3 <> "") Then
                    If (CliArrAct3 <> SvrArrAct3) Then MustSplit = True
                    If (CliArrAct3 <> SvrDepAct3) Then MustSplit = True
                    If (CliDepAct3 <> SvrDepAct3) Then MustSplit = True
                    If (CliDepAct3 <> SvrArrAct3) Then MustSplit = True
                End If
            End If
            If (CliArrAct3 = "") And (CliDepAct3 <> "") Then
                If (SvrArrAct3 <> "") And (SvrDepAct3 <> "") Then
                    If (CliDepAct3 <> SvrDepAct3) Then MustSplit = True
                    If (CliDepAct3 <> SvrArrAct3) Then MustSplit = True
                End If
            End If
            If (CliArrAct3 <> "") And (CliDepAct3 = "") Then
                If (SvrArrAct3 <> "") And (SvrDepAct3 <> "") Then
                    If (CliArrAct3 <> SvrArrAct3) Then MustSplit = True
                    If (CliArrAct3 <> SvrDepAct3) Then MustSplit = True
                End If
            End If
            If (SvrArrAct3 <> "") And (SvrDepAct3 <> "") Then
                If SvrArrAct3 <> SvrDepAct3 Then MustSplit = True
            End If
            If MustSplit Then
                CliArrFkey = Trim(NoopList(0).GetColumnValue(ArrLineNo, FkeyCol))
                CliDepFkey = Trim(NoopList(0).GetColumnValue(DepLineNo, FkeyCol))
                SvrArrFkey = Trim(NoopList(1).GetColumnValue(ArrLineNo, FkeyCol))
                SvrDepFkey = Trim(NoopList(1).GetColumnValue(DepLineNo, FkeyCol))
                If (CliArrFkey = SvrArrFkey) And (CliDepFkey = SvrDepFkey) Then
                    If CliArrAct3 = CliDepAct3 Then
                        MustSplit = False
                        IsValidOldRotation = False 'Triggers SendToImpMgr !!!
                    End If
                End If
            End If
            
            CliArrTime = Trim(NoopList(0).GetColumnValue(ArrLineNo, TimeCol))
            CliDepTime = Trim(NoopList(0).GetColumnValue(DepLineNo, TimeCol))
            SvrArrTime = Trim(NoopList(1).GetColumnValue(ArrLineNo, TimeCol))
            SvrDepTime = Trim(NoopList(1).GetColumnValue(DepLineNo, TimeCol))
            If CliArrAct3 = "" Then CliArrTime = ""
            If CliDepAct3 = "" Then CliDepTime = ""
            If (CliArrTime <> "") And (CliDepTime <> "") Then
                tmpArr = CedaFullDateToVb(CliArrTime)
                tmpDep = CedaFullDateToVb(CliDepTime)
                tmpDiff = CLng(DateDiff("n", tmpArr, tmpDep))
                If tmpDiff < MinGrdTime Then IsValidNewRotation = False
                If (SvrArrTime <> "") And (SvrDepTime <> "") Then
                    tmpArr = CedaFullDateToVb(CliArrTime)
                    tmpDep = CedaFullDateToVb(SvrDepTime)
                    tmpDiff = CLng(DateDiff("n", tmpArr, tmpDep))
                    If tmpDiff < MinGrdTime Then MustSplit = True
                    tmpArr = CedaFullDateToVb(SvrArrTime)
                    tmpDep = CedaFullDateToVb(CliDepTime)
                    tmpDiff = CLng(DateDiff("n", tmpArr, tmpDep))
                    If tmpDiff < MinGrdTime Then MustSplit = True
                End If
            End If
            If (CliArrTime = "") And (CliDepTime <> "") Then
                If (SvrArrTime <> "") And (SvrDepTime <> "") Then
                    tmpArr = CedaFullDateToVb(SvrArrTime)
                    tmpDep = CedaFullDateToVb(CliDepTime)
                    tmpDiff = CLng(DateDiff("n", tmpArr, tmpDep))
                    If tmpDiff < MinGrdTime Then MustSplit = True
                End If
            End If
            If (CliArrTime <> "") And (CliDepTime = "") Then
                If (SvrArrTime <> "") And (SvrDepTime <> "") Then
                    tmpArr = CedaFullDateToVb(CliArrTime)
                    tmpDep = CedaFullDateToVb(SvrDepTime)
                    tmpDiff = CLng(DateDiff("n", tmpArr, tmpDep))
                    If tmpDiff < MinGrdTime Then MustSplit = True
                End If
            End If
            If MustSplit Then
                SqlKey = Trim(NoopList(1).GetColumnValue(ArrLineNo, UrnoCol))
                SqlKey = SqlKey & "," & SqlKey
                SqlRet = UfisServer.CallCeda(UserAnswer, "SPR", "AFTTAB", "", "", SqlKey, "", 0, False, False)
                IsValidOldRotation = False
            End If
        End If
    End If
End Sub
Private Sub PrepareAftInsert(LineNo As Long, SqlCmd As String, SqlKey As String, Fields As String, Data As String, IgnoreFields As String)
    Dim CliRec As String
    Dim tmpAdid As String
    Dim tmpDate As String
    Dim tmpFlno As String
    Dim tmpAirl As String
    Dim tmpFltn As String
    Dim tmpFlns As String
    Dim tmpTime As String
    Dim chkTime As String
    Dim tmpVia3 As String
    
    CliRec = NoopList(0).GetLineValues(LineNo)
    SqlCmd = "ISF"
    'Extract of aftdata.doc ('flight' documentation)
    'Command ISF: Insert Seasonal Flight
    'Selection bei einem Flug
    '1. Date (YYYYMMDD) Begin of the period
    '2. Date (YYYYMMDD) End of the period
    '3. Operatioanl days (1-7) of the flight
    '4. Weekly frequency (1..2..) of the flight
    tmpDate = Left(GetRealItem(CliRec, TimeCol, ","), 8)
    SqlKey = tmpDate & "," & tmpDate & ",1234567,1"
    tmpAdid = GetRealItem(CliRec, AdidCol, ",")
    Fields = ""
    Data = ""
    Fields = Fields & ",FTYP"
    Data = Data & "," & GetRealItem(CliRec, FtypCol, ",")
    tmpFlno = GetRealItem(CliRec, FlnoCol, ",")
    StripAftFlno tmpFlno, tmpAirl, tmpFltn, tmpFlns
    Fields = Fields & ",FLNO"
    Data = Data & "," & tmpFlno
    Fields = Fields & ",ALC3"
    Data = Data & "," & Alc3LookUp(Trim(Left(GetRealItem(CliRec, FlnoCol, ","), 3)))
    Fields = Fields & ",FLTN"
    Data = Data & "," & tmpFltn
    Fields = Fields & ",FLNS"
    Data = Data & "," & tmpFlns
    Select Case tmpAdid
        Case "A"
            Fields = Fields & ",STOA"
            Data = Data & "," & GetRealItem(CliRec, TimeCol, ",") & "00"
            Fields = Fields & ",ORG3"
            Data = Data & "," & GetRealItem(CliRec, Apc3Col, ",")
            Fields = Fields & ",DES3"
            Data = Data & "," & HomeAirport
        Case "D"
            Fields = Fields & ",STOD"
            Data = Data & "," & GetRealItem(CliRec, TimeCol, ",") & "00"
            Fields = Fields & ",ORG3"
            Data = Data & "," & HomeAirport
            Fields = Fields & ",DES3"
            Data = Data & "," & GetRealItem(CliRec, Apc3Col, ",")
        Case Else
    End Select
    If InStr(IgnoreFields, "REGN") = 0 Then
        If GetRealItem(CliRec, RegnCol, ",") <> "" Then
            Fields = Fields & ",REGN"
            Data = Data & "," & GetRealItem(CliRec, RegnCol, ",")
        End If
    End If
    Fields = Fields & ",ACT3"
    Data = Data & "," & GetRealItem(CliRec, Act3Col, ",")
    Fields = Fields & ",STYP"
    Data = Data & "," & GetRealItem(CliRec, StypCol, ",")
    Fields = Fields & ",VIAL"
    tmpVia3 = GetRealItem(CliRec, Via3Col, ",")
    If (tmpVia3 <> "") And (MainDialog.UseVialFlag = "YES") Then
        Data = Data & "," & "1" & GetRealItem(CliRec, Via3Col, ",")
    Else
        Data = Data & "," & " " & GetRealItem(CliRec, Via3Col, ",")
    End If
    Fields = Mid(Fields, 2)
    Fields = Replace(Fields, "STYP", MainDialog.UseStypField, 1, -1, vbBinaryCompare)
    Data = Mid(Data, 2)
    If MainDialog.DataSystemType <> "OAFOS" Then
        If (Left(Data, 1) <> "S") And (Left(Data, 1) <> "O") Then
            SqlKey = ""
            SqlCmd = ""
        End If
    End If
    chkTime = GetImportCheckTime
    tmpTime = GetRealItem(CliRec, TimeCol, ",")
    If tmpTime < chkTime Then SqlCmd = ""
End Sub

Private Sub FinishAftInsert(SelLine As Long, DoJump As Boolean)
    Dim LineNo As Long
    Dim i As Integer
    NoopList(2).SetLineColor SelLine, vbBlack, LightGray
    NoopList(2).ResetCellProperties SelLine
    NoopList(2).SetLineStatusValue SelLine, -1
    NoopList(2).RedrawTab
    LineNo = Val(NoopList(2).GetColumnValue(SelLine, 4))
    For i = 0 To 1
        NoopList(i).SetLineColor LineNo, vbBlack, LightGray
        NoopList(i).RedrawTab
    Next
    CliFkeyCnt(2).Caption = Str(Val(CliFkeyCnt(2).Caption) - 1)
    CliFkeyCnt(4).Caption = Str(Val(CliFkeyCnt(4).Caption) + 1)
    SrvFkeyCnt(4).Caption = Str(Val(SrvFkeyCnt(4).Caption) + 1)
    If Val(CliFkeyCnt(7).Caption) > 0 Then
        CliFkeyCnt(7).Caption = Str(Val(CliFkeyCnt(7).Caption) - 1)
        CliFkeyCnt(5).Caption = Str(Val(CliFkeyCnt(5).Caption) + 1)
        SrvFkeyCnt(5).Caption = Str(Val(SrvFkeyCnt(5).Caption) + 1)
    End If
    If (DoJump = True) And (chkWork(8).Value = 1) And (Val(CliFkeyCnt(2).Caption) > 0) Then JumpToColoredLine CliFkeyCnt(2).BackColor, -1, -1
End Sub

Private Sub PrepareAftUpdate(LineNo As Long, SqlCmd As String, SqlKey As String, Fields As String, Data As String, IgnoreFields As String)
    Dim CliRec As String
    Dim SrvRec As String
    Dim tmpAdid As String
    Dim chkTime As String
    Dim tmpTime As String
    Dim tmpVia3 As String
    CliRec = NoopList(0).GetLineValues(LineNo)
    SrvRec = NoopList(1).GetLineValues(LineNo)
    SqlCmd = "UFR"
    SqlKey = "WHERE URNO=" & GetRealItem(SrvRec, UrnoCol, ",")
    Fields = ""
    Data = ""
    tmpAdid = GetRealItem(SrvRec, AdidCol, ",")
    If Not CompareValues(LineNo, FtypCol, False, True, "S,O") Then
        Fields = Fields & ",FTYP"
        Data = Data & "," & GetRealItem(CliRec, FtypCol, ",")
    End If
    If InStr(IgnoreFields, "REGN") = 0 Then
        If GetRealItem(CliRec, RegnCol, ",") <> "" Then
            If Not CompareValues(LineNo, RegnCol, False, True, "") Then
                Fields = Fields & ",REGN"
                Data = Data & "," & GetRealItem(CliRec, RegnCol, ",")
            End If
        End If
    End If
    If Not CompareValues(LineNo, Act3Col, False, True, "") Then
        Fields = Fields & ",ACT3"
        Data = Data & "," & GetRealItem(CliRec, Act3Col, ",")
    End If
    If Not CompareValues(LineNo, StypCol, False, True, "") Then
        Fields = Fields & ",STYP"
        Data = Data & "," & GetRealItem(CliRec, StypCol, ",")
    End If
    Select Case tmpAdid
        Case "A"
            If Not CompareValues(LineNo, TimeCol, False, True, "") Then
                Fields = Fields & ",STOA"
                Data = Data & "," & GetRealItem(CliRec, TimeCol, ",") & "00"
            End If
            If Not CompareValues(LineNo, Apc3Col, False, True, "") Then
                Fields = Fields & ",ORG3"
                Data = Data & "," & GetRealItem(CliRec, Apc3Col, ",")
            End If
        Case "D"
            If Not CompareValues(LineNo, TimeCol, False, True, "") Then
                Fields = Fields & ",STOD"
                Data = Data & "," & GetRealItem(CliRec, TimeCol, ",") & "00"
            End If
            If Not CompareValues(LineNo, Apc3Col, False, True, "") Then
                Fields = Fields & ",DES3"
                Data = Data & "," & GetRealItem(CliRec, Apc3Col, ",")
            End If
        Case Else
    End Select
    If Not CompareValues(LineNo, Via3Col, False, True, "") Then
        Fields = Fields & ",VIAL"
        tmpVia3 = GetRealItem(CliRec, Via3Col, ",")
        If (tmpVia3 <> "") And (MainDialog.UseVialFlag = "YES") Then
            Data = Data & "," & "1" & GetRealItem(CliRec, Via3Col, ",")
        Else
            Data = Data & "," & " " & GetRealItem(CliRec, Via3Col, ",")
        End If
    End If
    Fields = Mid(Fields, 2)
    Data = Mid(Data, 2)
    Fields = Replace(Fields, "STYP", MainDialog.UseStypField, 1, -1, vbBinaryCompare)
    If Fields = "" Then SqlCmd = ""
    chkTime = GetImportCheckTime
    tmpTime = GetRealItem(CliRec, TimeCol, ",")
    If tmpTime < chkTime Then SqlCmd = ""
    tmpTime = GetRealItem(SrvRec, TimeCol, ",")
    If tmpTime < chkTime Then SqlCmd = ""
End Sub

Private Sub FinishAftUpdate(SelLine As Long, DoJump As Boolean)
    Dim LineNo As Long
    Dim i As Integer
    NoopList(2).SetLineColor SelLine, vbBlack, LightGray
    NoopList(2).ResetCellProperties SelLine
    NoopList(2).SetLineStatusValue SelLine, -1
    NoopList(2).RedrawTab
    LineNo = Val(NoopList(2).GetColumnValue(SelLine, 4))
    If NoopList(0).GetColumnValue(LineNo, FlnoCol) <> "" Then
        NoopList(1).SetColumnValue LineNo, TimeCol, NoopList(0).GetColumnValue(LineNo, TimeCol)
        NoopList(1).SetColumnValue LineNo, Apc3Col, NoopList(0).GetColumnValue(LineNo, Apc3Col)
        NoopList(1).SetColumnValue LineNo, Via3Col, NoopList(0).GetColumnValue(LineNo, Via3Col)
        NoopList(1).SetColumnValue LineNo, StypCol, NoopList(0).GetColumnValue(LineNo, StypCol)
        NoopList(1).SetColumnValue LineNo, Act3Col, NoopList(0).GetColumnValue(LineNo, Act3Col)
    End If
    For i = 0 To 1
        NoopList(i).SetLineColor LineNo, vbBlack, LightGray
        NoopList(i).ResetLineDecorations LineNo
        NoopList(i).RedrawTab
    Next
    SrvFkeyCnt(1).Caption = Str(Val(SrvFkeyCnt(1).Caption) - 1)
    SrvFkeyCnt(4).Caption = Str(Val(SrvFkeyCnt(4).Caption) + 1)
    If Val(SrvFkeyCnt(8).Caption) > 0 Then
        SrvFkeyCnt(8).Caption = Str(Val(SrvFkeyCnt(8).Caption) - 1)
        SrvFkeyCnt(5).Caption = Str(Val(SrvFkeyCnt(5).Caption) + 1)
    End If
    CliFkeyCnt(1).Caption = Str(Val(CliFkeyCnt(1).Caption) - 1)
    CliFkeyCnt(4).Caption = Str(Val(CliFkeyCnt(4).Caption) + 1)
    If Val(CliFkeyCnt(8).Caption) > 0 Then
        CliFkeyCnt(8).Caption = Str(Val(CliFkeyCnt(8).Caption) - 1)
        CliFkeyCnt(5).Caption = Str(Val(CliFkeyCnt(5).Caption) + 1)
    End If
    If (DoJump = True) And (chkWork(8).Value = 1) And (Val(SrvFkeyCnt(1).Caption) > 0) Then JumpToColoredLine SrvFkeyCnt(1).BackColor, -1, -1
End Sub

Private Sub PrepareAftDelete(LineNo As Long, SqlCmd As String, SqlKey As String, Fields As String, Data As String)
    Dim SrvRec As String
    Dim CurFlno As String
    Dim RetVal As Integer
    Dim MsgTxt As String
    Dim tmpTime As String
    Dim chkTime As String
    SrvRec = NoopList(1).GetLineValues(LineNo)
    CurFlno = GetRealItem(SrvRec, FlnoCol, ",")
    RetVal = 1
    If CheckAlcDelError(CurFlno) Then
        MsgTxt = ""
        MsgTxt = MsgTxt & "You ignored errors of the import file" & vbNewLine
        MsgTxt = MsgTxt & "in the main data list. Thus the compared" & vbNewLine
        MsgTxt = MsgTxt & "flight lists are not complete regarding" & vbNewLine
        MsgTxt = MsgTxt & "the selected flight carrier." & vbNewLine
        MsgTxt = MsgTxt & "Do you want to delete flight '" & CurFlno & "' ?"
        RetVal = MyMsgBox.CallAskUser(0, 0, 0, "Deletion Control", MsgTxt, "stop2", "Yes;F,No", UserAnswer)
    End If
    If RetVal = 1 Then
        SqlCmd = "DFR"
        SqlKey = "WHERE URNO=" & GetRealItem(SrvRec, UrnoCol, ",")
    Else
        SqlCmd = ""
    End If
    chkTime = GetImportCheckTime
    tmpTime = GetRealItem(SrvRec, TimeCol, ",")
    If tmpTime < chkTime Then SqlCmd = ""
End Sub

Private Sub FinishAftDelete(SelLine As Long)
    Dim LineNo As Long
    Dim i As Integer
    NoopList(2).SetLineColor SelLine, vbBlack, LightGray
    NoopList(2).ResetCellProperties SelLine
    NoopList(2).SetLineStatusValue SelLine, -1
    NoopList(2).RedrawTab
    LineNo = Val(NoopList(2).GetColumnValue(SelLine, 4))
    For i = 0 To 1
        NoopList(i).SetLineColor LineNo, vbBlack, LightGray
        NoopList(i).ResetLineDecorations LineNo
        NoopList(i).RedrawTab
    Next
    SrvFkeyCnt(2).Caption = Str(Val(SrvFkeyCnt(2).Caption) - 1)
    SrvFkeyCnt(4).Caption = Str(Val(SrvFkeyCnt(4).Caption) + 1)
    CliFkeyCnt(4).Caption = Str(Val(CliFkeyCnt(4).Caption) + 1)
    If Val(SrvFkeyCnt(7).Caption) > 0 Then
        SrvFkeyCnt(7).Caption = Str(Val(SrvFkeyCnt(7).Caption) - 1)
        SrvFkeyCnt(5).Caption = Str(Val(SrvFkeyCnt(5).Caption) + 1)
        CliFkeyCnt(5).Caption = Str(Val(CliFkeyCnt(5).Caption) + 1)
    End If
    If (chkWork(8).Value = 1) And (Val(SrvFkeyCnt(2).Caption) > 0) Then JumpToColoredLine SrvFkeyCnt(2).BackColor, -1, -1
End Sub

Private Function TrimRecordData(RecData As String) As String
    Dim OldResult As String
    Dim NewResult As String
    Dim LastNonBlank As Long
    Dim CurPos As Long
    Dim MaxPos As Long
    OldResult = RecData
    NewResult = Replace(OldResult, " ,", ",", 1, -1, vbBinaryCompare)
    While NewResult <> OldResult
        OldResult = NewResult
        NewResult = Replace(OldResult, " ,", ",", 1, -1, vbBinaryCompare)
    Wend
    TrimRecordData = RTrim(NewResult)
End Function

Private Function CheckAlcDelError(ChkFlno As String) As Boolean
    Dim ChkAlc As String
    ChkAlc = "," + Trim(Left(ChkFlno, 3)) + ","
    If (MyFullMode) And (InStr(ChkErrorList, ChkAlc) > 0) Then
        CheckAlcDelError = True
    Else
        CheckAlcDelError = False
    End If
End Function

Private Function GetImportCheckTime()
    Dim chkTime As String
    chkTime = ServerTime.Tag
    GetImportCheckTime = chkTime
End Function
Public Sub EvaluateBc(BcCmd As String, BcTws As String, BcSelect As String, BcFields As String, BcData As String)
    Dim tmpResult As String
    Dim LineCode As String
    Dim CurLine As Long
    Select Case BcCmd
        Case "ACCBGN"
            TotalStartZeit = Timer
            TotalSecs.Caption = ""
            TotalSecs.Refresh
        Case "RAC", "ACCEND"
            tmpResult = Trim(Str(Timer - TotalStartZeit))
            If Left(tmpResult, 1) = "." Then tmpResult = "0" & tmpResult
            tmpResult = GetItem(tmpResult, 1, ".") & "s"
            TotalSecs.Caption = tmpResult
            TotalSecs.Refresh
        Case "RAX"
            LineCode = GetItem(BcTws, 4, ".")
            CurLine = Val(LineCode)
            If CurLine >= 0 Then
                tmpResult = Trim(NoopList(0).GetColumnValue(CurLine, RegnCol))
                If tmpResult <> "" Then
                    CedaProgress.AppendRaxData BcSelect, tmpResult
                End If
            End If
        Case Else
    End Select
End Sub

Private Sub SendDataToImpMgr(SelLine As Long, ArrLineNo As Long, DepLineNo As Long, IsValidOldRotation As Boolean, IsValidNewRotation As Boolean)
    Dim ArrRec As String
    Dim DepRec As String
    Dim ArrFldLst As String
    Dim DepFldLst As String
    Dim ImpArrRec As String
    Dim ImpDepRec As String
    Dim CedaFields As String
    Dim CedaData As String
    Dim SrvAnsw As String
    Dim tmpVia3 As String
    Dim ArrAct3 As String
    Dim DepAct3 As String
    Dim CurForeColor As Long
    Dim CurBackColor As Long
    Dim MainLineNo As Long
    
    ArrRec = ""
    DepRec = ""
    If ArrLineNo >= 0 Then ArrRec = NoopList(0).GetLineValues(ArrLineNo)
    If DepLineNo >= 0 Then DepRec = NoopList(0).GetLineValues(DepLineNo)
    ArrFldLst = "VPFR,VPTO,FRQD,FRQW,ORG3,VIAL,STOA,ACT3,STYP,FLNO,AIRL,FLTN,FLNS,DES3,FTYP"
    DepFldLst = "VPFR,VPTO,FRQD,FRQW,DES3,VIAL,STOD,ACT3,STYP,FLNO,AIRL,FLTN,FLNS,ORG3,FTYP"
    ArrFldLst = Replace(ArrFldLst, "STYP", MainDialog.UseStypField, 1, -1, vbBinaryCompare)
    DepFldLst = Replace(DepFldLst, "STYP", MainDialog.UseStypField, 1, -1, vbBinaryCompare)
    ImpArrRec = ""
    ImpArrRec = ImpArrRec & Left(GetRealItem(ArrRec, TimeCol, ","), 8) & ","
    ImpArrRec = ImpArrRec & Left(GetRealItem(ArrRec, TimeCol, ","), 8) & ","
    ImpArrRec = ImpArrRec & GetRealItem(ArrRec, DoopCol, ",") & ",1,"
    ImpArrRec = ImpArrRec & GetRealItem(ArrRec, Apc3Col, ",") & ","
    tmpVia3 = GetRealItem(ArrRec, Via3Col, ",")
    If (tmpVia3 <> "") And (MainDialog.UseVialFlag = "YES") Then
        ImpArrRec = ImpArrRec & "1" & tmpVia3 & ","
    Else
        ImpArrRec = ImpArrRec & " " & tmpVia3 & ","
    End If
    ImpArrRec = ImpArrRec & GetRealItem(ArrRec, TimeCol, ",") & "00,"
    ImpArrRec = ImpArrRec & GetRealItem(ArrRec, Act3Col, ",") & ","
    ImpArrRec = ImpArrRec & GetRealItem(ArrRec, StypCol, ",") & ","
    ImpArrRec = ImpArrRec & GetRealItem(ArrRec, FlnoCol, ",") & ","
    ImpArrRec = ImpArrRec & Left(GetRealItem(ArrRec, FkeyCol, ","), 3) & ","
    ImpArrRec = ImpArrRec & Trim(Mid(GetRealItem(ArrRec, FlnoCol, ","), 4, 5)) & ","
    ImpArrRec = ImpArrRec & Trim(Mid(GetRealItem(ArrRec, FlnoCol, ","), 9, 1)) & ","
    ImpArrRec = ImpArrRec & HomeAirport & ","
    ImpArrRec = ImpArrRec & GetRealItem(ArrRec, FtypCol, ",")
    ImpDepRec = ""
    ImpDepRec = ImpDepRec & Left(GetRealItem(DepRec, TimeCol, ","), 8) & ","
    ImpDepRec = ImpDepRec & Left(GetRealItem(DepRec, TimeCol, ","), 8) & ","
    ImpDepRec = ImpDepRec & GetRealItem(DepRec, DoopCol, ",") & ",1,"
    ImpDepRec = ImpDepRec & GetRealItem(DepRec, Apc3Col, ",") & ","
    tmpVia3 = GetRealItem(DepRec, Via3Col, ",")
    If (tmpVia3 <> "") And (MainDialog.UseVialFlag = "YES") Then
        ImpDepRec = ImpDepRec & "1" & tmpVia3 & ","
    Else
        ImpDepRec = ImpDepRec & " " & tmpVia3 & ","
    End If
    ImpDepRec = ImpDepRec & GetRealItem(DepRec, TimeCol, ",") & "00,"
    ImpDepRec = ImpDepRec & GetRealItem(DepRec, Act3Col, ",") & ","
    ImpDepRec = ImpDepRec & GetRealItem(DepRec, StypCol, ",") & ","
    ImpDepRec = ImpDepRec & GetRealItem(DepRec, FlnoCol, ",") & ","
    ImpDepRec = ImpDepRec & Left(GetRealItem(DepRec, FkeyCol, ","), 3) & ","
    ImpDepRec = ImpDepRec & Trim(Mid(GetRealItem(DepRec, FlnoCol, ","), 4, 5)) & ","
    ImpDepRec = ImpDepRec & Trim(Mid(GetRealItem(DepRec, FlnoCol, ","), 9, 1)) & ","
    ImpDepRec = ImpDepRec & HomeAirport & ","
    ImpDepRec = ImpDepRec & GetRealItem(DepRec, FtypCol, ",")
    
    If IsValidNewRotation Then
        CedaFields = ArrFldLst & vbLf & DepFldLst & vbLf
        CedaData = ImpArrRec & vbLf & ImpDepRec & vbLf
        UfisServer.CallCeda SrvAnsw, "EXCO", "AFTTAB", CedaFields, CedaData, "IMP,IMPORT", "", 0, False, False
    Else
        CedaFields = ArrFldLst & vbLf & DepFldLst & vbLf
        CedaData = ImpArrRec & vbLf & ",,,,,,,,,,,,,,," & vbLf
        UfisServer.CallCeda SrvAnsw, "EXCO", "AFTTAB", CedaFields, CedaData, "IMP,IMPORT", "", 0, False, False
        CedaFields = ArrFldLst & vbLf & DepFldLst & vbLf
        CedaData = ",,,,,,,,,,,,,,," & vbLf & ImpDepRec & vbLf
        UfisServer.CallCeda SrvAnsw, "EXCO", "AFTTAB", CedaFields, CedaData, "IMP,IMPORT", "", 0, False, False
    End If
    
    If SelLine <> ArrLineNo Then MainLineNo = GetMainLineNo(ArrLineNo) Else MainLineNo = SelLine
    NoopList(0).GetLineColor ArrLineNo, CurForeColor, CurBackColor
    If CurBackColor = CliFkeyCnt(1).BackColor Then FinishAftUpdate MainLineNo, False
    If CurBackColor = CliFkeyCnt(2).BackColor Then FinishAftInsert MainLineNo, False
    If SelLine <> DepLineNo Then MainLineNo = GetMainLineNo(DepLineNo) Else MainLineNo = SelLine
    NoopList(0).GetLineColor DepLineNo, CurForeColor, CurBackColor
    If CurBackColor = CliFkeyCnt(1).BackColor Then FinishAftUpdate MainLineNo, False
    If CurBackColor = CliFkeyCnt(2).BackColor Then FinishAftInsert MainLineNo, False
    
    If Val(CliFkeyCnt(10).Caption) > 1 Then CliFkeyCnt(10).Caption = Trim(Str(Val(CliFkeyCnt(10).Caption) - 1))
    If Val(CliFkeyCnt(10).Caption) > 1 Then CliFkeyCnt(10).Caption = Trim(Str(Val(CliFkeyCnt(10).Caption) - 1))
    If Val(SrvFkeyCnt(10).Caption) > 1 Then SrvFkeyCnt(10).Caption = Trim(Str(Val(SrvFkeyCnt(10).Caption) - 1))
    If Val(SrvFkeyCnt(10).Caption) > 1 Then SrvFkeyCnt(10).Caption = Trim(Str(Val(SrvFkeyCnt(10).Caption) - 1))
    NoopList(0).SetLineTag ArrLineNo, "DONE"
    NoopList(0).SetLineTag DepLineNo, "DONE"
    NoopList(0).RedrawTab
End Sub

Private Sub CheckKeyboardState(UseKeybdFlags As Integer)
    CheckKeyboard UseKeybdFlags
    StatusBar.Panels(4).Text = KeybdState
End Sub

