VERSION 5.00
Object = "{64E8E382-05E2-11D2-9B1D-9B0630BC8F12}#1.0#0"; "Tab.ocx"
Begin VB.Form SsimForm 
   Caption         =   "SSIM Export"
   ClientHeight    =   9405
   ClientLeft      =   60
   ClientTop       =   1590
   ClientWidth     =   15210
   BeginProperty Font 
      Name            =   "Courier New"
      Size            =   11.25
      Charset         =   0
      Weight          =   700
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   ScaleHeight     =   9405
   ScaleWidth      =   15210
   Begin VB.TextBox RejectCountValue 
      Alignment       =   2  'Center
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   2760
      Locked          =   -1  'True
      TabIndex        =   27
      Top             =   8950
      Width           =   615
   End
   Begin VB.TextBox ReadCountValue 
      Alignment       =   2  'Center
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   1080
      Locked          =   -1  'True
      TabIndex        =   26
      Top             =   8950
      Width           =   615
   End
   Begin VB.ComboBox cbSeason 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   720
      Sorted          =   -1  'True
      TabIndex        =   0
      Top             =   240
      Width           =   960
   End
   Begin VB.CommandButton CloseBtn 
      Caption         =   "&Close"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   13920
      TabIndex        =   14
      Top             =   240
      Width           =   975
   End
   Begin VB.Frame Frame1 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1155
      Left            =   0
      TabIndex        =   15
      Top             =   0
      Width           =   14955
      Begin VB.CommandButton AirlOptBtn 
         Caption         =   "+"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   7080
         TabIndex        =   6
         Top             =   775
         Width           =   255
      End
      Begin VB.CommandButton AirpOptBtn 
         Caption         =   "+"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   7080
         TabIndex        =   4
         Top             =   300
         Width           =   255
      End
      Begin VB.TextBox AirlFilter 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   5040
         TabIndex        =   5
         Top             =   720
         Width           =   1935
      End
      Begin VB.TextBox AirpFilter 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   5040
         TabIndex        =   3
         Top             =   240
         Width           =   1935
      End
      Begin VB.CommandButton DeleteBtn 
         Caption         =   "Delete"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   13920
         TabIndex        =   10
         Top             =   720
         Width           =   975
      End
      Begin VB.CommandButton CreateBtn 
         Caption         =   "Create"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   12960
         TabIndex        =   9
         Top             =   720
         Width           =   975
      End
      Begin VB.CommandButton StoreBtn 
         Caption         =   "Store"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   12000
         TabIndex        =   13
         Top             =   240
         Width           =   975
      End
      Begin VB.CommandButton ReadBtn 
         Caption         =   "Read"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   12000
         TabIndex        =   8
         Top             =   720
         Width           =   975
      End
      Begin VB.CommandButton FileNameBtn 
         Caption         =   "File Name"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   7440
         TabIndex        =   11
         Top             =   240
         Width           =   1095
      End
      Begin VB.TextBox OutFileName 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   8520
         TabIndex        =   12
         Top             =   240
         Width           =   3375
      End
      Begin VB.ComboBox cbFiles 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   8520
         Sorted          =   -1  'True
         TabIndex        =   7
         Top             =   720
         Width           =   3375
      End
      Begin VB.TextBox ValidTo 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   2760
         TabIndex        =   2
         Top             =   720
         Width           =   975
      End
      Begin VB.TextBox ValidFrom 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   2760
         TabIndex        =   1
         Top             =   240
         Width           =   975
      End
      Begin VB.Label Label6 
         Alignment       =   1  'Right Justify
         Caption         =   "Airline Filter"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   3840
         TabIndex        =   23
         Top             =   780
         Width           =   1140
      End
      Begin VB.Label Label5 
         Alignment       =   1  'Right Justify
         Caption         =   "Airport Filter"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   3840
         TabIndex        =   22
         Top             =   300
         Width           =   1140
      End
      Begin VB.Label Label4 
         Alignment       =   1  'Right Justify
         Caption         =   "Files"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   7440
         TabIndex        =   21
         Top             =   780
         Width           =   855
      End
      Begin VB.Label Label3 
         Alignment       =   1  'Right Justify
         Caption         =   "to"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   1680
         TabIndex        =   20
         Top             =   780
         Width           =   1000
      End
      Begin VB.Label Label2 
         Alignment       =   1  'Right Justify
         Caption         =   "Valid from"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   1680
         TabIndex        =   19
         Top             =   300
         Width           =   1000
      End
      Begin VB.Label Label1 
         Alignment       =   1  'Right Justify
         Caption         =   "Season"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   120
         TabIndex        =   18
         Top             =   300
         Width           =   600
      End
   End
   Begin TABLib.TAB TAB1 
      Height          =   7215
      Left            =   0
      TabIndex        =   16
      TabStop         =   0   'False
      Top             =   1560
      Width           =   14925
      _Version        =   65536
      _ExtentX        =   26326
      _ExtentY        =   12726
      _StockProps     =   0
   End
   Begin VB.Label RejectCountLabel 
      Caption         =   "Recs rej :"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   345
      Left            =   1920
      TabIndex        =   25
      Top             =   9000
      Width           =   855
   End
   Begin VB.Label ReadCountLabel 
      Caption         =   "Recs read :"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   345
      Left            =   120
      TabIndex        =   24
      Top             =   9000
      Width           =   975
   End
   Begin VB.Label HeaderName 
      Caption         =   "SSIM Data"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   0
      TabIndex        =   17
      Top             =   1200
      Width           =   2055
   End
End
Attribute VB_Name = "SsimForm"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
    Dim ilRC As Integer
    Dim seaTab As String
    Dim ssimFiles As String
    Dim vpfr As String
    Dim vpto As String
    Dim lastRecType As String
    Dim prevBufLine As String
    Dim ignoreLine As Boolean
    Dim nextLineNo As Long
    Dim nextTabLineNo As Integer
    Dim rowPrevBuf As Integer
    Dim rowCurBuf As Integer
    Dim readCount As Long
    Dim rejectCount As Long
    Dim emptyFreq As Boolean

Private Sub Form_Load()
    Dim TableName As String
    Dim fieldNames As String
    Dim tabSelection As String
    Dim NoOfLines As Integer
    Dim i As Integer
    
    Screen.MousePointer = vbHourglass
    TAB1.LineHeight = 18
    TAB1.FontName = "Courier New"
    TAB1.HeaderFontSize = 17
    TAB1.FontSize = 17
    TAB1.SetTabFontBold True
    TAB1.EnableHeaderSizing True
    TAB1.ShowHorzScroller True
    TAB1.EnableInlineEdit False
    TAB1.InplaceEditUpperCase = False
    TAB1.EmptyAreaRightColor = 12632256
    TAB1.Visible = False

    SsimForm.Caption = MainForm.GetText("SsimForm", "SsimForm", 1)
    FileNameBtn.Caption = MainForm.GetText("SsimForm", "FileNameBtn", 1)
    ReadBtn.Caption = MainForm.GetText("SsimForm", "ReadBtn", 1)
    StoreBtn.Caption = MainForm.GetText("SsimForm", "StoreBtn", 1)
    CreateBtn.Caption = MainForm.GetText("SsimForm", "CreateBtn", 1)
    DeleteBtn.Caption = MainForm.GetText("SsimForm", "DeleteBtn", 1)
    CloseBtn.Caption = MainForm.GetText("SsimForm", "CloseBtn", 1)
    Label1.Caption = MainForm.GetText("SsimForm", "Label1", 1)
    Label2.Caption = MainForm.GetText("SsimForm", "Label2", 1)
    Label3.Caption = MainForm.GetText("SsimForm", "Label3", 1)
    Label4.Caption = MainForm.GetText("SsimForm", "Label4", 1)
    Label5.Caption = MainForm.GetText("SsimForm", "Label5", 1)
    Label6.Caption = MainForm.GetText("SsimForm", "Label6", 1)
    ReadCountLabel.Caption = MainForm.GetText("SsimForm", "Label7", 1)
    RejectCountLabel.Caption = MainForm.GetText("SsimForm", "Label8", 1)
    
    cbSeason.Clear
    seaTab = ""
    TableName = "SEA" & MainForm.tableExt
    fieldNames = "SEAS,VPFR,VPTO"
    tabSelection = "ORDER BY VPFR"
    ilRC = MainForm.Ufis.CallServer("RT", TableName, fieldNames, "", tabSelection, "240")
    If ilRC = 0 Then
        NoOfLines = MainForm.Ufis.GetBufferCount
        For i = 0 To NoOfLines - 1
            cbSeason.AddItem GetItem(MainForm.Ufis.GetBufferLine(i), 1, ",")
            seaTab = seaTab & MainForm.Ufis.GetBufferLine(i) & vbCrLf
        Next i
    End If
    
    cbFiles.Clear
    ssimFiles = ""
    TableName = "SSIM"
    fieldNames = "FILENAMES"
    tabSelection = cbSeason.Text
    ilRC = MainForm.Ufis.CallServer("SSIR", TableName, fieldNames, "", tabSelection, "240")
    If ilRC = 0 Then
        NoOfLines = MainForm.Ufis.GetBufferCount
        For i = 0 To NoOfLines - 1
            cbFiles.AddItem GetItem(MainForm.Ufis.GetBufferLine(i), 1, ",")
            ssimFiles = ssimFiles & MainForm.Ufis.GetBufferLine(i) & vbCrLf
        Next i
    End If
    
    HeaderName.Caption = MainForm.GetText("SsimForm", "Header1", 1)
    Screen.MousePointer = vbArrow
End Sub

Private Sub CloseBtn_Click()
    SsimForm.Visible = False
    Unload SsimForm
End Sub

Private Sub AirpFilter_keyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
End Sub

Private Sub AirlFilter_keyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
End Sub

Private Sub AirpOptBtn_Click()

    If AirpOptBtn.Caption = "+" Then
        AirpOptBtn.Caption = "-"
    Else
        AirpOptBtn.Caption = "+"
    End If
    
End Sub

Private Sub AirlOptBtn_Click()

    If AirlOptBtn.Caption = "+" Then
        AirlOptBtn.Caption = "-"
    Else
        AirlOptBtn.Caption = "+"
    End If
    
End Sub

Private Sub cbFiles_Click()

    If cbFiles.Text <> "" Then
        ReadBtn_Click
    End If
End Sub

Private Sub FileNameBtn_Click()

    OutFileName.Text = ""
    MainForm.CommonDialog1.DialogTitle = MainForm.GetText("SsimForm", "DialogTitle1", 1)
    MainForm.CommonDialog1.Filter = MainForm.GetText("SsimForm", "DialogFilter1", 1)
    MainForm.CommonDialog1.FilterIndex = 1
    MainForm.CommonDialog1.ShowOpen
    OutFileName.Text = MainForm.CommonDialog1.FileName
    If OutFileName.Text <> "" Then
        StoreBtn_Click
    End If
    
End Sub

Private Sub cbSeason_Click()
    Dim selSeason As String
    Dim NoOfLines As Integer
    Dim i As Integer
    Dim il As Long
    Dim curLine As String
    Dim tmpSeason As String
    Dim tmpVpfr As String
    Dim tmpVpto As String
    Dim TableName As String
    Dim fieldNames As String
    Dim tabSelection As String
    
    selSeason = Trim(cbSeason.Text)
    If selSeason = "" Then
        MsgBox (MainForm.GetText("SsimForm", "MsgBox1", 1))
        Exit Sub
    End If
    
    NoOfLines = MainForm.GetLineCount(seaTab)
    For il = 1 To NoOfLines
        curLine = MainForm.GetLine(seaTab, il)
        tmpSeason = Trim(GetItem(curLine, 1, ","))
        If selSeason = tmpSeason Then
            tmpVpfr = Trim(GetItem(curLine, 2, ","))
            tmpVpto = Trim(GetItem(curLine, 3, ","))
            tmpVpfr = Mid(tmpVpfr, 7, 2) & "." & Mid(tmpVpfr, 5, 2) & "." & Mid(tmpVpfr, 1, 4)
            tmpVpto = Mid(tmpVpto, 7, 2) & "." & Mid(tmpVpto, 5, 2) & "." & Mid(tmpVpto, 1, 4)
        End If
    Next il
    ValidFrom.Text = tmpVpfr
    ValidTo.Text = tmpVpto
    vpfr = tmpVpfr
    vpto = tmpVpto

    Screen.MousePointer = vbHourglass
    cbFiles.Clear
    ssimFiles = ""
    TableName = "SSIM"
    fieldNames = "FILENAMES"
    tabSelection = cbSeason.Text
    ilRC = MainForm.Ufis.CallServer("SSIR", TableName, fieldNames, "", tabSelection, "240")
    If ilRC = 0 Then
        NoOfLines = MainForm.Ufis.GetBufferCount
        For i = 0 To NoOfLines - 1
            cbFiles.AddItem GetItem(MainForm.Ufis.GetBufferLine(i), 1, ",")
            ssimFiles = ssimFiles & MainForm.Ufis.GetBufferLine(i) & vbCrLf
        Next i
    End If
    Screen.MousePointer = vbArrow
    
End Sub

Private Sub ReadBtn_Click()
    Dim TableName As String
    Dim fieldNames As String
    Dim tabSelection As String
    Dim tmpVpfr As String
    Dim tmpVpto As String

    If cbFiles.Text = "" Then
        MsgBox (MainForm.GetText("SsimForm", "MsgBox2", 1))
        Exit Sub
    End If
    Screen.MousePointer = vbHourglass
    tmpVpfr = ValidFrom.Text
    If Mid(tmpVpfr, 3, 1) <> "." Then
        tmpVpfr = Mid(tmpVpfr, 1, 2) & "." & Mid(tmpVpfr, 3)
    End If
    If Mid(tmpVpfr, 6, 1) <> "." Then
        tmpVpfr = Mid(tmpVpfr, 1, 5) & "." & Mid(tmpVpfr, 6)
    End If
    ValidFrom.Text = tmpVpfr
    tmpVpto = ValidTo.Text
    If Mid(tmpVpto, 3, 1) <> "." Then
        tmpVpto = Mid(tmpVpto, 1, 2) & "." & Mid(tmpVpto, 3)
    End If
    If Mid(tmpVpto, 6, 1) <> "." Then
        tmpVpto = Mid(tmpVpto, 1, 5) & "." & Mid(tmpVpto, 6)
    End If
    ValidTo.Text = tmpVpto
    TAB1.ResetContent
    TAB1.HeaderString = cbFiles.Text
    TAB1.HeaderLengthString = 2000
    TableName = "SSIM"
    fieldNames = "READ"
    tabSelection = cbFiles.Text
    ilRC = MainForm.Ufis.CallServer("SSIR", TableName, fieldNames, "", tabSelection, "240")
    If ilRC = 0 Then
        FillTable tmpVpfr, tmpVpto
    End If
    TAB1.Visible = True
    TAB1.RedrawTab
    Screen.MousePointer = vbArrow

End Sub

Private Sub StoreBtn_Click()
    Dim i As Long
    Dim NoOfOutputLines As Long
    
    If OutFileName.Text = "" Then
        MsgBox (MainForm.GetText("SsimForm", "MsgBox3", 1))
        Exit Sub
    End If
    Screen.MousePointer = vbHourglass
    Open OutFileName.Text For Output As #1
        NoOfOutputLines = TAB1.GetLineCount
        For i = 0 To NoOfOutputLines - 1
            Print #1, GetItem(TAB1.GetLineValues(i), 1, ",")
        Next i
    Close #1
    Screen.MousePointer = vbArrow
    MsgBox (MainForm.GetText("SsimForm", "MsgBox4", 1))

End Sub
    
Private Sub CreateBtn_Click()
    Dim selSeason As String
    Dim TableName As String
    Dim fieldNames As String
    Dim tabSelection As String
    Dim tmpVpfr As String
    Dim tmpVpto As String
    
    selSeason = Trim(cbSeason.Text)
    If selSeason = "" Then
        MsgBox (MainForm.GetText("SsimForm", "MsgBox1", 1))
        Exit Sub
    End If
    Screen.MousePointer = vbHourglass
    tmpVpfr = ValidFrom.Text
    If Mid(tmpVpfr, 3, 1) <> "." Then
        tmpVpfr = Mid(tmpVpfr, 1, 2) & "." & Mid(tmpVpfr, 3)
    End If
    If Mid(tmpVpfr, 6, 1) <> "." Then
        tmpVpfr = Mid(tmpVpfr, 1, 5) & "." & Mid(tmpVpfr, 6)
    End If
    ValidFrom.Text = tmpVpfr
    tmpVpto = ValidTo.Text
    If Mid(tmpVpto, 3, 1) <> "." Then
        tmpVpto = Mid(tmpVpto, 1, 2) & "." & Mid(tmpVpto, 3)
    End If
    If Mid(tmpVpto, 6, 1) <> "." Then
        tmpVpto = Mid(tmpVpto, 1, 5) & "." & Mid(tmpVpto, 6)
    End If
    ValidTo.Text = tmpVpto
    TAB1.ResetContent
    TAB1.HeaderString = cbFiles.Text
    TAB1.HeaderLengthString = 2000
    TableName = "SSIM"
    fieldNames = "CREATE"
    tabSelection = selSeason
    ilRC = MainForm.Ufis.CallServer("SSIR", TableName, fieldNames, "", tabSelection, "240")
    If ilRC = 0 Then
        cbFiles.AddItem MainForm.Ufis.GetBufferLine(0)
        cbFiles.Text = MainForm.Ufis.GetBufferLine(0)
        TAB1.HeaderString = cbFiles.Text
        FillTable tmpVpfr, tmpVpto
    End If
    TAB1.Visible = True
    TAB1.RedrawTab
    Screen.MousePointer = vbArrow

End Sub
    
Private Sub DeleteBtn_Click()
    Dim TableName As String
    Dim fieldNames As String
    Dim tabSelection As String
    Dim NoOfLines As Integer
    Dim i As Integer
    
    If cbFiles.Text = "" Then
        MsgBox (MainForm.GetText("SsimForm", "MsgBox2", 1))
        Exit Sub
    End If
    Select Case MsgBox(MainForm.GetText("SsimForm", "MsgBox5", 1), vbOKCancel, MainForm.GetText("SsimForm", "MsgBox5", 2))
        Case vbCancel
            Exit Sub
        Case Else
            'just continue
    End Select
    Screen.MousePointer = vbHourglass
    TableName = "SSIM"
    fieldNames = "DELETE"
    tabSelection = cbFiles.Text
    ilRC = MainForm.Ufis.CallServer("SSIR", TableName, fieldNames, "", tabSelection, "240")
    If ilRC = 0 Then
        cbFiles.Clear
        ssimFiles = ""
        TableName = "SSIM"
        fieldNames = "FILENAMES"
        tabSelection = cbSeason.Text
        ilRC = MainForm.Ufis.CallServer("SSIR", TableName, fieldNames, "", tabSelection, "240")
        If ilRC = 0 Then
            NoOfLines = MainForm.Ufis.GetBufferCount
            For i = 0 To NoOfLines - 1
                cbFiles.AddItem GetItem(MainForm.Ufis.GetBufferLine(i), 1, ",")
                ssimFiles = ssimFiles & MainForm.Ufis.GetBufferLine(i) & vbCrLf
            Next i
        End If
    End If
    Screen.MousePointer = vbArrow

End Sub

Private Sub FillTable(newVpfr As String, newVpto As String)
    Dim NoOfLines As Long
    Dim i As Long
    Dim tmpAirp As String
    Dim tmpAirl As String
    
    prevBufLine = ""
    ignoreLine = False
    readCount = 0
    rejectCount = 0
    ReadCountValue.Text = readCount
    RejectCountValue.Text = rejectCount
    ReadCountValue.Refresh
    RejectCountValue.Refresh
    NoOfLines = MainForm.Ufis.GetBufferCount
    If vpfr <> newVpfr Or vpto <> newVpto Or AirpFilter.Text <> "" Or AirlFilter.Text <> "" Then
        newVpfr = Mid(newVpfr, 7) & Mid(newVpfr, 4, 2) & Mid(newVpfr, 1, 2)
        newVpto = Mid(newVpto, 7) & Mid(newVpto, 4, 2) & Mid(newVpto, 1, 2)
        If Len(AirpFilter.Text) > 0 Then
            tmpAirp = Trim(AirpFilter.Text)
            i = InStr(tmpAirp, ",")
            While i > 0
                Mid(tmpAirp, i, 1) = " "
                i = InStr(tmpAirp, ",")
            Wend
            AirpFilter.Text = Format(tmpAirp, ">")
        End If
        If Len(AirlFilter.Text) > 0 Then
            tmpAirl = Trim(AirlFilter.Text)
            i = InStr(tmpAirl, ",")
            While i > 0
                Mid(tmpAirl, i, 1) = " "
                i = InStr(tmpAirl, ",")
            Wend
            AirlFilter.Text = Format(tmpAirl, ">")
        End If
        nextLineNo = 3
        nextTabLineNo = 0
        lastRecType = "1"
        For i = 1 To NoOfLines - 1
            CheckLine MainForm.Ufis.GetBufferLine(i), newVpfr, newVpto
            readCount = readCount + 1
            ReadCountValue.Text = readCount
            ReadCountValue.Refresh
        Next i
    Else
        For i = 1 To NoOfLines - 1
            TAB1.InsertTextLineAt i - 1, MainForm.Ufis.GetBufferLine(i), False
            readCount = readCount + 1
            ReadCountValue.Text = readCount
            ReadCountValue.Refresh
        Next i
    End If
    
End Sub

Private Sub CheckLine(bufLine As String, curVpfr As String, curVpto As String)
    Dim lineVpfr As String
    Dim lineVpto As String
    Dim conVpfr As String
    Dim conVpto As String
    Dim tmpIncl As Boolean
    Dim tmpAirp1 As String
    Dim tmpAirp2 As String
    Dim tmpAirl As String
    Dim noWords As Integer
    Dim i As Integer
    Dim tmpFilter As String
    Dim noFillLines As Integer
    Dim j As Integer
    Dim savBufLine As String
    Dim valSeqV1 As Integer
    Dim valSeqV2 As Integer
    Dim tmpLine As String
    
    If Mid(bufLine, 1, 1) = "3" Then
        lastRecType = "3"
        lineVpfr = Mid(bufLine, 15, 7)
        lineVpto = Mid(bufLine, 22, 7)
        lineVpfr = "20" & Mid(lineVpfr, 6, 2) & MainForm.GetNumMonth(Mid(lineVpfr, 3, 3)) & Mid(lineVpfr, 1, 2)
        lineVpto = "20" & Mid(lineVpto, 6, 2) & MainForm.GetNumMonth(Mid(lineVpto, 3, 3)) & Mid(lineVpto, 1, 2)
        tmpIncl = True
        If Len(AirpFilter.Text) > 0 Then
            tmpAirp1 = Mid(bufLine, 37, 3)
            tmpAirp2 = Mid(bufLine, 55, 3)
            noWords = MainForm.WordCount(AirpFilter.Text)
            If AirpOptBtn.Caption = "+" Then
                tmpIncl = False
                i = 1
                While i <= noWords And tmpIncl = False
                    tmpFilter = MainForm.GetWord(AirpFilter.Text, i)
                    If tmpAirp1 = tmpFilter Or tmpAirp2 = tmpFilter Then
                        tmpIncl = True
                    End If
                    i = i + 1
                Wend
            Else
                i = 1
                While i <= noWords And tmpIncl = True
                    tmpFilter = MainForm.GetWord(AirpFilter.Text, i)
                    If tmpAirp1 = tmpFilter Or tmpAirp2 = tmpFilter Then
                        tmpIncl = False
                    End If
                    i = i + 1
                Wend
            End If
        End If
        If Len(AirlFilter.Text) > 0 And tmpIncl = True Then
            tmpAirl = Trim(Mid(bufLine, 3, 3))
            noWords = MainForm.WordCount(AirlFilter.Text)
            If AirlOptBtn.Caption = "+" Then
                tmpIncl = False
                i = 1
                While i <= noWords And tmpIncl = False
                    tmpFilter = MainForm.GetWord(AirlFilter.Text, i)
                    If tmpAirl = tmpFilter Then
                        tmpIncl = True
                    End If
                    i = i + 1
                Wend
            Else
                i = 1
                While i <= noWords And tmpIncl = True
                    tmpFilter = MainForm.GetWord(AirlFilter.Text, i)
                    If tmpAirl = tmpFilter Then
                        tmpIncl = False
                    End If
                    i = i + 1
                Wend
            End If
        End If
        If tmpIncl = True Then
            If Val(curVpto) < Val(lineVpfr) Or Val(curVpfr) > Val(lineVpto) Then
                emptyFreq = True
            Else
                If Val(lineVpfr) < Val(curVpfr) Then
                    conVpfr = Mid(curVpfr, 7, 2) & MainForm.GetAscMonth(Mid(curVpfr, 5, 2)) & Mid(curVpfr, 3, 2)
                    bufLine = Mid(bufLine, 1, 14) & conVpfr & Mid(bufLine, 22)
                End If
                If Val(lineVpto) > Val(curVpto) Then
                    conVpto = Mid(curVpto, 7, 2) & MainForm.GetAscMonth(Mid(curVpto, 5, 2)) & Mid(curVpto, 3, 2)
                    bufLine = Mid(bufLine, 1, 21) & conVpto & Mid(bufLine, 29)
                End If
                bufLine = CompressFreq(bufLine)
            End If
            If emptyFreq = True Then
                rejectCount = rejectCount + 1
                RejectCountValue.Text = rejectCount
                RejectCountValue.Refresh
                rowCurBuf = TAB1.GetLineCount - 1
                rowPrevBuf = rowCurBuf - 1
                bufLine = TAB1.GetLineValues(rowCurBuf)
                i = InStr(bufLine, ",")
                If i > 0 Then
                    bufLine = Mid(bufLine, 1, i - 1)
                End If
                prevBufLine = TAB1.GetLineValues(rowPrevBuf)
                i = InStr(prevBufLine, ",")
                If i > 0 Then
                    prevBufLine = Mid(prevBufLine, 1, i - 1)
                End If
                valSeqV1 = Val(Mid(prevBufLine, 12, 2))
                valSeqV2 = Val(Mid(bufLine, 12, 2))
                While valSeqV1 <> valSeqV2 And Mid(prevBufLine, 1, 1) = "3"
                    rowPrevBuf = rowPrevBuf - 1
                    prevBufLine = TAB1.GetLineValues(rowPrevBuf)
                    i = InStr(prevBufLine, ",")
                    If i > 0 Then
                        prevBufLine = Mid(prevBufLine, 1, i - 1)
                    End If
                    valSeqV1 = Val(Mid(prevBufLine, 12, 2))
                Wend
                If Len(prevBufLine) > 37 And Mid(prevBufLine, 1, 1) = "3" And _
                   Mid(prevBufLine, 3, 7) = Mid(bufLine, 3, 7) And _
                   Mid(prevBufLine, 14, 1) = Mid(bufLine, 14, 1) And _
                   Mid(prevBufLine, 37, 46) = Mid(bufLine, 37, 46) Then
                    savBufLine = bufLine
                    bufLine = CompressBuffer(prevBufLine, bufLine, 2)
                    If bufLine <> savBufLine Then
                        TAB1.DeleteLine rowPrevBuf
                        bufLine = Mid(bufLine, 1, 194) & MainForm.GetNumFormatted(rowPrevBuf - 7, 6)
                        bufLine = GenerateSequence(bufLine, rowPrevBuf)
                        TAB1.InsertTextLineAt rowPrevBuf, bufLine, False
                        rowCurBuf = TAB1.GetLineCount
                        rowPrevBuf = rowCurBuf - 1
                    End If
                End If
                rowCurBuf = TAB1.GetLineCount
                rowPrevBuf = rowCurBuf - 1
                prevBufLine = TAB1.GetLineValues(rowPrevBuf)
                i = InStr(prevBufLine, ",")
                If i > 0 Then
                    prevBufLine = Mid(prevBufLine, 1, i - 1)
                End If
                ignoreLine = True
            Else
                bufLine = CompressPeriod(bufLine)
                rowCurBuf = TAB1.GetLineCount
                rowPrevBuf = rowCurBuf - 1
                If ignoreLine = True And Len(prevBufLine) > 37 And Mid(prevBufLine, 1, 1) = "3" Then
                    valSeqV1 = Val(Mid(prevBufLine, 12, 2))
                    valSeqV2 = Val(Mid(bufLine, 12, 2))
                    While valSeqV1 <> valSeqV2 And Mid(prevBufLine, 1, 1) = "3"
                        rowPrevBuf = rowPrevBuf - 1
                        prevBufLine = TAB1.GetLineValues(rowPrevBuf)
                        i = InStr(prevBufLine, ",")
                        If i > 0 Then
                            prevBufLine = Mid(prevBufLine, 1, i - 1)
                        End If
                        valSeqV1 = Val(Mid(prevBufLine, 12, 2))
                    Wend
                    If Mid(prevBufLine, 3, 7) = Mid(bufLine, 3, 7) And _
                       Mid(prevBufLine, 14, 1) = Mid(bufLine, 14, 1) And _
                       Mid(prevBufLine, 37, 46) = Mid(bufLine, 37, 46) Then
                        bufLine = CompressBuffer(prevBufLine, bufLine, 1)
                    Else
                        ignoreLine = False
                    End If
                Else
                    ignoreLine = False
                End If
                bufLine = Mid(bufLine, 1, 194) & MainForm.GetNumFormatted(rowCurBuf - 7, 6)
                bufLine = GenerateSequence(bufLine, rowCurBuf)
                TAB1.InsertTextLineAt rowCurBuf, bufLine, False
                rowCurBuf = TAB1.GetLineCount
                rowPrevBuf = rowCurBuf - 1
                prevBufLine = TAB1.GetLineValues(rowPrevBuf)
                i = InStr(prevBufLine, ",")
                If i > 0 Then
                    prevBufLine = Mid(prevBufLine, 1, i - 1)
                End If
            End If
        Else
            rejectCount = rejectCount + 1
            RejectCountValue.Text = rejectCount
            RejectCountValue.Refresh
        End If
        nextTabLineNo = TAB1.GetLineCount
        nextLineNo = nextTabLineNo - 7
    Else
        If Mid(bufLine, 1, 1) = "2" Then
            lastRecType = "2"
            conVpfr = Mid(curVpfr, 7, 2) & MainForm.GetAscMonth(Mid(curVpfr, 5, 2)) & Mid(curVpfr, 3, 2)
            conVpto = Mid(curVpto, 7, 2) & MainForm.GetAscMonth(Mid(curVpto, 5, 2)) & Mid(curVpto, 3, 2)
            bufLine = Mid(bufLine, 1, 14) & conVpfr & conVpto & Mid(bufLine, 29)
            TAB1.InsertTextLineAt nextTabLineNo, bufLine, False
            nextTabLineNo = nextTabLineNo + 1
        Else
            If Mid(bufLine, 1, 1) = "5" Then
                If lastRecType = "3" Then
                    noFillLines = nextLineNo - 3
                    j = noFillLines \ 5
                    noFillLines = noFillLines - (j * 5)
                    noFillLines = 5 - noFillLines
                    If noFillLines < 5 Then
                        tmpLine = MainForm.GetNumFormatted(0, 200)
                        For j = 1 To noFillLines
                            TAB1.InsertTextLineAt nextTabLineNo, tmpLine, False
                            nextTabLineNo = nextTabLineNo + 1
                        Next j
                    End If
                End If
                lastRecType = "5"
                bufLine = Mid(bufLine, 1, 187) & MainForm.GetNumFormatted(nextLineNo, 6) & "E" & MainForm.GetNumFormatted(nextLineNo + 1, 6)
                TAB1.InsertTextLineAt nextTabLineNo, bufLine, False
                nextTabLineNo = nextTabLineNo + 1
            Else
                If lastRecType = "3" Then
                    lastRecType = "0"
                    noFillLines = nextLineNo - 3
                    j = noFillLines \ 5
                    noFillLines = noFillLines - (j * 5)
                    noFillLines = 5 - noFillLines
                    If noFillLines < 5 Then
                        bufLine = MainForm.GetNumFormatted(0, 200)
                        For j = 1 To noFillLines
                            TAB1.InsertTextLineAt nextTabLineNo, bufLine, False
                            nextTabLineNo = nextTabLineNo + 1
                        Next j
                    End If
                End If
                If lastRecType <> "0" Then
                    TAB1.InsertTextLineAt nextTabLineNo, bufLine, False
                    nextTabLineNo = nextTabLineNo + 1
                End If
            End If
        End If
    End If
    
End Sub

Function CompressFreq(curLine As String) As String
    Dim tmpVpfr As String
    Dim tmpVpto As String
    Dim oldDoop As String
    Dim newDoop As String
    Dim tmpWeek As String
    Dim tmpDay As Long
    Dim i As Integer
    
    emptyFreq = False
    tmpVpfr = Mid(curLine, 15, 7)
    tmpVpto = Mid(curLine, 22, 7)
    tmpVpfr = "20" & Mid(tmpVpfr, 6, 2) & MainForm.GetNumMonth(Mid(tmpVpfr, 3, 3)) & Mid(tmpVpfr, 1, 2)
    tmpVpto = "20" & Mid(tmpVpto, 6, 2) & MainForm.GetNumMonth(Mid(tmpVpto, 3, 3)) & Mid(tmpVpto, 1, 2)
    If Val(tmpVpto) - Val(tmpVpfr) < 6 Then
        oldDoop = Mid(curLine, 29, 7)
        newDoop = "       "
        For tmpDay = Val(tmpVpfr) To Val(tmpVpto)
            tmpWeek = Weekday(CedaDateToVb(CStr(tmpDay)), vbMonday)
            i = InStr(oldDoop, tmpWeek)
            If i > 0 Then
                If i = 1 Then
                    newDoop = CStr(i) & Mid(newDoop, 2)
                Else
                    If i = 7 Then
                        newDoop = Mid(newDoop, 1, 6) & CStr(i)
                    Else
                        newDoop = Mid(newDoop, 1, i - 1) & CStr(i) & Mid(newDoop, i + 1)
                    End If
                End If
            End If
        Next tmpDay
        curLine = Mid(curLine, 1, 28) & newDoop & Mid(curLine, 36)
        If newDoop = "       " Then
            emptyFreq = True
        End If
    End If
                
    CompressFreq = curLine
End Function

Function CompressPeriod(curLine As String) As String
    Dim tmpVpfr As String
    Dim tmpVpto As String
    Dim newDoop As String
    Dim tmpWeek As String
    Dim i As Integer
    
    tmpVpfr = Mid(curLine, 15, 7)
    tmpVpfr = "20" & Mid(tmpVpfr, 6, 2) & MainForm.GetNumMonth(Mid(tmpVpfr, 3, 3)) & Mid(tmpVpfr, 1, 2)
    newDoop = Mid(curLine, 29, 7)
    tmpWeek = Weekday(CedaDateToVb(tmpVpfr), vbMonday)
    i = InStr(newDoop, tmpWeek)
    While i <= 0
        tmpVpfr = DateAdd("n", 1440, CedaDateToVb(tmpVpfr))
        tmpVpfr = "20" & Mid(tmpVpfr, 7) & Mid(tmpVpfr, 4, 2) & Mid(tmpVpfr, 1, 2)
        tmpWeek = Weekday(CedaDateToVb(tmpVpfr), vbMonday)
        i = InStr(newDoop, tmpWeek)
    Wend
    tmpVpto = Mid(curLine, 22, 7)
    tmpVpto = "20" & Mid(tmpVpto, 6, 2) & MainForm.GetNumMonth(Mid(tmpVpto, 3, 3)) & Mid(tmpVpto, 1, 2)
    newDoop = Mid(curLine, 29, 7)
    tmpWeek = Weekday(CedaDateToVb(tmpVpto), vbMonday)
    i = InStr(newDoop, tmpWeek)
    While i <= 0
        tmpVpto = DateAdd("n", -1440, CedaDateToVb(tmpVpto))
        tmpVpto = "20" & Mid(tmpVpto, 7) & Mid(tmpVpto, 4, 2) & Mid(tmpVpto, 1, 2)
        tmpWeek = Weekday(CedaDateToVb(tmpVpto), vbMonday)
        i = InStr(newDoop, tmpWeek)
    Wend
    tmpVpfr = Mid(tmpVpfr, 7, 2) & MainForm.GetAscMonth(Mid(tmpVpfr, 5, 2)) & Mid(tmpVpfr, 3, 2)
    tmpVpto = Mid(tmpVpto, 7, 2) & MainForm.GetAscMonth(Mid(tmpVpto, 5, 2)) & Mid(tmpVpto, 3, 2)
    curLine = Mid(curLine, 1, 14) & tmpVpfr & tmpVpto & Mid(curLine, 29)
    
    CompressPeriod = curLine
End Function

Function CompressBuffer(curLine1 As String, curLine2 As String, mytype As Integer) As String
    Dim tmpVpfr As String
    Dim tmpVpto As String
    Dim oldDoop1 As String
    Dim oldDoop2 As String
    Dim newDoop As String
    Dim tmpWeek As String
    Dim tmpDay As Long
    Dim i As Integer
    Dim tmpBuf As String
    
    ignoreLine = False
    tmpVpto = Mid(curLine1, 22, 7)
    tmpVpfr = Mid(curLine2, 15, 7)
    tmpVpto = "20" & Mid(tmpVpto, 6, 2) & MainForm.GetNumMonth(Mid(tmpVpto, 3, 3)) & Mid(tmpVpto, 1, 2)
    tmpVpfr = "20" & Mid(tmpVpfr, 6, 2) & MainForm.GetNumMonth(Mid(tmpVpfr, 3, 3)) & Mid(tmpVpfr, 1, 2)
    If Val(tmpVpfr) - Val(tmpVpto) <= 1 Then
        tmpVpfr = Mid(curLine1, 15, 7)
        tmpVpto = Mid(curLine2, 22, 7)
        tmpVpfr = "20" & Mid(tmpVpfr, 6, 2) & MainForm.GetNumMonth(Mid(tmpVpfr, 3, 3)) & Mid(tmpVpfr, 1, 2)
        tmpVpto = "20" & Mid(tmpVpto, 6, 2) & MainForm.GetNumMonth(Mid(tmpVpto, 3, 3)) & Mid(tmpVpto, 1, 2)
        oldDoop1 = Mid(curLine1, 29, 7)
        oldDoop2 = Mid(curLine2, 29, 7)
        newDoop = "       "
        For tmpDay = Val(tmpVpfr) To Val(tmpVpto)
            tmpWeek = Weekday(CedaDateToVb(CStr(tmpDay)), vbMonday)
            i = InStr(oldDoop1, tmpWeek)
            If i <= 0 Then
                i = InStr(oldDoop2, tmpWeek)
            End If
            If i > 0 Then
                If i = 1 Then
                    newDoop = CStr(i) & Mid(newDoop, 2)
                Else
                    If i = 7 Then
                        newDoop = Mid(newDoop, 1, 6) & CStr(i)
                    Else
                        newDoop = Mid(newDoop, 1, i - 1) & CStr(i) & Mid(newDoop, i + 1)
                    End If
                End If
            End If
        Next tmpDay
        curLine2 = Mid(curLine2, 1, 28) & newDoop & Mid(curLine2, 36)
        tmpVpfr = Mid(tmpVpfr, 7, 2) & MainForm.GetAscMonth(Mid(tmpVpfr, 5, 2)) & Mid(tmpVpfr, 3, 2)
        tmpVpto = Mid(tmpVpto, 7, 2) & MainForm.GetAscMonth(Mid(tmpVpto, 5, 2)) & Mid(tmpVpto, 3, 2)
        curLine2 = Mid(curLine2, 1, 14) & tmpVpfr & tmpVpto & Mid(curLine2, 29)
        If mytype = 1 Then
            rowCurBuf = rowPrevBuf
            TAB1.DeleteLine rowCurBuf
        Else
            TAB1.DeleteLine rowCurBuf
        End If
        ignoreLine = True
    End If
    
    CompressBuffer = curLine2
End Function

Function GenerateSequence(curLine As String, curBufLine As Integer) As String
    Dim tmpBuf As String
    Dim strSeq As String
    Dim valSeq As Long
    Dim tmpIdx As Integer
    Dim valSeqV1 As String
    Dim valSeqV2 As String
    
    tmpIdx = curBufLine - 1
    tmpBuf = TAB1.GetLineValues(tmpIdx)
    valSeqV1 = Val(Mid(tmpBuf, 12, 2))
    valSeqV2 = Val(Mid(curLine, 12, 2))
    While valSeqV1 <> valSeqV2 And Mid(tmpBuf, 1, 1) = "3"
        tmpIdx = tmpIdx - 1
        tmpBuf = TAB1.GetLineValues(tmpIdx)
        valSeqV1 = Val(Mid(tmpBuf, 12, 2))
    Wend
    strSeq = Mid(tmpBuf, 10, 2)
    If Mid(tmpBuf, 3, 7) = Mid(curLine, 3, 7) Then
        valSeq = Val(strSeq) + 1
    Else
        valSeq = 1
    End If
    strSeq = MainForm.GetNumFormatted(valSeq, 2)
    curLine = Mid(curLine, 1, 9) & strSeq & Mid(curLine, 12)
    
    GenerateSequence = curLine
End Function

