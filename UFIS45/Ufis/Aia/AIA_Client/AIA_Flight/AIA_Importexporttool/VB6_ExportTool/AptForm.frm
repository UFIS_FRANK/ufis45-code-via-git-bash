VERSION 5.00
Object = "{64E8E382-05E2-11D2-9B1D-9B0630BC8F12}#1.0#0"; "Tab.ocx"
Begin VB.Form AptForm 
   Caption         =   "Airport Table"
   ClientHeight    =   9405
   ClientLeft      =   60
   ClientTop       =   1590
   ClientWidth     =   15210
   LinkTopic       =   "Form1"
   ScaleHeight     =   9405
   ScaleWidth      =   15210
   Begin VB.CommandButton ReadBtn 
      Caption         =   "Read Table"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   120
      TabIndex        =   0
      Top             =   240
      Width           =   1215
   End
   Begin VB.CommandButton UpdateBtn 
      Caption         =   "Update Table"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   1440
      TabIndex        =   1
      Top             =   240
      Width           =   1215
   End
   Begin VB.CommandButton CloseBtn 
      Caption         =   "&Close"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   13320
      TabIndex        =   2
      Top             =   240
      Width           =   1400
   End
   Begin VB.Frame Frame1 
      Height          =   690
      Left            =   0
      TabIndex        =   3
      Top             =   0
      Width           =   14955
   End
   Begin TABLib.TAB TAB1 
      Height          =   7575
      Left            =   0
      TabIndex        =   4
      TabStop         =   0   'False
      Top             =   1200
      Width           =   14925
      _Version        =   65536
      _ExtentX        =   26326
      _ExtentY        =   13361
      _StockProps     =   0
   End
   Begin VB.Label TableName 
      Caption         =   "Table Name"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   0
      TabIndex        =   5
      Top             =   840
      Width           =   5535
   End
End
Attribute VB_Name = "AptForm"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
    Dim curTable As String
    Dim tabSelection As String
    Dim fieldNames As String
    Dim fieldLables As String
    Dim fieldLength As String
    Dim headerLenStr As String
    Dim allowInsert As Boolean
    Dim allowDelete As Boolean
    Dim ilRC As Integer

Private Sub Form_Load()
    Dim NoOfFields As Integer
    Dim i As Integer
    Dim columnWidth As Integer
    Dim tabData As String
    Dim NoOfLines As Integer
    
    TAB1.LineHeight = 18
    TAB1.FontName = "Courier New"
    TAB1.HeaderFontSize = 17
    TAB1.FontSize = 17
    TAB1.SetTabFontBold True
    TAB1.EnableHeaderSizing True
    TAB1.ShowHorzScroller True
    TAB1.EnableInlineEdit True
    TAB1.InplaceEditUpperCase = False
    TAB1.EmptyAreaRightColor = 12632256
    TAB1.Visible = False

    AptForm.Caption = MainForm.GetText("AptForm", "AptForm", 1)
    ReadBtn.Caption = MainForm.GetText("AptForm", "ReadBtn", 1)
    UpdateBtn.Caption = MainForm.GetText("AptForm", "UpdateBtn", 1)
    CloseBtn.Caption = MainForm.GetText("AptForm", "CloseBtn", 1)
    
    TableName.Caption = MainForm.GetText("AptForm", "AptForm", 1)
    
    allowInsert = False
    allowDelete = False
    Screen.MousePointer = vbHourglass
    curTable = "APT"
    fieldNames = "URNO,APC3,APC4,APFN,TZON,TDIS,TDIW"
    tabSelection = "APC3,APC4"
    If tabSelection <> "" Then
        tabSelection = "ORDER BY " & tabSelection
    End If
    fieldLength = "10,3,4,32,8,4,4"
    fieldLables = MainForm.GetText("AptForm", "TableHeader", 1)
    i = InStr(fieldLables, ";")
    While i > 0
        fieldLables = Mid(fieldLables, 1, i - 1) & "," & Mid(fieldLables, i + 1)
        i = InStr(fieldLables, ";")
    Wend
    NoOfFields = ItemCount(fieldNames, ",")
    For i = 1 To NoOfFields
        If i = 1 Then
            headerLenStr = "0"
        Else
            columnWidth = Val(GetItem(fieldLength, i, ","))
            If columnWidth > 64 Then
                columnWidth = 64
            End If
            columnWidth = TextWidth(Space(columnWidth)) / Screen.TwipsPerPixelX * 3 + 24
            headerLenStr = headerLenStr & "," & columnWidth
        End If
    Next i
    TAB1.InplaceEditUpperCase = True
    TAB1.ResetContent
    MainForm.InitTabForCedaConnection TAB1
    TAB1.HeaderString = fieldLables
    TAB1.HeaderLengthString = headerLenStr
    curTable = curTable & MainForm.tableExt
    tabData = ""
    If MainForm.cdrhdlType = "YES" Then
        ilRC = TAB1.CedaAction("RT", curTable, fieldNames, tabData, tabSelection)
    Else
        ilRC = MainForm.Ufis.CallServer("RT", curTable, fieldNames, "", tabSelection, "240")
        If ilRC = 0 Then
            NoOfLines = MainForm.Ufis.GetBufferCount
            For i = 0 To NoOfLines - 1
                TAB1.InsertTextLineAt i, MainForm.Ufis.GetBufferLine(i), False
            Next i
        End If
    End If
    TAB1.NoFocusColumns = "0,1,2,3"
    TAB1.Visible = True
    TAB1.RedrawTab
    Screen.MousePointer = vbArrow
End Sub

Private Sub CloseBtn_Click()
    AptForm.Visible = False
    Unload AptForm
End Sub

Private Sub ReadBtn_Click()
    Dim tabData As String
    Dim NoOfLines As Integer
    Dim i As Integer
    
    Screen.MousePointer = vbHourglass
    TAB1.ResetContent
    TAB1.HeaderString = fieldLables
    TAB1.HeaderLengthString = headerLenStr
    tabData = ""
    If MainForm.cdrhdlType = "YES" Then
        ilRC = TAB1.CedaAction("RT", curTable, fieldNames, tabData, tabSelection)
    Else
        ilRC = MainForm.Ufis.CallServer("RT", curTable, fieldNames, "", tabSelection, "240")
        If ilRC = 0 Then
            NoOfLines = MainForm.Ufis.GetBufferCount
            For i = 0 To NoOfLines - 1
                TAB1.InsertTextLineAt i, MainForm.Ufis.GetBufferLine(i), False
            Next i
        End If
    End If
    TAB1.NoFocusColumns = "0,1,2,3"
    TAB1.RedrawTab
    Screen.MousePointer = vbArrow
End Sub

Private Sub TAB1_InplaceEditCell(ByVal lineNo As Long, ByVal colNo As Long, ByVal NewValue As String, ByVal OldValue As String)
    Dim llLineStatus As Long
    Dim tabData As String
    Dim tmpLine As String
    
    If NewValue <> OldValue Then
        llLineStatus = TAB1.GetLineStatusValue(lineNo)
        Select Case llLineStatus
            Case 2              'deleted line
                TAB1.SetColumnValue lineNo, colNo, OldValue
            Case 4              'inserted line
                tmpLine = TAB1.GetLineValues(TAB1.GetLineCount - 1)
                If Len(tmpLine) >= ItemCount(tmpLine, ",") Then
                    tabData = ""
                    TAB1.InsertTextLine tabData, True
                    TAB1.SetLineStatusValue TAB1.GetLineCount - 1, 4
                    TAB1.SetLineColor TAB1.GetLineCount - 1, vbBlack, 12640511
                End If
            Case 0
                TAB1.SetLineStatusValue lineNo, 1
                TAB1.SetLineColor lineNo, vbBlack, 12648384
                TAB1.RedrawTab
        End Select
        TAB1.SetCurrentSelection -1
    End If
End Sub

Private Sub UpdateBtn_Click()
    Dim updatedLines As String
    Dim insertedLines As String
    Dim deletedLines As String
    Dim updFieldList As String
    Dim updFieldData As String
    Dim updSelection As String
    Dim curLine As String
    Dim tmpDataItem As String
    Dim tmpItemLength As String
    Dim emptyLine As Boolean
    Dim i As Integer
    Dim j As Integer
    
    Screen.MousePointer = vbHourglass
    updatedLines = TAB1.GetLinesByStatusValue(1, 1)
    If Len(updatedLines) > 0 Then
        'building the update-fieldlist
        updFieldList = fieldNames
        i = InStr(updFieldList, ",")
        updFieldList = Mid(updFieldList, i + 1)
        updFieldList = updFieldList & ",LSTU,USEU"
        'loop thru all updated lines
        For i = 1 To ItemCount(updatedLines, ",")
            curLine = GetItem(updatedLines, i, ",")
            'building the update-string
            updFieldData = ""
            For j = 2 To ItemCount(fieldNames, ",")
                tmpDataItem = TAB1.GetColumnValue(curLine, j - 1)
                tmpItemLength = GetItem(fieldLength, j, ",")
                If Len(tmpDataItem) > tmpItemLength Then
                    tmpDataItem = Left(tmpDataItem, tmpItemLength)
                Else
                    If Len(tmpDataItem) = 0 Then
                        tmpDataItem = " "
                    End If
                End If
                If j = 5 Then
                    tmpDataItem = Format(tmpDataItem, ">")
                End If
                updFieldData = updFieldData & tmpDataItem & ","
            Next j
            updFieldData = updFieldData & GetTimeStamp(0) & ",Export"
            'building the where-statement
            updSelection = "WHERE URNO = " & TAB1.GetColumnValue(curLine, 0)
            'do the update
'MsgBox (curTable & vbCrLf & updFieldList & vbCrLf & updFieldData & vbCrLf & updSelection)
            ilRC = MainForm.Ufis.CallServer("URT", curTable, updFieldList, updFieldData, updSelection, "240")
            'reset line-status & colors
            If ilRC = 0 Then
                TAB1.SetLineStatusValue curLine, 0
                TAB1.SetLineColor curLine, vbBlack, vbWhite
                TAB1.RedrawTab
            Else
                MsgBox MainForm.GetText("AptForm", "MsgBox1", 1) & ilRC & MainForm.GetText("AptForm", "MsgBox1", 2), vbCritical, MainForm.GetText("AptForm", "MsgBox1", 3)
            End If
        Next i
    End If
    
    If allowInsert = True Then
        insertedLines = TAB1.GetLinesByStatusValue(4, 1)
        If Len(insertedLines) > 0 Then
            updFieldList = fieldNames
            i = InStr(updFieldList, ",")
            updFieldList = Mid(updFieldList, i + 1)
            updFieldList = updFieldList & ",CDAT,USEC"
            'loop thru all inserted lines
            emptyLine = False
            For i = 1 To ItemCount(insertedLines, ",")
                curLine = GetItem(insertedLines, i, ",")
                'building the insert-string
                updFieldData = ""
                For j = 2 To ItemCount(fieldNames, ",")
                    tmpDataItem = TAB1.GetColumnValue(curLine, j - 1)
                    If Len(tmpDataItem) <= 0 Then
                        tmpDataItem = " "
                    Else
                        tmpItemLength = GetItem(fieldLength, j, ",")
                        If Len(tmpDataItem) > tmpItemLength Then
                            tmpDataItem = Left(tmpDataItem, tmpItemLength)
                        End If
                    End If
                    updFieldData = updFieldData & tmpDataItem & ","
                Next j
                updFieldData = updFieldData & GetTimeStamp(0) & ",Export"
                'building the where-statement
                updSelection = ""
                'do the update
                If emptyLine = False Then
'MsgBox (curTable & vbCrLf & updFieldList & vbCrLf & updFieldData & vbCrLf & updSelection)
                    ilRC = MainForm.Ufis.CallServer("IBT", curTable, updFieldList, updFieldData, updSelection, "240")
                    'reset line-status & colors
                    If ilRC = 0 Then
                        tmpDataItem = GetItem(MainForm.Ufis.GetBufferLine(0), 1, ",")
                        TAB1.SetColumnValue curLine, 0, tmpDataItem
                        TAB1.SetLineStatusValue curLine, 0
                        TAB1.SetLineColor curLine, vbBlack, vbWhite
                        TAB1.RedrawTab
                    Else
                        MsgBox MainForm.GetText("AptForm", "MsgBox2", 1) & ilRC & MainForm.GetText("AptForm", "MsgBox2", 2), vbCritical, MainForm.GetText("AptForm", "MsgBox2", 3)
                    End If
                End If
            Next i
        End If
    End If
    
    If allowDelete = True Then
        deletedLines = TAB1.GetLinesByStatusValue(2, 1)
        If Len(deletedLines) > 0 Then
            Select Case MsgBox(MainForm.GetText("AptForm", "MsgBox3", 1) & " " & ItemCount(deletedLines, ",") & MainForm.GetText("AptForm", "MsgBox3", 2), vbOKCancel, MainForm.GetText("AptForm", "MsgBox3", 3))
                Case vbCancel
                    Exit Sub
                Case Else
                    'just continue
            End Select
            updFieldList = ""
            updFieldData = ""
            updSelection = ""
            'collect all urnos
            For i = 1 To ItemCount(deletedLines, ",")
                curLine = GetItem(deletedLines, i, ",")
                tmpDataItem = TAB1.GetColumnValue(curLine, 0)
                If Len(tmpDataItem) > 0 Then
                    updSelection = updSelection & tmpDataItem & ","
                End If
            Next i
            If Len(updSelection) > 0 Then
                updSelection = Left(updSelection, Len(updSelection) - 1)
                updSelection = "WHERE URNO IN (" & updSelection & ")"
                'do the delete
'MsgBox (curTable & vbCrLf & updFieldList & vbCrLf & updFieldData & vbCrLf & updSelection)
                ilRC = MainForm.Ufis.CallServer("DRT", curTable, updFieldList, updFieldData, updSelection, "240")
                'delete rows from tab
                If ilRC = 0 Then
                    For i = ItemCount(deletedLines, ",") To 1 Step -1
                        curLine = GetItem(deletedLines, i, ",")
                        TAB1.DeleteLine curLine
                    Next i
                    TAB1.RedrawTab
                Else
                    MsgBox MainForm.GetText("AptForm", "MsgBox4", 1) & ilRC & MainForm.GetText("AptForm", "MsgBox4", 2), vbCritical, MainForm.GetText("AptForm", "MsgBox4", 3)
                End If
            End If
        End If
    End If
    
    Screen.MousePointer = vbArrow
End Sub

Private Sub TAB1_SendRButtonClick(ByVal lineNo As Long, ByVal colNo As Long)
    Dim statusValue As Integer
    
    statusValue = TAB1.GetLineStatusValue(lineNo)
    Select Case statusValue
        Case 1
            TAB1.SetLineStatusValue lineNo, 0
            TAB1.SetLineColor lineNo, vbBlack, vbWhite
            TAB1.SetCurrentSelection -1
        Case 4
            TAB1.SetLineStatusValue lineNo, 0
            TAB1.SetLineColor lineNo, vbBlack, vbWhite
            TAB1.SetCurrentSelection -1
        Case 2
            TAB1.SetLineStatusValue lineNo, 0
            TAB1.SetLineColor lineNo, vbBlack, vbWhite
            TAB1.SetCurrentSelection -1
        Case Else
            If allowDelete = True Then
                TAB1.SetLineStatusValue lineNo, 2
                TAB1.SetLineColor lineNo, vbBlack, 255
                TAB1.SetCurrentSelection -1
            End If
    End Select
End Sub
