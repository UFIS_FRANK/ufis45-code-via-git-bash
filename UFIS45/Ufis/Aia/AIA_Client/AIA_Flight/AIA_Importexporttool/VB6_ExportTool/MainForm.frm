VERSION 5.00
Object = "{64E8E382-05E2-11D2-9B1D-9B0630BC8F12}#1.0#0"; "Tab.ocx"
Object = "{A2F31E92-C74F-11D3-A251-00500437F607}#1.0#0"; "UfisCom.ocx"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Begin VB.Form MainForm 
   Caption         =   "Ufis�"
   ClientHeight    =   10650
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   15210
   BeginProperty Font 
      Name            =   "Courier New"
      Size            =   11.25
      Charset         =   0
      Weight          =   700
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "MainForm.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   710
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   1014
   Begin VB.CommandButton AirportTabBtn 
      Caption         =   "&Airports"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   6120
      TabIndex        =   4
      Top             =   360
      Width           =   1400
   End
   Begin VB.CommandButton FlukoTabBtn 
      Caption         =   "&FHK Export"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   4680
      TabIndex        =   3
      Top             =   360
      Width           =   1400
   End
   Begin VB.CommandButton SlotcoTabBtn 
      Caption         =   "&SLOTCO Export"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   3240
      TabIndex        =   2
      Top             =   360
      Width           =   1400
   End
   Begin VB.CommandButton TtbTabBtn 
      Caption         =   "&Ttb Export"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   1800
      TabIndex        =   1
      Top             =   360
      Width           =   1400
   End
   Begin VB.CommandButton ReadBtn 
      Caption         =   "Read"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   12360
      TabIndex        =   7
      Top             =   360
      Width           =   850
   End
   Begin VB.TextBox InFileName 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   345
      Left            =   8760
      TabIndex        =   6
      Top             =   360
      Width           =   3495
   End
   Begin VB.CommandButton FileNameBtn 
      Caption         =   "File Name"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   7680
      TabIndex        =   5
      Top             =   360
      Width           =   1125
   End
   Begin MSComDlg.CommonDialog CommonDialog1 
      Left            =   3960
      Top             =   840
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.CommandButton SsimTabBtn 
      Caption         =   "&Ssim Export"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   360
      TabIndex        =   0
      Top             =   360
      Width           =   1400
   End
   Begin UFISCOMLib.UfisCom Ufis 
      Left            =   3120
      Top             =   840
      _Version        =   65536
      _ExtentX        =   1085
      _ExtentY        =   661
      _StockProps     =   0
   End
   Begin VB.Frame Frame1 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   690
      Left            =   225
      TabIndex        =   10
      Top             =   135
      Width           =   14715
      Begin VB.CommandButton ExitBtn 
         Caption         =   "&Exit"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   13080
         TabIndex        =   8
         Top             =   215
         Width           =   1400
      End
   End
   Begin TABLib.TAB TAB1 
      Height          =   8415
      Left            =   0
      TabIndex        =   9
      TabStop         =   0   'False
      Top             =   1440
      Width           =   14925
      _Version        =   65536
      _ExtentX        =   26326
      _ExtentY        =   14843
      _StockProps     =   0
   End
End
Attribute VB_Name = "MainForm"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public tableExt As String
Public HomeAirport As String
Public hostName As String
Public hostType As String
Public myLanguage As String
Public cdrhdlType As String
Public textPath As String

Dim ilRC As Integer

Private Sub Form_Load()

    TAB1.LineHeight = 18
    TAB1.FontName = "Courier New"
    TAB1.HeaderFontSize = 17
    TAB1.FontSize = 17
    TAB1.SetTabFontBold True
    TAB1.EnableHeaderSizing True
    TAB1.ShowHorzScroller True
    TAB1.EnableInlineEdit False
    TAB1.InplaceEditUpperCase = False
    TAB1.EmptyAreaRightColor = 12632256
    TAB1.Visible = False
    
    myLanguage = GetIniEntry("c:\ufis\system\ceda.ini", "EXPORT", "", "LANGUAGE", "1")
    HomeAirport = GetIniEntry("c:\ufis\system\ceda.ini", "GLOBAL", "", "HOMEAIRPORT", "ABC")
    tableExt = GetIniEntry("c:\ufis\system\ceda.ini", "GLOBAL", "", "TABLEEXT", "TAB")
    hostName = GetIniEntry("c:\ufis\system\ceda.ini", "EXPORT", "", "HOSTNAME", "ABC")
    hostType = GetIniEntry("c:\ufis\system\ceda.ini", "EXPORT", "", "HOSTTYPE", "XXX")
    cdrhdlType = GetIniEntry("c:\ufis\system\ceda.ini", "EXPORT", "", "CDRHDL", "XXX")
    textPath = GetIniEntry("c:\ufis\system\ceda.ini", "EXPORT", "", "TextPath", "")
    
    MainForm.Caption = MainForm.Caption & " " & GetText("MainForm", "MainForm", 1)
    SsimTabBtn.Caption = GetText("MainForm", "SsimTabBtn", 1)
    TtbTabBtn.Caption = GetText("MainForm", "TtbTabBtn", 1)
    SlotcoTabBtn.Caption = GetText("MainForm", "SlotcoTabBtn", 1)
    FlukoTabBtn.Caption = GetText("MainForm", "FlukoTabBtn", 1)
    AirportTabBtn.Caption = GetText("MainForm", "AirportTabBtn", 1)
    FileNameBtn.Caption = MainForm.GetText("MainForm", "FileNameBtn", 1)
    ReadBtn.Caption = MainForm.GetText("MainForm", "ReadBtn", 1)
    ExitBtn.Caption = GetText("MainForm", "ExitBtn", 1)

    Ufis.CleanupCom

    ilRC = Ufis.InitCom(hostName, "CEDA")
    ilRC = Ufis.SetCedaPerameters(hostType, HomeAirport, tableExt)
    MainForm.Caption = MainForm.Caption & " (" & GetText("MainForm", "LabelConnect", 1) & " " & hostName & ")"

End Sub

Private Sub Form_Unload(Cancel As Integer)
    'killing the netin on the server
    Ufis.CleanupCom

    Unload SsimForm
    Unload TtbForm
    Unload SlotcoForm
    Unload FlukoForm
    Unload AptForm

End Sub

Public Sub InitTabForCedaConnection(ByRef rTab As TABLib.TAB)
    'do the initializing for the CEDA-connection
    rTab.CedaServerName = hostName
    rTab.CedaPort = "3357"
    rTab.CedaHopo = HomeAirport
    rTab.CedaCurrentApplication = "Export"
    rTab.CedaTabext = tableExt
    rTab.CedaUser = "Export"

    rTab.CedaWorkstation = GetWorkstationName
    rTab.CedaSendTimeout = "120"
    rTab.CedaReceiveTimeout = "240"
    rTab.CedaRecordSeparator = Chr(10)
    rTab.CedaIdentifier = "Export"

    'do general initializing
    rTab.ShowHorzScroller True
    rTab.EnableHeaderSizing True
End Sub

Private Sub ExitBtn_Click()
    Ufis.CleanupCom
    End
End Sub

Private Sub SsimTabBtn_Click()
    SsimForm.Visible = True
End Sub

Private Sub TtbTabBtn_Click()
    TtbForm.Visible = True
End Sub

Private Sub SlotcoTabBtn_Click()
    SlotcoForm.Visible = True
End Sub

Private Sub FlukoTabBtn_Click()
    FlukoForm.Visible = True
End Sub

Private Sub AirportTabBtn_Click()
    AptForm.Visible = True
End Sub

Private Sub FileNameBtn_Click()

    InFileName.Text = ""
    CommonDialog1.DialogTitle = GetText("MainForm", "DialogTitle1", 1)
    CommonDialog1.Filter = GetText("MainForm", "DialogFilter1", 1)
    CommonDialog1.FilterIndex = 1
    CommonDialog1.ShowOpen
    InFileName.Text = CommonDialog1.FileName
    If InFileName.Text <> "" Then
        ReadBtn_Click
    End If
    
End Sub

Private Sub ReadBtn_Click()
    Dim tmpLine As String
    Dim i As Integer
    Dim lineNo As Long
    
    If InFileName.Text = "" Then
        MsgBox (GetText("MainForm", "MsgBox1", 1))
        Exit Sub
    End If
    Screen.MousePointer = vbHourglass
    TAB1.ResetContent
    TAB1.HeaderString = InFileName.Text
    TAB1.HeaderLengthString = 2000
    lineNo = 0
    Open InFileName.Text For Input As #1
        While Not EOF(1)
            Line Input #1, tmpLine
            i = InStr(tmpLine, ",")
            While i > 0
                Mid(tmpLine, i, 1) = " "
                i = InStr(tmpLine, ",")
            Wend
            TAB1.InsertTextLineAt lineNo, tmpLine, False
            lineNo = lineNo + 1
        Wend
    Close #1
    TAB1.Visible = True
    TAB1.RedrawTab
    Screen.MousePointer = vbArrow

End Sub

Public Function WordCount(TextArea As String) As Integer
    Dim myCount As Integer
    Dim tmpText As String
    Dim tmpCharC As String
    Dim tmpCharP As String
    Dim i As Integer
    
    'Clear all double blanks
    tmpText = ""
    tmpCharP = " "
    For i = 1 To Len(TextArea)
        tmpCharC = Mid(TextArea, i, 1)
        If tmpCharC <> " " Then
            tmpCharP = tmpCharC
            tmpText = tmpText & tmpCharC
        Else
            If tmpCharP <> " " Then
                tmpCharP = tmpCharC
                tmpText = tmpText & tmpCharC
            End If
        End If
    Next i
    If Len(tmpText) > 0 Then
        tmpCharC = Mid(tmpText, Len(tmpText), 1)
        If tmpCharC = " " Then
            tmpText = Mid(tmpText, 1, Len(tmpText) - 1)
        End If
    End If
    
    If tmpText = "" Then
        myCount = 0
    Else
        myCount = 1
        i = InStr(tmpText, " ")
        While i > 0
            myCount = myCount + 1
            tmpText = Mid(tmpText, i + 1)
            i = InStr(tmpText, " ")
            While i = 1
                tmpText = Mid(tmpText, i + 1)
                i = InStr(tmpText, " ")
            Wend
        Wend
    End If
    
    WordCount = myCount
End Function

Public Function GetWord(TextArea As String, WordNo As Integer) As String
    Dim curWord As String
    Dim tmpText As String
    Dim myCount As Integer
    Dim tmpCharC As String
    Dim tmpCharP As String
    Dim i As Integer
    
    'Clear all double blanks
    tmpText = ""
    tmpCharP = " "
    For i = 1 To Len(TextArea)
        tmpCharC = Mid(TextArea, i, 1)
        If tmpCharC <> " " Then
            tmpCharP = tmpCharC
            tmpText = tmpText & tmpCharC
        Else
            If tmpCharP <> " " Then
                tmpCharP = tmpCharC
                tmpText = tmpText & tmpCharC
            End If
        End If
    Next i
    If Len(tmpText) > 0 Then
        tmpCharC = Mid(tmpText, Len(tmpText), 1)
        If tmpCharC = " " Then
            tmpText = Mid(tmpText, 1, Len(tmpText) - 1)
        End If
    End If
    
    curWord = ""
    If tmpText <> "" And WordNo > 0 Then
        myCount = 1
        i = InStr(tmpText, " ")
        While i > 0 And curWord = ""
            If myCount = WordNo Then
                curWord = Left(tmpText, i - 1)
            Else
                myCount = myCount + 1
                tmpText = Mid(tmpText, i + 1)
                i = InStr(tmpText, " ")
            End If
        Wend
        If myCount = WordNo And curWord = "" Then
            curWord = tmpText
        End If
    End If
    
    GetWord = curWord
End Function

Public Function GetText(section As String, Label As String, item As Integer) As String
    Dim selText As String
    Dim ItemNbr As Integer
    
    selText = GetIniEntry(textPath & "\TextTable.txt", section, "", Label, "")
    If selText <> "" Then
        ItemNbr = Val(myLanguage)
        selText = GetItem(selText, ItemNbr, ",")
        selText = GetItem(selText, item, "#")
    End If
    
    GetText = selText
End Function

Public Function GetLineCount(TextBuffer As String) As Long
    Dim myCount As Long
    Dim tmpText As String
    Dim i As Integer
    
    myCount = 0
    tmpText = TextBuffer
    i = InStr(tmpText, vbCrLf)
    While i > 0
        myCount = myCount + 1
        tmpText = Mid(tmpText, i + 2)
        i = InStr(tmpText, vbCrLf)
    Wend
    
    GetLineCount = myCount
End Function

Public Function GetLine(TextBuffer As String, lineNo As Long) As String
    Dim resultLine As String
    Dim tmpText As String
    Dim myCount As Long
    Dim i As Integer
    
    resultLine = ""
    If lineNo > 0 Then
        myCount = 0
        tmpText = TextBuffer
        i = InStr(tmpText, vbCrLf)
        While i > 0 And myCount < lineNo
            myCount = myCount + 1
            If myCount < lineNo Then
                tmpText = Mid(tmpText, i + 2)
                i = InStr(tmpText, vbCrLf)
            End If
        Wend
        resultLine = Mid(tmpText, 1, i)
    End If
    
    GetLine = resultLine
End Function

Public Function GetNumMonth(month As String) As String
    Dim Result As String
    
    Select Case month
        Case "JAN"
            Result = "01"
        Case "FEB"
            Result = "02"
        Case "MAR"
            Result = "03"
        Case "APR"
            Result = "04"
        Case "MAY"
            Result = "05"
        Case "JUN"
            Result = "06"
        Case "JUL"
            Result = "07"
        Case "AUG"
            Result = "08"
        Case "SEP"
            Result = "09"
        Case "OCT"
            Result = "10"
        Case "NOV"
            Result = "11"
        Case "DEC"
            Result = "12"
        Case Else
            Result = "99"
    End Select
    GetNumMonth = Result
End Function

Public Function GetAscMonth(month As String) As String
    Dim Result As String
    
    Select Case month
        Case "01"
            Result = "JAN"
        Case "02"
            Result = "FEB"
        Case "03"
            Result = "MAR"
        Case "04"
            Result = "APR"
        Case "05"
            Result = "MAY"
        Case "06"
            Result = "JUN"
        Case "07"
            Result = "JUL"
        Case "08"
            Result = "AUG"
        Case "09"
            Result = "SEP"
        Case "10"
            Result = "OCT"
        Case "11"
            Result = "NOV"
        Case "12"
            Result = "DEC"
        Case Else
            Result = "XXX"
    End Select
    GetAscMonth = Result
End Function

Public Function GetNumFormatted(number As Long, length As Integer) As String
    Dim Result As String
    
    Result = number
    If Len(Result) >= length Then
        Result = Mid(Result, 1, length)
    Else
        While Len(Result) < length
            Result = "0" & Result
        Wend
    End If
    GetNumFormatted = Result
End Function

Public Sub SlotcoSetItem(itemlist As String, ItemNbr As Integer, ItemSep As String, NewValue As String)
    Dim Result As String
    Dim CurItm As Integer
    Dim ReqItm As Integer
    Dim MaxItm As Integer

    Result = ""
    ReqItm = ItemNbr - 1
    For CurItm = 1 To ReqItm
        Result = Result & SlotcoGetItem(itemlist, CurItm, ItemSep) & ItemSep
    Next
    Result = Result & NewValue & ItemSep
    MaxItm = ItemCount(itemlist, ItemSep)
    ReqItm = ReqItm + 2
    For CurItm = ReqItm To MaxItm
        Result = Result & SlotcoGetItem(itemlist, CurItm, ItemSep) & ItemSep
    Next
    itemlist = Left(Result, Len(Result) - Len(ItemSep))
End Sub

Public Function SlotcoGetItem(cpTxt As String, ipNbr As Integer, cpSep As String) As String
    Dim Result
    Dim ilSepLen As Integer
    Dim ilFirstPos As Integer
    Dim ilLastPos As Integer
    Dim ilItmLen As Integer
    Dim ilItmNbr As Integer

    Result = ""
    If ipNbr > 0 Then
        ilSepLen = Len(cpSep)
        ilFirstPos = 1
        ilLastPos = 1
        ilItmNbr = 0
        While (ilItmNbr < ipNbr) And (ilLastPos > 0)
            ilItmNbr = ilItmNbr + 1
            ilLastPos = InStr(ilFirstPos, cpTxt, cpSep)
            If (ilItmNbr < ipNbr) And (ilLastPos > 0) Then ilFirstPos = ilLastPos + ilSepLen
        Wend
        If ilLastPos < ilFirstPos Then ilLastPos = Len(cpTxt) + 1
        If (ilItmNbr = ipNbr) And (ilLastPos > ilFirstPos) Then
            ilItmLen = ilLastPos - ilFirstPos
            Result = Mid(cpTxt, ilFirstPos, ilItmLen)
        End If
    End If
    SlotcoGetItem = Result
End Function

