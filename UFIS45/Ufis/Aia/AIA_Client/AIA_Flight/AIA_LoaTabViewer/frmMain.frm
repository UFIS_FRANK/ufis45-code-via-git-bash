VERSION 5.00
Object = "{64E8E382-05E2-11D2-9B1D-9B0630BC8F12}#1.0#0"; "tab.ocx"
Begin VB.Form frmMain 
   Caption         =   "PAX And Loading Information"
   ClientHeight    =   9885
   ClientLeft      =   165
   ClientTop       =   450
   ClientWidth     =   12405
   Icon            =   "frmMain.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   659
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   827
   StartUpPosition =   3  'Windows Default
   Begin VB.CheckBox Check1 
      Caption         =   "Show Empty Values"
      Height          =   240
      Left            =   9810
      TabIndex        =   40
      Top             =   480
      Width           =   2130
   End
   Begin VB.Frame fraTab 
      Height          =   7455
      Index           =   2
      Left            =   5760
      TabIndex        =   31
      Top             =   1350
      Visible         =   0   'False
      Width           =   2535
      Begin TABLib.TAB tabInterim3 
         Height          =   1695
         Index           =   0
         Left            =   120
         TabIndex        =   34
         TabStop         =   0   'False
         Top             =   540
         Width           =   1755
         _Version        =   65536
         _ExtentX        =   3096
         _ExtentY        =   2990
         _StockProps     =   0
      End
      Begin TABLib.TAB tabInterim3 
         Height          =   4035
         Index           =   1
         Left            =   180
         TabIndex        =   35
         TabStop         =   0   'False
         Top             =   2805
         Width           =   1755
         _Version        =   65536
         _ExtentX        =   3096
         _ExtentY        =   7117
         _StockProps     =   0
      End
      Begin VB.Label TabLabel3 
         Caption         =   "PAX:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   0
         Left            =   120
         TabIndex        =   33
         Top             =   180
         Width           =   1455
      End
      Begin VB.Label TabLabel3 
         Caption         =   "BAG:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   1
         Left            =   135
         TabIndex        =   32
         Top             =   2400
         Width           =   1455
      End
   End
   Begin VB.CommandButton cmdClose 
      Caption         =   "&Close"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   4515
      TabIndex        =   4
      Top             =   9525
      Width           =   765
   End
   Begin VB.TextBox txtEstimated 
      Alignment       =   2  'Center
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   300
      Left            =   7455
      TabIndex        =   20
      TabStop         =   0   'False
      Top             =   450
      Width           =   1620
   End
   Begin VB.TextBox txtSched 
      Alignment       =   2  'Center
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   300
      Left            =   4890
      TabIndex        =   19
      TabStop         =   0   'False
      Top             =   450
      Width           =   1620
   End
   Begin VB.TextBox txtORIGDEST 
      Alignment       =   2  'Center
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   300
      Left            =   3180
      TabIndex        =   17
      TabStop         =   0   'False
      Top             =   450
      Width           =   675
   End
   Begin VB.TextBox txtADID 
      Alignment       =   2  'Center
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   300
      Left            =   960
      TabIndex        =   15
      TabStop         =   0   'False
      Top             =   450
      Width           =   1230
   End
   Begin VB.TextBox txtREGN 
      Alignment       =   2  'Center
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   300
      Left            =   4890
      TabIndex        =   13
      TabStop         =   0   'False
      Top             =   120
      Width           =   1620
   End
   Begin VB.TextBox txtACT 
      Alignment       =   2  'Center
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   300
      Left            =   3180
      TabIndex        =   11
      TabStop         =   0   'False
      Top             =   120
      Width           =   675
   End
   Begin VB.TextBox txtFLNO 
      Alignment       =   2  'Center
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   300
      Left            =   960
      TabIndex        =   9
      TabStop         =   0   'False
      Top             =   120
      Width           =   1230
   End
   Begin VB.Frame fraTab 
      Height          =   7455
      Index           =   1
      Left            =   3015
      TabIndex        =   8
      Top             =   1350
      Visible         =   0   'False
      Width           =   2535
      Begin TABLib.TAB tabInterim2 
         Height          =   1710
         Index           =   0
         Left            =   120
         TabIndex        =   24
         TabStop         =   0   'False
         Top             =   540
         Width           =   1950
         _Version        =   65536
         _ExtentX        =   3440
         _ExtentY        =   3016
         _StockProps     =   0
      End
      Begin TABLib.TAB tabInterim2 
         Height          =   1530
         Index           =   2
         Left            =   120
         TabIndex        =   29
         TabStop         =   0   'False
         Top             =   5325
         Width           =   1995
         _Version        =   65536
         _ExtentX        =   3519
         _ExtentY        =   2699
         _StockProps     =   0
      End
      Begin TABLib.TAB tabInterim2 
         Height          =   1530
         Index           =   1
         Left            =   135
         TabIndex        =   38
         TabStop         =   0   'False
         Top             =   3120
         Width           =   1950
         _Version        =   65536
         _ExtentX        =   3440
         _ExtentY        =   2699
         _StockProps     =   0
      End
      Begin VB.Label TabLabel2 
         Caption         =   "USR:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   1
         Left            =   135
         TabIndex        =   39
         Top             =   2805
         Width           =   1455
      End
      Begin VB.Label TabLabel2 
         Caption         =   "CPM:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   2
         Left            =   90
         TabIndex        =   30
         Top             =   4965
         Width           =   1455
      End
      Begin VB.Label TabLabel2 
         Caption         =   "LDM:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   0
         Left            =   120
         TabIndex        =   28
         Top             =   180
         Width           =   1455
      End
   End
   Begin VB.Frame fraTab 
      Height          =   7485
      Index           =   0
      Left            =   210
      TabIndex        =   7
      Top             =   1320
      Visible         =   0   'False
      Width           =   2535
      Begin TABLib.TAB tabInterim1 
         Height          =   495
         Index           =   0
         Left            =   135
         TabIndex        =   23
         TabStop         =   0   'False
         Top             =   435
         Width           =   2160
         _Version        =   65536
         _ExtentX        =   3810
         _ExtentY        =   873
         _StockProps     =   0
      End
      Begin TABLib.TAB tabInterim1 
         Height          =   2940
         Index           =   1
         Left            =   135
         TabIndex        =   25
         TabStop         =   0   'False
         Top             =   1530
         Width           =   2205
         _Version        =   65536
         _ExtentX        =   3889
         _ExtentY        =   5186
         _StockProps     =   0
      End
      Begin TABLib.TAB tabInterim1 
         Height          =   2040
         Index           =   2
         Left            =   135
         TabIndex        =   36
         TabStop         =   0   'False
         Top             =   4860
         Width           =   2160
         _Version        =   65536
         _ExtentX        =   3810
         _ExtentY        =   3598
         _StockProps     =   0
      End
      Begin VB.Label TabLabel1 
         Caption         =   "USR:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   2
         Left            =   135
         TabIndex        =   37
         Top             =   4605
         Width           =   1635
      End
      Begin VB.Label TabLabel1 
         Caption         =   "LDM:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   1
         Left            =   135
         TabIndex        =   27
         Top             =   1290
         Width           =   1635
      End
      Begin VB.Label TabLabel1 
         Caption         =   "MVT:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   0
         Left            =   135
         TabIndex        =   26
         Top             =   180
         Width           =   1455
      End
   End
   Begin VB.CommandButton cmdLoad 
      Caption         =   "Load"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   2355
      TabIndex        =   6
      Top             =   9525
      Visible         =   0   'False
      Width           =   765
   End
   Begin VB.TextBox txtAFTURNO 
      Height          =   285
      Left            =   1260
      TabIndex        =   0
      Text            =   "0"
      ToolTipText     =   "URNO of AFTTAB"
      Top             =   9540
      Visible         =   0   'False
      Width           =   1035
   End
   Begin VB.Label lblTab 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Transfer"
      ForeColor       =   &H80000008&
      Height          =   240
      Index           =   2
      Left            =   3720
      TabIndex        =   3
      Top             =   840
      Width           =   1395
   End
   Begin VB.Label Label8 
      Caption         =   "Estimated:"
      Height          =   225
      Left            =   6675
      TabIndex        =   22
      Top             =   495
      Width           =   855
   End
   Begin VB.Label Label7 
      Caption         =   "Scheduled:"
      Height          =   225
      Left            =   4050
      TabIndex        =   21
      Top             =   495
      Width           =   855
   End
   Begin VB.Label Label6 
      Caption         =   "Orig/Dest:"
      Height          =   225
      Left            =   2400
      TabIndex        =   18
      Top             =   495
      Width           =   795
   End
   Begin VB.Label Label5 
      Caption         =   "Type:"
      Height          =   225
      Left            =   180
      TabIndex        =   16
      Top             =   495
      Width           =   735
   End
   Begin VB.Label Label4 
      Caption         =   "REGN:"
      Height          =   225
      Left            =   4080
      TabIndex        =   14
      Top             =   165
      Width           =   555
   End
   Begin VB.Label Label3 
      Caption         =   "A/C:"
      Height          =   225
      Left            =   2790
      TabIndex        =   12
      Top             =   165
      Width           =   435
   End
   Begin VB.Label Label2 
      Caption         =   "FlightNo.:"
      Height          =   225
      Left            =   180
      TabIndex        =   10
      Top             =   165
      Width           =   735
   End
   Begin VB.Label lblTab 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Loading Data"
      ForeColor       =   &H80000008&
      Height          =   240
      Index           =   1
      Left            =   2100
      TabIndex        =   2
      Top             =   840
      Width           =   1395
   End
   Begin VB.Label lblTab 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      BorderStyle     =   1  'Fixed Single
      Caption         =   "PAX Data"
      ForeColor       =   &H80000008&
      Height          =   240
      Index           =   0
      Left            =   540
      TabIndex        =   1
      Top             =   840
      Width           =   1275
   End
   Begin VB.Label Label1 
      Caption         =   "Flight Urno:"
      Height          =   255
      Left            =   240
      TabIndex        =   5
      Top             =   9570
      Visible         =   0   'False
      Width           =   975
   End
End
Attribute VB_Name = "frmMain"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim TabsCount As Integer
Dim CurrentTab As Integer

Public Sub SelectTab(ByVal Index As Integer)
   Dim X1 As Long, X2 As Long, Y As Long
   fraTab(CurrentTab).Visible = False
   lblTab(CurrentTab).FontBold = False
   CurrentTab = Index
   fraTab(CurrentTab).Visible = True
   lblTab(CurrentTab).FontBold = True

   X1 = lblTab(Index).Left + 1   ' draw new Tab selection
   X2 = lblTab(Index).Left + lblTab(Index).Width - 2
   Y = lblTab(Index).Top + lblTab(Index).Height
   Me.Cls
   Me.Line (X1, Y)-(X2, Y), vbButtonFace
   Me.PSet (X1 - 1, Y), vb3DHighlight
   Me.PSet (X2, Y), vb3DShadow
   Me.PSet (X2 + 1, Y), vb3DDKShadow
   
End Sub

Private Sub StartTab()
    Dim i As Long
    Dim FolderTop As Long
    Dim FirstTop As Long
    Dim VertOffset As Long
    Dim LabelOffset As Long
    Dim X1 As Long, Y1 As Long
    Dim X2 As Long, Y2 As Long
    Dim ilTotalWidth As Long

    strSep = ";"
    ilTotalWidth = 848
    Me.Width = (ilTotalWidth - 18) * Screen.TwipsPerPixelX
    Me.AutoRedraw = True

    InitMyInterimTab tabInterim1(0)
    InitMyInterimTab tabInterim1(1)
    InitMyInterimTab tabInterim1(2)
    InitMyInterimTab tabInterim2(0)
    InitMyInterimTab tabInterim2(1)
    InitMyInterimTab tabInterim2(2)
    InitMyInterimTab tabInterim3(0)
    InitMyInterimTab tabInterim3(1)
    
    tabInterim1(0).Left = 10 * Screen.TwipsPerPixelX
    tabInterim1(1).Left = 10 * Screen.TwipsPerPixelX
    tabInterim1(2).Left = 10 * Screen.TwipsPerPixelX
    tabInterim2(0).Left = 10 * Screen.TwipsPerPixelX
    tabInterim2(1).Left = 10 * Screen.TwipsPerPixelX
    tabInterim2(2).Left = 10 * Screen.TwipsPerPixelX
    tabInterim3(0).Left = 10 * Screen.TwipsPerPixelX
    tabInterim3(1).Left = 10 * Screen.TwipsPerPixelX

    TabLabel1(0).Left = tabInterim1(0).Left
    TabLabel1(1).Left = tabInterim1(1).Left
    TabLabel1(2).Left = tabInterim1(2).Left
    TabLabel2(0).Left = tabInterim2(0).Left
    TabLabel2(1).Left = tabInterim2(1).Left
    TabLabel2(2).Left = tabInterim2(2).Left
    TabLabel3(0).Left = tabInterim3(0).Left
    TabLabel3(1).Left = tabInterim3(1).Left
    
    FirstTop = 20 * Screen.TwipsPerPixelY
    VertOffset = 20 * Screen.TwipsPerPixelY
    LabelOffset = 15 * Screen.TwipsPerPixelY
    
    tabInterim1(0).Top = FirstTop
    tabInterim1(0).Height = (1 + 1) * tabInterim1(0).LineHeight * Screen.TwipsPerPixelY
    TabLabel1(0).Top = tabInterim1(0).Top - LabelOffset
    tabInterim1(1).Top = tabInterim1(0).Top + tabInterim1(0).Height + VertOffset
    tabInterim1(1).Height = (22 + 1) * tabInterim1(1).LineHeight * Screen.TwipsPerPixelY
    TabLabel1(1).Top = tabInterim1(1).Top - LabelOffset
    tabInterim1(2).Top = tabInterim1(1).Top + tabInterim1(1).Height + VertOffset
    tabInterim1(2).Height = (8 + 1) * tabInterim1(2).LineHeight * Screen.TwipsPerPixelY
    TabLabel1(2).Top = tabInterim1(2).Top - LabelOffset
    
    tabInterim2(0).Top = FirstTop
    tabInterim2(0).Height = (22 + 1) * tabInterim2(0).LineHeight * Screen.TwipsPerPixelY
    TabLabel2(0).Top = tabInterim2(0).Top - LabelOffset
    tabInterim2(1).Top = tabInterim2(0).Top + tabInterim2(0).Height + VertOffset
    tabInterim2(1).Height = (8 + 1) * tabInterim2(1).LineHeight * Screen.TwipsPerPixelY
    TabLabel2(1).Top = tabInterim2(1).Top - LabelOffset
    tabInterim2(2).Top = tabInterim2(1).Top + tabInterim2(1).Height + VertOffset
    tabInterim2(2).Height = (1 + 1) * tabInterim2(2).LineHeight * Screen.TwipsPerPixelY
    TabLabel2(2).Top = tabInterim2(2).Top - LabelOffset
    
    tabInterim3(0).Top = FirstTop
    tabInterim3(0).Height = (24 + 1) * tabInterim3(0).LineHeight * Screen.TwipsPerPixelY
    TabLabel3(0).Top = tabInterim3(0).Top - LabelOffset
    tabInterim3(1).Top = tabInterim3(0).Top + tabInterim3(0).Height + VertOffset
    tabInterim3(1).Height = (9 + 1) * tabInterim3(1).LineHeight * Screen.TwipsPerPixelY
    TabLabel3(1).Top = tabInterim3(1).Top - LabelOffset
    
    FolderTop = 60

    lblTab(0).Left = 5                  ' position first lblTab in upper left corner
    lblTab(0).Top = FolderTop
    lblTab(0).BorderStyle = 0           ' no borders - all borders are only used in design time

    fraTab(0).Left = 10                 ' position first fraTab beneath lblTab
    fraTab(0).Top = FolderTop + 20
    fraTab(0).BorderStyle = 0           ' no borders - all borders are only used in design time

    For i = 1 To TabsCount - 1
       lblTab(i).Left = lblTab(i - 1).Left + lblTab(i - 1).Width
       lblTab(i).Top = lblTab(0).Top    ' align Tops
       lblTab(i).BorderStyle = 0        ' no borders
       fraTab(i).Left = fraTab(0).Left  ' all frames in same place
       fraTab(i).Top = fraTab(0).Top
       fraTab(i).BorderStyle = 0
       fraTab(i).Visible = False
    Next i
    
    For i = 0 To TabsCount - 1          ' draw raised borders
       X1 = lblTab(i).Left              ' for each lblTab
       Y1 = lblTab(i).Top - 4
       X2 = lblTab(i).Left + lblTab(i).Width - 2
       Y2 = lblTab(i).Top + lblTab(i).Height
       Me.Line (X1 + 1, Y1)-(X2 - 1, Y1), vb3DHighlight
       Me.Line (X1, Y1 + 1)-(X1, Y2), vb3DHighlight
       Me.Line (X2, Y1 + 1)-(X2, Y2), vb3DShadow
       Me.Line (X2 + 1, Y1 + 2)-(X2 + 1, Y2), vb3DDKShadow
    Next i

    X1 = lblTab(0).Left                   ' draw raised borders
    Y1 = lblTab(0).Top + lblTab(0).Height ' around the Tab-body
    X2 = ilTotalWidth - 35
    Y2 = cmdClose.Top - 8
    Me.Line (X1, Y1)-(X2, Y1), vb3DHighlight
    Me.Line (X1, Y1 + 1)-(X1, Y2 - 1), vb3DHighlight
    Me.Line (X1 + 1, Y2)-(X2, Y2), vb3DShadow
    Me.Line (X1, Y2 + 1)-(X2 + 1, Y2 + 1), vb3DDKShadow
    Me.Line (X2, Y1 + 1)-(X2, Y2 + 1), vb3DShadow
    Me.Line (X2 + 1, Y1)-(X2 + 1, Y2 + 1), vb3DDKShadow

    Me.Picture = Me.Image               ' make permanent background
    Me.AutoRedraw = False
    For i = 0 To TabsCount - 1
        fraTab(i).Height = cmdClose.Top - 90
        fraTab(i).Width = ilTotalWidth - 50
    Next i
    
    tabInterim1(0).Width = (ilTotalWidth - 70) * Screen.TwipsPerPixelX
    tabInterim1(1).Width = (ilTotalWidth - 70) * Screen.TwipsPerPixelX
    tabInterim1(2).Width = (ilTotalWidth - 70) * Screen.TwipsPerPixelX
    
    tabInterim2(0).Width = (ilTotalWidth - 70) * Screen.TwipsPerPixelX
    tabInterim2(1).Width = (ilTotalWidth - 70) * Screen.TwipsPerPixelX
    tabInterim2(2).Width = (ilTotalWidth - 70) * Screen.TwipsPerPixelX
    
    tabInterim3(0).Width = (ilTotalWidth - 70) * Screen.TwipsPerPixelX
    tabInterim3(1).Width = (ilTotalWidth - 70) * Screen.TwipsPerPixelX
    cmdClose.Left = (ilTotalWidth - cmdClose.Width - 40) / 2
End Sub

Private Sub Check1_Click()
    Dim l As Long
    For l = 0 To frmHiddenData.tabData(idxLOATAB).GetLineCount - 1
        frmHiddenData.tabData(idxLOATAB).SetLineStatusValue l, 0
    Next l
    RebuildAll
End Sub

Private Sub cmdClose_Click()
    End
End Sub

Private Sub Form_Paint()
   SelectTab CurrentTab
End Sub

Private Sub lblTab_Click(Index As Integer)
   SelectTab Index
End Sub

Private Sub cmdLoad_Click()
    strAFT_URNO = txtAFTURNO.Text
    Screen.MousePointer = vbHourglass
    frmHiddenData.LoadAll
    Screen.MousePointer = vbArrow
    RebuildAll
    Select Case GetItem(Command, 2, ",")
        Case "MVTPAX", "LDMPAX"
           SelectTab 0
        Case "LDMCGO", "CPMCGO"
           SelectTab 1
        Case "PTMPAX"
           SelectTab 2
        Case Else
    End Select
End Sub
Private Sub RebuildAll()
    SetFlightFields

    PreparePaxPages "P" + strSep + "MVT", tabInterim1(0)
    PreparePaxPages "P" + strSep + "LDM", tabInterim1(1)
    PreparePaxPages "P" + strSep + "USR", tabInterim1(2)

    PreparePaxPages "L" + strSep + "LDM", tabInterim2(0)
    PreparePaxPages "L" + strSep + "USR", tabInterim2(1)
    PreparePaxPages "L" + strSep + "CPM", tabInterim2(2)

    PreparePaxPages "P" + strSep + "PTM", tabInterim3(0)
    PreparePaxPages "L" + strSep + "PTM", tabInterim3(1)
End Sub

Private Sub InitMyInterimTab(myTab As TABLib.TAB)
    myTab.ResetContent
    myTab.SetFieldSeparator strSep
    myTab.HeaderString = "TLX " & strSep & "TYP" & strSep & "Description (Meaning of Value)" & strSep & "FLNO" & strSep & "APC" & strSep & " Value" & strSep & " "
    myTab.HeaderLengthString = "32,32,530,90,32,46,500"
    myTab.ColumnAlignmentString = "L,L,L,L,L,R,L"
    'myTab.FontName = "Courier New"
    myTab.FontName = "MS Sans Serif"
    myTab.FontSize = 14
    myTab.LineHeight = 14
    myTab.SetTabFontBold True
    myTab.EnableHeaderSizing True
    myTab.HeaderFontSize = 14
    myTab.LeftTextOffset = 2
    'myTab.ShowHorzScroller True
End Sub
Public Sub PreparePaxPages(strTypes As String, myTab As TABLib.TAB)
    Dim strLines As String
    Dim llLine As Long
    Dim llCnt As Long
    Dim i As Long
    Dim j As Long
    Dim strSearch As String
    Dim strDES3 As String
    Dim strDES3Compare As String
    Dim strCompare As String
    Dim idx As Long
    Dim strDescPart As String
    Dim strInsert As String
    Dim strNewLine As String
    Dim strLineNo As String
    Dim strFlno As String
    Dim strApc3 As String

    myTab.ResetContent

    idx = CLng(GetRealItemNo(frmHiddenData.strCfgTab, "Remark"))

    strLines = frmHiddenData.tabContentCfg.GetLinesByMultipleColumnValue("0,2", strTypes, 0)
    If Len(strLines) > 0 Then
        llCnt = CLng(ItemCount(strLines, ","))
        Dim blFound As Boolean
        For i = 0 To llCnt - 1
            strSearch = ""
            blFound = Check1.value
            llLine = CLng(GetRealItem(strLines, i, ","))

            strDescPart = frmHiddenData.tabContentCfg.GetColumnValue(llLine, 2) + strSep + _
                          frmHiddenData.tabContentCfg.GetColumnValue(llLine, 3) + strSep + _
                          frmHiddenData.tabContentCfg.GetColumnValue(llLine, idx) + strSep
            strDES3 = frmHiddenData.tabContentCfg.GetColumnValue(llLine, 8)

            For j = 2 To 6
                If j = 6 Then
                    strSearch = strSearch + frmHiddenData.tabContentCfg.GetColumnValue(llLine, j)
                Else
                    strSearch = strSearch + frmHiddenData.tabContentCfg.GetColumnValue(llLine, j) + strSep
                End If
            Next j

            For j = 0 To frmHiddenData.tabData(idxLOATAB).GetLineCount - 1
                If frmHiddenData.tabData(idxLOATAB).GetLineStatusValue(j) <> 1 Then
                    strCompare = frmHiddenData.tabData(idxLOATAB).GetColumnValue(j, 3) + strSep
                    strCompare = strCompare + frmHiddenData.tabData(idxLOATAB).GetColumnValue(j, 4) + strSep
                    strCompare = strCompare + frmHiddenData.tabData(idxLOATAB).GetColumnValue(j, 5) + strSep
                    strCompare = strCompare + frmHiddenData.tabData(idxLOATAB).GetColumnValue(j, 6) + strSep
                    strCompare = strCompare + frmHiddenData.tabData(idxLOATAB).GetColumnValue(j, 7)
                    strDES3Compare = frmHiddenData.tabData(idxLOATAB).GetColumnValue(j, 9)
                    If strCompare = strSearch Then
                        If (strDES3 = strDES3Compare) Or (Len(strDES3) > 0 And Len(strDES3Compare) > 0) Then
                            blFound = True
                            frmHiddenData.tabData(idxLOATAB).SetLineStatusValue j, 1
                            strNewLine = strDescPart
                            strNewLine = strNewLine + frmHiddenData.tabData(idxLOATAB).GetColumnValue(j, 8) + strSep
                            strNewLine = strNewLine + strDES3Compare + strSep
                            strNewLine = strNewLine + frmHiddenData.tabData(idxLOATAB).GetColumnValue(j, 10) + strSep
                            strFlno = frmHiddenData.tabData(idxLOATAB).GetColumnValue(j, 8)
                            If strFlno = "" Then strFlno = "..."
                            strApc3 = frmHiddenData.tabData(idxLOATAB).GetColumnValue(j, 9)
                            If strApc3 = "" Then strApc3 = "..."
                            strNewLine = Replace(strNewLine, "<FLNO>", strFlno, 1, 1, vbBinaryCompare)
                            strNewLine = Replace(strNewLine, "<DES3>", strApc3, 1, 1, vbBinaryCompare)
                            strInsert = strInsert + strNewLine + vbLf
                        End If
                    End If
                End If
            Next j
            If blFound = True Then
                If strInsert = "" Then
                    strInsert = strInsert + strDescPart
                End If
                myTab.InsertBuffer strInsert, vbLf
            End If
            strInsert = ""
        Next i
    End If
    myTab.RedrawTab
End Sub

Public Sub SetFlightFields()
    Dim idx As Integer
    Dim value As String
    
    If frmHiddenData.tabData(0).GetLineCount = 0 Then
        Exit Sub
    End If
    
    idx = GetRealItemNo(strAFTFIELDS, "FLNO")
    If idx <> -1 Then
        txtFLNO = frmHiddenData.tabData(0).GetColumnValue(0, idx)
    End If
    idx = GetRealItemNo(strAFTFIELDS, "ACT3")
    If idx <> -1 Then
        value = frmHiddenData.tabData(0).GetColumnValue(0, idx)
        If value = "" Then
            idx = GetRealItemNo(strAFTFIELDS, "ACT5")
            If idx <> -1 Then
                value = frmHiddenData.tabData(0).GetColumnValue(0, idx)
            End If
        End If
        txtACT = value
    End If
    idx = GetRealItemNo(strAFTFIELDS, "REGN")
    If idx <> -1 Then
        txtREGN = frmHiddenData.tabData(0).GetColumnValue(0, idx)
    End If
    idx = GetRealItemNo(strAFTFIELDS, "ADID")
    If idx <> -1 Then
        value = frmHiddenData.tabData(0).GetColumnValue(0, idx)
        If value = "A" Then
            txtADID = "Arrival"
        End If
        If value = "D" Then
            txtADID = "Departure"
        End If
    End If
    If txtADID = "Arrival" Then
        idx = GetRealItemNo(strAFTFIELDS, "ORG3")
        If idx <> -1 Then
            value = frmHiddenData.tabData(0).GetColumnValue(0, idx)
            txtORIGDEST = value
        End If
    End If
    If txtADID = "Departure" Then
        idx = GetRealItemNo(strAFTFIELDS, "DES3")
        If idx <> -1 Then
            value = frmHiddenData.tabData(0).GetColumnValue(0, idx)
            txtORIGDEST = value
        End If
    End If
    If txtADID = "Arrival" Then
        idx = GetRealItemNo(strAFTFIELDS, "STOA")
        If idx <> -1 Then
            value = frmHiddenData.tabData(0).GetColumnValue(0, idx)
            txtSched = DecodeSsimDayFormat(Left(value, 8), "CEDA", "SSIM2") & "/" & Mid(value, 9, 4)
        End If
    End If
    If txtADID = "Departure" Then
        idx = GetRealItemNo(strAFTFIELDS, "STOD")
        If idx <> -1 Then
            value = frmHiddenData.tabData(0).GetColumnValue(0, idx)
            txtSched = DecodeSsimDayFormat(Left(value, 8), "CEDA", "SSIM2") & "/" & Mid(value, 9, 4)
        End If
    End If
    If txtADID = "Arrival" Then
        idx = GetRealItemNo(strAFTFIELDS, "ETOA")
        If idx <> -1 Then
            value = Trim(frmHiddenData.tabData(0).GetColumnValue(0, idx))
            txtEstimated = ""
            If value <> "" Then txtEstimated = DecodeSsimDayFormat(Left(value, 8), "CEDA", "SSIM2") & "/" & Mid(value, 9, 4)
        End If
    End If
    If txtADID = "Departure" Then
        idx = GetRealItemNo(strAFTFIELDS, "ETOD")
        If idx <> -1 Then
            value = frmHiddenData.tabData(0).GetColumnValue(0, idx)
            txtEstimated = ""
            If value <> "" Then txtEstimated = DecodeSsimDayFormat(Left(value, 8), "CEDA", "SSIM2") & "/" & Mid(value, 9, 4)
        End If
    End If

End Sub

Private Sub Form_Load()
    Dim ilLeft As Integer
    Dim ilTop As Integer
    Dim blStandaloneOrDebug As Boolean
    Dim strCommand As String

    ' look after the command (afttab.urno) we got by the app we are started from
    strCommand = Command()
    If Len(strCommand) > 0 Then
        txtAFTURNO.Text = GetItem(strCommand, 1, ",")
        blStandaloneOrDebug = False
    Else
        blStandaloneOrDebug = True
    End If

    idxAFTTAB = 0
    idxLOATAB = 1
    TabsCount = lblTab.Count
    StartTab
    'strServer = "TONGA"
    strServer = GetIniEntry("", "LOATABVIEWER", "GLOBAL", "HOSTNAME", "")
    'strHOPO = "ATH"
    strHOPO = GetIniEntry("", "LOATABVIEWER", "GLOBAL", "HOMEAIRPORT", "")
    Load frmHiddenData
    If frmHiddenData.tabContentCfg.GetLineCount = 0 Then
        MsgBox "configfile is empty please check!"
        End
    End If
    ilLeft = GetSetting(AppName:="LoaTabViewer", _
                        section:="Startup", _
                        key:="frmMainLeft", _
                        Default:=-1)
    ilTop = GetSetting(AppName:="LoaTabViewer", _
                        section:="Startup", _
                        key:="frmMainTop", _
                        Default:=-1)
    If ilTop = -1 Or ilLeft = -1 Then
        Me.Move 0, 0
    Else
        Me.Move ilLeft, ilTop
    End If

    ' if we are in "runtime", start loading the data immediately
    If blStandaloneOrDebug = False Then
        cmdLoad_Click
    Else
        frmHiddenData.Show
        Label1.Visible = True
        txtAFTURNO.Visible = True
        cmdLoad.Visible = True
    End If
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Dim ilLeft As Integer
    Dim ilTop As Integer

    ilLeft = Me.Left
    ilTop = Me.Top
    SaveSetting "LoaTabViewer", _
                "Startup", _
                "frmMainLeft", _
                ilLeft
    SaveSetting "LoaTabViewer", _
                "Startup", _
                "frmMainTop", ilTop

    Unload frmHiddenData
End Sub

Private Sub tabInterim1_SendLButtonDblClick(Index As Integer, ByVal LineNo As Long, ByVal ColNo As Long)
    If LineNo = -1 Then
        tabInterim1(Index).Sort CStr(ColNo), True, True
        tabInterim1(Index).RedrawTab
    End If
End Sub

Private Sub tabInterim2_SendLButtonDblClick(Index As Integer, ByVal LineNo As Long, ByVal ColNo As Long)
    If LineNo = -1 Then
        tabInterim2(Index).Sort CStr(ColNo), True, True
        tabInterim2(Index).RedrawTab
    End If
End Sub

Private Sub tabInterim3_SendLButtonDblClick(Index As Integer, ByVal LineNo As Long, ByVal ColNo As Long)
    If LineNo = -1 Then
        tabInterim3(Index).Sort CStr(ColNo), True, True
        tabInterim3(Index).RedrawTab
    End If
End Sub

