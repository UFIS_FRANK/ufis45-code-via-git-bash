VERSION 5.00
Object = "{64E8E382-05E2-11D2-9B1D-9B0630BC8F12}#1.0#0"; "TAB.ocx"
Begin VB.Form frmHiddenData 
   Caption         =   "Hidden Data and Configuration"
   ClientHeight    =   10050
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   14175
   Icon            =   "frmHiddenData.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   10050
   ScaleWidth      =   14175
   StartUpPosition =   3  'Windows Default
   Begin TABLib.TAB tabData 
      Height          =   975
      Index           =   0
      Left            =   60
      TabIndex        =   1
      Tag             =   "AFTTAB;URNO,FLNO,ADID,REGN,ACT3,ACT5,STOA,STOD,ETOA,ETOD,ORG3,DES3"
      Top             =   4980
      Width           =   13515
      _Version        =   65536
      _ExtentX        =   23839
      _ExtentY        =   1720
      _StockProps     =   0
   End
   Begin TABLib.TAB tabContentCfg 
      Height          =   4575
      Left            =   60
      TabIndex        =   0
      Top             =   60
      Width           =   13695
      _Version        =   65536
      _ExtentX        =   24156
      _ExtentY        =   8070
      _StockProps     =   0
   End
   Begin TABLib.TAB tabData 
      Height          =   3855
      Index           =   1
      Left            =   60
      TabIndex        =   4
      Tag             =   "LOATAB;URNO,FLNU,RURN,DSSN,TYPE,STYP,SSTP,SSST,FLNO,APC3,VALU,RMRK,UNIT,TIME"
      Top             =   6180
      Width           =   13515
      _Version        =   65536
      _ExtentX        =   23839
      _ExtentY        =   6800
      _StockProps     =   0
   End
   Begin VB.Label lblCount 
      Caption         =   "Loaded Records: 0"
      Height          =   315
      Index           =   1
      Left            =   900
      TabIndex        =   6
      Top             =   5940
      Width           =   3375
   End
   Begin VB.Label lblTab 
      Caption         =   "LOATAB:"
      Height          =   255
      Index           =   1
      Left            =   60
      TabIndex        =   5
      Top             =   5940
      Width           =   795
   End
   Begin VB.Label lblCount 
      Caption         =   "Loaded Records: 0"
      Height          =   315
      Index           =   0
      Left            =   900
      TabIndex        =   3
      Top             =   4740
      Width           =   3375
   End
   Begin VB.Label lblTab 
      Caption         =   "AFTTAB:"
      Height          =   255
      Index           =   0
      Left            =   60
      TabIndex        =   2
      Top             =   4740
      Width           =   795
   End
End
Attribute VB_Name = "frmHiddenData"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public strCfgTab As String

Private Sub Form_Load()
    tabContentCfg.ResetContent
    tabContentCfg.EnableHeaderSizing True
    tabContentCfg.ShowHorzScroller True
    tabContentCfg.AutoSizeByHeader = True
    tabContentCfg.HeaderString = "TP,ID,tlx,TYPE,STYP,SSTP,SSST,FLNO,APC3,Remark"
    strCfgTab = "TP,ID,tlx,TYPE,STYP,SSTP,SSST,FLNO,APC3,Remark"
    tabContentCfg.HeaderLengthString = "30,120,50,30,30,30,30,80,50,400"
    tabContentCfg.SetFieldSeparator strSep
    tabContentCfg.ReadFromFile GetIniEntry("", "LOATABVIEWER", "", "INIFILE", "c:\ufis\system\LoaTabViewer.cfg")
    tabContentCfg.AutoSizeColumns
End Sub


Public Sub LoadAll()
    Dim myTab As TABLib.TAB
    Dim ilTabCnt As Integer
    Dim arr() As String
    Dim arrFields() As String
    Dim ilCount As Integer
    Dim strWhere As String
    Dim i As Integer
    
    ilTabCnt = 0
    For Each myTab In tabData
        arr = Split(myTab.Tag, ";")
        arrFields = Split(arr(1), ",")
        strWhere = ""
        myTab.ResetContent
        myTab.RedrawTab
        DoEvents
        myTab.AutoSizeByHeader = True
        myTab.HeaderString = ""
        myTab.HeaderLengthString = ""
        myTab.HeaderString = arr(1)
        myTab.EnableInlineEdit True
        For i = 0 To UBound(arrFields)
            If i = 0 Then
                myTab.HeaderLengthString = "80,"
            Else
                myTab.HeaderLengthString = myTab.HeaderLengthString + "80,"
            End If
        Next i
        myTab.CedaServerName = strServer
        myTab.CedaPort = "3357"
        myTab.CedaHopo = strHOPO
        myTab.CedaCurrentApplication = "LoaTabViewer"
        myTab.CedaTabext = "TAB"
        myTab.CedaUser = GetWindowsUserName()
        myTab.CedaWorkstation = GetWorkstationName()
        myTab.CedaSendTimeout = "3"
        myTab.CedaReceiveTimeout = "240"
        myTab.CedaRecordSeparator = Chr(10)
        myTab.CedaIdentifier = "LoaTab"
        myTab.ShowHorzScroller True
        myTab.EnableHeaderSizing True
        'frmMain.MainStatusBar.Panels(1).Text = "Loading: " + arr(0) + " ..."
        If arr(0) = "AFTTAB" Then
            strWhere = "WHERE URNO=" + strAFT_URNO
            strAFTFIELDS = arr(1)
        End If
        If arr(0) = "LOATAB" Then
            strWhere = "WHERE FLNU=" + strAFT_URNO
        End If
        If strWhere <> "" Then
            myTab.CedaAction "RT", arr(0), arr(1), "", strWhere
        End If
        myTab.RedrawTab
        lblCount(ilTabCnt) = "Table: " + arr(0) + "  Records: " + CStr(myTab.GetLineCount)
        
        ilTabCnt = ilTabCnt + 1
        myTab.AutoSizeColumns
    Next
    tabData(idxLOATAB).Sort "3,4,5,6,7", True, False
    tabData(idxLOATAB).RedrawTab
    tabContentCfg.Sort "2,3,4,5,6", True, False
    tabContentCfg.RedrawTab
End Sub
