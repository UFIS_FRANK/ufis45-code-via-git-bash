// CuteIF.cpp : Defines the class behaviors for the application.
//

#include "stdafx.h"
#include "CCSGlobl.h"
#include "CCSLog.h"
#include "CCSCedaCom.h"
#include "CCSBcHandle.h"
#include "CedaBasicData.h"
#include "BasicData.h"
#include "CuteIF.h"
#include "CuteIFDlg.h"
//#include "ClntTypes.h"
#include "CCSGlobl.h"
#include <AfxInet.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif



/////////////////////////////////////////////////////////////////////////////
// CCuteIFException -- used if something goes wrong for us

// CuteIF will throw its own exception type to handle problems it might
// encounter while fulfilling the user's request.

IMPLEMENT_DYNCREATE(CCuteIFException, CException)

CCuteIFException::CCuteIFException(int nCode)
	: m_nErrorCode(nCode)
{
}

void ThrowCuteIFException(int nCode)
{
	CCuteIFException* pEx = new CCuteIFException(nCode);
	throw pEx;
}



/////////////////////////////////////////////////////////////////////////////
// CCuteIFApp


bool CCuteIFApp::bgLogosAvail = true;
char CCuteIFApp::pcmLogoSourceCedaEntry[256];

BEGIN_MESSAGE_MAP(CCuteIFApp, CWinApp)
	//{{AFX_MSG_MAP(CCuteIFApp)
	//}}AFX_MSG
	ON_COMMAND(ID_HELP, CWinApp::OnHelp)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCuteIFApp construction

CCuteIFApp::CCuteIFApp()
{
}


CCuteIFApp::~CCuteIFApp()
{
	DeleteBrushes();

	if (bgDebug)
	{
		CTime olCurrTime = CTime::GetCurrentTime();
		of_debug << endl << "CUTEIFAPP TERMINATED! (" << olCurrTime.Format("%d.%m.%Y %H:%M:%S") << ")" << endl;
		of_debug.close();
	}

}


/////////////////////////////////////////////////////////////////////////////
// The one and only CCuteIFApp object

CCuteIFApp theApp;

/////////////////////////////////////////////////////////////////////////////
// CCuteIFApp initialization

BOOL CCuteIFApp::InitInstance()
{

	CCuteIFApp::TriggerWaitCursor(true);
	//SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));


	AfxEnableControlContainer();

	// Standard initialization

#ifdef _AFXDLL
	Enable3dControls();			// Call this when using MFC in a shared DLL
#else
	Enable3dControlsStatic();	// Call this when linking to MFC statically
#endif



	CTime oltmptime = CTime::GetCurrentTime();
	CString oltmpstr = oltmptime.Format("Datum: %d.%m.%Y");
	of_catch.open("CritErr.txt", ios::app);
	of_catch << oltmpstr.GetBuffer(0) << endl;
	of_catch << "=================" << endl;



	/////////////////////////////////////////////////////////////////////////////
	// Standard CCS-Initialization
	/////////////////////////////////////////////////////////////////////////////

	
	ogLog.SetAppName(ogAppName);
	ogCommHandler.SetAppName(ogAppName);


	// Standard CCS-Fonts and Brushes initialization (see ccsglobl.h)
    InitFont();
	CreateBrushes();
	

	ogBcHandle.SetCloMessage(GetString(IDS_STRING102));

    if (ogCommHandler.Initialize() != true)  // connection error?
    {
        AfxMessageBox(CString("CedaCom:\n") + ogCommHandler.LastError());
   		ogCommHandler.CleanUpCom();	
		ExitProcess(0);
   		return FALSE;
	}




	//::UfisDllAdmin("TRACE", "ON", "FATAL");


	// INIT Tablenames and Homeairport
	char pclConfigPath[256];
	char pclUser[256];
	char pclPassword[256];
	char pclDebug[256];

    if (getenv("CEDA") == NULL)
        strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
    else
        strcpy(pclConfigPath, getenv("CEDA"));


    GetPrivateProfileString("GLOBAL", "HOMEAIRPORT", "HAJ", pcgHome, sizeof pcgHome, pclConfigPath);
    GetPrivateProfileString("GLOBAL", "TABLEEXTENSION", "HAJ", pcgTableExt, sizeof pcgTableExt, pclConfigPath);
    GetPrivateProfileString("GLOBAL", "USER", "DEFAULT", pclUser, sizeof pclUser, pclConfigPath);
    GetPrivateProfileString("GLOBAL", "PASSWORD", "DEFAULT", pclPassword, sizeof pclPassword, pclConfigPath);

    GetPrivateProfileString(ogAppName, "DEBUG", "DEFAULT", pclDebug, sizeof pclDebug, pclConfigPath);
	// get the source of the logos!
	GetPrivateProfileString(ogAppName, "LOGO_SOURCE", "", pcmLogoSourceCedaEntry, sizeof pcmLogoSourceCedaEntry, pclConfigPath);

	CString olCedaLogoSource(pcmLogoSourceCedaEntry);

	if (olCedaLogoSource.IsEmpty())
	{
		DisableLogos();
	}
	else if (olCedaLogoSource.Left(7) == "http://")
	{
		if (olCedaLogoSource.Right(1) != '/')
		{
			olCedaLogoSource += '/';
		}

		// source is on an internet server!
		bgLogoSourceHttp = true;
		ogFileLogoPath.Empty();
		DWORD dwServiceType;
		if (!AfxParseURL(olCedaLogoSource, dwServiceType, ogHttpServer, ogHttpLogoPath, igHttpPort) ||
			dwServiceType != INTERNET_SERVICE_HTTP)
		{
			//cerr << _T("Error: Invalid Http adress!") << endl;
			//ThrowCuteIFException(1);
			DisableLogos();
		}
	}
	else
	{
		//cerr << _T("Error: NOT YET IMPLEMENTED!") << endl;
		//ThrowCuteIFException(1);

		// source is a spezial directory
		if (olCedaLogoSource.Right(1) != '\\')
		{
			olCedaLogoSource += '\\';
		}

		bgLogoSourceHttp = false;
		ogHttpServer.Empty();
		igHttpPort = 0;
		ogHttpLogoPath.Empty();

		ogFileLogoPath = olCedaLogoSource;
	}


	// logging for debugging
	bgDebug = false;
	if(strcmp(pclDebug, "DEFAULT") != 0)
	{
		bgDebug = true;
		of_debug.open( pclDebug, ios::out);

		CTime olCurrTime = CTime::GetCurrentTime();

		of_debug << "CUTEIFAPP STARTED! (" << olCurrTime.Format("%d.%m.%Y %H:%M:%S") << ")" << endl;
		of_debug << "==========================================" << endl << endl;
	}
	


	//ogBcHandle.SetHomeAirport(pcgHome);


	strcpy(CCSCedaData::pcmTableExt, pcgTableExt);
	strcpy(CCSCedaData::pcmHomeAirport, pcgHome);
	strcpy(CCSCedaData::pcmApplName, ogAppName);
	strcpy(CCSCedaData::pcmVersion, pcgVersion);
	strcpy(CCSCedaData::pcmInternalBuild, pcgInternalBuild);
	strcpy(CCSCedaData::pcmReqId, ogCommHandler.pcmReqId);
	strcpy(CCSCedaData::pcmUser, pclUser);

	ogBCD.SetTableExtension(CString(pcgTableExt));
	
 
	CString olDBTab;
	olDBTab.Format("FLD%s", pcgTableExt);
	ogBcHandle.AddTableCommand(olDBTab, "IRT", BC_FLD_NEW, true);
	ogBcHandle.AddTableCommand(olDBTab, "URT", BC_FLD_CHANGE, true);
	ogBcHandle.AddTableCommand(olDBTab, "DRT", BC_FLD_DELETE, true);

	olDBTab.Format("CCA%s", pcgTableExt);
	ogBcHandle.AddTableCommand(olDBTab, "IRT", BC_CCA_NEW, true);
	ogBcHandle.AddTableCommand(olDBTab, "DRT", BC_CCA_DELETE, true);
	ogBcHandle.AddTableCommand(olDBTab, "URT", BC_CCA_CHANGE, true);

	olDBTab.Format("AFT%s", pcgTableExt);
	ogBcHandle.AddTableCommand(olDBTab, "ISF", BC_FLIGHT_NEW, true);
	ogBcHandle.AddTableCommand(olDBTab, "IFR", BC_FLIGHT_NEW, true);
	ogBcHandle.AddTableCommand(olDBTab, "UFR", BC_FLIGHT_CHANGE, true);
	ogBcHandle.AddTableCommand(olDBTab, "DFR", BC_FLIGHT_DELETE, true);

	/*
	CLoginDialog olLoginDlg(pcgTableExt,ogAppName,pomBackGround);

	if((strcmp(pclUser, "DEFAULT") != 0) && (strcmp(pclPassword, "DEFAULT") != 0))
	{
		strcpy(pcgUser,pclUser);
		strcpy(pcgPasswd,pclPassword);
		ogBasicData.omUserID = pclUser;
		ogCommHandler.SetUser(pclUser);

		strcpy(CCSCedaData::pcmReqId, ogCommHandler.pcmReqId);
		strcpy(CCSCedaData::pcmUser, pclUser);


		if(!ogPrivList.Login(pcgTableExt,pclUser,pclPassword,ogAppName))
			return FALSE;
	}
	else
	{
		if( olLoginDlg.DoModal() != IDCANCEL )
		{
			int ilStartApp = IDOK;
			if(ogPrivList.GetStat("InitModu") == '1')
			{
				RegisterDlg olRegisterDlg;
				ilStartApp = olRegisterDlg.DoModal();
			}
			if(ilStartApp != IDOK)
			{
				return FALSE;
			}

		}
		else
		{
			return FALSE;
		}
	}

	ogCfgData.ReadUfisCedaConfig();
*/



	//ogDataSet.Init();

	//Set Homeairport 4 letter code
	ogBCD.SetObject("APT", "URNO,APC3,APC4,APFN,TDI1,TDI2,TICH,APTT");
	ogBCD.AddKeyMap("APT", "APC3");
	ogBCD.AddKeyMap("APT", "APC4");

	CString olApc4;
	ogBCD.GetField("APT","APC3", CString(pcgHome), "APC4", olApc4);
	strcpy(pcgHome4, olApc4);


	// set local-utc diff
	ogBasicData.SetLocalDiff();

	CCSCedaData::omLocalDiff1 = ogBasicData.GetLocalDiff1();
	CCSCedaData::omLocalDiff2 = ogBasicData.GetLocalDiff2();
	CCSCedaData::omTich = ogBasicData.GetTich() ;


	// read some basic data
	ReadBasicData();


	//InitCOMInterface();

	CCuteIFApp::TriggerWaitCursor(false);
	//SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));		
 

	CCuteIFDlg dlg;
	m_pMainWnd = &dlg;
	int nResponse = dlg.DoModal();
	if (nResponse == IDOK)
	{
	}
	else if (nResponse == IDCANCEL)
	{
	}


	of_catch.close();



	// Since the dialog has been closed, return FALSE so that we exit the
	//  application, rather than start the application's message pump.
	return FALSE;
}



CCuteIFApp::ReadBasicData()
{
	ogBCD.SetTableExtension(CString(pcgTableExt));
	ogBCD.SetSystabErrorMsg( GetString(IDS_STRING110), GetString(IDS_ERROR) );

	// Device-Table to get the IP-Adress of the FIDS-Monitors
	ogBCD.SetObject("DEV", "URNO,DFFD,DFCO,DADR");


	// read fids remarks for checkin-counter
	ogBCD.SetObject("FID", "URNO,CODE,BEME,REMT");
	ogBCD.AddKeyMap("FID", "CODE");
	ogBCD.Read(CString("FID"), CString("WHERE REMT='C' OR REMT='G'"));

	// read airline table
	ogBCD.SetObject("ALT", "URNO,ALC2,ALC3,ALFN,TERM,CASH");
	ogBCD.AddKeyMap("ALT", "ALC2");
	ogBCD.AddKeyMap("ALT", "ALC3");
	ogBCD.Read(CString("ALT"), CString("WHERE ALC3 <> ' '"));


	// initialize logo map table
	ogBCD.SetObject("FLZ", "URNO,FLDU,FLGU,SORT");
	ogBCD.AddKeyMap("FLZ", "FLDU");
	ogBCD.SetDdxType("FLZ", "IRT", FLZ_CHANGE);
	ogBCD.SetDdxType("FLZ", "DRT", FLZ_CHANGE);
	ogBCD.SetDdxType("FLZ", "URT", FLZ_CHANGE);


	// read logos
	ogBCD.SetObject("FLG", "URNO,LGSN,LGFN");
	ogBCD.AddKeyMap("FLG", "URNO");
	ogBCD.AddKeyMap("FLG", "LGFN");
	ogBCD.AddKeyMap("FLG", "LGSN");
	ogBCD.Read(CString("FLG"), CString("WHERE LGSN <> ' '"));


	// initialize freetext table
	ogBCD.SetObject("FXT", "URNO,FLDU,TEXT,SORT");
	ogBCD.AddKeyMap("FXT", "FLDU");
	ogBCD.SetDdxType("FXT", "IRT", FXT_CHANGE);
	ogBCD.SetDdxType("FXT", "DRT", FXT_CHANGE);
	ogBCD.SetDdxType("FXT", "URT", FXT_CHANGE);


/*
	ogBCD.SetObject("FLG", "URNO,LGSN,LGFN");
	//ogBCD.SetObjectDesc("ALT", GetString(IDS_STRING955));
	//ogBCD.SetTableHeader("ALT", "ALFN", GetString(IDS_STRING985));
	//ogBCD.SetTableHeader("ALT", "ALC2", GetString(IDS_STRING987));
	//ogBCD.SetTableHeader("ALT", "ALC3", GetString(IDS_STRING988));
	//ogBCD.AddKeyMap("ALT", "ALC2");
	ogBCD.AddKeyMap("FLG", "LGSN");
	ogBCD.AddKeyMap("FLG", "LGFN");
	ogBCD.Read(CString("FLG"), CString(""));
*/

}



// set 'wait' or 'arrow' cursor and consider a set counter!
void CCuteIFApp::TriggerWaitCursor(bool bpWait)
{
	static int ilWaitCount = 0;	// 0 means arrow cursor is set
								// >0 means wait cursor is set

	if (bpWait)
	{
		// wait cursor shall be set
		if (ilWaitCount < 1)
		{
			ilWaitCount = 1;
			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
		}
		else
		{
			// wait cursor is already set, so only update the counter
			ilWaitCount++;
		}
	}
	else
	{
		// arrow cursor shall be set
		if (ilWaitCount > 0)
		{
			ilWaitCount--;
			if (ilWaitCount == 0)
			{
				// only if all wait-cursor function calls are 'deleted' by arrow-cursor function calls
				SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
			}
		}
	}

}


bool CCuteIFApp::DumpDebugLog(const CString &ropStr)
{

	if (!bgDebug) return false;

	CTime olCurrTime = CTime::GetCurrentTime();
	of_debug << olCurrTime.Format("%d.%m.%Y %H:%M:%S:  ") << ropStr << endl;

	return true;
}


bool CCuteIFApp::CheckCedaError(const CCSCedaData &ropCedaData)
{
	if (ropCedaData.imLastReturnCode != ropCedaData.RC_SUCCESS)
	{
		if (ropCedaData.omLastErrorMessage.Find("ORA-") > -1 || ropCedaData.imLastReturnCode != ropCedaData.RC_CEDA_FAIL)
		{
			CString olMsg;
			// Dump into logfile
			olMsg.Format("ERROR: Ceda reports: '%s'", ropCedaData.omLastErrorMessage);
			DumpDebugLog(olMsg);

			// display message
			olMsg = CString("CEDA reports:\n\n") + ropCedaData.omLastErrorMessage;
			AfxMessageBox(olMsg, MB_ICONERROR | MB_APPLMODAL | MB_TOPMOST);

			return false;
		}
	}
	return true;
}


bool CCuteIFApp::DisableLogos()
{
	if (bgLogosAvail)
	{
		bgLogosAvail = false;
		CString olMess;
		olMess.Format(GetString(IDS_STRING112), pcmLogoSourceCedaEntry);
		AfxMessageBox(olMess, MB_ICONERROR | MB_OK | MB_APPLMODAL | MB_TOPMOST);
	}
	return true;
}


