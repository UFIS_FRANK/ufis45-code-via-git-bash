
// CPP-FILE 

#include "stdafx.h"
#include <afxwin.h>
#include "CCSGlobl.h"
#include "CCSCedaData.h"
#include "ccsddx.h"
#include "CedaFldData.h"
#include "BasicData.h"
#include "CCSTime.h"
#include "Utils.h"
#include "CuteIF.h"

#define RTYP_CKIC "CKIF"
#define RTYP_GATE "GTD1"
#define RTYP_BELT "BELT"


const FLDDATA &FLDDATA::operator=(const FLDDATA &ropFldData)
{

	Abti = ropFldData.Abti;
	Afti = ropFldData.Afti;
	Aurn = ropFldData.Aurn;
	Cdat = ropFldData.Cdat;
	strncpy(Crec, ropFldData.Crec, DBFIELDLEN_CREC);
	strncpy(Ctyp, ropFldData.Ctyp, DBFIELDLEN_CTYP);
	Dseq = ropFldData.Dseq;
	Lstu = ropFldData.Lstu;
	strncpy(Rnam, ropFldData.Rnam, DBFIELDLEN_RNAM);
	strncpy(Rtab, ropFldData.Rtab, DBFIELDLEN_RTAB);
	strncpy(Rtyp, ropFldData.Rtyp, DBFIELDLEN_RTYP);
	Rurn = ropFldData.Rurn;
	Urno = ropFldData.Urno;
	strncpy(Usec, ropFldData.Usec, DBFIELDLEN_USEC);
	strncpy(Useu, ropFldData.Useu, DBFIELDLEN_USEU);

	ResType = ropFldData.ResType;

	return *this;
}


void  ProcessFldBC(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	((CedaFldData*)vpInstance)->ProcessFldBc(ipDDXType,vpDataPointer,ropInstanceName);
}


CedaFldData::CedaFldData()
{
	// Create an array of CEDARECINFO for FLDDATA
	BEGIN_CEDARECINFO(FLDDATA,FldDataRecInfo)
		CCS_FIELD_DATE(Abti,"ABTI","actual boarding time", 1)
		CCS_FIELD_DATE(Afti,"AFTI","actual final call time", 1)
		CCS_FIELD_LONG(Aurn, "AURN", "Urno der Afttab oder Ccatab", 1)
		CCS_FIELD_DATE(Cdat,"CDAT","time of creation", 1)
		CCS_FIELD_CHAR_TRIM(Crec, "CREC", "Counter remark code", 1)
		CCS_FIELD_CHAR_TRIM(Ctyp, "CTYP", "Codeshare type", 1)
		CCS_FIELD_LONG(Dseq, "DSEQ", "Display sequence", 1)
		CCS_FIELD_DATE(Lstu,"LSTU","time of last update", 1)
		CCS_FIELD_CHAR_TRIM(Rnam, "RNAM", "Resource name", 1)
		CCS_FIELD_CHAR_TRIM(Rtab, "RTAB", "Related table", 1)
		CCS_FIELD_CHAR_TRIM(Rtyp, "RTYP", "Resource typ", 1)
		CCS_FIELD_LONG(Rurn, "RURN", "Related urno", 1)
		CCS_FIELD_LONG(Urno, "URNO", "Eindeutige Datensatz-Nr.", 1)
		CCS_FIELD_CHAR_TRIM(Usec, "USEC", "user which create the record", 1)
		CCS_FIELD_CHAR_TRIM(Useu, "USEU", "user which last updated the record", 1)
	END_CEDARECINFO //(FLDDATA)

	// Copy the record structure
	for (int i=0; i< sizeof(FldDataRecInfo)/sizeof(FldDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&FldDataRecInfo[i],sizeof(CEDARECINFO));
		omRecInfo.Add(prpCedaRecInfo);
	} // end for

	// initialize field names
	strcpy(pcmTableName,"FLD");
	strcpy(pcmFList,"ABTI,AFTI,AURN,CDAT,CREC,CTYP,DSEQ,LSTU,RNAM,RTAB,RTYP,RURN,URNO,USEC,USEU");
	pcmFieldList = pcmFList;

	ogDdx.Register((void *)this,BC_FLD_CHANGE, CString("BC_FLD_CHANGE"), CString("JobDataChange"), ProcessFldBC);
	ogDdx.Register((void *)this,BC_FLD_NEW, CString("BC_FLD_NEW"), CString("JobDataChange"), ProcessFldBC);
	ogDdx.Register((void *)this,BC_FLD_DELETE, CString("BC_FLD_DELETE"), CString("JobDataChange"), ProcessFldBC);

} // end Constructor



CedaFldData::~CedaFldData()
{
	Clear();
	ogDdx.UnRegister(this,NOTUSED);
}




void CedaFldData::Clear()
{
	//omUrnoMap.RemoveAll();
	//omData.DeleteAll();
}


/*
void CedaFldData::Read( long lpUrno)
{
	Clear();

	char pclSel[256];

	sprintf(pclSel, "WHERE URNO = '%ld'", lpUrno);

	if (CedaAction("RT", pclSel) == false)
	{
		return;
	}

	bool ilRc = true;

    for (int ilLc = 0; ilRc == true; ilLc++)
    {
		FLDDATA *prlFld = new FLDDATA;
		if ((ilRc = GetBufferRecord(ilLc,prlFld)) == true)
		{
			InsertInternal(prlFld);
		}
		else
		{
			delete prlFld;
		}
	}
}
*/


/*
FLDDATA *CedaFldData::GetFld(long lpUrno)
{

	int ilCount = omData.GetSize();

	FLDDATA *prlFld;	
	for (int i = 0; i < ilCount; i++)
	{
		prlFld = &omData[i];
		if ( omData[i].Urno == lpUrno)
		{
			return &omData[i];
		}
	}

	return NULL;
}
*/


/*
void CedaFldData::InsertInternal(FLDDATA *prpFld)
{
	omData.Add(prpFld);
	omUrnoMap.SetAt((void *)prpFld->Urno,prpFld);
}




bool CedaFldData::UpdateInternal(FLDDATA *prpFld)
{
	int ilCount = omData.GetSize();
	for (int i = 0; i < ilCount; i++)
	{
		if (omData[i].Urno == prpFld->Urno)
		{
			omData[i] = *prpFld;
			return true;
		}
	}
	return false;
}



void CedaFldData::DeleteInternal(FLDDATA *prpFld)
{
	omUrnoMap.RemoveKey((void *)prpFld->Urno);
	int ilCount = omData.GetSize();
	for (int i = 0; i < ilCount; i++)
	{
		if (omData[i].Urno == prpFld->Urno)
		{
			omData.DeleteAt(i);//Update omData
			break;
		}
	}
}
*/



FLDDATA *CedaFldData::GetCuteRecord()
{
	return &rmCuteFldData;
}


bool CedaFldData::SaveCuteRecord()
{
	bool olRc = true;
	CString olData;
	CString olSel;

	// get current utc time
	CTime olCurrUtc = CTime::GetCurrentTime();
	ogBasicData.LocalToUtc(olCurrUtc);

	CString olUser;
	olUser.Format("Fips%s", ogAppName);
 
	if (rmCuteFldData.Urno < 1)
	{
		// insert record
		rmCuteFldData.Urno = ogBasicData.GetNextUrno(); 
		rmCuteFldData.Cdat = olCurrUtc;
		strncpy(rmCuteFldData.Usec, olUser, DBFIELDLEN_USEC);

		MakeCedaData(&omRecInfo, olData, &rmCuteFldData);

		olRc = CedaAction("IRT", "", "", olData.GetBuffer(0));
		CCuteIFApp::CheckCedaError(*this);

		//TRACE("CedaFldData::SaveCuteRecord: Insert: %s", olData);
	}
	else 
	{
		rmCuteFldData.Lstu = olCurrUtc; 
		strncpy(rmCuteFldData.Useu, olUser, DBFIELDLEN_USEU);

		// update record
		olSel.Format("WHERE URNO = %ld", rmCuteFldData.Urno);
		
		MakeCedaData(&omRecInfo, olData, &rmCuteFldData);

		olRc = CedaAction("URT", olSel.GetBuffer(0), "", olData.GetBuffer(0));
		CCuteIFApp::CheckCedaError(*this);

		//TRACE("CedaFldData::SaveCuteRecord: Update: %s  %s", olSel, olData);
	}


	return olRc;
}



bool CedaFldData::ReadCuteRecord(const CString &ropName, ResTypes ipResType, long lpDseq, bool bpDdx /* = true */ )
{
	// set given values of intern cute record
	long llOldFldUrno = rmCuteFldData.Urno;
	rmCuteFldData.Urno = 0;
	strncpy(rmCuteFldData.Rnam, ropName, DBFIELDLEN_RNAM);

	CString olRtyp;
	if (ipResType == ResCKI)
	{
		olRtyp = RTYP_CKIC;
	}
	else if (ipResType == ResGAT)
	{
		olRtyp = RTYP_GATE;
	}
	else if (ipResType == ResBLT)
	{
		olRtyp = RTYP_BELT;
	}
	else
	{
		return false;
	}

	strncpy(rmCuteFldData.Rtyp, olRtyp, DBFIELDLEN_RTYP);
	rmCuteFldData.Dseq = -1;


	// Read from DB
	CString olSel;
	FLDDATA rmNewFldData;

	if (ipResType == ResBLT)
		olSel.Format("WHERE RNAM = '%s' AND RTYP = '%s' AND DSEQ = '0'", ropName, olRtyp);
	else
		olSel.Format("WHERE RNAM = '%s' AND RTYP = '%s' AND (DSEQ = '1' OR DSEQ = '0')", ropName, olRtyp);

	bool blRet = CedaAction("RT", olSel.GetBuffer(0));
	CCuteIFApp::CheckCedaError(*this);
	if (blRet)
	{
		// get record
		if (GetBufferRecord(0, &rmNewFldData) == true)
		{
			// only overwrite intern record if read was successfully
			rmCuteFldData = rmNewFldData;
		}
	}


	if (strcmp(rmCuteFldData.Rtyp, RTYP_CKIC) == 0)
		rmCuteFldData.ResType = ResCKI;
	if (strcmp(rmCuteFldData.Rtyp, RTYP_GATE) == 0)
		rmCuteFldData.ResType = ResGAT;
	if (strcmp(rmCuteFldData.Rtyp, RTYP_BELT) == 0)
		rmCuteFldData.ResType = ResBLT;


	if (rmCuteFldData.Urno < 1)
	{
		if (llOldFldUrno > 0 && bpDdx)
		{
			ogDdx.DataChanged((void *)this, FLDCUTE_CHANGE, (void *)&rmCuteFldData);
		}
		return false;
	}
	else
	{
		if (bpDdx)
			ogDdx.DataChanged((void *)this, FLDCUTE_CHANGE, (void *)&rmCuteFldData);
		return true;
	}
}






/////////////////////////////////////////////////////////////////////////////////
////////// Broadcast handling

void  CedaFldData::ProcessFldBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{

	struct BcStruct *prlBcData = (struct BcStruct *) vpDataPointer;

	// get urno!!
	long llUrno = MyGetUrnoFromSelection(CString(prlBcData->Selection));
	CString olData(prlBcData->Data);
	CString olFields(prlBcData->Fields);
	if (llUrno < 1)
	{
		CString olUrno("");
		if (GetListItemByField(olData, olFields, CString("URNO"), olUrno))
		{
			llUrno = atol(olUrno);
		}
	}
	

	//TRACE("CedaFldData::ProcessFldBc: Sel:  %s\n", prlBcData->Selection);
	//TRACE("CedaFldData::ProcessFldBc: Fiel: %s\n", prlBcData->Fields);
	//TRACE("CedaFldData::ProcessFldBc: Data: %s\n", prlBcData->Data);

	// check CuteFldData
	switch(ipDDXType)
	{
	case BC_FLD_NEW:
		{
			// check CuteFldData
			ProcessFldBcNEW(olData, olFields);
		}
		break;
	case BC_FLD_CHANGE:
		{
			// check CuteFldData
			if (llUrno == rmCuteFldData.Urno)
			{
				// new CuteFldData
				ReadCuteRecord(rmCuteFldData.Rnam, rmCuteFldData.ResType, rmCuteFldData.Dseq);
			}
			
		}
		break;
	case BC_FLD_DELETE:
		{
			// check CuteFldData
			if (llUrno == rmCuteFldData.Urno)
			{
				// mark CuteFldData as deleted
				rmCuteFldData.Urno = 0;
				ogDdx.DataChanged((void *)this, FLDCUTE_CHANGE, (void *)&rmCuteFldData);
			}
			
		}
		break;
	default:
		break;
	}

}





bool CedaFldData::ProcessFldBcNEW(CString &ropData, CString &ropFields)
{

	// check CuteFldData

	CString olRnam;
	CString olRtyp;
	CString olDseq;

	if (GetListItemByField(ropData, ropFields, CString("RNAM"), olRnam) &&
		GetListItemByField(ropData, ropFields, CString("RTYP"), olRtyp) &&
		GetListItemByField(ropData, ropFields, CString("DSEQ"), olDseq))
	{
		if (olRnam == rmCuteFldData.Rnam &&
			olRtyp == rmCuteFldData.Rtyp)// && strcmp(olDseq, "1") == 0) 
		{
			// new CuteFldData
			return ReadCuteRecord(rmCuteFldData.Rnam, rmCuteFldData.ResType, atol(olDseq));
		}
	}			

	return false; 
}



