// BltFreeTextDlg.cpp : implementation file
//

#include "stdafx.h"
#include "cuteif.h"
#include "CcsGlobl.h"
#include "CCSBcHandle.h"
#include "CedaBasicData.h"
#include "BltFreeTextDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif



//--PROCESS-CF---------------------------------------------------------------------------------------------

static void  ProcessBC(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	((BltFreeTextDlg *)vpInstance)->ProcessBc(ipDDXType,vpDataPointer,ropInstanceName);
}




/////////////////////////////////////////////////////////////////////////////
// BltFreeTextDlg dialog


BltFreeTextDlg::BltFreeTextDlg(const CString &ropBltName, CWnd* pParent /*=NULL*/)
	: CDialog(BltFreeTextDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(BltFreeTextDlg)
	//}}AFX_DATA_INIT
    
	omBltName = ropBltName;
	CDialog::Create(BltFreeTextDlg::IDD, NULL);
}


void BltFreeTextDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(BltFreeTextDlg)
	DDX_Control(pDX, IDC_BLT_NAME, m_BltName);
	DDX_Control(pDX, IDC_FREETEXT2, m_FreeText2);
	DDX_Control(pDX, IDC_FREETEXT1, m_FreeText1);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(BltFreeTextDlg, CDialog)
	//{{AFX_MSG_MAP(BltFreeTextDlg)
	ON_BN_CLICKED(IDOK, OnSave)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// BltFreeTextDlg message handlers


BOOL BltFreeTextDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

	CString olText;
	olText.Format(GetString(IDS_STRING113), omBltName);
	m_BltName.SetWindowText(olText);

	ReloadData();
	
	SetWindowPos(&wndTopMost, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_SHOWWINDOW);
	SetWindowPos(&wndNoTopMost, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_SHOWWINDOW);

	ogDdx.Register((void *)this, FLDCUTE_CHANGE, CString("FLDCUTE_CHANGE"), CString("JobDataChange"), ProcessBC);
	ogDdx.Register((void *)this, FXT_CHANGE, CString("FXT_CHANGE"), CString("JobDataChange"), ProcessBC);

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}



void BltFreeTextDlg::ReloadData()
{

	// Load Fld-Values
	omFldData.ReadCuteRecord(omBltName, ResBLT, 0, false);
	pomFldRecord = omFldData.GetCuteRecord();

	// Init Edit Fields
	CString olFreeT1;
	CString olFreeT2;
	if (pomFldRecord->Urno > 0)
	{
		omFxtData.DBGetFreetext(pomFldRecord->Urno, 1, olFreeT1);
		omFxtData.DBGetFreetext(pomFldRecord->Urno, 2, olFreeT2);
	}
	m_FreeText1.SetWindowText(olFreeT1);
	m_FreeText2.SetWindowText(olFreeT2);

}



void BltFreeTextDlg::OnSave() 
{
	CCuteIFApp::TriggerWaitCursor(true);
	SaveData();

	ogDdx.UnRegister(this,NOTUSED);
	CCuteIFApp::TriggerWaitCursor(false);

	
	CDialog::OnOK();
}



bool BltFreeTextDlg::SaveData()
{

	if (pomFldRecord->Urno < 1)
	{
		// create new fld record
		pomFldRecord->Dseq = 0;
		omFldData.SaveCuteRecord();
		// Load Fld-Values
		omFldData.ReadCuteRecord(omBltName, ResBLT, 0, false);
		pomFldRecord = omFldData.GetCuteRecord();
		if (pomFldRecord->Urno < 1)
		{
			// creation of new fld record failed!!! 
			// ABORT
			return false;
		}
	}

	// save free texts
	CString olFreeT1;
	CString olFreeT2;
	
	m_FreeText1.GetWindowText(olFreeT1);
	m_FreeText2.GetWindowText(olFreeT2);

	bool blRet1 = omFxtData.DBUpdateFreetext(pomFldRecord->Urno, 1, olFreeT1);
	bool blRet2 = omFxtData.DBUpdateFreetext(pomFldRecord->Urno, 2, olFreeT2);

	return blRet1 && blRet2;
}



void BltFreeTextDlg::OnCancel() 
{
	ogDdx.UnRegister(this,NOTUSED);	
	CDialog::OnCancel();
}



void BltFreeTextDlg::ProcessBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	if (!pomFldRecord)
		return;

	if (ipDDXType == FXT_CHANGE)
	{
		CString olCurrFldu;
		olCurrFldu.Format("%ld", pomFldRecord->Urno);
		RecordSet *prlBcRecord = (RecordSet *) vpDataPointer;
		if ((*prlBcRecord)[ogBCD.GetFieldIndex("FXT", "FLDU")] == olCurrFldu)
		{
			ReloadData();
			return;
		}
	}
	if (ipDDXType == FLDCUTE_CHANGE)
	{
		ReloadData();
		return;
	}

}






