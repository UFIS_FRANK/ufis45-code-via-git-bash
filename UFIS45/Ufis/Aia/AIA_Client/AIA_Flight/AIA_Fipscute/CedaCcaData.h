// CedaCcaData.h

#ifndef __CEDACCADATA__
#define __CEDACCADATA__
 
#include "stdafx.h"
#include "CCSGlobl.h"
#include "CCSPtrArray.h"
#include "CCSCedadata.h"
#include "CCSddx.h"
#include "CedaAftData.h"

#define DBFIELDLEN_CCA_CKIC 5
#define DBFIELDLEN_CCA_CKIF 1
#define DBFIELDLEN_CCA_CKIT 1
#define DBFIELDLEN_CCA_CTYP 1
#define DBFIELDLEN_CCA_PRFL 1
#define DBFIELDLEN_CCA_USEC 32
#define DBFIELDLEN_CCA_USEU 32
#define DBFIELDLEN_CCA_REMA 60
#define DBFIELDLEN_CCA_DISP 60
#define DBFIELDLEN_CCA_ACT3 5
#define DBFIELDLEN_CCA_FLNO 9
#define DBFIELDLEN_CCA_STAT 10
#define DBFIELDLEN_CCA_CGRU 256


//---------------------------------------------------------------------------------------------------------

struct CCADATA 
{
	CTime 	 Cdat; 		// Erstellungsdatum
	CTime 	 Ckba; 		// Belegung Checkin-Schalter aktueller Beginn
	CTime 	 Ckbs; 		// Belegung Checkin-Schalter geplanter Beginn
	CTime 	 Ckea; 		// Belegung Checkin-Schalter aktuelles Ende
	CTime 	 Ckes; 		// Belegung Checkin-Schalter geplantes Ende
	char 	 Ckic[DBFIELDLEN_CCA_CKIC + 1]; 	// Checkin-Schalter
	char 	 Ckif[DBFIELDLEN_CCA_CKIF + 1]; 	// fester Checkin-Schalter
	char 	 Ckit[DBFIELDLEN_CCA_CKIT + 1]; 	// Terminal Checkin-Schalter
	char 	 Ctyp[DBFIELDLEN_CCA_CTYP + 1]; 	// Check-In Typ (Common oder Flight)
	long 	 Flnu; 	// Eindeutige Datensatz-Nr. des zugeteilten Fluges
	CTime 	 Lstu; 		// Datum letzte Änderung
	char 	 Prfl[DBFIELDLEN_CCA_PRFL + 1]; 	// Protokollierungskennzeichen
	long 	 Urno; 		// Eindeutige Datensatz-Nr.
	char 	 Usec[DBFIELDLEN_CCA_USEC + 1]; 	// Anwender (Ersteller)
	char 	 Useu[DBFIELDLEN_CCA_USEU + 1]; 	// Anwender (letzte Änderung)
	char	 Rema[DBFIELDLEN_CCA_REMA + 1];	// Bemerkung
	char	 Disp[DBFIELDLEN_CCA_DISP + 1];	// Bemerkung
	char	 Act3[DBFIELDLEN_CCA_ACT3 + 1];	// Act3 of Flight
	char     Flno[DBFIELDLEN_CCA_FLNO + 1];	// Flno of Flight
	char	 Stat[DBFIELDLEN_CCA_STAT + 1];	// Status of Cca for Conflicts
	long	 Ghpu;		// Urno von Ghp
	long     Gpmu;		// Urno von Gpm
	long     Ghsu;		// Urno von Ghs
	char	 Cgru[DBFIELDLEN_CCA_CGRU + 1];	// Urnos aus GRN
	CTime	 Stod;
	int 	 Copn	; 	// Schalteröffnungszeit (in Min. vor Abflug)


	CCADATA(void)
	{ 
		memset(this,'\0',sizeof(*this));
		Urno		=   0;
		Flnu		=   0;
		Ghpu		=	0;
		Gpmu		=	0;
		Ghsu		=	0;
		Cdat		=	TIMENULL;
		Ckba		=	TIMENULL;
		Ckbs		=	TIMENULL;
		Ckea		=	TIMENULL;
		Ckes		=	TIMENULL;
		Lstu		=	TIMENULL;
		Stod		=	TIMENULL;
		strcpy(Stat, "0000000000");
	}

	const CCADATA& operator= ( const CCADATA& s)
	{

		Cdat = s.Cdat; 		
		Ckba = s.Ckba; 		
		Ckbs = s.Ckbs; 		
		Ckea = s.Ckea; 		
		Ckes = s.Ckes;
		Lstu = s.Lstu; 		
		Flnu = s.Flnu; 	
		Urno = s.Urno; 		
		Ghpu = s.Ghpu;		
		Gpmu = s.Gpmu;		
		Ghsu = s.Ghsu;		
		Copn = s.Copn;	 	
		Stod = s.Stod;	 	

		 strncpy(Ckic,s.Ckic, DBFIELDLEN_CCA_CKIC); 	
		 strncpy(Ckif,s.Ckif, DBFIELDLEN_CCA_CKIF); 	
		 strncpy(Ckit,s.Ckit, DBFIELDLEN_CCA_CKIT); 	
		 strncpy(Ctyp,s.Ctyp, DBFIELDLEN_CCA_CTYP); 	
		 strncpy(Prfl,s.Prfl, DBFIELDLEN_CCA_PRFL); 	
		 strncpy(Usec,s.Usec, DBFIELDLEN_CCA_USEC); 	
		 strncpy(Useu,s.Useu, DBFIELDLEN_CCA_USEU); 	
		 strncpy(Rema,s.Rema, DBFIELDLEN_CCA_REMA);	
		 strncpy(Disp,s.Disp, DBFIELDLEN_CCA_DISP);	
		 strncpy(Act3,s.Act3, DBFIELDLEN_CCA_ACT3);	
		 strncpy(Flno,s.Flno, DBFIELDLEN_CCA_FLNO);	
		 strncpy(Stat,s.Stat, DBFIELDLEN_CCA_STAT);	
		 strncpy(Cgru,s.Cgru, DBFIELDLEN_CCA_CGRU);	


		return *this;

	}


}; // end CcaDataStrukt

//---------------------------------------------------------------------------------------------------------
// Class declaratino

class CedaCcaData: public CCSCedaData
{
// Attributes
public:
    CMapPtrToPtr omUrnoMap;
    CPtrArray omData;
	char pcmListOfFields[2048];

// Operations
public:
    CedaCcaData();
	~CedaCcaData();
	void Register(void);
	void UnRegister(void);
	void ClearAll(bool bpWithRegistration = true);
    bool Read(CString opWhere, bool bpDdx = false);
	bool ReadCkic(const CString &ropCkic, bool bpDdx = true);
	bool ReadCkic(const CString &ropCkic, long lpFlnu, bool bpDdx = true);
	bool ReadUrno(long lpUrno, const CString &ropCkic, bool bpDdx /* = true */);

	//CCADATA *ReadCuteRecord(const CString &ropCkic);

	void ProcessBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);
	CCADATA *GetRecByUrno(long lpUrno) const;
	CCADATA *GetRecByFlnuCkic(long lpFlnu, const CString &ropCkic) const;
	bool GetAlc3FromRelFlight(const CCADATA *prpCca, CString &ropAlc) const;
	//bool GetOpenCcas(CPtrArray *popOpenCcas) const;
	bool GetMostRecentScheduled(CPtrArray *popCcas);
	bool SaveDiff(CCADATA *prpNewCca);


	// Private methods
private:
	CedaAftData omAftData;   // all flight of the loaded ccas
	
	//CCADATA rmCuteCcaData;
	CString omCurrCkic;

	bool ReadAllRelFlights();
	bool Save(CCADATA *prpCca);


	//bool IsOpen(const CCADATA &ropCca) const;

	bool InsertInternal(CCADATA *prpCca, bool bpDdx = true);
	bool UpdateInternal(CCADATA *prpCca, bool bpDdx = true);
	bool DeleteInternal(CCADATA *prpCca, bool bpDdx = true);

};

//---------------------------------------------------------------------------------------------------------

extern CedaCcaData ogCcaData;

#endif //__CEDACCADATA__
