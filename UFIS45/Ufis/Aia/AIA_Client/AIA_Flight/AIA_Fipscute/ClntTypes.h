/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * 
 *	UFISAppMang:	UFIS Client Application Manager Project
 *
 *	Proxy Communicator for Client Applications
 *	
 *	ABB Airport Technologies GmbH 2000
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 *		Author			Date			Comment
 *	------------------------------------------------------------------
 *
 *		cla	AAT/IR		02/09/2000		Initial version
 *		cla	AAT/IR		19/10/2000		Type for FipsCUTE added
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
// ClntTypes.h: General Definition of client types for C++
//				These definitions must be transfered to other 
//              languages like VB ...
//				
//////////////////////////////////////////////////////////////////////
#ifndef _CLNT_TYPES_H_
#define _CLNT_TYPES_H_

enum ClntTags
{
	TlxPool = 1,
	Fips,
	UFISAmMon,
	FipsCUTE
};

#endif