@echo off
REM -- First make map file from Microsoft Visual C++ generated resource.h
echo // MAKEHELP.BAT generated Help Map file.  Used by CUTEIF.HPJ. >"hlp\CuteIF.hm"
echo. >>"hlp\CuteIF.hm"
echo // Commands (ID_* and IDM_*) >>"hlp\CuteIF.hm"
makehm ID_,HID_,0x10000 IDM_,HIDM_,0x10000 resource.h >>"hlp\CuteIF.hm"
echo. >>"hlp\CuteIF.hm"
echo // Prompts (IDP_*) >>"hlp\CuteIF.hm"
makehm IDP_,HIDP_,0x30000 resource.h >>"hlp\CuteIF.hm"
echo. >>"hlp\CuteIF.hm"
echo // Resources (IDR_*) >>"hlp\CuteIF.hm"
makehm IDR_,HIDR_,0x20000 resource.h >>"hlp\CuteIF.hm"
echo. >>"hlp\CuteIF.hm"
echo // Dialogs (IDD_*) >>"hlp\CuteIF.hm"
makehm IDD_,HIDD_,0x20000 resource.h >>"hlp\CuteIF.hm"
echo. >>"hlp\CuteIF.hm"
echo // Frame Controls (IDW_*) >>"hlp\CuteIF.hm"
makehm IDW_,HIDW_,0x50000 resource.h >>"hlp\CuteIF.hm"
REM -- Make help for Project CUTEIF


echo Building Win32 Help files
start /wait hcw /C /E /M "hlp\CuteIF.hpj"
if errorlevel 1 goto :Error
if not exist "hlp\CuteIF.hlp" goto :Error
if not exist "hlp\CuteIF.cnt" goto :Error
echo.
if exist Debug\nul copy "hlp\CuteIF.hlp" Debug
if exist Debug\nul copy "hlp\CuteIF.cnt" Debug
if exist Release\nul copy "hlp\CuteIF.hlp" Release
if exist Release\nul copy "hlp\CuteIF.cnt" Release
echo.
goto :done

:Error
echo hlp\CuteIF.hpj(1) : error: Problem encountered creating help file

:done
echo.
