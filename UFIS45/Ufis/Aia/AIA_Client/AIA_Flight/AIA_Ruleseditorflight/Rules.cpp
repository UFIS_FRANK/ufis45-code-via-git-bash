// Rules.cpp : Defines the class behaviors for the application.
//

#include "stdafx.h"
#include "Rules.h"
#include "ButtonListDlg.h"
#include "InitialLoadDlg.h"
#include "LoginDlg.h"
#include "BasicData.h"
#include "BackGround.h"

//------------------------------
#include "CedaPSTData.h"
#include "CedaHTYData.h"
#include "CedaGATData.h"
#include "CedaCICData.h"
#include "CedaBLTData.h"
#include "CedaALTData.h"
#include "CedaAPTData.h"
#include "CedaACTData.h"
#include "CedaACRData.h"
#include "CedaGhsData.h"
#include "CedaGegData.h"
#include "CedaPerData.h"
#include "CedaGrmData.h"
#include "CedaGrnData.h"
#include "CedaGhpData.h"
#include "CedaGpmData.h"
#include "CedaPfcData.h"
#include "CedaPrcData.h"


// start bch
#include "CedaNATData.h"
#include "CedaACTData.h"
#include "CedaALTData.h"
#include "CedaAPTData.h"
#include "CedaBLTData.h"
#include "CedaPSTData.h"
#include "CedaGATData.h"
#include "CedaHTYData.h"

#include "CedaAloData.h"
#include "CedaSgrData.h"
#include "CedaSgmData.h"

//------------------------------
#include "DutyPreferences.h"
#include "RegisterDlg.h"
#include "PrivList.h"




#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif



/////////////////////////////////////////////////////////////////////////////
// CRulesApp

BEGIN_MESSAGE_MAP(CRulesApp, CWinApp)
	//{{AFX_MSG_MAP(CRulesApp)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	ON_BN_CLICKED(IDCANCEL, OnCancel)
	//}}AFX_MSG
	ON_COMMAND(ID_HELP, CWinApp::OnHelp)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CRulesApp construction

CRulesApp::CRulesApp()
{
	pogInitialLoad = NULL;
}

CRulesApp::~CRulesApp()
{
	TRACE("CCSPrjApp::~CCSPrjApp\n");
	if (pogDutyPreferences)
	{
		pogDutyPreferences->DestroyWindow();
		delete pogDutyPreferences;
		pogDutyPreferences = NULL;
	}
	DeleteBrushes();
}

/////////////////////////////////////////////////////////////////////////////
// The one and only CRulesApp object

CRulesApp theApp;

/////////////////////////////////////////////////////////////////////////////
// CRulesApp initialization

BOOL CRulesApp::InitInstance()
{
	// Initialize OLE libraries
	if (!AfxOleInit())
	{
		AfxMessageBox(IDP_OLE_INIT_FAILED);
		return FALSE;
	}

	AfxEnableControlContainer();

	// Standard initialization
	// If you are not using these features and wish to reduce the size
	//  of your final executable, you should remove from the following
	//  the specific initialization routines you do not need.

	#ifdef _AFXDLL
		Enable3dControls();			// Call this when using MFC in a shared DLL
	#else
		Enable3dControlsStatic();	// Call this when linking to MFC statically
	#endif

	// Parse the command line to see if launched as OLE server
	if (RunEmbedded() || RunAutomated())
	{
		// Register all OLE server (factories) as running.  This enables the
		//  OLE libraries to create objects from other applications.
		COleTemplateServer::RegisterAll();
	}
	else
	{
		// When a server application is launched stand-alone, it is a good idea
		//  to update the system registry in case it has been damaged.
		COleObjectFactory::UpdateRegistryAll();
	}

	/////////////////////////////////////////////////////////////////////////////
	// Standard CCS-Initialization
	/////////////////////////////////////////////////////////////////////////////

//	GXInit();
	CStringArray olCmdLineStghArray;
	olCmdLineStghArray.RemoveAll();
	CString olCmdLine =  LPCTSTR(m_lpCmdLine);
	//m_lpCmdLine => "AppName,UserID,Password"
	if(olCmdLine.GetLength() > 0)
	{
		if(ExtractItemList(olCmdLine,&olCmdLineStghArray) != 2)
		{
			MessageBox(NULL,GetString(IDS_STRING325),GetString(IDS_STRING326),MB_ICONERROR);
			return FALSE;
		}
	}



	// Standard CCS-Fonts and Brushes initialization
    InitFont();
	CreateBrushes();

	char pclConfigPath[142];
    if (getenv("CEDA") == NULL)
        strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
    else
        strcpy(pclConfigPath, getenv("CEDA"));

	char pclHome[64];

    GetPrivateProfileString("GLOBAL", "TABLEEXTENSION", "HAJ", pcgTableExt, sizeof pcgTableExt, pclConfigPath);
    GetPrivateProfileString("GLOBAL", "HOMEAIRPORT", "HAJ", pclHome, sizeof pclHome, pclConfigPath);


	strcpy(CCSCedaData::pcmTableExt, pcgTableExt);
	strcpy(CCSCedaData::pcmHomeAirport, pclHome);
	strcpy(CCSCedaData::pcmApplName, "RULES");

	ogBcHandle.SetHomeAirport(pclHome);


	// CCS-Desktop background with an bitmap (logo or copyright etc.. 
	// loaded in the methode CBackGround::OnPaint)
//	pomBackGround = new CBackGround;
//	pomBackGround->Create(NULL, IDD_BACKGROUND);





    if (ogCommHandler.Initialize() != true)  // connection error?
    {
        AfxMessageBox(GetString(IDS_STRING329) + ogCommHandler.LastError());
        return FALSE;
    }

	//if(MessageBox(NULL,"Only for internal use.\n Continue?","Warning!!!!!!!!!", MB_YESNO) == IDNO)
	//	return 0;



	// Register your broadcasts here
	ogBcHandle.AddTableCommand(CString("SGM") + pcgTableExt, CString("IRT"), BC_SGM_NEW, false);
	ogBcHandle.AddTableCommand(CString("SGM") + pcgTableExt, CString("URT"), BC_SGM_CHANGE, false);
	ogBcHandle.AddTableCommand(CString("SGM") + pcgTableExt, CString("DRT"), BC_SGM_DELETE, false);
	
	ogBcHandle.AddTableCommand(CString("SGR") + pcgTableExt, CString("IRT"), BC_SGR_NEW, false);
	ogBcHandle.AddTableCommand(CString("SGR") + pcgTableExt, CString("URT"), BC_SGR_CHANGE, false);
	ogBcHandle.AddTableCommand(CString("SGR") + pcgTableExt, CString("DRT"), BC_SGR_DELETE, false);



	CLoginDialog olLoginDlg(pcgTableExt,ogAppName,pomBackGround);



	if(olCmdLineStghArray.GetSize() == 2)
	{
		strcpy(CCSCedaData::pcmReqId, ogCommHandler.pcmReqId);
		strcpy(CCSCedaData::pcmUser, olCmdLineStghArray.GetAt(0));

		ogBasicData.omUserID = olCmdLineStghArray.GetAt(0);
		ogCommHandler.SetUser(olCmdLineStghArray.GetAt(0));
				
		if(!olLoginDlg.Login(olCmdLineStghArray.GetAt(0),olCmdLineStghArray.GetAt(1) ) )
			return FALSE;

	}
	else
	{
		if( olLoginDlg.DoModal() != IDCANCEL )
		{
			//strcpy(pcgUser,olLoginDlg.omUsername);
			//strcpy(pcgPasswd,olLoginDlg.omPassword);
			ogBasicData.omUserID = olLoginDlg.omUsername;
			ogCommHandler.SetUser(olLoginDlg.omUsername);
			int ilStartApp = IDOK;
			if(ogPrivList.GetStat("InitModu") == '1')
			{
				RegisterDlg olRegisterDlg;
				ilStartApp = olRegisterDlg.DoModal();
			}
			if(ilStartApp != IDOK)
			{
				return FALSE;
			}

		}
		else
		{
			return FALSE;
		}
	}




//	CLoginDlg olLoginDlg;
//	if (olLoginDlg.DoModal() != IDOK)
//        return FALSE;


	// InitialLoadDialog
    InitialLoad(NULL);


	// should the buttonlist be movable ?
	bgIsButtonListMovable = false;

	// everything ok, we can display the buttonlist
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));		
	char pclCaption[256]="";
	strcpy(pclCaption, GetString(IDS_STRING102));
	pogDutyPreferences = new DutyPreferences(IDS_STRING102, NULL, 0);
	pogDutyPreferences->m_psh.dwFlags &= ~(PSP_HASHELP);
	pogDutyPreferences->m_psh.dwFlags |= PSH_NOAPPLYNOW;

	int nResponse = pogDutyPreferences->DoModal();
	if (nResponse == IDOK || IDCANCEL)
	{
		if (pogDutyPreferences)
		{
			pogDutyPreferences->DestroyWindow();
			delete pogDutyPreferences;
			pogDutyPreferences = NULL;
		}
	}

		// TODO: Place code here to handle when the dialog is
		//  dismissed with OK
//	pogDutyPreferences->EndDialog(IDOK);EndModal

/*	CButtonListDlg dlg(pomBackGround);
	m_pMainWnd = &dlg;
	int nResponse = dlg.DoModal();
	if (nResponse == IDOK)
	{
		// TODO: Place code here to handle when the dialog is
		//  dismissed with OK
	}
	else if (nResponse == IDCANCEL)
	{
		// TODO: Place code here to handle when the dialog is
		//  dismissed with Cancel
	}
*/
	// Since the dialog has been closed, return FALSE so that we exit the
	//  application, rather than start the application's message pump.
	return FALSE;
}

void CRulesApp::OnCancel()
{
	if (pogDutyPreferences)
	{
		pogDutyPreferences->DestroyWindow();
		delete pogDutyPreferences;
		pogDutyPreferences = NULL;
	}
}

void CRulesApp::InitialLoad(CWnd *pParent)
{
/*
CedaACRData.h
CedaACTData.h
CedaALTData.h
CedaAPTData.h
CedaBLTData.h
CedaBsdData.h
CedaCfgData.h
CedaCICData.h
CedaGATData.h
CedaGegData.h
CedaGhpData.h
CedaGhsData.h
CedaGpmData.h
CedaGrmData.h
CedaGrnData.h
CedaHTYData.h
CedaNATData.h
CedaPerData.h
CedaPfcData.h
CedaPrcData.h
CedaPSTData.h
*/
	ogBasicData.omUserID = CString("Rules");
	ogCicData.SetTableName(CString(CString("CIC") + CString(pcgTableExt)));
	ogCfgData.SetTableName(CString(CString("VCD") + CString(pcgTableExt)));
	ogAcrData.SetTableName(CString(CString("ACR") + CString(pcgTableExt)));
	ogActData.SetTableName(CString(CString("ACT") + CString(pcgTableExt)));
	ogAltData.SetTableName(CString(CString("ALT") + CString(pcgTableExt)));
	ogAptData.SetTableName(CString(CString("APT") + CString(pcgTableExt)));
	ogBltData.SetTableName(CString(CString("BLT") + CString(pcgTableExt)));
	ogBsdData.SetTableName(CString(CString("BSD") + CString(pcgTableExt)));
	ogGatData.SetTableName(CString(CString("GAT") + CString(pcgTableExt)));
	ogGegData.SetTableName(CString(CString("GEG") + CString(pcgTableExt)));
	ogGhpData.SetTableName(CString(CString("GHP") + CString(pcgTableExt)));
	ogGhsData.SetTableName(CString(CString("GHS") + CString(pcgTableExt)));
	ogGpmData.SetTableName(CString(CString("GPM") + CString(pcgTableExt)));
	ogGrmData.SetTableName(CString(CString("GRM") + CString(pcgTableExt)));
	ogGrnData.SetTableName(CString(CString("GRN") + CString(pcgTableExt)));
	ogHtyData.SetTableName(CString(CString("HTY") + CString(pcgTableExt)));
	ogNatData.SetTableName(CString(CString("NAT") + CString(pcgTableExt)));
	ogPerData.SetTableName(CString(CString("PER") + CString(pcgTableExt)));
	ogPfcData.SetTableName(CString(CString("PFC") + CString(pcgTableExt)));
	ogPrcData.SetTableName(CString(CString("PRC") + CString(pcgTableExt)));
	ogPstData.SetTableName(CString(CString("PST") + CString(pcgTableExt)));

	pogInitialLoad = new CInitialLoadDlg(pParent);
	if (pogInitialLoad != NULL)
	{
		pogInitialLoad->SetMessage(LoadStg(IDS_STRING130));
	}
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));

	bgIsInitialized = false;

	ogGhsData.Read();
	pogInitialLoad->SetMessage(LoadStg(IDS_STRING131));
	ogGhpData.Read(" WHERE APPL = 'POPS'");
	pogInitialLoad->SetProgress(2);
	pogInitialLoad->SetMessage(LoadStg(IDS_STRING132));
	ogGpmData.Read();
	pogInitialLoad->SetMessage(LoadStg(IDS_STRING133));
	ogPrcData.Read();
//	ogCfgData.ReadCfgData();
//	ogCfgData.SetCfgData();
//	ogCfgData.ReadMonitorSetup();
	ogPfcData.Read();
	ogGegData.Read();
	pogInitialLoad->SetMessage(LoadStg(IDS_STRING134));
	ogActData.ReadAllACTs("");
	pogInitialLoad->SetMessage(LoadStg(IDS_STRING135));
	ogAptData.ReadAllAPTs();
	pogInitialLoad->SetMessage(LoadStg(IDS_STRING136));
	ogAltData.ReadAllALTs();
	pogInitialLoad->SetMessage(LoadStg(IDS_STRING137));
	ogPstData.ReadAllPSTs();
	ogGrnData.Read(" WHERE APPL='POPS'");
	ogGrmData.Read();
	pogInitialLoad->SetMessage(LoadStg(IDS_STRING138));
	ogNatData.ReadAllNATs();
	pogInitialLoad->SetProgress(2);
	pogInitialLoad->SetProgress(2);
	pogInitialLoad->SetProgress(2);
	pogInitialLoad->SetProgress(2);
	pogInitialLoad->SetMessage(LoadStg(IDS_STRING140));
	ogBltData.ReadAllBLTs();
	pogInitialLoad->SetProgress(2);
	pogInitialLoad->SetMessage(LoadStg(IDS_STRING141));
	pogInitialLoad->SetMessage(LoadStg(IDS_STRING142));
	ogGatData.ReadAllGATs();
	pogInitialLoad->SetProgress(2);
	pogInitialLoad->SetMessage(LoadStg(IDS_STRING143));
	ogHtyData.ReadAllHTYs();
	pogInitialLoad->SetProgress(2);
	//pogInitialLoad->SetMessage("Check-in counters werden geladen");
	//ogCcaData.Read();
	pogInitialLoad->SetProgress(2);
	pogInitialLoad->SetMessage(LoadStg(IDS_STRING144));
	ogPerData.Read();
	pogInitialLoad->SetProgress(2);
	pogInitialLoad->SetMessage(LoadStg(IDS_STRING145));
	ogPfcData.Read();
	pogInitialLoad->SetProgress(2);
	pogInitialLoad->SetMessage(LoadStg(IDS_STRING146));
	ogBsdData.Read();
	pogInitialLoad->SetProgress(2);
	pogInitialLoad->SetMessage(LoadStg(IDS_STRING147));

	ogAloData.ReadAloData();
	ogSgrData.Read();
	ogSgmData.Read();

    /////////////////////////////////////////////////////////////////////////////////////
    // add here your cedaXXXdata read methods
    /////////////////////////////////////////////////////////////////////////////////////

		
	if (pogInitialLoad != NULL)
	{
		pogInitialLoad->DestroyWindow();
		pogInitialLoad = NULL;
	}

}
