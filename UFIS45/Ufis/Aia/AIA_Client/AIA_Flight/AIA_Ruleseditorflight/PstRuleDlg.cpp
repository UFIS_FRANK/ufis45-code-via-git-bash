// PstRuleDlg.cpp : implementation file
//

#include <stdafx.h>
#include "Rules.h"
#include "PstRuleDlg.h"

#include "CedaAltData.h"
#include "CedaActData.h"
#include "CedapstData.h"
#include "resrc1.h"
#include "PositionMatrixDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#include "BasicData.h"

/////////////////////////////////////////////////////////////////////////////
// PstRuleDlg dialog


PstRuleDlg::PstRuleDlg(CWnd* pParent, PSTDATA *popPst)
	: CDialog(PstRuleDlg::IDD, pParent)
{
	pomPst = popPst;

	//{{AFX_DATA_INIT(PstRuleDlg)
	m_AL_List = _T("");
	m_Exce = _T("");
	m_Heig_Max = _T("");
	m_Heig_Min = _T("");
	m_Leng_Max = _T("");
	m_Leng_Min = _T("");
	m_Max_Ac = _T("");
	m_Nat_Restr = _T("");
	m_Npos1 = _T("");
	m_Npos2 = _T("");
	m_Npos3 = _T("");
	m_Npos4 = _T("");
	m_Npos5 = _T("");
	m_Npos6 = _T("");
	m_Npos7 = _T("");
	m_Npos8 = _T("");
	m_Npos9 = _T("");
	m_Npos10= _T("");
	m_Prio = _T("");
	m_Restr_To1 = _T("");
	m_Restr_To2 = _T("");
	m_Restr_To3 = _T("");
	m_Restr_To4 = _T("");
	m_Restr_To5 = _T("");
	m_Restr_To6 = _T("");
	m_Restr_To7 = _T("");
	m_Restr_To8 = _T("");
	m_Restr_To9 = _T("");
	m_Restr_To10= _T("");
	m_Span_Max = _T("");
	m_Span_Min = _T("");
	//}}AFX_DATA_INIT
	CStringArray olValues;
//	ExtractItemList(pomPst->Acgr, &olValues, '/');
	CString olTmpPosr = popPst->Posr;
	if(olTmpPosr.Replace(";","#") < 3)
	{
		olTmpPosr = popPst->Posr;
		olTmpPosr.Replace("/",";");
		strcpy(pomPst->Posr, olTmpPosr.GetBuffer(0));
	}
	
	ExtractItemList(popPst->Posr, &olValues, ';');

	if (olValues.GetSize() == 12)	// old format
	{
		for(int i = 0; i < olValues.GetSize(); i++)
		{
			olValues[i].TrimRight();
			CStringArray olItems;
			ExtractItemList(olValues[i], &olItems, '-');
			if(i == 0)//wing span
			{
				if(olItems.GetSize() == 2)
				{
					m_Span_Min = olItems[0];
					m_Span_Max = olItems[1];
				}
			}
			if(i == 1)//length
			{
				if(olItems.GetSize() == 2)
				{
					m_Leng_Min = olItems[0];
					m_Leng_Max = olItems[1];
				}
			}
			if(i==2)//height
			{
				if(olItems.GetSize() == 2)
				{
					m_Heig_Min = olItems[0];
					m_Heig_Max = olItems[1];
				}
			}
		}
		//handle the rest
		m_Exce		=	olValues[3];
		m_Npos1		=	olValues[4];
		m_Restr_To1	=	olValues[5];
		m_Npos2		=	olValues[6];
		m_Restr_To2	=	olValues[7];
		m_Max_Ac	=	olValues[8];
		m_Prio		=	olValues[9];
		m_Nat_Restr	=	olValues[10];
		m_AL_List	=	olValues[11];

		m_Restr_To3	=	"0";
		m_Restr_To4	=	"0";
		m_Restr_To5	=	"0";
		m_Restr_To6	=	"0";
		m_Restr_To7	=	"0";
		m_Restr_To8	=	"0";
		m_Restr_To9	=	"0";
		m_Restr_To10=	"0";

	}
	else if(olValues.GetSize() == 28)
	{
		for(int i = 0; i < olValues.GetSize(); i++)
		{
			olValues[i].TrimRight();
			CStringArray olItems;
			ExtractItemList(olValues[i], &olItems, '-');
			if(i == 0)//wing span
			{
				if(olItems.GetSize() == 2)
				{
					m_Span_Min = olItems[0];
					m_Span_Max = olItems[1];
				}
			}
			if(i == 1)//length
			{
				if(olItems.GetSize() == 2)
				{
					m_Leng_Min = olItems[0];
					m_Leng_Max = olItems[1];
				}
			}
			if(i==2)//height
			{
				if(olItems.GetSize() == 2)
				{
					m_Heig_Min = olItems[0];
					m_Heig_Max = olItems[1];
				}
			}
		}
		//handle the rest
		m_Exce		=	olValues[3];
		m_Npos1		=	olValues[4];
		m_Restr_To1	=	olValues[5];
		m_Npos2		=	olValues[6];
		m_Restr_To2	=	olValues[7];
		m_Max_Ac	=	olValues[8];
		m_Prio		=	olValues[9];
		m_Nat_Restr	=	olValues[10];
		m_AL_List	=	olValues[11];

		m_Npos3		=	olValues[12];
		m_Restr_To3	=	olValues[13];
		m_Npos4		=	olValues[14];
		m_Restr_To4	=	olValues[15];
		m_Npos5		=	olValues[16];
		m_Restr_To5	=	olValues[17];
		m_Npos6		=	olValues[18];
		m_Restr_To6	=	olValues[19];
		m_Npos7		=	olValues[20];
		m_Restr_To7	=	olValues[21];
		m_Npos8		=	olValues[22];
		m_Restr_To8	=	olValues[23];
		m_Npos9		=	olValues[24];
		m_Restr_To9	=	olValues[25];
		m_Npos10	=	olValues[26];
		m_Restr_To10=	olValues[27];

	}
}


void PstRuleDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(PstRuleDlg)
	DDX_Control(pDX, IDC_MATRIX1, m_Matrix1);
	DDX_Text(pDX, IDC_AL_LIST, m_AL_List);
	DDV_MaxChars(pDX, m_AL_List, 150);
	DDX_Text(pDX, IDC_EXCE, m_Exce);
	DDV_MaxChars(pDX, m_Exce, 150);
	DDX_Text(pDX, IDC_HEIG_MAX, m_Heig_Max);
	DDV_MaxChars(pDX, m_Heig_Max, 4);
	DDX_Text(pDX, IDC_HEIG_MIN, m_Heig_Min);
	DDV_MaxChars(pDX, m_Heig_Min, 4);
	DDX_Text(pDX, IDC_LENG_MAX, m_Leng_Max);
	DDV_MaxChars(pDX, m_Leng_Max, 5);
	DDX_Text(pDX, IDC_LENG_MIN, m_Leng_Min);
	DDV_MaxChars(pDX, m_Leng_Min, 5);
	DDX_Text(pDX, IDC_MAX_AC, m_Max_Ac);
	DDX_Text(pDX, IDC_NAT_RESTR, m_Nat_Restr);
	DDV_MaxChars(pDX, m_Nat_Restr, 150);
	DDX_Text(pDX, IDC_NPOS1, m_Npos1);
	DDV_MaxChars(pDX, m_Npos1, 5);
	DDX_Text(pDX, IDC_NPOS2, m_Npos2);
	DDV_MaxChars(pDX, m_Npos2, 5);
	DDX_Text(pDX, IDC_NPOS3, m_Npos3);
	DDV_MaxChars(pDX, m_Npos3, 5);
	DDX_Text(pDX, IDC_NPOS4, m_Npos4);
	DDV_MaxChars(pDX, m_Npos4, 5);
	DDX_Text(pDX, IDC_NPOS5, m_Npos5);
	DDV_MaxChars(pDX, m_Npos5, 5);
	DDX_Text(pDX, IDC_NPOS6, m_Npos6);
	DDV_MaxChars(pDX, m_Npos6, 5);
	DDX_Text(pDX, IDC_NPOS7, m_Npos7);
	DDV_MaxChars(pDX, m_Npos7, 5);
	DDX_Text(pDX, IDC_NPOS8, m_Npos8);
	DDV_MaxChars(pDX, m_Npos8, 5);
	DDX_Text(pDX, IDC_NPOS9, m_Npos9);
	DDV_MaxChars(pDX, m_Npos9, 5);
	DDX_Text(pDX, IDC_NPOS10, m_Npos10);
	DDV_MaxChars(pDX, m_Npos10, 5);
	DDX_Text(pDX, IDC_PRIO, m_Prio);
	DDV_MaxChars(pDX, m_Prio, 1);
	DDX_Text(pDX, IDC_RESTR_TO1, m_Restr_To1);
	DDX_Text(pDX, IDC_RESTR_TO2, m_Restr_To2);
	DDX_Text(pDX, IDC_RESTR_TO3, m_Restr_To3);
	DDX_Text(pDX, IDC_RESTR_TO4, m_Restr_To4);
	DDX_Text(pDX, IDC_RESTR_TO5, m_Restr_To5);
	DDX_Text(pDX, IDC_RESTR_TO6, m_Restr_To6);
	DDX_Text(pDX, IDC_RESTR_TO7, m_Restr_To7);
	DDX_Text(pDX, IDC_RESTR_TO8, m_Restr_To8);
	DDX_Text(pDX, IDC_RESTR_TO9, m_Restr_To9);
	DDX_Text(pDX, IDC_RESTR_TO10,m_Restr_To10);
	DDX_Text(pDX, IDC_SPAN_MAX, m_Span_Max);
	DDV_MaxChars(pDX, m_Span_Max, 5);
	DDX_Text(pDX, IDC_SPAN_MIN, m_Span_Min);
	DDV_MaxChars(pDX, m_Span_Min, 5);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(PstRuleDlg, CDialog)
	//{{AFX_MSG_MAP(PstRuleDlg)
	ON_BN_CLICKED(IDC_MATRIX1, OnMatrix1)
	ON_BN_CLICKED(IDC_MATRIX2, OnMatrix2)
	ON_BN_CLICKED(IDC_MATRIX3, OnMatrix3)
	ON_BN_CLICKED(IDC_MATRIX4, OnMatrix4)
	ON_BN_CLICKED(IDC_MATRIX5, OnMatrix5)
	ON_BN_CLICKED(IDC_MATRIX6, OnMatrix6)
	ON_BN_CLICKED(IDC_MATRIX7, OnMatrix7)
	ON_BN_CLICKED(IDC_MATRIX8, OnMatrix8)
	ON_BN_CLICKED(IDC_MATRIX9, OnMatrix9)
	ON_BN_CLICKED(IDC_MATRIX10,OnMatrix10)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// PstRuleDlg message handlers

void PstRuleDlg::OnOK() 
{
	// TODO: Add extra validation here
	bool blOK=true;
	int i=0;
	CString olMsg;
	CString olTmp;

	CString olSpan,
			olLength,
			olHeight;
	

	UpdateData(TRUE);

	CStringArray olArray;
	ExtractItemList(m_AL_List, &olArray, ' ');

	CString olTmp2;	
	for(i = 0; i < olArray.GetSize(); i++)
	{
		olTmp2 = olArray[i].GetBuffer(0);

		if(olTmp2.GetLength() > 1)
		{
			if(olTmp2[0] == '!' || olTmp2[0] == '=')
				olTmp2 = olTmp2.Right(olTmp2.GetLength() - 1);


			ALTDATA *prlAlt = ogAltData.GetAltByAlc2(olTmp2.GetBuffer(0));
			if(prlAlt == NULL)
			{
				prlAlt = ogAltData.GetAltByAlc3(olTmp2.GetBuffer(0));
				if(prlAlt == NULL)
				{
					olTmp.Format(GetString(IDS_STRING1430), olTmp2);
					olMsg+=olTmp;
				}
			}
		}
	}
	ExtractItemList(m_Exce, &olArray, ' ');
	for(i = 0; i < olArray.GetSize(); i++)
	{

		olTmp2 = olArray[i].GetBuffer(0);

		if(olTmp2.GetLength() > 1)
		{
			if(olTmp2[0] == '!' || olTmp2[0] == '=')
				olTmp2 = olTmp2.Right(olTmp2.GetLength() - 1);
		
		
			ACTDATA *prlAct = ogActData.GetActByAct3(olTmp2.GetBuffer(0));
			if(prlAct == NULL)
			{
				prlAct = ogActData.GetActByAct5(olTmp2.GetBuffer(0));
				if(prlAct == NULL)
				{
					olTmp.Format(GetString(IDS_STRING1431), olTmp2);
					olMsg+=olTmp;
				}
			}
		}
	}

	olSpan = m_Span_Max+"-"+m_Span_Min;
	olHeight = m_Heig_Max+"-"+m_Heig_Min;
	olLength = m_Leng_Max+"-"+m_Leng_Min;
	if(m_Max_Ac.IsEmpty())
	{
		m_Max_Ac = "1";
	}
	if(m_Prio.IsEmpty())
	{
		m_Prio = "0";
	}
	
	if(	!m_Npos1.IsEmpty())
	{
		PSTDATA *prlPst = ogPstData.GetPstByPnam(m_Npos1.GetBuffer(0));
		if(prlPst == NULL)
		{
			olTmp.Format(GetString(IDS_STRING1432), m_Npos1.GetBuffer(0));
			olMsg+=olTmp;
		}
	}
	else
	{
		if(	m_Restr_To1.IsEmpty())
		{
			m_Restr_To1	=	"0";
//			olMsg+=GetString(IDS_STRING1433);
		}
	}
	if(	!m_Npos2.IsEmpty())
	{
		PSTDATA *prlPst = ogPstData.GetPstByPnam(m_Npos2.GetBuffer(0));
		if(prlPst == NULL)
		{
			olTmp.Format(GetString(IDS_STRING1432), m_Npos2.GetBuffer(0));
			olMsg+=olTmp;
		}
	}
	else
	{
		if(	m_Restr_To2.IsEmpty())
		{
			m_Restr_To2	=	"0";
//			olMsg+=GetString(IDS_STRING1434);
		}
	}
	if(	!m_Npos3.IsEmpty())
	{
		PSTDATA *prlPst = ogPstData.GetPstByPnam(m_Npos3.GetBuffer(0));
		if(prlPst == NULL)
		{
			olTmp.Format(GetString(IDS_STRING1432), m_Npos3.GetBuffer(0));
			olMsg+=olTmp;
		}
	}
	else
	{
		if(	m_Restr_To3.IsEmpty())
		{
			m_Restr_To3	=	"0";
//			olMsg+=GetString(IDS_STRING1434);
		}
	}
	if(	!m_Npos4.IsEmpty())
	{
		PSTDATA *prlPst = ogPstData.GetPstByPnam(m_Npos4.GetBuffer(0));
		if(prlPst == NULL)
		{
			olTmp.Format(GetString(IDS_STRING1432), m_Npos4.GetBuffer(0));
			olMsg+=olTmp;
		}
	}
	else
	{
		if(	m_Restr_To4.IsEmpty())
		{
			m_Restr_To4	=	"0";
//			olMsg+=GetString(IDS_STRING1434);
		}
	}
	if(	!m_Npos5.IsEmpty())
	{
		PSTDATA *prlPst = ogPstData.GetPstByPnam(m_Npos5.GetBuffer(0));
		if(prlPst == NULL)
		{
			olTmp.Format(GetString(IDS_STRING1432), m_Npos5.GetBuffer(0));
			olMsg+=olTmp;
		}
	}
	else
	{
		if(	m_Restr_To5.IsEmpty())
		{
			m_Restr_To5	=	"0";
//			olMsg+=GetString(IDS_STRING1434);
		}
	}
	if(	!m_Npos6.IsEmpty())
	{
		PSTDATA *prlPst = ogPstData.GetPstByPnam(m_Npos6.GetBuffer(0));
		if(prlPst == NULL)
		{
			olTmp.Format(GetString(IDS_STRING1432), m_Npos6.GetBuffer(0));
			olMsg+=olTmp;
		}
	}
	else
	{
		if(	m_Restr_To6.IsEmpty())
		{
			m_Restr_To6	=	"0";
//			olMsg+=GetString(IDS_STRING1434);
		}
	}
	if(	!m_Npos7.IsEmpty())
	{
		PSTDATA *prlPst = ogPstData.GetPstByPnam(m_Npos7.GetBuffer(0));
		if(prlPst == NULL)
		{
			olTmp.Format(GetString(IDS_STRING1432), m_Npos7.GetBuffer(0));
			olMsg+=olTmp;
		}
	}
	else
	{
		if(	m_Restr_To7.IsEmpty())
		{
			m_Restr_To7	=	"0";
//			olMsg+=GetString(IDS_STRING1434);
		}
	}
	if(	!m_Npos8.IsEmpty())
	{
		PSTDATA *prlPst = ogPstData.GetPstByPnam(m_Npos8.GetBuffer(0));
		if(prlPst == NULL)
		{
			olTmp.Format(GetString(IDS_STRING1432), m_Npos8.GetBuffer(0));
			olMsg+=olTmp;
		}
	}
	else
	{
		if(	m_Restr_To8.IsEmpty())
		{
			m_Restr_To8	=	"0";
//			olMsg+=GetString(IDS_STRING1434);
		}
	}
	if(	!m_Npos9.IsEmpty())
	{
		PSTDATA *prlPst = ogPstData.GetPstByPnam(m_Npos9.GetBuffer(0));
		if(prlPst == NULL)
		{
			olTmp.Format(GetString(IDS_STRING1432), m_Npos9.GetBuffer(0));
			olMsg+=olTmp;
		}
	}
	else
	{
		if(	m_Restr_To9.IsEmpty())
		{
			m_Restr_To9	=	"0";
//			olMsg+=GetString(IDS_STRING1434);
		}
	}
	if(	!m_Npos10.IsEmpty())
	{
		PSTDATA *prlPst = ogPstData.GetPstByPnam(m_Npos10.GetBuffer(0));
		if(prlPst == NULL)
		{
			olTmp.Format(GetString(IDS_STRING1432), m_Npos10.GetBuffer(0));
			olMsg+=olTmp;
		}
	}
	else
	{
		if(	m_Restr_To10.IsEmpty())
		{
			m_Restr_To10	=	"0";
//			olMsg+=GetString(IDS_STRING1434);
		}
	}

	

	if(olMsg.IsEmpty())
	{

		CString olStr;
		if(m_AL_List.GetLength() > 150)
		{
			m_AL_List = m_AL_List.Left(150);
		}
		if(m_Exce.GetLength() > 150)
		{
			m_Exce = m_Exce.Left(150);
		}
	
		

		olStr += m_Span_Min+"-"+m_Span_Max+";";
		olStr += m_Leng_Min+"-"+m_Leng_Max+";";
		olStr += m_Heig_Min+"-"+m_Heig_Max+";";
		olStr += m_Exce+";";
		olStr += m_Npos1+";";
		olStr += m_Restr_To1+";";
		olStr += m_Npos2+";";
		olStr += m_Restr_To2+";";
		olStr += m_Max_Ac+";";
		olStr += m_Prio+";";
		olStr += m_Nat_Restr+";";
		olStr += m_AL_List+";";

		olStr += m_Npos3+";";
		olStr += m_Restr_To3+";";
		olStr += m_Npos4+";";
		olStr += m_Restr_To4+";";
		olStr += m_Npos5+";";
		olStr += m_Restr_To5+";";
		olStr += m_Npos6+";";
		olStr += m_Restr_To6+";";
		olStr += m_Npos7+";";
		olStr += m_Restr_To7+";";
		olStr += m_Npos8+";";
		olStr += m_Restr_To8+";";
		olStr += m_Npos9+";";
		olStr += m_Restr_To9+";";
		olStr += m_Npos10+";";
		olStr += m_Restr_To10;

//		strcpy(pomPst->Acgr, olStr.GetBuffer(0));
		strcpy(pomPst->Posr, olStr.GetBuffer(0));
		pomPst->IsChanged = DATA_CHANGED;
		ogPstData.SavePST(pomPst);

		CDialog::OnOK();
	}
	else
	{
		MessageBox(olMsg.GetBuffer(0), GetString(IDS_STRING117), MB_OK);
	}

}

BOOL PstRuleDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	UpdateData(FALSE);
	CString olCaption;
	olCaption.Format(GetString(IDS_STRING1436), pomPst->Pnam);
	SetWindowText(olCaption);
	
	// TODO: Add extra initialization here
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}



void PstRuleDlg::OnMatrix1() 
{
	// TODO: Add your control notification handler code here
	if (!UpdateData(TRUE))
		return;
	if (m_Npos1.IsEmpty())
		return;
	PositionMatrixDlg olDlg(pomPst->Pnam,m_Npos1,this);
	olDlg.DoModal();
}

void PstRuleDlg::OnMatrix2() 
{
	// TODO: Add your control notification handler code here
	if (!UpdateData(TRUE))
		return;
	if (m_Npos2.IsEmpty())
		return;
	PositionMatrixDlg olDlg(pomPst->Pnam,m_Npos2,this);
	olDlg.DoModal();
	
}

void PstRuleDlg::OnMatrix3() 
{
	// TODO: Add your control notification handler code here
	if (!UpdateData(TRUE))
		return;
	if (m_Npos3.IsEmpty())
		return;
	PositionMatrixDlg olDlg(pomPst->Pnam,m_Npos3,this);
	olDlg.DoModal();
	
}

void PstRuleDlg::OnMatrix4() 
{
	// TODO: Add your control notification handler code here
	if (!UpdateData(TRUE))
		return;
	if (m_Npos4.IsEmpty())
		return;
	PositionMatrixDlg olDlg(pomPst->Pnam,m_Npos4,this);
	olDlg.DoModal();
	
}

void PstRuleDlg::OnMatrix5() 
{
	// TODO: Add your control notification handler code here
	if (!UpdateData(TRUE))
		return;
	if (m_Npos5.IsEmpty())
		return;
	PositionMatrixDlg olDlg(pomPst->Pnam,m_Npos5,this);
	olDlg.DoModal();
	
}

void PstRuleDlg::OnMatrix6() 
{
	// TODO: Add your control notification handler code here
	if (!UpdateData(TRUE))
		return;
	if (m_Npos6.IsEmpty())
		return;
	PositionMatrixDlg olDlg(pomPst->Pnam,m_Npos6,this);
	olDlg.DoModal();
	
}

void PstRuleDlg::OnMatrix7() 
{
	// TODO: Add your control notification handler code here
	if (!UpdateData(TRUE))
		return;
	if (m_Npos7.IsEmpty())
		return;
	PositionMatrixDlg olDlg(pomPst->Pnam,m_Npos7,this);
	olDlg.DoModal();
	
}

void PstRuleDlg::OnMatrix8() 
{
	// TODO: Add your control notification handler code here
	if (!UpdateData(TRUE))
		return;
	if (m_Npos8.IsEmpty())
		return;
	PositionMatrixDlg olDlg(pomPst->Pnam,m_Npos8,this);
	olDlg.DoModal();
	
}

void PstRuleDlg::OnMatrix9() 
{
	// TODO: Add your control notification handler code here
	if (!UpdateData(TRUE))
		return;
	if (m_Npos9.IsEmpty())
		return;
	PositionMatrixDlg olDlg(pomPst->Pnam,m_Npos9,this);
	olDlg.DoModal();
	
}

void PstRuleDlg::OnMatrix10() 
{
	// TODO: Add your control notification handler code here
	if (!UpdateData(TRUE))
		return;
	if (m_Npos10.IsEmpty())
		return;
	PositionMatrixDlg olDlg(pomPst->Pnam,m_Npos10,this);
	olDlg.DoModal();
	
}
