// GatTableViewer.cpp: Implementierung der Klasse GatTableViewer.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"

#include "CedaGatData.h"

#include "GatRulesViewer.h"
#include <math.h>

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif


const int width = 574;

//////////////////////////////////////////////////////////////////////
// Konstruktion/Destruktion
//////////////////////////////////////////////////////////////////////

GatRulesViewer::GatRulesViewer()
{
	bmNoUpdatesNow = false;
	

}

GatRulesViewer::~GatRulesViewer()
{
	DeleteAll();
}

void GatRulesViewer::Attach(CCSTable *popTable)
{
    pomTable = popTable;
}

#pragma warning( disable : 4100 )  
void GatRulesViewer::ChangeViewTo(const char *pcpViewName)
{
    DeleteAll();    
    MakeLines();

	UpdateDisplay();
}
 

int GatRulesViewer::CompareGat(GATTABLE_LINEDATA *prpGat1, GATTABLE_LINEDATA *prpGat2)
{


     return 0;
}
#pragma warning( default : 4100 )  

/////////////////////////////////////////////////////////////////////////////
// GatRulesViewer -- code specific to this class


void GatRulesViewer::MakeLines()
{
	int ilGatCount = ogGatData.omData.GetSize();
	for(int illc = 0 ; illc < ilGatCount; illc++)
	{		
		GATDATA * prlGat = &ogGatData.omData[illc];
		MakeLine(prlGat);
		
	}
}


void GatRulesViewer::MakeGatLineData(GATDATA *popGat, CStringArray &ropArray)
{
	CStringArray olResbList;
//	ExtractItemList(popGat->Resb, &olResbList, '/');
	CString olTmpGatr = popGat->Gatr;
	if(olTmpGatr.Replace(";","#") < 3)
	{
		olTmpGatr =popGat->Gatr;
		olTmpGatr.Replace("/",";");
		strcpy(popGat->Gatr, olTmpGatr.GetBuffer(0));
	}
	
	ExtractItemList(popGat->Gatr, &olResbList, ';');
	for(int i = 0; i < olResbList.GetSize(); i++)
	{
		ropArray.Add(olResbList[i]);
	}
}

int GatRulesViewer::MakeLine(GATDATA *prpGat)
{
    // Update viewer data for this shift record
   GATTABLE_LINEDATA rlLine;

	rlLine.Rtnr = omLines.GetSize() +1;
	CStringArray olAcgrList;
	MakeGatLineData(prpGat, olAcgrList);
	int ilWordCount = olAcgrList.GetSize();

	CString olT;
	int ilActIdx = 0;
		
	rlLine.Gate = prpGat->Gnam;
	
	rlLine.T = prpGat->Term;
		
	rlLine.RBIT = prpGat->Rbab;
	
	if(ilWordCount >ilActIdx)
	{
		olT = olAcgrList[ilActIdx];
		rlLine.Id = olT;
		ilActIdx++;
	}
	if(ilWordCount >ilActIdx)
	{
		olT = olAcgrList[ilActIdx];
		rlLine.Max = olT;
		ilActIdx++;
	}

	if(ilWordCount >ilActIdx)
	{
		olT = olAcgrList[ilActIdx];
		rlLine.M = olT;
		ilActIdx++;
	}

	if(ilWordCount >ilActIdx)
	{
		olT = olAcgrList[ilActIdx];
		rlLine.In = olT;
		ilActIdx++;
	}

	if(ilWordCount >ilActIdx)
	{
		olT = olAcgrList[ilActIdx];
		rlLine.Out = olT;
		ilActIdx++;
	}

	if(ilWordCount >ilActIdx)
	{
		olT = olAcgrList[ilActIdx];
		rlLine.Prio = olT;
		ilActIdx++;
	}

	if(ilWordCount >ilActIdx)
	{
		olT = olAcgrList[ilActIdx];
		rlLine.Nat= olT;
		ilActIdx++;
	}

	if(ilWordCount >ilActIdx)
	{
		olT = olAcgrList[ilActIdx];
		rlLine.PAL= olT;
		ilActIdx++;
	}

	if(ilWordCount >ilActIdx)
	{
		olT = olAcgrList[ilActIdx];
		rlLine.PDest = olT;
		ilActIdx++;
	}


	return(CreateLine(&rlLine));
}


int GatRulesViewer::CreateLine(GATTABLE_LINEDATA *prpGat)
{
    int ilLineCount = omLines.GetSize();

	GATTABLE_LINEDATA rlGat;
	rlGat = *prpGat;
    omLines.NewAt(ilLineCount, rlGat);

    return ilLineCount;
}

void GatRulesViewer::DeleteLine(int ipLineno)
{
	omLines.DeleteAt(ipLineno);
}


BOOL GatRulesViewer::FindLine(long lpRtnr, int &rilLineno)
{
    for (rilLineno = 0; rilLineno < omLines.GetSize(); rilLineno++)
	{
      if (omLines[rilLineno].Rtnr == lpRtnr)
            return TRUE;
	}
    return FALSE;
}



/////////////////////////////////////////////////////////////////////////////
// GatRulesViewer - GATTABLE_LINEDATA array maintenance

void GatRulesViewer::DeleteAll()
{
    while (omLines.GetSize() > 0)
        DeleteLine(0);
}


/////////////////////////////////////////////////////////////////////////////
// GatRulesViewer - display drawing routine


void GatRulesViewer::MakeHeaderData()
{

	TABLE_HEADER_COLUMN rlHeader;
//	int ilColumnNo = -1;

	CStringArray olAcgrList;
	ExtractItemList(LoadStg(IDS_STRING1429), &olAcgrList, ',');
	
	CString olT;
	int ilActIdx = 0;
	int ilWordCount = olAcgrList.GetSize();
	

	rlHeader.Alignment = COLALIGN_CENTER;
	rlHeader.Length = 8 * 4; 
	rlHeader.Font = &ogCourier_Bold_8;
	rlHeader.SeparatorType = SEPA_NORMAL;//SEPA_NONE;//SEPA_LIKEVERTICAL

	rlHeader.Text = "";
	
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(),rlHeader);

	rlHeader.Length = 8 * 6; 
	rlHeader.Text = "";
	if(ilWordCount >ilActIdx)
	{
		olT = olAcgrList[ilActIdx];
		rlHeader.Text = olT;
		ilActIdx++;
	}
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(),rlHeader);
	rlHeader.Length = 8 * 2; 
	rlHeader.Text = "";
	if(ilWordCount >ilActIdx)
	{
		olT = olAcgrList[ilActIdx];
		rlHeader.Text = olT;
		ilActIdx++;
	}
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(),rlHeader);
	rlHeader.Length = 8 * 8; 
	rlHeader.Text = "";
	if(ilWordCount >ilActIdx)
	{
		olT = olAcgrList[ilActIdx];
		rlHeader.Text = olT;
		ilActIdx++;
	}
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(),rlHeader);
	rlHeader.Length = 8 * 3; 
	rlHeader.Text = "";
	if(ilWordCount >ilActIdx)
	{
		olT = olAcgrList[ilActIdx];
		rlHeader.Text = olT;
		ilActIdx++;
	}
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(),rlHeader);
	rlHeader.Length = 8 * 4; 
	rlHeader.Text = "";
	if(ilWordCount >ilActIdx)
	{
		olT = olAcgrList[ilActIdx];
		rlHeader.Text = olT;
		ilActIdx++;
	}
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(),rlHeader);
	rlHeader.Length = 8 * 4; 
	rlHeader.Text = "";
	if(ilWordCount >ilActIdx)
	{
		olT = olAcgrList[ilActIdx];
		rlHeader.Text = olT;
		ilActIdx++;
	}
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(),rlHeader);
	rlHeader.Length = 8 * 2; 
	rlHeader.Text = "";
	if(ilWordCount >ilActIdx)
	{
		olT = olAcgrList[ilActIdx];
		rlHeader.Text = olT;
		ilActIdx++;
	}
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(),rlHeader);
	rlHeader.Length = 8 * 2; 
	rlHeader.Text = "";
	if(ilWordCount >ilActIdx)
	{
		olT = olAcgrList[ilActIdx];
		rlHeader.Text = olT;
		ilActIdx++;
	}
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(),rlHeader);
	rlHeader.Length = 8 * 2; 
	rlHeader.Text = "";
	if(ilWordCount >ilActIdx)
	{
		olT = olAcgrList[ilActIdx];
		rlHeader.Text = olT;
		ilActIdx++;
	}
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(),rlHeader);

	rlHeader.Length = 8 * 8; 
	rlHeader.Text = "";
	if(ilWordCount >ilActIdx)
	{
		olT = olAcgrList[ilActIdx];
		rlHeader.Text = olT;
		ilActIdx++;
	}
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(),rlHeader);

	rlHeader.Length = 8 * 35; 
	rlHeader.Text = "";
	if(ilWordCount >ilActIdx)
	{
		olT = olAcgrList[ilActIdx];
		rlHeader.Text = olT;
		ilActIdx++;
	}
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(),rlHeader);

	rlHeader.Length = 8 * 10; 
	rlHeader.Text = "";
	if(ilWordCount >ilActIdx)
	{
		olT = olAcgrList[ilActIdx];
		rlHeader.Text = olT;
		ilActIdx++;
	}
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(),rlHeader);

	pomTable->SetHeaderFields(omHeaderDataArray);
	omHeaderDataArray.RemoveAll();

}



void GatRulesViewer::MakeColumnData(CCSPtrArray<TABLE_COLUMN> &ropColList,GATTABLE_LINEDATA *prpLine)
{

//	BOOL blNewLogicLine = FALSE;

	int ilColumnNo = 0;
	bool bldefekt = false;

	TABLE_COLUMN rlColumnData;
	rlColumnData.Alignment = COLALIGN_LEFT;
	rlColumnData.Lineno = imTotalLines;//*3)+ilLocalLine;
	rlColumnData.VerticalSeparator = SEPA_NORMAL;
	rlColumnData.SeparatorType = SEPA_NORMAL;
	rlColumnData.Font = &ogCourier_Regular_8;//&ogCourier_Regular_10;
	rlColumnData.BkColor = WHITE;
	rlColumnData.TextColor = BLACK;
	rlColumnData.Text.Format("%d",prpLine->Rtnr);  
	rlColumnData.Columnno = ilColumnNo++;
	ropColList.NewAt(ropColList.GetSize(), rlColumnData);


	rlColumnData.Text = prpLine->Gate;
	rlColumnData.Columnno = ilColumnNo++;
	ropColList.NewAt(ropColList.GetSize(), rlColumnData);

	rlColumnData.Text = prpLine->T;
	rlColumnData.Columnno = ilColumnNo++;
	ropColList.NewAt(ropColList.GetSize(), rlColumnData);

	rlColumnData.Text = prpLine->RBIT;
	rlColumnData.Columnno = ilColumnNo++;
	ropColList.NewAt(ropColList.GetSize(), rlColumnData);

	rlColumnData.Text = prpLine->Id;
	rlColumnData.Columnno = ilColumnNo++;
	ropColList.NewAt(ropColList.GetSize(), rlColumnData);

	rlColumnData.Text = prpLine->Max;
	rlColumnData.Columnno = ilColumnNo++;
	ropColList.NewAt(ropColList.GetSize(), rlColumnData);

	rlColumnData.Text = prpLine->M;
	rlColumnData.Columnno = ilColumnNo++;
	ropColList.NewAt(ropColList.GetSize(), rlColumnData);

	rlColumnData.Text = prpLine->In;
	rlColumnData.Columnno = ilColumnNo++;
	ropColList.NewAt(ropColList.GetSize(), rlColumnData);

	rlColumnData.Text = prpLine->Out;
	rlColumnData.Columnno = ilColumnNo++;
	ropColList.NewAt(ropColList.GetSize(), rlColumnData);

	rlColumnData.Text = prpLine->Prio;
	rlColumnData.Columnno = ilColumnNo++;
	ropColList.NewAt(ropColList.GetSize(), rlColumnData);

	rlColumnData.Text = prpLine->Nat;
	rlColumnData.Columnno = ilColumnNo++;
	ropColList.NewAt(ropColList.GetSize(), rlColumnData);

	rlColumnData.Text = prpLine->PAL;
	rlColumnData.Columnno = ilColumnNo++;
	ropColList.NewAt(ropColList.GetSize(), rlColumnData);

	rlColumnData.Text = prpLine->PDest;
	rlColumnData.Columnno = ilColumnNo++;

	ropColList.NewAt(ropColList.GetSize(), rlColumnData);

}



// UpdateDisplay: Load data selected by filter conditions to the display by using "omTable"
void GatRulesViewer::UpdateDisplay()
{

	imTotalLines = 0;
//	BOOL blNewLogicLine = FALSE;


	pomTable->SetShowSelection(FALSE);
	pomTable->ResetContent();
	//pomTable->SetDockingRange(3);
	
	MakeHeaderData();
	pomTable->SetDefaultSeparator();
//	pomTable->SetSelectMode(0);

	for (int ilLc = 0; ilLc < omLines.GetSize(); ilLc++)
	{
		CCSPtrArray<TABLE_COLUMN> olColList;

		MakeColumnData(olColList,&omLines[ilLc]);
		imTotalLines++;
		pomTable->AddTextLine(olColList, (void*)&(omLines[ilLc]));
		olColList.DeleteAll();
	}
    pomTable->DisplayTable();
}




