// CedaGATData.h

#ifndef __CEDAGATDATA__
#define __CEDAGATDATA__
 
#include "stdafx.h"
#include "basicdata.h"

//---------------------------------------------------------------------------------------------------------
// Record structure declaration

//@See: CedaGATData
//@Doc:	A structure for reading Duty data. We read all data from database and put them
//		into this struct and store the data in omData.  

//---------------------------------------------------------------------------------------------------------
struct GATDATA 
{
	CTime	 Bnaf; 		// Fluggastbr�cken nicht verf�gbar von
	CTime	 Bnat; 		// Fluggastbr�cken nicht verf�gbar bis
	CTime	 Cdat; 		// Erstellungsdatum
	char 	 Gnam[7]; 	// Gatename
	CTime	 Lstu; 		// Datum letzte �nderung
	CTime	 Nafr; 		// Nicht verf�gbar von
	CTime	 Nato; 		// Nicht verf�gbar bis
	char 	 Nobr[4]; 	// Anzahl Fluggastbr�cken
	char 	 Prfl[3]; 	// Protokollierungskennzeichen
	char 	 Rga1[7]; 	// Verkn�pftes Gate 1
	char 	 Rga2[7]; 	// Verkn�pftes Gate 2
	char 	 Tele[12]; 	// Telefonnummer
	long 	 Urno; 		// Eindeutige Datensatz-Nr.
	char 	 Usec[34]; 	// Anwender (Ersteller)
	char 	 Useu[34]; 	// Anwender (letzte �nderung)
	CTime	 Vafr; 		// G�ltig von
	CTime	 Vato; 		// G�ltig bis
	char 	 Resn[42]; 	// Grund f�r die Sperrung (Gate)
	char 	 Resb[42]; 	// Grund f�r die Sperrung (Fluggastbr�cken)
	char 	 Rbab[7]; 	// Verkn�pftes Gep�ckband (Ankunft)
	char 	 Busg[3]; 	// Bus-Gate
	char	 Term[2];	// Terminal
	char 	 Gatr[513]; 	// Regeln

	//DataCreated by this class
	int      IsChanged;

	GATDATA(void)
	{ memset(this,'\0',sizeof(*this));
	 Cdat=-1;Lstu=-1;Nafr=-1;Nato=-1;Vafr=-1;Vato=-1;Bnaf=-1;Bnat=-1;
	}

}; // end GATDataStrukt


//---------------------------------------------------------------------------------------------------------
// Class declaratino

//@Memo: Read and write GAT(duty) data
//@See: CedaData, CedaDutyData
/*@Doc:
  Reads and writes GAT(duty) data from and to database. Stores GAT data in memory and
  provides methods for searching and retrieving GATs from its internal list. Does
  some data preparations.

  Data of this class has to be updated by CEDA through broadcasts. This procedure and the
  necessary methods will be described later with the description of broadcast handling.

  {\bf CedaGATData} handles data from the server process. 
  See the specification for table descriptions.*/

class CedaGATData: public CCSCedaData
{
// Attributes
public:
    //@ManMemo: A map, containing the Urno field of all loaded GATs.
    CMapPtrToPtr omUrnoMap;
	CMapStringToPtr omGnamMap;

    //@ManMemo: GATDATA records read by ReadAllGAT().
    CCSPtrArray<GATDATA> omData;

	void CedaGATData::Convert();
	void SaveAll(void);


	void SetTableName(CString opTableName)
	{
		strcpy(pcmTableName, opTableName.GetBuffer(0));
	}
// Operations
public:
    //@ManMemo: Default constructor
    /*@Doc:
      Initialize {\bf omRecInfo} for offsets and types for each fields in {\bf GATDATA},
      the {\bf pcmTableName} with table name {\bf GATLSG}, the data members {\bf pcmFieldList}
      contains a list of used fields of {\bf GATDATA}.
    */
    CedaGATData();
	~CedaGATData();
	void Register(void);
	void ClearAll(void);

    //@ManMemo: Read all GATs.
    /*@Doc:
      Read all GATs. 

      This method will clear {\bf omData} and {\bf omKeyMap}. Convert each record
      to a GATDATA and store it in {\bf omData} and the Urno field in {\bf omKeyMap}.
    */
    bool ReadAllGATs();
    //@AreMemo: Read all GAT-Data corresponding to Where-condition
	bool ReadSpecial(CCSPtrArray<GATDATA> &ropGat,char *pspWhere);
    //@ManMemo: Add a new GAT and store the new data to the Database.
	bool InsertGAT(GATDATA *prpGAT,BOOL bpSendDdx = TRUE);
    //@ManMemo: Add data for a new GAT to the internal data.
	bool InsertGATInternal(GATDATA *prpGAT);
	//@ManMemo: Change the data of a special GAT
	bool UpdateGAT(GATDATA *prpGAT,BOOL bpSendDdx = TRUE);
    //@AreMemo: Change the data of a special GAT in the internal data.
	bool UpdateGATInternal(GATDATA *prpGAT);
    //@ManMemo: Delete a special GAT, also in the Database.
	bool DeleteGAT(long lpUrno);
    //@ManMemo: Delete a special GAT only in the internal data.
	bool DeleteGATInternal(GATDATA *prpGAT);
    //@AreMemo: Selects all GATs with a special Urno.
	GATDATA  *GetGATByUrno(long lpUrno);
    //@ManMemo: Search a GAT with a special Key.
	CString GATExists(GATDATA *prpGAT);
    //@AreMemo: Search a Gnam
	bool ExistGnam(char* popGnam);
	//@ManMemo: Writes a special GAT to the Database.
	bool SaveGAT(GATDATA *prpGAT);
    //@ManMemo: Contains the valid data for all existing GATs.
	char pcmGATFieldList[2048];
    //@ManMemo: Handle Broadcasts for GATs.
	void ProcessGATBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

	// Private methods
private:
    void PrepareGATData(GATDATA *prpGATData);
};


extern CedaGATData ogGatData;
//---------------------------------------------------------------------------------------------------------
#endif //__CEDAGATDATA__
