// CedaCICData.h

#ifndef __CEDACICDATA__
#define __CEDACICDATA__
 
#include "stdafx.h"
#include "basicdata.h"

//---------------------------------------------------------------------------------------------------------
// Record structure declaration

//@See: CedaCICData
//@Doc:	A structure for reading Duty data. We read all data from database and put them
//		into this struct and store the data in omData.  

//---------------------------------------------------------------------------------------------------------
struct CICDATA 
{
	CTime	 Cdat;		// Erstellungsdatum
	char 	 Cnam[7]; 	// Check-In Schalter Name
	char 	 Edpe[12]; 	// EDV-Ausr�stung
	CTime	 Lstu; 		// Datum letzte �nderung
	CTime	 Nafr;		// Nicht verf�gbar vom
	CTime	 Nato;		// Nicht verf�gbar bis
	char 	 Prfl[3]; 	// Protokollierungskennzeichen
	char 	 Tele[12]; 	// Telefonnummer
	long 	 Urno;		// Eindeutige Datensatz-Nr.
	char 	 Usec[34]; 	// Anwender (Ersteller)
	char 	 Useu[34]; 	// Anwender (letzte �nderung)
	CTime	 Vafr;		// G�ltig von
	CTime	 Vato;		// G�ltig bis
	char 	 Rgbl[7]; 	// Verkn�pfter Warteraum
	char 	 Resn[42]; 	// Grund f�r die Sperrung

	//DataCreated by this class
	int      IsChanged;

	CICDATA(void)
	{ memset(this,'\0',sizeof(*this));
	 Cdat=-1;Lstu=-1;Nafr=-1;Nato=-1;Vafr=-1;Vato=-1;
	}

}; // end CICDataStrukt


//---------------------------------------------------------------------------------------------------------
// Class declaratino

//@Memo: Read and write CIC(duty) data
//@See: CedaData, CedaDutyData
/*@Doc:
  Reads and writes CIC(duty) data from and to database. Stores CIC data in memory and
  provides methods for searching and retrieving CICs from its internal list. Does
  some data preparations.

  Data of this class has to be updated by CEDA through broadcasts. This procedure and the
  necessary methods will be described later with the description of broadcast handling.

  {\bf CedaCICData} handles data from the server process. 
  See the specification for table descriptions.*/

class CedaCICData: public CCSCedaData
{
// Attributes
public:
    //@ManMemo: A map, containing the Urno field of all loaded CICs.
    CMapPtrToPtr omUrnoMap;
	CMapStringToPtr omCnamMap;

    //@ManMemo: CICDATA records read by ReadAllCIC().
    CCSPtrArray<CICDATA> omData;

	void SetTableName(CString opTableName)
	{
		strcpy(pcmTableName, opTableName.GetBuffer(0));
	}
// Operations
public:
    //@ManMemo: Default constructor
    /*@Doc:
      Initialize {\bf omRecInfo} for offsets and types for each fields in {\bf CICDATA},
      the {\bf pcmTableName} with table name {\bf CICLSG}, the data members {\bf pcmFieldList}
      contains a list of used fields of {\bf CICDATA}.
    */
    CedaCICData();
	~CedaCICData();
	void Register(void);
	void ClearAll(void);

    //@ManMemo: Read all CICs.
    /*@Doc:
      Read all CICs. 

      This method will clear {\bf omData} and {\bf omKeyMap}. Convert each record
      to a CICDATA and store it in {\bf omData} and the Urno field in {\bf omKeyMap}.
    */
    bool ReadAllCICs();
    //@AreMemo: Read all CIC-Data corresponding to Where-condition
	bool ReadSpecial(CCSPtrArray<CICDATA> &ropCic,char *pspWhere);
    //@ManMemo: Add a new CIC and store the new data to the Database.
	bool InsertCIC(CICDATA *prpCIC,BOOL bpSendDdx = TRUE);
    //@ManMemo: Add data for a new CIC to the internal data.
	bool InsertCICInternal(CICDATA *prpCIC);
	//@ManMemo: Change the data of a special CIC
	bool UpdateCIC(CICDATA *prpCIC,BOOL bpSendDdx = TRUE);
    //@AreMemo: Change the data of a special CIC in the internal data.
	bool UpdateCICInternal(CICDATA *prpCIC);
    //@ManMemo: Delete a special CIC, also in the Database.
	bool DeleteCIC(long lpUrno);
    //@ManMemo: Delete a special CIC only in the internal data.
	bool DeleteCICInternal(CICDATA *prpCIC);
    //@AreMemo: Selects all CICs with a special Urno.
	CICDATA  *GetCICByUrno(long lpUrno);
    //@ManMemo: Search a CIC with a special Key.
	CString CICExists(CICDATA *prpCIC);
    //@AreMemo: Search a Cam
	bool ExistCnam(char* popCnam);
	//@ManMemo: Writes a special CIC to the Database.
	bool SaveCIC(CICDATA *prpCIC);
    //@ManMemo: Contains the valid data for all existing CICs.
	char pcmCICFieldList[2048];
    //@ManMemo: Handle Broadcasts for CICs.
	void ProcessCICBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

	// Private methods
private:
    void PrepareCICData(CICDATA *prpCICData);
};


extern CedaCICData ogCicData;
//---------------------------------------------------------------------------------------------------------
#endif //__CEDACICDATA__
