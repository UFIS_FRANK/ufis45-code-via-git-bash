// BackGround.cpp : implementation file
//

#include "stdafx.h"
#include "Rules.h"
#include "BackGround.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CBackGround dialog


CBackGround::CBackGround(CWnd* pParent /*=NULL*/)
	: CDialog(CBackGround::IDD, pParent)
{
	//{{AFX_DATA_INIT(CBackGround)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void CBackGround::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CBackGround)
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CBackGround, CDialog)
	//{{AFX_MSG_MAP(CBackGround)
	ON_WM_CREATE()
    ON_WM_PAINT()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CBackGround message handlers

void CBackGround::OnPaint() 
{
    CPaintDC dc(this); // device context for painting
    
    // Load the backgroundbitmap
	
    CBrush olBrush( RGB( 192, 192, 192 ) );
    CBrush *polOldBrush = dc.SelectObject( &olBrush );
    CRect olRect;
    GetClientRect( &olRect );
    dc.FillRect( &olRect, &olBrush );
    dc.SelectObject( polOldBrush );
    
    CDC olDC;
    olDC.CreateCompatibleDC( &dc );
    CBitmap olBitmap;
    olBitmap.LoadBitmap( IDB_UFIS43COPYRIGHT );
	olDC.SelectObject( &olBitmap );
 
    dc.BitBlt( 0, olRect.bottom - 60, 409, 52, &olDC, 0, 0, SRCCOPY );
    olDC.DeleteDC( );
    
    // Do not call CDialog::OnPaint() for painting messages
}

int CBackGround::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	if (CDialog::OnCreate(lpCreateStruct) == -1)
		return -1;
	
	// TODO: Add your specialized creation code here
	
	return 0;
}


BOOL CBackGround::Create(CWnd* pParentWnd, UINT nID) 
{

	return CDialog::Create(IDD, pParentWnd);
}


BOOL CBackGround::OnInitDialog() 
{
	CDialog::OnInitDialog();

	CRect olRect;

	olRect.bottom = ::GetSystemMetrics(SM_CYMAXIMIZED);
	olRect.right  = ::GetSystemMetrics(SM_CXMAXIMIZED);
	olRect.top = 0;
	olRect.left = 0;
	
	MoveWindow(&olRect, TRUE);
	ShowWindow(SW_SHOW);

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}



