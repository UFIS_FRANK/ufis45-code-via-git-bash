// CedaAloData.cpp - Stammdaten: Allocation Unit Types
//

#include "stdafx.h"
#include "afxwin.h"
#include "ccsglobl.h"
#include "CCSPtrArray.h"
#include "CCSCedaCom.h"
#include "CCSCedaData.h"
#include "ccsddx.h"
#include "ccslog.h"
#include "CCSBcHandle.h"
#include "BasicData.h"
#include "CedaAloData.h"

CedaAloData::CedaAloData()
{
   // Create an array of CEDARECINFO for ALODATA
    BEGIN_CEDARECINFO(ALODATA, AloDataRecInfo)
		FIELD_LONG(Urno,"URNO")
		FIELD_CHAR_TRIM(Aloc,"ALOC")
		FIELD_CHAR_TRIM(Alod,"ALOD")
		FIELD_CHAR_TRIM(Alot,"ALOT")
		FIELD_CHAR_TRIM(Reft,"REFT")
		FIELD_CHAR_TRIM(Refm,"REFM")
    END_CEDARECINFO

    // Copy the record structure
    for (int i = 0; i < sizeof(AloDataRecInfo)/sizeof(AloDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&AloDataRecInfo[i],sizeof(CEDARECINFO));
        omRecInfo.Add(prpCedaRecInfo);
	}

    // Initialize table names and field names
    strcpy(pcmTableName,"ALOTAB");
    pcmFieldList = "URNO,ALOC,ALOD,ALOT,REFT,REFM";
}

CedaAloData::~CedaAloData()
{
	TRACE("CedaAloData::~CedaAloData called\n");
	ClearAll();
}

void CedaAloData::ClearAll()
{
	omUrnoMap.RemoveAll();
	omData.DeleteAll();
}

BOOL CedaAloData::ReadAloData()
{
	// Select data from the database
	char pclWhere[100] = "";
	char pclCom[10] = "RT";
	if (CedaAction(pclCom, pclWhere) != true)
	{
		return false;
	}
	else
	{
		bool ilRc = true;
		for (int ilLc = 0; ilRc == true; ilLc++)
		{
			ALODATA *prlAlo = new ALODATA;
			if ((ilRc = GetBufferRecord(ilLc,prlAlo)) == true)
			{
				AddAloInternal(prlAlo);
			}
			else
			{
				delete prlAlo;
			}
		}
	}

	return TRUE;
}


BOOL CedaAloData::AddAloInternal(ALODATA *prpAlo)
{
	omData.Add(prpAlo);
	omUrnoMap.SetAt((void *)prpAlo->Urno,prpAlo);
	return TRUE;
}

void CedaAloData::GetAllGroupTypes(CCSPtrArray <ALODATA> &ropGroupTypes, bool bpReset /*true*/)
{
	if(bpReset)
	{
		ropGroupTypes.RemoveAll();
	}

	int ilAloCount = omData.GetSize();
	for (int ilLc = 0; ilLc < ilAloCount; ilLc++)
	{
		ALODATA *prlAlo = &omData[ilLc];
		if(*prlAlo->Alot == '2') // Group == "1"
		{
			ropGroupTypes.Add(prlAlo);
		}
	}
}

ALODATA *CedaAloData::GetAloByUrno(long lpUrno)
{
	ALODATA *prlAlo = NULL;
	omUrnoMap.Lookup((void *)lpUrno,(void *& )prlAlo);
	return prlAlo;
}

long CedaAloData::GetAloUrnoByName(const char *pcpName)
{
	long llUrno = 0L;
	int ilNumAlos = omData.GetSize();
	for(int ilAlo = 0; llUrno == 0L && ilAlo < ilNumAlos; ilAlo++)
	{
		if(!strcmp(omData[ilAlo].Aloc,pcpName))
		{
			llUrno = omData[ilAlo].Urno;
		}
	}

	return llUrno;
}

CString CedaAloData::GetAlocByUrno(long lpUrno)
{
	CString olAloc("");
	ALODATA *prlAlo = GetAloByUrno(lpUrno);
	if(prlAlo != NULL)
	{
		olAloc = prlAlo->Aloc;
	}
	return olAloc;
}

CString CedaAloData::GetTableName(void)
{
	return CString(pcmTableName);
}

ALODATA *CedaAloData::GetAloByName(const char *pcpName)
{
	long llUrno = GetAloUrnoByName(pcpName);
	if (llUrno == 0)
		return NULL;

	return GetAloByUrno(llUrno);
}