// CedaGhsData.h

#ifndef __CEDAGHSDATA__
#define __CEDAGHSDATA__
 
#include "stdafx.h"
#include "basicdata.h"

//---------------------------------------------------------------------------------------------------------

struct GHSDATA 
{
	char 	 Atrn[14]; 	// Auftragsnummer SAP-CO
	CTime	 Cdat;		// Erstellungsdatum
	char 	 Disp[3]; 	// Disponierbar (J/N)
	int 	 Lkam; 	// Anzahl Mitarbeiter
	char 	 Lkan[22]; 	// Annex
	char 	 Lkar[5]; 	// Abrechnungsschl�ssel
	char 	 Lkbz[3]; 	// Bezug
	char	 Lkcc[7];	//K�rzel
	char 	 Lkco[14]; 	// Code (Kurzname)
	int 	 Lkda; 	// Dauer
	char 	 Lkhc[12]; 	// Preis (Handlings Charge)
	int 	 Lknb; 	// Nachbereitung
	char 	 Lknm[42]; 	// Name (Bezeichnung)
	char 	 Lkst[3]; 	// Standart
	char 	 Lkty[3]; 	// Ger�t oder Personal
	int  	 Lkvb; 	// Vorbereitung
	CTime	 Lstu;		// Datum letzte �nderung
	char 	 Perm[258]; // Qualifikation
	char 	 Prfl[3]; 	// Protokollierungskennzeichen
	long 	 Urno; 		// Eindeutige Datensatz-Nr.
	char 	 Usec[34]; 	// Anwender (Ersteller)
	char 	 Useu[34]; 	// Anwender (letzte �nderung)
	char 	 Vrgc[258]; // G�ltige Ressourcengruppe
	char	 Wtyp[2];	// zu Fu�/Fahrt/Schlepp

	//DataCreated by this class
	int		 IsChanged;	// Check whether Data has Changed f�r Relaese

	GHSDATA(void)
	{ 
		memset(this,'\0',sizeof(*this));
		IsChanged	=	DATA_UNCHANGED;
		Cdat		=	TIMENULL;
		Lstu		=	TIMENULL;
	}

}; // end GhsDataStrukt

//---------------------------------------------------------------------------------------------------------
// Class declaratino

class CedaGhsData: public CCSCedaData
{
// Attributes
public:
    CMapPtrToPtr omUrnoMap;

    CCSPtrArray<GHSDATA> omData;

	char pcmListOfFields[2048];

	void SetTableName(CString opTableName)
	{
		strcpy(pcmTableName, opTableName.GetBuffer(0));
	}
// Operations
public:
    CedaGhsData();
	~CedaGhsData();
	virtual void GetDataInfo(CStringArray &ropFields, CStringArray &ropDesription, CStringArray &ropType);
	void Register(void);
	void ClearAll(bool bpWithRegistration = true);
    bool Read(char *pspWhere = NULL);
	bool Insert(GHSDATA *prpGhs);
	bool InsertInternal(GHSDATA *prpGhs);
	bool Update(GHSDATA *prpGhs);
	bool UpdateInternal(GHSDATA *prpGhs);
	bool Delete(long lpUrno);
	bool DeleteInternal(GHSDATA *prpGhs);
	bool ReadSpecialData(CCSPtrArray<GHSDATA> *popGhs,char *pspWhere,char *pspFieldList,bool ipSYS=true);
	bool Save(GHSDATA *prpGhs);
	void ProcessBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);
	GHSDATA  *GetGhsByUrno(long lpUrno);

	// Private methods
private:
    void PrepareGhsData(GHSDATA *prpGhsData);

};


extern CedaGhsData ogGhsData;
//---------------------------------------------------------------------------------------------------------

#endif //__CEDAGHSDATA__
