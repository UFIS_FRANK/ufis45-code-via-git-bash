// stviewer.cpp : implementation file
//

#include "stdafx.h"
#include "resource.h"
#include "CCSGlobl.h"
#include "CCSPtrArray.h"
//#include "cviewer.h"
#include "CCSBar.h"
#include "ccsddx.h"
#include "PremisPageDetailViewer.h"
#include "PremisPage.h"
#include "CedaGhsData.h"

#ifdef _DEBUG
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif

// Adjusted number of overlapped level in the given line
#define VisualMaxOverlapLevel(lineno)	(max(1, GetMaxOverlapLevel(lineno)))

// Local function prototype
static void GpmDetailCf(void *popInstance, int ipDDXType,
	void *vpDataPointer, CString &ropInstanceName);



/////////////////////////////////////////////////////////////////////////////
// PremisPageDetailViewer
//
PremisPageDetailViewer::PremisPageDetailViewer(long lpRkey)
{

	//omFkey = opFKey;
	lmRkey = lpRkey;
	pomAttachWnd = NULL;
	omBkBrush.CreateSolidBrush(RGB(235,235,235));
	omBarBrushGray.CreateSolidBrush(RGB(190,190,190));
	//omSortOrder.Add("Arbeitsbeginn");

	omOnBlock = CTime(1980, 1, 1, 1, 30, 0);
	omOfBlock = CTime(1980, 1, 1, 5, 30, 0);
	CString ol1 = omOnBlock.Format("%d.%m.%Y-%H:%M");
	CString ol2 = omOfBlock.Format("%d.%m.%Y-%H:%M");

	ogDdx.Register(this, GPM_NEW,
		CString(""), CString("GPM Changed"), GpmDetailCf);
	ogDdx.Register(this, GPM_CHANGE,
		CString(""), CString("GPM Changed"), GpmDetailCf);
	ogDdx.Register(this, GPM_DELETE,
		CString(""), CString("GPM Changed"), GpmDetailCf);
	ogDdx.Register(this, GPM_PREMIS_DELETE,
		CString(""), CString("GPM Changed"), GpmDetailCf);

}

PremisPageDetailViewer::~PremisPageDetailViewer()
{
   ogDdx.UnRegister(this, NOTUSED);
	ClearAll();
}

long PremisPageDetailViewer::GetCurrentRkey()
{
	return lmRkey;
}
void PremisPageDetailViewer::SetCurrentRkey(long lpRkey)
{
	lmRkey = lpRkey;
}

void PremisPageDetailViewer::ClearAll()
{
	int ilCount = 0, ilCount2 = 0;
	//omSortOrder.RemoveAll( );
	ilCount = omLines.GetSize();
	GPM_LINEDATA rlB;
	ilCount = omLines.GetSize();
	for(int i = 0; i < ilCount; i++)
	{
		omLines[i].Bars.DeleteAll();
	}
	omLines.DeleteAll();
}

void PremisPageDetailViewer::Attach(CWnd *popAttachWnd)
{
    pomAttachWnd = popAttachWnd;
}

void PremisPageDetailViewer::ChangeViewTo(long lpRkey)
{
	ClearAll();
	MakeLines();
	MakeBars();
}


BOOL PremisPageDetailViewer::IsPassFilter(GPMDATA *prpGpm)
{
	BOOL blRet = TRUE;
	if(prpGpm->Rkey == -1)
	{
		return TRUE;
	}
	GHSDATA *prlGhs = ogGhsData.GetGhsByUrno(prpGpm->Ghsu);
	if(prlGhs != NULL)
	{
		if(strcmp(prlGhs->Disp, "X") != 0)
		{
			blRet = FALSE;
		}
	}
	else
	{
		blRet = FALSE;
	}
	if(lmRkey != prpGpm->Rkey)
	{
		blRet = FALSE;
	}
	if(prpGpm->IsChanged == DATA_DELETED)
	{
		return FALSE;
	}
	if(strcmp(prpGpm->Disp, "D") != 0)
	{
		return FALSE;
	}
	return blRet;
}


int PremisPageDetailViewer::CompareLine(GPM_LINEDATA *prpLine1, GPM_LINEDATA *prpLine2)
{
	// Compare in the sort order, from the outermost to the innermost
	int ilCompareResult = 0;

	//if(prpLine2->Urno == -1 || prpLine1->Urno == -1)
	//	return 1;

	if(prpLine2->Lknm == CString("") && prpLine1->Lknm == CString(""))
	{
		return -1;
	}
	else if(prpLine2->Lknm == CString("") && prpLine1->Lknm != CString(""))
	{
		return 1;
	}
	if(prpLine2->Lknm != CString("") && prpLine1->Lknm == CString(""))
	{
		return -1;
	}
/*	if(prpLine2->Urno == -1 && prpLine1->Urno == -1)
	{
		return 0;
	}
	else if(prpLine2->Urno == -1 && prpLine1->Urno != -1)
	{
		return 1;
	}
	if(prpLine2->Urno != -1 && prpLine1->Urno == -1)
	{
		return -1;
	}
*/
	else
	{
		ilCompareResult = (prpLine1->Lknm == prpLine2->Lknm)? 0:
			(prpLine1->Lknm /*>*/< prpLine2->Lknm)? 1: -1;
	}	
	if (ilCompareResult != 0)
		return ilCompareResult;
	return 0;	// we can say that these two lines are equal
}




void PremisPageDetailViewer::MakeLines()
{
	int ilCount = 0;
	int l;
	int ilLineCount = 0;
	CCSPtrArray<GPMDATA> olGpm;
	//TO DO nicht �ber ogGpmData.omData gehen sondern Zeile reaktivieren
	ogGpmData.GetGpmByRkey(olGpm, lmRkey);
	ilCount = olGpm.GetSize();//pomJobData->GetSize();
	int ilAnz = 0;
	//ilCount = ogGpmData.omData.GetSize();
	for(l = 0; l < ilCount; l++)
	{
		GPMDATA rlGpm = olGpm[l];
		if(rlGpm.IsChanged != DATA_DELETED) //MWO XXX
		{
			if(strcmp(rlGpm.Disp, "D") == 0)
			{
				//GPMDATA prlGpm = ogGpmData.omData[l];
				//if()
				MakeLine(&rlGpm);
				ilAnz++;
			}
		}
	}
	//olGpm.DeleteAll();
	for(int i = ilAnz; i < 11; i++)
	{
		MakeEmptyLine();
	}
}

void PremisPageDetailViewer::MakeBars()
{
	int ilCount = 0;

	CCSPtrArray<GPMDATA> olGpm;
	//TO DO nicht �ber ogGpmData.omData gehen sondern Zeile reaktivieren
	ogGpmData.GetGpmByRkey(olGpm, lmRkey);
	ilCount = olGpm.GetSize();
//	ilCount = ogGpmData.omData.GetSize();
	for(int l = 0; l < ilCount; l++)
	{
		GPMDATA prlGpm = olGpm[l];//ogGpmData.omData[l];//olGpm[l];
		if(prlGpm.IsChanged != DATA_DELETED) //MWO XXX
		{
			CString olStr = prlGpm.Dube.Format("%d.%m.%Y-%H:%M");
			olStr = prlGpm.Duen.Format("%d.%m.%Y-%H:%M");
			int ilLineno;
			if (FindDutyBar(prlGpm.Urno, ilLineno))	// corresponding duty bar found?
			{
				MakeBar(ilLineno, &prlGpm);
			}
		}
	}
}

void PremisPageDetailViewer::MakeEmptyLine()
{
	GPM_LINEDATA rlLine;
	rlLine.Urno = -1;
	rlLine.Rkey = -1;
	rlLine.Ghsu = -1;
	rlLine.Urno = -1;
	rlLine.Lknm = CString("");
	CreateLine( &rlLine);
}

//void PremisPageDetailViewer::MakeLineOrManager(GPMDATA *prpGpm) -- pepper for Jobs
void PremisPageDetailViewer::MakeLine(GPMDATA *prpGpm)
{
	// Create a new line in found 

	if(IsPassFilter(prpGpm))
	{
		GPM_LINEDATA rlLine;
		rlLine.Urno = prpGpm->Urno;
		rlLine.Rkey = prpGpm->Rkey;
		rlLine.Ghsu = prpGpm->Ghsu;
		GHSDATA *prlGhs = ogGhsData.GetGhsByUrno(prpGpm->Ghsu);
		if(prlGhs != NULL)
		{
			rlLine.Lknm = prlGhs->Lknm;  
		}
	/*
		rlLine.Urno = prpGpm->Urno; 
		rlLine.Rkey = prpGpm->Rkey; 
		rlLine.Ghsu = prpGpm->Ghsu; 
		rlLine.Dtyp = prpGpm->Dtyp; 
		GHSDATA *prlGhs = ogGhsData.GetGhsByUrno();
		if(prlGhs != NULL)
		{
			rlLine.Lknm = prlGhs->Lknm;  
		}
		rlLine.Prta = prpGpm->Prta; 
		rlLine.Ttdu = prpGpm->Ttdu; 
		rlLine.Dtim = prpGpm->Dtim; 
		rlLine.Tfdu = prpGpm->Tfdu; 
		rlLine.Pota = prpGpm->Pota; 
		rlLine.Lkco = prpGpm->Lkco; 
		rlLine.Vrgc = prpGpm->Vrgc; 
		rlLine.Perm = prpGpm->Perm; 
		Pfkt = prpGpm->Pfkt; 
		rlLine.Noma = prpGpm->Noma; 
		rlLine.Etot = prpGpm->Etot; 
		rlLine.Prio = prpGpm->Prio; 
		rlLine.Didu = prpGpm->Didu; 
		rlLine.Duch = prpGpm->Duch; 
	*/
		CreateLine( &rlLine);
	}
}

void PremisPageDetailViewer::MakeBar(int ipLineno, GPMDATA *prpGpm)
{
	int ilCount = 0;
	GPM_BARDATA rlBar;

	rlBar.Urno = prpGpm->Urno;
	rlBar.Rkey = prpGpm->Rkey;
	rlBar.Ghsu = prpGpm->Ghsu;
	rlBar.Dtyp = CString(prpGpm->Dtyp);
	rlBar.Rtyp = CString(prpGpm->Rtyp);
	rlBar.Dube = CalcStartTime(prpGpm);
	rlBar.Duen = CalcEndTime(prpGpm);
	CString olB = rlBar.Dube.Format("%d.%m.%Y-%H:%M");
	CString olE = rlBar.Duen.Format("%d.%m.%Y-%H:%M");
	//rlBar.Lknm = CString(prpGpm->Lknm);
	rlBar.Prta = prpGpm->Prta;
	rlBar.Ttdu = prpGpm->Ttdu;
	rlBar.Dtim = prpGpm->Dtim;
	rlBar.Tfdu = prpGpm->Tfdu;
	rlBar.Pota = prpGpm->Pota;
	rlBar.Lkco = CString(prpGpm->Lkco);
	rlBar.Vrgc = CString(prpGpm->Vrgc);
	rlBar.Perm = CString(prpGpm->Perm);
	rlBar.Pfkt = CString(prpGpm->Pfkt);
	rlBar.Noma = prpGpm->Noma;
	rlBar.Etot = prpGpm->Etot;
	rlBar.Prio = prpGpm->Prio;
	rlBar.Didu = prpGpm->Didu;
	rlBar.Duch = prpGpm->Duch;
	//TO DO fill text for the Bar Text
	rlBar.Text = BarTextAndValues(prpGpm);//CString("10/10 ") + pclTmp;

	rlBar.FrameType = FRAMERECT;
	rlBar.MarkerType = MARKFULL;
	if(rlBar.Dtyp == "T")
	{
		rlBar.MarkerBrush = ogBrushs[YELLOW_IDX];
	}
	else if(rlBar.Dtyp == "F")
	{
		rlBar.MarkerBrush = ogBrushs[WHITE_IDX];
	}
	else if(rlBar.Dtyp == "N")
	{
		rlBar.MarkerBrush = ogBrushs[LIME_IDX];
	}
	rlBar.TextColor = BLACK;//ogColors[IDX_BLACK];
	rlBar.BkColor = YELLOW;//ogColors[IDX_YELLOW];
	int ilBarno = CreateBar(ipLineno, &rlBar);
}


CString PremisPageDetailViewer::LineText(GPMDATA *prpGpm)
{
	char buf[512];
	return buf;
}

CString PremisPageDetailViewer::BarTextAndValues(GPMDATA *prpGpm)
{
	CString olText("");
	char pclTmp[500]="";
	char pclAdd[300]="";
	CString olAdd;
	int ilFrom = prpGpm->Prta + prpGpm->Ttdu;
	int ilTo = prpGpm->Pota + prpGpm->Tfdu;

	CString olCgru;
	olCgru = CString(prpGpm->Perm);
	CStringArray olStrArray;
	ExtractItemList(olCgru, &olStrArray, ';');
	int ilItemCount = olStrArray.GetSize();
	for(int i = 0; i < ilItemCount; i++)
	{
		GRNDATA *prlGrn = ogGrnData.GetGrnByUrno(atol(olStrArray[i]));
		if(prlGrn != NULL)
		{
			if(i == 0)
			{
				olText += prlGrn->Grpn;
			}
			else
			{
				olText += CString("/") + prlGrn->Grpn;
			}
		}
	}


/*	if(prpGpm->Rtyp == CString("P"))
	{
		if(strcmp(prpGpm->Pfkt, "") != 0)
		{
			//pomAttachWnd
			CString olUrno;
			CString olSubString = prpGpm->Pfkt;
			bool blEnd = false;
			if(!olSubString.IsEmpty())
			{
				int pos;
				int olPos = 0;
				while(blEnd == false)
				{
					pos = olSubString.Find(';');
					if(pos == -1)
					{
						blEnd = true;
						olUrno = olSubString;
					}
					else
					{
						olUrno = olSubString.Mid(0, olSubString.Find(';'));
						olSubString = olSubString.Mid(olSubString.Find(';')+1, olSubString.GetLength( )-olSubString.Find(';')+1);
					}
					//omPfcUrnos.Add(atoi(olUrno));
					for(int k = 0; k < ((PremisPage *)pomAttachWnd)->omPfcData.GetSize(); k++)
					{
						if(((PremisPage *)pomAttachWnd)->omPfcData[k].Urno == atoi(olUrno))
						{
							olAdd +=  CString(((PremisPage *)pomAttachWnd)->omPfcData[k].Fctc) + CString("/");
						}
					}
				}
			}
			
		}
		olAdd = olAdd.Mid(0, olAdd.GetLength()-1);
		sprintf(pclAdd, " %s", olAdd);
	}
	else if(prpGpm->Rtyp == CString("G"))
	{
		if(strcmp(prpGpm->Vrgc, "") != 0)
		{
			CString olUrno;
			CString olSubString = prpGpm->Vrgc;
			bool blEnd = false;
			if(!olSubString.IsEmpty())
			{
				int pos;
				int olPos = 0;
				while(blEnd == false)
				{
					pos = olSubString.Find(';');
					if(pos == -1)
					{
						blEnd = true;
						olUrno = olSubString;
					}
					else
					{
						olUrno = olSubString.Mid(0, olSubString.Find(';'));
						olSubString = olSubString.Mid(olSubString.Find(';')+1, olSubString.GetLength( )-olSubString.Find(';')+1);
					}
					for(int k = 0; k < ((PremisPage *)pomAttachWnd)->omGegData.GetSize(); k++)
					{
						if(((PremisPage *)pomAttachWnd)->omGegData[k].Urno == atoi(olUrno))
						{
							olAdd +=  CString(((PremisPage *)pomAttachWnd)->omGegData[k].Gcde) + CString("/");
						}
					}
				}
			}
			
		}
		olAdd = olAdd.Mid(0, olAdd.GetLength()-1);
		sprintf(pclAdd, " %s", olAdd);
	}
	sprintf(pclTmp, "%d/%d (%dMA) %s", ilFrom, ilTo, prpGpm->Noma, pclAdd);
	olText = CString(pclTmp);
*/
	return olText;

}



BOOL PremisPageDetailViewer::FindDutyBar(/*CTime opDube,*/ long lpUrno, int &ripLineno)
{
		for (ripLineno = 0; ripLineno < GetLineCount(); ripLineno++)
			if (GetLine(ripLineno)->Urno == lpUrno)
				return TRUE;

	return FALSE;
}

BOOL PremisPageDetailViewer::FindBarGlobal(long lpUrno, int &ripLineno, int &ripBarno)
{
	int ilCount = omLines.GetSize();
		for(ripLineno = 0; ripLineno < ilCount; ripLineno++)
			for (ripBarno = 0; ripBarno < GetBarCount(ripLineno); ripBarno++)
				if (GetBar( ripLineno, ripBarno)->Urno == lpUrno)
					return TRUE;

	return FALSE;
}

BOOL PremisPageDetailViewer::FindBar(/*CTime opDube,*/long lpUrno, int &ripLineno, int &ripBarno)
{
			for (ripBarno = 0; ripBarno < GetBarCount(ripLineno); ripBarno++)
				if (GetBar( ripLineno, ripBarno)->Urno == lpUrno)
					return TRUE;

	return FALSE;
}


/////////////////////////////////////////////////////////////////////////////
// PremisPageDetailViewer -- Viewer basic operations
//

int PremisPageDetailViewer::GetNextFreeLine()
{
	for(int i = 0; i < omLines.GetSize(); i++)
		if(omLines[i].Rkey == -1)
			return i;

	return -1;
}

int PremisPageDetailViewer::GetLineCount()
{
    return omLines.GetSize();
}

GPM_LINEDATA *PremisPageDetailViewer::GetLine(int ipLineno)
{
    return &omLines[ipLineno];
}

CString PremisPageDetailViewer::GetLineText(int ipLineno, COLORREF *prpTextColor, COLORREF *prpBkColor)
{
	if(ipLineno < omLines.GetSize())
	{
		if((omLines[ipLineno].isAdditionalSpecial == TRUE) && (omLines[ipLineno].Urno != -1))
		{
			*prpTextColor = YELLOW;//ogColors[IDX_YELLOW];
			*prpBkColor = GRAY;//ogColors[IDX_GRAY];
		}
		else
		{
			*prpTextColor = BLACK;//ogColors[IDX_BLACK];
			*prpBkColor = SILVER;//ogColors[IDX_SILVER];
		}
		if(ipLineno > 0)
		{
			if(omLines[ipLineno-1].Lknm == omLines[ipLineno].Lknm)
			{
				return CString("");
			}
		}
		return omLines[ipLineno].Lknm; 
	}
	return CString("");
}

int PremisPageDetailViewer::GetVisualMaxOverlapLevel(int ipLineno)
{
	// For max level 0,1,2 we will use 2 levels, 3,4,5 we will use 5 levels, 6,7,8 we use 8 levels, and so on ...
	return GetMaxOverlapLevel(ipLineno);//MWO  / 3 * 3 + 2;
}

int PremisPageDetailViewer::GetMaxOverlapLevel( int ipLineno)
{
    return omLines[ipLineno].MaxOverlapLevel;
}


int PremisPageDetailViewer::GetBarCount(int ipLineno)
{
	 if(ipLineno < omLines.GetSize())
		return omLines[ipLineno].Bars.GetSize();
	 else
		 return 0;
}

GPM_BARDATA *PremisPageDetailViewer::GetBar(int ipLineno, int ipBarno)
{
    return &omLines[ipLineno].Bars[ipBarno];
}

CString PremisPageDetailViewer::GetBarText( int ipLineno, int ipBarno)
{
    return omLines[ipLineno].Bars[ipBarno].Text;
}



/////////////////////////////////////////////////////////////////////////////
// PremisPageDetailViewer - GPM_BARDATA array maintenance (overlapping version)
//



int PremisPageDetailViewer::CreateLine( GPM_LINEDATA *prpLine)
{
    int ilLineCount = omLines.GetSize();

// Generally, raw data will be sorted from left to right.
// So, scanning for the place of insertion backward is a little bit faster
//
    for (int ilLineno = 0; ilLineno < ilLineCount; ilLineno++)
		if (CompareLine(prpLine, GetLine(ilLineno)) >= 0)
            break;  // should be inserted after Lines[ilLineno-1]

    omLines.NewAt(ilLineno, *prpLine);
    GPM_LINEDATA *prlLine = &omLines[ilLineno];
    prlLine->MaxOverlapLevel = 0;   // there is no bar in this line right now
    return ilLineno;
}

void PremisPageDetailViewer::DeleteLine( int ipLineno)
{
    while (GetBarCount(ipLineno) > 0)
        DeleteBar(ipLineno, 0);

    omLines.DeleteAt(ipLineno);
}


// no overlapping version, data will be sorted by the field StartTime



void PremisPageDetailViewer::GetOverlappedBarsFromTime( int ipLineno,
	CTime opTime1, CTime opTime2, CUIntArray &ropBarnoList)
{
    GPM_LINEDATA *prlLine = GetLine(ipLineno);
    int ilBarCount = prlLine->Bars.GetSize();

	// Prepare the empty array of the involvement flag for each bar
	CCSPtrArray<BOOL> IsInvolvedBar;
	for (int i = 0; i < ilBarCount; i++)
		IsInvolvedBar.NewAt(i, FALSE);

	// We will loop again and again until there is no more bar in this overlapping found.
	// The overlapping group is at first determine by the given "opTime1" and "opTime2".
	// But if the bar we find is exceed this period of time, we will extend searching
	// to both left side and right side in the next round.
	//
	int nBarsFoundThisRound = -1;
	while (nBarsFoundThisRound != 0)
	{
		nBarsFoundThisRound = 0;
		for (int ilBarno = 0; ilBarno < ilBarCount; ilBarno++)
		{
			CTime olStartTime = prlLine->Bars[ilBarno].Dube;
			CTime olEndTime = prlLine->Bars[ilBarno].Duen;
			BOOL blIsOverlapped = IsReallyOverlapped(olStartTime, olEndTime, opTime1, opTime2);
			BOOL blIsWithIn = IsWithIn(olStartTime, olEndTime, opTime1, opTime2);
				// this IsWithIn() fix the real thin bar bug
			BOOL blIsInvolved = (blIsOverlapped || blIsWithIn);

			if (!IsInvolvedBar[ilBarno] && blIsInvolved)	// some more new involved bars?
			{
				IsInvolvedBar[ilBarno] = TRUE;
				nBarsFoundThisRound++;
				opTime1 = min(opTime1, prlLine->Bars[ilBarno].Dube);
				opTime2 = max(opTime2, prlLine->Bars[ilBarno].Duen);
			}
		}
	}

	// Create the list of involved bars, then store them to "ropBarnoList"
	ropBarnoList.RemoveAll();
	for (int ilBarno = 0; ilBarno < ilBarCount; ilBarno++)
	{
		if (IsInvolvedBar[ilBarno])
		{
			ropBarnoList.Add(ilBarno);
		}
	}
	IsInvolvedBar.DeleteAll();
}



int PremisPageDetailViewer::CreateBar(int ipLineno, GPM_BARDATA *prpBar, BOOL bpTopBar)
{
    GPM_LINEDATA *prlLine = GetLine( ipLineno);
    int ilBarCount = omLines[ipLineno].Bars.GetSize();

	// Id 14-Sep-1996
	// I use a new algorithm for bar insertion and new deletion.
	// This new algorithm will work somewhat slower than the previous version, but safer.
	// To give you a brief idea of this algorithm, here is the essential of insertion:
	//
	// Case A: Insert a new bar to be the topmost bar
	//		We will scan every bars, and for each bar which is in the same overlap group
	//		with the new bar, we will shift it downward one level, and update the field
	//		Line.MaxOverlapLevel. This new bar will be the last bar in painting order
	//		(for being topmost), and has overlapping level = 0.
	//
	// Case B: Insert a new bar to be the bottommost bar
	//		We will scan every bars for finding the maximum overlap level of bars which
	//		are in the same overlap group with this new bar (use level -1 if not found).
	//		Then use this maximum overlap level + 1 as overlap level of this new bar.
	//		This new bar will be the first bar in painting order (for being bottommost).
	//
	CUIntArray olBarnoList;
	GetOverlappedBarsFromTime(ipLineno,
		prpBar->Dube, prpBar->Duen, olBarnoList);
	int ilListCount = olBarnoList.GetSize();

	if (bpTopBar)
	{
		for (int ilLc = 0; ilLc < ilListCount; ilLc++)
		{
			int ilBarno = olBarnoList[ilLc];
			GPM_BARDATA *prlBar = &prlLine->Bars[ilBarno];
			int ilNewOverlapLevel = ++prlBar->OverlapLevel;
			prlLine->MaxOverlapLevel = max(prlLine->MaxOverlapLevel, ilNewOverlapLevel);
		}
		prlLine->Bars.NewAt(ilBarCount, *prpBar);	// to be the last bar in painting order
		prlLine->Bars[ilBarCount].OverlapLevel = 0;
		return ilBarCount;
	}
	else
	{
		int ilNewBarLevel = -1;
		for (int ilLc = 0; ilLc < ilListCount; ilLc++)
		{
			int ilBarno = olBarnoList[ilLc];
			GPM_BARDATA *prlBar = &prlLine->Bars[ilBarno];
			ilNewBarLevel = max(ilNewBarLevel, prlBar->OverlapLevel + 1);
				// the +1 means that we have to insert the new bar below this bar
		}
		prlLine->Bars.NewAt(0, *prpBar);	// to be the first bar in painting order
		prlLine->Bars[0].OverlapLevel = ilNewBarLevel;
		prlLine->MaxOverlapLevel = max(prlLine->MaxOverlapLevel, ilNewBarLevel);
		return 0;
	}
}

void PremisPageDetailViewer::DeleteBar(int ipLineno, int ipBarno)
{

    GPM_LINEDATA *prlLine = GetLine( ipLineno);
    GPM_BARDATA *prlBar = &prlLine->Bars[ipBarno];	// given bar is in painting order
	// Id 14-Sep-1996
	// I use a new algorithm for bar insertion and new deletion.
	// This new algorithm will work somewhat slower than the previous version, but safer.
	// To give you a brief idea of this algorithm, here is the essential of deletion:
	//
	// We will scan every bars, and for each bar which is in the same overlap group with
	// the new bar, we will back up them to a temporary array. Then we remove every bars
	// in this overlapping group, and insert these bar from the temporary array except
	// the one which has to be deleted back to the viewer.
	//
	// In some situation, if this overlapping group controls the maximum overlap level of
	// the line, we may have to rescan every bars in this line to make sure that the
	// maximum overlap level still be correct.
	//
	CUIntArray olBarnoList;
	GetOverlappedBarsFromTime(ipLineno,
		prlBar->Dube, prlBar->Duen, olBarnoList);
	int ilListCount = olBarnoList.GetSize();

	// Save the bar in this overlapping group to a temporary array
	CCSPtrArray<GPM_BARDATA> olSavedBars;
	BOOL blMustRecomputeMaxOverlapLevel = FALSE;
	for (int ilLc = 0; ilLc < ilListCount; ilLc++)
	{
		int ilBarno = olBarnoList[ilLc];
		GPM_BARDATA *prlBar = &prlLine->Bars[ilBarno];
		blMustRecomputeMaxOverlapLevel |= (prlBar->OverlapLevel == prlLine->MaxOverlapLevel);
			// check if deletion of this bar may change the line height
		if (ilBarno != ipBarno)	// must we save this bar?
			olSavedBars.NewAt(olSavedBars.GetSize(), prlLine->Bars[ilBarno]);
	}
	// Delete all bars (which are already backed up in a temporary array) from the viewer
	while (ilListCount-- > 0)
	{
		int ilBarno = olBarnoList[ilListCount];
		prlLine->Bars.DeleteAt(ilBarno);
	}
	// Then, we insert those bars back to the Gantt chart, except the specified one
	int ilSavedBarCount = olSavedBars.GetSize();
	for (int ilSavedBarno = 0; ilSavedBarno < ilSavedBarCount; ilSavedBarno++)
	{
		CreateBar(ipLineno, &olSavedBars[ilSavedBarno]);
	}
	olSavedBars.DeleteAll();
	// Then recompute the MaxOverlapLevel if it's necessary
	if (blMustRecomputeMaxOverlapLevel)
	{
		prlLine->MaxOverlapLevel = 0;
	    int ilBarCount = prlLine->Bars.GetSize();
		for (int ilBarno = 0; ilBarno < ilBarCount; ilBarno++)
		{
			GPM_BARDATA *prlBar = &prlLine->Bars[ilBarno];
			prlLine->MaxOverlapLevel = max(prlLine->MaxOverlapLevel, prlBar->OverlapLevel);
		}
	}
}



/////////////////////////////////////////////////////////////////////////////
// PremisPageDetailViewer - GPM_BARDATA calculation routines

// Return the bar number of the list box based on the given period [time1, time2]
int PremisPageDetailViewer::GetBarnoFromTime( int ipLineno, CTime opTime1, CTime opTime2,
    int ipOverlapLevel1, int ipOverlapLevel2)
{
    for (int i = GetBarCount(ipLineno)-1; i >= 0; i--)
    {
        GPM_BARDATA *prlBar = GetBar( ipLineno, i);
        if (opTime1 <= prlBar->Duen && prlBar->Dube<= opTime2 &&
            ipOverlapLevel1 <= prlBar->OverlapLevel && prlBar->OverlapLevel <= ipOverlapLevel2)
            return i;
    }
    return -1;
}



/////////////////////////////////////////////////////////////////////////////
// PremisPageDetailViewer private methods

// Returns the first bar no (in the time order) of the given overlap grop.
// What's special to this method is we could get the same overlapping bar either
// give "ipBarno" in paint order or "ipBarno" in time order.
//
int PremisPageDetailViewer::GetFirstOverlapBarno(GPM_LINEDATA *prpLine, int ipBarno)
{
    if (IsBetween(ipBarno, 0, prpLine->Bars.GetSize() - 1)) // valid element in array?
        return ipBarno - GetBarInTimeOrder(prpLine,ipBarno)->OverlapLevel;
    return -1;
}

// Return an pointer to the data of the specified bar (given bar is in time order)
GPM_BARDATA *PremisPageDetailViewer::GetBarInTimeOrder(GPM_LINEDATA *prpLine, int ipBarno)
{
    //return &prpLine->Bars[ipBarno + prpLine->TimeOrder[ipBarno]];
  // MCU 07.07 I've splitted the line for testing purpose (to see the i value)
	int ilBarno = max(ipBarno, 0);
	int ilIndex ;
	if(ilBarno < prpLine->Bars.GetSize())
	{
		 ilIndex = min(ipBarno + prpLine->TimeOrder[ilBarno],max(prpLine->Bars.GetSize()-1,0));
	}
	else
	{
		ilIndex = prpLine->Bars.GetSize()-1;
	}
    return &prpLine->Bars[max(ilIndex,0)];
}

// Return the right most time of the bars which is displayed before this bar (given bar is in time order)
CTime PremisPageDetailViewer::GetEndTimeOfBarsOnTheLeft(GPM_LINEDATA *prpLine, int ipBarno)
{
    CTime olEndTimeOfBarsOnTheLeft = TIMENULL;  // provide default value in case of error

    // Get the maximum ending time of the bars on the left of the given bar
    int ilFirstBar = GetFirstOverlapBarno(prpLine, ipBarno-1);
    if (ilFirstBar != -1)
        for (int i = ilFirstBar; GetFirstOverlapBarno(prpLine, i) == ilFirstBar; i++)
        {
            CTime olEndTimeOfThisBar = prpLine->Bars[i].Duen;
            if (olEndTimeOfThisBar > olEndTimeOfBarsOnTheLeft)
                olEndTimeOfBarsOnTheLeft = olEndTimeOfThisBar;
        }

    return olEndTimeOfBarsOnTheLeft;
}

// This routine will delete "ilTotalBars" bars start from the bar number
// "ilFirstBar" in the prlLine->Bars[]. Then all of these bars will be inserted into
// the array again, expected to be in the correct order.
void PremisPageDetailViewer::ReorderBars(int ipLineno, int ilFirstBar, int ilTotalBars)
{
    CCSPtrArray<GPM_BARDATA> Bars;
    GPM_LINEDATA *prlLine = GetLine(ipLineno);
    int ilBarno;

    // copy bars data out of the array and save them into the local "Bars" array
    for (ilBarno = 0; ilBarno < ilTotalBars; ilBarno++)
    {
        Bars.Add(&prlLine->Bars[ilFirstBar]);
        prlLine->Bars.RemoveAt(ilFirstBar);
        prlLine->TimeOrder.DeleteAt(ilFirstBar);
    }

    // insert bar to those machine one by one, then clean up the memory allocated for bars
    for (ilBarno = 0; ilBarno < ilTotalBars; ilBarno++)
    {
        GPM_BARDATA *prlBar = &Bars[ilBarno];
        CreateBar(ipLineno, prlBar);
        delete prlBar;
    }
}


/////////////////////////////////////////////////////////////////////////////
// PremisPageDetailViewer Jobs creation methods

static void GpmDetailCf(void *popInstance, int ipDDXType,
    void *vpDataPointer, CString &ropInstanceName)
{
    PremisPageDetailViewer *polViewer = (PremisPageDetailViewer *)popInstance;

	if (ipDDXType == GPM_NEW)
		polViewer->ProcessGpmNew((GPMDATA *)vpDataPointer);
	if (ipDDXType == GPM_CHANGE)
		polViewer->ProcessGpmChange((GPMDATA *)vpDataPointer);
	if (ipDDXType == GPM_DELETE)
		polViewer->ProcessGpmDelete((GPMDATA *)vpDataPointer);
	if (ipDDXType == GPM_PREMIS_DELETE)
		polViewer->ProcessPremisDelete((GPMDATA *)vpDataPointer);
}

//-----------------------------------------------------
// From DDX ==> there is a new Employee

void PremisPageDetailViewer::ProcessGpmNew(GPMDATA *prpGpm)
{
	int ilLineno;  //GetLine(int ipLineno)
	//FindBarGlobal(prpGpm->Urno, ilLineno, ilBarno);
	if(IsPassFilter(prpGpm))
	{
		MakeLine(prpGpm);
		if(FindDutyBar(prpGpm->Urno, ilLineno) == TRUE)
		{
			MakeBar(ilLineno, prpGpm);
			if (::IsWindow(pomAttachWnd->GetSafeHwnd()))
			{
				LONG lParam = ilLineno;
				pomAttachWnd->SendMessage(WM_UPDATEDIAGRAM, UD_INSERTLINE, lParam);
			}
		}
	}
}

void PremisPageDetailViewer::ProcessGpmChange(GPMDATA *prpGpm)
{
	int ilLineno;
	int ilBarno;
	//ChangeViewTo(SelectView());
	//return;
	if (FindBarGlobal(prpGpm->Urno, ilLineno, ilBarno))	// corresponding duty bar found
	{
		GPM_BARDATA rlBar;
		rlBar.Urno = prpGpm->Urno;
		rlBar.Rkey = prpGpm->Rkey;
		rlBar.Ghsu = prpGpm->Ghsu;
		rlBar.Dtyp = CString(prpGpm->Dtyp);
		rlBar.Rtyp = CString(prpGpm->Rtyp);
		rlBar.Dube = CalcStartTime(prpGpm);
		rlBar.Duen = CalcEndTime(prpGpm);
		CString olB = rlBar.Dube.Format("%d.%m.%Y-%H:%M");
		CString olE = rlBar.Duen.Format("%d.%m.%Y-%H:%M");
		GHSDATA *prlGhs = ogGhsData.GetGhsByUrno(prpGpm->Ghsu);
		if(prlGhs != NULL)
		{
			rlBar.Lknm = CString(prlGhs->Lknm);
		}
		rlBar.Prta = prpGpm->Prta;
		rlBar.Ttdu = prpGpm->Ttdu;
		rlBar.Dtim = prpGpm->Dtim;
		rlBar.Tfdu = prpGpm->Tfdu;
		rlBar.Pota = prpGpm->Pota;
		rlBar.Lkco = CString(prpGpm->Lkco);
		rlBar.Vrgc = CString(prpGpm->Vrgc);
		rlBar.Perm = CString(prpGpm->Perm);
		rlBar.Pfkt = CString(prpGpm->Pfkt);
		rlBar.Noma = prpGpm->Noma;
		rlBar.Etot = prpGpm->Etot;
		rlBar.Prio = prpGpm->Prio;
		rlBar.Didu = prpGpm->Didu;
		rlBar.Duch = prpGpm->Duch;

		//TO DO fill text for the Bar Text
		//rlBar.Text = prpGpm->Text;
		//char pclTmp[20]="";
		//sprintf(pclTmp, "(%dMA) -->", prpGpm->Noma);
		rlBar.Text = BarTextAndValues(prpGpm);//CString("10/10 ") + pclTmp;

		rlBar.FrameType = FRAMERECT;
		rlBar.MarkerType = MARKFULL;
		if(prpGpm->IsMarked == true) //== ((PremisPage *)pomAttachWnd)->pomCurrentGpm)
		{
			rlBar.MarkerBrush = ogBrushs[AQUA_IDX];
		}
		else
		{
			if(rlBar.Dtyp == "T")
			{
				rlBar.MarkerBrush = ogBrushs[YELLOW_IDX];
			}
			else if(rlBar.Dtyp == "F")
			{
				rlBar.MarkerBrush = ogBrushs[WHITE_IDX];
			}
			else if(rlBar.Dtyp == "N")
			{
				rlBar.MarkerBrush = ogBrushs[LIME_IDX];
			}
		}
		rlBar.TextColor = BLACK;//ogColors[IDX_BLACK];
		int ilOldMaxOverlapLevel = GetVisualMaxOverlapLevel(ilLineno);
		DeleteBar(ilLineno, ilBarno);
		CreateBar(ilLineno, &rlBar, TRUE);

		 // Notifies the attached window (if exist)
		if (::IsWindow(pomAttachWnd->GetSafeHwnd()))
		{
			   /*CJobDlg *polWnd = (CJobDlg *)pomAttachWnd;
				CTimeSpan olTo = (prpGpm->Dube + CTimeSpan(0,5,0,0)) - TIMENULL;
		      polWnd->omTimeScale.SetDisplayTimeFrame((prpGpm->Dube - CTimeSpan(0,1,0,0)), olTo, CTimeSpan(0, 0, 10, 0));
				polWnd->omGantt.SetDisplayWindow((prpGpm->Dube - CTimeSpan(3600)), (prpGpm->Dube +  polWnd->omTimeScale.GetDisplayDuration()));
				ChangeViewTo(SelectView(), (prpGpm->Dube - CTimeSpan(0,1,0,0)), prpGpm->Dube + CTimeSpan(0,5,0,0));
				*/
			 LONG lParam = ilLineno;

			 pomAttachWnd->SendMessage(WM_UPDATEDIAGRAM,	UD_UPDATELINE, lParam);
			 
		}
   }// if (FindBarGlobal(prpGpm->Key, ilLineno, ilBarno))	
	else
	{
		if(IsPassFilter(prpGpm))
		{
			MakeEmptyLine();
			int ilFreeLine = GetNextFreeLine();
			if(ilFreeLine != -1)
			{
				//strcpy(omLines[ilFreeLine].Fkey, prpGpm->Fkey);
				MakeBar(ilFreeLine, prpGpm);
				if (::IsWindow(pomAttachWnd->GetSafeHwnd()))
				{
					LONG lParam = ilLineno;
					pomAttachWnd->SendMessage(WM_UPDATEDIAGRAM, UD_INSERTLINE , lParam);
					lParam = ilFreeLine;
					pomAttachWnd->SendMessage(WM_UPDATEDIAGRAM, UD_INSERTLINE , lParam);
					
				}
			}
		}
	}
}
CTime PremisPageDetailViewer::CalcStartTime(GPMDATA *prpGpm)
{
	CTime olTime;
	CString olB = omOnBlock.Format("%d.%m.%Y-%H:%M");
	if(strcmp(prpGpm->Dtyp, "T") == 0) //Turnarround
	{
		olTime = omOnBlock;
	}
	else if(strcmp(prpGpm->Dtyp, "F") == 0) //Departure
	{
		olTime = omOfBlock;
		olTime -= CTimeSpan(prpGpm->Dtim*60);
	}
	else if(strcmp(prpGpm->Dtyp, "N") == 0) //Arrival
	{
		olTime = omOnBlock;
	}
	CString olB2 = olTime.Format("%d.%m.%Y-%H:%M");
	olTime -= CTimeSpan(prpGpm->Prta*60);
	olTime -= CTimeSpan(prpGpm->Ttdu*60);
	olB2 = olTime.Format("%d.%m.%Y-%H:%M");
	return olTime;
}

CTime PremisPageDetailViewer::CalcEndTime(GPMDATA *prpGpm)
{
	CTime olTime;
	if(strcmp(prpGpm->Dtyp, "T") == 0) //Turnarround
	{
		olTime = omOfBlock;
	}
	else if(strcmp(prpGpm->Dtyp, "F") == 0) //Departure
	{
		olTime = omOfBlock;
	}
	else if(strcmp(prpGpm->Dtyp, "N") == 0) //Arrival
	{
		olTime = omOnBlock;
		olTime += CTimeSpan(prpGpm->Dtim*60);
	}
	CString olB2 = olTime.Format("%d.%m.%Y-%H:%M");
	olTime += CTimeSpan(prpGpm->Tfdu*60);
	olTime += CTimeSpan(prpGpm->Pota*60);
	olB2 = olTime.Format("%d.%m.%Y-%H:%M");
	return olTime;
}

void PremisPageDetailViewer::ProcessPremisDelete(GPMDATA *prpGpm)
{
	pomAttachWnd->SendMessage(WM_UPDATEDIAGRAM,	UD_RESETCONTENT, 0);
	ClearAll();
}

void PremisPageDetailViewer::ProcessGpmDelete(GPMDATA *prpGpm)
{
	int ilLineno;

	if (FindDutyBar(prpGpm->Urno, ilLineno))	// corresponding duty bar found
	//if (FindBar(prpGpm->Key, ilLineno, ilBarno))	// corresponding duty bar found
	{
		// Update the Viewer's data
		int ilOldMaxOverlapLevel = VisualMaxOverlapLevel( ilLineno);
//		DeleteLine(ilLineno);
		// Notifies the attached window (if exist)
		if (::IsWindow(pomAttachWnd->GetSafeHwnd()))
		{
			LONG lParam = ilLineno;
			DeleteLine(ilLineno);
			pomAttachWnd->SendMessage(WM_UPDATEDIAGRAM,	UD_DELETELINE, lParam);
			MakeEmptyLine();
			int ilNewLine = omLines.GetSize()-1;
			if (::IsWindow(pomAttachWnd->GetSafeHwnd()))
			{
				LONG lParam = ilNewLine;
				pomAttachWnd->SendMessage(WM_UPDATEDIAGRAM, UD_INSERTLINE, lParam);
			}

/*			ChangeViewTo(0);
			if(((PremisPage *)pomAttachWnd)->pomGantt != NULL)
			{
				((PremisPage *)pomAttachWnd)->pomGantt->DestroyWindow();
				delete ((PremisPage *)pomAttachWnd)->pomGantt;
				((PremisPage *)pomAttachWnd)->MakeNewGantt();
				((PremisPage *)pomAttachWnd)->pomGantt->RepaintGanttChart();

			}
*/
		}
    }

}

