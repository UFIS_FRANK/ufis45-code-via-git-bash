#if !defined(AFX_GROUPNAMESPAGE_H__CFE7A532_40B2_11D1_9579_0000B4392C49__INCLUDED_)
#define AFX_GROUPNAMESPAGE_H__CFE7A532_40B2_11D1_9579_0000B4392C49__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// GroupNamesPage.h : header file
//
#include "CedaALTData.h"
#include "CedaACTData.h"
#include "CedaAPTData.h"
#include "CedaHTYData.h"
#include "CedaPSTData.h"
#include "CedaGATData.h"
#include "CedaCICData.h"
#include "CedaBLTData.h"
#include "CedaGrmData.h"
#include "CedaGrnData.h"
#include "CedaGhsData.h"
#include "CedaNATData.h"
#include "CedaGhpData.h"
#include "CedaBsdData.h"
#include "CCSEdit.h"
#include "Table.h"

enum 
{
	NEW_MODE,
	UPDATE_MODE
};

struct GROUPS
{
	GRNDATA Group;
	CCSPtrArray<GRMDATA> GroupMembers;
};

/////////////////////////////////////////////////////////////////////////////
// GroupNamesPage dialog

class GroupNamesPage : public CPropertyPage
{
	DECLARE_DYNCREATE(GroupNamesPage)

// Construction
public:
	GroupNamesPage();
	~GroupNamesPage();

// Dialog Data
	//{{AFX_DATA(GroupNamesPage)
	enum { IDD = IDD_NAMEGROUP_PAGE };
	CListBox	m_PremisList;
	CComboBox	m_Tables;
	CComboBox	m_GroupNames;
	CListBox	m_Available;
	CListBox	m_Assigned;
	CCSEdit	m_GRSN;
	//}}AFX_DATA


	CCSPtrArray<ALTDATA> omAltList;
	CCSPtrArray<ACTDATA> omActList;
	CCSPtrArray<APTDATA> omAptList;
	CCSPtrArray<HTYDATA> omHtyList;
	CCSPtrArray<PSTDATA> omPstList;
	CCSPtrArray<GATDATA> omGatList;
	CCSPtrArray<CICDATA> omCicList;
	CCSPtrArray<BLTDATA> omBltList;
	CCSPtrArray<GHSDATA> omGhsList;
	CCSPtrArray<NATDATA> omNatList;
	CCSPtrArray<GHPDATA> omGhpList;
	CCSPtrArray<BSDDATA> omBsdList;


	CCSPtrArray<ACTDATA> omGrpActList;
	CCSPtrArray<APTDATA> omGrpAptList;
	CCSPtrArray<ALTDATA> omGrpAltList;
	CCSPtrArray<HTYDATA> omGrpHtyList;
	CCSPtrArray<PSTDATA> omGrpPstList;
	CCSPtrArray<GATDATA> omGrpGatList;
	CCSPtrArray<CICDATA> omGrpCicList;
	CCSPtrArray<BLTDATA> omGrpBltList;
	CCSPtrArray<GHSDATA> omGrpGhsList;
	CCSPtrArray<NATDATA> omGrpNatList;
	CCSPtrArray<GHPDATA> omGrpGhpList;
	CCSPtrArray<BSDDATA> omGrpBsdList;

	CCSPtrArray<GROUPS> omGroupData;
	CUIntArray omGhpUrnos;
	GROUPS *pomCurrentGroup;
	CString omCurrentTable;

	int imMode;

	bool IsPassFilter();
	void ShowGroupAndMembers(CString &ropGroupName);
	void ShowGroupMembers();
	void LoadGroupsForTable();
	void ClearGroups();
	void MakeDiffList();
	void ReloadFromMemory();

	void LoadActFields();
	void LoadAptFields();
	void LoadAltFields();
	void LoadHtyFields();
	void LoadPstFields();
	void LoadGatFields();
	void LoadCicFields();
	void LoadBltFields();
	void LoadGhsFields();
	void LoadNatFields();
	void LoadGhpFields();
	void LoadBsdFields();

	LONG OnDeleteButton(WPARAM wParam, LPARAM lParam);

	void ShowPremisList();
// Overrides
	// ClassWizard generate virtual function overrides
	//{{AFX_VIRTUAL(GroupNamesPage)
	public:
	virtual BOOL OnKillActive();
	virtual BOOL OnSetActive();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	// Generated message map functions
	//{{AFX_MSG(GroupNamesPage)
	virtual BOOL OnInitDialog();
	afx_msg void OnAdd();
	afx_msg void OnDelete();
	virtual void OnOK();
	afx_msg void OnSelchangeCbTables();
	afx_msg void OnSelchangeCbGroupname();
	afx_msg void OnEditchangeCbGroupname();
	afx_msg void OnDblclkPremisList();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_GROUPNAMESPAGE_H__CFE7A532_40B2_11D1_9579_0000B4392C49__INCLUDED_)
