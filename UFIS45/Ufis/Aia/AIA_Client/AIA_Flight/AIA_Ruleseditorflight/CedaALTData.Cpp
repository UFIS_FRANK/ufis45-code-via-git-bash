// CedaALTData.cpp
 
#include "stdafx.h"
#include "CedaALTData.h"

// Local function prototype
static void ProcessALTCf(void *popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

//--CEDADATA-----------------------------------------------------------------------------------------------

CedaALTData ogAltData;


CedaALTData::CedaALTData()
{
    // Create an array of CEDARECINFO for ALTDATA
	BEGIN_CEDARECINFO(ALTDATA,ALTDataRecInfo)
		FIELD_LONG		(Urno,"URNO")
		FIELD_DATE		(Cdat,"CDAT")
		FIELD_CHAR_TRIM	(Usec,"USEC")
		FIELD_DATE		(Lstu,"LSTU")
		FIELD_CHAR_TRIM	(Prfl,"PRFL")
		FIELD_CHAR_TRIM	(Useu,"USEU")
		FIELD_CHAR_TRIM	(Alc2,"ALC2")
		FIELD_CHAR_TRIM	(Alc3,"ALC3")
		FIELD_CHAR_TRIM	(Alfn,"ALFN")
		FIELD_CHAR_TRIM	(Cash,"CASH")
		FIELD_DATE	    (Vafr,"VAFR")
		FIELD_DATE	    (Vato,"VATO")
	END_CEDARECINFO //(ALTDATA)


	// Copy the record structure
	for (int i=0; i< sizeof(ALTDataRecInfo)/sizeof(ALTDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&ALTDataRecInfo[i],sizeof(CEDARECINFO));
		omRecInfo.Add(prpCedaRecInfo);
	} // end for

	// initialize field names
	strcpy(pcmTableName,"ALT");
	strcat(pcmTableName,pcgTableExt);
	strcpy(pcmALTFieldList,"URNO,CDAT,USEC,LSTU,PRFL,USEU,ALC2,ALC3,ALFN,CASH,VAFR,VATO");
	pcmFieldList = pcmALTFieldList;
	omData.SetSize(0,1000);
	//omUrnoMap.InitHashTable(2001);
}

void CedaALTData::Register(void)
{
	ogDdx.Register((void *)this,BC_ALT_CHANGE,CString("ALTDATA"), CString("ALT-changed"),ProcessALTCf);
	ogDdx.Register((void *)this,BC_ALT_DELETE,CString("ALTDATA"), CString("ALT-deleted"),ProcessALTCf);
}

//--~CEDADATA----------------------------------------------------------------------------------------------

CedaALTData::~CedaALTData(void)
{
	omRecInfo.DeleteAll();
	ClearAll();
}

//--CLEAR-ALL----------------------------------------------------------------------------------------------

void CedaALTData::ClearAll(void)
{
    omUrnoMap.RemoveAll();
    omAlc2Map.RemoveAll();
    omAlc3Map.RemoveAll();
    omData.DeleteAll();
	ogDdx.UnRegister(this,NOTUSED);
}

//--READ-ALL-----------------------------------------------------------------------------------------------

bool CedaALTData::ReadAllALTs()
{
    // Select data from the database
	bool ilRc = true;
    omUrnoMap.RemoveAll();
    omAlc2Map.RemoveAll();
    omAlc3Map.RemoveAll();
    omData.DeleteAll();

	char pclWhere[256];

	strcpy(pclWhere, "WHERE ALC3<>' '");

	
	ilRc = CedaAction(ogBasicData.GetCedaCommand("RT"), pclWhere);
	if (ilRc != true)
	{
			if(!omLastErrorMessage.IsEmpty())
			{
				if(omLastErrorMessage.Find("ORA") != -1)
				{
					char pclMsg[2048]="";
					sprintf(pclMsg, "%s", omLastErrorMessage.GetBuffer(0));
					::MessageBox(NULL,pclMsg,GetString(IDS_STRING188),MB_OK);
					return false;
				}
			}
	}


	// Load data from CedaData into the dynamic array of record
	for (int ilLc = 0; ilRc == true; ilLc++)
	{
		ALTDATA *prpALT = new ALTDATA;
		if ((ilRc = GetBufferRecord(ilLc,prpALT)) == true)
		{
			prpALT->IsChanged = DATA_UNCHANGED;
			omData.Add(prpALT);//Update omData
			omUrnoMap.SetAt((void *)prpALT->Urno,prpALT);
			omAlc2Map.SetAt(prpALT->Alc2,prpALT);
			omAlc3Map.SetAt(prpALT->Alc3,prpALT);
		}
		else
		{
			delete prpALT;
		}
	}
    return true;
}

//--INSERT-------------------------------------------------------------------------------------------------

bool CedaALTData::InsertALT(ALTDATA *prpALT,BOOL bpSendDdx)
{
	ALTDATA *prlALT = new ALTDATA;
	memcpy(prlALT,prpALT,sizeof(ALTDATA));
	prlALT->IsChanged = DATA_NEW;
	SaveALT(prlALT); //Update Database
	InsertALTInternal(prlALT);
    return true;
}

//--INSERT-INTERNAL-------------------------------------------------------------------------------------------

bool CedaALTData::InsertALTInternal(ALTDATA *prpALT)
{
	//PrepareALTData(prpALT);
	ogDdx.DataChanged((void *)this, ALT_CHANGE,(void *)prpALT ); //Update Viewer
	omData.Add(prpALT);//Update omData
	omUrnoMap.SetAt((void *)prpALT->Urno,prpALT);
	omAlc2Map.SetAt(prpALT->Alc2,prpALT);
	omAlc3Map.SetAt(prpALT->Alc3,prpALT);
    return true;
}

//--DELETE-------------------------------------------------------------------------------------------------

bool CedaALTData::DeleteALT(long lpUrno)
{
	bool olRc = true;
	ALTDATA *prlALT = GetALTByUrno(lpUrno);
	if (prlALT != NULL)
	{
		prlALT->IsChanged = DATA_DELETED;
		olRc = SaveALT(prlALT); //Update Database
		DeleteALTInternal(prlALT);
	}
    return true;
}

//--DELETE-INTERNAL----------------------------------------------------------------------------------------

bool CedaALTData::DeleteALTInternal(ALTDATA *prpALT)
{
	ogDdx.DataChanged((void *)this,ALT_DELETE,(void *)prpALT); //Update Viewer
	omUrnoMap.RemoveKey((void *)prpALT->Urno);
	omAlc2Map.RemoveKey(prpALT->Alc2);
	omAlc3Map.RemoveKey(prpALT->Alc3);
	int ilALTCount = omData.GetSize();
	for (int ilLc = 0; ilLc < ilALTCount; ilLc++)
	{
		if (omData[ilLc].Urno == prpALT->Urno)
		{
			omData.DeleteAt(ilLc);//Update omData
			break;
		}
	}
    return true;
}

//--PREPARE-DATA-------------------------------------------------------------------------------------------

void CedaALTData::PrepareALTData(ALTDATA *prpALT)
{
	// TODO: add code here
}

//--UPDATE-------------------------------------------------------------------------------------------------

bool CedaALTData::UpdateALT(ALTDATA *prpALT,BOOL bpSendDdx)
{
	if (GetALTByUrno(prpALT->Urno) != NULL)
	{
		if (prpALT->IsChanged == DATA_UNCHANGED)
		{
			prpALT->IsChanged = DATA_CHANGED;
		}
		SaveALT(prpALT); //Update Database
		UpdateALTInternal(prpALT);
	}
    return true;
}

//--UPDATE-INTERNAL--------------------------------------------------------------------------------------------

bool CedaALTData::UpdateALTInternal(ALTDATA *prpALT)
{
	ALTDATA *prlALT = GetALTByUrno(prpALT->Urno);
	if (prlALT != NULL)
	{
		omAlc2Map.RemoveKey(prlALT->Alc2);
		omAlc3Map.RemoveKey(prlALT->Alc3);
		*prlALT = *prpALT; //Update omData
		omAlc2Map.SetAt(prlALT->Alc2,prlALT);
		omAlc3Map.SetAt(prlALT->Alc3,prlALT);
		ogDdx.DataChanged((void *)this,ALT_CHANGE,(void *)prlALT); //Update Viewer
	}
    return true;
}

//--GET-BY-URNO--------------------------------------------------------------------------------------------

ALTDATA *CedaALTData::GetALTByUrno(long lpUrno)
{
	ALTDATA  *prlALT;
	if (omUrnoMap.Lookup((void *)lpUrno,(void *& )prlALT) == TRUE)
	{
		return prlALT;
	}
	return NULL;
	
}

//--READSPECIALALTDATA-------------------------------------------------------------------------------------


ALTDATA * CedaALTData::GetAltByAlc2(char *pspAlc2)
{
	POSITION rlPos;
	bool blFound = false;
	ALTDATA *prlAlt;
	for ( rlPos = omUrnoMap.GetStartPosition(); rlPos != NULL; )
	{
		long llUrno;
		omUrnoMap.GetNextAssoc(rlPos, (void *&)llUrno, (void *&)prlAlt);
		if(prlAlt != NULL)
		{
			if(strcmp(prlAlt->Alc2, pspAlc2) == 0)
			{
				blFound = true;
			}
		}
	}
	if(blFound == true)
	{
		return prlAlt;
	}
	return NULL;
}

ALTDATA * CedaALTData::GetAltByAlc3(char *pspAlc3)
{
	POSITION rlPos;
	bool blFound = false;
	ALTDATA *prlAlt;
	for ( rlPos = omUrnoMap.GetStartPosition(); rlPos != NULL; )
	{
		long llUrno;
		omUrnoMap.GetNextAssoc(rlPos, (void *&)llUrno, (void *&)prlAlt);
		if(prlAlt != NULL)
		{
			if(strcmp(prlAlt->Alc3, pspAlc3) == 0)
			{
				blFound = true;
			}
		}
	}
	if(blFound == true)
	{
		return prlAlt;
	}
	return NULL;
}

bool CedaALTData::ReadSpecial(CCSPtrArray<ALTDATA> &ropAlt,char *pspWhere)
{
	bool ilRc = true;
	if (pspWhere == NULL)
	{
		ilRc = CedaAction("RT");
	}
	else
	{
		ilRc = CedaAction("RT",pspWhere);
	}
	if (ilRc != true)
	{
			if(!omLastErrorMessage.IsEmpty())
			{
				if(omLastErrorMessage.Find("ORA") != -1)
				{
					char pclMsg[2048]="";
					sprintf(pclMsg, "%s", omLastErrorMessage.GetBuffer(0));
					::MessageBox(NULL,pclMsg,GetString(IDS_STRING188),MB_OK);
					return false;
				}
			}
	}
	for (int ilLc = 0; ilRc == true; ilLc++)
	{
		ALTDATA *prpAlt = new ALTDATA;
		if ((ilRc = GetBufferRecord(ilLc,prpAlt)) == true)
		{
			ropAlt.Add(prpAlt);
		}
		else
		{
			delete prpAlt;
		}
	}
    return true;
}

//--EXIST-I------------------------------------------------------------------------------------------------
long CedaALTData::GetUrnoByAlc(char *pspAlc)
{
	ALTDATA  *prlALT;
	if(strcmp(pspAlc, "") == 0)
	{
		return 0;
	}

	if (omAlc2Map.Lookup((LPCSTR)pspAlc,(void *&)prlALT) == TRUE)
	{
		return prlALT->Urno;
	}

	if (omAlc3Map.Lookup((LPCSTR)pspAlc,(void *&)prlALT) == TRUE)
	{
		return prlALT->Urno;
	}
	return 0;
}

CString CedaALTData::ALTExists(ALTDATA *prpALT)
{
	CString olField = "";
	ALTDATA  *prlALT;

	if (omAlc2Map.Lookup((LPCSTR)prpALT->Alc2,(void *&)prlALT) == TRUE)
	{
		if(prlALT->Urno != prpALT->Urno)
		{
			olField += GetString(IDS_STRING191);
		}
	}

	if (omAlc3Map.Lookup((LPCSTR)prpALT->Alc3,(void *&)prlALT) == TRUE)
	{
		if(prlALT->Urno != prpALT->Urno)
		{
			olField += GetString(IDS_STRING192);
		}
	}

	return olField;
}

//--EXIST-II----------------------------------------------------------------------------------------------

bool CedaALTData::ExistAlc3(char* popAlc3)
{
	bool ilRc = true;
	char pclSelection[512];
	sprintf(pclSelection,"WHERE ALC3='%s'",popAlc3);
	ilRc = CedaAction(ogBasicData.GetCedaCommand("RT"),pcmTableName,"ALC3",pclSelection,"","");
	return ilRc;
}

//--SAVE---------------------------------------------------------------------------------------------------

bool CedaALTData::SaveALT(ALTDATA *prpALT)
{
	bool olRc = true;
	CString olListOfData;
	char pclSelection[124];
	char pclData[524];

	if (prpALT->IsChanged == DATA_UNCHANGED)
	{
		return true; // no change, nothing to do
	}

	switch(prpALT->IsChanged)
	{
	case DATA_NEW:
		MakeCedaData(&omRecInfo,olListOfData,prpALT);
		strcpy(pclData,olListOfData);
		olRc = CedaAction("IRT","","",pclData);
		prpALT->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_CHANGED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpALT->Urno);
		MakeCedaData(&omRecInfo,olListOfData,prpALT);
		strcpy(pclData,olListOfData);
		olRc = CedaAction("URT",pclSelection,"",pclData);
		prpALT->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_DELETED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpALT->Urno);
		olRc = CedaAction("DRT",pclSelection);
		//prpALT->IsChanged = DATA_UNCHANGED;
		break;
	}
	if(!omLastErrorMessage.IsEmpty())
	{
		if(omLastErrorMessage.Find("ORA") != -1)
		{
			char pclMsg[2048]="";
			sprintf(pclMsg, "%s", omLastErrorMessage.GetBuffer(0));
			::MessageBox(NULL,pclMsg,GetString(IDS_STRING188),MB_OK);
			return false;
		}
	}

    return true;
}

//--PROCESS-CF---------------------------------------------------------------------------------------------

void  ProcessALTCf(void * popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	switch(ipDDXType)
	{
	case BC_ALT_CHANGE :
	case BC_ALT_DELETE :
		((CedaALTData *)popInstance)->ProcessALTBc(ipDDXType,vpDataPointer,ropInstanceName);
		break;
	}
}

//--PROCESS-BC---------------------------------------------------------------------------------------------

void  CedaALTData::ProcessALTBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	struct BcStruct *prlALTData;
	long llUrno;
	prlALTData = (struct BcStruct *) vpDataPointer;
	CString olSelection = (CString)prlALTData->Selection;
	if (olSelection.Find('\'') != -1)
	{
		llUrno = GetUrnoFromSelection(prlALTData->Selection);
	}
	else
	{
		int ilFirst = olSelection.Find("=")+2;
		int ilLast  = olSelection.GetLength();
		llUrno = atol(olSelection.Mid(ilFirst,ilLast-ilFirst));
	}

	ALTDATA *prlALT;
	if(ipDDXType == BC_ALT_CHANGE)
	{
		if((prlALT = GetALTByUrno(llUrno)) != NULL)
		{
			GetRecordFromItemList(prlALT,prlALTData->Fields,prlALTData->Data);
			UpdateALTInternal(prlALT);
		}
		else
		{
			prlALT = new ALTDATA;
			GetRecordFromItemList(prlALT,prlALTData->Fields,prlALTData->Data);
			InsertALTInternal(prlALT);
		}
	}
	if(ipDDXType == BC_ALT_DELETE)
	{
		prlALT = GetALTByUrno(llUrno);
		if (prlALT != NULL)
		{
			DeleteALTInternal(prlALT);
		}
	}
}

//---------------------------------------------------------------------------------------------------------
