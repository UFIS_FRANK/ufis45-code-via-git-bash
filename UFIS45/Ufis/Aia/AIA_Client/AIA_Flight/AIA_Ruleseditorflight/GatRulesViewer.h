// GatRulesViewer.h: Schnittstelle f�r die Klasse GatRulesViewer.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_GatRulesViewer_H__7CC9F681_68A9_11D2_A4B5_0000B45A33F5__INCLUDED_)
#define AFX_GatRulesViewer_H__7CC9F681_68A9_11D2_A4B5_0000B45A33F5__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

struct GATTABLE_LINEDATA
{
	int Rtnr;
	CString Gate;
	CString T;
	CString RBIT;
	CString Id;
	CString Max;
	CString M;
	CString In;
	CString Out;
	CString	Prio;
	CString	Nat;
	CString	PAL;
	CString	PDest;
};



#include "CVIEWER.H"
#include "CCSTable.H"
#include "CedaGatData.h"


class GatRulesViewer : public CViewer  
{
public:
	GatRulesViewer();
	virtual ~GatRulesViewer();

	void Attach(CCSTable *popAttachWnd);
	/* void ChangeViewTo(const char *pcpViewName, CString opDate); */
    //@ManMemo: ChangeViewTo
    void ChangeViewTo(const char *pcpViewName);
    //@ManMemo: omLines
    CCSPtrArray<GATTABLE_LINEDATA> omLines;
	bool bmNoUpdatesNow;
private:
	
	
	int CompareGat(GATTABLE_LINEDATA *prpGat1, GATTABLE_LINEDATA *prpGat2);

    void MakeLines();

	void MakeGatLineData(GATDATA *popGat, CStringArray &ropArray);
	int MakeLine(GATDATA *prpGat1);
	int MakeLine();
	void MakeHeaderData();
	void MakeColumnData(CCSPtrArray<TABLE_COLUMN> &ropColList,GATTABLE_LINEDATA *prpLine);

	BOOL FindLine(char *pcpKeya, char *pcpKeyd, int &rilLineno);
	BOOL FindLine(long lpRtnr, int &rilLineno);
public:
    //@ManMemo: DeleteAll
	void DeleteAll();
    //@ManMemo: CreateLine
	//@ManMemo: SetStartEndTime
	int CreateLine(GATTABLE_LINEDATA *prpGat);
    //@ManMemo: DeleteLine
	void DeleteLine(int ipLineno);
	void ClearAll();


// Window refreshing routines
public:


    //@ManMemo: UpdateDisplay
	void UpdateDisplay();
private:
    CString omDate;
	// Attributes
private:
	CCSTable *pomTable;
	CCSPtrArray <TABLE_HEADER_COLUMN> omHeaderDataArray;
	int imTotalLines;

// Methods which handle changes (from Data Distributor)
public:
    //@ManMemo: omStartTime
    
    //@ManMemo: omEndTime
	
    //@ManMemo: omDay
	

};

#endif // !defined(AFX_GatRulesViewer_H__7CC9F681_68A9_11D2_A4B5_0000B45A33F5__INCLUDED_)
