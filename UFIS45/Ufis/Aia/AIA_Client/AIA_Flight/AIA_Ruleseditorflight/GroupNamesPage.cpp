// GroupNamesPage.cpp : implementation file
//

#include "stdafx.h"
#include "Rules.h"
#include "CCSGlobl.h"
#include "GroupNamesPage.h"
#include "DutyPreferences.h"
#include "PremisPage.h"
#include "CedaSysTabData.h"
#include "BasicData.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


static int CompareAct3(const ACTDATA **e1, const ACTDATA **e2);
static int CompareAPC3(const APTDATA **e1, const APTDATA **e2);
static int CompareALC2(const ALTDATA **e1, const ALTDATA **e2);
static int CompareHtyp(const HTYDATA **e1, const HTYDATA **e2);
static int ComparePnam(const PSTDATA **e1, const PSTDATA **e2);
static int CompareGnam(const GATDATA **e1, const GATDATA **e2);
static int CompareCnam(const CICDATA **e1, const CICDATA **e2);
static int CompareBnam(const BLTDATA **e1, const BLTDATA **e2);
static int CompareLknm(const GHSDATA **e1, const GHSDATA **e2);
static int CompareTnam(const NATDATA **e1 ,const NATDATA **e2);
static int ComparePrsn(const GHPDATA **e1 ,const GHPDATA **e2);
static int CompareBsdc(const BSDDATA **e1 ,const BSDDATA **e2);



extern DutyPreferences *pogDutyPreferences;

/////////////////////////////////////////////////////////////////////////////
// GroupNamesPage property page

IMPLEMENT_DYNCREATE(GroupNamesPage, CPropertyPage)

GroupNamesPage::GroupNamesPage() : CPropertyPage(GroupNamesPage::IDD)
{
	//{{AFX_DATA_INIT(GroupNamesPage)
	//}}AFX_DATA_INIT

	imMode = NEW_MODE;
	pomCurrentGroup = NULL;
	omAltList.DeleteAll();
	omActList.DeleteAll();
	omAptList.DeleteAll();
	omHtyList.DeleteAll();
	omPstList.DeleteAll();
	omGatList.DeleteAll();
	omCicList.DeleteAll();
	omBltList.DeleteAll();
	omGrpActList.DeleteAll();
	omGrpAptList.DeleteAll();
	omGrpAltList.DeleteAll();
	omGrpHtyList.DeleteAll();
	omGrpPstList.DeleteAll();
	omGrpGatList.DeleteAll();
	omGrpCicList.DeleteAll();
	omGrpBltList.DeleteAll();
	omGrpGhsList.DeleteAll();
	omGrpNatList.DeleteAll();
	omGrpGhpList.DeleteAll();
	omGrpBsdList.DeleteAll();
}

GroupNamesPage::~GroupNamesPage()
{
	ClearGroups();

	omAltList.DeleteAll();
	omActList.DeleteAll();
	omAptList.DeleteAll();
	omHtyList.DeleteAll();
	omPstList.DeleteAll();
	omGatList.DeleteAll();
	omCicList.DeleteAll();
	omBltList.DeleteAll();
	omGrpActList.DeleteAll();
	omGrpAptList.DeleteAll();
	omGrpAltList.DeleteAll();
	omGrpHtyList.DeleteAll();
	omGrpPstList.DeleteAll();
	omGrpGatList.DeleteAll();
	omGrpCicList.DeleteAll();
	omGrpBltList.DeleteAll();
	omGrpGhsList.DeleteAll();
	omGrpNatList.DeleteAll();
	omGrpGhpList.DeleteAll();
	omGrpBsdList.DeleteAll();
	omGroupData.DeleteAll();
}

void GroupNamesPage::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(GroupNamesPage)
	DDX_Control(pDX, IDC_PREMIS_LIST, m_PremisList);
	DDX_Control(pDX, IDC_CB_TABLES, m_Tables);
	DDX_Control(pDX, IDC_CB_GROUPNAME, m_GroupNames);
	DDX_Control(pDX, IDC_AVAILABLE, m_Available);
	DDX_Control(pDX, IDC_ASSIGNED, m_Assigned);
	DDX_Control(pDX, IDC_GRSN, m_GRSN);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(GroupNamesPage, CPropertyPage)
	//{{AFX_MSG_MAP(GroupNamesPage)
	ON_BN_CLICKED(IDC_ADD, OnAdd)
	ON_BN_CLICKED(IDC_DELETE, OnDelete)
	ON_CBN_SELCHANGE(IDC_CB_TABLES, OnSelchangeCbTables)
	ON_CBN_SELCHANGE(IDC_CB_GROUPNAME, OnSelchangeCbGroupname)
	ON_CBN_EDITCHANGE(IDC_CB_GROUPNAME, OnEditchangeCbGroupname)
	ON_MESSAGE(CLK_BUTTON_DELETE,		OnDeleteButton)
	ON_LBN_DBLCLK(IDC_PREMIS_LIST, OnDblclkPremisList)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// GroupNamesPage message handlers

BOOL GroupNamesPage::OnInitDialog() 
{
	CPropertyPage::OnInitDialog();
	m_Available.SetFont(&ogCourier_Regular_8);
	m_Assigned.SetFont(&ogCourier_Regular_8);
	m_PremisList.SetFont(&ogCourier_Regular_8);
	m_GRSN.SetTypeToString("X(3)", 3, 0);
	m_GroupNames.LimitText(12);

	m_Tables.ResetContent();
	m_Tables.AddString(LoadStg(IDS_STRING118));//Flugzeugtypen
	m_Tables.AddString(LoadStg(IDS_STRING119));//Flughäfen
	m_Tables.AddString(LoadStg(IDS_STRING120));//Fluggesellschaften
	m_Tables.AddString(LoadStg(IDS_STRING121));//Abfertigungsarten
	m_Tables.AddString(LoadStg(IDS_STRING122));//Positionen
	m_Tables.AddString(LoadStg(IDS_STRING123));//Gates
	m_Tables.AddString(LoadStg(IDS_STRING124));//Verkehrsarten
	m_Tables.AddString(LoadStg(IDS_STRING125));//Check-In Schalter
	m_Tables.AddString(LoadStg(IDS_STRING126));//Gepäckbänder
	m_Tables.AddString(LoadStg(IDS_STRING127));//Leistungskatalog
	m_Tables.AddString(LoadStg(IDS_STRING128));//Regeln
	m_Tables.AddString(LoadStg(IDS_STRING129));//Basisschichten

	return TRUE;  
}

void GroupNamesPage::OnAdd() 
{
	if(omCurrentTable.IsEmpty() == TRUE)
	{
		MessageBox( GetString(IDS_STRING160), GetString(IDS_STRING117), (MB_OK| MB_ICONEXCLAMATION));
		return;
	}
	CString olGrsn;
	m_GRSN.GetWindowText(olGrsn);
/*	if(olGrsn.GetLength() == 0)
	{
		MessageBox(GetString(IDS_STRING312), GetString(IDS_STRING117), (MB_OK| MB_ICONEXCLAMATION));
		return;
	}
*/
	CString olGrpName;
	m_GroupNames.GetWindowText(olGrpName);
	int ilCount = omGroupData.GetSize();
	for(int i = 0; i < ilCount; i++)
	{
		if(olGrpName == omGroupData[i].Group.Grpn)
		{
			pomCurrentGroup = &omGroupData[i];
			ShowGroupMembers();
			i = ilCount;
		}
		else
		{
			pomCurrentGroup = NULL;
		}
	}

	if(pomCurrentGroup == NULL && !olGrpName.IsEmpty())
	{
		imMode = NEW_MODE;
		pomCurrentGroup = new GROUPS;
		pomCurrentGroup->Group.Urno = ogBasicData.GetNextUrno();
		pomCurrentGroup->Group.IsChanged = DATA_NEW;
		strcpy(pomCurrentGroup->Group.Appl, "POPS");
		pomCurrentGroup->Group.Cdat = CTime::GetCurrentTime();
		pomCurrentGroup->Group.Lstu = CTime::GetCurrentTime();
		strcpy(pomCurrentGroup->Group.Usec, ogBasicData.omUserID);
		strcpy(pomCurrentGroup->Group.Useu, ogBasicData.omUserID);
		strcpy(pomCurrentGroup->Group.Grpn, olGrpName);
		strcpy(pomCurrentGroup->Group.Grsn, olGrsn);
		CString olCTab = omCurrentTable + CString(pcgTableExt);
		strcpy(pomCurrentGroup->Group.Tabn, olCTab);
		m_GroupNames.AddString(olGrpName);

	}
	else if(pomCurrentGroup != NULL && !olGrpName.IsEmpty())
	{
		imMode = UPDATE_MODE;
		pomCurrentGroup->Group.IsChanged = DATA_CHANGED;
		pomCurrentGroup->Group.Lstu = CTime::GetCurrentTime();
		strcpy(pomCurrentGroup->Group.Grsn, olGrsn);
		strcpy(pomCurrentGroup->Group.Useu, ogBasicData.omUserID);
	}
	else if(olGrpName.IsEmpty())
	{
		imMode = NEW_MODE;
		MessageBox(GetString(IDS_STRING1439), GetString(IDS_STRING117), (MB_OK|MB_ICONEXCLAMATION));
		return;
	}

	//Get All Selected Items of Listbox
	CString olStr;
	ilCount=0;
	int *ipSels;
	//CListBox
	ilCount = m_Available.GetSelCount();
	if(ilCount == 0)
	{
		return;
	}
	ipSels = (int*)malloc(ilCount*sizeof(int));
	ogGrnData.Save(&pomCurrentGroup->Group);
//	if(pomCurrentGroup->Group.IsChanged == DATA_NEW)
//	{
//		GRNDATA *prlGrn = new GRNDATA;
//		*prlGrn = pomCurrentGroup->Group;
//		ogGrnData.InsertInternal(prlGrn);
//	}
//	else if(pomCurrentGroup->Group.IsChanged == DATA_NEW)
//	{
		GRNDATA *prlGrn = ogGrnData.GetGrnByUrno(pomCurrentGroup->Group.Urno);
		if(prlGrn != NULL)
		{
			ogGrnData.UpdateInternal(prlGrn);
		}
		else
		{
			GRNDATA *prlGrn = new GRNDATA;
			*prlGrn = pomCurrentGroup->Group;
			ogGrnData.InsertInternal(prlGrn);
		}
//	}
	if(ilCount != LB_ERR)
	{
		m_Available.GetSelItems(ilCount, ipSels);
		for (int ili = 0; ili < ilCount; ili++)
		{
			//m_Available.GetText(ipSels[ili], olStr);
			GRMDATA rlGrm;
			rlGrm.Urno = ogBasicData.GetNextUrno();
			rlGrm.Cdat = CTime::GetCurrentTime();
			rlGrm.Lstu = CTime::GetCurrentTime();
			strcpy(rlGrm.Usec, ogBasicData.omUserID);
			strcpy(rlGrm.Useu, ogBasicData.omUserID);
			rlGrm.Gurn = pomCurrentGroup->Group.Urno;
			rlGrm.IsChanged = DATA_NEW;
			if(omCurrentTable == "ACT")
			{
				ACTDATA *prlData = (ACTDATA*)m_Available.GetItemDataPtr(ipSels[ili]);
				if(prlData != NULL)
				{
					sprintf(rlGrm.Valu, "%ld", prlData->Urno);
				}
			}
			else if(omCurrentTable == "APT")
			{
				APTDATA *prlData = (APTDATA*)m_Available.GetItemDataPtr(ipSels[ili]);
				if(prlData != NULL)
				{
					sprintf(rlGrm.Valu, "%ld", prlData->Urno);
				}
			}
			else if(omCurrentTable == "ALT")
			{
				ALTDATA *prlData = (ALTDATA*)m_Available.GetItemDataPtr(ipSels[ili]);
				if(prlData != NULL)
				{
					sprintf(rlGrm.Valu, "%ld", prlData->Urno);
				}
			}
			else if(omCurrentTable == "HTY")
			{
				HTYDATA *prlData = (HTYDATA*)m_Available.GetItemDataPtr(ipSels[ili]);
				if(prlData != NULL)
				{
					sprintf(rlGrm.Valu, "%ld", prlData->Urno);
				}
			}
			else if(omCurrentTable == "PST")
			{
				PSTDATA *prlData = (PSTDATA*)m_Available.GetItemDataPtr(ipSels[ili]);
				if(prlData != NULL)
				{
					sprintf(rlGrm.Valu, "%ld", prlData->Urno);
				}
			}
			else if(omCurrentTable == "GAT")
			{
				GATDATA *prlData = (GATDATA*)m_Available.GetItemDataPtr(ipSels[ili]);
				if(prlData != NULL)
				{
					sprintf(rlGrm.Valu, "%ld", prlData->Urno);
				}
			}
			else if(omCurrentTable == "CIC")
			{
				CICDATA *prlData = (CICDATA*)m_Available.GetItemDataPtr(ipSels[ili]);
				if(prlData != NULL)
				{
					sprintf(rlGrm.Valu, "%ld", prlData->Urno);
				}
			}
			else if(omCurrentTable == "BLT")
			{
				BLTDATA *prlData = (BLTDATA*)m_Available.GetItemDataPtr(ipSels[ili]);
				if(prlData != NULL)
				{
					sprintf(rlGrm.Valu, "%ld", prlData->Urno);
				}
			}
			else if(omCurrentTable == "GHS")
			{
				GHSDATA *prlData = (GHSDATA*)m_Available.GetItemDataPtr(ipSels[ili]);
				if(prlData != NULL)
				{
					sprintf(rlGrm.Valu, "%ld", prlData->Urno);
				}
			}
			else if(omCurrentTable == "NAT")
			{
				NATDATA *prlData = (NATDATA*)m_Available.GetItemDataPtr(ipSels[ili]);
				if(prlData != NULL)
				{
					sprintf(rlGrm.Valu, "%ld", prlData->Urno);
				}
			}
			else if(omCurrentTable == "GHP")
			{
				GHPDATA *prlData = (GHPDATA*)m_Available.GetItemDataPtr(ipSels[ili]);
				if(prlData != NULL)
				{
					sprintf(rlGrm.Valu, "%ld", prlData->Urno);
				}
			}
			else if(omCurrentTable == "BSD")
			{
				BSDDATA *prlData = (BSDDATA*)m_Available.GetItemDataPtr(ipSels[ili]);
				if(prlData != NULL)
				{
					sprintf(rlGrm.Valu, "%ld", prlData->Urno);
				}
			}

			ogGrmData.Save(&rlGrm);
			GRMDATA *prlGrm = new GRMDATA;
			*prlGrm = rlGrm;
			ogGrmData.InsertInternal(prlGrm);
			pomCurrentGroup->GroupMembers.NewAt(pomCurrentGroup->GroupMembers.GetSize(),rlGrm);
		}
		if(	imMode == NEW_MODE)
		{
			omGroupData.Add(pomCurrentGroup);
		}
	}
	ShowGroupMembers();
	MakeDiffList();
	free(ipSels);


}

void GroupNamesPage::OnDelete() 
{
	CString olStr;
	int ilCount=0;
	int *ipSels;
	bool blDeleteAll = false;
	CUIntArray olUrnoList;
	//CListBox
	ilCount = m_Assigned.GetSelCount();
	if(ilCount == 0)
	{
		return;
	}

	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));		

	ipSels = (int*)malloc(ilCount*sizeof(int));
	m_Assigned.GetSelItems(ilCount, ipSels);
	if(ilCount == m_Assigned.GetCount())
	{
		blDeleteAll = true;
	}
	for (int ili = 0; ili < ilCount; ili++)
	{
		if(omCurrentTable == "ALT")
		{
			ALTDATA *prlData = (ALTDATA*)m_Assigned.GetItemDataPtr(ipSels[ili]);
			if(prlData != NULL)
			{
				if(pomCurrentGroup != NULL)
				{
					int ilC2 = pomCurrentGroup->GroupMembers.GetSize();
					for(int j = 0; j < ilC2; j++)
					{
						if(atol(pomCurrentGroup->GroupMembers[j].Valu) ==prlData->Urno)
						{
							olUrnoList.Add(pomCurrentGroup->GroupMembers[j].Urno);
							j = ilC2;
						}
					}
				}
			}
		}
		else if(omCurrentTable == "ACT")
		{
			ACTDATA *prlData = (ACTDATA*)m_Assigned.GetItemDataPtr(ipSels[ili]);
			if(prlData != NULL)
			{
				if(pomCurrentGroup != NULL)
				{
					int ilC2 = pomCurrentGroup->GroupMembers.GetSize();
					for(int j = 0; j < ilC2; j++)
					{
						if(atol(pomCurrentGroup->GroupMembers[j].Valu) ==prlData->Urno)
						{
							olUrnoList.Add(pomCurrentGroup->GroupMembers[j].Urno);
							j = ilC2;
						}
					}
				}
			}
		}
		else if(omCurrentTable == "APT")
		{
			APTDATA *prlData = (APTDATA*)m_Assigned.GetItemDataPtr(ipSels[ili]);
			if(prlData != NULL)
			{
				if(pomCurrentGroup != NULL)
				{
					int ilC2 = pomCurrentGroup->GroupMembers.GetSize();
					for(int j = 0; j < ilC2; j++)
					{
						if(atol(pomCurrentGroup->GroupMembers[j].Valu) ==prlData->Urno)
						{
							olUrnoList.Add(pomCurrentGroup->GroupMembers[j].Urno);
							j = ilC2;
						}
					}
				}
			}
		}
		else if(omCurrentTable == "HTY")
		{
			HTYDATA *prlData = (HTYDATA*)m_Assigned.GetItemDataPtr(ipSels[ili]);
			if(prlData != NULL)
			{
				if(pomCurrentGroup != NULL)
				{
					int ilC2 = pomCurrentGroup->GroupMembers.GetSize();
					for(int j = 0; j < ilC2; j++)
					{
						if(atol(pomCurrentGroup->GroupMembers[j].Valu) ==prlData->Urno)
						{
							olUrnoList.Add(pomCurrentGroup->GroupMembers[j].Urno);
							j = ilC2;
						}
					}
				}
			}
		}
		else if(omCurrentTable == "PST")
		{
			PSTDATA *prlData = (PSTDATA*)m_Assigned.GetItemDataPtr(ipSels[ili]);
			if(prlData != NULL)
			{
				if(pomCurrentGroup != NULL)
				{
					int ilC2 = pomCurrentGroup->GroupMembers.GetSize();
					for(int j = 0; j < ilC2; j++)
					{
						if(atol(pomCurrentGroup->GroupMembers[j].Valu) ==prlData->Urno)
						{
							olUrnoList.Add(pomCurrentGroup->GroupMembers[j].Urno);
							j = ilC2;
						}
					}
				}
			}
		}
		else if(omCurrentTable == "GAT")
		{
			GATDATA *prlData = (GATDATA*)m_Assigned.GetItemDataPtr(ipSels[ili]);
			if(prlData != NULL)
			{
				if(pomCurrentGroup != NULL)
				{
					int ilC2 = pomCurrentGroup->GroupMembers.GetSize();
					for(int j = 0; j < ilC2; j++)
					{
						if(atol(pomCurrentGroup->GroupMembers[j].Valu) ==prlData->Urno)
						{
							olUrnoList.Add(pomCurrentGroup->GroupMembers[j].Urno);
							j = ilC2;
						}
					}
				}
			}
		}
		else if(omCurrentTable == "CIC")
		{
			CICDATA *prlData = (CICDATA*)m_Assigned.GetItemDataPtr(ipSels[ili]);
			if(prlData != NULL)
			{
				if(pomCurrentGroup != NULL)
				{
					int ilC2 = pomCurrentGroup->GroupMembers.GetSize();
					for(int j = 0; j < ilC2; j++)
					{
						if(atol(pomCurrentGroup->GroupMembers[j].Valu) ==prlData->Urno)
						{
							olUrnoList.Add(pomCurrentGroup->GroupMembers[j].Urno);
							j = ilC2;
						}
					}
				}
			}
		}
		else if(omCurrentTable == "BLT")
		{
			BLTDATA *prlData = (BLTDATA*)m_Assigned.GetItemDataPtr(ipSels[ili]);
			if(prlData != NULL)
			{
				if(pomCurrentGroup != NULL)
				{
					int ilC2 = pomCurrentGroup->GroupMembers.GetSize();
					for(int j = 0; j < ilC2; j++)
					{
						if(atol(pomCurrentGroup->GroupMembers[j].Valu) ==prlData->Urno)
						{
							olUrnoList.Add(pomCurrentGroup->GroupMembers[j].Urno);
							j = ilC2;
						}
					}
				}
			}
		}
		else if(omCurrentTable == "GHS")
		{
			GHSDATA *prlData = (GHSDATA*)m_Assigned.GetItemDataPtr(ipSels[ili]);
			if(prlData != NULL)
			{
				if(pomCurrentGroup != NULL)
				{
					int ilC2 = pomCurrentGroup->GroupMembers.GetSize();
					for(int j = 0; j < ilC2; j++)
					{
						if(atol(pomCurrentGroup->GroupMembers[j].Valu) ==prlData->Urno)
						{
							olUrnoList.Add(pomCurrentGroup->GroupMembers[j].Urno);
							j = ilC2;
						}
					}
				}
			}
		}
		else if(omCurrentTable == "NAT")
		{
			NATDATA *prlData = (NATDATA*)m_Assigned.GetItemDataPtr(ipSels[ili]);
			if(prlData != NULL)
			{
				if(pomCurrentGroup != NULL)
				{
					int ilC2 = pomCurrentGroup->GroupMembers.GetSize();
					for(int j = 0; j < ilC2; j++)
					{
						if(atol(pomCurrentGroup->GroupMembers[j].Valu) ==prlData->Urno)
						{
							olUrnoList.Add(pomCurrentGroup->GroupMembers[j].Urno);
							j = ilC2;
						}
					}
				}
			}
		}
		else if(omCurrentTable == "GHP")
		{
			GHPDATA *prlData = (GHPDATA*)m_Assigned.GetItemDataPtr(ipSels[ili]);
			if(prlData != NULL)
			{
				if(pomCurrentGroup != NULL)
				{
					int ilC2 = pomCurrentGroup->GroupMembers.GetSize();
					for(int j = 0; j < ilC2; j++)
					{
						if(atol(pomCurrentGroup->GroupMembers[j].Valu) ==prlData->Urno)
						{
							olUrnoList.Add(pomCurrentGroup->GroupMembers[j].Urno);
							j = ilC2;
						}
					}
				}
			}
		}
		else if(omCurrentTable == "BSD")
		{
			BSDDATA *prlData = (BSDDATA*)m_Assigned.GetItemDataPtr(ipSels[ili]);
			if(prlData != NULL)
			{
				if(pomCurrentGroup != NULL)
				{
					int ilC2 = pomCurrentGroup->GroupMembers.GetSize();
					for(int j = 0; j < ilC2; j++)
					{
						if(atol(pomCurrentGroup->GroupMembers[j].Valu) ==prlData->Urno)
						{
							olUrnoList.Add(pomCurrentGroup->GroupMembers[j].Urno);
							j = ilC2;
						}
					}
				}
			}
		}
	}
	ilCount = olUrnoList.GetSize();
	long llGurn = -1;
	for(int i = 0; i < ilCount; i++)
	{
		long llUrno = (long)olUrnoList[i];
		int ilC2 = pomCurrentGroup->GroupMembers.GetSize();
		//for(int j = ilC2-1; j >= 0; j--)
		for(int j = 0; j < ilC2; j++)
		{
			GRMDATA *prlGrm = &pomCurrentGroup->GroupMembers[j];
			llGurn = prlGrm->Gurn;

			if(pomCurrentGroup->GroupMembers[j].Urno == llUrno)
			{
				GRMDATA *prlGrm = &pomCurrentGroup->GroupMembers[j];
				if(prlGrm != NULL)
				{
					prlGrm->IsChanged = DATA_DELETED;
					ogGrmData.Save(prlGrm);
					GRMDATA *prlGrm2 = ogGrmData.GetGrmByUrno(prlGrm->Urno);
					if(prlGrm2 != NULL)
					{
						ogGrmData.DeleteInternal(prlGrm2);
					}
					pomCurrentGroup->GroupMembers.DeleteAt(j);
					j = ilC2;
				}
			}
		}
	}
	if(blDeleteAll == true)
	{
		CCSPtrArray<GRNDATA> olGrnList;
		char pclWhere[100]="";
		sprintf(pclWhere, " WHERE URNO=%ld", llGurn);
		GRNDATA *prlGrnDelete = ogGrnData.GetGrnByUrno(llGurn);
		if(prlGrnDelete != NULL/*ogGrnData.ReadSpecial(olGrnList, pclWhere) == true*/)
		{
			m_GRSN.SetWindowText(prlGrnDelete->Grsn);
			prlGrnDelete->IsChanged = DATA_DELETED;
			ogGrnData.Save(prlGrnDelete);
			ogGrnData.DeleteInternal(prlGrnDelete);
			m_GRSN.SetWindowText("");
			CString olText;
			m_GroupNames.GetWindowText(olText);
			int ilIdx = m_GroupNames.FindString( -1, olText);
			if(ilIdx != -1)
			{
				m_GroupNames.DeleteString(ilIdx);
				m_GroupNames.SetWindowText("");
			}
		}
	}
	free(ipSels);
	ReloadFromMemory();
	ShowGroupMembers();
	MakeDiffList();
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));		
	
}

void GroupNamesPage::OnOK()
{
}


void GroupNamesPage::MakeDiffList()
{
	
	if(pomCurrentGroup != NULL)
	{
		int ilCount = pomCurrentGroup->GroupMembers.GetSize();
		for(int i = 0; i < ilCount; i++)
		{
			long llUrno = atol(pomCurrentGroup->GroupMembers[i].Valu);
			int ilLBCount = m_Available.GetCount();
			for(int j = ilLBCount-1; j >= 0; j--)
			{
				if(omCurrentTable == "ACT")
				{
					ACTDATA *prlObj = (ACTDATA *)m_Available.GetItemDataPtr(j);
					if(prlObj != NULL)
					{
						if(prlObj->Urno == llUrno)
						{
							m_Available.DeleteString(j);
						}
					}
				}
				if(omCurrentTable == "ALT")
				{
					ALTDATA *prlObj = (ALTDATA *)m_Available.GetItemDataPtr(j);
					if(prlObj != NULL)
					{
						if(prlObj->Urno == llUrno)
						{
							m_Available.DeleteString(j);
						}
					}
				}
				else if(omCurrentTable == "APT")
				{
					APTDATA *prlObj = (APTDATA *)m_Available.GetItemDataPtr(j);
					if(prlObj != NULL)
					{
						if(prlObj->Urno == llUrno)
						{
							m_Available.DeleteString(j);
						}
					}
				}
				else if(omCurrentTable == "HTY")
				{
					HTYDATA *prlObj = (HTYDATA *)m_Available.GetItemDataPtr(j);
					if(prlObj != NULL)
					{
						if(prlObj->Urno == llUrno)
						{
							m_Available.DeleteString(j);
						}
					}
				}
				else if(omCurrentTable == "PST")
				{
					PSTDATA *prlObj = (PSTDATA *)m_Available.GetItemDataPtr(j);
					if(prlObj != NULL)
					{
						if(prlObj->Urno == llUrno)
						{
							m_Available.DeleteString(j);
						}
					}
				}
				else if(omCurrentTable == "GAT")
				{
					GATDATA *prlObj = (GATDATA *)m_Available.GetItemDataPtr(j);
					if(prlObj != NULL)
					{
						if(prlObj->Urno == llUrno)
						{
							m_Available.DeleteString(j);
						}
					}
				}
				else if(omCurrentTable == "CIC")
				{
					CICDATA *prlObj = (CICDATA *)m_Available.GetItemDataPtr(j);
					if(prlObj != NULL)
					{
						if(prlObj->Urno == llUrno)
						{
							m_Available.DeleteString(j);
						}
					}
				}
				else if(omCurrentTable == "BLT")
				{
					BLTDATA *prlObj = (BLTDATA *)m_Available.GetItemDataPtr(j);
					if(prlObj != NULL)
					{
						if(prlObj->Urno == llUrno)
						{
							m_Available.DeleteString(j);
						}
					}
				}
				else if(omCurrentTable == "GHS")
				{
					GHSDATA *prlObj = (GHSDATA *)m_Available.GetItemDataPtr(j);
					if(prlObj != NULL)
					{
						if(prlObj->Urno == llUrno)
						{
							m_Available.DeleteString(j);
						}
					}
				}
				else if(omCurrentTable == "NAT")
				{
					NATDATA *prlObj = (NATDATA *)m_Available.GetItemDataPtr(j);
					if(prlObj != NULL)
					{
						if(prlObj->Urno == llUrno)
						{
							m_Available.DeleteString(j);
						}
					}
				}
				else if(omCurrentTable == "GHP")
				{
					GHPDATA *prlObj = (GHPDATA *)m_Available.GetItemDataPtr(j);
					if(prlObj != NULL)
					{
						if(prlObj->Urno == llUrno)
						{
							m_Available.DeleteString(j);
						}
					}
				}
				else if(omCurrentTable == "BSD")
				{
					BSDDATA *prlObj = (BSDDATA *)m_Available.GetItemDataPtr(j);
					if(prlObj != NULL)
					{
						if(prlObj->Urno == llUrno)
						{
							m_Available.DeleteString(j);
						}
					}
				}
			}
		}
	}
}

void GroupNamesPage::OnSelchangeCbTables() 
{

	CString olSelStr;
	int ilIdx = -1;
	m_PremisList.ResetContent();
	m_Assigned.ResetContent();
	m_GroupNames.ResetContent();
	m_Available.ResetContent();
	pomCurrentGroup = NULL;
	ilIdx = m_Tables.GetCurSel();
	if(ilIdx != LB_ERR)
	{
		m_Tables.GetLBText( ilIdx, olSelStr );
		m_Available.ResetContent();
	}
	CCSPtrArray<GRNDATA> olGrnList;
	char pclWhere[100]="";
	sprintf(pclWhere, " WHERE GRPN='%s'", olSelStr);
	ogGrnData.GetGrnByGrpn(olGrnList, olSelStr.GetBuffer(0));
	if(olGrnList.GetSize() > 0)
	{
		m_GRSN.SetWindowText(olGrnList[0].Grsn);
	}
	else
	{
		m_GRSN.SetWindowText("");
	}
	olGrnList.DeleteAll();
	if(olSelStr == LoadStg(IDS_STRING118)) 
	{
		if(omCurrentTable != "ACT")
		{
			omCurrentTable = "ACT";
			LoadActFields();
			LoadGroupsForTable();
		}
		else
		{
			ReloadFromMemory();
			LoadGroupsForTable();
		}
	}
	else if(olSelStr == LoadStg(IDS_STRING119))
	{
		if(omCurrentTable != "APT")
		{
			omCurrentTable = "APT";
			LoadAptFields();
			LoadGroupsForTable();
		}
		else
		{
			ReloadFromMemory();
			LoadGroupsForTable();
		}
	}
	else if(olSelStr == LoadStg(IDS_STRING120))
	{
		if(omCurrentTable != "ALT")
		{
			omCurrentTable = "ALT";
			LoadAltFields();
			LoadGroupsForTable();
		}
		else
		{
			ReloadFromMemory();
			LoadGroupsForTable();
		}
	}
	else if(olSelStr == LoadStg(IDS_STRING121))
	{
		if(omCurrentTable != "HTY")
		{
			omCurrentTable = "HTY";
			LoadHtyFields();
			LoadGroupsForTable();
		}
		else
		{
			ReloadFromMemory();
			LoadGroupsForTable();
		}
	}
	else if(olSelStr == LoadStg(IDS_STRING122))
	{
		if(omCurrentTable != "PST")
		{
			omCurrentTable = "PST";
			LoadPstFields();
			LoadGroupsForTable();
		}
		else
		{
			ReloadFromMemory();
			LoadGroupsForTable();
		}
	}
	else if(olSelStr == LoadStg(IDS_STRING123))
	{
		if(omCurrentTable != "GAT")
		{
			omCurrentTable = "GAT";
			LoadGatFields();
			LoadGroupsForTable();
		}
		else
		{
			ReloadFromMemory();
			LoadGroupsForTable();
		}
	}
	else if(olSelStr == LoadStg(IDS_STRING125))
	{
		if(omCurrentTable != "CIC")
		{
			omCurrentTable = "CIC";
			LoadCicFields();
			LoadGroupsForTable();
		}
		else
		{
			ReloadFromMemory();
			LoadGroupsForTable();
		}
	}
	else if(olSelStr == LoadStg(IDS_STRING126))
	{
		if(omCurrentTable != "BLT")
		{
			omCurrentTable = "BLT";
			LoadBltFields();
			LoadGroupsForTable();
		}
		else
		{
			ReloadFromMemory();
			LoadGroupsForTable();
		}
	}
	else if(olSelStr == LoadStg(IDS_STRING127))
	{
		if(omCurrentTable != "GHS")
		{
			omCurrentTable = "GHS";
			LoadGhsFields();
			LoadGroupsForTable();
		}
		else
		{
			ReloadFromMemory();
			LoadGroupsForTable();
		}
	}
	else if(olSelStr == LoadStg(IDS_STRING124))
	{
		if(omCurrentTable != "NAT")
		{
			omCurrentTable = "NAT";
			LoadNatFields();
			LoadGroupsForTable();
		}
		else
		{
			ReloadFromMemory();
			LoadGroupsForTable();
		}
	}
	else if(olSelStr == LoadStg(IDS_STRING128))
	{
		if(omCurrentTable != "GHP")
		{
			omCurrentTable = "GHP";
			LoadGhpFields();
			LoadGroupsForTable();
		}
		else
		{
			ReloadFromMemory();
			LoadGroupsForTable();
		}
	}
	else if(olSelStr == LoadStg(IDS_STRING129))
	{
		if(omCurrentTable != "BSD")
		{
			omCurrentTable = "BSD";
			LoadBsdFields();
			LoadGroupsForTable();
		}
		else
		{
			ReloadFromMemory();
			LoadGroupsForTable();
		}
	}
}

void GroupNamesPage::LoadActFields()
{
	omAltList.DeleteAll();
	omAptList.DeleteAll();
	omHtyList.DeleteAll();
	omPstList.DeleteAll();
	omGatList.DeleteAll();
	omCicList.DeleteAll();
	omBltList.DeleteAll();
	omActList.DeleteAll();
	omGhsList.DeleteAll();
	omNatList.DeleteAll();
	omGhpList.DeleteAll();
	omBsdList.DeleteAll();

	CedaACTData olActData;
	olActData.ReadSpecial(omActList, "");
	omActList.Sort(CompareAct3);
	ReloadFromMemory();
}

void GroupNamesPage::LoadAptFields()
{
	omAltList.DeleteAll();
	omActList.DeleteAll();
	omHtyList.DeleteAll();
	omPstList.DeleteAll();
	omGatList.DeleteAll();
	omCicList.DeleteAll();
	omBltList.DeleteAll();
	omGhsList.DeleteAll();
	omNatList.DeleteAll();
	omGhpList.DeleteAll();
	omBsdList.DeleteAll();

	CedaAPTData olAptData;
	olAptData.ReadSpecial(omAptList, "");
	omAptList.Sort(CompareAPC3);
	ReloadFromMemory();
}

void GroupNamesPage::LoadAltFields()
{
	omActList.DeleteAll();
	omAptList.DeleteAll();
	omHtyList.DeleteAll();
	omPstList.DeleteAll();
	omGatList.DeleteAll();
	omCicList.DeleteAll();
	omBltList.DeleteAll();
	omGhsList.DeleteAll();
	omNatList.DeleteAll();
	omGhpList.DeleteAll();
	omBsdList.DeleteAll();

	CedaALTData olAltData;
	olAltData.ReadSpecial(omAltList, "WHERE ALC3<>' '");
	omAltList.Sort(CompareALC2);
	ReloadFromMemory();
}

void GroupNamesPage::LoadHtyFields()
{
	omAltList.DeleteAll();
	omActList.DeleteAll();
	omAptList.DeleteAll();
	omPstList.DeleteAll();
	omGatList.DeleteAll();
	omCicList.DeleteAll();
	omBltList.DeleteAll();
	omGhsList.DeleteAll();
	omNatList.DeleteAll();
	omGhpList.DeleteAll();
	omBsdList.DeleteAll();

	CedaHTYData olHtyData;
	olHtyData.ReadSpecial(omHtyList, "");
	omHtyList.Sort(CompareHtyp);
	ReloadFromMemory();
}

void GroupNamesPage::LoadPstFields()
{
	omAltList.DeleteAll();
	omActList.DeleteAll();
	omAptList.DeleteAll();
	omHtyList.DeleteAll();
	omGatList.DeleteAll();
	omCicList.DeleteAll();
	omBltList.DeleteAll();
	omGhsList.DeleteAll();
	omNatList.DeleteAll();
	omGhpList.DeleteAll();
	omBsdList.DeleteAll();

	CedaPSTData olPstData;
	olPstData.ReadSpecial(omPstList, "");
	omPstList.Sort(ComparePnam);
	ReloadFromMemory();
}

void GroupNamesPage::LoadGatFields()
{
	omAltList.DeleteAll();
	omActList.DeleteAll();
	omAptList.DeleteAll();
	omHtyList.DeleteAll();
	omPstList.DeleteAll();
	omCicList.DeleteAll();
	omBltList.DeleteAll();
	omGhsList.DeleteAll();
	omNatList.DeleteAll();
	omGhpList.DeleteAll();
	omBsdList.DeleteAll();

	CedaGATData olGatData;
	olGatData.ReadSpecial(omGatList, "");
	omGatList.Sort(CompareGnam);
	ReloadFromMemory();
}

void GroupNamesPage::LoadCicFields()
{
	omAltList.DeleteAll();
	omActList.DeleteAll();
	omAptList.DeleteAll();
	omHtyList.DeleteAll();
	omPstList.DeleteAll();
	omGatList.DeleteAll();
	omBltList.DeleteAll();
	omGhsList.DeleteAll();
	omNatList.DeleteAll();
	omGhpList.DeleteAll();
	omBsdList.DeleteAll();

	CedaCICData olCicData;
	olCicData.ReadSpecial(omCicList, "");
	omCicList.Sort(CompareCnam);
	ReloadFromMemory();
}

void GroupNamesPage::LoadBltFields()
{
	omAltList.DeleteAll();
	omActList.DeleteAll();
	omAptList.DeleteAll();
	omHtyList.DeleteAll();
	omPstList.DeleteAll();
	omGatList.DeleteAll();
	omCicList.DeleteAll();
	omGhsList.DeleteAll();
	omNatList.DeleteAll();
	omGhpList.DeleteAll();
	omBsdList.DeleteAll();

	CedaBLTData olBltData;
	olBltData.ReadSpecial(omBltList, "");
	omBltList.Sort(CompareBnam);
	ReloadFromMemory();
}

void GroupNamesPage::LoadGhsFields()
{
	omAltList.DeleteAll();
	omActList.DeleteAll();
	omAptList.DeleteAll();
	omHtyList.DeleteAll();
	omPstList.DeleteAll();
	omGatList.DeleteAll();
	omCicList.DeleteAll();
	omBltList.DeleteAll();
	omNatList.DeleteAll();
	omGhpList.DeleteAll();
	omBsdList.DeleteAll();

	CedaGhsData olGhsData;
	for(int i = 0; i < ogGhsData.omData.GetSize(); i++)
	{
		GHSDATA *prlGhs = &ogGhsData.omData[i];
		omGhsList.NewAt(omGhsList.GetSize(), ogGhsData.omData[i]);
	}
	omGhsList.Sort(CompareLknm);
	ReloadFromMemory();
}

void GroupNamesPage::LoadNatFields()
{
	omAltList.DeleteAll();
	omActList.DeleteAll();
	omAptList.DeleteAll();
	omHtyList.DeleteAll();
	omPstList.DeleteAll();
	omGatList.DeleteAll();
	omCicList.DeleteAll();
	omBltList.DeleteAll();
	omGhsList.DeleteAll();
	omGhpList.DeleteAll();
	omBsdList.DeleteAll();

	CedaNATData olNatData;
	for(int i = 0; i < ogNatData.omData.GetSize(); i++)
	{
		NATDATA *prlNat = &ogNatData.omData[i];
		omNatList.NewAt(omNatList.GetSize(), ogNatData.omData[i]);
	}
	omNatList.Sort(CompareTnam);
	ReloadFromMemory();
}

void GroupNamesPage::LoadGhpFields()
{
	omAltList.DeleteAll();
	omActList.DeleteAll();
	omAptList.DeleteAll();
	omHtyList.DeleteAll();
	omPstList.DeleteAll();
	omGatList.DeleteAll();
	omCicList.DeleteAll();
	omBltList.DeleteAll();
	omGhsList.DeleteAll();
	omNatList.DeleteAll();
	omBsdList.DeleteAll();

	CedaGhpData olGhpData;
	for(int i = 0; i < ogGhpData.omData.GetSize(); i++)
	{
		GHPDATA *prlGhp = &ogGhpData.omData[i];
		omGhpList.NewAt(omGhpList.GetSize(), ogGhpData.omData[i]);
	}
	omGhpList.Sort(ComparePrsn);
	ReloadFromMemory();
}


void GroupNamesPage::LoadBsdFields()
{
	omAltList.DeleteAll();
	omActList.DeleteAll();
	omAptList.DeleteAll();
	omHtyList.DeleteAll();
	omPstList.DeleteAll();
	omGatList.DeleteAll();
	omCicList.DeleteAll();
	omBltList.DeleteAll();
	omGhsList.DeleteAll();
	omNatList.DeleteAll();
	omGhpList.DeleteAll();

	CedaBsdData olBsdData;
	for(int i = 0; i < ogBsdData.omData.GetSize(); i++)
	{
		BSDDATA *prlBsd = &ogBsdData.omData[i];
		omBsdList.NewAt(omBsdList.GetSize(), ogBsdData.omData[i]);
	}
	omBsdList.Sort(CompareBsdc);
	ReloadFromMemory();
}


void GroupNamesPage::OnSelchangeCbGroupname() 
{
	CString olSelStr;
	int ilIdx;
	ilIdx = m_GroupNames.GetCurSel();
	if(ilIdx != LB_ERR)
	{
		m_GroupNames.GetLBText( ilIdx, olSelStr );
		ShowGroupAndMembers(olSelStr);
		ReloadFromMemory();
		MakeDiffList();
		CCSPtrArray<GRNDATA> olGrnList;
		//char pclWhere[100]="";
		//sprintf(pclWhere, " WHERE GRPN='%s'", olSelStr);
		ogGrnData.GetGrnByGrpn(olGrnList, olSelStr.GetBuffer(0));
		if(olGrnList.GetSize() > 0/*ogGrnData.ReadSpecial(olGrnList, pclWhere) == true*/)
		{
			m_GRSN.SetWindowText(olGrnList[0].Grsn);
		}
		else
		{
			m_GRSN.SetWindowText("");
		}
		olGrnList.DeleteAll();
		ShowPremisList();
	}
}

bool GroupNamesPage::IsPassFilter()
{
	return false;
}


static int CompareAct3(const ACTDATA **e1, const ACTDATA **e2)
{
	return (strcmp((**e1).Act3, (**e2).Act3));
}
static int CompareAPC3(const APTDATA **e1, const APTDATA **e2)
{
	return (strcmp((**e1).Apc3, (**e2).Apc3));
}
static int CompareALC2(const ALTDATA **e1, const ALTDATA **e2)
{
//	return (strcmp((**e1).Alc2, (**e2).Alc2));

		if (CString((**e1).Alc2) == CString((**e2).Alc2))
		{
			if (CString((**e1).Alc3) == CString((**e2).Alc3))
				return 0;

			if (CString((**e1).Alc3) > CString((**e2).Alc3))
				return 1;

			return -1;
		}

		if (CString((**e1).Alc2) > CString((**e2).Alc2))
			return 1;

		return -1;
}
static int CompareHtyp(const HTYDATA **e1, const HTYDATA **e2)
{
	return (strcmp((**e1).Htyp, (**e2).Htyp));
}
static int ComparePnam(const PSTDATA **e1, const PSTDATA **e2)
{
	return (strcmp((**e1).Pnam, (**e2).Pnam));
}
static int CompareGnam(const GATDATA **e1, const GATDATA **e2)
{
	return (strcmp((**e1).Gnam, (**e2).Gnam));
}
static int CompareCnam(const CICDATA **e1, const CICDATA **e2)
{
	return (strcmp((**e1).Cnam, (**e2).Cnam));
}
static int CompareBnam(const BLTDATA **e1, const BLTDATA **e2)
{
	return (strcmp((**e1).Bnam, (**e2).Bnam));
}

static int CompareLknm(const GHSDATA **e1, const GHSDATA **e2)
{
	return (strcmp((**e1).Lknm, (**e2).Lknm));
}
static int CompareTnam(const NATDATA **e1 ,const NATDATA **e2)
{
	return (strcmp((**e1).Tnam, (**e2).Tnam));
}
static int ComparePrsn(const GHPDATA **e1 ,const GHPDATA **e2)
{
	return (strcmp((**e1).Prsn, (**e2).Prsn));
}
static int CompareBsdc(const BSDDATA **e1 ,const BSDDATA **e2)
{
	return (strcmp((**e1).Bsdc, (**e2).Bsdc));
}


BOOL GroupNamesPage::OnKillActive() 
{
	pogDutyPreferences->pomCopyButton->EnableWindow(FALSE);
	pogDutyPreferences->pomStandardButton->EnableWindow(FALSE);
	pogDutyPreferences->pomAdditionalButton->EnableWindow(FALSE);	
	pogDutyPreferences->pomSaveButton->EnableWindow(FALSE);	
	pogDutyPreferences->pomNewButton->EnableWindow(FALSE);	
	pogDutyPreferences->pomUpdateButton->EnableWindow(FALSE);	
	pogDutyPreferences->pomDeleteButton->EnableWindow(FALSE);
	return CPropertyPage::OnKillActive();
}


void GroupNamesPage::OnEditchangeCbGroupname() 
{
	CString olGroupName;
	m_GroupNames.GetWindowText(olGroupName);
	ShowGroupAndMembers(olGroupName);
	ReloadFromMemory();
	MakeDiffList();

}

void GroupNamesPage::ShowGroupAndMembers(CString &ropGroupName)
{
	int ilCount = omGroupData.GetSize();
	for(int i = 0; i < ilCount; i++)
	{
		if(ropGroupName == omGroupData[i].Group.Grpn)
		{
			pomCurrentGroup = &omGroupData[i];
			imMode = UPDATE_MODE;
			ShowGroupMembers();
			i = ilCount;
		}
		else
		{
			imMode = NEW_MODE;
			pomCurrentGroup = NULL;
			m_Assigned.ResetContent();
		}
	}
	//MakeDiffList();
}

void GroupNamesPage::ShowGroupMembers()
{
	m_Assigned.ResetContent();
	if(pomCurrentGroup == NULL)
		return;

	omGrpActList.DeleteAll();
	omGrpAptList.DeleteAll();
	omGrpAltList.DeleteAll();
	omGrpHtyList.DeleteAll();
	omGrpPstList.DeleteAll();
	omGrpGatList.DeleteAll();
	omGrpCicList.DeleteAll();
	omGrpBltList.DeleteAll();
	omGrpGhsList.DeleteAll();
	omGrpNatList.DeleteAll();
	omGrpGhpList.DeleteAll();
	omGrpBsdList.DeleteAll();

	int ilCount = pomCurrentGroup->GroupMembers.GetSize();
	CStringArray olWhereList;
	CString olWhere;
	olWhere = CString(" WHERE URNO IN (");
	int ilAnz=0;
	for(int i = 0; i < ilCount; i++)
	{
		char pclUrno[30]="";
		if(i != 0 && (i % 50 == 0))
		{
			olWhere = olWhere.Mid(0, olWhere.GetLength()-1);
			olWhere += CString(")");
			olWhereList.Add(olWhere);
			olWhere = CString(" WHERE URNO IN ("); 

		}
		else
		{
			if(i + 1 == ilCount)
			{
				olWhere += CString(pomCurrentGroup->GroupMembers[i].Valu) + CString(")");//CString(") AND APPL='POPS')");
				olWhereList.Add(olWhere);
				//sprintf(pclUrno, "%s)", pomCurrentGroup->GroupMembers[i].Valu);
			}
			else
			{
				olWhere += CString(pomCurrentGroup->GroupMembers[i].Valu) + CString(",");
				//sprintf(pclUrno, "%s,", pomCurrentGroup->GroupMembers[i].Valu);
			}
		}
		//strcat(pclWhere, pclUrno);
	}

	if(	omCurrentTable == "ACT")
	{
		for(i = 0; i <  olWhereList.GetSize(); i++)
		{
			ogActData.ReadSpecial(omGrpActList, olWhereList[i].GetBuffer(0));
		}
		omGrpActList.Sort(CompareAct3);
		int ilC2 = omGrpActList.GetSize();
		for(i = 0; i < ilC2; i++)
		{
			char pclTextLine[256]="";
			sprintf(pclTextLine, "%-5s %-7s %-31s", omGrpActList[i].Act3, omGrpActList[i].Act5, omGrpActList[i].Acfn);
			m_Assigned.AddString(pclTextLine);
			m_Assigned.SetItemDataPtr(i, (void*)&omGrpActList[i]);
		}
	}
	if( omCurrentTable == "APT")
	{
		for(i = 0; i <  olWhereList.GetSize(); i++)
		{
			ogAptData.ReadSpecial(omGrpAptList, olWhereList[i].GetBuffer(0));
		}
		omGrpAptList.Sort(CompareAPC3);
		int ilC2 = omGrpAptList.GetSize();
		for(i = 0; i < ilC2; i++)
		{
			char pclTextLine[256]="";
			sprintf(pclTextLine, "%-5s %-7s %-31s", omGrpAptList[i].Apc3, omGrpAptList[i].Apc4, omGrpAptList[i].Apfn);
			m_Assigned.AddString(pclTextLine);
			m_Assigned.SetItemDataPtr(i, (void*)&omGrpAptList[i]);
		}
	}
	if(	omCurrentTable == "ALT")
	{
		for(i = 0; i <  olWhereList.GetSize(); i++)
		{
			ogAltData.ReadSpecial(omGrpAltList, olWhereList[i].GetBuffer(0));
		}
		omGrpAltList.Sort(CompareALC2);
		int ilC2 = omGrpAltList.GetSize();
		for(i = 0; i < ilC2; i++)
		{
			char pclTextLine[256]="";
			sprintf(pclTextLine, "%-5s %-7s %-31s", omGrpAltList[i].Alc2, omGrpAltList[i].Alc3, omGrpAltList[i].Alfn);
			m_Assigned.AddString(pclTextLine);
			m_Assigned.SetItemDataPtr(i, (void*)&omGrpAltList[i]);
		}
	}
	if(	omCurrentTable == "HTY")
	{
		for(i = 0; i <  olWhereList.GetSize(); i++)
		{
			ogHtyData.ReadSpecial(omGrpHtyList, olWhereList[i].GetBuffer(0));
		}
		omGrpHtyList.Sort(CompareHtyp);
		int ilC2 = omGrpHtyList.GetSize();
		for(i = 0; i < ilC2; i++)
		{
			char pclTextLine[256]="";
			sprintf(pclTextLine, "%-5s %-31s", omGrpHtyList[i].Htyp, omGrpHtyList[i].Hnam);
			m_Assigned.AddString(pclTextLine);
			m_Assigned.SetItemDataPtr(i, (void*)&omGrpHtyList[i]);
		}
	}
	if(	omCurrentTable == "PST")
	{
		for(i = 0; i <  olWhereList.GetSize(); i++)
		{
			ogPstData.ReadSpecial(omGrpPstList, olWhereList[i].GetBuffer(0));
		}
		omGrpPstList.Sort(ComparePnam);
		int ilC2 = omGrpPstList.GetSize();
		for(i = 0; i < ilC2; i++)
		{
			char pclTextLine[256]="";
			sprintf(pclTextLine, "%s", omGrpPstList[i].Pnam);
			m_Assigned.AddString(pclTextLine);
			m_Assigned.SetItemDataPtr(i, (void*)&omGrpPstList[i]);
		}
	}
	if(	omCurrentTable == "GAT")
	{
		for(i = 0; i <  olWhereList.GetSize(); i++)
		{
			ogGatData.ReadSpecial(omGrpGatList, olWhereList[i].GetBuffer(0));
		}
		omGrpGatList.Sort(CompareGnam);
		int ilC2 = omGrpGatList.GetSize();
		for(i = 0; i < ilC2; i++)
		{
			char pclTextLine[256]="";
			sprintf(pclTextLine, "%s", omGrpGatList[i].Gnam);
			m_Assigned.AddString(pclTextLine);
			m_Assigned.SetItemDataPtr(i, (void*)&omGrpGatList[i]);
		}
	}
	if(	omCurrentTable == "CIC")
	{
		for(i = 0; i <  olWhereList.GetSize(); i++)
		{
			ogCicData.ReadSpecial(omGrpCicList, olWhereList[i].GetBuffer(0));
		}
		omGrpCicList.Sort(CompareCnam);
		int ilC2 = omGrpCicList.GetSize();
		for(i = 0; i < ilC2; i++)
		{
			char pclTextLine[256]="";
			sprintf(pclTextLine, "%s", omGrpCicList[i].Cnam);
			m_Assigned.AddString(pclTextLine);
			m_Assigned.SetItemDataPtr(i, (void*)&omGrpCicList[i]);
		}
	}
	if(	omCurrentTable == "BLT")
	{
		for(i = 0; i <  olWhereList.GetSize(); i++)
		{
			ogBltData.ReadSpecial(omGrpBltList, olWhereList[i].GetBuffer(0));
		}
		omGrpBltList.Sort(CompareBnam);
		int ilC2 = omGrpBltList.GetSize();
		for(i = 0; i < ilC2; i++)
		{
			char pclTextLine[256]="";
			sprintf(pclTextLine, "%s", omGrpBltList[i].Bnam);
			m_Assigned.AddString(pclTextLine);
			m_Assigned.SetItemDataPtr(i, (void*)&omGrpBltList[i]);
		}
	}
	if(	omCurrentTable == "GHS")
	{
		for(i = 0; i <  olWhereList.GetSize(); i++)
		{
			ogGhsData.ReadSpecialData(&omGrpGhsList, olWhereList[i].GetBuffer(0), "URNO,LKNM", false);
		}
		omGrpGhsList.Sort(CompareLknm);
		int ilC2 = omGrpGhsList.GetSize();
		for(i = 0; i < ilC2; i++)
		{
			char pclTextLine[256]="";
			sprintf(pclTextLine, "%s", omGrpGhsList[i].Lknm);
			m_Assigned.AddString(pclTextLine);
			m_Assigned.SetItemDataPtr(i, (void*)&omGrpGhsList[i]);
		}
	}
	if(	omCurrentTable == "NAT")
	{
		for(i = 0; i <  olWhereList.GetSize(); i++)
		{
			ogNatData.ReadSpecialData(&omGrpNatList, olWhereList[i].GetBuffer(0), "URNO,TTYP,TNAM", false);
		}
		omGrpNatList.Sort(CompareTnam);
		int ilC2 = omGrpNatList.GetSize();
		for(i = 0; i < ilC2; i++)
		{
			char pclTextLine[256]="";
			sprintf(pclTextLine, "%6s  %s", omGrpNatList[i].Ttyp, omGrpNatList[i].Tnam);
			m_Assigned.AddString(pclTextLine);
			m_Assigned.SetItemDataPtr(i, (void*)&omGrpNatList[i]);
		}
	}
	if(	omCurrentTable == "GHP")
	{
		for(i = 0; i <  olWhereList.GetSize(); i++)
		{
			ogGhpData.ReadSpecialData(&omGrpGhpList, olWhereList[i].GetBuffer(0), "URNO,PRSN,PRCO", false);
		}
		omGrpGhpList.Sort(ComparePrsn);
		int ilC2 = omGrpGhpList.GetSize();
		for(i = 0; i < ilC2; i++)
		{
			char pclTextLine[256]="";
			PRCDATA *prlPrc = ogPrcData.GetPrcByUrno(atol(omGrpGhpList[i].Prco));
			if(prlPrc != NULL)
			{
				sprintf(pclTextLine, "%-5s %02d %s",prlPrc->Redc, prlPrc->Redl, omGrpGhpList[i].Prsn);
				m_Assigned.AddString(pclTextLine);
				m_Assigned.SetItemDataPtr(i, (void*)&omGrpGhpList[i]);
			}
		}
	}
	if(	omCurrentTable == "BSD")
	{
		for(i = 0; i <  olWhereList.GetSize(); i++)
		{
			ogBsdData.ReadSpecialData( &omGrpBsdList, olWhereList[i].GetBuffer(0), "URNO,BSDC,BSDS,BSDN", false);
		}
		omGrpBsdList.Sort(CompareBsdc);
		int ilC2 = omGrpBsdList.GetSize();
		for(i = 0; i < ilC2; i++)
		{
			char pclTextLine[256]="";
			sprintf(pclTextLine, "%-5s %-5s %-41s", omGrpBsdList[i].Bsdc, omGrpBsdList[i].Bsds, omGrpBsdList[i].Bsdn);
			m_Assigned.AddString(pclTextLine);
			m_Assigned.SetItemDataPtr(i, (void*)&omGrpBsdList[i]);
		}
	}
}

void GroupNamesPage::ClearGroups()
{
	int ilCount = omGroupData.GetSize();
	for(int i = 0; i < ilCount;  i++)
	{
		omGroupData[i].GroupMembers.DeleteAll();
	}
	omGroupData.DeleteAll();
}

void GroupNamesPage::LoadGroupsForTable()
{
	ClearGroups();
	m_GroupNames.ResetContent();
	char pclWhere[100]="";
	CCSPtrArray<GRNDATA> olGrnList;
	char pclTableName[16];
	strcpy( pclTableName, omCurrentTable);
	strcat( pclTableName, pcgTableExt);
	ogGrnData.GetGrnsByTabn(olGrnList, pclTableName, "POPS");
	int ilCount = olGrnList.GetSize();
	for(int i = 0; i < ilCount; i++)
	{
		GROUPS rlGroup;
		rlGroup.Group = olGrnList[i];
		CCSPtrArray<GRMDATA> olMemberList;
		//sprintf(pclWhere, " WHERE GURN = %d", olGrnList[i].Urno);
		//ogGrmData.ReadSpecial(olMemberList, pclWhere);
		ogGrmData.GetGrmDataByGrnUrno(olMemberList,olGrnList[i].Urno);
		int ilC = olMemberList.GetSize();
		for(int j = 0; j < ilC; j++)
		{
			GRMDATA rlGrm = olMemberList[j];
			rlGroup.GroupMembers.NewAt(rlGroup.GroupMembers.GetSize(),rlGrm);
		}
		omGroupData.NewAt(omGroupData.GetSize(), rlGroup);
		//rlGroup.GroupMembers.DeleteAll();
		m_GroupNames.AddString(olGrnList[i].Grpn);
		olMemberList.DeleteAll();
	}
	olGrnList.DeleteAll();
}

void GroupNamesPage::ReloadFromMemory()
{
	m_Available.ResetContent();
	if(omCurrentTable == "ACT")
	{
		char pclLine[500];
		int ilCount = omActList.GetSize();
		for(int i = 0; i < ilCount; i++)
		{
			sprintf(pclLine, "%-5s %-7s %-31s", omActList[i].Act3, omActList[i].Act5, omActList[i].Acfn);
			m_Available.AddString(pclLine);
			m_Available.SetItemDataPtr(i, (void *)&omActList[i]);
		}
	}
	else if(omCurrentTable == "ALT")
	{
		char pclLine[500];
		int ilCount = omAltList.GetSize();
		for(int i = 0; i < ilCount; i++)
		{
			sprintf(pclLine, "%-5s %-7s %-31s", omAltList[i].Alc2, omAltList[i].Alc3, omAltList[i].Alfn);
			m_Available.AddString(pclLine);
			m_Available.SetItemDataPtr(i, (void *)&omAltList[i]);
		}
	}
	else if(omCurrentTable == "APT")
	{
		char pclLine[500];
		int ilCount = omAptList.GetSize();
		for(int i = 0; i < ilCount; i++)
		{
			sprintf(pclLine, "%-5s %-7s %-31s", omAptList[i].Apc3, omAptList[i].Apc4, omAptList[i].Apfn);
			m_Available.AddString(pclLine);
			m_Available.SetItemDataPtr(i, (void *)&omAptList[i]);
		}
	}
	else if(omCurrentTable == "HTY")
	{
		char pclLine[500];
		int ilCount = omHtyList.GetSize();
		for(int i = 0; i < ilCount; i++)
		{
			sprintf(pclLine, "%-5s %-31s", omHtyList[i].Htyp, omHtyList[i].Hnam);
			m_Available.AddString(pclLine);
			m_Available.SetItemDataPtr(i, (void *)&omHtyList[i]);
		}
	}
	else if(omCurrentTable == "PST")
	{
		char pclLine[500];
		int ilCount = omPstList.GetSize();
		for(int i = 0; i < ilCount; i++)
		{
			sprintf(pclLine, "%s", omPstList[i].Pnam);
			m_Available.AddString(pclLine);
			m_Available.SetItemDataPtr(i, (void *)&omPstList[i]);
		}
	}
	else if(omCurrentTable == "GAT")
	{
		char pclLine[500];
		int ilCount = omGatList.GetSize();
		for(int i = 0; i < ilCount; i++)
		{
			sprintf(pclLine, "%s", omGatList[i].Gnam);
			m_Available.AddString(pclLine);
			m_Available.SetItemDataPtr(i, (void *)&omGatList[i]);
		}
	}
	else if(omCurrentTable == "CIC")
	{
		char pclLine[500];
		int ilCount = omCicList.GetSize();
		for(int i = 0; i < ilCount; i++)
		{
			sprintf(pclLine, "%s", omCicList[i].Cnam);
			m_Available.AddString(pclLine);
			m_Available.SetItemDataPtr(i, (void *)&omCicList[i]);
		}
	}
	else if(omCurrentTable == "BLT")
	{
		char pclLine[500];
		int ilCount = omBltList.GetSize();
		for(int i = 0; i < ilCount; i++)
		{
			sprintf(pclLine, "%s", omBltList[i].Bnam);
			m_Available.AddString(pclLine);
			m_Available.SetItemDataPtr(i, (void *)&omBltList[i]);
		}
	}
	else if(omCurrentTable == "GHS")
	{
		char pclLine[500];
		int ilCount = omGhsList.GetSize();
		for(int i = 0; i < ilCount; i++)
		{
			sprintf(pclLine, "%s", omGhsList[i].Lknm);
			m_Available.AddString(pclLine);
			m_Available.SetItemDataPtr(i, (void *)&omGhsList[i]);
		}
	}
	else if(omCurrentTable == "NAT")
	{
		char pclLine[500];
		int ilCount = omNatList.GetSize();
		for(int i = 0; i < ilCount; i++)
		{
			sprintf(pclLine, "%6s  %s", omNatList[i].Ttyp, omNatList[i].Tnam);
			m_Available.AddString(pclLine);
			m_Available.SetItemDataPtr(i, (void *)&omNatList[i]);
		}
	}
	else if(omCurrentTable == "GHP")
	{
		char pclLine[500];
		int ilCount = omGhpList.GetSize();
		for(int i = 0; i < ilCount; i++)
		{
			PRCDATA *prlPrc = ogPrcData.GetPrcByUrno(atol(omGhpList[i].Prco));
			if(prlPrc != NULL)
			{
				sprintf(pclLine, "%-5s %02d %s",prlPrc->Redc, prlPrc->Redl, omGhpList[i].Prsn);
				m_Available.AddString(pclLine);
				m_Available.SetItemDataPtr(i, (void *)&omGhpList[i]);
			}
		}
	}
	else if(omCurrentTable == "BSD")
	{
		char pclLine[500];
		int ilCount = omBsdList.GetSize();
		for(int i = 0; i < ilCount; i++)
		{
			sprintf(pclLine, "%-5s %-5s %-41s", omBsdList[i].Bsdc, omBsdList[i].Bsds, omBsdList[i].Bsdn);
			m_Available.AddString(pclLine);
			m_Available.SetItemDataPtr(i, (void *)&omBsdList[i]);
		}
	}
}

BOOL GroupNamesPage::OnSetActive() 
{
	// TODO: Add your specialized code here and/or call the base class
	pogDutyPreferences->pomCopyButton->EnableWindow(FALSE);
	pogDutyPreferences->pomStandardButton->EnableWindow(FALSE);
	pogDutyPreferences->pomAdditionalButton->EnableWindow(FALSE);	
	pogDutyPreferences->pomSaveButton->EnableWindow(FALSE);	
	pogDutyPreferences->pomNewButton->EnableWindow(FALSE);	
	pogDutyPreferences->pomUpdateButton->EnableWindow(FALSE);	
	pogDutyPreferences->pomDeleteButton->EnableWindow(TRUE);
	
	return CPropertyPage::OnSetActive();
}
LONG GroupNamesPage::OnDeleteButton(WPARAM wParam, LPARAM lParam)
{
	if(MessageBox(GetString(IDS_STRING1440), GetString(IDS_STRING311), (MB_ICONQUESTION|MB_YESNO)) == IDYES)
	{
		if((m_PremisList.GetCount() > 0)  && (omCurrentTable != "GHP"))
		{
			MessageBox(GetString(IDS_STRING313), GetString(IDS_STRING117), (MB_ICONEXCLAMATION|MB_OK));
		}
		else
		{
			for(int i = 0; i < m_Assigned.GetCount(); i++)
			{
				m_Assigned.SetSel(i);
			}
			OnDelete();
		}
	}
	return 0L;
}

void GroupNamesPage::OnDblclkPremisList() 
{
	CPropertySheet *polParent;
	polParent = (CPropertySheet*)GetParent();
	if(polParent != NULL)
	{
		polParent->SetActivePage(0);
		PremisPage *polPremisPage;
		polPremisPage = (PremisPage *)polParent->GetActivePage();
		if(polPremisPage != NULL)
		{
			int ilIndex = m_PremisList.GetCurSel();
			if(ilIndex != LB_ERR)
			{
				long llUrno = (int)omGhpUrnos[ilIndex];
				polPremisPage->ShowPremisByUrno(llUrno);
			}
		}
	}
}

void GroupNamesPage::ShowPremisList()
{

	m_PremisList.ResetContent();
	omGhpUrnos.RemoveAll();
	omGhpUrnos.RemoveAll();
	char pclListText[500]="";
	if(pomCurrentGroup != NULL)	
	{
		GRNDATA *prlGrn = ogGrnData.GetGrnByUrno(pomCurrentGroup->Group.Urno);
		if(prlGrn != NULL)
		{
			POSITION rlPos;
			for ( rlPos = ogGhpData.omUrnoMap.GetStartPosition(); rlPos != NULL; )
			{
				long llUrno;
				GHPDATA *prlGhp;
				ogGhpData.omUrnoMap.GetNextAssoc(rlPos, (void *&)llUrno, (void *&)prlGhp);
				if(prlGhp != NULL)
				{
					if(omCurrentTable == "ACT")
					{
						if(prlGrn->Urno == atoi(prlGhp->Actm))
						{
							PRCDATA *prlPrc = ogPrcData.GetPrcByUrno(atol(prlGhp->Prco));
							if(prlPrc != NULL)
							{
								sprintf(pclListText, "%-5s %02d    %-14s  %-40s   %s  -   %s", prlPrc->Redc, prlPrc->Redl,
								prlGhp->Prsn, prlGhp->Prna, prlGhp->Vpfr.Format("%d.%m.%Y"), prlGhp->Vpto.Format("%d.%m.%Y"));
								m_PremisList.AddString(pclListText);
								omGhpUrnos.Add(prlGhp->Urno);
							}
						}
					}
					else if(omCurrentTable == "ALT")
					{
						if(prlGrn->Urno == atoi(prlGhp->Alma) || prlGrn->Urno == atoi(prlGhp->Almd))
						{
							PRCDATA *prlPrc = ogPrcData.GetPrcByUrno(atol(prlGhp->Prco));
							if(prlPrc != NULL)
							{
								sprintf(pclListText, "%-5s %02d    %-14s  %-40%s   %s  -   %s", prlPrc->Redc, prlPrc->Redl,
								prlGhp->Prsn, prlGhp->Prna, prlGhp->Vpfr.Format("%d.%m.%Y"), prlGhp->Vpto.Format("%d.%m.%Y"));
								m_PremisList.AddString(pclListText);
								omGhpUrnos.Add(prlGhp->Urno);
							}
						}
					}
					else if(omCurrentTable == "APT")
					{
						if(prlGrn->Urno == atoi(prlGhp->Trra) || prlGrn->Urno == atoi(prlGhp->Trrd))
						{
							PRCDATA *prlPrc = ogPrcData.GetPrcByUrno(atol(prlGhp->Prco));
							if(prlPrc != NULL)
							{
								sprintf(pclListText, "%-5s %02d    %-14s  %-40s   %s  -   %s", prlPrc->Redc, prlPrc->Redl,
								prlGhp->Prsn, prlGhp->Prna, prlGhp->Vpfr.Format("%d.%m.%Y"), prlGhp->Vpto.Format("%d.%m.%Y"));
								m_PremisList.AddString(pclListText);
								omGhpUrnos.Add(prlGhp->Urno);
							}
						}
					}
					else if(omCurrentTable == "HTY")
					{
						if(prlGrn->Urno == atoi(prlGhp->Htga) || prlGrn->Urno == atoi(prlGhp->Htgd))
						{
							PRCDATA *prlPrc = ogPrcData.GetPrcByUrno(atol(prlGhp->Prco));
							if(prlPrc != NULL)
							{
								sprintf(pclListText, "%-5s %02d    %-14s  %-40s   %s  -   %s", prlPrc->Redc, prlPrc->Redl,
								prlGhp->Prsn, prlGhp->Prna, prlGhp->Vpfr.Format("%d.%m.%Y"), prlGhp->Vpto.Format("%d.%m.%Y"));
								m_PremisList.AddString(pclListText);
								omGhpUrnos.Add(prlGhp->Urno);
							}
						}
					}
					else if(omCurrentTable == "PST")
					{
						if(prlGrn->Urno == atoi(prlGhp->Pcaa) || prlGrn->Urno == atoi(prlGhp->Pcad))
						{
							PRCDATA *prlPrc = ogPrcData.GetPrcByUrno(atol(prlGhp->Prco));
							if(prlPrc != NULL)
							{
								sprintf(pclListText, "%-5s %02d    %-14s  %-40s   %s  -   %s", prlPrc->Redc, prlPrc->Redl,
								prlGhp->Prsn, prlGhp->Prna, prlGhp->Vpfr.Format("%d.%m.%Y"), prlGhp->Vpto.Format("%d.%m.%Y"));
								m_PremisList.AddString(pclListText);
								omGhpUrnos.Add(prlGhp->Urno);
							}
						}
					}
					else if(omCurrentTable == "NAT")
					{
						if(prlGrn->Urno == atoi(prlGhp->Ttga) || prlGrn->Urno == atoi(prlGhp->Ttgd))
						{
							PRCDATA *prlPrc = ogPrcData.GetPrcByUrno(atol(prlGhp->Prco));
							if(prlPrc != NULL)
							{
								sprintf(pclListText, "%-5s %02d    %-14s  %-40s   %s  -   %s", prlPrc->Redc, prlPrc->Redl,
								prlGhp->Prsn, prlGhp->Prna, prlGhp->Vpfr.Format("%d.%m.%Y"), prlGhp->Vpto.Format("%d.%m.%Y"));
								m_PremisList.AddString(pclListText);
								omGhpUrnos.Add(prlGhp->Urno);
							}
						}
					}
					else if(omCurrentTable == "GHP")
					{
						m_PremisList.ResetContent();
//						PRCDATA *prlPrc = ogPrcData.GetPrcByUrno(atol(prlGhp->Prco));
//						if(prlPrc != NULL)
//						{
//
//							sprintf(pclListText, "%-5s %02d    %-14s  %-40s   %s  -   %s", prlPrc->Redc, prlPrc->Redl,
//							prlGhp->Prsn, prlGhp->Prna, prlGhp->Vpfr.Format("%d.%m.%Y"), prlGhp->Vpto.Format("%d.%m.%Y"));
//							m_PremisList.AddString(pclListText);
//							omGhpUrnos.Add(prlGhp->Urno);
//						}
					}
					else if(omCurrentTable == "BSD")
					{
						; // nothing to do
					}

				}
			}
		}
	}
}
