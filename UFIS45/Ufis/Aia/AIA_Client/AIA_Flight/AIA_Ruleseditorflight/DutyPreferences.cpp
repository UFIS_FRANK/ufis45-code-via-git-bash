// DutyPreferences.cpp : implementation file
//

#include "stdafx.h"
#include "Rules.h"
#include "CCSGlobl.h"
#include "DutyPreferences.h"
#include "PremisAwDlg.h"
#include "resrc1.h"
#include "CedaGATData.h"
#include "CedaPSTData.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


PremisAwDlg *pogPremisAwDlg;
DutyPreferences *pogDutyPreferences;

/////////////////////////////////////////////////////////////////////////////
// DutyPreferences

IMPLEMENT_DYNAMIC(DutyPreferences, CPropertySheet)

DutyPreferences::DutyPreferences(UINT nIDCaption, CWnd* pParentWnd, UINT iSelectPage)
	:CPropertySheet(nIDCaption, pParentWnd, iSelectPage)
{

    CPropertySheet::Create(pParentWnd, WS_MAXIMIZEBOX  | WS_MINIMIZEBOX  | WS_CAPTION | WS_SYSMENU |  WS_VISIBLE | WS_SYSMENU | WS_POPUP | WS_CAPTION | DS_MODALFRAME | WS_THICKFRAME);
	pogDutyPreferences = this;
	pogPremisAwDlg = NULL;
	omGroupNamePage.m_psp.dwFlags &= ~(PSP_HASHELP);
	omPremisPage.m_psp.dwFlags &= ~(PSP_HASHELP);
	AddPage(&omPremisPage);
	AddPage(&omGroupNamePage);
	AddPage(&omGatposPage);
	AddPage(&omGatposPage2);

	pomSaveButton = NULL;
	pomCancelButton = NULL;
	pomNewButton = NULL;
	pomUpdateButton = NULL;
	pomDeleteButton = NULL;
	pomCopyButton = NULL;
	pomStandardButton = NULL;
	pomAdditionalButton = NULL;
	pomComboBox = NULL;
	pomBereich = NULL;
}

DutyPreferences::DutyPreferences(LPCTSTR pszCaption, CWnd* pParentWnd, UINT iSelectPage)
	:CPropertySheet(pszCaption, pParentWnd, iSelectPage)
{
    CPropertySheet::Create(pParentWnd, WS_MAXIMIZEBOX  | WS_MINIMIZEBOX  | WS_CAPTION | WS_SYSMENU |  WS_VISIBLE | WS_SYSMENU | WS_POPUP | WS_CAPTION | DS_MODALFRAME | WS_THICKFRAME);
	pogDutyPreferences = this;
	omGroupNamePage.m_psp.dwFlags &= ~(PSP_HASHELP);
	omPremisPage.m_psp.dwFlags &= ~(PSP_HASHELP);
	AddPage(&omPremisPage);
	AddPage(&omGroupNamePage);
	AddPage(&omGatposPage);
	AddPage(&omGatposPage2);
	pomSaveButton = NULL;
	pomCancelButton = NULL;
	pomNewButton = NULL;
	pomUpdateButton = NULL;
	pomDeleteButton = NULL;
	pomCopyButton = NULL;
	pomStandardButton = NULL;
	pomAdditionalButton = NULL;
	pomComboBox = NULL;
	pomBereich = NULL;
}

DutyPreferences::~DutyPreferences()
{
//	pogDutyPreferences = NULL;

	delete pomSaveButton;
	delete pomCancelButton;
	delete pomNewButton;
	delete pomUpdateButton;
	delete pomDeleteButton;
	delete pomCopyButton;
	delete pomStandardButton;
	delete pomAdditionalButton;
	delete pomComboBox;
	delete pomBereich;
}


BEGIN_MESSAGE_MAP(DutyPreferences, CPropertySheet)
	//{{AFX_MSG_MAP(DutyPreferences)
		ON_MESSAGE(WM_RESETRELEASE, OnResetRelease)
		ON_MESSAGE(WM_SETRELEASE, OnSetRelease)
		ON_WM_CHAR()
		ON_WM_KEYDOWN()
		ON_WM_DESTROY()
		ON_WM_NCDESTROY()
		ON_BN_CLICKED(ID_BUTTON_SAVE,		OnSaveButton)
		ON_BN_CLICKED(ID_BUTTON_NEW,		OnNewButton)
		ON_BN_CLICKED(ID_BUTTON_CLOSE,		OnCloseButton)
		ON_BN_CLICKED(ID_BUTTON_UPDATE,		OnUpdateButton)
		ON_BN_CLICKED(ID_BUTTON_DELETE,		OnDeleteButton)
		ON_BN_CLICKED(ID_BUTTON_COPY,		OnCopyButton)
		ON_BN_CLICKED(ID_BUTTON_STANDARD,	OnStandardButton)
		ON_BN_CLICKED(ID_BUTTON_ADDITIONAL, OnAdditionalButton)
		ON_BN_CLICKED(IDCANCEL, OnCancel)
		ON_WM_CLOSE()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// DutyPreferences message handlers

BOOL DutyPreferences::OnInitDialog() 
{
	BOOL bResult = CPropertySheet::OnInitDialog();


	HICON m_hIcon = AfxGetApp()->LoadIcon(IDI_UFIS);
	SetIcon(m_hIcon, FALSE);

	CWnd *olHelpButton = GetDlgItem(IDHELP);
	if (olHelpButton != NULL)
	{
		olHelpButton->ShowWindow(SW_HIDE);
	}
	CWnd *olOkButton = GetDlgItem(IDOK);
	if (olOkButton != NULL)
	{
		olOkButton->ShowWindow(SW_HIDE);
	}
	int ilHeight;
	int ilWidth;
	CWnd *olCancelButton = GetDlgItem(IDCANCEL);
	if (olCancelButton != NULL)
	{
		olCancelButton->ShowWindow(SW_HIDE);

		CRect olR;
		CRect olCB;
		olCancelButton->GetWindowRect(olCB);

		GetClientRect(&olR);
		ilHeight = olCB.bottom - olCB.top;
		ilWidth = olCB.right - olCB.left;
		olCB.top = 2;
		olCB.left = 285;
		olCB.bottom = olCB.top + ilHeight;
		olCB.right = olCB.left + ilWidth;

		olCancelButton->SetWindowText(LoadStg(IDS_STRING156));
		olCancelButton->MoveWindow(olCB);
		

	}
	/***********************
	CButton  *olButton = (CButton *) GetDlgItem(IDCANCEL);
	if (olButton != NULL)
	{
		UINT ilStyle = olButton->GetButtonStyle();
		ilStyle &= ~BS_DEFPUSHBUTTON;
		olButton->SetButtonStyle(ilStyle,TRUE);

	}
	********************/
	// ---resize the CTabCtrl---
	CRect olCR;
	GetWindowRect(&olCR);
	CRect rcCTabCtrl;
	CTabCtrl *plTab = GetTabControl();
	//ASSERT(plTab);
	plTab->GetWindowRect(&rcCTabCtrl);
	ScreenToClient(&rcCTabCtrl);
	rcCTabCtrl.top += ilHeight;
	//rcCTabCtrl.right += ResizeRight;
	rcCTabCtrl.bottom = olCR.bottom - 1;
	plTab->MoveWindow(&rcCTabCtrl);

	// ---resize the page---
	CPropertyPage *polPage = GetActivePage();
	//ASSERT(plPage);
	CRect olPageRect;
	polPage->GetWindowRect(&olPageRect);
	ScreenToClient(&olPageRect);
	olPageRect.top += ilHeight;
	olPageRect.bottom = olCR.bottom - 1;
	polPage->MoveWindow(&olPageRect);

	//SetFont(&ogSmallFonts_Regular_7);
	olCR.bottom -= 10;
	MoveWindow(olCR);

	//SetFont(&ogMSSansSerif_Regular_8);
   CRect olPosRect;
	CRect olTmpRect;
	GetClientRect(olPosRect);
	olPosRect.bottom = olPosRect.top + ilHeight;
	olPosRect.right = 100;
	olTmpRect = olPosRect;
	olTmpRect.top += 5;
	//SetFont(&ogMSSansSerif_Regular_8);
	pomBereich = new CStatic();
	pomBereich->Create(LoadStg(IDS_STRING148),WS_VISIBLE,olTmpRect,this,ID_STATIC_BEEREICH);
	pomBereich->SetFont(&ogMSSansSerif_Regular_8);
//	pomBereich->ShowWindow(SW_SHOWNORMAL);
	pomBereich->ShowWindow(SW_HIDE);
	olPosRect.left += 50;
	olPosRect.right += ilWidth + 30;
	CRect olLBRect = olPosRect;
	olLBRect.top += 2;
	olLBRect.bottom = 100;
	pomComboBox = new CComboBox();
	pomComboBox->Create(WS_VISIBLE|WS_TABSTOP|CBS_AUTOHSCROLL|CBS_DROPDOWNLIST|WS_VSCROLL,olLBRect,this,ID_COMBOBOX_COMBO);
	pomComboBox->SetFont(&ogMSSansSerif_Regular_8);
	pomComboBox->AddString("CKI");
	pomComboBox->SetCurSel(0);
	pomComboBox->ShowWindow(SW_HIDE);
	
	//pomComboBox->SetDroppedWidth( 100 );
	pomBereich->ShowWindow(SW_SHOWNORMAL);
	olPosRect.left = olLBRect.right + 2;
	olPosRect.right = olPosRect.left + ilWidth;
	olPosRect.top +=2; 
	olPosRect.bottom +=2;
	pomSaveButton = new CCSButtonCtrl();
	pomSaveButton->Create(LoadStg(IDS_STRING149),BS_OWNERDRAW|WS_VISIBLE|BS_PUSHBUTTON|WS_TABSTOP,olPosRect,this,ID_BUTTON_SAVE);
	pomSaveButton->SetFont(&ogMSSansSerif_Regular_8);
	pomSaveButton->SetColors(::GetSysColor(COLOR_BTNFACE), ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
	pomSaveButton->ShowWindow(SW_SHOWNORMAL);
	olPosRect.left  = +olPosRect.right+2;
	olPosRect.right = olPosRect.left + ilWidth;
	if (olCancelButton != NULL)
	{
		CRect olR;
		CRect olCB;
		olCancelButton->GetWindowRect(olCB);
		ScreenToClient(olCB);
//		olPosRect.left = olCB.right+2;
//		olPosRect.right = olPosRect.left + ilWidth;
		olPosRect.left = olCB.left;
		olPosRect.right = olCB.left + ilWidth;
	}
     

	pomCancelButton = new CButton();
	pomCancelButton->Create(LoadStg(IDS_STRING156),WS_VISIBLE|BS_PUSHBUTTON|WS_TABSTOP,olPosRect,this,ID_BUTTON_CLOSE);
	pomCancelButton->SetFont(&ogMSSansSerif_Regular_8);
	pomCancelButton->ShowWindow(SW_SHOWNORMAL);
	olPosRect.left = olPosRect.right+2;
	olPosRect.right = olPosRect.left + ilWidth;

	pomNewButton  = new CButton();
	pomNewButton->Create(LoadStg(IDS_STRING150),WS_VISIBLE|BS_PUSHBUTTON|WS_TABSTOP,olPosRect,this,ID_BUTTON_NEW);
	pomNewButton->SetFont(&ogMSSansSerif_Regular_8);
	pomNewButton->ShowWindow(SW_SHOWNORMAL);
	olPosRect.left = olPosRect.right+2;
	olPosRect.right = olPosRect.left + ilWidth;

	pomUpdateButton = new CButton();
	pomUpdateButton->Create(LoadStg(IDS_STRING151),WS_VISIBLE|BS_PUSHBUTTON|WS_TABSTOP,olPosRect,this,ID_BUTTON_UPDATE);
	pomUpdateButton->SetFont(&ogMSSansSerif_Regular_8);
	pomUpdateButton->ShowWindow(SW_SHOWNORMAL);
	olPosRect.left = olPosRect.right+2;
	olPosRect.right = olPosRect.left + ilWidth;

	pomDeleteButton = new CButton();
	pomDeleteButton->Create(LoadStg(IDS_STRING152),WS_VISIBLE|BS_PUSHBUTTON|WS_TABSTOP,olPosRect,this,ID_BUTTON_DELETE);
	pomDeleteButton->SetFont(&ogMSSansSerif_Regular_8);
	pomDeleteButton->ShowWindow(SW_SHOWNORMAL);
	olPosRect.left = olPosRect.right+2;
	olPosRect.right = olPosRect.left + ilWidth;

	pomCopyButton = new CButton();
	pomCopyButton->Create(LoadStg(IDS_STRING153),WS_VISIBLE|BS_PUSHBUTTON|WS_TABSTOP,olPosRect,this,ID_BUTTON_COPY);
	pomCopyButton->SetFont(&ogMSSansSerif_Regular_8);
	pomCopyButton->ShowWindow(SW_SHOWNORMAL);
	olPosRect.left = olPosRect.right+2;
	olPosRect.right = olPosRect.left + ilWidth;

	pomStandardButton = new CButton();
	pomStandardButton->Create(LoadStg(IDS_STRING154),WS_VISIBLE|BS_PUSHBUTTON|WS_TABSTOP,olPosRect,this,ID_BUTTON_STANDARD);
	pomStandardButton->SetFont(&ogMSSansSerif_Regular_8);
	pomStandardButton->ShowWindow(SW_SHOWNORMAL);
	olPosRect.left = olPosRect.right+2;
	olPosRect.right = olPosRect.left + ilWidth;

	pomAdditionalButton = new CButton();
	pomAdditionalButton->Create(LoadStg(IDS_STRING155),WS_VISIBLE|BS_PUSHBUTTON|WS_TABSTOP,olPosRect,this,ID_BUTTON_ADDITIONAL);
	pomAdditionalButton->SetFont(&ogMSSansSerif_Regular_8);
	pomAdditionalButton->ShowWindow(SW_SHOWNORMAL);
	olPosRect.left = olPosRect.right+2;
	olPosRect.right = olPosRect.left + ilWidth;
	
//	SetWindowPos(&wndTop,0, 0, 0, 0, SWP_SHOWWINDOW | SWP_NOMOVE | SWP_NOSIZE);

//-------------
//Monitorsetup
	CRect olRect;
	GetWindowRect(&olRect);
	ilWidth = olRect.right -olRect.left;
	int ilWhichMonitor = ogCfgData.GetMonitorForWindow(CString(MON_RULES_STRING));
	int ilMonitors = ogCfgData.GetMonitorForWindow(CString(MON_COUNT_STRING));
	int left, top, right, bottom;
	int ilCXMonitor;
	if(ilWhichMonitor == 1)
	{
		top = olRect.top;
		bottom = olRect.bottom;
		switch(ilMonitors)
		{
		case 1:
			ilCXMonitor = ::GetSystemMetrics(SM_CXSCREEN);
			left = (int)((ilCXMonitor-ilWidth)/2);
			right = left + ilWidth;//GetSystemMetrics(SM_CXSCREEN);
			break;
		case 2:
			ilCXMonitor = (int)(::GetSystemMetrics(SM_CXSCREEN)/2);
			left = (int)((ilCXMonitor-ilWidth)/2);
			right = left + ilWidth;//GetSystemMetrics(SM_CXSCREEN);
			break;
		case 3:
			ilCXMonitor = (int)(::GetSystemMetrics(SM_CXSCREEN)/3);
			left = (int)((ilCXMonitor-ilWidth)/2);
			right = left + ilWidth;//GetSystemMetrics(SM_CXSCREEN);
			break;
		default:
			ilCXMonitor = ::GetSystemMetrics(SM_CXSCREEN);
			left = (int)((ilCXMonitor-ilWidth)/2);
			right = left + ilWidth;//GetSystemMetrics(SM_CXSCREEN);
			break;
		}
	}
	else if(ilWhichMonitor == 2)
	{
		top = olRect.top;
		bottom = olRect.bottom;
		switch(ilMonitors)
		{
		case 1:
			ilCXMonitor = ::GetSystemMetrics(SM_CXSCREEN);
			left = (int)((ilCXMonitor-ilWidth)/2);
			right = left + ilWidth;//GetSystemMetrics(SM_CXSCREEN);
			break;
		case 2:
			ilCXMonitor = (int)(::GetSystemMetrics(SM_CXSCREEN)/2);
			left = (int)((ilCXMonitor-ilWidth)/2)+(ilCXMonitor);
			right = left + ilWidth;//GetSystemMetrics(SM_CXSCREEN);
			break;
		case 3:
			ilCXMonitor = (int)(::GetSystemMetrics(SM_CXSCREEN)/3);
			left = (int)((ilCXMonitor-ilWidth)/2)+(ilCXMonitor);
			right = left + ilWidth;//GetSystemMetrics(SM_CXSCREEN);
			break;
		default:
			right = GetSystemMetrics(SM_CXSCREEN);
			break;
		}
	}
	else if(ilWhichMonitor == 3)
	{
		top = olRect.top;
		bottom = olRect.bottom;
		switch(ilMonitors)
		{
		case 1:
			ilCXMonitor = ::GetSystemMetrics(SM_CXSCREEN);
			left = (int)((ilCXMonitor-ilWidth)/2);
			right = left + ilWidth;//GetSystemMetrics(SM_CXSCREEN);
			break;
		case 2:
			ilCXMonitor = (int)(::GetSystemMetrics(SM_CXSCREEN)/2);
			left = (int)((ilCXMonitor-ilWidth)/2);
			right = left + ilWidth;//GetSystemMetrics(SM_CXSCREEN);
			break;
		case 3:
			ilCXMonitor = (int)(::GetSystemMetrics(SM_CXSCREEN)/3);
			left = (int)((ilCXMonitor-ilWidth)/2)+(2*ilCXMonitor);
			right = left + ilWidth;//GetSystemMetrics(SM_CXSCREEN);
			break;
		default:
			right = GetSystemMetrics(SM_CXSCREEN);
			break;
		}
	}
//-------------
	MoveWindow(CRect(left, top, right, bottom));
	SetWindowPos(&wndTop,0, 0, 0, 0, SWP_SHOWWINDOW | SWP_NOMOVE | SWP_NOSIZE);
	OnNewButton();
	return bResult;

}

void DutyPreferences::OnCancel()
{
	OnCloseButton();
}

void DutyPreferences::OnOK()
{
	int i= 4711;
}

void DutyPreferences::OnSaveButton()
{
	int ilIdx = GetActiveIndex();
	switch(ilIdx)
	{
	case 0:
		omPremisPage.SendMessage(CLK_BUTTON_SAVE, 0, 0);
		break;
	case 1:
		omGroupNamePage.SendMessage(CLK_BUTTON_SAVE, 0, 0);
		break;
	}
}
void DutyPreferences::OnNewButton()
{
	int ilIdx = GetActiveIndex();
	switch(ilIdx)
	{
	case 0:
		omPremisPage.SendMessage(CLK_BUTTON_NEW, 0, 0);
		break;
	case 1:
		omGroupNamePage.SendMessage(CLK_BUTTON_NEW, 0, 0);
		break;
	}
}
void DutyPreferences::OnUpdateButton()
{
	int ilIdx = GetActiveIndex();
	switch(ilIdx)
	{
	case 0:
		omPremisPage.SendMessage(CLK_BUTTON_UPDATE, 0, 0);
		break;
	case 1:
		omGroupNamePage.SendMessage(CLK_BUTTON_UPDATE, 0, 0);
		break;
	}
}
void DutyPreferences::OnDeleteButton()
{
	int ilIdx = GetActiveIndex();
	switch(ilIdx)
	{
	case 0:
		omPremisPage.SendMessage(CLK_BUTTON_DELETE, 0, 0);
		break;
	case 1:
		omGroupNamePage.SendMessage(CLK_BUTTON_DELETE, 0, 0);
		break;
	}
}
void DutyPreferences::OnCopyButton()
{
	int ilIdx = GetActiveIndex();
	switch(ilIdx)
	{
	case 0:
		omPremisPage.SendMessage(CLK_BUTTON_COPY, 0, 0);
		break;
	case 1:
		omGroupNamePage.SendMessage(CLK_BUTTON_COPY, 0, 0);
		break;
	}
}
void DutyPreferences::OnStandardButton()
{
	int ilIdx = GetActiveIndex();
	switch(ilIdx)
	{
	case 0:
		omPremisPage.SendMessage(CLK_BUTTON_STANDARD, 0, 0);
		break;
	case 1:
		omGroupNamePage.SendMessage(CLK_BUTTON_STANDARD, 0, 0);
		break;
	}
}
void DutyPreferences::OnAdditionalButton()
{
	int ilIdx = GetActiveIndex();
	switch(ilIdx)
	{
	case 0:
		omPremisPage.SendMessage(CLK_BUTTON_ADDITIONAL, 0, 0);
		break;
	case 1:
		omGroupNamePage.SendMessage(CLK_BUTTON_ADDITIONAL, 0, 0);
		break;
	}
}


LONG DutyPreferences::OnResetRelease(WPARAM wParam, LPARAM lParam)
{
	pomSaveButton->SetColors( ::GetSysColor(COLOR_BTNFACE), ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
	pomSaveButton->UpdateWindow();
	return 0L;
}

LONG DutyPreferences::OnSetRelease(WPARAM wParam, LPARAM lParam)
{
	pomSaveButton->SetColors( ogColors[RED_IDX], ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
	pomSaveButton->UpdateWindow();
	return 0L;
}


void DutyPreferences::OnChar(UINT nChar, UINT nRepCnt, UINT nFlags) 
{
	// TODO: Add your message handler code here and/or call default
	
	CPropertySheet::OnChar(nChar, nRepCnt, nFlags);
}

void DutyPreferences::OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags) 
{
	// TODO: Add your message handler code here and/or call default
	
	CPropertySheet::OnKeyDown(nChar, nRepCnt, nFlags);
}

void DutyPreferences::OnDestroy() 
{
	CPropertySheet::OnDestroy();

	
}

void DutyPreferences::OnNcDestroy() 
{
	CPropertySheet::OnNcDestroy();
	
	// TODO: Add your message handler code here
	
}

void DutyPreferences::OnClose() 
{
	OnCloseButton();
}


void DutyPreferences::OnCloseButton() 
{
	// TODO: Add your message handler code here and/or call default
	
	/*
	if(MessageBox( "Save Data?", GetString(IDS_STRING117), (MB_YESNO| MB_ICONEXCLAMATION))== IDYES)
	{

		SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
		
		ogGatData.SaveAll();
		ogPstData.SaveAll();
		SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));


	}
	*/




	if(MessageBox( GetString(IDS_STRING330), GetString(IDS_STRING117), (MB_YESNO| MB_ICONEXCLAMATION)) != IDYES)
		return;

	CPropertySheet::EndDialog(0);
}

BOOL DutyPreferences::OnCommand(WPARAM wParam, LPARAM lParam) 
{
	// TODO: Add your specialized code here and/or call the base class
	
//	return CPropertySheet::OnCommand(wParam, lParam);
	int low = LOWORD(lParam);
	int high = HIWORD(lParam);

	if (wParam == IDCANCEL || wParam == IDOK )
		return TRUE;

	return CPropertySheet::OnCommand(wParam, lParam);
}
