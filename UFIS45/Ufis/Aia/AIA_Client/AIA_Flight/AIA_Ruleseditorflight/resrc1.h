//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by Rules.rc
//
#define IDD_POS_MATRIX                  140
#define IDS_STRING330                   330
#define IDC_ALBB                        1067
#define IDC_ALWR                        1068
#define IDC_NPOS3                       1069
#define IDC_MATRIX                      1069
#define IDC_NPOS4                       1070
#define IDC_NPOS5                       1071
#define IDC_NPOS6                       1072
#define IDC_NPOS7                       1073
#define IDC_NPOS8                       1074
#define IDC_NPOS9                       1075
#define IDC_NPOS10                      1076
#define IDC_MATRIX1                     1077
#define IDC_MATRIX2                     1078
#define IDC_MATRIX3                     1079
#define IDC_MATRIX4                     1080
#define IDC_MATRIX5                     1081
#define IDC_MATRIX6                     1082
#define IDC_MATRIX7                     1083
#define IDC_MATRIX8                     1084
#define IDC_MATRIX9                     1085
#define IDC_MATRIX10                    1086
#define IDC_RESTR_TO3                   1087
#define IDC_RESTR_TO4                   1088
#define IDC_RESTR_TO5                   1089
#define IDC_RESTR_TO6                   1090
#define IDC_RESTR_TO7                   1091
#define IDC_RESTR_TO8                   1092
#define IDC_RESTR_TO9                   1093
#define IDC_RESTR_TO10                  1094
#define IDS_STRING1459                  1457
#define IDS_STRING1460                  1458
#define IDS_REGN                        1459

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        140
#define _APS_NEXT_COMMAND_VALUE         32772
#define _APS_NEXT_CONTROL_VALUE         1095
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
