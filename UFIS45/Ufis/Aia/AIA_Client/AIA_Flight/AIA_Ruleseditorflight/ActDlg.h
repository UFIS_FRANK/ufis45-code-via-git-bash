#if !defined(AFX_ACTDLG_H__CBE92B92_64CB_11D1_B412_0000B45A33F5__INCLUDED_)
#define AFX_ACTDLG_H__CBE92B92_64CB_11D1_B412_0000B45A33F5__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// ActDlg.h : header file
//

// ActDlg.h : header file
//
#include "CCSGlobl.h"
#include "CedaActData.h"
/////////////////////////////////////////////////////////////////////////////
// ActDlg dialog

class ActDlg : public CDialog
{
// Construction
public:
	ActDlg(CWnd* pParent, char *pspActl);   // standard constructor
	~ActDlg();
	CCSPtrArray<ACTDATA> omActData;
// Dialog Data
	//{{AFX_DATA(ActDlg)
	enum { IDD = IDD_ACT_DLG };
	CListBox	m_List;
	//}}AFX_DATA

	CString omAircraft;
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(ActDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(ActDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnDblclkList1();
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_ACTDLG_H__CBE92B92_64CB_11D1_B412_0000B45A33F5__INCLUDED_)
