// PosTableViewer.cpp: Implementierung der Klasse PosTableViewer.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"



#include "PosRulesViewer.h"
#include <math.h>

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif


const int width = 574;

//////////////////////////////////////////////////////////////////////
// Konstruktion/Destruktion
//////////////////////////////////////////////////////////////////////

PosRulesViewer::PosRulesViewer()
{
	bmNoUpdatesNow = false;
	

}

PosRulesViewer::~PosRulesViewer()
{
	DeleteAll();
}

void PosRulesViewer::Attach(CCSTable *popTable)
{
    pomTable = popTable;
}

#pragma warning( disable : 4100 )  
void PosRulesViewer::ChangeViewTo(const char *pcpViewName)
{
    DeleteAll();    
    MakeLines();

	UpdateDisplay();
}
 

int PosRulesViewer::ComparePos(POSTABLE_LINEDATA *prpPos1, POSTABLE_LINEDATA *prpPos2)
{


     return 0;
}
#pragma warning( default : 4100 )  

/////////////////////////////////////////////////////////////////////////////
// PosRulesViewer -- code specific to this class


void PosRulesViewer::MakeLines()
{
	int ilPosCount = ogPstData.omData.GetSize();
	for(int illc = 0 ; illc < ilPosCount; illc++)
	{		
		PSTDATA * prlPos = &ogPstData.omData[illc];
		MakeLine(prlPos);
		
	}
}

void PosRulesViewer::MakePosLineData(PSTDATA *popPst, CStringArray &ropArray)
{
	CString olReGates;
	POSITION rlPos;
	for ( rlPos = ogGatData.omUrnoMap.GetStartPosition(); rlPos != NULL; )
	{
		long llUrno;
		GATDATA *prlGat;
		ogGatData.omUrnoMap.GetNextAssoc(rlPos, (void *&)llUrno, (void *&)prlGat);
		if(prlGat != NULL)
		{
			if(strcmp(popPst->Pnam, prlGat->Rga2) == 0)
			{
				olReGates+=prlGat->Gnam + CString("  ");
			}
			if(strcmp(popPst->Pnam, prlGat->Rga1) == 0)
			{
				olReGates+=prlGat->Gnam + CString("  ");
			}
		}
	}
	ropArray.Add(olReGates);
	CStringArray olAcgrList;
//	ExtractItemList(popPst->Acgr, &olAcgrList, '/');

	CString olTmpPosr = popPst->Posr;
	if(olTmpPosr.Replace(";","#") < 3)
	{
		olTmpPosr = popPst->Posr;
		olTmpPosr.Replace(";","|");
		strcpy(popPst->Posr, olTmpPosr.GetBuffer(0));
	}
	
	ExtractItemList(popPst->Posr, &olAcgrList, ';');

	for(int i = 0; i < olAcgrList.GetSize(); i++)
	{
		ropArray.Add(olAcgrList[i]);
	}
}


int PosRulesViewer::MakeLine(PSTDATA *prpPos)
{
    // Update viewer data for this shift record
   POSTABLE_LINEDATA rlLine;

	rlLine.Rtnr = omLines.GetSize() +1;
	CStringArray olAcgrList;
	MakePosLineData(prpPos, olAcgrList);
	int ilWordCount = olAcgrList.GetSize();

	CString olT;
	int ilActIdx = 0;
		
	rlLine.Position = prpPos->Pnam;
	
	if(ilWordCount >ilActIdx)
	{
		olT = olAcgrList[ilActIdx];
		rlLine.RelGat = olT;
		ilActIdx++;
	}
	if(ilWordCount >ilActIdx)
	{
		olT = olAcgrList[ilActIdx];
		rlLine.WSpan = olT;
		ilActIdx++;
	}
	if(ilWordCount >ilActIdx)
	{
		olT = olAcgrList[ilActIdx];
		rlLine.Length = olT;
		ilActIdx++;
	}
	if(ilWordCount >ilActIdx)
	{
		olT = olAcgrList[ilActIdx];
		rlLine.Height = olT;
		ilActIdx++;
	}
	if(ilWordCount >ilActIdx)
	{
		olT = olAcgrList[ilActIdx];
		rlLine.ACExL = olT;
		ilActIdx++;
	}
	if(ilWordCount >ilActIdx)
	{
		olT = olAcgrList[ilActIdx];
		rlLine.Pos1 = olT;
		ilActIdx++;
	}

	if(ilWordCount >ilActIdx)
	{
		olT = olAcgrList[ilActIdx];
		rlLine.Max1 = olT;
		ilActIdx++;
	}

	if(ilWordCount >ilActIdx)
	{
		olT = olAcgrList[ilActIdx];
		rlLine.Pos2 = olT;
		ilActIdx++;
	}

	if(ilWordCount >ilActIdx)
	{
		olT = olAcgrList[ilActIdx];
		rlLine.Max2 = olT;
		ilActIdx++;
	}

	if(ilWordCount >ilActIdx)
	{
		olT = olAcgrList[ilActIdx];
		rlLine.M = olT;
		ilActIdx++;
	}

	if(ilWordCount >ilActIdx)
	{
		olT = olAcgrList[ilActIdx];
		rlLine.Prio= olT;
		ilActIdx++;
	}

	if(ilWordCount >ilActIdx)
	{
		olT = olAcgrList[ilActIdx];
		rlLine.Nat= olT;
		ilActIdx++;
	}

	if(ilWordCount >ilActIdx)
	{
		olT = olAcgrList[ilActIdx];
		rlLine.PAL = olT;
		ilActIdx++;
	}


	return(CreateLine(&rlLine));
}


int PosRulesViewer::CreateLine(POSTABLE_LINEDATA *prpPos)
{
    int ilLineCount = omLines.GetSize();

	POSTABLE_LINEDATA rlPos;
	rlPos = *prpPos;
    omLines.NewAt(ilLineCount, rlPos);

    return ilLineCount;
}

void PosRulesViewer::DeleteLine(int ipLineno)
{
	omLines.DeleteAt(ipLineno);
}


BOOL PosRulesViewer::FindLine(long lpRtnr, int &rilLineno)
{
    for (rilLineno = 0; rilLineno < omLines.GetSize(); rilLineno++)
	{
      if (omLines[rilLineno].Rtnr == lpRtnr)
            return TRUE;
	}
    return FALSE;
}



/////////////////////////////////////////////////////////////////////////////
// PosRulesViewer - POSTABLE_LINEDATA array maintenance

void PosRulesViewer::DeleteAll()
{
    while (omLines.GetSize() > 0)
        DeleteLine(0);
}


/////////////////////////////////////////////////////////////////////////////
// PosRulesViewer - display drawing routine


void PosRulesViewer::MakeHeaderData()
{

	TABLE_HEADER_COLUMN rlHeader;
//	int ilColumnNo = -1;

	CStringArray olAcgrList;
	ExtractItemList(LoadStg(IDS_STRING1428), &olAcgrList, ',');
	
	CString olT;
	int ilActIdx = 0;
	int ilWordCount = olAcgrList.GetSize();
	

	// Laufende Nummer
	rlHeader.Alignment = COLALIGN_CENTER;
	rlHeader.Length = 8 * 3; 
	rlHeader.Font = &ogCourier_Bold_8;
	rlHeader.SeparatorType = SEPA_NORMAL;//SEPA_NONE;//SEPA_LIKEVERTICAL

	rlHeader.Text = "";
	
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(),rlHeader);

	// Position
	rlHeader.Length = 8 * 3; 
	rlHeader.Text = "";
	if(ilWordCount >ilActIdx)
	{
		olT = olAcgrList[ilActIdx];
		rlHeader.Text = olT;
		ilActIdx++;
	}

	// Rel Gates
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(),rlHeader);
	rlHeader.Length = 8 * 7; 
	rlHeader.Text = "";
	if(ilWordCount >ilActIdx)
	{
		olT = olAcgrList[ilActIdx];
		rlHeader.Text = olT;
		ilActIdx++;
	}

	// Wing span
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(),rlHeader);
	rlHeader.Length = 8 * 10; 
	rlHeader.Text = "";
	if(ilWordCount >ilActIdx)
	{
		olT = olAcgrList[ilActIdx];
		rlHeader.Text = olT;
		ilActIdx++;
	}

	// Length
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(),rlHeader);
	rlHeader.Length = 8 * 10; 
	rlHeader.Text = "";
	if(ilWordCount >ilActIdx)
	{
		olT = olAcgrList[ilActIdx];
		rlHeader.Text = olT;
		ilActIdx++;
	}

	// Heigth
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(),rlHeader);
	rlHeader.Length = 8 * 8; 
	rlHeader.Text = "";
	if(ilWordCount >ilActIdx)
	{
		olT = olAcgrList[ilActIdx];
		rlHeader.Text = olT;
		ilActIdx++;
	}

	// AC Para
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(),rlHeader);
	rlHeader.Length = 8 * 8; 
	rlHeader.Text = "";
	if(ilWordCount >ilActIdx)
	{
		olT = olAcgrList[ilActIdx];
		rlHeader.Text = olT;
		ilActIdx++;
	}

	// Pos1
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(),rlHeader);
	rlHeader.Length = 8 * 3; 
	rlHeader.Text = "";
	if(ilWordCount >ilActIdx)
	{
		olT = olAcgrList[ilActIdx];
		rlHeader.Text = olT;
		ilActIdx++;
	}
	// Res 1

	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(),rlHeader);
	rlHeader.Length = 8 * 5; 
	rlHeader.Text = "";
	if(ilWordCount >ilActIdx)
	{
		olT = olAcgrList[ilActIdx];
		rlHeader.Text = olT;
		ilActIdx++;
	}
	// Pos2
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(),rlHeader);
	rlHeader.Length = 8 * 3; 
	rlHeader.Text = "";
	if(ilWordCount >ilActIdx)
	{
		olT = olAcgrList[ilActIdx];
		rlHeader.Text = olT;
		ilActIdx++;
	}
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(),rlHeader);

	// Res 2
	rlHeader.Length = 8 * 5; 
	rlHeader.Text = "";
	if(ilWordCount >ilActIdx)
	{
		olT = olAcgrList[ilActIdx];
		rlHeader.Text = olT;
		ilActIdx++;
	}
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(),rlHeader);

	// Max ac
	rlHeader.Length = 8 * 2; 
	rlHeader.Text = "";
	if(ilWordCount >ilActIdx)
	{
		olT = olAcgrList[ilActIdx];
		rlHeader.Text = olT;
		ilActIdx++;
	}
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(),rlHeader);

	// Prio
	rlHeader.Length = 8 * 1; 
	rlHeader.Text = "";
	if(ilWordCount >ilActIdx)
	{
		olT = olAcgrList[ilActIdx];
		rlHeader.Text = olT;
		ilActIdx++;
	}
	// Nat code
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(),rlHeader);
	rlHeader.Length = 8 * 10; 
	rlHeader.Text = "";
	if(ilWordCount >ilActIdx)
	{
		olT = olAcgrList[ilActIdx];
		rlHeader.Text = olT;
		ilActIdx++;
	}
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(),rlHeader);
	rlHeader.Length = 8 * 5; 
	rlHeader.Text = "";
	if(ilWordCount >ilActIdx)
	{
		olT = olAcgrList[ilActIdx];
		rlHeader.Text = olT;
		ilActIdx++;
	}
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(),rlHeader);

	pomTable->SetHeaderFields(omHeaderDataArray);
	omHeaderDataArray.RemoveAll();

}



void PosRulesViewer::MakeColumnData(CCSPtrArray<TABLE_COLUMN> &ropColList,POSTABLE_LINEDATA *prpLine)
{

//	BOOL blNewLogicLine = FALSE;

	int ilColumnNo = 0;
	bool bldefekt = false;

	TABLE_COLUMN rlColumnData;
	rlColumnData.Alignment = COLALIGN_LEFT;
	rlColumnData.Lineno = imTotalLines;//*3)+ilLocalLine;
	rlColumnData.VerticalSeparator = SEPA_NORMAL;
	rlColumnData.SeparatorType = SEPA_NORMAL;
	rlColumnData.Font = &ogCourier_Regular_8;//&ogCourier_Regular_10;
	rlColumnData.BkColor = WHITE;
	rlColumnData.TextColor = BLACK;
	rlColumnData.Text.Format("%d",prpLine->Rtnr);  
	rlColumnData.Columnno = ilColumnNo++;
	ropColList.NewAt(ropColList.GetSize(), rlColumnData);


	rlColumnData.Text = prpLine->Position;
	rlColumnData.Columnno = ilColumnNo++;
	ropColList.NewAt(ropColList.GetSize(), rlColumnData);

	rlColumnData.Text = prpLine->RelGat;
	rlColumnData.Columnno = ilColumnNo++;
	ropColList.NewAt(ropColList.GetSize(), rlColumnData);

	rlColumnData.Text = prpLine->WSpan;
	rlColumnData.Columnno = ilColumnNo++;
	ropColList.NewAt(ropColList.GetSize(), rlColumnData);

	rlColumnData.Text = prpLine->Length;
	rlColumnData.Columnno = ilColumnNo++;
	ropColList.NewAt(ropColList.GetSize(), rlColumnData);

	rlColumnData.Text = prpLine->Height;
	rlColumnData.Columnno = ilColumnNo++;
	ropColList.NewAt(ropColList.GetSize(), rlColumnData);

	rlColumnData.Text = prpLine->ACExL;
	rlColumnData.Columnno = ilColumnNo++;
	ropColList.NewAt(ropColList.GetSize(), rlColumnData);

	rlColumnData.Text = prpLine->Pos1;
	rlColumnData.Columnno = ilColumnNo++;
	ropColList.NewAt(ropColList.GetSize(), rlColumnData);

	rlColumnData.Text = prpLine->Max1;
	rlColumnData.Columnno = ilColumnNo++;
	ropColList.NewAt(ropColList.GetSize(), rlColumnData);

	rlColumnData.Text = prpLine->Pos2;
	rlColumnData.Columnno = ilColumnNo++;
	ropColList.NewAt(ropColList.GetSize(), rlColumnData);

	rlColumnData.Text = prpLine->Max2;
	rlColumnData.Columnno = ilColumnNo++;
	ropColList.NewAt(ropColList.GetSize(), rlColumnData);

	rlColumnData.Text = prpLine->M;
	rlColumnData.Columnno = ilColumnNo++;
	ropColList.NewAt(ropColList.GetSize(), rlColumnData);

	rlColumnData.Text = prpLine->Prio;
	rlColumnData.Columnno = ilColumnNo++;
	ropColList.NewAt(ropColList.GetSize(), rlColumnData);

	rlColumnData.Text = prpLine->Nat;
	rlColumnData.Columnno = ilColumnNo++;
	ropColList.NewAt(ropColList.GetSize(), rlColumnData);

	rlColumnData.Text = prpLine->PAL;
	rlColumnData.Columnno = ilColumnNo++;
	ropColList.NewAt(ropColList.GetSize(), rlColumnData);


}



// UpdateDisplay: Load data selected by filter conditions to the display by using "omTable"
void PosRulesViewer::UpdateDisplay()
{

	imTotalLines = 0;
//	BOOL blNewLogicLine = FALSE;


	pomTable->SetShowSelection(FALSE);
	pomTable->ResetContent();
	//pomTable->SetDockingRange(3);
	
	MakeHeaderData();
	pomTable->SetDefaultSeparator();
//	pomTable->SetSelectMode(0);

	for (int ilLc = 0; ilLc < omLines.GetSize(); ilLc++)
	{
		CCSPtrArray<TABLE_COLUMN> olColList;

		MakeColumnData(olColList,&omLines[ilLc]);
		imTotalLines++;
		pomTable->AddTextLine(olColList, (void*)&(omLines[ilLc]));
		olColList.DeleteAll();
	}
    pomTable->DisplayTable();
}




