// NatureAllocation.cpp : implementation file
//

#include "stdafx.h"
#include "Rules.h"
#include "BasicData.h"
#include "NatureAllocation.h"
#include "resrc1.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// NatureAllocation dialog


NatureAllocation::NatureAllocation(CWnd* pParent, NATDATA *popNat)
	: CDialog(NatureAllocation::IDD, pParent)
{
	pomNat = popNat;
	//{{AFX_DATA_INIT(NatureAllocation)
	m_Alga = FALSE;
	m_Alpo = FALSE;
	m_Albb = FALSE;
	m_Alwr = FALSE;
	//}}AFX_DATA_INIT
	if(CString(pomNat->Alga) == "X")
	{
		m_Alga = TRUE;
	}
	if(CString(pomNat->Alpo) == "X")
	{
		m_Alpo = TRUE;
	}
	if(CString(pomNat->Albb) == "X")
	{
		m_Albb = TRUE;
	}
	if(CString(pomNat->Alwr) == "X")
	{
		m_Alwr = TRUE;
	}
}


void NatureAllocation::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(NatureAllocation)
	DDX_Check(pDX, IDC_ALGA, m_Alga);
	DDX_Check(pDX, IDC_ALPO, m_Alpo);
	DDX_Check(pDX, IDC_ALBB, m_Albb);
	DDX_Check(pDX, IDC_ALWR, m_Alwr);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(NatureAllocation, CDialog)
	//{{AFX_MSG_MAP(NatureAllocation)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// NatureAllocation message handlers

BOOL NatureAllocation::OnInitDialog() 
{
	CDialog::OnInitDialog();

	UpdateData(FALSE);
	CString olText;
	olText.Format(GetString(IDS_STRING1438), pomNat->Ttyp);
	SetWindowText(olText);
	// TODO: Add extra initialization here
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void NatureAllocation::OnOK() 
{
	// TODO: Add extra validation here
	UpdateData(TRUE);
	if(m_Alga == TRUE) 
	{
		strcpy(pomNat->Alga, "X");
		pomNat->IsChanged = DATA_CHANGED;
	}
	else
	{
		strcpy(pomNat->Alga, " ");
		pomNat->IsChanged = DATA_CHANGED;
	}
	if(m_Alpo == TRUE)
	{
		strcpy(pomNat->Alpo, "X");
		pomNat->IsChanged = DATA_CHANGED;
	}
	else
	{
		strcpy(pomNat->Alpo, " ");
		pomNat->IsChanged = DATA_CHANGED;
	}
	if(m_Albb == TRUE)
	{
		strcpy(pomNat->Albb, "X");
		pomNat->IsChanged = DATA_CHANGED;
	}
	else
	{
		strcpy(pomNat->Albb, " ");
		pomNat->IsChanged = DATA_CHANGED;
	}
	if(m_Alwr == TRUE)
	{
		strcpy(pomNat->Alwr, "X");
		pomNat->IsChanged = DATA_CHANGED;
	}
	else
	{
		strcpy(pomNat->Alwr, " ");
		pomNat->IsChanged = DATA_CHANGED;
	}
	ogNatData.SaveNAT(pomNat);

	CDialog::OnOK();
}
