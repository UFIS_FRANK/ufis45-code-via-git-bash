// AcrTableDlg.cpp : implementation file
//

#include "stdafx.h"
#include "CCSGlobl.h"
#include "CCSEdit.h"
#include "AcrTableDlg.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// AcrTableDlg dialog


AcrTableDlg::AcrTableDlg(CWnd* pParent, char *pspRegn /*=NULL*/)
	: CDialog(AcrTableDlg::IDD, pParent)
{
	if(pspRegn != NULL)
	{
		omRegn = CString(pspRegn);
	}
	//{{AFX_DATA_INIT(AcrTableDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	if(!omRegn.IsEmpty())
	{
		char pclWhere[100];
		sprintf(pclWhere, " WHERE REGN LIKE '%s%%'", omRegn);
		ogAcrData.ReadAllACRs(pclWhere);
	}
	if(ogAcrData.omData.GetSize() == 0)
	{
		ogAcrData.ReadAllACRs();
	}
}


void AcrTableDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(AcrTableDlg)
	DDX_Control(pDX, IDC_REGLIST, m_List);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(AcrTableDlg, CDialog)
	//{{AFX_MSG_MAP(AcrTableDlg)
	ON_LBN_DBLCLK(IDC_REGLIST, OnDblclkReglist)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// AcrTableDlg message handlers

BOOL AcrTableDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	char pclList[500];
	bool blFound = false;
	m_List.SetFont(&ogCourier_Regular_8);

/*	if(!omRegn.IsEmpty())
	{
		int ilCount = ogAcrData.omData.GetSize();
		for(int i = 0; i < ilCount; i++)
		{
			if(strcmp(ogAcrData.omData[i].Regi, omRegn) == 0)
			{
				sprintf(pclList, "%-7s  %-3s  %-3s  %-1s  %-1s", 
						 ogAcrData.omData[i].Regi, ogAcrData.omData[i].Actl, ogAcrData.omData[i].Owne, 
						  ogAcrData.omData[i].Apui, ogAcrData.omData[i].Acmd);
				m_List.AddString(pclList);
				m_List.SetItemDataPtr( i, (void*)&ogAcrData.omData[i]);
				blFound = true;
				break;
			}
		}
	}

	if(blFound == false)
	{
*/		int ilCount = ogAcrData.omData.GetSize();
		for(int i = 0; i < ilCount; i++)
		{
			sprintf(pclList, "%-12s  %-3s  %-3s  %-1s  ", 
					 ogAcrData.omData[i].Regn, ogAcrData.omData[i].Act3, ogAcrData.omData[i].Owne, 
					  ogAcrData.omData[i].Apui/*, ogAcrData.omData[i].Acmd*/);
			m_List.AddString(pclList);
			m_List.SetItemDataPtr( i, (void*)&ogAcrData.omData[i]);
		}
//	}	
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
	return TRUE;
}

void AcrTableDlg::OnOK() 
{
	
	int ilIndex = m_List.GetCurSel();
	ACRDATA *prlAcr;
	if(ilIndex != LB_ERR)
	{
		prlAcr = (ACRDATA *)m_List.GetItemDataPtr(ilIndex);
		omRegn = prlAcr->Regn;//prlAltCString(ogAltData.omData[ilIndex].Alc2);
	}
	CDialog::OnOK();
}

void AcrTableDlg::OnDblclkReglist() 
{
	OnOK();
}
