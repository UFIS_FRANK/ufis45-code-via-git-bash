// CedaBLTData.h

#ifndef __CEDABLTDATA__
#define __CEDABLTDATA__
 
#include "stdafx.h"
#include "basicdata.h"

//---------------------------------------------------------------------------------------------------------
// Record structure declaration

//@See: CedaBLTData
//@Doc:	A structure for reading Duty data. We read all data from database and put them
//		into this struct and store the data in omData.  

//---------------------------------------------------------------------------------------------------------
struct BLTDATA 
{
	char 	 Bnam[7]; 	// Gep�ckband Name
	CTime	 Cdat; 		// Erstellungsdatum
	CTime	 Lstu; 		// Datum letzte �nderung
	CTime	 Nafr; 		// Nicht verf�gbar von
	CTime	 Nato; 		// Nicht verf�gbar bis
	char 	 Resn[42];	// Grund der Sperrung
	char 	 Prfl[3]; 	// Protokollierungskennzeichen
	char 	 Tele[12]; 	// Telefonnummer
	long 	 Urno; 		// Eindeutige Datensatz-Nr.
	char 	 Usec[34]; 	// Anwender (Ersteller)
	char 	 Useu[34]; 	// Anwender (letzte �nderung)
	CTime	 Vafr; 		// G�ltig von
	CTime	 Vato; 		// G�ltig bis

	//DataCreated by this class
	int      IsChanged;

	BLTDATA(void)
	{ memset(this,'\0',sizeof(*this));
	 Cdat=-1;Lstu=-1;Nafr=-1;Nato=-1;Vafr=-1;Vato=-1;
	}

}; // end BLTDataStrukt


//---------------------------------------------------------------------------------------------------------
// Class declaratino

//@Memo: Read and write BLT(duty) data
//@See: CedaData, CedaDutyData
/*@Doc:
  Reads and writes BLT(duty) data from and to database. Stores BLT data in memory and
  provides methods for searching and retrieving BLTs from its internal list. Does
  some data preparations.

  Data of this class has to be updated by CEDA through broadcasts. This procedure and the
  necessary methods will be described later with the description of broadcast handling.

  {\bf CedaBLTData} handles data from the server process. 
  See the specification for table descriptions.*/

class CedaBLTData: public CCSCedaData
{
// Attributes
public:
    //@ManMemo: A map, containing the Urno field of all loaded BLTs.
    CMapPtrToPtr omUrnoMap;
	CMapStringToPtr omBnamMap;

    //@ManMemo: BLTDATA records read by ReadAllBLT().
    CCSPtrArray<BLTDATA> omData;

	void SetTableName(CString opTableName)
	{
		strcpy(pcmTableName, opTableName.GetBuffer(0));
	}
// Operations
public:
    //@ManMemo: Default constructor
    /*@Doc:
      Initialize {\bf omRecInfo} for offsets and types for each fields in {\bf BLTDATA},
      the {\bf pcmTableName} with table name {\bf BLTLSG}, the data members {\bf pcmFieldList}
      contains a list of used fields of {\bf BLTDATA}.
    */
    CedaBLTData();
	~CedaBLTData();
	void Register(void);
	void ClearAll(void);

    //@ManMemo: Read all BLTs.
    /*@Doc:
      Read all BLTs. 

      This method will clear {\bf omData} and {\bf omKeyMap}. Convert each record
      to a BLTDATA and store it in {\bf omData} and the Urno field in {\bf omKeyMap}.
    */
    bool ReadAllBLTs();
    //@AreMemo: Read all BLT-Data corresponding to Where-condition
	bool ReadSpecial(CCSPtrArray<BLTDATA> &ropBlt,char *pspWhere);
    //@ManMemo: Add a new BLT and store the new data to the Database.
	bool InsertBLT(BLTDATA *prpBLT,BOOL bpSendDdx = TRUE);
    //@ManMemo: Add data for a new BLT to the internal data.
	bool InsertBLTInternal(BLTDATA *prpBLT);
	//@ManMemo: Change the data of a special BLT
	bool UpdateBLT(BLTDATA *prpBLT,BOOL bpSendDdx = TRUE);
    //@AreMemo: Change the data of a special BLT in the internal data.
	bool UpdateBLTInternal(BLTDATA *prpBLT);
    //@ManMemo: Delete a special BLT, also in the Database.
	bool DeleteBLT(long lpUrno);
    //@ManMemo: Delete a special BLT only in the internal data.
	bool DeleteBLTInternal(BLTDATA *prpBLT);
    //@AreMemo: Selects all BLTs with a special Urno.
	BLTDATA  *GetBLTByUrno(long lpUrno);
    //@ManMemo: Search a BLT with a special Key.
	CString BLTExists(BLTDATA *prpBLT);
    //@AreMemo: Search a Bnam
	bool ExistBnam(char* popBnam);
	//@ManMemo: Writes a special BLT to the Database.
	bool SaveBLT(BLTDATA *prpBLT);
    //@ManMemo: Contains the valid data for all existing BLTs.
	char pcmBLTFieldList[2048];
    //@ManMemo: Handle Broadcasts for BLTs.
	void ProcessBLTBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

	// Private methods
private:
    void PrepareBLTData(BLTDATA *prpBLTData);
};


extern CedaBLTData ogBltData;

//---------------------------------------------------------------------------------------------------------
#endif //__CEDABLTDATA__
