// PremisPage.cpp : implementation file
//

#include "stdafx.h"
#include "Rules.h"
#include "CCSGlobl.h"
#include "PremisPage.h"
#include "PremisAwDlg.h"
#include "DutyPreferences.h"
#include "CedaGrnData.h"
#include "CedaGhsData.h"
#include "AcrTableDlg.h"
#include "ActDlg.h"
#include "PrcList.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

extern PremisAwDlg *pogPremisAwDlg;
extern DutyPreferences *pogDutyPreferences;
GhsList *pogGhsList;

static int ComparePrcData(const PRCDATA **e1, const PRCDATA **e2);
static int CompareToPrco(const GHPDATA **e1, const GHPDATA **e2);
static int ComparePerData(const PERDATA **e1, const PERDATA **e2);
static int ComparePfcData(const PFCDATA **e1, const PFCDATA **e2);
static int CompareGegData(const GEGDATA **e1, const GEGDATA **e2);
static int CompareGroup(const GRNDATA **e1, const GRNDATA **e2);
static int CompareGpm(const GPMDATA **e1, const GPMDATA **e2);
/////////////////////////////////////////////////////////////////////////////
// PremisPage property page

IMPLEMENT_DYNCREATE(PremisPage, CPropertyPage)

PremisPage::PremisPage() : CPropertyPage(PremisPage::IDD)
{
	//{{AFX_DATA_INIT(PremisPage)
	m_CBDtyp_Idx = -1;
	//}}AFX_DATA_INIT
	pomCurrentGhp = new GHPDATA;
	pomGantt = NULL;
	m_m_BAwPrco = new CCSButtonCtrl(true);
	pomCurrentGhp->Urno = ogBasicData.GetNextUrno();
	pomCurrentGhp->IsChanged = DATA_NEW;
	pomBkGhp = NULL;
	pogGhsList = NULL;
	pomCurrentGpm = NULL;
	pomViewer = new PremisPageDetailViewer(0);
	pomViewer->SetCurrentRkey(pomCurrentGhp->Urno);
	imVerticalScaleIndent = 150;
	imMode = NEW_MODE;	
	pomTable = NULL;	
	bmBarChanged = false;
	imLastSelectedUrno = -1;
	pomStdTable = new CCSTable;
    pomStdTable->tempFlag = 2;
	pomSonderTable = new CCSTable;
    pomSonderTable->tempFlag = 2;

}

PremisPage::~PremisPage()
{
	if(pomStdTable != NULL)
	{
		delete pomStdTable;
	}
	if(pomSonderTable != NULL)
	{
		delete pomSonderTable;
	}
	delete pomViewer;
	delete m_m_BAwPrco;
	if(pomGantt != NULL)
	{
		pomGantt->DestroyWindow();
		delete pomGantt;
	}
	if(pomTable != NULL)
	{
		delete pomTable;
	}
	if(pogGhsList != NULL)
	{
		pogGhsList->DestroyWindow();
	}
	delete pomCurrentGhp;
	if(pomBkGhp != NULL)
	{
		delete pomBkGhp;
	}
	omLines.DeleteAll();
	omOldGpmList.DeleteAll();
	omGpmData.DeleteAll();

	omPerData.DeleteAll();
	omGegData.DeleteAll();
	omPfcData.DeleteAll();
	omPrcData.DeleteAll();

	omCicGrpData.DeleteAll();
	omTrraData.DeleteAll();
	omTrrdData.DeleteAll();
	omPcaaData.DeleteAll();
	omPcadData.DeleteAll();
	omAtmData.DeleteAll();
	omAlmaData.DeleteAll();
	omAlmdData.DeleteAll();
	omTtgaData.DeleteAll();
	omTtgdData.DeleteAll();
	omHtgaData.DeleteAll();
	omHtgdData.DeleteAll();
	omStdData.DeleteAll();
	omZusatzData.DeleteAll();

}

void PremisPage::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(PremisPage)
	DDX_Control(pDX, IDC_HTGD, m_Htgd);
	DDX_Control(pDX, IDC_HTGA, m_Htga);
	DDX_Control(pDX, IDC_TTGD, m_Ttgd);
	DDX_Control(pDX, IDC_TTGA, m_Ttga);
	DDX_Control(pDX, IDC_SSIC_FRAME, m_ScciFrame);
	DDX_Control(pDX, IDC_OSDU_FRAME, m_OsduFrame);
//	DDX_Control(pDX, IDC_PRCO, m_Prco);
	DDX_Control(pDX, IDC_PCAD, m_PCAD);
	DDX_Control(pDX, IDC_PCAA, m_PCAA);
	DDX_Control(pDX, IDC_TRRD, m_TRRD);
	DDX_Control(pDX, IDC_TRRA, m_TRRA);
	DDX_Control(pDX, IDC_ALMD, m_ALMD);
	DDX_Control(pDX, IDC_ALMA, m_ALMA);
	DDX_Control(pDX, IDC_RES_GRP, m_Resource);
	DDX_Control(pDX, IDC_PERM, m_Perm);
	DDX_Control(pDX, IDC_CHGR, m_Chgr);
	DDX_Control(pDX, IDC_AW_REGN, m_BAwRegn);
	DDX_Control(pDX, IDC_AW_ACT3, m_m_BAwAct5);
	DDX_Control(pDX, IDC_AW_ACT_D, m_m_BAwActD);
	DDX_Control(pDX, IDC_AW_ACT_A, m_BAwActA);
	DDX_Control(pDX, IDC_STATUSBAR, m_Statusbar);
	DDX_Control(pDX, IDC_REMA, m_Rema);
	DDX_Control(pDX, IDC_LSTU, m_Lstu);
	DDX_Control(pDX, IDC_LKCO_PERM_VRGC, m_LkcoPerm);
	DDX_Control(pDX, IDC_DUCH, m_Duch);
	DDX_Control(pDX, IDC_DTYP, m_Dtyp);
	DDX_Control(pDX, IDC_DOPD7, m_Dopd7);
	DDX_Control(pDX, IDC_DOPD6, m_Dopd6);
	DDX_Control(pDX, IDC_DOPD5, m_Dopd5);
	DDX_Control(pDX, IDC_DOPD4, m_Dopd4);
	DDX_Control(pDX, IDC_DOPD3, m_Dopd3);
	DDX_Control(pDX, IDC_DOPD2, m_Dopd2);
	DDX_Control(pDX, IDC_DOPD1, m_Dopd1);
	DDX_Control(pDX, IDC_DOPD_ALL, m_Dopd_All);
	DDX_Control(pDX, IDC_DOPA7, m_Dopa7);
	DDX_Control(pDX, IDC_DOPA6, m_Dopa6);
	DDX_Control(pDX, IDC_DOPA5, m_Dopa5);
	DDX_Control(pDX, IDC_DOPA4, m_Dopa4);
	DDX_Control(pDX, IDC_DOPA3, m_Dopa3);
	DDX_Control(pDX, IDC_DOPA2, m_Dopa2);
	DDX_Control(pDX, IDC_DOPA1, m_Dopa1);
	DDX_Control(pDX, IDC_DOPA_ALL, m_Dopa_All);
	DDX_Control(pDX, IDC_ACTM, m_Actm);
	DDX_Control(pDX, IDC_ACT3, m_Act5);
	DDX_Control(pDX, IDC_DEST, m_Dest);
	DDX_CBIndex(pDX, IDC_DTYP, m_CBDtyp_Idx);
	DDX_Control(pDX, IDC_FLCA, m_Flca);
	DDX_Control(pDX, IDC_FLCD, m_Flcd);
	DDX_Control(pDX, IDC_FLNA, m_Flna);
	DDX_Control(pDX, IDC_FLND, m_Flnd);
	DDX_Control(pDX, IDC_FLSA, m_Flsa);
	DDX_Control(pDX, IDC_FLSD, m_Flsd);
	DDX_Control(pDX, IDC_HTPA, m_Htpa);
	DDX_Control(pDX, IDC_HTPD, m_Htpd);
	DDX_Control(pDX, IDC_ORIG, m_Orig);
	DDX_Control(pDX, IDC_PRNA, m_Prna);
	DDX_Control(pDX, IDC_PRSN, m_Prsn);
	DDX_Control(pDX, IDC_REGN, m_Regn);
	DDX_Control(pDX, IDC_SCCA, m_Scca);
	DDX_Control(pDX, IDC_SCCD, m_Sccd);
	DDX_Control(pDX, IDC_TTPA, m_Ttpa);
	DDX_Control(pDX, IDC_TTPD, m_Ttpd);
	DDX_Control(pDX, IDC_VI1A, m_Vi1a);
	DDX_Control(pDX, IDC_VI1D, m_Vi1d);
	DDX_Control(pDX, IDC_VPFR_DATE, m_Vpfr_Date);
	DDX_Control(pDX, IDC_VPFR_TIME, m_Vpfr_Time);
	DDX_Control(pDX, IDC_VPTO_DATE, m_Vpto_Date);
	DDX_Control(pDX, IDC_VPTO_TIME, m_Vpto_Time);
	DDX_Control(pDX, IDC_PRTA, m_Prta);
	DDX_Control(pDX, IDC_TFDU, m_Tfdu);
	DDX_Control(pDX, IDC_TTDU, m_Ttdu);
	DDX_Control(pDX, IDC_DTIM, m_Dtim);
	DDX_Control(pDX, IDC_POTA, m_Pota);
	DDX_Control(pDX, IDC_NOMA, m_Noma);
	DDX_Control(pDX, IDC_ETOT, m_Etot);
	DDX_Control(pDX, IDC_PRIO, m_Prio);
	DDX_Control(pDX, IDC_DIDU, m_Didu);
	DDX_Control(pDX, IDC_MAXT, m_Maxt);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(PremisPage, CPropertyPage)
	//{{AFX_MSG_MAP(PremisPage)
	ON_WM_CTLCOLOR()
	ON_WM_PAINT()
    ON_MESSAGE(WM_UPDATEDIAGRAM,		OnUpdateDiagram)
	ON_BN_CLICKED(IDC_AW_PRCO, OnAwPrco)
//	ON_EN_KILLFOCUS(IDC_PRCO, OnKillfocusPrco)
	ON_EN_KILLFOCUS(IDC_PRNA, OnKillfocusPrna)
	ON_EN_KILLFOCUS(IDC_PRSN, OnKillfocusPrsn)
	ON_EN_KILLFOCUS(IDC_VPFR_DATE, OnKillfocusVpfrDate)
	ON_EN_KILLFOCUS(IDC_VPFR_TIME, OnKillfocusVpfrTime)
	ON_EN_KILLFOCUS(IDC_VPTO_DATE, OnKillfocusVptoDate)
	ON_EN_KILLFOCUS(IDC_VPTO_TIME, OnKillfocusVptoTime)
	ON_CBN_SELCHANGE(IDC_DTYP, OnSelchangeDtyp)
	ON_EN_KILLFOCUS(IDC_DUCH, OnKillfocusDuch)
	ON_BN_CLICKED(IDC_AW_REGN, OnAwRegn)
	ON_BN_CLICKED(IDC_AW_ACT3, OnAwAct5)
	ON_BN_CLICKED(IDC_DOPA_ALL, OnDopaAll)
	ON_BN_CLICKED(IDC_DOPD_ALL, OnDopdAll)
	ON_EN_KILLFOCUS(IDC_FLCA, OnKillfocusFlca)
	ON_EN_KILLFOCUS(IDC_FLNA, OnKillfocusFlna)
	ON_EN_KILLFOCUS(IDC_FLSA, OnKillfocusFlsa)
	ON_EN_KILLFOCUS(IDC_ALMA, OnKillfocusAlma)
	ON_EN_KILLFOCUS(IDC_ORIG, OnKillfocusOrig)
	ON_EN_KILLFOCUS(IDC_VI1A, OnKillfocusVi1a)
	ON_EN_KILLFOCUS(IDC_TRRA, OnKillfocusTrra)
	ON_EN_KILLFOCUS(IDC_SCCA, OnKillfocusScca)
	ON_EN_KILLFOCUS(IDC_PCAA, OnKillfocusPcaa)
	ON_EN_KILLFOCUS(IDC_TTPA, OnKillfocusTtpa)
	ON_EN_KILLFOCUS(IDC_HTPA, OnKillfocusHtpa)
	ON_EN_KILLFOCUS(IDC_FLCD, OnKillfocusFlcd)
	ON_EN_KILLFOCUS(IDC_FLND, OnKillfocusFlnd)
	ON_EN_KILLFOCUS(IDC_FLSD, OnKillfocusFlsd)
	ON_EN_KILLFOCUS(IDC_ALMD, OnKillfocusAlmd)
	ON_EN_KILLFOCUS(IDC_DEST, OnKillfocusDest)
	ON_EN_KILLFOCUS(IDC_VI1D, OnKillfocusVi1d)
	ON_EN_KILLFOCUS(IDC_TRRD, OnKillfocusTrrd)
	ON_EN_KILLFOCUS(IDC_SCCD, OnKillfocusSccd)
	ON_EN_KILLFOCUS(IDC_PCAD, OnKillfocusPcad)
	ON_EN_KILLFOCUS(IDC_TTPD, OnKillfocusTtpd)
	ON_EN_KILLFOCUS(IDC_HTPD, OnKillfocusHtpd)
	ON_LBN_SELCHANGE(IDC_LKCO_PERM_VRGC, OnSelchangeLkcoPermVrgc)
	ON_LBN_SELCHANGE(IDC_PERM, OnSelchangePerm)
	ON_CBN_SELCHANGE(IDC_ALMA, OnSelchangeAlma)
	ON_CBN_SELCHANGE(IDC_ACTM, OnSelchangeActm)
	ON_CBN_SELCHANGE(IDC_ALMD, OnSelchangeAlmd)
	ON_CBN_SELCHANGE(IDC_TRRA, OnSelchangeTrra)
	ON_CBN_SELCHANGE(IDC_TRRD, OnSelchangeTrrd)
	ON_CBN_SELCHANGE(IDC_PCAA, OnSelchangePcaa)
	ON_CBN_SELCHANGE(IDC_PCAD, OnSelchangePcad)
	ON_BN_CLICKED(IDC_DOPA2, OnDopa2)
	ON_BN_CLICKED(IDC_DOPA1, OnDopa1)
	ON_BN_CLICKED(IDC_DOPA3, OnDopa3)
	ON_BN_CLICKED(IDC_DOPA4, OnDopa4)
	ON_BN_CLICKED(IDC_DOPA5, OnDopa5)
	ON_BN_CLICKED(IDC_DOPA6, OnDopa6)
	ON_BN_CLICKED(IDC_DOPA7, OnDopa7)
	ON_BN_CLICKED(IDC_DOPD1, OnDopd1)
	ON_BN_CLICKED(IDC_DOPD2, OnDopd2)
	ON_BN_CLICKED(IDC_DOPD3, OnDopd3)
	ON_BN_CLICKED(IDC_DOPD4, OnDopd4)
	ON_BN_CLICKED(IDC_DOPD5, OnDopd5)
	ON_BN_CLICKED(IDC_DOPD6, OnDopd6) 
	ON_BN_CLICKED(IDC_DOPD7, OnDopd7)
	ON_EN_CHANGE(IDC_MAXT, OnChangeMaxt)
	ON_EN_CHANGE(IDC_PRSN, OnChangePrsn)
	ON_EN_CHANGE(IDC_PRNA, OnChangePrna)
	ON_EN_CHANGE(IDC_VPFR_DATE, OnChangeVpfrDate)
	ON_EN_CHANGE(IDC_VPFR_TIME, OnChangeVpfrTime)
	ON_EN_CHANGE(IDC_VPTO_DATE, OnChangeVptoDate)
	ON_EN_CHANGE(IDC_VPTO_TIME, OnChangeVptoTime)
//	ON_CBN_SELCHANGE(IDC_PRCO, OnSelchangePrco)
	ON_MESSAGE(WM_DRAGOVER, OnDragOver)
	ON_MESSAGE(WM_DROP, OnDrop)
    ON_MESSAGE(WM_TABLE_LBUTTONDOWN, OnTableLButtonDown)
	ON_MESSAGE(WM_TABLE_RBUTTONDOWN, OnTableRButtonDown)
    ON_MESSAGE(WM_TABLE_DRAGBEGIN, OnTableDragBegin)
	ON_COMMAND(44, OnMenuServiceDelete)
	ON_MESSAGE(CLK_BUTTON_SAVE,			OnSaveButton)
	ON_MESSAGE(CLK_BUTTON_NEW,			OnNewButton)
	ON_MESSAGE(CLK_BUTTON_UPDATE,		OnUpdateButton)
	ON_MESSAGE(CLK_BUTTON_DELETE,		OnDeleteButton)
	ON_MESSAGE(CLK_BUTTON_COPY,			OnCopyButton)
	ON_MESSAGE(CLK_BUTTON_STANDARD,		OnStandardButton)
	ON_MESSAGE(CLK_BUTTON_ADDITIONAL,	OnAdditionalButton)
	ON_MESSAGE(WM_TABLE_LBUTTONDBLCLK,  OnTableLButtonDblClk)
	ON_MESSAGE(WM_EDIT_KILLFOCUS, OnCCSEditKillFocus)
	ON_MESSAGE(BAR_CHANGED, OnBarChanged)
	ON_MESSAGE(GHS_BAR_DELETED, OnBarDeleted)
	ON_CBN_SELCHANGE(IDC_TTGA, OnSelchangeTtga)
	ON_CBN_SELCHANGE(IDC_TTGD, OnSelchangeTtgd)
	ON_CBN_SELCHANGE(IDC_HTGA, OnSelchangeHtga)
	ON_CBN_SELCHANGE(IDC_HTGD, OnSelchangeHtgd)
	ON_WM_CHAR()
	ON_WM_KEYDOWN()

	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// PremisPage message handlers

BOOL PremisPage::OnInitDialog() 
{
	CPropertyPage::OnInitDialog();
	m_DragDropTarget.RegisterTarget(this, this);
	
	CWnd *polWnd = GetDlgItem(IDC_GRP_ROTATION);
	if(polWnd != NULL)
	{
		polWnd->SetFont(&ogMSSansSerif_Bold_8);
	}
	polWnd = GetDlgItem(IDC_GRP_ARRIVAL);
	if(polWnd != NULL)
	{
		polWnd->SetFont(&ogMSSansSerif_Bold_8);
	}
	polWnd = GetDlgItem(IDC_GRP_DEPARTURE);
	if(polWnd != NULL)
	{
		polWnd->SetFont(&ogMSSansSerif_Bold_8);
	}
	polWnd = GetDlgItem(IDC_GRP_DUTYDATA);
	if(polWnd != NULL)
	{
		polWnd->SetFont(&ogMSSansSerif_Bold_8);
	}
	//m_SAddStandard.SetBkColor(GREEN);
	// TODO: Add extra initialization here
	OnSetActive();
//Set Formatting and Colors for all Contols

//	m_Prco.SetFont(&ogCourier_Bold_10/*ogCourier_Bold_8*/);

	int ilC2 = ogPrcData.omData.GetSize();
	for(int i = 0; i < ilC2; i++)
	{
		omPrcData.NewAt(omPrcData.GetSize(), ogPrcData.omData[i]);
	}
/*	omPrcData.Sort(ComparePrcData);
	for(i = 0; i < ilC2; i++)
	{
		char pclText[20]="";
		sprintf(pclText, "%-5s %02ld", omPrcData[i].Redc, omPrcData[i].Redl);
		m_Prco.AddString(pclText);
	}
*/
	SetControls();


//Prepare Gantt Chart, TimeScale and Viewer
	CRect olTSRect;
	GetClientRect(&olTSRect);
	olTSRect.top = 347;
	olTSRect.bottom = 365;
	olTSRect.left += 161;
	olTSRect.right -= 10;
    pomViewer->Attach(this);
    pomViewer->ChangeViewTo(0);
    omTimeScale.Create(NULL, "TimeScale", WS_CHILD | WS_VISIBLE,
        CRect(olTSRect.left, olTSRect.top, olTSRect.right, olTSRect.bottom),
        this, 0, NULL);
	CTime olTSStartTime = CTime(1980, 1, 1, 0, 0, 0);
    CTimeSpan olTSDuration = CTimeSpan(0, 7, 0, 0);
    CTimeSpan olTSInterval = CTimeSpan(0, 0, 10, 0);

    omTimeScale.SetDisplayTimeFrame(olTSStartTime, olTSDuration, olTSInterval);
	//MakeNewGantt();

	pomGantt = new PremisPageGantt;
    pomGantt->SetTimeScale(&omTimeScale);
    pomGantt->SetViewer(pomViewer);
	pomGantt->SetStatusBar(&m_Statusbar);
	CTimeSpan dur = omTimeScale.GetDisplayDuration();
    pomGantt->SetDisplayWindow(olTSStartTime, olTSStartTime + omTimeScale.GetDisplayDuration());
    pomGantt->SetVerticalScaleWidth(150);
	pomGantt->SetVerticalScaleIndent(imVerticalScaleIndent);
	pomGantt->SetVerticalScaleColors(BLACK, SILVER, AQUA, SILVER);
    pomGantt->SetFonts(3, 3);
    //pomGantt->SetVerticalScaleColors(NAVY, SILVER, YELLOW, SILVER);
	pomGantt->SetGanttChartColors(NAVY, SILVER, YELLOW, SILVER);
    pomGantt->Create(WS_BORDER, CRect(olTSRect.left-imVerticalScaleIndent+1, olTSRect.bottom + 1, olTSRect.right, olTSRect.bottom+200), (CWnd*)this);
	pomGantt->AttachWindow(this);

	omTimeScale.ShowWindow(SW_HIDE);

	//LoadPfcData();
	//LoadPerData();
	ogPfcData.ReadSpecialData(&omPfcData,"","URNO,FCTC",true);
	ogGegData.ReadSpecialData(&omGegData,"","URNO,GCDE",true);
	int inCCC = omGegData.GetSize();
	ogPerData.ReadSpecialData(&omPerData,"","URNO,PRMC",true);

	CRect rect;

	
	m_ScciFrame.GetWindowRect(&rect);
    //rect.InflateRect(1, 1);     // hiding the CTable window border
	ScreenToClient(rect);
    pomSonderTable->SetTableData(this, rect.left, rect.right, rect.top, rect.bottom, ::GetSysColor(COLOR_WINDOWTEXT), ::GetSysColor(COLOR_WINDOW), ::GetSysColor(COLOR_HIGHLIGHTTEXT), ::GetSysColor(COLOR_HIGHLIGHT), &ogMSSansSerif_Regular_8);

	m_OsduFrame.GetWindowRect(&rect);
    //rect.InflateRect(1, 1);     // hiding the CTable window border
	ScreenToClient(rect);
    pomStdTable->SetTableData(this, rect.left, rect.right, rect.top, rect.bottom, ::GetSysColor(COLOR_WINDOWTEXT), ::GetSysColor(COLOR_WINDOW), ::GetSysColor(COLOR_HIGHLIGHTTEXT), ::GetSysColor(COLOR_HIGHLIGHT), &ogMSSansSerif_Regular_8);

	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));		
	ShowSpecialTables();
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

HBRUSH PremisPage::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor) 
{
	
	HBRUSH hbr = 0;
	long llRc;

//	m_NCol = RGB(0,255,0);
	CWnd *polWnd;// = GetDlgItem(IDC_ADD_STANDARD);
	polWnd = GetDlgItem(IDC_GRP_ROTATION);
	if (polWnd->m_hWnd == pWnd->m_hWnd)
	{
		llRc = pDC->SetBkColor(SILVER);
		llRc = pDC->SetTextColor(PURPLE);
		hbr = CreateSolidBrush(SILVER);
		return hbr;
	}
	polWnd = GetDlgItem(IDC_GRP_ARRIVAL);
	if (polWnd->m_hWnd == pWnd->m_hWnd)
	{
		llRc = pDC->SetBkColor(SILVER);
		llRc = pDC->SetTextColor(PURPLE);
		hbr = CreateSolidBrush(SILVER);
		return hbr;
	}
	polWnd = GetDlgItem(IDC_GRP_DEPARTURE);
	if (polWnd->m_hWnd == pWnd->m_hWnd)
	{
		llRc = pDC->SetBkColor(SILVER);
		llRc = pDC->SetTextColor(PURPLE);
		hbr = CreateSolidBrush(SILVER);
		return hbr;
	}
	polWnd = GetDlgItem(IDC_GRP_DUTYDATA);
	if (polWnd->m_hWnd == pWnd->m_hWnd)
	{
		llRc = pDC->SetBkColor(SILVER);
		llRc = pDC->SetTextColor(PURPLE);
		hbr = CreateSolidBrush(SILVER);
		return hbr;
	}
/*	polWnd = GetDlgItem(IDC_PRCO);
	if (polWnd->m_hWnd == pWnd->m_hWnd)
	{
		llRc = pDC->SetBkColor(LTYELLOW);
		llRc = pDC->SetTextColor(BLACK);
		hbr = CreateSolidBrush(LTYELLOW);
		return hbr;
	}
*/	
	return CDialog::OnCtlColor(pDC,pWnd,nCtlColor);
}


void PremisPage::OnClose()
{
	int i=4711;
}

void PremisPage::OnOK()
{
	int i=4711;
}

LONG PremisPage::OnDrop(UINT wParam, LONG lParam)
{
	bool blIsSonder = false;
	bool blIsStandard = false;
	CPoint olDropPosition;
    ::GetCursorPos(&olDropPosition);
	CRect olRect1;
	CRect olRect2;
	pomStdTable->GetWindowRect(olRect1);
	pomSonderTable->GetWindowRect(olRect2);
	if(olRect1.PtInRect(olDropPosition) == TRUE)
		blIsStandard = true;
	if(olRect2.PtInRect(olDropPosition) == TRUE)
		blIsSonder = true;


	CCSDragDropCtrl *polDragDropCtrl = &m_DragDropTarget;
	CDWordArray olGhsUrnos;
	for (int ilIndex = 0; ilIndex < polDragDropCtrl->GetDataCount(); ilIndex++)
	{
		GHSDATA *prlGhs = ogGhsData.GetGhsByUrno(polDragDropCtrl->GetDataDWord(ilIndex));
		if(prlGhs != NULL)
		{
			olGhsUrnos.Add(polDragDropCtrl->GetDataDWord(ilIndex));
		}
	}

	if(olGhsUrnos.GetSize() > 0)
	{
		GHPDATA *prlGhp = pomCurrentGhp;
		GHSDATA *prlGhs = ogGhsData.GetGhsByUrno(olGhsUrnos[0]);
		if(blIsSonder == true)
		{
			CreateGpmFromGhsList(this, olGhsUrnos, pomCurrentGhp, CString("Z"));
		}
		if(blIsStandard == true)
		{
			CreateGpmFromGhsList(this, olGhsUrnos, pomCurrentGhp, CString("S"));
		}
		SendMessage(BAR_CHANGED);
		ShowSpecialTables();
	}

	return 0L; 
}
LONG PremisPage::OnDragOver(UINT, LONG)
{
	CPoint olDropPosition;
    ::GetCursorPos(&olDropPosition);
	CRect olRect1;
	CRect olRect2;
	pomStdTable->GetWindowRect(olRect1);
	pomSonderTable->GetWindowRect(olRect2);
	int ilClass = m_DragDropTarget.GetDataClass(); 
	if (ilClass == DIT_GHSLIST)
	{
		if(olRect1.PtInRect(olDropPosition) == TRUE)
			return 0;
		if(olRect2.PtInRect(olDropPosition) == TRUE)
			return 0;
	}
	return -1;
}


void PremisPage::OnMenuServiceDelete()
{
	if(pomCurrentGpm != NULL)
	{
		GPMDATA *prlGpm = ogGpmData.GetGpmByUrno(pomCurrentGpm->Urno);
		if(prlGpm != NULL)
		{
			ogGpmData.Delete(prlGpm, false);
			ogGpmData.DeleteInternal(prlGpm, false);
			SendMessage(GHS_BAR_DELETED);
			ShowSpecialTables();
		}
	}
	
}
LONG PremisPage::OnTableDragBegin(UINT wParam, LONG lParam)
{
	CCSTABLENOTIFY *polNotify;
	polNotify = (CCSTABLENOTIFY*)lParam;
	if(pomCurrentGhp != NULL )
	{
		if(polNotify->SourceTable == (CWnd*)pomSonderTable)
		{
			if(omZusatzData.GetSize() == 0)
			{
				pomSonderTable->GetCTableListBox()->SetCurSel(-1);
			}
		}
		if(polNotify->SourceTable == (CWnd*)pomStdTable)
		{
			if(omStdData.GetSize() == 0)
			{
				pomStdTable->GetCTableListBox()->SetCurSel(-1);
			}
		}
	}
	return 0L;
}

LONG PremisPage::OnTableRButtonDown(UINT wParam, LONG lParam)
{
	int ilLine = (int)wParam;
	CPoint Point;
	::GetCursorPos(&Point);
	CCSTABLENOTIFY *polNotify;
	polNotify = (CCSTABLENOTIFY*)lParam;
	if(pomTable != NULL)
	{
			return -1L;
	}
	if(pomCurrentGhp != NULL )
	{
		if(polNotify->SourceTable == (CWnd*)pomSonderTable)
		{
			if(omZusatzData.GetSize() == 0)
			{
				pomSonderTable->GetCTableListBox()->SetCurSel(-1);
			}
			else
			{
				bool blFound = false;
				pomStdTable->GetCTableListBox()->SetCurSel(-1);
				CCSPtrArray<GPMDATA> olGpm;
				ogGpmData.GetGpmByRkey(olGpm, pomCurrentGhp->Urno);
				for(int i = 0; i < olGpm.GetSize(); i++)
				{
					GPMDATA *prlGmp = ogGpmData.GetGpmByUrno(olGpm[i].Urno);
					if(prlGmp != NULL)
					{
						if(prlGmp->IsChanged != DATA_DELETED)//MWO XXX
						{
							prlGmp->IsMarked = false;
							ogDdx.DataChanged((void *)this,GPM_CHANGE,(void *)prlGmp);
						}
					}
				}
				int ilCount = omZusatzData.GetSize();
				for( i = 0; (i < ilCount && blFound == false); i++)
				{
					if(i == ilLine)
					{
						GPMDATA *prlGpm = ogGpmData.GetGpmByUrno(omZusatzData[i].Urno);
						if(prlGpm != NULL)
						{
							 SetDutyFields(prlGpm);
							 pomSonderTable->GetCTableListBox()->SetCurSel(ilLine);
							 CMenu menu;
							 menu.CreatePopupMenu();
							 for (int i = menu.GetMenuItemCount(); --i >= 0;)
								menu.RemoveMenu(i, MF_BYPOSITION);
 							 menu.AppendMenu(MF_STRING,44, GetString(IDS_STRING315));	// confirm job
							 //ClientToScreen(&Point);
							 menu.TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON, Point.x, Point.y, this, NULL);
							 blFound = true;
						}
					}
				}
			}
		}
		if(polNotify->SourceTable == (CWnd*)pomStdTable)
		{
			if(omStdData.GetSize() == 0)
			{
				pomStdTable->GetCTableListBox()->SetCurSel(-1);
			}
			else
			{
				bool blFound = false;
				pomSonderTable->GetCTableListBox()->SetCurSel(-1);
				CCSPtrArray<GPMDATA> olGpm;
				ogGpmData.GetGpmByRkey(olGpm, pomCurrentGhp->Urno);
				for(int i = 0; i < olGpm.GetSize(); i++)
				{
					GPMDATA *prlGmp = ogGpmData.GetGpmByUrno(olGpm[i].Urno);
					if(prlGmp != NULL)
					{
						if(prlGmp->IsChanged != DATA_DELETED)//MWO XXX
						{
							prlGmp->IsMarked = false;
							ogDdx.DataChanged((void *)this,GPM_CHANGE,(void *)prlGmp);
						}
					}
				}
				int ilCount = omStdData.GetSize();
				for( i = 0; (i < ilCount && blFound == false); i++)
				{
					if(i == ilLine)
					{
						GPMDATA *prlGpm = ogGpmData.GetGpmByUrno(omStdData[i].Urno);
						if(prlGpm != NULL)
						{
							 SetDutyFields(prlGpm);
							 pomStdTable->GetCTableListBox()->SetCurSel(ilLine);
							 CMenu menu;
							 menu.CreatePopupMenu();
							 for (int i = menu.GetMenuItemCount(); --i >= 0;)
								menu.RemoveMenu(i, MF_BYPOSITION);
 							 menu.AppendMenu(MF_STRING,44, GetString(IDS_STRING315));	// confirm job
							 //ClientToScreen(&Point);
							 menu.TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON, Point.x, Point.y, this, NULL);
							 blFound = true;
							 blFound = true;
						}
					}
				}
			}
		}
	}//if(pomCurrentGhp != NULL)
	return 0L;
}


LONG PremisPage::OnTableLButtonDown(UINT wParam, LONG lParam)
{
	int ilLine = (int)wParam;
	CCSTABLENOTIFY *polNotify;
	polNotify = (CCSTABLENOTIFY*)lParam;
	if(pomCurrentGhp != NULL )
	{
		if(polNotify->SourceTable == (CWnd*)pomSonderTable)
		{
			if(omZusatzData.GetSize() == 0)
			{
				pomSonderTable->GetCTableListBox()->SetCurSel(-1);
			}
			else
			{
				bool blFound = false;
				pomStdTable->GetCTableListBox()->SetCurSel(-1);
				CCSPtrArray<GPMDATA> olGpm;
				ogGpmData.GetGpmByRkey(olGpm, pomCurrentGhp->Urno);
				for(int i = 0; i < olGpm.GetSize(); i++)
				{
					GPMDATA *prlGmp = ogGpmData.GetGpmByUrno(olGpm[i].Urno);
					if(prlGmp != NULL)
					{
						if(prlGmp->IsChanged != DATA_DELETED)//MWO XXX
						{
							prlGmp->IsMarked = false;
							ogDdx.DataChanged((void *)this,GPM_CHANGE,(void *)prlGmp);
						}
					}
				}
				int ilCount = omZusatzData.GetSize();
				for( i = 0; (i < ilCount && blFound == false); i++)
				{
					if(i == ilLine)
					{
						GPMDATA *prlGpm = ogGpmData.GetGpmByUrno(omZusatzData[i].Urno);
						if(prlGpm != NULL)
						{
							SetDutyFields(prlGpm);
							blFound = true;
						}
					}
				}
			}
		}
		if(polNotify->SourceTable == (CWnd*)pomStdTable)
		{
			if(omStdData.GetSize() == 0)
			{
				pomStdTable->GetCTableListBox()->SetCurSel(-1);
			}
			else
			{
				bool blFound = false;
				pomSonderTable->GetCTableListBox()->SetCurSel(-1);
				CCSPtrArray<GPMDATA> olGpm;
				ogGpmData.GetGpmByRkey(olGpm, pomCurrentGhp->Urno);
				for(int i = 0; i < olGpm.GetSize(); i++)
				{
					GPMDATA *prlGmp = ogGpmData.GetGpmByUrno(olGpm[i].Urno);
					if(prlGmp != NULL)
					{
						if(prlGmp->IsChanged != DATA_DELETED)//MWO XXX
						{
							prlGmp->IsMarked = false;
							ogDdx.DataChanged((void *)this,GPM_CHANGE,(void *)prlGmp);
						}
					}
				}
				int ilCount = omStdData.GetSize();
				for( i = 0; (i < ilCount && blFound == false); i++)
				{
					if(i == ilLine)
					{
						GPMDATA *prlGpm = ogGpmData.GetGpmByUrno(omStdData[i].Urno);
						if(prlGpm != NULL)
						{
							SetDutyFields(prlGpm);
							blFound = true;
						}
					}
				}
			}
		}
	}//if(pomCurrentGhp != NULL)

	return 0L;
}


void PremisPage::ShowSpecialTables()
{
	CRect rect;
	int ilLineCount1=0;
	int ilLineCount2=0;
	m_ScciFrame.GetWindowRect(&rect);
    //rect.InflateRect(1, 1);     // hiding the CTable window border
	ScreenToClient(rect);
	
	CCSPtrArray <TABLE_HEADER_COLUMN> omHeaderDataArray;

	omStdData.DeleteAll();
	omZusatzData.DeleteAll();

	TABLE_HEADER_COLUMN rlHeader;
	pomSonderTable->SetShowSelection(TRUE);
	pomSonderTable->SetSelectMode(0);
	pomSonderTable->ResetContent();
	pomSonderTable->UnsetDefaultSeparator();
	rlHeader.Text = CString(GetString(IDS_STRING316));
	rlHeader.Font = &ogSmallFonts_Bold_7;//&ogMSSansSerif_Bold_8;//&ogScalingFonts[10];
	rlHeader.TextColor = COLORREF(BLUE);
	rlHeader.Alignment = COLALIGN_CENTER;
	rlHeader.Length = 200;//rect.right - rect.left-100; 
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
//	rlHeader.Text = CString("");
//	rlHeader.Length = 0;//rect.right - rect.left-100; 
//	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	pomSonderTable->SetHeaderFields(omHeaderDataArray);
	omHeaderDataArray.DeleteAll();

	CCSPtrArray<TABLE_COLUMN> olColList;
	pomSonderTable->ResetContent();

	CCSPtrArray<GPMDATA> olGpmList;
	ogGpmData.GetGpmByRkey( olGpmList, pomCurrentGhp->Urno);
	olGpmList.Sort(CompareGpm);
	TABLE_COLUMN rlColumnData;
	for(int i = 0; i < olGpmList.GetSize(); i++)
	{
		GPMDATA rlGpm = olGpmList[i];
		if(olGpmList[i].IsChanged != DATA_DELETED)
		{
			if(strcmp(olGpmList[i].Disp, "Z") == 0)
			{
				GHSDATA *prlGhs = ogGhsData.GetGhsByUrno(olGpmList[i].Ghsu);
				omZusatzData.NewAt(omZusatzData.GetSize(), olGpmList[i]);

				if(prlGhs != NULL)
				{
					rlColumnData.Text = prlGhs->Lknm;
				}
				else
				{
					rlColumnData.Text = "";
				}
				rlColumnData.Font = &ogScalingFonts[8];
				rlColumnData.HorizontalSeparator = SEPA_NONE;
				olColList.NewAt(olColList.GetSize(), rlColumnData);
				rlColumnData.SeparatorType = SEPA_NONE;
				rlColumnData.VerticalSeparator = SEPA_NONE;
				pomSonderTable->AddTextLine(olColList, (void*)NULL);
				olColList.DeleteAll();
				ilLineCount1++;

			}
		}

	}
	for(i = ilLineCount1; i < 6; i++)
	{
		rlColumnData.Text = "";
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		pomSonderTable->AddTextLine(olColList, (void*)NULL);
		olColList.DeleteAll();
	}

	pomSonderTable->DisplayTable();
//-----
	m_OsduFrame.GetWindowRect(&rect);
	ScreenToClient(rect);

	pomStdTable->SetShowSelection(TRUE);
	pomStdTable->SetSelectMode(0);
	pomStdTable->ResetContent();
	pomStdTable->UnsetDefaultSeparator();
	rlHeader.Text = CString(GetString(IDS_STRING317));
	rlHeader.Font = &ogSmallFonts_Bold_7;//&ogMSSansSerif_Bold_8;//&ogScalingFonts[10];
	rlHeader.TextColor = COLORREF(GREEN);
	rlHeader.Alignment = COLALIGN_CENTER;
	rlHeader.Length = 190;//rect.right - rect.left-100; 
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	pomStdTable->SetHeaderFields(omHeaderDataArray);
	omHeaderDataArray.DeleteAll();

	pomStdTable->ResetContent();

	for( i = 0; i < olGpmList.GetSize(); i++)
	{
		if(strcmp(olGpmList[i].Disp, "S") == 0)
		{
			omStdData.NewAt(omStdData.GetSize(), olGpmList[i]);
			GHSDATA *prlGhs = ogGhsData.GetGhsByUrno(olGpmList[i].Ghsu);
			if(prlGhs != NULL)
			{
				rlColumnData.Text = prlGhs->Lknm;
			}
			else
			{
				rlColumnData.Text = "";
			}
			olColList.NewAt(olColList.GetSize(), rlColumnData);
			rlColumnData.SeparatorType = SEPA_NONE;
			rlColumnData.VerticalSeparator = SEPA_NONE;
			pomStdTable->AddTextLine(olColList, (void*)NULL);
			olColList.DeleteAll();
			ilLineCount2++;
		}

	}
	for(i = ilLineCount2; i < 6; i++)
	{
		rlColumnData.Text = "";
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		pomStdTable->AddTextLine(olColList, (void*)NULL);
		olColList.DeleteAll();
	}

	pomStdTable->DisplayTable();

}

void PremisPage::OnPaint() 
{
	CPaintDC dc(this); // device context for painting
	//Paint Header for Gantt
	CString olText;
	CRect olRect;
	GetClientRect(&olRect);
	olRect.top = 350;
	olRect.bottom = 368;
	olRect.left += 161 + 50;
	olRect.right -= 10;
	CTime olTime = CTime(1980, 1, 1, 0, 30, 0);
	int ilX = omTimeScale.GetXFromTime(olTime) + imVerticalScaleIndent +5;
    COLORREF nOldTextColor = dc.SetTextColor(GREEN);
    COLORREF nOldBackgroundColor = dc.SetBkColor(SILVER);
	CFont *polOldFont = dc.SelectObject(&ogSmallFonts_Bold_7);
	olText = "-60";
	dc.ExtTextOut(ilX, olRect.top, ETO_OPAQUE, &olRect, olText, lstrlen(olText), NULL);
	olTime += CTimeSpan(3600);
	ilX = omTimeScale.GetXFromTime(olTime) + imVerticalScaleIndent-5;
	olRect.left += ilX;
	olText = "on block";
	dc.ExtTextOut(ilX, olRect.top, ETO_OPAQUE, &olRect, olText, lstrlen(olText), NULL);
	olTime += CTimeSpan(3600);
	ilX = omTimeScale.GetXFromTime(olTime) + imVerticalScaleIndent+5;
	olRect.left += ilX;
	olText = "+60";
	dc.ExtTextOut(ilX, olRect.top, ETO_OPAQUE, &olRect, olText, lstrlen(olText), NULL);
	olTime += CTimeSpan(3600);
	ilX = omTimeScale.GetXFromTime(olTime) + imVerticalScaleIndent-10;
	olRect.left += ilX;
	olText = "+120/";
	dc.ExtTextOut(ilX, olRect.top, ETO_OPAQUE, &olRect, olText, lstrlen(olText), NULL);
	dc.SetTextColor(WHITE);
	ilX += 25;
	olRect.left += ilX;
	olText = "-120";
	dc.ExtTextOut(ilX, olRect.top, ETO_OPAQUE, &olRect, olText, lstrlen(olText), NULL);
	olTime += CTimeSpan(3600);
	ilX = omTimeScale.GetXFromTime(olTime) + imVerticalScaleIndent+5;
	olRect.left += ilX;
	olText = "-60";
	dc.ExtTextOut(ilX, olRect.top, ETO_OPAQUE, &olRect, olText, lstrlen(olText), NULL);
	olTime += CTimeSpan(3600);
	ilX = omTimeScale.GetXFromTime(olTime) + imVerticalScaleIndent-5;
	olRect.left += ilX;
	olText = "off block";
	dc.ExtTextOut(ilX, olRect.top, ETO_OPAQUE, &olRect, olText, lstrlen(olText), NULL);
	olTime += CTimeSpan(3600);
	ilX = omTimeScale.GetXFromTime(olTime) + imVerticalScaleIndent+5;
	olRect.left += ilX;
	olText = "+60";
	dc.ExtTextOut(ilX, olRect.top, ETO_OPAQUE, &olRect, olText, lstrlen(olText), NULL);

    dc.SetTextColor(nOldTextColor);
    dc.SetBkColor(nOldBackgroundColor);
	dc.SelectObject(polOldFont);
	
}

void PremisPage::SetControls()
{
	m_Statusbar.SetWindowText("");
	//CEdit	m_Rema;
	//CListBox	m_Osdu;
	//CStatic	m_Lstu.SetWindowText("");
	//CListBox	m_LkcoPerm;
	//CButton	m_Duch;
	//CComboBox	m_Dtyp;
	m_Dopd7.SetCheck(1);
	m_Dopd6.SetCheck(1);
	m_Dopd5.SetCheck(1);
	m_Dopd4.SetCheck(1);
	m_Dopd3.SetCheck(1);
	m_Dopd2.SetCheck(1);
	m_Dopd1.SetCheck(1);
	m_Dopd_All.SetCheck(1);
	m_Dopa7.SetCheck(1);
	m_Dopa6.SetCheck(1);
	m_Dopa5.SetCheck(1);
	m_Dopa4.SetCheck(1);
	m_Dopa3.SetCheck(1);
	m_Dopa2.SetCheck(1);
	m_Dopa1.SetCheck(1);
	m_Dopa_All.SetCheck(1);
	//CString	m_Chgr;
//	int		m_CBDtyp_Idx;

	//All CCSEdits
	m_Dest.SetEditFont(&ogMSSansSerif_Bold_8);
	m_Dest.SetTypeToString(CString("", 3));
//MWO TO DO 
//	m_Alma.SetEditFont(&ogMSSansSerif_Bold_8);
//	m_Alma.SetTypeToString(CString("", 3));
//	m_Almd.SetEditFont(&ogMSSansSerif_Bold_8);
//	m_Almd.SetTypeToString(CString("", 3));
	m_Act5.SetEditFont(&ogMSSansSerif_Bold_8);
	m_Act5.SetTypeToString(CString("", 3));
	m_Flca.SetEditFont(&ogMSSansSerif_Bold_8);
	m_Flca.SetTypeToString(CString("", 3));
	m_Flcd.SetEditFont(&ogMSSansSerif_Bold_8);
	m_Flcd.SetTypeToString(CString("", 3));
	m_Flna.SetEditFont(&ogMSSansSerif_Bold_8);
	m_Flna.SetTypeToInt(0, 99999);
	m_Flnd.SetEditFont(&ogMSSansSerif_Bold_8);
	m_Flnd.SetTypeToInt(0, 99999);
	m_Flsa.SetEditFont(&ogMSSansSerif_Bold_8);
	m_Flsa.SetTypeToString(CString("", 1));
	m_Flsd.SetEditFont(&ogMSSansSerif_Bold_8);
	m_Flsd.SetTypeToString(CString("", 1));
	m_Htpa.SetEditFont(&ogMSSansSerif_Bold_8);
	m_Htpa.SetTypeToString(CString("", 3));
	m_Htpd.SetEditFont(&ogMSSansSerif_Bold_8);
	m_Htpd.SetTypeToString(CString("", 3));
	m_Orig.SetEditFont(&ogMSSansSerif_Bold_8);
	m_Orig.SetTypeToString(CString("", 3));
//	m_PCAD.SetEditFont(&ogMSSansSerif_Bold_8);
//	m_PCAD.SetTypeToString(CString("", 1));
//	m_PCAA.SetEditFont(&ogMSSansSerif_Bold_8);
//	m_PCAA.SetTypeToString(CString("", 1));
//	m_Prco.SetBKColor(LTYELLOW);
//	m_Prco.SetEditFont(&ogMSSansSerif_Bold_8);
//	m_Prco.SetTypeToString("X(5)", 5, 1);
//	m_Prco.SetEditFont(&ogMSSansSerif_Bold_8);
	m_Prna.SetBKColor(LTYELLOW);
	m_Prna.SetEditFont(&ogMSSansSerif_Bold_8);
	m_Maxt.SetBKColor(LTYELLOW);
	m_Maxt.SetEditFont(&ogMSSansSerif_Bold_8);
	m_Maxt.SetTypeToString("###", 3, 1);
	m_Prna.SetTypeToString(CString(""), 64, 1);
	m_Prna.SetEditFont(&ogMSSansSerif_Bold_8);
	m_Prsn.SetBKColor(LTYELLOW);
	m_Prsn.SetEditFont(&ogMSSansSerif_Bold_8);
	m_Prsn.SetTypeToString(CString(""), 12, 1);
	m_Regn.SetEditFont(&ogMSSansSerif_Bold_8);
	m_Regn.SetTypeToString(CString("", 12));
	m_Scca.SetEditFont(&ogMSSansSerif_Bold_8);
	m_Scca.SetTypeToString(CString("", 1));
	m_Sccd.SetEditFont(&ogMSSansSerif_Bold_8);
	m_Sccd.SetTypeToString(CString("", 1));
//MWO TO DO
//	m_Trra.SetEditFont(&ogMSSansSerif_Bold_8);
//	m_Trra.SetTypeToString(CString("", 20));
//	m_Trrd.SetEditFont(&ogMSSansSerif_Bold_8);
//	m_Trrd.SetTypeToString(CString("", 20));
	m_Ttpa.SetEditFont(&ogMSSansSerif_Bold_8);
	m_Ttpa.SetTypeToString(CString("", 3));
	m_Ttpd.SetEditFont(&ogMSSansSerif_Bold_8);
	m_Ttpd.SetTypeToString(CString("", 3));
	m_Vi1a.SetEditFont(&ogMSSansSerif_Bold_8);
	m_Vi1a.SetTypeToString(CString("", 3));
	m_Vi1d.SetEditFont(&ogMSSansSerif_Bold_8);
	m_Vi1d.SetTypeToString(CString("", 3));
	m_Vpfr_Date.SetBKColor(LTYELLOW);
	m_Vpfr_Date.SetTypeToDate(true);
	m_Vpfr_Date.SetInitText(CTime::GetCurrentTime().Format("%d.%m.%Y"));
	m_Vpfr_Date.SetEditFont(&ogMSSansSerif_Bold_8);
	m_Vpfr_Time.SetBKColor(LTYELLOW);
	m_Vpfr_Time.SetTypeToTime(true, false);
	m_Vpfr_Time.SetInitText("00:00");
	m_Vpfr_Time.SetEditFont(&ogMSSansSerif_Bold_8);
	m_Vpto_Date.SetBKColor(LTYELLOW);
	m_Vpto_Date.SetTypeToDate(true);
	m_Vpto_Date.SetInitText(CTime::GetCurrentTime().Format("%d.%m.%Y"));
	m_Vpto_Date.SetEditFont(&ogMSSansSerif_Bold_8);
	m_Vpto_Time.SetBKColor(LTYELLOW);
	m_Vpto_Time.SetTypeToTime(true, false);
	m_Vpto_Time.SetInitText("00:00");
	m_Vpto_Time.SetEditFont(&ogMSSansSerif_Bold_8);



	m_Prta.SetEditFont(&ogMSSansSerif_Bold_8);;
	m_Prta.SetTypeToInt(-999, 999);
	m_Tfdu.SetEditFont(&ogMSSansSerif_Bold_8);
	m_Tfdu.SetTypeToInt(-999, 999);
	m_Ttdu.SetEditFont(&ogMSSansSerif_Bold_8);
	m_Ttdu.SetTypeToInt(-999, 999);
	m_Dtim.SetEditFont(&ogMSSansSerif_Bold_8);
	m_Dtim.SetTypeToInt(0, 999);
	m_Pota.SetEditFont(&ogMSSansSerif_Bold_8);
	m_Pota.SetTypeToInt(-999,999);
	m_Noma.SetEditFont(&ogMSSansSerif_Bold_8);
	m_Noma.SetTypeToInt(0, 999);
	m_Etot.SetEditFont(&ogMSSansSerif_Bold_8);
	m_Etot.SetTypeToInt(0, 999);
	m_Prio.SetEditFont(&ogMSSansSerif_Bold_8);
	m_Prio.SetTypeToInt(0, 9);
	m_Didu.SetEditFont(&ogMSSansSerif_Bold_8);
	m_Didu.SetTypeToInt(0, 9);
}

LONG PremisPage::OnUpdateDiagram(WPARAM wParam, LPARAM lParam)
{
    CString olStr;
    int ipGroupNo = HIWORD(lParam);
    int ipLineNo = LOWORD(lParam);
	 CTime olTime = CTime((time_t) lParam);


    switch (wParam)
    {
        case UD_INSERTLINE :
            pomGantt->InsertString(ipLineNo, "");
			   pomGantt->RepaintItemHeight(ipLineNo);
        break;
        case UD_UPDATELINE :
            pomGantt->RepaintVerticalScale(ipLineNo);
            pomGantt->RepaintGanttChart(ipLineNo);
        break;

        case UD_DELETELINE :
            pomGantt->DeleteString(ipLineNo);
			break;
		case UD_RESETCONTENT:
			pomGantt->ResetContent();
			break;
		break;
    }

    return 0L;
}

void PremisPage::OnAwPrco() 
{
//	CRect olRect;
//	GetClientRect(&olRect);
//	pogPremisAwDlg = new PremisAwDlg(this, olRect);

	if(m_m_BAwPrco->Recess())
	{
		m_m_BAwPrco->Recess(false);
		//SendMessage(CLK_BUTTON_NEW, 0, 0);
		
		if(pomTable != NULL)
		{
			pomTable->ShowWindow(SW_HIDE);
			delete pomTable;
			omLines.DeleteAll();
			pogDutyPreferences->pomCopyButton->EnableWindow(FALSE);
			pomTable = NULL;
		}
		ShowAllControls();
		UpdateWindow();

	}
	else
	{
		pomTable = new CTable;
		pomTable->tempFlag = 2;
		pomTable->SetSelectMode(0);
		m_m_BAwPrco->Recess(true);
		HideAllControls();
		pogDutyPreferences->pomCopyButton->EnableWindow(TRUE);
		CRect rect;
		GetClientRect(&rect);
		rect.InflateRect(1, 1);     // hiding the CTable window border

		CRect PRect;
		GetParent()->GetClientRect(&PRect);
		

		rect.bottom  = PRect.bottom -20;

		pomTable->SetTableData(this, rect.left, rect.right, rect.top + 30, rect.bottom, ::GetSysColor(COLOR_WINDOWTEXT), ::GetSysColor(COLOR_WINDOW), ::GetSysColor(COLOR_HIGHLIGHTTEXT), ::GetSysColor(COLOR_HIGHLIGHT), &ogMSSansSerif_Regular_8);
		//pomTable->SetPosition(-1, rect.left, rect.top + 40, rect.bottom);
		omLines.DeleteAll();
		UpdateDisplay();
	}
}

void PremisPage::UpdateDisplay()
{
	int ilLineNo=-1;
   pomTable->SetHeaderFields(GetString(IDS_STRING318));
   pomTable->SetFormatList("12|70|30");

	pomTable->ResetContent();
	ogGhpData.omData.Sort(CompareToPrco);
	int ilCount = ogGhpData.omData.GetSize();//omLines.GetSize();
	for(int i = 0; i < ilCount; i++)
	{
		GHPDATA rlGhp = ogGhpData.omData[i];
		CreateLine(&rlGhp);
		if(rlGhp.Urno == imLastSelectedUrno)
		{
			ilLineNo = i;
		}
		pomTable->AddTextLine(Format(&omLines[i]), &omLines[i]);
	}

	pomTable->DisplayTable();
	if(ilLineNo != -1)
	{
		pomTable->GetCTableListBox()->SetCurSel(ilLineNo);
	}
	pomTable->SetFocus();
	//pomTable->SetWindowPos(&wndTopMost, 0, 0, 0, 0, SWP_SHOWWINDOW | SWP_NOMOVE | SWP_NOSIZE);
}
CString PremisPage::Format(GHPDATA *prpGhp)
{
   CString s;
   CString olText;
   //CString olPrco;
   olText = CString(prpGhp->Prna);

//   PRCDATA *prlPrc = ogPrcData.GetPrcByUrno(atol(prpGhp->Prco));
//   if(prlPrc != NULL)
   {
//	char pclText[20]="";
//	sprintf(pclText, "%-5s %02d",prlPrc->Redc, prlPrc->Redl);
//	s =  CString(pclText) + "|";
	s+=  CString(prpGhp->Prsn) + "|";
	s+=  olText + "|";
	s+=  prpGhp->Vpfr.Format("%d.%m.%Y - ") + prpGhp->Vpto.Format("%d.%m.%Y") + "|";
   }
	return s;
}

void PremisPage::CreateLine(GHPDATA *prpGhp)
{
	GHPDATA rlGhp;
	rlGhp = *prpGhp;
	omLines.NewAt(omLines.GetSize(), rlGhp);
}

void PremisPage::ShowPremisByUrno(long lpUrno)
{
	GHPDATA *prlGhp = ogGhpData.GetGhpByUrno(lpUrno);
	if(prlGhp != NULL)
	{
		imLastSelectedUrno = prlGhp->Urno;
		//pomTable->DestroyWindow();
		//delete pomTable;
		//omLines.DeleteAll();
		ResetMask();
		if(pomCurrentGhp != NULL)
		{
			delete pomCurrentGhp;
			pomCurrentGhp = NULL;
		}
		pomCurrentGhp = new GHPDATA;//ogGhpData.GetGhpByUrno(prlGhp->Urno);
		*pomCurrentGhp = *prlGhp;
		rmOldGhp = *pomCurrentGhp;

		if(pomCurrentGhp != NULL)
		{
			CCSPtrArray<GPMDATA> olTmpGpmList;
			omOldGpmList.DeleteAll();
			omGpmData.DeleteAll();
			ogGpmData.GetGpmByRkey(olTmpGpmList, pomCurrentGhp->Urno);
			for(int i = 0; i < olTmpGpmList.GetSize(); i++)
			{
				if(olTmpGpmList[i].IsChanged != DATA_DELETED ) //MWO XXX
				{
					omOldGpmList.NewAt(omOldGpmList.GetSize(), olTmpGpmList[i]);
					omGpmData.NewAt(omOldGpmList.GetSize(), olTmpGpmList[i]);
				}
			}
		}
	

		imMode = UPDATE_MODE;
		pomViewer->SetCurrentRkey(pomCurrentGhp->Urno);
		pomViewer->ChangeViewTo(0);
		if(pomGantt != NULL)
		{
			pomGantt->DestroyWindow();
			delete pomGantt;
			MakeNewGantt();
		}
//		CTime olTSStartTime = CTime(1980, 1, 1, 0, 0, 0);
//		CTimeSpan olTSDuration = CTimeSpan(0, 7, 0, 0);
//		CTimeSpan olTSInterval = CTimeSpan(0, 0, 10, 0);
//		pomGantt->RepaintGanttChart();
//		pomGantt->RepaintGanttChart(-1, olTSStartTime, (olTSStartTime + olTSDuration), true);

		pogDutyPreferences->SendMessage(WM_RESETRELEASE);
		ShowAllControls();
		ShowCurrentData();
		pomGantt->RepaintGanttChart();
		pogDutyPreferences->pomCopyButton->EnableWindow(FALSE);
		ShowSpecialTables();
	}

}

LONG PremisPage::OnTableLButtonDblClk(UINT wParam, LONG lParam)
{
	int ilitemID;

	ilitemID = (int)wParam;
	GHPDATA rlLine;

	if(pomCurrentGhp != NULL)
	{
		CCSPtrArray<GPMDATA> olGpmList;
		ogGpmData.GetGpmByRkey(olGpmList, pomCurrentGhp->Urno);
		for(int i = 0; i < olGpmList.GetSize(); i++)
		{
			GPMDATA *prlGpm = ogGpmData.GetGpmByUrno(olGpmList[i].Urno);
			if(prlGpm != NULL)
			{
				if(prlGpm->IsChanged == DATA_NEW)
				{
					ogGpmData.DeleteInternal(prlGpm, true);
				}
				if(prlGpm->IsChanged == DATA_DELETED)
				{
					prlGpm->IsChanged = DATA_UNCHANGED;
				}
			}
		}
	}


	if(ilitemID < 0 ||omLines.GetSize() < ilitemID+1)
	{
		return 0L;
	}
	rlLine = omLines[ilitemID];

	long llUrno = omLines[ilitemID].Urno;
	ShowPremisByUrno(llUrno);
	return 0L;
}

void PremisPage::HideAllControls()
{
	m_Statusbar.ShowWindow(SW_HIDE);
	m_Rema.ShowWindow(SW_HIDE);
	//m_Osdu.ShowWindow(SW_HIDE);
	pomSonderTable->ShowWindow(SW_HIDE);
	pomStdTable->ShowWindow(SW_HIDE);

	m_Lstu.ShowWindow(SW_HIDE);
	m_LkcoPerm.ShowWindow(SW_HIDE);
	m_Duch.ShowWindow(SW_HIDE);
	m_Dtyp.ShowWindow(SW_HIDE);
	m_Dopd7.ShowWindow(SW_HIDE);
	m_Dopd6.ShowWindow(SW_HIDE);
	m_Dopd5.ShowWindow(SW_HIDE);
	m_Dopd4.ShowWindow(SW_HIDE);
	m_Dopd3.ShowWindow(SW_HIDE);
	m_Dopd2.ShowWindow(SW_HIDE);
	m_Dopd1.ShowWindow(SW_HIDE);
	m_Dopd_All.ShowWindow(SW_HIDE);
	m_Dopa7.ShowWindow(SW_HIDE);
	m_Dopa6.ShowWindow(SW_HIDE);
	m_Dopa5.ShowWindow(SW_HIDE);
	m_Dopa4.ShowWindow(SW_HIDE);
	m_Dopa3.ShowWindow(SW_HIDE);
	m_Dopa2.ShowWindow(SW_HIDE);
	m_Dopa1.ShowWindow(SW_HIDE);
	m_Dopa_All.ShowWindow(SW_HIDE);
	m_Prio.ShowWindow(SW_HIDE);
	m_Maxt.ShowWindow(SW_HIDE);

	//All CCSEdits
	m_Dest.ShowWindow(SW_HIDE);
	m_Didu.ShowWindow(SW_HIDE);
	m_Dtim.ShowWindow(SW_HIDE);
//MWO TO DO
//	m_Alma.ShowWindow(SW_HIDE);
//	m_Almd.ShowWindow(SW_HIDE);
	m_ALMA.ShowWindow(SW_SHOWNORMAL);
	m_ALMD.ShowWindow(SW_SHOWNORMAL);
	m_ALMA.EnableWindow(FALSE);
	m_ALMD.EnableWindow(FALSE);
	m_PCAD.EnableWindow(FALSE);
	m_PCAA.EnableWindow(FALSE);
	m_TRRA.EnableWindow(FALSE);
	m_TRRD.EnableWindow(FALSE);
	m_Actm.EnableWindow(FALSE);
	m_Ttga.EnableWindow(FALSE);
	m_Ttgd.EnableWindow(FALSE);
	m_Htga.EnableWindow(FALSE);
	m_Htgd.EnableWindow(FALSE);


	m_Act5.ShowWindow(SW_HIDE);
	m_Etot.ShowWindow(SW_HIDE);
	m_Flca.ShowWindow(SW_HIDE);
	m_Flcd.ShowWindow(SW_HIDE);
	m_Flna.ShowWindow(SW_HIDE);
	m_Flnd.ShowWindow(SW_HIDE);
	m_Flsa.ShowWindow(SW_HIDE);
	m_Flsd.ShowWindow(SW_HIDE);
	m_Htpa.ShowWindow(SW_HIDE);
	m_Htpd.ShowWindow(SW_HIDE);
	m_Noma.ShowWindow(SW_HIDE);
	m_Orig.ShowWindow(SW_HIDE);
	m_PCAD.ShowWindow(SW_HIDE);
	m_PCAA.ShowWindow(SW_HIDE);
	m_Pota.ShowWindow(SW_HIDE);
	//m_Prco.ShowWindow(SW_HIDE);
	m_Prio.ShowWindow(SW_HIDE);
	m_Prna.ShowWindow(SW_HIDE);
	//m_Prsn.ShowWindow(SW_HIDE);
	m_Prta.ShowWindow(SW_HIDE);
	m_Regn.ShowWindow(SW_HIDE);
	m_Scca.ShowWindow(SW_HIDE);
	m_Sccd.ShowWindow(SW_HIDE);
	m_Tfdu.ShowWindow(SW_HIDE);
//MWO TO DO
//	m_Trra.ShowWindow(SW_HIDE);
//	m_Trrd.ShowWindow(SW_HIDE);
	m_Ttdu.ShowWindow(SW_HIDE);
	m_Ttpa.ShowWindow(SW_HIDE);
	m_Ttpd.ShowWindow(SW_HIDE);
	m_Vi1a.ShowWindow(SW_HIDE);
	m_Vi1d.ShowWindow(SW_HIDE);
	//m_Vpfr_Date.ShowWindow(SW_HIDE);
	//m_Vpfr_Time.ShowWindow(SW_HIDE);
	//m_Vpto_Date.ShowWindow(SW_HIDE);
	//m_Vpto_Time.ShowWindow(SW_HIDE);
	pomGantt->ShowWindow(SW_HIDE);
	m_BAwRegn.ShowWindow(SW_HIDE);
	//m_m_BAwPrco.ShowWindow(SW_HIDE);
	m_m_BAwAct5.ShowWindow(SW_HIDE);
	m_m_BAwActD.ShowWindow(SW_HIDE);
	m_BAwActA.ShowWindow(SW_HIDE);
}
void PremisPage::ShowAllControls()
{
	pomSonderTable->ShowWindow(SW_SHOWNORMAL);
	pomStdTable->ShowWindow(SW_SHOWNORMAL);

	m_Statusbar.ShowWindow(SW_SHOWNORMAL);
	m_Rema.ShowWindow(SW_SHOWNORMAL);
	m_Lstu.ShowWindow(SW_SHOWNORMAL);
//	m_LkcoPerm.ShowWindow(SW_SHOWNORMAL);
	m_Duch.ShowWindow(SW_SHOWNORMAL);
	m_Dtyp.ShowWindow(SW_SHOWNORMAL);
	m_Dopd7.ShowWindow(SW_SHOWNORMAL);
	m_Dopd6.ShowWindow(SW_SHOWNORMAL);
	m_Dopd5.ShowWindow(SW_SHOWNORMAL);
	m_Dopd4.ShowWindow(SW_SHOWNORMAL);
	m_Dopd3.ShowWindow(SW_SHOWNORMAL);
	m_Dopd2.ShowWindow(SW_SHOWNORMAL);
	m_Dopd1.ShowWindow(SW_SHOWNORMAL);
	m_Dopd_All.ShowWindow(SW_SHOWNORMAL);
	m_Dopa7.ShowWindow(SW_SHOWNORMAL);
	m_Dopa6.ShowWindow(SW_SHOWNORMAL);
	m_Dopa5.ShowWindow(SW_SHOWNORMAL);
	m_Dopa4.ShowWindow(SW_SHOWNORMAL);
	m_Dopa3.ShowWindow(SW_SHOWNORMAL);
	m_Dopa2.ShowWindow(SW_SHOWNORMAL);
	m_Dopa1.ShowWindow(SW_SHOWNORMAL);
	m_Dopa_All.ShowWindow(SW_SHOWNORMAL);
	m_Prio.ShowWindow(SW_SHOWNORMAL);
	m_Maxt.ShowWindow(SW_SHOWNORMAL);

	//All CCSEdits
	m_Dest.ShowWindow(SW_SHOWNORMAL);
	m_Didu.ShowWindow(SW_SHOWNORMAL);
	m_Dtim.ShowWindow(SW_SHOWNORMAL);
//MWO TO DO
//	m_Alma.ShowWindow(SW_SHOWNORMAL);
//	m_Almd.ShowWindow(SW_SHOWNORMAL);
	m_ALMA.ShowWindow(SW_SHOWNORMAL);
	m_ALMD.ShowWindow(SW_SHOWNORMAL);
	m_ALMA.EnableWindow(TRUE);
	m_ALMD.EnableWindow(TRUE);
	m_Actm.EnableWindow(TRUE);

	m_Act5.ShowWindow(SW_SHOWNORMAL);
	m_Etot.ShowWindow(SW_SHOWNORMAL);
	m_Flca.ShowWindow(SW_SHOWNORMAL);
	m_Flcd.ShowWindow(SW_SHOWNORMAL);
	m_Flna.ShowWindow(SW_SHOWNORMAL);
	m_Flnd.ShowWindow(SW_SHOWNORMAL);
	m_Flsa.ShowWindow(SW_SHOWNORMAL);
	m_Flsd.ShowWindow(SW_SHOWNORMAL);
	m_Htpa.ShowWindow(SW_SHOWNORMAL);
	m_Htpd.ShowWindow(SW_SHOWNORMAL);
	m_Noma.ShowWindow(SW_SHOWNORMAL);
	m_Orig.ShowWindow(SW_SHOWNORMAL);
	m_PCAD.ShowWindow(SW_SHOWNORMAL);
	m_PCAA.ShowWindow(SW_SHOWNORMAL);
	m_PCAD.EnableWindow(TRUE);
	m_PCAA.EnableWindow(TRUE);
	m_TRRA.EnableWindow(TRUE);
	m_TRRD.EnableWindow(TRUE);
	m_Ttga.EnableWindow(TRUE);
	m_Ttgd.EnableWindow(TRUE);
	m_Htga.EnableWindow(TRUE);
	m_Htgd.EnableWindow(TRUE);
	m_Pota.ShowWindow(SW_SHOWNORMAL);
//	m_Prco.ShowWindow(SW_SHOWNORMAL);
	m_Prio.ShowWindow(SW_SHOWNORMAL);
	m_Prna.ShowWindow(SW_SHOWNORMAL);
	m_Prsn.ShowWindow(SW_SHOWNORMAL);
	m_Prta.ShowWindow(SW_SHOWNORMAL);
	m_Regn.ShowWindow(SW_SHOWNORMAL);
	m_Scca.ShowWindow(SW_SHOWNORMAL);
	m_Sccd.ShowWindow(SW_SHOWNORMAL);
	m_Tfdu.ShowWindow(SW_SHOWNORMAL);
//MWO TO DO
	//m_Trra.ShowWindow(SW_SHOWNORMAL);
	//m_Trrd.ShowWindow(SW_SHOWNORMAL);
	m_Ttdu.ShowWindow(SW_SHOWNORMAL);
	m_Ttpa.ShowWindow(SW_SHOWNORMAL);
	m_Ttpd.ShowWindow(SW_SHOWNORMAL);
	m_Vi1a.ShowWindow(SW_SHOWNORMAL);
	m_Vi1d.ShowWindow(SW_SHOWNORMAL);
	m_Vpfr_Date.ShowWindow(SW_SHOWNORMAL);
	m_Vpfr_Time.ShowWindow(SW_SHOWNORMAL);
	m_Vpto_Date.ShowWindow(SW_SHOWNORMAL);
	m_Vpto_Time.ShowWindow(SW_SHOWNORMAL);
	pomGantt->ShowWindow(SW_SHOWNORMAL);
	m_BAwRegn.ShowWindow(SW_SHOWNORMAL);
	//m_m_BAwPrco->ShowWindow(SW_SHOWNORMAL);
	m_m_BAwAct5.ShowWindow(SW_SHOWNORMAL);
	m_m_BAwActD.ShowWindow(SW_SHOWNORMAL);
	m_BAwActA.ShowWindow(SW_SHOWNORMAL);
}


LONG PremisPage::OnSaveButton(WPARAM wParam, LPARAM lParam)
{
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));

	if(GetAllFields() == true)
	{
		if(imMode == NEW_MODE)
		{
			GHPDATA *prlNewGhp = new GHPDATA;
			*prlNewGhp = *pomCurrentGhp;
			prlNewGhp->Cdat = CTime::GetCurrentTime();
			strcpy(prlNewGhp->Usec,  ogBasicData.omUserID);
			strcpy(prlNewGhp->Appl, "POPS");
			ogGhpData.InsertInternal(prlNewGhp);
			ogGhpData.Save(prlNewGhp);
			CCSPtrArray<GPMDATA> olGpmList;
			ogGpmData.GetGpmByRkey(olGpmList, prlNewGhp->Urno);
			int ilCount = olGpmList.GetSize();
			for(int i = 0; i < ilCount; i++)
			{
				GPMDATA *prlGpm = ogGpmData.GetGpmByUrno(olGpmList[i].Urno);
				if(prlGpm != NULL)
				{
					if(prlGpm->IsChanged == DATA_DELETED)
					{
						ogGpmData.DeleteInternal(prlGpm);
					}
					else
					{
						strcpy(prlGpm->Useu, ogBasicData.omUserID);
						ogGpmData.Save(prlGpm);
					}
				}
			}
		}
		if(imMode == UPDATE_MODE)
		{
			pomCurrentGhp->Lstu = CTime::GetCurrentTime();
			strcpy(pomCurrentGhp->Useu,  ogBasicData.omUserID);
			strcpy(pomCurrentGhp->Appl, "POPS");
			ogGhpData.UpdateInternal(pomCurrentGhp);
			ogGhpData.Save(pomCurrentGhp);
			CCSPtrArray<GPMDATA> olGpmList;
			ogGpmData.GetGpmByRkey(olGpmList, pomCurrentGhp->Urno);
			int ilCount = olGpmList.GetSize();
			for(int i = 0; i < ilCount; i++)
			{
				GPMDATA *prlGpm = ogGpmData.GetGpmByUrno(olGpmList[i].Urno);
				if(prlGpm != NULL)
				{
					strcpy(prlGpm->Useu, ogBasicData.omUserID);
					ogGpmData.Save(prlGpm);
					if(prlGpm->IsChanged == DATA_DELETED)
					{
						ogGpmData.DeleteInternal(prlGpm);
					}
				}
			}
			CCSPtrArray<GPMDATA> olTmpGpmList;
			omOldGpmList.DeleteAll();
			omGpmData.DeleteAll();
			ogGpmData.GetGpmByRkey(olTmpGpmList, pomCurrentGhp->Urno);
			for(i = 0; i < olTmpGpmList.GetSize(); i++)
			{
				if(olTmpGpmList[i].IsChanged != DATA_DELETED)//MWO XXX
				{
					omOldGpmList.NewAt(omOldGpmList.GetSize(), olTmpGpmList[i]);
					omGpmData.NewAt(omOldGpmList.GetSize(), olTmpGpmList[i]);
				}
			}
		}
	}
	CString olLstu;
	if(pomCurrentGhp->Lstu != TIMENULL)
		olLstu = pomCurrentGhp->Lstu.Format("%d.%m.%Y - %H:%M");
	else if(pomCurrentGhp->Cdat != TIMENULL)
		olLstu = pomCurrentGhp->Cdat.Format("%d.%m.%Y - %H:%M");
	if(strcmp(pomCurrentGhp->Useu, "") != 0)
		olLstu += CString("  ") + CString(pomCurrentGhp->Useu);
	else if(strcmp(pomCurrentGhp->Usec, "") != 0)
		olLstu += CString("  ") + CString(pomCurrentGhp->Usec);
	m_Lstu.SetWindowText(olLstu);
	imMode = UPDATE_MODE;
	pogDutyPreferences->SendMessage(WM_RESETRELEASE);
	bmBarChanged = false;
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));

	return 0L;
}

LONG PremisPage::OnNewButton(WPARAM wParam, LPARAM lParam)
{
	ResetMask();
	if(pomCurrentGhp != NULL)
	{
		CCSPtrArray<GPMDATA> olGpmList;
		ogGpmData.GetGpmByRkey(olGpmList, pomCurrentGhp->Urno);
		for(int i = 0; i < olGpmList.GetSize(); i++)
		{
			GPMDATA *prlGpm = ogGpmData.GetGpmByUrno(olGpmList[i].Urno);
			if(prlGpm != NULL)
			{
				prlGpm->IsChanged = DATA_UNCHANGED;
			}
		}

		delete pomCurrentGhp;
		pomCurrentGhp = new GHPDATA;
		pomCurrentGhp->IsChanged = DATA_NEW;
		pomCurrentGhp->Urno = ogBasicData.GetNextUrno();
		pomCurrentGhp->Cdat = CTime::GetCurrentTime();
		strcpy(pomCurrentGhp->Usec, ogBasicData.omUserID);
		pomViewer->SetCurrentRkey(pomCurrentGhp->Urno);
		m_PCAD.SetCurSel(-1);
		m_PCAA.SetCurSel(-1);
		m_TRRD.SetCurSel(-1);
		m_TRRA.SetCurSel(-1);
		m_ALMD.SetCurSel(-1);
		m_ALMA.SetCurSel(-1);
		m_Actm.SetCurSel(-1);
		m_Ttga.SetCurSel(-1);
		m_Ttgd.SetCurSel(-1);
		m_Htga.SetCurSel(-1);
		m_Htgd.SetCurSel(-1);




	}
	omStdData.DeleteAll();
	omZusatzData.DeleteAll();
	m_Perm.ResetContent();
	m_LkcoPerm.ResetContent();
	ShowSpecialTables();
	imMode = NEW_MODE;
	return 0L;
}

LONG PremisPage::OnUpdateButton(WPARAM wParam, LPARAM lParam)
{
	if(pomTable != NULL)
	{

		int ilLineNo = pomTable->GetCTableListBox()->GetCurSel();


		long llUrno = omLines[ilLineNo].Urno;
		ShowPremisByUrno(llUrno);

		delete pomTable;
		pomTable = NULL;
		ShowAllControls();
	}
	return 0L;
}

LONG PremisPage::OnDeleteButton(WPARAM wParam, LPARAM lParam)
{

	char pclMsg[256]="";
	if(pomTable != NULL)
	{ 
		//Delete GHP and GPMs, which are currently selected
		//if no row selected ==> do nothing
		//OK fetch selection
		int ilitemID;
		ilitemID = pomTable->GetCTableListBox()->GetCurSel();
		if(ilitemID == LB_ERR)
		{
			return 0L;
		}

		GHPDATA rlLine;
		rlLine = omLines[ilitemID];
		long llUrno = omLines[ilitemID].Urno;
		GHPDATA *prlGhp = ogGhpData.GetGhpByUrno(llUrno);
		if(prlGhp != NULL)
		{
//			char pclPrco[30]="";
//			PRCDATA *prlPrc = ogPrcData.GetPrcByUrno(atoi(prlGhp->Prco));
//			if(prlPrc != NULL)
//			{
//				sprintf(pclPrco, "%s %02ld", prlPrc->Redc, prlPrc->Redl);
//			}
			sprintf(pclMsg, GetString(IDS_STRING319), prlGhp->Prsn);
			if(MessageBox(pclMsg, GetString(IDS_STRING320), (MB_ICONQUESTION|MB_YESNO)) == IDYES)
			{
				CCSPtrArray<GPMDATA> olGpmList;
				ogGpmData.GetGpmByRkey(olGpmList, prlGhp->Urno);
				int ilCount = olGpmList.GetSize();
				for(int i = ilCount-1; i >= 0 ; i--)
				{
					GPMDATA *prlGpm = ogGpmData.GetGpmByUrno(olGpmList[i].Urno);
					if(prlGpm != NULL)
					{
						prlGpm->IsChanged = DATA_DELETED;
						ogGpmData.Save(prlGpm);
						ogGpmData.DeleteInternal(prlGpm);
					}
				}
				GHPDATA *prlGhp2 = ogGhpData.GetGhpByUrno(prlGhp->Urno);
				if(prlGhp2 != NULL)
				{
					long llCurrentUrno;
					long llDeleteUrno = prlGhp2->Urno;
					if(pomCurrentGhp != NULL)
					{
						llCurrentUrno = pomCurrentGhp->Urno;
					}
					prlGhp2->IsChanged = DATA_DELETED;
					ogGhpData.Save(prlGhp2);
					ogGhpData.DeleteInternal(prlGhp2);
					pomTable->DeleteTextLine(ilitemID);
					omLines.DeleteAt(ilitemID);
					if(llCurrentUrno == llDeleteUrno)
					{
						OnNewButton(0,0);
					}
				}
			}
		}
	}
	else
	{
		if(imMode == NEW_MODE)
		{
			MessageBox(GetString(IDS_STRING321), GetString(IDS_STRING117), (MB_ICONEXCLAMATION|MB_OK));
			return 0L;
		}
		if(pomCurrentGhp != NULL)
		{
//			char pclPrco[30]="";
//			PRCDATA *prlPrc = ogPrcData.GetPrcByUrno(atoi(pomCurrentGhp->Prco));
//			if(prlPrc != NULL)
//			{
//				sprintf(pclPrco, "%s %02ld", prlPrc->Redc, prlPrc->Redl);
//			}
			sprintf(pclMsg, GetString(IDS_STRING319), pomCurrentGhp->Prsn);
			if(MessageBox(pclMsg, GetString(IDS_STRING320), (MB_ICONQUESTION|MB_YESNO)) == IDYES)
			{
				CCSPtrArray<GPMDATA> olGpmList;
				ogGpmData.GetGpmByRkey(olGpmList, pomCurrentGhp->Urno);
				int ilCount = olGpmList.GetSize();
				for(int i = ilCount-1; i >= 0 ; i--)
				{
					GPMDATA *prlGpm = ogGpmData.GetGpmByUrno(olGpmList[i].Urno);
					if(prlGpm != NULL)
					{
						prlGpm->IsChanged = DATA_DELETED;
						ogGpmData.Save(prlGpm);
						ogGpmData.DeleteInternal(prlGpm);
					}
				}
				GHPDATA *prlGhp = ogGhpData.GetGhpByUrno(pomCurrentGhp->Urno);
				if(prlGhp != NULL)
				{
					prlGhp->IsChanged = DATA_DELETED;
					ogGhpData.Save(prlGhp);
					ogGhpData.DeleteInternal(prlGhp);
				}
				OnNewButton(0,0);
			}
		}
	}
	return 0L;
}

LONG PremisPage::OnCopyButton(WPARAM wParam, LPARAM lParam)
{
	if(pomTable != NULL)
	{ 
		int ilitemID;
		ilitemID = pomTable->GetCTableListBox()->GetCurSel();
		if(ilitemID == LB_ERR)
		{
			return 0L;
		}
		

		GHPDATA rlLine;
		rlLine = omLines[ilitemID];
		long llUrno = omLines[ilitemID].Urno;
		GHPDATA *prlGhp = ogGhpData.GetGhpByUrno(llUrno);
		if(prlGhp != NULL)
		{

			ResetMask();
			GHPDATA *prlNewGhd = new GHPDATA;
			*prlNewGhd = *prlGhp;
			prlNewGhd->IsChanged = DATA_NEW;
			prlNewGhd->Urno = ogBasicData.GetNextUrno();
			ogGhpData.InsertInternal(prlNewGhd);
			ogGhpData.Save(prlNewGhd);
			CCSPtrArray<GPMDATA> olGpmList;
			ogGpmData.GetGpmByRkey(olGpmList, prlGhp->Urno);
			int ilCount = olGpmList.GetSize();
			for(int i = ilCount-1; i >= 0 ; i--)
			{
				GPMDATA *prlGpm = ogGpmData.GetGpmByUrno(olGpmList[i].Urno);
				if(prlGpm != NULL)
				{
					if(prlGpm->IsChanged != DATA_DELETED) //MWO XXX
					{
						GPMDATA *prlNewGpm = new GPMDATA;
						*prlNewGpm = *prlGpm;
						prlNewGpm->Urno = ogBasicData.GetNextUrno();
						prlNewGpm->Rkey = prlNewGhd->Urno;
						prlNewGpm->IsChanged = DATA_NEW;
						ogGpmData.InsertInternal(prlNewGpm);
						ogGpmData.Save(prlNewGpm);
					}
				}
			}
			*pomCurrentGhp = *prlNewGhd;
			imMode = UPDATE_MODE;
			pomViewer->SetCurrentRkey(pomCurrentGhp->Urno);
			pomViewer->ChangeViewTo(0);
			if(pomGantt != NULL)
			{
				pomGantt->DestroyWindow();
				delete pomGantt;
				MakeNewGantt();
			}
//			CTime olTSStartTime = CTime(1980, 1, 1, 0, 0, 0);
//			CTimeSpan olTSDuration = CTimeSpan(0, 7, 0, 0);
//			CTimeSpan olTSInterval = CTimeSpan(0, 0, 10, 0);
//			pomGantt->RepaintGanttChart();
//			pomGantt->RepaintGanttChart(-1, olTSStartTime, (olTSStartTime + olTSDuration), true);

			pogDutyPreferences->SendMessage(WM_RESETRELEASE);
			ShowAllControls();
			ShowCurrentData();
			pomGantt->RepaintGanttChart();
		}
	}
	else
	{
		MessageBox(GetString(IDS_STRING322), GetString(IDS_STRING117), MB_OK);
	}
	return 0L;
}

LONG PremisPage::OnStandardButton(WPARAM wParam, LPARAM lParam)
{
	if(pomTable != NULL)
	{
		return 0L; //DO nothing
//		delete pomTable;
//		pomTable = NULL;
//		ShowAllControls();
	}

//	bool blPrco		= false;//m_Prco.GetStatus();
	bool blPrsn		= m_Prsn.GetStatus();
	bool blPrna		= m_Prna.GetStatus();
	bool blVpfrDate = m_Vpfr_Date.GetStatus();
	bool blVpFrTime = m_Vpfr_Time.GetStatus();
	bool blVpToDate = m_Vpto_Date.GetStatus();
	bool blVpToTime = m_Vpto_Time.GetStatus();

	CString olText;
//	m_Prco.GetWindowText(olText);
//	if(!olText.IsEmpty())
//	{
//		blPrco = true;
//	}

	if(/*blPrco && */blPrsn	&& blPrna && blVpfrDate &&	blVpFrTime && blVpToDate && blVpToTime)
	{
		int ilCount = ogGhsData.omData.GetSize();
		CDWordArray olGhsDispUrnos;
		CDWordArray olGhsSonstUrnos;

		for(int i = 0; i < ilCount; i++)
		{
			//check, ob Standard Leistung
			if(strcmp(ogGhsData.omData[i].Lkst, "X") == 0)
			{
				//Check, ob Leistung disponierbar
				if(strcmp(ogGhsData.omData[i].Disp, "X") == 0)
				{
					olGhsDispUrnos.Add(ogGhsData.omData[i].Urno);
				}
				else
				{
					olGhsSonstUrnos.Add(ogGhsData.omData[i].Urno);
				}
			}
		}
		if(olGhsDispUrnos.GetSize() > 0)
		{
			long llRkey = pomCurrentGhp->Urno;
			GHSDATA *prlGhs = ogGhsData.GetGhsByUrno(olGhsDispUrnos[0]);
			CreateGpmFromGhsList(this, olGhsDispUrnos, pomCurrentGhp);
			SendMessage(BAR_CHANGED);
		}
		CString olOsdu;
/*		ilCount = olGhsSonstUrnos.GetSize();
		for(i = 0; i < ilCount; i++)
		{
			GHSDATA *prlGhs = ogGhsData.GetGhsByUrno(olGhsSonstUrnos[i]);
			if(prlGhs != NULL)
			{
				m_Osdu.AddString(prlGhs->Lknm);
				char pclUrno[12];
				sprintf(pclUrno, "%ld;", olGhsSonstUrnos[i]);
				olOsdu += CString(pclUrno);

			}
		}
		if(strlen(pomCurrentGhp->Osdu) < 242)
			strcat(pomCurrentGhp->Osdu, olOsdu);
*/
	}
	else
	{
		CString olMsg;
//		if(blPrco == false)
//			olMsg += "Eingabe \"Code\" fehlt oder ist unzulässig Bsp: ==> R1\n";
		if(blPrsn == false)
			olMsg += GetString(IDS_STRING109);
		if(blPrna == false)
			olMsg += GetString(IDS_STRING110);
		if(blVpfrDate == false)
			olMsg += GetString(IDS_STRING111);
		if(blVpFrTime == false)
			olMsg += GetString(IDS_STRING112);
		if(blVpToDate == false)
			olMsg += GetString(IDS_STRING113);
		if(blVpToTime == false)
			olMsg += GetString(IDS_STRING114);
		MessageBox(olMsg, GetString(IDS_STRING117), (MB_OK|MB_ICONEXCLAMATION));
		CString olText;
//		m_Prco.GetWindowText(olText);
//		if(olText.IsEmpty())
//		{
//			m_Prco.SetFocus();
//			return 0;
//		}
//		if(blPrco == false)
//		{
//			m_Prco.SetFocus();
//			return 0;
//		}
		if(blPrsn == false)
		{
			m_Prsn.SetFocus();
			return 0;
		}
		if(blPrna == false)
		{
			m_Prna.SetFocus();
			return 0;
		}
		if(blVpfrDate == false)
		{
			m_Vpfr_Date.SetFocus();
			return 0;
		}
		if(blVpFrTime == false)
		{
			m_Vpfr_Time.SetFocus();
			return 0;
		}
		if(blVpToDate == false)
		{
			m_Vpto_Date.SetFocus();
			return 0;
		}
		if(blVpToTime == false)
		{
			m_Vpto_Time.SetFocus();
			return 0;
		}
	}

	return 0L;
}

LONG PremisPage::OnAdditionalButton(WPARAM wParam, LPARAM lParam)
{
//	bool blPrco		= false;//m_Prco.GetStatus();
	bool blPrsn		= m_Prsn.GetStatus();
	bool blPrna		= m_Prna.GetStatus();
	bool blVpfrDate = m_Vpfr_Date.GetStatus();
	bool blVpFrTime = m_Vpfr_Time.GetStatus();
	bool blVpToDate = m_Vpto_Date.GetStatus();
	bool blVpToTime = m_Vpto_Time.GetStatus();
	bool blMaxt     = m_Maxt.GetStatus();

	CString olText;
//	m_Prco.GetWindowText(olText);
//	if(!olText.IsEmpty())
//	{
//		blPrco = true;
//	}

	if(/*blPrco && */blPrsn	&& blPrna && blVpfrDate &&	blVpFrTime && blVpToDate && blVpToTime && blMaxt)
	{
		pogGhsList = new GhsList(this);
	}
	else
	{
		CString olMsg;
//		if(blPrco == false)
//			olMsg += GetString(IDS_STRING323)"Eingabe \"Code\" fehlt oder ist unzulässig Bsp: ==> R1\n";
		if(blPrsn == false)
			olMsg += GetString(IDS_STRING109);
		if(blPrna == false)
			olMsg += GetString(IDS_STRING110);
		if(blVpfrDate == false)
			olMsg += GetString(IDS_STRING111);
		if(blVpFrTime == false)
			olMsg += GetString(IDS_STRING112);
		if(blVpToDate == false)
			olMsg += GetString(IDS_STRING113);
		if(blVpToTime == false)
			olMsg += GetString(IDS_STRING114);
		if(blMaxt == false)
			olMsg += GetString(IDS_STRING324);
		MessageBox(olMsg, GetString(IDS_STRING117), (MB_OK|MB_ICONEXCLAMATION));
		CString olText;
/*		m_Prco.GetWindowText(olText);
		if(olText.IsEmpty())
		{
			m_Prco.SetFocus();
			return 0;
		}
*/
//		if(blPrco == false)
//		{
//			m_Prco.SetFocus();
//			return 0;
//		}
		if(blPrsn == false)
		{
			m_Prsn.SetFocus();
			return 0;
		}
		if(blPrna == false)
		{
			m_Prna.SetFocus();
			return 0;
		}
		if(blVpfrDate == false)
		{
			m_Vpfr_Date.SetFocus();
			return 0;
		}
		if(blVpFrTime == false)
		{
			m_Vpfr_Time.SetFocus();
			return 0;
		}
		if(blVpToDate == false)
		{
			m_Vpto_Date.SetFocus();
			return 0;
		}
		if(blVpToTime == false)
		{
			m_Vpto_Time.SetFocus();
			return 0;
		}
	}
	return 0L;
}


BOOL PremisPage::OnKillActive() 
{
	pogDutyPreferences->pomCopyButton->EnableWindow(FALSE);
	pogDutyPreferences->pomStandardButton->EnableWindow(FALSE);
	pogDutyPreferences->pomAdditionalButton->EnableWindow(FALSE);	
	pogDutyPreferences->pomSaveButton->EnableWindow(FALSE);	
	pogDutyPreferences->pomNewButton->EnableWindow(FALSE);	
	pogDutyPreferences->pomUpdateButton->EnableWindow(FALSE);	
	pogDutyPreferences->pomDeleteButton->EnableWindow(TRUE);
	UpdateData(FALSE);
	if(pogGhsList != NULL)
	{
		pogGhsList->DestroyWindow();
	}
	return CPropertyPage::OnKillActive();
}

BOOL PremisPage::OnSetActive() 
{
	// TODO: Add your specialized code here and/or call the base class
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
	omTrraData.DeleteAll();
	omTrrdData.DeleteAll();
	omPcaaData.DeleteAll();
	omPcadData.DeleteAll();
	omAtmData.DeleteAll();
	omAlmaData.DeleteAll();
	omAlmdData.DeleteAll();
	omTtgaData.DeleteAll();
	omTtgdData.DeleteAll();
	omHtgaData.DeleteAll();
	omHtgdData.DeleteAll();
	omCicGrpData.DeleteAll();

	char pclTableName[16];

	m_Perm.ResetContent();
	strcpy(pclTableName,"CIC");
	strcat(pclTableName,pcgTableExt);
	ogGrnData.GetGrnsByTabn(omCicGrpData, pclTableName, "POPS");
	omCicGrpData.Sort(CompareGroup);
	for(int i = 0; i < omCicGrpData.GetSize(); i++)
	{
		m_Perm.AddString(omCicGrpData[i].Grpn);
		//m_Actm.AddString(omAtmData[i].Grsn);
	}

	m_Actm.ResetContent();
	strcpy(pclTableName,"ACT");
	strcat(pclTableName,pcgTableExt);
	ogGrnData.GetGrnsByTabn(omAtmData, pclTableName, "POPS");
	m_Actm.AddString("");
	omAtmData.Sort(CompareGroup);
	for(i = 0; i < omAtmData.GetSize(); i++)
	{
		m_Actm.AddString(omAtmData[i].Grpn);
		//m_Actm.AddString(omAtmData[i].Grsn);
	}
	m_TRRA.ResetContent();
	m_TRRD.ResetContent();
	strcpy(pclTableName,"APT");
	strcat(pclTableName,pcgTableExt);
	ogGrnData.GetGrnsByTabn(omTrraData, pclTableName, "POPS");
	ogGrnData.GetGrnsByTabn(omTrrdData, pclTableName, "POPS");

	m_TRRA.AddString("");
	m_TRRD.AddString("");
	omTrraData.Sort(CompareGroup);
	for(i = 0; i < omTrraData.GetSize(); i++)
	{
		m_TRRA.AddString(omTrraData[i].Grpn);
		m_TRRD.AddString(omTrraData[i].Grpn);
		//m_TRRA.AddString(omTrraData[i].Grsn);
		//m_TRRD.AddString(omTrraData[i].Grsn);
	}
	m_ALMA.ResetContent();
	m_ALMD.ResetContent();
	strcpy(pclTableName,"ALT");
	strcat(pclTableName,pcgTableExt);
	ogGrnData.GetGrnsByTabn(omAlmaData, pclTableName, "POPS");
	ogGrnData.GetGrnsByTabn(omAlmdData, pclTableName, "POPS");
	
	m_ALMA.AddString("");
	m_ALMD.AddString("");
	omAlmaData.Sort(CompareGroup);
	omAlmdData.Sort(CompareGroup);
	for(i = 0; i < omAlmaData.GetSize(); i++)
	{
		m_ALMA.AddString(omAlmaData[i].Grpn);
		m_ALMD.AddString(omAlmaData[i].Grpn);
		//m_ALMA.AddString(omAlmaData[i].Grsn);
		//m_ALMD.AddString(omAlmaData[i].Grsn);
	}
	m_PCAA.ResetContent();
	m_PCAD.ResetContent();
	strcpy(pclTableName,"PST");
	strcat(pclTableName,pcgTableExt);
	ogGrnData.GetGrnsByTabn(omPcaaData, pclTableName, "POPS");
	ogGrnData.GetGrnsByTabn(omPcadData, pclTableName, "POPS");
	m_PCAA.AddString("");
	m_PCAD.AddString("");
	omPcaaData.Sort(CompareGroup);
	omPcadData.Sort(CompareGroup);

	for(i = 0; i < omPcaaData.GetSize(); i++)
	{
		m_PCAA.AddString(omPcaaData[i].Grpn);
		m_PCAD.AddString(omPcaaData[i].Grpn);
		//m_PCAA.AddString(omPcaaData[i].Grsn);
		//m_PCAD.AddString(omPcaaData[i].Grsn);
	}
/*	
	omTtgaData
	omTtgdData
	omHtgaData
	omHtgdData
*/
	m_Ttga.ResetContent();
	m_Ttga.AddString("");
	strcpy(pclTableName,"NAT");
	strcat(pclTableName,pcgTableExt);
	ogGrnData.GetGrnsByTabn(omTtgaData, pclTableName, "POPS");
	omTtgaData.Sort(CompareGroup);
	for(i = 0; i < omTtgaData.GetSize(); i++)
	{
		m_Ttga.AddString(omTtgaData[i].Grpn);
	}
	m_Ttgd.ResetContent();
	m_Ttgd.AddString("");
	strcpy(pclTableName,"NAT");
	strcat(pclTableName,pcgTableExt);
	ogGrnData.GetGrnsByTabn(omTtgdData, pclTableName, "POPS");
	omTtgdData.Sort(CompareGroup);
	for(i = 0; i < omTtgdData.GetSize(); i++)
	{
		m_Ttgd.AddString(omTtgdData[i].Grpn);
	}
	m_Htga.ResetContent();
	m_Htga.AddString("");
	strcpy(pclTableName,"HTY");
	strcat(pclTableName,pcgTableExt);
	ogGrnData.GetGrnsByTabn(omHtgaData, pclTableName, "POPS");
	omHtgaData.Sort(CompareGroup);
	for(i = 0; i < omHtgaData.GetSize(); i++)
	{
		m_Htga.AddString(omHtgaData[i].Grpn);
	}
	m_Htgd.ResetContent();
	m_Htgd.AddString("");
	strcpy(pclTableName,"HTY");
	strcat(pclTableName,pcgTableExt);
	ogGrnData.GetGrnsByTabn(omHtgdData, pclTableName, "POPS");
	omHtgdData.Sort(CompareGroup);
	for(i = 0; i < omHtgdData.GetSize(); i++)
	{
		m_Htgd.AddString(omHtgdData[i].Grpn);
	}




	ShowCurrentData();
	//SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
	return CPropertyPage::OnSetActive();
}

void PremisPage::OnKillfocusPrco() 
{
/*	if(CheckMussFelder() == TRUE)	
	{
		pogDutyPreferences->SendMessage(WM_SETRELEASE);
	}
*/
}

void PremisPage::OnKillfocusPrna() 
{
	if(CheckMussFelder() == TRUE)	
	{
		pogDutyPreferences->SendMessage(WM_SETRELEASE);
	}
}

void PremisPage::OnKillfocusPrsn() 
{
	if(CheckMussFelder() == TRUE)	
	{
		pogDutyPreferences->SendMessage(WM_SETRELEASE);
	}
}

void PremisPage::OnKillfocusVpfrDate() 
{
	if(CheckMussFelder() == TRUE)	
	{
		pogDutyPreferences->SendMessage(WM_SETRELEASE);
	}
}

void PremisPage::OnKillfocusVpfrTime() 
{
	if(CheckMussFelder() == TRUE)	
	{
		pogDutyPreferences->SendMessage(WM_SETRELEASE);
	}
}

void PremisPage::OnKillfocusVptoDate() 
{
	if(CheckMussFelder() == TRUE)	
	{
		pogDutyPreferences->SendMessage(WM_SETRELEASE);
	}
}

void PremisPage::OnKillfocusVptoTime() 
{
	if(CheckMussFelder() == TRUE)	
	{
		pogDutyPreferences->SendMessage(WM_SETRELEASE);
	}
}

bool PremisPage::CheckMussFelder()
{
//	bool blPrco = false;//		= m_Prco.GetStatus();
	bool blPrsn		= m_Prsn.GetStatus();
	bool blPrna		= m_Prna.GetStatus();
	bool blVpfrDate = m_Vpfr_Date.GetStatus();
	bool blVpFrTime = m_Vpfr_Time.GetStatus();
	bool blVpToDate = m_Vpto_Date.GetStatus();
	bool blVpToTime = m_Vpto_Time.GetStatus();
	bool blMaxt     = m_Maxt.GetStatus();

	CString olText;
//	m_Prco.GetWindowText(olText);
//	if(!olText.IsEmpty())
//	{
//		blPrco = true;
//	}

	if(imMode == NEW_MODE)
	{
		if(/*blPrco && */blPrsn	&& blPrna && blVpfrDate &&	blVpFrTime && blVpToDate && blVpToTime && blMaxt)
		{
			CString olText;
			//m_Prco.GetWindowText(olText);
			pogDutyPreferences->SendMessage(WM_SETRELEASE);
			return true;
		}
		else
		{
			pogDutyPreferences->SendMessage(WM_RESETRELEASE);
			return false;
		}
	}
	if(imMode == UPDATE_MODE)
	{
		bool blAllFieldsOK = GetAllFields(false);
		if((/*blPrco && */blPrsn	&& blPrna && blVpfrDate &&	blVpFrTime && blVpToDate && blVpToTime) &&
			(ogGhpData.IsDataChanged(&rmOldGhp, pomCurrentGhp) == true) && (blAllFieldsOK == true))
		{
			pogDutyPreferences->SendMessage(WM_SETRELEASE);
			pomCurrentGhp->IsChanged = DATA_CHANGED;
			return true;
		}
		else
		{
			pomCurrentGhp->IsChanged = DATA_UNCHANGED;
			if(bmBarChanged == false)
			{
				pogDutyPreferences->SendMessage(WM_RESETRELEASE);
			}
			return false;
		}
	}
	return true;
}



LONG PremisPage::OnCCSEditKillFocus( UINT wParam, LPARAM lParam)
{
	CCSEDITNOTIFY *prlNotify = (CCSEDITNOTIFY*)lParam;
		CheckMussFelder();
//	if((UINT)m_Prco.imID == wParam)
//	{
//		CheckMussFelder();
//	}
	if((UINT)m_Prsn.imID == wParam)
	{
		CheckMussFelder();
	}
	if((UINT)m_Prna.imID == wParam)
	{
		CheckMussFelder();
	}
	if((UINT)m_Vpfr_Date.imID == wParam)
	{
		CheckMussFelder();
	}
	if((UINT)m_Vpfr_Time.imID == wParam)
	{
		CheckMussFelder();
	}
	if((UINT)m_Vpto_Date.imID == wParam)
	{
		CheckMussFelder();
	}
	if((UINT)m_Vpto_Time.imID == wParam)
	{
		CheckMussFelder();
	}
	if((UINT)m_Maxt.imID == wParam)
	{
		CheckMussFelder();
		CString olText;
		m_Maxt.GetWindowText(olText);
		pomCurrentGhp->Maxt = atoi(olText);
		UpdateAllTurnarrounds();
	}

	if((UINT)m_Prta.imID == wParam)
	{
		CString olText;
		m_Prta.GetWindowText(olText);
		if(pomCurrentGpm != NULL)
		{
			pomCurrentGpm->Prta = atoi(olText);
			if(ogGpmData.IsDataChanged(&rmOldGpm, pomCurrentGpm))
			{
				bmBarChanged = true;
				pogDutyPreferences->SendMessage(WM_SETRELEASE);
			}
			ogGpmData.UpdateInternal(pomCurrentGpm);
		}
	}
	if((UINT)m_Tfdu.imID == wParam)
	{
		CString olText;
		m_Tfdu.GetWindowText(olText);
		if(pomCurrentGpm != NULL)
		{
			pomCurrentGpm->Tfdu = atoi(olText);
			if(ogGpmData.IsDataChanged(&rmOldGpm, pomCurrentGpm))
			{
				bmBarChanged = true;
				pogDutyPreferences->SendMessage(WM_SETRELEASE);
			}
			ogGpmData.UpdateInternal(pomCurrentGpm);
		}
	}

	if((UINT)m_Ttdu.imID == wParam)
	{
		CString olText;
		m_Ttdu.GetWindowText(olText);
		if(pomCurrentGpm != NULL)
		{
			pomCurrentGpm->Ttdu = atoi(olText);
			if(ogGpmData.IsDataChanged(&rmOldGpm, pomCurrentGpm))
			{
				bmBarChanged = true;
				pogDutyPreferences->SendMessage(WM_SETRELEASE);
			}
			ogGpmData.UpdateInternal(pomCurrentGpm);
		}
	}

	if((UINT)m_Dtim.imID == wParam)
	{
		CString olText;
		m_Dtim.GetWindowText(olText);
		if(pomCurrentGpm != NULL)
		{
			pomCurrentGpm->Dtim = atoi(olText);
			if(ogGpmData.IsDataChanged(&rmOldGpm, pomCurrentGpm))
			{
				bmBarChanged = true;
				pogDutyPreferences->SendMessage(WM_SETRELEASE);
			}
			ogGpmData.UpdateInternal(pomCurrentGpm);
		}
	}
		
	if((UINT)m_Pota.imID == wParam)
	{
		CString olText;
		m_Pota.GetWindowText(olText);
		if(pomCurrentGpm != NULL)
		{
			pomCurrentGpm->Pota = atoi(olText);
			if(ogGpmData.IsDataChanged(&rmOldGpm, pomCurrentGpm))
			{
				bmBarChanged = true;
				pogDutyPreferences->SendMessage(WM_SETRELEASE);
			}
			ogGpmData.UpdateInternal(pomCurrentGpm);
		}
	}

	if((UINT)m_Noma.imID == wParam)
	{
		CString olText;
		m_Noma.GetWindowText(olText);
		if(pomCurrentGpm != NULL)
		{
			pomCurrentGpm->Noma = atoi(olText);
			if(ogGpmData.IsDataChanged(&rmOldGpm, pomCurrentGpm))
			{
				bmBarChanged = true;
				pogDutyPreferences->SendMessage(WM_SETRELEASE);
			}
			ogGpmData.UpdateInternal(pomCurrentGpm);
		}
	}

	if((UINT)m_Etot.imID == wParam)
	{
		CString olText;
		m_Etot.GetWindowText(olText);
		if(pomCurrentGpm != NULL)
		{
			pomCurrentGpm->Etot = atoi(olText);
			if(ogGpmData.IsDataChanged(&rmOldGpm, pomCurrentGpm))
			{
				bmBarChanged = true;
				pogDutyPreferences->SendMessage(WM_SETRELEASE);
			}
			ogGpmData.UpdateInternal(pomCurrentGpm);
		}
	}
	if((UINT)m_Prio.imID == wParam)
	{
		CString olText;
		m_Prio.GetWindowText(olText);
		if(pomCurrentGpm != NULL)
		{
			pomCurrentGpm->Prio = atoi(olText);
			if(ogGpmData.IsDataChanged(&rmOldGpm, pomCurrentGpm))
			{
				bmBarChanged = true;
				pogDutyPreferences->SendMessage(WM_SETRELEASE);
			}
			ogGpmData.UpdateInternal(pomCurrentGpm);
		}
	}
	if((UINT)m_Didu.imID == wParam)
	{
		CString olText;
		m_Didu.GetWindowText(olText);
		if(pomCurrentGpm != NULL)
		{
			pomCurrentGpm->Didu = atoi(olText);
			if(ogGpmData.IsDataChanged(&rmOldGpm, pomCurrentGpm))
			{
				bmBarChanged = true;
				pogDutyPreferences->SendMessage(WM_SETRELEASE);
			}
			ogGpmData.UpdateInternal(pomCurrentGpm);
		}
	}

	char buffer[64];

	if((UINT)m_Flca.imID == wParam)
	{
		prlNotify->UserStatus = true;
		prlNotify->Status = true;

		strcpy(buffer, prlNotify->Text);

		if(ogAltData.GetUrnoByAlc(buffer) <= 0)
		{
			prlNotify->Status = false;
		}
	}

	if((UINT)m_Flcd.imID == wParam)
	{
		prlNotify->UserStatus = true;
		prlNotify->Status = true;
		strcpy(buffer, prlNotify->Text);

		if(ogAltData.GetUrnoByAlc(buffer) <= 0)
		{
			prlNotify->Status = false;
		}
	}

	if((UINT)m_Prsn.imID == wParam)
	{
		prlNotify->UserStatus = true;
		prlNotify->Status = true;
		strcpy(buffer, prlNotify->Text);

		if(pomCurrentGhp != NULL)
		{
			int ilCount = ogGhpData.CheckPrsn(buffer);

			if(( (imMode == UPDATE_MODE) && (ilCount > 1) ) || ( (imMode == NEW_MODE) && (ilCount > 0)) )
			{
				MessageBox(GetString(IDS_STRING159), GetString(IDS_STRING117));
			}
		}
	}






	return 0L;
}

void PremisPage::SetDutyFields(GPMDATA *prpGpm)
{
	if(prpGpm != NULL)
	{
		pomCurrentGpm = prpGpm;
		rmOldGpm = *prpGpm;
		char pclTmp[500]="";
		sprintf(pclTmp, "%d", prpGpm->Prta);
		m_Prta.SetWindowText(pclTmp);

		if(strcmp(prpGpm->Dtyp, "T") == 0)
		{
			int ilIdx = m_Dtyp.FindString( -1, LoadStg(IDS_STRING104));
			m_Dtyp.SetCurSel(ilIdx);
		}
		else if(strcmp(prpGpm->Dtyp, "F") == 0)
		{
			int ilIdx = m_Dtyp.FindString( -1, LoadStg(IDS_STRING105));
			m_Dtyp.SetCurSel(ilIdx);
		}
		else if(strcmp(prpGpm->Dtyp, "N") == 0)
		{
			int ilIdx = m_Dtyp.FindString( -1, LoadStg(IDS_STRING103));
			m_Dtyp.SetCurSel(ilIdx);
		}
		sprintf(pclTmp, "%d", prpGpm->Tfdu);
		m_Tfdu.SetWindowText(pclTmp);
		sprintf(pclTmp, "%d", prpGpm->Ttdu);
		m_Ttdu.SetWindowText(pclTmp);
//MWO DTIM Behandlung
		if(strcmp(prpGpm->Dtyp, "T") == 0)
		{
			CString olMaxt;
			m_Maxt.GetWindowText(olMaxt);
			int ilMaxt = atoi(olMaxt);
			if(prpGpm->Dtim > ilMaxt)
			{
				prpGpm->Dtim = ilMaxt;
			}
		}
		sprintf(pclTmp, "%d", prpGpm->Dtim);
		m_Dtim.SetWindowText(pclTmp);
		sprintf(pclTmp, "%d", prpGpm->Pota);
		m_Pota.SetWindowText(pclTmp);
		sprintf(pclTmp, "%d", prpGpm->Noma);
		m_Noma.SetWindowText(pclTmp);
		sprintf(pclTmp, "%d", prpGpm->Etot);
		m_Etot.SetWindowText(pclTmp);
//		sprintf(pclTmp, "%d", prpGpm->Prio);
//		m_Prio.SetWindowText(pclTmp);
		sprintf(pclTmp, "%d", prpGpm->Didu);
		m_Didu.SetWindowText(pclTmp);
		if(strcmp(prpGpm->Duch, "J") == 0)
		{
			m_Duch.SetCheck(1);
		}
		else
		{
			m_Duch.SetCheck(0);
		}
		m_LkcoPerm.ResetContent();
		if(strcmp(pomCurrentGpm->Rtyp, "P") == 0)
		{
			m_Resource.SetWindowText(LoadStg(IDS_STRING106));
			LoadPfcData();
			m_Perm.EnableWindow(TRUE);
			LoadPerData();
			m_Perm.ShowWindow(SW_SHOWNORMAL);
		}
		else if(strcmp(pomCurrentGpm->Rtyp, "G") == 0)
		{
			m_Resource.SetWindowText(LoadStg(IDS_STRING107));
			LoadGegData();
			LoadPerData();
			//m_Perm.ResetContent();
			//m_Perm.EnableWindow(FALSE);
			//m_Perm.ShowWindow(SW_HIDE);
		}
		else
		{
			m_Resource.SetWindowText(LoadStg(IDS_STRING108));
			m_Perm.ResetContent();
			m_LkcoPerm.ResetContent();
			m_Perm.ShowWindow(SW_HIDE);
		}

		CString olPerms = CString(prpGpm->Perm);
		CString olGegs = CString(prpGpm->Vrgc);
		if(!olPerms.IsEmpty())
		{

		}
	}	
	else
	{
		m_LkcoPerm.ResetContent();
		m_Perm.ResetContent();
		m_Prta.SetWindowText("");
		m_Tfdu.SetWindowText("");
		m_Ttdu.SetWindowText("");
		m_Dtim.SetWindowText("");
		m_Pota.SetWindowText("");
		m_Noma.SetWindowText("");
		m_Etot.SetWindowText("");
		m_Prio.SetWindowText("");
		m_Didu.SetWindowText("");
	}
}




void PremisPage::OnSelchangeDtyp() 
{
	int ilIdx = m_Dtyp.GetCurSel();
	if(pomCurrentGpm == NULL)
		return;
	if(ilIdx != LB_ERR)
	{
		CString olText;
		m_Dtyp.GetLBText(ilIdx, olText);
		if(olText == LoadStg(IDS_STRING103))
		{
			olText = "N";
		}
		else if(olText == LoadStg(IDS_STRING105))
		{
			olText = "F";
		}
		else if(olText == LoadStg(IDS_STRING104))
		{
			olText = "T";
		}
		strcpy(pomCurrentGpm->Dtyp, olText);
		ogGpmData.UpdateInternal(pomCurrentGpm);
	}
}


void PremisPage::OnKillfocusDuch() 
{
}

bool PremisPage::GetAllFields(bool bpWithMessageBox)
{
/*
	m_Dest
	m_Alma
	m_Almd
	m_Act5
	m_Flca
	m_Flcd
	m_Flna
	m_Flnd
	m_Flsa
	m_Flsd
	m_Htpa
	m_Htpd
	m_Orig
	m_PCAD
	m_PCAA
	m_Prco
	m_Prna
	m_Prsn
	m_Regn
	m_Scca
	m_Sccd
	m_Trra
	m_Ttpa
	m_Ttpd
	m_Vi1a
	m_Vi1d
	m_Vpfr_Date
	m_Vpfr_Time
	m_Vpto_Date
	m_Vpto_Time
*/
	CString olText;
	int i;
	bool blDest = m_Dest.GetStatus();
//	bool blAlma = m_Alma.GetStatus();
//	bool blAlmd = m_Almd.GetStatus();
	bool blAct5 = m_Act5.GetStatus();
	bool blFlca = m_Flca.GetStatus();
	bool blFlcd = m_Flcd.GetStatus();
	bool blFlna = m_Flna.GetStatus();
	bool blFlnd = m_Flnd.GetStatus();
	bool blFlsa = m_Flsa.GetStatus();
	bool blFlsd = m_Flsd.GetStatus();
	bool blHtpa = m_Htpa.GetStatus();
	bool blHtpd = m_Htpd.GetStatus();
	bool blOrig = m_Orig.GetStatus();
//	bool blPcad = m_PCAD.GetStatus();
//	bool blPcaa = m_PCAA.GetStatus();
//	bool blPrco = false;//m_Prco.GetStatus();
	bool blPrna = m_Prna.GetStatus();
	bool blPrsn = m_Prsn.GetStatus();
	bool blRegn = m_Regn.GetStatus();
	bool blScca = m_Scca.GetStatus();
	bool blSccd = m_Sccd.GetStatus();
//	bool blTrra = m_Trra.GetStatus();
	bool blTtpa = m_Ttpa.GetStatus();
	bool blTtpd = m_Ttpd.GetStatus();
	bool blVi1a = m_Vi1a.GetStatus();
	bool blVi1d = m_Vi1d.GetStatus();
	bool blVpfr_Date = m_Vpfr_Date.GetStatus();
	bool blVpfr_Time = m_Vpfr_Time.GetStatus();
	bool blVpto_Date = m_Vpto_Date.GetStatus();
	bool blVpto_Time = m_Vpto_Time.GetStatus();
	bool blMaxt = m_Maxt.GetStatus();
	bool blPrio = m_Prio.GetStatus();
	bool blDateOk = true;

//	m_Prco.GetWindowText(olText);
//	if(!olText.IsEmpty())
//	{
//		blPrco = true;
//	}
	CTime olFrom, olTo;
	if(blVpfr_Date && blVpfr_Time && blVpto_Date && blVpto_Time)
	{
		CString olTStr, olDStr;
		m_Vpfr_Date.GetWindowText(olDStr);
		m_Vpfr_Time.GetWindowText(olTStr);
		olFrom = DateHourStringToDate(olDStr, olTStr);

		m_Vpto_Date.GetWindowText(olDStr);
		m_Vpto_Time.GetWindowText(olTStr);
		olTo = DateHourStringToDate(olDStr, olTStr);
		if(olTo < olFrom)
		{
			blDateOk = false;
		}
	}
	
	if(blDest /*&& blAlma && blAlmd*/ && blAct5 && blFlca && blFlcd && blFlna && blFlnd && blFlsa &&
	   blFlsd && blHtpa && blHtpd && blOrig /*&& blPcad && blPcaa && blPrco */&& blPrna && blPrsn &&
	   blRegn && blScca && blSccd /*&& blTrra*/ && blTtpa && blTtpd && blVi1a && blVi1d && blVpfr_Date &&
	   blVpfr_Time && blVpto_Date && blVpto_Time && blDateOk && blMaxt && blPrio)
	{
		if(pomCurrentGhp != NULL)
		{
			if(imMode == NEW_MODE)
			{
				strcpy(pomCurrentGhp->Usec, ogBasicData.omUserID);
			}
			else if(imMode == UPDATE_MODE)
			{
				strcpy(pomCurrentGhp->Useu, ogBasicData.omUserID);
			}
			m_Dest.GetWindowText(olText);
			strcpy(pomCurrentGhp->Dest, olText);
//MWO TO DO
			m_ALMA.GetWindowText(olText);
			if(olText == CString("") || olText == CString(" "))
			{
					strcpy(pomCurrentGhp->Alma, "");
			}
			else
			{
				bool blFound = false;
				for(i = 0; i < omAlmaData.GetSize(); i++)
				{
					//if(strcmp(omAlmaData[i].Grsn, olText) == 0)
					if(strcmp(omAlmaData[i].Grpn, olText) == 0)
					{
						sprintf(pomCurrentGhp->Alma, "%ld", omAlmaData[i].Urno);
						blFound = true;
					}
				}
				if(blFound == false)
				{
					strcpy(pomCurrentGhp->Alma, "");
				}
			}
			m_ALMD.GetWindowText(olText);
			if(olText == CString("") || olText == CString(" "))
			{
					strcpy(pomCurrentGhp->Almd, "");
			}
			else
			{
				bool blFound = false;
				for(i = 0; i < omAlmdData.GetSize(); i++)
				{
					if(strcmp(omAlmdData[i].Grpn, olText) == 0)
					//if(strcmp(omAlmdData[i].Grsn, olText) == 0)
					{
						sprintf(pomCurrentGhp->Almd, "%ld",omAlmdData[i].Urno);
						blFound = true;
					}
				}
				if(blFound == false)
				{
					strcpy(pomCurrentGhp->Almd, "");
				}
			}
			//strcpy(pomCurrentGhp->Almd, olText);
			m_PCAA.GetWindowText(olText);
			if(olText == CString("") || olText == CString(" "))
			{
					strcpy(pomCurrentGhp->Pcaa, "");
			}
			else
			{
				bool blFound = false;
				for(i = 0; i < omPcaaData.GetSize(); i++)
				{
					if(strcmp(omPcaaData[i].Grpn, olText) == 0)
					//if(strcmp(omPcaaData[i].Grsn, olText) == 0)
					{
						sprintf(pomCurrentGhp->Pcaa, "%ld",omPcaaData[i].Urno);
						blFound = true;
					}
				}
				if(blFound == false)
				{
					strcpy(pomCurrentGhp->Pcaa, "");
				}
			}		
			//strcpy(pomCurrentGhp->Pcaa, olText);
			m_PCAD.GetWindowText(olText);
			if(olText == CString("") || olText == CString(" "))
			{
					strcpy(pomCurrentGhp->Pcad, "");
			}
			else
			{
				bool blFound = false;
				for(i = 0; i < omPcadData.GetSize(); i++)
				{
					if(strcmp(omPcadData[i].Grpn, olText) == 0)
					//if(strcmp(omPcadData[i].Grsn, olText) == 0)
					{
						sprintf(pomCurrentGhp->Pcad, "%ld",omPcadData[i].Urno);
						blFound = true;
					}
				}
				if(blFound == false)
				{
					strcpy(pomCurrentGhp->Pcad, "");
				}
			}
			//strcpy(pomCurrentGhp->Pcad, olText);
			
			m_Actm.GetWindowText(olText);
			if(olText == CString("") || olText == CString(" "))
			{
					strcpy(pomCurrentGhp->Actm, "");
			}
			else
			{
				bool blFound = false;
				for(i = 0; i < omAtmData.GetSize(); i++)
				{
					if(strcmp(omAtmData[i].Grpn, olText) == 0)
					//if(strcmp(omAtmData[i].Grsn, olText) == 0)
					{
						sprintf(pomCurrentGhp->Actm, "%ld",omAtmData[i].Urno);
						blFound = true;
					}
				}
				if(blFound == false)
				{
					strcpy(pomCurrentGhp->Actm, "");
				}
			}
			//strcpy(pomCurrentGhp->Actm, olText);
//MWO TO DO
			m_Htga.GetWindowText(olText);
			if(olText == CString("") || olText == CString(" "))
			{
					strcpy(pomCurrentGhp->Htga, "");
			}
			else
			{
				bool blFound = false;
				for(i = 0; i < omHtgaData.GetSize(); i++)
				{
					if(strcmp(omHtgaData[i].Grpn, olText) == 0)
					//if(strcmp(omHtgaData[i].Grsn, olText) == 0)
					{
						sprintf(pomCurrentGhp->Htga, "%ld",omHtgaData[i].Urno);
						blFound = true;
					}
				}
				if(blFound == false)
				{
					strcpy(pomCurrentGhp->Htga, "");
				}
			}
	//----------------------------------
			m_Htgd.GetWindowText(olText);
			if(olText == CString("") || olText == CString(" "))
			{
					strcpy(pomCurrentGhp->Htgd, "");
			}
			else
			{
				bool blFound = false;
				for(i = 0; i < omHtgdData.GetSize(); i++)
				{
					if(strcmp(omHtgdData[i].Grpn, olText) == 0)
					//if(strcmp(omHtgdData[i].Grsn, olText) == 0)
					{
						sprintf(pomCurrentGhp->Htgd, "%ld",omHtgdData[i].Urno);
						blFound = true;
					}
				}
				if(blFound == false)
				{
					strcpy(pomCurrentGhp->Htgd, "");
				}
			}
	//----------------------------------
			m_Ttga.GetWindowText(olText);
			if(olText == CString("") || olText == CString(" "))
			{
					strcpy(pomCurrentGhp->Ttga, "");
			}
			else
			{
				bool blFound = false;
				for(i = 0; i < omTtgaData.GetSize(); i++)
				{
					if(strcmp(omTtgaData[i].Grpn, olText) == 0)
					//if(strcmp(omTtgaData[i].Grsn, olText) == 0)
					{
						sprintf(pomCurrentGhp->Ttga, "%ld",omTtgaData[i].Urno);
						blFound = true;
					}
				}
				if(blFound == false)
				{
					strcpy(pomCurrentGhp->Ttga, "");
				}
			}
	//----------------------------------
			m_Ttgd.GetWindowText(olText);
			if(olText == CString("") || olText == CString(" "))
			{
					strcpy(pomCurrentGhp->Ttgd, "");
			}
			else
			{
				bool blFound = false;
				for(i = 0; i < omTtgdData.GetSize(); i++)
				{
					if(strcmp(omTtgdData[i].Grpn, olText) == 0)
					//if(strcmp(omTtgdData[i].Grsn, olText) == 0)
					{
						sprintf(pomCurrentGhp->Ttgd, "%ld",omTtgdData[i].Urno);
						blFound = true;
					}
				}
				if(blFound == false)
				{
					strcpy(pomCurrentGhp->Ttgd, "");
				}
			}
	//----------------------------------
			m_TRRA.GetWindowText(olText);
			if(olText == CString("") || olText == CString(" "))
			{
					strcpy(pomCurrentGhp->Trra, "");
			}
			else
			{
				bool blFound = false;
				for(i = 0; i < omTrraData.GetSize(); i++)
				{
					if(strcmp(omTrraData[i].Grpn, olText) == 0)
					//if(strcmp(omTrraData[i].Grsn, olText) == 0)
					{
						sprintf(pomCurrentGhp->Trra, "%ld",omTrraData[i].Urno);
						blFound = true;
					}
				}
				if(blFound == false)
				{
					strcpy(pomCurrentGhp->Trra, "");
				}
			}
			//strcpy(pomCurrentGhp->Trra, olText);
			m_TRRD.GetWindowText(olText);
			if(olText == CString("") || olText == CString(" "))
			{
					strcpy(pomCurrentGhp->Trrd, "");
			}
			else
			{
				bool blFound = false;
				for(i = 0; i < omTrrdData.GetSize(); i++)
				{
					if(strcmp(omTrrdData[i].Grpn, olText) == 0)
					//if(strcmp(omTrrdData[i].Grsn, olText) == 0)
					{
						sprintf(pomCurrentGhp->Trrd, "%ld",omTrrdData[i].Urno);
						blFound = true;
					}
				}
				if(blFound == false)
				{
					strcpy(pomCurrentGhp->Trrd, "");
				}
			}
			//strcpy(pomCurrentGhp->Trrd, olText);
			m_Act5.GetWindowText(olText);
			strcpy(pomCurrentGhp->Act5, olText);
			m_Flca.GetWindowText(olText);
			strcpy(pomCurrentGhp->Flca, olText);
			m_Flcd.GetWindowText(olText);
			strcpy(pomCurrentGhp->Flcd, olText);
			m_Flna.GetWindowText(olText);
			strcpy(pomCurrentGhp->Flna, olText);
			m_Flnd.GetWindowText(olText);
			strcpy(pomCurrentGhp->Flnd, olText);
			m_Flsa.GetWindowText(olText);
			strcpy(pomCurrentGhp->Flsa, olText);
			m_Flsd.GetWindowText(olText);
			strcpy(pomCurrentGhp->Flsd, olText);
			m_Htpa.GetWindowText(olText);
			strcpy(pomCurrentGhp->Htpa, olText);
			m_Htpd.GetWindowText(olText);
			strcpy(pomCurrentGhp->Htpd, olText);
			m_Orig.GetWindowText(olText);
			strcpy(pomCurrentGhp->Orig, olText);
			//m_PCAD.GetWindowText(olText);
			//strcpy(pomCurrentGhp->Pcad, olText);
//			m_Prco.GetWindowText(olText);
//			int ilIdx = m_Prco.GetCurSel();
/*			if(ilIdx != -1)
			{
				char pclUrno[20]="";
				sprintf(pclUrno, "%ld", omPrcData[ilIdx].Urno);
				strcpy(pomCurrentGhp->Prco, pclUrno);
			}

*/
			m_Prna.GetWindowText(olText);
			for(int i=0;i>-1;)
			{
				i = olText.Find("\r\n");
				if(i != -1)
				{
					olText = olText.Left(i) + " " + olText.Mid(i+2,olText.GetLength());
				}
			}
			strcpy(pomCurrentGhp->Prna, olText);
			m_Prsn.GetWindowText(olText);
			strcpy(pomCurrentGhp->Prsn, olText);
			m_Regn.GetWindowText(olText);
			strcpy(pomCurrentGhp->Regn, olText);
			m_Scca.GetWindowText(olText);
			strcpy(pomCurrentGhp->Scca, olText);
			m_Sccd.GetWindowText(olText);
			strcpy(pomCurrentGhp->Sccd,olText);
			m_Ttpa.GetWindowText(olText);
			strcpy(pomCurrentGhp->Ttpa, olText);
			m_Ttpd.GetWindowText(olText);
			strcpy(pomCurrentGhp->Ttpd, olText);
			m_Vi1a.GetWindowText(olText);
			strcpy(pomCurrentGhp->Vi1a, olText);
			m_Vi1d.GetWindowText(olText);
			strcpy(pomCurrentGhp->Vi1d, olText);

			CString olTimeStr, olDateStr;
			m_Vpfr_Date.GetWindowText(olDateStr);
			m_Vpfr_Time.GetWindowText(olTimeStr);
			CTime olVpfr = DateHourStringToDate(olDateStr, olTimeStr);
			pomCurrentGhp->Vpfr = olVpfr;

			m_Vpto_Date.GetWindowText(olDateStr);
			m_Vpto_Time.GetWindowText(olTimeStr);
			CTime olVpto = DateHourStringToDate(olDateStr, olTimeStr);
			pomCurrentGhp->Vpto = olVpto;

			m_Rema.GetWindowText(olText);
			strcpy(pomCurrentGhp->Rema, olText);

			m_Chgr.GetWindowText(olText);
			strcpy(pomCurrentGhp->Chgr, olText);
			m_Maxt.GetWindowText(olText);
			pomCurrentGhp->Maxt = atoi(olText);
			m_Prio.GetWindowText(olText);
			pomCurrentGhp->Prio = atoi(olText);
			//DOPA und DOPD
			CString olDopa, olDopd;
			if(m_Dopa1.GetCheck() == 1)
				olDopa += "1";
			if(m_Dopa2.GetCheck() == 1)
				olDopa += "2";
			if(m_Dopa3.GetCheck() == 1)
				olDopa += "3";
			if(m_Dopa4.GetCheck() == 1)
				olDopa += "4";
			if(m_Dopa5.GetCheck() == 1)
				olDopa += "5";
			if(m_Dopa6.GetCheck() == 1)
				olDopa += "6";
			if(m_Dopa7.GetCheck() == 1)
				olDopa += "7";
			if(m_Dopd1.GetCheck() == 1)
				olDopd += "1";
			if(m_Dopd2.GetCheck() == 1)
				olDopd += "2";
			if(m_Dopd3.GetCheck() == 1)
				olDopd += "3";
			if(m_Dopd4.GetCheck() == 1)
				olDopd += "4";
			if(m_Dopd5.GetCheck() == 1)
				olDopd += "5";
			if(m_Dopd6.GetCheck() == 1)
				olDopd += "6";
			if(m_Dopd7.GetCheck() == 1)
				olDopd += "7";
			strcpy(pomCurrentGhp->Dopa, olDopa);
			strcpy(pomCurrentGhp->Dopd, olDopd);
		}
		return true;		
	}
	else
	{
		CString olMsg;
//		if(blPrco == false)
//			olMsg += GetString(IDS_STRING328);
			
		if((blFlca == false) || (blFlcd == false))
			olMsg += LoadStg(IDS_STRING157);
		
		if(blPrsn == false)
			olMsg += LoadStg(IDS_STRING109);
		if(blPrna == false)
			olMsg += LoadStg(IDS_STRING110);
		if(blVpfr_Date == false)
			olMsg += LoadStg(IDS_STRING111);
		if(blVpfr_Time == false)
			olMsg += LoadStg(IDS_STRING112);
		if(blVpto_Date == false)
			olMsg += LoadStg(IDS_STRING113);
		if(blVpto_Time == false)
			olMsg += LoadStg(IDS_STRING114);
		if(blDateOk == false)
			olMsg += LoadStg(IDS_STRING115);
		if(blMaxt == false)
			olMsg += LoadStg(IDS_STRING116);

		if(bpWithMessageBox == true)
		{
			MessageBox(olMsg, LoadStg(IDS_STRING117), (MB_OK|MB_ICONEXCLAMATION));

			if(blDateOk == false)
			{
				//m_Vpto_Date.SetFocus();
				return 0;
			}
//			if(blPrco == false)
//			{
//				m_Prco.SetFocus();
//				return 0;
//			}
			/*
			if(blPrsn == false)
			{
				m_Prsn.SetFocus();
				return 0;
			}
			if(blPrna == false)
			{
				m_Prna.SetFocus();
				return 0;
			}
			if(blVpfr_Date == false)
			{
				m_Vpfr_Date.SetFocus();
				return 0;
			}
			if(blVpfr_Time == false)
			{
				m_Vpfr_Time.SetFocus();
				return 0;
			}
			if(blVpto_Date == false)
			{
				m_Vpto_Date.SetFocus();
				return 0;
			}
			if(blVpto_Time == false)
			{
				m_Vpto_Time.SetFocus();
				return 0;
			}
			*/
			return false;
		}
	}


 


	return true;
}

void PremisPage::ClearMask()
{
	//m_Osdu.ResetContent();
	m_Dest.SetWindowText("");
//MWO TO DO
//	m_Alma.SetWindowText("");
//	m_Almd.SetWindowText("");
	m_Act5.SetWindowText("");
	m_Flca.SetWindowText("");
	m_Flcd.SetWindowText("");
	m_Flna.SetWindowText("");
	m_Flnd.SetWindowText("");
	m_Flsa.SetWindowText("");
	m_Flsd.SetWindowText("");
	m_Htpa.SetWindowText("");
	m_Htpd.SetWindowText("");
	m_Orig.SetWindowText("");
	m_PCAD.SetWindowText("");
//	m_Prco.SetCurSel(-1);//SetWindowText("");
	m_Prna.SetWindowText("");
	m_Prsn.SetWindowText("");
	m_Regn.SetWindowText("");
	m_Scca.SetWindowText("");
	m_Sccd.SetWindowText("");
//MWO TO DO
//	m_Trra.SetWindowText("");
//	m_Trrd.SetWindowText("");
	m_Ttpa.SetWindowText("");
	m_Ttpd.SetWindowText("");
	m_Vi1a.SetWindowText("");
	m_Vi1d.SetWindowText("");
	m_Prta.SetWindowText("");
	m_Tfdu.SetWindowText("");
	m_Ttdu.SetWindowText("");
	m_Dtim.SetWindowText("");
	m_Pota.SetWindowText("");
	m_Noma.SetWindowText("");
	m_Etot.SetWindowText("");
	m_Prio.SetWindowText("");
	m_Didu.SetWindowText("");

	pomSonderTable->ResetContent();
	pomStdTable->ResetContent();
	omStdData.DeleteAll();
	omZusatzData.DeleteAll();
	ShowSpecialTables();

	omStdData.DeleteAll();
	omZusatzData.DeleteAll();
	
	CTime olTime = CTime::GetCurrentTime();
	m_Vpfr_Date.SetWindowText(olTime.Format("%d.%m.%Y"));
	m_Vpfr_Time.SetWindowText(olTime.Format("%H:%M"));
	m_Vpto_Date.SetWindowText(olTime.Format("%d.%m.%Y"));
	m_Vpto_Time.SetWindowText(olTime.Format("%H:%M"));

	//Reset Gantt-Chart
	ogDdx.DataChanged((void *)this,GPM_PREMIS_DELETE,(void *)NULL);
	pomViewer->SetCurrentRkey(-1);
	pomViewer->ChangeViewTo(0);
	if(pomGantt != NULL)
	{
		pomGantt->DestroyWindow();
		delete pomGantt;
		MakeNewGantt();
	}
	m_Lstu.SetWindowText("");

	//CTime olTSStartTime = CTime(1980, 1, 1, 0, 0, 0);
    //CTimeSpan olTSDuration = CTimeSpan(0, 7, 0, 0);
    //CTimeSpan olTSInterval = CTimeSpan(0, 0, 10, 0);
	//pomGantt->RepaintGanttChart();
	//pomGantt->RepaintGanttChart(-1, olTSStartTime, (olTSStartTime + olTSDuration), true);
	pogDutyPreferences->SendMessage(WM_RESETRELEASE);
	m_Dtyp.SetCurSel(-1);
//	m_Prco.SetFocus();
}


void PremisPage::MakeNewGantt()
{
	CTime olTSStartTime = CTime(1980, 1, 1, 0, 0, 0);
	CRect olTSRect;
	GetClientRect(&olTSRect);
	olTSRect.top = 347;
	olTSRect.bottom = 365;
	olTSRect.left += 161;
	olTSRect.right -= 10;

	pomGantt = new PremisPageGantt;
    pomGantt->SetTimeScale(&omTimeScale);
	pomGantt->SetStatusBar(&m_Statusbar);
    pomGantt->SetViewer(pomViewer);
	CTimeSpan dur = omTimeScale.GetDisplayDuration();
    pomGantt->SetDisplayWindow(olTSStartTime, olTSStartTime + omTimeScale.GetDisplayDuration());
    pomGantt->SetVerticalScaleWidth(150);
	pomGantt->SetVerticalScaleIndent(imVerticalScaleIndent);
	pomGantt->SetVerticalScaleColors(BLACK, SILVER, AQUA, SILVER);
    pomGantt->SetFonts(3, 3);
    //pomGantt->SetVerticalScaleColors(NAVY, SILVER, YELLOW, SILVER);
	pomGantt->SetGanttChartColors(NAVY, SILVER, YELLOW, SILVER);
    pomGantt->Create(WS_BORDER, CRect(olTSRect.left-imVerticalScaleIndent+1, olTSRect.bottom + 1, olTSRect.right, olTSRect.bottom+200), (CWnd*)this);
	pomGantt->AttachWindow(this);
}

void PremisPage::ResetMask()
{
	if(pomTable != NULL)
	{
		pomTable->DestroyWindow();
		delete pomTable;
		pomTable = NULL;
		omLines.DeleteAll();
		ShowAllControls();
		m_m_BAwPrco->Recess(false);
	}
	ClearMask();
}

void PremisPage::ShowCurrentData()
{
	//SetDutyFields(pomCurrentGhp);

	m_Dest.SetWindowText(pomCurrentGhp->Dest);
//MWO TO DO
	//m_Prco.SetWindowText(pomCurrentGhp->Prco);
/*	for(int i = 0; i < omPrcData.GetSize(); i++)
	{
		long llU = omPrcData[i].Urno;
		if(atoi(pomCurrentGhp->Prco) == omPrcData[i].Urno)
		{
			m_Prco.SetCurSel(i);
			i = omPrcData.GetSize();
		}
	}
*/
	int ilIdx = -1;
	//m_Alma.SetWindowText(pomCurrentGhp->Alma);
	bool blFound = false;
	for(int i = 0; i < omAlmaData.GetSize(); i++)
	{
		if(omAlmaData[i].Urno == atoi(pomCurrentGhp->Alma))
		{
			ilIdx = m_ALMA.FindString(-1, omAlmaData[i].Grpn);
			//ilIdx = m_ALMA.FindString(-1, omAlmaData[i].Grsn);
			if(ilIdx != -1)
			{
				m_ALMA.SetCurSel(ilIdx);
				blFound = true;
			}
			else
			{
				m_ALMA.SetCurSel(-1);
			}
		}
	}
	if(blFound == false)
	{
		m_ALMA.SetCurSel(-1);
	}
	blFound = false;
	for(i = 0; i < omAlmdData.GetSize(); i++)
	{
		if(omAlmdData[i].Urno == atoi(pomCurrentGhp->Almd))
		{
			ilIdx = m_ALMD.FindString(-1, omAlmdData[i].Grpn);
			//ilIdx = m_ALMD.FindString(-1, omAlmdData[i].Grsn);
			if(ilIdx != -1)
			{
				m_ALMD.SetCurSel(ilIdx);
				blFound = true;
			}
			else
			{
				m_ALMD.SetCurSel(-1);
			}
		}
	}
	if(blFound == false)
	{
		m_ALMD.SetCurSel(-1);
	}
	blFound = false;
//--------------------
	for(i = 0; i < omTtgaData.GetSize(); i++)
	{
		if(omTtgaData[i].Urno == atoi(pomCurrentGhp->Ttga))
		{
			ilIdx = m_Ttga.FindString(-1, omTtgaData[i].Grpn);
			//ilIdx = m_Ttga.FindString(-1, omTtgaData[i].Grsn);
			if(ilIdx != -1)
			{
				m_Ttga.SetCurSel(ilIdx);
				blFound = true;
			}
			else
			{
				m_Ttga.SetCurSel(-1);
			}
		}
	}
	if(blFound == false)
	{
		m_Ttga.SetCurSel(-1);
	}
	blFound = false;
//----------------
	for(i = 0; i < omTtgdData.GetSize(); i++)
	{
		if(omTtgdData[i].Urno == atoi(pomCurrentGhp->Ttgd))
		{
			ilIdx = m_Ttgd.FindString(-1, omTtgdData[i].Grpn);
			//ilIdx = m_Ttgd.FindString(-1, omTtgdData[i].Grsn);
			if(ilIdx != -1)
			{
				m_Ttgd.SetCurSel(ilIdx);
				blFound = true;
			}
			else
			{
				m_Ttgd.SetCurSel(-1);
			}
		}
	}
	if(blFound == false)
	{
		m_Ttgd.SetCurSel(-1);
	}
	blFound = false;
//----------------
	for(i = 0; i < omHtgaData.GetSize(); i++)
	{
		if(omHtgaData[i].Urno == atoi(pomCurrentGhp->Htga))
		{
			ilIdx = m_Htga.FindString(-1, omHtgaData[i].Grpn);
			//ilIdx = m_Htga.FindString(-1, omHtgaData[i].Grsn);
			if(ilIdx != -1)
			{
				m_Htga.SetCurSel(ilIdx);
				blFound = true;
			}
			else
			{
				m_Htga.SetCurSel(-1);
			}
		}
	}
	if(blFound == false)
	{
		m_Htga.SetCurSel(-1);
	}
	blFound = false;
//----------------
	for(i = 0; i < omHtgdData.GetSize(); i++)
	{
		if(omHtgdData[i].Urno == atoi(pomCurrentGhp->Htgd))
		{
			ilIdx = m_Htgd.FindString(-1, omHtgdData[i].Grpn);
			//ilIdx = m_Htgd.FindString(-1, omHtgdData[i].Grsn);
			if(ilIdx != -1)
			{
				m_Htgd.SetCurSel(ilIdx);
				blFound = true;
			}
			else
			{
				m_Htgd.SetCurSel(-1);
			}
		}
	}
	if(blFound == false)
	{
		m_Htgd.SetCurSel(-1);
	}
	blFound = false;
//----------------

	for(i = 0; i < omTrraData.GetSize(); i++)
	{
		if(omTrraData[i].Urno == atoi(pomCurrentGhp->Trra))
		{
			ilIdx = m_TRRA.FindString(-1, omTrraData[i].Grpn);
			//ilIdx = m_TRRA.FindString(-1, omTrraData[i].Grsn);
			if(ilIdx != -1)
			{
				m_TRRA.SetCurSel(ilIdx);
				blFound = true;
			}
			else
			{
				m_TRRA.SetCurSel(-1);
			}
		}
	}
	if(blFound == false)
	{
		m_TRRA.SetCurSel(-1);
	}
	blFound = false;
//----------------
	for(i = 0; i < omTrrdData.GetSize(); i++)
	{
		if(omTrrdData[i].Urno == atoi(pomCurrentGhp->Trrd))
		{
			ilIdx = m_TRRD.FindString(-1, omTrrdData[i].Grpn);
			//ilIdx = m_TRRD.FindString(-1, omTrrdData[i].Grsn);
			if(ilIdx != -1)
			{
				m_TRRD.SetCurSel(ilIdx);
				blFound = true;
			}
			else
			{
				m_TRRD.SetCurSel(-1);
			}
		}
	}
	if(blFound == false)
	{
		m_TRRD.SetCurSel(-1);
	}
	blFound = false;
	for(i = 0; i < omAtmData.GetSize(); i++)
	{
		if(omAtmData[i].Urno == atoi(pomCurrentGhp->Actm))
		{
			ilIdx = m_Actm.FindString(-1, omAtmData[i].Grpn);
			//ilIdx = m_Actm.FindString(-1, omAtmData[i].Grsn);
			if(ilIdx != -1)
			{
				m_Actm.SetCurSel(ilIdx);
				blFound = true;
			}
			else
			{
				m_Actm.SetCurSel(-1);
			}
		}
	}
	if(blFound == false)
	{
		m_Actm.SetCurSel(-1);
	}
	blFound = false;
	for(i = 0; i < omPcaaData.GetSize(); i++)
	{
		if(omPcaaData[i].Urno == atoi(pomCurrentGhp->Pcaa))
		{
			ilIdx = m_PCAA.FindString(-1, omPcaaData[i].Grpn);
			//ilIdx = m_PCAA.FindString(-1, omPcaaData[i].Grsn);
			if(ilIdx != -1)
			{
				m_PCAA.SetCurSel(ilIdx);
				blFound = true;
			}
			else
			{
				m_PCAA.SetCurSel(-1);
			}
		}
	}
	if(blFound == false)
	{
		m_PCAA.SetCurSel(-1);
	}
	blFound = false;
	for(i = 0; i < omPcadData.GetSize(); i++)
	{
		if(omPcadData[i].Urno == atoi(pomCurrentGhp->Pcad))
		{
			ilIdx = m_PCAD.FindString(-1, omPcadData[i].Grpn);
			//ilIdx = m_PCAD.FindString(-1, omPcadData[i].Grsn);
			if(ilIdx != -1)
			{
				m_PCAD.SetCurSel(ilIdx);
				blFound = true;
			}
			else
			{
				m_PCAD.SetCurSel(-1);
			}
		}
	}
	if(blFound == false)
	{
		m_PCAD.SetCurSel(-1);
	}
	blFound = false;
	m_Act5.SetWindowText(pomCurrentGhp->Act5);
	m_Flca.SetWindowText(pomCurrentGhp->Flca);
	m_Flcd.SetWindowText(pomCurrentGhp->Flcd);
	m_Flna.SetWindowText(pomCurrentGhp->Flna);
	m_Flnd.SetWindowText(pomCurrentGhp->Flnd);
	m_Flsa.SetWindowText(pomCurrentGhp->Flsa);
	m_Flsd.SetWindowText(pomCurrentGhp->Flsd);
	m_Htpa.SetWindowText(pomCurrentGhp->Htpa);
	m_Htpd.SetWindowText(pomCurrentGhp->Htpd);
	m_Orig.SetWindowText(pomCurrentGhp->Orig);
	m_PCAD.SetWindowText(pomCurrentGhp->Pcad);
	m_Prna.SetWindowText(pomCurrentGhp->Prna);
	m_Prsn.SetWindowText(pomCurrentGhp->Prsn);
	m_Regn.SetWindowText(pomCurrentGhp->Regn);

	char pclText[10]="";
	sprintf(pclText, "%d", pomCurrentGhp->Maxt);
	m_Maxt.SetWindowText(pclText);
	sprintf(pclText, "%d", pomCurrentGhp->Prio);
	m_Prio.SetWindowText(pclText);

	m_Rema.SetWindowText(pomCurrentGhp->Rema);
	m_Chgr.SetWindowText(pomCurrentGhp->Chgr);
	char pclTmp[300]="";
	m_Scca.SetWindowText(pomCurrentGhp->Scca);
	m_Sccd.SetWindowText(pomCurrentGhp->Sccd);
	m_Ttpa.SetWindowText(pomCurrentGhp->Ttpa);
	m_Ttpd.SetWindowText(pomCurrentGhp->Ttpd);
	m_Vi1a.SetWindowText(pomCurrentGhp->Vi1a);
	m_Vi1d.SetWindowText(pomCurrentGhp->Vi1d);

	if(pomCurrentGhp->Vpfr == TIMENULL)
	{
		m_Vpfr_Date.SetWindowText(CTime::GetCurrentTime().Format("%d.%m.%Y"));
		m_Vpfr_Time.SetWindowText(CTime::GetCurrentTime().Format("%H:%M"));
	}
	else
	{
		m_Vpfr_Date.SetWindowText(pomCurrentGhp->Vpfr.Format("%d.%m.%Y"));
		m_Vpfr_Time.SetWindowText(pomCurrentGhp->Vpfr.Format("%H:%M"));
	}

	if(pomCurrentGhp->Vpto == TIMENULL)
	{
		m_Vpto_Date.SetWindowText(CTime::GetCurrentTime().Format("%d.%m.%Y"));
		m_Vpto_Time.SetWindowText(CTime::GetCurrentTime().Format("%H:%M"));
	}
	else
	{
		m_Vpto_Date.SetWindowText(pomCurrentGhp->Vpto.Format("%d.%m.%Y"));
		m_Vpto_Time.SetWindowText(pomCurrentGhp->Vpto.Format("%H:%M"));
	}
	//Dopa und Dopd
	if(CString(pomCurrentGhp->Dopa).Find("1") != -1)
		m_Dopa1.SetCheck(1);
	else
		m_Dopa1.SetCheck(0);
	if(CString(pomCurrentGhp->Dopa).Find("2") != -1)
		m_Dopa2.SetCheck(1);
	else
		m_Dopa2.SetCheck(0);
	if(CString(pomCurrentGhp->Dopa).Find("3") != -1)
		m_Dopa3.SetCheck(1);
	else
		m_Dopa3.SetCheck(0);
	if(CString(pomCurrentGhp->Dopa).Find("4") != -1)
		m_Dopa4.SetCheck(1);
	else
		m_Dopa4.SetCheck(0);
	if(CString(pomCurrentGhp->Dopa).Find("5") != -1)
		m_Dopa5.SetCheck(1);
	else
		m_Dopa5.SetCheck(0);
	if(CString(pomCurrentGhp->Dopa).Find("6") != -1)
		m_Dopa6.SetCheck(1);
	else
		m_Dopa6.SetCheck(0);
	if(CString(pomCurrentGhp->Dopa).Find("7") != -1)
		m_Dopa7.SetCheck(1);
	else
		m_Dopa7.SetCheck(0);

	if(CString(pomCurrentGhp->Dopa).Find("1234567") != -1)
		m_Dopa_All.SetCheck(1);
	else
		m_Dopa_All.SetCheck(0);

	if(CString(pomCurrentGhp->Dopd).Find("1") != -1)
		m_Dopd1.SetCheck(1);
	else
		m_Dopd1.SetCheck(0);
	if(CString(pomCurrentGhp->Dopd).Find("2") != -1)
		m_Dopd2.SetCheck(1);
	else
		m_Dopd2.SetCheck(0);
	if(CString(pomCurrentGhp->Dopd).Find("3") != -1)
		m_Dopd3.SetCheck(1);
	else
		m_Dopd3.SetCheck(0);
	if(CString(pomCurrentGhp->Dopd).Find("4") != -1)
		m_Dopd4.SetCheck(1);
	else
		m_Dopd4.SetCheck(0);
	if(CString(pomCurrentGhp->Dopd).Find("5") != -1)
		m_Dopd5.SetCheck(1);
	else
		m_Dopd5.SetCheck(0);
	if(CString(pomCurrentGhp->Dopd).Find("6") != -1)
		m_Dopd6.SetCheck(1);
	else
		m_Dopd6.SetCheck(0);
	if(CString(pomCurrentGhp->Dopd).Find("7") != -1)
		m_Dopd7.SetCheck(1);
	else
		m_Dopd7.SetCheck(0);

	if(CString(pomCurrentGhp->Dopd).Find("1234567") != -1)
		m_Dopd_All.SetCheck(1);
	else
		m_Dopd_All.SetCheck(0);

	CString olLstu;
	if(pomCurrentGhp->Lstu != TIMENULL)
		olLstu = pomCurrentGhp->Lstu.Format("%d.%m.%Y - %H:%M");
	else if(pomCurrentGhp->Cdat != TIMENULL)
		olLstu = pomCurrentGhp->Cdat.Format("%d.%m.%Y - %H:%M");
	if(strcmp(pomCurrentGhp->Useu, "") != 0)
		olLstu += CString("  ") + CString(pomCurrentGhp->Useu);
	else if(strcmp(pomCurrentGhp->Usec, "") != 0)
		olLstu += CString("  ") + CString(pomCurrentGhp->Usec);
	m_Lstu.SetWindowText(olLstu);
	//MakeOsduList(pomCurrentGhp->Osdu);
}		


void PremisPage::OnAwRegn() 
{
	CString olRegn;
	m_Regn.GetWindowText(olRegn);
	char pclRegn[20]="";
	sprintf(pclRegn, "%s", olRegn);
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
	AcrTableDlg *polDlg = new AcrTableDlg(this, pclRegn);
	if(polDlg->DoModal() == IDOK)
	{
		CString olRegn;
		olRegn = polDlg->omRegn;
		m_Regn.SetWindowText(olRegn);
		m_Regn.SetFocus();
	}
	delete polDlg;
	
}

void PremisPage::OnAwAct5() 
{
	CString olAct5;
	m_Act5.GetWindowText(olAct5);
	char pclText[10]="";
	sprintf(pclText, "%s", olAct5);
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
	ActDlg *polDlg = new ActDlg(this, pclText);
	if(polDlg->DoModal() == IDOK)
	{
		CString olText;
		olText = polDlg->omAircraft;
		m_Act5.SetWindowText(olText);
	}
	delete polDlg;
}

static int CompareToPrco(const GHPDATA **e1, const GHPDATA **e2)
{
	int ilRet;
/*	ilRet = (int)(strcmp((**e1).Prco,(**e2).Prco)); 
	if(ilRet == 0)
	{
		ilRet = (int)(strcmp((**e1).Prsn,(**e2).Prsn));
	}
*/
	CString olS1 = CString((**e1).Prsn);
	CString olS2 = CString((**e2).Prsn);
	return olS1.Collate(olS2.GetBuffer(0));
	return ilRet;
}

void PremisPage::OnDopaAll() 
{
	if(m_Dopa_All.GetCheck() == 1) // Is checked
	{
		m_Dopa1.SetCheck(1);
		m_Dopa2.SetCheck(1);
		m_Dopa3.SetCheck(1);
		m_Dopa4.SetCheck(1);
		m_Dopa5.SetCheck(1);
		m_Dopa6.SetCheck(1);
		m_Dopa7.SetCheck(1);
	}
	pogDutyPreferences->SendMessage(WM_SETRELEASE);
}

void PremisPage::OnDopdAll() 
{

	if(m_Dopd_All.GetCheck() == 1) // Is checked
	{
		m_Dopd1.SetCheck(1);
		m_Dopd2.SetCheck(1);
		m_Dopd3.SetCheck(1);
		m_Dopd4.SetCheck(1);
		m_Dopd5.SetCheck(1);
		m_Dopd6.SetCheck(1);
		m_Dopd7.SetCheck(1);
	}
	pogDutyPreferences->SendMessage(WM_SETRELEASE);
}

void PremisPage::OnKillfocusFlca() 
{
		CheckMussFelder();
}

void PremisPage::OnKillfocusFlna() 
{
		CheckMussFelder();
}

void PremisPage::OnKillfocusFlsa() 
{
		CheckMussFelder();
}

void PremisPage::OnKillfocusAlma() 
{
		//CheckMussFelder();
}

void PremisPage::OnKillfocusOrig() 
{
		CheckMussFelder();
}

void PremisPage::OnKillfocusVi1a() 
{
	CheckMussFelder();
}

void PremisPage::OnKillfocusTrra() 
{
		//CheckMussFelder();
}

void PremisPage::OnKillfocusScca() 
{
		CheckMussFelder();
}

void PremisPage::OnKillfocusPcaa() 
{

		CheckMussFelder();
}

void PremisPage::OnKillfocusTtpa() 
{
	CheckMussFelder();
}

void PremisPage::OnKillfocusHtpa() 
{
	CheckMussFelder();
}

void PremisPage::OnKillfocusFlcd() 
{
	CheckMussFelder();
}

void PremisPage::OnKillfocusFlnd() 
{
	CheckMussFelder();
}

void PremisPage::OnKillfocusFlsd() 
{
	CheckMussFelder();
}

void PremisPage::OnKillfocusAlmd() 
{
	//	CheckMussFelder();
}

void PremisPage::OnKillfocusDest() 
{
	CheckMussFelder();
}

void PremisPage::OnKillfocusVi1d() 
{
	CheckMussFelder();
}

void PremisPage::OnKillfocusTrrd() 
{
	//CheckMussFelder();
}

void PremisPage::OnKillfocusSccd() 
{
	CheckMussFelder();
}

void PremisPage::OnKillfocusPcad() 
{
	CheckMussFelder();
}

void PremisPage::OnKillfocusTtpd() 
{
	CheckMussFelder();
}

void PremisPage::OnKillfocusHtpd() 
{
	CheckMussFelder();
}

void PremisPage::OnCancel()
{
	if(imMode == NEW_MODE)
	{
/*		CCSPtrArray<GPMDATA> olGpmList;
		if(pomCurrentGhp != NULL)
		{
			ogGpmData.GetGpmByRkey(olGpmList, pomCurrentGhp->Urno);
			int ilCount = olGpmList.GetSize();
			for(int i = 0; i < ilCount; i++)
			{
				GPMDATA *prlGpm = ogGpmData.GetGpmByUrno(olGpmList[i].Urno);
				if(prlGpm != NULL)
				{
					ogGpmData.DeleteInternal(prlGpm);
				}
			}
		}
*/
		//int ilCount = omOldGpmList.GetSize();
		//for(int i = 0; i < ilCount; i++)
		//{
		//	ogGpmData.InsertInternal(&omOldGpmList[i]);
		//}
	}
	if(imMode == UPDATE_MODE)
	{
		CCSPtrArray<GPMDATA> olGpmList;
		if(pomCurrentGhp != NULL)
		{
			ogGpmData.GetGpmByRkey(olGpmList, pomCurrentGhp->Urno);
			int ilCount = olGpmList.GetSize();
			for(int i = 0; i < ilCount; i++)
			{
				GPMDATA *prlGpm = ogGpmData.GetGpmByUrno(olGpmList[i].Urno);
				if(prlGpm != NULL)
				{
					ogGpmData.DeleteInternal(prlGpm, false);
				}
			}
		}
		int ilCount = omOldGpmList.GetSize();
		for(int i = 0; i < ilCount; i++)
		{
			GPMDATA *prlGpm = new GPMDATA;
			*prlGpm = omOldGpmList[i];
			ogGpmData.InsertInternal(prlGpm, false);
		}
	}
	omOldGpmList.DeleteAll();
}

void PremisPage::OnBarChanged()
{
	bmBarChanged = true;
	pogDutyPreferences->SendMessage(WM_SETRELEASE);
}
void PremisPage::OnBarDeleted()
{
	bmBarChanged = true;
	pogDutyPreferences->SendMessage(WM_SETRELEASE);
	pomCurrentGpm = NULL;
	SetDutyFields(NULL);
}

void PremisPage::LoadPerData()
{
//	omPerData.DeleteAll();
	if(pomCurrentGpm != NULL)
	{
		MakePerUrnoList(pomCurrentGpm->Perm);
	}
//	if(ogPerData.ReadSpecialData(&omPerData,"","URNO,PRMC",true) == true)
	{
		
		m_Perm.ResetContent();
		int ilCount = omCicGrpData.GetSize();
		omCicGrpData.Sort(CompareGroup);
		m_Perm.SetSel(-1);
		for(int i = 0; i < omCicGrpData.GetSize(); i++)
		{
			m_Perm.AddString(omCicGrpData[i].Grpn);
			for(int j = 0; j < omPerUrnos.GetSize(); j++)
			{
				if((UINT)omCicGrpData[i].Urno == omPerUrnos[j])
				{
					m_Perm.SetSel(i);
				}
			}
		}
/*
		m_Perm.ResetContent();
		int ilCount = omPerData.GetSize();
		omPerData.Sort(ComparePerData);
		m_Perm.SetSel(-1);
		for(int i = 0; i < omPerData.GetSize(); i++)
		{
			m_Perm.AddString(omPerData[i].Prmc);
			for(int j = 0; j < omPerUrnos.GetSize(); j++)
			{
				if((UINT)omPerData[i].Urno == omPerUrnos[j])
				{
					m_Perm.SetSel(i);
				}
			}
		}
*/
	}
}
void PremisPage::LoadPfcData()
{
//	omPfcData.DeleteAll();
	if(pomCurrentGpm != NULL)
	{
		MakePfcUrnoList(pomCurrentGpm->Pfkt);
	}
//	if(ogPfcData.ReadSpecialData(&omPfcData,"","URNO,FCTC",true) == true)
	{
		m_LkcoPerm.ResetContent();
		int ilCount = omPfcData.GetSize();
		omPfcData.Sort(ComparePfcData);
		m_LkcoPerm.SetSel(-1);
		for(int i = 0; i < ilCount; i++)
		{
			m_LkcoPerm.AddString(omPfcData[i].Fctc);
			for(int j = 0; j < omPfcUrnos.GetSize(); j++)
			{
				long l = omPfcData[i].Urno;
				if((UINT)omPfcData[i].Urno == omPfcUrnos[j])
				{
					m_LkcoPerm.SetSel(i);
				}
			}
		}
	}
}
void PremisPage::LoadGegData()
{
//	omGegData.DeleteAll();
	if(pomCurrentGpm != NULL)
	{
		MakeGegUrnoList(pomCurrentGpm->Vrgc);
	}
//	if(ogGegData.ReadSpecialData(&omGegData,"","URNO,GCDE",true) == true)
	{
		m_LkcoPerm.ResetContent();
		int ilCount = omGegData.GetSize();
		omGegData.Sort(CompareGegData);
		m_LkcoPerm.SetSel(-1);
		for(int i = 0; i < ilCount; i++)
		{
			m_LkcoPerm.AddString(omGegData[i].Gcde);
			for(int j = 0; j < omGegUrnos.GetSize(); j++)
			{
				if((UINT)omGegData[i].Urno == omGegUrnos[j])
				{
					m_LkcoPerm.SetSel(i);
				}
			}
		}
	}
}

static int ComparePerData(const PERDATA **e1, const PERDATA **e2)
{
	return (strcmp((**e1).Prmc, (**e2).Prmc));
}

static int ComparePrcData(const PRCDATA **e1, const PRCDATA **e2)
{
	char pclRedu1[20]="";
	char pclRedu2[20]="";
	sprintf(pclRedu1, "%s%02ld", (**e1).Redc, (**e1).Redl);
	sprintf(pclRedu2, "%s%02ld", (**e2).Redc, (**e2).Redl);
	return (strcmp(pclRedu1, pclRedu2));
}

static int ComparePfcData(const PFCDATA **e1, const PFCDATA **e2)
{
	return (strcmp((**e1).Fctc, (**e2).Fctc));
}

static int CompareGegData(const GEGDATA **e1, const GEGDATA **e2)
{
	return (strcmp((**e1).Gcde, (**e2).Gcde));
}

static int CompareGroup(const GRNDATA **e1, const GRNDATA **e2)
{
		return (strcmp((**e1).Grpn, (**e2).Grpn));
}

static int CompareGpm(const GPMDATA **e1, const GPMDATA **e2)
{
	GHSDATA *prlGhs1 = ogGhsData.GetGhsByUrno((**e1).Ghsu);
	GHSDATA *prlGhs2 = ogGhsData.GetGhsByUrno((**e2).Ghsu);
	if(prlGhs2 != NULL && prlGhs1 != NULL)
	{
		return (strcmp((**e1).Lkco, (**e2).Lkco));
	}
	else
	{
		return 0;
	}
}
void PremisPage::MakeOsduList(char *pspVrgc)
{
	CString olUrno;
	CString olSubString = pspVrgc;
	//m_Osdu.ResetContent();
	bool blEnd = false;
	if(!olSubString.IsEmpty())
	{
		int pos;
		int olPos = 0;
		while(blEnd == false)
		{
			pos = olSubString.Find(';');
			if(pos == -1)
			{
				blEnd = true;
				olUrno = olSubString;
			}
			else
			{
				olUrno = olSubString.Mid(0, olSubString.Find(';'));
				olSubString = olSubString.Mid(olSubString.Find(';')+1, olSubString.GetLength( )-olSubString.Find(';')+1);
			}
			GHSDATA *prlGhs = ogGhsData.GetGhsByUrno(atoi(olUrno));
			if(prlGhs != NULL)
			{
				//m_Osdu.AddString(prlGhs->Lknm);
			}
			//omPerUrnos.Add(atoi(olUrno));
		}
	}
}


void PremisPage::MakePerUrnoList(char *pspVrgc)
{
	CString olUrno;
	CString olSubString = pspVrgc;
	omPerUrnos.RemoveAll();
	CStringArray olList;
	ExtractItemList(pspVrgc, &olList, ';');
	for(int i = 0; i < olList.GetSize(); i++)
	{
		omPerUrnos.Add(atoi(olList[i].GetBuffer(0)));
	}
}

void PremisPage::MakePfcUrnoList(char *pspVrgc)
{
	CString olUrno;
	CString olSubString = pspVrgc;
	omPfcUrnos.RemoveAll();
	bool blEnd = false;
	if(!olSubString.IsEmpty())
	{
		int pos;
		int olPos = 0;
		while(blEnd == false)
		{
			pos = olSubString.Find(';');
			if(pos == -1)
			{
				blEnd = true;
				olUrno = olSubString;
			}
			else
			{
				olUrno = olSubString.Mid(0, olSubString.Find(';'));
				olSubString = olSubString.Mid(olSubString.Find(';')+1, olSubString.GetLength( )-olSubString.Find(';')+1);
			}
			omPfcUrnos.Add(atoi(olUrno));
		}
	}
}
void PremisPage::MakeGegUrnoList(char *pspVrgc)
{
	CString olUrno;
	CString olSubString = pspVrgc;
	omGegUrnos.RemoveAll();
	bool blEnd = false;
	if(!olSubString.IsEmpty())
	{
		int pos;
		int olPos = 0;
		while(blEnd == false)
		{
			pos = olSubString.Find(';');
			if(pos == -1)
			{
				blEnd = true;
				olUrno = olSubString;
			}
			else
			{
				olUrno = olSubString.Mid(0, olSubString.Find(';'));
				olSubString = olSubString.Mid(olSubString.Find(';')+1, olSubString.GetLength( )-olSubString.Find(';')+1);
			}
			omGegUrnos.Add(atoi(olUrno));
		}
	}
}

void PremisPage::OnSelchangeLkcoPermVrgc() 
{
	if(pomCurrentGpm != NULL)
	{
		int *pilItems = new int[m_LkcoPerm.GetCount()];
		CString olUrno;
		int ilC = m_LkcoPerm.GetSelItems( m_LkcoPerm.GetCount(),  pilItems);
		for(int k = 0; k < ilC; k++)
		{
			char pclUrno[300];
			if(strcmp(pomCurrentGpm->Rtyp, "P") == 0)
			{
				sprintf(pclUrno, "%ld;", omPfcData[pilItems[k]].Urno);
			}
			else if(strcmp(pomCurrentGpm->Rtyp, "G") == 0)
			{
				sprintf(pclUrno, "%ld;", omGegData[pilItems[k]].Urno);
			}
			olUrno += CString(pclUrno);
		}
		olUrno = olUrno.Mid(0, olUrno.GetLength()-1);
		if(strcmp(pomCurrentGpm->Rtyp, "P") == 0)
		{
			strcpy(pomCurrentGpm->Pfkt, olUrno);
		}
		else if(strcmp(pomCurrentGpm->Rtyp, "G") == 0)
		{
			strcpy(pomCurrentGpm->Vrgc, olUrno);
		}
		if(ogGpmData.IsDataChanged(&rmOldGpm, pomCurrentGpm))
		{
			bmBarChanged = true;
			pogDutyPreferences->SendMessage(WM_SETRELEASE);
		}
		ogGpmData.UpdateInternal(pomCurrentGpm);
	}
}

void PremisPage::OnSelchangePerm() 
{
	if(pomCurrentGpm != NULL)
	{
		int *pilPermItems = new int[m_Perm.GetCount()];
		CString olPerm;
		int ilC = m_Perm.GetSelItems( m_Perm.GetCount(),  pilPermItems);
		for(int k = 0; k < ilC; k++)
		{
			char pclPerm[300];
			sprintf(pclPerm, "%ld;", omCicGrpData[pilPermItems[k]].Urno);
			olPerm += CString(pclPerm);
		}
		olPerm = olPerm.Mid(0, olPerm.GetLength()-1);
		strcpy(pomCurrentGpm->Perm, olPerm);
		if(ogGpmData.IsDataChanged(&rmOldGpm, pomCurrentGpm))
		{
			bmBarChanged = true;
			pogDutyPreferences->SendMessage(WM_SETRELEASE);
		}
		CCSPtrArray<GPMDATA> olGpmList;
		ogGpmData.GetGpmByRkey(olGpmList, pomCurrentGpm->Rkey);
		for(int i = 0; i < olGpmList.GetSize(); i++)
		{
			GPMDATA *prlGpm = ogGpmData.GetGpmByUrno(olGpmList[i].Urno);
			if(prlGpm != NULL)
			{
				if(prlGpm->IsChanged != DATA_DELETED)//MWO XXX
				{
					strcpy(prlGpm->Perm, olPerm);
					ogGpmData.UpdateInternal(prlGpm);
				}
			}
		}
		delete pilPermItems;
	}//end if(pomCurrentGpm != NULL)
}



void PremisPage::OnSelchangeAlma() 
{
		CheckMussFelder();
}

void PremisPage::OnSelchangeActm() 
{
		CheckMussFelder();
}

void PremisPage::OnSelchangeAlmd() 
{
		CheckMussFelder();
}

void PremisPage::OnSelchangeTrra() 
{
		CheckMussFelder();
}

void PremisPage::OnSelchangeTrrd() 
{
		CheckMussFelder();
}

void PremisPage::OnSelchangePcaa() 
{
		CheckMussFelder();
}

void PremisPage::OnSelchangePcad() 
{
		CheckMussFelder();
}

void PremisPage::OnDopa2() 
{
	pogDutyPreferences->SendMessage(WM_SETRELEASE);
}

void PremisPage::OnDopa1() 
{
	pogDutyPreferences->SendMessage(WM_SETRELEASE);
}

void PremisPage::OnDopa3() 
{
	pogDutyPreferences->SendMessage(WM_SETRELEASE);
}

void PremisPage::OnDopa4() 
{
	pogDutyPreferences->SendMessage(WM_SETRELEASE);
}

void PremisPage::OnDopa5() 
{
	pogDutyPreferences->SendMessage(WM_SETRELEASE);
}

void PremisPage::OnDopa6() 
{
	pogDutyPreferences->SendMessage(WM_SETRELEASE);
}

void PremisPage::OnDopa7() 
{
	pogDutyPreferences->SendMessage(WM_SETRELEASE);
}

void PremisPage::OnDopd1() 
{
	pogDutyPreferences->SendMessage(WM_SETRELEASE);
}

void PremisPage::OnDopd2() 
{
	pogDutyPreferences->SendMessage(WM_SETRELEASE);
}

void PremisPage::OnDopd3() 
{
	pogDutyPreferences->SendMessage(WM_SETRELEASE);
}

void PremisPage::OnDopd4() 
{
	pogDutyPreferences->SendMessage(WM_SETRELEASE);
}

void PremisPage::OnDopd5() 
{
	pogDutyPreferences->SendMessage(WM_SETRELEASE);
}

void PremisPage::OnDopd6() 
{
	pogDutyPreferences->SendMessage(WM_SETRELEASE);
}

void PremisPage::OnDopd7() 
{
	pogDutyPreferences->SendMessage(WM_SETRELEASE);
}

void PremisPage::OnChangeMaxt() 
{
	CheckMussFelder();
}

void PremisPage::OnChangePrsn() 
{
	CheckMussFelder();
}

void PremisPage::OnChangePrna() 
{
	CheckMussFelder();
}

void PremisPage::OnChangeVpfrDate() 
{
	CheckMussFelder();
}

void PremisPage::OnChangeVpfrTime() 
{
	CheckMussFelder();
}

void PremisPage::OnChangeVptoDate() 
{
	CheckMussFelder();
}

void PremisPage::OnChangeVptoTime() 
{
	CheckMussFelder();
}

void PremisPage::OnSelchangePrco() 
{
	//CheckMussFelder();
}

void PremisPage::UpdateAllTurnarrounds()
{
	CCSPtrArray<GPMDATA> olGpmList;
	ogGpmData.GetGpmByRkey(olGpmList, pomCurrentGhp->Urno);
	int ilCount = olGpmList.GetSize();
	for(int i = 0; i < ilCount; i++)
	{
		GPMDATA *prlGpm = ogGpmData.GetGpmByUrno(olGpmList[i].Urno);
		if(prlGpm != NULL)
		{
			if(prlGpm->IsChanged != DATA_DELETED ) //MWO XXX
			{
				if(strcmp(prlGpm->Dtyp, "T") == 0)
				{
					if(pomCurrentGhp->Maxt < prlGpm->Dtim)
					{
						prlGpm->Dtim = pomCurrentGhp->Maxt;
						bmBarChanged = true;
						pogDutyPreferences->SendMessage(WM_SETRELEASE);
						ogGpmData.UpdateInternal(prlGpm);
					}
				}
			}
		}
	}
	if(pomCurrentGpm != NULL)
	{
		SetDutyFields(pomCurrentGpm);
	}
}


void PremisPage::OnSelchangeTtga() 
{
	CheckMussFelder();
}

void PremisPage::OnSelchangeTtgd() 
{
	CheckMussFelder();
}

void PremisPage::OnSelchangeHtga() 
{
	CheckMussFelder();
}

void PremisPage::OnSelchangeHtgd() 
{
	CheckMussFelder();
}
void PremisPage::CreateGpmFromGhsList(CWnd *popWnd, CDWordArray &ropGhsUrnos, GHPDATA *prpGhp,
								   CString opDisp)
{
	int ilGhsCount = ropGhsUrnos.GetSize();
	if(prpGhp != NULL)
	{
		for(int i = 0; i < ilGhsCount; i++)
		{
			GHSDATA *prlGhs = ogGhsData.GetGhsByUrno(ropGhsUrnos[i]);
			if(prlGhs != NULL)
			{
				if(prlGhs->Lkam > 1)
				{
					for(int j = 0; j < prlGhs->Lkam; j++)
					{
						GPMDATA *prlGpm = new GPMDATA;
						prlGpm->IsChanged = DATA_NEW;
						prlGpm->Urno = ogBasicData.GetNextUrno();
						strcpy(prlGpm->Usec, ogBasicData.omUserID);
						prlGpm->Rkey = prpGhp->Urno;
						prlGpm->Noma = 1;
						strcpy(prlGpm->Disp, opDisp);
						prlGpm->Ghsu = prlGhs->Urno;
						prlGpm->Prta = prlGhs->Lkvb;
						prlGpm->Ttdu = 0;
						if(strcmp(prlGhs->Lkbz,"T") == 0)
						{
							if(prpGhp->Maxt < prlGhs->Lkda)
							{
								prlGpm->Dtim = prpGhp->Maxt;
							}
							else
							{
								prlGpm->Dtim = prlGhs->Lkda;
							}
						}
						else
						{
							prlGpm->Dtim = prlGhs->Lkda;
						}
						prlGpm->Tfdu = 0;
						strcpy(prlGpm->Rtyp, prlGhs->Lkty);
						strcpy(prlGpm->Lkco, prlGhs->Lkcc);
						prlGpm->Pota = prlGhs->Lknb;
						strcpy(prlGpm->Perm, prlGhs->Perm);
						if(strcmp(prlGpm->Rtyp, "P") == 0)
						{
							strcpy(prlGpm->Pfkt, prlGhs->Vrgc);
						}
						else if(strcmp(prlGpm->Rtyp, "G") == 0)
						{
							strcpy(prlGpm->Vrgc, prlGhs->Vrgc);
						}
						//TO DO Holger Fragen, ob nicht MA-Fkts auch in Ghs erfasst werden
						//strcpy(prlGpm->Pfkt, "");
						prlGpm->Etot = 0;
						prlGpm->Prio = 0;
						prlGpm->Didu = 0;
						strcpy(prlGpm->Duch, "J");
						strcpy(prlGpm->Dtyp, prlGhs->Lkbz);
						ogGpmData.InsertInternal(prlGpm);
					}
				}
				else
				{
					GPMDATA *prlGpm = new GPMDATA;
					prlGpm->IsChanged = DATA_NEW;
					prlGpm->Urno = ogBasicData.GetNextUrno();
					strcpy(prlGpm->Usec, ogBasicData.omUserID);
					prlGpm->Noma = 0;
					if(prlGhs->Lkam == 1)
					{
						prlGpm->Noma = 1;
					}
					strcpy(prlGpm->Disp, opDisp);
					prlGpm->Rkey = prpGhp->Urno;
					prlGpm->Ghsu = prlGhs->Urno;
					prlGpm->Prta = prlGhs->Lkvb;
					prlGpm->Ttdu = 0;
					if(strcmp(prlGhs->Lkbz,"T") == 0)
					{
						if(prpGhp->Maxt < prlGhs->Lkda)
						{
							prlGpm->Dtim = prpGhp->Maxt;
						}
						else
						{
							prlGpm->Dtim = prlGhs->Lkda;
						}
					}
					else
					{
						prlGpm->Dtim = prlGhs->Lkda;
					}
					prlGpm->Tfdu = 0;
					strcpy(prlGpm->Rtyp, prlGhs->Lkty);
					strcpy(prlGpm->Lkco, prlGhs->Lkcc);
					prlGpm->Pota = prlGhs->Lknb;
					strcpy(prlGpm->Perm, prlGhs->Perm);
					if(strcmp(prlGpm->Rtyp, "P") == 0)
					{
						strcpy(prlGpm->Pfkt, prlGhs->Vrgc);
					}
					else if(strcmp(prlGpm->Rtyp, "G") == 0)
					{
						strcpy(prlGpm->Vrgc, prlGhs->Vrgc);
					} 
					//TO DO Holger Fragen, ob nicht MA-Fkts auch in Ghs erfasst werden
					//strcpy(prlGpm->Pfkt, "");
					prlGpm->Etot = 0;
					prlGpm->Prio = 0;
					prlGpm->Didu = 0;
					strcpy(prlGpm->Duch, "J");
					strcpy(prlGpm->Dtyp, prlGhs->Lkbz);
					ogGpmData.InsertInternal(prlGpm);
				}
			}
		}
	}
}
void PremisPage::OnChar(UINT nChar, UINT nRepCnt, UINT nFlags) 
{
	// TODO: Add your message handler code here and/or call default
	
	CPropertyPage::OnChar(nChar, nRepCnt, nFlags);
}

void PremisPage::OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags) 
{
	// TODO: Add your message handler code here and/or call default
	
	CPropertyPage::OnKeyDown(nChar, nRepCnt, nFlags);
}


