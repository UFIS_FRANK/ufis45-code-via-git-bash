// GatRuleDlg.cpp : implementation file
//

#include "stdafx.h"
#include "Rules.h"
#include "GatRuleDlg.h"
#include "BasicDAta.h"
#include "CedaAltData.h"
#include "CedaAPTData.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// GatRuleDlg dialog


GatRuleDlg::GatRuleDlg(CWnd* pParent, GATDATA *popGat)
	: CDialog(GatRuleDlg::IDD, pParent)
{
	pomGat = popGat;
	//{{AFX_DATA_INIT(GatRuleDlg)
	m_Flti = _T("");
	m_Inbo = FALSE;
	m_Maxp = 0;
	m_Mult = 0;
	m_Natr = _T("");
	m_Outb = FALSE;
	m_Pral = _T("");
	m_Prio = 0;
	m_PDest = _T("");
	//}}AFX_DATA_INIT

	CStringArray olArray;
//	CString olStr = CString(pomGat->Resb);
	
	CString olTmpGatr = pomGat->Gatr;
	if(olTmpGatr.Replace(";","#") < 2)
	{
		olTmpGatr =pomGat->Gatr;
		olTmpGatr.Replace("/",";");
		strcpy(pomGat->Gatr, olTmpGatr.GetBuffer(0));
	}
	
	CString olStr = CString(pomGat->Gatr);
	ExtractItemList(olStr, &olArray, ';');
	if(olArray.GetSize() == 9)
	{
		m_Flti = olArray[0];//_T("");
		m_Maxp = atoi(olArray[1]);//0;
		m_Mult = atoi(olArray[2]);//0;
		if(olArray[3] == "X")
		{
			m_Inbo = TRUE;//FALSE;
		}
		if(olArray[4] == "X")
		{
			m_Outb = TRUE;//FALSE;
		}

		m_Prio = atoi(olArray[5]);//0;
		m_Natr = olArray[6];//_T("");
		m_Pral = olArray[7];//_T("");
		m_PDest = olArray[8];//_T("");
	}
}


void GatRuleDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(GatRuleDlg)
	DDX_Control(pDX, IDC_PRIO, m_PrioCtrl);
	DDX_Control(pDX, IDC_MULT, m_MultCtrl);
	DDX_Control(pDX, IDC_MAXP, m_MaxPCtrl);
	DDX_Text(pDX, IDC_FLTI, m_Flti);
	DDX_Check(pDX, IDC_INBO, m_Inbo);
	DDX_Text(pDX, IDC_MAXP, m_Maxp);
	DDV_MinMaxInt(pDX, m_Maxp, 0, 999);
	DDX_Text(pDX, IDC_MULT, m_Mult);
	DDV_MinMaxInt(pDX, m_Mult, 0, 99);
	DDX_Text(pDX, IDC_NATR, m_Natr);
	DDV_MaxChars(pDX, m_Natr, 150);
	DDX_Check(pDX, IDC_OUTB, m_Outb);
	DDX_Text(pDX, IDC_PRAL, m_Pral);
	DDV_MaxChars(pDX, m_Pral, 150);
	DDX_Text(pDX, IDC_PRIO, m_Prio);
	DDV_MinMaxInt(pDX, m_Prio, 0, 9);
	DDX_Text(pDX, IDC_PDEST, m_PDest);
	DDV_MaxChars(pDX, m_PDest, 150);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(GatRuleDlg, CDialog)
	//{{AFX_MSG_MAP(GatRuleDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// GatRuleDlg message handlers

BOOL GatRuleDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	UpdateData(FALSE);
	CString olCaption;
	olCaption.Format(GetString(IDS_STRING1435), pomGat->Gnam, pomGat->Term);
	SetWindowText(olCaption);
	// TODO: Add extra initialization here
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void GatRuleDlg::OnOK() 
{

	CString olValues;
	CString olTmp;
	CString olStr;
	CString olMsg;
	CString olTmpTxt;
	m_MaxPCtrl.GetWindowText(olTmpTxt);
	
	int ilLength = olTmpTxt.GetLength();
	bool blIsValid = true;
	if(ilLength > 3 || ilLength == 0)
		blIsValid = false;
	for (int illc = 0;(illc < ilLength) && blIsValid;illc++)
	{
		CString olDummy = olTmpTxt[illc];
		int ilfsc = atoi(olDummy);
		if((atoi(olDummy) == 0) && (olDummy.Find("0") < 0))
		{
			blIsValid = false;
		}
	}

	if(	!blIsValid )
	{
		MessageBox(GetString(IDS_STRING1441), GetString(IDS_STRING117), MB_OK);
		m_MaxPCtrl.SetFocus();
		return;
	}

	m_PrioCtrl.GetWindowText(olTmpTxt);

	ilLength = olTmpTxt.GetLength();
	blIsValid = true;
	if(ilLength > 1 || ilLength == 0)
		blIsValid = false;
	for (illc = 0;(illc < ilLength) && blIsValid;illc++)
	{
		CString olDummy = olTmpTxt[illc];
		int ilfsc = atoi(olDummy);
		if((atoi(olDummy) == 0) && (olDummy.Find("0") < 0))
		{
			blIsValid = false;
		}
	}

	if(	!blIsValid )
	{
		MessageBox(GetString(IDS_STRING1443), GetString(IDS_STRING117), MB_OK);
		m_PrioCtrl.SetFocus();
		return;
	}
	
	m_MultCtrl.GetWindowText(olTmpTxt);

	ilLength = olTmpTxt.GetLength();
	blIsValid = true;
	if(ilLength > 2 || ilLength == 0)
		blIsValid = false;
	for (illc = 0;(illc < ilLength) && blIsValid;illc++)
	{
		CString olDummy = olTmpTxt[illc];
		int ilfsc = atoi(olDummy);
		if((atoi(olDummy) == 0) && (olDummy.Find("0") < 0))
		{
			blIsValid = false;
		}
	}

	if(	!blIsValid )
	{
		MessageBox(GetString(IDS_STRING1442), GetString(IDS_STRING117), MB_OK);
		m_MultCtrl.SetFocus();
		return;
	}
	
	

	UpdateData(TRUE);

	olValues+=m_Flti+";";
	//m_Flti = olArray[0];//_T("");
	olStr.Format("%d", m_Maxp);
	olValues+=olStr+";";
	//m_Maxp = atoi(olArray[1]);//0;
	olStr.Format("%d", m_Mult);
	olValues+=olStr+";";
	//m_Mult = atoi(olArray[2]);//0;
	if(m_Inbo == TRUE)
	{
		olValues+= CString("X") + ";";
		//m_Inbo = TRUE;//FALSE;
	}
	else
	{
		olValues+= CString(" ") + ";";
	}
	if(m_Outb == TRUE)
	{
		olValues+= CString("X") + ";";
		//m_Outb = TRUE;//FALSE;
	}
	else
	{
		olValues+= CString(" ") + ";";
	}

	olStr.Format("%d", m_Prio);
	olValues+=olStr+";";
//	m_Prio = atoi(olArray[5]);//0;
	olValues+=m_Natr+";";
//	m_Natr = olArray[6];//_T("");
	CStringArray olArray;
	ExtractItemList(m_Pral, &olArray, ' ');

	/*
	for(int i = 0; i < olArray.GetSize(); i++)
	{
		ALTDATA *prlAlt = ogAltData.GetAltByAlc2(olArray[i].GetBuffer(0));
		if(prlAlt == NULL)
		{
			prlAlt = ogAltData.GetAltByAlc3(olArray[i].GetBuffer(0));
			if(prlAlt == NULL)
			{
				olTmp.Format(GetString(IDS_STRING1430), olArray[i].GetBuffer(0));
				olMsg+=olTmp;
			}
		}
	}
	*/



	CString olTmp2;	
	
	for(int i = 0; i < olArray.GetSize(); i++)
	{
		olTmp2 = olArray[i].GetBuffer(0);

		if(olTmp2.GetLength() > 1)
		{
			if(olTmp2[0] == '!' || olTmp2[0] == '=')
				olTmp2 = olTmp2.Right(olTmp2.GetLength() - 1);


			ALTDATA *prlAlt = ogAltData.GetAltByAlc2(olTmp2.GetBuffer(0));
			if(prlAlt == NULL)
			{
				prlAlt = ogAltData.GetAltByAlc3(olTmp2.GetBuffer(0));
				if(prlAlt == NULL)
				{
					olTmp.Format(GetString(IDS_STRING1430), olTmp2);
					olMsg+=olTmp;
				}
			}
		}
	}



	olValues+=m_Pral+";";


	ExtractItemList(m_PDest, &olArray, ' ');


	for(i = 0; i < olArray.GetSize(); i++)
	{
		olTmp2 = olArray[i].GetBuffer(0);

		if(olTmp2.GetLength() > 1)
		{
			if(olTmp2[0] == '!' || olTmp2[0] == '=')
				olTmp2 = olTmp2.Right(olTmp2.GetLength() - 1);


			if(ogAptData.GetUrnoByApc(olTmp2.GetBuffer(0)) == 0)
			{
				olTmp.Format(GetString(IDS_STRING1456), olTmp2);
				olMsg+=olTmp;
			}
		}
	}


/*
	char clPDest[3];

	strcpy(clPDest, m_PDest);

	if (m_PDest.GetLength() > 0 && ogAptData.GetUrnoByApc(clPDest) == 0)
	{			
		olTmp.Format(GetString(IDS_STRING1456),m_PDest );
		olMsg+=olTmp;
	}
*/
	olValues+=m_PDest;

//	m_Pral = olArray[7];//_T("");

	if(olMsg.IsEmpty())
	{
//		strcpy(pomGat->Resb, olValues.GetBuffer(0));
		strcpy(pomGat->Gatr, olValues.GetBuffer(0));
		pomGat->IsChanged = DATA_CHANGED;
		ogGatData.SaveGAT( pomGat);
		CDialog::OnOK();
	}
	else
	{
		MessageBox(olMsg, GetString(IDS_STRING117), MB_OK);
	}
}
