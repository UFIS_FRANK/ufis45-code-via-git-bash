
// cJobd.cpp - Class for handling Job data
//

#include "stdafx.h"
#include <afxwin.h>
#include "resource.h"
#include "CCSGlobl.h"
#include "CCSCedaData.h"
#include "CedaGhpData.h"
#include "ccsddx.h"
#include "CCSBcHandle.h"



void ProcessGhpCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

CedaGhpData ogGhpData;

CedaGhpData::CedaGhpData()
{   
    BEGIN_CEDARECINFO(GHPDATA, GhpDataRecInfo)
		CCS_FIELD_LONG(Urno, "URNO", "Eindeutige Datensatz-Nr.", 1)
		CCS_FIELD_DATE(Cdat, "CDAT", "Erstellungsdatum", 1)
		CCS_FIELD_CHAR_TRIM(Usec, "USEC", "Anwender (Ersteller)", 1)
		CCS_FIELD_DATE(Lstu, "LSTU", "Datum letzte �nderung", 1)
		CCS_FIELD_CHAR_TRIM(Prfl, "PRFL", "Protokollierungskennung", 1)
		CCS_FIELD_CHAR_TRIM(Useu, "USEU", "Anwender (letzte �nderung)", 1)
		CCS_FIELD_DATE(Vpfr, "VAFR", "G�ltig ab", 1)
		CCS_FIELD_DATE(Vpto, "VATO", "G�ltig bis", 1)
		CCS_FIELD_CHAR_TRIM(Prna, "PRNA", "Name (Bezeichnung)", 1)
		CCS_FIELD_CHAR_TRIM(Prsn, "PRSN", "Name (Kurzname)", 1)
		CCS_FIELD_CHAR_TRIM(Prco, "PRCO", "Code (Reduktionsstufe)", 1)
		CCS_FIELD_CHAR_TRIM(Regn, "REGN", "LFZ-Kennzeichen", 1)
		CCS_FIELD_CHAR_TRIM(Act5, "ACT5", "Flugzeugtyp", 1)
		CCS_FIELD_CHAR_TRIM(Actm, "ACTM", "Gruppenwert Flugzeugtyp", 1)
		CCS_FIELD_CHAR_TRIM(Flca, "FLCA", "Fluggesellschaft Ankunft", 1)
		CCS_FIELD_CHAR_TRIM(Flna, "FLNA", "Flugnummer Ankunft", 1)
		CCS_FIELD_CHAR_TRIM(Flsa, "FLSA", "Flugnummer Suffix Ankunft", 1)
		CCS_FIELD_CHAR_TRIM(Alma, "ALMA", "Gruppenwert Fluggesellschaft", 1)
		CCS_FIELD_CHAR_TRIM(Orig, "ORIG", "Ausgangsflughafen", 1)
		CCS_FIELD_CHAR_TRIM(Vi1a, "VI1A", "Zwischenflughafen Ankunft", 1)
		CCS_FIELD_CHAR_TRIM(Trra, "TRRA", "Verkehrsgebiet Ankunft", 1)
		CCS_FIELD_CHAR_TRIM(Scca, "SCCA", "Anzahl Sektoren Ankunft", 1)
		CCS_FIELD_CHAR_TRIM(Pcaa, "PCAA", "Positionskategorie Ankunft", 1)
		CCS_FIELD_CHAR_TRIM(Ttpa, "TTPA", "Verkehrsart Ankunft", 1)
		CCS_FIELD_CHAR_TRIM(Htpa, "HTPA", "Abfertigungsart Ankunft", 1)
		CCS_FIELD_CHAR_TRIM(Dopa, "DOPA", "G�ltige Tage Ankunft", 1)
		CCS_FIELD_CHAR_TRIM(Flcd, "FLCD", "Fluggesellschaft Abflug", 1)
		CCS_FIELD_CHAR_TRIM(Flnd, "FLND", "Flugnummer Abflug", 1)
		CCS_FIELD_CHAR_TRIM(Flsd, "FLSD", "Flugnummer Suffix Abflug", 1)
		CCS_FIELD_CHAR_TRIM(Almd, "ALMD", "Gruppenwert Fluggesellschaft", 1)
		CCS_FIELD_CHAR_TRIM(Dest, "DEST", "Zielflughafen", 1)
		CCS_FIELD_CHAR_TRIM(Vi1d, "VI1D", "Zwischenflughafen Abflug", 1)
		CCS_FIELD_CHAR_TRIM(Trrd, "TRRD", "Verkehrsgebiet Abflug", 1)
		CCS_FIELD_CHAR_TRIM(Sccd, "SCCD", "Anzahl Sektoren Abflug", 1)
		CCS_FIELD_CHAR_TRIM(Pcad, "PCAD", "Positionskategorie Abflug", 1)
		CCS_FIELD_CHAR_TRIM(Ttpd, "TTPD", "Verkehrsart Abflug", 1)
		CCS_FIELD_CHAR_TRIM(Htpd, "HTPD", "Abfertigungsart Abflug", 1)
		CCS_FIELD_CHAR_TRIM(Dopd, "DOPD", "G�ltige Tage Abflug", 1)
		CCS_FIELD_CHAR_TRIM(Osdu, "OSDU", "Andere Standardleistungen (1-n)", 1)
		CCS_FIELD_CHAR_TRIM(Ssic, "SSIC", "Vertragliche Sonderleistungen (1-n)", 1)
		CCS_FIELD_CHAR_TRIM(Rema, "REMA", "Bemerkungen zur Regel", 1)
		CCS_FIELD_CHAR_TRIM(Chgr, "CHGR", "�nderungsmitteilung zur Regel", 1)
		CCS_FIELD_INT(		Maxt, "MAXT", "Maximale Einsatzzeit", 1)
		CCS_FIELD_INT(		Prio, "PRIO", "Priorit�t der Pr�misse", 1)
		CCS_FIELD_CHAR_TRIM(Ttga, "TTGA",  "Verkehrsart Gruppe Ankunft", 1)
		CCS_FIELD_CHAR_TRIM(Ttgd, "TTGD",  "Verkehrsart Gruppe Abflug", 1)
		CCS_FIELD_CHAR_TRIM(Htga, "HTGA",  "Abfertigungsart Gruppe Ankunft", 1)
		CCS_FIELD_CHAR_TRIM(Htgd, "HTGD",  "Abfertigungsart Gruppe Ankunft", 1)
		CCS_FIELD_CHAR_TRIM(Appl, "APPL",  "Applikation", 1)
		CCS_FIELD_CHAR_TRIM(Flia, "FLIA",  "Flug ID Arrival", 1)
		CCS_FIELD_CHAR_TRIM(Flid, "FLID",  "Flug ID Depatrue", 1)
	END_CEDARECINFO
/* Macro laesst kein CString zu MBR 11.01.1999
    BEGIN_CEDARECINFO(GHPDATA, GhpDataRecInfo)
		CCS_FIELD_LONG(Urno, "URNO", GetString(IDS_STRING162), 1)
		CCS_FIELD_DATE(Cdat, "CDAT", GetString(IDS_STRING163), 1)
		CCS_FIELD_CHAR_TRIM(Usec, "USEC", GetString(IDS_STRING164), 1)
		CCS_FIELD_DATE(Lstu, "LSTU", GetString(IDS_STRING165), 1)
		CCS_FIELD_CHAR_TRIM(Prfl, "PRFL", GetString(IDS_STRING208), 1)
		CCS_FIELD_CHAR_TRIM(Useu, "USEU", GetString(IDS_STRING167), 1)
		CCS_FIELD_DATE(Vpfr, "VAFR", GetString(IDS_STRING234), 1)
		CCS_FIELD_DATE(Vpto, "VATO", GetString(IDS_STRING186), 1)
		CCS_FIELD_CHAR_TRIM(Prna, "PRNA", GetString(IDS_STRING235), 1)
		CCS_FIELD_CHAR_TRIM(Prsn, "PRSN", GetString(IDS_STRING236), 1)
		CCS_FIELD_CHAR_TRIM(Prco, "PRCO", GetString(IDS_STRING237), 1)
		CCS_FIELD_CHAR_TRIM(Regn, "REGN", GetString(IDS_STRING238), 1)
		CCS_FIELD_CHAR_TRIM(Act5, "ACT5", GetString(IDS_STRING239), 1)
		CCS_FIELD_CHAR_TRIM(Actm, "ACTM", GetString(IDS_STRING240), 1)
		CCS_FIELD_CHAR_TRIM(Flca, "FLCA", GetString(IDS_STRING241), 1)
		CCS_FIELD_CHAR_TRIM(Flna, "FLNA", GetString(IDS_STRING242), 1)
		CCS_FIELD_CHAR_TRIM(Flsa, "FLSA", GetString(IDS_STRING243), 1)
		CCS_FIELD_CHAR_TRIM(Alma, "ALMA", GetString(IDS_STRING244), 1)
		CCS_FIELD_CHAR_TRIM(Orig, "ORIG", GetString(IDS_STRING245), 1)
		CCS_FIELD_CHAR_TRIM(Vi1a, "VI1A", GetString(IDS_STRING246), 1)
		CCS_FIELD_CHAR_TRIM(Trra, "TRRA", GetString(IDS_STRING247), 1)
		CCS_FIELD_CHAR_TRIM(Scca, "SCCA", GetString(IDS_STRING248), 1)
		CCS_FIELD_CHAR_TRIM(Pcaa, "PCAA", GetString(IDS_STRING249), 1)
		CCS_FIELD_CHAR_TRIM(Ttpa, "TTPA", GetString(IDS_STRING250), 1)
		CCS_FIELD_CHAR_TRIM(Htpa, "HTPA", GetString(IDS_STRING251), 1)
		CCS_FIELD_CHAR_TRIM(Dopa, "DOPA", GetString(IDS_STRING252), 1)
		CCS_FIELD_CHAR_TRIM(Flcd, "FLCD", GetString(IDS_STRING253), 1)
		CCS_FIELD_CHAR_TRIM(Flnd, "FLND", GetString(IDS_STRING254), 1)
		CCS_FIELD_CHAR_TRIM(Flsd, "FLSD", GetString(IDS_STRING255), 1)
		CCS_FIELD_CHAR_TRIM(Almd, "ALMD", GetString(IDS_STRING256), 1)
		CCS_FIELD_CHAR_TRIM(Dest, "DEST", GetString(IDS_STRING257), 1)
		CCS_FIELD_CHAR_TRIM(Vi1d, "VI1D", GetString(IDS_STRING258), 1)
		CCS_FIELD_CHAR_TRIM(Trrd, "TRRD", GetString(IDS_STRING259), 1)
		CCS_FIELD_CHAR_TRIM(Sccd, "SCCD", GetString(IDS_STRING260), 1)
		CCS_FIELD_CHAR_TRIM(Pcad, "PCAD", GetString(IDS_STRING261), 1)
		CCS_FIELD_CHAR_TRIM(Ttpd, "TTPD", GetString(IDS_STRING262), 1)
		CCS_FIELD_CHAR_TRIM(Htpd, "HTPD", GetString(IDS_STRING263), 1)
		CCS_FIELD_CHAR_TRIM(Dopd, "DOPD", GetString(IDS_STRING264), 1)
		CCS_FIELD_CHAR_TRIM(Osdu, "OSDU", GetString(IDS_STRING265), 1)
		CCS_FIELD_CHAR_TRIM(Ssic, "SSIC", GetString(IDS_STRING266), 1)
		CCS_FIELD_CHAR_TRIM(Rema, "REMA", GetString(IDS_STRING267), 1)
		CCS_FIELD_CHAR_TRIM(Chgr, "CHGR", GetString(IDS_STRING268), 1)
		CCS_FIELD_INT(		Maxt, "MAXT", GetString(IDS_STRING269), 1)
		CCS_FIELD_INT(		Prio, "PRIO", GetString(IDS_STRING270), 1)
		CCS_FIELD_CHAR_TRIM(Ttga, "TTGA", GetString(IDS_STRING271), 1)
		CCS_FIELD_CHAR_TRIM(Ttgd, "TTGD", GetString(IDS_STRING272), 1)
		CCS_FIELD_CHAR_TRIM(Htga, "HTGA", GetString(IDS_STRING273), 1)
		CCS_FIELD_CHAR_TRIM(Htgd, "HTGD", GetString(IDS_STRING274), 1)
		CCS_FIELD_CHAR_TRIM(Appl, "APPL", GetString(IDS_STRING275), 1)
	END_CEDARECINFO
*/
    // Copy the record structure
    for (int i = 0; i < sizeof(GhpDataRecInfo)/sizeof(GhpDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&GhpDataRecInfo[i],sizeof(CEDARECINFO));
        omRecInfo.Add(prpCedaRecInfo);
	}
    // Initialize table names and field names
	strcpy(pcmTableName,"GHP");
	strcat(pcmTableName,pcgTableExt);
    sprintf(pcmListOfFields, "URNO,CDAT,USEC,LSTU,PRFL,USEU,VAFR,VATO,"
							 "PRNA,PRSN,PRCO,REGN,ACT5,ACTM,FLCA,FLNA,"
							 "FLSA,ALMA,ORIG,VI1A,TRRA,SCCA,PCAA,TTPA,"
							 "HTPA,DOPA,FLCD,FLND,FLSD,ALMD,DEST,VI1D,"
							 "TRRD,SCCD,PCAD,TTPD,HTPD,DOPD,OSDU,"
							 "SSIC,REMA,CHGR,MAXT,PRIO,TTGA,TTGD,HTGA,HTGD,APPL,FLIA,FLID");

	 pcmFieldList = pcmListOfFields; //because in CedaData pcmFieldList is just a pointer

	 ogDdx.Register((void *)this,BC_GHP_CHANGE,CString("BC_JOBCHANGE"), CString("JobDataChange"),ProcessGhpCf);
	 ogDdx.Register((void *)this,BC_GHP_NEW,CString("BC_JOBCHANGE"), CString("JobDataChange"),ProcessGhpCf);
	 ogDdx.Register((void *)this,BC_GHP_DELETE,CString("BC_JOBCHANGE"), CString("JobDataChange"),ProcessGhpCf);

	omData.RemoveAll();
}

CedaGhpData::~CedaGhpData()
{
	ogDdx.UnRegister(this,NOTUSED);
	ClearAll();
	
}	
bool CedaGhpData::ReadSpecialData(CCSPtrArray<GHPDATA> *popGhp,char *pspWhere,char *pspFieldList,bool ipSYS/*=true*/)
{
	bool ilRc = true;
	char pclFieldList[256] = " ";

	if(strlen(pspFieldList) > 0) 
	{
		strcpy(pclFieldList, pspFieldList);
	}
	if(ipSYS == true) 
	{
		if (CedaAction("SYS",pcmTableName,pclFieldList,pspWhere,"",pcgDataBuf) == false)
		{
			if(!omLastErrorMessage.IsEmpty())
			{
				if(omLastErrorMessage.Find("ORA") != -1)
				{
					char pclMsg[2048]="";
					sprintf(pclMsg, "%s", omLastErrorMessage.GetBuffer(0));
					::MessageBox(NULL,pclMsg,GetString(IDS_STRING188),MB_OK);
					return false;
				}
			}
		}
	}
	else
	{
		if (CedaAction("RT",pcmTableName,pclFieldList,pspWhere,"",pcgDataBuf) == false)
		{
			if(!omLastErrorMessage.IsEmpty())
			{
				if(omLastErrorMessage.Find("ORA") != -1)
				{
					char pclMsg[2048]="";
					sprintf(pclMsg, "%s", omLastErrorMessage.GetBuffer(0));
					::MessageBox(NULL,pclMsg,GetString(IDS_STRING188),MB_OK);
					return false;
				}
			}
		}
	}
	if(popGhp != NULL)
	{
		for (int ilLc = 0; ilRc == true; ilLc++)
		{
			GHPDATA *prpGhp = new GHPDATA;
			if ((ilRc = GetBufferRecord(ilLc,prpGhp,CString(pclFieldList))) == true)
			{
				popGhp->Add(prpGhp);
			}
			else
			{
				delete prpGhp;
			}
		}
		if(popGhp->GetSize() == 0)
			return false;
	}
    return true;
}

bool CedaGhpData::Read(char *pspWhere /*NULL*/)
{
    //char where[512];
	omData.DeleteAll();
	bool ilRc = true;

	CTime omTime;

    // Select data from the database
	if(pspWhere == NULL)
	{	
		if (CedaAction("RT", "") == false)
		{
			if(!omLastErrorMessage.IsEmpty())
			{
				if(omLastErrorMessage.Find("ORA") != -1)
				{
					char pclMsg[2048]="";
					sprintf(pclMsg, "%s", omLastErrorMessage.GetBuffer(0));
					::MessageBox(NULL,pclMsg,GetString(IDS_STRING188),MB_OK);
					return false;
				}
			}
		}
	}
	else
	{
		if (CedaAction("RT", pspWhere) == false)
		{
			if(!omLastErrorMessage.IsEmpty())
			{
				if(omLastErrorMessage.Find("ORA") != -1)
				{
					char pclMsg[2048]="";
					sprintf(pclMsg, "%s", omLastErrorMessage.GetBuffer(0));
					::MessageBox(NULL,pclMsg,GetString(IDS_STRING188),MB_OK);
					return false;
				}
			}
		}
	}


	ClearAll();

    for (int ilLc = 0; ilRc == true; ilLc++)
    {
		GHPDATA *prlGhp = new GHPDATA;
		if ((ilRc = GetBufferRecord(ilLc,prlGhp)) == true)
		{
			InsertInternal(prlGhp);
			//omData.Add(prlGhp);
		}
		else
		{
			delete prlGhp;
		}
	}

    return true;
}
bool CedaGhpData::ClearAll()
{

	omUrnoMap.RemoveAll();
	omRuleGroupMap.RemoveAll();
    omData.DeleteAll();

    return true;
}



/////////////////////////////////////////////////////////////////////////////
// Update data methods (called from PrePlanTable class)

void CedaGhpData::InsertInternal(GHPDATA *prpGhp)
{
	
	
	omData.Add(prpGhp);
	omUrnoMap.SetAt((void *)prpGhp->Urno,prpGhp);
	ogDdx.DataChanged((void *)this,GHP_NEW,(void *)prpGhp);

}
/*
void CedaGhpData::InsertInternal(GHPDATA *prpGhp)
{
	GHPDATA *prlGhp = new GHPDATA;
	*prlGhp = *prpGhp;
	omData.Add(prlGhp);
	omUrnoMap.SetAt((void *)prpGhp->Urno,prlGhp);
	ogDdx.DataChanged((void *)this,GHP_NEW,(void *)prlGhp);

}
*/
void CedaGhpData::DeleteInternal(GHPDATA *prpGhp)
{
	ogDdx.DataChanged((void *)this,GHP_DELETE,(void *)prpGhp);
	omUrnoMap.RemoveKey((void *)prpGhp->Urno);
	int ilCount = omData.GetSize();
	for (int i = 0; i < ilCount; i++)
	{
		if (omData[i].Urno == prpGhp->Urno)
		{
			omData.DeleteAt(i);//Update omData
			break;
		}
	}
}

void CedaGhpData::UpdateInternal(GHPDATA *prpGhp)
{
	GHPDATA *prlGhp = GetGhpByUrno(prpGhp->Urno);
	if (prlGhp != NULL)
	{
		if(prpGhp->IsChanged != DATA_NEW)
		{
			prpGhp->IsChanged = DATA_CHANGED;
		}
		omUrnoMap.RemoveKey((void *)prlGhp->Urno);
		*prlGhp = *prpGhp; //Update omData
		omUrnoMap.SetAt((void *)prlGhp->Urno,prlGhp);
		ogDdx.DataChanged((void *)this,GHP_CHANGE,(void *)prpGhp);
	}
}

GHPDATA *CedaGhpData::GetGhpByUrno(long lpUrno)
{
	GHPDATA  *prlGhp;
	if (omUrnoMap.Lookup((void *)lpUrno,(void *& )prlGhp) == TRUE)
	{
		return prlGhp;
	}
	return NULL;
}


bool CedaGhpData::Delete(GHPDATA *prpGhp, bool bpWithSave)
{
	ogDdx.DataChanged((void *)this,GHP_DELETE,(void *)prpGhp);
	if(prpGhp->IsChanged == DATA_UNCHANGED)
	{
		prpGhp->IsChanged = DATA_DELETED;
	}
	if(bpWithSave == true)
	{
		Save(prpGhp);
	}
	return true;
}

bool CedaGhpData::Insert(GHPDATA *prpGhp, bool bpWithSave)
{
	ogDdx.DataChanged((void *)this,GHP_NEW,(void *)prpGhp);
	if(prpGhp->IsChanged == DATA_UNCHANGED)
	{
		prpGhp->IsChanged = DATA_NEW;
	}
	if(bpWithSave == true)
	{
		Save(prpGhp);
	}

	return true;
}

bool CedaGhpData::Update(GHPDATA *prpGhp, bool bpWithSave)
{
	ogDdx.DataChanged((void *)this,GHP_CHANGE,(void *)prpGhp);
	if(prpGhp->IsChanged == DATA_UNCHANGED)
	{
		prpGhp->IsChanged = DATA_CHANGED;
	}
	if(bpWithSave == true)
	{
		Save(prpGhp);
	}
	return true;

}



void  ProcessGhpCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	ogGhpData.ProcessGhpBc(ipDDXType,vpDataPointer,ropInstanceName);
}


void  CedaGhpData::ProcessGhpBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	GHPDATA *prlGhp = (GHPDATA *)vpDataPointer;
	switch(ipDDXType)
	{
	case BC_GHP_NEW:
		omData.NewAt(omData.GetSize(), *prlGhp);
		ogDdx.DataChanged((void *)this,GHP_NEW,(void *)prlGhp);
		break;
	case BC_GHP_CHANGE:
		{
			int ilC = omData.GetSize();
			for(int i = 0; i < ilC; i++)
			{
				if(omData[i].Urno == prlGhp->Urno)
				{
					omData.SetAt(i, prlGhp);
					ogDdx.DataChanged((void *)this,GHP_CHANGE,(void *)prlGhp);
					i = ilC;
				}
			}
		}
		break;
	case BC_GHP_DELETE:
		{
			int ilC = omData.GetSize();
			for(int i = 0; i < ilC; i++)
			{
				if(omData[i].Urno == prlGhp->Urno)
				{
					ogDdx.DataChanged((void *)this,GHP_DELETE,(void *)prlGhp);
					omData.DeleteAt(i);
					i = ilC;
				}
			}
		}
		break;
	default:
		break;
	}
}


bool CedaGhpData::Save(GHPDATA *prpGhp)
{
	bool olRc = true;
	CString olListOfData;
	char pclSelection[124];
	char pclData[524];

	if (prpGhp->IsChanged == DATA_UNCHANGED)
	{
		return true; // no change, nothing to do
	}

	switch(prpGhp->IsChanged)   // New job, insert into database
	{
	case DATA_NEW:
		MakeCedaData(&omRecInfo,olListOfData,prpGhp);
		strcpy(pclData,olListOfData);
		olRc = CedaAction("IRT","","",pclData);
		prpGhp->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_CHANGED:
		sprintf(pclSelection, "WHERE URNO = '%ld'", prpGhp->Urno);
		MakeCedaData(&omRecInfo,olListOfData,prpGhp);
		strcpy(pclData,olListOfData);
		olRc = CedaAction("URT",pclSelection,"",pclData);
		prpGhp->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_DELETED:
		sprintf(pclSelection, "WHERE URNO = '%ld'", prpGhp->Urno);
		olRc = CedaAction("DRT",pclSelection);
		if (olRc == true)
		{
/*			int ilC = omData.GetSize();
			for(int i = 0; i < ilC; i++)
			{
				if(omData[i].Urno == prpGhp->Urno)
				{
					omData.DeleteAt(i);
				}
			}
*/
		}
		break;
	}
	if(!omLastErrorMessage.IsEmpty())
	{
		if(omLastErrorMessage.Find("ORA") != -1)
		{
			char pclMsg[2048]="";
			sprintf(pclMsg, "%s", omLastErrorMessage.GetBuffer(0));
			::MessageBox(NULL,pclMsg,GetString(IDS_STRING188),MB_OK);
			return false;
		}
	}

   return true;
}

CString CedaGhpData::GetFieldList()
{
	CString olFieldList = CString(pcmFieldList);
	return olFieldList;
}

bool CedaGhpData::IsDataChanged(GHPDATA *prpOld, GHPDATA *prpNew)
{
	CString olFieldList;
	CString olListOfData;

	olFieldList = GetFieldList();
	MakeCedaData(olListOfData, olFieldList, (void*)prpOld, (void*)prpNew);
	if(olFieldList.IsEmpty())
	{
		return false;
	}
	else
	{
		return true;
	}

}

int  CedaGhpData::CheckPrsn(char *pspName)
{
	int ilCount = 0;
	for(int i = omData.GetSize() -1 ; i >= 0; i--)
	{
		if(strcmp( omData[i].Prsn, pspName) == 0)
			ilCount++;

	}
	return ilCount;

}


