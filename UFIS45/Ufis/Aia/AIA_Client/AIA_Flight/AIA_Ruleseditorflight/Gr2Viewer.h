// Gr2Viewer.h: Schnittstelle f�r die Klasse Gr2Viewer.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_Gr2Viewer_H__7CC9F681_68A9_11D2_A4B5_0000B45A33F5__INCLUDED_)
#define AFX_Gr2Viewer_H__7CC9F681_68A9_11D2_A4B5_0000B45A33F5__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

struct GR2TABLE_LINEDATA
{
	int Rtnr;
	CString Nature;
	CString Pos;
	CString Gate;
	CString Belt;
	CString Wro;
};



#include "CVIEWER.H"
#include "CCSTable.H"
#include "CedaNatData.h"



class Gr2Viewer : public CViewer  
{
public:
	Gr2Viewer();
	virtual ~Gr2Viewer();

	void Attach(CCSTable *popAttachWnd);
	/* void ChangeViewTo(const char *pcpViewName, CString opDate); */
    //@ManMemo: ChangeViewTo
    void ChangeViewTo(const char *pcpViewName);
    //@ManMemo: omLines
    CCSPtrArray<GR2TABLE_LINEDATA> omLines;
	bool bmNoUpdatesNow;
private:
	
	
	int CompareGat(GR2TABLE_LINEDATA *prpNat1, GR2TABLE_LINEDATA *prpNat2);

    void MakeLines();

	void MakeGatLineData(NATDATA *popNAT, CStringArray &ropArray);
	int MakeLine(NATDATA *prpNat1);
	int MakeLine();
	void MakeHeaderData();
	void MakeColumnData(CCSPtrArray<TABLE_COLUMN> &ropColList,GR2TABLE_LINEDATA *prpLine);

	BOOL FindLine(char *pcpKeya, char *pcpKeyd, int &rilLineno);
	BOOL FindLine(long lpRtnr, int &rilLineno);
public:
    //@ManMemo: DeleteAll
	void DeleteAll();
    //@ManMemo: CreateLine
	//@ManMemo: SetStartEndTime
	int CreateLine(GR2TABLE_LINEDATA *prpGat);
    //@ManMemo: DeleteLine
	void DeleteLine(int ipLineno);
	void ClearAll();


// Window refreshing routines
public:


    //@ManMemo: UpdateDisplay
	void UpdateDisplay();
private:
    CString omDate;
	// Attributes
private:
	CCSTable *pomTable;
	CCSPtrArray <TABLE_HEADER_COLUMN> omHeaderDataArray;
	int imTotalLines;

// Methods which handle changes (from Data Distributor)
public:
    //@ManMemo: omStartTime
    
    //@ManMemo: omEndTime
	
    //@ManMemo: omDay
	

};

#endif // !defined(AFX_Gr2Viewer_H__7CC9F681_68A9_11D2_A4B5_0000B45A33F5__INCLUDED_)
