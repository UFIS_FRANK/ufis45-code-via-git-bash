// stviewer.h : header file
//

#ifndef __PREMISPAGEDETAILVIEWER__
#define __PREMISPAGEDETAILVIEWER__

//#include "cviewer.h"
#include "CCSPtrArray.h"
#include "CedaGpmData.h"

struct GPM_BARDATA 
{
	long	Urno; //Unique Record Number
	long	Rkey; //Foreign Key of Premis
	long	Ghsu; //Foreign Key iof Ground handling Services
    CString Text; //Bar Text ==> dynamically generated
	CString Dtyp; //Duty Type
	CString Rtyp; //Resourcen Typ
    CTime	Dube; //Duty Begin ==> from before/after onbl, ofbl
    CTime	Duen; //Duty End ==> from before/after onbl, ofbl
	CString Lknm; //Full name of Ground Handling Service
	int		Prta; //preparation time
	int		Ttdu; //Time way to duty
	int		Dtim; //Duration of duty
	int		Tfdu; //time way from duty
	int		Pota; //Post operation time
	CString Lkco; //Short Name of Ground Handling Services
	CString	Vrgc; //Available Ressource Groups
	CString Perm; //Needed GPE Permits
	CString Pfkt; //Needed GPE Funktions
	int		Noma; //Number of employees ==> if more than one => make several bar
	int		Etot; //Earliest take over
	int		Prio; //Priority of duty
	int		Didu; //Difficulty of duty
	CString  Duch; //create Duties

    int FrameType;
    int MarkerType;
    CBrush *MarkerBrush;
	COLORREF TextColor;
	COLORREF BkColor;
    int OverlapLevel;       // zero for the top-most level, each level lowered by 4 pixels
};
struct GPM_LINEDATA
{
	long Urno;
	long Rkey;
	long Ghsu;
	CTime Dube;
	CTime Duen;
	CString Lknm;
	bool isAdditionalSpecial;
    int MaxOverlapLevel;    // maximum number of overlapped level of bars in this line
    CCSPtrArray<GPM_BARDATA> Bars;		// in order of painting (leftmost is the bottommost)
    CCSPtrArray<int> TimeOrder;				// maintain overlapped bars, see below
	COLORREF TextColor;
	COLORREF BkColor;
	GPM_LINEDATA(void)
	{
		TextColor = ogColors[BLACK_IDX];
		BkColor = ogColors[SILVER_IDX];
		isAdditionalSpecial = TRUE;
		Lknm = CString("");
		//Lkco = "LKNM - Text";
	}
};
/////////////////////////////////////////////////////////////////////////////
// StaffDiagramViewer window

class PremisPageDetailViewer
{
// Constructions
public:
    PremisPageDetailViewer(long Rkey);
    ~PremisPageDetailViewer();
	void ClearAll();

	void Attach(CWnd *popAttachWnd);
	void ChangeViewTo(long lpUrno);

	long lmRkey;
	
	CTime omOnBlock;//(1980, 1, 1, 1, 30, 0);
	CTime omOfBlock;//(1980, 1, 1, 5, 30, 0);
// Internal data processing routines
public:
	BOOL IsPassFilter(GPMDATA *prp);
	int CompareLine(GPM_LINEDATA *prpLine1, GPM_LINEDATA *prpLine2);
	void MakeLines();
	void MakeBars();
	void MakeLine(GPMDATA *prpGpm);
	void MakeEmptyLine();
void MakeBar(int ipLineno, GPMDATA *prpGpm);

	CString LineText(GPMDATA *prpGpm);
	CString BarTextAndValues(GPMDATA *prpGpm);

	CTime CalcStartTime(GPMDATA *prpGpm);
	CTime CalcEndTime(GPMDATA *prpGpm);
	BOOL FindDutyBar(/*CTime opDube*/long Urno, int &ripLineno);
	BOOL FindBar(/*CTime Dube*/long Urno, int &ripLineno, int &ripBarno);
	BOOL FindBarGlobal(long Urno, int &ripLineno, int &ripBarno);
// Operations
public:
	void SetCurrentRkey(long lpRkey);
	long GetCurrentRkey();
	int GetNextFreeLine();
    int GetLineCount();
    GPM_LINEDATA *GetLine( int ipLineno);
    CString GetLineText( int ipLineno, COLORREF *prpBkColor, COLORREF *prpTextColor);
	 int GetVisualMaxOverlapLevel(int ipLineno);
    int GetMaxOverlapLevel(int ipLineno);
    int GetBkBarCount( int ipLineno);
    int GetBarCount(int ipLineno);
    GPM_BARDATA *GetBar( int ipLineno, int ipBarno);
    CString GetBarText(int ipLineno, int ipBarno);

    int CreateLine( GPM_LINEDATA *prpLine);
    void DeleteLine(int ipLineno);
    int CreateBar(int ipLineno, GPM_BARDATA *prpBar, BOOL bpFrontBar = TRUE);
    void DeleteBar(int ipLineno, int ipBarno);

    int GetBarnoFromTime(int ipLineno, CTime opTime1, CTime opTime2,
        int ipOverlapLevel1, int ipOverlapLevel2);

private:
    int GetFirstOverlapBarno(GPM_LINEDATA *prpLine, int ipBarno);
    GPM_BARDATA *GetBarInTimeOrder(GPM_LINEDATA *prpLine, int ipBarno);
	 void GetOverlappedBarsFromTime( int ipLineno, CTime opTime1, CTime opTime2, CUIntArray &ropBarnoList);

    CTime GetEndTimeOfBarsOnTheLeft(GPM_LINEDATA *prpLine, int ipBarno);
	void ReorderBars(int ipLineno, int ilFirstBar, int ilTotalBars);

// Attributes used for filtering condition
private:

// Attributes
private:
	CWnd *pomAttachWnd;
	CBrush omBkBrush;
	CBrush omBreakBrush;
	CBrush omBarBrushGray;
    CCSPtrArray<GPM_LINEDATA> omLines;
	CStringArray omSortOrder;


// Methods which handle changes (from Data Distributor)
public:
	void ProcessGpmChange(GPMDATA *prpGpm);
	void ProcessGpmNew(GPMDATA *prpGpm);
	void ProcessGpmDelete(GPMDATA *prpGpm);
	void ProcessPremisDelete(GPMDATA *prpGpm);
};

/////////////////////////////////////////////////////////////////////////////

#endif //__PREMISPAGEDETAILVIEWER__
