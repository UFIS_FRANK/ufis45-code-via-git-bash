// CedaACTData.h

#ifndef __CEDAACTDATA__
#define __CEDAACTDATA__
 
//#include "stdafx.h"
#include "basicdata.h"

//---------------------------------------------------------------------------------------------------------
// Record structure declaration

//@See: CedaACTData
//@Doc:	A structure for reading Duty data. We read all data from database and put them
//		into this struct and store the data in omData.  

//---------------------------------------------------------------------------------------------------------
struct ACTDATA 
{
	long 	 Urno; 		// Eindeutige Datensatz-Nr.
	CTime	 Cdat;		// Erstellungsdatum
	char 	 Usec[34]; 	// Anwender (Ersteller)
	CTime	 Lstu;	 	// Datum letzte �nderung
	char 	 Prfl[3]; 	// Protokollierungskennzeichen
	char 	 Useu[34]; 	// Anwender (letzte �nderung)
	char 	 Act3[5]; 	// Flugzeug-Typ 3-Letter Code (IATA)
	char 	 Act5[7]; 	// Flugzeug-Typ 5-Letter Code (ICAO)
	char 	 Acti[7]; 	// Modifizierter ICAO Code
	char 	 Acfn[32]; 	// Flugzeug-Bezeichnung
	char 	 Acws[7]; 	// Spannweite
	char 	 Acle[7]; 	// L�nge
	char 	 Ache[7]; 	// H�he
	char 	 Seat[5]; 	// Anzahl Sitzpl�tze
	char 	 Enty[5]; 	// Antriebsart
	char 	 Enno[3]; 	// Anzahl Triebwerke
	CTime	 Vafr;	 	// G�ltig von
	CTime  Vato;	 	// G�ltig bis
	char 	 Nads[3]; 	// kein Andocksystem verwenden

	//DataCreated by this class
	int		 IsChanged;

	ACTDATA(void)
	{ 
		memset(this,'\0',sizeof(*this));
		Cdat=-1;Lstu=-1;Vafr=-1;Vato=-1;
	}

}; // end ACTDataStrukt

//---------------------------------------------------------------------------------------------------------
// Class declaratino

//@Memo: Read and write ACT(duty) data
//@See: CedaData, CedaDutyData
/*@Doc:
  Reads and writes ACT(duty) data from and to database. Stores ACT data in memory and
  provides methods for searching and retrieving ACTs from its internal list. Does
  some data preparations.

  Data of this class has to be updated by CEDA through broadcasts. This procedure and the
  necessary methods will be described later with the description of broadcast handling.

  {\bf CedaACTData} handles data from the server process. 
  See the specification for table descriptions.*/

class CedaACTData: public CCSCedaData
{
// Attributes
public:
    //@ManMemo: A map, containing the Urno field of all loaded ACTs.
    CMapPtrToPtr omUrnoMap;
    CMapStringToPtr omAct3Map;
    CMapStringToPtr omAct5Map;

    //@ManMemo: ACTDATA records read by ReadAllACT().
    CCSPtrArray<ACTDATA> omData;

	void SetTableName(CString opTableName)
	{
		strcpy(pcmTableName, opTableName.GetBuffer(0));
	}
// Operations
public:
    //@ManMemo: Default constructor
    /*@Doc:
      Initialize {\bf omRecInfo} for offsets and types for each fields in {\bf ACTDATA},
      the {\bf pcmTableName} with table name {\bf ACTLSG}, the data members {\bf pcmFieldList}
      contains a list of used fields of {\bf ACTDATA}.
    */
    CedaACTData();
	~CedaACTData();
	
	void Register(void);
	void ClearAll(bool bpWithRegistration = true);

    //@ManMemo: Read all ACTs.
    /*@Doc:
      Read all ACTs. 

      This method will clear {\bf omData} and {\bf omKeyMap}. Convert each record
      to a ACTDATA and store it in {\bf omData} and the Urno field in {\bf omKeyMap}.
    */
    bool ReadAllACTs(char *pspWhere = NULL);
    //@AreMemo: Read all ACT-Data corresponding to Where-condition
	bool ReadSpecial(CCSPtrArray<ACTDATA> &ropAct,char *pspWhere);
    //@ManMemo: Add a new ACT and store the new data to the Database.
	bool InsertACT(ACTDATA *prpACT,BOOL bpSendDdx = TRUE);
    //@ManMemo: Add data for a new ACT to the internal data.
	bool InsertACTInternal(ACTDATA *prpACT);
	//@ManMemo: Change the data of a special ACT
	bool UpdateACT(ACTDATA *prpACT,BOOL bpSendDdx = TRUE);
    //@AreMemo: Change the data of a special ACT in the internal data.
	bool UpdateACTInternal(ACTDATA *prpACT);
    //@ManMemo: Delete a special ACT, also in the Database.
	bool DeleteACT(long lpUrno);
    //@ManMemo: Delete a special ACT only in the internal data.
	bool DeleteACTInternal(ACTDATA *prpACT);
    //@AreMemo: Selects all ACTs with a special Urno.
	ACTDATA  *GetACTByUrno(long lpUrno);
	ACTDATA  *GetActByAct3(char *pcpAct3);
	ACTDATA  *GetActByAct5(char *pcpAct5);
    //@AreMemo: Search a ACT with a special Key.
	CString ACTExists(ACTDATA *prpACT);
    //@AreMemo: Search a Act3 and give back Act5
	bool ExistsSpecial(char *popAct3,char *popAct5);
	//@ManMemo: Writes a special ACT to the Database.
	bool SaveACT(ACTDATA *prpACT);
    //@ManMemo: Contains the valid data for all existing ACTs.
	char pcmACTFieldList[2048];
    //@ManMemo: Handle Broadcasts for ACTs.
	void ProcessACTBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);
	//@ManMeno: Get Urno from Act3 or Act5
	long GetUrnoByAct(char *pspAct);
	// Private methods
private:
    void PrepareACTData(ACTDATA *prpACTData);
};

//---------------------------------------------------------------------------------------------------------
extern CedaACTData ogActData;

#endif //__CEDAACTDATA__
