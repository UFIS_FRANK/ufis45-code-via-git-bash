// Gr2Viewer.cpp: Implementierung der Klasse GatTableViewer.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"

#include "CedaGatData.h"

#include "Gr2Viewer.h"
#include <math.h>

#ifdef _DEBUG 
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif


const int width = 574;

//////////////////////////////////////////////////////////////////////
// Konstruktion/Destruktion
//////////////////////////////////////////////////////////////////////

Gr2Viewer::Gr2Viewer()
{
	bmNoUpdatesNow = false;
	

}

Gr2Viewer::~Gr2Viewer()
{
	DeleteAll();
}

void Gr2Viewer::Attach(CCSTable *popTable)
{
    pomTable = popTable;
}

#pragma warning( disable : 4100 )  
void Gr2Viewer::ChangeViewTo(const char *pcpViewName)
{
    DeleteAll();    
    MakeLines();

	UpdateDisplay();
}
 

int Gr2Viewer::CompareGat(GR2TABLE_LINEDATA *prpGat1, GR2TABLE_LINEDATA *prpGat2)
{


     return 0;
}
#pragma warning( default : 4100 )  

/////////////////////////////////////////////////////////////////////////////
// Gr2Viewer -- code specific to this class


void Gr2Viewer::MakeLines()
{
	int ilGatCount = ogNatData.omData.GetSize();
	for(int illc = 0 ; illc < ilGatCount; illc++)
	{		
		NATDATA * prlNat = &ogNatData.omData[illc];
		MakeLine(prlNat);
		
	}
}


int Gr2Viewer::MakeLine(NATDATA *prpNat)
{
    // Update viewer data for this shift record
   GR2TABLE_LINEDATA rlLine;

	rlLine.Rtnr = omLines.GetSize() +1;
	rlLine.Nature = prpNat->Ttyp;
	rlLine.Pos = prpNat->Alpo;
	rlLine.Gate = prpNat->Alga;
	rlLine.Belt = prpNat->Albb;
	rlLine.Wro = prpNat->Alwr;


	
	return(CreateLine(&rlLine));
}


int Gr2Viewer::CreateLine(GR2TABLE_LINEDATA *prpGat)
{
    int ilLineCount = omLines.GetSize();

	GR2TABLE_LINEDATA rlGat;
	rlGat = *prpGat;
    omLines.NewAt(ilLineCount, rlGat);

    return ilLineCount;
}

void Gr2Viewer::DeleteLine(int ipLineno)
{
	omLines.DeleteAt(ipLineno);
}


BOOL Gr2Viewer::FindLine(long lpRtnr, int &rilLineno)
{
    for (rilLineno = 0; rilLineno < omLines.GetSize(); rilLineno++)
	{
      if (omLines[rilLineno].Rtnr == lpRtnr)
            return TRUE;
	}
    return FALSE;
}



/////////////////////////////////////////////////////////////////////////////
// Gr2Viewer - GR2TABLE_LINEDATA array maintenance

void Gr2Viewer::DeleteAll()
{
    while (omLines.GetSize() > 0)
        DeleteLine(0);
}


/////////////////////////////////////////////////////////////////////////////
// Gr2Viewer - display drawing routine


void Gr2Viewer::MakeHeaderData()
{

	TABLE_HEADER_COLUMN rlHeader;
//	int ilColumnNo = -1;


	CStringArray olAcgrList;
	ExtractItemList(LoadStg(IDS_STRING1437), &olAcgrList, ',');
	
	CString olT;
	int ilActIdx = 0;
	int ilWordCount = olAcgrList.GetSize();
	

	rlHeader.Alignment = COLALIGN_CENTER;
	rlHeader.Length = 8*3; 
	rlHeader.Font = &ogCourier_Bold_8;
	rlHeader.SeparatorType = SEPA_NORMAL;//SEPA_NONE;//SEPA_LIKEVERTICAL

	rlHeader.Text = "";
	
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(),rlHeader);

	rlHeader.Length = 8 * 10; 
	rlHeader.Text = "";
	if(ilWordCount >ilActIdx)
	{
		olT = olAcgrList[ilActIdx];
		rlHeader.Text = olT;
		ilActIdx++;
	}
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(),rlHeader);

	rlHeader.Length = 8 * 10; 
	rlHeader.Text = "";
	if(ilWordCount >ilActIdx)
	{
		olT = olAcgrList[ilActIdx];
		rlHeader.Text = olT;
		ilActIdx++;
	}
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(),rlHeader);

	rlHeader.Length = 8 * 10; 
	rlHeader.Text = "";
	if(ilWordCount >ilActIdx)
	{
		olT = olAcgrList[ilActIdx];
		rlHeader.Text = olT;
		ilActIdx++;
	}
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(),rlHeader);

	rlHeader.Length = 8 * 10; 
	rlHeader.Text = "";
	if(ilWordCount >ilActIdx)
	{
		olT = olAcgrList[ilActIdx];
		rlHeader.Text = olT;
		ilActIdx++;
	}
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(),rlHeader);

	rlHeader.Length = 8 * 10; 
	rlHeader.Text = "";
	if(ilWordCount >ilActIdx)
	{
		olT = olAcgrList[ilActIdx];
		rlHeader.Text = olT;
		ilActIdx++;
	}
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(),rlHeader);


	pomTable->SetHeaderFields(omHeaderDataArray);
	omHeaderDataArray.RemoveAll();

}



void Gr2Viewer::MakeColumnData(CCSPtrArray<TABLE_COLUMN> &ropColList,GR2TABLE_LINEDATA *prpLine)
{

//	BOOL blNewLogicLine = FALSE;

	int ilColumnNo = 0;
	bool bldefekt = false;

	TABLE_COLUMN rlColumnData;
	rlColumnData.Alignment = COLALIGN_LEFT;
	rlColumnData.Lineno = imTotalLines;//*3)+ilLocalLine;
	rlColumnData.VerticalSeparator = SEPA_NORMAL;
	rlColumnData.SeparatorType = SEPA_NORMAL;
	rlColumnData.Font = &ogCourier_Regular_8;//&ogCourier_Regular_10;
	rlColumnData.BkColor = WHITE;
	rlColumnData.TextColor = BLACK;
	rlColumnData.Text.Format("%d",prpLine->Rtnr);  
	rlColumnData.Columnno = ilColumnNo++;
	ropColList.NewAt(ropColList.GetSize(), rlColumnData);


	rlColumnData.Text = prpLine->Nature;
	rlColumnData.Columnno = ilColumnNo++;
	ropColList.NewAt(ropColList.GetSize(), rlColumnData);

	rlColumnData.Text = prpLine->Pos;
	rlColumnData.Columnno = ilColumnNo++;
	ropColList.NewAt(ropColList.GetSize(), rlColumnData);

	rlColumnData.Text = prpLine->Gate;
	rlColumnData.Columnno = ilColumnNo++;
	ropColList.NewAt(ropColList.GetSize(), rlColumnData);

	rlColumnData.Text = prpLine->Belt;
	rlColumnData.Columnno = ilColumnNo++;
	ropColList.NewAt(ropColList.GetSize(), rlColumnData);

	rlColumnData.Text = prpLine->Wro;
	rlColumnData.Columnno = ilColumnNo++;
	ropColList.NewAt(ropColList.GetSize(), rlColumnData);

}



// UpdateDisplay: Load data selected by filter conditions to the display by using "omTable"
void Gr2Viewer::UpdateDisplay()
{

	imTotalLines = 0;
//	BOOL blNewLogicLine = FALSE;


	pomTable->SetShowSelection(FALSE);
	pomTable->ResetContent();
	//pomTable->SetDockingRange(3);
	
	MakeHeaderData();
	pomTable->SetDefaultSeparator();
//	pomTable->SetSelectMode(0);

	for (int ilLc = 0; ilLc < omLines.GetSize(); ilLc++)
	{
		CCSPtrArray<TABLE_COLUMN> olColList;

		MakeColumnData(olColList,&omLines[ilLc]);
		imTotalLines++;
		pomTable->AddTextLine(olColList, (void*)&(omLines[ilLc]));
		olColList.DeleteAll();
	}
    pomTable->DisplayTable();
}




