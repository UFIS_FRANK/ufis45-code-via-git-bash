#if !defined(AFX_DUTYPREFERENCES_H__CFE7A533_40B2_11D1_9579_0000B4392C49__INCLUDED_)
#define AFX_DUTYPREFERENCES_H__CFE7A533_40B2_11D1_9579_0000B4392C49__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// DutyPreferences.h : header file
//

#include "PremisPage.h"
#include "GroupNamesPage.h"
#include "GatPosRules.h"
#include "GrRules2.h"

#include "Table.h"
/////////////////////////////////////////////////////////////////////////////
// DutyPreferences

#define ID_BUTTON_SAVE			(WM_USER+100)
#define ID_BUTTON_NEW			(WM_USER+101)
#define ID_BUTTON_UPDATE		(WM_USER+102)
#define ID_BUTTON_DELETE		(WM_USER+103)
#define ID_BUTTON_COPY			(WM_USER+104)
#define ID_BUTTON_STANDARD		(WM_USER+105)
#define ID_BUTTON_ADDITIONAL	(WM_USER+106)
#define ID_STATIC_BEEREICH		(WM_USER+108)
#define ID_COMBOBOX_COMBO		(WM_USER+109)
#define ID_BUTTON_CLOSE			(WM_USER+110)

#define CLK_BUTTON_SAVE			(WM_USER+10000)
#define CLK_BUTTON_NEW			(WM_USER+10001)
#define CLK_BUTTON_UPDATE		(WM_USER+10002)
#define CLK_BUTTON_DELETE		(WM_USER+10003)
#define CLK_BUTTON_COPY			(WM_USER+10004)
#define CLK_BUTTON_STANDARD		(WM_USER+10005)
#define CLK_BUTTON_ADDITIONAL	(WM_USER+10006)


class DutyPreferences : public CPropertySheet
{
	DECLARE_DYNAMIC(DutyPreferences)

// Construction
public:
	DutyPreferences(UINT nIDCaption, CWnd* pParentWnd = NULL, UINT iSelectPage = 0);
	DutyPreferences(LPCTSTR pszCaption, CWnd* pParentWnd = NULL, UINT iSelectPage = 0);

// Attributes
public:

	GroupNamesPage omGroupNamePage;
	PremisPage     omPremisPage;
	GatPosRules    omGatposPage;
	GrRules2	   omGatposPage2;

	CCSButtonCtrl *pomSaveButton;
	CButton	  *pomCancelButton,
			  *pomNewButton,
			  *pomUpdateButton,
			  *pomDeleteButton,
			  *pomCopyButton,
			  *pomStandardButton,
			  *pomAdditionalButton;
	CComboBox *pomComboBox;
	CStatic   *pomBereich;

	void OnSaveButton();
	void OnNewButton();
	void OnCloseButton();
	void OnUpdateButton();
	void OnDeleteButton();
	void OnCopyButton();
	void OnStandardButton();
	void OnAdditionalButton();

	
	
	// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(DutyPreferences)
	public:
	virtual BOOL OnInitDialog();
	protected:
	virtual BOOL OnCommand(WPARAM wParam, LPARAM lParam);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~DutyPreferences();

	// Generated message map functions
protected:
	//{{AFX_MSG(DutyPreferences)
	virtual void OnOK();
	afx_msg LONG OnResetRelease(WPARAM wParam, LPARAM lParam);
	afx_msg LONG OnSetRelease(WPARAM wParam, LPARAM lParam);
	afx_msg void OnChar(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg void OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg void OnDestroy();
	afx_msg void OnNcDestroy();
	afx_msg void OnClose();
	afx_msg void OnCancel();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


extern DutyPreferences *pogDutyPreferences;
/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DUTYPREFERENCES_H__CFE7A533_40B2_11D1_9579_0000B4392C49__INCLUDED_)
