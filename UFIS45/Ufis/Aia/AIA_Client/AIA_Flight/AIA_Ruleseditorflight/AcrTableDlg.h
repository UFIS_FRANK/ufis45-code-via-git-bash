#if !defined(AFX_ACRTABLEDLG_H__CBE92B91_64CB_11D1_B412_0000B45A33F5__INCLUDED_)
#define AFX_ACRTABLEDLG_H__CBE92B91_64CB_11D1_B412_0000B45A33F5__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
#include "cedaAcrData.h"
#include "Table.h"


struct ACR_LINES
{
	CString Regn;
	CString Owne;
	CString Apui;
	CString Acmd;
};

class AcrTableDlg : public CDialog
{
// Construction
public:
	AcrTableDlg(CWnd* pParent = NULL, char *pspRegn = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(AcrTableDlg)
	enum { IDD = IDD_ACRTABLE_DLG };
	CListBox	m_List;
	//}}AFX_DATA

	CString omRegn;
	CTable omTable;
	CCSPtrArray<ACR_LINES> omLines;

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(AcrTableDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(AcrTableDlg)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	afx_msg void OnDblclkReglist();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_ACRTABLEDLG_H__CBE92B91_64CB_11D1_B412_0000B45A33F5__INCLUDED_)
