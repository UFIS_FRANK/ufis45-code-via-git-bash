#if !defined(AFX_PSTRULEDLG_H__BDF1B1D2_F17A_11D2_A1A5_0000B4984BBE__INCLUDED_)
#define AFX_PSTRULEDLG_H__BDF1B1D2_F17A_11D2_A1A5_0000B4984BBE__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// PstRuleDlg.h : header file
//

#include "CedaPstData.h"
/////////////////////////////////////////////////////////////////////////////
// PstRuleDlg dialog

class PstRuleDlg : public CDialog
{
// Construction
public:
	PstRuleDlg(CWnd* pParent, PSTDATA *popPst);   // standard constructor


	PSTDATA *pomPst;
// Dialog Data
	//{{AFX_DATA(PstRuleDlg)
	enum { IDD = IDD_PST_RULES };
	CButton	m_Matrix1;
	CString	m_AL_List;
	CString	m_Exce;
	CString	m_Heig_Max;
	CString	m_Heig_Min;
	CString	m_Leng_Max;
	CString	m_Leng_Min;
	CString	m_Max_Ac;
	CString	m_Nat_Restr;
	CString	m_Npos1;
	CString	m_Npos2;
	CString	m_Npos3;
	CString	m_Npos4;
	CString	m_Npos5;
	CString	m_Npos6;
	CString	m_Npos7;
	CString	m_Npos8;
	CString	m_Npos9;
	CString	m_Npos10;
	CString	m_Prio;
	CString	m_Restr_To1;
	CString	m_Restr_To2;
	CString	m_Restr_To3;
	CString	m_Restr_To4;
	CString	m_Restr_To5;
	CString	m_Restr_To6;
	CString	m_Restr_To7;
	CString	m_Restr_To8;
	CString	m_Restr_To9;
	CString	m_Restr_To10;
	CString	m_Span_Max;
	CString	m_Span_Min;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(PstRuleDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(PstRuleDlg)
	virtual void OnOK();
	virtual BOOL OnInitDialog();
	afx_msg void OnMatrix1();
	afx_msg void OnMatrix2();
	afx_msg void OnMatrix3();
	afx_msg void OnMatrix4();
	afx_msg void OnMatrix5();
	afx_msg void OnMatrix6();
	afx_msg void OnMatrix7();
	afx_msg void OnMatrix8();
	afx_msg void OnMatrix9();
	afx_msg void OnMatrix10();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_PSTRULEDLG_H__BDF1B1D2_F17A_11D2_A1A5_0000B4984BBE__INCLUDED_)
