// CedaGhsData.cpp
 
#include "stdafx.h"
#include "CedaGhsData.h"


void ProcessGhsCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

//--CEDADATA-----------------------------------------------------------------------------------------------

CedaGhsData ogGhsData;

CedaGhsData::CedaGhsData() : CCSCedaData(&ogCommHandler)
{
    BEGIN_CEDARECINFO(GHSDATA, GhsDataRecInfo)
		CCS_FIELD_CHAR_TRIM	(Atrn,"ATRN","Auftragsnummer SAP-CO",0)
		CCS_FIELD_DATE		(Cdat,"CDAT","Erstellungsdatum",0)
		CCS_FIELD_CHAR_TRIM	(Disp,"DISP","Disponierbar (J/N)",0)
		CCS_FIELD_INT	(Lkam,"LKAM","Anzahl Mitarbeiter",0)
		CCS_FIELD_CHAR_TRIM	(Lkan,"LKAN","Annex",0)
		CCS_FIELD_CHAR_TRIM	(Lkar,"LKAR","Abrechnungsschl�ssel",0)
		CCS_FIELD_CHAR_TRIM	(Lkbz,"LKBZ","Bezug",0)
		CCS_FIELD_CHAR_TRIM	(Lkcc, "LKCC", "Kuerzel", 0)
		CCS_FIELD_CHAR_TRIM	(Lkco,"LKCO","Code (Kurzname)",0)
		CCS_FIELD_INT	(Lkda,"LKDA","Dauer",0)
		CCS_FIELD_CHAR_TRIM	(Lkhc,"LKHC","Preis (Handlings Charge)",0)
		CCS_FIELD_INT	(Lknb,"LKNB","Nachbereitung",0)
		CCS_FIELD_CHAR_TRIM	(Lknm,"LKNM","Name (Bezeichnung)",0)
		CCS_FIELD_CHAR_TRIM	(Lkst,"LKST","Standart",0)
		CCS_FIELD_CHAR_TRIM	(Lkty,"LKTY","Ger�t oder Personal",0)
		CCS_FIELD_INT	(Lkvb,"LKVB","Vorbereitung",0)
		CCS_FIELD_DATE		(Lstu,"LSTU","Datum letzte �nderung",0)
		CCS_FIELD_CHAR_TRIM	(Perm,"PERM","Qualifikation",0)
		CCS_FIELD_CHAR_TRIM	(Prfl,"PRFL","Protokollierungskennzeichen",0)
		CCS_FIELD_LONG		(Urno,"URNO","Eindeutige Datensatz-Nr.",0)
		CCS_FIELD_CHAR_TRIM	(Usec,"USEC","Anwender (Ersteller)",0)
		CCS_FIELD_CHAR_TRIM	(Useu,"USEU","Anwender (letzte �nderung)",0)
		CCS_FIELD_CHAR_TRIM	(Vrgc,"VRGC","G�ltige Ressourcengruppe",0)
		CCS_FIELD_CHAR_TRIM	(Wtyp, "WTYP", "zu Fu�/Fahrt/Schlepp", 0)
    END_CEDARECINFO
/* Macro laest kein CString zu MBR 11.01.99
    BEGIN_CEDARECINFO(GHSDATA, GhsDataRecInfo)     
		CCS_FIELD_CHAR_TRIM	(Atrn,"ATRN",GetString(IDS_STRING276),0)
		CCS_FIELD_DATE		(Cdat,"CDAT",GetString(IDS_STRING163),0)
		CCS_FIELD_CHAR_TRIM	(Disp,"DISP",GetString(IDS_STRING277),0)
		CCS_FIELD_INT	(Lkam,"LKAM",GetString(IDS_STRING278),0)
		CCS_FIELD_CHAR_TRIM	(Lkan,"LKAN",GetString(IDS_STRING279),0)
		CCS_FIELD_CHAR_TRIM	(Lkar,"LKAR",GetString(IDS_STRING280),0)
		CCS_FIELD_CHAR_TRIM	(Lkbz,"LKBZ",GetString(IDS_STRING281),0)
		CCS_FIELD_CHAR_TRIM	(Lkcc, "LKCC", GetString(IDS_STRING282), 0)
		CCS_FIELD_CHAR_TRIM	(Lkco,"LKCO",GetString(IDS_STRING283),0)
		CCS_FIELD_INT	(Lkda,"LKDA",GetString(IDS_STRING284),0)
		CCS_FIELD_CHAR_TRIM	(Lkhc,"LKHC",GetString(IDS_STRING285),0)
		CCS_FIELD_INT	(Lknb,"LKNB",GetString(IDS_STRING286),0)
		CCS_FIELD_CHAR_TRIM	(Lknm,"LKNM",GetString(IDS_STRING235),0)
		CCS_FIELD_CHAR_TRIM	(Lkst,"LKST",GetString(IDS_STRING287),0)
		CCS_FIELD_CHAR_TRIM	(Lkty,"LKTY",GetString(IDS_STRING288),0)
		CCS_FIELD_INT	(Lkvb,"LKVB",GetString(IDS_STRING289),0)
		CCS_FIELD_DATE		(Lstu,"LSTU",GetString(IDS_STRING165),0)
		CCS_FIELD_CHAR_TRIM	(Perm,"PERM",GetString(IDS_STRING290),0)
		CCS_FIELD_CHAR_TRIM	(Prfl,"PRFL",GetString(IDS_STRING166),0)
		CCS_FIELD_LONG		(Urno,"URNO",GetString(IDS_STRING162),0)
		CCS_FIELD_CHAR_TRIM	(Usec,"USEC",GetString(IDS_STRING164),0)
		CCS_FIELD_CHAR_TRIM	(Useu,"USEU",GetString(IDS_STRING164),0)
		CCS_FIELD_CHAR_TRIM	(Vrgc,"VRGC",GetString(IDS_STRING291),0)
		CCS_FIELD_CHAR_TRIM	(Wtyp, "WTYP",GetString(IDS_STRING292), 0)
    END_CEDARECINFO
*/
    // Copy the record structure
    for (int i = 0; i < sizeof(GhsDataRecInfo)/sizeof(GhsDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&GhsDataRecInfo[i],sizeof(CEDARECINFO));
        omRecInfo.Add(prpCedaRecInfo);
	}
    // Initialize table names and field names
	strcpy(pcmTableName,"GHS");
	strcat(pcmTableName,pcgTableExt);
    sprintf(pcmListOfFields, "ATRN,CDAT,DISP,LKAM,LKAN,LKAR,LKBZ,LKCC,LKCO,LKDA,LKHC,LKNB,LKNM,LKST,LKTY,LKVB,LSTU,PERM,PRFL,URNO,USEC,USEU,VRGC,WTYP");

	Register();
	pcmFieldList = pcmListOfFields; //because in CedaData pcmFieldList is just a pointer

	omData.RemoveAll();
}

//---------------------------------------------------------------------------------------------------------

void CedaGhsData::GetDataInfo(CStringArray &ropFields, CStringArray &ropDesription, CStringArray &ropType)
{
	ropFields.Add("ATRN");
	ropFields.Add("CDAT");
	ropFields.Add("DISP");
	ropFields.Add("LKAM");
	ropFields.Add("LKAN");
	ropFields.Add("LKAR");
	ropFields.Add("LKBZ");
	ropFields.Add("LKCO");
	ropFields.Add("LKDA");
	ropFields.Add("LKHC");
	ropFields.Add("LKNB");
	ropFields.Add("LKNM");
	ropFields.Add("LKST");
	ropFields.Add("LKTY");
	ropFields.Add("LKVB");
	ropFields.Add("LSTU");
	ropFields.Add("PERM");
	ropFields.Add("PRFL");
	ropFields.Add("URNO");
	ropFields.Add("USEC");
	ropFields.Add("USEU");
	ropFields.Add("VRGC");

	ropDesription.Add(GetString(IDS_STRING276));
	ropDesription.Add(GetString(IDS_STRING163));
	ropDesription.Add(GetString(IDS_STRING277));
	ropDesription.Add(GetString(IDS_STRING278));
	ropDesription.Add(GetString(IDS_STRING279));
	ropDesription.Add(GetString(IDS_STRING280));
	ropDesription.Add(GetString(IDS_STRING281));
	ropDesription.Add(GetString(IDS_STRING283));
	ropDesription.Add(GetString(IDS_STRING284));
	ropDesription.Add(GetString(IDS_STRING285));
	ropDesription.Add(GetString(IDS_STRING286));
	ropDesription.Add(GetString(IDS_STRING235));
	ropDesription.Add(GetString(IDS_STRING278));
	ropDesription.Add(GetString(IDS_STRING288));
	ropDesription.Add(GetString(IDS_STRING289));
	ropDesription.Add(GetString(IDS_STRING165));
	ropDesription.Add(GetString(IDS_STRING290));
	ropDesription.Add(GetString(IDS_STRING166));
	ropDesription.Add(GetString(IDS_STRING162));
	ropDesription.Add(GetString(IDS_STRING164));
	ropDesription.Add(GetString(IDS_STRING167));
	ropDesription.Add(GetString(IDS_STRING291));

	ropType.Add("String");
	ropType.Add("Date");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("Date");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
}

//--REGISTER----------------------------------------------------------------------------------------------

void CedaGhsData::Register(void)
{
	ogDdx.Register((void *)this,BC_GHS_CHANGE,	CString("GHSDATA"), CString("Ghs-changed"),	ProcessGhsCf);
	ogDdx.Register((void *)this,BC_GHS_NEW,		CString("GHSDATA"), CString("Ghs-new"),		ProcessGhsCf);
	ogDdx.Register((void *)this,BC_GHS_DELETE,	CString("GHSDATA"), CString("Ghs-deleted"),	ProcessGhsCf);
}

//--~CEDADATA----------------------------------------------------------------------------------------------

CedaGhsData::~CedaGhsData(void)
{
	omRecInfo.DeleteAll();
	ClearAll();
}

//--CLEAR-ALL----------------------------------------------------------------------------------------------

void CedaGhsData::ClearAll(bool bpWithRegistration)
{
    omUrnoMap.RemoveAll();
    omData.DeleteAll();
	if(bpWithRegistration)
	{
		ogDdx.UnRegister(this,NOTUSED);
	}
}

//--READ-ALL-----------------------------------------------------------------------------------------------

bool CedaGhsData::Read(char *pspWhere /*NULL*/)
{
    // Select data from the database
	bool ilRc = true;
    omUrnoMap.RemoveAll();
    omData.DeleteAll();
	if(pspWhere == NULL)
	{	
		ilRc = CedaAction("RT");
	}
	else
	{
		ilRc = CedaAction("RT", pspWhere);
	}
	if (ilRc != true)
	{
			if(!omLastErrorMessage.IsEmpty())
			{
				if(omLastErrorMessage.Find("ORA") != -1)
				{
					char pclMsg[2048]="";
					sprintf(pclMsg, "%s", omLastErrorMessage.GetBuffer(0));
					::MessageBox(NULL,pclMsg,GetString(IDS_STRING188),MB_OK);
					return false;
				}
			}
	}
	// Load data from CedaData into the dynamic array of record
	for (int ilLc = 0; ilRc == true; ilLc++)
	{
		GHSDATA *prlGhs = new GHSDATA;
		if ((ilRc = GetFirstBufferRecord(prlGhs)) == true)
		{
			omData.Add(prlGhs);//Update omData
			omUrnoMap.SetAt((void *)prlGhs->Urno,prlGhs);
		}
		else
		{
			delete prlGhs;
		}
	}
    return true;

}

//--INSERT-------------------------------------------------------------------------------------------------

bool CedaGhsData::Insert(GHSDATA *prpGhs)
{
	prpGhs->IsChanged = DATA_NEW;
	if(Save(prpGhs) == false) return false; //Update Database
	InsertInternal(prpGhs);
    return true;
}

//--INSERT-INTERNAL--------------------------------------------------------------------------------------

bool CedaGhsData::InsertInternal(GHSDATA *prpGhs)
{
	omData.Add(prpGhs);//Update omData
	omUrnoMap.SetAt((void *)prpGhs->Urno,prpGhs);
	ogDdx.DataChanged((void *)this, GHS_NEW,(void *)prpGhs ); //Update Viewer
    return true;
}

//--DELETE-------------------------------------------------------------------------------------------------

bool CedaGhsData::Delete(long lpUrno)
{
	GHSDATA *prlGhs = GetGhsByUrno(lpUrno);
	if (prlGhs != NULL)
	{
		prlGhs->IsChanged = DATA_DELETED;
		if(Save(prlGhs) == false) return false; //Update Database
		DeleteInternal(prlGhs);
	}
    return true;
}

//--DELETE-INTERNAL----------------------------------------------------------------------------------------

bool CedaGhsData::DeleteInternal(GHSDATA *prpGhs)
{
	ogDdx.DataChanged((void *)this,GHS_DELETE,(void *)prpGhs); //Update Viewer
	omUrnoMap.RemoveKey((void *)prpGhs->Urno);
	int ilGhsCount = omData.GetSize();
	for (int ilLc = 0; ilLc < ilGhsCount; ilLc++)
	{
		if (omData[ilLc].Urno == prpGhs->Urno)
		{
			omData.DeleteAt(ilLc);//Update omData
			break;
		}
	}
    return true;
}

//--UPDATE-------------------------------------------------------------------------------------------------

bool CedaGhsData::Update(GHSDATA *prpGhs)
{
	if (GetGhsByUrno(prpGhs->Urno) != NULL)
	{
		if (prpGhs->IsChanged == DATA_UNCHANGED)
		{
			prpGhs->IsChanged = DATA_CHANGED;
		}
		if(Save(prpGhs) == false) return false; //Update Database
		UpdateInternal(prpGhs);
	}
    return true;
}

//--UPDATE-INTERNAL--------------------------------------------------------------------------------------------

bool CedaGhsData::UpdateInternal(GHSDATA *prpGhs)
{
	GHSDATA *prlGhs = GetGhsByUrno(prpGhs->Urno);
	if (prlGhs != NULL)
	{
		*prlGhs = *prpGhs; //Update omData
		ogDdx.DataChanged((void *)this,GHS_CHANGE,(void *)prlGhs); //Update Viewer
	}
    return true;
}

//--GET-BY-URNO--------------------------------------------------------------------------------------------

GHSDATA *CedaGhsData::GetGhsByUrno(long lpUrno)
{
	GHSDATA  *prlGhs;
	if (omUrnoMap.Lookup((void *)lpUrno,(void *& )prlGhs) == TRUE)
	{
		return prlGhs;
	}
	return NULL;
}

//--READSPECIALDATA-------------------------------------------------------------------------------------

bool CedaGhsData::ReadSpecialData(CCSPtrArray<GHSDATA> *popGhs,char *pspWhere,char *pspFieldList,bool ipSYS/*=true*/)
{
	bool ilRc = true;
	char pclFieldList[256] = " ";

	if(strlen(pspFieldList) > 0) 
	{
		strcpy(pclFieldList, pspFieldList);
	}
	if(ipSYS == true) 
	{
		if (CedaAction("SYS",pcmTableName,pclFieldList,pspWhere,"",pcgDataBuf) == false)
		{
			if(!omLastErrorMessage.IsEmpty())
			{
				if(omLastErrorMessage.Find("ORA") != -1)
				{
					char pclMsg[2048]="";
					sprintf(pclMsg, "%s", omLastErrorMessage.GetBuffer(0));
					::MessageBox(NULL,pclMsg,GetString(IDS_STRING188),MB_OK);
					return false;
				}
			}
		}
	}
	else
	{
		if (CedaAction("RT",pcmTableName,pclFieldList,pspWhere,"",pcgDataBuf) == false)
		{
			if(!omLastErrorMessage.IsEmpty())
			{
				if(omLastErrorMessage.Find("ORA") != -1)
				{
					char pclMsg[2048]="";
					sprintf(pclMsg, "%s", omLastErrorMessage.GetBuffer(0));
					::MessageBox(NULL,pclMsg,GetString(IDS_STRING188),MB_OK);
					return false;
				}
			}
		}
	}
	if(popGhs != NULL)
	{
		for (int ilLc = 0; ilRc == true; ilLc++)
		{
			GHSDATA *prpGhs = new GHSDATA;
			if ((ilRc = GetBufferRecord(ilLc,prpGhs,CString(pclFieldList))) == true)
			{
				popGhs->Add(prpGhs);
			}
			else
			{
				delete prpGhs;
			}
		}
		if(popGhs->GetSize() == 0) return false;
	}
    return true;
}

//--SAVE---------------------------------------------------------------------------------------------------

bool CedaGhsData::Save(GHSDATA *prpGhs)
{
	bool ilRc = true;
	CString olListOfData;
	char pclSelection[124];
	char pclData[524];

	if (prpGhs->IsChanged == DATA_UNCHANGED)
	{
		return true; // no change, nothing to do
	}
	switch(prpGhs->IsChanged)
	{
	case DATA_NEW:
		MakeCedaData(&omRecInfo,olListOfData,prpGhs);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("IRT","","",pclData);
		prpGhs->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_CHANGED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpGhs->Urno);
		MakeCedaData(&omRecInfo,olListOfData,prpGhs);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("URT",pclSelection,"",pclData);
		prpGhs->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_DELETED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpGhs->Urno);
		ilRc = CedaAction("DRT",pclSelection);
		break;
	}
	if(!omLastErrorMessage.IsEmpty())
	{
		if(omLastErrorMessage.Find("ORA") != -1)
		{
			char pclMsg[2048]="";
			sprintf(pclMsg, "%s", omLastErrorMessage.GetBuffer(0));
			::MessageBox(NULL,pclMsg,GetString(IDS_STRING188),MB_OK);
			return false;
		}
	}
    return ilRc;
}

//--PROCESS-CF---------------------------------------------------------------------------------------------

void  ProcessGhsCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	ogGhsData.ProcessBc(ipDDXType,vpDataPointer,ropInstanceName);
}

//--PROCESS-BC---------------------------------------------------------------------------------------------

void  CedaGhsData::ProcessBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	struct BcStruct *prlGhsData;
	prlGhsData = (struct BcStruct *) vpDataPointer;
	CString olSelection = (CString)prlGhsData->Selection;
	GHSDATA *prlGhs;
	if(ipDDXType == BC_GHS_NEW)
	{
		prlGhs = new GHSDATA;
		GetRecordFromItemList(prlGhs,prlGhsData->Fields,prlGhsData->Data);
		InsertInternal(prlGhs);
	}
	if(ipDDXType == BC_GHS_CHANGE)
	{
		long llUrno;
		if (olSelection.Find('\'') != -1)
		{
			llUrno = GetUrnoFromSelection(prlGhsData->Selection);
		}
		else
		{
			int ilFirst = olSelection.Find("=")+2;
			int ilLast  = olSelection.GetLength();
			llUrno = atol(olSelection.Mid(ilFirst,ilLast-ilFirst));
		}
		prlGhs = GetGhsByUrno(llUrno);
		if(prlGhs != NULL)
		{
			GetRecordFromItemList(prlGhs,prlGhsData->Fields,prlGhsData->Data);
			UpdateInternal(prlGhs);
		}
		else
		{
			GHSDATA *prlGhsNew = new GHSDATA;
			GetRecordFromItemList(prlGhsNew,prlGhsData->Fields,prlGhsData->Data);
			InsertInternal(prlGhsNew);
		}
	}
	if(ipDDXType == BC_GHS_DELETE)
	{
		long llUrno;
		CString olSelection = (CString)prlGhsData->Selection;
		if (olSelection.Find('\'') != -1)
		{
			llUrno = GetUrnoFromSelection(prlGhsData->Selection);
		}
		else
		{
			int ilFirst = olSelection.Find("=")+2;
			int ilLast  = olSelection.GetLength();
			llUrno = atol(olSelection.Mid(ilFirst,ilLast-ilFirst));
		}
		prlGhs = GetGhsByUrno(llUrno);
		if (prlGhs != NULL)
		{
			DeleteInternal(prlGhs);
		}
	}
}

//---------------------------------------------------------------------------------------------------------
