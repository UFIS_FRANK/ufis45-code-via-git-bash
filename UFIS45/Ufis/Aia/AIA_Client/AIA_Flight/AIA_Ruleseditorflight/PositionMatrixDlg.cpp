// PositionMatrixDlg.cpp : implementation file
//

#include "stdafx.h"
#include "rules.h"
#include "resrc1.h"
#include "PositionMatrixDlg.h"
#include "GridFenster.h"
#include "BasicData.h"
#include "CedaGrnData.h"
#include "CedaPstData.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// PositionMatrixDlg dialog


PositionMatrixDlg::PositionMatrixDlg(const CString& ropFromPos,const CString& ropToPos,CWnd* pParent /*=NULL*/)
	: CDialog(PositionMatrixDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(PositionMatrixDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	static blFirst = true;
	if (blFirst)
	{
		blFirst = false;
		GXInit();
	}

	pomMatrix = new CGridFenster(this);
	pomModified = NULL;
	omFromPos = ropFromPos;
	omToPos	  = ropToPos;	
}

PositionMatrixDlg::~PositionMatrixDlg()
{
	delete pomMatrix;
	delete[] pomModified;
}

void PositionMatrixDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(PositionMatrixDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(PositionMatrixDlg, CDialog)
	//{{AFX_MSG_MAP(PositionMatrixDlg)
	ON_WM_PAINT()
	ON_MESSAGE(GRID_MESSAGE_CELLCLICK,OnGridMessageCellClick)
	ON_MESSAGE(GRID_MESSAGE_ENDEDITING,OnGridMessageEndEditing)
	ON_WM_SIZE()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// PositionMatrixDlg message handlers

void PositionMatrixDlg::OnOK() 
{
	// TODO: Add extra validation here
	if (!UpdateData(TRUE))
		return;

	CString olACFrom;
	CString olACTo;
	CString olValue;
	for (int i = 1; i <= pomMatrix->GetRowCount(); i++)
	{
		for (int j = 1; j <= pomMatrix->GetColCount(); j++)
		{
			if (pomModified[(i - 1) * pomMatrix->GetColCount() + (j-1)])
			{
				olACFrom = pomMatrix->GetValueRowCol(i,0);
				olACTo	 = pomMatrix->GetValueRowCol(0,j);
				olValue  = pomMatrix->GetValueRowCol(i,j);
				if (!ogBasicData.SetDependingAircraftGroups(ogPstData.GetUrnoByPst(omFromPos.GetBuffer(0)),ogPstData.GetUrnoByPst(omToPos.GetBuffer(0)),olACFrom,olACTo,olValue))
				{
					CString olMsg;
					olMsg.Format("Can\'t update field[%d,%d]",i,j);
					AfxMessageBox(olMsg);
					return;
				}
				else
				{
					TRACE("Saving [%d,%d] = (%s)\n",i,j,olValue);
				}

				if (!ogBasicData.SetDependingAircraftGroups(ogPstData.GetUrnoByPst(omToPos.GetBuffer(0)),ogPstData.GetUrnoByPst(omFromPos.GetBuffer(0)),olACTo,olACFrom,olValue))
				{
					CString olMsg;
					olMsg.Format("Can\'t update field[%d,%d]",i,j);
					AfxMessageBox(olMsg);
					return;
				}
				else
				{
					TRACE("Saving [%d,%d] = (%s)\n",i,j,olValue);
				}
			}
		}
	}

	CDialog::OnOK();
}

BOOL PositionMatrixDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	pomMatrix->SubclassDlgItem(IDC_MATRIX,this);

	CString olCaption;
	olCaption.Format(GetString(IDS_STRING1459),omFromPos,omToPos);
	this->SetWindowText(olCaption);

	CCSPtrArray<GRNDATA> olAircraftGroups;	
	ogGrnData.GetGrnsByTabn(olAircraftGroups,"ACTTAB","POPS");	
	bool blValid;
	CString olValue;
	for (int i = olAircraftGroups.GetSize() - 1; i >= 0; i--)
	{
		blValid = false;
		olValue = olAircraftGroups[i].Grpn;
		if (olValue.Find(this->omFromPos) == 0)
			blValid = true;
		else if (olValue.Find(this->omToPos) == 0)
			blValid = true;
		else if (olValue.Find('*') == 0)
			blValid = true;

		if (!blValid)
			olAircraftGroups.DeleteAt(i);
	}

	// initialize selection grid
	pomMatrix->Initialize();
	pomMatrix->SetAllowMultiSelect(false);
	pomMatrix->SetSortingEnabled(false);

	// disable immediate update
	pomMatrix->LockUpdate(TRUE);
	pomMatrix->GetParam()->EnableUndo(FALSE);
	// column width is static
	pomMatrix->GetParam()->EnableTrackColWidth(FALSE);
	// row height is static
	pomMatrix->GetParam()->EnableTrackRowHeight(FALSE);
	// column headers aren't numbered
	pomMatrix->GetParam()->SetNumberedColHeaders(FALSE);

	pomMatrix->SetColCount(olAircraftGroups.GetSize());

	// set the size of the row number
	pomMatrix->SetRowCount(olAircraftGroups.GetSize());

	CGXStyle olStyle;
	olStyle.SetEnabled(FALSE);
	olStyle.SetReadOnly(TRUE);
	pomMatrix->SetStyleRange(CGXRange(0,0),olStyle.SetValue("A/C group"));
	pomMatrix->SetColWidth(0,0,100);

	pomModified = new bool[olAircraftGroups.GetSize() * olAircraftGroups.GetSize()];
	memset(pomModified,false,olAircraftGroups.GetSize() * olAircraftGroups.GetSize() * sizeof(bool));

	for (i = 1; i <= olAircraftGroups.GetSize();i++)
	{
		pomMatrix->SetStyleRange(CGXRange(0,i),olStyle.SetValue(olAircraftGroups[i-1].Grpn));
		pomMatrix->SetStyleRange(CGXRange(i,0),olStyle.SetValue(olAircraftGroups[i-1].Grpn));
		pomMatrix->SetColWidth(i,i,100);
	}

	olAircraftGroups.DeleteAll();

	pomMatrix->SetRowHeight(0, 0, 30);

	pomMatrix->SetScrollBarMode(SB_BOTH,gxnAutomatic|gxnEnhanced,TRUE);


	// enable immediate update
	pomMatrix->LockUpdate(FALSE);

	UpdateGrid();

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void PositionMatrixDlg::UpdateGrid()
{
	pomMatrix->GetParam()->SetLockReadOnly(FALSE);

	// enable immediate update
	pomMatrix->LockUpdate(FALSE);

	CGXStyle olStyle;
	olStyle.SetEnabled(TRUE);
	olStyle.SetReadOnly(TRUE);

	for (int i = 1; i <= pomMatrix->GetRowCount(); i++)		
	{
		
		for (int j = 1; j <= pomMatrix->GetColCount(); j++)
		{
			CString olACFrom= pomMatrix->GetValueRowCol(i,0);
			CString olACTo	= pomMatrix->GetValueRowCol(0,j);
			
			CString olValue = ogBasicData.GetDependingAircraftGroups(ogPstData.GetUrnoByPst(omFromPos.GetBuffer(0)),ogPstData.GetUrnoByPst(omToPos.GetBuffer(0)),olACFrom,olACTo);
			switch(olValue[0])
			{
			case 'Y' :
				pomMatrix->SetStyleRange(CGXRange(i,j,i,j),
											olStyle.SetValue("Y")
											.SetHorizontalAlignment(DT_CENTER)
											.SetInterior(RGB(0,255,0))														
										);
			break;
			case 'N' :
				pomMatrix->SetStyleRange(CGXRange(i,j,i,j),
											olStyle.SetValue("N")
											.SetHorizontalAlignment(DT_CENTER)
											.SetInterior(RGB(255,0,0))														
										);
			break;
			case ' ' :
				pomMatrix->SetStyleRange(CGXRange(i,j,i,j),
											olStyle.SetValue(" ")
											.SetHorizontalAlignment(DT_CENTER)
											.SetInterior(RGB(255,255,255))														
										);
			break;
			}
		}
	}
	
	pomMatrix->GetParam()->SetLockReadOnly(true);
	pomMatrix->Redraw();

}

void PositionMatrixDlg::OnPaint() 
{
	CPaintDC dc(this); // device context for painting
	
	// TODO: Add your message handler code here
	CRect olRect;
	pomMatrix->GetWindowRect(&olRect);
	this->ScreenToClient(&olRect);
	
	::SetGraphicsMode(dc.m_hDC,GM_ADVANCED);

	CFont *polFont = this->GetFont();
	if (polFont)
	{
		CFont olFont;
		LOGFONT olLogFont;
		polFont->GetLogFont(&olLogFont);
		olLogFont.lfOrientation = 0;
		olLogFont.lfEscapement  = 0;
		olLogFont.lfWeight		= 700;
		olFont.CreateFontIndirect(&olLogFont);
		polFont = dc.SelectObject(&olFont);

		dc.SetBkColor(WHITE);
		
		CString olName;
		olName.Format(GetString(IDS_STRING1460),omToPos);
		dc.TextOut(olRect.TopLeft().x+20,olRect.TopLeft().y - 15,olName);

		dc.SelectObject(polFont);

		olFont.DeleteObject();

		olLogFont.lfOrientation = 900;
		olLogFont.lfEscapement  = 900;
		olLogFont.lfWeight		= 700;
		olFont.CreateFontIndirect(&olLogFont);
		polFont = dc.SelectObject(&olFont);

		olName.Format(GetString(IDS_STRING1460),omFromPos);
		dc.TextOut(olRect.TopLeft().x-20,olRect.BottomRight().y - 15,olName);
		dc.SelectObject(polFont);

		olFont.DeleteObject();
	}

	
	// Do not call CDialog::OnPaint() for painting messages
}


LONG PositionMatrixDlg::OnGridMessageEndEditing(WPARAM wParam,LPARAM lParam)
{
	CELLPOS *prlPos = (CELLPOS *)lParam;
	
	if (prlPos)
	{
		if (prlPos->Row > 0 && prlPos->Col > 0)
		{
		}
	}

	return 1;
}

LONG PositionMatrixDlg::OnGridMessageCellClick(WPARAM wParam,LPARAM lParam)
{
	CELLPOS *prlPos = (CELLPOS *)lParam;
	
	if (prlPos)
	{
		if (prlPos->Row > 0 && prlPos->Col > 0)
		{
			
			CGXStyle olStyle;
			olStyle.SetEnabled(TRUE);
			olStyle.SetReadOnly(TRUE);
			
			pomMatrix->GetParam()->SetLockReadOnly(FALSE);
//			pomConflictSetup->LockUpdate(FALSE);

			CString olValue = pomMatrix->GetValueRowCol(prlPos->Row,prlPos->Col);
			switch(olValue[0])
			{
			case 'Y' :
				pomMatrix->SetStyleRange(CGXRange(prlPos->Row,prlPos->Col,prlPos->Row,prlPos->Col),
											olStyle.SetValue("N")
											.SetHorizontalAlignment(DT_CENTER)
											.SetInterior(RGB(255,0,0))														
										);
			break;
			case 'N' :
				pomMatrix->SetStyleRange(CGXRange(prlPos->Row,prlPos->Col,prlPos->Row,prlPos->Col),
											olStyle.SetValue(" ")
											.SetHorizontalAlignment(DT_CENTER)
											.SetInterior(RGB(255,255,255))														
										);
			break;
			case ' ' :
				pomMatrix->SetStyleRange(CGXRange(prlPos->Row,prlPos->Col,prlPos->Row,prlPos->Col),
											olStyle.SetValue("Y")
											.SetHorizontalAlignment(DT_CENTER)
											.SetInterior(RGB(0,255,0))														
										);
			break;
			}

			pomModified[(prlPos->Row - 1) * pomMatrix->GetColCount() + (prlPos->Col - 1)] = true;

			pomMatrix->GetParam()->SetLockReadOnly(TRUE);
			pomMatrix->RedrawRowCol(prlPos->Row,prlPos->Col,prlPos->Row,prlPos->Col);
//			pomMatrix->Redraw();
		}
	}
	return 1;
}

void PositionMatrixDlg::OnSize(UINT nType, int cx, int cy) 
{
	CDialog::OnSize(nType, cx, cy);
	
	// TODO: Add your message handler code here
	if (pomMatrix != NULL && ::IsWindow(pomMatrix->m_hWnd))
	{
		CRect olRect;
		pomMatrix->GetWindowRect(&olRect);
		this->ScreenToClient(&olRect); 
		cx = 0.95 * cx;	// with aspect ratio between child window width / dialog width
		cy = 0.80 * cy;	// with aspect ration between child window height / dialog height

		int dx = cx - olRect.Width();
		int dy = cy - olRect.Height();

		pomMatrix->MoveWindow(olRect.TopLeft().x,olRect.TopLeft().y,cx,cy);
//		pomMatrix->Redraw();

		CWnd *polWnd = GetDlgItem(IDOK);
		if (polWnd)
		{
			polWnd->GetWindowRect(&olRect);
			this->ScreenToClient(&olRect); 
			olRect.OffsetRect(dx,dy);
			polWnd->MoveWindow(&olRect,FALSE);
		}

		polWnd = GetDlgItem(IDCANCEL);
		if (polWnd)
		{
			polWnd->GetWindowRect(&olRect);
			this->ScreenToClient(&olRect); 
			olRect.OffsetRect(dx,dy);
			polWnd->MoveWindow(&olRect,FALSE);
		}

		this->Invalidate();
	}
}

