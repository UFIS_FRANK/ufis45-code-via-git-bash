// CedaPfcData.cpp
 
#include "stdafx.h"
#include "CedaPfcData.h"


void ProcessPfcCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

//--CEDADATA-----------------------------------------------------------------------------------------------
CedaPfcData ogPfcData;

CedaPfcData::CedaPfcData() : CCSCedaData(&ogCommHandler)
{
	// Create an array of CEDARECINFO for PFCDATA
	BEGIN_CEDARECINFO(PFCDATA,PfcDataRecInfo)
		FIELD_DATE(Cdat,"CDAT")
		FIELD_CHAR_TRIM(Dptc,"DPTC")
		FIELD_CHAR_TRIM(Fctc,"FCTC")
		FIELD_CHAR_TRIM(Fctn,"FCTN")
		FIELD_DATE(Lstu,"LSTU")
		FIELD_CHAR_TRIM(Prfl,"PRFL")
		FIELD_CHAR_TRIM(Rema,"REMA")
		FIELD_LONG(Urno,"URNO")
		FIELD_CHAR_TRIM(Usec,"USEC")
		FIELD_CHAR_TRIM(Useu,"USEU")

	END_CEDARECINFO //(PFCDATA)

	// Copy the record structure
	for (int i=0; i< sizeof(PfcDataRecInfo)/sizeof(PfcDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&PfcDataRecInfo[i],sizeof(CEDARECINFO));
		omRecInfo.Add(prpCedaRecInfo);
	} // end for

	// initialize field names
	strcpy(pcmTableName,"PFC");
	strcat(pcmTableName,pcgTableExt);
	strcpy(pcmListOfFields,"CDAT,DPTC,FCTC,FCTN,LSTU,PRFL,REMA,URNO,USEC,USEU");
	pcmFieldList = pcmListOfFields; //because in CedaData pcmFieldList is just a pointer

	omData.RemoveAll();
}

//---------------------------------------------------------------------------------------------------------

void CedaPfcData::GetDataInfo(CStringArray &ropFields, CStringArray &ropDesription, CStringArray &ropType)
{
	ropFields.Add("CDAT");
	ropFields.Add("DPTC");
	ropFields.Add("FCTC");
	ropFields.Add("FCTN");
	ropFields.Add("LSTU");
	ropFields.Add("PRFL");
	ropFields.Add("REMA");
	ropFields.Add("URNO");
	ropFields.Add("USEC");
	ropFields.Add("USEU");



	ropDesription.Add(GetString(IDS_STRING163));
	ropDesription.Add(GetString(IDS_STRING301));
	ropDesription.Add(GetString(IDS_STRING201));
	ropDesription.Add(GetString(IDS_STRING203));
	ropDesription.Add(GetString(IDS_STRING165));
	ropDesription.Add(GetString(IDS_STRING166));
	ropDesription.Add(GetString(IDS_STRING185));
	ropDesription.Add(GetString(IDS_STRING162));
	ropDesription.Add(GetString(IDS_STRING164));
	ropDesription.Add(GetString(IDS_STRING167));



	ropType.Add("Date");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("Date");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
}//---------------------------------------
//--REGISTER----------------------------------------------------------------------------------------------

void CedaPfcData::Register(void)
{
	ogDdx.Register((void *)this,BC_PFC_CHANGE,	CString("PFCDATA"), CString("Pfc-changed"),	ProcessPfcCf);
	ogDdx.Register((void *)this,BC_PFC_NEW,		CString("PFCDATA"), CString("Pfc-new"),		ProcessPfcCf);
	ogDdx.Register((void *)this,BC_PFC_DELETE,	CString("PFCDATA"), CString("Pfc-deleted"),	ProcessPfcCf);
}

//--~CEDADATA----------------------------------------------------------------------------------------------

CedaPfcData::~CedaPfcData(void)
{
	omRecInfo.DeleteAll();
	ClearAll();
}

//--CLEAR-ALL----------------------------------------------------------------------------------------------

void CedaPfcData::ClearAll(bool bpWithRegistration)
{
    omUrnoMap.RemoveAll();
    omData.DeleteAll();
	if(bpWithRegistration)
	{
		ogDdx.UnRegister(this,NOTUSED);
	}
}

//--READ-ALL-----------------------------------------------------------------------------------------------

bool CedaPfcData::Read(char *pspWhere /*NULL*/)
{
    // Select data from the database
	bool ilRc = true;
    omUrnoMap.RemoveAll();
    omData.DeleteAll();
	if(pspWhere == NULL)
	{	
		ilRc = CedaAction("RT");
	}
	else
	{
		ilRc = CedaAction("RT", pspWhere);
	}
	if (ilRc != true)
	{
			if(!omLastErrorMessage.IsEmpty())
			{
				if(omLastErrorMessage.Find("ORA") != -1)
				{
					char pclMsg[2048]="";
					sprintf(pclMsg, "%s", omLastErrorMessage.GetBuffer(0));
					::MessageBox(NULL,pclMsg,GetString(IDS_STRING188),MB_OK);
					return false;
				}
			}
	}
	// Load data from CedaData into the dynamic array of record
	for (int ilLc = 0; ilRc == true; ilLc++)
	{
		PFCDATA *prlPfc = new PFCDATA;
		if ((ilRc = GetFirstBufferRecord(prlPfc)) == true)
		{
			omData.Add(prlPfc);//Update omData
			omUrnoMap.SetAt((void *)prlPfc->Urno,prlPfc);
		}
		else
		{
			delete prlPfc;
		}
	}
    return true;
}

//--INSERT-------------------------------------------------------------------------------------------------

bool CedaPfcData::Insert(PFCDATA *prpPfc)
{
	prpPfc->IsChanged = DATA_NEW;
	if(Save(prpPfc) == false) return false; //Update Database
	InsertInternal(prpPfc);
    return true;
}

//--INSERT-INTERNAL--------------------------------------------------------------------------------------

bool CedaPfcData::InsertInternal(PFCDATA *prpPfc)
{
	ogDdx.DataChanged((void *)this, PFC_NEW,(void *)prpPfc ); //Update Viewer
	omData.Add(prpPfc);//Update omData
	omUrnoMap.SetAt((void *)prpPfc->Urno,prpPfc);
    return true;
}

//--DELETE-------------------------------------------------------------------------------------------------

bool CedaPfcData::Delete(long lpUrno)
{
	PFCDATA *prlPfc = GetPfcByUrno(lpUrno);
	if (prlPfc != NULL)
	{
		prlPfc->IsChanged = DATA_DELETED;
		if(Save(prlPfc) == false) return false; //Update Database
		DeleteInternal(prlPfc);
	}
    return true;
}

//--DELETE-INTERNAL----------------------------------------------------------------------------------------

bool CedaPfcData::DeleteInternal(PFCDATA *prpPfc)
{
	ogDdx.DataChanged((void *)this,PFC_DELETE,(void *)prpPfc); //Update Viewer
	omUrnoMap.RemoveKey((void *)prpPfc->Urno);
	int ilPfcCount = omData.GetSize();
	for (int ilLc = 0; ilLc < ilPfcCount; ilLc++)
	{
		if (omData[ilLc].Urno == prpPfc->Urno)
		{
			omData.DeleteAt(ilLc);//Update omData
			break;
		}
	}
    return true;
}

//--UPDATE-------------------------------------------------------------------------------------------------

bool CedaPfcData::Update(PFCDATA *prpPfc)
{
	if (GetPfcByUrno(prpPfc->Urno) != NULL)
	{
		if (prpPfc->IsChanged == DATA_UNCHANGED)
		{
			prpPfc->IsChanged = DATA_CHANGED;
		}
		if(Save(prpPfc) == false) return false; //Update Database
		UpdateInternal(prpPfc);
	}
    return true;
}

//--UPDATE-INTERNAL--------------------------------------------------------------------------------------------

bool CedaPfcData::UpdateInternal(PFCDATA *prpPfc)
{
	PFCDATA *prlPfc = GetPfcByUrno(prpPfc->Urno);
	if (prlPfc != NULL)
	{
		*prlPfc = *prpPfc; //Update omData
		ogDdx.DataChanged((void *)this,PFC_CHANGE,(void *)prlPfc); //Update Viewer
	}
    return true;
}

//--GET-BY-URNO--------------------------------------------------------------------------------------------

PFCDATA *CedaPfcData::GetPfcByUrno(long lpUrno)
{
	PFCDATA  *prlPfc;
	if (omUrnoMap.Lookup((void *)lpUrno,(void *& )prlPfc) == TRUE)
	{
		return prlPfc;
	}
	return NULL;
}

//--READSPECIALDATA-------------------------------------------------------------------------------------

bool CedaPfcData::ReadSpecialData(CCSPtrArray<PFCDATA> *popPfc,char *pspWhere,char *pspFieldList,bool ipSYS/*=true*/)
{
	bool ilRc = true;
	char pclFieldList[256] = " ";

	if(strlen(pspFieldList) > 0) 
	{
		strcpy(pclFieldList, pspFieldList);
	}
	if(ipSYS == true) 
	{
		if (CedaAction("SYS",pcmTableName,pclFieldList,pspWhere,"",pcgDataBuf) == false)
		{
			if(!omLastErrorMessage.IsEmpty())
			{
				if(omLastErrorMessage.Find("ORA") != -1)
				{
					char pclMsg[2048]="";
					sprintf(pclMsg, "%s", omLastErrorMessage.GetBuffer(0));
					::MessageBox(NULL,pclMsg,GetString(IDS_STRING188),MB_OK);
					return false;
				}
			}
		}
	}
	else
	{
		if (CedaAction("RT",pcmTableName,pclFieldList,pspWhere,"",pcgDataBuf) == false)
		{
			if(!omLastErrorMessage.IsEmpty())
			{
				if(omLastErrorMessage.Find("ORA") != -1)
				{
					char pclMsg[2048]="";
					sprintf(pclMsg, "%s", omLastErrorMessage.GetBuffer(0));
					::MessageBox(NULL,pclMsg,GetString(IDS_STRING188),MB_OK);
					return false;
				}
			}
		}
	}
	if(popPfc != NULL)
	{
		for (int ilLc = 0; ilRc == true; ilLc++)
		{
			PFCDATA *prpPfc = new PFCDATA;
			if ((ilRc = GetBufferRecord(ilLc,prpPfc,CString(pclFieldList))) == true)
			{
				popPfc->Add(prpPfc);
			}
			else
			{
				delete prpPfc;
			}
		}
		if(popPfc->GetSize() == 0) return false;
	}
    return true;
}

//--SAVE---------------------------------------------------------------------------------------------------

bool CedaPfcData::Save(PFCDATA *prpPfc)
{
	bool ilRc = true;
	CString olListOfData;
	char pclSelection[124];
	char pclData[524];

	if (prpPfc->IsChanged == DATA_UNCHANGED)
	{
		return true; // no change, nothing to do
	}
	switch(prpPfc->IsChanged)
	{
	case DATA_NEW:
		MakeCedaData(&omRecInfo,olListOfData,prpPfc);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("IRT","","",pclData);
		prpPfc->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_CHANGED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpPfc->Urno);
		MakeCedaData(&omRecInfo,olListOfData,prpPfc);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("URT",pclSelection,"",pclData);
		prpPfc->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_DELETED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpPfc->Urno);
		ilRc = CedaAction("DRT",pclSelection);
		break;
	}
	if(!omLastErrorMessage.IsEmpty())
	{
		if(omLastErrorMessage.Find("ORA") != -1)
		{
			char pclMsg[2048]="";
			sprintf(pclMsg, "%s", omLastErrorMessage.GetBuffer(0));
			::MessageBox(NULL,pclMsg,GetString(IDS_STRING188),MB_OK);
			return false;
		}
	}
    return ilRc;
}

//--PROCESS-CF---------------------------------------------------------------------------------------------

void  ProcessPfcCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	ogPfcData.ProcessBc(ipDDXType,vpDataPointer,ropInstanceName);
}

//--PROCESS-BC---------------------------------------------------------------------------------------------

void  CedaPfcData::ProcessBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	struct BcStruct *prlPfcData;
	prlPfcData = (struct BcStruct *) vpDataPointer;
	PFCDATA *prlPfc;
	if(ipDDXType == BC_PFC_NEW)
	{
		prlPfc = new PFCDATA;
		GetRecordFromItemList(prlPfc,prlPfcData->Fields,prlPfcData->Data);
		InsertInternal(prlPfc);
	}
	if(ipDDXType == BC_PFC_CHANGE)
	{
		long llUrno = GetUrnoFromSelection(prlPfcData->Selection);
		prlPfc = GetPfcByUrno(llUrno);
		if(prlPfc != NULL)
		{
			GetRecordFromItemList(prlPfc,prlPfcData->Fields,prlPfcData->Data);
			UpdateInternal(prlPfc);
		}
	}
	if(ipDDXType == BC_PFC_DELETE)
	{
		long llUrno;
		CString olSelection = (CString)prlPfcData->Selection;
		if (olSelection.Find('\'') != -1)
		{
			llUrno = GetUrnoFromSelection(prlPfcData->Selection);
		}
		else
		{
			int ilFirst = olSelection.Find("=")+2;
			int ilLast  = olSelection.GetLength();
			llUrno = atol(olSelection.Mid(ilFirst,ilLast-ilFirst));
		}
		prlPfc = GetPfcByUrno(llUrno);
		if (prlPfc != NULL)
		{
			DeleteInternal(prlPfc);
		}
	}
}

//---------------------------------------------------------------------------------------------------------
