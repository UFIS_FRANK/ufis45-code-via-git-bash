# Microsoft Developer Studio Project File - Name="Rules" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Application" 0x0101

CFG=Rules - Win32 Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "Rules.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "Rules.mak" CFG="Rules - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "Rules - Win32 Release" (based on "Win32 (x86) Application")
!MESSAGE "Rules - Win32 Debug" (based on "Win32 (x86) Application")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""
CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "Rules - Win32 Release"

# PROP BASE Use_MFC 2
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 1
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "c:\Ufis_Bin\Release"
# PROP Intermediate_Dir "C:\Ufis_intermediate\Ruleseditorflight\release"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MD /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_AFXDLL" /Yu"stdafx.h" /FD /c
# ADD CPP /nologo /MT /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /Yu"stdafx.h" /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /o "NUL" /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /o "NUL" /win32
# ADD BASE RSC /l 0x407 /d "NDEBUG" /d "_AFXDLL"
# ADD RSC /l 0x407 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 /nologo /subsystem:windows /machine:I386
# ADD LINK32 C:\Ufis_Bin\release\ufis32.lib c:\Ufis_Bin\ClassLib\release\ccsclass.lib /nologo /subsystem:windows /machine:I386

!ELSEIF  "$(CFG)" == "Rules - Win32 Debug"

# PROP BASE Use_MFC 2
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 1
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "C:\Ufis_Bin\debug"
# PROP Intermediate_Dir "C:\Ufis_intermediate\Ruleseditorflight\debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MDd /W3 /Gm /GX /Zi /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_AFXDLL" /Yu"stdafx.h" /FD /c
# ADD CPP /nologo /MTd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /FR /Yu"stdafx.h" /FD /c
# ADD BASE MTL /nologo /D "_DEBUG" /mktyplib203 /o "NUL" /win32
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /o "NUL" /win32
# ADD BASE RSC /l 0x407 /d "_DEBUG" /d "_AFXDLL"
# ADD RSC /l 0x809 /d "_DEBUG _ENGLISH"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 /nologo /subsystem:windows /debug /machine:I386 /pdbtype:sept
# ADD LINK32 C:\Ufis_Bin\debug\ufis32.lib c:\Ufis_Bin\ClassLib\debug\ccsclass.lib /nologo /subsystem:windows /debug /machine:I386 /pdbtype:sept

!ENDIF 

# Begin Target

# Name "Rules - Win32 Release"
# Name "Rules - Win32 Debug"
# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Source File

SOURCE=.\AcrTableDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\ActDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\BackGround.cpp
# End Source File
# Begin Source File

SOURCE=.\BasicData.cpp
# End Source File
# Begin Source File

SOURCE=.\ButtonListDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\CCSGlobl.cpp
# End Source File
# Begin Source File

SOURCE=.\CedaACRData.Cpp
# End Source File
# Begin Source File

SOURCE=.\CedaACTData.Cpp
# End Source File
# Begin Source File

SOURCE=.\CedaAloData.cpp
# End Source File
# Begin Source File

SOURCE=.\CedaALTData.Cpp
# End Source File
# Begin Source File

SOURCE=.\CedaAPTData.Cpp
# End Source File
# Begin Source File

SOURCE=.\CedaBLTData.Cpp
# End Source File
# Begin Source File

SOURCE=.\CedaBsdData.cpp
# End Source File
# Begin Source File

SOURCE=.\CedaCfgData.cpp
# End Source File
# Begin Source File

SOURCE=.\CedaCICData.Cpp
# End Source File
# Begin Source File

SOURCE=.\CedaGATData.Cpp
# End Source File
# Begin Source File

SOURCE=.\CedaGegData.Cpp
# End Source File
# Begin Source File

SOURCE=.\CedaGhpData.cpp
# End Source File
# Begin Source File

SOURCE=.\CedaGhsData.cpp
# End Source File
# Begin Source File

SOURCE=.\CedaGpmData.cpp
# End Source File
# Begin Source File

SOURCE=.\CedaGrmData.cpp
# End Source File
# Begin Source File

SOURCE=.\CedaGrnData.cpp
# End Source File
# Begin Source File

SOURCE=.\CedaHTYData.Cpp
# End Source File
# Begin Source File

SOURCE=.\CedaInitModuData.Cpp
# End Source File
# Begin Source File

SOURCE=.\CedaNATData.Cpp
# End Source File
# Begin Source File

SOURCE=.\CedaPerData.Cpp
# End Source File
# Begin Source File

SOURCE=.\CedaPfcData.cpp
# End Source File
# Begin Source File

SOURCE=.\CedaPrcData.cpp
# End Source File
# Begin Source File

SOURCE=.\CedaPSTData.Cpp
# End Source File
# Begin Source File

SOURCE=.\CedaSgmData.cpp
# End Source File
# Begin Source File

SOURCE=.\CedaSgrData.cpp
# End Source File
# Begin Source File

SOURCE=.\CVIEWER.CPP
# End Source File
# Begin Source File

SOURCE=.\DutyPreferences.cpp
# End Source File
# Begin Source File

SOURCE=.\GatPosRules.cpp
# End Source File
# Begin Source File

SOURCE=.\GatRuleDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\GatRulesViewer.cpp
# End Source File
# Begin Source File

SOURCE=.\GhsList.cpp
# End Source File
# Begin Source File

SOURCE=.\Gr2Viewer.cpp
# End Source File
# Begin Source File

SOURCE=.\GridFenster.cpp
# End Source File
# Begin Source File

SOURCE=.\GroupNamesPage.cpp
# End Source File
# Begin Source File

SOURCE=.\GrRules2.cpp
# End Source File
# Begin Source File

SOURCE=.\InitialLoadDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\LoginDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\MakeHelp.bat
# End Source File
# Begin Source File

SOURCE=.\NatureAllocation.cpp
# End Source File
# Begin Source File

SOURCE=.\PositionMatrixDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\PosRulesViewer.cpp
# End Source File
# Begin Source File

SOURCE=.\PrcList.cpp
# End Source File
# Begin Source File

SOURCE=.\PremisPage.cpp
# End Source File
# Begin Source File

SOURCE=.\PremisPageDetailViewer.CPP
# End Source File
# Begin Source File

SOURCE=.\PremisPageGantt.CPP
# End Source File
# Begin Source File

SOURCE=.\PrivList.cpp
# End Source File
# Begin Source File

SOURCE=.\ProxyDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\PstRuleDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\RegisterDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\Rules.cpp
# End Source File
# Begin Source File

SOURCE=.\hlp\Rules.hpj
# End Source File
# Begin Source File

SOURCE=.\Rules.odl
# End Source File
# Begin Source File

SOURCE=.\Rules.rc
# End Source File
# Begin Source File

SOURCE=.\StdAfx.cpp
# ADD CPP /Yc"stdafx.h"
# End Source File
# Begin Source File

SOURCE=.\TABLE.CPP
# End Source File
# End Group
# Begin Group "Header Files"

# PROP Default_Filter "h;hpp;hxx;hm;inl"
# Begin Source File

SOURCE=.\BackGround.h
# End Source File
# Begin Source File

SOURCE=.\BasicData.h
# End Source File
# Begin Source File

SOURCE=.\ButtonListDlg.h
# End Source File
# Begin Source File

SOURCE=.\CCSGLOBL.H
# End Source File
# Begin Source File

SOURCE=.\CedaAloData.h
# End Source File
# Begin Source File

SOURCE=.\CedaCfgData.h
# End Source File
# Begin Source File

SOURCE=.\CedaInitModuData.h
# End Source File
# Begin Source File

SOURCE=.\CedaSgmData.h
# End Source File
# Begin Source File

SOURCE=.\CedaSgrData.h
# End Source File
# Begin Source File

SOURCE=.\DutyPreferences.h
# End Source File
# Begin Source File

SOURCE=.\GatPosRules.h
# End Source File
# Begin Source File

SOURCE=.\GatRuleDlg.h
# End Source File
# Begin Source File

SOURCE=.\GatRulesViewer.h
# End Source File
# Begin Source File

SOURCE=.\Gr2Viewer.h
# End Source File
# Begin Source File

SOURCE=.\GridFenster.h
# End Source File
# Begin Source File

SOURCE=.\GrRules2.h
# End Source File
# Begin Source File

SOURCE=.\InitialLoadDlg.h
# End Source File
# Begin Source File

SOURCE=.\LoginDlg.h
# End Source File
# Begin Source File

SOURCE=.\NatureAllocation.h
# End Source File
# Begin Source File

SOURCE=.\PositionMatrixDlg.h
# End Source File
# Begin Source File

SOURCE=.\PosRulesViewer.h
# End Source File
# Begin Source File

SOURCE=.\PremisPage.h
# End Source File
# Begin Source File

SOURCE=.\PrivList.h
# End Source File
# Begin Source File

SOURCE=.\ProxyDlg.h
# End Source File
# Begin Source File

SOURCE=.\PstRuleDlg.h
# End Source File
# Begin Source File

SOURCE=.\RegisterDlg.h
# End Source File
# Begin Source File

SOURCE=.\resource.h
# End Source File
# Begin Source File

SOURCE=.\Rules.h
# End Source File
# Begin Source File

SOURCE=.\hlp\Rules.hm
# End Source File
# Begin Source File

SOURCE=.\StdAfx.h
# End Source File
# End Group
# Begin Group "Resource Files"

# PROP Default_Filter "ico;cur;bmp;dlg;rc2;rct;bin;cnt;rtf;gif;jpg;jpeg;jpe"
# Begin Source File

SOURCE=.\res\about.bmp
# End Source File
# Begin Source File

SOURCE=.\hlp\AfxDlg.rtf
# End Source File
# Begin Source File

SOURCE=.\res\bmp00001.bmp
# End Source File
# Begin Source File

SOURCE=.\res\ccs.bmp
# End Source File
# Begin Source File

SOURCE=.\res\ico00001.ico
# End Source File
# Begin Source File

SOURCE=.\res\ico00002.ico
# End Source File
# Begin Source File

SOURCE=.\res\ico00003.ico
# End Source File
# Begin Source File

SOURCE=.\res\icon1.ico
# End Source File
# Begin Source File

SOURCE=.\res\icon2.ico
# End Source File
# Begin Source File

SOURCE=.\res\idr_main.ico
# End Source File
# Begin Source File

SOURCE=.\res\LOGIN.BMP
# End Source File
# Begin Source File

SOURCE=.\res\Logo_ccs.bmp
# End Source File
# Begin Source File

SOURCE=.\hlp\Rules.cnt
# End Source File
# Begin Source File

SOURCE=.\res\Rules.ico
# End Source File
# Begin Source File

SOURCE=.\res\Rules.rc2
# End Source File
# Begin Source File

SOURCE=.\res\TITLEBAR.bmp
# End Source File
# Begin Source File

SOURCE=.\res\ufis.ico
# End Source File
# Begin Source File

SOURCE=.\res\ufis43co.bmp
# End Source File
# Begin Source File

SOURCE=.\res\UFISBITMAP.bmp
# End Source File
# End Group
# Begin Source File

SOURCE=.\hlp\CCSPRJ.HLP
# End Source File
# Begin Source File

SOURCE=.\hlp\CCSPrj.LOG
# End Source File
# Begin Source File

SOURCE=.\ReadMe.txt
# End Source File
# Begin Source File

SOURCE=.\Rules.reg
# End Source File
# End Target
# End Project
