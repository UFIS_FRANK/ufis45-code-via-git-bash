// CedaAPTData.h

#ifndef __CEDAAPTDATA__
#define __CEDAAPTDATA__
 
#include "stdafx.h"
#include "basicdata.h"

//---------------------------------------------------------------------------------------------------------
// Record structure declaration

//@See: CedaAPTData
//@Doc:	A structure for reading Duty data. We read all data from database and put them
//		into this struct and store the data in omData.  

//---------------------------------------------------------------------------------------------------------
struct APTDATA 
{
	long 	 Urno;		// Eindeutige Datensatz-Nr.
	CTime	 Cdat;		// Erstellungsdatum
	char 	 Usec[34]; 	// Anwender (Ersteller)
	CTime	 Lstu;	 	// Datum letzte �nderung
	char 	 Prfl[3]; 	// Protokollierungskennzeichen
	char 	 Useu[34]; 	// Anwender (letzte �nderung)
	char 	 Apc3[5]; 	// Flughafen 3-Letter Code
	char 	 Apc4[6]; 	// Flughafen 4-Letter Code
	char 	 Apfn[34]; 	// Flughafen Name
	char 	 Land[4]; 	// Landeskennung
	char 	 Etof[6]; 	// Standardflugzeit
	CTime	 Tich;	 	// Uhrzeitwechsel in GMT
	char 	 Tdi1[6]; 	// Zeitdifferenz 1 bis TICH
	char 	 Tdi2[6]; 	// Zeitdifferenz 2 bis TICH
	char 	 Apsn[17]; 	// Flughafen Kurzname
	CTime	 Vafr;	 	// G�ltig von
	CTime	 Vato;	 	// G�ltig bis

	//DataCreated by this class
	int      IsChanged;

	APTDATA(void)
	{ memset(this,'\0',sizeof(*this));
	 Cdat=-1;Lstu=-1;Tich=-1;Vafr=-1;Vato=-1;
	}


}; // end APTDataStrukt


//---------------------------------------------------------------------------------------------------------
// Class declaratino

//@Memo: Read and write APT(duty) data
//@See: CedaData, CedaDutyData
/*@Doc:
  Reads and writes APT(duty) data from and to database. Stores APT data in memory and
  provides methods for searching and retrieving APTs from its internal list. Does
  some data preparations.

  Data of this class has to be updated by CEDA through broadcasts. This procedure and the
  necessary methods will be described later with the description of broadcast handling.

  {\bf CedaAPTData} handles data from the server process. 
  See the specification for table descriptions.*/

class CedaAPTData: public CCSCedaData
{
// Attributes
public:
    //@ManMemo: A map, containing the Urno field of all loaded APTs.
    CMapPtrToPtr omUrnoMap;
	CMapStringToPtr omApc3Map;
	CMapStringToPtr omApc4Map;

    //@ManMemo: APTDATA records read by ReadAllAPT().
    CCSPtrArray<APTDATA> omData;

	void SetTableName(CString opTableName)
	{
		strcpy(pcmTableName, opTableName.GetBuffer(0));
	}
// Operations
public:
    //@ManMemo: Default constructor
    /*@Doc:
      Initialize {\bf omRecInfo} for offsets and types for each fields in {\bf APTDATA},
      the {\bf pcmTableName} with table name {\bf APTLSG}, the data members {\bf pcmFieldList}
      contains a list of used fields of {\bf APTDATA}.
    */
    CedaAPTData();
	~CedaAPTData();
	void Register(void);
	void ClearAll(void);

    //@ManMemo: Read all APTs.
    /*@Doc:
      Read all APTs. 

      This method will clear {\bf omData} and {\bf omKeyMap}. Convert each record
      to a APTDATA and store it in {\bf omData} and the Urno field in {\bf omKeyMap}.
    */
    bool ReadAllAPTs();
    //@AreMemo: Read all APT-Data corresponding to Where-condition
	bool ReadSpecial(CCSPtrArray<APTDATA> &ropApt,char *pspWhere);
    //@ManMemo: Add a new APT and store the new data to the Database.
	bool InsertAPT(APTDATA *prpAPT,BOOL bpSendDdx = TRUE);
    //@ManMemo: Add data for a new APT to the internal data.
	bool InsertAPTInternal(APTDATA *prpAPT);
	//@ManMemo: Change the data of a special APT
	bool UpdateAPT(APTDATA *prpAPT,BOOL bpSendDdx = TRUE);
    //@AreMemo: Change the data of a special APT in the internal data.
	bool UpdateAPTInternal(APTDATA *prpAPT);
    //@ManMemo: Delete a special APT, also in the Database.
	bool DeleteAPT(long lpUrno);
    //@ManMemo: Delete a special APT only in the internal data.
	bool DeleteAPTInternal(APTDATA *prpAPT);
    //@ManMemo: Selects all APTs with a special Urno.
	APTDATA  *GetAPTByUrno(long lpUrno);
    //@AreMemo: Search a APT with a special Key.
	CString APTExists(APTDATA *prpAPT);
    //@AreMemo: Search a Apc3
	bool ExistApc3(char* popApc3);
	//@ManMemo: Writes a special APT to the Database.
	bool SaveAPT(APTDATA *prpAPT);
    //@ManMemo: Contains the valid data for all existing APTs.
	char pcmAPTFieldList[2048];
    //@ManMemo: Handle Broadcasts for APTs.
	void ProcessAPTBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);
	long GetUrnoByApc(char *pspApc);
	// Private methods
private:
    void PrepareAPTData(APTDATA *prpAPTData);
};


extern CedaAPTData ogAptData;
//---------------------------------------------------------------------------------------------------------

#endif //__CEDAAPTDATA__
