#if !defined(AFX_BACKGROUND_H__50697781_2045_11D1_82CE_0080AD1DC701__INCLUDED_)
#define AFX_BACKGROUND_H__50697781_2045_11D1_82CE_0080AD1DC701__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// BackGround.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CBackGround dialog

class CBackGround : public CDialog
{
// Construction
public:
	CBackGround(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CBackGround)
	enum { IDD = IDD_BACKGROUND };
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CBackGround)
	public:
	virtual BOOL Create(CWnd* pParentWnd, UINT nID);
	virtual BOOL OnInitDialog();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CBackGround)
    afx_msg void OnPaint();
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

protected:

};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_BACKGROUND_H__50697781_2045_11D1_82CE_0080AD1DC701__INCLUDED_)
