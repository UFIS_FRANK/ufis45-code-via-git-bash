#if !defined(AFX_GATRULEDLG_H__FB4A7141_F271_11D2_A1A5_0000B4984BBE__INCLUDED_)
#define AFX_GATRULEDLG_H__FB4A7141_F271_11D2_A1A5_0000B4984BBE__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// GatRuleDlg.h : header file
//

#include "CedaGatData.h"
/////////////////////////////////////////////////////////////////////////////
// GatRuleDlg dialog

class GatRuleDlg : public CDialog
{
// Construction
public:
	GatRuleDlg(CWnd* pParent, GATDATA *popGat);   // standard constructor

// Dialog Data
	//{{AFX_DATA(GatRuleDlg)
	enum { IDD = IDD_GAT_RULE_DLG };
	CEdit	m_PrioCtrl;
	CEdit	m_MultCtrl;
	CEdit	m_MaxPCtrl;
	CString	m_Flti;
	BOOL	m_Inbo;
	int		m_Maxp;
	int		m_Mult;
	CString	m_Natr;
	BOOL	m_Outb;
	CString	m_Pral;
	int		m_Prio;
	CString	m_PDest;
	//}}AFX_DATA

	GATDATA *pomGat;

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(GatRuleDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(GatRuleDlg)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_GATRULEDLG_H__FB4A7141_F271_11D2_A1A5_0000B4984BBE__INCLUDED_)
