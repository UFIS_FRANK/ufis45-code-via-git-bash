// UfisBroadcastsCtl.cpp : Implementation of the CUfisBroadcastsCtrl ActiveX Control class.

#include "stdafx.h"
#include "UfisBroadcasts.h"
#include "UfisBroadcastsCtl.h"
#include "UfisBroadcastsPpg.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#define MAXHOSTNAME 256

IMPLEMENT_DYNCREATE(CUfisBroadcastsCtrl, COleControl)


/////////////////////////////////////////////////////////////////////////////
// Message map

BEGIN_MESSAGE_MAP(CUfisBroadcastsCtrl, COleControl)
	//{{AFX_MSG_MAP(CUfisBroadcastsCtrl)
	// NOTE - ClassWizard will add and remove message map entries
	//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG_MAP
	ON_OLEVERB(AFX_IDS_VERB_EDIT, OnEdit)
	ON_OLEVERB(AFX_IDS_VERB_PROPERTIES, OnProperties)
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// Dispatch map

BEGIN_DISPATCH_MAP(CUfisBroadcastsCtrl, COleControl)
	//{{AFX_DISPATCH_MAP(CUfisBroadcastsCtrl)
	DISP_FUNCTION(CUfisBroadcastsCtrl, "InitComm", InitComm, VT_BOOL, VTS_NONE)
	//}}AFX_DISPATCH_MAP
	DISP_FUNCTION_ID(CUfisBroadcastsCtrl, "AboutBox", DISPID_ABOUTBOX, AboutBox, VT_EMPTY, VTS_NONE)
END_DISPATCH_MAP()


/////////////////////////////////////////////////////////////////////////////
// Event map

BEGIN_EVENT_MAP(CUfisBroadcastsCtrl, COleControl)
	//{{AFX_EVENT_MAP(CUfisBroadcastsCtrl)
	EVENT_CUSTOM("SendBroadcast", FireSendBroadcast, VTS_BSTR  VTS_BSTR  VTS_BSTR  VTS_BSTR  VTS_BSTR  VTS_BSTR  VTS_BSTR  VTS_BSTR  VTS_BSTR  VTS_BSTR)
	EVENT_CUSTOM("ReceiveBroadcast", FireReceiveBroadcast, VTS_BSTR  VTS_BSTR  VTS_BSTR  VTS_BSTR  VTS_BSTR  VTS_BSTR  VTS_BSTR  VTS_BSTR  VTS_BSTR  VTS_BSTR  VTS_BSTR)
	//}}AFX_EVENT_MAP
END_EVENT_MAP()


/////////////////////////////////////////////////////////////////////////////
// Property pages

// TODO: Add more property pages as needed.  Remember to increase the count!
BEGIN_PROPPAGEIDS(CUfisBroadcastsCtrl, 1)
	PROPPAGEID(CUfisBroadcastsPropPage::guid)
END_PROPPAGEIDS(CUfisBroadcastsCtrl)


/////////////////////////////////////////////////////////////////////////////
// Initialize class factory and guid

IMPLEMENT_OLECREATE_EX(CUfisBroadcastsCtrl, "UFISBROADCASTS.UfisBroadcastsCtrl.1",
	0x7da325c5, 0xd4a4, 0x11d3, 0x9b, 0x36, 0x2, 0x1, 0x28, 0xb0, 0xa0, 0xff)


/////////////////////////////////////////////////////////////////////////////
// Type library ID and version

IMPLEMENT_OLETYPELIB(CUfisBroadcastsCtrl, _tlid, _wVerMajor, _wVerMinor)


/////////////////////////////////////////////////////////////////////////////
// Interface IDs

const IID BASED_CODE IID_DUfisBroadcasts =
		{ 0x7da325c3, 0xd4a4, 0x11d3, { 0x9b, 0x36, 0x2, 0x1, 0x28, 0xb0, 0xa0, 0xff } };
const IID BASED_CODE IID_DUfisBroadcastsEvents =
		{ 0x7da325c4, 0xd4a4, 0x11d3, { 0x9b, 0x36, 0x2, 0x1, 0x28, 0xb0, 0xa0, 0xff } };


/////////////////////////////////////////////////////////////////////////////
// Control type information

static const DWORD BASED_CODE _dwUfisBroadcastsOleMisc =
	OLEMISC_INVISIBLEATRUNTIME |
	OLEMISC_ACTIVATEWHENVISIBLE |
	OLEMISC_SETCLIENTSITEFIRST |
	OLEMISC_INSIDEOUT |
	OLEMISC_CANTLINKINSIDE |
	OLEMISC_RECOMPOSEONRESIZE;

IMPLEMENT_OLECTLTYPE(CUfisBroadcastsCtrl, IDS_UFISBROADCASTS, _dwUfisBroadcastsOleMisc)


/////////////////////////////////////////////////////////////////////////////
// CUfisBroadcastsCtrl::CUfisBroadcastsCtrlFactory::UpdateRegistry -
// Adds or removes system registry entries for CUfisBroadcastsCtrl

BOOL CUfisBroadcastsCtrl::CUfisBroadcastsCtrlFactory::UpdateRegistry(BOOL bRegister)
{
	// TODO: Verify that your control follows apartment-model threading rules.
	// Refer to MFC TechNote 64 for more information.
	// If your control does not conform to the apartment-model rules, then
	// you must modify the code below, changing the 6th parameter from
	// afxRegInsertable | afxRegApartmentThreading to afxRegInsertable.

	if (bRegister)
		return AfxOleRegisterControlClass(
			AfxGetInstanceHandle(),
			m_clsid,
			m_lpszProgID,
			IDS_UFISBROADCASTS,
			IDB_UFISBROADCASTS,
			afxRegInsertable | afxRegApartmentThreading,
			_dwUfisBroadcastsOleMisc,
			_tlid,
			_wVerMajor,
			_wVerMinor);
	else
		return AfxOleUnregisterClass(m_clsid, m_lpszProgID);
}


/////////////////////////////////////////////////////////////////////////////
// CUfisBroadcastsCtrl::CUfisBroadcastsCtrl - Constructor

CUfisBroadcastsCtrl::CUfisBroadcastsCtrl()
{
	InitializeIIDs(&IID_DUfisBroadcasts, &IID_DUfisBroadcastsEvents);
	pomBcSocket = NULL;
	omPorts.Add(3352);
	omPorts.Add(3351);
	imActualPort = 0;
	// TODO: Initialize your control's instance data here.
}


void CUfisBroadcastsCtrl::Init()
{

}

/////////////////////////////////////////////////////////////////////////////
// CUfisBroadcastsCtrl::~CUfisBroadcastsCtrl - Destructor

CUfisBroadcastsCtrl::~CUfisBroadcastsCtrl()
{
	if(pomBcSocket != NULL)
	{
		pomBcSocket->Close();
		delete pomBcSocket;
	}
	// TODO: Cleanup your control's instance data here.
}


/////////////////////////////////////////////////////////////////////////////
// CUfisBroadcastsCtrl::OnDraw - Drawing function

void CUfisBroadcastsCtrl::OnDraw(
			CDC* pdc, const CRect& rcBounds, const CRect& rcInvalid)
{
	// TODO: Replace the following code with your own drawing code.
	//pdc->FillRect(rcBounds, CBrush::FromHandle((HBRUSH)GetStockObject(WHITE_BRUSH)));
	//pdc->Ellipse(rcBounds);
	CBrush olBlackBrush(COLORREF(RGB(0,0,0)));
	int ilH = rcBounds.bottom - rcBounds.top;
	CSize ilTH = pdc->GetTextExtent("BC-Comm");
	ilH = (int)(ilH/2);
	ilH -= (int)(ilTH.cy/2);
	pdc->FillRect(rcBounds, CBrush::FromHandle((HBRUSH)GetStockObject(LTGRAY_BRUSH)));
	pdc->FrameRect(rcBounds, &olBlackBrush);
	pdc->SetTextColor(COLORREF(RGB(255,128,0)));
	pdc->SetBkMode(TRANSPARENT);
	pdc->TextOut(0, ilH, "BC-Ufis");
}


/////////////////////////////////////////////////////////////////////////////
// CUfisBroadcastsCtrl::DoPropExchange - Persistence support

void CUfisBroadcastsCtrl::DoPropExchange(CPropExchange* pPX)
{
	ExchangeVersion(pPX, MAKELONG(_wVerMinor, _wVerMajor));
	COleControl::DoPropExchange(pPX);

	// TODO: Call PX_ functions for each persistent custom property.

}


/////////////////////////////////////////////////////////////////////////////
// CUfisBroadcastsCtrl::OnResetState - Reset control to default state

void CUfisBroadcastsCtrl::OnResetState()
{
	COleControl::OnResetState();  // Resets defaults found in DoPropExchange

	// TODO: Reset any other control state here.
}


/////////////////////////////////////////////////////////////////////////////
// CUfisBroadcastsCtrl::AboutBox - Display an "About" box to the user

void CUfisBroadcastsCtrl::AboutBox()
{
	CDialog dlgAbout(IDD_ABOUTBOX_UFISBROADCASTS);
	dlgAbout.DoModal();
}


/////////////////////////////////////////////////////////////////////////////
// CUfisBroadcastsCtrl message handlers

BOOL CUfisBroadcastsCtrl::InitComm() 
{
	CString olSockAdress;
	UINT iulPort;
	pomBcSocket = new BCSocket(this);
	pomBcSocket->GetSockName(olSockAdress, iulPort);
//	if(pomBcSocket->Create(3352, SOCK_DGRAM, olSockAdress) == FALSE)
	char pclSocketAdress[128];

	strcpy(pclSocketAdress,"192.9.200.150");
	//strcpy(pclSocketAdress,"182.9.200.157");



	char pclHost[MAXHOSTNAME+1];
	BOOL blRc;
	if (!  gethostname(pclHost,MAXHOSTNAME) )
	{
		struct  hostent *prlHost;
		prlHost = gethostbyname(pclHost);
		if (prlHost == NULL)
		{
			imLastError = WSAGetLastError ();
			CString olTmpCaption;
			olTmpCaption.Format("GetHost(By)Name failed <%s> <%d>",
				pclHost,imLastError);
			AfxMessageBox(olTmpCaption);
		}
		else
		{
			SOCKADDR_IN	dummy;


			for(int ilLc = 0; prlHost->h_addr_list[ilLc] != NULL; ilLc++)
			{
				memcpy((char FAR *)&dummy.sin_addr,
					prlHost->h_addr_list[ilLc],
					 prlHost->h_length);

				omSocketAdresses.Add(inet_ntoa(dummy.sin_addr));
			}

			if (omSocketAdresses.GetSize() > 0)
			{
				imActualSocketAdress = 0;
				blRc = pomBcSocket->Create(omPorts[imActualPort],SOCK_DGRAM, omSocketAdresses[imActualSocketAdress]);
//				olTmpCaption.Format("Adresse: <%s> Port: <%d>",omSocketAdresses[imActualSocketAdress],omPorts[imActualPort]); 
//				SetWindowText(olTmpCaption);

			}
		}
	}
	
	return TRUE;
}

void CUfisBroadcastsCtrl::ProcessPendingRead()
{
	if (pomBcSocket == NULL)
		return;

	char pclBuf[2048];
	CString olBcText;


	CString ol_bc_num,
			ol_dest_name,
			ol_orig_name,
			ol_recv_name,
			ol_command,
			ol_obj_name,
			ol_twe,
			ol_tws;
	char pclSelection[1024];
	char pclFields[1024];
	char pclData[1024];



	memset(pclBuf,0,sizeof(pclBuf));
	int ilBytesReceived = pomBcSocket->Receive(pclBuf,2000);


	COMMIF  *Packet;
	BC_HEAD *PbcHead;
	CMDBLK  *prlCmdBlk;
	char	*pclPtr;

	Packet = (COMMIF *) pclBuf;

	int ilCmd = abs(Packet->command);
	if (ilCmd > 0 && (ilCmd % 100 == 0))
	{
  		PbcHead = (BC_HEAD  *) Packet->data;                         
		prlCmdBlk = (CMDBLK *) PbcHead->data;

		bool blFilterPassed = false;

//		bool blHostFilter = ogHostString.Find(PbcHead->orig_name) > -1 || ogHostString.Find(";Alle;") > -1;
//		bool blCmdFilter = ogCmdString.Find(prlCmdBlk->command) > -1 || ogCmdString.Find(";Alle;") > -1;
//		bool blTableFilter = ogTableString.Find(prlCmdBlk->obj_name) > -1 || ogTableString.Find(";Alle;") > -1;

//		if (blHostFilter && blCmdFilter && blTableFilter)
		{

			pclPtr = prlCmdBlk->data;		// Begin of Selection
			strcpy(pclSelection,pclPtr);
			pclPtr += strlen(pclPtr) + 1;	// Begin of FieldList
			strcpy(pclFields,pclPtr);
			pclPtr += strlen(pclPtr) + 1;	// Begin of DataList
			strcpy(pclData,pclPtr);

			ol_bc_num.Format("%4d",PbcHead->bc_num);
			ol_dest_name = PbcHead->dest_name;
			ol_orig_name = PbcHead->orig_name;
			ol_recv_name = PbcHead->recv_name;
			ol_command = prlCmdBlk->command;
			ol_obj_name = prlCmdBlk->obj_name;
			ol_twe = prlCmdBlk->tw_end;
			ol_tws = prlCmdBlk->tw_start;
			/* OLD
			FireSendBroadcast(ol_bc_num,ol_dest_name,ol_recv_name,ol_command,
							  ol_obj_name,pclSelection,pclFields,pclData, ol_twe, ol_tws);
			*/
			FireReceiveBroadcast(ol_bc_num,ol_dest_name,ol_recv_name,ol_command,
							  ol_obj_name,pclSelection,pclFields,pclData, ol_twe, ol_tws, ol_orig_name );
			/*********************
			sprintf(pclOutBuf,"bc_num = %4d, Dest: <%s> Orig: <%s> Recv: <%s> Cmd: <%s> Table: <%s> Selection: %s Fields: %s Data: %s",
					PbcHead->bc_num,PbcHead->dest_name,PbcHead->orig_name,PbcHead->recv_name,
					prlCmdBlk->command,prlCmdBlk->obj_name,pclSelection,pclFields,pclData);
					********************/
		}
//	}
//	else
//	{
//		char pclTmp[512];
//		sprintf(pclTmp,"BC Unbekannt: %s",snap(pclBuf,40));
//		olBcText = pclTmp;
	}

}
