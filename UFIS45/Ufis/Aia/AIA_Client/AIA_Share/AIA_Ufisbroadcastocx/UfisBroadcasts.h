#if !defined(AFX_UFISBROADCASTS_H__7DA325CB_D4A4_11D3_9B36_020128B0A0FF__INCLUDED_)
#define AFX_UFISBROADCASTS_H__7DA325CB_D4A4_11D3_9B36_020128B0A0FF__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

// UfisBroadcasts.h : main header file for UFISBROADCASTS.DLL

#if !defined( __AFXCTL_H__ )
	#error include 'afxctl.h' before including this file
#endif

#include "resource.h"       // main symbols

/////////////////////////////////////////////////////////////////////////////
// CUfisBroadcastsApp : See UfisBroadcasts.cpp for implementation.

class CUfisBroadcastsApp : public COleControlModule
{
public:
	BOOL InitInstance();
	int ExitInstance();
};

extern const GUID CDECL _tlid;
extern const WORD _wVerMajor;
extern const WORD _wVerMinor;

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_UFISBROADCASTS_H__7DA325CB_D4A4_11D3_9B36_020128B0A0FF__INCLUDED)
