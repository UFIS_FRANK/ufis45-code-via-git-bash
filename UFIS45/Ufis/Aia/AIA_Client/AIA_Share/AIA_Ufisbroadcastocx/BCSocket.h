#if !defined(AFX_BCSOCKET_H__7DA325D8_D4A4_11D3_9B36_020128B0A0FF__INCLUDED_)
#define AFX_BCSOCKET_H__7DA325D8_D4A4_11D3_9B36_020128B0A0FF__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// BCSocket.h : header file
//

//#include "UfisBroadcastsCtl.h"

class CUfisBroadcastsCtrl;
/////////////////////////////////////////////////////////////////////////////
// BCSocket command target

class BCSocket : public CSocket
{
// Attributes
public:

// Operations
public:
	BCSocket(CUfisBroadcastsCtrl *popCtrl);
	CUfisBroadcastsCtrl *pomCtrl;
	virtual ~BCSocket();

// Overrides
public:
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(BCSocket)
	public:
	virtual void OnReceive(int nErrorCode);
	//}}AFX_VIRTUAL

	// Generated message map functions
	//{{AFX_MSG(BCSocket)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_MSG

// Implementation
protected:
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_BCSOCKET_H__7DA325D8_D4A4_11D3_9B36_020128B0A0FF__INCLUDED_)
