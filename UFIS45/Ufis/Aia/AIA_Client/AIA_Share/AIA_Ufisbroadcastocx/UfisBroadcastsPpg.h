#if !defined(AFX_UFISBROADCASTSPPG_H__7DA325D5_D4A4_11D3_9B36_020128B0A0FF__INCLUDED_)
#define AFX_UFISBROADCASTSPPG_H__7DA325D5_D4A4_11D3_9B36_020128B0A0FF__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

// UfisBroadcastsPpg.h : Declaration of the CUfisBroadcastsPropPage property page class.

////////////////////////////////////////////////////////////////////////////
// CUfisBroadcastsPropPage : See UfisBroadcastsPpg.cpp.cpp for implementation.

class CUfisBroadcastsPropPage : public COlePropertyPage
{
	DECLARE_DYNCREATE(CUfisBroadcastsPropPage)
	DECLARE_OLECREATE_EX(CUfisBroadcastsPropPage)

// Constructor
public:
	CUfisBroadcastsPropPage();

// Dialog Data
	//{{AFX_DATA(CUfisBroadcastsPropPage)
	enum { IDD = IDD_PROPPAGE_UFISBROADCASTS };
		// NOTE - ClassWizard will add data members here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_DATA

// Implementation
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Message maps
protected:
	//{{AFX_MSG(CUfisBroadcastsPropPage)
		// NOTE - ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_UFISBROADCASTSPPG_H__7DA325D5_D4A4_11D3_9B36_020128B0A0FF__INCLUDED)
