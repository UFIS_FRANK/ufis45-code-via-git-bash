#if !defined(AFX_UFISBROADCASTSCTL_H__7DA325D3_D4A4_11D3_9B36_020128B0A0FF__INCLUDED_)
#define AFX_UFISBROADCASTSCTL_H__7DA325D3_D4A4_11D3_9B36_020128B0A0FF__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

// UfisBroadcastsCtl.h : Declaration of the CUfisBroadcastsCtrl ActiveX Control class.

#include "BCSocket.h"
/////////////////////////////////////////////////////////////////////////////
// CUfisBroadcastsCtrl : See UfisBroadcastsCtl.cpp for implementation.

typedef struct {
	char		dest_name[10];
	char		orig_name[10];
	char		recv_name[10];
	short		bc_num;
	char		seq_id[10];
	short		tot_buf;
	short		act_buf;
	char		ref_seq_id[10];
	short		rc;
	short		tot_size;
	short		cmd_size;
	short		data_size;
	char		data[1];
}BC_HEAD;

/* 
 *
 * Following the Command Block  CMDBLK
 *
 */
typedef struct {
	char		command[6];	
	char		obj_name[33];
	char		order[2];
	char		tw_start[33];
	char		tw_end[33];
	char		data[1];
}CMDBLK;
          
typedef	struct {
		short	command;
		long	length;
		char	data[1];
}COMMIF;






class CUfisBroadcastsCtrl : public COleControl
{
	DECLARE_DYNCREATE(CUfisBroadcastsCtrl)

// Constructor
public:
	CUfisBroadcastsCtrl();

	BCSocket *pomBcSocket;
	CStringArray omSocketAdresses;
	HICON m_hIcon;
	int imLastError;
	int imActualSocketAdress;
	int imActualPort;
	CWordArray omPorts;
	void ProcessPendingRead();

	void Init();
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CUfisBroadcastsCtrl)
	public:
	virtual void OnDraw(CDC* pdc, const CRect& rcBounds, const CRect& rcInvalid);
	virtual void DoPropExchange(CPropExchange* pPX);
	virtual void OnResetState();
	//}}AFX_VIRTUAL

// Implementation
protected:
	~CUfisBroadcastsCtrl();

	DECLARE_OLECREATE_EX(CUfisBroadcastsCtrl)    // Class factory and guid
	DECLARE_OLETYPELIB(CUfisBroadcastsCtrl)      // GetTypeInfo
	DECLARE_PROPPAGEIDS(CUfisBroadcastsCtrl)     // Property page IDs
	DECLARE_OLECTLTYPE(CUfisBroadcastsCtrl)		// Type name and misc status

// Message maps
	//{{AFX_MSG(CUfisBroadcastsCtrl)
		// NOTE - ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

// Dispatch maps
	//{{AFX_DISPATCH(CUfisBroadcastsCtrl)
	afx_msg BOOL InitComm();
	//}}AFX_DISPATCH
	DECLARE_DISPATCH_MAP()

	afx_msg void AboutBox();

// Event maps
	//{{AFX_EVENT(CUfisBroadcastsCtrl)
	void FireSendBroadcast(LPCTSTR BcNum, LPCTSTR DestName, LPCTSTR RecvName, LPCTSTR Command, LPCTSTR ObjName, LPCTSTR Selection, LPCTSTR Fields, LPCTSTR Data, LPCTSTR twe, LPCTSTR tws)
		{FireEvent(eventidSendBroadcast,EVENT_PARAM(VTS_BSTR  VTS_BSTR  VTS_BSTR  VTS_BSTR  VTS_BSTR  VTS_BSTR  VTS_BSTR  VTS_BSTR  VTS_BSTR  VTS_BSTR), BcNum, DestName, RecvName, Command, ObjName, Selection, Fields, Data, twe, tws);}
	void FireReceiveBroadcast(LPCTSTR BcNum, LPCTSTR DestName, LPCTSTR RecvName, LPCTSTR Command, LPCTSTR ObjName, LPCTSTR Selection, LPCTSTR Fields, LPCTSTR Data, LPCTSTR twe, LPCTSTR tws, LPCTSTR OrigName)
		{FireEvent(eventidReceiveBroadcast,EVENT_PARAM(VTS_BSTR  VTS_BSTR  VTS_BSTR  VTS_BSTR  VTS_BSTR  VTS_BSTR  VTS_BSTR  VTS_BSTR  VTS_BSTR  VTS_BSTR  VTS_BSTR), BcNum, DestName, RecvName, Command, ObjName, Selection, Fields, Data, twe, tws, OrigName);}
	//}}AFX_EVENT
	DECLARE_EVENT_MAP()

// Dispatch and event IDs
public:
	enum {
	//{{AFX_DISP_ID(CUfisBroadcastsCtrl)
	dispidInitComm = 1L,
	eventidSendBroadcast = 1L,
	eventidReceiveBroadcast = 2L,
	//}}AFX_DISP_ID
	};
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_UFISBROADCASTSCTL_H__7DA325D3_D4A4_11D3_9B36_020128B0A0FF__INCLUDED)
