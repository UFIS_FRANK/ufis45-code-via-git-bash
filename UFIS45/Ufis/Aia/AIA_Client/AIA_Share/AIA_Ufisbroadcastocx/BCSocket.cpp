// BCSocket.cpp : implementation file
//

#include "stdafx.h"
#include "UfisBroadcasts.h"
#include "BCSocket.h"
#include "UfisBroadcastsCtl.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// BCSocket

BCSocket::BCSocket(CUfisBroadcastsCtrl *popCtrl)
{
	pomCtrl = popCtrl;
}

BCSocket::~BCSocket()
{
}


// Do not edit the following lines, which are needed by ClassWizard.
#if 0
BEGIN_MESSAGE_MAP(BCSocket, CSocket)
	//{{AFX_MSG_MAP(BCSocket)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()
#endif	// 0

/////////////////////////////////////////////////////////////////////////////
// BCSocket member functions


void BCSocket::OnReceive(int nErrorCode) 
{
	CSocket::OnReceive(nErrorCode);
	pomCtrl->ProcessPendingRead();
}
