// UfisBroadcastsPpg.cpp : Implementation of the CUfisBroadcastsPropPage property page class.

#include "stdafx.h"
#include "UfisBroadcasts.h"
#include "UfisBroadcastsPpg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


IMPLEMENT_DYNCREATE(CUfisBroadcastsPropPage, COlePropertyPage)


/////////////////////////////////////////////////////////////////////////////
// Message map

BEGIN_MESSAGE_MAP(CUfisBroadcastsPropPage, COlePropertyPage)
	//{{AFX_MSG_MAP(CUfisBroadcastsPropPage)
	// NOTE - ClassWizard will add and remove message map entries
	//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// Initialize class factory and guid

IMPLEMENT_OLECREATE_EX(CUfisBroadcastsPropPage, "UFISBROADCASTS.UfisBroadcastsPropPage.1",
	0x7da325c6, 0xd4a4, 0x11d3, 0x9b, 0x36, 0x2, 0x1, 0x28, 0xb0, 0xa0, 0xff)


/////////////////////////////////////////////////////////////////////////////
// CUfisBroadcastsPropPage::CUfisBroadcastsPropPageFactory::UpdateRegistry -
// Adds or removes system registry entries for CUfisBroadcastsPropPage

BOOL CUfisBroadcastsPropPage::CUfisBroadcastsPropPageFactory::UpdateRegistry(BOOL bRegister)
{
	if (bRegister)
		return AfxOleRegisterPropertyPageClass(AfxGetInstanceHandle(),
			m_clsid, IDS_UFISBROADCASTS_PPG);
	else
		return AfxOleUnregisterClass(m_clsid, NULL);
}


/////////////////////////////////////////////////////////////////////////////
// CUfisBroadcastsPropPage::CUfisBroadcastsPropPage - Constructor

CUfisBroadcastsPropPage::CUfisBroadcastsPropPage() :
	COlePropertyPage(IDD, IDS_UFISBROADCASTS_PPG_CAPTION)
{
	//{{AFX_DATA_INIT(CUfisBroadcastsPropPage)
	// NOTE: ClassWizard will add member initialization here
	//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_DATA_INIT
}


/////////////////////////////////////////////////////////////////////////////
// CUfisBroadcastsPropPage::DoDataExchange - Moves data between page and properties

void CUfisBroadcastsPropPage::DoDataExchange(CDataExchange* pDX)
{
	//{{AFX_DATA_MAP(CUfisBroadcastsPropPage)
	// NOTE: ClassWizard will add DDP, DDX, and DDV calls here
	//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_DATA_MAP
	DDP_PostProcessing(pDX);
}


/////////////////////////////////////////////////////////////////////////////
// CUfisBroadcastsPropPage message handlers
