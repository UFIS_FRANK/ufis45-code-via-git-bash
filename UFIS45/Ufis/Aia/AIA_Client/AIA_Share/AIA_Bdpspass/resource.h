//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by BDPSPASS.rc
//
#define IDB_LOGIN                       10
#define IDM_ABOUTBOX                    0x0010
#define IDI_UFIS                        58
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_BDPSPASS_DIALOG             102
#define IDS_INVALIDUSERNAME             102
#define IDS_ERRORCAPTION                103
#define IDS_DIFFERENTPASSWORDS          104
#define IDS_INVALIDPASSWORD             105
#define IDS_USERNAME_NOTFOUND           106
#define IDS_INVALIDOLDPASSWORD          107
#define IDS_DBERROR                     108
#define IDR_MAINFRAME                   128
#define IDC_USIDHEAD                    1000
#define IDC_OLDPASSHEAD                 1001
#define IDC_NEWPASS1HEAD                1002
#define IDC_NEWPASS2HEAD                1003
#define IDC_USID                        1004
#define IDC_OLDPASS                     1005
#define IDC_NEWPASS1                    1006
#define IDC_NEWPASS2                    1007

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        129
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1005
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
