#ifndef __CEDASETPWD__
#define __CEDASETPWD__

#include "CCSCedaData.h"


class CedaSetPwd: public CCSCedaData
{
private:

public:

	//Last error message returned from Login().
	CString omErrorMessage;

	//Constructor.
	CedaSetPwd();

	//Destructor.
	~CedaSetPwd();

	bool Update(const char *pcpHomeAirport, const char *pcpUsid, const char *pcpOldPass, const char *pcpNewPass);

};

extern CedaSetPwd ogCedaSetPwd;

#endif //__CEDASETPWD__
