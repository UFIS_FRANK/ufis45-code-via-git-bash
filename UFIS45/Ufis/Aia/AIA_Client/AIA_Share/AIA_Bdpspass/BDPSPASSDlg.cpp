// BDPSPASSDlg.cpp : implementation file
//

#include "stdafx.h"
#include "BDPSPASS.h"
#include "BDPSPASSDlg.h"
#include "CCSGlobl.h"
#include "CCSEdit.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CAboutDlg dialog used for App About

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Dialog Data
	//{{AFX_DATA(CAboutDlg)
	enum { IDD = IDD_ABOUTBOX };
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAboutDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	//{{AFX_MSG(CAboutDlg)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
	//{{AFX_DATA_INIT(CAboutDlg)
	//}}AFX_DATA_INIT
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAboutDlg)
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
	//{{AFX_MSG_MAP(CAboutDlg)
		// No message handlers
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CBDPSPASSDlg dialog

CBDPSPASSDlg::CBDPSPASSDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CBDPSPASSDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CBDPSPASSDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	// Note that LoadIcon does not require a subsequent DestroyIcon in Win32
	m_hIcon = AfxGetApp()->LoadIcon(IDI_UFIS);
}

void CBDPSPASSDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CBDPSPASSDlg)
	DDX_Control(pDX, IDC_NEWPASS1, m_NewPass1);
	DDX_Control(pDX, IDC_NEWPASS2, m_NewPass2);
	DDX_Control(pDX, IDC_OLDPASS, m_OldPass);
	DDX_Control(pDX, IDC_USID, m_Usid);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CBDPSPASSDlg, CDialog)
	//{{AFX_MSG_MAP(CBDPSPASSDlg)
	ON_WM_SYSCOMMAND()
	ON_WM_DESTROY()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CBDPSPASSDlg message handlers

BOOL CBDPSPASSDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	m_Usid.SetTypeToString("X(32)",32,1);
	m_Usid.SetBKColor(YELLOW);
	m_Usid.SetTextErrColor(RED);
	m_Usid.SetFocus();

	m_OldPass.SetTypeToString("X(32)",32,0);
	m_OldPass.SetBKColor(YELLOW);
	m_OldPass.SetTextErrColor(RED);

	m_NewPass1.SetTypeToString("X(32)",32,3);
	m_NewPass1.SetBKColor(YELLOW);
	m_NewPass1.SetTextErrColor(RED);

	m_NewPass2.SetTypeToString("X(32)",32,3);
	m_NewPass2.SetBKColor(YELLOW);
	m_NewPass2.SetTextErrColor(RED);


	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon
	
	// TODO: Add extra initialization here
	
	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CBDPSPASSDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

void CBDPSPASSDlg::OnDestroy()
{
	WinHelp(0L, HELP_QUIT);
	CDialog::OnDestroy();
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CBDPSPASSDlg::OnPaint() 
{
    CPaintDC dc(this); // device context for painting
    CDC olDC;
    olDC.CreateCompatibleDC( &dc );
    CBitmap olBitmap;
    olBitmap.LoadBitmap( IDB_LOGIN );
    olDC.SelectObject( &olBitmap );
    dc.BitBlt( 0, 190, 459, 52, &olDC, 0, 0, SRCCOPY );
    olDC.DeleteDC( );

	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, (WPARAM) dc.GetSafeHdc(), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// The system calls this to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CBDPSPASSDlg::OnQueryDragIcon()
{
	return (HCURSOR) m_hIcon;
}

void CBDPSPASSDlg::OnOK() 
{
	bool blRc = true;

	CString olUsid,olOldPass,olNewPass1,olNewPass2;

	m_Usid.GetWindowText(olUsid);
	m_OldPass.GetWindowText(olOldPass);
	m_NewPass1.GetWindowText(olNewPass1);
	m_NewPass2.GetWindowText(olNewPass2);

	if(m_Usid.GetStatus() == false)
	{
		MessageBox(GetString(IDS_INVALIDUSERNAME),GetString(IDS_ERRORCAPTION),MB_ICONINFORMATION);
		m_Usid.SetFocus();
		blRc = false;
	}

	if( blRc && olNewPass1 != olNewPass2 )
	{
		MessageBox(GetString(IDS_DIFFERENTPASSWORDS),GetString(IDS_ERRORCAPTION),MB_ICONINFORMATION);
		m_NewPass1.SetFocus();
		blRc = false;
	}
	if(blRc)
	{
		int ilNewPassLen = olNewPass1.GetLength();
		if(ilNewPassLen < 3 || ilNewPassLen > 32 || olNewPass1[0] == ' ')
		{
			MessageBox(GetString(IDS_INVALIDPASSWORD),GetString(IDS_ERRORCAPTION),MB_ICONINFORMATION);
			m_NewPass1.SetFocus();
			blRc = false;
		}
	}
	if(blRc)
	{
		ogCommHandler.SetUser(olUsid);
		strcpy(CCSCedaData::pcmUser, olUsid);
		strcpy(CCSCedaData::pcmReqId, ogCommHandler.pcmReqId);

		ogCedaSetPwd.ReadUfisCedaConfig();
		
		blRc = ogCedaSetPwd.Update(pcgHome,olUsid,olOldPass,olNewPass1);

		if( !blRc )
		{
			if( strstr(ogCedaSetPwd.omErrorMessage,"INVALID_USER") != NULL )
			{
				MessageBox(GetString(IDS_USERNAME_NOTFOUND),GetString(IDS_ERRORCAPTION),MB_ICONINFORMATION);
				m_Usid.SetFocus();
			}
			else if( strstr(ogCedaSetPwd.omErrorMessage,"INVALID_PASSWORD") != NULL )
			{
				MessageBox(GetString(IDS_INVALIDOLDPASSWORD),GetString(IDS_ERRORCAPTION),MB_ICONINFORMATION);
				m_OldPass.SetFocus();
			}
			else
			{
				// database error so force exit
				blRc = true;
				MessageBox(GetString(IDS_DBERROR),GetString(IDS_ERRORCAPTION),MB_ICONINFORMATION);
			}
		}
	}

	if(blRc)
		CDialog::OnOK();
}

