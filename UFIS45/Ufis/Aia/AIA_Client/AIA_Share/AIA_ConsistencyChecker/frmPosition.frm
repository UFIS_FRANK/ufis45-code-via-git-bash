VERSION 5.00
Begin VB.Form frmPosition 
   Caption         =   "Edit rule for position <x> ..."
   ClientHeight    =   7200
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   8865
   Icon            =   "frmPosition.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   ScaleHeight     =   7200
   ScaleWidth      =   8865
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton Command2 
      Caption         =   "&Cancel"
      Height          =   375
      Left            =   4485
      TabIndex        =   43
      Top             =   6660
      Width           =   1215
   End
   Begin VB.CommandButton cmdOk 
      Caption         =   "OK"
      Default         =   -1  'True
      Height          =   375
      Left            =   3180
      TabIndex        =   42
      Top             =   6660
      Width           =   1215
   End
   Begin VB.TextBox txtValue 
      Height          =   315
      Index           =   14
      Left            =   2100
      TabIndex        =   31
      Top             =   4800
      Width           =   4995
   End
   Begin VB.TextBox txtValue 
      Height          =   315
      Index           =   13
      Left            =   2100
      TabIndex        =   30
      Top             =   4380
      Width           =   4995
   End
   Begin VB.TextBox txtValue 
      Height          =   315
      Index           =   12
      Left            =   2100
      TabIndex        =   29
      Top             =   3960
      Width           =   4995
   End
   Begin VB.TextBox txtValue 
      Height          =   315
      Index           =   11
      Left            =   2100
      TabIndex        =   28
      Top             =   3060
      Width           =   915
   End
   Begin VB.TextBox txtValue 
      Height          =   315
      Index           =   10
      Left            =   2100
      TabIndex        =   27
      Top             =   2640
      Width           =   915
   End
   Begin VB.TextBox txtValue 
      Height          =   315
      Index           =   9
      Left            =   6120
      TabIndex        =   22
      Top             =   2220
      Width           =   915
   End
   Begin VB.TextBox txtValue 
      Height          =   315
      Index           =   8
      Left            =   2100
      TabIndex        =   21
      Top             =   2220
      Width           =   915
   End
   Begin VB.TextBox txtValue 
      Height          =   315
      Index           =   7
      Left            =   6120
      TabIndex        =   20
      Top             =   1800
      Width           =   915
   End
   Begin VB.TextBox txtValue 
      Height          =   315
      Index           =   6
      Left            =   2100
      TabIndex        =   19
      Top             =   1800
      Width           =   915
   End
   Begin VB.TextBox txtValue 
      Height          =   315
      Index           =   5
      Left            =   4140
      TabIndex        =   11
      Top             =   1020
      Width           =   915
   End
   Begin VB.TextBox txtValue 
      Height          =   315
      Index           =   4
      Left            =   2100
      TabIndex        =   10
      Top             =   1020
      Width           =   915
   End
   Begin VB.TextBox txtValue 
      Height          =   315
      Index           =   3
      Left            =   4140
      TabIndex        =   6
      Top             =   600
      Width           =   915
   End
   Begin VB.TextBox txtValue 
      Height          =   315
      Index           =   2
      Left            =   2100
      TabIndex        =   5
      Top             =   600
      Width           =   915
   End
   Begin VB.TextBox txtValue 
      Height          =   315
      Index           =   1
      Left            =   4140
      TabIndex        =   3
      Top             =   180
      Width           =   915
   End
   Begin VB.TextBox txtValue 
      Height          =   315
      Index           =   0
      Left            =   2100
      TabIndex        =   1
      Top             =   180
      Width           =   915
   End
   Begin VB.Label Label27 
      Caption         =   "without prefix: preferred use this value"
      Height          =   195
      Left            =   2100
      TabIndex        =   41
      Top             =   6120
      Width           =   3255
   End
   Begin VB.Label Label26 
      Caption         =   "= Exclusively used by this value"
      Height          =   195
      Left            =   2100
      TabIndex        =   40
      Top             =   5820
      Width           =   3255
   End
   Begin VB.Label Label25 
      Caption         =   "! Exclude this value"
      Height          =   195
      Left            =   2100
      TabIndex        =   39
      Top             =   5520
      Width           =   3255
   End
   Begin VB.Label Label24 
      Caption         =   "Format description:"
      Height          =   195
      Left            =   240
      TabIndex        =   38
      Top             =   5520
      Width           =   1635
   End
   Begin VB.Label Label23 
      Caption         =   "e.g.: !J C"
      Height          =   195
      Left            =   7140
      TabIndex        =   37
      Top             =   4860
      Width           =   1455
   End
   Begin VB.Label Label22 
      Caption         =   "e.g.: !B747 A321"
      Height          =   195
      Left            =   7140
      TabIndex        =   36
      Top             =   4440
      Width           =   1455
   End
   Begin VB.Label Label21 
      Caption         =   "e.g.: !DLH !US"
      Height          =   195
      Left            =   7140
      TabIndex        =   35
      Top             =   4020
      Width           =   1455
   End
   Begin VB.Label Label20 
      Caption         =   "Nature code parameter:"
      Height          =   195
      Left            =   240
      TabIndex        =   34
      Top             =   4860
      Width           =   1695
   End
   Begin VB.Label Label19 
      Caption         =   "A/C type parameter:"
      Height          =   195
      Left            =   240
      TabIndex        =   33
      Top             =   4440
      Width           =   1695
   End
   Begin VB.Label Label18 
      Caption         =   "A/L Parameter:"
      Height          =   195
      Left            =   240
      TabIndex        =   32
      Top             =   4020
      Width           =   1695
   End
   Begin VB.Line Line1 
      BorderColor     =   &H00000000&
      BorderWidth     =   2
      X1              =   120
      X2              =   8580
      Y1              =   3660
      Y2              =   3660
   End
   Begin VB.Label Label17 
      Caption         =   "m (wingspan)"
      Height          =   195
      Left            =   7080
      TabIndex        =   26
      Top             =   2280
      Width           =   1215
   End
   Begin VB.Label Label16 
      Caption         =   "m (wingspan)"
      Height          =   195
      Left            =   7080
      TabIndex        =   25
      Top             =   1860
      Width           =   1215
   End
   Begin VB.Label Label15 
      Caption         =   "Restricted to (1):"
      Height          =   195
      Left            =   4740
      TabIndex        =   24
      Top             =   2280
      Width           =   1215
   End
   Begin VB.Label Label14 
      Caption         =   "Restricted to (1):"
      Height          =   195
      Left            =   4740
      TabIndex        =   23
      Top             =   1860
      Width           =   1215
   End
   Begin VB.Label Label13 
      Caption         =   "Priority:"
      Height          =   195
      Left            =   240
      TabIndex        =   18
      Top             =   3120
      Width           =   975
   End
   Begin VB.Label Label12 
      Caption         =   "May A/C No.:"
      Height          =   195
      Left            =   240
      TabIndex        =   17
      Top             =   2700
      Width           =   1275
   End
   Begin VB.Label Label11 
      Caption         =   "Next Position(2):"
      Height          =   195
      Left            =   240
      TabIndex        =   16
      Top             =   2280
      Width           =   1215
   End
   Begin VB.Label Label10 
      Caption         =   "Next Position(1):"
      Height          =   195
      Left            =   240
      TabIndex        =   15
      Top             =   1860
      Width           =   1215
   End
   Begin VB.Label Label9 
      Caption         =   "m (max)"
      Height          =   195
      Left            =   5100
      TabIndex        =   14
      Top             =   1080
      Width           =   615
   End
   Begin VB.Label Label8 
      Caption         =   "m (min)"
      Height          =   195
      Left            =   3060
      TabIndex        =   13
      Top             =   1080
      Width           =   615
   End
   Begin VB.Label Label7 
      Caption         =   "Length:"
      Height          =   195
      Left            =   240
      TabIndex        =   12
      Top             =   1080
      Width           =   975
   End
   Begin VB.Label Label6 
      Caption         =   "m (max)"
      Height          =   195
      Left            =   5100
      TabIndex        =   9
      Top             =   660
      Width           =   615
   End
   Begin VB.Label Label5 
      Caption         =   "m (min)"
      Height          =   195
      Left            =   3060
      TabIndex        =   8
      Top             =   660
      Width           =   615
   End
   Begin VB.Label Label4 
      Caption         =   "Door Height:"
      Height          =   195
      Left            =   240
      TabIndex        =   7
      Top             =   660
      Width           =   975
   End
   Begin VB.Label Label3 
      Caption         =   "m (max)"
      Height          =   195
      Left            =   5100
      TabIndex        =   4
      Top             =   240
      Width           =   615
   End
   Begin VB.Label Label2 
      Caption         =   "m (min)"
      Height          =   195
      Left            =   3060
      TabIndex        =   2
      Top             =   240
      Width           =   615
   End
   Begin VB.Label Label1 
      Caption         =   "Wing Span:"
      Height          =   195
      Left            =   240
      TabIndex        =   0
      Top             =   240
      Width           =   975
   End
End
Attribute VB_Name = "frmPosition"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public Fieldlist As String
Public Datalist As String
Public Fieldlens As String
Public Typelist As String
Public Tabindex As Long
Public CaptionPart As String
Public WhichRules As String


Private Sub cmdOk_Click()
    Dim strResult As String
    Dim strValue As String
    Dim cnt As Integer
    Dim strErr As String
    Dim strVbCrLf As String
    Dim strLineNo As String
    Dim llLineNo As Long
    Dim strUrno As String
'=======================================
' 1. Validity check of the single fields
    strResult = strResult + CheckAirlinesList(MakeItemList(txtValue(12))) + vbCrLf
    strResult = strResult + CheckAircraftList(MakeItemList(txtValue(13))) + vbCrLf
    strResult = strResult + CheckNatureList(MakeItemList(txtValue(14))) + vbCrLf
    
    strResult = strResult + CheckPositionList(txtValue(6)) + vbCrLf
    strResult = strResult + CheckPositionList(txtValue(8)) + vbCrLf
    strVbCrLf = vbCrLf + vbCrLf + vbCrLf + vbCrLf + vbCrLf
    
    If strResult <> strVbCrLf Then
        strErr = vbCrLf + "Following irregularities occured:" + vbCrLf
        strErr = strErr + "========================" + vbCrLf + strResult
        MsgBox strErr
        Exit Sub
    End If
'===========================================================
' 2. Collection of data and update in Position Rules section
' --------
'  0  1  2    3      4       5       6         7            8   9   10 11 12 13 14    15
'URNO,#,Pos,Gates,Wingspan,Length,Height,A/C Parameterlist,Pos,Max,Pos,Max,M,P,Nat.,A/L Parameter
strUrno = GetRealItem(Datalist, 0, ",")
strLineNo = frmMain.tabPositions.GetLinesByColumnValue(0, strUrno, 1)
llLineNo = CLng(strLineNo)
strValue = txtValue(0) + "-" + txtValue(1)
frmMain.tabPositions.SetColumnValue llLineNo, 4, strValue
strValue = txtValue(2) + "-" + txtValue(3)
frmMain.tabPositions.SetColumnValue llLineNo, 6, strValue
strValue = txtValue(4) + "-" + txtValue(5)
frmMain.tabPositions.SetColumnValue llLineNo, 5, strValue
frmMain.tabPositions.SetColumnValue llLineNo, 8, txtValue(6)
frmMain.tabPositions.SetColumnValue llLineNo, 9, txtValue(7)
frmMain.tabPositions.SetColumnValue llLineNo, 10, txtValue(8)
frmMain.tabPositions.SetColumnValue llLineNo, 11, txtValue(9)
frmMain.tabPositions.SetColumnValue llLineNo, 12, txtValue(10)
frmMain.tabPositions.SetColumnValue llLineNo, 13, txtValue(11)
frmMain.tabPositions.SetColumnValue llLineNo, 15, txtValue(12)
frmMain.tabPositions.SetColumnValue llLineNo, 7, txtValue(13)
frmMain.tabPositions.SetColumnValue llLineNo, 14, txtValue(14)
frmMain.tabPositions.RedrawTab
'==============================================================
' 3. Collection of data to update the data gird and stor in DB
'  0   1    2    3    4    5    6     7    8    9   10   11   12   13   14   15   16   17   18   19   20   21
'Urno,Acus,Cdat,Fuls,Gpus,Lstu,Nafr,Nato,Pcas,Pnam,Prfl,Pubk,Taxi,Tele,Usec,Useu,Vafr,Vato,Resn,Dgss,Acgr,Posr
'Span,Len,Height,Aircrafts,Nextpos,Restricted to,Nextpos,Restricted to,MaxAC,Prio,Natures,Airlines
strLineNo = frmHiddenData.tabData(3).GetLinesByColumnValue(0, GetRealItem(Datalist, 0, ","), 1)
llLineNo = CLng(strLineNo)
strValue = strValue + txtValue(0) + "-" + txtValue(1) + ";"
strValue = strValue + txtValue(4) + "-" + txtValue(5) + ";"
strValue = strValue + txtValue(2) + "-" + txtValue(3) + ";"
strValue = strValue + txtValue(13) + ";"

strValue = strValue + txtValue(6) + ";"
strValue = strValue + txtValue(7) + ";"
strValue = strValue + txtValue(8) + ";"
strValue = strValue + txtValue(9) + ";"
strValue = strValue + txtValue(10) + ";"
strValue = strValue + txtValue(11) + ";"
strValue = strValue + txtValue(14) + ";"
strValue = strValue + txtValue(12) + ";"
frmHiddenData.tabData(3).SetColumnValue llLineNo, 21, strValue
frmHiddenData.tabData(3).RedrawTab
frmHiddenData.Ufis.CallServer "URT", "PSTTAB", "POSR", strValue, "WHERE URNO=" + strUrno, "120"

Unload Me
End Sub

Private Sub Command2_Click()
    Unload Me
End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
    If KeyAscii = vbKeyEscape Then
        Unload Me
    End If
End Sub

Private Sub Form_Load()
    PrepareValues
End Sub
'================================================
' Lopps through the field and datalist and
' sets the values into the edit fields
'================================================
Public Sub PrepareValues()
    Dim ilFieldCount As Integer
    Dim strCurrValue As String
    Dim idx As Integer
    Dim i As Integer
    If Fieldlist = "" Then
        Exit Sub
    End If
    ilFieldCount = ItemCount(Fieldlist, ",")
    For i = 0 To ilFieldCount - 2
        txtValue(i) = GetRealItem(Datalist, i + 1, ",") ' i+1 because urno does not interest
    Next i
    Me.Caption = "Edit rule for position " + CaptionPart
End Sub


Private Sub txtValue_GotFocus(Index As Integer)
    txtValue(Index).selstart = 0
    txtValue(Index).SelLength = Len(txtValue(Index).Text)
End Sub

Private Sub txtValue_Change(Index As Integer)
    Dim Fieldlen As Integer
    Dim strVal As String

    Fieldlen = CInt(GetRealItem(Fieldlens, Index + 1, ","))
    strVal = txtValue(Index).Text
    If Len(strVal) >= Fieldlen Then
        txtValue(Index).Text = left(txtValue(Index).Text, Fieldlen)
        txtValue(Index).selstart = Fieldlen
    End If
        
End Sub
Private Sub txtValue_KeyPress(Index As Integer, KeyAscii As Integer)
    Dim Fieldlen As Integer
    Dim strVal As String
    Dim FieldType As String
    Dim sellen As Integer
    Dim selstart As Integer
    Dim strBefore As String
    Dim strAfter As String

    Fieldlen = CInt(GetRealItem(Fieldlens, Index + 1, ","))
    FieldType = GetRealItem(Typelist, Index + 1, ",")
    strVal = txtValue(Index).Text
    sellen = txtValue(Index).SelLength
    selstart = txtValue(Index).selstart
    If KeyAscii = vbKeyBack Then
        Exit Sub
    End If
    Select Case FieldType
    Case "long"
        Select Case KeyAscii
            Case 48 To 57
            Case Else
            KeyAscii = 0
        End Select
    Case "double"
        Select Case KeyAscii
            Case 48 To 57
            Case 46
            Case 44
                KeyAscii = 46
            Case Else
                KeyAscii = 0
        End Select
    Case "string"
        'Everything allowed
    End Select
    If Len(strVal) = Fieldlen And sellen = 0 Then
        If KeyAscii <> vbKeyBack Then  'Backspace
            KeyAscii = 0
        End If
    End If
End Sub
