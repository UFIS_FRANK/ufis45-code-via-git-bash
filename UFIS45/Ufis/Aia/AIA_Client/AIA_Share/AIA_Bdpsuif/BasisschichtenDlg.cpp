// BasisschichtenDlg.cpp : implementation file
//

#include "stdafx.h"
#include "bdpsuif.h"
#include "BasisschichtenDlg.h"
#include "PrivList.h"
#include "CedaOdaData.h"
#include "CheckReferenz.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// BasisschichtenDlg dialog


BasisschichtenDlg::BasisschichtenDlg(BSDDATA *popBsd, bool bpIsDynamic/*true*/, CWnd* pParent /*=NULL*/)
	: CDialog(BasisschichtenDlg::IDD, pParent)
{
	pomBsd = popBsd;
	bmIsDynamic = bpIsDynamic;
	pomStatus = NULL;
	//{{AFX_DATA_INIT(BasisschichtenDlg)
	//}}AFX_DATA_INIT
}


void BasisschichtenDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(BasisschichtenDlg)
	DDX_Control(pDX, IDC_BKR1_S, m_BKR1_S);
	DDX_Control(pDX, IDC_BKR1_A, m_BKR1_A);
	DDX_Control(pDX, IDOK,		 m_OK);
	DDX_Control(pDX, IDC_BEWC,   m_BEWC);
	DDX_Control(pDX, IDC_BEWC_ALL, m_BEWC_ALL);
	DDX_Control(pDX, IDC_BKD1,	 m_BKD1);
	DDX_Control(pDX, IDC_BKF1,   m_BKF1);
	DDX_Control(pDX, IDC_BKT1,   m_BKT1);
	DDX_Control(pDX, IDC_BSDC,   m_BSDC);
	DDX_Control(pDX, IDC_BSDK,   m_BSDK);
	DDX_Control(pDX, IDC_BSDN,   m_BSDN);
	DDX_Control(pDX, IDC_BSDS,   m_BSDS);
	DDX_Control(pDX, IDC_CDAT_D, m_CDATD);
	DDX_Control(pDX, IDC_CDAT_T, m_CDATT);
	DDX_Control(pDX, IDC_CTRC1,	 m_CTRC1);
	DDX_Control(pDX, IDC_DYN,	 m_DYN);
	DDX_Control(pDX, IDC_ESBG,	 m_ESBG);
	DDX_Control(pDX, IDC_EXTRA,	 m_EXTRA);
	DDX_Control(pDX, IDC_LSEN,	 m_LSEN);
	DDX_Control(pDX, IDC_LSTU_D, m_LSTUD);
	DDX_Control(pDX, IDC_LSTU_T, m_LSTUT);
	DDX_Control(pDX, IDC_REMA,	 m_REMA);
	DDX_Control(pDX, IDC_SDU1,	 m_SDU1);
	DDX_Control(pDX, IDC_SEX1,	 m_SEX1);
	DDX_Control(pDX, IDC_SSH1,	 m_SSH1);
	DDX_Control(pDX, IDC_STAT,   m_STAT);
	DDX_Control(pDX, IDC_USEC,	 m_USEC);
	DDX_Control(pDX, IDC_USEU,	 m_USEU);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(BasisschichtenDlg, CDialog)
	//{{AFX_MSG_MAP(BasisschichtenDlg)
    ON_MESSAGE(WM_EDIT_KILLFOCUS, OnKillfocus)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// BasisschichtenDlg message handlers
//----------------------------------------------------------------------------------------

BOOL BasisschichtenDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

	if(m_Caption == LoadStg(IDS_STRING150))
		bmChangeAction = true;
	else
		bmChangeAction = false;

	m_Caption = LoadStg(IDS_STRING164) + " <" + m_Caption + ">";
	SetWindowText(m_Caption);
	
	char clStat = ogPrivList.GetStat("BASISSCHICHTENDLG.m_OK");
	SetWndStatAll(clStat,m_OK);

	//------------------------------------
	m_CDATD.SetBKColor(SILVER);
	m_CDATD.SetInitText(pomBsd->Cdat.Format("%d.%m.%Y"));
	m_CDATD.SetSecState(ogPrivList.GetStat("BASISSCHICHTENDLG.m_CDAT"));
	// - - - - - - - - - - - - - - - - - -
	m_CDATT.SetBKColor(SILVER);
	m_CDATT.SetInitText(pomBsd->Cdat.Format("%H:%M"));
	m_CDATT.SetSecState(ogPrivList.GetStat("BASISSCHICHTENDLG.m_CDAT"));
	//------------------------------------
	m_LSTUD.SetBKColor(SILVER);
	m_LSTUD.SetInitText(pomBsd->Lstu.Format("%d.%m.%Y"));
	m_LSTUD.SetSecState(ogPrivList.GetStat("BASISSCHICHTENDLG.m_LSTU"));
	// - - - - - - - - - - - - - - - - - -
	m_LSTUT.SetBKColor(SILVER);
	m_LSTUT.SetInitText(pomBsd->Lstu.Format("%H:%M"));
	m_LSTUT.SetSecState(ogPrivList.GetStat("BASISSCHICHTENDLG.m_LSTU"));
	//------------------------------------
	m_USEC.SetBKColor(SILVER);
	m_USEC.SetInitText(pomBsd->Usec);
	m_USEC.SetSecState(ogPrivList.GetStat("BASISSCHICHTENDLG.m_USEC"));
	//------------------------------------
	m_USEU.SetBKColor(SILVER);
	m_USEU.SetInitText(pomBsd->Useu);
	m_USEU.SetSecState(ogPrivList.GetStat("BASISSCHICHTENDLG.m_USEU"));
	//------------------------------------//WHITE,YELLOW,SILVER,RED,LYELLOW
	m_BSDC.SetFormat("x|#x|#x|#x|#x|#x|#x|#x|#");
	m_BSDC.SetTextLimit(1,8);
	m_BSDC.SetBKColor(YELLOW);  
	m_BSDC.SetTextErrColor(RED);
	m_BSDC.SetInitText(pomBsd->Bsdc);
	omOldCode = pomBsd->Bsdc;
	m_BSDC.SetSecState(ogPrivList.GetStat("BASISSCHICHTENDLG.m_BSDC"));
	//------------------------------------
	m_BSDK.SetTypeToString("X(12)",12,1);
	m_BSDK.SetBKColor(YELLOW);  
	m_BSDK.SetTextErrColor(RED);
	m_BSDK.SetInitText(pomBsd->Bsdk);
	m_BSDK.SetSecState(ogPrivList.GetStat("BASISSCHICHTENDLG.m_BSDK"));
	//------------------------------------
	m_BSDN.SetTypeToString("X(40)",40,1);
	m_BSDN.SetBKColor(YELLOW);  
	m_BSDN.SetTextErrColor(RED);
	m_BSDN.SetInitText(pomBsd->Bsdn);
	m_BSDN.SetSecState(ogPrivList.GetStat("BASISSCHICHTENDLG.m_BSDN"));
	//------------------------------------
	m_BSDS.SetFormat("x|#x|#x|#");
	m_BSDS.SetTextLimit(0,3);
	m_BSDS.SetTextErrColor(RED);
	m_BSDS.SetInitText(pomBsd->Bsds);
	m_BSDS.SetSecState(ogPrivList.GetStat("BASISSCHICHTENDLG.m_BSDS"));
	//------------------------------------
	m_CTRC1.SetFormat("x|#x|#x|#x|#x|#");
	m_CTRC1.SetTextLimit(0,5);
	m_CTRC1.SetTextErrColor(RED);
	m_CTRC1.SetInitText(pomBsd->Ctrc);
	m_CTRC1.SetSecState(ogPrivList.GetStat("BASISSCHICHTENDLG.m_CTRC1"));
	//------------------------------------
	m_ESBG.SetTypeToTime(true);
	m_ESBG.SetBKColor(YELLOW);  
	m_ESBG.SetTextErrColor(RED);
	if(strlen(pomBsd->Esbg) > 0)
	{
		char pclTmp[10]="";
		sprintf(pclTmp, "%s:%s", CString(pomBsd->Esbg).Left(2), CString(pomBsd->Esbg).Right(2));
		m_ESBG.SetInitText(pclTmp);
	}
	m_ESBG.SetSecState(ogPrivList.GetStat("BASISSCHICHTENDLG.m_ESBG"));
	//------------------------------------
	m_LSEN.SetTypeToTime(true);
	m_LSEN.SetBKColor(YELLOW);  
	m_LSEN.SetTextErrColor(RED);
	if(strlen(pomBsd->Lsen) > 0)
	{
		char pclTmp[10]="";
		sprintf(pclTmp, "%s:%s", CString(pomBsd->Lsen).Left(2), CString(pomBsd->Lsen).Right(2));
		m_LSEN.SetInitText(pclTmp);
	}
	m_LSEN.SetSecState(ogPrivList.GetStat("BASISSCHICHTENDLG.m_LSEN"));
	//------------------------------------
	m_SDU1.SetTypeToInt(0, 9999);
	m_SDU1.SetBKColor(YELLOW);  
	m_SDU1.SetTextErrColor(RED);
	m_SDU1.SetInitText(pomBsd->Sdu1);
	m_SDU1.SetSecState(ogPrivList.GetStat("BASISSCHICHTENDLG.m_SDU1"));
	//------------------------------------
	m_SEX1.SetTypeToInt(0, 9999);
	m_SEX1.SetTextErrColor(RED);
	m_SEX1.SetInitText(pomBsd->Sex1);
	m_SEX1.SetSecState(ogPrivList.GetStat("BASISSCHICHTENDLG.m_SEX1"));
	//------------------------------------
	m_SSH1.SetTypeToInt(0, 9999);
	m_SSH1.SetTextErrColor(RED);
	m_SSH1.SetInitText(pomBsd->Ssh1);
	m_SSH1.SetSecState(ogPrivList.GetStat("BASISSCHICHTENDLG.m_SSH1"));
	//------------------------------------
	m_BKF1.SetTypeToTime(false);
	m_BKF1.SetTextErrColor(RED);
	if(strlen(pomBsd->Bkf1) > 0)
	{
		char pclTmp[10]="";
		sprintf(pclTmp, "%s:%s", CString(pomBsd->Bkf1).Left(2), CString(pomBsd->Bkf1).Right(2));
		m_BKF1.SetInitText(pclTmp);
	}
	m_BKF1.SetSecState(ogPrivList.GetStat("BASISSCHICHTENDLG.m_BKF1"));
	//------------------------------------
	m_BKT1.SetTypeToTime(false);
	m_BKT1.SetTextErrColor(RED);
	if(strlen(pomBsd->Bkt1) > 0)
	{
		char pclTmp[10]="";
		sprintf(pclTmp, "%s:%s", CString(pomBsd->Bkt1).Left(2), CString(pomBsd->Bkt1).Right(2));
		m_BKT1.SetInitText(pclTmp);
	}
	m_BKT1.SetSecState(ogPrivList.GetStat("BASISSCHICHTENDLG.m_BKT1"));
	//------------------------------------
	m_BKD1.SetTypeToInt(0, 9999);
	m_BKD1.SetTextErrColor(RED);
	m_BKD1.SetInitText(pomBsd->Bkd1);
	m_BKD1.SetSecState(ogPrivList.GetStat("BASISSCHICHTENDLG.m_BKD1"));
	//------------------------------------
	m_BEWC.SetFormat("x|#x|#x|#x|#x|#");
	m_BEWC.SetTextLimit(0,5);
	m_BEWC.SetTextErrColor(RED);
	m_BEWC.SetInitText(pomBsd->Bewc);
	m_BEWC.SetSecState(ogPrivList.GetStat("BASISSCHICHTENDLG.m_BEWC"));
	//------------------------------------
	m_BEWC_ALL.SetTypeToString("X(40)",40,0);
	m_BEWC_ALL.SetTextErrColor(RED);
	m_BEWC_ALL.SetBKColor(SILVER);  
	if(strcmp(pomBsd->Bewc, "") != 0)
	{
		char clWhere[100];
		CCSPtrArray<ASFDATA> olAsfList;
		sprintf(clWhere,"WHERE BEWC='%s'",pomBsd->Bewc);
		if(ogAsfData.ReadSpecialData(&olAsfList, clWhere, "BEWN", false) == true)
		{
			m_BEWC_ALL.SetInitText(CString(olAsfList[0].Bewn));
		}
	}
	m_BEWC_ALL.SetSecState(ogPrivList.GetStat("BASISSCHICHTENDLG.m_BEWC_ALL"));
	//------------------------------------
	m_REMA.SetTypeToString("X(60)",60,0);
	m_REMA.SetTextErrColor(RED);
	m_REMA.SetInitText(pomBsd->Rema);
	m_REMA.SetSecState(ogPrivList.GetStat("BASISSCHICHTENDLG.m_REMA"));
	//--------------------------------------
	clStat = ogPrivList.GetStat("BASISSCHICHTENDLG.m_BKR");
	m_BKR1_A.SetCheck(1);
	if(strcmp(pomBsd->Bkr1, "A") == 0) 
	{
		m_BKR1_A.SetCheck(1);
		m_BKR1_S.SetCheck(0);
	}
	else if(strcmp(pomBsd->Bkr1, "S") == 0) 
	{
		m_BKR1_S.SetCheck(1);
		m_BKR1_A.SetCheck(0);
	}
	SetWndStatAll(clStat,m_BKR1_S);
	SetWndStatAll(clStat,m_BKR1_A);
	
	// Type option buttons
	if(strcmp(pomBsd->Type, "D") == 0)
	{
		m_DYN.SetCheck(1);
	}
	else
	{
		m_DYN.SetCheck(0);
	}
	if(strcmp(pomBsd->Type, "S") == 0)
	{
		m_STAT.SetCheck(1);
	}
	else
	{
		m_STAT.SetCheck(0);
	}
	if(strcmp(pomBsd->Type, "E") == 0)
	{
		m_EXTRA.SetCheck(1);
	}
	else
	{
		m_EXTRA.SetCheck(0);
	}
	//--------------------------------------
	return TRUE;  
}

//----------------------------------------------------------------------------------------

void BasisschichtenDlg::OnOK() 
{
	AfxGetApp()->DoWaitCursor(1);
	CString olErrorText, olNewCode;
	bool ilStatus = true;
	if(m_BSDC.GetStatus() == false) 
	{
		ilStatus = false;
		if(m_BSDC.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING237) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING237) + ogNotFormat;
		}
	}
	if(m_BSDK.GetStatus() == false) 
	{
		ilStatus = false;
		if(m_BSDK.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING330) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING330) + ogNotFormat;
		}
	}
	if(m_BSDN.GetStatus() == false) 
	{
		ilStatus = false;
		if(m_BSDN.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING229) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING229) + ogNotFormat;
		}
	}
	if(m_BSDS.GetStatus() == false) 
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING331) + ogNotFormat;
	}
	if(m_CTRC1.GetStatus() == false) 
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING332) + ogNotFormat;
	}
	if(m_ESBG.GetStatus() == false) 
	{
		ilStatus = false;
		if(m_ESBG.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING333) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING333) + ogNotFormat;
		}
	}
	if(m_LSEN.GetStatus() == false) 
	{
		ilStatus = false;
		if(m_LSEN.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING334) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING334) + ogNotFormat;
		}
	}
	if(m_SDU1.GetStatus() == false) 
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING335) + ogNotFormat;
	}
	if(m_SEX1.GetStatus() == false) 
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING336) + ogNotFormat;
	}
	if(m_SSH1.GetStatus() == false) 
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING337) + ogNotFormat;
	}
	if(m_BKF1.GetStatus() == false) 
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING338) + ogNotFormat;
	}
	if(m_BKT1.GetStatus() == false) 
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING339) + ogNotFormat;
	}
	if(m_BKD1.GetStatus() == false) 
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING340) + ogNotFormat;
	}
	if(m_BEWC.GetStatus() == false) 
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING341) + ogNotFormat;
	}
	if(m_REMA.GetStatus() == false) 
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING342) + ogNotFormat;
	}

	//*** 09.09.99 SHA ***
	if(m_SDU1.GetWindowTextLength() == 0) 
		m_SDU1.SetInitText("0");


	if(ilStatus && m_DYN.GetCheck() == 1)
	{
		CString olSdu,olSex,olSsh;
		int ilSdu,ilSex,ilSsh;
		m_SDU1.GetWindowText(olSdu);
		m_SEX1.GetWindowText(olSex);
		m_SSH1.GetWindowText(olSsh);
		CTime olEsbg,olLsen;
		CString olText;
		
		m_ESBG.GetWindowText(olText); 
		olEsbg = CTime(1999,1,1,atoi(olText.Left(2)),atoi(olText.Right(2)),0);
		
		m_LSEN.GetWindowText(olText); 
		olText = olText.Left(2) + olText.Right(2);
		olLsen = CTime(1999,1,1,atoi(olText.Left(2)),atoi(olText.Right(2)),0);
		if(olEsbg>olLsen)
		{
			olLsen += CTimeSpan(1,0,0,0);
		}
		ilSdu = atoi(olSdu);
		CTimeSpan olSpan = olLsen-olEsbg;

		if(ilSdu<=0 || ilSdu>olSpan.GetTotalMinutes())
		{
			ilStatus = false;
			olErrorText += LoadStg(IDS_STRING592);
		}

		ilSex = atoi(olSex);
		if(ilSex<0)
		{
			ilStatus = false;
			olErrorText += LoadStg(IDS_STRING593);
		}

		ilSsh = atoi(olSsh);
		if(ilSsh<0 || (ilSsh>ilSdu && ilSdu>0))
		{
			ilStatus = false;
			olErrorText += LoadStg(IDS_STRING594);
		}
	}



	//Konsistenz-Check//
	if(ilStatus == true)
	{
		CString olBsdc;
		char clWhere[100];

		CCSPtrArray<BSDDATA> olBsdList;
		m_BSDC.GetWindowText(olBsdc);
		olNewCode = olBsdc;
		sprintf(clWhere,"WHERE BSDC='%s'",olBsdc);
		if(ogBsdData.ReadSpecialData(&olBsdList,clWhere,"URNO,BSDC",false) == true)
		{
			if(olBsdList[0].Urno != pomBsd->Urno)
			{
				ilStatus = false;
				olErrorText += LoadStg(IDS_STRING237) + LoadStg(IDS_EXIST);
			}
		}
		olBsdList.DeleteAll();

		CCSPtrArray<ODADATA> olOdaCPA;
		sprintf(clWhere,"WHERE SDAC='%s'",olBsdc);
		if(ogOdaData.ReadSpecialData(&olOdaCPA,clWhere,"URNO,SDAC",false) == true)
		{
			ilStatus = false;
			olErrorText += LoadStg(IDS_STRING237) + LoadStg(IDS_EXISTINODA);
		}
		olOdaCPA.DeleteAll();

		if(m_CTRC1.GetWindowTextLength() != 0) 
		{
			char clWhere[100];
			CCSPtrArray<COTDATA> olCotList;
			CString olCtrc;m_CTRC1.GetWindowText(olCtrc);
			sprintf(clWhere,"WHERE CTRC='%s'",olCtrc);
			if(ogCotData.ReadSpecialData(&olCotList, clWhere, "CTRC", false) == false)
			{
				ilStatus = false;
				olErrorText += LoadStg(IDS_STRING349);
			}
			olCotList.DeleteAll();
		}

		if(m_BEWC.GetWindowTextLength() != 0) 
		{
			char clWhere[100];
			CCSPtrArray<ASFDATA> olAsfList;
			CString olBewc; m_BEWC.GetWindowText(olBewc);
			sprintf(clWhere,"WHERE BEWC='%s'",olBewc);
			if(ogAsfData.ReadSpecialData(&olAsfList, clWhere, "BEWC", false) == false)
			{
				ilStatus = false;
				olErrorText += LoadStg(IDS_STRING350);
			}
			olAsfList.DeleteAll();
		}
	}
    //////////////////////
	AfxGetApp()->DoWaitCursor(-1);
	if(ilStatus == true)
	{
		bool blChangeCode = true;
		if(olNewCode != omOldCode && bmChangeAction)
		{
			// Referenz Check
			CheckReferenz olCheckReferenz;
			if(pomStatus != NULL) pomStatus->SetWindowText(LoadStg(IDS_STRING120));
			int ilCount = olCheckReferenz.Check(_BSD, pomBsd->Urno, omOldCode);
			if(pomStatus != NULL) pomStatus->SetWindowText(LoadStg(ST_OK));
			CString olTxt;
			if(ilCount>0)
			{
				blChangeCode = false;
				olTxt.Format("%d %s",ilCount, LoadStg(IDS_STRING119));
				if(IDYES == MessageBox(olTxt,LoadStg(IDS_STRING149),(MB_ICONEXCLAMATION | MB_YESNO | MB_DEFBUTTON2)))
				{
					blChangeCode = true;
				}
			}
			// END Referenz Check
		}
		if(blChangeCode)
		{
			CString olText;
			m_BEWC.GetWindowText(olText); strcpy(pomBsd->Bewc, olText);
			m_BKD1.GetWindowText(olText); strcpy(pomBsd->Bkd1, olText);
			m_BKF1.GetWindowText(olText); 
			olText = olText.Left(2) + olText.Right(2);
			strcpy(pomBsd->Bkf1, olText);
			m_BKT1.GetWindowText(olText); 
			olText = olText.Left(2) + olText.Right(2);
			strcpy(pomBsd->Bkt1, olText);
			m_BSDC.GetWindowText(olText); strcpy(pomBsd->Bsdc, olText);
			m_BSDK.GetWindowText(olText); strcpy(pomBsd->Bsdk, olText);
			m_BSDN.GetWindowText(olText); strcpy(pomBsd->Bsdn, olText);
			m_BSDS.GetWindowText(olText); strcpy(pomBsd->Bsds, olText);
			m_CTRC1.GetWindowText(olText); strcpy(pomBsd->Ctrc, olText);
			m_ESBG.GetWindowText(olText); 
			olText = olText.Left(2) + olText.Right(2);
			strcpy(pomBsd->Esbg, olText);
			m_LSEN.GetWindowText(olText); 
			olText = olText.Left(2) + olText.Right(2);
			strcpy(pomBsd->Lsen, olText);
			m_SDU1.GetWindowText(olText); strcpy(pomBsd->Sdu1, olText);
			m_SEX1.GetWindowText(olText); strcpy(pomBsd->Sex1, olText);
			m_SSH1.GetWindowText(olText); strcpy(pomBsd->Ssh1, olText);

			if(bmIsDynamic == true)
			{
				strcpy(pomBsd->Type, "D");//Dynamic
			}
			else
			{
				strcpy(pomBsd->Type, "S");//Static
			}
			if(m_BKR1_A.GetCheck() == 1)
			{
				strcpy(pomBsd->Bkr1, "A");
			}
			else
			{
				strcpy(pomBsd->Bkr1, "S");
			}
			if(m_DYN.GetCheck() == 1)
			{
				strcpy(pomBsd->Type, "D");
			}
			if(m_STAT.GetCheck() == 1)
			{
				strcpy(pomBsd->Type, "S");
			}
			if(m_EXTRA.GetCheck() == 1)
			{
				strcpy(pomBsd->Type, "E");
			}
			//wandelt Return in Blank//
			CString olTemp;
			m_REMA.GetWindowText(olTemp);
			for(int i=0;i>-1;)
			{
				i = olTemp.Find("\r\n");
				if(i != -1)
				{
					if(olTemp.GetLength() > (i+2))
						olTemp = olTemp.Left(i) + " " + olTemp.Mid(i+2);
					else
						olTemp = olTemp.Left(i);
				}
			}
			strcpy(pomBsd->Rema,olTemp);
			////////////////////////////

			CDialog::OnOK();
		}
	}
	else
	{
		Beep(440,70);
		MessageBox(olErrorText,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
		m_BSDC.SetFocus();
		m_BSDC.SetSel(0,-1);
	}
}

//----------------------------------------------------------------------------------------

LONG BasisschichtenDlg::OnKillfocus(UINT lParam, LONG wParam) 
{
	if(lParam == (UINT)m_BEWC.imID ) 
	{
		if(m_BEWC.GetWindowTextLength() != 0)
		{
			char clWhere[100];
			CCSPtrArray<ASFDATA> olAsfList;
			CString olBewc; m_BEWC.GetWindowText(olBewc);
			sprintf(clWhere,"WHERE BEWC='%s'",olBewc);
			if(ogAsfData.ReadSpecialData(&olAsfList, clWhere, "BEWC,BEWN", false) == true)
			{
				m_BEWC_ALL.SetInitText(CString(olAsfList[0].Bewn));
			}
			else
			{
				m_BEWC_ALL.SetInitText("");
			}
			olAsfList.DeleteAll();
		}
		else
		{
			m_BEWC_ALL.SetInitText("");
		}
	}
	return 0L;
}

//----------------------------------------------------------------------------------------
