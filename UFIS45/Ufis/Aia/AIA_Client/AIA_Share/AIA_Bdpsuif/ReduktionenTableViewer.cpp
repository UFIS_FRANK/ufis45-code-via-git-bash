// ReduktionenTableViewer.cpp 
//

#include "stdafx.h"
#include "ReduktionenTableViewer.h"
#include "CcsDdx.h"
#include "resource.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif


// Local function prototype
static void ReduktionenTableCf(void *popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

//-----------------------------------------------------------------------------------------------
// ReduktionenTableViewer
//

ReduktionenTableViewer::ReduktionenTableViewer(CCSPtrArray<PRCDATA> *popData)
{
	pomData = popData;

	bmIsFromSearch = FALSE;
    pomReduktionenTable = NULL;
    ogDdx.Register(this, PRC_CHANGE, CString("ReduktionenTableViewer"), CString("Reduktionen Update"), ReduktionenTableCf);
    ogDdx.Register(this, PRC_NEW,    CString("ReduktionenTableViewer"), CString("Reduktionen New"),    ReduktionenTableCf);
    ogDdx.Register(this, PRC_DELETE, CString("ReduktionenTableViewer"), CString("Reduktionen Delete"), ReduktionenTableCf);
}

//-----------------------------------------------------------------------------------------------

ReduktionenTableViewer::~ReduktionenTableViewer()
{
	ogDdx.UnRegister(this, NOTUSED);
    DeleteAll();
	omHeaderDataArray.DeleteAll();
}

//-----------------------------------------------------------------------------------------------

void ReduktionenTableViewer::Attach(CCSTable *popTable)
{
    pomReduktionenTable = popTable;
}

//-----------------------------------------------------------------------------------------------

void ReduktionenTableViewer::ChangeViewTo(const char *pcpViewName)
{
    DeleteAll();    
    MakeLines();
	UpdateDisplay();
}

//-----------------------------------------------------------------------------------------------

void ReduktionenTableViewer::MakeLines()
{
	int ilReduktionenCount = pomData->GetSize();
	for (int ilLc = 0; ilLc < ilReduktionenCount; ilLc++)
	{
		PRCDATA *prlReduktionenData = &pomData->GetAt(ilLc);
		MakeLine(prlReduktionenData);
	}
}

//-----------------------------------------------------------------------------------------------

void ReduktionenTableViewer::MakeLine(PRCDATA *prpReduktionen)
{

    //if( !IsPassFilter(prpReduktionen)) return;

    // Update viewer data for this shift record
    REDUKTIONENTABLE_LINEDATA rlReduktionen;

	rlReduktionen.Urno = prpReduktionen->Urno;
	rlReduktionen.Redc = prpReduktionen->Redc;
	char plcRedl[10]="";
	sprintf(plcRedl,"%ld", prpReduktionen->Redl);
	rlReduktionen.Redl = CString(plcRedl);
	rlReduktionen.Redn = prpReduktionen->Redn;

	CreateLine(&rlReduktionen);
}

//-----------------------------------------------------------------------------------------------

void ReduktionenTableViewer::CreateLine(REDUKTIONENTABLE_LINEDATA *prpReduktionen)
{
    int ilLineCount = omLines.GetSize();
    for (int ilLineno = ilLineCount; ilLineno > 0; ilLineno--)
        if (CompareReduktionen(prpReduktionen, &omLines[ilLineno-1]) >= 0)
	        break;							 // should be inserted after Lines[ilLineno-1]
	REDUKTIONENTABLE_LINEDATA rlReduktionen;
	rlReduktionen = *prpReduktionen;
    omLines.NewAt(ilLineno, rlReduktionen);
}

//-----------------------------------------------------------------------------------------------
// UpdateDisplay: Load data selected by filter conditions to the display by using "omTable"

void ReduktionenTableViewer::UpdateDisplay()
{
	int ilLines = omLines.GetSize();
	const int ilColumns = 2;	//<===== Spaltenanzahl setzen (von 0 ab)
	omHeaderDataArray.DeleteAll();

	pomReduktionenTable->SetShowSelection(TRUE);
	pomReduktionenTable->ResetContent();

	TABLE_HEADER_COLUMN rlHeader;
	rlHeader.Alignment = COLALIGN_LEFT;
	rlHeader.Font = &ogCourier_Regular_10;

	int ilPos = 1;
	rlHeader.Length = 42; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING279),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 17; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING279),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 332; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING279),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);

	pomReduktionenTable->SetHeaderFields(omHeaderDataArray);
	pomReduktionenTable->SetDefaultSeparator();
	pomReduktionenTable->SetTableEditable(FALSE);

	TABLE_COLUMN rlColumnData;
	rlColumnData.Font = &ogCourier_Regular_10;
	rlColumnData.BkColor = WHITE;
	rlColumnData.Alignment = COLALIGN_LEFT;
	
    for (int ilLineNo = 0; ilLineNo < ilLines; ilLineNo++)
	{
		CCSPtrArray<TABLE_COLUMN> olColList;

		rlColumnData.Text = omLines[ilLineNo].Redc;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Alignment = COLALIGN_RIGHT;
		rlColumnData.Text = omLines[ilLineNo].Redl;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Alignment = COLALIGN_LEFT;
		rlColumnData.Text = omLines[ilLineNo].Redn;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		pomReduktionenTable->AddTextLine(olColList,(void*)NULL);
		olColList.DeleteAll();
	}
	pomReduktionenTable->DisplayTable();
}

//-----------------------------------------------------------------------------------------------

CString ReduktionenTableViewer::Format(REDUKTIONENTABLE_LINEDATA *prpLine)
{
    CString s;
	// ???
    return s;
}

//-----------------------------------------------------------------------------------------------

bool ReduktionenTableViewer::FindReduktionen(char *pcpReduktionenKeya, char *pcpReduktionenKeyd, int& ilItem)
{
	int ilCount = omLines.GetSize();
    for (ilItem = 0; ilItem < ilCount; ilItem++)
    {
/*		if ( (omLines[ilItem].Keya == pcpReduktionenKeya) &&
			 (omLines[ilItem].Keyd == pcpReduktionenKeyd)    )
			return TRUE;*/
	}
	return false;
}

//-----------------------------------------------------------------------------------------------

static void ReduktionenTableCf(void *popInstance, int ipDDXType,
    void *vpDataPointer, CString &ropInstanceName)
{
    ReduktionenTableViewer *polViewer = (ReduktionenTableViewer *)popInstance;
    if (ipDDXType == PRC_CHANGE || ipDDXType == PRC_NEW) polViewer->ProcessReduktionenChange((PRCDATA *)vpDataPointer);
    if (ipDDXType == PRC_DELETE) polViewer->ProcessReduktionenDelete((PRCDATA *)vpDataPointer);
} 


//-----------------------------------------------------------------------------------------------

void ReduktionenTableViewer::ProcessReduktionenChange(PRCDATA *prpReduktionen)
{
	int ilItem = 0;
	int ilColumnNo = 0;
	TABLE_COLUMN rlColumn;
	CCSPtrArray <TABLE_COLUMN> olLine;

	rlColumn.Lineno = ilItem;
	rlColumn.VerticalSeparator = SEPA_NONE;
	rlColumn.SeparatorType = SEPA_NONE;
	rlColumn.BkColor = WHITE;
	rlColumn.Font = &ogCourier_Regular_10;
	rlColumn.Alignment = COLALIGN_LEFT;

	rlColumn.Text = prpReduktionen->Redc;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Alignment = COLALIGN_RIGHT;
	char plcRedl[10]="";
	sprintf(plcRedl,"%ld", prpReduktionen->Redl);
	rlColumn.Text = CString(plcRedl);
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Alignment = COLALIGN_LEFT;
	rlColumn.Text = prpReduktionen->Redn;
	olLine.NewAt(olLine.GetSize(), rlColumn);

	
	if (FindLine(prpReduktionen->Urno, ilItem))
	{
        REDUKTIONENTABLE_LINEDATA *prlLine = &omLines[ilItem];


		prlLine->Urno = prpReduktionen->Urno;
		prlLine->Redc = prpReduktionen->Redc;
		char plcRedl[10]="";
		sprintf(plcRedl,"%ld", prpReduktionen->Redl);
		prlLine->Redl = CString(plcRedl);
		prlLine->Redn = prpReduktionen->Redn;


		pomReduktionenTable->ChangeTextLine(ilItem, &olLine, (void *)prlLine);
		pomReduktionenTable->DisplayTable();
	}
	else
	{
		MakeLine(prpReduktionen);
		if (FindLine(prpReduktionen->Urno, ilItem))
		{
	        REDUKTIONENTABLE_LINEDATA *prlLine = &omLines[ilItem];
			if(prlLine != NULL)
			{
				pomReduktionenTable->AddTextLine(olLine, (void *)prlLine);
				pomReduktionenTable->DisplayTable();
			}
		}
	}
	olLine.DeleteAll();
}

//-----------------------------------------------------------------------------------------------

void ReduktionenTableViewer::ProcessReduktionenDelete(PRCDATA *prpReduktionen)
{
	int ilItem;
	if (FindLine(prpReduktionen->Urno, ilItem))
	{
		DeleteLine(ilItem);
		pomReduktionenTable->DeleteTextLine(ilItem);
		pomReduktionenTable->DisplayTable();
	}
}

//-----------------------------------------------------------------------------------------------

bool ReduktionenTableViewer::IsPassFilter(PRCDATA *prpReduktionen)
{

	return true;
}

//-----------------------------------------------------------------------------------------------

int ReduktionenTableViewer::CompareReduktionen(REDUKTIONENTABLE_LINEDATA *prpReduktionen1, REDUKTIONENTABLE_LINEDATA *prpReduktionen2)
{
/*	int ilCase=0;
	int	ilCompareResult;
	if(prpReduktionen1->Tifd == prpReduktionen2->Tifd)
	{
		ilCompareResult= 0;
	}
	else
	{
		if(prpReduktionen1->Tifd > prpReduktionen2->Tifd)
		{
			ilCompareResult = 1;
		}
		else
		{
			ilCompareResult = -1;
		}
	}
	return ilCompareResult;
*/  
	return 0;
}

//-----------------------------------------------------------------------------------------------

void ReduktionenTableViewer::DeleteLine(int ipLineno)
{
	omLines.DeleteAt(ipLineno);
}

//-----------------------------------------------------------------------------------------------

bool ReduktionenTableViewer::FindLine(long lpUrno, int &rilLineno)
{
    for (rilLineno = 0; rilLineno < omLines.GetSize(); rilLineno++)
	{
        if (omLines[rilLineno].Urno == lpUrno)
            return true;
	}
    return false;
}
//-----------------------------------------------------------------------------------------------

void ReduktionenTableViewer::DeleteAll()
{
    while (omLines.GetSize() > 0)
    DeleteLine(0);
}

//-----------------------------------------------------------------------------------------------
// Printing Routines
//-----------------------------------------------------------------------------------------------

void ReduktionenTableViewer::PrintTableView()
{
	CString olFooter1,olFooter2;
	omTableName = LoadStg(IDS_STRING187);
	int ilOrientation = PRINT_PORTRAET;//  PRINT_PORTRAET  PRINT_LANDSCAPE
	pomPrint = new CCSPrint((CWnd*)this,ilOrientation,45);// FirstLine(P=200,L=200),LeftOffset(P=200,L=50)
	//pomPrint->imMaxLines = 38;  // (P=57,L=38)
	//omBitmap.LoadBitmap(IDB_HAJLOGO);
	//pomPrint->SetBitmaps(&omBitmap,&omBitmap);
	if (pomPrint != NULL)
	{
		if (pomPrint->InitializePrinter(ilOrientation) == TRUE)
		{
			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
			pomPrint->imLineNo = pomPrint->imMaxLines + 1;
			DOCINFO	rlDocInfo;
			memset(&rlDocInfo, 0, sizeof(DOCINFO));
			rlDocInfo.cbSize = sizeof( DOCINFO );
			rlDocInfo.lpszDocName = omTableName;	
			pomPrint->omCdc.StartDoc( &rlDocInfo );
			pomPrint->imPageNo = 0;
			int ilLines = omLines.GetSize();
			olFooter1.Format("%d %s",ilLines,omTableName);
			for(int i = 0; i < ilLines; i++ ) 
			{
				if(pomPrint->imLineNo >= pomPrint->imMaxLines)
				{
					if(pomPrint->imPageNo > 0)
					{
						olFooter2.Format("%s: %d",LoadStg(IDS_STRING222),pomPrint->imPageNo);
						pomPrint->PrintUIFFooter(olFooter1,"",olFooter2);
						pomPrint->omCdc.EndPage();
					}
					PrintTableHeader();
				}
				if(pomPrint->imLineNo == (pomPrint->imMaxLines-1) || i == (ilLines-1))
				{
					PrintTableLine(&omLines[i],true);
				}
				else
				{
					PrintTableLine(&omLines[i],false);
				}
			}
			olFooter2.Format("%s: %d",LoadStg(IDS_STRING222),pomPrint->imPageNo);
			pomPrint->PrintUIFFooter(olFooter1,"",olFooter2);
			pomPrint->omCdc.EndPage();
			pomPrint->omCdc.EndDoc();
		}
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
	delete pomPrint;
	//omBitmap.DeleteObject();
	pomPrint = NULL;
	}
}

//-----------------------------------------------------------------------------------------------

bool ReduktionenTableViewer::PrintTableHeader()
{
	pomPrint->omCdc.StartPage();
	pomPrint->imPageNo++;
	pomPrint->imLineNo = 0;

	pomPrint->PrintUIFHeader(omTableName,CString(CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M")),pomPrint->imFirstLine-10);

	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;

	rlElement.Alignment   = PRINT_LEFT;
	rlElement.FrameLeft   = PRINT_NOFRAME;
	rlElement.FrameRight  = PRINT_NOFRAME;
	rlElement.FrameTop    = PRINT_NOFRAME;
	rlElement.FrameBottom = PRINT_FRAMEMEDIUM;
	rlElement.pFont       = &pomPrint->ogCourierNew_Bold_8;

	int ilSize = omHeaderDataArray.GetSize();
	for(int i=0;i<ilSize;i++)
	{
		//if(i!=1&&i!=2)// <= setzen, wenn Spalten nicht angezeigt werden sollen 
		{
			rlElement.Length = (int)(omHeaderDataArray[i].Length*dgCCSPrintFactor);
			if(rlElement.Length < igCCSPrintMinLength)rlElement.Length+=igCCSPrintMoreLength; 
			rlElement.Text   = omHeaderDataArray[i].Text;
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
		}
	}
	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();


	return true;
}

//-----------------------------------------------------------------------------------------------

bool ReduktionenTableViewer::PrintTableLine(REDUKTIONENTABLE_LINEDATA *prpLine,bool bpLastLine)
{
	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;
	rlElement.pFont      = &pomPrint->ogCourierNew_Regular_8;
	int ilSize = omHeaderDataArray.GetSize();
	for(int i=0;i<ilSize;i++)
	{
		//if(i!=1&&i!=1)// <= setzen, wenn Spalten nicht angezeigt werden sollen 
		{
			rlElement.Length = (int)(omHeaderDataArray[i].Length*dgCCSPrintFactor);
			if(rlElement.Length < igCCSPrintMinLength)rlElement.Length+=igCCSPrintMoreLength; 
			rlElement.Alignment  = PRINT_LEFT;
			rlElement.FrameTop   = PRINT_FRAMETHIN;
			if(bpLastLine == true)
			{
				rlElement.FrameBottom = PRINT_FRAMETHIN;
			}
			else
			{
				rlElement.FrameBottom = PRINT_NOFRAME;
			}
			rlElement.FrameLeft  = PRINT_NOFRAME;
			rlElement.FrameRight = PRINT_NOFRAME;

			switch(i)
			{
			case 0:
				rlElement.FrameLeft  = PRINT_FRAMETHIN;
				rlElement.Text = prpLine->Redc;
				break;
			case 1:
				rlElement.Alignment = PRINT_RIGHT;
				rlElement.Text = prpLine->Redl;
				break;
			case 2:
				rlElement.FrameRight  = PRINT_FRAMETHIN;
				rlElement.Text = prpLine->Redn;
				break;
			}
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
		}
	}
	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();
	return true;
}

//-----------------------------------------------------------------------------------------------
