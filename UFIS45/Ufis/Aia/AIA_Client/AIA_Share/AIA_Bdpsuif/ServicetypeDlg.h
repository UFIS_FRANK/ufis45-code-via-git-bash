#if !defined(AFX_SERVICETYPEDLG_H__81720C72_333C_11D1_B39D_0000C016B067__INCLUDED_)
#define AFX_SERVICETYPEDLG_H__81720C72_333C_11D1_B39D_0000C016B067__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// ServicetypeDlg.h : header file
//
#include "CedaSTYData.h"
#include "CCSEdit.h"

/////////////////////////////////////////////////////////////////////////////
// ServicetypeDlg dialog

class ServicetypeDlg : public CDialog
{
// Construction
public:
	ServicetypeDlg(STYDATA *popSTY,CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(ServicetypeDlg)
	enum { IDD = IDD_SERVICETYPEDLG };
	CButton	m_OK;
	CString	m_Caption;
	CCSEdit	m_CDATD;
	CCSEdit	m_CDATT;
	CCSEdit	m_LSTUD;
	CCSEdit	m_LSTUT;
	CCSEdit	m_USEU;
	CCSEdit	m_USEC;
	CCSEdit	m_STYP;
	CCSEdit	m_SNAM;
	CCSEdit	m_VAFRD;
	CCSEdit	m_VAFRT;
	CCSEdit	m_VATOD;
	CCSEdit	m_VATOT;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(ServicetypeDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(ServicetypeDlg)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

private:

	STYDATA *pomSTY;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SERVICETYPEDLG_H__81720C72_333C_11D1_B39D_0000C016B067__INCLUDED_)
