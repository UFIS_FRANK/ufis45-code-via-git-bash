// CedaStfData.h

#ifndef __CEDAPSTFDATA__
#define __CEDAPSTFDATA__
 
#include "stdafx.h"
#include "basicdata.h"
#include <afxdisp.h>

//---------------------------------------------------------------------------------------------------------

struct STFDATA 
{
	CTime 	 Cdat;			// Erstellungsdatum
	COleDateTime 	 Dodm; 	// Austrittsdatum
	COleDateTime 	 Doem; 	// Eintrittsdatum
	char 	 Finm[22];		// Vorname
	char 	 Gsmn[22];		// GSM-Number
	char 	 Ktou[7];		// Urlaubsansprüche
	char 	 Lanm[42];		// Name
	char 	 Lino[22];		// License Number
	CTime 	 Lstu; 			// Datum letzte Änderung
	char 	 Makr[7]; 		// Mitarbeiterkreis
	char 	 Matr[7];		// Matricula
	char 	 Peno[22];		// Personalnummer
	char 	 Perc[5];		// Kürzel
	char 	 Prfl[3];		// Protokollierungskennung
	char 	 Rema[62]; 		// Bemerkungen
	char 	 Shnm[14];		// Kurzname
	char 	 Sken[7];		// Schichtkennzeichen
	char 	 Teld[22];		// Telefonnr. dienstlich
	char 	 Telh[22];		// Telefonnr. Handy
	char 	 Telp[22];		// Telefonnr. privat
	char 	 Tohr[3];		// Soll an SAP-HR übermittelt werden
	long 	 Urno; 			// Eindeutige Datensatz-Nr.
	char 	 Usec[34];		// Anwender (Ersteller)
	char 	 Useu[34];		// Anwender (letzte Änderung)
	char 	 Zutb[7];		// Zusätzliche Urlaubstage Schwerbehinderung
	char 	 Zutf[7];		// Zusätzliche Urlaubstage Freistellungstage
	char 	 Zutn[7]; 		// Zusätzliche Urlaubstage Nachtschicht
	char 	 Zutw[7]; 		// Zusätzliche Urlaubstage Wechselschicht

	//DataCreated by this class
	int      IsChanged;

	//long, CTime
	STFDATA(void)
	{ 
		memset(this,'\0',sizeof(*this));
		Cdat=-1;
		Lstu=-1;
		Dodm.SetStatus(COleDateTime::invalid);
		Doem.SetStatus(COleDateTime::invalid);
	}

}; // end STFDataStruct

//---------------------------------------------------------------------------------------------------------
// Class declaratino

class CedaStfData: public CCSCedaData
{
// Attributes
public:
    CMapPtrToPtr omUrnoMap;

    CCSPtrArray<STFDATA> omData;

	char pcmListOfFields[2048];

// OStfations
public:
    CedaStfData();
	~CedaStfData();
	virtual void GetDataInfo(CStringArray &ropFields, CStringArray &ropDesription, CStringArray &ropType);
	void Register(void);
	void ClearAll(bool bpWithRegistration = true);
    bool Read(char *pspWhere = NULL);
	bool Insert(STFDATA *prpStf);
	bool InsertInternal(STFDATA *prpStf);
	bool Update(STFDATA *prpStf);
	bool UpdateInternal(STFDATA *prpStf);
	bool Delete(long lpUrno);
	bool DeleteInternal(STFDATA *prpStf);
	bool ReadSpecialData(CCSPtrArray<STFDATA> *popStf,char *pspWhere,char *pspFieldList,bool ipSYS=true);
	bool Save(STFDATA *prpStf);
	void ProcessBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);
	STFDATA  *GetStfByUrno(long lpUrno);


	// Private methods
private:
    void PrepareStfData(STFDATA *prpStfData);

};

//---------------------------------------------------------------------------------------------------------


#endif //__CEDAPSTFDATA__
