// LeistungskatalogDlg.cpp : implementation file
//

#include "stdafx.h"
#include "BDPSUIF.h"
#include "LeistungskatalogDlg.h"
#include "PrivList.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// LeistungskatalogDlg dialog
static int CompareList(const LIST_DATA **e1, const LIST_DATA **e2);


LeistungskatalogDlg::LeistungskatalogDlg(GHSDATA *popGhs,CWnd* pParent) : CDialog(LeistungskatalogDlg::IDD, pParent)
{
	pomGhs = popGhs;

	//{{AFX_DATA_INIT(LeistungskatalogDlg)
	//}}AFX_DATA_INIT
}

LeistungskatalogDlg::~LeistungskatalogDlg()
{
	omGegList.DeleteAll();
	omPfcList.DeleteAll();
	omListData.DeleteAll();
	omPermList.DeleteAll();//ARE new
	omUrnos.RemoveAll();
	omPerms.RemoveAll();
}

void LeistungskatalogDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(LeistungskatalogDlg)
	DDX_Control(pDX, IDC_WTYP_FUSS, m_WTYP_FUSS);
	DDX_Control(pDX, IDC_WTYP_FAHRT, m_WTYP_FAHRT);
	DDX_Control(pDX, IDC_WTYP_SCHLEPP, m_WTYP_SCHLEPP);
	DDX_Control(pDX, IDC_PERM,		m_PERM_CB);
	DDX_Control(pDX, IDC_VRGC,		m_VRGC);
	DDX_Control(pDX, IDC_LKTY_P,	m_LKTYP);
	DDX_Control(pDX, IDC_LKTY_G,	m_LKTYG);
	DDX_Control(pDX, IDOK,			m_OK);
	DDX_Control(pDX, IDC_DISP,		m_DISP);
	DDX_Control(pDX, IDC_LKBZ_N,	m_LKBZN);
	DDX_Control(pDX, IDC_LKBZ_F,	m_LKBZF);
	DDX_Control(pDX, IDC_LKBZ_T,	m_LKBZT);
	DDX_Control(pDX, IDC_LKST,		m_LKST);
	DDX_Control(pDX, IDC_LKHC,		m_LKHC);
	DDX_Control(pDX, IDC_ATRN,		m_ATRN);
	DDX_Control(pDX, IDC_CDAT_D,	m_CDATD);
	DDX_Control(pDX, IDC_CDAT_T,	m_CDATT);
	DDX_Control(pDX, IDC_LKAM,		m_LKAM);
	DDX_Control(pDX, IDC_LKAN,		m_LKAN);
	DDX_Control(pDX, IDC_LKAR,		m_LKAR);
	DDX_Control(pDX, IDC_LKCO,		m_LKCO);
	DDX_Control(pDX, IDC_LKDA,		m_LKDA);
	DDX_Control(pDX, IDC_LKNB,		m_LKNB);
	DDX_Control(pDX, IDC_LKNM,		m_LKNM);
	DDX_Control(pDX, IDC_LKVB,		m_LKVB);
	DDX_Control(pDX, IDC_LSTU_D,	m_LSTUD);
	DDX_Control(pDX, IDC_LSTU_T,	m_LSTUT);
	DDX_Control(pDX, IDC_USEC,		m_USEC);
	DDX_Control(pDX, IDC_USEU,		m_USEU);
	DDX_Control(pDX, IDC_LKCC,		m_LKCC);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(LeistungskatalogDlg, CDialog)
	//{{AFX_MSG_MAP(LeistungskatalogDlg)
	ON_BN_CLICKED(IDC_SELECT_PERM, OnSelectPerm)
	ON_BN_CLICKED(IDC_LKTY_P, OnLktyP)
	ON_BN_CLICKED(IDC_LKTY_G, OnLktyG)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// LeistungskatalogDlg message handlers
//----------------------------------------------------------------------------------------

BOOL LeistungskatalogDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	m_Caption = LoadStg(IDS_STRING181) + " <" + m_Caption + ">";
	SetWindowText(m_Caption);

	char clStat = ogPrivList.GetStat("LEISTUNGSKATALOGDLG.m_OK");
	SetWndStatAll(clStat,m_OK);

//	SetWndStatAll('-',m_SelectPERM);

	//------------------------------------
	m_CDATD.SetBKColor(SILVER);
	m_CDATD.SetInitText(pomGhs->Cdat.Format("%d.%m.%Y"));
	m_CDATD.SetSecState(ogPrivList.GetStat("LEISTUNGSKATALOGDLG.m_CDAT"));
	// - - - - - - - - - - - - - - - - - -
	m_CDATT.SetBKColor(SILVER);
	m_CDATT.SetInitText(pomGhs->Cdat.Format("%H:%M"));
	m_CDATT.SetSecState(ogPrivList.GetStat("LEISTUNGSKATALOGDLG.m_CDAT"));
	//------------------------------------
	m_LSTUD.SetBKColor(SILVER);
	m_LSTUD.SetInitText(pomGhs->Lstu.Format("%d.%m.%Y"));
	m_LSTUD.SetSecState(ogPrivList.GetStat("LEISTUNGSKATALOGDLG.m_LSTU"));
	// - - - - - - - - - - - - - - - - - -
	m_LSTUT.SetBKColor(SILVER);
	m_LSTUT.SetInitText(pomGhs->Lstu.Format("%H:%M"));
	m_LSTUT.SetSecState(ogPrivList.GetStat("LEISTUNGSKATALOGDLG.m_LSTU"));
	//------------------------------------
	m_USEC.SetBKColor(SILVER);
	m_USEC.SetInitText(pomGhs->Usec);
	m_USEC.SetSecState(ogPrivList.GetStat("LEISTUNGSKATALOGDLG.m_USEC"));
	//------------------------------------
	m_USEU.SetBKColor(SILVER);
	m_USEU.SetInitText(pomGhs->Useu);
	m_USEU.SetSecState(ogPrivList.GetStat("LEISTUNGSKATALOGDLG.m_USEU"));
	//------------------------------------//WHITE,YELLOW,SILVER,RED,LYELLOW
	m_LKNM.SetTypeToString("X(40)",40,1);
	m_LKNM.SetBKColor(YELLOW);
	m_LKNM.SetTextErrColor(RED);
	m_LKNM.SetInitText(pomGhs->Lknm);
	m_LKNM.SetSecState(ogPrivList.GetStat("LEISTUNGSKATALOGDLG.m_LKNM"));
	//------------------------------------
	//m_LKCC.SetFormat("x|#x|#x|#x|#x|#");
	m_LKCC.SetTypeToString("X(5)",5,1);
	//m_LKCC.SetTextLimit(0,5);
	m_LKCC.SetBKColor(YELLOW);  
	m_LKCC.SetTextErrColor(RED);
	m_LKCC.SetInitText(pomGhs->Lkcc);
	m_LKCC.SetSecState(ogPrivList.GetStat("LEISTUNGSKATALOGDLG.m_LKCC"));
	//------------------------------------
	m_LKCO.SetTypeToString("X(12)",12,1);
	//m_LKCO.SetTextLimit(1,3);
	m_LKCO.SetBKColor(YELLOW);  
	m_LKCO.SetTextErrColor(RED);
	m_LKCO.SetInitText(pomGhs->Lkco);
	m_LKCO.SetSecState(ogPrivList.GetStat("LEISTUNGSKATALOGDLG.m_LKCO"));
	//------------------------------------
	m_LKAR.SetTypeToString("X(3)",3,1);
	m_LKAR.SetBKColor(YELLOW);  
	m_LKAR.SetTextErrColor(RED);
	m_LKAR.SetInitText(pomGhs->Lkar);
	m_LKAR.SetSecState(ogPrivList.GetStat("LEISTUNGSKATALOGDLG.m_LKAR"));
	//------------------------------------
	m_ATRN.SetTypeToString("X(12)",12,0);
	m_ATRN.SetTextErrColor(RED);
	m_ATRN.SetInitText(pomGhs->Atrn);
	m_ATRN.SetSecState(ogPrivList.GetStat("LEISTUNGSKATALOGDLG.m_ATRN"));
	//------------------------------------
	m_LKHC.SetTypeToDouble(7,2,0,9999999.0);
	m_LKHC.SetTextErrColor(RED);
	m_LKHC.SetInitText(pomGhs->Lkhc);
	m_LKHC.SetSecState(ogPrivList.GetStat("LEISTUNGSKATALOGDLG.m_LKHC"));
	//------------------------------------
	m_LKAN.SetTypeToString("X(20)",20,0);
	m_LKAN.SetTextErrColor(RED);
	m_LKAN.SetInitText(pomGhs->Lkan);
	m_LKAN.SetSecState(ogPrivList.GetStat("LEISTUNGSKATALOGDLG.m_LKAN"));
	//------------------------------------
	m_LKAM.SetTypeToString("##",99,0);
	m_LKAM.SetTextErrColor(RED);
	if(strcmp(pomGhs->Lkam, "") == 0)
	{
		m_LKAM.SetInitText("0");
	}
	else
	{
		m_LKAM.SetInitText(pomGhs->Lkam);
	}
	m_LKAM.SetSecState(ogPrivList.GetStat("LEISTUNGSKATALOGDLG.m_LKAM"));
	//------------------------------------
	m_LKBZN.SetCheck(0);
	m_LKBZF.SetCheck(0);
	m_LKBZT.SetCheck(0);
	if (pomGhs->Lkbz[0] == 'N') m_LKBZN.SetCheck(1);
	else if (pomGhs->Lkbz[0] == 'F') m_LKBZF.SetCheck(1);
	else if (pomGhs->Lkbz[0] == 'T') m_LKBZT.SetCheck(1);
	else m_LKBZN.SetCheck(1);
	clStat = ogPrivList.GetStat("LEISTUNGSKATALOGDLG.m_LKBZ");
	SetWndStatAll(clStat,m_LKBZN);
	SetWndStatAll(clStat,m_LKBZF);
	SetWndStatAll(clStat,m_LKBZT);
	//------------------------------------
	if (pomGhs->Lkst[0] == 'X') m_LKST.SetCheck(1);
	clStat = ogPrivList.GetStat("LEISTUNGSKATALOGDLG.m_LKST");
	SetWndStatAll(clStat,m_LKST);
	//------------------------------------
	if (pomGhs->Disp[0] == 'X') m_DISP.SetCheck(1);
	clStat = ogPrivList.GetStat("LEISTUNGSKATALOGDLG.m_DISP");
	SetWndStatAll(clStat,m_DISP);
	//------------------------------------
	m_LKVB.SetTypeToInt(0,999);
	m_LKVB.SetBKColor(YELLOW);
	m_LKVB.SetTextErrColor(RED);
	m_LKVB.SetInitText(pomGhs->Lkvb);
	m_LKVB.SetSecState(ogPrivList.GetStat("LEISTUNGSKATALOGDLG.m_LKVB"));
	//------------------------------------
	m_LKDA.SetTypeToInt(0,999);
	m_LKDA.SetBKColor(YELLOW);
	m_LKDA.SetTextErrColor(RED);
	m_LKDA.SetInitText(pomGhs->Lkda);
	m_LKDA.SetSecState(ogPrivList.GetStat("LEISTUNGSKATALOGDLG.m_LKDA"));
	//------------------------------------
	m_LKNB.SetTypeToInt(-99,999);
	m_LKNB.SetBKColor(YELLOW);
	m_LKNB.SetTextErrColor(RED);
	m_LKNB.SetInitText(pomGhs->Lknb);
	m_LKNB.SetSecState(ogPrivList.GetStat("LEISTUNGSKATALOGDLG.m_LKNB"));
	//------------------------------------
	/*m_PERM.SetTypeToString("X(256)",256,0);
	m_PERM.SetTextErrColor(RED);
	m_PERM.SetInitText(pomGhs->Perm);
	m_PERM.SetSecState(ogPrivList.GetStat("LEISTUNGSKATALOGDLG.m_PERM"));
	*/
	//------------------------------------
	MakeUrnoList(pomGhs->Vrgc);
	MakePermUrnoList(pomGhs->Perm);
	LoadPermList();
	if(strcmp(pomGhs->Lkty, "G") == 0)
	{
		m_LKTYG.SetCheck(1);
		OnLktyG();
	}
	else if(strcmp(pomGhs->Lkty, "P") == 0)
	{
		m_LKTYP.SetCheck(1);
		OnLktyP();
	}
	if(strcmp(pomGhs->Wtyp, "G") == 0)
	{
		m_WTYP_FUSS.SetCheck(1);
		m_WTYP_FAHRT.SetCheck(0);
		m_WTYP_SCHLEPP.SetCheck(0);
	}
	else if(strcmp(pomGhs->Wtyp, "F") == 0)
	{
		m_WTYP_FUSS.SetCheck(0);
		m_WTYP_FAHRT.SetCheck(1);
		m_WTYP_SCHLEPP.SetCheck(0);
	}
	else if(strcmp(pomGhs->Wtyp, "S") == 0)
	{
		m_WTYP_FUSS.SetCheck(0);
		m_WTYP_FAHRT.SetCheck(0);
		m_WTYP_SCHLEPP.SetCheck(1);
	}
	else
	{
		m_WTYP_FUSS.SetCheck(1);
	}
/*	m_VRGC.SetTypeToString("X(256)",256,0);
	m_VRGC.SetTextErrColor(RED);
	m_VRGC.SetInitText(pomGhs->Vrgc);
	m_VRGC.SetSecState(ogPrivList.GetStat("LEISTUNGSKATALOGDLG.m_VRGC"));
*/	//------------------------------------
	return TRUE;  
}

//----------------------------------------------------------------------------------------

void LeistungskatalogDlg::OnOK() 
{
	AfxGetApp()->DoWaitCursor(1);
	CString olErrorText;
	bool ilStatus = true;
	if(m_LKNM.GetStatus() == false)
	{
		ilStatus = false;
		if(m_LKNM.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING288) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING288) + ogNotFormat;
		}
	}
	if(m_LKCC.GetStatus() == false)
	{
		ilStatus = false;
		if(m_LKCC.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING237) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING237) + ogNotFormat;
		}
	}
	if(m_LKCO.GetStatus() == false)
	{
		ilStatus = false;
		if(m_LKCO.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING301) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING301) + ogNotFormat;
		}
	}
	if(m_LKAR.GetStatus() == false)
	{
		ilStatus = false;
		if(m_LKAR.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING45) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING45) + ogNotFormat;
		}
	}
	if(m_ATRN.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING46) + ogNotFormat;
	}
	if(m_LKHC.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING47) + ogNotFormat;
	}
	if(m_LKAN.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING48) + ogNotFormat;
	}
	if(m_LKAM.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING49) + ogNotFormat;
	}
	if(m_LKVB.GetStatus() == false)
	{
		ilStatus = false;
		if(m_LKVB.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING50) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING50) + ogNotFormat;
		}
	}
	if(m_LKDA.GetStatus() == false)
	{
		ilStatus = false;
		if(m_LKDA.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING381) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING381) + ogNotFormat;
		}
	}
	if(m_LKNB.GetStatus() == false)
	{
		ilStatus = false;
		if(m_LKNB.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING51) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING51) + ogNotFormat;
		}
	}

	///////////////////////////////////////////////////////////////////////////
	//Konsistenz-Check//
	if (ilStatus == true)
	{
		CString olLkcc;//
		char clWhere[100];
		CCSPtrArray<GHSDATA> olList;

		if(m_LKCC.GetWindowTextLength() != 0)
		{
			m_LKCC.GetWindowText(olLkcc);
			sprintf(clWhere,"WHERE LKCC='%s'",olLkcc);
			if(ogGhsData.ReadSpecialData(&olList,clWhere,"URNO",false) == true)
			{
				long llUrno = olList[0].Urno;
				if(pomGhs->Urno != llUrno)
				{
					ilStatus = false;
					olErrorText += LoadStg(IDS_STRING237) + LoadStg(IDS_EXIST);
				}
			}
			olList.DeleteAll();
		}
	}

	///////////////////////////////////////////////////////////////////////////
	AfxGetApp()->DoWaitCursor(-1);
	if (ilStatus == true)
	{
		
		int *pilItems1 = new int[m_VRGC.GetCount()];
		int *pilItems2 = new int[m_VRGC.GetCount()];
		int *pilPermItems = new int[m_PERM_CB.GetCount()];
		CString olVrgc;
		CString olPerm;
		int ilC = m_PERM_CB.GetSelItems( m_PERM_CB.GetCount(),  pilPermItems);
		for(int k = 0; k < ilC; k++)
		{
			char pclPerm[300];
			sprintf(pclPerm, "%ld;", omPermList[pilPermItems[k]].Urno);
			olPerm += CString(pclPerm);
		}
		olPerm = olPerm.Mid(0, olPerm.GetLength()-1);
		strcpy(pomGhs->Perm, olPerm);

		if(m_LKTYP.GetCheck() == 1)
		{
			strcpy(pomGhs->Lkty, "P");
			int ilCount = m_VRGC.GetSelItems( m_VRGC.GetCount(),  pilItems1);
			for(int j = 0; j < ilCount; j++)
			{
				char pclVrgc[300];
				sprintf(pclVrgc, "%ld;", omListData[pilItems1[j]].Urno);
				olVrgc += CString(pclVrgc);
			}
			olVrgc = olVrgc.Mid(0, olVrgc.GetLength()-1);
			strcpy(pomGhs->Vrgc, olVrgc);
		}
		else if(m_LKTYG.GetCheck() == 1)
		{
			strcpy(pomGhs->Lkty, "G");
			int ilCount = m_VRGC.GetSelItems( m_VRGC.GetCount(), pilItems2);
			for(int j = 0; j < ilCount; j++)
			{
				char pclVrgc[300];
				sprintf(pclVrgc, "%ld;", omListData[pilItems2[j]].Urno);
				olVrgc += CString(pclVrgc);
			}
			olVrgc = olVrgc.Mid(0, olVrgc.GetLength()-1);
			strcpy(pomGhs->Vrgc, olVrgc);
		}
		delete pilItems1;
		delete pilItems2;
		delete pilPermItems;

		m_LKNM.GetWindowText(pomGhs->Lknm,41);
		m_LKAR.GetWindowText(pomGhs->Lkar,4);
		m_LKHC.GetWindowText(pomGhs->Lkhc,11);
		m_ATRN.GetWindowText(pomGhs->Atrn,13);
		m_LKAN.GetWindowText(pomGhs->Lkan,21);
		CString olLkam;
		m_LKAM.GetWindowText(olLkam);
		if(olLkam.GetLength() == 0)
		{
			olLkam = CString("0");
		}
		strcpy(pomGhs->Lkam, olLkam);
		m_LKCC.GetWindowText(pomGhs->Lkcc, 6);
		m_LKCO.GetWindowText(pomGhs->Lkco, 13);
		if (m_LKBZN.GetCheck() == 1) pomGhs->Lkbz[0] = 'N';
		else if (m_LKBZF.GetCheck() == 1) pomGhs->Lkbz[0] = 'F';
		else if (m_LKBZT.GetCheck() == 1) pomGhs->Lkbz[0] = 'T';
		(m_LKST.GetCheck() == 1) ? pomGhs->Lkst[0] = 'X' : pomGhs->Lkst[0] = ' ';
		(m_DISP.GetCheck() == 1) ? pomGhs->Disp[0] = 'X' : pomGhs->Disp[0] = ' ';
		if(m_WTYP_FUSS.GetCheck() == 1)
		{
			strcpy(pomGhs->Wtyp, "G");
		}
		if(m_WTYP_FAHRT.GetCheck() == 1)
		{
			strcpy(pomGhs->Wtyp, "F");
		}
		if(m_WTYP_SCHLEPP.GetCheck() == 1)
		{
			strcpy(pomGhs->Wtyp, "S");
		}
		
		m_LKVB.GetWindowText(pomGhs->Lkvb,4);
		m_LKDA.GetWindowText(pomGhs->Lkda,4);
		m_LKNB.GetWindowText(pomGhs->Lknb,4);
	
		CDialog::OnOK();
	}
	else
	{
		Beep(440,70);
		MessageBox(olErrorText,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
		m_LKNM.SetFocus();
		m_LKNM.SetSel(0,-1);
	}
}

//----------------------------------------------------------------------------------------

void LeistungskatalogDlg::OnSelectPerm() 
{
	// TODO: Add your control notification handler code here
	
}

//----------------------------------------------------------------------------------------

void LeistungskatalogDlg::OnLktyP() 
{

	omPfcList.DeleteAll();
	omListData.DeleteAll();
	if(ogPfcData.ReadSpecialData(&omPfcList,"","URNO,FCTC",false) == true)
	{
		omListData.DeleteAll();
		m_VRGC.ResetContent();
		int ilCount = omPfcList.GetSize();
		for(int i = 0; i < ilCount; i++)
		{
			LIST_DATA rlList;
			rlList.Urno = omPfcList[i].Urno;
			strcpy(rlList.Code, omPfcList[i].Fctc);
			omListData.NewAt(omListData.GetSize(), rlList);
		}
		omListData.Sort(CompareList);
		for( i = 0; i < omListData.GetSize(); i++)
		{
			m_VRGC.AddString(omListData[i].Code); 
			for(int j = 0; j < omUrnos.GetSize(); j++)
			{
				if(omUrnos[j] == (UINT)omListData[i].Urno)
				{
					m_VRGC.SetSel(i);
				}
			}
		}
		for( i = 0; i < omListData.GetSize(); i++)
		{
			for(int j = 0; j < omUrnos.GetSize(); j++)
			{
				if(omUrnos[j] == (UINT)omListData[i].Urno)
				{
					m_VRGC.SetSel(i);
				}
			}
		}
	}

}

//----------------------------------------------------------------------------------------

void LeistungskatalogDlg::LoadPermList()
{
	omPermList.DeleteAll();
	CCSPtrArray<PERDATA> olPerList;
	if(ogPerData.ReadSpecialData(&olPerList,"","URNO,PRMC",false) == true)
	{
		omPermList.DeleteAll();
		m_PERM_CB.ResetContent();
		int ilCount = olPerList.GetSize();
		for(int i = 0; i < ilCount; i++)
		{
			LIST_DATA rlList;
			rlList.Urno = olPerList[i].Urno;
			strcpy(rlList.Code, olPerList[i].Prmc);
			omPermList.NewAt(omPermList.GetSize(), rlList);
		}
		omPermList.Sort(CompareList);
		for( i = 0; i < omPermList.GetSize(); i++)
		{
			m_PERM_CB.AddString(omPermList[i].Code);
		}
		for( i = 0; i < omPermList.GetSize(); i++)
		{
			for(int j = 0; j < omPerms.GetSize(); j++)
			{
				long u1 = omPerms[j];
				long u2 = omPermList[i].Urno;
				if(omPerms[j] == (UINT)omPermList[i].Urno)
				{
					m_PERM_CB.SetSel(i);
				}
			}
		}
	}
	olPerList.DeleteAll();
}

//----------------------------------------------------------------------------------------

void LeistungskatalogDlg::OnLktyG() 
{

	omGegList.DeleteAll();
	omListData.DeleteAll();
	if(ogGegData.ReadSpecialData(&omGegList,"","URNO,GCDE",false) == true)
	{
		omListData.DeleteAll();
		m_VRGC.ResetContent();
		int ilCount = omGegList.GetSize();
		for(int i = 0; i < ilCount; i++)
		{
			LIST_DATA rlList;
			rlList.Urno = omGegList[i].Urno;
			strcpy(rlList.Code, omGegList[i].Gcde);
			omListData.NewAt(omListData.GetSize(), rlList);
		}
		omListData.Sort(CompareList);
		for( i = 0; i < omListData.GetSize(); i++)
		{
			m_VRGC.AddString(omListData[i].Code);
		}
		for( i = 0; i < omListData.GetSize(); i++)
		{
			for(int j = 0; j < omUrnos.GetSize(); j++)
			{
				long u1 = omUrnos[j];
				long u2 = omListData[i].Urno;
				if(omUrnos[j] == (UINT)omListData[i].Urno)
				{
					m_VRGC.SetSel(i);
				}
			}
		}
	}
}

//----------------------------------------------------------------------------------------

void LeistungskatalogDlg::MakePermUrnoList(char *pspVrgc)
{
	CString olUrno;
	CString olSubString = pspVrgc;
	bool blEnd = false;
	if(!olSubString.IsEmpty())
	{
		int pos;
		int olPos = 0;
		while(blEnd == false)
		{
			pos = olSubString.Find(';');
			if(pos == -1)
			{
				blEnd = true;
				olUrno = olSubString;
			}
			else
			{
				olUrno = olSubString.Mid(0, olSubString.Find(';'));
				olSubString = olSubString.Mid(olSubString.Find(';')+1, olSubString.GetLength( )-olSubString.Find(';')+1);
			}
			omPerms.Add(atoi(olUrno));
		}
	}
}

//----------------------------------------------------------------------------------------

CString LeistungskatalogDlg::MakePermUrnoList()
{
	char pclPerm[256];
	CString olPerm;
	for(int i = 0; i < omPerms.GetSize(); i++)
	{
		char pclUrno[20]="";
		sprintf(pclUrno, "%ld;", omPerms[i]);
		olPerm += CString(pclUrno);
	}
	olPerm = olPerm.Mid(0, (olPerm.GetLength()-1));
	strcpy(pclPerm, olPerm);
	return CString(pclPerm);
}

//----------------------------------------------------------------------------------------

void LeistungskatalogDlg::MakeUrnoList(char *pspVrgc)
{
	CString olUrno;
	CString olSubString = pspVrgc;
	bool blEnd = false;
	if(!olSubString.IsEmpty())
	{
		int pos;
		int olPos = 0;
		while(blEnd == false)
		{
			pos = olSubString.Find(';');
			if(pos == -1)
			{
				blEnd = true;
				olUrno = olSubString;
			}
			else
			{
				olUrno = olSubString.Mid(0, olSubString.Find(';'));
				olSubString = olSubString.Mid(olSubString.Find(';')+1, olSubString.GetLength( )-olSubString.Find(';')+1);
			}
			omUrnos.Add(atoi(olUrno));
		}
	}
}

//----------------------------------------------------------------------------------------

CString LeistungskatalogDlg::MakeUrnoList()
{
	char pclVrgc[256];
	CString olVrgc;
	for(int i = 0; i < omUrnos.GetSize(); i++)
	{
		char pclUrno[20]="";
		sprintf(pclUrno, "%ld;", omUrnos[i]);
		olVrgc += CString(pclUrno);
	}
	olVrgc = olVrgc.Mid(0, (olVrgc.GetLength()-1));
	strcpy(pclVrgc, olVrgc);
	return CString(pclVrgc);
}

//----------------------------------------------------------------------------------------

static int CompareList(const LIST_DATA **e1, const LIST_DATA **e2)
{
	return (strcmp((**e1).Code, (**e2).Code));
}

//----------------------------------------------------------------------------------------
