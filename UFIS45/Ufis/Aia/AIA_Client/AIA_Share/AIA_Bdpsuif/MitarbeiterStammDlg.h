#if !defined(AFX_MITARBEITERSTAMMDLG_H__889A99B1_75EA_11D1_B431_0000B45A33F5__INCLUDED_)
#define AFX_MITARBEITERSTAMMDLG_H__889A99B1_75EA_11D1_B431_0000B45A33F5__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// MitarbeiterStammDlg.h : header file
//

#include "CCSGlobl.h"
#include "CCSEdit.h"
#include "CCSTable.h"
#include "CedaOrgData.h"
#include "CedaPfcData.h"
#include "CedaPerData.h"
#include "CedaCotData.h"
#include "CedaTeaDAta.h"
#include "CedaWgpData.h"
#include "CedaStfData.h"
#include "CedaSorData.h"
#include "CedaSpfData.h"
#include "CedaSpeData.h"
#include "CedaScoData.h"
#include "CedaSteData.h"
#include "CedaSwgData.h"
//*** 27.08.99 SHA ***
#include "CedaActData.h"
#include "CedaAltData.h"

/////////////////////////////////////////////////////////////////////////////
// MitarbeiterStammDlg dialog

class MitarbeiterStammDlg : public CDialog
{
// Construction
public:
	MitarbeiterStammDlg(STFDATA *popStf, 
						CCSPtrArray<SORDATA> *popSorData,
						CCSPtrArray<SPFDATA> *popSpfData,
						CCSPtrArray<SPEDATA> *popSpeData,
						CCSPtrArray<SCODATA> *popScoData,
						CCSPtrArray<STEDATA> *popSteData,
						CCSPtrArray<SWGDATA> *popSwgData,
						CWnd* pParent = NULL);   // standard constructor
	~MitarbeiterStammDlg();

// Dialog Data
	//{{AFX_DATA(MitarbeiterStammDlg)
	enum { IDD = IDD_MITARBEITERDLG };
	CCSEdit	m_REMA;
	CStatic	m_HELP_GRP;
	CButton	m_OK;
	CButton	m_THOR;
	CStatic	m_HELP_TEA;
	CStatic	m_HELP_PFC;
	CStatic	m_HELP_PER;
	CStatic	m_HELP_ORG;
	CStatic	m_HELP_COT;
	CCSEdit	m_CDATD;
	CCSEdit	m_CDATT;
	CCSEdit	m_DODDM_D;
	CCSEdit	m_DDODM_T;
	CCSEdit	m_DOEM_D;
	CCSEdit	m_DOEM_T;
	CCSEdit	m_FINM;
	CCSEdit	m_GSMN;
	CCSEdit	m_KTOU;
	CCSEdit	m_LANM;
	CCSEdit	m_LINO;
	CCSEdit	m_MATR;
	CCSEdit	m_PENO;
	CCSEdit	m_PERC;
	CCSEdit	m_SHNM;
	CCSEdit	m_TELD;
	CCSEdit	m_TELH;
	CCSEdit	m_TELP;
	CCSEdit	m_USEC;
	CCSEdit	m_USEU;
	CCSEdit	m_ZUTB;
	CCSEdit	m_ZUTF;
	CCSEdit	m_ZUTN;
	CCSEdit	m_ZUTW;
	CString m_Caption;
	CCSEdit	m_LSTUD;
	CCSEdit	m_LSTUT;
	//}}AFX_DATA
	
	STFDATA *pomStf;

	CCSTable *pomOrgTable,
			 *pomPfcTable,
			 *pomPerTable,
			 *pomCotTable,
			 *pomWgpTable,
			 *pomTeaTable;

	CCSPtrArray<SORDATA> *pomSorData;
	CCSPtrArray<SPFDATA> *pomSpfData;
	CCSPtrArray<SPEDATA> *pomSpeData;
	CCSPtrArray<SCODATA> *pomScoData;
	CCSPtrArray<STEDATA> *pomSteData;
	CCSPtrArray<SWGDATA> *pomSwgData;

	//*** 27.08.99 SHA ***
	CCSPtrArray<ACTDATA> olAct;
	CStringArray olActArray;
	CCSPtrArray<ALTDATA> olAlt;
	CStringArray olAltArray;

	void InitTables();
	LONG OnTableIPEditKillfocus( UINT wParam, LPARAM lParam);
	void OnBf_Button(int ipColY);
	void OnBg_Button(int ipColY);
	void OnBo_Button(int ipColY);
	void OnBq_Button(int ipColY);
	void OnBa_Button(int ipColY);
	void OnBgrp_Button(int ipColY);

	//*** 02.11.99 SHA ***
	long SearchFctc(CString ilFctc);
	void SaveNewFctc(CString ilFctc);

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(MitarbeiterStammDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	// Generated message map functions
	//{{AFX_MSG(MitarbeiterStammDlg)
	virtual void OnOK();
	virtual BOOL OnInitDialog();
    afx_msg LONG OnKillfocus(UINT wParam, LONG lParam);
	afx_msg void OnBf1();
	afx_msg void OnBf2();
	afx_msg void OnBf3();
	afx_msg void OnBf4();
	afx_msg void OnBf5();
	afx_msg void OnBg1();
	afx_msg void OnBg2();
	afx_msg void OnBg3();
	afx_msg void OnBg4();
	afx_msg void OnBg5();
	afx_msg void OnBo1();
	afx_msg void OnBo2();
	afx_msg void OnBo3();
	afx_msg void OnBo4();
	afx_msg void OnBo5();
	afx_msg void OnBq1();
	afx_msg void OnBq2();
	afx_msg void OnBq3();
	afx_msg void OnBq4();
	afx_msg void OnBq5();
	afx_msg void OnBa1();
	afx_msg void OnBa2();
	afx_msg void OnBa3();
	afx_msg void OnBa4();
	afx_msg void OnBa5();
	afx_msg void OnBgrp1();
	afx_msg void OnBgrp2();
	afx_msg void OnBgrp3();
	afx_msg void OnBgrp4();
	afx_msg void OnBgrp5();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MITARBEITERSTAMMDLG_H__889A99B1_75EA_11D1_B431_0000B45A33F5__INCLUDED_)
