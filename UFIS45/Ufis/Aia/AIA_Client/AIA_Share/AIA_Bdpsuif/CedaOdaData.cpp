// CedaOdaData.cpp
 
#include "stdafx.h"
#include "CedaOdaData.h"
#include "resource.h"
#include "resrc1.h"


void ProcessOdaCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

//--CEDADATA-----------------------------------------------------------------------------------------------

CedaOdaData::CedaOdaData() : CCSCedaData(&ogCommHandler)
{ 
	// Create an array of CEDARECINFO for ODADataStruct
	BEGIN_CEDARECINFO(ODADATA,ODADataRecInfo)
		FIELD_DATE		(Cdat,"CDAT")
		FIELD_CHAR_TRIM	(Ctrc,"CTRC")
		FIELD_CHAR_TRIM	(Dptc,"DPTC")
		FIELD_CHAR_TRIM	(Dura,"DURA")
		FIELD_DATE		(Lstu,"LSTU")
		FIELD_CHAR_TRIM	(Prfl,"PRFL")
		FIELD_CHAR_TRIM	(Rema,"REMA")
		FIELD_CHAR_TRIM	(Sdac,"SDAC")
		FIELD_CHAR_TRIM	(Sdak,"SDAK")
		FIELD_CHAR_TRIM	(Sdan,"SDAN")
		FIELD_CHAR_TRIM	(Sdas,"SDAS")
#ifdef CLIENT_SHA
		FIELD_LONG		(Syme,"SYME")
		FIELD_CHAR_TRIM	(Symi,"SYMI")
#endif
		FIELD_CHAR_TRIM	(Tatp,"TATP")
		FIELD_CHAR_TRIM	(Tsap,"TSAP")
		FIELD_CHAR_TRIM	(Type,"TYPE")
		FIELD_CHAR_TRIM	(Upln,"UPLN")
		FIELD_LONG		(Urno,"URNO")
		FIELD_CHAR_TRIM	(Usec,"USEC")
		FIELD_CHAR_TRIM	(Useu,"USEU")
		FIELD_CHAR_TRIM	(Free,"FREE")
	END_CEDARECINFO //(ODADataStruct)

	// FIELD_LONG, FIELD_DATE 
	// Copy the record structure
	for (int i=0; i< sizeof(ODADataRecInfo)/sizeof(ODADataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&ODADataRecInfo[i],sizeof(CEDARECINFO));
		omRecInfo.Add(prpCedaRecInfo);
	} // end for

	// initialize field names
	strcpy(pcmTableName,"ODA");
	
	
	
	
	
	//~~~~~ 13.08.99 SHA : 
#ifdef	CLIENT_SHA
	strcpy(pcmListOfFields,"CDAT,CTRC,DPTC,DURA,LSTU,PRFL,REMA,SDAC,SDAK,SDAN,SDAS,SYME,SYMI,TATP,TSAP,TYPE,UPLN,URNO,USEC,USEU,FREE");
#else
	strcpy(pcmListOfFields,"CDAT,CTRC,DPTC,DURA,LSTU,PRFL,REMA,SDAC,SDAK,SDAN,SDAS,TATP,TSAP,TYPE,UPLN,URNO,USEC,USEU,FREE");
#endif
	pcmFieldList = pcmListOfFields; //because in CedaData pcmFieldList is just a pointer







	omData.RemoveAll();
}

//---------------------------------------------------------------------------------------------------------

void CedaOdaData::GetDataInfo(CStringArray &ropFields, CStringArray &ropDesription, CStringArray &ropType)
{
	ropFields.Add("CDAT");
	ropFields.Add("CTRC");
	ropFields.Add("DPTC");
	ropFields.Add("DURA");
	ropFields.Add("LSTU");
	ropFields.Add("PRFL");
	ropFields.Add("REMA");
	ropFields.Add("SDAC");
	ropFields.Add("SDAK");
	ropFields.Add("SDAN");
	ropFields.Add("SDAS");
	ropFields.Add("URNO");
	ropFields.Add("USEC");
	ropFields.Add("USEU");

	ropFields.Add("TATP");
	ropFields.Add("TSAP");
	ropFields.Add("TYPE");
	ropFields.Add("UPLN");
	ropFields.Add("FREE");


	ropDesription.Add(LoadStg(IDS_STRING343));
	ropDesription.Add(LoadStg(IDS_STRING332));
	ropDesription.Add(LoadStg(IDS_STRING469));
	ropDesription.Add(LoadStg(IDS_STRING470));
	ropDesription.Add(LoadStg(IDS_STRING344));
	ropDesription.Add(LoadStg(IDS_STRING345));
	ropDesription.Add(LoadStg(IDS_STRING342));
	ropDesription.Add(LoadStg(IDS_STRING237));
	ropDesription.Add(LoadStg(IDS_STRING330));
	ropDesription.Add(LoadStg(IDS_STRING229));
	ropDesription.Add(LoadStg(IDS_STRING331));
	ropDesription.Add(LoadStg(IDS_STRING346));
	ropDesription.Add(LoadStg(IDS_STRING347));
	ropDesription.Add(LoadStg(IDS_STRING348));


	ropDesription.Add(LoadStg(IDS_STRING1019));
	ropDesription.Add(LoadStg(IDS_STRING1020));
	ropDesription.Add(LoadStg(IDS_STRING1021));
	ropDesription.Add(LoadStg(IDS_STRING1022));
	ropDesription.Add(LoadStg(IDS_STRING1023));

	ropType.Add("Date");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("Date");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");


	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");

}

//--REGISTER----------------------------------------------------------------------------------------------

void CedaOdaData::Register(void)
{
	ogDdx.Register((void *)this,BC_ODA_CHANGE,	CString("ODADATA"), CString("Oda-changed"),	ProcessOdaCf);
	ogDdx.Register((void *)this,BC_ODA_NEW,		CString("ODADATA"), CString("Oda-new"),		ProcessOdaCf);
	ogDdx.Register((void *)this,BC_ODA_DELETE,	CString("ODADATA"), CString("Oda-deleted"),	ProcessOdaCf);
}

//--~CEDADATA----------------------------------------------------------------------------------------------

CedaOdaData::~CedaOdaData(void)
{
	omRecInfo.DeleteAll();
	ClearAll();
}

//--CLEAR-ALL----------------------------------------------------------------------------------------------

void CedaOdaData::ClearAll(bool bpWithRegistration)
{
    omUrnoMap.RemoveAll();
    omData.DeleteAll();
	if(bpWithRegistration)
	{
		ogDdx.UnRegister(this,NOTUSED);
	}
}

//--READ-ALL-----------------------------------------------------------------------------------------------

bool CedaOdaData::Read(char *pspWhere /*NULL*/)
{
    // Select data from the database
	bool ilRc = true;
    omUrnoMap.RemoveAll();
    omData.DeleteAll();
	if(pspWhere == NULL)
	{	
		ilRc = CedaAction("RT");
	}
	else
	{
		ilRc = CedaAction("RT", pspWhere);
	}
	if (ilRc != true)
	{
		return ilRc;
	}
	// Load data from CedaData into the dynamic array of record
	for (int ilLc = 0; ilRc == true; ilLc++)
	{
		ODADATA *prlOda = new ODADATA;
		if ((ilRc = GetFirstBufferRecord(prlOda)) == true)
		{
			omData.Add(prlOda);//Update omData
			omUrnoMap.SetAt((void *)prlOda->Urno,prlOda);
		}
		else
		{
			delete prlOda;
		}
	}
    return true;
}

//--INSERT-------------------------------------------------------------------------------------------------

bool CedaOdaData::Insert(ODADATA *prpOda)
{
	prpOda->IsChanged = DATA_NEW;
	if(Save(prpOda) == false) return false; //Update Database
	InsertInternal(prpOda);
    return true;
}

//--INSERT-INTERNAL--------------------------------------------------------------------------------------

bool CedaOdaData::InsertInternal(ODADATA *prpOda)
{
	ogDdx.DataChanged((void *)this, ODA_NEW,(void *)prpOda ); //Update Viewer
	omData.Add(prpOda);//Update omData
	omUrnoMap.SetAt((void *)prpOda->Urno,prpOda);
    return true;
}

//--DELETE-------------------------------------------------------------------------------------------------

bool CedaOdaData::Delete(long lpUrno)
{
	ODADATA *prlOda = GetOdaByUrno(lpUrno);
	if (prlOda != NULL)
	{
		prlOda->IsChanged = DATA_DELETED;
		if(Save(prlOda) == false) return false; //Update Database
		DeleteInternal(prlOda);
	}
    return true;
}

//--DELETE-INTERNAL----------------------------------------------------------------------------------------

bool CedaOdaData::DeleteInternal(ODADATA *prpOda)
{
	ogDdx.DataChanged((void *)this,ODA_DELETE,(void *)prpOda); //Update Viewer
	omUrnoMap.RemoveKey((void *)prpOda->Urno);
	int ilOdaCount = omData.GetSize();
	for (int ilLc = 0; ilLc < ilOdaCount; ilLc++)
	{
		if (omData[ilLc].Urno == prpOda->Urno)
		{
			omData.DeleteAt(ilLc);//Update omData
			break;
		}
	}
    return true;
}

//--UPDATE-------------------------------------------------------------------------------------------------

bool CedaOdaData::Update(ODADATA *prpOda)
{
	if (GetOdaByUrno(prpOda->Urno) != NULL)
	{
		if (prpOda->IsChanged == DATA_UNCHANGED)
		{
			prpOda->IsChanged = DATA_CHANGED;
		}
		if(Save(prpOda) == false) return false; //Update Database
		UpdateInternal(prpOda);
	}
    return true;
}

//--UPDATE-INTERNAL--------------------------------------------------------------------------------------------

bool CedaOdaData::UpdateInternal(ODADATA *prpOda)
{
	ODADATA *prlOda = GetOdaByUrno(prpOda->Urno);
	if (prlOda != NULL)
	{
		*prlOda = *prpOda; //Update omData
		ogDdx.DataChanged((void *)this,ODA_CHANGE,(void *)prlOda); //Update Viewer
	}
    return true;
}

//--GET-BY-URNO--------------------------------------------------------------------------------------------

ODADATA *CedaOdaData::GetOdaByUrno(long lpUrno)
{
	ODADATA  *prlOda;
	if (omUrnoMap.Lookup((void *)lpUrno,(void *& )prlOda) == TRUE)
	{
		return prlOda;
	}
	return NULL;
}

//--READSPECIALDATA-------------------------------------------------------------------------------------

bool CedaOdaData::ReadSpecialData(CCSPtrArray<ODADATA> *popOda,char *pspWhere,char *pspFieldList,bool ipSYS/*=true*/)
{
	bool ilRc = true;
	char pclFieldList[256] = " ";

	if(strlen(pspFieldList) > 0) 
	{
		strcpy(pclFieldList, pspFieldList);
	}
	if(ipSYS == true) 
	{
		if (CedaAction("SYS","ODA",pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	else
	{
		if (CedaAction("RT","ODA",pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	if(popOda != NULL)
	{
		for (int ilLc = 0; ilRc == true; ilLc++)
		{
			ODADATA *prpOda = new ODADATA;
			if ((ilRc = GetBufferRecord(ilLc,prpOda,CString(pclFieldList))) == true)
			{
				popOda->Add(prpOda);
			}
			else
			{
				delete prpOda;
			}
		}
		if(popOda->GetSize() == 0) return false;
	}
    return true;
}

//--SAVE---------------------------------------------------------------------------------------------------

bool CedaOdaData::Save(ODADATA *prpOda)
{
	bool ilRc = true;
	CString olListOfData;
	char pclSelection[124];
	char pclData[524];

	if (prpOda->IsChanged == DATA_UNCHANGED)
	{
		return true; // no change, nothing to do
	}
	switch(prpOda->IsChanged)
	{
	case DATA_NEW:
		MakeCedaData(&omRecInfo,olListOfData,prpOda);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("IRT","","",pclData);
		prpOda->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_CHANGED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpOda->Urno);
		MakeCedaData(&omRecInfo,olListOfData,prpOda);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("URT",pclSelection,"",pclData);
		prpOda->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_DELETED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpOda->Urno);
		ilRc = CedaAction("DRT",pclSelection);
		break;
	}
    return ilRc;
}

//--PROCESS-CF---------------------------------------------------------------------------------------------

void  ProcessOdaCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	ogOdaData.ProcessBc(ipDDXType,vpDataPointer,ropInstanceName);
}

//--PROCESS-BC---------------------------------------------------------------------------------------------

void  CedaOdaData::ProcessBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	struct BcStruct *prlOdaData;
	prlOdaData = (struct BcStruct *) vpDataPointer;
	ODADATA *prlOda;
	if(ipDDXType == BC_ODA_NEW)
	{
		prlOda = new ODADATA;
		GetRecordFromItemList(prlOda,prlOdaData->Fields,prlOdaData->Data);
		InsertInternal(prlOda);
	}
	if(ipDDXType == BC_ODA_CHANGE)
	{
		long llUrno = GetUrnoFromSelection(prlOdaData->Selection);
		prlOda = GetOdaByUrno(llUrno);
		if(prlOda != NULL)
		{
			GetRecordFromItemList(prlOda,prlOdaData->Fields,prlOdaData->Data);
			UpdateInternal(prlOda);
		}
	}
	if(ipDDXType == BC_ODA_DELETE)
	{
		long llUrno;
		CString olSelection = (CString)prlOdaData->Selection;
		if (olSelection.Find('\'') != -1)
		{
			llUrno = GetUrnoFromSelection(prlOdaData->Selection);
		}
		else
		{
			int ilFirst = olSelection.Find("=")+2;
			int ilLast  = olSelection.GetLength();
			llUrno = atol(olSelection.Mid(ilFirst,ilLast-ilFirst));
		}
		prlOda = GetOdaByUrno(llUrno);
		if (prlOda != NULL)
		{
			DeleteInternal(prlOda);
		}
	}
}

//---------------------------------------------------------------------------------------------------------
