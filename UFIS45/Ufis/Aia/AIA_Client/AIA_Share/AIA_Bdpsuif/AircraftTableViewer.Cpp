// AircraftTableViewer.cpp 
//

#include "stdafx.h"
#include "AircraftTableViewer.h"
#include "CcsDdx.h"
#include "resource.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif


// Local function prototype
static void AircraftTableCf(void *popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

//-----------------------------------------------------------------------------------------------
// AircraftTableViewer
//

AircraftTableViewer::AircraftTableViewer(CCSPtrArray<ACTDATA> *popData)
{
	pomData = popData;

	bmIsFromSearch = FALSE;
    pomAircraftTable = NULL;
    ogDdx.Register(this, ACT_CHANGE, CString("AIRCRAFTTABLEVIEWER"), CString("Aircraft Update/new"), AircraftTableCf);
    ogDdx.Register(this, ACT_DELETE, CString("AIRCRAFTTABLEVIEWER"), CString("Aircraft Delete"), AircraftTableCf);
}

//-----------------------------------------------------------------------------------------------

AircraftTableViewer::~AircraftTableViewer()
{
	ogDdx.UnRegister(this, NOTUSED);
    DeleteAll();
	omHeaderDataArray.DeleteAll();
}

//-----------------------------------------------------------------------------------------------

void AircraftTableViewer::Attach(CCSTable *popTable)
{
    pomAircraftTable = popTable;
}

//-----------------------------------------------------------------------------------------------

void AircraftTableViewer::ChangeViewTo(const char *pcpViewName)
{
    DeleteAll();    
    MakeLines();
	UpdateDisplay();
}

//-----------------------------------------------------------------------------------------------

void AircraftTableViewer::MakeLines()
{
	int ilAircraftCount = pomData->GetSize();
	for (int ilLc = 0; ilLc < ilAircraftCount; ilLc++)
	{
		ACTDATA *prlAircraftData = &pomData->GetAt(ilLc);
		MakeLine(prlAircraftData);
	}
}

//-----------------------------------------------------------------------------------------------

void AircraftTableViewer::MakeLine(ACTDATA *prpAircraft)
{

    //if( !IsPassFilter(prpAircraft)) return;

    // Update viewer data for this shift record
    AIRCRAFTTABLE_LINEDATA rlAircraft;

	rlAircraft.Urno = prpAircraft->Urno; 
	rlAircraft.Act3 = prpAircraft->Act3; 
	rlAircraft.Act5 = prpAircraft->Act5; 
	rlAircraft.Acti = prpAircraft->Acti; 
	rlAircraft.Acfn = prpAircraft->Acfn; 
	rlAircraft.Nads = prpAircraft->Nads; 
 	rlAircraft.Acws = prpAircraft->Acws; 
 	rlAircraft.Acle = prpAircraft->Acle; 
	rlAircraft.Ache = prpAircraft->Ache; 
	rlAircraft.Seat = prpAircraft->Seat; 
	rlAircraft.Seaf = prpAircraft->Seaf; 
	rlAircraft.Seab = prpAircraft->Seab; 
	rlAircraft.Seae = prpAircraft->Seae; 
	rlAircraft.Enty = prpAircraft->Enty; 
	rlAircraft.Enno = prpAircraft->Enno; 
 	rlAircraft.Vafr = prpAircraft->Vafr.Format("%d.%m.%Y %H:%M"); 
 	rlAircraft.Vato = prpAircraft->Vato.Format("%d.%m.%Y %H:%M"); 
	rlAircraft.Afmc = prpAircraft->Afmc; 
	rlAircraft.Altc = prpAircraft->Altc; 
	rlAircraft.Modc = prpAircraft->Modc; 
//	rlAircraft.Acbt = prpAircraft->Acbt; 

	CreateLine(&rlAircraft);
}

//-----------------------------------------------------------------------------------------------

void AircraftTableViewer::CreateLine(AIRCRAFTTABLE_LINEDATA *prpAircraft)
{
    int ilLineCount = omLines.GetSize();
    for (int ilLineno = ilLineCount; ilLineno > 0; ilLineno--)
        if (CompareAircraft(prpAircraft, &omLines[ilLineno-1]) >= 0)
	        break;							 // should be inserted after Lines[ilLineno-1]
	AIRCRAFTTABLE_LINEDATA rlAircraft;
	rlAircraft = *prpAircraft;
    omLines.NewAt(ilLineno, rlAircraft);
}

//-----------------------------------------------------------------------------------------------
// UpdateDisplay: Load data selected by filter conditions to the display by using "omTable"

void AircraftTableViewer::UpdateDisplay()
{
	int ilLines = omLines.GetSize();
	const int ilColumns = 13;	//<===== Spaltenanzahl setzen (von 0 ab)
	//~~~~~ 12.08.99 SHA : 18
	omHeaderDataArray.DeleteAll();

	pomAircraftTable->SetShowSelection(TRUE);
	pomAircraftTable->ResetContent();

	TABLE_HEADER_COLUMN rlHeader;
	rlHeader.Alignment = COLALIGN_LEFT;
	rlHeader.Font = &ogCourier_Regular_10;

	int ilPos = 1;
	rlHeader.Length = 33; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING251),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 42; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING251),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 42; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING251),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 320; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING251),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 8; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING251),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 52; 
	//rlHeader.Alignment = COLALIGN_RIGHT;
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING251),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 52; 
	//rlHeader.Alignment = COLALIGN_RIGHT;
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING251),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 52; 
	//rlHeader.Alignment = COLALIGN_RIGHT;
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING251),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 25; 
	rlHeader.Alignment = COLALIGN_LEFT;
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING251),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 25; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING251),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 45; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING251),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 125; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING251),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 125; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING251),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);

	//~~~~~ 12.08.99 SHA : 
	/*
	rlHeader.Length = 8; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING251),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 133; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING251),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 133; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING251),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 25; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING251),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 25; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING251),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 25; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING251),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	*/
//	rlHeader.Length = 10; 
//	rlHeader.Text = GetListItem(LoadStg(IDS_STRING251),ilPos++,true,'|');
//	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	pomAircraftTable->SetHeaderFields(omHeaderDataArray);

	pomAircraftTable->SetDefaultSeparator();
	pomAircraftTable->SetTableEditable(FALSE);

	TABLE_COLUMN rlColumnData;
	rlColumnData.Font = &ogCourier_Regular_10;
	rlColumnData.BkColor = WHITE;
	rlColumnData.Alignment = COLALIGN_LEFT;
	
    for (int ilLineNo = 0; ilLineNo < ilLines; ilLineNo++)
	{
		CCSPtrArray<TABLE_COLUMN> olColList;

		rlColumnData.Text = omLines[ilLineNo].Act3;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Act5;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Acti;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Acfn;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		(omLines[ilLineNo].Nads == 'X') ? rlColumnData.Text = cgYes : rlColumnData.Text = cgNo;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		//rlColumnData.Alignment = COLALIGN_RIGHT;
		rlColumnData.Text = omLines[ilLineNo].Acws;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Acle;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Ache;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Seat;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		/*
		rlColumnData.Text = omLines[ilLineNo].Seaf;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Seab;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Seae;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		*/
		rlColumnData.Text = omLines[ilLineNo].Enno;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		rlColumnData.Alignment = COLALIGN_LEFT;
		rlColumnData.Text = omLines[ilLineNo].Enty;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		rlColumnData.Text = omLines[ilLineNo].Vafr;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		rlColumnData.Text = omLines[ilLineNo].Vato;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		//~~~~~ 12.08.99 SHA : 
		/*
		rlColumnData.Text = omLines[ilLineNo].Afmc;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Altc;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Modc;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		*/

//		rlColumnData.Text = omLines[ilLineNo].Acbt;
//		olColList.NewAt(olColList.GetSize(), rlColumnData);

		pomAircraftTable->AddTextLine(olColList,(void*)NULL);
		olColList.DeleteAll();
	}
	pomAircraftTable->DisplayTable();
}

//-----------------------------------------------------------------------------------------------

CString AircraftTableViewer::Format(AIRCRAFTTABLE_LINEDATA *prpLine)
{
    CString s;
	// ???
    return s;
}

//-----------------------------------------------------------------------------------------------

bool AircraftTableViewer::FindAircraft(char *pcpAircraftKeya, char *pcpAircraftKeyd, int& ilItem)
{
	int ilCount = omLines.GetSize();
    for (ilItem = 0; ilItem < ilCount; ilItem++)
    {
/*		if ( (omLines[ilItem].Keya == pcpAircraftKeya) &&
			 (omLines[ilItem].Keyd == pcpAircraftKeyd)    )
			return TRUE;*/
	}
	return false;
}

//-----------------------------------------------------------------------------------------------

static void AircraftTableCf(void *popInstance, int ipDDXType,
    void *vpDataPointer, CString &ropInstanceName)
{
    AircraftTableViewer *polViewer = (AircraftTableViewer *)popInstance;
    if (ipDDXType == ACT_CHANGE) polViewer->ProcessAircraftChange((ACTDATA *)vpDataPointer);
    if (ipDDXType == ACT_DELETE) polViewer->ProcessAircraftDelete((ACTDATA *)vpDataPointer);
} 


//-----------------------------------------------------------------------------------------------

void AircraftTableViewer::ProcessAircraftChange(ACTDATA *prpAircraft)
{
	int ilItem = 0;
	int ilColumnNo = 0;
	TABLE_COLUMN rlColumn;
	CCSPtrArray <TABLE_COLUMN> olLine;

	rlColumn.Lineno = ilItem;
	rlColumn.VerticalSeparator = SEPA_NONE;
	rlColumn.SeparatorType = SEPA_NONE;
	rlColumn.BkColor = WHITE;
	rlColumn.Font = &ogCourier_Regular_10;
	rlColumn.Alignment = COLALIGN_LEFT;

	rlColumn.Text = prpAircraft->Act3;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpAircraft->Act5;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpAircraft->Acti;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpAircraft->Acfn;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	(*prpAircraft->Nads == 'X') ? rlColumn.Text = cgYes : rlColumn.Text = cgNo;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Alignment = COLALIGN_RIGHT;
	rlColumn.Text = prpAircraft->Acws;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpAircraft->Acle;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpAircraft->Ache;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	//~~~~~ 12.08.99 SHA : 
	/*
	rlColumn.Text = prpAircraft->Seaf;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpAircraft->Seab;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpAircraft->Seae;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	*/
	rlColumn.Text = prpAircraft->Seat;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);

	rlColumn.Text = prpAircraft->Enno;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);


	rlColumn.Alignment = COLALIGN_LEFT;
	rlColumn.Text = prpAircraft->Enty;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	
	
	rlColumn.Text = prpAircraft->Vafr.Format("%d.%m.%Y %H:%M"); 
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpAircraft->Vato.Format("%d.%m.%Y %H:%M"); 
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);

	//~~~~~ 12.08.99 SHA : 
	/*
	rlColumn.Text = prpAircraft->Afmc;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpAircraft->Altc;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpAircraft->Modc;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	*/

//	rlColumn.Text = prpAircraft->Acbt;
//	rlColumn.Columnno = ilColumnNo++;
//	olLine.NewAt(olLine.GetSize(), rlColumn);

	if (FindLine(prpAircraft->Urno, ilItem))
	{
        AIRCRAFTTABLE_LINEDATA *prlLine = &omLines[ilItem];

		prlLine->Urno = prpAircraft->Urno;
		prlLine->Act3 = prpAircraft->Act3;
		prlLine->Act5 = prpAircraft->Act5;
		prlLine->Acti = prpAircraft->Acti;
		prlLine->Acfn = prpAircraft->Acfn;
		prlLine->Nads = prpAircraft->Nads;
		prlLine->Acws = prpAircraft->Acws;
		prlLine->Acle = prpAircraft->Acle;
		prlLine->Ache = prpAircraft->Ache;
		prlLine->Seat = prpAircraft->Seat;
		prlLine->Seaf = prpAircraft->Seaf;
		prlLine->Seab = prpAircraft->Seab;
		prlLine->Seae = prpAircraft->Seae;
		prlLine->Enty = prpAircraft->Enty;
		prlLine->Enno = prpAircraft->Enno;
		prlLine->Vafr = prpAircraft->Vafr.Format("%d.%m.%Y %H:%M");
		prlLine->Vato = prpAircraft->Vato.Format("%d.%m.%Y %H:%M");
		prlLine->Afmc = prpAircraft->Afmc;
		prlLine->Altc = prpAircraft->Altc;
		prlLine->Modc = prpAircraft->Modc;
//		prlLine->Acbt = prpAircraft->Acbt;

		pomAircraftTable->ChangeTextLine(ilItem, &olLine, (void *)prlLine);
		pomAircraftTable->DisplayTable();
	}
	else
	{
		MakeLine(prpAircraft);
		if (FindLine(prpAircraft->Urno, ilItem))
		{
	        AIRCRAFTTABLE_LINEDATA *prlLine = &omLines[ilItem];
			if(prlLine != NULL)
			{
				pomAircraftTable->AddTextLine(olLine, (void *)prlLine);
				pomAircraftTable->DisplayTable();
			}
		}
	}
	olLine.DeleteAll();
}

//-----------------------------------------------------------------------------------------------

void AircraftTableViewer::ProcessAircraftDelete(ACTDATA *prpAircraft)
{
	int ilItem;
	if (FindLine(prpAircraft->Urno, ilItem))
	{
		DeleteLine(ilItem);
		pomAircraftTable->DeleteTextLine(ilItem);
		pomAircraftTable->DisplayTable();
	}
}

//-----------------------------------------------------------------------------------------------

bool AircraftTableViewer::IsPassFilter(ACTDATA *prpAircraft)
{

	return true;
}

//-----------------------------------------------------------------------------------------------

int AircraftTableViewer::CompareAircraft(AIRCRAFTTABLE_LINEDATA *prpAircraft1, AIRCRAFTTABLE_LINEDATA *prpAircraft2)
{
/*	int ilCase=0;
	int	ilCompareResult;
	if(prpAircraft1->Tifd == prpAircraft2->Tifd)
	{
		ilCompareResult= 0;
	}
	else
	{
		if(prpAircraft1->Tifd > prpAircraft2->Tifd)
		{
			ilCompareResult = 1;
		}
		else
		{
			ilCompareResult = -1;
		}
	}
	return ilCompareResult;
*/  
	return 0;
}

//-----------------------------------------------------------------------------------------------

void AircraftTableViewer::DeleteLine(int ipLineno)
{
	omLines.DeleteAt(ipLineno);
}

//-----------------------------------------------------------------------------------------------

bool AircraftTableViewer::FindLine(long lpUrno, int &rilLineno)
{
    for (rilLineno = 0; rilLineno < omLines.GetSize(); rilLineno++)
	{
        if (omLines[rilLineno].Urno == lpUrno)
            return true;
	}
    return false;
}
//-----------------------------------------------------------------------------------------------

void AircraftTableViewer::DeleteAll()
{
    while (omLines.GetSize() > 0)
        DeleteLine(0);
}

//-----------------------------------------------------------------------------------------------
// Printing Routines
//-----------------------------------------------------------------------------------------------

void AircraftTableViewer::PrintTableView()
{
	CString olFooter1,olFooter2;
	omTableName = LoadStg(IDS_STRING176);
	int ilOrientation = PRINT_LANDSCAPE;//  PRINT_PORTRAET  PRINT_LANDSCAPE
	pomPrint = new CCSPrint((CWnd*)this,ilOrientation,45);// FirstLine(P=200,L=200),LeftOffset(P=200,L=50)
	//pomPrint->imMaxLines = 38;  // (P=57,L=38)
	//omBitmap.LoadBitmap(IDB_HAJLOGO);
	//pomPrint->SetBitmaps(&omBitmap,&omBitmap);
	if (pomPrint != NULL)
	{
		if (pomPrint->InitializePrinter(ilOrientation) == TRUE)
		{
			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
			pomPrint->imLineNo = pomPrint->imMaxLines + 1;
			DOCINFO	rlDocInfo;
			memset(&rlDocInfo, 0, sizeof(DOCINFO));
			rlDocInfo.cbSize = sizeof( DOCINFO );
			rlDocInfo.lpszDocName = omTableName;	
			pomPrint->omCdc.StartDoc( &rlDocInfo );
			pomPrint->imPageNo = 0;
			int ilLines = omLines.GetSize();
			olFooter1.Format("%d %s",ilLines,omTableName);
			for(int i = 0; i < ilLines; i++ ) 
			{
				if(pomPrint->imLineNo >= pomPrint->imMaxLines)
				{
					if(pomPrint->imPageNo > 0)
					{
						olFooter2.Format("%s: %d",LoadStg(IDS_STRING222),pomPrint->imPageNo);
						pomPrint->PrintUIFFooter(olFooter1,"",olFooter2);
						pomPrint->omCdc.EndPage();
					}
					PrintTableHeader();
				}
				if(pomPrint->imLineNo == (pomPrint->imMaxLines-1) || i == (ilLines-1))
				{
					PrintTableLine(&omLines[i],true);
				}
				else
				{
					PrintTableLine(&omLines[i],false);
				}
			}
			olFooter2.Format("%s: %d",LoadStg(IDS_STRING222),pomPrint->imPageNo);
			pomPrint->PrintUIFFooter(olFooter1,"",olFooter2);
			pomPrint->omCdc.EndPage();
			pomPrint->omCdc.EndDoc();
		}
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
	delete pomPrint;
	//omBitmap.DeleteObject();
	pomPrint = NULL;
	}
}

//-----------------------------------------------------------------------------------------------

bool AircraftTableViewer::PrintTableHeader()
{
	pomPrint->omCdc.StartPage();
	pomPrint->imPageNo++;
	pomPrint->imLineNo = 0;

	pomPrint->PrintUIFHeader(omTableName,CString(CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M")),pomPrint->imFirstLine-10);

	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;

	rlElement.Alignment   = PRINT_LEFT;
	rlElement.FrameLeft   = PRINT_NOFRAME;
	rlElement.FrameRight  = PRINT_NOFRAME;
	rlElement.FrameTop    = PRINT_NOFRAME;
	rlElement.FrameBottom = PRINT_FRAMEMEDIUM;
	rlElement.pFont       = &pomPrint->ogCourierNew_Bold_8;

	int ilSize = omHeaderDataArray.GetSize();
	for(int i=0;i<ilSize;i++)
	{
		//if(i!=1&&i!=2)// <= setzen, wenn Spalten nicht angezeigt werden sollen 
		{
			rlElement.Length = (int)(omHeaderDataArray[i].Length*dgCCSPrintFactor);
			if(rlElement.Length < igCCSPrintMinLength)rlElement.Length+=igCCSPrintMoreLength; 
			rlElement.Text   = omHeaderDataArray[i].Text;
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
		}
	}
	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();


	return true;
}

//-----------------------------------------------------------------------------------------------

bool AircraftTableViewer::PrintTableLine(AIRCRAFTTABLE_LINEDATA *prpLine,bool bpLastLine)
{
	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;
	rlElement.pFont      = &pomPrint->ogCourierNew_Regular_8;
	int ilSize = omHeaderDataArray.GetSize();
	for(int i=0;i<ilSize;i++)
	{
		//if(i!=1&&i!=1)// <= setzen, wenn Spalten nicht angezeigt werden sollen 
		{
			rlElement.Length = (int)(omHeaderDataArray[i].Length*dgCCSPrintFactor);
			if(rlElement.Length < igCCSPrintMinLength)rlElement.Length+=igCCSPrintMoreLength; 
			rlElement.Alignment  = PRINT_LEFT;
			rlElement.FrameTop   = PRINT_FRAMETHIN;
			if(bpLastLine == true)
			{
				rlElement.FrameBottom = PRINT_FRAMETHIN;
			}
			else
			{
				rlElement.FrameBottom = PRINT_NOFRAME;
			}
			rlElement.FrameLeft  = PRINT_NOFRAME;
			rlElement.FrameRight = PRINT_NOFRAME;
			switch(i)
			{
			case 0:
				{
					rlElement.FrameLeft  = PRINT_FRAMETHIN;
					rlElement.Text		= prpLine->Act3;
				}
				break;
			case 1:
				{
					rlElement.Text		= prpLine->Act5;
				}
				break;
			case 2:
				{
					rlElement.Text		= prpLine->Acti;
				}
				break;
			case 3:
				{
					rlElement.Text		= prpLine->Acfn;
				}
				break;
			case 4:
				{
					rlElement.Text		= prpLine->Nads;
				}
				break;
			case 5:
				{
					//rlElement.Alignment = PRINT_RIGHT;
					rlElement.Alignment = PRINT_LEFT;
					rlElement.Text		= prpLine->Acws;
				}
				break;
			case 6:
				{
					//rlElement.Alignment = PRINT_RIGHT;
					rlElement.Alignment = PRINT_LEFT;
					rlElement.Text		= prpLine->Acle;
				}
				break;
			case 7:
				{
					//rlElement.Alignment = PRINT_RIGHT;
					rlElement.Alignment = PRINT_LEFT;
					rlElement.Text		= prpLine->Ache;
				}
				break;
			case 8:
				{
					//rlElement.Alignment = PRINT_RIGHT;
					rlElement.Alignment = PRINT_LEFT;
					rlElement.Text		= prpLine->Seat;
				}
				break;
			case 9:
				{
					//rlElement.Alignment = PRINT_RIGHT;
					rlElement.Alignment = PRINT_LEFT;
					//rlElement.Text		= prpLine->Seaf;
					rlElement.Text		= prpLine->Enno;
				}
				break;
			case 10:
				{
					//rlElement.Alignment = PRINT_RIGHT;
					rlElement.Alignment = PRINT_LEFT;
					//rlElement.Text		= prpLine->Seab;
					rlElement.Text		= prpLine->Enty;
				}
				break;
			case 11:
				{
					//rlElement.Alignment = PRINT_RIGHT;
					rlElement.Alignment = PRINT_LEFT;
					//rlElement.Text		= prpLine->Seae;
					rlElement.Text		= prpLine->Vafr;
				}
				break;
			case 12:
				{
					rlElement.FrameRight= PRINT_FRAMETHIN;
					rlElement.Text		= prpLine->Vato;
				}
				break;
			}
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
		}
	}
	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();
	return true;
}

//-----------------------------------------------------------------------------------------------
