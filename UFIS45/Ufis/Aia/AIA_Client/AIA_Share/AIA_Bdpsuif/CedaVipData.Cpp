// CedaVipData.cpp
 
#include "stdafx.h"
#include "CedaVipData.h"
#include "resource.h"


void ProcessVipCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

//--CEDADATA-----------------------------------------------------------------------------------------------

CedaVipData::CedaVipData() : CCSCedaData(&ogCommHandler)
{
	// Create an array of CEDARECINFO for VIPDataStruct
	BEGIN_CEDARECINFO(VIPDATA,VIPDataRecInfo)
		FIELD_DATE		(Cdat,"CDAT")
		FIELD_LONG		(Flnu,"FLNU")
		FIELD_DATE		(Lstu,"LSTU")
		FIELD_CHAR_TRIM	(Nogr,"NOGR")
		FIELD_CHAR_TRIM	(Nopx,"NOPX")
		FIELD_CHAR_TRIM	(Paxn,"PAXN")
		FIELD_CHAR_TRIM	(Paxr,"PAXR")
		FIELD_CHAR_TRIM	(Prfl,"PRFL")
		FIELD_LONG		(Urno,"URNO")
		FIELD_CHAR_TRIM	(Usec,"USEC")
		FIELD_CHAR_TRIM	(Useu,"USEU")

	END_CEDARECINFO //(VIPDataStruct)

	// FIELD_LONG, FIELD_DATE 
	// Copy the record structure
	for (int i=0; i< sizeof(VIPDataRecInfo)/sizeof(VIPDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&VIPDataRecInfo[i],sizeof(CEDARECINFO));
		omRecInfo.Add(prpCedaRecInfo);
	} // end for

	// initialize field names
	strcpy(pcmTableName,"VIP");
	strcpy(pcmListOfFields,"CDAT,FLNU,LSTU,NOGR,NOPX,PAXN,PAXR,PRFL,URNO,USEC,USEU");
	pcmFieldList = pcmListOfFields; //because in CedaData pcmFieldList is just a pointer

	omData.RemoveAll();
}

//---------------------------------------------------------------------------------------------------------

void CedaVipData::GetDataInfo(CStringArray &ropFields, CStringArray &ropDesription, CStringArray &ropType)
{
	ropFields.Add("CDAT");
	ropFields.Add("FLNU");
	ropFields.Add("LSTU");
	ropFields.Add("NOGR");
	ropFields.Add("NOPX");
	ropFields.Add("PAXN");
	ropFields.Add("PAXR");
	ropFields.Add("PRFL");
	ropFields.Add("URNO");
	ropFields.Add("USEC");
	ropFields.Add("USEU");

	ropDesription.Add(LoadStg(IDS_STRING343));
	ropDesription.Add(LoadStg(IDS_STRING573));
	ropDesription.Add(LoadStg(IDS_STRING344));
	ropDesription.Add(LoadStg(IDS_STRING599));
	ropDesription.Add(LoadStg(IDS_STRING600));
	ropDesription.Add(LoadStg(IDS_STRING601));
	ropDesription.Add(LoadStg(IDS_STRING602));
	ropDesription.Add(LoadStg(IDS_STRING345));
	ropDesription.Add(LoadStg(IDS_STRING346));
	ropDesription.Add(LoadStg(IDS_STRING347));
	ropDesription.Add(LoadStg(IDS_STRING348));

	ropType.Add("Date");
	ropType.Add("String");
	ropType.Add("Date");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");

}

//--REGISTER----------------------------------------------------------------------------------------------

void CedaVipData::Register(void)
{
	ogDdx.Register((void *)this,BC_VIP_CHANGE,	CString("VIPDATA"), CString("Vip-changed"),	ProcessVipCf);
	ogDdx.Register((void *)this,BC_VIP_NEW,		CString("VIPDATA"), CString("Vip-new"),		ProcessVipCf);
	ogDdx.Register((void *)this,BC_VIP_DELETE,	CString("VIPDATA"), CString("Vip-deleted"),	ProcessVipCf);
}

//--~CEDADATA----------------------------------------------------------------------------------------------

CedaVipData::~CedaVipData(void)
{
	omRecInfo.DeleteAll();
	ClearAll();
}

//--CLEAR-ALL----------------------------------------------------------------------------------------------

void CedaVipData::ClearAll(bool bpWithRegistration)
{
    omUrnoMap.RemoveAll();
    omData.DeleteAll();
	if(bpWithRegistration)
	{
		ogDdx.UnRegister(this,NOTUSED);
	}
}

//--READ-ALL-----------------------------------------------------------------------------------------------

bool CedaVipData::Read(char *pspWhere /*NULL*/)
{
    // Select data from the database
	bool ilRc = true;
    omUrnoMap.RemoveAll();
    omData.DeleteAll();
	if(pspWhere == NULL)
	{	
		ilRc = CedaAction("RT");
	}
	else
	{
		ilRc = CedaAction("RT", pspWhere);
	}
	if (ilRc != true)
	{
		return ilRc;
	}
	// Load data from CedaData into the dynamic array of record
	for (int ilLc = 0; ilRc == true; ilLc++)
	{
		VIPDATA *prlVip = new VIPDATA;
		if ((ilRc = GetFirstBufferRecord(prlVip)) == true)
		{
			omData.Add(prlVip);//Update omData
			omUrnoMap.SetAt((void *)prlVip->Urno,prlVip);
		}
		else
		{
			delete prlVip;
		}
	}
    return true;
}

//--INSERT-------------------------------------------------------------------------------------------------

bool CedaVipData::Insert(VIPDATA *prpVip)
{
	prpVip->IsChanged = DATA_NEW;
	if(Save(prpVip) == false) return false; //Update Database
	InsertInternal(prpVip);
    return true;
}

//--INSERT-INTERNAL--------------------------------------------------------------------------------------

bool CedaVipData::InsertInternal(VIPDATA *prpVip)
{
	ogDdx.DataChanged((void *)this, VIP_NEW,(void *)prpVip ); //Update Viewer
	omData.Add(prpVip);//Update omData
	omUrnoMap.SetAt((void *)prpVip->Urno,prpVip);
    return true;
}

//--DELETE-------------------------------------------------------------------------------------------------

bool CedaVipData::Delete(long lpUrno)
{
	VIPDATA *prlVip = GetVipByUrno(lpUrno);
	if (prlVip != NULL)
	{
		prlVip->IsChanged = DATA_DELETED;
		if(Save(prlVip) == false) return false; //Update Database
		DeleteInternal(prlVip);
	}
    return true;
}

//--DELETE-INTERNAL----------------------------------------------------------------------------------------

bool CedaVipData::DeleteInternal(VIPDATA *prpVip)
{
	ogDdx.DataChanged((void *)this,VIP_DELETE,(void *)prpVip); //Update Viewer
	omUrnoMap.RemoveKey((void *)prpVip->Urno);
	int ilVipCount = omData.GetSize();
	for (int ilLc = 0; ilLc < ilVipCount; ilLc++)
	{
		if (omData[ilLc].Urno == prpVip->Urno)
		{
			omData.DeleteAt(ilLc);//Update omData
			break;
		}
	}
    return true;
}

//--UPDATE-------------------------------------------------------------------------------------------------

bool CedaVipData::Update(VIPDATA *prpVip)
{
	if (GetVipByUrno(prpVip->Urno) != NULL)
	{
		if (prpVip->IsChanged == DATA_UNCHANGED)
		{
			prpVip->IsChanged = DATA_CHANGED;
		}
		if(Save(prpVip) == false) return false; //Update Database
		UpdateInternal(prpVip);
	}
    return true;
}

//--UPDATE-INTERNAL--------------------------------------------------------------------------------------------

bool CedaVipData::UpdateInternal(VIPDATA *prpVip)
{
	VIPDATA *prlVip = GetVipByUrno(prpVip->Urno);
	if (prlVip != NULL)
	{
		*prlVip = *prpVip; //Update omData
		ogDdx.DataChanged((void *)this,VIP_CHANGE,(void *)prlVip); //Update Viewer
	}
    return true;
}

//--GET-BY-URNO--------------------------------------------------------------------------------------------

VIPDATA *CedaVipData::GetVipByUrno(long lpUrno)
{
	VIPDATA  *prlVip;
	if (omUrnoMap.Lookup((void *)lpUrno,(void *& )prlVip) == TRUE)
	{
		return prlVip;
	}
	return NULL;
}

//--READSPECIALDATA-------------------------------------------------------------------------------------

bool CedaVipData::ReadSpecialData(CCSPtrArray<VIPDATA> *popVip,char *pspWhere,char *pspFieldList,bool ipSYS/*=true*/)
{
	bool ilRc = true;
	char pclFieldList[256] = " ";

	if(strlen(pspFieldList) > 0) 
	{
		strcpy(pclFieldList, pspFieldList);
	}
	if(ipSYS == true) 
	{
		if (CedaAction("SYS","VIP",pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	else
	{
		if (CedaAction("RT","VIP",pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	if(popVip != NULL)
	{
		for (int ilLc = 0; ilRc == true; ilLc++)
		{
			VIPDATA *prpVip = new VIPDATA;
			if ((ilRc = GetBufferRecord(ilLc,prpVip,CString(pclFieldList))) == true)
			{
				popVip->Add(prpVip);
			}
			else
			{
				delete prpVip;
			}
		}
		if(popVip->GetSize() == 0) return false;
	}
    return true;
}

//--SAVE---------------------------------------------------------------------------------------------------

bool CedaVipData::Save(VIPDATA *prpVip)
{
	bool ilRc = true;
	CString olListOfData;
	char pclSelection[124];
	char pclData[524];

	if (prpVip->IsChanged == DATA_UNCHANGED)
	{
		return true; // no change, nothing to do
	}
	switch(prpVip->IsChanged)
	{
	case DATA_NEW:
		MakeCedaData(&omRecInfo,olListOfData,prpVip);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("IRT","","",pclData);
		prpVip->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_CHANGED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpVip->Urno);
		MakeCedaData(&omRecInfo,olListOfData,prpVip);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("URT",pclSelection,"",pclData);
		prpVip->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_DELETED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpVip->Urno);
		ilRc = CedaAction("DRT",pclSelection);
		break;
	}
    return ilRc;
}

//--PROCESS-CF---------------------------------------------------------------------------------------------

void  ProcessVipCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	ogVipData.ProcessBc(ipDDXType,vpDataPointer,ropInstanceName);
}

//--PROCESS-BC---------------------------------------------------------------------------------------------

void  CedaVipData::ProcessBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	struct BcStruct *prlVipData;
	prlVipData = (struct BcStruct *) vpDataPointer;
	VIPDATA *prlVip;
	if(ipDDXType == BC_VIP_NEW)
	{
		prlVip = new VIPDATA;
		GetRecordFromItemList(prlVip,prlVipData->Fields,prlVipData->Data);
		InsertInternal(prlVip);
	}
	if(ipDDXType == BC_VIP_CHANGE)
	{
		long llUrno = GetUrnoFromSelection(prlVipData->Selection);
		prlVip = GetVipByUrno(llUrno);
		if(prlVip != NULL)
		{
			GetRecordFromItemList(prlVip,prlVipData->Fields,prlVipData->Data);
			UpdateInternal(prlVip);
		}
	}
	if(ipDDXType == BC_VIP_DELETE)
	{
		long llUrno;
		CString olSelection = (CString)prlVipData->Selection;
		if (olSelection.Find('\'') != -1)
		{
			llUrno = GetUrnoFromSelection(prlVipData->Selection);
		}
		else
		{
			int ilFirst = olSelection.Find("=")+2;
			int ilLast  = olSelection.GetLength();
			llUrno = atol(olSelection.Mid(ilFirst,ilLast-ilFirst));
		}
		prlVip = GetVipByUrno(llUrno);
		if (prlVip != NULL)
		{
			DeleteInternal(prlVip);
		}
	}
}

//---------------------------------------------------------------------------------------------------------
