#ifndef __ORGANISATIONSEINHEITENTABLEVIEWER_H__
#define __ORGANISATIONSEINHEITENTABLEVIEWER_H__

#include "stdafx.h"
#include "CedaOrgData.h"
#include "CCSTable.h"
#include "CViewer.h"
#include "CCSPrint.h"


struct ORGANISATIONSEINHEITENTABLE_LINEDATA
{
	long	Urno;
	CString	Dpt1;
	CString	Dpt2;
	CString	Dptn;
	CString	Rema;
};

/////////////////////////////////////////////////////////////////////////////
// OrganisationseinheitenTableViewer

class OrganisationseinheitenTableViewer : public CViewer
{
// Constructions
public:
    OrganisationseinheitenTableViewer(CCSPtrArray<ORGDATA> *popData);
    ~OrganisationseinheitenTableViewer();

    void Attach(CCSTable *popAttachWnd);
    virtual void ChangeViewTo(const char *pcpViewName);
// Internal data processing routines
private:
	bool IsPassFilter(ORGDATA *prpOrganisationseinheiten);
	int CompareOrganisationseinheiten(ORGANISATIONSEINHEITENTABLE_LINEDATA *prpOrganisationseinheiten1, ORGANISATIONSEINHEITENTABLE_LINEDATA *prpOrganisationseinheiten2);
    void MakeLines();
	void MakeLine(ORGDATA *prpOrganisationseinheiten);
	bool FindLine(long lpUrno, int &rilLineno);
// Operations
public:
	void DeleteAll();
	void CreateLine(ORGANISATIONSEINHEITENTABLE_LINEDATA *prpOrganisationseinheiten);
	void DeleteLine(int ipLineno);
// Window refreshing routines
public:
	void UpdateDisplay();
	CString Format(ORGANISATIONSEINHEITENTABLE_LINEDATA *prpLine);
	void ProcessOrganisationseinheitenChange(ORGDATA *prpOrganisationseinheiten);
	void ProcessOrganisationseinheitenDelete(ORGDATA *prpOrganisationseinheiten);
	bool FindOrganisationseinheiten(char *prpOrganisationseinheitenKeya, char *prpOrganisationseinheitenKeyd, int& ilItem);
// Attributes used for filtering condition
private:
	bool bmIsFromSearch;
// Attributes
private:
    CCSTable *pomOrganisationseinheitenTable;
	CCSPtrArray<ORGDATA> *pomData;
// Methods which handle changes (from Data Distributor)
public:
    CCSPtrArray<ORGANISATIONSEINHEITENTABLE_LINEDATA> omLines;
///////
	CCSPtrArray<TABLE_HEADER_COLUMN> omHeaderDataArray;
//Print 
	void PrintTableView();
	bool PrintTableLine(ORGANISATIONSEINHEITENTABLE_LINEDATA *prpLine,bool bpLastLine);
	bool PrintTableHeader();
	CCSPrint *pomPrint;
	//CBitmap omBitmap;
	CString omTableName;

};

#endif //__ORGANISATIONSEINHEITENTABLEVIEWER_H__
