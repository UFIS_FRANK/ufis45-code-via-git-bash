// PlanungsgruppenTableViewer.cpp 
//

#include "stdafx.h"
#include "PlanungsgruppenTableViewer.h"
#include "CcsDdx.h"
#include "resource.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif


// Local function prototype
static void PlanungsgruppenTableCf(void *popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

//-----------------------------------------------------------------------------------------------
// PlanungsgruppenTableViewer
//

PlanungsgruppenTableViewer::PlanungsgruppenTableViewer(CCSPtrArray<PGPDATA> *popData)
{
	pomData = popData;

	bmIsFromSearch = FALSE;
    pomPlanungsgruppenTable = NULL;
    ogDdx.Register(this, PGP_CHANGE, CString("PlanungsgruppenTableViewer"), CString("Planungsgruppen Update"), PlanungsgruppenTableCf);
    ogDdx.Register(this, PGP_NEW,    CString("PlanungsgruppenTableViewer"), CString("Planungsgruppen New"),    PlanungsgruppenTableCf);
    ogDdx.Register(this, PGP_DELETE, CString("PlanungsgruppenTableViewer"), CString("Planungsgruppen Delete"), PlanungsgruppenTableCf);
}

//-----------------------------------------------------------------------------------------------

PlanungsgruppenTableViewer::~PlanungsgruppenTableViewer()
{
	ogDdx.UnRegister(this, NOTUSED);
    DeleteAll();
	omHeaderDataArray.DeleteAll();
}

//-----------------------------------------------------------------------------------------------

void PlanungsgruppenTableViewer::Attach(CCSTable *popTable)
{
    pomPlanungsgruppenTable = popTable;
}

//-----------------------------------------------------------------------------------------------

void PlanungsgruppenTableViewer::ChangeViewTo(const char *pcpViewName)
{
    DeleteAll();    
    MakeLines();
	UpdateDisplay();
}

//-----------------------------------------------------------------------------------------------

void PlanungsgruppenTableViewer::MakeLines()
{
	int ilPlanungsgruppenCount = pomData->GetSize();
	for (int ilLc = 0; ilLc < ilPlanungsgruppenCount; ilLc++)
	{
		PGPDATA *prlPlanungsgruppenData = &pomData->GetAt(ilLc);
		MakeLine(prlPlanungsgruppenData);
	}
}

//-----------------------------------------------------------------------------------------------

void PlanungsgruppenTableViewer::MakeLine(PGPDATA *prpPlanungsgruppen)
{

    //if( !IsPassFilter(prpPlanungsgruppen)) return;

    // Update viewer data for this shift record
    PLANUNGSGRUPPENTABLE_LINEDATA rlPlanungsgruppen;

	rlPlanungsgruppen.Urno = prpPlanungsgruppen->Urno;	// uniform resource number
	rlPlanungsgruppen.Cdat = prpPlanungsgruppen->Cdat; 	// creation date
	rlPlanungsgruppen.Lstu = prpPlanungsgruppen->Lstu; 	// last update
	rlPlanungsgruppen.Usec = prpPlanungsgruppen->Usec; 	// user (creator)
	rlPlanungsgruppen.Useu = prpPlanungsgruppen->Useu; 	// user (last change)
	rlPlanungsgruppen.Pgpc = prpPlanungsgruppen->Pgpc; 	// code
	rlPlanungsgruppen.Type = prpPlanungsgruppen->Type; 	// type of planning group (only PFCs so far, MNE 19.1.99)
	rlPlanungsgruppen.Pgpn = prpPlanungsgruppen->Pgpn; 	// name
	rlPlanungsgruppen.Rema = prpPlanungsgruppen->Rema; 	// remark
	rlPlanungsgruppen.Pgpm = prpPlanungsgruppen->Pgpm; 	// members (separated by semicolon)
	rlPlanungsgruppen.Minm = prpPlanungsgruppen->Minm;  // min number of members (separated by semicolon)
	rlPlanungsgruppen.Maxm = prpPlanungsgruppen->Maxm;	// max number of members(separated by semicolon)
	rlPlanungsgruppen.Grpm = MakeGroupString(prpPlanungsgruppen);

	CreateLine(&rlPlanungsgruppen);
}
//-----------------------------------------------------------------------------------------------

void PlanungsgruppenTableViewer::CreateLine(PLANUNGSGRUPPENTABLE_LINEDATA *prpPlanungsgruppen)
{
    int ilLineCount = omLines.GetSize();

// Following lines mustn't be used - sorting takes place in another place now
/*
    for (int ilLineno = ilLineCount; ilLineno > 0; ilLineno--)
        if (ComparePlanungsgruppen(prpPlanungsgruppen, &omLines[ilLineno-1]) >= 0)
	        break;							 // should be inserted after Lines[ilLineno-1]
*/

	PLANUNGSGRUPPENTABLE_LINEDATA rlPlanungsgruppen;
	rlPlanungsgruppen = *prpPlanungsgruppen;
    omLines.NewAt(omLines.GetSize(), rlPlanungsgruppen);
}
//-----------------------------------------------------------------------------------------------


// UpdateDisplay: Load data selected by filter conditions to the display by using "omTable"

void PlanungsgruppenTableViewer::UpdateDisplay()
{
	int ilLines = omLines.GetSize();
	const int ilColumns = 3;	//<===== Spaltenanzahl setzen (von 0 ab)
	omHeaderDataArray.DeleteAll();

	pomPlanungsgruppenTable->SetShowSelection(TRUE);
	pomPlanungsgruppenTable->ResetContent();

	TABLE_HEADER_COLUMN rlHeader;
	rlHeader.Alignment = COLALIGN_LEFT;
	rlHeader.Font = &ogCourier_Regular_8;

	int ilPos = 1;
	rlHeader.Length = 42; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING492),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 332; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING492),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 25; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING492),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 320; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING492),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 500; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING492),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);

	pomPlanungsgruppenTable->SetHeaderFields(omHeaderDataArray);
	pomPlanungsgruppenTable->SetDefaultSeparator();
	pomPlanungsgruppenTable->SetTableEditable(FALSE);

	TABLE_COLUMN rlColumnData;
	rlColumnData.Font = &ogCourier_Regular_8;
	rlColumnData.BkColor = WHITE;
	rlColumnData.Alignment = COLALIGN_LEFT;
	
    for (int ilLineNo = 0; ilLineNo < ilLines; ilLineNo++)
	{
		CCSPtrArray<TABLE_COLUMN> olColList;
		rlColumnData.Text = omLines[ilLineNo].Pgpc;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Pgpn;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Type;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Rema;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Grpm;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		pomPlanungsgruppenTable->AddTextLine(olColList,(void*)NULL);
		olColList.DeleteAll();
	}
	pomPlanungsgruppenTable->DisplayTable();
}
//-----------------------------------------------------------------------------------------------

CString PlanungsgruppenTableViewer::MakeGroupString(PGPDATA *prpPgp)
{
	CString olResult = "";
	
	CString olTmp = "";
	CString olPgpm = prpPgp->Pgpm;
	CString olMinm = prpPgp->Minm;
	CString olMaxm = prpPgp->Maxm;
	
	CStringArray rlPgpm;
	CStringArray rlMinm;
	CStringArray rlMaxm;
	
	char clSeparator;
	clSeparator = '|';
	
	ExtractItemList(olPgpm, &rlPgpm, clSeparator);	// Function defined in CCSBasicFunc (part of CCSClassLib)
	ExtractItemList(olMinm, &rlMinm, clSeparator);
	ExtractItemList(olMaxm, &rlMaxm, clSeparator);
	
	for (int ilLC = 0; ilLC < rlPgpm.GetSize() && ilLC < rlMinm.GetSize(); ilLC++)
	{
		olResult += rlPgpm.GetAt(ilLC);
		olResult += CString("(");

		olResult += rlMinm.GetAt(ilLC);
		olResult += CString("/");

		if(ilLC < rlMaxm.GetSize())
			olResult += rlMaxm.GetAt(ilLC);
		else
			olResult += "";

		olResult += CString(") ");
	}

	return olResult;
}

//-----------------------------------------------------------------------------------------------


CString PlanungsgruppenTableViewer::Format(PLANUNGSGRUPPENTABLE_LINEDATA *prpLine)
{
    CString s;
	// ???
    return s;
}

//-----------------------------------------------------------------------------------------------

bool PlanungsgruppenTableViewer::FindPlanungsgruppen(long lpPgpUrno, char *pcpPgpName, int& ilItem)
{
	// can't find a line without urno or name
	if (lpPgpUrno > 0 && pcpPgpName != NULL)
	{
		int ilCount = omLines.GetSize();
		for (ilItem = 0; ilItem < ilCount; ilItem++)
		{
			if ( (omLines[ilItem].Urno == lpPgpUrno) || (omLines[ilItem].Pgpn == *pcpPgpName))
				return true;
		}
	}
	return false;
}

//-----------------------------------------------------------------------------------------------

static void PlanungsgruppenTableCf(void *popInstance, int ipDDXType,
    void *vpDataPointer, CString &ropInstanceName)
{
    PlanungsgruppenTableViewer *polViewer = (PlanungsgruppenTableViewer *)popInstance;
    if (ipDDXType == PGP_CHANGE || ipDDXType == PGP_NEW) polViewer->ProcessPlanungsgruppenChange((PGPDATA *)vpDataPointer);
    if (ipDDXType == PGP_DELETE) polViewer->ProcessPlanungsgruppenDelete((PGPDATA *)vpDataPointer);
} 


//-----------------------------------------------------------------------------------------------

void PlanungsgruppenTableViewer::ProcessPlanungsgruppenChange(PGPDATA *prpPlanungsgruppen)
{
	int ilItem = 0;
	int ilColumnNo = 0;
	TABLE_COLUMN rlColumn;
	CCSPtrArray <TABLE_COLUMN> olLine;
	
	// Update table data struct
	rlColumn.Lineno = ilItem;
	rlColumn.VerticalSeparator = SEPA_NONE;
	rlColumn.SeparatorType = SEPA_NONE;
	rlColumn.BkColor = WHITE;
	rlColumn.Font = &ogCourier_Regular_8;
	rlColumn.Alignment = COLALIGN_LEFT;

	rlColumn.Text = prpPlanungsgruppen->Pgpc;	
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpPlanungsgruppen->Pgpn;	
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpPlanungsgruppen->Type;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpPlanungsgruppen->Rema;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = MakeGroupString(prpPlanungsgruppen);
	olLine.NewAt(olLine.GetSize(), rlColumn);
		
	if (FindLine(prpPlanungsgruppen->Urno, ilItem))
	{	
		// Update viewer data struct 
        PLANUNGSGRUPPENTABLE_LINEDATA *prlLine = &omLines[ilItem];

		prlLine->Pgpc = prpPlanungsgruppen->Pgpc;	
		prlLine->Pgpn = prpPlanungsgruppen->Pgpn; 	
		prlLine->Type = prpPlanungsgruppen->Type; 	
		prlLine->Rema = prpPlanungsgruppen->Rema; 	
		prlLine->Pgpm = prpPlanungsgruppen->Pgpm;
		prlLine->Minm = prpPlanungsgruppen->Minm;
		prlLine->Maxm = prpPlanungsgruppen->Maxm;
		prlLine->Grpm = MakeGroupString(prpPlanungsgruppen);
			
		pomPlanungsgruppenTable->ChangeTextLine(ilItem, &olLine, (void *)prlLine);
		pomPlanungsgruppenTable->DisplayTable();
	}
	else
	{
		MakeLine(prpPlanungsgruppen);
		if (FindLine(prpPlanungsgruppen->Urno, ilItem))
		{
			PLANUNGSGRUPPENTABLE_LINEDATA *prlLine = &omLines[ilItem];
			if(prlLine != NULL)
			{
				pomPlanungsgruppenTable->AddTextLine(olLine, (void *)prlLine);
				pomPlanungsgruppenTable->DisplayTable();
			}
		}
	}
	olLine.DeleteAll();
}

//-----------------------------------------------------------------------------------------------

void PlanungsgruppenTableViewer::ProcessPlanungsgruppenDelete(PGPDATA *prpPlanungsgruppen)
{
	int ilItem;
	if (FindLine(prpPlanungsgruppen->Urno, ilItem))
	{
		DeleteLine(ilItem);
		pomPlanungsgruppenTable->DeleteTextLine(ilItem);
		pomPlanungsgruppenTable->DisplayTable();
	}
}

//-----------------------------------------------------------------------------------------------

bool PlanungsgruppenTableViewer::IsPassFilter(PGPDATA *prpPlanungsgruppen)
{

	return true;
}

//-----------------------------------------------------------------------------------------------

int PlanungsgruppenTableViewer::ComparePlanungsgruppen(PLANUNGSGRUPPENTABLE_LINEDATA *prpPlanungsgruppen1, PLANUNGSGRUPPENTABLE_LINEDATA *prpPlanungsgruppen2)
{
	int ilCase=0;
	int	ilCompareResult;
	if(prpPlanungsgruppen1->Pgpc == prpPlanungsgruppen2->Pgpc)
	{
		ilCompareResult= 0;
	}
	else
	{
		if(prpPlanungsgruppen1->Pgpc > prpPlanungsgruppen2->Pgpc)
		{
			ilCompareResult = 1;
		}
		else
		{
			ilCompareResult = -1;
		}
	}
	return ilCompareResult;
  
	// return 0;
}

//-----------------------------------------------------------------------------------------------

void PlanungsgruppenTableViewer::DeleteLine(int ipLineno)
{
	omLines.DeleteAt(ipLineno);
}

//-----------------------------------------------------------------------------------------------

bool PlanungsgruppenTableViewer::FindLine(long lpUrno, int &rilLineno)
{
    for (rilLineno = 0; rilLineno < omLines.GetSize(); rilLineno++)
	{
        if (omLines[rilLineno].Urno == lpUrno)
            return true;
	}
    return false;
}
//-----------------------------------------------------------------------------------------------

void PlanungsgruppenTableViewer::DeleteAll()
{
    while (omLines.GetSize() > 0)
    DeleteLine(0);
}

//-----------------------------------------------------------------------------------------------
// Printing Routines
//-----------------------------------------------------------------------------------------------

void PlanungsgruppenTableViewer::PrintTableView()
{
	CString olFooter1,olFooter2;
	omTableName = LoadStg(IDS_STRING200);
	int ilOrientation = PRINT_LANDSCAPE;//  PRINT_PORTRAET  PRINT_LANDSCAPE
	pomPrint = new CCSPrint((CWnd*)this,ilOrientation,45);// FirstLine(P=200,L=200),LeftOffset(P=200,L=50)
	//pomPrint->imMaxLines = 38;  // (P=57,L=38)
	//omBitmap.LoadBitmap(IDB_HAJLOGO);
	//pomPrint->SetBitmaps(&omBitmap,&omBitmap);
	if (pomPrint != NULL)
	{
		if (pomPrint->InitializePrinter(ilOrientation) == TRUE)
		{
			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
			pomPrint->imLineNo = pomPrint->imMaxLines + 1;
			DOCINFO	rlDocInfo;
			memset(&rlDocInfo, 0, sizeof(DOCINFO));
			rlDocInfo.cbSize = sizeof( DOCINFO );
			rlDocInfo.lpszDocName = omTableName;	
			pomPrint->omCdc.StartDoc( &rlDocInfo );
			pomPrint->imPageNo = 0;
			int ilLines = omLines.GetSize();
			olFooter1.Format("%d %s",ilLines,omTableName);
			for(int i = 0; i < ilLines; i++ ) 
			{
				if(pomPrint->imLineNo >= pomPrint->imMaxLines)
				{
					if(pomPrint->imPageNo > 0)
					{
						olFooter2.Format("%s: %d",LoadStg(IDS_STRING222),pomPrint->imPageNo);
						pomPrint->PrintUIFFooter(olFooter1,"",olFooter2);
						pomPrint->omCdc.EndPage();
					}
					PrintTableHeader();
				}
				if(pomPrint->imLineNo == (pomPrint->imMaxLines-1) || i == (ilLines-1))
				{
					PrintTableLine(&omLines[i],true);
				}
				else
				{
					PrintTableLine(&omLines[i],false);
				}
			}
			olFooter2.Format("%s: %d",LoadStg(IDS_STRING222),pomPrint->imPageNo);
			pomPrint->PrintUIFFooter(olFooter1,"",olFooter2);
			pomPrint->omCdc.EndPage();
			pomPrint->omCdc.EndDoc();
		}
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
	delete pomPrint;
	//omBitmap.DeleteObject();
	pomPrint = NULL;
	}
}

//-----------------------------------------------------------------------------------------------

bool PlanungsgruppenTableViewer::PrintTableHeader()
{
	pomPrint->omCdc.StartPage();
	pomPrint->imPageNo++;
	pomPrint->imLineNo = 0;

	pomPrint->PrintUIFHeader(omTableName,CString(CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M")),pomPrint->imFirstLine-10);

	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;

	rlElement.Alignment   = PRINT_LEFT;
	rlElement.FrameLeft   = PRINT_NOFRAME;
	rlElement.FrameRight  = PRINT_NOFRAME;
	rlElement.FrameTop    = PRINT_NOFRAME;
	rlElement.FrameBottom = PRINT_FRAMEMEDIUM;
	rlElement.pFont       = &pomPrint->ogCourierNew_Bold_8;

	int ilSize = omHeaderDataArray.GetSize();
	for(int i=0;i<ilSize;i++)
	{
		if(i!=3)// <= setzen, wenn Spalten nicht angezeigt werden sollen 
		{
			rlElement.Length = (int)(omHeaderDataArray[i].Length*dgCCSPrintFactor);
			if(rlElement.Length < igCCSPrintMinLength)rlElement.Length+=igCCSPrintMoreLength; 
			rlElement.Text   = omHeaderDataArray[i].Text;
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
		}
	}
	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();

	return true;
}

//-----------------------------------------------------------------------------------------------

bool PlanungsgruppenTableViewer::PrintTableLine(PLANUNGSGRUPPENTABLE_LINEDATA *prpLine,bool bpLastLine)
{
	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;
	rlElement.pFont      = &pomPrint->ogCourierNew_Regular_8;
	int ilSize = omHeaderDataArray.GetSize();
	for(int i=0;i<ilSize;i++)
	{
		if(i!=3)// <= setzen, wenn Spalten nicht angezeigt werden sollen 
		{
			rlElement.Length = (int)(omHeaderDataArray[i].Length*dgCCSPrintFactor);
			if(rlElement.Length < igCCSPrintMinLength)rlElement.Length+=igCCSPrintMoreLength; 
			rlElement.Alignment  = PRINT_LEFT;
			rlElement.FrameTop   = PRINT_FRAMETHIN;
			if(bpLastLine == true)
			{
				rlElement.FrameBottom = PRINT_FRAMETHIN;
			}
			else
			{
				rlElement.FrameBottom = PRINT_NOFRAME;
			}
			rlElement.FrameLeft  = PRINT_NOFRAME;
			rlElement.FrameRight = PRINT_NOFRAME;
			switch(i)
			{
			case 0:
				rlElement.FrameLeft  = PRINT_FRAMETHIN;
				rlElement.Text = prpLine->Pgpc;
				break;
			case 1:
				rlElement.Text = prpLine->Pgpn;
				break;
			case 2:
				rlElement.Text = prpLine->Type;
				break;
			case 3:
				rlElement.Text = prpLine->Rema;
				break;
			case 4:
				rlElement.FrameRight = PRINT_FRAMETHIN;
				rlElement.Text = prpLine->Grpm;
				break;
			}
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
		}
	}
	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();
	return true;
}

//-----------------------------------------------------------------------------------------------
