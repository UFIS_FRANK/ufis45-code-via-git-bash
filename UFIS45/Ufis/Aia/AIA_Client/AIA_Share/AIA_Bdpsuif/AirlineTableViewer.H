#ifndef __AIRLINETABLEVIEWER_H__
#define __AIRLINETABLEVIEWER_H__

#include "stdafx.h"
#include "CedaALTData.h"
#include "CCSTable.h"
#include "CViewer.h"
#include "CCSPrint.h"


struct AIRLINETABLE_LINEDATA
{
	long	Urno;
	CString	Alc2;
	CString	Alc3;
	CString	Akey;
	CString	Alfn;
	CString	Cash;
	CString	Term;
	CString	Vafr;
	CString	Vato;
	CString	Rprt;
	CString	Wrko;
	CString	Admd;
	CString 	 Add1;
	CString 	 Add2;
	CString 	 Add3;
	CString 	 Add4;
	CString 	 Base;
	CString 	 Cont;
	CString 	 Ctry;
	CString 	 Emps;
	CString 	 Exec;
	CString 	 Fond;
	CString 	 Iano;
	CString 	 Iata;
	CString 	 Ical;
	CString 	 Icao;
	CString 	 Lcod;
	CString 	 Phon;
	CString 	 Self;
	CString 	 Sita;
	CString 	 Telx;
	CString 	 Text;
	CString 	 Tfax;
	CString 	 Webs;
	CString 	 Home;
	CString 	 Doin;
};

/////////////////////////////////////////////////////////////////////////////
// AirlineTableViewer

class AirlineTableViewer : public CViewer
{
// Constructions
public:
    AirlineTableViewer(CCSPtrArray<ALTDATA> *popData);
    ~AirlineTableViewer();

    void Attach(CCSTable *popAttachWnd);
    virtual void ChangeViewTo(const char *pcpViewName);
// Internal data processing routines
private:
	bool IsPassFilter(ALTDATA *prpAirline);
	int CompareAirline(AIRLINETABLE_LINEDATA *prpAirline1, AIRLINETABLE_LINEDATA *prpAirline2);
    void MakeLines();
	void MakeLine(ALTDATA *prpAirline);
	bool FindLine(long lpUrno, int &rilLineno);
// Operations
public:
	void DeleteAll();
	void CreateLine(AIRLINETABLE_LINEDATA *prpAirline);
	void DeleteLine(int ipLineno);
// Window refreshing routines
public:
	void UpdateDisplay();
	CString Format(AIRLINETABLE_LINEDATA *prpLine);
	void ProcessAirlineChange(ALTDATA *prpAirline);
	void ProcessAirlineDelete(ALTDATA *prpAirline);
	bool FindAirline(char *prpAirlineKeya, char *prpAirlineKeyd, int& ilItem);
// Attributes used for filtering condition
private:
	bool bmIsFromSearch;
// Attributes
private:
    CCSTable *pomAirlineTable;
	CCSPtrArray<ALTDATA> *pomData;
// Methods which handle changes (from Data Distributor)
public:
    CCSPtrArray<AIRLINETABLE_LINEDATA> omLines;
///////
	CCSPtrArray<TABLE_HEADER_COLUMN> omHeaderDataArray;
//Print 
	void PrintTableView();
	bool PrintTableLine(AIRLINETABLE_LINEDATA *prpLine,bool bpLastLine);
	bool PrintTableHeader();
	CCSPrint *pomPrint;
	//CBitmap omBitmap;
	CString omTableName;

};

#endif //__AIRLINETABLEVIEWER_H__
