// CedaPrcData.cpp
 
#include "stdafx.h"
#include "CedaPrcData.h"
#include "resource.h"


void ProcessPrcCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

//--CEDADATA-----------------------------------------------------------------------------------------------

CedaPrcData::CedaPrcData() : CCSCedaData(&ogCommHandler)
{
	// Create an array of CEDARECINFO for PRCDataStruct
	BEGIN_CEDARECINFO(PRCDATA,PRCDataRecInfo)
		FIELD_DATE		(Cdat,"CDAT")
		FIELD_DATE		(Lstu,"LSTU")
		FIELD_CHAR_TRIM	(Appl,"APPL")
		FIELD_CHAR_TRIM	(Prfl,"PRFL")
		FIELD_CHAR_TRIM	(Redc,"REDC")
		FIELD_INT		(Redl,"REDL")
		FIELD_CHAR_TRIM	(Redn,"REDN")
		FIELD_LONG		(Urno,"URNO")
		FIELD_CHAR_TRIM	(Usec,"USEC")
		FIELD_CHAR_TRIM	(Useu,"USEU")

	END_CEDARECINFO //(PRCDataStruct)

	// FIELD_LONG, FIELD_DATE 
	// Copy the record structure
	for (int i=0; i< sizeof(PRCDataRecInfo)/sizeof(PRCDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&PRCDataRecInfo[i],sizeof(CEDARECINFO));
		omRecInfo.Add(prpCedaRecInfo);
	} // end for

	// initialize field names
	strcpy(pcmTableName,"PRC");
	strcpy(pcmListOfFields,"CDAT,LSTU,APPL,PRFL,REDC,REDL,REDN,URNO,USEC,USEU");
	pcmFieldList = pcmListOfFields; //because in CedaData pcmFieldList is just a pointer

	omData.RemoveAll();
}

//---------------------------------------------------------------------------------------------------------

void CedaPrcData::GetDataInfo(CStringArray &ropFields, CStringArray &ropDesription, CStringArray &ropType)
{
	ropFields.Add("CDAT");
	ropFields.Add("LSTU");
	ropFields.Add("PRFL");
	ropFields.Add("REDC");
	ropFields.Add("REDN");
	ropFields.Add("URNO");
	ropFields.Add("USEC");
	ropFields.Add("USEU");
	ropFields.Add("APPL");
	ropFields.Add("REDL");

	ropDesription.Add(LoadStg(IDS_STRING343));
	ropDesription.Add(LoadStg(IDS_STRING344));
	ropDesription.Add(LoadStg(IDS_STRING345));
	ropDesription.Add(LoadStg(IDS_STRING486));
	ropDesription.Add(LoadStg(IDS_STRING487));
	ropDesription.Add(LoadStg(IDS_STRING346));
	ropDesription.Add(LoadStg(IDS_STRING347));
	ropDesription.Add(LoadStg(IDS_STRING348));
	ropDesription.Add(LoadStg(IDS_STRING488));
	ropDesription.Add(LoadStg(IDS_STRING489));

	ropType.Add("Date");
	ropType.Add("Date");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");

}

//--REGISTER----------------------------------------------------------------------------------------------

void CedaPrcData::Register(void)
{
	ogDdx.Register((void *)this,BC_PRC_CHANGE,	CString("PRCDATA"), CString("Prc-changed"),	ProcessPrcCf);
	ogDdx.Register((void *)this,BC_PRC_NEW,		CString("PRCDATA"), CString("Prc-new"),		ProcessPrcCf);
	ogDdx.Register((void *)this,BC_PRC_DELETE,	CString("PRCDATA"), CString("Prc-deleted"),	ProcessPrcCf);
}

//--~CEDADATA----------------------------------------------------------------------------------------------

CedaPrcData::~CedaPrcData(void)
{
	omRecInfo.DeleteAll();
	ClearAll();
}

//--CLEAR-ALL----------------------------------------------------------------------------------------------

void CedaPrcData::ClearAll(bool bpWithRegistration)
{
    omUrnoMap.RemoveAll();
    omData.DeleteAll();
	if(bpWithRegistration)
	{
		ogDdx.UnRegister(this,NOTUSED);
	}
}

//--READ-ALL-----------------------------------------------------------------------------------------------

bool CedaPrcData::Read(char *pspWhere /*NULL*/)
{
    // Select data from the database
	bool ilRc = true;
    omUrnoMap.RemoveAll();
    omData.DeleteAll();
	if(pspWhere == NULL)
	{	
		ilRc = CedaAction("RT");
	}
	else
	{
		ilRc = CedaAction("RT", pspWhere);
	}
	if (ilRc != true)
	{
		return ilRc;
	}
	// Load data from CedaData into the dynamic array of record
	for (int ilLc = 0; ilRc == true; ilLc++)
	{
		PRCDATA *prlPrc = new PRCDATA;
		if ((ilRc = GetFirstBufferRecord(prlPrc)) == true)
		{
			omData.Add(prlPrc);//Update omData
			omUrnoMap.SetAt((void *)prlPrc->Urno,prlPrc);
		}
		else
		{
			delete prlPrc;
		}
	}
    return true;
}

//--INSERT-------------------------------------------------------------------------------------------------

bool CedaPrcData::Insert(PRCDATA *prpPrc)
{
	prpPrc->IsChanged = DATA_NEW;
	if(Save(prpPrc) == false) return false; //Update Database
	InsertInternal(prpPrc);
    return true;
}

//--INSERT-INTERNAL--------------------------------------------------------------------------------------

bool CedaPrcData::InsertInternal(PRCDATA *prpPrc)
{
	ogDdx.DataChanged((void *)this, PRC_NEW,(void *)prpPrc ); //Update Viewer
	omData.Add(prpPrc);//Update omData
	omUrnoMap.SetAt((void *)prpPrc->Urno,prpPrc);
    return true;
}

//--DELETE-------------------------------------------------------------------------------------------------

bool CedaPrcData::Delete(long lpUrno)
{
	PRCDATA *prlPrc = GetPrcByUrno(lpUrno);
	if (prlPrc != NULL)
	{
		prlPrc->IsChanged = DATA_DELETED;
		if(Save(prlPrc) == false) return false; //Update Database
		DeleteInternal(prlPrc);
	}
    return true;
}

//--DELETE-INTERNAL----------------------------------------------------------------------------------------

bool CedaPrcData::DeleteInternal(PRCDATA *prpPrc)
{
	ogDdx.DataChanged((void *)this,PRC_DELETE,(void *)prpPrc); //Update Viewer
	omUrnoMap.RemoveKey((void *)prpPrc->Urno);
	int ilPrcCount = omData.GetSize();
	for (int ilLc = 0; ilLc < ilPrcCount; ilLc++)
	{
		if (omData[ilLc].Urno == prpPrc->Urno)
		{
			omData.DeleteAt(ilLc);//Update omData
			break;
		}
	}
    return true;
}

//--UPDATE-------------------------------------------------------------------------------------------------

bool CedaPrcData::Update(PRCDATA *prpPrc)
{
	if (GetPrcByUrno(prpPrc->Urno) != NULL)
	{
		if (prpPrc->IsChanged == DATA_UNCHANGED)
		{
			prpPrc->IsChanged = DATA_CHANGED;
		}
		if(Save(prpPrc) == false) return false; //Update Database
		UpdateInternal(prpPrc);
	}
    return true;
}

//--UPDATE-INTERNAL--------------------------------------------------------------------------------------------

bool CedaPrcData::UpdateInternal(PRCDATA *prpPrc)
{
	PRCDATA *prlPrc = GetPrcByUrno(prpPrc->Urno);
	if (prlPrc != NULL)
	{
		*prlPrc = *prpPrc; //Update omData
		ogDdx.DataChanged((void *)this,PRC_CHANGE,(void *)prlPrc); //Update Viewer
	}
    return true;
}

//--GET-BY-URNO--------------------------------------------------------------------------------------------

PRCDATA *CedaPrcData::GetPrcByUrno(long lpUrno)
{
	PRCDATA  *prlPrc;
	if (omUrnoMap.Lookup((void *)lpUrno,(void *& )prlPrc) == TRUE)
	{
		return prlPrc;
	}
	return NULL;
}

//--READSPECIALDATA-------------------------------------------------------------------------------------

bool CedaPrcData::ReadSpecialData(CCSPtrArray<PRCDATA> *popPrc,char *pspWhere,char *pspFieldList,bool ipSYS/*=true*/)
{
	bool ilRc = true;
	char pclFieldList[256] = " ";

	if(strlen(pspFieldList) > 0) 
	{
		strcpy(pclFieldList, pspFieldList);
	}
	if(ipSYS == true) 
	{
		if (CedaAction("SYS","PRC",pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	else
	{
		if (CedaAction("RT","PRC",pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	if(popPrc != NULL)
	{
		for (int ilLc = 0; ilRc == true; ilLc++)
		{
			PRCDATA *prpPrc = new PRCDATA;
			if ((ilRc = GetBufferRecord(ilLc,prpPrc,CString(pclFieldList))) == true)
			{
				popPrc->Add(prpPrc);
			}
			else
			{
				delete prpPrc;
			}
		}
		if(popPrc->GetSize() == 0) return false;
	}
    return true;
}

//--SAVE---------------------------------------------------------------------------------------------------

bool CedaPrcData::Save(PRCDATA *prpPrc)
{
	bool ilRc = true;
	CString olListOfData;
	char pclSelection[124];
	char pclData[524];

	if (prpPrc->IsChanged == DATA_UNCHANGED)
	{
		return true; // no change, nothing to do
	}
	switch(prpPrc->IsChanged)
	{
	case DATA_NEW:
		MakeCedaData(&omRecInfo,olListOfData,prpPrc);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("IRT","","",pclData);
		prpPrc->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_CHANGED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpPrc->Urno);
		MakeCedaData(&omRecInfo,olListOfData,prpPrc);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("URT",pclSelection,"",pclData);
		prpPrc->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_DELETED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpPrc->Urno);
		ilRc = CedaAction("DRT",pclSelection);
		break;
	}
    return ilRc;
}

//--PROCESS-CF---------------------------------------------------------------------------------------------

void  ProcessPrcCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	ogPrcData.ProcessBc(ipDDXType,vpDataPointer,ropInstanceName);
}

//--PROCESS-BC---------------------------------------------------------------------------------------------

void  CedaPrcData::ProcessBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	struct BcStruct *prlPrcData;
	prlPrcData = (struct BcStruct *) vpDataPointer;
	PRCDATA *prlPrc;
	if(ipDDXType == BC_PRC_NEW)
	{
		prlPrc = new PRCDATA;
		GetRecordFromItemList(prlPrc,prlPrcData->Fields,prlPrcData->Data);
		InsertInternal(prlPrc);
	}
	if(ipDDXType == BC_PRC_CHANGE)
	{
		long llUrno = GetUrnoFromSelection(prlPrcData->Selection);
		prlPrc = GetPrcByUrno(llUrno);
		if(prlPrc != NULL)
		{
			GetRecordFromItemList(prlPrc,prlPrcData->Fields,prlPrcData->Data);
			UpdateInternal(prlPrc);
		}
	}
	if(ipDDXType == BC_PRC_DELETE)
	{
		long llUrno;
		CString olSelection = (CString)prlPrcData->Selection;
		if (olSelection.Find('\'') != -1)
		{
			llUrno = GetUrnoFromSelection(prlPrcData->Selection);
		}
		else
		{
			int ilFirst = olSelection.Find("=")+2;
			int ilLast  = olSelection.GetLength();
			llUrno = atol(olSelection.Mid(ilFirst,ilLast-ilFirst));
		}
		prlPrc = GetPrcByUrno(llUrno);
		if (prlPrc != NULL)
		{
			DeleteInternal(prlPrc);
		}
	}
}

//---------------------------------------------------------------------------------------------------------
