// CedaTWYData.h

#ifndef __CEDATWYDATA__
#define __CEDATWYDATA__
 
#include "stdafx.h"
#include "basicdata.h"

//---------------------------------------------------------------------------------------------------------
// Record structure declaration

struct TWYDATA 
{
	CTime	 Cdat; 		// Erstellungsdatum
	CTime	 Lstu; 		// Datum letzte �nderung
	CTime	 Nafr; 		// Nicht verf�gbar vom
	CTime	 Nato;		// Nicht verf�gbar bis
	char 	 Prfl[3]; 	// Protokollierungskennzeichen
	char 	 Resn[42]; 	// Grund f�r die Sperrung
	char 	 Rgrw[6]; 	// Verkn�pfte Start-, Landebahn
	char 	 Tnam[7]; 	// Taxiway Bezeichnung
	long 	 Urno; 		// Eindeutige Datensatz-Nr.
	char 	 Usec[34]; 	// Anwender (Ersteller)
	char 	 Useu[34]; 	// Anwender (letzte �nderung)
	CTime	 Vafr; 		// G�ltig von
	CTime	 Vato; 		// G�ltig bis
	char 	 Home[4]; 	// Anwender (letzte �nderung)

	//DataCreated by this class
	int      IsChanged;

	TWYDATA(void)
	{ 
		memset(this,'\0',sizeof(*this));
		Cdat=-1;
		Lstu=-1;
		Nafr=-1;
		Nato=-1;
		Vafr=-1;
		Vato=-1;
	};

}; // end TWYDataStrukt


//---------------------------------------------------------------------------------------------------------
// Class declaratino


class CedaTWYData: public CCSCedaData
{
// Attributes
public:
    CMapPtrToPtr omUrnoMap;

    CCSPtrArray<TWYDATA> omData;

	char pcmTWYFieldList[2048];

// Operations
public:
    CedaTWYData();
	~CedaTWYData();
	void Register(void);
	
	void ClearAll(void);
	virtual void GetDataInfo(CStringArray &ropFields, CStringArray &ropDesription, CStringArray &ropType);

    bool Read(char *pcpWhere = NULL);
	bool ReadSpecialData(CCSPtrArray<TWYDATA> *popTwy,char *pspWhere,char *pspFieldList,bool ipSYS=true);
	bool InsertTWY(TWYDATA *prpTWY,BOOL bpSendDdx = TRUE);
	bool InsertTWYInternal(TWYDATA *prpTWY);
	bool UpdateTWY(TWYDATA *prpTWY,BOOL bpSendDdx = TRUE);
	bool UpdateTWYInternal(TWYDATA *prpTWY);
	bool DeleteTWY(long lpUrno);
	bool DeleteTWYInternal(TWYDATA *prpTWY);
	TWYDATA  *GetTWYByUrno(long lpUrno);
	bool SaveTWY(TWYDATA *prpTWY);
	void ProcessTWYBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

	// Private methods
private:
    void PrepareTWYData(TWYDATA *prpTWYData);
};

//---------------------------------------------------------------------------------------------------------

#endif //__CEDATWYDATA__
