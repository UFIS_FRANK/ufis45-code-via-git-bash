// CedaSTYData.cpp
 
#include "stdafx.h"
#include "CedaSTYData.h"
#include "resource.h"


// Local function prototype
static void ProcessSTYCf(void *popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

//--CEDADATA-----------------------------------------------------------------------------------------------

CedaSTYData::CedaSTYData() : CCSCedaData(&ogCommHandler)
{
    // Create an array of CEDARECINFO for STYDATA
	BEGIN_CEDARECINFO(STYDATA,STYDataRecInfo)
		FIELD_DATE		(Cdat,"CDAT")
		FIELD_DATE		(Lstu,"LSTU")
		FIELD_CHAR_TRIM	(Prfl,"PRFL")
		FIELD_CHAR_TRIM	(Snam,"SNAM")
		FIELD_CHAR_TRIM	(Styp,"STYP")
		FIELD_LONG		(Urno,"URNO")
		FIELD_CHAR_TRIM	(Usec,"USEC")
		FIELD_CHAR_TRIM	(Useu,"USEU")
		FIELD_DATE		(Vafr,"VAFR")
		FIELD_DATE		(Vato,"VATO")
	END_CEDARECINFO //(STYDATA)


	// Copy the record structure
	for (int i=0; i< sizeof(STYDataRecInfo)/sizeof(STYDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&STYDataRecInfo[i],sizeof(CEDARECINFO));
		omRecInfo.Add(prpCedaRecInfo);
	} // end for

	// initialize field names
	strcpy(pcmTableName,"STY");
	strcpy(pcmSTYFieldList,"CDAT,LSTU,PRFL,SNAM,STYP,URNO,USEC,USEU,VAFR,VATO");
	pcmFieldList = pcmSTYFieldList;

	omData.SetSize(0,1000);
	//omUrnoMap.InitHashTable(2001);
}

//-------------------------------------------------------------------------------------------------------

void CedaSTYData::GetDataInfo(CStringArray &ropFields, CStringArray &ropDesription, CStringArray &ropType)
{
	ropFields.Add("CDAT");
	ropFields.Add("LSTU");
	ropFields.Add("PRFL");
	ropFields.Add("SNAM");
	ropFields.Add("STYP");
	ropFields.Add("URNO");
	ropFields.Add("USEC");
	ropFields.Add("USEU");
	ropFields.Add("VAFR");
	ropFields.Add("VATO");

	ropDesription.Add(LoadStg(IDS_STRING343));
	ropDesription.Add(LoadStg(IDS_STRING344));
	ropDesription.Add(LoadStg(IDS_STRING345));
	ropDesription.Add(LoadStg(IDS_STRING448));
	ropDesription.Add(LoadStg(IDS_STRING15));
	ropDesription.Add(LoadStg(IDS_STRING346));
	ropDesription.Add(LoadStg(IDS_STRING347));
	ropDesription.Add(LoadStg(IDS_STRING348));
	ropDesription.Add(LoadStg(IDS_STRING230));
	ropDesription.Add(LoadStg(IDS_STRING231));

	ropType.Add("Date");
	ropType.Add("Date");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("Date");
	ropType.Add("Date");

}

//-------------------------------------------------------------------------------------------------------

void CedaSTYData::Register(void)
{
	ogDdx.Register((void *)this,BC_STY_CHANGE,CString("STYDATA"), CString("STY-changed"),ProcessSTYCf);
	ogDdx.Register((void *)this,BC_STY_DELETE,CString("STYDATA"), CString("STY-deleted"),ProcessSTYCf);
}

//--~CEDADATA----------------------------------------------------------------------------------------------

CedaSTYData::~CedaSTYData(void)
{
	omRecInfo.DeleteAll();
	ClearAll();
}

//--CLEAR-ALL----------------------------------------------------------------------------------------------

void CedaSTYData::ClearAll(void)
{
    omUrnoMap.RemoveAll();
    omData.DeleteAll();
	ogDdx.UnRegister(this,NOTUSED);
}

//--READ-ALL-----------------------------------------------------------------------------------------------

bool CedaSTYData::Read(char *pcpWhere)
{
    // Select data from the database
	bool ilRc = true;
    omUrnoMap.RemoveAll();
    omData.DeleteAll();
	if(pcpWhere != NULL)
		ilRc = CedaAction(ogBasicData.GetCedaCommand("RT"), pcpWhere);
	else
		ilRc = CedaAction(ogBasicData.GetCedaCommand("RT"));
	if (ilRc != true)
	{
		return false;
	}
	// Load data from CedaData into the dynamic array of record
	for (int ilLc = 0; ilRc == true; ilLc++)
	{
		STYDATA *prpSTY = new STYDATA;
		if ((ilRc = GetFirstBufferRecord(prpSTY)) == true)
		{
			prpSTY->IsChanged = DATA_UNCHANGED;
			omData.Add(prpSTY);//Update omData
			omUrnoMap.SetAt((void *)prpSTY->Urno,prpSTY);
		}
		else
		{
			delete prpSTY;
		}
	}
    return true;
}

//--INSERT-------------------------------------------------------------------------------------------------

bool CedaSTYData::InsertSTY(STYDATA *prpSTY,BOOL bpSendDdx)
{
	prpSTY->IsChanged = DATA_NEW;
	if(SaveSTY(prpSTY) == false) return false; //Update Database
	InsertSTYInternal(prpSTY);
    return true;
}

//--INSERT-INTERNAL-------------------------------------------------------------------------------------------

bool CedaSTYData::InsertSTYInternal(STYDATA *prpSTY)
{
	//PrepareSTYData(prpSTY);
	ogDdx.DataChanged((void *)this, STY_CHANGE,(void *)prpSTY ); //Update Viewer
	omData.Add(prpSTY);//Update omData
	omUrnoMap.SetAt((void *)prpSTY->Urno,prpSTY);
    return true;
}

//--DELETE-------------------------------------------------------------------------------------------------

bool CedaSTYData::DeleteSTY(long lpUrno)
{
	STYDATA *prlSTY = GetSTYByUrno(lpUrno);
	if (prlSTY != NULL)
	{
		prlSTY->IsChanged = DATA_DELETED;
		if(SaveSTY(prlSTY) == false) return false; //Update Database
		DeleteSTYInternal(prlSTY);
	}
    return true;
}

//--DELETE-INTERNAL----------------------------------------------------------------------------------------

bool CedaSTYData::DeleteSTYInternal(STYDATA *prpSTY)
{
	ogDdx.DataChanged((void *)this,STY_DELETE,(void *)prpSTY); //Update Viewer
	omUrnoMap.RemoveKey((void *)prpSTY->Urno);
	int ilSTYCount = omData.GetSize();
	for (int ilLc = 0; ilLc < ilSTYCount; ilLc++)
	{
		if (omData[ilLc].Urno == prpSTY->Urno)
		{
			omData.DeleteAt(ilLc);//Update omData
			break;
		}
	}
    return true;
}

//--PREPARE-DATA-------------------------------------------------------------------------------------------

void CedaSTYData::PrepareSTYData(STYDATA *prpSTY)
{
	// TODO: add code here
}

//--UPDATE-----------------------------------------------------------------------------------------------

bool CedaSTYData::UpdateSTY(STYDATA *prpSTY,BOOL bpSendDdx)
{
	if (GetSTYByUrno(prpSTY->Urno) != NULL)
	{
		if (prpSTY->IsChanged == DATA_UNCHANGED)
		{
			prpSTY->IsChanged = DATA_CHANGED;
		}
		if(SaveSTY(prpSTY) == false) return false; //Update Database
		UpdateSTYInternal(prpSTY);
	}
    return true;
}

//--UPDATE-INTERNAL-----------------------------------------------------------------------------------------

bool CedaSTYData::UpdateSTYInternal(STYDATA *prpSTY)
{
	STYDATA *prlSTY = GetSTYByUrno(prpSTY->Urno);
	if (prlSTY != NULL)
	{
		*prlSTY = *prpSTY; //Update omData
		ogDdx.DataChanged((void *)this,STY_CHANGE,(void *)prlSTY); //Update Viewer
	}
    return true;
}

//--GET-BY-URNO--------------------------------------------------------------------------------------------

STYDATA *CedaSTYData::GetSTYByUrno(long lpUrno)
{
	STYDATA  *prlSTY;
	if (omUrnoMap.Lookup((void *)lpUrno,(void *& )prlSTY) == TRUE)
	{
		return prlSTY;
	}
	return NULL;
	
}

//--READSPECIALDATA-------------------------------------------------------------------------------------

bool CedaSTYData::ReadSpecialData(CCSPtrArray<STYDATA> *popSty,char *pspWhere,char *pspFieldList,bool ipSYS/*=true*/)
{
	bool ilRc = true;
	char pclFieldList[256] = " ";

	if(strlen(pspFieldList) > 0) 
	{
		strcpy(pclFieldList, pspFieldList);
	}
	if(ipSYS == true) 
	{
		if (CedaAction("SYS","STY",pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	else
	{
		if (CedaAction("RT","STY",pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	if(popSty != NULL)
	{
		for (int ilLc = 0; ilRc == true; ilLc++)
		{
			STYDATA *prpSty = new STYDATA;
			if ((ilRc = GetBufferRecord(ilLc,prpSty,CString(pclFieldList))) == true)
			{
				popSty->Add(prpSty);
			}
			else
			{
				delete prpSty;
			}
		}
		if(popSty->GetSize() == 0) return false;
	}
    return true;
}

//--SAVE---------------------------------------------------------------------------------------------------

bool CedaSTYData::SaveSTY(STYDATA *prpSTY)
{
	bool ilRc = true;
	CString olListOfData;
	char pclSelection[124];
	char pclData[524];

	if (prpSTY->IsChanged == DATA_UNCHANGED)
	{
		return true; // no change, nothing to do
	}

	switch(prpSTY->IsChanged)
	{
	case DATA_NEW:
		MakeCedaData(&omRecInfo,olListOfData,prpSTY);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("IRT","","",pclData);
		prpSTY->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_CHANGED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpSTY->Urno);
		MakeCedaData(&omRecInfo,olListOfData,prpSTY);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("URT",pclSelection,"",pclData);
		prpSTY->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_DELETED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpSTY->Urno);
		ilRc = CedaAction("DRT",pclSelection);
		break;
	}

    return ilRc;
}

//--PROCESS-CF---------------------------------------------------------------------------------------------

void  ProcessSTYCf(void * popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	switch(ipDDXType)
	{
	case BC_STY_CHANGE :
	case BC_STY_DELETE :
		((CedaSTYData *)popInstance)->ProcessSTYBc(ipDDXType,vpDataPointer,ropInstanceName);
		break;
	}
}

//--PROCESS-BC---------------------------------------------------------------------------------------------

void  CedaSTYData::ProcessSTYBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	struct BcStruct *prlSTYData;
	long llUrno;
	prlSTYData = (struct BcStruct *) vpDataPointer;
	CString olSelection = (CString)prlSTYData->Selection;
	if (olSelection.Find('\'') != -1)
	{
		llUrno = GetUrnoFromSelection(prlSTYData->Selection);
	}
	else
	{
		int ilFirst = olSelection.Find("=")+2;
		int ilLast  = olSelection.GetLength();
		llUrno = atol(olSelection.Mid(ilFirst,ilLast-ilFirst));
	}

	STYDATA *prlSTY;
	prlSTY = GetSTYByUrno(llUrno);
	if(ipDDXType == BC_STY_CHANGE)
	{
		if (prlSTY != NULL)
		{
			GetRecordFromItemList(prlSTY,prlSTYData->Fields,prlSTYData->Data);
			UpdateSTYInternal(prlSTY);
		}
		else
		{
			prlSTY = new STYDATA;
			GetRecordFromItemList(prlSTY,prlSTYData->Fields,prlSTYData->Data);
			InsertSTYInternal(prlSTY);
		}
	}
	if(ipDDXType == BC_STY_DELETE)
	{
		if (prlSTY != NULL)
		{
			DeleteSTYInternal(prlSTY);
		}
	}
}

//---------------------------------------------------------------------------------------------------------
