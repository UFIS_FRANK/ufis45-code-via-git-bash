#if !defined(AFX_LFZREGIEXT_H__BE6D1CB1_4EFF_11D3_9812_00001C0411B3__INCLUDED_)
#define AFX_LFZREGIEXT_H__BE6D1CB1_4EFF_11D3_9812_00001C0411B3__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// LFZRegiExt.h : header file
//
#include "CCSEdit.h"

/////////////////////////////////////////////////////////////////////////////
// CLFZRegiExt dialog

class CLFZRegiExt : public CDialog
{
// Construction
public:
	CLFZRegiExt(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CLFZRegiExt)
	enum { IDD = IDD_LFZREGISTRATIONDLG_EXT };
	CCSEdit	m_cLadp_t;
	CCSEdit	m_cLadp_d;
	CButton	m_cLsdb;
	CCSEdit	m_cOdat;
	CCSEdit	m_cLcod;
	CCSEdit	m_cLsdf;
	CCSEdit	m_cDeli;
	CCSEdit	m_cIata;
	CCSEdit	m_cCont;
	CCSEdit	m_cIcao;
	CCSEdit	m_cFuse;
	CCSEdit	m_cEnna;
	CCSEdit	m_cGall;
	CCSEdit	m_cPnum;
	CCSEdit	m_cYear;
	CCSEdit	m_cScod;
	CCSEdit	m_cExrg;
	CCSEdit	m_cStxt;
	CCSEdit	m_cEnty;
	CCSEdit	m_cEfis;
	CCSEdit	m_cRest;
	CCSEdit	m_cNois;
	CCSEdit	m_cPowd;
	CCSEdit	m_cConf;
	CCSEdit	m_cFrst;
	CCSEdit	m_cSeri;
	CCSEdit	m_cType;
	CCSEdit	m_cCnam;
	CCSEdit	m_cRegnExt;
	CString	m_RegnExt;
	CString	m_Cnam;
	CString	m_Type;
	CString	m_Seri;
	CString	m_Frst;
	CString	m_Conf;
	CString	m_Powd;
	CString	m_Nois;
	CString	m_Rest;
	CString	m_Efis;
	CString	m_Enty;
	CString	m_Stxt;
	CString	m_Exrg;
	CString	m_Scod;
	CString	m_Year;
	CString	m_Pnum;
	CString	m_Gall;
	CString	m_Enna;
	CString	m_Fuse;
	CString	m_Icao;
	CString	m_Cont;
	CString	m_Iata;
	CString	m_Deli;
	CString	m_Lsdf;
	CString	m_Lcod;
	CString	m_Odat;
	BOOL	m_Lsdb;
	BOOL	m_Lsfb;
	BOOL	m_Lstb;
	BOOL	m_Oope;
	BOOL	m_Oopt;
	BOOL	m_Oord;
	BOOL	m_Opbb;
	BOOL	m_Opbf;
	BOOL	m_Opwb;
	BOOL	m_Wobo;
	BOOL	m_Wfub;
	BOOL	m_Regb;
	BOOL	m_Cvtd;
	BOOL	m_Strd;
	CString	m_Ladp_d;
	CString	m_Ladp_t;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CLFZRegiExt)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CLFZRegiExt)
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_LFZREGIEXT_H__BE6D1CB1_4EFF_11D3_9812_00001C0411B3__INCLUDED_)
