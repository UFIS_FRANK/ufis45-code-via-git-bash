// STGrid.cpp: implementation of the CSTGrid class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
//#include "Grp.h"
#include "STGrid.h"
#include "ccsglobl.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

//*** 10.11.99 SHA ***
//*** SPECIAL FOR STFDIALOG! ***

BEGIN_MESSAGE_MAP(CSTGrid, CGXGridWnd)

	ON_WM_LBUTTONDOWN()

END_MESSAGE_MAP()


CSTGrid::CSTGrid()
{

}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
CSTGrid::~CSTGrid()
{

}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
BOOL CSTGrid::SubClassDlgItem(UINT nID, CWnd *pParent)
{
	CWnd::SubclassDlgItem(nID,pParent);
	return TRUE;
}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
void CSTGrid::OnLButtonDown(UINT nFlags, CPoint point) 
{

    ROWCOL  Row;
	ROWCOL  Col;
	CRowColArray olRows;

	

	//--- Betroffene Zelle ermitten
    HitTest(point, &Row,  &Col, NULL);

	//--- Check f�r StingRay Fehler
	if(Col > GetColCount())
		return;   

	CGXGridWnd ::OnLButtonDown(nFlags, point);
/*
	if ((nFlags & MK_LBUTTON) && (nFlags & MK_SHIFT))
	{
		int ilSelCount = (int)GetSelectedRows( olRows);
		int ilLastSel = -1;
		if(ilSelCount > 0)
		{
			ilLastSel = (int) olRows[ilSelCount-1];
			SelectRange(CGXRange(Row, 0, imTopSelectedRow, GetColCount()), TRUE);
		}
	}
*/
	if(Row > 0)
	{
//		SelectRange(CGXRange(Row, 0, Row, GetColCount()), TRUE);
//		imTopSelectedRow = Row;
	}

	return;
}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
BOOL CSTGrid::OnLButtonClickedRowCol(ROWCOL nRow, ROWCOL nCol, UINT nFlags, CPoint pt)	
{
	//--- Check f�r StingRay Fehler
	if(nCol > GetColCount())
		return FALSE;


	//--- Sortieren je nachdem welcher Header gedr�ckt wurde
	if(nRow == 0 && nCol != 0)
	{
		CGXSortInfoArray  sortInfo;
		sortInfo .SetSize(1);
		
		// switch between sorting in ascending / descending order with each click
		if (bmSortAscend == TRUE)
		{
			sortInfo[0].sortOrder = CGXSortInfo::descending;
			bmSortAscend = FALSE;
		}
		else
		{
			sortInfo[0].sortOrder = CGXSortInfo::ascending;
			bmSortAscend = TRUE;
		}
		
		sortInfo[0].nRC = nCol;                       
		sortInfo[0].sortType = CGXSortInfo::autodetect;  
		SortRows( CGXRange().SetTable(), sortInfo); 

		//--- Merke welche Spalte f�r sorting verwendet wurde
		//imSortKey  = nCol;

		//--- Check ob numerisch sortiert wurde 
		CGXStyle  il_Cell_Style;
		GetStyleRowCol(1, nCol, il_Cell_Style);


		if(il_Cell_Style.GetValueType() ==  GX_VT_NUMERIC )
		  bmSortNumerical = TRUE;
		else
		  bmSortNumerical = FALSE;
	}
	return FALSE;
}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
BOOL CSTGrid::OnGridKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags)
{

	if (nChar==9)
		return FALSE;
	else
	{
		CGXGridCore::OnGridKeyDown(nChar,nRepCnt,nFlags);
		return TRUE;
	}
}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
int CSTGrid::FindRow(CString cValue,int iCol,BOOL bMark, BOOL bSelect)
{
	int ilMaxCols=GetColCount();

	//***LAST COL IMMER URNO
	if (iCol==0) 
		iCol=GetColCount();
	//afxDump << iCol <<"\n";

	//afxDump << "######" << cValue <<"\n";
	//afxDump << GetRowCount() <<"\n";
	for (UINT iRow=1;iRow<GetRowCount();iRow++)
	{
		//afxDump << GetValueRowCol(iRow,iCol) <<"\n";
		if (GetValueRowCol(iRow,iCol)==cValue)
		{
			if (bMark)
				SetStyleRange(CGXRange(iRow,1,iRow,ilMaxCols),CGXStyle().SetInterior(YELLOW));
			else
				SetStyleRange(CGXRange(iRow,1,iRow,ilMaxCols),CGXStyle().SetInterior(WHITE));
		}
	}

	return 0;     
}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
int CSTGrid::ClearMarks()
{ 
	SetStyleRange(CGXRange(1,1,GetRowCount(),GetColCount()),CGXStyle().SetInterior(WHITE));
	return 0;
}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
void CSTGrid::SetGrdID(long iID)
{
	mGrdID=iID;
}


BOOL CSTGrid::OnDeleteCell()
{
	
	return true;
}

BOOL CSTGrid::OnEndEditing(ROWCOL nRow, ROWCOL nCol)
{
	SetValueRange(CGXRange(nRow,8),"UPD");

	if (nCol==2)
	{

		CString olCode=GetValueRowCol(nRow,2);
		int i;
		for(i = 0; i < olHagCodes.GetSize(); i++)
		{
			if (olHagCodes.GetAt(i)==olCode)
			{
				CString cl;
				cl =olHagNames.GetAt(i);
				SetValueRange(CGXRange(nRow,3),olHagNames.GetAt(i));
				SetValueRange(CGXRange(nRow,4),olHagPhones.GetAt(i));
				SetValueRange(CGXRange(nRow,5),olHagFax.GetAt(i));

				break;
			}
	}
 

	}
	return true;
}


BOOL CSTGrid::LButtonUp(UINT nFlags, CPoint point, UINT nHitState)
{

	return true;
}
