// AircraftFamDlg.cpp : implementation file
//

#include "stdafx.h"
#include "bdpsuif.h"
#include "AircraftFamDlg.h"
#include "PrivList.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// AircraftFamDlg dialog


AircraftFamDlg::AircraftFamDlg(AFMDATA *popAfm, CWnd* pParent /*=NULL*/)
	: CDialog(AircraftFamDlg::IDD, pParent)
{
	pomAfm = popAfm;
	//{{AFX_DATA_INIT(AircraftFamDlg)
	//}}AFX_DATA_INIT
}


void AircraftFamDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(AircraftFamDlg)
	DDX_Control(pDX, IDOK, m_OK);
	DDX_Control(pDX, IDC_CDAT_D, m_CDATD);
	DDX_Control(pDX, IDC_CDAT_T, m_CDATT);
	DDX_Control(pDX, IDC_AFMC, m_AFMC);
	DDX_Control(pDX, IDC_ANAM, m_ANAM);
	DDX_Control(pDX, IDC_LSTU_D, m_LSTUD);
	DDX_Control(pDX, IDC_LSTU_T, m_LSTUT);
	DDX_Control(pDX, IDC_USEC, m_USEC);
	DDX_Control(pDX, IDC_USEU, m_USEU);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(AircraftFamDlg, CDialog)
	//{{AFX_MSG_MAP(AircraftFamDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// AircraftFamDlg message handlers
//----------------------------------------------------------------------------------------

BOOL AircraftFamDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

	m_Caption = LoadStg(IDS_STRING618) + " <" + m_Caption + ">";
	SetWindowText(m_Caption);
	
	char clStat = ogPrivList.GetStat("AIRCRAFTFAMDLG.m_OK");
	SetWndStatAll(clStat,m_OK);

	//------------------------------------
	m_CDATD.SetBKColor(SILVER);
	m_CDATD.SetInitText(pomAfm->Cdat.Format("%d.%m.%Y"));
	m_CDATD.SetSecState(ogPrivList.GetStat("AIRCRAFTFAMDLG.m_CDAT"));
	// - - - - - - - - - - - - - - - - - -
	m_CDATT.SetBKColor(SILVER);
	m_CDATT.SetInitText(pomAfm->Cdat.Format("%H:%M"));
	m_CDATT.SetSecState(ogPrivList.GetStat("AIRCRAFTFAMDLG.m_CDAT"));
	//------------------------------------
	m_LSTUD.SetBKColor(SILVER);
	m_LSTUD.SetInitText(pomAfm->Lstu.Format("%d.%m.%Y"));
	m_LSTUD.SetSecState(ogPrivList.GetStat("AIRCRAFTFAMDLG.m_LSTU"));
	// - - - - - - - - - - - - - - - - - -
	m_LSTUT.SetBKColor(SILVER);
	m_LSTUT.SetInitText(pomAfm->Lstu.Format("%H:%M"));
	m_LSTUT.SetSecState(ogPrivList.GetStat("AIRCRAFTFAMDLG.m_LSTU"));
	//------------------------------------
	m_USEC.SetBKColor(SILVER);
	m_USEC.SetInitText(pomAfm->Usec);
	m_USEC.SetSecState(ogPrivList.GetStat("AIRCRAFTFAMDLG.m_USEC"));
	//------------------------------------
	m_USEU.SetBKColor(SILVER);
	m_USEU.SetInitText(pomAfm->Useu);
	m_USEU.SetSecState(ogPrivList.GetStat("AIRCRAFTFAMDLG.m_USEU"));
	//------------------------------------//WHITE,YELLOW,SILVER,RED,LYELLOW
	m_AFMC.SetFormat("x|#x|#x|#x|#x|#");
	m_AFMC.SetTextLimit(1,5);
	m_AFMC.SetBKColor(YELLOW);  
	m_AFMC.SetTextErrColor(RED);
	m_AFMC.SetInitText(pomAfm->Afmc);
	m_AFMC.SetSecState(ogPrivList.GetStat("AIRCRAFTFAMDLG.m_AFMC"));
	//------------------------------------
	m_ANAM.SetTypeToString("X(40)",40,1);
	m_ANAM.SetBKColor(YELLOW);  
	m_ANAM.SetTextErrColor(RED);
	m_ANAM.SetInitText(pomAfm->Anam);
	m_ANAM.SetSecState(ogPrivList.GetStat("AIRCRAFTFAMDLG.m_ANAM"));
	//------------------------------------
	return TRUE;  
}

//----------------------------------------------------------------------------------------

void AircraftFamDlg::OnOK() 
{
	AfxGetApp()->DoWaitCursor(1);
	CString olErrorText;
	bool ilStatus = true;
	if(m_AFMC.GetStatus() == false)
	{
		ilStatus = false;
		if(m_AFMC.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING237) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING237) + ogNotFormat;
		}
	}
	if(m_ANAM.GetStatus() == false)
	{
		ilStatus = false;
		if(m_ANAM.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING229) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING229) + ogNotFormat;
		}
	}
	///////////////////////////////////////////////////////////////////
	//Konsistenz-Check//
	if(ilStatus == true)
	{
		CString olText;//
		if(m_AFMC.GetWindowTextLength() != 0)
		{
			CCSPtrArray<AFMDATA> olAfmCPA;
			char clWhere[100];
			m_AFMC.GetWindowText(olText);
			sprintf(clWhere,"WHERE AFMC='%s'",olText);
			if(ogAFMData.ReadSpecialData(&olAfmCPA,clWhere,"URNO",false) == true)
			{
				if(olAfmCPA[0].Urno != pomAfm->Urno)
				{
					ilStatus = false;
					olErrorText += LoadStg(IDS_STRING237) + LoadStg(IDS_EXIST);
				}
			}
			olAfmCPA.DeleteAll();
		}
	}
	///////////////////////////////////////////////////////////////////
	AfxGetApp()->DoWaitCursor(-1);
	if (ilStatus == true)
	{
		m_AFMC.GetWindowText(pomAfm->Afmc,6);
		m_ANAM.GetWindowText(pomAfm->Anam,40);
		//wandelt Return in Blank//
		////////////////////////////

		CDialog::OnOK();
	}
	else
	{
		Beep(440,70);
		MessageBox(olErrorText,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
		m_AFMC.SetFocus();
	}	

	
}

//----------------------------------------------------------------------------------------
