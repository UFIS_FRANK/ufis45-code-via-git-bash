// Stammdaten.cpp : implementation file
//

#include "stdafx.h"
#include "BDPSUIF.h"
#include "Stammdaten.h"
#include "CCSBcHandle.h"
#include "CCSCedaCom.h"
#include "PrivList.h"
#include "CheckReferenz.h"

#include "InfoDlg.h"
#include "AirlineDlg.h"
#include "AircraftDlg.h"
#include "LFZRegiDlg.h"
#include "AirportDlg.h"
#include "RunwayDlg.h"
#include "TaxiwayDlg.h"
#include "PositionDlg.h"
#include "GateDlg.h"
#include "CheckinCounterDlg.h"
#include "BeggagebeltDlg.h"
#include "ExitDlg.h"
#include "DelaycodeDlg.h"
#include "TelexadressDlg.h"
#include "TraffictypeDlg.h"
#include "HandlingagentDlg.h"
#include "WaitingroomDlg.h"
#include "HandlingtypeDlg.h"
#include "ServicetypeDlg.h"
#include "FidsCommandDlg.h"
#include "ErgVerkehrsartenDlg.h"
#include "ErgAbfertigungsartenDlg.h"
#include "StandardverkettungenDlg.h"
#include "FlugplansaisonDlg.h"
#include "LeistungskatalogDlg.h"
#include "QualifikationenDlg.h"
#include "OrganisationseinheitenDlg.h"
#include "ArbeitsvertragsartenDlg.h"
#include "BewertungsfaktorenDlg.h"
#include "BasisschichtenDlg.h"
#include "FunktionenDlg.h"
#include "FahrgemeinschaftenDlg.h"
#include "ReduktionenDlg.h"
#include "DiensteUndAbwesenheitenDlg.h"
#include "MitarbeiterStammDlg.h"
#include "GeraeteGruppenDlg.h"
#include "WegezeitenDlg.h"
#include "OrganizerDlg.h"
#include "TimeParametersDlg.h"
#include "HolidayDlg.h"
#include "ArbeitsgruppenDlg.h"
#include "PlanungsgruppenDlg.h"
#include "VeryImpPers.h"
#include "ChkInCounterClass.h"
#include "AirPortWet.h"
#include "AircraftFamDlg.h"
#include "EngineTypeDlg.h"

#include "CedaALTData.h"
#include "CedaACTData.h"
#include "CedaACRData.h"
#include "CedaAPTData.h"
#include "CedaRWYData.h"
#include "CedaTWYData.h"
#include "CedaPSTData.h"
#include "CedaGATData.h"
#include "CedaCICData.h"
#include "CedaBLTData.h"
#include "CedaEXTData.h"
#include "CedaDENData.h"
#include "CedaMVTData.h"
#include "CedaNATData.h"
#include "CedaHAGData.h"
#include "CedaWROData.h"
#include "CedaHTYData.h"
#include "CedaSTYData.h"
#include "CedaFIDData.h"
#include "CedaSphData.h"
#include "CedaStrData.h"
#include "CedaSeaData.h"
#include "CedaGhsData.h"
#include "CedaPerData.h"
#include "CedaGrnData.h"
#include "CedaOrgData.h"
#include "CedaPfcData.h"
#include "CedaCotData.h"
#include "CedaAsfData.h"
#include "CedaBsdData.h"
#include "CedaOdaData.h"
#include "CedaTeaData.h"
#include "CedaStfData.h"
#include "CedaWayData.h"
#include "CedaPrcData.h"
#include "CedaSorData.h"
#include "CedaSpfData.h"
#include "CedaSpeData.h"
#include "CedaScoData.h"
#include "CedaSteData.h"
#include "CedaChtData.h"
#include "CedaTipData.h"
#include "CedaBlkData.h"
#include "CedaHolData.h"
#include "CedaWgpData.h"
#include "CedaSwgData.h"
#include "CedaAwiData.h"
#include "CedaCccData.h"
#include "CedaVipData.h"
#include "CedaAFMData.h"
#include "CedaENTData.h"

#include "BDPSPropertySheet.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

int igSelectTab;
bool bgIsDialogOpen;


static int Compare_Menue( const TABLES **e1, const TABLES  **e2);

/////////////////////////////////////////////////////////////////////////////
// CStammdaten dialog
CStammdaten::CStammdaten(CWnd* pParent /*=NULL*/) : CDialog(CStammdaten::IDD, pParent)
{
	//{{AFX_DATA_INIT(CStammdaten)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	pomCurrentViewer = NULL;
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
	pomTable = NULL;
}

CStammdaten::~CStammdaten()
{
	delete pomTable;
	omTables.DeleteAll();
}

void CStammdaten::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CStammdaten)
	DDX_Control(pDX, IDC_ANZEIGE,	m_AnzeigeComboBox);
	DDX_Control(pDX, IDC_EINFUEGEN,	m_EINFUEGEN);
	DDX_Control(pDX, IDC_AENDERN,	m_AENDERN);
	DDX_Control(pDX, IDC_LOESCHEN,	m_LOESCHEN);
	DDX_Control(pDX, IDC_KOPIEREN,	m_KOPIEREN);
	DDX_Control(pDX, IDC_ERSETZEN,	m_ERSETZEN);
	DDX_Control(pDX, IDC_ANSICHT,	m_ANSICHT);
	DDX_Control(pDX, IDC_DRUCKEN,	m_DRUCKEN);
	DDX_Control(pDX, IDC_EXCEL,		m_EXCEL);
	DDX_Control(pDX, IDC_STATUS,	m_Status);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CStammdaten, CDialog)
	//{{AFX_MSG_MAP(CStammdaten)
	ON_BN_CLICKED(IDC_ANSICHT,			ShowAnsicht)
	ON_BN_CLICKED(IDC_AENDERN,			OnAendern)
	ON_CBN_SELCHANGE(IDC_ANZEIGE,		OnSelchangeAnzeige)
	ON_BN_CLICKED(IDC_DRUCKEN,			OnDrucken)
	ON_BN_CLICKED(IDC_EINFUEGEN,		OnEinfuegen)
	ON_BN_CLICKED(IDC_ERSETZEN,			OnErsetzen)
	ON_BN_CLICKED(IDC_KOPIEREN,			OnKopieren)
	ON_BN_CLICKED(IDC_LOESCHEN,			OnLoeschen)
	ON_COMMAND(ID_HELP,					OnHilfe)
	ON_COMMAND(MID_ALLGEMEIN,			OnAllgemein)
	ON_WM_PAINT()
	ON_MESSAGE(WM_TABLE_LBUTTONDBLCLK,	OnTableLButtonDblclk)
	ON_MESSAGE(WM_BCADD,				OnBcAdd)
	ON_COMMAND(MID_DRUCKEN,				OnDrucken)
	ON_COMMAND(MID_INFO,				OnInfo)
	ON_WM_DESTROY()
	ON_WM_QUERYDRAGICON()
	ON_COMMAND_RANGE(20000,20100,		OnChangeMenu)
	ON_WM_SIZE()
	ON_MESSAGE(WM_EVALUATE_CMDLINE,		OnEvaluateCmdLine)
	ON_BN_CLICKED(IDC_EXCEL,			OnExcel)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CStammdaten message handlers


BOOL CStammdaten::OnInitDialog() 
{
	CDialog::OnInitDialog();

	char clStat;
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	omReadErrTxt.LoadString(ST_READERR);
	omInsertErrTxt.LoadString(ST_INSERTERR);
	omUpdateErrTxt.LoadString(ST_UPDATEERR);
	omDeleteErrTxt.LoadString(ST_DELETEERR);


	//--- Set_Window_Text ------------------------------------------------------
	SetWindowText(ogCmdLineStghArray.GetAt(0));
	m_ANSICHT.  SetWindowText(LoadStg(IDS_STRING213));
	m_EINFUEGEN.SetWindowText(LoadStg(IDS_STRING214));
	m_AENDERN.  SetWindowText(LoadStg(IDS_STRING215));
	m_LOESCHEN. SetWindowText(LoadStg(IDS_STRING216));
	m_KOPIEREN. SetWindowText(LoadStg(IDS_STRING217));
	m_ERSETZEN. SetWindowText(LoadStg(IDS_STRING218));
	m_DRUCKEN.  SetWindowText(LoadStg(IDS_STRING219));
	m_EXCEL.	SetWindowText(LoadStg(IDS_STRING889));
	//---end> Set_Window_Text ------------------------------------------------------

	imSelectTabOld = TAB_NO_TAB;
	igSelectTab = TAB_NO_TAB;

	CRect olrectScreen(0, 0, 1024, 768);
    MoveWindow(&olrectScreen, TRUE);
	CRect olrectStammdaten;
	GetWindowRect(&olrectStammdaten);
	ScreenToClient(&olrectStammdaten);

	CRect olrectStatus;
	m_Status.GetWindowRect(&olrectStatus);
	int ilStatusHigh = olrectStatus.bottom-olrectStatus.top;
	olrectStatus = CRect(1,olrectStammdaten.bottom-(ilStatusHigh+3),olrectStammdaten.right-3,olrectStammdaten.bottom-3);
	m_Status.MoveWindow(olrectStatus);

	clStat = ogPrivList.GetStat("m_AnzeigeComboBox");
	SetWndStatAll(clStat,m_AnzeigeComboBox);
	clStat = ogPrivList.GetStat("m_ANSICHT");
	SetWndStatAll(clStat,m_ANSICHT);
	clStat = ogPrivList.GetStat("m_EINFUEGEN");
	SetWndStatAll(clStat,m_EINFUEGEN);
	clStat = ogPrivList.GetStat("m_AENDERN");
	SetWndStatAll(clStat,m_AENDERN);
	clStat = ogPrivList.GetStat("m_LOESCHEN");
	SetWndStatAll(clStat,m_LOESCHEN);
	clStat = ogPrivList.GetStat("m_KOPIEREN");
	SetWndStatAll(clStat,m_KOPIEREN);
	clStat = ogPrivList.GetStat("m_ERSETZEN");
	//SetWndStatAll(clStat,m_ERSETZEN);
	clStat = ogPrivList.GetStat("m_DRUCKEN");
	SetWndStatAll(clStat,m_DRUCKEN);
	clStat = ogPrivList.GetStat("m_EXCEL");
	SetWndStatAll(clStat,m_EXCEL);

	omUIFMenu.LoadMenu(IDR_UIFMENU);
	SetMenu(&omUIFMenu);
	omUIFMenu.InsertMenu( MID_DRUCKEN, MF_SEPARATOR,SEPARATOR);

	//------ SubMenues ----------------------------------------------

	omFPMSMenu.		 CreatePopupMenu();
	omAllgemeinMenu. CreatePopupMenu();

	omUIFMenu.InsertMenu(SEPARATOR,MF_POPUP,(UINT)omAllgemeinMenu.GetSafeHmenu(),LoadStg(IDS_STRING143));
	omUIFMenu.InsertMenu(SEPARATOR,MF_POPUP,(UINT)omFPMSMenu.GetSafeHmenu(),LoadStg(IDS_STRING144));


	omUIFMenu.ModifyMenu(MID_DRUCKEN,	MF_BYCOMMAND, MID_DRUCKEN,	LoadStg(IDS_STRING208));
	omUIFMenu.ModifyMenu(IDCANCEL,		MF_BYCOMMAND, IDCANCEL,		LoadStg(IDS_STRING209));
	omUIFMenu.ModifyMenu(ID_HELP,		MF_BYCOMMAND, ID_HELP,		LoadStg(IDS_STRING210));
	omUIFMenu.ModifyMenu(MID_ALLGEMEIN,	MF_BYCOMMAND, MID_ALLGEMEIN,LoadStg(IDS_STRING211));
	omUIFMenu.ModifyMenu(MID_INFO,		MF_BYCOMMAND, MID_INFO,		LoadStg(IDS_STRING212));
	
	TABLES *polTables;

	///////////////////////////////////////////////////////////////////////////////
	polTables = new TABLES;
	polTables->TableCode	= "HAG";
	polTables->MenueID		= TAB_HANDLINGAGENT;
	polTables->SubMenueID	= SUBMENUE_GENERAL; 
	polTables->TableName	= LoadStg(IDS_STRING160);
	polTables->InitModuName = "Handling agents"; //Always English, do not translate!
	omTables.Add(polTables);
	///////////////////////////////////////////////////////////////////////////////
	polTables = new TABLES;
	polTables->TableCode	= "HTY";
	polTables->MenueID		= TAB_HANDLINGTYPE;
	polTables->SubMenueID	= SUBMENUE_GENERAL; 
	polTables->TableName	= LoadStg(IDS_STRING161);
	polTables->InitModuName = "Handling types"; //Always English, do not translate!
	omTables.Add(polTables);
	///////////////////////////////////////////////////////////////////////////////
	polTables = new TABLES;
	polTables->TableCode	= "COT";
	polTables->MenueID		= TAB_ARBEITSVERTRAGSARTEN;
	polTables->SubMenueID	= SUBMENUE_FPMS; 
	polTables->TableName	= LoadStg(IDS_STRING162);
	polTables->InitModuName = "Contract types"; //Always English, do not translate!
	omTables.Add(polTables);
	///////////////////////////////////////////////////////////////////////////////
	polTables = new TABLES;
	polTables->TableCode	= "EXT";
	polTables->MenueID		= TAB_EXIT;
	polTables->SubMenueID	= SUBMENUE_GENERAL; 
	polTables->TableName	= LoadStg(IDS_STRING163);
	polTables->InitModuName = "Exits"; //Always English, do not translate!
	omTables.Add(polTables);
	///////////////////////////////////////////////////////////////////////////////
	polTables = new TABLES;
	polTables->TableCode	= "BSD";
	polTables->MenueID		= TAB_BASISSCHICHTEN;
	polTables->SubMenueID	= SUBMENUE_FPMS;  
	polTables->TableName	= LoadStg(IDS_STRING164);
	polTables->InitModuName = "Basic shifts"; //Always English, do not translate!
	omTables.Add(polTables);
	///////////////////////////////////////////////////////////////////////////////
	polTables = new TABLES;
	polTables->TableCode	= "ASF";
	polTables->MenueID		= TAB_BEWERTUNGSFAKTOREN;
	polTables->SubMenueID	= SUBMENUE_FPMS;  
	polTables->TableName	= LoadStg(IDS_STRING165);
	polTables->InitModuName = "Evaluation factors"; //Always English, do not translate!
	omTables.Add(polTables);
	///////////////////////////////////////////////////////////////////////////////
	polTables = new TABLES;
	polTables->TableCode	= "CIC";
	polTables->MenueID		= TAB_CHECKINCOUNTER;
	polTables->SubMenueID	= SUBMENUE_GENERAL; 
	polTables->TableName	= LoadStg(IDS_STRING166);
	polTables->InitModuName = "Check-in counter"; //Always English, do not translate!
	omTables.Add(polTables);
	///////////////////////////////////////////////////////////////////////////////
	polTables = new TABLES;
	polTables->TableCode	= "DEN";
	polTables->MenueID		= TAB_DELAYCODE;
	polTables->SubMenueID	= SUBMENUE_GENERAL; 
	polTables->TableName	= LoadStg(IDS_STRING167);
	polTables->InitModuName = "Delay codes"; //Always English, do not translate!
	omTables.Add(polTables);
	///////////////////////////////////////////////////////////////////////////////
	polTables = new TABLES;
	polTables->TableCode	= "ODA";
	polTables->MenueID		= TAB_DIENSTEUNDABWESENHEITEN;
	polTables->SubMenueID	= SUBMENUE_FPMS;  
	polTables->TableName	= LoadStg(IDS_STRING168);
	polTables->InitModuName = "Shifts & Absences"; //Always English, do not translate!
	omTables.Add(polTables);
	///////////////////////////////////////////////////////////////////////////////
	polTables = new TABLES;
	polTables->TableCode	= "SPH";
	polTables->MenueID		= TAB_ERGABFERTIGUNGSARTEN;
	polTables->SubMenueID	= SUBMENUE_FPMS;  
	polTables->TableName	= LoadStg(IDS_STRING169);
	polTables->InitModuName = "Extension handling types"; //Always English, do not translate!
	omTables.Add(polTables);
	///////////////////////////////////////////////////////////////////////////////
	polTables = new TABLES;
	polTables->TableCode	= "SPH";
	polTables->MenueID		= TAB_ERGVERKEHRSARTEN;
	polTables->SubMenueID	= SUBMENUE_FPMS;  
	polTables->TableName	= LoadStg(IDS_STRING170);
	polTables->InitModuName = "Extension natures"; //Always English, do not translate!
	omTables.Add(polTables);
	///////////////////////////////////////////////////////////////////////////////
	polTables = new TABLES;
	polTables->TableCode	= "TEA";
	polTables->MenueID		= TAB_FAHRGEMEINSCHAFTEN;
	polTables->SubMenueID	= SUBMENUE_FPMS;  
	polTables->TableName	= LoadStg(IDS_STRING171);
	polTables->InitModuName = "Car pooling"; //Always English, do not translate!
	omTables.Add(polTables);
	///////////////////////////////////////////////////////////////////////////////
	polTables = new TABLES;
	polTables->TableCode	= "FID";
	polTables->MenueID		= TAB_FIDSCOMMAND;
	polTables->SubMenueID	= SUBMENUE_GENERAL; 
	polTables->TableName	= LoadStg(IDS_STRING172);
	polTables->InitModuName = "FIDS comments"; //Always English, do not translate!
	omTables.Add(polTables);
	///////////////////////////////////////////////////////////////////////////////
	polTables = new TABLES;
	polTables->TableCode	= "ALT";
	polTables->MenueID		= TAB_AIRLINE;
	polTables->SubMenueID	= SUBMENUE_GENERAL; 
	polTables->TableName	= LoadStg(IDS_STRING173);
	polTables->InitModuName = "Airlines"; //Always English, do not translate!
	omTables.Add(polTables);
	///////////////////////////////////////////////////////////////////////////////
	polTables = new TABLES;
	polTables->TableCode	= "APT";
	polTables->MenueID		= TAB_AIRPORT;
	polTables->SubMenueID	= SUBMENUE_GENERAL; 
	polTables->TableName	= LoadStg(IDS_STRING174);
	polTables->InitModuName = "Airports"; //Always English, do not translate!
	omTables.Add(polTables);
	///////////////////////////////////////////////////////////////////////////////
	polTables = new TABLES;
	polTables->TableCode	= "SEA";
	polTables->MenueID		= TAB_FLUGPLANSAISON;
	polTables->SubMenueID	= SUBMENUE_FPMS;  
	polTables->TableName	= LoadStg(IDS_STRING175);
	polTables->InitModuName = "Flight schedule season"; //Always English, do not translate!
	omTables.Add(polTables);
	///////////////////////////////////////////////////////////////////////////////
	polTables = new TABLES;
	polTables->TableCode	= "ACT";
	polTables->MenueID		= TAB_AIRCRAFT;
	polTables->SubMenueID	= SUBMENUE_GENERAL; 
	polTables->TableName	= LoadStg(IDS_STRING176);
	polTables->InitModuName = "Aircraft types"; //Always English, do not translate!
	omTables.Add(polTables);
	///////////////////////////////////////////////////////////////////////////////
	polTables = new TABLES;
	polTables->TableCode	= "PFC";
	polTables->MenueID		= TAB_FUNKTIONEN;
	polTables->SubMenueID	= SUBMENUE_FPMS;  
	polTables->TableName	= LoadStg(IDS_STRING177);
	polTables->InitModuName = "Functions"; //Always English, do not translate!
	omTables.Add(polTables);
	///////////////////////////////////////////////////////////////////////////////
	polTables = new TABLES;
	polTables->TableCode	= "GAT";
	polTables->MenueID		= TAB_GATE;
	polTables->SubMenueID	= SUBMENUE_GENERAL; 
	polTables->TableName	= LoadStg(IDS_STRING178);
	polTables->InitModuName = "Gates"; //Always English, do not translate!
	omTables.Add(polTables);
	///////////////////////////////////////////////////////////////////////////////
	polTables = new TABLES;
	polTables->TableCode	= "BLT";
	polTables->MenueID		= TAB_BEGGAGEBELT;
	polTables->SubMenueID	= SUBMENUE_GENERAL; 
	polTables->TableName	= LoadStg(IDS_STRING179);
	polTables->InitModuName = "Baggage belts (arrival)"; //Always English, do not translate!
	omTables.Add(polTables);
	///////////////////////////////////////////////////////////////////////////////
	polTables = new TABLES;
	polTables->TableCode	= "GEG";
	polTables->MenueID		= TAB_GERAETEGRUPPEN;
	polTables->SubMenueID	= SUBMENUE_FPMS;  
	polTables->TableName	= LoadStg(IDS_STRING180);
	polTables->InitModuName = "Equipment groups"; //Always English, do not translate!
	omTables.Add(polTables);
	///////////////////////////////////////////////////////////////////////////////
	polTables = new TABLES;
	polTables->TableCode	= "GHS";
	polTables->MenueID		= TAB_LEISTUNGSKATALOG;
	polTables->SubMenueID	= SUBMENUE_FPMS;  
	polTables->TableName	= LoadStg(IDS_STRING181);
	polTables->InitModuName = "Service catalog"; //Always English, do not translate!
	omTables.Add(polTables);
	///////////////////////////////////////////////////////////////////////////////
	polTables = new TABLES;
	polTables->TableCode	= "ACR";
	polTables->MenueID		= TAB_LFZREGISTRATION;
	polTables->SubMenueID	= SUBMENUE_GENERAL; 
	polTables->TableName	= LoadStg(IDS_STRING182);
	polTables->InitModuName = "Registrations"; //Always English, do not translate!
	omTables.Add(polTables);
	///////////////////////////////////////////////////////////////////////////////
	polTables = new TABLES;
	polTables->TableCode	= "STF";
	polTables->MenueID		= TAB_MITARBEITERSTAMM;
	polTables->SubMenueID	= SUBMENUE_FPMS;  
	polTables->TableName	= LoadStg(IDS_STRING183);
	polTables->InitModuName = "Employee base"; //Always English, do not translate!
	omTables.Add(polTables);
	///////////////////////////////////////////////////////////////////////////////
	polTables = new TABLES;
	polTables->TableCode	= "ORG";
	polTables->MenueID		= TAB_ORGANISATIONSEINHEITEN;
	polTables->SubMenueID	= SUBMENUE_FPMS;  
	polTables->TableName	= LoadStg(IDS_STRING184);
	polTables->InitModuName = "Organisational units"; //Always English, do not translate!
	omTables.Add(polTables);
	///////////////////////////////////////////////////////////////////////////////
	polTables = new TABLES;
	polTables->TableCode	= "PST";
	polTables->MenueID		= TAB_POSITION;
	polTables->SubMenueID	= SUBMENUE_GENERAL; 
	polTables->TableName	= LoadStg(IDS_STRING185);
	polTables->InitModuName = "Positions"; //Always English, do not translate!
	omTables.Add(polTables);
	///////////////////////////////////////////////////////////////////////////////
	polTables = new TABLES;
	polTables->TableCode	= "PER";
	polTables->MenueID		= TAB_QUALIFIKATIONEN;
	polTables->SubMenueID	= SUBMENUE_FPMS;  
	polTables->TableName	= LoadStg(IDS_STRING186);
	polTables->InitModuName = "Qualifications"; //Always English, do not translate!
	omTables.Add(polTables);
	///////////////////////////////////////////////////////////////////////////////
	polTables = new TABLES;
	polTables->TableCode	= "PRC";
	polTables->MenueID		= TAB_REDUKTIONEN;
	polTables->SubMenueID	= SUBMENUE_FPMS;  
	polTables->TableName	= LoadStg(IDS_STRING187);
	polTables->InitModuName = "Reductions"; //Always English, do not translate!
	omTables.Add(polTables);
	///////////////////////////////////////////////////////////////////////////////
	polTables = new TABLES;
	polTables->TableCode	= "STY";
	polTables->MenueID		= TAB_SERVICETYPE;
	polTables->SubMenueID	= SUBMENUE_GENERAL; 
	polTables->TableName	= LoadStg(IDS_STRING188);
	polTables->InitModuName = "Service types"; //Always English, do not translate!
	omTables.Add(polTables);
	///////////////////////////////////////////////////////////////////////////////
/**************
	polTables = new TABLES;
	polTables->TableCode	= "STR";
	polTables->MenueID		= TAB_STANDARDVERKETTUNGEN;
	polTables->SubMenueID	= SUBMENUE_FPMS;  
	polTables->TableName	= LoadStg(IDS_STRING189);
	polTables->InitModuName = "Standard links"; //Always English, do not translate!
	omTables.Add(polTables);
*************/
	///////////////////////////////////////////////////////////////////////////////
	polTables = new TABLES;
	polTables->TableCode	= "RWY";
	polTables->MenueID		= TAB_RUNWAY;
	polTables->SubMenueID	= SUBMENUE_GENERAL; 
	polTables->TableName	= LoadStg(IDS_STRING190);
	polTables->InitModuName = "Runways"; //Always English, do not translate!
	omTables.Add(polTables);
	///////////////////////////////////////////////////////////////////////////////
	polTables = new TABLES;
	polTables->TableCode	= "TWY";
	polTables->MenueID		= TAB_TAXIWAY;
	polTables->SubMenueID	= SUBMENUE_GENERAL; 
	polTables->TableName	= LoadStg(IDS_STRING191);
	polTables->InitModuName = "Taxiways"; //Always English, do not translate!
	omTables.Add(polTables);
	///////////////////////////////////////////////////////////////////////////////
	polTables = new TABLES;
	polTables->TableCode	= "MVT";
	polTables->MenueID		= TAB_TELEXADRESS;
	polTables->SubMenueID	= SUBMENUE_GENERAL;
	polTables->TableName	= LoadStg(IDS_STRING192);
	polTables->InitModuName = "Telex addresses"; //Always English, do not translate!
	omTables.Add(polTables);
	///////////////////////////////////////////////////////////////////////////////
	polTables = new TABLES;
	polTables->TableCode	= "CHT";
	polTables->MenueID		= TAB_ORGANIZER;
	polTables->SubMenueID	= SUBMENUE_FPMS;  
	polTables->TableName	= LoadStg(IDS_STRING193);
	polTables->InitModuName = "Organizer"; //Always English, do not translate!
	omTables.Add(polTables);
	///////////////////////////////////////////////////////////////////////////////
	polTables = new TABLES;
	polTables->TableCode	= "NAT";
	polTables->MenueID		= TAB_TRAFFICTYPE;
	polTables->SubMenueID	= SUBMENUE_GENERAL; 
	polTables->TableName	= LoadStg(IDS_STRING194);
	polTables->InitModuName = "Natures"; //Always English, do not translate!
	omTables.Add(polTables);
	///////////////////////////////////////////////////////////////////////////////
	polTables = new TABLES;
	polTables->TableCode	= "WRO";
	polTables->MenueID		= TAB_WAITINGROOM;
	polTables->SubMenueID	= SUBMENUE_GENERAL; 
	polTables->TableName	= LoadStg(IDS_STRING195);
	polTables->InitModuName = "Lounges"; //Always English, do not translate!
	omTables.Add(polTables);
	///////////////////////////////////////////////////////////////////////////////
	polTables = new TABLES;
	polTables->TableCode	= "WAY";
	polTables->MenueID		= TAB_WEGEZEITEN;
	polTables->SubMenueID	= SUBMENUE_FPMS;  
	polTables->TableName	= LoadStg(IDS_STRING196);
	polTables->InitModuName = "Route times"; //Always English, do not translate!
	omTables.Add(polTables);
	///////////////////////////////////////////////////////////////////////////////
	polTables = new TABLES;
	polTables->TableCode	= "TIP";
	polTables->MenueID		= TAB_TIMEPARAMETERS;
	polTables->SubMenueID	= SUBMENUE_GENERAL;  
	polTables->TableName	= LoadStg(IDS_STRING197);
	polTables->InitModuName = "Time Parameters"; //Always English, do not translate!
	omTables.Add(polTables);
	///////////////////////////////////////////////////////////////////////////////
	polTables = new TABLES;
	polTables->TableCode	= "HOL";
	polTables->MenueID		= TAB_HOLIDAY;
	polTables->SubMenueID	= SUBMENUE_GENERAL;  
	polTables->TableName	= LoadStg(IDS_STRING198);
	polTables->InitModuName = "Holiday"; //Always English, do not translate!
	omTables.Add(polTables);
	///////////////////////////////////////////////////////////////////////////////
	polTables = new TABLES;
	polTables->TableCode	= "WGP";
	polTables->MenueID		= TAB_ARBEITSGRUPPEN;
	polTables->SubMenueID	= SUBMENUE_FPMS;  
	polTables->TableName	= LoadStg(IDS_STRING199);
	polTables->InitModuName = "Work Groups"; //Always English, do not translate!
	omTables.Add(polTables);
	///////////////////////////////////////////////////////////////////////////////
	polTables = new TABLES;
	polTables->TableCode	= "PGP";
	polTables->MenueID		= TAB_PLANNING_GROUPS;
	polTables->SubMenueID	= SUBMENUE_FPMS;  
	polTables->TableName	= LoadStg(IDS_STRING200);
	polTables->InitModuName = "Planning Groups"; //Always English, do not translate!
	omTables.Add(polTables);
	///////////////////////////////////////////////////////////////////////////////
	polTables = new TABLES;
	polTables->TableCode	= "AWI";
	polTables->MenueID		= TAB_AIRPORTWET;
	polTables->SubMenueID	= SUBMENUE_FPMS;  
	polTables->TableName	= LoadStg(IDS_STRING201);
	polTables->InitModuName = "Airport Weather"; //Always English, do not translate!
	omTables.Add(polTables);
	///////////////////////////////////////////////////////////////////////////////
	polTables = new TABLES;
	polTables->TableCode	= "CCC";
	polTables->MenueID		= TAB_COUNTERCLASS;
	polTables->SubMenueID	= SUBMENUE_FPMS;  
	polTables->TableName	= LoadStg(IDS_STRING202);
	polTables->InitModuName = "Counter Class"; //Always English, do not translate!
	omTables.Add(polTables);
	///////////////////////////////////////////////////////////////////////////////
	polTables = new TABLES;
	polTables->TableCode	= "VIP";
	polTables->MenueID		= TAB_VERYIMPPERS;
	polTables->SubMenueID	= SUBMENUE_FPMS;  
	polTables->TableName	= LoadStg(IDS_STRING203);
	polTables->InitModuName = "Very Important Persons"; //Always English, do not translate!
	omTables.Add(polTables);
	///////////////////////////////////////////////////////////////////////////////
	polTables = new TABLES;
	polTables->TableCode	= "ENT";
	polTables->MenueID		= TAB_ENGINETYPE;
	polTables->SubMenueID	= SUBMENUE_GENERAL;  
	polTables->TableName	= LoadStg(IDS_STRING619);
	polTables->InitModuName = "Engine Type"; //Always English, do not translate!
	omTables.Add(polTables);
	///////////////////////////////////////////////////////////////////////////////
	polTables = new TABLES;
	polTables->TableCode	= "AFM";
	polTables->MenueID		= TAB_AIRCRAFTFAM;
	polTables->SubMenueID	= SUBMENUE_GENERAL;  
	polTables->TableName	= LoadStg(IDS_STRING618);
	polTables->InitModuName = "Aircraft Family"; //Always English, do not translate!
	omTables.Add(polTables);
	///////////////////////////////////////////////////////////////////////////////

	omTables.Sort(Compare_Menue);

	int ilTabSize = omTables.GetSize();
	
	for(int ilTab = 0; ilTab < ilTabSize; ilTab++)
	{
		polTables = &omTables[ilTab];
		if(ogCmdLineStghArray.GetAt(3).Find(omTables[ilTab].TableCode) != -1)
		{
			clStat = ogPrivList.GetStat(omTables[ilTab].InitModuName); 
			if(omTables[ilTab].SubMenueID == SUBMENUE_GENERAL)
			{
				if(clStat != '-') omAllgemeinMenu.InsertMenu(MF_BYPOSITION|-1, MF_STRING,omTables[ilTab].MenueID,omTables[ilTab].TableName);
				if(clStat == '0') omAllgemeinMenu.EnableMenuItem(MF_BYCOMMAND|omTables[ilTab].MenueID,MF_GRAYED);
// TRACE("General Menue '%s' mode %s (%c)\n",omTables[ilTab].TableName,omTables[ilTab].InitModuName,clStat);
			}
			else if(omTables[ilTab].SubMenueID == SUBMENUE_FPMS)
			{
				if(clStat != '-') omFPMSMenu.InsertMenu(MF_BYPOSITION|-1, MF_STRING,omTables[ilTab].MenueID,omTables[ilTab].TableName);
				if(clStat == '0') omFPMSMenu.EnableMenuItem(MF_BYCOMMAND|omTables[ilTab].MenueID,MF_GRAYED);
// TRACE("FPMS Menue '%s' mode %s (%c)\n",omTables[ilTab].TableName,omTables[ilTab].InitModuName,clStat);
			}
			else
			{
				if(clStat != '-') omAllgemeinMenu.InsertMenu(MF_BYPOSITION|-1, MF_STRING,omTables[ilTab].MenueID,omTables[ilTab].TableName);
				if(clStat == '0') omAllgemeinMenu.EnableMenuItem(MF_BYCOMMAND|omTables[ilTab].MenueID,MF_GRAYED);
// TRACE("Other Menue '%s' mode %s (%c)\n",omTables[ilTab].TableName,omTables[ilTab].InitModuName,clStat);
			}
		}
		else
		{
// TRACE("Error, Tab-Shortcut '%s' not found in %s\n",omTables[ilTab].TableCode,ogCmdLineStghArray);
		}
	}

	if(ogPrivList.GetStat("m_DRUCKEN") != '1') omUIFMenu.EnableMenuItem(MF_BYCOMMAND|MID_DRUCKEN,MF_GRAYED);
	if(ogPrivList.GetStat("m_EXCEL") != '1') omUIFMenu.EnableMenuItem(MF_BYCOMMAND|MID_EXCEL,MF_GRAYED);

	if(omAllgemeinMenu.GetMenuItemCount()==0) omUIFMenu.EnableMenuItem(MF_BYCOMMAND|(UINT)omAllgemeinMenu.GetSafeHmenu(),MF_GRAYED);
	if(omFPMSMenu.GetMenuItemCount()==0) omUIFMenu.EnableMenuItem(MF_BYCOMMAND|(UINT)omFPMSMenu.GetSafeHmenu(),MF_GRAYED);

	if(ogPrivList.GetStat("m_AnzeigeComboBox") == '0') m_AnzeigeComboBox.EnableWindow(FALSE);
	//------ end> SubMenues ----------------------------------------------

	ShowWindow(SW_SHOW);
	m_AnzeigeComboBox.SetFocus();
	bgIsDialogOpen = false;	

	ogCommHandler.RegisterBcWindow(this);
	ogBcHandle.GetBc();

	pomTable = new CCSTable;
	pomTable->imSelectMode = 0;
	CRect olrectTable;
	GetClientRect(&olrectTable);
	olrectTable.InflateRect(1,1);     // hiding the CTable window border
	pomTable->SetTableData(this, olrectTable.left+2, olrectTable.right-2, olrectTable.top+35, olrectTable.bottom-(ilStatusHigh+3));

	m_resizeHelper.Init(this->m_hWnd);

	// default behaviour is to resize sub windows proportional to the 
	// resize of the parent you can fix horizontal and/or vertical 
	// dimensions for some sub windows by either specifying the 
	// sub window by its hwnd:

	m_resizeHelper.Fix(IDC_ANZEIGE,DlgResizeHelper::kWidth,DlgResizeHelper::kHeight);
	m_resizeHelper.Fix(IDC_EINFUEGEN,DlgResizeHelper::kWidth,DlgResizeHelper::kHeight);
	m_resizeHelper.Fix(IDC_AENDERN,DlgResizeHelper::kWidth,DlgResizeHelper::kHeight);
	m_resizeHelper.Fix(IDC_LOESCHEN,DlgResizeHelper::kWidth,DlgResizeHelper::kHeight);
	m_resizeHelper.Fix(IDC_KOPIEREN,DlgResizeHelper::kWidth,DlgResizeHelper::kHeight);
	m_resizeHelper.Fix(IDC_ERSETZEN,DlgResizeHelper::kWidth,DlgResizeHelper::kHeight);
	m_resizeHelper.Fix(IDC_ANSICHT,DlgResizeHelper::kWidth,DlgResizeHelper::kHeight);
	m_resizeHelper.Fix(IDC_DRUCKEN,DlgResizeHelper::kWidth,DlgResizeHelper::kHeight);
	m_resizeHelper.Fix(IDC_ANSICHT,DlgResizeHelper::kWidth,DlgResizeHelper::kHeight);
	m_resizeHelper.Fix(IDC_STATUS,DlgResizeHelper::kWidth,DlgResizeHelper::kHeight);

	if (ogCmdLineStghArray.GetSize() == 5)
	{
		this->PostMessage(WM_EVALUATE_CMDLINE,0,0L);						
	}

	return TRUE;
}
//------------------------------------------------------------------------------------------------------

void CStammdaten::OnPaint() 
{
	CPaintDC olDC(this); // device context for painting
	CRect olrectStammdaten;
	GetWindowRect(&olrectStammdaten);
	ScreenToClient(&olrectStammdaten);
	CPen olGrayPen;
	olGrayPen.CreatePen(PS_SOLID, 1, COLORREF(GRAY)); 
	olDC.SelectObject(&olGrayPen);
	olDC.MoveTo(0, 0);
	olDC.LineTo(olrectStammdaten.right,0);
	olDC.MoveTo(0, 35);
	olDC.LineTo(olrectStammdaten.right,35);

	//olDC.SelectStockObject(LTGRAY_BRUSH);
	//olDC.SelectStockObject(NULL_BRUSH);
	//olDC.Rectangle(0,30,1018,724);
}
HCURSOR CStammdaten::OnQueryDragIcon()
{
	return (HCURSOR) m_hIcon;
}
void CStammdaten::OnDestroy()
{
	WinHelp(0L, HELP_QUIT);
	CDialog::OnDestroy();
}

//------------------------------------------------------------------------------------------------------

void CStammdaten::OnChangeMenu(UINT nID)
{
	char clStat1,clStat2;
	if(!bgIsDialogOpen)
	{
		imSelectTabOld = igSelectTab;
		igSelectTab = nID;

		for(int i=omTables.GetSize()-1;i>=0;i--)
		{
			if(omTables[i].MenueID == nID)
			{
				omTabTxt = omTables[i].InitModuName;
				omTableName = omTables[i].TableName;
				i = -1;
			}
		}

		if ((imSelectTabOld != igSelectTab) && (igSelectTab != TAB_NO_TAB))
		{
			CString olFunc;
			olFunc = "ANSICHT_" + omTabTxt;
			clStat1 = ogPrivList.GetStat("m_ANSICHT");
			clStat2 = ogPrivList.GetStat(olFunc);
			SetWndStatPrio_1(clStat1,clStat2,m_ANSICHT);
			olFunc = "m_AnzeigeComboBox_" + omTabTxt;
			clStat1 = ogPrivList.GetStat("m_AnzeigeComboBox");
			clStat2 = ogPrivList.GetStat(olFunc);
			SetWndStatPrio_1(clStat1,clStat2,m_AnzeigeComboBox);
			//if(/*m_AnzeigeComboBox.GetCount() == 0 && */m_AnzeigeComboBox.IsWindowVisible()) m_AnzeigeComboBox.EnableWindow(FALSE);
			olFunc = "EINFUEGEN_" + omTabTxt;
			clStat1 = ogPrivList.GetStat("m_EINFUEGEN");
			clStat2 = ogPrivList.GetStat(olFunc);
			SetWndStatPrio_1(clStat1,clStat2,m_EINFUEGEN);
			olFunc = "AENDERN_" + omTabTxt;
			clStat1 = ogPrivList.GetStat("m_AENDERN");
			clStat2 = ogPrivList.GetStat(olFunc);
			SetWndStatPrio_1(clStat1,clStat2,m_AENDERN);
			olFunc = "LOESCHEN_" + omTabTxt;
			clStat1 = ogPrivList.GetStat("m_LOESCHEN");
			clStat2 = ogPrivList.GetStat(olFunc);
			SetWndStatPrio_1(clStat1,clStat2,m_LOESCHEN);
			olFunc = "KOPIEREN_" + omTabTxt;
			clStat1 = ogPrivList.GetStat("m_KOPIEREN");
			clStat2 = ogPrivList.GetStat(olFunc);
			SetWndStatPrio_1(clStat1,clStat2,m_KOPIEREN);
			olFunc = "ERSETZEN_" + omTabTxt;
			clStat1 = ogPrivList.GetStat("m_ERSETZEN");
			clStat2 = ogPrivList.GetStat(olFunc);
			//SetWndStatPrio_1(clStat1,clStat2,m_ERSETZEN);
			olFunc = "DRUCKEN_" + omTabTxt;
			clStat1 = ogPrivList.GetStat("m_DRUCKEN");
			clStat2 = ogPrivList.GetStat(olFunc);
			SetWndStatPrio_1(clStat1,clStat2,m_DRUCKEN);
			if(ogPrivList.GetStat("m_DRUCKEN") != '1'|| ogPrivList.GetStat(olFunc) != '1')
			{
				omUIFMenu.EnableMenuItem(MF_BYCOMMAND|MID_DRUCKEN,MF_GRAYED);
			}
			else
			{
				omUIFMenu.EnableMenuItem(MF_BYCOMMAND|MID_DRUCKEN,MF_ENABLED);
			}

			olFunc = "EXCEL_" + omTabTxt;
			clStat1 = ogPrivList.GetStat("m_EXCEL");
			clStat2 = ogPrivList.GetStat(olFunc);
			SetWndStatPrio_1(clStat1,clStat2,m_EXCEL);
			if(ogPrivList.GetStat("m_EXCEL") != '1'|| ogPrivList.GetStat(olFunc) != '1')
			{
				omUIFMenu.EnableMenuItem(MF_BYCOMMAND|MID_EXCEL,MF_GRAYED);
			}
			else
			{
				omUIFMenu.EnableMenuItem(MF_BYCOMMAND|MID_EXCEL,MF_ENABLED);
			}

		}

		if ((imSelectTabOld != igSelectTab) && (imSelectTabOld == TAB_NO_TAB))
		{
			ChangeViewer();
		}
		else if ((imSelectTabOld != igSelectTab) && (imSelectTabOld != TAB_NO_TAB) && (igSelectTab != TAB_NO_TAB))
		{
			ClearData(imSelectTabOld);
			ChangeViewer();
		}
	}
}

//------------------------------------------------------------------------------------------------------

void CStammdaten::UpdateComboAnsicht()
{
	CStringArray olArray;
	CStringArray olStrArr;
	ogAnsicht = "";
	CComboBox *polCB = (CComboBox *)GetDlgItem(IDC_ANZEIGE);
	polCB->ResetContent();
	pomCurrentViewer->GetViews(olStrArr); 
	for (int ilIndex = 0; ilIndex < olStrArr.GetSize(); ilIndex++)
	{
		polCB->AddString(olStrArr[ilIndex]);
	}
	CString olViewName = pomCurrentViewer->GetViewName();

	ilIndex = polCB->FindString(-1,olViewName);
		
	if (ilIndex != CB_ERR)
	{
		polCB->SetCurSel(ilIndex);
	}
}

//------------------------------------------------------------------------------------------------------

void CStammdaten::OnSelchangeAnzeige() 
{
	if(!bgIsDialogOpen)
	{
		char clText[64];
		m_AnzeigeComboBox.GetLBText(m_AnzeigeComboBox.GetCurSel(), clText);
		ChangeViewTo(clText);
	}
}

//------------------------------------------------------------------------------------------------------

void CStammdaten::ChangeViewTo(char *pspViewName)
{
	CString olTmpWhere;
	CString olWhere;
	CString olOrderBy;

	if(CString(pspViewName) == "") return;
	ogAnsicht = pspViewName;

	AfxGetApp()->DoWaitCursor(1);

	pomCurrentViewer->SelectView(pspViewName);
	pomCurrentViewer->GetBDPSOrderBy(olOrderBy);

	switch(igSelectTab)
	{
		case TAB_AIRLINE:
			{
				if(pomAirlineViewer->GetBDPSWhere(olTmpWhere) == true)
				{
					olWhere = CString(" WHERE ") + olTmpWhere;
				}
				if(!olOrderBy.IsEmpty())
				{
					olWhere += CString(" ORDER BY ") + olOrderBy;
				}
				SetStatusText(0,LoadStg(IDS_STRING173)+LoadStg(ST_READ));
				ogALTData.Register();
				if(ogALTData.Read(olWhere.GetBuffer(0))==false)
				{	
					ogALTData.omLastErrorMessage.MakeLower();
					if(ogALTData.omLastErrorMessage.Find("no data found")==-1)
					{
						omErrorTxt.Format("%s %d\n%s",omReadErrTxt, ogALTData.imLastReturnCode, ogALTData.omLastErrorMessage);
						MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
					}
				}	
				SetStatusText(ST_CREATETABLE);
				pomCurrentViewer->ChangeViewTo(pspViewName);
				SetStatusText();
			}
			break;
		case TAB_AIRCRAFT:
			{
				if(pomAircraftViewer->GetBDPSWhere(olTmpWhere) == true)
				{
					olWhere = CString(" WHERE ") + olTmpWhere;
				}
				if(!olOrderBy.IsEmpty())
				{
					olWhere += CString(" ORDER BY ") + olOrderBy;
				}
				SetStatusText(0,LoadStg(IDS_STRING176)+LoadStg(ST_READ));
				ogACTData.Register();
				if(ogACTData.Read(olWhere.GetBuffer(0))==false)
				{
					ogACTData.omLastErrorMessage.MakeLower();
					if(ogACTData.omLastErrorMessage.Find("no data found")==-1)
					{
						omErrorTxt.Format("%s %d\n%s",omReadErrTxt, ogACTData.imLastReturnCode, ogACTData.omLastErrorMessage);
						MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
					}
				}	
				SetStatusText(ST_CREATETABLE);
				pomCurrentViewer->ChangeViewTo(pspViewName);
				SetStatusText();
			}
			break;
		case TAB_LFZREGISTRATION:
			{
				bool blRead = true;
				if(pomLFZRegiViewer->GetBDPSWhere(olTmpWhere) == true)
				{
					olWhere = CString(" WHERE ") + olTmpWhere; //+ CString("AND UFIS = '1'");
				}
				else
				{
//					olWhere = CString("AND UFIS = '1'");
					if (IDNO == MessageBox(LoadStg(IDS_STRING147),LoadStg(IDS_STRING146),(MB_ICONEXCLAMATION | MB_YESNO | MB_DEFBUTTON2)))
					{
						blRead = false;
					}
				}
				if(blRead == true)
				{
					if(!olOrderBy.IsEmpty())
					{
						olWhere += CString(" ORDER BY ") + olOrderBy;
					}
					SetStatusText(0,LoadStg(IDS_STRING182)+LoadStg(ST_READ));
					ogACRData.Register();
					if(ogACRData.Read(olWhere.GetBuffer(0))==false)
					{
						ogACRData.omLastErrorMessage.MakeLower();
						if(ogACRData.omLastErrorMessage.Find("no data found")==-1)
						{
							omErrorTxt.Format("%s %d\n%s",omReadErrTxt, ogACRData.imLastReturnCode, ogACRData.omLastErrorMessage);
							MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
						}
					}	
					SetStatusText(ST_CREATETABLE);
					pomCurrentViewer->ChangeViewTo(pspViewName);
					SetStatusText();
				}
			}
			break;
		case TAB_AIRPORT:
			{
				if(pomAirportViewer->GetBDPSWhere(olTmpWhere) == true)
				{
					olWhere = CString(" WHERE ") + olTmpWhere;
				}
				if(!olOrderBy.IsEmpty())
				{
					olWhere += CString(" ORDER BY ") + olOrderBy;
				}
				SetStatusText(0,LoadStg(IDS_STRING174)+LoadStg(ST_READ));
				ogAPTData.Register();
				if(ogAPTData.Read(olWhere.GetBuffer(0))==false)
				{
					ogAPTData.omLastErrorMessage.MakeLower();
					if(ogAPTData.omLastErrorMessage.Find("no data found")==-1)
					{
						omErrorTxt.Format("%s %d\n%s",omReadErrTxt, ogAPTData.imLastReturnCode, ogAPTData.omLastErrorMessage);
						MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
					}
				}	
				SetStatusText(ST_CREATETABLE);
				pomCurrentViewer->ChangeViewTo(pspViewName);
				SetStatusText();
			}
			break;
		case TAB_RUNWAY:
			{
				if(pomRunwayViewer->GetBDPSWhere(olTmpWhere) == true)
				{
					olWhere = CString(" WHERE ") + olTmpWhere;
				}
				if(!olOrderBy.IsEmpty())
				{
					olWhere += CString(" ORDER BY ") + olOrderBy;
				}
				SetStatusText(0,LoadStg(IDS_STRING190)+LoadStg(ST_READ));
				ogRWYData.Register();
				if(ogRWYData.Read(olWhere.GetBuffer(0))==false)
				{
					ogRWYData.omLastErrorMessage.MakeLower();
					if(ogRWYData.omLastErrorMessage.Find("no data found")==-1)
					{
						omErrorTxt.Format("%s %d\n%s",omReadErrTxt, ogRWYData.imLastReturnCode, ogRWYData.omLastErrorMessage);
						MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
					}
				}	
				SetStatusText(ST_CREATETABLE);
				pomCurrentViewer->ChangeViewTo(pspViewName);
				SetStatusText();
			}
			break;
		case TAB_TAXIWAY:
			{
				if(pomTaxiwayViewer->GetBDPSWhere(olTmpWhere) == true)
				{
					olWhere = CString(" WHERE ") + olTmpWhere;
				}
				if(!olOrderBy.IsEmpty())
				{
					olWhere += CString(" ORDER BY ") + olOrderBy;
				}
				SetStatusText(0,LoadStg(IDS_STRING191)+LoadStg(ST_READ));
				ogTWYData.Register();
				if(ogTWYData.Read(olWhere.GetBuffer(0))==false)
				{
					ogTWYData.omLastErrorMessage.MakeLower();
					if(ogTWYData.omLastErrorMessage.Find("no data found")==-1)
					{
						omErrorTxt.Format("%s %d\n%s",omReadErrTxt, ogTWYData.imLastReturnCode, ogTWYData.omLastErrorMessage);
						MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
					}
				}	
				SetStatusText(ST_CREATETABLE);
				pomCurrentViewer->ChangeViewTo(pspViewName);
				SetStatusText(); 
			}
			break;
		case TAB_POSITION:
			{
				if(pomPositionViewer->GetBDPSWhere(olTmpWhere) == true)
				{
					olWhere = CString(" WHERE ") + olTmpWhere;
				}
				if(!olOrderBy.IsEmpty())
				{
					olWhere += CString(" ORDER BY ") + olOrderBy;
				}
				SetStatusText(0,LoadStg(IDS_STRING185)+LoadStg(ST_READ));
				ogPSTData.Register();
				if(ogPSTData.Read(olWhere.GetBuffer(0))==false)
				{
					ogPSTData.omLastErrorMessage.MakeLower();
					if(ogPSTData.omLastErrorMessage.Find("no data found")==-1)
					{
						omErrorTxt.Format("%s %d\n%s",omReadErrTxt, ogPSTData.imLastReturnCode, ogPSTData.omLastErrorMessage);
						MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
					}
				}	
				SetStatusText(ST_CREATETABLE);
				pomCurrentViewer->ChangeViewTo(pspViewName);
				SetStatusText(); 
			}
			break;
		case TAB_GATE:
			{
				if(pomGateViewer->GetBDPSWhere(olTmpWhere) == true)
				{
					olWhere = CString(" WHERE ") + olTmpWhere;
				}
				if(!olOrderBy.IsEmpty())
				{
					olWhere += CString(" ORDER BY ") + olOrderBy;
				}
				SetStatusText(0,LoadStg(IDS_STRING178)+LoadStg(ST_READ));
				ogGATData.Register();
				if(ogGATData.Read(olWhere.GetBuffer(0))==false)
				{
					ogGATData.omLastErrorMessage.MakeLower();
					if(ogGATData.omLastErrorMessage.Find("no data found")==-1)
					{
						omErrorTxt.Format("%s %d\n%s",omReadErrTxt, ogGATData.imLastReturnCode, ogGATData.omLastErrorMessage);
						MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
					}
				}	
				SetStatusText(ST_CREATETABLE);
				pomCurrentViewer->ChangeViewTo(pspViewName);
				SetStatusText(); 
			}
			break;
		case TAB_CHECKINCOUNTER:
			{
				if(pomCheckinCounterViewer->GetBDPSWhere(olTmpWhere) == true)
				{
					olWhere = CString(" WHERE ") + olTmpWhere;
				}
				if(!olOrderBy.IsEmpty())
				{
					olWhere += CString(" ORDER BY ") + olOrderBy;
				}
				SetStatusText(0,LoadStg(IDS_STRING166)+LoadStg(ST_READ));
				ogCICData.Register();
				if(ogCICData.Read(olWhere.GetBuffer(0))==false)
				{
					ogCICData.omLastErrorMessage.MakeLower();
					if(ogCICData.omLastErrorMessage.Find("no data found")==-1)
					{
						omErrorTxt.Format("%s %d\n%s",omReadErrTxt, ogCICData.imLastReturnCode, ogCICData.omLastErrorMessage);
						MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
					}
				}	
				SetStatusText(ST_CREATETABLE);
				pomCurrentViewer->ChangeViewTo(pspViewName);
				SetStatusText(); 
			}
			break;
		case TAB_BEGGAGEBELT:
			{
				if(pomBeggagebeltViewer->GetBDPSWhere(olTmpWhere) == true)
				{
					olWhere = CString(" WHERE ") + olTmpWhere;
				}
				if(!olOrderBy.IsEmpty())
				{
					olWhere += CString(" ORDER BY ") + olOrderBy;
				}
				SetStatusText(0,LoadStg(IDS_STRING179)+LoadStg(ST_READ));
				ogBLTData.Register();
				if(ogBLTData.Read(olWhere.GetBuffer(0))==false)
				{
					ogBLTData.omLastErrorMessage.MakeLower();
					if(ogBLTData.omLastErrorMessage.Find("no data found")==-1)
					{
						omErrorTxt.Format("%s %d\n%s",omReadErrTxt, ogBLTData.imLastReturnCode, ogBLTData.omLastErrorMessage);
						MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
					}
				}	
				SetStatusText(ST_CREATETABLE);
				pomCurrentViewer->ChangeViewTo(pspViewName);
				SetStatusText(); 
			}
			break;
		case TAB_EXIT:
			{
				if(pomExitViewer->GetBDPSWhere(olTmpWhere) == true)
				{
					olWhere = CString(" WHERE ") + olTmpWhere;
				}
				if(!olOrderBy.IsEmpty())
				{
					olWhere += CString(" ORDER BY ") + olOrderBy;
				}
				SetStatusText(0,LoadStg(IDS_STRING163)+LoadStg(ST_READ));
				ogEXTData.Register();
				if(ogEXTData.Read(olWhere.GetBuffer(0))==false)
				{
					ogEXTData.omLastErrorMessage.MakeLower();
					if(ogEXTData.omLastErrorMessage.Find("no data found")==-1)
					{
						omErrorTxt.Format("%s %d\n%s",omReadErrTxt, ogEXTData.imLastReturnCode, ogEXTData.omLastErrorMessage);
						MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
					}
				}	
				SetStatusText(ST_CREATETABLE);
				pomCurrentViewer->ChangeViewTo(pspViewName);
				SetStatusText(); 
			}
			break;
		case TAB_DELAYCODE:
			{
				if(pomDelaycodeViewer->GetBDPSWhere(olTmpWhere) == true)
				{
					olWhere = CString(" WHERE ") + olTmpWhere;
				}
				if(!olOrderBy.IsEmpty())
				{
					olWhere += CString(" ORDER BY ") + olOrderBy;
				}
				SetStatusText(0,LoadStg(IDS_STRING167)+LoadStg(ST_READ));
				ogDENData.Register();
				if(ogDENData.Read(olWhere.GetBuffer(0))==false)
				{
					ogDENData.omLastErrorMessage.MakeLower();
					if(ogDENData.omLastErrorMessage.Find("no data found")==-1)
					{
						omErrorTxt.Format("%s %d\n%s",omReadErrTxt, ogDENData.imLastReturnCode, ogDENData.omLastErrorMessage);
						MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
					}
				}	
				SetStatusText(ST_CREATETABLE);
				pomCurrentViewer->ChangeViewTo(pspViewName);
				SetStatusText();
			}
			break;
		case TAB_TELEXADRESS:
			{
				if(pomTelexadressViewer->GetBDPSWhere(olTmpWhere) == true)
				{
					olWhere = CString(" WHERE ") + olTmpWhere;
				}
				if(!olOrderBy.IsEmpty())
				{
					olWhere += CString(" ORDER BY ") + olOrderBy;
				}
				SetStatusText(0,LoadStg(IDS_STRING192)+LoadStg(ST_READ));
				ogMVTData.Register();
				if(ogMVTData.Read(olWhere.GetBuffer(0))==false)
				{
					ogMVTData.omLastErrorMessage.MakeLower();
					if(ogMVTData.omLastErrorMessage.Find("no data found")==-1)
					{
						omErrorTxt.Format("%s %d\n%s",omReadErrTxt, ogMVTData.imLastReturnCode, ogMVTData.omLastErrorMessage);
						MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
					}
				}	
				SetStatusText(ST_CREATETABLE);
				pomCurrentViewer->ChangeViewTo(pspViewName);
				SetStatusText(); 
			}
			break;
		case TAB_TRAFFICTYPE:
			{
				if(pomTraffictypeViewer->GetBDPSWhere(olTmpWhere) == true)
				{
					olWhere = CString(" WHERE ") + olTmpWhere;
				}
				if(!olOrderBy.IsEmpty())
				{
					olWhere += CString(" ORDER BY ") + olOrderBy;
				}
				SetStatusText(0,LoadStg(IDS_STRING194)+LoadStg(ST_READ));
				ogNATData.Register();
				if(ogNATData.Read(olWhere.GetBuffer(0))==false)
				{
					ogNATData.omLastErrorMessage.MakeLower();
					if(ogNATData.omLastErrorMessage.Find("no data found")==-1)
					{
						omErrorTxt.Format("%s %d\n%s",omReadErrTxt, ogNATData.imLastReturnCode, ogNATData.omLastErrorMessage);
						MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
					}
				}	
				SetStatusText(ST_CREATETABLE);
				pomCurrentViewer->ChangeViewTo(pspViewName);
				SetStatusText(); 
			}
			break;
		case TAB_HANDLINGAGENT:
			{
				if(pomHandlingagentViewer->GetBDPSWhere(olTmpWhere) == true)
				{
					olWhere = CString(" WHERE ") + olTmpWhere;
				}
				if(!olOrderBy.IsEmpty())
				{
					olWhere += CString(" ORDER BY ") + olOrderBy;
				}
				SetStatusText(0,LoadStg(IDS_STRING160)+LoadStg(ST_READ));
				ogHAGData.Register();
				if(ogHAGData.Read(olWhere.GetBuffer(0))==false )
				{
					ogHAGData.omLastErrorMessage.MakeLower();
					if(ogHAGData.omLastErrorMessage.Find("no data found")==-1)
					{
						omErrorTxt.Format("%s %d\n%s",omReadErrTxt, ogHAGData.imLastReturnCode, ogHAGData.omLastErrorMessage);
						MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
					}
				}	
				SetStatusText(ST_CREATETABLE);
				pomCurrentViewer->ChangeViewTo(pspViewName);
				SetStatusText();
			}
			break;
		case TAB_WAITINGROOM:
			{
				if(pomWaitingroomViewer->GetBDPSWhere(olTmpWhere) == true)
				{
					olWhere = CString(" WHERE ") + olTmpWhere;
				}
				if(!olOrderBy.IsEmpty())
				{
					olWhere += CString(" ORDER BY ") + olOrderBy;
				}
				SetStatusText(0,LoadStg(IDS_STRING195)+LoadStg(ST_READ));
				ogWROData.Register();
				if(ogWROData.Read(olWhere.GetBuffer(0))==false)
				{
					ogWROData.omLastErrorMessage.MakeLower();
					if(ogWROData.omLastErrorMessage.Find("no data found")==-1)
					{
						omErrorTxt.Format("%s %d\n%s",omReadErrTxt, ogWROData.imLastReturnCode, ogWROData.omLastErrorMessage);
						MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
					}
				}	
				SetStatusText(ST_CREATETABLE);
				pomCurrentViewer->ChangeViewTo(pspViewName);
				SetStatusText(); 
			}
			break;
		case TAB_HANDLINGTYPE:
			{
				if(pomHandlingtypeViewer->GetBDPSWhere(olTmpWhere) == true)
				{
					olWhere = CString(" WHERE ") + olTmpWhere;
				}
				if(!olOrderBy.IsEmpty())
				{
					olWhere += CString(" ORDER BY ") + olOrderBy;
				}
				SetStatusText(0,LoadStg(IDS_STRING161)+LoadStg(ST_READ));
				ogHTYData.Register();
				if(ogHTYData.Read(olWhere.GetBuffer(0))==false)
				{
					ogHTYData.omLastErrorMessage.MakeLower();
					if(ogHTYData.omLastErrorMessage.Find("no data found")==-1)
					{
						omErrorTxt.Format("%s %d\n%s",omReadErrTxt, ogHTYData.imLastReturnCode, ogHTYData.omLastErrorMessage);
						MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
					}
				}	
				SetStatusText(ST_CREATETABLE);
				pomCurrentViewer->ChangeViewTo(pspViewName);
				SetStatusText(); 
			}
			break;
		case TAB_SERVICETYPE:
			{
				if(pomServicetypeViewer->GetBDPSWhere(olTmpWhere) == true)
				{
					olWhere = CString(" WHERE ") + olTmpWhere;
				}
				if(!olOrderBy.IsEmpty())
				{
					olWhere += CString(" ORDER BY ") + olOrderBy;
				}
				SetStatusText(0,LoadStg(IDS_STRING188)+LoadStg(ST_READ));
				ogSTYData.Register();
				if(ogSTYData.Read(olWhere.GetBuffer(0))==false)
				{
					ogSTYData.omLastErrorMessage.MakeLower();
					if(ogSTYData.omLastErrorMessage.Find("no data found")==-1)
					{
						omErrorTxt.Format("%s %d\n%s",omReadErrTxt, ogSTYData.imLastReturnCode, ogSTYData.omLastErrorMessage);
						MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
					}
				}	
				SetStatusText(ST_CREATETABLE);
				pomCurrentViewer->ChangeViewTo(pspViewName);
				SetStatusText(); 
			}
			break;
		case TAB_FIDSCOMMAND:
			{
				if(pomFidsCommandViewer->GetBDPSWhere(olTmpWhere) == true)
				{
					olWhere = CString(" WHERE ") + olTmpWhere;
				}
				if(!olOrderBy.IsEmpty())
				{
					olWhere += CString(" ORDER BY ") + olOrderBy;
				}
				SetStatusText(0,LoadStg(IDS_STRING172)+LoadStg(ST_READ));
				ogFIDData.Register();
				if(ogFIDData.Read(olWhere.GetBuffer(0))==false)
				{
					ogFIDData.omLastErrorMessage.MakeLower();
					if(ogFIDData.omLastErrorMessage.Find("no data found")==-1)
					{
						omErrorTxt.Format("%s %d\n%s",omReadErrTxt, ogFIDData.imLastReturnCode, ogFIDData.omLastErrorMessage);
						MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
					}
				}	
				SetStatusText(ST_CREATETABLE);
				pomCurrentViewer->ChangeViewTo(pspViewName);
				SetStatusText(); 
			}
			break;
		case TAB_ERGVERKEHRSARTEN:
			{
				if(pomErgVerkehrsartenViewer->GetBDPSWhere(olTmpWhere) == true)
				{
					olWhere = CString(" WHERE ") + olTmpWhere + CString(" AND EART='V'");
				}
				else
				{
					olWhere = CString(" WHERE EART='V'");
				}
				if(!olOrderBy.IsEmpty())
				{
					olWhere += CString(" ORDER BY ") + olOrderBy;
				}
				SetStatusText(0,LoadStg(IDS_STRING170)+LoadStg(ST_READ));
				ogSphData.Register();
				if(ogSphData.Read(olWhere.GetBuffer(0))==false)
				{
					ogSphData.omLastErrorMessage.MakeLower();
					if(ogSphData.omLastErrorMessage.Find("no data found")==-1)
					{
						omErrorTxt.Format("%s %d\n%s",omReadErrTxt, ogSphData.imLastReturnCode, ogSphData.omLastErrorMessage);
						MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
					}
				}
				SetStatusText(ST_CREATETABLE);
				pomCurrentViewer->ChangeViewTo(pspViewName);
				SetStatusText();
			}
			break;
		case TAB_STANDARDVERKETTUNGEN:
			{
				if(pomStandardverkettungenViewer->GetBDPSWhere(olTmpWhere) == true)
				{
					olWhere = CString(" WHERE ") + olTmpWhere;
				}
				if(!olOrderBy.IsEmpty())
				{
					olWhere += CString(" ORDER BY ") + olOrderBy;
				}
				SetStatusText(0,LoadStg(IDS_STRING189)+LoadStg(ST_READ));
				ogStrData.Register();
				if(ogStrData.Read(olWhere.GetBuffer(0))==false)
				{
					ogStrData.omLastErrorMessage.MakeLower();
					if(ogStrData.omLastErrorMessage.Find("no data found")==-1)
					{
						omErrorTxt.Format("%s %d\n%s",omReadErrTxt, ogStrData.imLastReturnCode, ogStrData.omLastErrorMessage);
						MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
					}
				}
				SetStatusText(ST_CREATETABLE);
				pomCurrentViewer->ChangeViewTo(pspViewName);
				SetStatusText();
			}
			break;
		case TAB_FLUGPLANSAISON:
			{
				if(pomFlugplansaisonViewer->GetBDPSWhere(olTmpWhere) == true)
				{
					olWhere = CString(" WHERE ") + olTmpWhere;
				}
				if(!olOrderBy.IsEmpty())
				{
					olWhere += CString(" ORDER BY ") + olOrderBy;
				}
				SetStatusText(0,LoadStg(IDS_STRING175)+LoadStg(ST_READ));
				ogSeaData.Register();
				if(ogSeaData.Read(olWhere.GetBuffer(0))==false)
				{
					ogSeaData.omLastErrorMessage.MakeLower();
					if(ogSeaData.omLastErrorMessage.Find("no data found")==-1)
					{
						omErrorTxt.Format("%s %d\n%s",omReadErrTxt, ogSeaData.imLastReturnCode, ogSeaData.omLastErrorMessage);
						MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
					}
				}	
				SetStatusText(ST_CREATETABLE);
				pomCurrentViewer->ChangeViewTo(pspViewName);
				SetStatusText();
			}
			break;
		case TAB_LEISTUNGSKATALOG:
			{
				if(pomLeistungskatalogViewer->GetBDPSWhere(olTmpWhere) == true)
				{
					olWhere = CString(" WHERE ") + olTmpWhere;
				}
				if(!olOrderBy.IsEmpty())
				{
					olWhere += CString(" ORDER BY ") + olOrderBy;
				}
				SetStatusText(0,LoadStg(IDS_STRING181)+LoadStg(ST_READ));
				ogGhsData.Register();
				if(ogGhsData.Read(olWhere.GetBuffer(0))==false)
				{
					ogGhsData.omLastErrorMessage.MakeLower();
					if(ogGhsData.omLastErrorMessage.Find("no data found")==-1)
					{
						omErrorTxt.Format("%s %d\n%s",omReadErrTxt, ogGhsData.imLastReturnCode, ogGhsData.omLastErrorMessage);
						MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
					}
				}	
				SetStatusText(ST_CREATETABLE);
				pomCurrentViewer->ChangeViewTo(pspViewName);
				SetStatusText();
			}
			break;
		case TAB_QUALIFIKATIONEN:
			{
				if(pomQualifikationenViewer->GetBDPSWhere(olTmpWhere) == true)
				{
					olWhere = CString(" WHERE ") + olTmpWhere;
				}
				if(!olOrderBy.IsEmpty())
				{
					olWhere += CString(" ORDER BY ") + olOrderBy;
				}
				SetStatusText(0,LoadStg(IDS_STRING186)+LoadStg(ST_READ));
				ogPerData.Register();
				if(ogPerData.Read(olWhere.GetBuffer(0))==false)
				{
					ogPerData.omLastErrorMessage.MakeLower();
					if(ogPerData.omLastErrorMessage.Find("no data found")==-1)
					{
						omErrorTxt.Format("%s %d\n%s",omReadErrTxt, ogPerData.imLastReturnCode, ogPerData.omLastErrorMessage);
						MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
					}
				}	
				SetStatusText(ST_CREATETABLE);
				pomCurrentViewer->ChangeViewTo(pspViewName);
				SetStatusText();
			}
			break;
		case TAB_ORGANISATIONSEINHEITEN:
			{
				if(pomOrganisationseinheitenViewer->GetBDPSWhere(olTmpWhere) == true)
				{
					olWhere = CString(" WHERE ") + olTmpWhere;
				}
				if(!olOrderBy.IsEmpty())
				{
					olWhere += CString(" ORDER BY ") + olOrderBy;
				}
				SetStatusText(0,LoadStg(IDS_STRING184)+LoadStg(ST_READ));
				ogOrgData.Register();
				if(ogOrgData.Read(olWhere.GetBuffer(0))==false)
				{
					ogOrgData.omLastErrorMessage.MakeLower();
					if(ogOrgData.omLastErrorMessage.Find("no data found")==-1)
					{
						omErrorTxt.Format("%s %d\n%s",omReadErrTxt, ogOrgData.imLastReturnCode, ogOrgData.omLastErrorMessage);
						MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
					}
				}	
				SetStatusText(ST_CREATETABLE);
				pomCurrentViewer->ChangeViewTo(pspViewName);
				SetStatusText();
			}
			break;
		case TAB_WEGEZEITEN:				
			{
				if(pomCurrentViewer->GetBDPSWhere(olTmpWhere) == true)
				{
					olWhere = CString(" WHERE ") + olTmpWhere;
				}
				if(!olOrderBy.IsEmpty())
				{
					olWhere += CString(" ORDER BY ") + olOrderBy;
				}
				SetStatusText(0,LoadStg(IDS_STRING196)+LoadStg(ST_READ));
				ogWayData.Register();
				if(ogWayData.Read(olWhere.GetBuffer(0))==false)
				{
					ogWayData.omLastErrorMessage.MakeLower();
					if(ogWayData.omLastErrorMessage.Find("no data found")==-1)
					{
						omErrorTxt.Format("%s %d\n%s",omReadErrTxt, ogWayData.imLastReturnCode, ogWayData.omLastErrorMessage);
						MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
					}
				}	
				SetStatusText(ST_CREATETABLE);
				pomCurrentViewer->ChangeViewTo(pspViewName);
				SetStatusText();
			}
			break;
		case TAB_FUNKTIONEN:				
			{
				if(pomCurrentViewer->GetBDPSWhere(olTmpWhere) == true)
				{
					olWhere = CString(" WHERE ") + olTmpWhere;
				}
				if(!olOrderBy.IsEmpty())
				{
					olWhere += CString(" ORDER BY ") + olOrderBy;
				}
				SetStatusText(0,LoadStg(IDS_STRING177)+LoadStg(ST_READ));
				ogPfcData.Register();
				if(ogPfcData.Read(olWhere.GetBuffer(0))==false)
				{
					ogPfcData.omLastErrorMessage.MakeLower();
					if(ogPfcData.omLastErrorMessage.Find("no data found")==-1)
					{
						omErrorTxt.Format("%s %d\n%s",omReadErrTxt, ogPfcData.imLastReturnCode, ogPfcData.omLastErrorMessage);
						MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
					}
				}	
				SetStatusText(ST_CREATETABLE);
				pomCurrentViewer->ChangeViewTo(pspViewName);
				SetStatusText();
			}
			break;
		case TAB_ARBEITSVERTRAGSARTEN:	
			{
				if(pomCurrentViewer->GetBDPSWhere(olTmpWhere) == true)
				{
					olWhere = CString(" WHERE ") + olTmpWhere;
				}
				if(!olOrderBy.IsEmpty())
				{
					olWhere += CString(" ORDER BY ") + olOrderBy;
				}
				SetStatusText(0,LoadStg(IDS_STRING162)+LoadStg(ST_READ));
				ogCotData.Register();
				if(ogCotData.Read(olWhere.GetBuffer(0))==false)
				{
					ogCotData.omLastErrorMessage.MakeLower();
					if(ogCotData.omLastErrorMessage.Find("no data found")==-1)
					{
						omErrorTxt.Format("%s %d\n%s",omReadErrTxt, ogCotData.imLastReturnCode, ogCotData.omLastErrorMessage);
						MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
					}
				}	
				SetStatusText(ST_CREATETABLE);
				pomCurrentViewer->ChangeViewTo(pspViewName);
				SetStatusText();
			}
			break;
		case TAB_BEWERTUNGSFAKTOREN:		
			{
				if(pomCurrentViewer->GetBDPSWhere(olTmpWhere) == true)
				{
					olWhere = CString(" WHERE ") + olTmpWhere;
				}
				if(!olOrderBy.IsEmpty())
				{
					olWhere += CString(" ORDER BY ") + olOrderBy;
				}
				SetStatusText(0,LoadStg(IDS_STRING165)+LoadStg(ST_READ));
				ogAsfData.Register();
				if(ogAsfData.Read(olWhere.GetBuffer(0))==false)
				{
					ogAsfData.omLastErrorMessage.MakeLower();
					if(ogAsfData.omLastErrorMessage.Find("no data found")==-1)
					{
						omErrorTxt.Format("%s %d\n%s",omReadErrTxt, ogAsfData.imLastReturnCode, ogAsfData.omLastErrorMessage);
						MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
					}
				}	
				SetStatusText(ST_CREATETABLE);
				pomCurrentViewer->ChangeViewTo(pspViewName);
				SetStatusText();
			}
			break;
		case TAB_BASISSCHICHTEN:			
			{
				if(pomCurrentViewer->GetBDPSWhere(olTmpWhere) == true)
				{
					olWhere = CString(" WHERE ") + olTmpWhere;
				}
				if(!olOrderBy.IsEmpty())
				{
					olWhere += CString(" ORDER BY ") + olOrderBy;
				}
				SetStatusText(0,LoadStg(IDS_STRING164)+LoadStg(ST_READ));
				ogBsdData.Register();
				if(ogBsdData.Read(olWhere.GetBuffer(0))==false)
				{
					ogBsdData.omLastErrorMessage.MakeLower();
					if(ogBsdData.omLastErrorMessage.Find("no data found")==-1)
					{
						omErrorTxt.Format("%s %d\n%s",omReadErrTxt, ogBsdData.imLastReturnCode, ogBsdData.omLastErrorMessage);
						MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
					}
				}	
				SetStatusText(ST_CREATETABLE);
				pomCurrentViewer->ChangeViewTo(pspViewName);
				SetStatusText();
			}
			break;
		case TAB_DIENSTEUNDABWESENHEITEN:
			{
				if(pomCurrentViewer->GetBDPSWhere(olTmpWhere) == true)
				{
					olWhere = CString(" WHERE ") + olTmpWhere;
				}
				if(!olOrderBy.IsEmpty())
				{
					olWhere += CString(" ORDER BY ") + olOrderBy;
				}
				SetStatusText(0,LoadStg(IDS_STRING168)+LoadStg(ST_READ));
				ogOdaData.Register();
				if(ogOdaData.Read(olWhere.GetBuffer(0))==false)
				{
					ogOdaData.omLastErrorMessage.MakeLower();
					if(ogOdaData.omLastErrorMessage.Find("no data found")==-1)
					{
						omErrorTxt.Format("%s %d\n%s",omReadErrTxt, ogOdaData.imLastReturnCode, ogOdaData.omLastErrorMessage);
						MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
					}
				}	
				SetStatusText(ST_CREATETABLE);
				pomCurrentViewer->ChangeViewTo(pspViewName);
				SetStatusText();
			}
			break;
		case TAB_FAHRGEMEINSCHAFTEN:		
			{
				if(pomCurrentViewer->GetBDPSWhere(olTmpWhere) == true)
				{
					olWhere = CString(" WHERE ") + olTmpWhere;
				}
				if(!olOrderBy.IsEmpty())
				{
					olWhere += CString(" ORDER BY ") + olOrderBy;
				}
				SetStatusText(0,LoadStg(IDS_STRING171)+LoadStg(ST_READ));
				ogTeaData.Register();
				if(ogTeaData.Read(olWhere.GetBuffer(0))==false)
				{
					ogTeaData.omLastErrorMessage.MakeLower();
					if(ogTeaData.omLastErrorMessage.Find("no data found")==-1)
					{
						omErrorTxt.Format("%s %d\n%s",omReadErrTxt, ogTeaData.imLastReturnCode, ogTeaData.omLastErrorMessage);
						MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
					}
				}	
				SetStatusText(ST_CREATETABLE);
				pomCurrentViewer->ChangeViewTo(pspViewName);
				SetStatusText();
			}
			break;
		case TAB_ARBEITSGRUPPEN:		
			{
				if(pomCurrentViewer->GetBDPSWhere(olTmpWhere) == true)
				{
					olWhere = CString(" WHERE ") + olTmpWhere;
				}
				if(!olOrderBy.IsEmpty())
				{
					olWhere += CString(" ORDER BY ") + olOrderBy;
				}
				SetStatusText(0,LoadStg(IDS_STRING199)+LoadStg(ST_READ));
				ogWgpData.Register();
				if(ogWgpData.Read(olWhere.GetBuffer(0))==false)
				{
					ogWgpData.omLastErrorMessage.MakeLower();
					if(ogWgpData.omLastErrorMessage.Find("no data found")==-1)
					{
						omErrorTxt.Format("%s %d\n%s",omReadErrTxt, ogWgpData.imLastReturnCode, ogWgpData.omLastErrorMessage);
						MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
					}
				}	
				SetStatusText(ST_CREATETABLE);
				pomCurrentViewer->ChangeViewTo(pspViewName);
				SetStatusText();
			}
			break;
		case TAB_MITARBEITERSTAMM:		
			{
				if(pomCurrentViewer->GetBDPSWhere(olTmpWhere) == true)
				{
					olWhere = CString(" WHERE ") + olTmpWhere;
				}
				if(!olOrderBy.IsEmpty())
				{
					olWhere += CString(" ORDER BY ") + olOrderBy;
				}
				SetStatusText(0,LoadStg(IDS_STRING183)+LoadStg(ST_READ));
				ogStfData.Register();
				if(ogStfData.Read(olWhere.GetBuffer(0))==false)
				{
					ogStfData.omLastErrorMessage.MakeLower();
					if(ogStfData.omLastErrorMessage.Find("no data found")==-1)
					{
						omErrorTxt.Format("%s %d\n%s",omReadErrTxt, ogStfData.imLastReturnCode, ogStfData.omLastErrorMessage);
						MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
					}
				}	
				SetStatusText(ST_CREATETABLE);
				pomCurrentViewer->ChangeViewTo(pspViewName);
				SetStatusText();
			}
			break;
		case TAB_ERGABFERTIGUNGSARTEN:		
			{
				if(pomCurrentViewer->GetBDPSWhere(olTmpWhere) == true)
				{
					olWhere = CString(" WHERE ") + olTmpWhere + CString(" AND EART='A'");
				}
				else
				{
					olWhere = CString(" WHERE EART='A'");
				}
				if(!olOrderBy.IsEmpty())
				{
					olWhere += CString(" ORDER BY ") + olOrderBy;
				}
				SetStatusText(0,LoadStg(IDS_STRING169)+LoadStg(ST_READ));
				ogSphData.Register();
				if(ogSphData.Read(olWhere.GetBuffer(0))==false)
				{
					ogSphData.omLastErrorMessage.MakeLower();
					if(ogSphData.omLastErrorMessage.Find("no data found")==-1)
					{
						omErrorTxt.Format("%s %d\n%s",omReadErrTxt, ogSphData.imLastReturnCode, ogSphData.omLastErrorMessage);
						MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
					}
				}	
				SetStatusText(ST_CREATETABLE);
				pomCurrentViewer->ChangeViewTo(pspViewName);
				SetStatusText();
			}
			break;
		case TAB_REDUKTIONEN:			
			{
				if(pomCurrentViewer->GetBDPSWhere(olTmpWhere) == true)
				{
					olWhere = CString(" WHERE ") + olTmpWhere;
				}
				if(!olOrderBy.IsEmpty())
				{
					olWhere += CString(" ORDER BY ") + olOrderBy;
				}
				SetStatusText(0,LoadStg(IDS_STRING187)+LoadStg(ST_READ));
				ogPrcData.Register();
				if(ogPrcData.Read(olWhere.GetBuffer(0))==false)
				{
					ogPrcData.omLastErrorMessage.MakeLower();
					if(ogPrcData.omLastErrorMessage.Find("no data found") == -1)
					{
						omErrorTxt.Format("%s %d\n%s",omReadErrTxt, ogPrcData.imLastReturnCode, ogPrcData.omLastErrorMessage);
						MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
					}
				}	
				SetStatusText(ST_CREATETABLE);
				pomCurrentViewer->ChangeViewTo(pspViewName);
				SetStatusText();
			}
			break;
		case TAB_GERAETEGRUPPEN:
			{
				if(pomCurrentViewer->GetBDPSWhere(olTmpWhere) == true)
				{
					olWhere = CString(" WHERE ") + olTmpWhere;
				}
				if(!olOrderBy.IsEmpty())
				{
					olWhere += CString(" ORDER BY ") + olOrderBy;
				}
				SetStatusText(0,LoadStg(IDS_STRING180)+LoadStg(ST_READ));
				ogGegData.Register();
				if(ogGegData.Read(olWhere.GetBuffer(0))==false)
				{
					ogGegData.omLastErrorMessage.MakeLower();
					if(ogGegData.omLastErrorMessage.Find("no data found")==-1)
					{
						omErrorTxt.Format("%s %d\n%s",omReadErrTxt, ogGegData.imLastReturnCode, ogGegData.omLastErrorMessage);
						MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
					}
				}	
				SetStatusText(ST_CREATETABLE);
				pomCurrentViewer->ChangeViewTo(pspViewName);
				SetStatusText();
			}
			break;
		case TAB_ORGANIZER:		
			{
				if(pomCurrentViewer->GetBDPSWhere(olTmpWhere) == true)
				{
					olWhere = CString(" WHERE ") + olTmpWhere;
				}
				if(!olOrderBy.IsEmpty())
				{
					olWhere += CString(" ORDER BY ") + olOrderBy;
				}
				SetStatusText(0,LoadStg(IDS_STRING193)+LoadStg(ST_READ));
				ogChtData.Register();
				if(ogChtData.Read(olWhere.GetBuffer(0))==false)
				{
					ogChtData.omLastErrorMessage.MakeLower();
					if(ogChtData.omLastErrorMessage.Find("no data found")==-1)
					{
						omErrorTxt.Format("%s %d\n%s",omReadErrTxt, ogChtData.imLastReturnCode, ogChtData.omLastErrorMessage);
						MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
					}
				}	
				SetStatusText(ST_CREATETABLE);
				pomCurrentViewer->ChangeViewTo(pspViewName);
				SetStatusText();
			}
			break;
		case TAB_TIMEPARAMETERS:		
			{
				if(pomCurrentViewer->GetBDPSWhere(olTmpWhere) == true)
				{
					olWhere = CString(" WHERE ") + olTmpWhere;
				}
				if(!olOrderBy.IsEmpty())
				{
					olWhere += CString(" ORDER BY ") + olOrderBy;
				}
				SetStatusText(0,LoadStg(IDS_STRING197)+LoadStg(ST_READ));
				ogTipData.Register();
				if(ogTipData.Read(olWhere.GetBuffer(0))==false)
				{
					ogTipData.omLastErrorMessage.MakeLower();
					if(ogTipData.omLastErrorMessage.Find("no data found")==-1)
					{
						omErrorTxt.Format("%s %d\n%s",omReadErrTxt, ogTipData.imLastReturnCode, ogTipData.omLastErrorMessage);
						MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
					}
				}	
				SetStatusText(ST_CREATETABLE);
				pomCurrentViewer->ChangeViewTo(pspViewName);
				SetStatusText();
			}
			break;
		case TAB_HOLIDAY:		
			{
				if(pomCurrentViewer->GetBDPSWhere(olTmpWhere) == true)
				{
					olWhere = CString(" WHERE ") + olTmpWhere;
				}
				if(!olOrderBy.IsEmpty())
				{
					olWhere += CString(" ORDER BY ") + olOrderBy;
				}
				SetStatusText(0,LoadStg(IDS_STRING198)+LoadStg(ST_READ));
				ogHolData.Register();
				if(ogHolData.Read(olWhere.GetBuffer(0))==false)
				{
					ogHolData.omLastErrorMessage.MakeLower();
					if(ogHolData.omLastErrorMessage.Find("no data found")==-1)
					{
						omErrorTxt.Format("%s %d\n%s",omReadErrTxt, ogHolData.imLastReturnCode, ogHolData.omLastErrorMessage);
						MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
					}
				}	
				SetStatusText(ST_CREATETABLE);
				pomCurrentViewer->ChangeViewTo(pspViewName);
				SetStatusText();
			}
			break;
		case TAB_PLANNING_GROUPS:
			{
				if(pomCurrentViewer->GetBDPSWhere(olTmpWhere) == true)
				{
					olWhere = CString(" WHERE ") + olTmpWhere;
				}
				if(!olOrderBy.IsEmpty())
				{
					olWhere += CString(" ORDER BY ") + olOrderBy;
				}
				SetStatusText(0,LoadStg(IDS_STRING200) + LoadStg(ST_READ));
				ogPgpData.Register();
 				if(ogPgpData.Read(olWhere.GetBuffer(0)) == false)
				{
					ogPgpData.omLastErrorMessage.MakeLower();
					if(ogPgpData.omLastErrorMessage.Find("no data found")==-1)
					{
						omErrorTxt.Format("%s %d\n%s",omReadErrTxt, ogPgpData.imLastReturnCode, ogPgpData.omLastErrorMessage);
						MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
					}
				}	
				SetStatusText(ST_CREATETABLE);
				pomCurrentViewer->ChangeViewTo(pspViewName);
				SetStatusText();
			}
			break;
		case TAB_AIRPORTWET:
			{
				if(pomCurrentViewer->GetBDPSWhere(olTmpWhere) == true)
				{
					olWhere = CString(" WHERE ") + olTmpWhere;
				}
				if(!olOrderBy.IsEmpty())
				{
					olWhere += CString(" ORDER BY ") + olOrderBy;
				}
				SetStatusText(0,LoadStg(IDS_STRING201) + LoadStg(ST_READ));
				ogAwiData.Register();
 				if(ogAwiData.Read(olWhere.GetBuffer(0)) == false)
				{
					ogAwiData.omLastErrorMessage.MakeLower();
					if(ogAwiData.omLastErrorMessage.Find("no data found")==-1)
					{
						omErrorTxt.Format("%s %d\n%s",omReadErrTxt, ogAwiData.imLastReturnCode, ogAwiData.omLastErrorMessage);
						MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
					}
				}	
				SetStatusText(ST_CREATETABLE);
				pomCurrentViewer->ChangeViewTo(pspViewName);
				SetStatusText();
			}
			break;
		case TAB_COUNTERCLASS:
			{
				if(pomCurrentViewer->GetBDPSWhere(olTmpWhere) == true)
				{
					olWhere = CString(" WHERE ") + olTmpWhere;
				}
				if(!olOrderBy.IsEmpty())
				{
					olWhere += CString(" ORDER BY ") + olOrderBy;
				}
				SetStatusText(0,LoadStg(IDS_STRING202) + LoadStg(ST_READ));
				ogCccData.Register();
 				if(ogCccData.Read(olWhere.GetBuffer(0)) == false)
				{
					ogCccData.omLastErrorMessage.MakeLower();
					if(ogCccData.omLastErrorMessage.Find("no data found")==-1)
					{
						omErrorTxt.Format("%s %d\n%s",omReadErrTxt, ogCccData.imLastReturnCode, ogCccData.omLastErrorMessage);
						MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
					}
				}	
				SetStatusText(ST_CREATETABLE);
				pomCurrentViewer->ChangeViewTo(pspViewName);
				SetStatusText();
			}
			break;
		case TAB_VERYIMPPERS:
			{
				if(pomCurrentViewer->GetBDPSWhere(olTmpWhere) == true)
				{
					olWhere = CString(" WHERE ") + olTmpWhere;
				}
				if(!olOrderBy.IsEmpty())
				{
					olWhere += CString(" ORDER BY ") + olOrderBy;
				}
				SetStatusText(0,LoadStg(IDS_STRING203) + LoadStg(ST_READ));
				ogVipData.Register();
 				if(ogVipData.Read(olWhere.GetBuffer(0)) == false)
				{
					ogVipData.omLastErrorMessage.MakeLower();
					if(ogVipData.omLastErrorMessage.Find("no data found")==-1)
					{
						omErrorTxt.Format("%s %d\n%s",omReadErrTxt, ogVipData.imLastReturnCode, ogVipData.omLastErrorMessage);
						MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
					}
				}	
				SetStatusText(ST_CREATETABLE);
				pomCurrentViewer->ChangeViewTo(pspViewName);
				SetStatusText();
			}
			break;
		case TAB_AIRCRAFTFAM:		
			{
				if(pomCurrentViewer->GetBDPSWhere(olTmpWhere) == true)
				{
					olWhere = CString(" WHERE ") + olTmpWhere;
				}
				if(!olOrderBy.IsEmpty())
				{
					olWhere += CString(" ORDER BY ") + olOrderBy;
				}
				SetStatusText(0,LoadStg(IDS_STRING171)+LoadStg(ST_READ));
				ogAFMData.Register();
				if(ogAFMData.Read(olWhere.GetBuffer(0))==false)
				{
					ogAFMData.omLastErrorMessage.MakeLower();
					if(ogAFMData.omLastErrorMessage.Find("no data found")==-1)
					{
						omErrorTxt.Format("%s %d\n%s",omReadErrTxt, ogAFMData.imLastReturnCode, ogTeaData.omLastErrorMessage);
						MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
					}
				}	
				SetStatusText(ST_CREATETABLE);
				pomCurrentViewer->ChangeViewTo(pspViewName);
				SetStatusText();
			}
			break;
		case TAB_ENGINETYPE:		
			{
				if(pomCurrentViewer->GetBDPSWhere(olTmpWhere) == true)
				{
					olWhere = CString(" WHERE ") + olTmpWhere;
				}
				if(!olOrderBy.IsEmpty())
				{
					olWhere += CString(" ORDER BY ") + olOrderBy;
				}
				SetStatusText(0,LoadStg(IDS_STRING171)+LoadStg(ST_READ));
				ogENTData.Register();
				if(ogENTData.Read(olWhere.GetBuffer(0))==false)
				{
					ogENTData.omLastErrorMessage.MakeLower();
					if(ogENTData.omLastErrorMessage.Find("no data found")==-1)
					{
						omErrorTxt.Format("%s %d\n%s",omReadErrTxt, ogENTData.imLastReturnCode, ogTeaData.omLastErrorMessage);
						MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
					}
				}	
				SetStatusText(ST_CREATETABLE);
				pomCurrentViewer->ChangeViewTo(pspViewName);
				SetStatusText();
			}
			break;
		default:
			{
				// Do nothing
			}
			break; 
	}
	AfxGetApp()->DoWaitCursor(-1);

}

//------------------------------------------------------------------------------------------------------

void CStammdaten::ShowAnsicht()
{
	BDPSPropertySheet olDlg(omCurrentView, this, pomCurrentViewer);//&omViewer);
	//bmIsViewOpen = TRUE;
	if(olDlg.DoModal() != IDCANCEL)
	{
		CString olTmpWhere;
		CString olWhere;
		UpdateComboAnsicht();
		ChangeViewTo(pomCurrentViewer->GetViewName().GetBuffer(0));

	}
}

//------------------------------------------------------------------------------------------------------

void CStammdaten::ChangeViewer()
{
	AfxGetApp()->DoWaitCursor(1);

	if(igSelectTab!=TAB_NO_TAB)
	{
		SetWindowText(ogCmdLineStghArray.GetAt(0) + "   <" + omTableName + ">");
		omCurrentView = omTableName;
		ogAnsicht = "<Default>";
	}


	switch(igSelectTab)
	{
		case TAB_AIRLINE:
			{
				pomAirlineViewer = new AirlineTableViewer(&ogALTData.omData);
				pomCurrentViewer = (CViewer *)pomAirlineViewer; 
				pomCurrentViewer->SetViewerKey(CString("UIF_AIRLINE"));
				pomAirlineViewer->Attach(pomTable);
				pomAirlineViewer->ChangeViewTo(pomCurrentViewer->GetViewName());
				UpdateComboAnsicht();
				if(m_AnzeigeComboBox.GetCount() == 0)
				{
					ShowAnsicht();
				}
			}
			break;
		case TAB_AIRCRAFT:
			{
				pomAircraftViewer = new AircraftTableViewer(&ogACTData.omData);
				pomCurrentViewer = (CViewer *)pomAircraftViewer;
				pomCurrentViewer->SetViewerKey(CString("UIF_AIRCRAFT"));
				pomAircraftViewer->Attach(pomTable);
				pomAircraftViewer->ChangeViewTo(pomCurrentViewer->GetViewName());
				UpdateComboAnsicht();
				if(m_AnzeigeComboBox.GetCount() == 0)
				{
					ShowAnsicht();
				}
			}
			break;
		case TAB_LFZREGISTRATION:
			{
				pomLFZRegiViewer = new LFZRegiTableViewer(&ogACRData.omData);
				pomCurrentViewer = (CViewer *)pomLFZRegiViewer;
				pomCurrentViewer->SetViewerKey(CString("UIF_LFZREGISTR"));
				pomLFZRegiViewer->Attach(pomTable);
				pomLFZRegiViewer->ChangeViewTo(pomCurrentViewer->GetViewName());
				UpdateComboAnsicht();
				if(m_AnzeigeComboBox.GetCount() == 0)
				{
					ShowAnsicht();
				}
			}
			break;
		case TAB_AIRPORT:
			{
				pomAirportViewer = new AirportTableViewer(&ogAPTData.omData);
				pomCurrentViewer = (CViewer *)pomAirportViewer;
				pomCurrentViewer->SetViewerKey(CString("UIF_AIRPORT"));
				pomAirportViewer->Attach(pomTable);
				pomAirportViewer->ChangeViewTo(pomCurrentViewer->GetViewName());
				UpdateComboAnsicht();
				if(m_AnzeigeComboBox.GetCount() == 0)
				{
					ShowAnsicht();
				}
			}
			break;
		case TAB_RUNWAY:
			{
				pomRunwayViewer = new RunwayTableViewer(&ogRWYData.omData);
				pomCurrentViewer = (CViewer *)pomRunwayViewer;
				pomCurrentViewer->SetViewerKey(CString("UIF_RUNWAY"));
				pomRunwayViewer->Attach(pomTable);
				pomRunwayViewer->ChangeViewTo(pomCurrentViewer->GetViewName());
				UpdateComboAnsicht();
				if(m_AnzeigeComboBox.GetCount() == 0)
				{
					ShowAnsicht();
				}
			}
			break;
		case TAB_TAXIWAY:
			{
				pomTaxiwayViewer = new TaxiwayTableViewer(&ogTWYData.omData);
				pomCurrentViewer = (CViewer *)pomTaxiwayViewer;
				pomCurrentViewer->SetViewerKey(CString("UIF_TAXIWAY"));
				pomTaxiwayViewer->Attach(pomTable);
				pomTaxiwayViewer->ChangeViewTo(pomCurrentViewer->GetViewName());
				UpdateComboAnsicht();
				if(m_AnzeigeComboBox.GetCount() == 0)
				{
					ShowAnsicht();
				}
			}
			break;
		case TAB_POSITION:
			{
				pomPositionViewer = new PositionTableViewer(&ogPSTData.omData);
				pomCurrentViewer = (CViewer *)pomPositionViewer;
				pomCurrentViewer->SetViewerKey(CString("UIF_POSITION"));
				pomPositionViewer->Attach(pomTable);
				pomPositionViewer->ChangeViewTo(pomCurrentViewer->GetViewName());
				UpdateComboAnsicht();
				if(m_AnzeigeComboBox.GetCount() == 0)
				{
					ShowAnsicht();
				}
			}
			break;
		case TAB_GATE:
			{
				pomGateViewer = new GateTableViewer(&ogGATData.omData);
				pomCurrentViewer = (CViewer *)pomGateViewer;
				pomCurrentViewer->SetViewerKey(CString("UIF_GATE"));
				pomGateViewer->Attach(pomTable);
				pomGateViewer->ChangeViewTo(pomCurrentViewer->GetViewName());
				UpdateComboAnsicht();
				if(m_AnzeigeComboBox.GetCount() == 0)
				{
					ShowAnsicht();
				}
			}
			break;
		case TAB_CHECKINCOUNTER:
			{
				pomCheckinCounterViewer = new CheckinCounterTableViewer(&ogCICData.omData);
				pomCurrentViewer = (CViewer *)pomCheckinCounterViewer;
				pomCurrentViewer->SetViewerKey(CString("UIF_CHECKINCOUNTER"));
				pomCheckinCounterViewer->Attach(pomTable);
				pomCheckinCounterViewer->ChangeViewTo(pomCurrentViewer->GetViewName());
				UpdateComboAnsicht();
				if(m_AnzeigeComboBox.GetCount() == 0)
				{
					ShowAnsicht();
				}
			}
			break;
		case TAB_BEGGAGEBELT:
			{
				pomBeggagebeltViewer = new BeggagebeltTableViewer(&ogBLTData.omData);
				pomCurrentViewer = (CViewer *)pomBeggagebeltViewer;
				pomCurrentViewer->SetViewerKey(CString("UIF_BEGGAGEBELT"));
				pomBeggagebeltViewer->Attach(pomTable);
				pomBeggagebeltViewer->ChangeViewTo(pomCurrentViewer->GetViewName());
				UpdateComboAnsicht();
				if(m_AnzeigeComboBox.GetCount() == 0)
				{
					ShowAnsicht();
				}
			}
			break;
		case TAB_EXIT:
			{
				pomExitViewer = new ExitTableViewer(&ogEXTData.omData);
				pomCurrentViewer = (CViewer *)pomExitViewer;
				pomCurrentViewer->SetViewerKey(CString("UIF_EXIT"));
				pomExitViewer->Attach(pomTable);
				pomExitViewer->ChangeViewTo(pomCurrentViewer->GetViewName());
				UpdateComboAnsicht();
				if(m_AnzeigeComboBox.GetCount() == 0)
				{
					ShowAnsicht();
				}
			}
			break;
		case TAB_DELAYCODE:
			{
				pomDelaycodeViewer = new DelaycodeTableViewer(&ogDENData.omData);
				pomCurrentViewer = (CViewer *)pomDelaycodeViewer;
				pomCurrentViewer->SetViewerKey(CString("UIF_DELAYCODE"));
				pomDelaycodeViewer->Attach(pomTable);
				pomDelaycodeViewer->ChangeViewTo(pomCurrentViewer->GetViewName());
				UpdateComboAnsicht();
				if(m_AnzeigeComboBox.GetCount() == 0)
				{
					ShowAnsicht();
				}
			}
			break;
		case TAB_TELEXADRESS:
			{
				pomTelexadressViewer = new TelexadressTableViewer(&ogMVTData.omData);
				pomCurrentViewer = (CViewer *)pomTelexadressViewer;
				pomCurrentViewer->SetViewerKey(CString("UIF_TELEXADRESS"));
				pomTelexadressViewer->Attach(pomTable);
				pomTelexadressViewer->ChangeViewTo(pomCurrentViewer->GetViewName());
				UpdateComboAnsicht();
				if(m_AnzeigeComboBox.GetCount() == 0)
				{
					ShowAnsicht();
				}
			}
			break;
		case TAB_TRAFFICTYPE:
			{
				pomTraffictypeViewer = new TraffictypeTableViewer(&ogNATData.omData);
				pomCurrentViewer = (CViewer *)pomTraffictypeViewer;
				pomCurrentViewer->SetViewerKey(CString("UIF_TRAFFICTYPE"));
				pomTraffictypeViewer->Attach(pomTable);
				pomTraffictypeViewer->ChangeViewTo(pomCurrentViewer->GetViewName());
				UpdateComboAnsicht();
				if(m_AnzeigeComboBox.GetCount() == 0)
				{
					ShowAnsicht();
				}
			}
			break;
		case TAB_HANDLINGAGENT:
			{
				pomHandlingagentViewer = new HandlingagentTableViewer(&ogHAGData.omData);
				pomCurrentViewer = (CViewer *)pomHandlingagentViewer;
				pomCurrentViewer->SetViewerKey(CString("UIF_HANDLINGAGENT"));
				pomHandlingagentViewer->Attach(pomTable);
				pomHandlingagentViewer->ChangeViewTo(pomCurrentViewer->GetViewName());
				UpdateComboAnsicht();
				if(m_AnzeigeComboBox.GetCount() == 0)
				{
					ShowAnsicht();
				}
			}
			break;
		case TAB_WAITINGROOM:
			{
				pomWaitingroomViewer = new WaitingroomTableViewer(&ogWROData.omData);
				pomCurrentViewer = (CViewer *)pomWaitingroomViewer;
				pomCurrentViewer->SetViewerKey(CString("UIF_WAITINGROOM"));
				pomWaitingroomViewer->Attach(pomTable);
				pomWaitingroomViewer->ChangeViewTo(pomCurrentViewer->GetViewName());
				UpdateComboAnsicht();
				if(m_AnzeigeComboBox.GetCount() == 0)
				{
					ShowAnsicht();
				}
			}
			break;
		case TAB_HANDLINGTYPE:
			{
				pomHandlingtypeViewer = new HandlingtypeTableViewer(&ogHTYData.omData);
				pomCurrentViewer = (CViewer *)pomHandlingtypeViewer;
				pomCurrentViewer->SetViewerKey(CString("UIF_HANDLINGTYPE"));
				pomHandlingtypeViewer->Attach(pomTable);
				pomHandlingtypeViewer->ChangeViewTo(pomCurrentViewer->GetViewName());
				UpdateComboAnsicht();
				if(m_AnzeigeComboBox.GetCount() == 0)
				{
					ShowAnsicht();
				}
			}
			break;
		case TAB_SERVICETYPE:
			{
				pomServicetypeViewer = new ServicetypeTableViewer(&ogSTYData.omData);
				pomCurrentViewer = (CViewer *)pomServicetypeViewer;
				pomCurrentViewer->SetViewerKey(CString("UIF_SERVICETYPE"));
				pomServicetypeViewer->Attach(pomTable);
				pomServicetypeViewer->ChangeViewTo(pomCurrentViewer->GetViewName());
				UpdateComboAnsicht();
				if(m_AnzeigeComboBox.GetCount() == 0)
				{
					ShowAnsicht();
				}
			}
			break;
		case TAB_FIDSCOMMAND:
			{		
				pomFidsCommandViewer = new FidsCommandTableViewer(&ogFIDData.omData);
				pomCurrentViewer = (CViewer *)pomFidsCommandViewer;
				pomCurrentViewer->SetViewerKey(CString("UIF_FIDSCOMMAND"));
				pomFidsCommandViewer->Attach(pomTable);
				pomFidsCommandViewer->ChangeViewTo(pomCurrentViewer->GetViewName());
				UpdateComboAnsicht();
				if(m_AnzeigeComboBox.GetCount() == 0)
				{
					ShowAnsicht();
				}
			}
			break;
		case TAB_ERGVERKEHRSARTEN:
			{
				pomErgVerkehrsartenViewer = new ErgVerkehrsartenTableViewer(&ogSphData.omData);
				pomCurrentViewer = (CViewer *)pomErgVerkehrsartenViewer;
				pomCurrentViewer->SetViewerKey(CString("UIF_ERGVERKEHRSAR"));
				pomErgVerkehrsartenViewer->Attach(pomTable);
				pomErgVerkehrsartenViewer->ChangeViewTo(pomCurrentViewer->GetViewName());
				UpdateComboAnsicht();
				if(m_AnzeigeComboBox.GetCount() == 0)
				{
					ShowAnsicht();
				}
			}
			break;
		case TAB_STANDARDVERKETTUNGEN:
			{
				pomStandardverkettungenViewer = new StandardverkettungenTableViewer(&ogStrData.omData);
				pomCurrentViewer = (CViewer *)pomStandardverkettungenViewer;
				pomCurrentViewer->SetViewerKey(CString("UIF_STDARDVERKETT"));
				pomStandardverkettungenViewer->Attach(pomTable);
				pomStandardverkettungenViewer->ChangeViewTo(pomCurrentViewer->GetViewName());
				UpdateComboAnsicht();
				if(m_AnzeigeComboBox.GetCount() == 0)
				{
					ShowAnsicht();
				}
			}
			break;
		case TAB_FLUGPLANSAISON:
			{
				pomFlugplansaisonViewer = new FlugplansaisonTableViewer(&ogSeaData.omData);
				pomCurrentViewer = (CViewer *)pomFlugplansaisonViewer;
				pomCurrentViewer->SetViewerKey(CString("UIF_FLUGPLANSAISON"));
				pomFlugplansaisonViewer->Attach(pomTable);
				pomFlugplansaisonViewer->ChangeViewTo(pomCurrentViewer->GetViewName());
				UpdateComboAnsicht();
				if(m_AnzeigeComboBox.GetCount() == 0)
				{
					ShowAnsicht();
				}
			}
			break;
		case TAB_LEISTUNGSKATALOG:
			{
				pomLeistungskatalogViewer = new LeistungskatalogTableViewer(&ogGhsData.omData);
				pomCurrentViewer = (CViewer *)pomLeistungskatalogViewer;
				pomCurrentViewer->SetViewerKey(CString("UIF_LEISTUNGSKATA"));
				pomLeistungskatalogViewer->Attach(pomTable);
				pomLeistungskatalogViewer->ChangeViewTo(pomCurrentViewer->GetViewName());
				UpdateComboAnsicht();
				if(m_AnzeigeComboBox.GetCount() == 0)
				{
					ShowAnsicht();
				}
			}
			break;
		case TAB_QUALIFIKATIONEN:
			{
				pomQualifikationenViewer = new QualifikationenTableViewer(&ogPerData.omData);
				pomCurrentViewer = (CViewer *)pomQualifikationenViewer;
				pomCurrentViewer->SetViewerKey(CString("UIF_QUALIFIKATIONEN"));
				pomQualifikationenViewer->Attach(pomTable);
				pomQualifikationenViewer->ChangeViewTo(pomCurrentViewer->GetViewName());
				UpdateComboAnsicht();
				if(m_AnzeigeComboBox.GetCount() == 0)
				{
					ShowAnsicht();
				}
			}
			break;
		case TAB_ORGANISATIONSEINHEITEN:
			{
				pomOrganisationseinheitenViewer = new OrganisationseinheitenTableViewer(&ogOrgData.omData);
				pomCurrentViewer = (CViewer *)pomOrganisationseinheitenViewer;
				pomCurrentViewer->SetViewerKey(CString("UIF_ORGANISATIONSEINHEITEN"));
				pomOrganisationseinheitenViewer->Attach(pomTable);
				pomOrganisationseinheitenViewer->ChangeViewTo(pomCurrentViewer->GetViewName());
				UpdateComboAnsicht();
				if(m_AnzeigeComboBox.GetCount() == 0)
				{
					ShowAnsicht();
				}
			}
			break;
		case TAB_WEGEZEITEN:				
			{
				pomWegezeitenViewer = new WegezeitenTableViewer(&ogWayData.omData);
				pomCurrentViewer = (CViewer *)pomWegezeitenViewer;
				pomCurrentViewer->SetViewerKey(CString("UIF_WEGEZEITEN"));
				pomWegezeitenViewer->Attach(pomTable);
				pomWegezeitenViewer->ChangeViewTo(pomCurrentViewer->GetViewName());
				UpdateComboAnsicht();
				if(m_AnzeigeComboBox.GetCount() == 0)
				{
					ShowAnsicht();
				}
			}
			break;
		case TAB_FUNKTIONEN:				
			{
				pomFunktionenViewer = new FunktionenTableViewer(&ogPfcData.omData);
				pomCurrentViewer = (CViewer *)pomFunktionenViewer;
				pomCurrentViewer->SetViewerKey(CString("UIF_FUNKTIONEN"));
				pomFunktionenViewer->Attach(pomTable);
				pomFunktionenViewer->ChangeViewTo(pomCurrentViewer->GetViewName());
				UpdateComboAnsicht();
				if(m_AnzeigeComboBox.GetCount() == 0)
				{
					ShowAnsicht();
				}
			}
			break;
		case TAB_ARBEITSVERTRAGSARTEN:	
			{
				pomArbeitsvertragsartenViewer = new ArbeitsvertragsartenTableViewer(&ogCotData.omData);
				pomCurrentViewer = (CViewer *)pomArbeitsvertragsartenViewer;
				pomCurrentViewer->SetViewerKey(CString("UIF_ARBEITSVERTRAGSARTEN"));
				pomArbeitsvertragsartenViewer->Attach(pomTable);
				pomArbeitsvertragsartenViewer->ChangeViewTo(pomCurrentViewer->GetViewName());
				UpdateComboAnsicht();
				if(m_AnzeigeComboBox.GetCount() == 0)
				{
					ShowAnsicht();
				}
			}
			break;
		case TAB_BEWERTUNGSFAKTOREN:		
			{
				pomBewertungsfaktorenViewer = new BewertungsfaktorenTableViewer(&ogAsfData.omData);
				pomCurrentViewer = (CViewer *)pomBewertungsfaktorenViewer;
				pomCurrentViewer->SetViewerKey(CString("UIF_BEWERTUNGSFAKTOREN"));
				pomBewertungsfaktorenViewer->Attach(pomTable);
				pomBewertungsfaktorenViewer->ChangeViewTo(pomCurrentViewer->GetViewName());
				UpdateComboAnsicht();
				if(m_AnzeigeComboBox.GetCount() == 0)
				{
					ShowAnsicht();
				}
			}
			break;
		case TAB_BASISSCHICHTEN:			
			{
				pomBasisschichtenViewer = new BasisschichtenTableViewer(&ogBsdData.omData);
				pomCurrentViewer = (CViewer *)pomBasisschichtenViewer;
				pomCurrentViewer->SetViewerKey(CString("UIF_BASISSCHICHTEN"));
				pomBasisschichtenViewer->Attach(pomTable);
				pomBasisschichtenViewer->ChangeViewTo(pomCurrentViewer->GetViewName());
				UpdateComboAnsicht();
				if(m_AnzeigeComboBox.GetCount() == 0)
				{
					ShowAnsicht();
				}
			}
			break;
		case TAB_DIENSTEUNDABWESENHEITEN:
			{
				pomDiensteUndAbwesenheitenViewer = new DiensteUndAbwesenheitenTableViewer(&ogOdaData.omData);
				pomCurrentViewer = (CViewer *)pomDiensteUndAbwesenheitenViewer;
				pomCurrentViewer->SetViewerKey(CString("UIF_DIENSTEUNDABWESENHEITEN"));
				pomDiensteUndAbwesenheitenViewer->Attach(pomTable);
				pomDiensteUndAbwesenheitenViewer->ChangeViewTo(pomCurrentViewer->GetViewName());
				UpdateComboAnsicht();
				if(m_AnzeigeComboBox.GetCount() == 0)
				{
					ShowAnsicht();
				}
			}
			break;
		case TAB_FAHRGEMEINSCHAFTEN:		
			{
				pomFahrgemeinschaftenViewer = new FahrgemeinschaftenTableViewer(&ogTeaData.omData);
				pomCurrentViewer = (CViewer *)pomFahrgemeinschaftenViewer;
				pomCurrentViewer->SetViewerKey(CString("UIF_FAHRGEMEINSCHAFTEN"));
				pomFahrgemeinschaftenViewer->Attach(pomTable);
				pomFahrgemeinschaftenViewer->ChangeViewTo(pomCurrentViewer->GetViewName());
				UpdateComboAnsicht();
				if(m_AnzeigeComboBox.GetCount() == 0)
				{
					ShowAnsicht();
				}
			}
			break;
		case TAB_ARBEITSGRUPPEN:		
			{
				pomArbeitsgruppenViewer = new ArbeitsgruppenTableViewer(&ogWgpData.omData);
				pomCurrentViewer = (CViewer *)pomArbeitsgruppenViewer;
				pomCurrentViewer->SetViewerKey(CString("UIF_ARBEITSGRUPPEN"));
				pomArbeitsgruppenViewer->Attach(pomTable);
				pomArbeitsgruppenViewer->ChangeViewTo(pomCurrentViewer->GetViewName());
				UpdateComboAnsicht();
				if(m_AnzeigeComboBox.GetCount() == 0)
				{
					ShowAnsicht();
				}
			}
			break;
		case TAB_MITARBEITERSTAMM:		
			{
				pomMitarbeiterstammViewer = new MitarbeiterstammTableViewer(&ogStfData.omData);
				pomCurrentViewer = (CViewer *)pomMitarbeiterstammViewer;
				pomCurrentViewer->SetViewerKey(CString("UIF_MITARBEITERSTAMM"));
				pomMitarbeiterstammViewer->Attach(pomTable);
				pomMitarbeiterstammViewer->ChangeViewTo(pomCurrentViewer->GetViewName());
				UpdateComboAnsicht();
				if(m_AnzeigeComboBox.GetCount() == 0)
				{
					ShowAnsicht();
				}
			}
			break;
		case TAB_ERGABFERTIGUNGSARTEN:		
			{
				pomErgAbfertigungsartenViewer = new ErgAbfertigungsartenTableViewer(&ogSphData.omData);
				pomCurrentViewer = (CViewer *)pomErgAbfertigungsartenViewer;
				pomCurrentViewer->SetViewerKey(CString("UIF_ERGABFERTIGUNGSARTEN"));
				pomErgAbfertigungsartenViewer->Attach(pomTable);
				pomErgAbfertigungsartenViewer->ChangeViewTo(pomCurrentViewer->GetViewName());
				UpdateComboAnsicht();
				if(m_AnzeigeComboBox.GetCount() == 0)
				{
					ShowAnsicht();
				}
			}
			break;
		case TAB_REDUKTIONEN:
			{
				pomReduktionenViewer = new ReduktionenTableViewer(&ogPrcData.omData);
				pomCurrentViewer = (CViewer *)pomReduktionenViewer;
				pomCurrentViewer->SetViewerKey(CString("UIF_REDUKTIONEN"));
				pomReduktionenViewer->Attach(pomTable);
				pomReduktionenViewer->ChangeViewTo(pomCurrentViewer->GetViewName());
				UpdateComboAnsicht();
				if(m_AnzeigeComboBox.GetCount() == 0)
				{
					ShowAnsicht();
				}
			}
			break;
		case TAB_GERAETEGRUPPEN:
			{
				pomGeraetegruppenViewer = new GeraetegruppenTableViewer(&ogGegData.omData);
				pomCurrentViewer = (CViewer *)pomGeraetegruppenViewer;
				pomCurrentViewer->SetViewerKey(CString("UIF_GERAETEGRUPPEN"));
				pomGeraetegruppenViewer->Attach(pomTable);
				pomGeraetegruppenViewer->ChangeViewTo(pomCurrentViewer->GetViewName());
				UpdateComboAnsicht();
				if(m_AnzeigeComboBox.GetCount() == 0)
				{
					ShowAnsicht();
				}
			}
			break;
		case TAB_ORGANIZER:
			{
				pomOrganizerViewer = new OrganizerTableViewer(&ogChtData.omData);
				pomCurrentViewer = (CViewer *)pomOrganizerViewer;
				pomCurrentViewer->SetViewerKey(CString("UIF_ORGANIZER"));
				pomOrganizerViewer->Attach(pomTable);
				pomOrganizerViewer->ChangeViewTo(pomCurrentViewer->GetViewName());
				UpdateComboAnsicht();
				if(m_AnzeigeComboBox.GetCount() == 0)
				{
					ShowAnsicht();
				}
			}
			break;
		case TAB_TIMEPARAMETERS:
			{
				pomTimeParametersViewer = new TimeParametersTableViewer(&ogTipData.omData);
				pomCurrentViewer = (CViewer *)pomTimeParametersViewer;
				pomCurrentViewer->SetViewerKey(CString("UIF_TIMEPARAMETERS"));
				pomTimeParametersViewer->Attach(pomTable);
				pomTimeParametersViewer->ChangeViewTo(pomCurrentViewer->GetViewName());
				UpdateComboAnsicht();
				if(m_AnzeigeComboBox.GetCount() == 0)
				{
					ShowAnsicht();
				}
			}
			break;
		case TAB_HOLIDAY:
			{
				pomHolidayViewer = new HolidayTableViewer(&ogHolData.omData);
				pomCurrentViewer = (CViewer *)pomHolidayViewer;
				pomCurrentViewer->SetViewerKey(CString("UIF_HOLIDAY"));
				pomHolidayViewer->Attach(pomTable);
				pomHolidayViewer->ChangeViewTo(pomCurrentViewer->GetViewName());
				UpdateComboAnsicht();
				if(m_AnzeigeComboBox.GetCount() == 0)
				{
					ShowAnsicht();
				}
			}
			break;
		case TAB_PLANNING_GROUPS:
			{
				pomPlanungsgruppenViewer = new PlanungsgruppenTableViewer(&ogPgpData.omData);
				pomCurrentViewer = (CViewer *)pomPlanungsgruppenViewer;
				pomCurrentViewer->SetViewerKey(CString("UIF_PLANUNGSGRUPPEN"));
				pomPlanungsgruppenViewer->Attach(pomTable);
				pomPlanungsgruppenViewer->ChangeViewTo(pomCurrentViewer->GetViewName());
				UpdateComboAnsicht();
				if(m_AnzeigeComboBox.GetCount() == 0)
				{
					ShowAnsicht();
				}
			}
			break;
		case TAB_AIRPORTWET:
			{
				pomAirportwetViewer = new AirportwetTableViewer(&ogAwiData.omData);
				pomCurrentViewer = (CViewer *)pomAirportwetViewer;
				pomCurrentViewer->SetViewerKey(CString("UIF_AIRPORTWET"));
				pomAirportwetViewer->Attach(pomTable);
				pomAirportwetViewer->ChangeViewTo(pomCurrentViewer->GetViewName());
				UpdateComboAnsicht();
				if(m_AnzeigeComboBox.GetCount() == 0)
				{
					ShowAnsicht();
				}
			}
			break;
		case TAB_COUNTERCLASS:
			{
				pomCounterclassViewer = new CounterclassTableViewer(&ogCccData.omData);
				pomCurrentViewer = (CViewer *)pomCounterclassViewer;
				pomCurrentViewer->SetViewerKey(CString("UIF_COUNTERCLASS"));
				pomCounterclassViewer->Attach(pomTable);
				pomCounterclassViewer->ChangeViewTo(pomCurrentViewer->GetViewName());
				UpdateComboAnsicht();
				if(m_AnzeigeComboBox.GetCount() == 0)
				{
					ShowAnsicht();
				}
			}
			break;
		case TAB_VERYIMPPERS:
			{
				pomVeryimppersViewer = new VeryImpPersTableViewer(&ogVipData.omData);
				pomCurrentViewer = (CViewer *)pomVeryimppersViewer;
				pomCurrentViewer->SetViewerKey(CString("UIF_VERYIMPPERS"));
				pomVeryimppersViewer->Attach(pomTable);
				pomVeryimppersViewer->ChangeViewTo(pomCurrentViewer->GetViewName());
				UpdateComboAnsicht();
				if(m_AnzeigeComboBox.GetCount() == 0)
				{
					ShowAnsicht();
				}
			}
			break;
		case TAB_AIRCRAFTFAM:		
			{
				pomAircraftFamViewer = new AircraftFamTableViewer(&ogAFMData.omData);
				pomCurrentViewer = (CViewer *)pomAircraftFamViewer;
				pomCurrentViewer->SetViewerKey(CString("UIF_AIRCRAFTFAM"));
				pomAircraftFamViewer->Attach(pomTable);
				pomAircraftFamViewer->ChangeViewTo(pomCurrentViewer->GetViewName());
				UpdateComboAnsicht();
				if(m_AnzeigeComboBox.GetCount() == 0)
				{
					ShowAnsicht();
				}
			}
			break;
		case TAB_ENGINETYPE:		
			{
				pomEngineTypeViewer = new EngineTypeTableViewer(&ogENTData.omData);
				pomCurrentViewer = (CViewer *)pomEngineTypeViewer;
				pomCurrentViewer->SetViewerKey(CString("UIF_ENGINETYPE"));
				pomEngineTypeViewer->Attach(pomTable);
				pomEngineTypeViewer->ChangeViewTo(pomCurrentViewer->GetViewName());
				UpdateComboAnsicht();
				if(m_AnzeigeComboBox.GetCount() == 0)
				{
					ShowAnsicht();
				}
			}
			break;
		default:
			{
				// Do nothing
			}
			break;
	}

	if (pomTable != NULL && ::IsWindow(pomTable->m_hWnd))
	{
		CRect olrectStatus;
		m_Status.GetWindowRect(&olrectStatus);
		int ilStatusHigh = olrectStatus.bottom-olrectStatus.top;

		CRect olrectTable;
		GetClientRect(&olrectTable);
		olrectTable.InflateRect(1,1);     // hiding the CTable window border
		this->pomTable->SetPosition(olrectTable.left+2, olrectTable.right-2, olrectTable.top+35, olrectTable.bottom-(ilStatusHigh+3));
	}

	AfxGetApp()->DoWaitCursor(-1);
}

//------------------------------------------------------------------------------------------------------

void CStammdaten::OnAendern() 
{
	if(!bgIsDialogOpen)
	{
		bgIsDialogOpen = true;
		switch(igSelectTab)
		{
			case TAB_AIRLINE:
				{
					int ilLineNo = pomTable->GetCurSel();
					if (ilLineNo == -1)
					{
						MessageBox(LoadStg(IDS_STRING148),LoadStg(IDS_STRING149),MB_ICONEXCLAMATION);
						break;
					}
					ALTDATA *prlALT = ogALTData.GetALTByUrno(pomAirlineViewer->omLines[ilLineNo].Urno);
					if (prlALT != NULL)
					{
						ALTDATA rlALT = *prlALT;
						AirlineDlg *olALTDlg = new AirlineDlg(&rlALT,this);
						olALTDlg->m_Caption = LoadStg(IDS_STRING150);
						if (olALTDlg->DoModal() == IDOK)
						{
							rlALT.Lstu = CTime::GetCurrentTime();
							strcpy(rlALT.Useu,cgUserName);
							AfxGetApp()->DoWaitCursor(1);
							if(ogALTData.UpdateALT(&rlALT)==false)
							{
								omErrorTxt.Format("%s %d\n%s",omUpdateErrTxt,ogALTData.imLastReturnCode, ogALTData.omLastErrorMessage);
								MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
							}	
							AfxGetApp()->DoWaitCursor(-1);
						}
						delete olALTDlg;
					}
				}
				break;
			case TAB_TIMEPARAMETERS:			
				{
					int ilLineNo = pomTable->GetCurSel();
					if (ilLineNo == -1)
					{
						MessageBox(LoadStg(IDS_STRING148),LoadStg(IDS_STRING149),MB_ICONEXCLAMATION);
						break;
					}
					TIPDATA *prlTIP = ogTipData.GetTipByUrno(pomTimeParametersViewer->omLines[ilLineNo].Urno);
					if (prlTIP != NULL)
					{
						TIPDATA rlTIP = *prlTIP;
						TimeParametersDlg *olTIPDlg = new TimeParametersDlg(&rlTIP,this);
						olTIPDlg->m_Caption = LoadStg(IDS_STRING150);
						if (olTIPDlg->DoModal() == IDOK)
						{
							rlTIP.Lstu = CTime::GetCurrentTime();
							strcpy(rlTIP.Useu,cgUserName);
							AfxGetApp()->DoWaitCursor(1);
							if(ogTipData.Update(&rlTIP)==false)
							{
								omErrorTxt.Format("%s %d\n%s",omUpdateErrTxt,ogTipData.imLastReturnCode, ogTipData.omLastErrorMessage);
								MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
							}	
							AfxGetApp()->DoWaitCursor(-1);
						}
						delete olTIPDlg;
					}
				}
				break;
			case TAB_AIRCRAFT:
				{
					int ilLineNo = pomTable->GetCurSel();
					if (ilLineNo == -1)
					{
						MessageBox(LoadStg(IDS_STRING148),LoadStg(IDS_STRING149),MB_ICONEXCLAMATION);
						break;
					}
					ACTDATA *prlACT = ogACTData.GetACTByUrno(pomAircraftViewer->omLines[ilLineNo].Urno);
					if (prlACT != NULL)
					{
						ACTDATA rlACT = *prlACT;
						AircraftDlg *olACTDlg = new AircraftDlg(&rlACT,this);
						olACTDlg->m_Caption = LoadStg(IDS_STRING150);
						if (olACTDlg->DoModal() == IDOK)
						{
							rlACT.Lstu = CTime::GetCurrentTime();
							strcpy(rlACT.Useu,cgUserName);
							AfxGetApp()->DoWaitCursor(1);
							if(ogACTData.UpdateACT(&rlACT)==false)
							{
								omErrorTxt.Format("%s %d\n%s",omUpdateErrTxt,ogACTData.imLastReturnCode, ogACTData.omLastErrorMessage);
								MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
							}	
							AfxGetApp()->DoWaitCursor(-1);
						}
						delete olACTDlg;
					}
				}
				break;
			case TAB_LFZREGISTRATION:
				{
					int ilLineNo = pomTable->GetCurSel();
					if (ilLineNo == -1)
					{
						MessageBox(LoadStg(IDS_STRING148),LoadStg(IDS_STRING149),MB_ICONEXCLAMATION);
						break;
					}
					ACRDATA *prlACR = ogACRData.GetACRByUrno(pomLFZRegiViewer->omLines[ilLineNo].Urno);
					if (prlACR != NULL)
					{
						ACRDATA rlACR = *prlACR;
						LFZRegiDlg *olACRDlg = new LFZRegiDlg(&rlACR,this);
						olACRDlg->m_Caption = LoadStg(IDS_STRING150);
						if (olACRDlg->DoModal() == IDOK)
						{
							rlACR.Lstu = CTime::GetCurrentTime();
							strcpy(rlACR.Useu,cgUserName);
							AfxGetApp()->DoWaitCursor(1);
							if(ogACRData.UpdateACR(&rlACR)==false)
							{
								omErrorTxt.Format("%s %d\n%s",omUpdateErrTxt,ogACRData.imLastReturnCode, ogACRData.omLastErrorMessage);
								MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
							}	
							AfxGetApp()->DoWaitCursor(-1);
						}
						delete olACRDlg;
					}
				}
				break;
			case TAB_AIRPORT:
				{
					int ilLineNo = pomTable->GetCurSel();
					if (ilLineNo == -1)
					{
						MessageBox(LoadStg(IDS_STRING148),LoadStg(IDS_STRING149),MB_ICONEXCLAMATION);
						break;
					}
					APTDATA *prlAPT = ogAPTData.GetAPTByUrno(pomAirportViewer->omLines[ilLineNo].Urno);
					if (prlAPT != NULL)
					{
						APTDATA rlAPT = *prlAPT;
						AirportDlg *olAPTDlg = new AirportDlg(&rlAPT,this);
						olAPTDlg->m_Caption = LoadStg(IDS_STRING150);
						if (olAPTDlg->DoModal() == IDOK)
						{
							rlAPT.Lstu = CTime::GetCurrentTime();
							strcpy(rlAPT.Useu,cgUserName);
							AfxGetApp()->DoWaitCursor(1);
							if(ogAPTData.UpdateAPT(&rlAPT)==false)
							{
								omErrorTxt.Format("%s %d\n%s",omUpdateErrTxt,ogAPTData.imLastReturnCode, ogAPTData.omLastErrorMessage);
								MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
							}	
							AfxGetApp()->DoWaitCursor(-1);
						}
						delete olAPTDlg;
					}
				}
				break;
			case TAB_RUNWAY:
				{
					int ilLineNo = pomTable->GetCurSel();
					if (ilLineNo == -1)
					{
						MessageBox(LoadStg(IDS_STRING148),LoadStg(IDS_STRING149),MB_ICONEXCLAMATION);
						break;
					}
					RWYDATA *prlRWY = ogRWYData.GetRWYByUrno(pomRunwayViewer->omLines[ilLineNo].Urno);
					if (prlRWY != NULL)
					{
						RWYDATA rlRWY = *prlRWY;
						RunwayDlg *olRWYDlg = new RunwayDlg(&rlRWY,this);
						olRWYDlg->m_Caption = LoadStg(IDS_STRING150);
						if (olRWYDlg->DoModal() == IDOK)
						{
							rlRWY.Lstu = CTime::GetCurrentTime();
							strcpy(rlRWY.Useu,cgUserName);
							AfxGetApp()->DoWaitCursor(1);
							if(ogRWYData.UpdateRWY(&rlRWY)==false)
							{
								omErrorTxt.Format("%s %d\n%s",omUpdateErrTxt,ogRWYData.imLastReturnCode, ogRWYData.omLastErrorMessage);
								MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
							}	
							AfxGetApp()->DoWaitCursor(-1);
						}
						delete olRWYDlg;
					}  
				}
				break;
			case TAB_TAXIWAY:
				{
					int ilLineNo = pomTable->GetCurSel();
					if (ilLineNo == -1)
					{
						MessageBox(LoadStg(IDS_STRING148),LoadStg(IDS_STRING149),MB_ICONEXCLAMATION);
						break;
					}
					TWYDATA *prlTWY = ogTWYData.GetTWYByUrno(pomTaxiwayViewer->omLines[ilLineNo].Urno);
					if (prlTWY != NULL)
					{
						TWYDATA rlTWY = *prlTWY;
						TaxiwayDlg *olTWYDlg = new TaxiwayDlg(&rlTWY,this);
						olTWYDlg->m_Caption = LoadStg(IDS_STRING150);
						if (olTWYDlg->DoModal() == IDOK)
						{
							rlTWY.Lstu = CTime::GetCurrentTime();
							strcpy(rlTWY.Useu,cgUserName);
							AfxGetApp()->DoWaitCursor(1);
							if(ogTWYData.UpdateTWY(&rlTWY)==false)
							{
								omErrorTxt.Format("%s %d\n%s",omUpdateErrTxt,ogTWYData.imLastReturnCode, ogTWYData.omLastErrorMessage);
								MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
							}	
							AfxGetApp()->DoWaitCursor(-1);
						}
						delete olTWYDlg;
					}  
				}
				break;
			case TAB_POSITION:
				{
					int ilLineNo = pomTable->GetCurSel();
					if (ilLineNo == -1)
					{
						MessageBox(LoadStg(IDS_STRING148),LoadStg(IDS_STRING149),MB_ICONEXCLAMATION);
						break;
					}
					PSTDATA *prlPST = ogPSTData.GetPSTByUrno(pomPositionViewer->omLines[ilLineNo].Urno);
					if (prlPST != NULL)
					{
						PSTDATA rlPST = *prlPST;
						PositionDlg *olPSTDlg = new PositionDlg(&rlPST,this);
						olPSTDlg->m_Caption = LoadStg(IDS_STRING150);
						if (olPSTDlg->DoModal() == IDOK)
						{
							rlPST.Lstu = CTime::GetCurrentTime();
							strcpy(rlPST.Useu,cgUserName);
							AfxGetApp()->DoWaitCursor(1);
							if(ogPSTData.UpdatePST(&rlPST)==false)
							{
								omErrorTxt.Format("%s %d\n%s",omUpdateErrTxt,ogPSTData.imLastReturnCode, ogPSTData.omLastErrorMessage);
								MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
							}
							
							CString olBurn;
							olBurn.Format("%d",rlPST.Urno);
							UpdateBlkTable(&olPSTDlg->omBlkPtrA,&olPSTDlg->omDeleteBlkPtrA,olBurn,"PST");

							AfxGetApp()->DoWaitCursor(-1);
						}
						delete olPSTDlg;
					}  
				}
				break;
			case TAB_GATE:
				{
					int ilLineNo = pomTable->GetCurSel();
					if (ilLineNo == -1)
					{
						MessageBox(LoadStg(IDS_STRING148),LoadStg(IDS_STRING149),MB_ICONEXCLAMATION);
						break;
					}
					GATDATA *prlGAT = ogGATData.GetGATByUrno(pomGateViewer->omLines[ilLineNo].Urno);
					if (prlGAT != NULL)
					{
						GATDATA rlGAT = *prlGAT;
						GateDlg *olGATDlg = new GateDlg(&rlGAT,this);
						olGATDlg->m_Caption = LoadStg(IDS_STRING150);
						if (olGATDlg->DoModal() == IDOK)
						{
							rlGAT.Lstu = CTime::GetCurrentTime();
							strcpy(rlGAT.Useu,cgUserName);
							AfxGetApp()->DoWaitCursor(1);
							if(ogGATData.UpdateGAT(&rlGAT)==false)
							{
								omErrorTxt.Format("%s %d\n%s",omUpdateErrTxt,ogGATData.imLastReturnCode, ogGATData.omLastErrorMessage);
								MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
							}
							
							CString olBurn;
							olBurn.Format("%d",rlGAT.Urno);
							UpdateBlkTable(&olGATDlg->omBlkPtrA,&olGATDlg->omDeleteBlkPtrA,olBurn,"GAT");

							AfxGetApp()->DoWaitCursor(-1);
						}
						delete olGATDlg;
					}  
				}
				break;
			case TAB_CHECKINCOUNTER:
				{
					int ilLineNo = pomTable->GetCurSel();
					if (ilLineNo == -1)
					{
						MessageBox(LoadStg(IDS_STRING148),LoadStg(IDS_STRING149),MB_ICONEXCLAMATION);
						break;
					}
					CICDATA *prlCIC = ogCICData.GetCICByUrno(pomCheckinCounterViewer->omLines[ilLineNo].Urno);
					if (prlCIC != NULL)
					{
						CICDATA rlCIC     = *prlCIC;
						CICDATA rlCIC_Old = *prlCIC;
						CheckinCounterDlg *olCICDlg = new CheckinCounterDlg(&rlCIC,this);
						olCICDlg->m_Caption = LoadStg(IDS_STRING150);
						if (olCICDlg->DoModal() == IDOK)
						{
							AfxGetApp()->DoWaitCursor(1);

							CString olListOfData, olFieldList;
							ogCICData.CompareCIC(olListOfData, olFieldList, &rlCIC_Old, &rlCIC);
//							if(olListOfData.IsEmpty() == FALSE)
//							{
								rlCIC.Lstu = CTime::GetCurrentTime();
								strcpy(rlCIC.Useu,cgUserName);
								if(ogCICData.UpdateCIC(&rlCIC)==false)
								{
									omErrorTxt.Format("%s %d\n%s",omUpdateErrTxt,ogCICData.imLastReturnCode, ogCICData.omLastErrorMessage);
									MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
								}
//							}
							CString olBurn;
							olBurn.Format("%d",rlCIC.Urno);
							UpdateBlkTable(&olCICDlg->omBlkPtrA,&olCICDlg->omDeleteBlkPtrA,olBurn,"CIC");
							
							AfxGetApp()->DoWaitCursor(-1);
						}
						delete olCICDlg;
					}  
				}
				break;
			case TAB_BEGGAGEBELT:
				{
					int ilLineNo = pomTable->GetCurSel();
					if (ilLineNo == -1)
					{
						MessageBox(LoadStg(IDS_STRING148),LoadStg(IDS_STRING149),MB_ICONEXCLAMATION);
						break;
					}
					BLTDATA *prlBLT = ogBLTData.GetBLTByUrno(pomBeggagebeltViewer->omLines[ilLineNo].Urno);
					if (prlBLT != NULL)
					{
						BLTDATA rlBLT = *prlBLT;
						BeggagebeltDlg *olBLTDlg = new BeggagebeltDlg(&rlBLT,this);
						olBLTDlg->m_Caption = LoadStg(IDS_STRING150);
						if (olBLTDlg->DoModal() == IDOK)
						{
							rlBLT.Lstu = CTime::GetCurrentTime();
							strcpy(rlBLT.Useu,cgUserName);
							AfxGetApp()->DoWaitCursor(1);
							if(ogBLTData.UpdateBLT(&rlBLT)==false)
							{
								omErrorTxt.Format("%s %d\n%s",omUpdateErrTxt,ogBLTData.imLastReturnCode, ogBLTData.omLastErrorMessage);
								MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
							}
							
							CString olBurn;
							olBurn.Format("%d",rlBLT.Urno);
							UpdateBlkTable(&olBLTDlg->omBlkPtrA,&olBLTDlg->omDeleteBlkPtrA,olBurn,"BLT");

							AfxGetApp()->DoWaitCursor(-1);
						}
						delete olBLTDlg;
					}  
				}
				break;
			case TAB_EXIT:
				{
					int ilLineNo = pomTable->GetCurSel();
					if (ilLineNo == -1)
					{
						MessageBox(LoadStg(IDS_STRING148),LoadStg(IDS_STRING149),MB_ICONEXCLAMATION);
						break;
					}
					EXTDATA *prlEXT = ogEXTData.GetEXTByUrno(pomExitViewer->omLines[ilLineNo].Urno);
					if (prlEXT != NULL)
					{
						EXTDATA rlEXT = *prlEXT;
						ExitDlg *olEXTDlg = new ExitDlg(&rlEXT,this);
						olEXTDlg->m_Caption = LoadStg(IDS_STRING150);
						if (olEXTDlg->DoModal() == IDOK)
						{
							rlEXT.Lstu = CTime::GetCurrentTime();
							strcpy(rlEXT.Useu,cgUserName);
							AfxGetApp()->DoWaitCursor(1);
							if(ogEXTData.UpdateEXT(&rlEXT)==false)
							{
								omErrorTxt.Format("%s %d\n%s",omUpdateErrTxt,ogEXTData.imLastReturnCode, ogEXTData.omLastErrorMessage);
								MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
							}	
							AfxGetApp()->DoWaitCursor(-1);
						}
						delete olEXTDlg;
					} 
				}
				break;
			case TAB_DELAYCODE:
				{
					int ilLineNo = pomTable->GetCurSel();
					if (ilLineNo == -1)
					{
						MessageBox(LoadStg(IDS_STRING148),LoadStg(IDS_STRING149),MB_ICONEXCLAMATION);
						break;
					}
					DENDATA *prlDEN = ogDENData.GetDENByUrno(pomDelaycodeViewer->omLines[ilLineNo].Urno);
					if (prlDEN != NULL)
					{
						DENDATA rlDEN = *prlDEN;
						DelaycodeDlg *olDENDlg = new DelaycodeDlg(&rlDEN,this);
						olDENDlg->m_Caption = LoadStg(IDS_STRING150);
						if (olDENDlg->DoModal() == IDOK)
						{
							rlDEN.Lstu = CTime::GetCurrentTime();
							strcpy(rlDEN.Useu,cgUserName);
							AfxGetApp()->DoWaitCursor(1);
							if(ogDENData.UpdateDEN(&rlDEN)==false)
							{
								omErrorTxt.Format("%s %d\n%s",omUpdateErrTxt,ogDENData.imLastReturnCode, ogDENData.omLastErrorMessage);
								MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
							}	
							AfxGetApp()->DoWaitCursor(-1);
						}
						delete olDENDlg;
					}  
				}
				break;
			case TAB_TELEXADRESS:
				{
					int ilLineNo = pomTable->GetCurSel();
					if (ilLineNo == -1)
					{
						MessageBox(LoadStg(IDS_STRING148),LoadStg(IDS_STRING149),MB_ICONEXCLAMATION);
						break;
					}
					MVTDATA *prlMVT = ogMVTData.GetMVTByUrno(pomTelexadressViewer->omLines[ilLineNo].Urno);
					if (prlMVT != NULL)
					{
						MVTDATA rlMVT = *prlMVT;
						TelexadressDlg *olMVTDlg = new TelexadressDlg(&rlMVT,this);
						olMVTDlg->m_Caption = LoadStg(IDS_STRING150);
						if (olMVTDlg->DoModal() == IDOK)
						{
							rlMVT.Lstu = CTime::GetCurrentTime();
							AfxGetApp()->DoWaitCursor(1);
							strcpy(rlMVT.Useu,cgUserName);
							if(ogMVTData.UpdateMVT(&rlMVT)==false)
							{
								omErrorTxt.Format("%s %d\n%s",omUpdateErrTxt,ogMVTData.imLastReturnCode, ogMVTData.omLastErrorMessage);
								MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
							}	
							AfxGetApp()->DoWaitCursor(-1);
						}
						delete olMVTDlg;
					}  
				}
				break;
			case TAB_TRAFFICTYPE:
				{
					int ilLineNo = pomTable->GetCurSel();
					if (ilLineNo == -1)
					{
						MessageBox(LoadStg(IDS_STRING148),LoadStg(IDS_STRING149),MB_ICONEXCLAMATION);
						break;
					}
					NATDATA *prlNAT = ogNATData.GetNATByUrno(pomTraffictypeViewer->omLines[ilLineNo].Urno);
					if (prlNAT != NULL)
					{
						NATDATA rlNAT = *prlNAT;
						TraffictypeDlg *olNATDlg = new TraffictypeDlg(&rlNAT,this);
						olNATDlg->m_Caption = LoadStg(IDS_STRING150);
						if (olNATDlg->DoModal() == IDOK)
						{
							rlNAT.Lstu = CTime::GetCurrentTime();
							strcpy(rlNAT.Useu,cgUserName);
							AfxGetApp()->DoWaitCursor(1);
							if(ogNATData.UpdateNAT(&rlNAT)==false)
							{
								omErrorTxt.Format("%s %d\n%s",omUpdateErrTxt,ogNATData.imLastReturnCode, ogNATData.omLastErrorMessage);
								MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
							}	
							AfxGetApp()->DoWaitCursor(-1);
						}
						delete olNATDlg;
					}  
				}
				break;
			case TAB_HANDLINGAGENT:
				{
					int ilLineNo = pomTable->GetCurSel();
					if (ilLineNo == -1)
					{
						MessageBox(LoadStg(IDS_STRING148),LoadStg(IDS_STRING149),MB_ICONEXCLAMATION);
						break;
					}
					HAGDATA *prlHAG = ogHAGData.GetHAGByUrno(pomHandlingagentViewer->omLines[ilLineNo].Urno);
					if (prlHAG != NULL)
					{
						HAGDATA rlHAG = *prlHAG;
						HandlingagentDlg *olHAGDlg = new HandlingagentDlg(&rlHAG,this);
						olHAGDlg->m_Caption = LoadStg(IDS_STRING150);
						if (olHAGDlg->DoModal() == IDOK)
						{
							rlHAG.Lstu = CTime::GetCurrentTime();
							strcpy(rlHAG.Useu,cgUserName);
							AfxGetApp()->DoWaitCursor(1);
							if(ogHAGData.UpdateHAG(&rlHAG)==false)
							{
								omErrorTxt.Format("%s %d\n%s",omUpdateErrTxt,ogHAGData.imLastReturnCode, ogHAGData.omLastErrorMessage);
								MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
							}	
							AfxGetApp()->DoWaitCursor(-1);
						}
						delete olHAGDlg;
					}
				}
				break;
			case TAB_WAITINGROOM:
				{
					int ilLineNo = pomTable->GetCurSel();
					if (ilLineNo == -1)
					{
						MessageBox(LoadStg(IDS_STRING148),LoadStg(IDS_STRING149),MB_ICONEXCLAMATION);
						break;
					}
					WRODATA *prlWRO = ogWROData.GetWROByUrno(pomWaitingroomViewer->omLines[ilLineNo].Urno);
					if (prlWRO != NULL)
					{
						WRODATA rlWRO = *prlWRO;
						WaitingroomDlg *olWRODlg = new WaitingroomDlg(&rlWRO,this);
						olWRODlg->m_Caption = LoadStg(IDS_STRING150);
						if (olWRODlg->DoModal() == IDOK)
						{
							rlWRO.Lstu = CTime::GetCurrentTime();
							strcpy(rlWRO.Useu,cgUserName);
							AfxGetApp()->DoWaitCursor(1);
							if(ogWROData.UpdateWRO(&rlWRO)==false)
							{
								omErrorTxt.Format("%s %d\n%s",omUpdateErrTxt,ogWROData.imLastReturnCode, ogWROData.omLastErrorMessage);
								MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
							}	
							else
							{
								CString olBurn;
								olBurn.Format("%d",rlWRO.Urno);
								UpdateBlkTable(&olWRODlg->omBlkPtrA,&olWRODlg->omDeleteBlkPtrA,olBurn,"WRO");
							}

							AfxGetApp()->DoWaitCursor(-1);
						}
						delete olWRODlg;
					}  
				}
				break;
			case TAB_HANDLINGTYPE:
				{
					int ilLineNo = pomTable->GetCurSel();
					if (ilLineNo == -1)
					{
						MessageBox(LoadStg(IDS_STRING148),LoadStg(IDS_STRING149),MB_ICONEXCLAMATION);
						break;
					}
					HTYDATA *prlHTY = ogHTYData.GetHTYByUrno(pomHandlingtypeViewer->omLines[ilLineNo].Urno);
					if (prlHTY != NULL)
					{
						HTYDATA rlHTY = *prlHTY;
						HandlingtypeDlg *olHTYDlg = new HandlingtypeDlg(&rlHTY,this);
						olHTYDlg->m_Caption = LoadStg(IDS_STRING150);
						if (olHTYDlg->DoModal() == IDOK)
						{
							rlHTY.Lstu = CTime::GetCurrentTime();
							strcpy(rlHTY.Useu,cgUserName);
							AfxGetApp()->DoWaitCursor(1);
							if(ogHTYData.UpdateHTY(&rlHTY)==false)
							{
								omErrorTxt.Format("%s %d\n%s",omUpdateErrTxt,ogHTYData.imLastReturnCode, ogHTYData.omLastErrorMessage);
								MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
							}	
							AfxGetApp()->DoWaitCursor(-1);
						}
						delete olHTYDlg;
					}  
				}
				break;
			case TAB_SERVICETYPE:
				{
					int ilLineNo = pomTable->GetCurSel();
					if (ilLineNo == -1)
					{
						MessageBox(LoadStg(IDS_STRING148),LoadStg(IDS_STRING149),MB_ICONEXCLAMATION);
						break;
					}
					STYDATA *prlSTY = ogSTYData.GetSTYByUrno(pomServicetypeViewer->omLines[ilLineNo].Urno);
					if (prlSTY != NULL)
					{
						STYDATA rlSTY = *prlSTY;
						ServicetypeDlg *olSTYDlg = new ServicetypeDlg(&rlSTY,this);
						olSTYDlg->m_Caption = LoadStg(IDS_STRING150);
						if (olSTYDlg->DoModal() == IDOK)
						{
							rlSTY.Lstu = CTime::GetCurrentTime();
							strcpy(rlSTY.Useu,cgUserName);
							AfxGetApp()->DoWaitCursor(1);
							if(ogSTYData.UpdateSTY(&rlSTY)==false)
							{
								omErrorTxt.Format("%s %d\n%s",omUpdateErrTxt,ogSTYData.imLastReturnCode, ogSTYData.omLastErrorMessage);
								MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
							}	
							AfxGetApp()->DoWaitCursor(-1);
						}
						delete olSTYDlg;
					}  
				}
				break;
			case TAB_FIDSCOMMAND:
				{		
					int ilLineNo = pomTable->GetCurSel();
					if (ilLineNo == -1)
					{
						MessageBox(LoadStg(IDS_STRING148),LoadStg(IDS_STRING149),MB_ICONEXCLAMATION);
						break;
					}
					FIDDATA *prlFID = ogFIDData.GetFIDByUrno(pomFidsCommandViewer->omLines[ilLineNo].Urno);
					if (prlFID != NULL)
					{
						FIDDATA rlFID = *prlFID;
						FidsCommandDlg *olFIDDlg = new FidsCommandDlg(&rlFID,this);
						olFIDDlg->m_Caption = LoadStg(IDS_STRING150);
						if (olFIDDlg->DoModal() == IDOK)
						{
							rlFID.Lstu = CTime::GetCurrentTime();
							strcpy(rlFID.Useu,cgUserName);
							AfxGetApp()->DoWaitCursor(1);
							if(ogFIDData.UpdateFID(&rlFID)==false)
							{
								omErrorTxt.Format("%s %d\n%s",omUpdateErrTxt,ogFIDData.imLastReturnCode, ogFIDData.omLastErrorMessage);
								MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
							}	
							AfxGetApp()->DoWaitCursor(-1);
						}
						delete olFIDDlg;
					}
				}
				break;
			case TAB_ERGVERKEHRSARTEN:
				{
					int ilLineNo = pomTable->GetCurSel();
					if (ilLineNo == -1)
					{
						MessageBox(LoadStg(IDS_STRING148),LoadStg(IDS_STRING149),MB_ICONEXCLAMATION);
						break;
					}
					SPHDATA *prlSph = ogSphData.GetSphByUrno(pomErgVerkehrsartenViewer->omLines[ilLineNo].Urno);
					if (prlSph != NULL)
					{
						SPHDATA rlSph = *prlSph;
						ErgVerkehrsartenDlg *olSphDlg = new ErgVerkehrsartenDlg(&rlSph,this);
						olSphDlg->m_Caption = LoadStg(IDS_STRING150);
						if (olSphDlg->DoModal() == IDOK)
						{
							strcpy(rlSph.Eart,"V");
 							rlSph.Lstu = CTime::GetCurrentTime();
							strcpy(rlSph.Useu,cgUserName);
							AfxGetApp()->DoWaitCursor(1);
							if(ogSphData.Update(&rlSph)==false)
							{
								omErrorTxt.Format("%s %d\n%s",omUpdateErrTxt,ogSphData.imLastReturnCode, ogSphData.omLastErrorMessage);
								MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
							}	
							AfxGetApp()->DoWaitCursor(-1);
						}
						delete olSphDlg;
					}
				}
				break;
			case TAB_STANDARDVERKETTUNGEN:
				{
					int ilLineNo = pomTable->GetCurSel();
					if (ilLineNo == -1)
					{
						MessageBox(LoadStg(IDS_STRING148),LoadStg(IDS_STRING149),MB_ICONEXCLAMATION);
						break;
					}
					STRDATA *prlStr = ogStrData.GetStrByUrno(pomStandardverkettungenViewer->omLines[ilLineNo].Urno);
					if (prlStr != NULL)
					{
						STRDATA rlStr = *prlStr;
						StandardverkettungenDlg *olStrDlg = new StandardverkettungenDlg(&rlStr,this);
						olStrDlg->m_Caption = LoadStg(IDS_STRING150);
						if (olStrDlg->DoModal() == IDOK)
						{
							rlStr.Lstu = CTime::GetCurrentTime();
							strcpy(rlStr.Useu,cgUserName);
							AfxGetApp()->DoWaitCursor(1);
							if(ogStrData.Update(&rlStr)==false)
							{
								omErrorTxt.Format("%s %d\n%s",omUpdateErrTxt,ogStrData.imLastReturnCode, ogStrData.omLastErrorMessage);
								MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
							}	
							AfxGetApp()->DoWaitCursor(-1);
						}
						delete olStrDlg;
					}
				}
				break;
			case TAB_FLUGPLANSAISON:
				{
					int ilLineNo = pomTable->GetCurSel();
					if (ilLineNo == -1)
					{
						MessageBox(LoadStg(IDS_STRING148),LoadStg(IDS_STRING149),MB_ICONEXCLAMATION);
						break;
					}
					SEADATA *prlSea = ogSeaData.GetSeaByUrno(pomFlugplansaisonViewer->omLines[ilLineNo].Urno);
					if (prlSea != NULL)
					{
						SEADATA rlSea = *prlSea;
						FlugplansaisonDlg *olSeaDlg = new FlugplansaisonDlg(&rlSea,this);
						olSeaDlg->m_Caption = LoadStg(IDS_STRING150);
						if (olSeaDlg->DoModal() == IDOK)
						{
							rlSea.Lstu = CTime::GetCurrentTime();
							strcpy(rlSea.Useu,cgUserName);
							AfxGetApp()->DoWaitCursor(1);
							if(ogSeaData.Update(&rlSea)==false)
							{
								omErrorTxt.Format("%s %d\n%s",omUpdateErrTxt,ogSeaData.imLastReturnCode, ogSeaData.omLastErrorMessage);
								MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
							}	
							AfxGetApp()->DoWaitCursor(-1);
						}
						delete olSeaDlg;
					}
				}
				break;
			case TAB_LEISTUNGSKATALOG:
				{
					int ilLineNo = pomTable->GetCurSel();
					if (ilLineNo == -1)
					{
						MessageBox(LoadStg(IDS_STRING148),LoadStg(IDS_STRING149),MB_ICONEXCLAMATION);
						break;
					}
					GHSDATA *prlGhs = ogGhsData.GetGhsByUrno(pomLeistungskatalogViewer->omLines[ilLineNo].Urno);
					if (prlGhs != NULL)
					{
						GHSDATA rlGhs = *prlGhs;
						LeistungskatalogDlg *olGhsDlg = new LeistungskatalogDlg(&rlGhs,this);
						olGhsDlg->m_Caption = LoadStg(IDS_STRING150);
						if (olGhsDlg->DoModal() == IDOK)
						{
							rlGhs.Lstu = CTime::GetCurrentTime();
							strcpy(rlGhs.Useu,cgUserName);
							AfxGetApp()->DoWaitCursor(1);
							if(ogGhsData.Update(&rlGhs)==false)
							{
								omErrorTxt.Format("%s %d\n%s",omUpdateErrTxt,ogGhsData.imLastReturnCode, ogGhsData.omLastErrorMessage);
								MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
							}	
							AfxGetApp()->DoWaitCursor(-1);
						}
						delete olGhsDlg;
					}
				}
				break;
			case TAB_QUALIFIKATIONEN:
				{
					int ilLineNo = pomTable->GetCurSel();
					if (ilLineNo == -1)
					{
						MessageBox(LoadStg(IDS_STRING148),LoadStg(IDS_STRING149),MB_ICONEXCLAMATION);
						break;
					}
					PERDATA *prlPer = ogPerData.GetPerByUrno(pomQualifikationenViewer->omLines[ilLineNo].Urno);
					if (prlPer != NULL)
					{
						PERDATA rlPer = *prlPer;
						QualifikationenDlg *olPerDlg = new QualifikationenDlg(&rlPer,this);
						olPerDlg->m_Caption = LoadStg(IDS_STRING150);
						if (olPerDlg->DoModal() == IDOK)
						{
							rlPer.Lstu = CTime::GetCurrentTime();
							strcpy(rlPer.Useu,cgUserName);
							AfxGetApp()->DoWaitCursor(1);
							if(ogPerData.Update(&rlPer)==false)
							{
								omErrorTxt.Format("%s %d\n%s",omUpdateErrTxt,ogPerData.imLastReturnCode, ogPerData.omLastErrorMessage);
								MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
							}	
							AfxGetApp()->DoWaitCursor(-1);
						}
						delete olPerDlg;
					}
				}
				break;
			case TAB_ORGANISATIONSEINHEITEN:
				{
					int ilLineNo = pomTable->GetCurSel();
					if (ilLineNo == -1)
					{
						MessageBox(LoadStg(IDS_STRING148),LoadStg(IDS_STRING149),MB_ICONEXCLAMATION);
						break;
					}
					ORGDATA *prlOrg = ogOrgData.GetOrgByUrno(pomOrganisationseinheitenViewer->omLines[ilLineNo].Urno);
					if (prlOrg != NULL)
					{
						ORGDATA rlOrg = *prlOrg;
						OrganisationseinheitenDlg *olOrgDlg = new OrganisationseinheitenDlg(&rlOrg,this);
						olOrgDlg->m_Caption = LoadStg(IDS_STRING150);
						if (olOrgDlg->DoModal() == IDOK)
						{
							rlOrg.Lstu = CTime::GetCurrentTime();
							strcpy(rlOrg.Useu,cgUserName);
							AfxGetApp()->DoWaitCursor(1);
							if(ogOrgData.Update(&rlOrg)==false)
							{
								omErrorTxt.Format("%s %d\n%s",omUpdateErrTxt,ogOrgData.imLastReturnCode, ogOrgData.omLastErrorMessage);
								MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
							}	
							AfxGetApp()->DoWaitCursor(-1);
						}
						delete olOrgDlg;
					}
				}
				break;
			case TAB_WEGEZEITEN:				
				{
					int ilLineNo = pomTable->GetCurSel();
					if (ilLineNo == -1)
					{
						MessageBox(LoadStg(IDS_STRING148),LoadStg(IDS_STRING149),MB_ICONEXCLAMATION);
						break;
					}
					WAYDATA *prlWay = ogWayData.GetWayByUrno(pomWegezeitenViewer->omLines[ilLineNo].Urno);
					if (prlWay != NULL)
					{
						WAYDATA rlWay = *prlWay;
						WegezeitenDlg *polWayDlg = new WegezeitenDlg(&rlWay,this);
						polWayDlg->m_Caption = LoadStg(IDS_STRING150);
						if (polWayDlg->DoModal() == IDOK)
						{
							rlWay.Lstu = CTime::GetCurrentTime();
							strcpy(rlWay.Useu,cgUserName);
							AfxGetApp()->DoWaitCursor(1);
							if(ogWayData.Update(&rlWay)==false)
							{
								omErrorTxt.Format("%s %d\n%s",omUpdateErrTxt,ogWayData.imLastReturnCode, ogWayData.omLastErrorMessage);
								MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
							}	
							AfxGetApp()->DoWaitCursor(-1);
						}
						delete polWayDlg;
					}
				}
				break;
			case TAB_FUNKTIONEN:				
				{
					int ilLineNo = pomTable->GetCurSel();
					if (ilLineNo == -1)
					{
						MessageBox(LoadStg(IDS_STRING148),LoadStg(IDS_STRING149),MB_ICONEXCLAMATION);
						break;
					}
					PFCDATA *prlPfc = ogPfcData.GetPfcByUrno(pomFunktionenViewer->omLines[ilLineNo].Urno);
					if (prlPfc != NULL)
					{
						PFCDATA rlPfc = *prlPfc;
						FunktionenDlg *polPfcDlg = new FunktionenDlg(&rlPfc,this);
						polPfcDlg->m_Caption = LoadStg(IDS_STRING150);
						if (polPfcDlg->DoModal() == IDOK)
						{
							rlPfc.Lstu = CTime::GetCurrentTime();
							strcpy(rlPfc.Useu,cgUserName);
							AfxGetApp()->DoWaitCursor(1);
							if(ogPfcData.Update(&rlPfc)==false)
							{
								omErrorTxt.Format("%s %d\n%s",omUpdateErrTxt,ogPfcData.imLastReturnCode, ogPfcData.omLastErrorMessage);
								MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
							}	
							AfxGetApp()->DoWaitCursor(-1);
						}
						delete polPfcDlg;
					}
				}
				break;
			case TAB_ARBEITSVERTRAGSARTEN:	
				{
					int ilLineNo = pomTable->GetCurSel();
					if (ilLineNo == -1)
					{
						MessageBox(LoadStg(IDS_STRING148),LoadStg(IDS_STRING149),MB_ICONEXCLAMATION);
						break;
					}
					COTDATA *prlCot = ogCotData.GetCotByUrno(pomArbeitsvertragsartenViewer->omLines[ilLineNo].Urno);
					if (prlCot != NULL)
					{
						COTDATA rlCot = *prlCot;
						ArbeitsvertragsartenDlg *polCotDlg = new ArbeitsvertragsartenDlg(&rlCot,this);
						polCotDlg->m_Caption = LoadStg(IDS_STRING150);
						if (polCotDlg->DoModal() == IDOK)
						{
							rlCot.Lstu = CTime::GetCurrentTime();
							strcpy(rlCot.Useu,cgUserName);
							AfxGetApp()->DoWaitCursor(1);
							if(ogCotData.Update(&rlCot)==false)
							{
								omErrorTxt.Format("%s %d\n%s",omUpdateErrTxt,ogCotData.imLastReturnCode, ogCotData.omLastErrorMessage);
								MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
							}	
							AfxGetApp()->DoWaitCursor(-1);
						}
						delete polCotDlg;
					}
				}
				break;
			case TAB_BEWERTUNGSFAKTOREN:		
				{
					int ilLineNo = pomTable->GetCurSel();
					if (ilLineNo == -1)
					{
						MessageBox(LoadStg(IDS_STRING148),LoadStg(IDS_STRING149),MB_ICONEXCLAMATION);
						break;
					}
					ASFDATA *prlAsf = ogAsfData.GetAsfByUrno(pomBewertungsfaktorenViewer->omLines[ilLineNo].Urno);
					if (prlAsf != NULL)
					{
						ASFDATA rlAsf = *prlAsf;
						BewertungsfaktorenDlg *polAsfDlg = new BewertungsfaktorenDlg(&rlAsf,this);
						polAsfDlg->m_Caption = LoadStg(IDS_STRING150);
						if (polAsfDlg->DoModal() == IDOK)
						{
							rlAsf.Lstu = CTime::GetCurrentTime();
							strcpy(rlAsf.Useu,cgUserName);
							AfxGetApp()->DoWaitCursor(1);
							if(ogAsfData.Update(&rlAsf)==false)
							{
								omErrorTxt.Format("%s %d\n%s",omUpdateErrTxt,ogAsfData.imLastReturnCode, ogAsfData.omLastErrorMessage);
								MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
							}	
							AfxGetApp()->DoWaitCursor(-1);
						}
						delete polAsfDlg;
					}
				}
				break;
			case TAB_BASISSCHICHTEN:			
				{
					int ilLineNo = pomTable->GetCurSel();
					if (ilLineNo == -1)
					{
						MessageBox(LoadStg(IDS_STRING148),LoadStg(IDS_STRING149),MB_ICONEXCLAMATION);
						break;
					}
					BSDDATA *prlBsd = ogBsdData.GetBsdByUrno(pomBasisschichtenViewer->omLines[ilLineNo].Urno);
					if (prlBsd != NULL)
					{
						BSDDATA rlBsd = *prlBsd;
						BasisschichtenDlg *polBsdDlg = new BasisschichtenDlg(&rlBsd,true,this);
						polBsdDlg->m_Caption = LoadStg(IDS_STRING150);
						polBsdDlg->pomStatus = &m_Status;
						if (polBsdDlg->DoModal() == IDOK)
						{
							rlBsd.Lstu = CTime::GetCurrentTime();
							strcpy(rlBsd.Useu,cgUserName);
							AfxGetApp()->DoWaitCursor(1);
							if(ogBsdData.Update(&rlBsd)==false)
							{
								omErrorTxt.Format("%s %d\n%s",omUpdateErrTxt,ogBsdData.imLastReturnCode, ogBsdData.omLastErrorMessage);
								MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
							}	
							AfxGetApp()->DoWaitCursor(-1);
						}
						delete polBsdDlg;
					}
				}
				break;
			case TAB_DIENSTEUNDABWESENHEITEN:
				{
					int ilLineNo = pomTable->GetCurSel();
					if (ilLineNo == -1)
					{
						MessageBox(LoadStg(IDS_STRING148),LoadStg(IDS_STRING149),MB_ICONEXCLAMATION);
						break;
					}
					ODADATA *prlOda = ogOdaData.GetOdaByUrno(pomDiensteUndAbwesenheitenViewer->omLines[ilLineNo].Urno);
					if (prlOda != NULL)
					{
						ODADATA rlOda = *prlOda;
						DiensteUndAbwesenheitenDlg *polOdaDlg = new DiensteUndAbwesenheitenDlg(&rlOda,this);
						polOdaDlg->m_Caption = LoadStg(IDS_STRING150);
						if (polOdaDlg->DoModal() == IDOK)
						{
							rlOda.Lstu = CTime::GetCurrentTime();
							strcpy(rlOda.Useu,cgUserName);
							AfxGetApp()->DoWaitCursor(1);
							if(ogOdaData.Update(&rlOda)==false)
							{
								omErrorTxt.Format("%s %d\n%s",omUpdateErrTxt,ogOdaData.imLastReturnCode, ogOdaData.omLastErrorMessage);
								MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
							}	
							AfxGetApp()->DoWaitCursor(-1);
						}
						delete polOdaDlg;
					}
				}
				break;
			case TAB_FAHRGEMEINSCHAFTEN:		
				{
					int ilLineNo = pomTable->GetCurSel();
					if (ilLineNo == -1)
					{
						MessageBox(LoadStg(IDS_STRING148),LoadStg(IDS_STRING149),MB_ICONEXCLAMATION);
						break;
					}
					TEADATA *prlTea = ogTeaData.GetTeaByUrno(pomFahrgemeinschaftenViewer->omLines[ilLineNo].Urno);
					if (prlTea != NULL)
					{
						TEADATA rlTea = *prlTea;
						FahrgemeinschaftenDlg *polTeaDlg = new FahrgemeinschaftenDlg(&rlTea,this);
						polTeaDlg->m_Caption = LoadStg(IDS_STRING150);
						if (polTeaDlg->DoModal() == IDOK)
						{
							rlTea.Lstu = CTime::GetCurrentTime();
							strcpy(rlTea.Useu,cgUserName);
							AfxGetApp()->DoWaitCursor(1);
							if(ogTeaData.Update(&rlTea)==false)
							{
								omErrorTxt.Format("%s %d\n%s",omUpdateErrTxt,ogTeaData.imLastReturnCode, ogTeaData.omLastErrorMessage);
								MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
							}	
							AfxGetApp()->DoWaitCursor(-1);
						}
						delete polTeaDlg;
					}
				}
				break;
			case TAB_ARBEITSGRUPPEN:		
				{
					int ilLineNo = pomTable->GetCurSel();
					if (ilLineNo == -1)
					{
						MessageBox(LoadStg(IDS_STRING148),LoadStg(IDS_STRING149),MB_ICONEXCLAMATION);
						break;
					}
					WGPDATA *prlWgp = ogWgpData.GetWgpByUrno(pomArbeitsgruppenViewer->omLines[ilLineNo].Urno);
					if (prlWgp != NULL)
					{
						WGPDATA rlWgp = *prlWgp;
						CArbeitsgruppenDlg *polWgpDlg = new CArbeitsgruppenDlg(&rlWgp,this);
						polWgpDlg->m_Caption = LoadStg(IDS_STRING150);
						if (polWgpDlg->DoModal() == IDOK)
						{
							rlWgp.Lstu = CTime::GetCurrentTime();
							strcpy(rlWgp.Useu,cgUserName);
							AfxGetApp()->DoWaitCursor(1);
							if(ogWgpData.Update(&rlWgp)==false)
							{
								omErrorTxt.Format("%s %d\n%s",omUpdateErrTxt,ogWgpData.imLastReturnCode, ogWgpData.omLastErrorMessage);
								MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
							}	
							AfxGetApp()->DoWaitCursor(-1);
						}
						delete polWgpDlg;
					}
				}
				break;
			case TAB_MITARBEITERSTAMM:		
				{
					int ilLineNo = pomTable->GetCurSel();
					if (ilLineNo == -1)
					{
						MessageBox(LoadStg(IDS_STRING148),LoadStg(IDS_STRING149),MB_ICONEXCLAMATION);
						break;
					}
					STFDATA *prlStf = ogStfData.GetStfByUrno(pomMitarbeiterstammViewer->omLines[ilLineNo].Urno);
					if (prlStf != NULL)
					{
						char pclWhere[1024]="";
						int i;
						sprintf(pclWhere, " WHERE SURN = %ld", prlStf->Urno);
						AfxGetApp()->DoWaitCursor(1);
						ogSorData.Read(pclWhere);
						ogSpfData.Read(pclWhere);
						ogSpeData.Read(pclWhere);
						ogScoData.Read(pclWhere);
						ogSteData.Read(pclWhere);
						ogSwgData.Read(pclWhere);
						AfxGetApp()->DoWaitCursor(-1);
						
						STFDATA rlStf = *prlStf;
						MitarbeiterStammDlg *polStfDlg = new MitarbeiterStammDlg(&rlStf, 
																				  &ogSorData.omData,
																				  &ogSpfData.omData,
																				  &ogSpeData.omData,
																				  &ogScoData.omData,
																				  &ogSteData.omData,
																				  &ogSwgData.omData,
																				  this);
						polStfDlg->m_Caption = LoadStg(IDS_STRING150);
						if (polStfDlg->DoModal() == IDOK)
						{
							rlStf.Lstu = CTime::GetCurrentTime();
							strcpy(rlStf.Useu,cgUserName);
							AfxGetApp()->DoWaitCursor(1);
							if(ogStfData.Update(&rlStf)==false)
							{
								omErrorTxt.Format("%s %d\n%s",omUpdateErrTxt,ogStfData.imLastReturnCode, ogStfData.omLastErrorMessage);
								MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
							}	
							AfxGetApp()->DoWaitCursor(-1);
							for(i = 0; i < ogSorData.omData.GetSize(); i++)
							{
								SORDATA *prlSor;// = ogSorData.GetSorByUrno(ogSorData.omData[i].Urno);
								if(ogSorData.omData[i].IsChanged == DATA_NEW)
								{
									prlSor = &ogSorData.omData[i];
									ogSorData.omData[i].Urno = ogBasicData.GetNextUrno();	//new URNO
									ogSorData.omData[i].Surn = rlStf.Urno;
								}
								else
								{
									prlSor = ogSorData.GetSorByUrno(ogSorData.omData[i].Urno);
								}
								if(ogSorData.Save(prlSor)==false)
								{
									omErrorTxt.Format("%s %d\n%s",omInsertErrTxt,ogSorData.imLastReturnCode, ogSorData.omLastErrorMessage);
									MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
								}
							}
							ogSorData.omUrnoMap.RemoveAll();
							ogSorData.omData.DeleteAll();
							for(i = 0; i < ogSpfData.omData.GetSize(); i++)
							{
								SPFDATA *prlSpf;// = ogSpfData.GetSpfByUrno(ogSpfData.omData[i].Urno);
								if(ogSpfData.omData[i].IsChanged == DATA_NEW)
								{
									prlSpf = &ogSpfData.omData[i];
									ogSpfData.omData[i].Urno = ogBasicData.GetNextUrno();	//new URNO
									ogSpfData.omData[i].Surn = rlStf.Urno;
								}
								else
								{
									prlSpf = ogSpfData.GetSpfByUrno(ogSpfData.omData[i].Urno);
								}
								if(ogSpfData.Save(prlSpf)==false)
								{
									omErrorTxt.Format("%s %d\n%s",omInsertErrTxt,ogSpfData.imLastReturnCode, ogSpfData.omLastErrorMessage);
									MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
								}
							}
							for(i = 0; i < ogSpeData.omData.GetSize(); i++)
							{
								SPEDATA *prlSpe;// = ogSpeData.GetSpeByUrno(ogSpeData.omData[i].Urno);
								if(ogSpeData.omData[i].IsChanged == DATA_NEW)
								{
									prlSpe = &ogSpeData.omData[i];
									ogSpeData.omData[i].Urno = ogBasicData.GetNextUrno();	//new URNO
									ogSpeData.omData[i].Surn = rlStf.Urno;
								}
								else
								{
									prlSpe = ogSpeData.GetSpeByUrno(ogSpeData.omData[i].Urno);
								}
								if(ogSpeData.Save(prlSpe)==false)
								{
									omErrorTxt.Format("%s %d\n%s",omInsertErrTxt,ogSpeData.imLastReturnCode, ogSpeData.omLastErrorMessage);
									MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
								}
							}
							for(i = 0; i < ogScoData.omData.GetSize(); i++)
							{
								SCODATA *prlSco;// = ogScoData.GetScoByUrno(ogScoData.omData[i].Urno);
								if(ogScoData.omData[i].IsChanged == DATA_NEW)
								{
									prlSco = &ogScoData.omData[i];
									ogScoData.omData[i].Urno = ogBasicData.GetNextUrno();	//new URNO
									ogScoData.omData[i].Surn = rlStf.Urno;
								}
								else
								{
									prlSco = ogScoData.GetScoByUrno(ogScoData.omData[i].Urno);
								}
								if(ogScoData.Save(prlSco)==false)
								{
									omErrorTxt.Format("%s %d\n%s",omInsertErrTxt,ogScoData.imLastReturnCode, ogScoData.omLastErrorMessage);
									MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
								}
							}
							for(i = 0; i < ogSteData.omData.GetSize(); i++)
							{
								STEDATA *prlSte;// = ogSteData.GetSteByUrno(ogSteData.omData[i].Urno);
								if(ogSteData.omData[i].IsChanged == DATA_NEW)
								{
									prlSte = &ogSteData.omData[i];
									ogSteData.omData[i].Urno = ogBasicData.GetNextUrno();	//new URNO
									ogSteData.omData[i].Surn = rlStf.Urno;
								}
								else
								{
									prlSte = ogSteData.GetSteByUrno(ogSteData.omData[i].Urno);
								}
								if(ogSteData.Save(prlSte)==false)
								{
									omErrorTxt.Format("%s %d\n%s",omInsertErrTxt,ogSteData.imLastReturnCode, ogSteData.omLastErrorMessage);
									MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
								}
							}
							for(i = 0; i < ogSwgData.omData.GetSize(); i++)
							{
								SWGDATA *prlSwg;// = ogSwgData.GetSwgByUrno(ogSwgData.omData[i].Urno);
								if(ogSwgData.omData[i].IsChanged == DATA_NEW)
								{
									prlSwg = &ogSwgData.omData[i];
									ogSwgData.omData[i].Urno = ogBasicData.GetNextUrno();	//new URNO
									ogSwgData.omData[i].Surn = rlStf.Urno;
								}
								else
								{
									prlSwg = ogSwgData.GetSwgByUrno(ogSwgData.omData[i].Urno);
								}
								if(ogSwgData.Save(prlSwg)==false)
								{
									omErrorTxt.Format("%s %d\n%s",omInsertErrTxt,ogSwgData.imLastReturnCode, ogSwgData.omLastErrorMessage);
									MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
								}
							}
						}
						delete polStfDlg;
					}
				}
				break;
			case TAB_ERGABFERTIGUNGSARTEN:		
				{
					int ilLineNo = pomTable->GetCurSel();
					if (ilLineNo == -1)
					{
						MessageBox(LoadStg(IDS_STRING148),LoadStg(IDS_STRING149),MB_ICONEXCLAMATION);
						break;
					}
					SPHDATA *prlSph = ogSphData.GetSphByUrno(pomErgAbfertigungsartenViewer->omLines[ilLineNo].Urno);
					if (prlSph != NULL)
					{
						SPHDATA rlSph = *prlSph;
						ErgAbfertigungsartenDlg *olSphDlg = new ErgAbfertigungsartenDlg(&rlSph,this);
						olSphDlg->m_Caption = LoadStg(IDS_STRING150);
						if (olSphDlg->DoModal() == IDOK)
						{
							strcpy(rlSph.Eart,"A");
							rlSph.Lstu = CTime::GetCurrentTime();
							strcpy(rlSph.Useu,cgUserName);
							AfxGetApp()->DoWaitCursor(1);
							if(ogSphData.Update(&rlSph)==false)
							{
								omErrorTxt.Format("%s %d\n%s",omUpdateErrTxt,ogSphData.imLastReturnCode, ogSphData.omLastErrorMessage);
								MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
							}	
							AfxGetApp()->DoWaitCursor(-1);
						}
						delete olSphDlg;
					}

				}
				break;
			case TAB_REDUKTIONEN:			
				{
					int ilLineNo = pomTable->GetCurSel();
					if (ilLineNo == -1)
					{
						MessageBox(LoadStg(IDS_STRING148),LoadStg(IDS_STRING149),MB_ICONEXCLAMATION);
						break;
					}
					PRCDATA *prlPrc = ogPrcData.GetPrcByUrno(pomReduktionenViewer->omLines[ilLineNo].Urno);
					if (prlPrc != NULL)
					{
						PRCDATA rlPrc = *prlPrc;
						ReduktionenDlg *polPrcDlg = new ReduktionenDlg(&rlPrc,this);
						polPrcDlg->m_Caption = LoadStg(IDS_STRING150);
						if (polPrcDlg->DoModal() == IDOK)
						{
							rlPrc.Lstu = CTime::GetCurrentTime();
							strcpy(rlPrc.Useu,cgUserName);
							AfxGetApp()->DoWaitCursor(1);
							if(ogPrcData.Update(&rlPrc)==false)
							{
								omErrorTxt.Format("%s %d\n%s",omUpdateErrTxt,ogPrcData.imLastReturnCode, ogPrcData.omLastErrorMessage);
								MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
							}	
							AfxGetApp()->DoWaitCursor(-1);
						}
						delete polPrcDlg;
					}
				}
				break;
			case TAB_GERAETEGRUPPEN:			
				{
					int ilLineNo = pomTable->GetCurSel();
					if (ilLineNo == -1)
					{
						MessageBox(LoadStg(IDS_STRING148),LoadStg(IDS_STRING149),MB_ICONEXCLAMATION);
						break;
					}
					GEGDATA *prlGeg = ogGegData.GetGegByUrno(pomGeraetegruppenViewer->omLines[ilLineNo].Urno);
					if (prlGeg != NULL)
					{
						GEGDATA rlGeg = *prlGeg;
						GeraeteGruppenDlg *polGegDlg = new GeraeteGruppenDlg(&rlGeg,this);
						polGegDlg->m_Caption = LoadStg(IDS_STRING150);
						if (polGegDlg->DoModal() == IDOK)
						{
							rlGeg.Lstu = CTime::GetCurrentTime();
							strcpy(rlGeg.Useu,cgUserName);
							AfxGetApp()->DoWaitCursor(1);
							if(ogGegData.Update(&rlGeg)==false)
							{
								omErrorTxt.Format("%s %d\n%s",omUpdateErrTxt,ogGegData.imLastReturnCode, ogGegData.omLastErrorMessage);
								MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
							}	
							AfxGetApp()->DoWaitCursor(-1);
						}
						delete polGegDlg;
					}
				}
				break;
			case TAB_ORGANIZER:			
				{
					int ilLineNo = pomTable->GetCurSel();
					if (ilLineNo == -1)
					{
						MessageBox(LoadStg(IDS_STRING148),LoadStg(IDS_STRING149),MB_ICONEXCLAMATION);
						break;
					}
					CHTDATA *prlCht = ogChtData.GetChtByUrno(pomOrganizerViewer->omLines[ilLineNo].Urno);
					if (prlCht != NULL)
					{
						CHTDATA rlCht = *prlCht;
						OrganizerDlg *polChtDlg = new OrganizerDlg(&rlCht,this);
						polChtDlg->m_Caption = LoadStg(IDS_STRING150);
						if (polChtDlg->DoModal() == IDOK)
						{
							rlCht.Lstu = CTime::GetCurrentTime();
							strcpy(rlCht.Useu,cgUserName);
							AfxGetApp()->DoWaitCursor(1);
							if(ogChtData.Update(&rlCht)==false)
							{
								omErrorTxt.Format("%s %d\n%s",omUpdateErrTxt,ogChtData.imLastReturnCode, ogChtData.omLastErrorMessage);
								MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
							}	
							AfxGetApp()->DoWaitCursor(-1);
						}
						delete polChtDlg;
					}
				}
				break;
			case TAB_HOLIDAY:			
				{
					int ilLineNo = pomTable->GetCurSel();
					if (ilLineNo == -1)
					{
						MessageBox(LoadStg(IDS_STRING148),LoadStg(IDS_STRING149),MB_ICONEXCLAMATION);
						break;
					}
					HOLDATA *prlHol = ogHolData.GetHolByUrno(pomHolidayViewer->omLines[ilLineNo].Urno);
					if (prlHol != NULL)
					{
						HOLDATA rlHol = *prlHol;
						HolidayDlg *polHolDlg = new HolidayDlg(&rlHol,this);
						polHolDlg->m_Caption = LoadStg(IDS_STRING150);
						if (polHolDlg->DoModal() == IDOK)
						{
							rlHol.Lstu = CTime::GetCurrentTime();
							strcpy(rlHol.Useu,cgUserName);
							AfxGetApp()->DoWaitCursor(1);
							if(ogHolData.Update(&rlHol)==false)
							{
								omErrorTxt.Format("%s %d\n%s",omUpdateErrTxt,ogHolData.imLastReturnCode, ogHolData.omLastErrorMessage);
								MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
							}	
							AfxGetApp()->DoWaitCursor(-1);
						}
						delete polHolDlg;
					}
				}
				break;
			case TAB_PLANNING_GROUPS:
				{
					int ilLineNo = pomTable->GetCurSel();
					if (ilLineNo == -1)
					{
						MessageBox(LoadStg(IDS_STRING148),LoadStg(IDS_STRING149),MB_ICONEXCLAMATION);
						break;
					}
					PGPDATA *prlPgp = ogPgpData.GetPgpByUrno(pomPlanungsgruppenViewer->omLines[ilLineNo].Urno);
					if (prlPgp != NULL)
					{
						PGPDATA rlPgp = *prlPgp;
						PlanungsgruppenDlg *polPgpDlg = new PlanungsgruppenDlg(&rlPgp,this);
						polPgpDlg->m_Caption = LoadStg(IDS_STRING150);
						if (polPgpDlg->DoModal() == IDOK)
						{
							rlPgp.Lstu = CTime::GetCurrentTime();
							strcpy(rlPgp.Useu,cgUserName);
							AfxGetApp()->DoWaitCursor(1);
							if(ogPgpData.Update(&rlPgp) == false)
							{
								omErrorTxt.Format("%s %d\n%s",omUpdateErrTxt,ogPgpData.imLastReturnCode, ogPgpData.omLastErrorMessage);
								MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
							}	
							AfxGetApp()->DoWaitCursor(-1);
						}
						delete polPgpDlg;
					}
				}
				break;
			case TAB_AIRPORTWET:
				{
					int ilLineNo = pomTable->GetCurSel();
					if (ilLineNo == -1)
					{
						MessageBox(LoadStg(IDS_STRING148),LoadStg(IDS_STRING149),MB_ICONEXCLAMATION);
						break;
					}
					AWIDATA *prlAwi = ogAwiData.GetAwiByUrno(pomAirportwetViewer->omLines[ilLineNo].Urno);
					if (prlAwi != NULL)
					{
						AWIDATA rlAwi = *prlAwi;
						CAirPortWet *polAwiDlg = new CAirPortWet(&rlAwi,this);
						polAwiDlg->m_Caption = LoadStg(IDS_STRING150);
						if (polAwiDlg->DoModal() == IDOK)
						{
/*							rlAwi.Lstu = CTime::GetCurrentTime();
							strcpy(rlAwi.Useu,cgUserName);
*/							AfxGetApp()->DoWaitCursor(1);
							if(ogAwiData.Update(&rlAwi) == false)
							{
								omErrorTxt.Format("%s %d\n%s",omUpdateErrTxt,ogAwiData.imLastReturnCode, ogAwiData.omLastErrorMessage);
								MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
							}	
							AfxGetApp()->DoWaitCursor(-1);
						}
						delete polAwiDlg;
					}
				}
				break;
			case TAB_COUNTERCLASS:
				{
					int ilLineNo = pomTable->GetCurSel();
					if (ilLineNo == -1)
					{
						MessageBox(LoadStg(IDS_STRING148),LoadStg(IDS_STRING149),MB_ICONEXCLAMATION);
						break;
					}
					CCCDATA *prlCcc = ogCccData.GetCccByUrno(pomCounterclassViewer->omLines[ilLineNo].Urno);
					if (prlCcc != NULL)
					{
						CCCDATA rlCcc = *prlCcc;
						CChkInCounterClass *polCccDlg = new CChkInCounterClass(&rlCcc,this);
						polCccDlg->m_Caption = LoadStg(IDS_STRING150);
						if (polCccDlg->DoModal() == IDOK)
						{
							rlCcc.Lstu = CTime::GetCurrentTime();
							strcpy(rlCcc.Useu,cgUserName);
							AfxGetApp()->DoWaitCursor(1);
							if(ogCccData.Update(&rlCcc) == false)
							{
								omErrorTxt.Format("%s %d\n%s",omUpdateErrTxt,ogCccData.imLastReturnCode, ogCccData.omLastErrorMessage);
								MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
							}	
							AfxGetApp()->DoWaitCursor(-1);
						}
						delete polCccDlg;
					}
				}
				break;
			case TAB_VERYIMPPERS:
				{
					int ilLineNo = pomTable->GetCurSel();
					if (ilLineNo == -1)
					{
						MessageBox(LoadStg(IDS_STRING148),LoadStg(IDS_STRING149),MB_ICONEXCLAMATION);
						break;
					}
					VIPDATA *prlVip = ogVipData.GetVipByUrno(pomVeryimppersViewer->omLines[ilLineNo].Urno);
					if (prlVip != NULL)
					{
						VIPDATA rlVip = *prlVip;
						CVeryImpPers *polVipDlg = new CVeryImpPers(&rlVip,this);
						polVipDlg->m_Caption = LoadStg(IDS_STRING150);
						if (polVipDlg->DoModal() == IDOK)
						{
							rlVip.Lstu = CTime::GetCurrentTime();
							strcpy(rlVip.Useu,cgUserName);
							AfxGetApp()->DoWaitCursor(1);
							if(ogVipData.Update(&rlVip) == false)
							{
								omErrorTxt.Format("%s %d\n%s",omUpdateErrTxt,ogVipData.imLastReturnCode, ogVipData.omLastErrorMessage);
								MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
							}	
							AfxGetApp()->DoWaitCursor(-1);
						}
						delete polVipDlg;
					}
				}
				break;
			case TAB_AIRCRAFTFAM:		
				{
					int ilLineNo = pomTable->GetCurSel();
					if (ilLineNo == -1)
					{
						MessageBox(LoadStg(IDS_STRING148),LoadStg(IDS_STRING149),MB_ICONEXCLAMATION);
						break;
					}
					AFMDATA *prlAfm = ogAFMData.GetAfmByUrno(pomAircraftFamViewer->omLines[ilLineNo].Urno);
					if (prlAfm != NULL)
					{
						AFMDATA rlAfm = *prlAfm;
						AircraftFamDlg *polAfmDlg = new AircraftFamDlg(&rlAfm,this);
						polAfmDlg->m_Caption = LoadStg(IDS_STRING150);
						if (polAfmDlg->DoModal() == IDOK)
						{
							rlAfm.Lstu = CTime::GetCurrentTime();
							strcpy(rlAfm.Useu,cgUserName);
							AfxGetApp()->DoWaitCursor(1);
							if(ogAFMData.Update(&rlAfm)==false)
							{
								omErrorTxt.Format("%s %d\n%s",omUpdateErrTxt,ogAFMData.imLastReturnCode, ogAFMData.omLastErrorMessage);
								MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
							}	
							AfxGetApp()->DoWaitCursor(-1);
						}
						delete polAfmDlg;
					}
				}
				break;
			case TAB_ENGINETYPE:		
				{
					int ilLineNo = pomTable->GetCurSel();
					if (ilLineNo == -1)
					{
						MessageBox(LoadStg(IDS_STRING148),LoadStg(IDS_STRING149),MB_ICONEXCLAMATION);
						break;
					}
					ENTDATA *prlEnt = ogENTData.GetEntByUrno(pomEngineTypeViewer->omLines[ilLineNo].Urno);
					if (prlEnt != NULL)
					{
						ENTDATA rlEnt = *prlEnt;
						EngineTypeDlg *polEntDlg = new EngineTypeDlg(&rlEnt,this);
						polEntDlg->m_Caption = LoadStg(IDS_STRING150);
						if (polEntDlg->DoModal() == IDOK)
						{
							rlEnt.Lstu = CTime::GetCurrentTime();
							strcpy(rlEnt.Useu,cgUserName);
							AfxGetApp()->DoWaitCursor(1);
							if(ogENTData.Update(&rlEnt)==false)
							{
								omErrorTxt.Format("%s %d\n%s",omUpdateErrTxt,ogENTData.imLastReturnCode, ogENTData.omLastErrorMessage);
								MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
							}	
							AfxGetApp()->DoWaitCursor(-1);
						}
						delete polEntDlg;
					}
				}
				break;
		}
		bgIsDialogOpen = false;
	}
}

//------------------------------------------------------------------------------------------------------

void CStammdaten::OnEinfuegen() 
{
	if(!bgIsDialogOpen)
	{
		bgIsDialogOpen = true;
		switch(igSelectTab)
		{
			case TAB_AIRLINE:
				{
					ALTDATA rlALT;
					AirlineDlg *olALTDlg = new AirlineDlg(&rlALT,this);
					olALTDlg->m_Caption = LoadStg(IDS_STRING151);
					rlALT.Vafr = CTime::GetCurrentTime();
					if (olALTDlg->DoModal() == IDOK)
					{
						rlALT.Urno = ogBasicData.GetNextUrno();	//new URNO
						rlALT.Cdat = CTime::GetCurrentTime();
						strcpy(rlALT.Usec,cgUserName);
						ALTDATA *prlALT = new ALTDATA;
						*prlALT = rlALT;
						AfxGetApp()->DoWaitCursor(1);
						if(ogALTData.InsertALT(prlALT)==false)
						{
							delete prlALT;
							omErrorTxt.Format("%s %d\n%s",omInsertErrTxt, ogALTData.imLastReturnCode, ogALTData.omLastErrorMessage);
							MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
						}	
						AfxGetApp()->DoWaitCursor(-1);
					}
					delete olALTDlg;
				}
				break;
			case TAB_AIRCRAFT:
				{
					ACTDATA rlACT;
					AircraftDlg *olACTDlg = new AircraftDlg(&rlACT,this);
					olACTDlg->m_Caption = LoadStg(IDS_STRING151);
					rlACT.Vafr = CTime::GetCurrentTime();
					if (olACTDlg->DoModal() == IDOK)
					{
						rlACT.Urno = ogBasicData.GetNextUrno();	//new URNO
						rlACT.Cdat = CTime::GetCurrentTime();

						//~~~~~ 17.08.99 SHA 
						//*** STRANGE VALUE IN SEAE ***
						strcpy(rlACT.Seae,"");

						strcpy(rlACT.Usec,cgUserName);
						ACTDATA *prlACT = new ACTDATA;
						*prlACT = rlACT;
						AfxGetApp()->DoWaitCursor(1);
						if(ogACTData.InsertACT(prlACT)==false)
						{
							delete prlACT;
							omErrorTxt.Format("%s %d\n%s",omInsertErrTxt, ogACTData.imLastReturnCode, ogACTData.omLastErrorMessage);
							MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
						}	
						AfxGetApp()->DoWaitCursor(-1);
					}
					delete olACTDlg;
				}
				break;
			case TAB_LFZREGISTRATION:
				{
					ACRDATA rlACR;
					LFZRegiDlg *olACRDlg = new LFZRegiDlg(&rlACR,this);
					olACRDlg->m_Caption = LoadStg(IDS_STRING151);
					rlACR.Vafr = CTime::GetCurrentTime();
					if (olACRDlg->DoModal() == IDOK)
					{
						rlACR.Urno = ogBasicData.GetNextUrno();	//new URNO
						strcpy(rlACR.Hopo, pcgHome);
						rlACR.Cdat = CTime::GetCurrentTime();
						strcpy(rlACR.Usec,cgUserName);
						ACRDATA *prlACR = new ACRDATA;
						*prlACR = rlACR;
						AfxGetApp()->DoWaitCursor(1);
						if(ogACRData.InsertACR(prlACR)==false)
						{
							delete prlACR;
							omErrorTxt.Format("%s %d\n%s",omInsertErrTxt, ogACRData.imLastReturnCode, ogACRData.omLastErrorMessage);
							MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
						}	
						AfxGetApp()->DoWaitCursor(-1);
					}
					delete olACRDlg;
				}
				break;
			case TAB_AIRPORT:
				{
					APTDATA rlAPT;
					AirportDlg *olAPTDlg = new AirportDlg(&rlAPT,this);
					olAPTDlg->m_Caption = LoadStg(IDS_STRING151);
					rlAPT.Vafr = CTime::GetCurrentTime();
					if (olAPTDlg->DoModal() == IDOK)
					{
						rlAPT.Urno = ogBasicData.GetNextUrno();	//new URNO
						rlAPT.Cdat = CTime::GetCurrentTime();
						strcpy(rlAPT.Usec,cgUserName);
						APTDATA *prlAPT = new APTDATA;
						*prlAPT = rlAPT;
						AfxGetApp()->DoWaitCursor(1);
						if(ogAPTData.InsertAPT(prlAPT)==false)
						{
							delete prlAPT;
							omErrorTxt.Format("%s %d\n%s",omInsertErrTxt, ogAPTData.imLastReturnCode, ogAPTData.omLastErrorMessage);
							MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
						}	
						AfxGetApp()->DoWaitCursor(-1);
					}
					delete olAPTDlg;
				}
				break;
			case TAB_RUNWAY:
				{
					RWYDATA rlRWY;
					RunwayDlg *olRWYDlg = new RunwayDlg(&rlRWY,this);
					olRWYDlg->m_Caption = LoadStg(IDS_STRING151);
					rlRWY.Vafr = CTime::GetCurrentTime();
					if (olRWYDlg->DoModal() == IDOK)
					{
						rlRWY.Urno = ogBasicData.GetNextUrno();	//new URNO
						rlRWY.Cdat = CTime::GetCurrentTime();
						strcpy(rlRWY.Usec,cgUserName);
						RWYDATA *prlRWY = new RWYDATA;
						*prlRWY = rlRWY;
						AfxGetApp()->DoWaitCursor(1);
						if(ogRWYData.InsertRWY(prlRWY)==false)
						{
							delete prlRWY;
							omErrorTxt.Format("%s %d\n%s",omInsertErrTxt, ogRWYData.imLastReturnCode, ogRWYData.omLastErrorMessage);
							MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
						}	
						AfxGetApp()->DoWaitCursor(-1);
					}
					delete olRWYDlg; 
				}
				break;
			case TAB_TAXIWAY:
				{
					TWYDATA rlTWY;
					TaxiwayDlg *olTWYDlg = new TaxiwayDlg(&rlTWY,this);
					olTWYDlg->m_Caption = LoadStg(IDS_STRING151);
					rlTWY.Vafr = CTime::GetCurrentTime();
					if (olTWYDlg->DoModal() == IDOK)
					{
						rlTWY.Urno = ogBasicData.GetNextUrno();	//new URNO
						rlTWY.Cdat = CTime::GetCurrentTime();
						strcpy(rlTWY.Usec,cgUserName);
						TWYDATA *prlTWY = new TWYDATA;
						*prlTWY = rlTWY;
						AfxGetApp()->DoWaitCursor(1);
						if(ogTWYData.InsertTWY(prlTWY)==false)
						{
							delete prlTWY;
							omErrorTxt.Format("%s %d\n%s",omInsertErrTxt, ogTWYData.imLastReturnCode, ogTWYData.omLastErrorMessage);
							MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
						}	
						AfxGetApp()->DoWaitCursor(-1);
					}
					delete olTWYDlg; 
				}
				break;
			case TAB_POSITION:
				{
					PSTDATA rlPST;
					PositionDlg *olPSTDlg = new PositionDlg(&rlPST,this);
					olPSTDlg->m_Caption = LoadStg(IDS_STRING151);
					rlPST.Vafr = CTime::GetCurrentTime();
					if (olPSTDlg->DoModal() == IDOK)
					{
						rlPST.Urno = ogBasicData.GetNextUrno();	//new URNO
						rlPST.Cdat = CTime::GetCurrentTime();
						strcpy(rlPST.Usec,cgUserName);
						PSTDATA *prlPST = new PSTDATA;
						*prlPST = rlPST;
						AfxGetApp()->DoWaitCursor(1);
						if(ogPSTData.InsertPST(prlPST)==false)
						{
							delete prlPST;
							omErrorTxt.Format("%s %d\n%s",omInsertErrTxt, ogPSTData.imLastReturnCode, ogPSTData.omLastErrorMessage);
							MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
						}
						
						CString olBurn;
						olBurn.Format("%d",rlPST.Urno);
						UpdateBlkTable(&olPSTDlg->omBlkPtrA,&olPSTDlg->omDeleteBlkPtrA,olBurn,"PST");

						AfxGetApp()->DoWaitCursor(-1);
					}
					delete olPSTDlg; 
				}
				break;
			case TAB_GATE:
				{
					GATDATA rlGAT;
					GateDlg *olGATDlg = new GateDlg(&rlGAT,this);
					olGATDlg->m_Caption = LoadStg(IDS_STRING151);
					rlGAT.Vafr = CTime::GetCurrentTime();
					if (olGATDlg->DoModal() == IDOK)
					{
						rlGAT.Urno = ogBasicData.GetNextUrno();	//new URNO
						rlGAT.Cdat = CTime::GetCurrentTime();
						strcpy(rlGAT.Usec,cgUserName);
						GATDATA *prlGAT = new GATDATA;
						*prlGAT = rlGAT;
						AfxGetApp()->DoWaitCursor(1);
						if(ogGATData.InsertGAT(prlGAT)==false)
						{
							delete prlGAT;
							omErrorTxt.Format("%s %d\n%s",omInsertErrTxt, ogGATData.imLastReturnCode, ogGATData.omLastErrorMessage);
							MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
						}
						
						CString olBurn;
						olBurn.Format("%d",rlGAT.Urno);
						UpdateBlkTable(&olGATDlg->omBlkPtrA,&olGATDlg->omDeleteBlkPtrA,olBurn,"GAT");

						AfxGetApp()->DoWaitCursor(-1);
					}
					delete olGATDlg;
				}
				break;
			case TAB_CHECKINCOUNTER:
				{
					CICDATA rlCIC;
					CheckinCounterDlg *olCICDlg = new CheckinCounterDlg(&rlCIC,this);
					olCICDlg->m_Caption = LoadStg(IDS_STRING151);
					rlCIC.Vafr = CTime::GetCurrentTime();
					if (olCICDlg->DoModal() == IDOK)
					{
						rlCIC.Urno = ogBasicData.GetNextUrno();	//new URNO
						rlCIC.Cdat = CTime::GetCurrentTime();
						strcpy(rlCIC.Usec,cgUserName);
						CICDATA *prlCIC = new CICDATA;
						*prlCIC = rlCIC;
						AfxGetApp()->DoWaitCursor(1);
						if(ogCICData.InsertCIC(prlCIC)==false)
						{
							delete prlCIC;
							omErrorTxt.Format("%s %d\n%s",omInsertErrTxt, ogCICData.imLastReturnCode, ogCICData.omLastErrorMessage);
							MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
						}
						
						CString olBurn;
						olBurn.Format("%d",rlCIC.Urno);
						UpdateBlkTable(&olCICDlg->omBlkPtrA,&olCICDlg->omDeleteBlkPtrA,olBurn,"CIC");

						AfxGetApp()->DoWaitCursor(-1);
					}
					delete olCICDlg; 
				}
				break;
			case TAB_BEGGAGEBELT:
				{
					BLTDATA rlBLT;
					BeggagebeltDlg *olBLTDlg = new BeggagebeltDlg(&rlBLT,this);
					olBLTDlg->m_Caption = LoadStg(IDS_STRING151);
					rlBLT.Vafr = CTime::GetCurrentTime();
					if (olBLTDlg->DoModal() == IDOK)
					{
						rlBLT.Urno = ogBasicData.GetNextUrno();	//new URNO
						rlBLT.Cdat = CTime::GetCurrentTime();
						strcpy(rlBLT.Usec,cgUserName);
						BLTDATA *prlBLT = new BLTDATA;
						*prlBLT = rlBLT;
						AfxGetApp()->DoWaitCursor(1);
						if(ogBLTData.InsertBLT(prlBLT)==false)
						{
							delete prlBLT;
							omErrorTxt.Format("%s %d\n%s",omInsertErrTxt, ogBLTData.imLastReturnCode, ogBLTData.omLastErrorMessage);

							MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
						}
						
						CString olBurn;
						olBurn.Format("%d",rlBLT.Urno);
						UpdateBlkTable(&olBLTDlg->omBlkPtrA,&olBLTDlg->omDeleteBlkPtrA,olBurn,"BLT");

						AfxGetApp()->DoWaitCursor(-1);
					}
					delete olBLTDlg;
				}
				break;
			case TAB_EXIT:
				{
					EXTDATA rlEXT;
					ExitDlg *olEXTDlg = new ExitDlg(&rlEXT,this);
					olEXTDlg->m_Caption = LoadStg(IDS_STRING151);
					rlEXT.Vafr = CTime::GetCurrentTime();
					if (olEXTDlg->DoModal() == IDOK)
					{
						rlEXT.Urno = ogBasicData.GetNextUrno();	//new URNO
						rlEXT.Cdat = CTime::GetCurrentTime();
						strcpy(rlEXT.Usec,cgUserName);
						EXTDATA *prlEXT = new EXTDATA;
						*prlEXT = rlEXT;
						AfxGetApp()->DoWaitCursor(1);
						if(ogEXTData.InsertEXT(prlEXT)==false)
						{
							delete prlEXT;
							omErrorTxt.Format("%s %d\n%s",omInsertErrTxt,ogEXTData.imLastReturnCode, ogEXTData.omLastErrorMessage);
							MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
						}	
						AfxGetApp()->DoWaitCursor(-1);
					}
					delete olEXTDlg; 
				}
				break;
			case TAB_DELAYCODE:
				{
					DENDATA rlDEN;
					DelaycodeDlg *olDENDlg = new DelaycodeDlg(&rlDEN,this);
					olDENDlg->m_Caption = LoadStg(IDS_STRING151);
					rlDEN.Vafr = CTime::GetCurrentTime();
					if (olDENDlg->DoModal() == IDOK)
					{
						rlDEN.Urno = ogBasicData.GetNextUrno();	//new URNO
						rlDEN.Cdat = CTime::GetCurrentTime();
						strcpy(rlDEN.Usec,cgUserName);
						DENDATA *prlDEN = new DENDATA;
						*prlDEN = rlDEN;
						AfxGetApp()->DoWaitCursor(1);
						if(ogDENData.InsertDEN(prlDEN)==false)
						{
							delete prlDEN;
							omErrorTxt.Format("%s %d\n%s",omInsertErrTxt,ogDENData.imLastReturnCode, ogDENData.omLastErrorMessage);
							MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
						}	
						AfxGetApp()->DoWaitCursor(-1);
					}
					delete olDENDlg; 
				}
				break;
			case TAB_TELEXADRESS:
				{
					MVTDATA rlMVT;
					TelexadressDlg *olMVTDlg = new TelexadressDlg(&rlMVT,this);
					olMVTDlg->m_Caption = LoadStg(IDS_STRING151);
					rlMVT.Vafr = CTime::GetCurrentTime();
					if (olMVTDlg->DoModal() == IDOK)
					{
						rlMVT.Urno = ogBasicData.GetNextUrno();	//new URNO
						rlMVT.Cdat = CTime::GetCurrentTime();
						strcpy(rlMVT.Usec,cgUserName);
						MVTDATA *prlMVT = new MVTDATA;
						*prlMVT = rlMVT;
						AfxGetApp()->DoWaitCursor(1);
						if(ogMVTData.InsertMVT(prlMVT)==false)
						{
							delete prlMVT;
							omErrorTxt.Format("%s %d\n%s",omInsertErrTxt,ogMVTData.imLastReturnCode, ogMVTData.omLastErrorMessage);
							MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
						}	
						AfxGetApp()->DoWaitCursor(-1);
					}
					delete olMVTDlg; 
				}
				break;
			case TAB_TRAFFICTYPE:
				{
					NATDATA rlNAT;
					TraffictypeDlg *olNATDlg = new TraffictypeDlg(&rlNAT,this);
					olNATDlg->m_Caption = LoadStg(IDS_STRING151);
					rlNAT.Vafr = CTime::GetCurrentTime();
					if (olNATDlg->DoModal() == IDOK)
					{
						rlNAT.Urno = ogBasicData.GetNextUrno();	//new URNO
						rlNAT.Cdat = CTime::GetCurrentTime();
						strcpy(rlNAT.Usec,cgUserName);
						NATDATA *prlNAT = new NATDATA;
						*prlNAT = rlNAT;
						AfxGetApp()->DoWaitCursor(1);
						if(ogNATData.InsertNAT(prlNAT)==false)
						{
							delete prlNAT;
							omErrorTxt.Format("%s %d\n%s",omInsertErrTxt,ogNATData.imLastReturnCode, ogNATData.omLastErrorMessage);
							MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
						}	
						AfxGetApp()->DoWaitCursor(-1);
					}
					delete olNATDlg; 
				}
				break;
			case TAB_HANDLINGAGENT:
				{
					HAGDATA rlHAG;
					HandlingagentDlg *olHAGDlg = new HandlingagentDlg(&rlHAG,this);
					olHAGDlg->m_Caption = LoadStg(IDS_STRING151);
					rlHAG.Vafr = CTime::GetCurrentTime();
					if (olHAGDlg->DoModal() == IDOK)
					{
						rlHAG.Urno = ogBasicData.GetNextUrno();	//new URNO
						rlHAG.Cdat = CTime::GetCurrentTime();
						strcpy(rlHAG.Usec,cgUserName);
						HAGDATA *prlHAG = new HAGDATA;
						*prlHAG = rlHAG;
						AfxGetApp()->DoWaitCursor(1);
						if(ogHAGData.InsertHAG(prlHAG)==false)
						{
							delete prlHAG;
							omErrorTxt.Format("%s %d\n%s",omInsertErrTxt,ogHAGData.imLastReturnCode, ogHAGData.omLastErrorMessage);
							MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
						}	
						AfxGetApp()->DoWaitCursor(-1);
					}
					delete olHAGDlg;
				}
				break;
			case TAB_WAITINGROOM:
				{
					WRODATA rlWRO;
					WaitingroomDlg *olWRODlg = new WaitingroomDlg(&rlWRO,this);
					olWRODlg->m_Caption = LoadStg(IDS_STRING151);
					rlWRO.Vafr = CTime::GetCurrentTime();
					if (olWRODlg->DoModal() == IDOK)
					{
						rlWRO.Urno = ogBasicData.GetNextUrno();	//new URNO
						rlWRO.Cdat = CTime::GetCurrentTime();
						strcpy(rlWRO.Usec,cgUserName);
						WRODATA *prlWRO = new WRODATA;
						*prlWRO = rlWRO;
						AfxGetApp()->DoWaitCursor(1);
						if(ogWROData.InsertWRO(prlWRO)==false)
						{
							delete prlWRO;
							omErrorTxt.Format("%s %d\n%s",omInsertErrTxt,ogWROData.imLastReturnCode, ogWROData.omLastErrorMessage);
							MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
						}	
						else
						{
							CString olBurn;
							olBurn.Format("%d",rlWRO.Urno);
							UpdateBlkTable(&olWRODlg->omBlkPtrA,&olWRODlg->omDeleteBlkPtrA,olBurn,"WRO");
						}

						AfxGetApp()->DoWaitCursor(-1);
					}
					delete olWRODlg; 
				}
				break;
			case TAB_HANDLINGTYPE:
				{
					HTYDATA rlHTY;
					HandlingtypeDlg *olHTYDlg = new HandlingtypeDlg(&rlHTY,this);
					olHTYDlg->m_Caption = LoadStg(IDS_STRING151);
					rlHTY.Vafr = CTime::GetCurrentTime();
					if (olHTYDlg->DoModal() == IDOK)
					{
						rlHTY.Urno = ogBasicData.GetNextUrno();	//new URNO
						rlHTY.Cdat = CTime::GetCurrentTime();
						strcpy(rlHTY.Usec,cgUserName);
						HTYDATA *prlHTY = new HTYDATA;
						*prlHTY = rlHTY;
						AfxGetApp()->DoWaitCursor(1);
						if(ogHTYData.InsertHTY(prlHTY)==false)
						{
							delete prlHTY;
							omErrorTxt.Format("%s %d\n%s",omInsertErrTxt,ogHTYData.imLastReturnCode, ogHTYData.omLastErrorMessage);
							MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
						}	
						AfxGetApp()->DoWaitCursor(-1);
					}
					delete olHTYDlg; 
				}
				break;
			case TAB_SERVICETYPE:
				{
					STYDATA rlSTY;
					ServicetypeDlg *olSTYDlg = new ServicetypeDlg(&rlSTY,this);
					olSTYDlg->m_Caption = LoadStg(IDS_STRING151);
					rlSTY.Vafr = CTime::GetCurrentTime();
					if (olSTYDlg->DoModal() == IDOK)
					{
						rlSTY.Urno = ogBasicData.GetNextUrno();	//new URNO
						rlSTY.Cdat = CTime::GetCurrentTime();
						strcpy(rlSTY.Usec,cgUserName);
						STYDATA *prlSTY = new STYDATA;
						*prlSTY = rlSTY;
						AfxGetApp()->DoWaitCursor(1);
						if(ogSTYData.InsertSTY(prlSTY)==false)
						{
							delete prlSTY;
							omErrorTxt.Format("%s %d\n%s",omInsertErrTxt,ogSTYData.imLastReturnCode, ogSTYData.omLastErrorMessage);
							MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
						}	
						AfxGetApp()->DoWaitCursor(-1);
					}
					delete olSTYDlg; 
				}
				break;
			case TAB_FIDSCOMMAND:
				{		
					FIDDATA rlFID;
					FidsCommandDlg *olFIDDlg = new FidsCommandDlg(&rlFID,this);
					olFIDDlg->m_Caption = LoadStg(IDS_STRING151);
					rlFID.Vafr = CTime::GetCurrentTime();
					if (olFIDDlg->DoModal() == IDOK)
					{
						rlFID.Urno = ogBasicData.GetNextUrno();	//new URNO
						rlFID.Cdat = CTime::GetCurrentTime();
						strcpy(rlFID.Usec,cgUserName);
						FIDDATA *prlFID = new FIDDATA;
						*prlFID = rlFID;
						AfxGetApp()->DoWaitCursor(1);
						if(ogFIDData.InsertFID(prlFID)==false)
						{
							delete prlFID;
							omErrorTxt.Format("%s %d\n%s",omInsertErrTxt,ogFIDData.imLastReturnCode, ogFIDData.omLastErrorMessage);
							MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
						}	
						AfxGetApp()->DoWaitCursor(-1);
					}
					delete olFIDDlg; 
				}
				break;
			case TAB_ERGVERKEHRSARTEN:
				{
					SPHDATA rlSph;
					ErgVerkehrsartenDlg *olSphDlg = new ErgVerkehrsartenDlg(&rlSph,this);
					olSphDlg->m_Caption = LoadStg(IDS_STRING151);
					if (olSphDlg->DoModal() == IDOK)
					{
						rlSph.Urno = ogBasicData.GetNextUrno();	//new URNO
						rlSph.Cdat = CTime::GetCurrentTime();
						strcpy(rlSph.Eart,"V");
						strcpy(rlSph.Usec,cgUserName);
						SPHDATA *prlSph = new SPHDATA;
						*prlSph = rlSph;
						AfxGetApp()->DoWaitCursor(1);
						if(ogSphData.Insert(prlSph)==false)
						{
							delete prlSph;
							omErrorTxt.Format("%s %d\n%s",omInsertErrTxt,ogSphData.imLastReturnCode, ogSphData.omLastErrorMessage);
							MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
						}	
						AfxGetApp()->DoWaitCursor(-1);
					}
					delete olSphDlg;
				}
				break;
			case TAB_STANDARDVERKETTUNGEN:
				{
					STRDATA rlStr;
					StandardverkettungenDlg *olStrDlg = new StandardverkettungenDlg(&rlStr,this);
					olStrDlg->m_Caption = LoadStg(IDS_STRING151);
					rlStr.Vafr = CTime::GetCurrentTime();
					if (olStrDlg->DoModal() == IDOK)
					{
						rlStr.Urno = ogBasicData.GetNextUrno();	//new URNO
						rlStr.Cdat = CTime::GetCurrentTime();
						strcpy(rlStr.Usec,cgUserName);
						STRDATA *prlStr = new STRDATA;
						*prlStr = rlStr;
						AfxGetApp()->DoWaitCursor(1);
						if(ogStrData.Insert(prlStr)==false)
						{
							delete prlStr;
							omErrorTxt.Format("%s %d\n%s",omInsertErrTxt,ogStrData.imLastReturnCode, ogStrData.omLastErrorMessage);
							MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
						}	
						AfxGetApp()->DoWaitCursor(-1);
					}
					delete olStrDlg;
				}
				break;
			case TAB_FLUGPLANSAISON:
				{
					SEADATA rlSea;
					FlugplansaisonDlg *olSeaDlg = new FlugplansaisonDlg(&rlSea,this);
					olSeaDlg->m_Caption = LoadStg(IDS_STRING151);
					//rlSea.Vpfr = CTime::GetCurrentTime();
					if (olSeaDlg->DoModal() == IDOK)
					{
						rlSea.Urno = ogBasicData.GetNextUrno();	//new URNO
						rlSea.Cdat = CTime::GetCurrentTime();
						strcpy(rlSea.Usec,cgUserName);
						SEADATA *prlSea = new SEADATA;
						*prlSea = rlSea;
						AfxGetApp()->DoWaitCursor(1);
						if(ogSeaData.Insert(prlSea)==false)
						{
							delete prlSea;
							omErrorTxt.Format("%s %d\n%s",omInsertErrTxt,ogSeaData.imLastReturnCode, ogSeaData.omLastErrorMessage);
							MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
						}	
						AfxGetApp()->DoWaitCursor(-1);
					}
					delete olSeaDlg;
				}
				break;
			case TAB_LEISTUNGSKATALOG:
				{
					GHSDATA rlGhs;
					LeistungskatalogDlg *olGhsDlg = new LeistungskatalogDlg(&rlGhs,this);
					olGhsDlg->m_Caption = LoadStg(IDS_STRING151);
					if (olGhsDlg->DoModal() == IDOK)
					{
						rlGhs.Urno = ogBasicData.GetNextUrno();	//new URNO
						rlGhs.Cdat = CTime::GetCurrentTime();
						strcpy(rlGhs.Usec,cgUserName);
						GHSDATA *prlGhs = new GHSDATA;
						*prlGhs = rlGhs;
						AfxGetApp()->DoWaitCursor(1);
						if(ogGhsData.Insert(prlGhs)==false)
						{
							delete prlGhs;
							omErrorTxt.Format("%s %d\n%s",omInsertErrTxt,ogGhsData.imLastReturnCode, ogGhsData.omLastErrorMessage);
							MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
						}	
						AfxGetApp()->DoWaitCursor(-1);
					}
					delete olGhsDlg;
				}
				break;
			case TAB_QUALIFIKATIONEN:
				{
					PERDATA rlPer;
					QualifikationenDlg *olPerDlg = new QualifikationenDlg(&rlPer,this);
					olPerDlg->m_Caption = LoadStg(IDS_STRING151);
					if (olPerDlg->DoModal() == IDOK)
					{
						rlPer.Urno = ogBasicData.GetNextUrno();	//new URNO
						rlPer.Cdat = CTime::GetCurrentTime();
						strcpy(rlPer.Usec,cgUserName);
						PERDATA *prlPer = new PERDATA;
						*prlPer = rlPer;
						AfxGetApp()->DoWaitCursor(1);
						if(ogPerData.Insert(prlPer)==false)
						{
							delete prlPer;
							omErrorTxt.Format("%s %d\n%s",omInsertErrTxt,ogPerData.imLastReturnCode, ogPerData.omLastErrorMessage);
							MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
						}	
						AfxGetApp()->DoWaitCursor(-1);
					}
					delete olPerDlg;
				}
				break;
			case TAB_ORGANISATIONSEINHEITEN:
				{
					ORGDATA rlOrg;
					OrganisationseinheitenDlg *olOrgDlg = new OrganisationseinheitenDlg(&rlOrg,this);
					olOrgDlg->m_Caption = LoadStg(IDS_STRING151);
					if (olOrgDlg->DoModal() == IDOK)
					{
						rlOrg.Urno = ogBasicData.GetNextUrno();	//new URNO
						rlOrg.Cdat = CTime::GetCurrentTime();
						strcpy(rlOrg.Usec,cgUserName);
						ORGDATA *prlOrg = new ORGDATA;
						*prlOrg = rlOrg;
						AfxGetApp()->DoWaitCursor(1);
						if(ogOrgData.Insert(prlOrg)==false)
						{
							delete prlOrg;
							omErrorTxt.Format("%s %d\n%s",omInsertErrTxt,ogOrgData.imLastReturnCode, ogOrgData.omLastErrorMessage);
							MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
						}	
						AfxGetApp()->DoWaitCursor(-1);
					}
					delete olOrgDlg;
				}
				break;
			case TAB_WEGEZEITEN:				
				{
					WAYDATA rlWay;
					WegezeitenDlg *polDlg = new WegezeitenDlg(&rlWay, this);
					polDlg->m_Caption = LoadStg(IDS_STRING151);
					if (polDlg->DoModal() == IDOK)
					{
						rlWay.Urno = ogBasicData.GetNextUrno();	//new URNO
						rlWay.Cdat = CTime::GetCurrentTime();
						strcpy(rlWay.Usec,cgUserName);
						WAYDATA *prlWay = new WAYDATA;
						*prlWay = rlWay;
						AfxGetApp()->DoWaitCursor(1);
						if(ogWayData.Insert(prlWay)==false)
						{
							delete prlWay;
							omErrorTxt.Format("%s %d\n%s",omInsertErrTxt,ogWayData.imLastReturnCode, ogWayData.omLastErrorMessage);
							MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
						}	
						AfxGetApp()->DoWaitCursor(-1);
					}
					delete polDlg;
				}
				break;
			case TAB_FUNKTIONEN:				
				{
					PFCDATA rlPfc;
					FunktionenDlg *polDlg = new FunktionenDlg(&rlPfc, this);
					polDlg->m_Caption = LoadStg(IDS_STRING151);
					if (polDlg->DoModal() == IDOK)
					{
						rlPfc.Urno = ogBasicData.GetNextUrno();	//new URNO
						rlPfc.Cdat = CTime::GetCurrentTime();
						strcpy(rlPfc.Usec,cgUserName);
						PFCDATA *prlPfc = new PFCDATA;
						*prlPfc = rlPfc;
						AfxGetApp()->DoWaitCursor(1);
						if(ogPfcData.Insert(prlPfc)==false)
						{
							delete prlPfc;
							omErrorTxt.Format("%s %d\n%s",omInsertErrTxt,ogPfcData.imLastReturnCode, ogPfcData.omLastErrorMessage);
							MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
						}	
						AfxGetApp()->DoWaitCursor(-1);
					}
					delete polDlg;
				}
				break;
			case TAB_ARBEITSVERTRAGSARTEN:	
				{
					COTDATA rlCot;
					ArbeitsvertragsartenDlg *polDlg = new ArbeitsvertragsartenDlg(&rlCot,this);
					polDlg->m_Caption = LoadStg(IDS_STRING151);
					if (polDlg->DoModal() == IDOK)
					{
						rlCot.Urno = ogBasicData.GetNextUrno();	//new URNO
						rlCot.Cdat = CTime::GetCurrentTime();
						strcpy(rlCot.Usec,cgUserName);
						COTDATA *prlCot = new COTDATA;
						*prlCot = rlCot;
						AfxGetApp()->DoWaitCursor(1);
						if(ogCotData.Insert(prlCot)==false)
						{
							delete prlCot;
							omErrorTxt.Format("%s %d\n%s",omInsertErrTxt,ogCotData.imLastReturnCode, ogCotData.omLastErrorMessage);
							MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
						}	
						AfxGetApp()->DoWaitCursor(-1);
					}
					delete polDlg;
				}
				break;
			case TAB_BEWERTUNGSFAKTOREN:		
				{
					ASFDATA rlAsf;
					BewertungsfaktorenDlg *polDlg = new BewertungsfaktorenDlg(&rlAsf,this);
					polDlg->m_Caption = LoadStg(IDS_STRING151);
					if (polDlg->DoModal() == IDOK)
					{
						rlAsf.Urno = ogBasicData.GetNextUrno();	//new URNO
						rlAsf.Cdat = CTime::GetCurrentTime();
						strcpy(rlAsf.Usec,cgUserName);
						ASFDATA *prlAsf = new ASFDATA;
						*prlAsf = rlAsf;
						AfxGetApp()->DoWaitCursor(1);
						if(ogAsfData.Insert(prlAsf)==false)
						{
							delete prlAsf;
							omErrorTxt.Format("%s %d\n%s",omInsertErrTxt,ogAsfData.imLastReturnCode, ogAsfData.omLastErrorMessage);
							MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
						}	
						AfxGetApp()->DoWaitCursor(-1);
					}
					delete polDlg;
				}
				break;
			case TAB_BASISSCHICHTEN:			
				{
					BSDDATA rlBsd;
					BasisschichtenDlg *polDlg = new BasisschichtenDlg(&rlBsd,true, this);
					polDlg->m_Caption = LoadStg(IDS_STRING151);
					if (polDlg->DoModal() == IDOK)
					{
						rlBsd.Urno = ogBasicData.GetNextUrno();	//new URNO
						rlBsd.Cdat = CTime::GetCurrentTime();
						strcpy(rlBsd.Usec,cgUserName);
						BSDDATA *prlBsd = new BSDDATA;
						*prlBsd = rlBsd;
						AfxGetApp()->DoWaitCursor(1);
						if(ogBsdData.Insert(prlBsd)==false)
						{
							delete prlBsd;
							omErrorTxt.Format("%s %d\n%s",omInsertErrTxt,ogBsdData.imLastReturnCode, ogBsdData.omLastErrorMessage);
							MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
						}	
						AfxGetApp()->DoWaitCursor(-1);
					}
					delete polDlg;
				}
				break;
			case TAB_DIENSTEUNDABWESENHEITEN:
				{
					ODADATA rlOda;
					DiensteUndAbwesenheitenDlg *polDlg = new DiensteUndAbwesenheitenDlg(&rlOda,this);
					polDlg->m_Caption = LoadStg(IDS_STRING151);
					if (polDlg->DoModal() == IDOK)
					{
						rlOda.Urno = ogBasicData.GetNextUrno();	//new URNO
						rlOda.Cdat = CTime::GetCurrentTime();
						strcpy(rlOda.Usec,cgUserName);
						ODADATA *prlOda = new ODADATA;
						*prlOda = rlOda;
						AfxGetApp()->DoWaitCursor(1);
						if(ogOdaData.Insert(prlOda)==false)
						{
							delete prlOda;
							omErrorTxt.Format("%s %d\n%s",omInsertErrTxt,ogOdaData.imLastReturnCode, ogOdaData.omLastErrorMessage);
							MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
						}	
						AfxGetApp()->DoWaitCursor(-1);
					}
					delete polDlg;
				}
				break;
			case TAB_FAHRGEMEINSCHAFTEN:		
				{
					TEADATA rlTea;
					FahrgemeinschaftenDlg *polDlg = new FahrgemeinschaftenDlg(&rlTea, this);
					polDlg->m_Caption = LoadStg(IDS_STRING151);
					if (polDlg->DoModal() == IDOK)
					{
						rlTea.Urno = ogBasicData.GetNextUrno();	//new URNO
						rlTea.Cdat = CTime::GetCurrentTime();
						strcpy(rlTea.Usec,cgUserName);
						TEADATA *prlTea = new TEADATA;
						*prlTea = rlTea;
						AfxGetApp()->DoWaitCursor(1);
						if(ogTeaData.Insert(prlTea)==false)
						{
							delete prlTea;
							omErrorTxt.Format("%s %d\n%s",omInsertErrTxt,ogTeaData.imLastReturnCode, ogTeaData.omLastErrorMessage);
							MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
						}	
						AfxGetApp()->DoWaitCursor(-1);
					}
					delete polDlg;
				}
				break;
			case TAB_ARBEITSGRUPPEN:		
				{
					WGPDATA rlWgp;
					CArbeitsgruppenDlg *polDlg = new CArbeitsgruppenDlg(&rlWgp, this);
					polDlg->m_Caption = LoadStg(IDS_STRING151);
					if (polDlg->DoModal() == IDOK)
					{
						rlWgp.Urno = ogBasicData.GetNextUrno();	//new URNO
						rlWgp.Cdat = CTime::GetCurrentTime();
						strcpy(rlWgp.Usec,cgUserName);
						WGPDATA *prlWgp = new WGPDATA;
						*prlWgp = rlWgp;
						AfxGetApp()->DoWaitCursor(1);
						if(ogWgpData.Insert(prlWgp)==false)
						{
							delete prlWgp;
							omErrorTxt.Format("%s %d\n%s",omInsertErrTxt,ogWgpData.imLastReturnCode, ogWgpData.omLastErrorMessage);
							MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
						}	
						AfxGetApp()->DoWaitCursor(-1);
					}
					delete polDlg;
				}
				break;
			case TAB_MITARBEITERSTAMM:		
				{
					STFDATA rlStf;
					CCSPtrArray<SORDATA> olSorData;
					CCSPtrArray<SPFDATA> olSpfData;
					CCSPtrArray<SPEDATA> olSpeData;
					CCSPtrArray<SCODATA> olScoData;
					CCSPtrArray<STEDATA> olSteData;
					CCSPtrArray<SWGDATA> olSwgData;
					MitarbeiterStammDlg *polDlg = new MitarbeiterStammDlg(&rlStf, 
																		  &olSorData,
																		  &olSpfData,
																		  &olSpeData,
																		  &olScoData,
																		  &olSteData,
																		  &olSwgData,
																		  this);
					polDlg->m_Caption = LoadStg(IDS_STRING151);
					if (polDlg->DoModal() == IDOK)
					{
						int i;
						rlStf.Urno = ogBasicData.GetNextUrno();	//new URNO
						rlStf.Cdat = CTime::GetCurrentTime();
						strcpy(rlStf.Usec,cgUserName);
						STFDATA *prlStf = new STFDATA;
						*prlStf = rlStf;
						AfxGetApp()->DoWaitCursor(1);
						if(ogStfData.Insert(prlStf)==false)
						{
							delete prlStf;
							omErrorTxt.Format("%s %d\n%s",omInsertErrTxt,ogStfData.imLastReturnCode, ogStfData.omLastErrorMessage);
							MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
						}	
						for(i = 0; i < olSorData.GetSize(); i++)
						{
							SORDATA *prlSor = new SORDATA;
							olSorData[i].Urno = ogBasicData.GetNextUrno();	//new URNO
							olSorData[i].Surn = rlStf.Urno;
							*prlSor = olSorData[i];
							if(ogSorData.Insert(prlSor)==false)
							{
								omErrorTxt.Format("%s %d\n%s",omInsertErrTxt,ogSorData.imLastReturnCode, ogSorData.omLastErrorMessage);
								MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
							}	
						}
						for(i = 0; i < olSpfData.GetSize(); i++)
						{
							SPFDATA *prlSpf = new SPFDATA;
							olSpfData[i].Urno = ogBasicData.GetNextUrno();	//new URNO
							olSpfData[i].Surn = rlStf.Urno;
							*prlSpf = olSpfData[i];
							if(ogSpfData.Insert(prlSpf)==false)
							{
								omErrorTxt.Format("%s %d\n%s",omInsertErrTxt,ogSpfData.imLastReturnCode, ogSpfData.omLastErrorMessage);
								MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
							}	
						}
						for(i = 0; i < olSpeData.GetSize(); i++)
						{
							SPEDATA *prlSpe = new SPEDATA;
							olSpeData[i].Urno = ogBasicData.GetNextUrno();	//new URNO
							olSpeData[i].Surn = rlStf.Urno;
							*prlSpe = olSpeData[i];
							if(ogSpeData.Insert(prlSpe)==false)
							{
								omErrorTxt.Format("%s %d\n%s",omInsertErrTxt,ogSpeData.imLastReturnCode, ogSpeData.omLastErrorMessage);
								MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
							}	
						}
						for(i = 0; i < olScoData.GetSize(); i++)
						{
							SCODATA *prlSco = new SCODATA;
							olScoData[i].Urno = ogBasicData.GetNextUrno();	//new URNO
							olScoData[i].Surn = rlStf.Urno;
							*prlSco = olScoData[i];
							if(ogScoData.Insert(prlSco)==false)
							{
								omErrorTxt.Format("%s %d\n%s",omInsertErrTxt,ogScoData.imLastReturnCode, ogScoData.omLastErrorMessage);
								MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
							}	
						}
						for(i = 0; i < olSteData.GetSize(); i++)
						{
							STEDATA *prlSte = new STEDATA;
							olSteData[i].Urno = ogBasicData.GetNextUrno();	//new URNO
							olSteData[i].Surn = rlStf.Urno;
							*prlSte = olSteData[i];
							if(ogSteData.Insert(prlSte)==false)
							{
								omErrorTxt.Format("%s %d\n%s",omInsertErrTxt,ogSteData.imLastReturnCode, ogSteData.omLastErrorMessage);
								MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
							}	
						}
						for(i = 0; i < olSwgData.GetSize(); i++)
						{
							SWGDATA *prlSwg = new SWGDATA;
							olSwgData[i].Urno = ogBasicData.GetNextUrno();	//new URNO
							olSwgData[i].Surn = rlStf.Urno;
							*prlSwg = olSwgData[i];
							if(ogSwgData.Insert(prlSwg)==false)
							{
								omErrorTxt.Format("%s %d\n%s",omInsertErrTxt,ogSwgData.imLastReturnCode, ogSwgData.omLastErrorMessage);
								MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
							}	
						}

						AfxGetApp()->DoWaitCursor(-1);
					}
					delete polDlg;
					olSorData.DeleteAll();
					olSpfData.DeleteAll();
					olSpeData.DeleteAll();
					olScoData.DeleteAll();
					olSteData.DeleteAll();
					olSwgData.DeleteAll();
				}
				break;
			case TAB_ERGABFERTIGUNGSARTEN:		
				{
					SPHDATA rlSph;
					ErgAbfertigungsartenDlg *olSphDlg = new ErgAbfertigungsartenDlg(&rlSph,this);
					olSphDlg->m_Caption = LoadStg(IDS_STRING151);
					if (olSphDlg->DoModal() == IDOK)
					{
						rlSph.Urno = ogBasicData.GetNextUrno();	//new URNO
						rlSph.Cdat = CTime::GetCurrentTime();
						strcpy(rlSph.Eart,"A");
						strcpy(rlSph.Usec,cgUserName);
						SPHDATA *prlSph = new SPHDATA;
						*prlSph = rlSph;
						AfxGetApp()->DoWaitCursor(1);
						if(ogSphData.Insert(prlSph)==false)
						{
							delete prlSph;
							omErrorTxt.Format("%s %d\n%s",omInsertErrTxt,ogSphData.imLastReturnCode, ogSphData.omLastErrorMessage);
							MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
						}	
						AfxGetApp()->DoWaitCursor(-1);
					}
					delete olSphDlg;
				}
				break;
			case TAB_REDUKTIONEN:			
				{
					PRCDATA rlPrc;
					ReduktionenDlg *polDlg = new ReduktionenDlg(&rlPrc, this);
					polDlg->m_Caption = LoadStg(IDS_STRING151);
					if (polDlg->DoModal() == IDOK)
					{
						rlPrc.Urno = ogBasicData.GetNextUrno();	//new URNO
						rlPrc.Cdat = CTime::GetCurrentTime();
						strcpy(rlPrc.Usec,cgUserName);
						PRCDATA *prlPrc = new PRCDATA;
						*prlPrc = rlPrc;
						AfxGetApp()->DoWaitCursor(1);
						if(ogPrcData.Insert(prlPrc)==false)
						{
							delete prlPrc;
							omErrorTxt.Format("%s %d\n%s",omInsertErrTxt,ogPrcData.imLastReturnCode, ogPrcData.omLastErrorMessage);
							MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
						}	
						AfxGetApp()->DoWaitCursor(-1);
					}
					delete polDlg;
				}
				break;
			case TAB_GERAETEGRUPPEN:			
				{
					GEGDATA rlGeg;
					GeraeteGruppenDlg *polDlg = new GeraeteGruppenDlg(&rlGeg, this);
					polDlg->m_Caption = LoadStg(IDS_STRING151);
					if (polDlg->DoModal() == IDOK)
					{
						rlGeg.Urno = ogBasicData.GetNextUrno();	//new URNO
						rlGeg.Cdat = CTime::GetCurrentTime();
						strcpy(rlGeg.Usec,cgUserName);
						GEGDATA *prlGeg = new GEGDATA;
						*prlGeg = rlGeg;
						AfxGetApp()->DoWaitCursor(1);
						if(ogGegData.Insert(prlGeg)==false)
						{
							delete prlGeg;
							omErrorTxt.Format("%s %d\n%s",omInsertErrTxt,ogGegData.imLastReturnCode, ogGegData.omLastErrorMessage);
							MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
						}	
						AfxGetApp()->DoWaitCursor(-1);
					}
					delete polDlg;
				}
				break;
			case TAB_ORGANIZER:			
				{
					CHTDATA rlCht;
					OrganizerDlg *polDlg = new OrganizerDlg(&rlCht, this);
					polDlg->m_Caption = LoadStg(IDS_STRING151);
					if (polDlg->DoModal() == IDOK)
					{
						rlCht.Urno = ogBasicData.GetNextUrno();	//new URNO
						rlCht.Cdat = CTime::GetCurrentTime();
						strcpy(rlCht.Usec,cgUserName);
						CHTDATA *prlCht = new CHTDATA;
						*prlCht = rlCht;
						AfxGetApp()->DoWaitCursor(1);
						if(ogChtData.Insert(prlCht)==false)
						{
							delete prlCht;
							omErrorTxt.Format("%s %d\n%s",omInsertErrTxt,ogChtData.imLastReturnCode, ogChtData.omLastErrorMessage);
							MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
						}	
						AfxGetApp()->DoWaitCursor(-1);
					}
					delete polDlg;
				}
				break;
			case TAB_TIMEPARAMETERS:			
				{
					TIPDATA rlTip;
					TimeParametersDlg *polDlg = new TimeParametersDlg(&rlTip, this);
					polDlg->m_Caption = LoadStg(IDS_STRING151);
					if (polDlg->DoModal() == IDOK)
					{
						rlTip.Urno = ogBasicData.GetNextUrno();	//new URNO
						rlTip.Cdat = CTime::GetCurrentTime();
						strcpy(rlTip.Usec,cgUserName);
						TIPDATA *prlTip = new TIPDATA;
						*prlTip = rlTip;
						AfxGetApp()->DoWaitCursor(1);
						if(ogTipData.Insert(prlTip)==false)
						{
							delete prlTip;
							omErrorTxt.Format("%s %d\n%s",omInsertErrTxt,ogTipData.imLastReturnCode, ogTipData.omLastErrorMessage);
							MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
						}	
						AfxGetApp()->DoWaitCursor(-1);
					}
					delete polDlg;
				}
				break;
			case TAB_HOLIDAY:			
				{
					HOLDATA rlHol;
					HolidayDlg *polDlg = new HolidayDlg(&rlHol, this);
					polDlg->m_Caption = LoadStg(IDS_STRING151);
					if (polDlg->DoModal() == IDOK)
					{
						rlHol.Urno = ogBasicData.GetNextUrno();	//new URNO
						rlHol.Cdat = CTime::GetCurrentTime();
						strcpy(rlHol.Usec,cgUserName);
						HOLDATA *prlHol = new HOLDATA;
						*prlHol = rlHol;
						AfxGetApp()->DoWaitCursor(1);
						if(ogHolData.Insert(prlHol)==false)
						{
							delete prlHol;
							omErrorTxt.Format("%s %d\n%s",omInsertErrTxt,ogHolData.imLastReturnCode, ogHolData.omLastErrorMessage);
							MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
						}	
						AfxGetApp()->DoWaitCursor(-1);
					}
					delete polDlg;
				}
				break;
			case TAB_PLANNING_GROUPS:			
				{
					PGPDATA rlPgp;
					PlanungsgruppenDlg *polDlg = new PlanungsgruppenDlg(&rlPgp, this);
					polDlg->m_Caption = LoadStg(IDS_STRING151);
					if (polDlg->DoModal() == IDOK)
					{
						rlPgp.Urno = ogBasicData.GetNextUrno();	//new URNO
						rlPgp.Cdat = CTime::GetCurrentTime();
						strcpy(rlPgp.Usec,cgUserName);
						rlPgp.Lstu = CTime::GetCurrentTime();
						strcpy(rlPgp.Useu,cgUserName);
						PGPDATA *prlPgp = new PGPDATA;
						*prlPgp = rlPgp;
						AfxGetApp()->DoWaitCursor(1);
						if(ogPgpData.Insert(prlPgp)==false)
						{
							delete prlPgp;
							omErrorTxt.Format("%s %d\n%s",omInsertErrTxt,ogPgpData.imLastReturnCode, ogPgpData.omLastErrorMessage);
							MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
						}	
						AfxGetApp()->DoWaitCursor(-1);
					}
					delete polDlg;
				}
				break;
			case TAB_AIRPORTWET:			
				{
					AWIDATA rlAwi;
					CAirPortWet *polDlg = new CAirPortWet(&rlAwi, this);
					polDlg->m_Caption = LoadStg(IDS_STRING151);
					if (polDlg->DoModal() == IDOK)
					{
						rlAwi.Urno = ogBasicData.GetNextUrno();	//new URNO
//						rlAwi.Cdat = CTime::GetCurrentTime();
//						strcpy(rlAwi.Usec,cgUserName);
//						rlAwi.Lstu = CTime::GetCurrentTime();
//						strcpy(rlAwi.Useu,cgUserName);
						AWIDATA *prlAwi = new AWIDATA;
						*prlAwi = rlAwi;
						AfxGetApp()->DoWaitCursor(1);
						if(ogAwiData.Insert(prlAwi)==false)
						{
							delete prlAwi;
							omErrorTxt.Format("%s %d\n%s",omInsertErrTxt,ogAwiData.imLastReturnCode, ogAwiData.omLastErrorMessage);
							MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
						}	
						AfxGetApp()->DoWaitCursor(-1);
					}
					delete polDlg;
				}
				break;
			case TAB_COUNTERCLASS:			
				{
					CCCDATA rlCcc;
					CChkInCounterClass *polDlg = new CChkInCounterClass(&rlCcc, this);
					polDlg->m_Caption = LoadStg(IDS_STRING151);
					if (polDlg->DoModal() == IDOK)
					{
						rlCcc.Urno = ogBasicData.GetNextUrno();	//new URNO
						rlCcc.Cdat = CTime::GetCurrentTime();
						strcpy(rlCcc.Usec,cgUserName);
						rlCcc.Lstu = CTime::GetCurrentTime();
						strcpy(rlCcc.Useu,cgUserName);
						CCCDATA *prlCcc = new CCCDATA;
						*prlCcc = rlCcc;
						AfxGetApp()->DoWaitCursor(1);
						if(ogCccData.Insert(prlCcc)==false)
						{
							delete prlCcc;
							omErrorTxt.Format("%s %d\n%s",omInsertErrTxt,ogCccData.imLastReturnCode, ogCccData.omLastErrorMessage);
							MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
						}	
						AfxGetApp()->DoWaitCursor(-1);
					}
					delete polDlg;
				}
				break;
			case TAB_VERYIMPPERS:			
				{
					VIPDATA rlVip;
					CVeryImpPers *polDlg = new CVeryImpPers(&rlVip, this);
					polDlg->m_Caption = LoadStg(IDS_STRING151);
					if (polDlg->DoModal() == IDOK)
					{
						rlVip.Urno = ogBasicData.GetNextUrno();	//new URNO
						rlVip.Cdat = CTime::GetCurrentTime();
						strcpy(rlVip.Usec,cgUserName);
						rlVip.Lstu = CTime::GetCurrentTime();
						strcpy(rlVip.Useu,cgUserName);
						VIPDATA *prlVip = new VIPDATA;
						*prlVip = rlVip;
						AfxGetApp()->DoWaitCursor(1);
						if(ogVipData.Insert(prlVip)==false)
						{
							delete prlVip;
							omErrorTxt.Format("%s %d\n%s",omInsertErrTxt,ogVipData.imLastReturnCode, ogVipData.omLastErrorMessage);
							MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
						}	
						AfxGetApp()->DoWaitCursor(-1);
					}
					delete polDlg;
				}
				break;
			case TAB_AIRCRAFTFAM:		
				{
					AFMDATA rlAfm;
					AircraftFamDlg *polDlg = new AircraftFamDlg(&rlAfm, this);
					polDlg->m_Caption = LoadStg(IDS_STRING151);
					if (polDlg->DoModal() == IDOK)
					{
						rlAfm.Urno = ogBasicData.GetNextUrno();	//new URNO
						rlAfm.Cdat = CTime::GetCurrentTime();
						strcpy(rlAfm.Usec,cgUserName);
						AFMDATA *prlAfm = new AFMDATA;
						*prlAfm = rlAfm;
						AfxGetApp()->DoWaitCursor(1);
						if(ogAFMData.Insert(prlAfm)==false)
						{
							delete prlAfm;
							omErrorTxt.Format("%s %d\n%s",omInsertErrTxt,ogAFMData.imLastReturnCode, ogAFMData.omLastErrorMessage);
							MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
						}	
						AfxGetApp()->DoWaitCursor(-1);
					}
					delete polDlg;
				}
				break;
			case TAB_ENGINETYPE:		
				{
					ENTDATA rlEnt;
					EngineTypeDlg *polDlg = new EngineTypeDlg(&rlEnt, this);
					polDlg->m_Caption = LoadStg(IDS_STRING151);
					if (polDlg->DoModal() == IDOK)
					{
						rlEnt.Urno = ogBasicData.GetNextUrno();	//new URNO
						rlEnt.Cdat = CTime::GetCurrentTime();
						strcpy(rlEnt.Usec,cgUserName);
						ENTDATA *prlEnt = new ENTDATA;
						*prlEnt = rlEnt;
						AfxGetApp()->DoWaitCursor(1);
						if(ogENTData.Insert(prlEnt)==false)
						{
							delete prlEnt;
							omErrorTxt.Format("%s %d\n%s",omInsertErrTxt,ogENTData.imLastReturnCode, ogENTData.omLastErrorMessage);
							MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
						}	
						AfxGetApp()->DoWaitCursor(-1);
					}
					delete polDlg;
				}
				break;
			default:
			{

			}
			break; // Do nothing
		}
		bgIsDialogOpen = false;
	}		
}

//------------------------------------------------------------------------------------------------------

void CStammdaten::OnKopieren() 
{
	if(!bgIsDialogOpen)
	{
		bgIsDialogOpen = true;
		switch(igSelectTab)
		{
			case TAB_AIRLINE:
				{
					int ilLineNo = pomTable->GetCurSel();
					if (ilLineNo == -1)
					{
						MessageBox(LoadStg(IDS_STRING148),LoadStg(IDS_STRING149),MB_ICONEXCLAMATION);
						break;
					}

					ALTDATA *prlOldALT = ogALTData.GetALTByUrno(pomAirlineViewer->omLines[ilLineNo].Urno);
					if (prlOldALT != NULL)
					{
						ALTDATA rlALT = *prlOldALT;
						rlALT.Urno = 0;
						rlALT.Cdat = TIMENULL;
						rlALT.Lstu = TIMENULL;
						rlALT.Usec[0] = '\0';
						rlALT.Useu[0] = '\0';
						AirlineDlg *olALTDlg = new AirlineDlg(&rlALT,this);
						olALTDlg->m_Caption = LoadStg(IDS_STRING152);
						if (olALTDlg->DoModal() == IDOK)
						{
							rlALT.Urno = ogBasicData.GetNextUrno();	//new URNO
							rlALT.Cdat = CTime::GetCurrentTime();
							strcpy(rlALT.Usec,cgUserName);
							ALTDATA *prlALT = new ALTDATA;
							*prlALT = rlALT;
							AfxGetApp()->DoWaitCursor(1);
							if(ogALTData.InsertALT(prlALT)==false)
							{
								delete prlALT;
								omErrorTxt.Format("%s %d\n%s",omInsertErrTxt,ogALTData.imLastReturnCode, ogALTData.omLastErrorMessage);
								MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
							}	
							AfxGetApp()->DoWaitCursor(-1);
						}
						delete olALTDlg;
					}
				}
				break;
			case TAB_AIRCRAFT:
				{
					int ilLineNo = pomTable->GetCurSel();
					if (ilLineNo == -1)
					{
						MessageBox(LoadStg(IDS_STRING148),LoadStg(IDS_STRING149),MB_ICONEXCLAMATION);
						break;
					}
					ACTDATA *prlOldACT = ogACTData.GetACTByUrno(pomAircraftViewer->omLines[ilLineNo].Urno);
					if (prlOldACT != NULL)
					{
						ACTDATA rlACT = *prlOldACT;
						rlACT.Urno = 0;
						rlACT.Cdat = TIMENULL;
						rlACT.Lstu = TIMENULL;
						rlACT.Usec[0] = '\0';
						rlACT.Useu[0] = '\0';
						AircraftDlg *olACTDlg = new AircraftDlg(&rlACT,this);
						olACTDlg->m_Caption = LoadStg(IDS_STRING152);
						if (olACTDlg->DoModal() == IDOK)
						{
							rlACT.Urno = ogBasicData.GetNextUrno();	//new URNO
							rlACT.Cdat = CTime::GetCurrentTime();
							strcpy(rlACT.Usec,cgUserName);
							ACTDATA *prlACT = new ACTDATA;
							*prlACT = rlACT;
							AfxGetApp()->DoWaitCursor(1);
							if(ogACTData.InsertACT(prlACT)==false)
							{
								delete prlACT;
								omErrorTxt.Format("%s %d\n%s",omInsertErrTxt,ogACTData.imLastReturnCode, ogACTData.omLastErrorMessage);
								MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
							}	
							AfxGetApp()->DoWaitCursor(-1);
						}
						delete olACTDlg;
					}
				}
				break;
			case TAB_LFZREGISTRATION:
				{
					int ilLineNo = pomTable->GetCurSel();
					if (ilLineNo == -1)
					{
						MessageBox(LoadStg(IDS_STRING148),LoadStg(IDS_STRING149),MB_ICONEXCLAMATION);
						break;
					}
					ACRDATA *prlOldACR = ogACRData.GetACRByUrno(pomLFZRegiViewer->omLines[ilLineNo].Urno);
					if (prlOldACR != NULL)
					{
						ACRDATA rlACR = *prlOldACR;
						rlACR.Urno = 0;
						rlACR.Cdat = TIMENULL;
						rlACR.Lstu = TIMENULL;
						rlACR.Usec[0] = '\0';
						rlACR.Useu[0] = '\0';
						LFZRegiDlg *olACRDlg = new LFZRegiDlg(&rlACR,this);
						olACRDlg->m_Caption = LoadStg(IDS_STRING152);
						if (olACRDlg->DoModal() == IDOK)
						{
							rlACR.Urno = ogBasicData.GetNextUrno();	//new URNO
							strcpy(rlACR.Hopo, pcgHome);
							rlACR.Cdat = CTime::GetCurrentTime();
							strcpy(rlACR.Usec,cgUserName);
							ACRDATA *prlACR = new ACRDATA;
							*prlACR = rlACR;
							AfxGetApp()->DoWaitCursor(1);
							if(ogACRData.InsertACR(prlACR)==false)
							{
								delete prlACR;
								omErrorTxt.Format("%s %d\n%s",omInsertErrTxt,ogACRData.imLastReturnCode, ogACRData.omLastErrorMessage);
								MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
							}	
							AfxGetApp()->DoWaitCursor(-1);
						}
						delete olACRDlg;
					}
				}
				break;
			case TAB_AIRPORT:
				{
					int ilLineNo = pomTable->GetCurSel();
					if (ilLineNo == -1)
					{
						MessageBox(LoadStg(IDS_STRING148),LoadStg(IDS_STRING149),MB_ICONEXCLAMATION);
						break;
					}
					APTDATA *prlOldAPT = ogAPTData.GetAPTByUrno(pomAirportViewer->omLines[ilLineNo].Urno);
					if (prlOldAPT != NULL)
					{
						APTDATA rlAPT = *prlOldAPT;
						rlAPT.Urno = 0;
						rlAPT.Cdat = TIMENULL;
						rlAPT.Lstu = TIMENULL;
						rlAPT.Usec[0] = '\0';
						rlAPT.Useu[0] = '\0';
						AirportDlg *olAPTDlg = new AirportDlg(&rlAPT,this);
						olAPTDlg->m_Caption = LoadStg(IDS_STRING152);
						if (olAPTDlg->DoModal() == IDOK)
						{
							rlAPT.Urno = ogBasicData.GetNextUrno();	//new URNO
							rlAPT.Cdat = CTime::GetCurrentTime();
							strcpy(rlAPT.Usec,cgUserName);
							APTDATA *prlAPT = new APTDATA;
							*prlAPT = rlAPT;
							AfxGetApp()->DoWaitCursor(1);
							if(ogAPTData.InsertAPT(prlAPT)==false)
							{
								delete prlAPT;
								omErrorTxt.Format("%s %d\n%s",omInsertErrTxt,ogAPTData.imLastReturnCode, ogAPTData.omLastErrorMessage);
								MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
							}	
							AfxGetApp()->DoWaitCursor(-1);
						}
						delete olAPTDlg;
					}
				}
				break;
			case TAB_RUNWAY:
				{
					int ilLineNo = pomTable->GetCurSel();
					if (ilLineNo == -1)
					{
						MessageBox(LoadStg(IDS_STRING148),LoadStg(IDS_STRING149),MB_ICONEXCLAMATION);
						break;
					}
					RWYDATA *prlOldRWY = ogRWYData.GetRWYByUrno(pomRunwayViewer->omLines[ilLineNo].Urno);
					if (prlOldRWY != NULL)
					{
						RWYDATA rlRWY = *prlOldRWY;
						rlRWY.Urno = 0;
						rlRWY.Cdat = TIMENULL;
						rlRWY.Lstu = TIMENULL;
						rlRWY.Usec[0] = '\0';
						rlRWY.Useu[0] = '\0';
						RunwayDlg *olRWYDlg = new RunwayDlg(&rlRWY,this);
						olRWYDlg->m_Caption = LoadStg(IDS_STRING152);
						if (olRWYDlg->DoModal() == IDOK)
						{
							rlRWY.Urno = ogBasicData.GetNextUrno();	//new URNO
							rlRWY.Cdat = CTime::GetCurrentTime();
							strcpy(rlRWY.Usec,cgUserName);
							RWYDATA *prlRWY = new RWYDATA;
							*prlRWY = rlRWY;
							AfxGetApp()->DoWaitCursor(1);
							if(ogRWYData.InsertRWY(prlRWY)==false)
							{
								delete prlRWY;
								omErrorTxt.Format("%s %d\n%s",omInsertErrTxt,ogRWYData.imLastReturnCode, ogRWYData.omLastErrorMessage);
								MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
							}	
							AfxGetApp()->DoWaitCursor(-1);
						}
						delete olRWYDlg;
					} 
				}
				break;
			case TAB_TAXIWAY:
				{
					int ilLineNo = pomTable->GetCurSel();
					if (ilLineNo == -1)
					{
						MessageBox(LoadStg(IDS_STRING148),LoadStg(IDS_STRING149),MB_ICONEXCLAMATION);
						break;
					}
					TWYDATA *prlOldTWY = ogTWYData.GetTWYByUrno(pomTaxiwayViewer->omLines[ilLineNo].Urno);
					if (prlOldTWY != NULL)
					{
						TWYDATA rlTWY = *prlOldTWY;
						rlTWY.Urno = 0;
						rlTWY.Cdat = TIMENULL;
						rlTWY.Lstu = TIMENULL;
						rlTWY.Usec[0] = '\0';
						rlTWY.Useu[0] = '\0';
						TaxiwayDlg *olTWYDlg = new TaxiwayDlg(&rlTWY,this);
						olTWYDlg->m_Caption = LoadStg(IDS_STRING152);
						if (olTWYDlg->DoModal() == IDOK)
						{
							rlTWY.Urno = ogBasicData.GetNextUrno();	//new URNO
							rlTWY.Cdat = CTime::GetCurrentTime();
							strcpy(rlTWY.Usec,cgUserName);
							TWYDATA *prlTWY = new TWYDATA;
							*prlTWY = rlTWY;
							AfxGetApp()->DoWaitCursor(1);
							if(ogTWYData.InsertTWY(prlTWY)==false)
							{
								delete prlTWY;
								omErrorTxt.Format("%s %d\n%s",omInsertErrTxt,ogTWYData.imLastReturnCode, ogTWYData.omLastErrorMessage);
								MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
							}	
							AfxGetApp()->DoWaitCursor(-1);
						}
						delete olTWYDlg;
					} 
				}
				break;
			case TAB_POSITION:
				{
					int ilLineNo = pomTable->GetCurSel();
					if (ilLineNo == -1)
					{
						MessageBox(LoadStg(IDS_STRING148),LoadStg(IDS_STRING149),MB_ICONEXCLAMATION);
						break;
					}
					PSTDATA *prlOldPST = ogPSTData.GetPSTByUrno(pomPositionViewer->omLines[ilLineNo].Urno);
					if (prlOldPST != NULL)
					{
						PSTDATA rlPST = *prlOldPST;
						rlPST.Urno = 0;
						rlPST.Cdat = TIMENULL;
						rlPST.Lstu = TIMENULL;
						rlPST.Usec[0] = '\0';
						rlPST.Useu[0] = '\0';
						PositionDlg *olPSTDlg = new PositionDlg(&rlPST,this);
						olPSTDlg->m_Caption = LoadStg(IDS_STRING152);
						if (olPSTDlg->DoModal() == IDOK)
						{
							rlPST.Urno = ogBasicData.GetNextUrno();	//new URNO
							rlPST.Cdat = CTime::GetCurrentTime();
							strcpy(rlPST.Usec,cgUserName);
							PSTDATA *prlPST = new PSTDATA;
							*prlPST = rlPST;
							AfxGetApp()->DoWaitCursor(1);
							if(ogPSTData.InsertPST(prlPST)==false)
							{
								delete prlPST;
								omErrorTxt.Format("%s %d\n%s",omInsertErrTxt,ogPSTData.imLastReturnCode, ogPSTData.omLastErrorMessage);
								MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
							}
							
							CString olBurn;
							olBurn.Format("%d",rlPST.Urno);
							UpdateBlkTable(&olPSTDlg->omBlkPtrA,&olPSTDlg->omDeleteBlkPtrA,olBurn,"PST");

							AfxGetApp()->DoWaitCursor(-1);
						}
						delete olPSTDlg;
					} 
				}
				break;
			case TAB_GATE:
				{
					int ilLineNo = pomTable->GetCurSel();
					if (ilLineNo == -1)
					{
						MessageBox(LoadStg(IDS_STRING148),LoadStg(IDS_STRING149),MB_ICONEXCLAMATION);
						break;
					}
					GATDATA *prlOldGAT = ogGATData.GetGATByUrno(pomGateViewer->omLines[ilLineNo].Urno);
					if (prlOldGAT != NULL)
					{
						GATDATA rlGAT = *prlOldGAT;
						rlGAT.Urno = 0;
						rlGAT.Cdat = TIMENULL;
						rlGAT.Lstu = TIMENULL;
						rlGAT.Usec[0] = '\0';
						rlGAT.Useu[0] = '\0';
						GateDlg *olGATDlg = new GateDlg(&rlGAT,this);
						olGATDlg->m_Caption = LoadStg(IDS_STRING152);
						if (olGATDlg->DoModal() == IDOK)
						{
							rlGAT.Urno = ogBasicData.GetNextUrno();	//new URNO
							rlGAT.Cdat = CTime::GetCurrentTime();
							strcpy(rlGAT.Usec,cgUserName);
							GATDATA *prlGAT = new GATDATA;
							*prlGAT = rlGAT;
							AfxGetApp()->DoWaitCursor(1);
							if(ogGATData.InsertGAT(prlGAT)==false)
							{
								delete prlGAT;
								omErrorTxt.Format("%s %d\n%s",omInsertErrTxt,ogGATData.imLastReturnCode, ogGATData.omLastErrorMessage);
								MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
							}
							
							CString olBurn;
							olBurn.Format("%d",rlGAT.Urno);
							UpdateBlkTable(&olGATDlg->omBlkPtrA,&olGATDlg->omDeleteBlkPtrA,olBurn,"GAT");

							AfxGetApp()->DoWaitCursor(-1);
						}
						delete olGATDlg;
					} 
				}
				break;
			case TAB_CHECKINCOUNTER:
				{
					int ilLineNo = pomTable->GetCurSel();
					if (ilLineNo == -1)
					{
						MessageBox(LoadStg(IDS_STRING148),LoadStg(IDS_STRING149),MB_ICONEXCLAMATION);
						break;
					}
					CICDATA *prlOldCIC = ogCICData.GetCICByUrno(pomCheckinCounterViewer->omLines[ilLineNo].Urno);
					if (prlOldCIC != NULL)
					{
						CICDATA rlCIC = *prlOldCIC;
						rlCIC.Urno = 0;
						rlCIC.Cdat = TIMENULL;
						rlCIC.Lstu = TIMENULL;
						rlCIC.Usec[0] = '\0';
						rlCIC.Useu[0] = '\0';
						CheckinCounterDlg *olCICDlg = new CheckinCounterDlg(&rlCIC,this);
						olCICDlg->m_Caption = LoadStg(IDS_STRING152);
						if (olCICDlg->DoModal() == IDOK)
						{
							rlCIC.Urno = ogBasicData.GetNextUrno();	//new URNO
							rlCIC.Cdat = CTime::GetCurrentTime();
							strcpy(rlCIC.Usec,cgUserName);
							CICDATA *prlCIC = new CICDATA;
							*prlCIC = rlCIC;
							AfxGetApp()->DoWaitCursor(1);
							if(ogCICData.InsertCIC(prlCIC)==false)
							{
								delete prlCIC;
								omErrorTxt.Format("%s %d\n%s",omInsertErrTxt,ogCICData.imLastReturnCode, ogCICData.omLastErrorMessage);
								MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
							}	

							CString olBurn;
							olBurn.Format("%d",rlCIC.Urno);
							UpdateBlkTable(&olCICDlg->omBlkPtrA,&olCICDlg->omDeleteBlkPtrA,olBurn,"CIC");

							AfxGetApp()->DoWaitCursor(-1);
						}
						delete olCICDlg;
					} 
				}
				break;
			case TAB_BEGGAGEBELT:
				{
					int ilLineNo = pomTable->GetCurSel();
					if (ilLineNo == -1)
					{
						MessageBox(LoadStg(IDS_STRING148),LoadStg(IDS_STRING149),MB_ICONEXCLAMATION);
						break;
					}
					BLTDATA *prlOldBLT = ogBLTData.GetBLTByUrno(pomBeggagebeltViewer->omLines[ilLineNo].Urno);
					if (prlOldBLT != NULL)
					{
						BLTDATA rlBLT = *prlOldBLT;
						rlBLT.Urno = 0;
						rlBLT.Cdat = TIMENULL;
						rlBLT.Lstu = TIMENULL;
						rlBLT.Usec[0] = '\0';
						rlBLT.Useu[0] = '\0';
						BeggagebeltDlg *olBLTDlg = new BeggagebeltDlg(&rlBLT,this);
						olBLTDlg->m_Caption = LoadStg(IDS_STRING152);
						if (olBLTDlg->DoModal() == IDOK)
						{
							rlBLT.Urno = ogBasicData.GetNextUrno();	//new URNO
							rlBLT.Cdat = CTime::GetCurrentTime();
							strcpy(rlBLT.Usec,cgUserName);
							BLTDATA *prlBLT = new BLTDATA;
							*prlBLT = rlBLT;
							AfxGetApp()->DoWaitCursor(1);
							if(ogBLTData.InsertBLT(prlBLT)==false)
							{
								delete prlBLT;
								omErrorTxt.Format("%s %d\n%s",omInsertErrTxt,ogBLTData.imLastReturnCode, ogBLTData.omLastErrorMessage);
								MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
							}
							
							CString olBurn;
							olBurn.Format("%d",rlBLT.Urno);
							UpdateBlkTable(&olBLTDlg->omBlkPtrA,&olBLTDlg->omDeleteBlkPtrA,olBurn,"BLT");

							AfxGetApp()->DoWaitCursor(-1);
						}
						delete olBLTDlg;
					} 
				}
				break;
			case TAB_EXIT:
				{
					int ilLineNo = pomTable->GetCurSel();
					if (ilLineNo == -1)
					{
						MessageBox(LoadStg(IDS_STRING148),LoadStg(IDS_STRING149),MB_ICONEXCLAMATION);
						break;
					}
					EXTDATA *prlOldEXT = ogEXTData.GetEXTByUrno(pomExitViewer->omLines[ilLineNo].Urno);
					if (prlOldEXT != NULL)
					{
						EXTDATA rlEXT = *prlOldEXT;
						rlEXT.Urno = 0;
						rlEXT.Cdat = TIMENULL;
						rlEXT.Lstu = TIMENULL;
						rlEXT.Usec[0] = '\0';
						rlEXT.Useu[0] = '\0';
						ExitDlg *olEXTDlg = new ExitDlg(&rlEXT,this);
						olEXTDlg->m_Caption = LoadStg(IDS_STRING152);
						if (olEXTDlg->DoModal() == IDOK)
						{
							rlEXT.Urno = ogBasicData.GetNextUrno();	//new URNO
							rlEXT.Cdat = CTime::GetCurrentTime();
							strcpy(rlEXT.Usec,cgUserName);
							EXTDATA *prlEXT = new EXTDATA;
							*prlEXT = rlEXT;
							AfxGetApp()->DoWaitCursor(1);
							if(ogEXTData.InsertEXT(prlEXT)==false)
							{
								delete prlEXT;
								omErrorTxt.Format("%s %d\n%s",omInsertErrTxt,ogEXTData.imLastReturnCode, ogEXTData.omLastErrorMessage);
								MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
							}	
							AfxGetApp()->DoWaitCursor(-1);
						}
						delete olEXTDlg;
					}
				}
				break;
			case TAB_DELAYCODE:
				{
					int ilLineNo = pomTable->GetCurSel();
					if (ilLineNo == -1)
					{
						MessageBox(LoadStg(IDS_STRING148),LoadStg(IDS_STRING149),MB_ICONEXCLAMATION);
						break;
					}
					DENDATA *prlOldDEN = ogDENData.GetDENByUrno(pomDelaycodeViewer->omLines[ilLineNo].Urno);
					if (prlOldDEN != NULL)
					{
						DENDATA rlDEN = *prlOldDEN;
						rlDEN.Urno = 0;
						rlDEN.Cdat = TIMENULL;
						rlDEN.Lstu = TIMENULL;
						rlDEN.Usec[0] = '\0';
						rlDEN.Useu[0] = '\0';
						DelaycodeDlg *olDENDlg = new DelaycodeDlg(&rlDEN,this);
						olDENDlg->m_Caption = LoadStg(IDS_STRING152);
						if (olDENDlg->DoModal() == IDOK)
						{
							rlDEN.Urno = ogBasicData.GetNextUrno();	//new URNO
							rlDEN.Cdat = CTime::GetCurrentTime();
							strcpy(rlDEN.Usec,cgUserName);
							DENDATA *prlDEN = new DENDATA;
							*prlDEN = rlDEN;
							AfxGetApp()->DoWaitCursor(1);
							if(ogDENData.InsertDEN(prlDEN)==false)
							{
								delete prlDEN;
								omErrorTxt.Format("%s %d\n%s",omInsertErrTxt,ogDENData.imLastReturnCode, ogDENData.omLastErrorMessage);
								MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
							}	
							AfxGetApp()->DoWaitCursor(-1);
						}
						delete olDENDlg;
					} 
				}
				break;
			case TAB_TELEXADRESS:
				{
					int ilLineNo = pomTable->GetCurSel();
					if (ilLineNo == -1)
					{
						MessageBox(LoadStg(IDS_STRING148),LoadStg(IDS_STRING149),MB_ICONEXCLAMATION);
						break;
					}
					MVTDATA *prlOldMVT = ogMVTData.GetMVTByUrno(pomTelexadressViewer->omLines[ilLineNo].Urno);
					if (prlOldMVT != NULL)
					{
						MVTDATA rlMVT = *prlOldMVT;
						rlMVT.Urno = 0;
						rlMVT.Cdat = TIMENULL;
						rlMVT.Lstu = TIMENULL;
						rlMVT.Usec[0] = '\0';
						rlMVT.Useu[0] = '\0';
						TelexadressDlg *olMVTDlg = new TelexadressDlg(&rlMVT,this);
						olMVTDlg->m_Caption = LoadStg(IDS_STRING152);
						if (olMVTDlg->DoModal() == IDOK)
						{
							rlMVT.Urno = ogBasicData.GetNextUrno();	//new URNO
							rlMVT.Cdat = CTime::GetCurrentTime();
							strcpy(rlMVT.Usec,cgUserName);
							MVTDATA *prlMVT = new MVTDATA;
							*prlMVT = rlMVT;
							AfxGetApp()->DoWaitCursor(1);
							if(ogMVTData.InsertMVT(prlMVT)==false)
							{
								delete prlMVT;
								omErrorTxt.Format("%s %d\n%s",omInsertErrTxt,ogMVTData.imLastReturnCode, ogMVTData.omLastErrorMessage);
								MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
							}	
							AfxGetApp()->DoWaitCursor(-1);
						}
						delete olMVTDlg;
					} 
				}
				break;
			case TAB_TRAFFICTYPE:
				{
					int ilLineNo = pomTable->GetCurSel();
					if (ilLineNo == -1)
					{
						MessageBox(LoadStg(IDS_STRING148),LoadStg(IDS_STRING149),MB_ICONEXCLAMATION);
						break;
					}
					NATDATA *prlOldNAT = ogNATData.GetNATByUrno(pomTraffictypeViewer->omLines[ilLineNo].Urno);
					if (prlOldNAT != NULL)
					{
						NATDATA rlNAT = *prlOldNAT;
						rlNAT.Urno = 0;
						rlNAT.Cdat = TIMENULL;
						rlNAT.Lstu = TIMENULL;
						rlNAT.Usec[0] = '\0';
						rlNAT.Useu[0] = '\0';
						TraffictypeDlg *olNATDlg = new TraffictypeDlg(&rlNAT,this);
						olNATDlg->m_Caption = LoadStg(IDS_STRING152);
						if (olNATDlg->DoModal() == IDOK)
						{
							rlNAT.Urno = ogBasicData.GetNextUrno();	//new URNO
							rlNAT.Cdat = CTime::GetCurrentTime();
							strcpy(rlNAT.Usec,cgUserName);
							NATDATA *prlNAT = new NATDATA;
							*prlNAT = rlNAT;
							AfxGetApp()->DoWaitCursor(1);
							if(ogNATData.InsertNAT(prlNAT)==false)
							{
								delete prlNAT;
								omErrorTxt.Format("%s %d\n%s",omInsertErrTxt,ogNATData.imLastReturnCode, ogNATData.omLastErrorMessage);
								MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
							}	
							AfxGetApp()->DoWaitCursor(-1);
						}
						delete olNATDlg;
					} 
				}
				break;
			case TAB_HANDLINGAGENT:
				{
					int ilLineNo = pomTable->GetCurSel();
					if (ilLineNo == -1)
					{
						MessageBox(LoadStg(IDS_STRING148),LoadStg(IDS_STRING149),MB_ICONEXCLAMATION);
						break;
					}
					HAGDATA *prlOldHAG = ogHAGData.GetHAGByUrno(pomHandlingagentViewer->omLines[ilLineNo].Urno);
					if (prlOldHAG != NULL)
					{
						HAGDATA rlHAG = *prlOldHAG;
						rlHAG.Urno = 0;
						rlHAG.Cdat = TIMENULL;
						rlHAG.Lstu = TIMENULL;
						rlHAG.Usec[0] = '\0';
						rlHAG.Useu[0] = '\0';
						HandlingagentDlg *olHAGDlg = new HandlingagentDlg(&rlHAG,this);
						olHAGDlg->m_Caption = LoadStg(IDS_STRING152);
						if (olHAGDlg->DoModal() == IDOK)
						{
							rlHAG.Urno = ogBasicData.GetNextUrno();	//new URNO
							rlHAG.Cdat = CTime::GetCurrentTime();
							strcpy(rlHAG.Usec,cgUserName);
							HAGDATA *prlHAG = new HAGDATA;
							*prlHAG = rlHAG;
							AfxGetApp()->DoWaitCursor(1);
							if(ogHAGData.InsertHAG(prlHAG)==false)
							{
								delete prlHAG;
								omErrorTxt.Format("%s %d\n%s",omInsertErrTxt,ogHAGData.imLastReturnCode, ogHAGData.omLastErrorMessage);
								MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
							}	
							AfxGetApp()->DoWaitCursor(-1);
						}
						delete olHAGDlg;
					}
				}
				break;
			case TAB_WAITINGROOM:
				{
					int ilLineNo = pomTable->GetCurSel();
					if (ilLineNo == -1)
					{
						MessageBox(LoadStg(IDS_STRING148),LoadStg(IDS_STRING149),MB_ICONEXCLAMATION);
						break;
					}
					WRODATA *prlOldWRO = ogWROData.GetWROByUrno(pomWaitingroomViewer->omLines[ilLineNo].Urno);
					if (prlOldWRO != NULL)
					{
						WRODATA rlWRO = *prlOldWRO;
						rlWRO.Urno = 0;
						rlWRO.Cdat = TIMENULL;
						rlWRO.Lstu = TIMENULL;
						rlWRO.Usec[0] = '\0';
						rlWRO.Useu[0] = '\0';
						WaitingroomDlg *olWRODlg = new WaitingroomDlg(&rlWRO,this);
						olWRODlg->m_Caption = LoadStg(IDS_STRING152);
						if (olWRODlg->DoModal() == IDOK)
						{
							rlWRO.Urno = ogBasicData.GetNextUrno();	//new URNO
							rlWRO.Cdat = CTime::GetCurrentTime();
							strcpy(rlWRO.Usec,cgUserName);
							WRODATA *prlWRO = new WRODATA;
							*prlWRO = rlWRO;
							AfxGetApp()->DoWaitCursor(1);
							if(ogWROData.InsertWRO(prlWRO)==false)
							{
								delete prlWRO;
								omErrorTxt.Format("%s %d\n%s",omInsertErrTxt,ogWROData.imLastReturnCode, ogWROData.omLastErrorMessage);
								MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
							}	
							else
							{
								CString olBurn;
								olBurn.Format("%d",rlWRO.Urno);
								UpdateBlkTable(&olWRODlg->omBlkPtrA,&olWRODlg->omDeleteBlkPtrA,olBurn,"WRO");
							}

							AfxGetApp()->DoWaitCursor(-1);
						}
						delete olWRODlg;
					} 
				}
				break;
			case TAB_HANDLINGTYPE:
				{
					int ilLineNo = pomTable->GetCurSel();
					if (ilLineNo == -1)
					{
						MessageBox(LoadStg(IDS_STRING148),LoadStg(IDS_STRING149),MB_ICONEXCLAMATION);
						break;
					}
					HTYDATA *prlOldHTY = ogHTYData.GetHTYByUrno(pomHandlingtypeViewer->omLines[ilLineNo].Urno);
					if (prlOldHTY != NULL)
					{
						HTYDATA rlHTY = *prlOldHTY;
						rlHTY.Urno = 0;
						rlHTY.Cdat = TIMENULL;
						rlHTY.Lstu = TIMENULL;
						rlHTY.Usec[0] = '\0';
						rlHTY.Useu[0] = '\0';
						HandlingtypeDlg *olHTYDlg = new HandlingtypeDlg(&rlHTY,this);
						olHTYDlg->m_Caption = LoadStg(IDS_STRING152);
						if (olHTYDlg->DoModal() == IDOK)
						{
							rlHTY.Urno = ogBasicData.GetNextUrno();	//new URNO
							rlHTY.Cdat = CTime::GetCurrentTime();
							strcpy(rlHTY.Usec,cgUserName);
							HTYDATA *prlHTY = new HTYDATA;
							*prlHTY = rlHTY;
							AfxGetApp()->DoWaitCursor(1);
							if(ogHTYData.InsertHTY(prlHTY)==false)
							{
								delete prlHTY;
								omErrorTxt.Format("%s %d\n%s",omInsertErrTxt,ogHTYData.imLastReturnCode, ogHTYData.omLastErrorMessage);
								MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
							}	
							AfxGetApp()->DoWaitCursor(-1);
						}
						delete olHTYDlg;
					} 
				}
				break;
			case TAB_SERVICETYPE:
				{
					int ilLineNo = pomTable->GetCurSel();
					if (ilLineNo == -1)
					{
						MessageBox(LoadStg(IDS_STRING148),LoadStg(IDS_STRING149),MB_ICONEXCLAMATION);
						break;
					}
					STYDATA *prlOldSTY = ogSTYData.GetSTYByUrno(pomServicetypeViewer->omLines[ilLineNo].Urno);
					if (prlOldSTY != NULL)
					{
						STYDATA rlSTY = *prlOldSTY;
						rlSTY.Urno = 0;
						rlSTY.Cdat = TIMENULL;
						rlSTY.Lstu = TIMENULL;
						rlSTY.Usec[0] = '\0';
						rlSTY.Useu[0] = '\0';
						ServicetypeDlg *olSTYDlg = new ServicetypeDlg(&rlSTY,this);
						olSTYDlg->m_Caption = LoadStg(IDS_STRING152);
						if (olSTYDlg->DoModal() == IDOK)
						{
							rlSTY.Urno = ogBasicData.GetNextUrno();	//new URNO
							rlSTY.Cdat = CTime::GetCurrentTime();
							strcpy(rlSTY.Usec,cgUserName);
							STYDATA *prlSTY = new STYDATA;
							*prlSTY = rlSTY;
							AfxGetApp()->DoWaitCursor(1);
							if(ogSTYData.InsertSTY(prlSTY)==false)
							{
								delete prlSTY;
								omErrorTxt.Format("%s %d\n%s",omInsertErrTxt,ogSTYData.imLastReturnCode, ogSTYData.omLastErrorMessage);
								MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
							}	
							AfxGetApp()->DoWaitCursor(-1);
						}
						delete olSTYDlg;
					} 
				}
				break;
			case TAB_FIDSCOMMAND:
				{		
					int ilLineNo = pomTable->GetCurSel();
					if (ilLineNo == -1)
					{
						MessageBox(LoadStg(IDS_STRING148),LoadStg(IDS_STRING149),MB_ICONEXCLAMATION);
						break;
					}
					FIDDATA *prlOldFID = ogFIDData.GetFIDByUrno(pomFidsCommandViewer->omLines[ilLineNo].Urno);
					if (prlOldFID != NULL)
					{
						FIDDATA rlFID = *prlOldFID;
						rlFID.Urno = 0;
						rlFID.Cdat = TIMENULL;
						rlFID.Lstu = TIMENULL;
						rlFID.Usec[0] = '\0';
						rlFID.Useu[0] = '\0';
						FidsCommandDlg *olFIDDlg = new FidsCommandDlg(&rlFID,this);
						olFIDDlg->m_Caption = LoadStg(IDS_STRING152);
						if (olFIDDlg->DoModal() == IDOK)
						{
							rlFID.Urno = ogBasicData.GetNextUrno();	//new URNO
							rlFID.Cdat = CTime::GetCurrentTime();
							strcpy(rlFID.Usec,cgUserName);
							FIDDATA *prlFID = new FIDDATA;
							*prlFID = rlFID;
							AfxGetApp()->DoWaitCursor(1);
							if(ogFIDData.InsertFID(prlFID)==false)
							{
								delete prlFID;
								omErrorTxt.Format("%s %d\n%s",omInsertErrTxt,ogFIDData.imLastReturnCode, ogFIDData.omLastErrorMessage);
								MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
							}	
							AfxGetApp()->DoWaitCursor(-1);
						}
						delete olFIDDlg;
					}
				}
				break;
			case TAB_ERGVERKEHRSARTEN:
				{
					int ilLineNo = pomTable->GetCurSel();
					if (ilLineNo == -1)
					{
						MessageBox(LoadStg(IDS_STRING148),LoadStg(IDS_STRING149),MB_ICONEXCLAMATION);
						break;
					}
					SPHDATA *prlOldSph = ogSphData.GetSphByUrno(pomErgVerkehrsartenViewer->omLines[ilLineNo].Urno);
					if (prlOldSph != NULL)
					{
						SPHDATA rlSph = *prlOldSph;
						rlSph.Urno = 0;
						rlSph.Cdat = TIMENULL;
						rlSph.Lstu = TIMENULL;
						rlSph.Usec[0] = '\0';
						rlSph.Useu[0] = '\0';
						ErgVerkehrsartenDlg *olSphDlg = new ErgVerkehrsartenDlg(&rlSph,this);
						olSphDlg->m_Caption = LoadStg(IDS_STRING152);
						if (olSphDlg->DoModal() == IDOK)
						{
							rlSph.Urno = ogBasicData.GetNextUrno();	//new URNO
							rlSph.Cdat = CTime::GetCurrentTime();
							strcpy(rlSph.Eart,"V");
							strcpy(rlSph.Usec,cgUserName);
							SPHDATA *prlSph = new SPHDATA;
							*prlSph = rlSph;
							AfxGetApp()->DoWaitCursor(1);
							if(ogSphData.Insert(prlSph)==false)
							{
								delete prlSph;
								omErrorTxt.Format("%s %d\n%s",omInsertErrTxt,ogSphData.imLastReturnCode, ogSphData.omLastErrorMessage);
								MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
							}	
							AfxGetApp()->DoWaitCursor(-1);
						}
						delete olSphDlg;
					}
				}
				break;
			case TAB_STANDARDVERKETTUNGEN:
				{
					int ilLineNo = pomTable->GetCurSel();
					if (ilLineNo == -1)
					{
						MessageBox(LoadStg(IDS_STRING148),LoadStg(IDS_STRING149),MB_ICONEXCLAMATION);
						break;
					}
					STRDATA *prlOldStr = ogStrData.GetStrByUrno(pomStandardverkettungenViewer->omLines[ilLineNo].Urno);
					if (prlOldStr != NULL)
					{
						STRDATA rlStr = *prlOldStr;
						rlStr.Urno = 0;
						rlStr.Cdat = TIMENULL;
						rlStr.Lstu = TIMENULL;
						rlStr.Usec[0] = '\0';
						rlStr.Useu[0] = '\0';
						StandardverkettungenDlg *olStrDlg = new StandardverkettungenDlg(&rlStr,this);
						olStrDlg->m_Caption = LoadStg(IDS_STRING152);
						if (olStrDlg->DoModal() == IDOK)
						{
							rlStr.Urno = ogBasicData.GetNextUrno();	//new URNO
							rlStr.Cdat = CTime::GetCurrentTime();
							strcpy(rlStr.Usec,cgUserName);
							STRDATA *prlStr = new STRDATA;
							*prlStr = rlStr;
							AfxGetApp()->DoWaitCursor(1);
							if(ogStrData.Insert(prlStr)==false)
							{
								delete prlStr;
								omErrorTxt.Format("%s %d\n%s",omInsertErrTxt,ogStrData.imLastReturnCode, ogStrData.omLastErrorMessage);
								MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
							}	
							AfxGetApp()->DoWaitCursor(-1);
						}
						delete olStrDlg;
					}
				}
				break;
			case TAB_FLUGPLANSAISON:
				{
					int ilLineNo = pomTable->GetCurSel();
					if (ilLineNo == -1)
					{
						MessageBox(LoadStg(IDS_STRING148),LoadStg(IDS_STRING149),MB_ICONEXCLAMATION);
						break;
					}
					SEADATA *prlOldSea = ogSeaData.GetSeaByUrno(pomFlugplansaisonViewer->omLines[ilLineNo].Urno);
					if (prlOldSea != NULL)
					{
						SEADATA rlSea = *prlOldSea;
						rlSea.Urno = 0;
						rlSea.Cdat = TIMENULL;
						rlSea.Lstu = TIMENULL;
						rlSea.Usec[0] = '\0';
						rlSea.Useu[0] = '\0';
						FlugplansaisonDlg *olSeaDlg = new FlugplansaisonDlg(&rlSea,this);
						olSeaDlg->m_Caption = LoadStg(IDS_STRING152);
						if (olSeaDlg->DoModal() == IDOK)
						{
							rlSea.Urno = ogBasicData.GetNextUrno();	//new URNO
							rlSea.Cdat = CTime::GetCurrentTime();
							strcpy(rlSea.Usec,cgUserName);
							SEADATA *prlSea = new SEADATA;
							*prlSea = rlSea;
							AfxGetApp()->DoWaitCursor(1);
							if(ogSeaData.Insert(prlSea)==false)
							{
								delete prlSea;
								omErrorTxt.Format("%s %d\n%s",omInsertErrTxt,ogSeaData.imLastReturnCode, ogSeaData.omLastErrorMessage);
								MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
							}	
							AfxGetApp()->DoWaitCursor(-1);
						}
						delete olSeaDlg;
					}
				}
				break;
			case TAB_LEISTUNGSKATALOG:
				{
					int ilLineNo = pomTable->GetCurSel();
					if (ilLineNo == -1)
					{
						MessageBox(LoadStg(IDS_STRING148),LoadStg(IDS_STRING149),MB_ICONEXCLAMATION);
						break;
					}
					GHSDATA *prlOldGhs = ogGhsData.GetGhsByUrno(pomLeistungskatalogViewer->omLines[ilLineNo].Urno);
					if (prlOldGhs != NULL)
					{
						GHSDATA rlGhs = *prlOldGhs;
						rlGhs.Urno = 0;
						rlGhs.Cdat = TIMENULL;
						rlGhs.Lstu = TIMENULL;
						rlGhs.Usec[0] = '\0';
						rlGhs.Useu[0] = '\0';
						LeistungskatalogDlg *olGhsDlg = new LeistungskatalogDlg(&rlGhs,this);
						olGhsDlg->m_Caption = LoadStg(IDS_STRING152);
						if (olGhsDlg->DoModal() == IDOK)
						{
							rlGhs.Urno = ogBasicData.GetNextUrno();	//new URNO
							rlGhs.Cdat = CTime::GetCurrentTime();
							strcpy(rlGhs.Usec,cgUserName);
							GHSDATA *prlGhs = new GHSDATA;
							*prlGhs = rlGhs;
							AfxGetApp()->DoWaitCursor(1);
							if(ogGhsData.Insert(prlGhs)==false)
							{
								delete prlGhs;
								omErrorTxt.Format("%s %d\n%s",omInsertErrTxt,ogGhsData.imLastReturnCode, ogGhsData.omLastErrorMessage);
								MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
							}	
							AfxGetApp()->DoWaitCursor(-1);
						}
						delete olGhsDlg;
					}
				}
				break;
			case TAB_QUALIFIKATIONEN:
				{
					int ilLineNo = pomTable->GetCurSel();
					if (ilLineNo == -1)
					{
						MessageBox(LoadStg(IDS_STRING148),LoadStg(IDS_STRING149),MB_ICONEXCLAMATION);
						break;
					}
					PERDATA *prlOldPer = ogPerData.GetPerByUrno(pomQualifikationenViewer->omLines[ilLineNo].Urno);
					if (prlOldPer != NULL)
					{
						PERDATA rlPer = *prlOldPer;
						rlPer.Urno = 0;
						rlPer.Cdat = TIMENULL;
						rlPer.Lstu = TIMENULL;
						rlPer.Usec[0] = '\0';
						rlPer.Useu[0] = '\0';
						QualifikationenDlg *olPerDlg = new QualifikationenDlg(&rlPer,this);
						olPerDlg->m_Caption = LoadStg(IDS_STRING152);
						if (olPerDlg->DoModal() == IDOK)
						{
							rlPer.Urno = ogBasicData.GetNextUrno();	//new URNO
							rlPer.Cdat = CTime::GetCurrentTime();
							strcpy(rlPer.Usec,cgUserName);
							PERDATA *prlPer = new PERDATA;
							*prlPer = rlPer;
							AfxGetApp()->DoWaitCursor(1);
							if(ogPerData.Insert(prlPer)==false)
							{
								delete prlPer;
								omErrorTxt.Format("%s %d\n%s",omInsertErrTxt,ogPerData.imLastReturnCode, ogPerData.omLastErrorMessage);
								MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
							}	
							AfxGetApp()->DoWaitCursor(-1);
						}
						delete olPerDlg;
					}
				}
				break;
			case TAB_ORGANISATIONSEINHEITEN:
				{
					int ilLineNo = pomTable->GetCurSel();
					if (ilLineNo == -1)
					{
						MessageBox(LoadStg(IDS_STRING148),LoadStg(IDS_STRING149),MB_ICONEXCLAMATION);
						break;
					}
					ORGDATA *prlOldOrg = ogOrgData.GetOrgByUrno(pomOrganisationseinheitenViewer->omLines[ilLineNo].Urno);
					if (prlOldOrg != NULL)
					{
						ORGDATA rlOrg = *prlOldOrg;
						rlOrg.Urno = 0;
						rlOrg.Cdat = TIMENULL;
						rlOrg.Lstu = TIMENULL;
						rlOrg.Usec[0] = '\0';
						rlOrg.Useu[0] = '\0';
						OrganisationseinheitenDlg *olOrgDlg = new OrganisationseinheitenDlg(&rlOrg,this);
						olOrgDlg->m_Caption = LoadStg(IDS_STRING152);
						if (olOrgDlg->DoModal() == IDOK)
						{
							rlOrg.Urno = ogBasicData.GetNextUrno();	//new URNO
							rlOrg.Cdat = CTime::GetCurrentTime();
							strcpy(rlOrg.Usec,cgUserName);
							ORGDATA *prlOrg = new ORGDATA;
							*prlOrg = rlOrg;
							AfxGetApp()->DoWaitCursor(1);
							if(ogOrgData.Insert(prlOrg)==false)
							{
								delete prlOrg;
								omErrorTxt.Format("%s %d\n%s",omInsertErrTxt,ogOrgData.imLastReturnCode, ogOrgData.omLastErrorMessage);
								MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
							}	
							AfxGetApp()->DoWaitCursor(-1);
						}
						delete olOrgDlg;
					}
				}
				break;
			case TAB_WEGEZEITEN:				
				{
					int ilLineNo = pomTable->GetCurSel();
					if (ilLineNo == -1)
					{
						MessageBox(LoadStg(IDS_STRING148),LoadStg(IDS_STRING149),MB_ICONEXCLAMATION);
						break;
					}
					WAYDATA *prlOldWay = ogWayData.GetWayByUrno(pomWegezeitenViewer->omLines[ilLineNo].Urno);
					if (prlOldWay != NULL)
					{
						WAYDATA rlWay = *prlOldWay;
						rlWay.Urno = 0;
						rlWay.Cdat = TIMENULL;
						rlWay.Lstu = TIMENULL;
						rlWay.Usec[0] = '\0';
						rlWay.Useu[0] = '\0';
						WegezeitenDlg *olWayDlg = new WegezeitenDlg(&rlWay, this);
						olWayDlg->m_Caption = LoadStg(IDS_STRING152);
						if (olWayDlg->DoModal() == IDOK)
						{
							rlWay.Urno = ogBasicData.GetNextUrno();	//new URNO
							rlWay.Cdat = CTime::GetCurrentTime();
							strcpy(rlWay.Usec,cgUserName);
							WAYDATA *prlWay = new WAYDATA;
							*prlWay = rlWay;
							AfxGetApp()->DoWaitCursor(1);
							if(ogWayData.Insert(prlWay)==false)
							{
								delete prlWay;
								omErrorTxt.Format("%s %d\n%s",omInsertErrTxt,ogWayData.imLastReturnCode, ogWayData.omLastErrorMessage);
								MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
							}	
							AfxGetApp()->DoWaitCursor(-1);
						}
						delete olWayDlg;
					}
				}
				break;
			case TAB_FUNKTIONEN:				
				{
					int ilLineNo = pomTable->GetCurSel();
					if (ilLineNo == -1)
					{
						MessageBox(LoadStg(IDS_STRING148),LoadStg(IDS_STRING149),MB_ICONEXCLAMATION);
						break;
					}
					PFCDATA *prlOldPfc = ogPfcData.GetPfcByUrno(pomFunktionenViewer->omLines[ilLineNo].Urno);
					if (prlOldPfc != NULL)
					{
						PFCDATA rlPfc = *prlOldPfc;
						rlPfc.Urno = 0;
						rlPfc.Cdat = TIMENULL;
						rlPfc.Lstu = TIMENULL;
						rlPfc.Usec[0] = '\0';
						rlPfc.Useu[0] = '\0';
						FunktionenDlg *olPfcDlg = new FunktionenDlg(&rlPfc, this);
						olPfcDlg->m_Caption = LoadStg(IDS_STRING152);
						if (olPfcDlg->DoModal() == IDOK)
						{
							rlPfc.Urno = ogBasicData.GetNextUrno();	//new URNO
							rlPfc.Cdat = CTime::GetCurrentTime();
							strcpy(rlPfc.Usec,cgUserName);
							PFCDATA *prlPfc = new PFCDATA;
							*prlPfc = rlPfc;
							AfxGetApp()->DoWaitCursor(1);
							if(ogPfcData.Insert(prlPfc)==false)
							{
								delete prlPfc;
								omErrorTxt.Format("%s %d\n%s",omInsertErrTxt,ogPfcData.imLastReturnCode, ogPfcData.omLastErrorMessage);
								MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
							}	
							AfxGetApp()->DoWaitCursor(-1);
						}
						delete olPfcDlg;
					}
				}
				break;
			case TAB_ARBEITSVERTRAGSARTEN:	
				{
					int ilLineNo = pomTable->GetCurSel();
					if (ilLineNo == -1)
					{
						MessageBox(LoadStg(IDS_STRING148),LoadStg(IDS_STRING149),MB_ICONEXCLAMATION);
						break;
					}
					COTDATA *prlOldCot = ogCotData.GetCotByUrno(pomArbeitsvertragsartenViewer->omLines[ilLineNo].Urno);
					if (prlOldCot != NULL)
					{
						COTDATA rlCot = *prlOldCot;
						rlCot.Urno = 0;
						rlCot.Cdat = TIMENULL;
						rlCot.Lstu = TIMENULL;
						rlCot.Usec[0] = '\0';
						rlCot.Useu[0] = '\0';
						ArbeitsvertragsartenDlg *olCotDlg = new ArbeitsvertragsartenDlg(&rlCot,this);
						olCotDlg->m_Caption = LoadStg(IDS_STRING152);
						if (olCotDlg->DoModal() == IDOK)
						{
							rlCot.Urno = ogBasicData.GetNextUrno();	//new URNO
							rlCot.Cdat = CTime::GetCurrentTime();
							strcpy(rlCot.Usec,cgUserName);
							COTDATA *prlCot = new COTDATA;
							*prlCot = rlCot;
							AfxGetApp()->DoWaitCursor(1);
							if(ogCotData.Insert(prlCot)==false)
							{
								delete prlCot;
								omErrorTxt.Format("%s %d\n%s",omInsertErrTxt,ogCotData.imLastReturnCode, ogCotData.omLastErrorMessage);
								MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
							}	
							AfxGetApp()->DoWaitCursor(-1);
						}
						delete olCotDlg;
					}
				}
				break;
			case TAB_BEWERTUNGSFAKTOREN:		
				{
					int ilLineNo = pomTable->GetCurSel();
					if (ilLineNo == -1)
					{
						MessageBox(LoadStg(IDS_STRING148),LoadStg(IDS_STRING149),MB_ICONEXCLAMATION);
						break;
					}
					ASFDATA *prlOldAsf = ogAsfData.GetAsfByUrno(pomBewertungsfaktorenViewer->omLines[ilLineNo].Urno);
					if (prlOldAsf != NULL)
					{
						ASFDATA rlAsf = *prlOldAsf;
						rlAsf.Urno = 0;
						rlAsf.Cdat = TIMENULL;
						rlAsf.Lstu = TIMENULL;
						rlAsf.Usec[0] = '\0';
						rlAsf.Useu[0] = '\0';
						BewertungsfaktorenDlg *olAsfDlg = new BewertungsfaktorenDlg(&rlAsf,this);
						olAsfDlg->m_Caption = LoadStg(IDS_STRING152);
						if (olAsfDlg->DoModal() == IDOK)
						{
							rlAsf.Urno = ogBasicData.GetNextUrno();	//new URNO
							rlAsf.Cdat = CTime::GetCurrentTime();
							strcpy(rlAsf.Usec,cgUserName);
							ASFDATA *prlAsf = new ASFDATA;
							*prlAsf = rlAsf;
							AfxGetApp()->DoWaitCursor(1);
							if(ogAsfData.Insert(prlAsf)==false)
							{
								delete prlAsf;
								omErrorTxt.Format("%s %d\n%s",omInsertErrTxt,ogAsfData.imLastReturnCode, ogAsfData.omLastErrorMessage);
								MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
							}	
							AfxGetApp()->DoWaitCursor(-1);
						}
						delete olAsfDlg;
					}
				}
				break;
			case TAB_BASISSCHICHTEN:			
				{
					int ilLineNo = pomTable->GetCurSel();
					if (ilLineNo == -1)
					{
						MessageBox(LoadStg(IDS_STRING148),LoadStg(IDS_STRING149),MB_ICONEXCLAMATION);
						break;
					}
					BSDDATA *prlOldBsd = ogBsdData.GetBsdByUrno(pomBasisschichtenViewer->omLines[ilLineNo].Urno);
					if (prlOldBsd != NULL)
					{
						BSDDATA rlBsd = *prlOldBsd;
						rlBsd.Urno = 0;
						rlBsd.Cdat = TIMENULL;
						rlBsd.Lstu = TIMENULL;
						rlBsd.Usec[0] = '\0';
						rlBsd.Useu[0] = '\0';
						BasisschichtenDlg *olBsdDlg = new BasisschichtenDlg(&rlBsd,true, this);
						olBsdDlg->m_Caption = LoadStg(IDS_STRING152);
						if (olBsdDlg->DoModal() == IDOK)
						{
							rlBsd.Urno = ogBasicData.GetNextUrno();	//new URNO
							rlBsd.Cdat = CTime::GetCurrentTime();
							strcpy(rlBsd.Usec,cgUserName);
							BSDDATA *prlBsd = new BSDDATA;
							*prlBsd = rlBsd;
							AfxGetApp()->DoWaitCursor(1);
							if(ogBsdData.Insert(prlBsd)==false)
							{
								delete prlBsd;
								omErrorTxt.Format("%s %d\n%s",omInsertErrTxt,ogBsdData.imLastReturnCode, ogBsdData.omLastErrorMessage);
								MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
							}	
							AfxGetApp()->DoWaitCursor(-1);
						}
						delete olBsdDlg;
					}
				}
				break;
			case TAB_DIENSTEUNDABWESENHEITEN:
				{
					int ilLineNo = pomTable->GetCurSel();
					if (ilLineNo == -1)
					{
						MessageBox(LoadStg(IDS_STRING148),LoadStg(IDS_STRING149),MB_ICONEXCLAMATION);
						break;
					}
					ODADATA *prlOldOda = ogOdaData.GetOdaByUrno(pomDiensteUndAbwesenheitenViewer->omLines[ilLineNo].Urno);
					if (prlOldOda != NULL)
					{
						ODADATA rlOda = *prlOldOda;
						rlOda.Urno = 0;
						rlOda.Cdat = TIMENULL;
						rlOda.Lstu = TIMENULL;
						rlOda.Usec[0] = '\0';
						rlOda.Useu[0] = '\0';
						DiensteUndAbwesenheitenDlg *olOdaDlg = new DiensteUndAbwesenheitenDlg(&rlOda,this);
						olOdaDlg->m_Caption = LoadStg(IDS_STRING152);
						if (olOdaDlg->DoModal() == IDOK)
						{
							rlOda.Urno = ogBasicData.GetNextUrno();	//new URNO
							rlOda.Cdat = CTime::GetCurrentTime();
							strcpy(rlOda.Usec,cgUserName);
							ODADATA *prlOda = new ODADATA;
							*prlOda = rlOda;
							AfxGetApp()->DoWaitCursor(1);
							if(ogOdaData.Insert(prlOda)==false)
							{
								delete prlOda;
								omErrorTxt.Format("%s %d\n%s",omInsertErrTxt,ogOdaData.imLastReturnCode, ogOdaData.omLastErrorMessage);
								MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
							}	
							AfxGetApp()->DoWaitCursor(-1);
						}
						delete olOdaDlg;
					}
				}
				break;
			case TAB_FAHRGEMEINSCHAFTEN:		
				{
					int ilLineNo = pomTable->GetCurSel();
					if (ilLineNo == -1)
					{
						MessageBox(LoadStg(IDS_STRING148),LoadStg(IDS_STRING149),MB_ICONEXCLAMATION);
						break;
					}
					TEADATA *prlOldTea = ogTeaData.GetTeaByUrno(pomFahrgemeinschaftenViewer->omLines[ilLineNo].Urno);
					if (prlOldTea != NULL)
					{
						TEADATA rlTea = *prlOldTea;
						rlTea.Urno = 0;
						rlTea.Cdat = TIMENULL;
						rlTea.Lstu = TIMENULL;
						rlTea.Usec[0] = '\0';
						rlTea.Useu[0] = '\0';
						FahrgemeinschaftenDlg *olTeaDlg = new FahrgemeinschaftenDlg(&rlTea, this);
						olTeaDlg->m_Caption = LoadStg(IDS_STRING152);
						if (olTeaDlg->DoModal() == IDOK)
						{
							rlTea.Urno = ogBasicData.GetNextUrno();	//new URNO
							rlTea.Cdat = CTime::GetCurrentTime();
							strcpy(rlTea.Usec,cgUserName);
							TEADATA *prlTea = new TEADATA;
							*prlTea = rlTea;
							AfxGetApp()->DoWaitCursor(1);
							if(ogTeaData.Insert(prlTea)==false)
							{
								delete prlTea;
								omErrorTxt.Format("%s %d\n%s",omInsertErrTxt,ogTeaData.imLastReturnCode, ogTeaData.omLastErrorMessage);
								MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
							}	
							AfxGetApp()->DoWaitCursor(-1);
						}
						delete olTeaDlg;
					}
				}
				break;
			case TAB_ARBEITSGRUPPEN:		
				{
					int ilLineNo = pomTable->GetCurSel();
					if (ilLineNo == -1)
					{
						MessageBox(LoadStg(IDS_STRING148),LoadStg(IDS_STRING149),MB_ICONEXCLAMATION);
						break;
					}
					WGPDATA *prlOldWgp = ogWgpData.GetWgpByUrno(pomArbeitsgruppenViewer->omLines[ilLineNo].Urno);
					if (prlOldWgp != NULL)
					{
						WGPDATA rlWgp = *prlOldWgp;
						rlWgp.Urno = 0;
						rlWgp.Cdat = TIMENULL;
						rlWgp.Lstu = TIMENULL;
						rlWgp.Usec[0] = '\0';
						rlWgp.Useu[0] = '\0';
						CArbeitsgruppenDlg *olWgpDlg = new CArbeitsgruppenDlg(&rlWgp, this);
						//olWgpDlg->m_Caption = LoadStg(IDS_STRING152);
						if (olWgpDlg->DoModal() == IDOK)
						{
							rlWgp.Urno = ogBasicData.GetNextUrno();	//new URNO
							rlWgp.Cdat = CTime::GetCurrentTime();
							strcpy(rlWgp.Usec,cgUserName);
							WGPDATA *prlWgp = new WGPDATA;
							*prlWgp = rlWgp;
							AfxGetApp()->DoWaitCursor(1);
							if(ogWgpData.Insert(prlWgp)==false)
							{
								delete prlWgp;
								omErrorTxt.Format("%s %d\n%s",omInsertErrTxt,ogWgpData.imLastReturnCode, ogWgpData.omLastErrorMessage);
								MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
							}	
							AfxGetApp()->DoWaitCursor(-1);
						}
						delete olWgpDlg;
					}
				}
				break;
			case TAB_MITARBEITERSTAMM:		
				{
					int i;
					CCSPtrArray<SORDATA> olSorData;
					CCSPtrArray<SPFDATA> olSpfData;
					CCSPtrArray<SPEDATA> olSpeData;
					CCSPtrArray<SCODATA> olScoData;
					CCSPtrArray<STEDATA> olSteData;
					CCSPtrArray<SWGDATA> olSwgData;
					int ilLineNo = pomTable->GetCurSel();
					if (ilLineNo == -1)
					{
						MessageBox(LoadStg(IDS_STRING148),LoadStg(IDS_STRING149),MB_ICONEXCLAMATION);
						break;
					}
					STFDATA *prlStf = ogStfData.GetStfByUrno(pomMitarbeiterstammViewer->omLines[ilLineNo].Urno);
					if (prlStf != NULL)
					{
						char pclWhere[1024]="";
						sprintf(pclWhere, " WHERE SURN = %ld", prlStf->Urno);
						AfxGetApp()->DoWaitCursor(1);
						ogSorData.Read(pclWhere);
						ogSpfData.Read(pclWhere);
						ogSpeData.Read(pclWhere);
						ogScoData.Read(pclWhere);
						ogSteData.Read(pclWhere);
						ogSwgData.Read(pclWhere);

						for(i = 0; i < ogSorData.omData.GetSize(); i++)
						{
							SORDATA *prlSor = new SORDATA;
							*prlSor = ogSorData.omData[i];
							prlSor->Urno = 0;
							prlSor->Surn = 0;
							olSorData.Add(prlSor);
						}
						for(i = 0; i < ogSpfData.omData.GetSize(); i++)
						{
							SPFDATA *prlSpf = new SPFDATA;
							*prlSpf = ogSpfData.omData[i];
							prlSpf->Urno = 0;
							prlSpf->Surn = 0;
							olSpfData.Add(prlSpf);
						}
						for(i = 0; i < ogSpeData.omData.GetSize(); i++)
						{
							SPEDATA *prlSpe = new SPEDATA;
							*prlSpe = ogSpeData.omData[i];
							prlSpe->Urno = 0;
							prlSpe->Surn = 0;
							olSpeData.Add(prlSpe);
						}
						for(i = 0; i < ogScoData.omData.GetSize(); i++)
						{
							SCODATA *prlSco = new SCODATA;
							*prlSco = ogScoData.omData[i];
							prlSco->Urno = 0;
							prlSco->Surn = 0;
							olScoData.Add(prlSco);
						}
						for(i = 0; i < ogSteData.omData.GetSize(); i++)
						{
							STEDATA *prlSte = new STEDATA;
							*prlSte = ogSteData.omData[i];
							prlSte->Urno = 0;
							prlSte->Surn = 0;
							olSteData.Add(prlSte);
						}
						for(i = 0; i < ogSwgData.omData.GetSize(); i++)
						{
							SWGDATA *prlSwg = new SWGDATA;
							*prlSwg = ogSwgData.omData[i];
							prlSwg->Urno = 0;
							prlSwg->Surn = 0;
							olSwgData.Add(prlSwg);
						}
						AfxGetApp()->DoWaitCursor(-1);
						STFDATA rlStf = *prlStf;
						rlStf.Urno = 0;
						rlStf.Cdat = TIMENULL;
						rlStf.Lstu = TIMENULL;
						rlStf.Usec[0] = '\0';
						rlStf.Useu[0] = '\0';
						MitarbeiterStammDlg *polDlg = new MitarbeiterStammDlg(&rlStf, 
																			  &olSorData,
																			  &olSpfData,
																			  &olSpeData,
																			  &olScoData,
																			  &olSteData,
																			  &olSwgData,
																			  this);
						polDlg->m_Caption = LoadStg(IDS_STRING152);
						if (polDlg->DoModal() == IDOK)
						{
							rlStf.Urno = ogBasicData.GetNextUrno();	//new URNO
							rlStf.Cdat = CTime::GetCurrentTime();
							strcpy(rlStf.Usec,cgUserName);
							STFDATA *prlStf = new STFDATA;
							*prlStf = rlStf;
							AfxGetApp()->DoWaitCursor(1);
							if(ogStfData.Insert(prlStf)==false)
							{
								delete prlStf;
								omErrorTxt.Format("%s %d\n%s",omInsertErrTxt,ogStfData.imLastReturnCode,ogStfData .omLastErrorMessage);
								MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
							}	
							for(i = 0; i < olSorData.GetSize(); i++)
							{
								if(olSorData[i].IsChanged == DATA_NEW)
								{
									SORDATA *prlSor = new SORDATA;
									olSorData[i].Urno = ogBasicData.GetNextUrno();	//new URNO
									olSorData[i].Surn = rlStf.Urno;
									*prlSor = olSorData[i];
									if(ogSorData.Insert(prlSor)==false)
									{
										delete prlSor;
										omErrorTxt.Format("%s %d\n%s",omInsertErrTxt,ogSorData.imLastReturnCode, ogSorData.omLastErrorMessage);
										MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
									}
								}
							}
							for(i = 0; i < olSpfData.GetSize(); i++)
							{
								if(olSpfData[i].IsChanged == DATA_NEW)
								{
									SPFDATA *prlSpf = new SPFDATA;
									olSpfData[i].Urno = ogBasicData.GetNextUrno();	//new URNO
									olSpfData[i].Surn = rlStf.Urno;
									*prlSpf = olSpfData[i];
									if(ogSpfData.Insert(prlSpf)==false)
									{
										delete prlSpf;
										omErrorTxt.Format("%s %d\n%s",omInsertErrTxt,ogSpfData.imLastReturnCode, ogSpfData.omLastErrorMessage);
										MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
									}
								}
							}
							for(i = 0; i < olSpeData.GetSize(); i++)
							{
								if(olSpeData[i].IsChanged == DATA_NEW)
								{
									SPEDATA *prlSpe = new SPEDATA;
									olSpeData[i].Urno = ogBasicData.GetNextUrno();	//new URNO
									olSpeData[i].Surn = rlStf.Urno;
									*prlSpe = olSpeData[i];
									if(ogSpeData.Insert(prlSpe)==false)
									{
										delete prlSpe;
										omErrorTxt.Format("%s %d\n%s",omInsertErrTxt,ogSpeData.imLastReturnCode, ogSpeData.omLastErrorMessage);
										MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
									}
								}
							}
							for(i = 0; i < olScoData.GetSize(); i++)
							{
								if(olScoData[i].IsChanged == DATA_NEW)
								{
									SCODATA *prlSco = new SCODATA;
									olScoData[i].Urno = ogBasicData.GetNextUrno();	//new URNO
									olScoData[i].Surn = rlStf.Urno;
									*prlSco = olScoData[i];
									if(ogScoData.Insert(prlSco)==false)
									{
										delete prlSco;
										omErrorTxt.Format("%s %d\n%s",omInsertErrTxt,ogScoData.imLastReturnCode, ogScoData.omLastErrorMessage);
										MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
									}
								}
							}
							for(i = 0; i < olSteData.GetSize(); i++)
							{
								if(olSteData[i].IsChanged == DATA_NEW)
								{
									STEDATA *prlSte = new STEDATA;
									olSteData[i].Urno = ogBasicData.GetNextUrno();	//new URNO
									olSteData[i].Surn = rlStf.Urno;
									*prlSte = olSteData[i];
									if(ogSteData.Insert(prlSte)==false)
									{
										delete prlSte;
										omErrorTxt.Format("%s %d\n%s",omInsertErrTxt,ogSteData.imLastReturnCode, ogSteData.omLastErrorMessage);
										MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
									}
								}
							}
							for(i = 0; i < olSwgData.GetSize(); i++)
							{
								if(olSwgData[i].IsChanged == DATA_NEW)
								{
									SWGDATA *prlSwg = new SWGDATA;
									olSwgData[i].Urno = ogBasicData.GetNextUrno();	//new URNO
									olSwgData[i].Surn = rlStf.Urno;
									*prlSwg = olSwgData[i];
									if(ogSwgData.Insert(prlSwg)==false)
									{
										delete prlSwg;
										omErrorTxt.Format("%s %d\n%s",omInsertErrTxt,ogSwgData.imLastReturnCode, ogSwgData.omLastErrorMessage);
										MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
									}
								}
							}

							AfxGetApp()->DoWaitCursor(-1);
						}
						delete polDlg;
					}
					olSorData.DeleteAll();
					olSpfData.DeleteAll();
					olSpeData.DeleteAll();
					olScoData.DeleteAll();
					olSteData.DeleteAll();
					olSwgData.DeleteAll();
				}
				break;
			case TAB_ERGABFERTIGUNGSARTEN:		
				{
					int ilLineNo = pomTable->GetCurSel();
					if (ilLineNo == -1)
					{
						MessageBox(LoadStg(IDS_STRING148),LoadStg(IDS_STRING149),MB_ICONEXCLAMATION);
						break;
					}
					SPHDATA *prlOldSph = ogSphData.GetSphByUrno(pomErgAbfertigungsartenViewer->omLines[ilLineNo].Urno);
					if (prlOldSph != NULL)
					{
						SPHDATA rlSph = *prlOldSph;
						rlSph.Urno = 0;
						rlSph.Cdat = TIMENULL;
						rlSph.Lstu = TIMENULL;
						rlSph.Usec[0] = '\0';
						rlSph.Useu[0] = '\0';
						ErgAbfertigungsartenDlg *olSphDlg = new ErgAbfertigungsartenDlg(&rlSph,this);
						olSphDlg->m_Caption = LoadStg(IDS_STRING152);
						if (olSphDlg->DoModal() == IDOK)
						{
							rlSph.Urno = ogBasicData.GetNextUrno();	//new URNO
							rlSph.Cdat = CTime::GetCurrentTime();
							strcpy(rlSph.Eart,"A");
							strcpy(rlSph.Usec,cgUserName);
							SPHDATA *prlSph = new SPHDATA;
							*prlSph = rlSph;
							AfxGetApp()->DoWaitCursor(1);
							if(ogSphData.Insert(prlSph)==false)
							{
								delete prlSph;
								omErrorTxt.Format("%s %d\n%s",omInsertErrTxt,ogSphData.imLastReturnCode, ogSphData.omLastErrorMessage);
								MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
							}	
							AfxGetApp()->DoWaitCursor(-1);
						}
						delete olSphDlg;
					}
				}
				break;
			case TAB_REDUKTIONEN:			
				{
					int ilLineNo = pomTable->GetCurSel();
					if (ilLineNo == -1)
					{
						MessageBox(LoadStg(IDS_STRING148),LoadStg(IDS_STRING149),MB_ICONEXCLAMATION);
						break;
					}
					PRCDATA *prlOldPrc = ogPrcData.GetPrcByUrno(pomReduktionenViewer->omLines[ilLineNo].Urno);
					if (prlOldPrc != NULL)
					{
						PRCDATA rlPrc = *prlOldPrc;
						rlPrc.Urno = 0;
						rlPrc.Cdat = TIMENULL;
						rlPrc.Lstu = TIMENULL;
						rlPrc.Usec[0] = '\0';
						rlPrc.Useu[0] = '\0';
						ReduktionenDlg *olPrcDlg = new ReduktionenDlg(&rlPrc, this);
						olPrcDlg->m_Caption = LoadStg(IDS_STRING152);
						if (olPrcDlg->DoModal() == IDOK)
						{
							rlPrc.Urno = ogBasicData.GetNextUrno();	//new URNO
							rlPrc.Cdat = CTime::GetCurrentTime();
							strcpy(rlPrc.Usec,cgUserName);
							PRCDATA *prlPrc = new PRCDATA;
							*prlPrc = rlPrc;
							AfxGetApp()->DoWaitCursor(1);
							if(ogPrcData.Insert(prlPrc)==false)
							{
								delete prlPrc;
								omErrorTxt.Format("%s %d\n%s",omInsertErrTxt,ogPrcData.imLastReturnCode, ogPrcData.omLastErrorMessage);
								MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
							}	
							AfxGetApp()->DoWaitCursor(-1);
						}
						delete olPrcDlg;
					}
				}
				break;
			case TAB_GERAETEGRUPPEN:			
				{
					int ilLineNo = pomTable->GetCurSel();
					if (ilLineNo == -1)
					{
						MessageBox(LoadStg(IDS_STRING148),LoadStg(IDS_STRING149),MB_ICONEXCLAMATION);
						break;
					}
					GEGDATA *prlOldGeg = ogGegData.GetGegByUrno(pomGeraetegruppenViewer->omLines[ilLineNo].Urno);
					if (prlOldGeg != NULL)
					{
						GEGDATA rlGeg = *prlOldGeg;
						rlGeg.Urno = 0;
						rlGeg.Cdat = TIMENULL;
						rlGeg.Lstu = TIMENULL;
						rlGeg.Usec[0] = '\0';
						rlGeg.Useu[0] = '\0';
						GeraeteGruppenDlg *olGegDlg = new GeraeteGruppenDlg(&rlGeg, this);
						olGegDlg->m_Caption = LoadStg(IDS_STRING152);
						if (olGegDlg->DoModal() == IDOK)
						{
							rlGeg.Urno = ogBasicData.GetNextUrno();	//new URNO
							rlGeg.Cdat = CTime::GetCurrentTime();
							strcpy(rlGeg.Usec,cgUserName);
							GEGDATA *prlGeg = new GEGDATA;
							*prlGeg = rlGeg;
							AfxGetApp()->DoWaitCursor(1);
							if(ogGegData.Insert(prlGeg)==false)
							{
								delete prlGeg;
								omErrorTxt.Format("%s %d\n%s",omInsertErrTxt,ogGegData.imLastReturnCode, ogGegData.omLastErrorMessage);
								MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
							}	
							AfxGetApp()->DoWaitCursor(-1);
						}
						delete olGegDlg;
					}
				}
				break;
			case TAB_ORGANIZER:			
				{
					int ilLineNo = pomTable->GetCurSel();
					if (ilLineNo == -1)
					{
						MessageBox(LoadStg(IDS_STRING148),LoadStg(IDS_STRING149),MB_ICONEXCLAMATION);
						break;
					}
					CHTDATA *prlOldCht = ogChtData.GetChtByUrno(pomOrganizerViewer->omLines[ilLineNo].Urno);
					if (prlOldCht != NULL)
					{
						CHTDATA rlCht = *prlOldCht;
						rlCht.Urno = 0;
						rlCht.Cdat = TIMENULL;
						rlCht.Lstu = TIMENULL;
						rlCht.Usec[0] = '\0';
						rlCht.Useu[0] = '\0';
						OrganizerDlg *olChtDlg = new OrganizerDlg(&rlCht, this);
						olChtDlg->m_Caption = LoadStg(IDS_STRING152);
						if (olChtDlg->DoModal() == IDOK)
						{
							rlCht.Urno = ogBasicData.GetNextUrno();	//new URNO
							rlCht.Cdat = CTime::GetCurrentTime();
							strcpy(rlCht.Usec,cgUserName);
							CHTDATA *prlCht = new CHTDATA;
							*prlCht = rlCht;
							AfxGetApp()->DoWaitCursor(1);
							if(ogChtData.Insert(prlCht)==false)
							{
								delete prlCht;
								omErrorTxt.Format("%s %d\n%s",omInsertErrTxt,ogChtData.imLastReturnCode, ogChtData.omLastErrorMessage);
								MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
							}	
							AfxGetApp()->DoWaitCursor(-1);
						}
						delete olChtDlg;
					}
				}
				break;
			case TAB_TIMEPARAMETERS:			
				{
					int ilLineNo = pomTable->GetCurSel();
					if (ilLineNo == -1)
					{
						MessageBox(LoadStg(IDS_STRING148),LoadStg(IDS_STRING149),MB_ICONEXCLAMATION);
						break;
					}
					TIPDATA *prlOldTip = ogTipData.GetTipByUrno(pomTimeParametersViewer->omLines[ilLineNo].Urno);
					if (prlOldTip != NULL)
					{
						TIPDATA rlTip = *prlOldTip;
						rlTip.Urno = 0;
						rlTip.Cdat = TIMENULL;
						rlTip.Lstu = TIMENULL;
						rlTip.Usec[0] = '\0';
						rlTip.Useu[0] = '\0';
						TimeParametersDlg *olTipDlg = new TimeParametersDlg(&rlTip, this);
						olTipDlg->m_Caption = LoadStg(IDS_STRING152);
						if (olTipDlg->DoModal() == IDOK)
						{
							rlTip.Urno = ogBasicData.GetNextUrno();	//new URNO
							rlTip.Cdat = CTime::GetCurrentTime();
							strcpy(rlTip.Usec,cgUserName);
							TIPDATA *prlTip = new TIPDATA;
							*prlTip = rlTip;
							AfxGetApp()->DoWaitCursor(1);
							if(ogTipData.Insert(prlTip)==false)
							{
								delete prlTip;
								omErrorTxt.Format("%s %d\n%s",omInsertErrTxt,ogTipData.imLastReturnCode, ogTipData.omLastErrorMessage);
								MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
							}	
							AfxGetApp()->DoWaitCursor(-1);
						}
						delete olTipDlg;
					}
				}
				break;
			case TAB_HOLIDAY:			
				{
					int ilLineNo = pomTable->GetCurSel();
					if (ilLineNo == -1)
					{
						MessageBox(LoadStg(IDS_STRING148),LoadStg(IDS_STRING149),MB_ICONEXCLAMATION);
						break;
					}
					HOLDATA *prlOldHol = ogHolData.GetHolByUrno(pomHolidayViewer->omLines[ilLineNo].Urno);
					if (prlOldHol != NULL)
					{
						HOLDATA rlHol = *prlOldHol;
						rlHol.Urno = 0;
						rlHol.Cdat = TIMENULL;
						rlHol.Lstu = TIMENULL;
						rlHol.Usec[0] = '\0';
						rlHol.Useu[0] = '\0';
						HolidayDlg *olHolDlg = new HolidayDlg(&rlHol, this);
						olHolDlg->m_Caption = LoadStg(IDS_STRING152);
						if (olHolDlg->DoModal() == IDOK)
						{
							rlHol.Urno = ogBasicData.GetNextUrno();	//new URNO
							rlHol.Cdat = CTime::GetCurrentTime();
							strcpy(rlHol.Usec,cgUserName);
							HOLDATA *prlHol = new HOLDATA;
							*prlHol = rlHol;
							AfxGetApp()->DoWaitCursor(1);
							if(ogHolData.Insert(prlHol)==false)
							{
								delete prlHol;
								omErrorTxt.Format("%s %d\n%s",omInsertErrTxt,ogHolData.imLastReturnCode, ogHolData.omLastErrorMessage);
								MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
							}	
							AfxGetApp()->DoWaitCursor(-1);
						}
						delete olHolDlg;
					}
				}
				break;
			case TAB_PLANNING_GROUPS:			
				{
					int ilLineNo = pomTable->GetCurSel();
					if (ilLineNo == -1)
					{
						MessageBox(LoadStg(IDS_STRING148),LoadStg(IDS_STRING149),MB_ICONEXCLAMATION);
						break;
					}
					PGPDATA *prlOldPgp = ogPgpData.GetPgpByUrno(pomPlanungsgruppenViewer->omLines[ilLineNo].Urno);
					if (prlOldPgp != NULL)
					{
						PGPDATA rlPgp = *prlOldPgp;
						rlPgp.Urno = 0;
						rlPgp.Cdat = TIMENULL;
						rlPgp.Lstu = TIMENULL;
						rlPgp.Usec[0] = '\0';
						rlPgp.Useu[0] = '\0';
						PlanungsgruppenDlg *polPgpDlg = new PlanungsgruppenDlg(&rlPgp, this);
						polPgpDlg->m_Caption = LoadStg(IDS_STRING152);
						if (polPgpDlg->DoModal() == IDOK)
						{
							rlPgp.Urno = ogBasicData.GetNextUrno();	//new URNO
							rlPgp.Cdat = CTime::GetCurrentTime();
							strcpy(rlPgp.Usec,cgUserName);
							PGPDATA *prlPgp = new PGPDATA;
							*prlPgp = rlPgp;
							AfxGetApp()->DoWaitCursor(1);
							if(ogPgpData.Insert(prlPgp)==false)
							{
								delete prlPgp;
								omErrorTxt.Format("%s %d\n%s",omInsertErrTxt,ogPgpData.imLastReturnCode, ogPgpData.omLastErrorMessage);
								MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
							}	
							AfxGetApp()->DoWaitCursor(-1);
						}
						delete polPgpDlg;
					}
				}
				break;
			case TAB_AIRPORTWET:			
				{
					int ilLineNo = pomTable->GetCurSel();
					if (ilLineNo == -1)
					{
						MessageBox(LoadStg(IDS_STRING148),LoadStg(IDS_STRING149),MB_ICONEXCLAMATION);
						break;
					}
					AWIDATA *prlOldAwi = ogAwiData.GetAwiByUrno(pomAirportwetViewer->omLines[ilLineNo].Urno);
					if (prlOldAwi != NULL)
					{
						AWIDATA rlAwi = *prlOldAwi;
						rlAwi.Urno = 0;
//						rlAwi.Cdat = TIMENULL;
//						rlAwi.Lstu = TIMENULL;
//						rlAwi.Usec[0] = '\0';
//						rlAwi.Useu[0] = '\0';
						CAirPortWet *polAwiDlg = new CAirPortWet(&rlAwi, this);
						polAwiDlg->m_Caption = LoadStg(IDS_STRING152);
						if (polAwiDlg->DoModal() == IDOK)
						{
							rlAwi.Urno = ogBasicData.GetNextUrno();	//new URNO
//							rlAwi.Cdat = CTime::GetCurrentTime();
//							strcpy(rlAwi.Usec,cgUserName);
							AWIDATA *prlAwi = new AWIDATA;
							*prlAwi = rlAwi;
							AfxGetApp()->DoWaitCursor(1);
							if(ogAwiData.Insert(prlAwi)==false)
							{
								delete prlAwi;
								omErrorTxt.Format("%s %d\n%s",omInsertErrTxt,ogAwiData.imLastReturnCode, ogAwiData.omLastErrorMessage);
								MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
							}	
							AfxGetApp()->DoWaitCursor(-1);
						}
						delete polAwiDlg;
					}
				}
				break;
			case TAB_COUNTERCLASS:			
				{
					int ilLineNo = pomTable->GetCurSel();
					if (ilLineNo == -1)
					{
						MessageBox(LoadStg(IDS_STRING148),LoadStg(IDS_STRING149),MB_ICONEXCLAMATION);
						break;
					}
					CCCDATA *prlOldCcc = ogCccData.GetCccByUrno(pomCounterclassViewer->omLines[ilLineNo].Urno);
					if (prlOldCcc != NULL)
					{
						CCCDATA rlCcc = *prlOldCcc;
						rlCcc.Urno = 0;
						rlCcc.Cdat = TIMENULL;
						rlCcc.Lstu = TIMENULL;
						rlCcc.Usec[0] = '\0';
						rlCcc.Useu[0] = '\0';
						CChkInCounterClass *polCccDlg = new CChkInCounterClass(&rlCcc, this);
						polCccDlg->m_Caption = LoadStg(IDS_STRING152);
						if (polCccDlg->DoModal() == IDOK)
						{
							rlCcc.Urno = ogBasicData.GetNextUrno();	//new URNO
							rlCcc.Cdat = CTime::GetCurrentTime();
							strcpy(rlCcc.Usec,cgUserName);
							CCCDATA *prlCcc = new CCCDATA;
							*prlCcc = rlCcc;
							AfxGetApp()->DoWaitCursor(1);
							if(ogCccData.Insert(prlCcc)==false)
							{
								delete prlCcc;
								omErrorTxt.Format("%s %d\n%s",omInsertErrTxt,ogCccData.imLastReturnCode, ogCccData.omLastErrorMessage);
								MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
							}	
							AfxGetApp()->DoWaitCursor(-1);
						}
						delete polCccDlg;
					}
				}
				break;
			case TAB_VERYIMPPERS:			
				{
					int ilLineNo = pomTable->GetCurSel();
					if (ilLineNo == -1)
					{
						MessageBox(LoadStg(IDS_STRING148),LoadStg(IDS_STRING149),MB_ICONEXCLAMATION);
						break;
					}
					VIPDATA *prlOldVip = ogVipData.GetVipByUrno(pomVeryimppersViewer->omLines[ilLineNo].Urno);
					if (prlOldVip != NULL)
					{
						VIPDATA rlVip = *prlOldVip;
						rlVip.Urno = 0;
						rlVip.Cdat = TIMENULL;
						rlVip.Lstu = TIMENULL;
						rlVip.Usec[0] = '\0';
						rlVip.Useu[0] = '\0';
						CVeryImpPers *polVipDlg = new CVeryImpPers(&rlVip, this);
						polVipDlg->m_Caption = LoadStg(IDS_STRING152);
						if (polVipDlg->DoModal() == IDOK)
						{
							rlVip.Urno = ogBasicData.GetNextUrno();	//new URNO
							rlVip.Cdat = CTime::GetCurrentTime();
							strcpy(rlVip.Usec,cgUserName);
							VIPDATA *prlVip = new VIPDATA;
							*prlVip = rlVip;
							AfxGetApp()->DoWaitCursor(1);
							if(ogVipData.Insert(prlVip)==false)
							{
								delete prlVip;
								omErrorTxt.Format("%s %d\n%s",omInsertErrTxt,ogVipData.imLastReturnCode, ogVipData.omLastErrorMessage);
								MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
							}	
							AfxGetApp()->DoWaitCursor(-1);
						}
						delete polVipDlg;
					}
				}
				break;
			case TAB_AIRCRAFTFAM:		
				{
					int ilLineNo = pomTable->GetCurSel();
					if (ilLineNo == -1)
					{
						MessageBox(LoadStg(IDS_STRING148),LoadStg(IDS_STRING149),MB_ICONEXCLAMATION);
						break;
					}
					AFMDATA *prlOldAfm = ogAFMData.GetAfmByUrno(pomAircraftFamViewer->omLines[ilLineNo].Urno);
					if (prlOldAfm != NULL)
					{
						AFMDATA rlAfm = *prlOldAfm;
						rlAfm.Urno = 0;
						rlAfm.Cdat = TIMENULL;
						rlAfm.Lstu = TIMENULL;
						rlAfm.Usec[0] = '\0';
						rlAfm.Useu[0] = '\0';
						AircraftFamDlg *olAfmDlg = new AircraftFamDlg(&rlAfm, this);
						olAfmDlg->m_Caption = LoadStg(IDS_STRING152);
						if (olAfmDlg->DoModal() == IDOK)
						{
							rlAfm.Urno = ogBasicData.GetNextUrno();	//new URNO
							rlAfm.Cdat = CTime::GetCurrentTime();
							strcpy(rlAfm.Usec,cgUserName);
							AFMDATA *prlAfm = new AFMDATA;
							*prlAfm = rlAfm;
							AfxGetApp()->DoWaitCursor(1);
							if(ogAFMData.Insert(prlAfm)==false)
							{
								delete prlAfm;
								omErrorTxt.Format("%s %d\n%s",omInsertErrTxt,ogAFMData.imLastReturnCode, ogAFMData.omLastErrorMessage);
								MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
							}	
							AfxGetApp()->DoWaitCursor(-1);
						}
						delete olAfmDlg;
					}
				}
				break;
			case TAB_ENGINETYPE:		
				{
					int ilLineNo = pomTable->GetCurSel();
					if (ilLineNo == -1)
					{
						MessageBox(LoadStg(IDS_STRING148),LoadStg(IDS_STRING149),MB_ICONEXCLAMATION);
						break;
					}
					ENTDATA *prlOldEnt = ogENTData.GetEntByUrno(pomEngineTypeViewer->omLines[ilLineNo].Urno);
					if (prlOldEnt != NULL)
					{
						ENTDATA rlEnt = *prlOldEnt;
						rlEnt.Urno = 0;
						rlEnt.Cdat = TIMENULL;
						rlEnt.Lstu = TIMENULL;
						rlEnt.Usec[0] = '\0';
						rlEnt.Useu[0] = '\0';
						EngineTypeDlg *olEntDlg = new EngineTypeDlg(&rlEnt, this);
						olEntDlg->m_Caption = LoadStg(IDS_STRING152);
						if (olEntDlg->DoModal() == IDOK)
						{
							rlEnt.Urno = ogBasicData.GetNextUrno();	//new URNO
							rlEnt.Cdat = CTime::GetCurrentTime();
							strcpy(rlEnt.Usec,cgUserName);
							ENTDATA *prlEnt = new ENTDATA;
							*prlEnt = rlEnt;
							AfxGetApp()->DoWaitCursor(1);
							if(ogENTData.Insert(prlEnt)==false)
							{
								delete prlEnt;
								omErrorTxt.Format("%s %d\n%s",omInsertErrTxt,ogENTData.imLastReturnCode, ogENTData.omLastErrorMessage);
								MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
							}	
							AfxGetApp()->DoWaitCursor(-1);
						}
						delete olEntDlg;
					}
				}
				break;
			default:
				{
					// Do nothing
				}
				break; 
		}
		bgIsDialogOpen = false;
	}		
}

//------------------------------------------------------------------------------------------------------

void CStammdaten::OnLoeschen() 
{
	if(!bgIsDialogOpen)
	{
		bgIsDialogOpen = true;
		switch(igSelectTab)
		{
			case TAB_AIRLINE:
				{
					int ilLineNo = pomTable->GetCurSel();
					if (ilLineNo == -1)
					{
						MessageBox(LoadStg(IDS_STRING148),LoadStg(IDS_STRING149),MB_ICONEXCLAMATION);
						break;
					}
					ALTDATA *prlALT = ogALTData.GetALTByUrno(pomAirlineViewer->omLines[ilLineNo].Urno);
					if (prlALT != NULL)
					{
						ALTDATA rlALT = *prlALT;
						if (IDYES == MessageBox(LoadStg(IDS_STRING153),LoadStg(IDS_STRING154),(MB_ICONQUESTION | MB_YESNO | MB_DEFBUTTON2)))
						{
							AfxGetApp()->DoWaitCursor(1);
							if(ogALTData.DeleteALT(rlALT.Urno)==false)
							{
								omErrorTxt.Format("%s %d\n%s",omDeleteErrTxt,ogALTData.imLastReturnCode, ogALTData.omLastErrorMessage);
								MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
							}	
							AfxGetApp()->DoWaitCursor(-1);
						}
					}
				}
				break;
			case TAB_AIRCRAFT:
				{
					int ilLineNo = pomTable->GetCurSel();
					if (ilLineNo == -1)
					{
						MessageBox(LoadStg(IDS_STRING148),LoadStg(IDS_STRING149),MB_ICONEXCLAMATION);
						break;
					}
					ACTDATA *prlACT = ogACTData.GetACTByUrno(pomAircraftViewer->omLines[ilLineNo].Urno);
					if (prlACT != NULL)
					{
						ACTDATA rlACT = *prlACT;
						if (IDYES == MessageBox(LoadStg(IDS_STRING153),LoadStg(IDS_STRING154),(MB_ICONQUESTION | MB_YESNO | MB_DEFBUTTON2)))
						{
							AfxGetApp()->DoWaitCursor(1);
							if(ogACTData.DeleteACT(rlACT.Urno)==false)
							{
								omErrorTxt.Format("%s %d\n%s",omDeleteErrTxt,ogACTData.imLastReturnCode, ogACTData.omLastErrorMessage);
								MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
							}	
							AfxGetApp()->DoWaitCursor(-1);
						}
					}
				}
				break;
			case TAB_LFZREGISTRATION:
				{
					int ilLineNo = pomTable->GetCurSel();
					if (ilLineNo == -1)
					{
						MessageBox(LoadStg(IDS_STRING148),LoadStg(IDS_STRING149),MB_ICONEXCLAMATION);
						break;
					}
					ACRDATA *prlACR = ogACRData.GetACRByUrno(pomLFZRegiViewer->omLines[ilLineNo].Urno);
					if (prlACR != NULL)
					{
						ACRDATA rlACR = *prlACR;
						if (IDYES == MessageBox(LoadStg(IDS_STRING153),LoadStg(IDS_STRING154),(MB_ICONQUESTION | MB_YESNO | MB_DEFBUTTON2)))
						{
							AfxGetApp()->DoWaitCursor(1);
							if(ogACRData.DeleteACR(rlACR.Urno)==false)
							{
								omErrorTxt.Format("%s %d\n%s",omDeleteErrTxt,ogACRData.imLastReturnCode, ogACRData.omLastErrorMessage);
								MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
							}	
							AfxGetApp()->DoWaitCursor(-1);
						}
					}
				}
				break;
			case TAB_AIRPORT:
				{
					int ilLineNo = pomTable->GetCurSel();
					if (ilLineNo == -1)
					{
						MessageBox(LoadStg(IDS_STRING148),LoadStg(IDS_STRING149),MB_ICONEXCLAMATION);
						break;
					}
					APTDATA *prlAPT = ogAPTData.GetAPTByUrno(pomAirportViewer->omLines[ilLineNo].Urno);
					if (prlAPT != NULL)
					{
						APTDATA rlAPT = *prlAPT;
						if (IDYES == MessageBox(LoadStg(IDS_STRING153),LoadStg(IDS_STRING154),(MB_ICONQUESTION | MB_YESNO | MB_DEFBUTTON2)))
						{
							AfxGetApp()->DoWaitCursor(1);
							if(ogAPTData.DeleteAPT(rlAPT.Urno)==false)
							{
								omErrorTxt.Format("%s %d\n%s",omDeleteErrTxt,ogAPTData.imLastReturnCode, ogAPTData.omLastErrorMessage);
								MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
							}	
							AfxGetApp()->DoWaitCursor(-1);
						}
					}
				}
				break;
			case TAB_RUNWAY:
				{
					int ilLineNo = pomTable->GetCurSel();
					if (ilLineNo == -1)
					{
						MessageBox(LoadStg(IDS_STRING148),LoadStg(IDS_STRING149),MB_ICONEXCLAMATION);
						break;
					}
					RWYDATA *prlRWY = ogRWYData.GetRWYByUrno(pomRunwayViewer->omLines[ilLineNo].Urno);
					if (prlRWY != NULL)
					{
						RWYDATA rlRWY = *prlRWY;
						if (IDYES == MessageBox(LoadStg(IDS_STRING153),LoadStg(IDS_STRING154),(MB_ICONQUESTION | MB_YESNO | MB_DEFBUTTON2)))
						{
							AfxGetApp()->DoWaitCursor(1);
							if(ogRWYData.DeleteRWY(rlRWY.Urno)==false)
							{
								omErrorTxt.Format("%s %d\n%s",omDeleteErrTxt,ogRWYData.imLastReturnCode, ogRWYData.omLastErrorMessage);
								MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
							}	
							AfxGetApp()->DoWaitCursor(-1);
						}
					} 
				}
				break;
			case TAB_TAXIWAY:
				{
					int ilLineNo = pomTable->GetCurSel();
					if (ilLineNo == -1)
					{
						MessageBox(LoadStg(IDS_STRING148),LoadStg(IDS_STRING149),MB_ICONEXCLAMATION);
						break;
					}
					TWYDATA *prlTWY = ogTWYData.GetTWYByUrno(pomTaxiwayViewer->omLines[ilLineNo].Urno);
					if (prlTWY != NULL)
					{
						TWYDATA rlTWY = *prlTWY;
						if (IDYES == MessageBox(LoadStg(IDS_STRING153),LoadStg(IDS_STRING154),(MB_ICONQUESTION | MB_YESNO | MB_DEFBUTTON2)))
						{
							AfxGetApp()->DoWaitCursor(1);
							if(ogTWYData.DeleteTWY(rlTWY.Urno)==false)
							{
								omErrorTxt.Format("%s %d\n%s",omDeleteErrTxt,ogTWYData.imLastReturnCode, ogTWYData.omLastErrorMessage);
								MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
							}	
							AfxGetApp()->DoWaitCursor(-1);
						}
					} 
				}
				break;
			case TAB_POSITION:
				{
					int ilLineNo = pomTable->GetCurSel();
					if (ilLineNo == -1)
					{
						MessageBox(LoadStg(IDS_STRING148),LoadStg(IDS_STRING149),MB_ICONEXCLAMATION);
						break;
					}
					PSTDATA *prlPST = ogPSTData.GetPSTByUrno(pomPositionViewer->omLines[ilLineNo].Urno);
					if (prlPST != NULL)
					{
						PSTDATA rlPST = *prlPST;
						if (IDYES == MessageBox(LoadStg(IDS_STRING153),LoadStg(IDS_STRING154),(MB_ICONQUESTION | MB_YESNO | MB_DEFBUTTON2)))
						{
							AfxGetApp()->DoWaitCursor(1);
							if(ogPSTData.DeletePST(rlPST.Urno)==false)
							{
								omErrorTxt.Format("%s %d\n%s",omDeleteErrTxt,ogPSTData.imLastReturnCode, ogPSTData.omLastErrorMessage);
								MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
							}	
							AfxGetApp()->DoWaitCursor(-1);
						}
					} 
				}
				break;
			case TAB_GATE:
				{
					int ilLineNo = pomTable->GetCurSel();
					if (ilLineNo == -1)
					{
						MessageBox(LoadStg(IDS_STRING148),LoadStg(IDS_STRING149),MB_ICONEXCLAMATION);
						break;
					}
					GATDATA *prlGAT = ogGATData.GetGATByUrno(pomGateViewer->omLines[ilLineNo].Urno);
					if (prlGAT != NULL)
					{
						GATDATA rlGAT = *prlGAT;
						if (IDYES == MessageBox(LoadStg(IDS_STRING153),LoadStg(IDS_STRING154),(MB_ICONQUESTION | MB_YESNO | MB_DEFBUTTON2)))
						{
							AfxGetApp()->DoWaitCursor(1);
							if(ogGATData.DeleteGAT(rlGAT.Urno)==false)
							{
								omErrorTxt.Format("%s %d\n%s",omDeleteErrTxt,ogGATData.imLastReturnCode, ogGATData.omLastErrorMessage);
								MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
							}	
							AfxGetApp()->DoWaitCursor(-1);
						}
					} 
				}
				break;
			case TAB_CHECKINCOUNTER:
				{
					int ilLineNo = pomTable->GetCurSel();
					if (ilLineNo == -1)
					{
						MessageBox(LoadStg(IDS_STRING148),LoadStg(IDS_STRING149),MB_ICONEXCLAMATION);
						break;
					}
					CICDATA *prlCIC = ogCICData.GetCICByUrno(pomCheckinCounterViewer->omLines[ilLineNo].Urno);
					if (prlCIC != NULL)
					{
						CICDATA rlCIC = *prlCIC;
						if (IDYES == MessageBox(LoadStg(IDS_STRING153),LoadStg(IDS_STRING154),(MB_ICONQUESTION | MB_YESNO | MB_DEFBUTTON2)))
						{
							AfxGetApp()->DoWaitCursor(1);

							BLKDATA rlBlk;
							rlBlk.IsChanged = DATA_DELETED;
							rlBlk.Burn = rlCIC.Urno;
							strcpy(rlBlk.Tabn, "CIC");

							bool blErr = ogBlkData.Save(&rlBlk);
							ogBlkData.omLastErrorMessage.MakeLower();
							if(blErr == false  && ogBlkData.omLastErrorMessage.Find("no data found")==-1)
							{
								omErrorTxt.Format("%s %d\n%s",omDeleteErrTxt,ogBlkData.imLastReturnCode, ogBlkData.omLastErrorMessage);
								MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
							}
							else
							{
								if(ogCICData.DeleteCIC(rlCIC.Urno)==false)
								{
									omErrorTxt.Format("%s %d\n%s",omDeleteErrTxt,ogCICData.imLastReturnCode, ogCICData.omLastErrorMessage);
									MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
								}	
							}	

							AfxGetApp()->DoWaitCursor(-1);
						}
					} 
				}
				break;
			case TAB_BEGGAGEBELT:
				{
					int ilLineNo = pomTable->GetCurSel();
					if (ilLineNo == -1)
					{
						MessageBox(LoadStg(IDS_STRING148),LoadStg(IDS_STRING149),MB_ICONEXCLAMATION);
						break;
					}
					BLTDATA *prlBLT = ogBLTData.GetBLTByUrno(pomBeggagebeltViewer->omLines[ilLineNo].Urno);
					if (prlBLT != NULL)
					{
						BLTDATA rlBLT = *prlBLT;
						if (IDYES == MessageBox(LoadStg(IDS_STRING153),LoadStg(IDS_STRING154),(MB_ICONQUESTION | MB_YESNO | MB_DEFBUTTON2)))
						{
							AfxGetApp()->DoWaitCursor(1);
							BLKDATA rlBlk;
							rlBlk.IsChanged = DATA_DELETED;
							rlBlk.Burn = rlBLT.Urno;
							strcpy(rlBlk.Tabn, "BLT");

							bool blErr = ogBlkData.Save(&rlBlk);
							ogBlkData.omLastErrorMessage.MakeLower();
							if(blErr == false  && ogBlkData.omLastErrorMessage.Find("no data found")==-1)
							{
								omErrorTxt.Format("%s %d\n%s",omDeleteErrTxt,ogBlkData.imLastReturnCode, ogBlkData.omLastErrorMessage);
								MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
							}
							else
							{
								if(ogBLTData.DeleteBLT(rlBLT.Urno)==false)
								{
									omErrorTxt.Format("%s %d\n%s",omDeleteErrTxt,ogBLTData.imLastReturnCode, ogBLTData.omLastErrorMessage);
									MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
								}	
							}	
							
							AfxGetApp()->DoWaitCursor(-1);
						}
					} 
				}
				break;
			case TAB_EXIT:
				{
					int ilLineNo = pomTable->GetCurSel();
					if (ilLineNo == -1)
					{
						MessageBox(LoadStg(IDS_STRING148),LoadStg(IDS_STRING149),MB_ICONEXCLAMATION);
						break;
					}
					EXTDATA *prlEXT = ogEXTData.GetEXTByUrno(pomExitViewer->omLines[ilLineNo].Urno);
					if (prlEXT != NULL)
					{
						EXTDATA rlEXT = *prlEXT;
						if (IDYES == MessageBox(LoadStg(IDS_STRING153),LoadStg(IDS_STRING154),(MB_ICONQUESTION | MB_YESNO | MB_DEFBUTTON2)))
						{
							AfxGetApp()->DoWaitCursor(1);
							if(ogEXTData.DeleteEXT(rlEXT.Urno)==false)
							{
								omErrorTxt.Format("%s %d\n%s",omDeleteErrTxt,ogEXTData.imLastReturnCode, ogEXTData.omLastErrorMessage);
								MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
							}	
							AfxGetApp()->DoWaitCursor(-1);
						}
					} 
				}
				break;
			case TAB_DELAYCODE:
				{
					int ilLineNo = pomTable->GetCurSel();
					if (ilLineNo == -1)
					{
						MessageBox(LoadStg(IDS_STRING148),LoadStg(IDS_STRING149),MB_ICONEXCLAMATION);
						break;
					}
					DENDATA *prlDEN = ogDENData.GetDENByUrno(pomDelaycodeViewer->omLines[ilLineNo].Urno);
					if (prlDEN != NULL)
					{
						DENDATA rlDEN = *prlDEN;
						if (IDYES == MessageBox(LoadStg(IDS_STRING153),LoadStg(IDS_STRING154),(MB_ICONQUESTION | MB_YESNO | MB_DEFBUTTON2)))
						{
							AfxGetApp()->DoWaitCursor(1);
							if(ogDENData.DeleteDEN(rlDEN.Urno)==false)
							{
								omErrorTxt.Format("%s %d\n%s",omDeleteErrTxt,ogDENData.imLastReturnCode, ogDENData.omLastErrorMessage);
								MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
							}	
							AfxGetApp()->DoWaitCursor(-1);
						}
					} 
				}
				break;
			case TAB_TELEXADRESS:
				{
					int ilLineNo = pomTable->GetCurSel();
					if (ilLineNo == -1)
					{
						MessageBox(LoadStg(IDS_STRING148),LoadStg(IDS_STRING149),MB_ICONEXCLAMATION);
						break;
					}
					MVTDATA *prlMVT = ogMVTData.GetMVTByUrno(pomTelexadressViewer->omLines[ilLineNo].Urno);
					if (prlMVT != NULL)
					{
						MVTDATA rlMVT = *prlMVT;
						if (IDYES == MessageBox(LoadStg(IDS_STRING153),LoadStg(IDS_STRING154),(MB_ICONQUESTION | MB_YESNO | MB_DEFBUTTON2)))
						{
							AfxGetApp()->DoWaitCursor(1);
							if(ogMVTData.DeleteMVT(rlMVT.Urno)==false)
							{
								omErrorTxt.Format("%s %d\n%s",omDeleteErrTxt,ogMVTData.imLastReturnCode, ogMVTData.omLastErrorMessage);
								MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
							}	
							AfxGetApp()->DoWaitCursor(-1);
						}
					} 
				}
				break;
			case TAB_TRAFFICTYPE:
				{
					int ilLineNo = pomTable->GetCurSel();
					if (ilLineNo == -1)
					{
						MessageBox(LoadStg(IDS_STRING148),LoadStg(IDS_STRING149),MB_ICONEXCLAMATION);
						break;
					}
					NATDATA *prlNAT = ogNATData.GetNATByUrno(pomTraffictypeViewer->omLines[ilLineNo].Urno);
					if (prlNAT != NULL)
					{
						NATDATA rlNAT = *prlNAT;
						if (IDYES == MessageBox(LoadStg(IDS_STRING153),LoadStg(IDS_STRING154),(MB_ICONQUESTION | MB_YESNO | MB_DEFBUTTON2)))
						{
							AfxGetApp()->DoWaitCursor(1);
							if(ogNATData.DeleteNAT(rlNAT.Urno)==false)
							{
								omErrorTxt.Format("%s %d\n%s",omDeleteErrTxt,ogNATData.imLastReturnCode, ogNATData.omLastErrorMessage);
								MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
							}	
							AfxGetApp()->DoWaitCursor(-1);
						}
					} 
				}
				break;
			case TAB_HANDLINGAGENT:
				{
					int ilLineNo = pomTable->GetCurSel();
					if (ilLineNo == -1)
					{
						MessageBox(LoadStg(IDS_STRING148),LoadStg(IDS_STRING149),MB_ICONEXCLAMATION);
						break;
					}
					HAGDATA *prlHAG = ogHAGData.GetHAGByUrno(pomHandlingagentViewer->omLines[ilLineNo].Urno);
					if (prlHAG != NULL)
					{
						HAGDATA rlHAG = *prlHAG;
						if (IDYES == MessageBox(LoadStg(IDS_STRING153),LoadStg(IDS_STRING154),(MB_ICONQUESTION | MB_YESNO | MB_DEFBUTTON2)))
						{
							AfxGetApp()->DoWaitCursor(1);
							if(ogHAGData.DeleteHAG(rlHAG.Urno)==false)
							{
								omErrorTxt.Format("%s %d\n%s",omDeleteErrTxt,ogHAGData.imLastReturnCode, ogHAGData.omLastErrorMessage);
								MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
							}	
							AfxGetApp()->DoWaitCursor(-1);
						}
					}
				}
				break;
			case TAB_WAITINGROOM:
				{
					int ilLineNo = pomTable->GetCurSel();
					if (ilLineNo == -1)
					{
						MessageBox(LoadStg(IDS_STRING148),LoadStg(IDS_STRING149),MB_ICONEXCLAMATION);
						break;
					}
					WRODATA *prlWRO = ogWROData.GetWROByUrno(pomWaitingroomViewer->omLines[ilLineNo].Urno);
					if (prlWRO != NULL)
					{
						WRODATA rlWRO = *prlWRO;
						if (IDYES == MessageBox(LoadStg(IDS_STRING153),LoadStg(IDS_STRING154),(MB_ICONQUESTION | MB_YESNO | MB_DEFBUTTON2)))
						{
							AfxGetApp()->DoWaitCursor(1);

							BLKDATA rlBlk;
							rlBlk.IsChanged = DATA_DELETED;
							rlBlk.Burn = rlWRO.Urno;
							strcpy(rlBlk.Tabn, "WRO");

							bool blErr = ogBlkData.Save(&rlBlk);
							ogBlkData.omLastErrorMessage.MakeLower();
							if(blErr == false  && ogBlkData.omLastErrorMessage.Find("no data found")==-1)
							{
								omErrorTxt.Format("%s %d\n%s",omDeleteErrTxt,ogBlkData.imLastReturnCode, ogBlkData.omLastErrorMessage);
								MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
							}
							else
							{
								if(ogWROData.DeleteWRO(rlWRO.Urno)==false)
								{
									omErrorTxt.Format("%s %d\n%s",omDeleteErrTxt,ogWROData.imLastReturnCode, ogWROData.omLastErrorMessage);
									MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
								}	
							}	

							AfxGetApp()->DoWaitCursor(-1);
						}
					} 
				}
				break;
			case TAB_HANDLINGTYPE:
				{
					int ilLineNo = pomTable->GetCurSel();
					if (ilLineNo == -1)
					{
						MessageBox(LoadStg(IDS_STRING148),LoadStg(IDS_STRING149),MB_ICONEXCLAMATION);
						break;
					}
					HTYDATA *prlHTY = ogHTYData.GetHTYByUrno(pomHandlingtypeViewer->omLines[ilLineNo].Urno);
					if (prlHTY != NULL)
					{
						HTYDATA rlHTY = *prlHTY;
						if (IDYES == MessageBox(LoadStg(IDS_STRING153),LoadStg(IDS_STRING154),(MB_ICONQUESTION | MB_YESNO | MB_DEFBUTTON2)))
						{
							AfxGetApp()->DoWaitCursor(1);
							if(ogHTYData.DeleteHTY(rlHTY.Urno)==false)
							{
								omErrorTxt.Format("%s %d\n%s",omDeleteErrTxt,ogHTYData.imLastReturnCode, ogHTYData.omLastErrorMessage);
								MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
							}	
							AfxGetApp()->DoWaitCursor(-1);
						}
					} 
				}
				break;
			case TAB_SERVICETYPE:
				{
					int ilLineNo = pomTable->GetCurSel();
					if (ilLineNo == -1)
					{
						MessageBox(LoadStg(IDS_STRING148),LoadStg(IDS_STRING149),MB_ICONEXCLAMATION);
						break;
					}
					STYDATA *prlSTY = ogSTYData.GetSTYByUrno(pomServicetypeViewer->omLines[ilLineNo].Urno);
					if (prlSTY != NULL)
					{
						STYDATA rlSTY = *prlSTY;
						if (IDYES == MessageBox(LoadStg(IDS_STRING153),LoadStg(IDS_STRING154),(MB_ICONQUESTION | MB_YESNO | MB_DEFBUTTON2)))
						{
							AfxGetApp()->DoWaitCursor(1);
 							if(ogSTYData.DeleteSTY(rlSTY.Urno)==false)
							{
								omErrorTxt.Format("%s %d\n%s",omDeleteErrTxt,ogSTYData.imLastReturnCode, ogSTYData.omLastErrorMessage);
								MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
							}	
							AfxGetApp()->DoWaitCursor(-1);
						}
					} 
				}
				break;
			case TAB_FIDSCOMMAND:
				{		
					int ilLineNo = pomTable->GetCurSel();
					if (ilLineNo == -1)
					{
						MessageBox(LoadStg(IDS_STRING148),LoadStg(IDS_STRING149),MB_ICONEXCLAMATION);
						break;
					}
					FIDDATA *prlFID = ogFIDData.GetFIDByUrno(pomFidsCommandViewer->omLines[ilLineNo].Urno);
					if (prlFID != NULL)
					{
						FIDDATA rlFID = *prlFID;
						if (IDYES == MessageBox(LoadStg(IDS_STRING153),LoadStg(IDS_STRING154),(MB_ICONQUESTION | MB_YESNO | MB_DEFBUTTON2)))
						{
							AfxGetApp()->DoWaitCursor(1);
							if(ogFIDData.DeleteFID(rlFID.Urno)==false)
							{
								omErrorTxt.Format("%s %d\n%s",omDeleteErrTxt,ogFIDData.imLastReturnCode, ogFIDData.omLastErrorMessage);
								MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
							}	
							AfxGetApp()->DoWaitCursor(-1);
						}
					} 
				}
				break;
			case TAB_ERGVERKEHRSARTEN:
				{
					int ilLineNo = pomTable->GetCurSel();
					if (ilLineNo == -1)
					{
						MessageBox(LoadStg(IDS_STRING148),LoadStg(IDS_STRING149),MB_ICONEXCLAMATION);
						break;
					}
					SPHDATA *prlSph = ogSphData.GetSphByUrno(pomErgVerkehrsartenViewer->omLines[ilLineNo].Urno);
					if (prlSph != NULL)
					{
						SPHDATA rlSph = *prlSph;
						if (IDYES == MessageBox(LoadStg(IDS_STRING153),LoadStg(IDS_STRING154),(MB_ICONQUESTION | MB_YESNO | MB_DEFBUTTON2)))
						{
							AfxGetApp()->DoWaitCursor(1);
							if(ogSphData.Delete(rlSph.Urno)==false)
							{
								omErrorTxt.Format("%s %d\n%s",omDeleteErrTxt,ogSphData.imLastReturnCode, ogSphData.omLastErrorMessage);
								MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
							}	
							AfxGetApp()->DoWaitCursor(-1);
						}
					}
				}
				break;
			case TAB_STANDARDVERKETTUNGEN:
				{
					int ilLineNo = pomTable->GetCurSel();
					if (ilLineNo == -1)
					{
						MessageBox(LoadStg(IDS_STRING148),LoadStg(IDS_STRING149),MB_ICONEXCLAMATION);
						break;
					}
					STRDATA *prlStr = ogStrData.GetStrByUrno(pomStandardverkettungenViewer->omLines[ilLineNo].Urno);
					if (prlStr != NULL)
					{
						STRDATA rlStr = *prlStr;
						if (IDYES == MessageBox(LoadStg(IDS_STRING153),LoadStg(IDS_STRING154),(MB_ICONQUESTION | MB_YESNO | MB_DEFBUTTON2)))
						{
							AfxGetApp()->DoWaitCursor(1);
							if(ogStrData.Delete(rlStr.Urno)==false)
							{
								omErrorTxt.Format("%s %d\n%s",omDeleteErrTxt,ogStrData.imLastReturnCode, ogStrData.omLastErrorMessage);
								MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
							}	
							AfxGetApp()->DoWaitCursor(-1);
						}
					}
				}
				break;
			case TAB_FLUGPLANSAISON:
				{
					int ilLineNo = pomTable->GetCurSel();
					if (ilLineNo == -1)
					{
						MessageBox(LoadStg(IDS_STRING148),LoadStg(IDS_STRING149),MB_ICONEXCLAMATION);
						break;
					}
					SEADATA *prlSea = ogSeaData.GetSeaByUrno(pomFlugplansaisonViewer->omLines[ilLineNo].Urno);
					if (prlSea != NULL)
					{
						SEADATA rlSea = *prlSea;
						if (IDYES == MessageBox(LoadStg(IDS_STRING153),LoadStg(IDS_STRING154),(MB_ICONQUESTION | MB_YESNO | MB_DEFBUTTON2)))
						{
							AfxGetApp()->DoWaitCursor(1);
							if(ogSeaData.Delete(rlSea.Urno)==false)
							{
								omErrorTxt.Format("%s %d\n%s",omDeleteErrTxt,ogSeaData.imLastReturnCode, ogSeaData.omLastErrorMessage);
								MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
							}	
							AfxGetApp()->DoWaitCursor(-1);
						}
					}
				}
				break;
			case TAB_LEISTUNGSKATALOG:
				{
					int ilLineNo = pomTable->GetCurSel();
					if (ilLineNo == -1)
					{
						MessageBox(LoadStg(IDS_STRING148),LoadStg(IDS_STRING149),MB_ICONEXCLAMATION);
						break;
					}
					GHSDATA *prlGhs = ogGhsData.GetGhsByUrno(pomLeistungskatalogViewer->omLines[ilLineNo].Urno);
					if (prlGhs != NULL)
					{
						GHSDATA rlGhs = *prlGhs;
						if (IDYES == MessageBox(LoadStg(IDS_STRING153),LoadStg(IDS_STRING154),(MB_ICONQUESTION | MB_YESNO | MB_DEFBUTTON2)))
						{
							AfxGetApp()->DoWaitCursor(1);
							if(ogGhsData.Delete(rlGhs.Urno)==false)
							{
								omErrorTxt.Format("%s %d\n%s",omDeleteErrTxt,ogGhsData.imLastReturnCode,ogGhsData .omLastErrorMessage);
								MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
							}	
							AfxGetApp()->DoWaitCursor(-1);
						}
					}
				}
				break;
			case TAB_QUALIFIKATIONEN:
				{
					int ilLineNo = pomTable->GetCurSel();
					if (ilLineNo == -1)
					{
						MessageBox(LoadStg(IDS_STRING148),LoadStg(IDS_STRING149),MB_ICONEXCLAMATION);
						break;
					}
					PERDATA *prlPer = ogPerData.GetPerByUrno(pomQualifikationenViewer->omLines[ilLineNo].Urno);
					if (prlPer != NULL)
					{
						PERDATA rlPer = *prlPer;
						if (IDYES == MessageBox(LoadStg(IDS_STRING153),LoadStg(IDS_STRING154),(MB_ICONQUESTION | MB_YESNO | MB_DEFBUTTON2)))
						{
							AfxGetApp()->DoWaitCursor(1);
							if(ogPerData.Delete(rlPer.Urno)==false)
							{
								omErrorTxt.Format("%s %d\n%s",omDeleteErrTxt,ogPerData.imLastReturnCode, ogPerData.omLastErrorMessage);
								MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
							}	
							AfxGetApp()->DoWaitCursor(-1);
						}
					}
				}
				break;
			case TAB_ORGANISATIONSEINHEITEN:
				{
					int ilLineNo = pomTable->GetCurSel();
					if (ilLineNo == -1)
					{
						MessageBox(LoadStg(IDS_STRING148),LoadStg(IDS_STRING149),MB_ICONEXCLAMATION);
						break;
					}
					ORGDATA *prlOrg = ogOrgData.GetOrgByUrno(pomOrganisationseinheitenViewer->omLines[ilLineNo].Urno);
					if (prlOrg != NULL)
					{
						ORGDATA rlOrg = *prlOrg;
						if (IDYES == MessageBox(LoadStg(IDS_STRING153),LoadStg(IDS_STRING154),(MB_ICONQUESTION | MB_YESNO | MB_DEFBUTTON2)))
						{
							AfxGetApp()->DoWaitCursor(1);
							if(ogOrgData.Delete(rlOrg.Urno)==false)
							{
								omErrorTxt.Format("%s %d\n%s",omDeleteErrTxt,ogOrgData.imLastReturnCode, ogOrgData.omLastErrorMessage);
								MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
							}	
							AfxGetApp()->DoWaitCursor(-1);
						}
					}
				}
				break;
			case TAB_WEGEZEITEN:				
				{
					int ilLineNo = pomTable->GetCurSel();
					if (ilLineNo == -1)
					{
						MessageBox(LoadStg(IDS_STRING148),LoadStg(IDS_STRING149),MB_ICONEXCLAMATION);
						break;
					}
					WAYDATA *prlWay = ogWayData.GetWayByUrno(pomWegezeitenViewer->omLines[ilLineNo].Urno);
					if (prlWay != NULL)
					{
						WAYDATA rlWay = *prlWay;
						if (IDYES == MessageBox(LoadStg(IDS_STRING153),LoadStg(IDS_STRING154),(MB_ICONQUESTION | MB_YESNO | MB_DEFBUTTON2)))
						{
							AfxGetApp()->DoWaitCursor(1);
							if(ogWayData.Delete(rlWay.Urno)==false)
							{
								omErrorTxt.Format("%s %d\n%s",omDeleteErrTxt,ogWayData.imLastReturnCode, ogWayData.omLastErrorMessage);
								MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
							}	
							AfxGetApp()->DoWaitCursor(-1);
						}
					}
				}
				break;
			case TAB_FUNKTIONEN:				
				{
					int ilLineNo = pomTable->GetCurSel();
					if (ilLineNo == -1)
					{
						MessageBox(LoadStg(IDS_STRING148),LoadStg(IDS_STRING149),MB_ICONEXCLAMATION);
						break;
					}
					PFCDATA *prlPfc = ogPfcData.GetPfcByUrno(pomFunktionenViewer->omLines[ilLineNo].Urno);
					if (prlPfc != NULL)
					{
						PFCDATA rlPfc = *prlPfc;
						if (IDYES == MessageBox(LoadStg(IDS_STRING153),LoadStg(IDS_STRING154),(MB_ICONQUESTION | MB_YESNO | MB_DEFBUTTON2)))
						{
							AfxGetApp()->DoWaitCursor(1);
							if(ogPfcData.Delete(rlPfc.Urno)==false)
							{
								omErrorTxt.Format("%s %d\n%s",omDeleteErrTxt,ogPfcData.imLastReturnCode, ogPfcData.omLastErrorMessage);
								MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
							}	
							AfxGetApp()->DoWaitCursor(-1);
						}
					}
				}
				break;
			case TAB_ARBEITSVERTRAGSARTEN:	
				{
					int ilLineNo = pomTable->GetCurSel();
					if (ilLineNo == -1)
					{
						MessageBox(LoadStg(IDS_STRING148),LoadStg(IDS_STRING149),MB_ICONEXCLAMATION);
						break;
					}
					COTDATA *prlCot = ogCotData.GetCotByUrno(pomArbeitsvertragsartenViewer->omLines[ilLineNo].Urno);
					if (prlCot != NULL)
					{
						COTDATA rlCot = *prlCot;
						if (IDYES == MessageBox(LoadStg(IDS_STRING153),LoadStg(IDS_STRING154),(MB_ICONQUESTION | MB_YESNO | MB_DEFBUTTON2)))
						{
							AfxGetApp()->DoWaitCursor(1);
							if(ogCotData.Delete(rlCot.Urno)==false)
							{
								omErrorTxt.Format("%s %d\n%s",omDeleteErrTxt,ogCotData.imLastReturnCode, ogCotData.omLastErrorMessage);
								MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
							}	
							AfxGetApp()->DoWaitCursor(-1);
						}
					}
				}
				break;
			case TAB_BEWERTUNGSFAKTOREN:		
				{
					int ilLineNo = pomTable->GetCurSel();
					if (ilLineNo == -1)
					{
						MessageBox(LoadStg(IDS_STRING148),LoadStg(IDS_STRING149),MB_ICONEXCLAMATION);
						break;
					}
					ASFDATA *prlAsf = ogAsfData.GetAsfByUrno(pomBewertungsfaktorenViewer->omLines[ilLineNo].Urno);
					if (prlAsf != NULL)
					{
						ASFDATA rlAsf = *prlAsf;
						if (IDYES == MessageBox(LoadStg(IDS_STRING153),LoadStg(IDS_STRING154),(MB_ICONQUESTION | MB_YESNO | MB_DEFBUTTON2)))
						{
							AfxGetApp()->DoWaitCursor(1);
							if(ogAsfData.Delete(rlAsf.Urno)==false)
							{
								omErrorTxt.Format("%s %d\n%s",omDeleteErrTxt,ogAsfData.imLastReturnCode, ogAsfData.omLastErrorMessage);
								MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
							}	
							AfxGetApp()->DoWaitCursor(-1);
						}
					}
				}
				break;
			case TAB_BASISSCHICHTEN:			
				{
					int ilLineNo = pomTable->GetCurSel();
					if (ilLineNo == -1)
					{
						MessageBox(LoadStg(IDS_STRING148),LoadStg(IDS_STRING149),MB_ICONEXCLAMATION);
						break;
					}
					BSDDATA *prlBsd = ogBsdData.GetBsdByUrno(pomBasisschichtenViewer->omLines[ilLineNo].Urno);
					if (prlBsd != NULL)
					{
						BSDDATA rlBsd = *prlBsd;
						if (IDYES == MessageBox(LoadStg(IDS_STRING153),LoadStg(IDS_STRING154),(MB_ICONQUESTION | MB_YESNO | MB_DEFBUTTON2)))
						{
							// Referenz Check
							CheckReferenz olCheckReferenz;
							SetStatusText(0,LoadStg(IDS_STRING120));
							int ilCount = olCheckReferenz.Check(_BSD, rlBsd.Urno, rlBsd.Bsdc);
							SetStatusText();
							bool blDelete = true;
							CString olTxt;
							if(ilCount>0)
							{
								blDelete = false;
								olTxt.Format("%d %s",ilCount, LoadStg(IDS_STRING119));
								if(IDYES == MessageBox(olTxt,LoadStg(IDS_STRING149),(MB_ICONEXCLAMATION | MB_YESNO | MB_DEFBUTTON2)))
								{
									blDelete = true;
								}
							}
							// END Referenz Check
							if(blDelete)
							{
								AfxGetApp()->DoWaitCursor(1);
								if(ogBsdData.Delete(rlBsd.Urno)==false)
								{
									omErrorTxt.Format("%s %d\n%s",omDeleteErrTxt,ogBsdData.imLastReturnCode, ogBsdData.omLastErrorMessage);
									MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
								}	
								AfxGetApp()->DoWaitCursor(-1);
							}
						}
					}
				}
				break;
			case TAB_DIENSTEUNDABWESENHEITEN:
				{
					int ilLineNo = pomTable->GetCurSel();
					if (ilLineNo == -1)
					{
						MessageBox(LoadStg(IDS_STRING148),LoadStg(IDS_STRING149),MB_ICONEXCLAMATION);
						break;
					}
					ODADATA *prlOda = ogOdaData.GetOdaByUrno(pomDiensteUndAbwesenheitenViewer->omLines[ilLineNo].Urno);
					if (prlOda != NULL)
					{
						ODADATA rlOda = *prlOda;
						if (IDYES == MessageBox(LoadStg(IDS_STRING153),LoadStg(IDS_STRING154),(MB_ICONQUESTION | MB_YESNO | MB_DEFBUTTON2)))
						{
							AfxGetApp()->DoWaitCursor(1);
							if(ogOdaData.Delete(rlOda.Urno)==false)
							{
								omErrorTxt.Format("%s %d\n%s",omDeleteErrTxt,ogOdaData.imLastReturnCode, ogOdaData.omLastErrorMessage);
								MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
							}	
							AfxGetApp()->DoWaitCursor(-1);
						}
					}
				}
				break;
			case TAB_FAHRGEMEINSCHAFTEN:		
				{
					int ilLineNo = pomTable->GetCurSel();
					if (ilLineNo == -1)
					{
						MessageBox(LoadStg(IDS_STRING148),LoadStg(IDS_STRING149),MB_ICONEXCLAMATION);
						break;
					}
					TEADATA *prlTea = ogTeaData.GetTeaByUrno(pomFahrgemeinschaftenViewer->omLines[ilLineNo].Urno);
					if (prlTea != NULL)
					{
						TEADATA rlTea = *prlTea;
						if (IDYES == MessageBox(LoadStg(IDS_STRING153),LoadStg(IDS_STRING154),(MB_ICONQUESTION | MB_YESNO | MB_DEFBUTTON2)))
						{
							AfxGetApp()->DoWaitCursor(1);
							if(ogTeaData.Delete(rlTea.Urno)==false)
							{
								omErrorTxt.Format("%s %d\n%s",omDeleteErrTxt,ogTeaData.imLastReturnCode, ogTeaData.omLastErrorMessage);
								MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
							}	
							AfxGetApp()->DoWaitCursor(-1);
						}
					}
				}
				break;
			case TAB_ARBEITSGRUPPEN:		
				{
					int ilLineNo = pomTable->GetCurSel();
					if (ilLineNo == -1)
					{
						MessageBox(LoadStg(IDS_STRING148),LoadStg(IDS_STRING149),MB_ICONEXCLAMATION);
						break;
					}
					WGPDATA *prlWgp = ogWgpData.GetWgpByUrno(pomArbeitsgruppenViewer->omLines[ilLineNo].Urno);
					if (prlWgp != NULL)
					{
						WGPDATA rlWgp = *prlWgp;
						if (IDYES == MessageBox(LoadStg(IDS_STRING153),LoadStg(IDS_STRING154),(MB_ICONQUESTION | MB_YESNO | MB_DEFBUTTON2)))
						{
							AfxGetApp()->DoWaitCursor(1);
							if(ogWgpData.Delete(rlWgp.Urno)==false)
							{
								omErrorTxt.Format("%s %d\n%s",omDeleteErrTxt,ogWgpData.imLastReturnCode, ogWgpData.omLastErrorMessage);
								MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
							}	
							AfxGetApp()->DoWaitCursor(-1);
						}
					}
				}
				break;
			case TAB_MITARBEITERSTAMM:		
				{
					int ilLineNo = pomTable->GetCurSel();
					if (ilLineNo == -1)
					{
						MessageBox(LoadStg(IDS_STRING148),LoadStg(IDS_STRING149),MB_ICONEXCLAMATION);
						break;
					}
					STFDATA *prlStf = ogStfData.GetStfByUrno(pomMitarbeiterstammViewer->omLines[ilLineNo].Urno);
					if (prlStf != NULL)
					{
						STFDATA rlStf = *prlStf;
						if (IDYES == MessageBox(LoadStg(IDS_STRING153),LoadStg(IDS_STRING154),(MB_ICONQUESTION | MB_YESNO | MB_DEFBUTTON2)))
						{
							AfxGetApp()->DoWaitCursor(1);
							char pclWhere[1024]="";
							int i;
							sprintf(pclWhere, " WHERE SURN = %ld", prlStf->Urno);
							AfxGetApp()->DoWaitCursor(1);
							ogSorData.Read(pclWhere);
							ogSpfData.Read(pclWhere);
							ogSpeData.Read(pclWhere);
							ogScoData.Read(pclWhere);
							ogSteData.Read(pclWhere);
							ogSwgData.Read(pclWhere);
							for(i = ogSorData.omData.GetSize()-1; i >= 0 ; i--)
							{
								ogSorData.Delete(ogSorData.omData[i].Urno);
							}
							for(i = ogSpfData.omData.GetSize()-1; i >= 0 ; i--)
							{
								ogSpfData.Delete(ogSpfData.omData[i].Urno);
							}
							for(i = ogSpeData.omData.GetSize()-1; i >= 0 ; i--)
							{
								ogSpeData.Delete(ogSpeData.omData[i].Urno);
							}
							for(i = ogScoData.omData.GetSize()-1; i >= 0 ; i--)
							{
								ogScoData.Delete(ogScoData.omData[i].Urno);
							}
							for(i = ogSteData.omData.GetSize()-1; i >= 0 ; i--)
							{
								ogSteData.Delete(ogSteData.omData[i].Urno);
							}
							for(i = ogSwgData.omData.GetSize()-1; i >= 0 ; i--)
							{
								ogSwgData.Delete(ogSwgData.omData[i].Urno);
							}
							if(ogStfData.Delete(rlStf.Urno)==false)
							{
								omErrorTxt.Format("%s %d\n%s",omDeleteErrTxt,ogStfData.imLastReturnCode, ogStfData.omLastErrorMessage);
								MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
							}	
							AfxGetApp()->DoWaitCursor(-1);
						}
					}
				}
				break;
			case TAB_ERGABFERTIGUNGSARTEN:		
				{
					int ilLineNo = pomTable->GetCurSel();
					if (ilLineNo == -1)
					{
						MessageBox(LoadStg(IDS_STRING148),LoadStg(IDS_STRING149),MB_ICONEXCLAMATION);
						break;
					}
					SPHDATA *prlSph = ogSphData.GetSphByUrno(pomErgAbfertigungsartenViewer->omLines[ilLineNo].Urno);
					if (prlSph != NULL)
					{
						SPHDATA rlSph = *prlSph;
						if (IDYES == MessageBox(LoadStg(IDS_STRING153),LoadStg(IDS_STRING154),(MB_ICONQUESTION | MB_YESNO | MB_DEFBUTTON2)))
						{
							AfxGetApp()->DoWaitCursor(1);
							if(ogSphData.Delete(rlSph.Urno)==false)
							{
								omErrorTxt.Format("%s %d\n%s",omDeleteErrTxt,ogSphData.imLastReturnCode, ogSphData.omLastErrorMessage);
								MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
							}	
							AfxGetApp()->DoWaitCursor(-1);
						}
					}
				}
				break;
			case TAB_REDUKTIONEN:			
				{
					int ilLineNo = pomTable->GetCurSel();
					if (ilLineNo == -1)
					{
						MessageBox(LoadStg(IDS_STRING148),LoadStg(IDS_STRING149),MB_ICONEXCLAMATION);
						break;
					}
					PRCDATA *prlPrc = ogPrcData.GetPrcByUrno(pomReduktionenViewer->omLines[ilLineNo].Urno);
					if (prlPrc != NULL)
					{
						PRCDATA rlPrc = *prlPrc;
						if (IDYES == MessageBox(LoadStg(IDS_STRING153),LoadStg(IDS_STRING154),(MB_ICONQUESTION | MB_YESNO | MB_DEFBUTTON2)))
						{
							AfxGetApp()->DoWaitCursor(1);
							if(ogPrcData.Delete(rlPrc.Urno)==false)
							{
								omErrorTxt.Format("%s %d\n%s",omDeleteErrTxt,ogPrcData.imLastReturnCode, ogPrcData.omLastErrorMessage);
								MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
							}	
							AfxGetApp()->DoWaitCursor(-1);
						}
					}
				}
				break;
			case TAB_GERAETEGRUPPEN:			
				{
					int ilLineNo = pomTable->GetCurSel();
					if (ilLineNo == -1)
					{
						MessageBox(LoadStg(IDS_STRING148),LoadStg(IDS_STRING149),MB_ICONEXCLAMATION);
						break;
					}
					GEGDATA *prlGeg = ogGegData.GetGegByUrno(pomGeraetegruppenViewer->omLines[ilLineNo].Urno);
					if (prlGeg != NULL)
					{
						GEGDATA rlGeg = *prlGeg;
						if (IDYES == MessageBox(LoadStg(IDS_STRING153),LoadStg(IDS_STRING154),(MB_ICONQUESTION | MB_YESNO | MB_DEFBUTTON2)))
						{
							AfxGetApp()->DoWaitCursor(1);
							if(ogGegData.Delete(rlGeg.Urno)==false)
							{
								omErrorTxt.Format("%s %d\n%s",omDeleteErrTxt,ogGegData.imLastReturnCode, ogGegData.omLastErrorMessage);
								MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
							}	
							AfxGetApp()->DoWaitCursor(-1);
						}
					}
				}
				break;
			case TAB_ORGANIZER:			
				{
					int ilLineNo = pomTable->GetCurSel();
					if (ilLineNo == -1)
					{
						MessageBox(LoadStg(IDS_STRING148),LoadStg(IDS_STRING149),MB_ICONEXCLAMATION);
						break;
					}
					CHTDATA *prlCht = ogChtData.GetChtByUrno(pomOrganizerViewer->omLines[ilLineNo].Urno);
					if (prlCht != NULL)
					{
						CHTDATA rlCht = *prlCht;
						if (IDYES == MessageBox(LoadStg(IDS_STRING153),LoadStg(IDS_STRING154),(MB_ICONQUESTION | MB_YESNO | MB_DEFBUTTON2)))
						{
							AfxGetApp()->DoWaitCursor(1);
							if(ogChtData.Delete(rlCht.Urno)==false)
							{
								omErrorTxt.Format("%s %d\n%s",omDeleteErrTxt,ogChtData.imLastReturnCode, ogChtData.omLastErrorMessage);
								MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
							}	
							AfxGetApp()->DoWaitCursor(-1);
						}
					}
				}
				break;
			case TAB_TIMEPARAMETERS:			
				{
					int ilLineNo = pomTable->GetCurSel();
					if (ilLineNo == -1)
					{
						MessageBox(LoadStg(IDS_STRING148),LoadStg(IDS_STRING149),MB_ICONEXCLAMATION);
						break;
					}
					TIPDATA *prlTip = ogTipData.GetTipByUrno(pomTimeParametersViewer->omLines[ilLineNo].Urno);
					if (prlTip != NULL)
					{
						TIPDATA rlTip = *prlTip;
						if (IDYES == MessageBox(LoadStg(IDS_STRING153),LoadStg(IDS_STRING154),(MB_ICONQUESTION | MB_YESNO | MB_DEFBUTTON2)))
						{
							AfxGetApp()->DoWaitCursor(1);
							if(ogTipData.Delete(rlTip.Urno)==false)
							{
								omErrorTxt.Format("%s %d\n%s",omDeleteErrTxt,ogTipData.imLastReturnCode, ogTipData.omLastErrorMessage);
								MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
							}	
							AfxGetApp()->DoWaitCursor(-1);
						}
					}
				}
				break;
			case TAB_HOLIDAY:			
				{
					int ilLineNo = pomTable->GetCurSel();
					if (ilLineNo == -1)
					{
						MessageBox(LoadStg(IDS_STRING148),LoadStg(IDS_STRING149),MB_ICONEXCLAMATION);
						break;
					}
					HOLDATA *prlHol = ogHolData.GetHolByUrno(pomHolidayViewer->omLines[ilLineNo].Urno);
					if (prlHol != NULL)
					{
						HOLDATA rlHol = *prlHol;
						if (IDYES == MessageBox(LoadStg(IDS_STRING153),LoadStg(IDS_STRING154),(MB_ICONQUESTION | MB_YESNO | MB_DEFBUTTON2)))
						{
							AfxGetApp()->DoWaitCursor(1);
							if(ogHolData.Delete(rlHol.Urno)==false)
							{
								omErrorTxt.Format("%s %d\n%s",omDeleteErrTxt,ogHolData.imLastReturnCode, ogHolData.omLastErrorMessage);
								MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
							}	
							AfxGetApp()->DoWaitCursor(-1);
						}
					}
				}
				break;
			case TAB_PLANNING_GROUPS:			
				{
					int ilLineNo = pomTable->GetCurSel();
					if (ilLineNo == -1)
					{
						MessageBox(LoadStg(IDS_STRING148),LoadStg(IDS_STRING149),MB_ICONEXCLAMATION);
						break;
					}
					PGPDATA *prlPgp = ogPgpData.GetPgpByUrno(pomPlanungsgruppenViewer->omLines[ilLineNo].Urno);
					if (prlPgp != NULL)
					{
						PGPDATA rlPgp = *prlPgp;
						if (IDYES == MessageBox(LoadStg(IDS_STRING153),LoadStg(IDS_STRING154),(MB_ICONQUESTION | MB_YESNO | MB_DEFBUTTON2)))
						{
							AfxGetApp()->DoWaitCursor(1);
							if(ogPgpData.Delete(rlPgp.Urno) == false)
							{
								omErrorTxt.Format("%s %d\n%s",omDeleteErrTxt,ogPgpData.imLastReturnCode, ogPgpData.omLastErrorMessage);
								MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
							}	
							AfxGetApp()->DoWaitCursor(-1);
						}
					}
				}
				break;
			case TAB_VERYIMPPERS:			
				{
					int ilLineNo = pomTable->GetCurSel();
					if (ilLineNo == -1)
					{
						MessageBox(LoadStg(IDS_STRING148),LoadStg(IDS_STRING149),MB_ICONEXCLAMATION);
						break;
					}
					VIPDATA *prlVip = ogVipData.GetVipByUrno(pomVeryimppersViewer->omLines[ilLineNo].Urno);
					if (prlVip != NULL)
					{
						VIPDATA rlVip = *prlVip;
						if (IDYES == MessageBox(LoadStg(IDS_STRING153),LoadStg(IDS_STRING154),(MB_ICONQUESTION | MB_YESNO | MB_DEFBUTTON2)))
						{
							AfxGetApp()->DoWaitCursor(1);
							if(ogVipData.Delete(rlVip.Urno) == false)
							{
								omErrorTxt.Format("%s %d\n%s",omDeleteErrTxt,ogVipData.imLastReturnCode, ogVipData.omLastErrorMessage);
								MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
							}	
							AfxGetApp()->DoWaitCursor(-1);
						}
					}
				}
				break;
			case TAB_AIRPORTWET:			
				{
					int ilLineNo = pomTable->GetCurSel();
					if (ilLineNo == -1)
					{
						MessageBox(LoadStg(IDS_STRING148),LoadStg(IDS_STRING149),MB_ICONEXCLAMATION);
						break;
					}
					AWIDATA *prlAwi = ogAwiData.GetAwiByUrno(pomAirportwetViewer->omLines[ilLineNo].Urno);
					if (prlAwi != NULL)
					{
						AWIDATA rlAwi = *prlAwi;
						if (IDYES == MessageBox(LoadStg(IDS_STRING153),LoadStg(IDS_STRING154),(MB_ICONQUESTION | MB_YESNO | MB_DEFBUTTON2)))
						{
							AfxGetApp()->DoWaitCursor(1);
							if(ogAwiData.Delete(rlAwi.Urno) == false)
							{
								omErrorTxt.Format("%s %d\n%s",omDeleteErrTxt,ogAwiData.imLastReturnCode, ogAwiData.omLastErrorMessage);
								MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
							}	
							AfxGetApp()->DoWaitCursor(-1);
						}
					}
				}
				break;
			case TAB_COUNTERCLASS:			
				{
					int ilLineNo = pomTable->GetCurSel();
					if (ilLineNo == -1)
					{
						MessageBox(LoadStg(IDS_STRING148),LoadStg(IDS_STRING149),MB_ICONEXCLAMATION);
						break;
					}
					CCCDATA *prlCcc = ogCccData.GetCccByUrno(pomCounterclassViewer->omLines[ilLineNo].Urno);
					if (prlCcc != NULL)
					{
						CCCDATA rlCcc = *prlCcc;
						if (IDYES == MessageBox(LoadStg(IDS_STRING153),LoadStg(IDS_STRING154),(MB_ICONQUESTION | MB_YESNO | MB_DEFBUTTON2)))
						{
							AfxGetApp()->DoWaitCursor(1);
							if(ogCccData.Delete(rlCcc.Urno) == false)
							{
								omErrorTxt.Format("%s %d\n%s",omDeleteErrTxt,ogCccData.imLastReturnCode, ogCccData.omLastErrorMessage);
								MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
							}	
							AfxGetApp()->DoWaitCursor(-1);
						}
					}
				}
				break;
			case TAB_AIRCRAFTFAM:		
				{
					int ilLineNo = pomTable->GetCurSel();
					if (ilLineNo == -1)
					{
						MessageBox(LoadStg(IDS_STRING148),LoadStg(IDS_STRING149),MB_ICONEXCLAMATION);
						break;
					}
					AFMDATA *prlAfm = ogAFMData.GetAfmByUrno(pomAircraftFamViewer->omLines[ilLineNo].Urno);
					if (prlAfm != NULL)
					{
						AFMDATA rlAfm = *prlAfm;
						if (IDYES == MessageBox(LoadStg(IDS_STRING153),LoadStg(IDS_STRING154),(MB_ICONQUESTION | MB_YESNO | MB_DEFBUTTON2)))
						{
							AfxGetApp()->DoWaitCursor(1);
							if(ogAFMData.Delete(rlAfm.Urno)==false)
							{
								omErrorTxt.Format("%s %d\n%s",omDeleteErrTxt,ogENTData.imLastReturnCode, ogENTData.omLastErrorMessage);
								MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
							}	
							AfxGetApp()->DoWaitCursor(-1);
						}
					}
				}
				break;
			case TAB_ENGINETYPE:		
				{
					int ilLineNo = pomTable->GetCurSel();
					if (ilLineNo == -1)
					{
						MessageBox(LoadStg(IDS_STRING148),LoadStg(IDS_STRING149),MB_ICONEXCLAMATION);
						break;
					}
					ENTDATA *prlEnt = ogENTData.GetEntByUrno(pomEngineTypeViewer->omLines[ilLineNo].Urno);
					if (prlEnt != NULL)
					{
						ENTDATA rlEnt = *prlEnt;
						if (IDYES == MessageBox(LoadStg(IDS_STRING153),LoadStg(IDS_STRING154),(MB_ICONQUESTION | MB_YESNO | MB_DEFBUTTON2)))
						{
							AfxGetApp()->DoWaitCursor(1);
							if(ogENTData.Delete(rlEnt.Urno)==false)
							{
								omErrorTxt.Format("%s %d\n%s",omDeleteErrTxt,ogENTData.imLastReturnCode, ogENTData.omLastErrorMessage);
								MessageBox(omErrorTxt,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
							}	
							AfxGetApp()->DoWaitCursor(-1);
						}
					}
				}
				break;
			default:
				{
					// Do nothing
				}
				break; 
		}
		bgIsDialogOpen = false;
	}	
}

//------------------------------------------------------------------------------------------------------

void CStammdaten::OnErsetzen() 
{
	if(!bgIsDialogOpen)
	{
		bgIsDialogOpen = true;
		Beep(440,70);
		switch(igSelectTab)
		{
			case TAB_AIRLINE:
				{

				}
				break;
			case TAB_AIRCRAFT:
				{

				}
				break;
			case TAB_LFZREGISTRATION:
				{

				}
				break;
			case TAB_AIRPORT:
				{

				}
				break;
			case TAB_RUNWAY:
				{

				}
				break;
			case TAB_TAXIWAY:
				{

				}
				break;
			case TAB_POSITION:
				{

				}
				break;
			case TAB_GATE:
				{

				}
				break;
			case TAB_CHECKINCOUNTER:
				{

				}
				break;
			case TAB_BEGGAGEBELT:
				{

				}
				break;
			case TAB_EXIT:
				{

				}
				break;
			case TAB_DELAYCODE:
				{

				}
				break;
			case TAB_TELEXADRESS:
				{

				}
				break;
			case TAB_TRAFFICTYPE:
				{

				}
				break;
			case TAB_HANDLINGAGENT:
				{

				}
				break;
			case TAB_WAITINGROOM:
				{

				}
				break;
			case TAB_HANDLINGTYPE:
				{

				}
				break;
			case TAB_SERVICETYPE:
				{

				}
				break;
			case TAB_FIDSCOMMAND:
				{		

				}
				break;
			case TAB_ORGANIZER:
				{		

				}
				break;
			case TAB_TIMEPARAMETERS:
				{		

				}
				break;
			case TAB_HOLIDAY:
				{		

				}
				break;
			case TAB_PLANNING_GROUPS:
				{		

				}
				break;
			case TAB_VERYIMPPERS:
				{		

				}
				break;
			case TAB_COUNTERCLASS:
				{		

				}
				break;
			case TAB_AIRPORTWET:
				{		

				}
				break;
			case TAB_AIRCRAFTFAM:
				{		

				}
				break;
			case TAB_ENGINETYPE:
				{		

				}
				break;
			default:
				{
					// Do nothing
				}
				break; 
		}
		bgIsDialogOpen = false;
	}	
}

//------------------------------------------------------------------------------------------------------

void CStammdaten::OnDrucken() 
{
	if(!bgIsDialogOpen)
	{
		bgIsDialogOpen = true;
		switch(igSelectTab)
		{
			case TAB_AIRLINE:
				{
					if(pomAirlineViewer->omLines.GetSize()!=0)
						pomAirlineViewer->PrintTableView();
				}
				break;
			case TAB_AIRCRAFT:
				{
					if(pomAircraftViewer->omLines.GetSize()!=0)
						pomAircraftViewer->PrintTableView();
				}
				break;
			case TAB_LFZREGISTRATION:
				{
					if(pomLFZRegiViewer->omLines.GetSize()!=0)
						pomLFZRegiViewer->PrintTableView();
				}
				break;
			case TAB_AIRPORT:
				{
					if(pomAirportViewer->omLines.GetSize()!=0)
						pomAirportViewer->PrintTableView();
				}
				break;
			case TAB_RUNWAY:
				{
					if(pomRunwayViewer->omLines.GetSize()!=0)
						pomRunwayViewer->PrintTableView();
				}
				break;
			case TAB_TAXIWAY:
				{
					if(pomTaxiwayViewer->omLines.GetSize()!=0)
						pomTaxiwayViewer->PrintTableView();
				}
				break;
			case TAB_POSITION:
				{
					if(pomPositionViewer->omLines.GetSize()!=0)
						pomPositionViewer->PrintTableView();
				}
				break;
			case TAB_GATE:
				{
					if(pomGateViewer->omLines.GetSize()!=0)
						pomGateViewer->PrintTableView();
				}
				break;
			case TAB_CHECKINCOUNTER:
				{
					if(pomCheckinCounterViewer->omLines.GetSize()!=0)
						pomCheckinCounterViewer->PrintTableView();
				}
				break;
			case TAB_BEGGAGEBELT:
				{
					if(pomBeggagebeltViewer->omLines.GetSize()!=0)
						pomBeggagebeltViewer->PrintTableView();
				}
				break;
			case TAB_EXIT:
				{
					if(pomExitViewer->omLines.GetSize()!=0)
						pomExitViewer->PrintTableView();
				}
				break;
			case TAB_DELAYCODE:
				{
					if(pomDelaycodeViewer->omLines.GetSize()!=0)
						pomDelaycodeViewer->PrintTableView();
				}
				break;
			case TAB_TELEXADRESS:
				{
					if(pomTelexadressViewer->omLines.GetSize()!=0)
						pomTelexadressViewer->PrintTableView();
				}
				break;
			case TAB_TRAFFICTYPE:
				{
					if(pomTraffictypeViewer->omLines.GetSize()!=0)
						pomTraffictypeViewer->PrintTableView();
				}
				break;
			case TAB_HANDLINGAGENT:
				{
					if(pomHandlingagentViewer->omLines.GetSize()!=0)
						pomHandlingagentViewer->PrintTableView();
				}
				break;
			case TAB_WAITINGROOM:
				{
					if(pomWaitingroomViewer->omLines.GetSize()!=0)
						pomWaitingroomViewer->PrintTableView();
				}
				break;
			case TAB_HANDLINGTYPE:
				{
					if(pomHandlingtypeViewer->omLines.GetSize()!=0)
						pomHandlingtypeViewer->PrintTableView();
				}
				break;
			case TAB_SERVICETYPE:
				{
					if(pomServicetypeViewer->omLines.GetSize()!=0)
						pomServicetypeViewer->PrintTableView();
				}
				break;
			case TAB_FIDSCOMMAND:
				{		
					if(pomFidsCommandViewer->omLines.GetSize()!=0)
						pomFidsCommandViewer->PrintTableView();
				}
				break;
			case TAB_ERGVERKEHRSARTEN:
				{		
					if(pomErgVerkehrsartenViewer->omLines.GetSize()!=0)
						pomErgVerkehrsartenViewer->PrintTableView();
				}
				break;
			case TAB_STANDARDVERKETTUNGEN:
				{		
					if(pomStandardverkettungenViewer->omLines.GetSize()!=0)
						pomStandardverkettungenViewer->PrintTableView();
				}
				break;
			case TAB_FLUGPLANSAISON:
				{		
					if(pomFlugplansaisonViewer->omLines.GetSize()!=0)
						pomFlugplansaisonViewer->PrintTableView();
				}
				break;
			case TAB_LEISTUNGSKATALOG:
				{		
					if(pomLeistungskatalogViewer->omLines.GetSize()!=0)
						pomLeistungskatalogViewer->PrintTableView();
				}
				break;
			case TAB_QUALIFIKATIONEN:
				{		
					if(pomQualifikationenViewer->omLines.GetSize()!=0)
						pomQualifikationenViewer->PrintTableView();
				}
				break;
			case TAB_ORGANISATIONSEINHEITEN:
				{		
					if(pomOrganisationseinheitenViewer->omLines.GetSize()!=0)
						pomOrganisationseinheitenViewer->PrintTableView();
				}
				break;
			case TAB_WEGEZEITEN:				
				{
					if(pomWegezeitenViewer->omLines.GetSize()!=0)
						pomWegezeitenViewer->PrintTableView();
				}
				break;
			case TAB_FUNKTIONEN:				
				{
					if(pomFunktionenViewer->omLines.GetSize()!=0)
						pomFunktionenViewer->PrintTableView();
				}
				break;
			case TAB_ARBEITSVERTRAGSARTEN:	
				{
					if(pomArbeitsvertragsartenViewer->omLines.GetSize()!=0)
						pomArbeitsvertragsartenViewer->PrintTableView();
				}
				break;
			case TAB_BEWERTUNGSFAKTOREN:		
				{
					if(pomBewertungsfaktorenViewer->omLines.GetSize()!=0)
						pomBewertungsfaktorenViewer->PrintTableView();
				}
				break;
			case TAB_BASISSCHICHTEN:			
				{
					if(pomBasisschichtenViewer->omLines.GetSize()!=0)
						pomBasisschichtenViewer->PrintTableView();
				}
				break;
			case TAB_DIENSTEUNDABWESENHEITEN:
				{
					if(pomDiensteUndAbwesenheitenViewer->omLines.GetSize()!=0)
						pomDiensteUndAbwesenheitenViewer->PrintTableView();
				}
				break;
			case TAB_FAHRGEMEINSCHAFTEN:		
				{
					if(pomFahrgemeinschaftenViewer->omLines.GetSize()!=0)
						pomFahrgemeinschaftenViewer->PrintTableView();
				}
				break;
			case TAB_ARBEITSGRUPPEN:		
				{
					if(pomArbeitsgruppenViewer->omLines.GetSize()!=0)
						pomArbeitsgruppenViewer->PrintTableView();
				}
				break;
			case TAB_MITARBEITERSTAMM:		
				{
					if(pomMitarbeiterstammViewer->omLines.GetSize()!=0)
						pomMitarbeiterstammViewer->PrintTableView();
				}
				break;
			case TAB_ERGABFERTIGUNGSARTEN:		
				{
					if(pomErgAbfertigungsartenViewer->omLines.GetSize()!=0)
						pomErgAbfertigungsartenViewer->PrintTableView();
				}
				break;
			case TAB_REDUKTIONEN:			
				{
					if(pomReduktionenViewer->omLines.GetSize()!=0)
						pomReduktionenViewer->PrintTableView();
				}
				break;
			case TAB_GERAETEGRUPPEN:			
				{
					if(pomGeraetegruppenViewer->omLines.GetSize()!=0)
						pomGeraetegruppenViewer->PrintTableView();
				}
				break;
			case TAB_ORGANIZER:			
				{
					if(pomOrganizerViewer->omLines.GetSize()!=0)
						pomOrganizerViewer->PrintTableView();
				}
				break;
			case TAB_TIMEPARAMETERS:			
				{
					if(pomTimeParametersViewer->omLines.GetSize()!=0)
						pomTimeParametersViewer->PrintTableView();
				}
				break;
			case TAB_HOLIDAY:			
				{
					if(pomHolidayViewer->omLines.GetSize()!=0)
						pomHolidayViewer->PrintTableView();
				}
				break;
			case TAB_PLANNING_GROUPS:			
				{
					if(pomPlanungsgruppenViewer->omLines.GetSize()!=0)
						pomPlanungsgruppenViewer->PrintTableView();
				}
				break;
			case TAB_AIRPORTWET:			
				{
					if(pomAirportwetViewer->omLines.GetSize()!=0)
						pomAirportwetViewer->PrintTableView();
				}
				break;
			case TAB_COUNTERCLASS:			
				{
					if(pomCounterclassViewer->omLines.GetSize()!=0)
						pomCounterclassViewer->PrintTableView();
				}
				break;
			case TAB_VERYIMPPERS:			
				{
					if(pomVeryimppersViewer->omLines.GetSize()!=0)
						pomVeryimppersViewer->PrintTableView();
				}
				break;
			case TAB_AIRCRAFTFAM:		
				{
					if(pomAircraftFamViewer->omLines.GetSize()!=0)
						pomAircraftFamViewer->PrintTableView();
				}
				break;
			case TAB_ENGINETYPE:		
				{
					if(pomEngineTypeViewer->omLines.GetSize()!=0)
						pomEngineTypeViewer->PrintTableView();
				}
				break;
			default:
				{
					// Do nothing
				}
				break; 
		}
		bgIsDialogOpen = false;
	}		
}

//------------------------------------------------------------------------------------------------------

void CStammdaten::OnHilfe() 
{
/*	if(!bgIsDialogOpen)
	{
		bgIsDialogOpen = true;
		Beep(440,70);
		bgIsDialogOpen = false;
	}
*/
	AfxGetApp()->WinHelp(0,HELP_CONTENTS);
}

//------------------------------------------------------------------------------------------------------

void CStammdaten::OnAllgemein() 
{
	if(!bgIsDialogOpen)
	{
		bgIsDialogOpen = true;
		Beep(440,70);
		bgIsDialogOpen = false;
	}		
}

//------------------------------------------------------------------------------------------------------

void CStammdaten::OnBeenden() 
{
	if(!bgIsDialogOpen)
	{
		bgIsDialogOpen = true;
		if (IDYES == MessageBox(LoadStg(IDS_STRING155),LoadStg(IDS_STRING154),(MB_ICONQUESTION | MB_YESNO)))
		{
			ClearData(igSelectTab);
			CDialog::OnCancel();	
		}
		bgIsDialogOpen = false;
	}	
}

//------------------------------------------------------------------------------------------------------

void CStammdaten::OnOK() 
{
	//do nothing
}

//------------------------------------------------------------------------------------------------------

void CStammdaten::OnCancel() 
{
	OnBeenden();
}

//------------------------------------------------------------------------------------------------------

void CStammdaten::ClearData(int ilSelectTabOld) 
{
	switch(ilSelectTabOld)
	{
		case TAB_AIRLINE:
			delete pomAirlineViewer;
			ogALTData.ClearAll();
			break;
		case TAB_AIRCRAFT:
			delete pomAircraftViewer;
			ogACTData.ClearAll();
			break;
		case TAB_LFZREGISTRATION:
			delete pomLFZRegiViewer;
			ogACRData.ClearAll();
			break;
		case TAB_AIRPORT:
			delete pomAirportViewer;
			ogAPTData.ClearAll(); 
			break;
		case TAB_RUNWAY:
			delete pomRunwayViewer;
			ogRWYData.ClearAll(); 
			break;
		case TAB_TAXIWAY:
			delete pomTaxiwayViewer;
			ogTWYData.ClearAll(); 
			break;
		case TAB_POSITION:
			delete pomPositionViewer;
			ogPSTData.ClearAll(); 
			break;
		case TAB_GATE:
			delete pomGateViewer;
			ogGATData.ClearAll(); 
			break;
		case TAB_CHECKINCOUNTER:
			delete pomCheckinCounterViewer;
			ogCICData.ClearAll(); 
			break;
		case TAB_BEGGAGEBELT:
			delete pomBeggagebeltViewer;
			ogBLTData.ClearAll(); 
			break;
		case TAB_EXIT:
			delete pomExitViewer;
			ogEXTData.ClearAll(); 
			break;
		case TAB_DELAYCODE:
			delete pomDelaycodeViewer;
			ogDENData.ClearAll(); 
			break;
		case TAB_TELEXADRESS:
			delete pomTelexadressViewer;
			ogMVTData.ClearAll(); 
			break;
		case TAB_TRAFFICTYPE:
			delete pomTraffictypeViewer;
			ogNATData.ClearAll(); 
			break;
		case TAB_HANDLINGAGENT:
			delete pomHandlingagentViewer;
			ogHAGData.ClearAll(); 
			break;
		case TAB_WAITINGROOM:
			delete pomWaitingroomViewer;
			ogWROData.ClearAll(); 
			break;
		case TAB_HANDLINGTYPE:
			delete pomHandlingtypeViewer;
			ogHTYData.ClearAll(); 
			break;
		case TAB_SERVICETYPE:
			delete pomServicetypeViewer;
			ogSTYData.ClearAll(); 
			break;
		case TAB_FIDSCOMMAND:
			delete pomFidsCommandViewer;
			ogFIDData.ClearAll(); 
			break;
		case TAB_ERGVERKEHRSARTEN:
			delete pomErgVerkehrsartenViewer;
			ogSphData.ClearAll();
			break;
		case TAB_STANDARDVERKETTUNGEN:
			delete pomStandardverkettungenViewer;
			ogStrData.ClearAll();
			break;
		case TAB_FLUGPLANSAISON:
			delete pomFlugplansaisonViewer;
			ogSeaData.ClearAll();
			break;
		case TAB_LEISTUNGSKATALOG:
			delete pomLeistungskatalogViewer;
			ogGhsData.ClearAll();
			break;
		case TAB_QUALIFIKATIONEN:
			delete pomQualifikationenViewer;
			ogPerData.ClearAll();
			break;
		case TAB_ORGANISATIONSEINHEITEN:
			delete pomOrganisationseinheitenViewer;
			ogOrgData.ClearAll();
			break;
		case TAB_WEGEZEITEN:					
			delete pomWegezeitenViewer;
			ogWayData.ClearAll();
			break;
		case TAB_FUNKTIONEN:				
			delete pomFunktionenViewer;	
			ogPfcData.ClearAll();
			break;
		case TAB_ARBEITSVERTRAGSARTEN:		
			delete pomArbeitsvertragsartenViewer;
			ogCotData.ClearAll();
			break;
		case TAB_BEWERTUNGSFAKTOREN:		
			delete pomBewertungsfaktorenViewer;
			ogAsfData.ClearAll();
			break;
		case TAB_BASISSCHICHTEN:			
			delete pomBasisschichtenViewer;
			ogBsdData.ClearAll();
			break;
		case TAB_DIENSTEUNDABWESENHEITEN:	
			delete pomDiensteUndAbwesenheitenViewer;
			ogOdaData.ClearAll();
			break;
		case TAB_FAHRGEMEINSCHAFTEN:		
			delete pomFahrgemeinschaftenViewer;
			ogTeaData.ClearAll();
			break;
		case TAB_ARBEITSGRUPPEN:		
			delete pomArbeitsgruppenViewer;
			ogWgpData.ClearAll();
			break;
		case TAB_MITARBEITERSTAMM:			
			delete pomMitarbeiterstammViewer;
			ogStfData.ClearAll();
			break;
		case TAB_ERGABFERTIGUNGSARTEN:		
			delete pomErgAbfertigungsartenViewer;
			ogSphData.ClearAll();
			break;
		case TAB_REDUKTIONEN:				
			delete pomReduktionenViewer;
			ogPrcData.ClearAll();
			break;
		case TAB_GERAETEGRUPPEN:				
			delete pomGeraetegruppenViewer;
			ogPrcData.ClearAll();
			break;
		case TAB_ORGANIZER:				
			delete pomOrganizerViewer;
			ogChtData.ClearAll();
			break;
		case TAB_TIMEPARAMETERS:				
			delete pomTimeParametersViewer;
			ogTipData.ClearAll();
			break;
		case TAB_HOLIDAY:				
			delete pomHolidayViewer;
			ogHolData.ClearAll();
			break;
		case TAB_PLANNING_GROUPS:				
			delete pomPlanungsgruppenViewer;
			ogPgpData.ClearAll();
			break;
		case TAB_AIRPORTWET:				
			delete pomAirportwetViewer;
			ogAwiData.ClearAll();
			break;
		case TAB_COUNTERCLASS:				
			delete pomCounterclassViewer;
			ogCccData.ClearAll();
			break;
		case TAB_VERYIMPPERS:				
			delete pomVeryimppersViewer;
			ogVipData.ClearAll();
			break;
		case TAB_AIRCRAFTFAM:				
			delete pomAircraftFamViewer;
			ogAFMData.ClearAll();
			break;
		case TAB_ENGINETYPE:				
			delete pomEngineTypeViewer;
			ogENTData.ClearAll();
			break;
		default:
			{
				// Do nothing
			}
			break; 
	}
}

//------------------------------------------------------------------------------------------------------

bool CStammdaten::SetStatusText(UINT olTextID, CString olText)
{
	CString olSettingText = "";
	if(olTextID != 0)
	{
		olSettingText.LoadString(olTextID);
	}
	if(olText != "NOTEXT")
	{
		olSettingText += olText; 
	}
	m_Status.SetWindowText(olSettingText);
	return true;
}

//------------------------------------------------------------------------------------------------------

LONG CStammdaten::OnTableLButtonDblclk(UINT wParam, LONG lParam) 
{
	if(!bgIsDialogOpen && m_AENDERN.IsWindowEnabled() && m_AENDERN.IsWindowVisible())
	{
		OnAendern();
	}
	return 0L;
}

//------------------------------------------------------------------------------------------------------

void CStammdaten::OnInfo()
{
	if(!bgIsDialogOpen)
	{
		bgIsDialogOpen = true;
		InfoDlg dlgInfo;
		dlgInfo.DoModal();
		bgIsDialogOpen = false;
	}		
}

//------------------------------------------------------------------------------------------------------

LONG CStammdaten::OnBcAdd(UINT /*wParam*/, LONG /*lParam*/)
{
	//Beep(440,70);Beep(440,70);
	ogBcHandle.GetBc();
	return TRUE;
}

//------------------------------------------------------------------------------------------------------

bool CStammdaten::UpdateBlkTable(CCSPtrArray<BLKDATA> *popBlkPtrA, CCSPtrArray<BLKDATA> *popDeleteBlkPtrA, CString opBurn, CString opTabn)
{
	int ilChanged = popBlkPtrA->GetSize();
	for(int i=0;i<ilChanged;i++)
	{
		BLKDATA rlBlk = popBlkPtrA->GetAt(i);

		if(rlBlk.IsChanged == DATA_CHANGED || rlBlk.IsChanged == DATA_NEW)
		{
			if(rlBlk.IsChanged == DATA_NEW)
			{
				rlBlk.Urno = ogBasicData.GetNextUrno();	//new URNO
				rlBlk.Burn = atoi(opBurn); 
				sprintf(rlBlk.Tabn,"%s",opTabn);
			}
			ogBlkData.Save(&rlBlk);
		}
	}

	int ilDeleted = popDeleteBlkPtrA->GetSize();
	for(i=0;i<ilDeleted;i++)
	{
		BLKDATA rlBlk = popDeleteBlkPtrA->GetAt(i);
		if(rlBlk.Urno > 0)
		{
			ogBlkData.Save(&rlBlk);
		}
	}

	return true;
}

//------------------------------------------------------------------------------------------------------

static int Compare_Menue( const TABLES **e1, const TABLES **e2)
{
	return (strcmp((**e1).TableName, (**e2).TableName));
}

//------------------------------------------------------------------------------------------------------

void CStammdaten::OnSize(UINT nType, int cx, int cy) 
{
	CDialog::OnSize(nType, cx, cy);
	
	// TODO: Add your message handler code here
	m_resizeHelper.OnSize();
	if (this->pomTable != NULL && ::IsWindow(this->pomTable->m_hWnd))
	{
		CRect olrectStatus;
		m_Status.GetWindowRect(&olrectStatus);
		int ilStatusHigh = olrectStatus.bottom-olrectStatus.top;

		CRect olrectTable;
		GetClientRect(&olrectTable);
		olrectTable.InflateRect(1,1);     // hiding the CTable window border
		this->pomTable->SetPosition(olrectTable.left+2, olrectTable.right-2, olrectTable.top+35, olrectTable.bottom-(ilStatusHigh+3));

		this->pomTable->DisplayTable();
	}

	this->Invalidate();

}

LONG CStammdaten::OnEvaluateCmdLine(UINT wParam, LONG lParam)
{
	CString olStartupTable;
	long	llStartupUrno;

	if (ogBasicData.GetCmdLineResource(olStartupTable,llStartupUrno))
	{
		for (int ilTab = 0; ilTab < omTables.GetSize(); ilTab++)
		{
			if (omTables[ilTab].TableCode == olStartupTable)
			{
				if (omTables[ilTab].SubMenueID == SUBMENUE_GENERAL)
				{
					if (!(omAllgemeinMenu.GetMenuState(omTables[ilTab].MenueID,MF_BYCOMMAND) & MF_GRAYED))
					{
						WORD wParam = omTables[ilTab].MenueID;
						this->SendMessage(WM_COMMAND,wParam,NULL);						

						if (pomCurrentViewer != NULL)
						{
							if (pomCurrentViewer->GetViewName() != "<Default>")
							{
								int ind = m_AnzeigeComboBox.FindStringExact(-1,"<Default>");
								if (ind != CB_ERR)
								{
									m_AnzeigeComboBox.SetCurSel(ind);
									DWORD lParam = MAKELPARAM(IDC_ANZEIGE,CBN_SELCHANGE);
									this->SendMessage(WM_COMMAND,lParam,(LPARAM)m_AnzeigeComboBox.m_hWnd);
								}
							}

							bool blLine = false;
							int ilLine = -1;

							switch(omTables[ilTab].MenueID)
							{
							case TAB_POSITION:
								blLine = pomPositionViewer->FindLine(llStartupUrno,ilLine);
							break;
							case TAB_GATE:
								blLine = pomGateViewer->FindLine(llStartupUrno,ilLine);
							break;
							case TAB_BEGGAGEBELT :
								blLine = pomBeggagebeltViewer->FindLine(llStartupUrno,ilLine);
							break;
							case TAB_WAITINGROOM :
								blLine = pomWaitingroomViewer->FindLine(llStartupUrno,ilLine);
							break;
							case TAB_CHECKINCOUNTER :
								blLine = pomCheckinCounterViewer->FindLine(llStartupUrno,ilLine);
							break;
							}

							if (blLine && ilLine > -1)
							{
								pomTable->SelectLine(ilLine);
								this->SendMessage(WM_TABLE_LBUTTONDBLCLK,0,0L);
							}
						}

						
					}
				}
				break;
			}
		}
	}

	return 0;
}

void CStammdaten::OnExcel() 
{
	if(!bgIsDialogOpen)
	{
		bgIsDialogOpen = true;
		Beep(440,70);

		char pclConfigPath[256];
		char pclExcelPath[256];
		char pclTrenner[64];
		char pclListSeparator[64];	
		char pclUseAutomation[64];

		if (getenv("CEDA") == NULL)
			strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
		else
			strcpy(pclConfigPath, getenv("CEDA"));

		GetPrivateProfileString(ogAppName, "Excel", "DEFAULT",pclExcelPath, sizeof pclExcelPath, pclConfigPath);


		GetPrivateProfileString(ogAppName, "ExcelSeparator", ";",pclTrenner, sizeof pclTrenner, pclConfigPath);

		GetPrivateProfileString(ogAppName, "ListSeparator", ",",pclListSeparator, sizeof pclListSeparator, pclConfigPath);

		GetPrivateProfileString(ogAppName, "ExcelAutomation", "NO",pclUseAutomation, sizeof pclUseAutomation, pclConfigPath);

		if (stricmp(pclUseAutomation,"YES") != 0  && strcmp(pclExcelPath, "DEFAULT") == 0)
		{
			MessageBox(LoadStg(IDS_STRING992),LoadStg(IDS_STRING145), MB_ICONERROR);
		}

		CWaitCursor olWait;

		EXCELINFO olExcelInfo;
		if (stricmp(pclUseAutomation,"YES") == 0)
		{
			olExcelInfo.bmUseAutomation = true; 
		}
		else
		{
			olExcelInfo.bmUseAutomation = false; 
			olExcelInfo.omExcelPath		= pclExcelPath;
			olExcelInfo.omDelimiter		= pclTrenner;
			olExcelInfo.omListSeparator = pclListSeparator;
		}

		CCSCedaData *polTable = NULL;
		CPtrArray	*polData  = NULL;			
		char		*pomFieldsToIgnore = NULL;
		CStringArray olInfoFields,olInfoDescription,olInfoType;

		switch(igSelectTab)
		{
			case TAB_AIRLINE:
				{
					polTable = &ogALTData;
					polData  = &ogALTData.omData;
					ogALTData.GetDataInfo(olInfoFields,olInfoDescription,olInfoType);
				}
				break;
			case TAB_AIRCRAFT:
				{
					polTable = &ogACTData;
					polData  = &ogACTData.omData;
					ogACTData.GetDataInfo(olInfoFields,olInfoDescription,olInfoType);
				}
				break;
			case TAB_LFZREGISTRATION:
				{
					polTable = &ogACRData;
					polData  = &ogACRData.omData;
					ogACRData.GetDataInfo(olInfoFields,olInfoDescription,olInfoType);
				}
				break;
			case TAB_AIRPORT:
				{
					polTable = &ogAPTData;
					polData  = &ogAPTData.omData;
					ogAPTData.GetDataInfo(olInfoFields,olInfoDescription,olInfoType);

				}
				break;
			case TAB_RUNWAY:
				{
					polTable = &ogRWYData;
					polData  = &ogRWYData.omData;
					ogRWYData.GetDataInfo(olInfoFields,olInfoDescription,olInfoType);

				}
				break;
			case TAB_TAXIWAY:
				{
					polTable = &ogTWYData;
					polData  = &ogTWYData.omData;
					ogTWYData.GetDataInfo(olInfoFields,olInfoDescription,olInfoType);
				}
				break;
			case TAB_POSITION:
				{
					polTable = &ogPSTData;
					polData  = &ogPSTData.omData;
					ogPSTData.GetDataInfo(olInfoFields,olInfoDescription,olInfoType);
				}
				break;
			case TAB_GATE:
				{
					polTable = &ogGATData;
					polData  = &ogGATData.omData;
					ogGATData.GetDataInfo(olInfoFields,olInfoDescription,olInfoType);
				}
				break;
			case TAB_CHECKINCOUNTER:
				{
					polTable = &ogCICData;
					polData  = &ogCICData.omData;
					ogCICData.GetDataInfo(olInfoFields,olInfoDescription,olInfoType);
				}
				break;
			case TAB_BEGGAGEBELT:
				{
					polTable = &ogBLTData;
					polData  = &ogBLTData.omData;
					ogBLTData.GetDataInfo(olInfoFields,olInfoDescription,olInfoType);
				}
				break;
			case TAB_EXIT:
				{
					polTable = &ogEXTData;
					polData  = &ogEXTData.omData;
					ogEXTData.GetDataInfo(olInfoFields,olInfoDescription,olInfoType);
				}
				break;
			case TAB_DELAYCODE:
				{
					polTable = &ogDENData;
					polData  = &ogDENData.omData;
					ogDENData.GetDataInfo(olInfoFields,olInfoDescription,olInfoType);
				}
				break;
			case TAB_TELEXADRESS:
				{
					polTable = &ogMVTData;
					polData  = &ogMVTData.omData;
					ogMVTData.GetDataInfo(olInfoFields,olInfoDescription,olInfoType);
				}
				break;
			case TAB_TRAFFICTYPE:
				{
					polTable = &ogNATData;
					polData  = &ogNATData.omData;
					ogNATData.GetDataInfo(olInfoFields,olInfoDescription,olInfoType);
				}
				break;
			case TAB_HANDLINGAGENT:
				{
					polTable = &ogHAGData;
					polData  = &ogHAGData.omData;
					ogHAGData.GetDataInfo(olInfoFields,olInfoDescription,olInfoType);
				}
				break;
			case TAB_WAITINGROOM:
				{
					polTable = &ogWROData;
					polData  = &ogWROData.omData;
					ogWROData.GetDataInfo(olInfoFields,olInfoDescription,olInfoType);
				}
				break;
			case TAB_HANDLINGTYPE:
				{
					polTable = &ogHTYData;
					polData  = &ogHTYData.omData;
					ogHTYData.GetDataInfo(olInfoFields,olInfoDescription,olInfoType);
				}
				break;
			case TAB_SERVICETYPE:
				{
					polTable = &ogSTYData;
					polData  = &ogSTYData.omData;
					ogSTYData.GetDataInfo(olInfoFields,olInfoDescription,olInfoType);
				}
				break;
			case TAB_FIDSCOMMAND:
				{		
					polTable = &ogFIDData;
					polData  = &ogFIDData.omData;
					ogFIDData.GetDataInfo(olInfoFields,olInfoDescription,olInfoType);
				}
				break;
			case TAB_ERGVERKEHRSARTEN:
				{		
					polTable = &ogSphData;
					polData  = &ogSphData.omData;
					ogSphData.GetDataInfo(olInfoFields,olInfoDescription,olInfoType);
				}
				break;
			case TAB_STANDARDVERKETTUNGEN:
				{		
					polTable = &ogStrData;
					polData  = &ogStrData.omData;
					ogStrData.GetDataInfo(olInfoFields,olInfoDescription,olInfoType);
				}
				break;
			case TAB_FLUGPLANSAISON:
				{		
					polTable = &ogSeaData;
					polData  = &ogSeaData.omData;
					ogSeaData.GetDataInfo(olInfoFields,olInfoDescription,olInfoType);
				}
				break;
			case TAB_LEISTUNGSKATALOG:
				{		
					polTable = &ogGhsData;
					polData  = &ogGhsData.omData;
					pomFieldsToIgnore = "PERM,VRGC";
					ogGhsData.GetDataInfo(olInfoFields,olInfoDescription,olInfoType);


					ogPerData.Read();

					EXCELVALUEINFO olPermInfo;

					olPermInfo.omHeaderText = "Qualifications";

					for (int i = 0; i < ogGhsData.omData.GetSize(); i++)
					{
						CStringArray olPerms;													
						::ExtractItemList(ogGhsData.omData[i].Perm,&olPerms,';');
						CString olValue;
						for (int j = 0; j < olPerms.GetSize();j++)
						{
							PERDATA *polPer = ogPerData.GetPerByUrno(atol(olPerms[j]));
							if (polPer != NULL)
							{
								olValue += polPer->Prmc;
								olValue += '|';
							}
						}

						olPermInfo.omValues.Add(olValue);
					}

					olExcelInfo.omAdditional.New(olPermInfo);
					ogPerData.ClearAll(false);


					EXCELVALUEINFO olVrgcInfo;
					olVrgcInfo.omHeaderText = "Groups";

					ogPfcData.Read();

					for (i = 0; i < ogGhsData.omData.GetSize(); i++)
					{
						CStringArray olVrgc;													
						::ExtractItemList(ogGhsData.omData[i].Vrgc,&olVrgc,';');
						CString olValue;
						for (int j = 0; j < olVrgc.GetSize();j++)
						{
							PFCDATA *polPfc = ogPfcData.GetPfcByUrno(atol(olVrgc[j]));
							if (polPfc != NULL)
							{
								olValue += polPfc->Fctc;
								olValue += '|';
							}
						}

						olVrgcInfo.omValues.Add(olValue);
					}


					olExcelInfo.omAdditional.New(olVrgcInfo);
					ogPfcData.ClearAll(false);

				}
				break;
			case TAB_QUALIFIKATIONEN:
				{		
					polTable = &ogPerData;
					polData  = &ogPerData.omData;
					ogPerData.GetDataInfo(olInfoFields,olInfoDescription,olInfoType);
				}
				break;
			case TAB_ORGANISATIONSEINHEITEN:
				{		
					polTable = &ogOrgData;
					polData  = &ogOrgData.omData;
					ogOrgData.GetDataInfo(olInfoFields,olInfoDescription,olInfoType);
				}
				break;
			case TAB_WEGEZEITEN:
				{		
					polTable = &ogWayData;
					polData  = &ogWayData.omData;
					ogWayData.GetDataInfo(olInfoFields,olInfoDescription,olInfoType);
				}
				break;
			case TAB_FUNKTIONEN:
				{		
					polTable = &ogPfcData;
					polData  = &ogPfcData.omData;
					ogPfcData.GetDataInfo(olInfoFields,olInfoDescription,olInfoType);
				}
				break;
			case TAB_ARBEITSVERTRAGSARTEN:
				{		
					polTable = &ogCotData;
					polData  = &ogCotData.omData;
					ogCotData.GetDataInfo(olInfoFields,olInfoDescription,olInfoType);
				}
				break;
			case TAB_BEWERTUNGSFAKTOREN:
				{		
					polTable = &ogAsfData;
					polData  = &ogAsfData.omData;
					ogAsfData.GetDataInfo(olInfoFields,olInfoDescription,olInfoType);
				}
				break;
			case TAB_BASISSCHICHTEN:
				{		
					polTable = &ogBsdData;
					polData  = &ogBsdData.omData;
					ogBsdData.GetDataInfo(olInfoFields,olInfoDescription,olInfoType);
				}
				break;
			case TAB_DIENSTEUNDABWESENHEITEN:
				{		
					polTable = &ogOdaData;
					polData  = &ogOdaData.omData;
					ogOdaData.GetDataInfo(olInfoFields,olInfoDescription,olInfoType);
				}
				break;
			case TAB_FAHRGEMEINSCHAFTEN:
				{		
					polTable = &ogTeaData;
					polData  = &ogTeaData.omData;
					ogTeaData.GetDataInfo(olInfoFields,olInfoDescription,olInfoType);
				}
				break;
			case TAB_ARBEITSGRUPPEN:
				{		
					polTable = &ogWgpData;
					polData  = &ogWgpData.omData;
					ogWgpData.GetDataInfo(olInfoFields,olInfoDescription,olInfoType);
				}
				break;
			case TAB_MITARBEITERSTAMM:
				{
					polTable = &ogStfData;
					polData  = &ogStfData.omData;
					ogStfData.GetDataInfo(olInfoFields,olInfoDescription,olInfoType);
				}
			break;
			case TAB_ERGABFERTIGUNGSARTEN:
				{		
					polTable = &ogSphData;
					polData  = &ogSphData.omData;
					ogSphData.GetDataInfo(olInfoFields,olInfoDescription,olInfoType);
				}
				break;
			case TAB_REDUKTIONEN:
				{		
					polTable = &ogPrcData;
					polData  = &ogPrcData.omData;
					ogPrcData.GetDataInfo(olInfoFields,olInfoDescription,olInfoType);
				}
				break;
			case TAB_GERAETEGRUPPEN:
				{		
					polTable = &ogGegData;
					polData  = &ogGegData.omData;
					ogGegData.GetDataInfo(olInfoFields,olInfoDescription,olInfoType);
				}
				break;
			case TAB_ORGANIZER:
				{		
					polTable = &ogChtData;
					polData  = &ogChtData.omData;
					ogChtData.GetDataInfo(olInfoFields,olInfoDescription,olInfoType);
				}
				break;
			case TAB_TIMEPARAMETERS:
				{		
					polTable = &ogTipData;
					polData  = &ogTipData.omData;
					ogTipData.GetDataInfo(olInfoFields,olInfoDescription,olInfoType);
				}
				break;
			case TAB_HOLIDAY:
				{		
					polTable = &ogHolData;
					polData  = &ogHolData.omData;
					ogHolData.GetDataInfo(olInfoFields,olInfoDescription,olInfoType);

				}
				break;
			case TAB_PLANNING_GROUPS:
				{		
					polTable = &ogPgpData;
					polData  = &ogPgpData.omData;
					ogPgpData.GetDataInfo(olInfoFields,olInfoDescription,olInfoType);
				}
				break;
			case TAB_AIRPORTWET:
				{		
					polTable = &ogAwiData;
					polData  = &ogAwiData.omData;
					ogAwiData.GetDataInfo(olInfoFields,olInfoDescription,olInfoType);
				}
				break;
			case TAB_COUNTERCLASS:
				{		
					polTable = &ogCccData;
					polData  = &ogCccData.omData;
					ogCccData.GetDataInfo(olInfoFields,olInfoDescription,olInfoType);
				}
				break;
			case TAB_VERYIMPPERS:
				{		
					polTable = &ogVipData;
					polData  = &ogVipData.omData;
					ogVipData.GetDataInfo(olInfoFields,olInfoDescription,olInfoType);
				}
				break;
			case TAB_AIRCRAFTFAM:
				{		
					polTable = &ogAFMData;
					polData  = &ogAFMData.omData;
					ogAFMData.GetDataInfo(olInfoFields,olInfoDescription,olInfoType);
				}
				break;
			case TAB_ENGINETYPE:
				{		
					polTable = &ogENTData;
					polData  = &ogENTData.omData;
					ogENTData.GetDataInfo(olInfoFields,olInfoDescription,olInfoType);

				}
				break;
			default:
				{
					// Do nothing
					bgIsDialogOpen = false;
					return;
				}
				break; 
		}


		if (polTable != NULL)
		{
			olExcelInfo.omTable.omName = polTable->GetTableName();

			CString olFieldList = polTable->GetFieldList();

			CStringArray olFields;
			::ExtractItemList(olFieldList,&olFields,',');
			for (int i = 0; i < olFields.GetSize(); i++)
			{
				if (pomFieldsToIgnore != NULL && strstr(pomFieldsToIgnore,olFields[i]) != NULL) 
					continue;

				if (olFields[i] != "URNO")
				{
					EXCELFIELDINFO olFieldInfo;
					olFieldInfo.omName		= olFields[i]; 
					olFieldInfo.omHeaderText= olFields[i];
					for (int j = 0; j < olInfoFields.GetSize();j++)
					{
						if (olInfoFields[j] == olFields[i])
						{
							olFieldInfo.omHeaderText= olInfoDescription[j];
							break;
						}
					}
					olExcelInfo.omTable.omFields.New(olFieldInfo);
				}
			}

			olExcelInfo.omTable.omData.Append(*polData);
			polTable->GenerateExcelOutput(olExcelInfo);

		}

		bgIsDialogOpen = false;
	}	
}
