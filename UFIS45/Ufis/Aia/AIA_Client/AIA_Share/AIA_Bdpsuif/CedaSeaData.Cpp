// CedaSeaData.cpp
 
#include "stdafx.h"
#include "CedaSeaData.h"
#include "resource.h"


void ProcessSeaCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

//--CEDADATA-----------------------------------------------------------------------------------------------

CedaSeaData::CedaSeaData() : CCSCedaData(&ogCommHandler)
{
    BEGIN_CEDARECINFO(SEADATA, SeaDataRecInfo)
		FIELD_CHAR_TRIM	(Beme,"BEME")
		FIELD_DATE		(Cdat,"CDAT")
		FIELD_DATE		(Lstu,"LSTU")
		FIELD_CHAR_TRIM	(Prfl,"PRFL")
		FIELD_CHAR_TRIM	(Seas,"SEAS")
		FIELD_LONG		(Urno,"URNO")
		FIELD_CHAR_TRIM	(Usec,"USEC")
		FIELD_CHAR_TRIM	(Useu,"USEU")
		FIELD_DATE		(Vpfr,"VPFR")
		FIELD_DATE		(Vpto,"VPTO")
    END_CEDARECINFO

    // Copy the record structure
    for (int i = 0; i < sizeof(SeaDataRecInfo)/sizeof(SeaDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&SeaDataRecInfo[i],sizeof(CEDARECINFO));
        omRecInfo.Add(prpCedaRecInfo);
	}
    // Initialize table names and field names
    strcpy(pcmTableName,"SEA");
    sprintf(pcmListOfFields,"BEME,CDAT,LSTU,PRFL,SEAS,URNO,USEC,USEU,VPFR,VPTO");

	pcmFieldList = pcmListOfFields; //because in CedaData pcmFieldList is just a pointer

	omData.RemoveAll();
}

//----------------------------------------------------------------------------------------------------

void CedaSeaData::GetDataInfo(CStringArray &ropFields, CStringArray &ropDesription, CStringArray &ropType)
{
	ropFields.Add("BEME");
	ropFields.Add("CDAT");
	ropFields.Add("LSTU");
	ropFields.Add("PRFL");
	ropFields.Add("SEAS");
	ropFields.Add("URNO");
	ropFields.Add("USEC");
	ropFields.Add("USEU");
	ropFields.Add("VPFR");
	ropFields.Add("VPTO");

	ropDesription.Add(LoadStg(IDS_STRING238));
	ropDesription.Add(LoadStg(IDS_STRING343));
	ropDesription.Add(LoadStg(IDS_STRING344));
	ropDesription.Add(LoadStg(IDS_STRING345));
	ropDesription.Add(LoadStg(IDS_STRING480));
	ropDesription.Add(LoadStg(IDS_STRING346));
	ropDesription.Add(LoadStg(IDS_STRING347));
	ropDesription.Add(LoadStg(IDS_STRING348));
	ropDesription.Add(LoadStg(IDS_STRING230));
	ropDesription.Add(LoadStg(IDS_STRING231));

	ropType.Add("String");
	ropType.Add("Date");
	ropType.Add("Date");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("Date");
	ropType.Add("Date");

}

//--REGISTER----------------------------------------------------------------------------------------------

void CedaSeaData::Register(void)
{
	ogDdx.Register((void *)this,BC_SEA_CHANGE,	CString("SEADATA"), CString("Sea-changed"),	ProcessSeaCf);
	ogDdx.Register((void *)this,BC_SEA_NEW,		CString("SEADATA"), CString("Sea-new"),		ProcessSeaCf);
	ogDdx.Register((void *)this,BC_SEA_DELETE,	CString("SEADATA"), CString("Sea-deleted"),	ProcessSeaCf);
}

//--~CEDADATA----------------------------------------------------------------------------------------------

CedaSeaData::~CedaSeaData(void)
{
	omRecInfo.DeleteAll();
	ClearAll();
}

//--CLEAR-ALL----------------------------------------------------------------------------------------------

void CedaSeaData::ClearAll(bool bpWithRegistration)
{
    omUrnoMap.RemoveAll();
    omData.DeleteAll();
	if(bpWithRegistration)
	{
		ogDdx.UnRegister(this,NOTUSED);
	}
}

//--READ-ALL-----------------------------------------------------------------------------------------------

bool CedaSeaData::Read(char *pspWhere /*NULL*/)
{
    // Select data from the database
	bool ilRc = true;
    omUrnoMap.RemoveAll();
    omData.DeleteAll();
	if(pspWhere == NULL)
	{	
		ilRc = CedaAction("RT");
	}
	else
	{
		ilRc = CedaAction("RT", pspWhere);
	}
	if (ilRc != true)
	{
		return false;
	}
	// Load data from CedaData into the dynamic array of record
	for (int ilLc = 0; ilRc == true; ilLc++)
	{
		SEADATA *prlSea = new SEADATA;
		if ((ilRc = GetFirstBufferRecord(prlSea)) == true)
		{
			omData.Add(prlSea);//Update omData
			omUrnoMap.SetAt((void *)prlSea->Urno,prlSea);
		}
		else
		{
			delete prlSea;
		}
	}
    return true;

}

//--INSERT-------------------------------------------------------------------------------------------------

bool CedaSeaData::Insert(SEADATA *prpSea)
{
	prpSea->IsChanged = DATA_NEW;
	if(Save(prpSea) == false) return false; //Update Database
	InsertInternal(prpSea);
    return true;
}

//--INSERT-INTERNAL--------------------------------------------------------------------------------------

bool CedaSeaData::InsertInternal(SEADATA *prpSea)
{
	ogDdx.DataChanged((void *)this, SEA_NEW,(void *)prpSea ); //Update Viewer
	omData.Add(prpSea);//Update omData
	omUrnoMap.SetAt((void *)prpSea->Urno,prpSea);
    return true;
}

//--DELETE-------------------------------------------------------------------------------------------------

bool CedaSeaData::Delete(long lpUrno)
{
	SEADATA *prlSea = GetSeaByUrno(lpUrno);
	if (prlSea != NULL)
	{
		prlSea->IsChanged = DATA_DELETED;
		if(Save(prlSea) == false) return false; //Update Database
		DeleteInternal(prlSea);
	}
    return true;
}

//--DELETE-INTERNAL----------------------------------------------------------------------------------------

bool CedaSeaData::DeleteInternal(SEADATA *prpSea)
{
	ogDdx.DataChanged((void *)this,SEA_DELETE,(void *)prpSea); //Update Viewer
	omUrnoMap.RemoveKey((void *)prpSea->Urno);
	int ilSeaCount = omData.GetSize();
	for (int ilLc = 0; ilLc < ilSeaCount; ilLc++)
	{
		if (omData[ilLc].Urno == prpSea->Urno)
		{
			omData.DeleteAt(ilLc);//Update omData
			break;
		}
	}
    return true;
}

//--UPDATE-------------------------------------------------------------------------------------------------

bool CedaSeaData::Update(SEADATA *prpSea)
{
	if (GetSeaByUrno(prpSea->Urno) != NULL)
	{
		if (prpSea->IsChanged == DATA_UNCHANGED)
		{
			prpSea->IsChanged = DATA_CHANGED;
		}
		if(Save(prpSea) == false) return false; //Update Database
		UpdateInternal(prpSea);
	}
    return true;
}

//--UPDATE-INTERNAL--------------------------------------------------------------------------------------------

bool CedaSeaData::UpdateInternal(SEADATA *prpSea)
{
	SEADATA *prlSea = GetSeaByUrno(prpSea->Urno);
	if (prlSea != NULL)
	{
		*prlSea = *prpSea; //Update omData
		ogDdx.DataChanged((void *)this,SEA_CHANGE,(void *)prlSea); //Update Viewer
	}
    return true;
}

//--GET-BY-URNO--------------------------------------------------------------------------------------------

SEADATA *CedaSeaData::GetSeaByUrno(long lpUrno)
{
	SEADATA  *prlSea;
	if (omUrnoMap.Lookup((void *)lpUrno,(void *& )prlSea) == TRUE)
	{
		return prlSea;
	}
	return NULL;
}

//--READSPECIALDATA-------------------------------------------------------------------------------------

bool CedaSeaData::ReadSpecialData(CCSPtrArray<SEADATA> *popSea,char *pspWhere,char *pspFieldList,bool ipSYS/*=true*/)
{
	bool ilRc = true;
	char pclFieldList[256] = " ";

	if(strlen(pspFieldList) > 0) 
	{
		strcpy(pclFieldList, pspFieldList);
	}
	if(ipSYS == true) 
	{
		if (CedaAction("SYS","SEA",pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	else
	{
		if (CedaAction("RT","SEA",pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	if(popSea != NULL)
	{
		for (int ilLc = 0; ilRc == true; ilLc++)
		{
			SEADATA *prpSea = new SEADATA;
			if ((ilRc = GetBufferRecord(ilLc,prpSea,CString(pclFieldList))) == true)
			{
				popSea->Add(prpSea);
			}
			else
			{
				delete prpSea;
			}
		}
		if(popSea->GetSize() == 0) return false;
	}
    return true;
}

//--SAVE---------------------------------------------------------------------------------------------------

bool CedaSeaData::Save(SEADATA *prpSea)
{
	bool ilRc = true;
	CString olListOfData;
	char pclSelection[124];
	char pclData[524];

	if (prpSea->IsChanged == DATA_UNCHANGED)
	{
		return true; // no change, nothing to do
	}
	switch(prpSea->IsChanged)
	{
	case DATA_NEW:
		MakeCedaData(&omRecInfo,olListOfData,prpSea);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("IRT","","",pclData);
		prpSea->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_CHANGED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpSea->Urno);
		MakeCedaData(&omRecInfo,olListOfData,prpSea);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("URT",pclSelection,"",pclData);
		prpSea->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_DELETED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpSea->Urno);
		ilRc = CedaAction("DRT",pclSelection);
		break;
	}
    return ilRc;
}

//--PROCESS-CF---------------------------------------------------------------------------------------------

void  ProcessSeaCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	ogSeaData.ProcessBc(ipDDXType,vpDataPointer,ropInstanceName);
}

//--PROCESS-BC---------------------------------------------------------------------------------------------

void  CedaSeaData::ProcessBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	struct BcStruct *prlSeaData;
	prlSeaData = (struct BcStruct *) vpDataPointer;
	SEADATA *prlSea;
	if(ipDDXType == BC_SEA_NEW)
	{
		prlSea = new SEADATA;
		GetRecordFromItemList(prlSea,prlSeaData->Fields,prlSeaData->Data);
		InsertInternal(prlSea);
	}
	if(ipDDXType == BC_SEA_CHANGE)
	{
		long llUrno = GetUrnoFromSelection(prlSeaData->Selection);
		prlSea = GetSeaByUrno(llUrno);
		if(prlSea != NULL)
		{
			GetRecordFromItemList(prlSea,prlSeaData->Fields,prlSeaData->Data);
			UpdateInternal(prlSea);
		}
	}
	if(ipDDXType == BC_SEA_DELETE)
	{
		long llUrno;
		CString olSelection = (CString)prlSeaData->Selection;
		if (olSelection.Find('\'') != -1)
		{
			llUrno = GetUrnoFromSelection(prlSeaData->Selection);
		}
		else
		{
			int ilFirst = olSelection.Find("=")+2;
			int ilLast  = olSelection.GetLength();
			llUrno = atol(olSelection.Mid(ilFirst,ilLast-ilFirst));
		}
		prlSea = GetSeaByUrno(llUrno);
		if (prlSea != NULL)
		{
			DeleteInternal(prlSea);
		}
	}
}

//---------------------------------------------------------------------------------------------------------
