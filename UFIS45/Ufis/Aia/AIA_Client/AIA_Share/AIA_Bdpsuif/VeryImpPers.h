#if !defined(AFX_VERYIMPPERS_H__6FB70432_EE67_11D2_AD3D_004095436A98__INCLUDED_)
#define AFX_VERYIMPPERS_H__6FB70432_EE67_11D2_AD3D_004095436A98__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// VeryImpPers.h : header file
//
#include "PrivList.h"
#include "CCSEdit.h"
#include "CCSComboBox.h"
#include "CedaVipData.h"

/////////////////////////////////////////////////////////////////////////////
// CVeryImpPers dialog

class CVeryImpPers : public CDialog
{
// Construction
public:
	CVeryImpPers(VIPDATA *prpVip,CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CVeryImpPers)
	enum { IDD = IDD_VIPDLG };
	CButton	m_OK;
	CCSEdit	m_CdatD;
	CCSEdit	m_CdatT;
	CCSEdit	m_LstuD;
	CCSEdit	m_LstuT;
	CCSEdit	m_NoGr;
	CCSEdit	m_NoPx;
	CCSEdit	m_Paxn;
	CCSEdit	m_Paxr;
	CCSEdit	m_Usec;
	CCSEdit	m_Useu;
	CString m_Caption;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CVeryImpPers)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CVeryImpPers)
	virtual void OnCancel();
	virtual void OnOK();
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
private:
		VIPDATA *pomVip;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_VERYIMPPERS_H__6FB70432_EE67_11D2_AD3D_004095436A98__INCLUDED_)
