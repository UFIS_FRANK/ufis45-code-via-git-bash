#if !defined(AFX_HANDLINGTYPEDLG_H__81720C71_333C_11D1_B39D_0000C016B067__INCLUDED_)
#define AFX_HANDLINGTYPEDLG_H__81720C71_333C_11D1_B39D_0000C016B067__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// HandlingtypeDlg.h : header file
//
#include "CedaHTYData.h"
#include "CCSEdit.h"

/////////////////////////////////////////////////////////////////////////////
// HandlingtypeDlg dialog

class HandlingtypeDlg : public CDialog
{
// Construction
public:
	HandlingtypeDlg(HTYDATA *popHTY,CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(HandlingtypeDlg)
	enum { IDD = IDD_HANDLINGTYPEDLG };
	CButton	m_OK;
	CString	m_Caption;
	CCSEdit	m_CDATD;
	CCSEdit	m_CDATT;
	CCSEdit	m_LSTUD;
	CCSEdit	m_LSTUT;
	CCSEdit	m_USEU;
	CCSEdit	m_USEC;
	CCSEdit	m_HTYP;
	CCSEdit	m_HNAM;
	CCSEdit	m_VAFRD;
	CCSEdit	m_VAFRT;
	CCSEdit	m_VATOD;
	CCSEdit	m_VATOT;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(HandlingtypeDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(HandlingtypeDlg)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

private:

	HTYDATA *pomHTY;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_HANDLINGTYPEDLG_H__81720C71_333C_11D1_B39D_0000C016B067__INCLUDED_)
