// ServicetypeDlg.cpp : implementation file
//

#include "stdafx.h"
#include "BDPSUIF.h"
#include "ServicetypeDlg.h"
#include "PrivList.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// ServicetypeDlg dialog


ServicetypeDlg::ServicetypeDlg(STYDATA *popSTY,CWnd* pParent /*=NULL*/) : CDialog(ServicetypeDlg::IDD, pParent)
{
	pomSTY = popSTY;

	//{{AFX_DATA_INIT(ServicetypeDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void ServicetypeDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(ServicetypeDlg)
	DDX_Control(pDX, IDOK, m_OK);
	DDX_Control(pDX, IDC_CDAT_D, m_CDATD);
	DDX_Control(pDX, IDC_CDAT_T, m_CDATT);
	DDX_Control(pDX, IDC_LSTU_D, m_LSTUD);
	DDX_Control(pDX, IDC_LSTU_T, m_LSTUT);
	DDX_Control(pDX, IDC_USEU,	 m_USEU);
	DDX_Control(pDX, IDC_USEC,	 m_USEC);
	DDX_Control(pDX, IDC_STYP,   m_STYP);
	DDX_Control(pDX, IDC_SNAM,   m_SNAM);
	DDX_Control(pDX, IDC_VAFR_D, m_VAFRD);
	DDX_Control(pDX, IDC_VAFR_T, m_VAFRT);
	DDX_Control(pDX, IDC_VATO_D, m_VATOD);
	DDX_Control(pDX, IDC_VATO_T, m_VATOT);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(ServicetypeDlg, CDialog)
	//{{AFX_MSG_MAP(ServicetypeDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// ServicetypeDlg message handlers
//----------------------------------------------------------------------------------------

BOOL ServicetypeDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	m_Caption = LoadStg(IDS_STRING188) + " <" + m_Caption + ">";
	SetWindowText(m_Caption);

	char clStat = ogPrivList.GetStat("SERVICETYPEDLG.m_OK");
	SetWndStatAll(clStat,m_OK);

	//------------------------------------
	m_CDATD.SetBKColor(SILVER);
	m_CDATD.SetInitText(pomSTY->Cdat.Format("%d.%m.%Y"));
	m_CDATD.SetSecState(ogPrivList.GetStat("SERVICETYPEDLG.m_CDAT"));
	m_CDATT.SetBKColor(SILVER);
	m_CDATT.SetInitText(pomSTY->Cdat.Format("%H:%M"));
	m_CDATT.SetSecState(ogPrivList.GetStat("SERVICETYPEDLG.m_CDAT"));
	//------------------------------------
	m_LSTUD.SetBKColor(SILVER);
	m_LSTUD.SetInitText(pomSTY->Lstu.Format("%d.%m.%Y"));
	m_LSTUD.SetSecState(ogPrivList.GetStat("SERVICETYPEDLG.m_LSTU"));
	m_LSTUT.SetBKColor(SILVER);
	m_LSTUT.SetInitText(pomSTY->Lstu.Format("%H:%M"));
	m_LSTUT.SetSecState(ogPrivList.GetStat("SERVICETYPEDLG.m_LSTU"));
	//------------------------------------
	m_USEC.SetBKColor(SILVER);
	m_USEC.SetInitText(pomSTY->Usec);
	m_USEC.SetSecState(ogPrivList.GetStat("SERVICETYPEDLG.m_USEC"));
	//------------------------------------
	m_USEU.SetBKColor(SILVER);
	m_USEU.SetInitText(pomSTY->Useu);
	m_USEU.SetSecState(ogPrivList.GetStat("SERVICETYPEDLG.m_USEU"));
	//------------------------------------//WHITE,YELLOW,SILVER,RED,LYELLOW
	m_STYP.SetFormat("x|#x|#");
	m_STYP.SetTextLimit(1,2);
	m_STYP.SetBKColor(YELLOW);  
	m_STYP.SetTextErrColor(RED);
	m_STYP.SetInitText(pomSTY->Styp);
	m_STYP.SetSecState(ogPrivList.GetStat("SERVICETYPEDLG.m_STYP"));
	//------------------------------------
	m_SNAM.SetTypeToString("X(30)",30,1);
	m_SNAM.SetBKColor(YELLOW);
	m_SNAM.SetTextErrColor(RED);
	m_SNAM.SetInitText(pomSTY->Snam);
	m_SNAM.SetSecState(ogPrivList.GetStat("SERVICETYPEDLG.m_SNAM"));
	//------------------------------------
	m_VAFRD.SetTypeToDate(true);
	m_VAFRD.SetTextErrColor(RED);
	m_VAFRD.SetBKColor(YELLOW);
	m_VAFRD.SetInitText(pomSTY->Vafr.Format("%d.%m.%Y"));
	m_VAFRD.SetSecState(ogPrivList.GetStat("SERVICETYPEDLG.m_VAFR"));
	// - - - - - - - - - - - - - - - - - -
	m_VAFRT.SetTypeToTime(true);
	m_VAFRT.SetTextErrColor(RED);
	m_VAFRT.SetBKColor(YELLOW);
	m_VAFRT.SetInitText(pomSTY->Vafr.Format("%H:%M"));
	m_VAFRT.SetSecState(ogPrivList.GetStat("SERVICETYPEDLG.m_VAFR"));
	//------------------------------------
	m_VATOD.SetTypeToDate();
	m_VATOD.SetTextErrColor(RED);
	m_VATOD.SetInitText(pomSTY->Vato.Format("%d.%m.%Y"));
	m_VATOD.SetSecState(ogPrivList.GetStat("SERVICETYPEDLG.m_VATO"));
	// - - - - - - - - - - - - - - - - - -
	m_VATOT.SetTypeToTime();
	m_VATOT.SetTextErrColor(RED);
	m_VATOT.SetInitText(pomSTY->Vato.Format("%H:%M"));
	m_VATOT.SetSecState(ogPrivList.GetStat("SERVICETYPEDLG.m_VATO"));
	//------------------------------------
	return TRUE;  
}

//----------------------------------------------------------------------------------------

void ServicetypeDlg::OnOK() 
{
	AfxGetApp()->DoWaitCursor(1);
	CString olErrorText;
	bool ilStatus = true;
	if(m_STYP.GetStatus() == false)
	{
		ilStatus = false;
		if(m_STYP.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING237) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING237) + ogNotFormat;
		}
	}
	if(m_SNAM.GetStatus() == false)
	{
		ilStatus = false;
		if(m_SNAM.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING288) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING288) + ogNotFormat;
		}
	}
	if(m_VAFRD.GetStatus() == false)
	{
		ilStatus = false;
		if(m_VAFRD.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING230) + LoadStg(IDS_STRING232) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING230) + LoadStg(IDS_STRING232) + ogNotFormat;
		}
	}
	if(m_VAFRT.GetStatus() == false)
	{
		ilStatus = false;
		if(m_VAFRT.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING230) + LoadStg(IDS_STRING233) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING230) + LoadStg(IDS_STRING233) + ogNotFormat;
		}
	}
	if(m_VATOD.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING231) + LoadStg(IDS_STRING232) + ogNotFormat;
	}
	if(m_VATOT.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING231) + LoadStg(IDS_STRING233) + ogNotFormat;
	}

	///////////////////////////////////////////////////////////////////////////
	CString olVafrd,olVafrt,olVatod,olVatot;
	CTime olTmpTimeFr,olTmpTimeTo;
	m_VAFRD.GetWindowText(olVafrd);
	m_VAFRT.GetWindowText(olVafrt);
	m_VATOD.GetWindowText(olVatod);
	m_VATOT.GetWindowText(olVatot);

	if((m_VAFRD.GetWindowTextLength() != 0 && m_VAFRT.GetWindowTextLength() == 0) || (m_VAFRD.GetWindowTextLength() == 0 && m_VAFRT.GetWindowTextLength() != 0))
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING234);
	}
	if((m_VATOD.GetWindowTextLength() != 0 && m_VATOT.GetWindowTextLength() == 0) || (m_VATOD.GetWindowTextLength() == 0 && m_VATOT.GetWindowTextLength() != 0))
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING235);
	}
	if(m_VAFRD.GetStatus() == true && m_VAFRT.GetStatus() == true && m_VATOD.GetStatus() == true && m_VATOT.GetStatus() == true)
	{
		olTmpTimeFr = DateHourStringToDate(olVafrd,olVafrt);
		olTmpTimeTo = DateHourStringToDate(olVatod,olVatot);
		if(olTmpTimeTo != -1 && (olTmpTimeTo < olTmpTimeFr || olTmpTimeFr == -1))
		{
			ilStatus = false;
			olErrorText += LoadStg(IDS_STRING236);
		}
	}

	//Konsistenz-Check//
	if(ilStatus == true)
	{
		CString olStyp;
		char clWhere[100];
		CCSPtrArray<STYDATA> olStyCPA;

		m_STYP.GetWindowText(olStyp);
		sprintf(clWhere,"WHERE STYP='%s'",olStyp);
		if(ogSTYData.ReadSpecialData(&olStyCPA,clWhere,"URNO,STYP",false) == true)
		{
			if(olStyCPA[0].Urno != pomSTY->Urno)
			{
				ilStatus = false;
				olErrorText += LoadStg(IDS_STRING237) + LoadStg(IDS_EXIST);
			}
		}
		olStyCPA.DeleteAll();
	}

	///////////////////////////////////////////////////////////////////////////
	AfxGetApp()->DoWaitCursor(-1);
	if (ilStatus == true)
	{
		m_STYP.GetWindowText(pomSTY->Styp,3);
		m_SNAM.GetWindowText(pomSTY->Snam,31);
		pomSTY->Vafr = DateHourStringToDate(olVafrd,olVafrt);
		pomSTY->Vato = DateHourStringToDate(olVatod,olVatot);

		CDialog::OnOK();
	}
	else
	{
		Beep(440,70);
		MessageBox(olErrorText,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
		m_STYP.SetFocus();
		m_STYP.SetSel(0,-1);
	}
}

//----------------------------------------------------------------------------------------

