// MitarbeiterstammTableViewer.cpp 
//

#include "stdafx.h"
#include "MitarbeiterstammTableViewer.h"
#include "CcsDdx.h"
#include "resource.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif


// Local function prototype
static void MitarbeiterstammTableCf(void *popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

//-----------------------------------------------------------------------------------------------
// MitarbeiterstammTableViewer
//

MitarbeiterstammTableViewer::MitarbeiterstammTableViewer(CCSPtrArray<STFDATA> *popData)
{
	pomData = popData;

	bmIsFromSearch = FALSE;
    pomMitarbeiterstammTable = NULL;
    ogDdx.Register(this, STF_CHANGE, CString("MitarbeiterstammTableViewer"), CString("Mitarbeiterstamm Update"), MitarbeiterstammTableCf);
    ogDdx.Register(this, STF_NEW,    CString("MitarbeiterstammTableViewer"), CString("Mitarbeiterstamm New"),    MitarbeiterstammTableCf);
    ogDdx.Register(this, STF_DELETE, CString("MitarbeiterstammTableViewer"), CString("Mitarbeiterstamm Delete"), MitarbeiterstammTableCf);
}

//-----------------------------------------------------------------------------------------------

MitarbeiterstammTableViewer::~MitarbeiterstammTableViewer()
{
	ogDdx.UnRegister(this, NOTUSED);
    DeleteAll();
	omHeaderDataArray.DeleteAll();
}

//-----------------------------------------------------------------------------------------------

void MitarbeiterstammTableViewer::Attach(CCSTable *popTable)
{
    pomMitarbeiterstammTable = popTable;
}

//-----------------------------------------------------------------------------------------------

void MitarbeiterstammTableViewer::ChangeViewTo(const char *pcpViewName)
{
    DeleteAll();    
    MakeLines();
	UpdateDisplay();
}

//-----------------------------------------------------------------------------------------------

void MitarbeiterstammTableViewer::MakeLines()
{
	int ilMitarbeiterstammCount = pomData->GetSize();
	for (int ilLc = 0; ilLc < ilMitarbeiterstammCount; ilLc++)
	{
		STFDATA *prlMitarbeiterstammData = &pomData->GetAt(ilLc);
		MakeLine(prlMitarbeiterstammData);
	}
}

//-----------------------------------------------------------------------------------------------

void MitarbeiterstammTableViewer::MakeLine(STFDATA *prpMitarbeiterstamm)
{

    //if( !IsPassFilter(prpMitarbeiterstamm)) return;

    // Update viewer data for this shift record
    MITARBEITERSTAMMTABLE_LINEDATA rlMitarbeiterstamm;

	rlMitarbeiterstamm.Urno = prpMitarbeiterstamm->Urno;	// Eindeutige Datensatz-Nr.
	rlMitarbeiterstamm.Lanm = prpMitarbeiterstamm->Lanm; 	// Name
	rlMitarbeiterstamm.Finm = prpMitarbeiterstamm->Finm; 	// Vorname
	rlMitarbeiterstamm.Shnm = prpMitarbeiterstamm->Shnm; 	// Kurzname
	rlMitarbeiterstamm.Perc = prpMitarbeiterstamm->Perc; 	// Kürzel
	rlMitarbeiterstamm.Peno = prpMitarbeiterstamm->Peno; 	// Personalnummer
	//*** 04.10.99 SHA ***
	//rlMitarbeiterstamm.Makr = prpMitarbeiterstamm->Makr; 	// Mitarbeiterkreis
	//rlMitarbeiterstamm.Sken = prpMitarbeiterstamm->Sken; 	// Schichtkennzeichen
	rlMitarbeiterstamm.Teld = prpMitarbeiterstamm->Teld; 	// Telefonnr. dienstlich
	rlMitarbeiterstamm.Telh = prpMitarbeiterstamm->Telh; 	// Telefonnr. Handy
	rlMitarbeiterstamm.Telp = prpMitarbeiterstamm->Telp; 	// Telefonnr. privat
	rlMitarbeiterstamm.Doem = prpMitarbeiterstamm->Doem;	// Eintrittsdatum
	rlMitarbeiterstamm.Dodm = prpMitarbeiterstamm->Dodm;	// Austrittsdatum
	rlMitarbeiterstamm.Ktou = prpMitarbeiterstamm->Ktou; 	// Urlaubsansprüche
	rlMitarbeiterstamm.Zutf = prpMitarbeiterstamm->Zutf; 	// Zusätzliche Urlaubstage Freistellungstage
	rlMitarbeiterstamm.Zutb = prpMitarbeiterstamm->Zutb; 	// Zusätzliche Urlaubstage Schwerbehinderung
	rlMitarbeiterstamm.Zutw = prpMitarbeiterstamm->Zutw; 	// Zusätzliche Urlaubstage Wechselschicht
	rlMitarbeiterstamm.Zutn = prpMitarbeiterstamm->Zutn; 	// Zusätzliche Urlaubstage Nachtschicht
	rlMitarbeiterstamm.Tohr = prpMitarbeiterstamm->Tohr; 	// Soll an SAP-HR übermittelt werden
	rlMitarbeiterstamm.Rema = prpMitarbeiterstamm->Rema; 	// Bemerkungen
	rlMitarbeiterstamm.Lino = prpMitarbeiterstamm->Lino; 	// Name
	rlMitarbeiterstamm.Gsmn = prpMitarbeiterstamm->Gsmn; 	// Vorname
	rlMitarbeiterstamm.Matr = prpMitarbeiterstamm->Matr; 	// Kurzname

	CreateLine(&rlMitarbeiterstamm);
}

//-----------------------------------------------------------------------------------------------

void MitarbeiterstammTableViewer::CreateLine(MITARBEITERSTAMMTABLE_LINEDATA *prpMitarbeiterstamm)
{
    int ilLineCount = omLines.GetSize();
    for (int ilLineno = ilLineCount; ilLineno > 0; ilLineno--)
        if (CompareMitarbeiterstamm(prpMitarbeiterstamm, &omLines[ilLineno-1]) >= 0)
	        break;							 // should be inserted after Lines[ilLineno-1]
	MITARBEITERSTAMMTABLE_LINEDATA rlMitarbeiterstamm;
	rlMitarbeiterstamm = *prpMitarbeiterstamm;
    omLines.NewAt(ilLineno, rlMitarbeiterstamm);
}

//-----------------------------------------------------------------------------------------------
// UpdateDisplay: Load data selected by filter conditions to the display by using "omTable"

void MitarbeiterstammTableViewer::UpdateDisplay()
{
	int ilLines = omLines.GetSize();
	const int ilColumns = 18;	//<===== Spaltenanzahl setzen (von 0 ab)
	omHeaderDataArray.DeleteAll();

	pomMitarbeiterstammTable->SetShowSelection(TRUE);
	pomMitarbeiterstammTable->ResetContent();

	TABLE_HEADER_COLUMN rlHeader;
	rlHeader.Alignment = COLALIGN_LEFT;
	rlHeader.Font = &ogCourier_Regular_10;

	int ilPos = 1;
	rlHeader.Length = 120; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING274),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 120; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING274),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 100; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING274),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 30; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING274),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 150; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING274),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	//*** 04.10.99 SHA ***
	ilPos++;
	//rlHeader.Length = 80; 
	//rlHeader.Text = GetListItem(LoadStg(IDS_STRING274),ilPos++,true,'|');
	//omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 80; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING274),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 120; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING274),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 120; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING274),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 120; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING274),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 100; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING274),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 100; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING274),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 50; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING274),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 50; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING274),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 50; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING274),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 50; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING274),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 50; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING274),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 30; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING274),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 300; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING274),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);

	pomMitarbeiterstammTable->SetHeaderFields(omHeaderDataArray);
	pomMitarbeiterstammTable->SetDefaultSeparator();
	pomMitarbeiterstammTable->SetTableEditable(FALSE);

	TABLE_COLUMN rlColumnData;
	rlColumnData.Font = &ogCourier_Regular_10;
	rlColumnData.BkColor = WHITE;
	rlColumnData.Alignment = COLALIGN_LEFT;
	
    for (int ilLineNo = 0; ilLineNo < ilLines; ilLineNo++)
	{
		CCSPtrArray<TABLE_COLUMN> olColList;
		rlColumnData.Text = omLines[ilLineNo].Lanm;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Finm;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Shnm;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Perc;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Peno;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		//*** 04.10.99 SHA ***
		//rlColumnData.Text = omLines[ilLineNo].Makr;
		//olColList.NewAt(olColList.GetSize(), rlColumnData);
		//rlColumnData.Text = omLines[ilLineNo].Sken;
		rlColumnData.Text = omLines[ilLineNo].Lino;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Teld;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Telh;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Telp;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Doem.Format("%d.%m.%Y");
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Dodm.Format("%d.%m.%Y");
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Ktou;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Zutf;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Zutb;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Zutw;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Zutn;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Tohr;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Rema;//Gsmn;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Lino;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Matr;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Rema;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		pomMitarbeiterstammTable->AddTextLine(olColList,(void*)NULL);
		olColList.DeleteAll();
	}
	pomMitarbeiterstammTable->DisplayTable();
}

//-----------------------------------------------------------------------------------------------

CString MitarbeiterstammTableViewer::Format(MITARBEITERSTAMMTABLE_LINEDATA *prpLine)
{
    CString s;
	// ???
    return s;
}

//-----------------------------------------------------------------------------------------------

bool MitarbeiterstammTableViewer::FindMitarbeiterstamm(char *pcpMitarbeiterstammKeya, char *pcpMitarbeiterstammKeyd, int& ilItem)
{
	int ilCount = omLines.GetSize();
    for (ilItem = 0; ilItem < ilCount; ilItem++)
    {
/*		if ( (omLines[ilItem].Keya == pcpMitarbeiterstammKeya) &&
			 (omLines[ilItem].Keyd == pcpMitarbeiterstammKeyd)    )
			return TRUE;*/
	}
	return false;
}

//-----------------------------------------------------------------------------------------------

static void MitarbeiterstammTableCf(void *popInstance, int ipDDXType,
    void *vpDataPointer, CString &ropInstanceName)
{
    MitarbeiterstammTableViewer *polViewer = (MitarbeiterstammTableViewer *)popInstance;
    if (ipDDXType == STF_CHANGE || ipDDXType == STF_NEW) polViewer->ProcessMitarbeiterstammChange((STFDATA *)vpDataPointer);
    if (ipDDXType == STF_DELETE) polViewer->ProcessMitarbeiterstammDelete((STFDATA *)vpDataPointer);
} 


//-----------------------------------------------------------------------------------------------

void MitarbeiterstammTableViewer::ProcessMitarbeiterstammChange(STFDATA *prpMitarbeiterstamm)
{
	int ilItem = 0;
	int ilColumnNo = 0;
	TABLE_COLUMN rlColumn;
	CCSPtrArray <TABLE_COLUMN> olLine;

	rlColumn.Lineno = ilItem;
	rlColumn.VerticalSeparator = SEPA_NONE;
	rlColumn.SeparatorType = SEPA_NONE;
	rlColumn.BkColor = WHITE;
	rlColumn.Font = &ogCourier_Regular_10;
	rlColumn.Alignment = COLALIGN_LEFT;

	rlColumn.Text = prpMitarbeiterstamm->Lanm;	
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpMitarbeiterstamm->Finm;	
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpMitarbeiterstamm->Shnm;	
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpMitarbeiterstamm->Perc;	
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpMitarbeiterstamm->Peno;	
	olLine.NewAt(olLine.GetSize(), rlColumn);
	//*** 04.10.99 SHA ***
	//rlColumn.Text = prpMitarbeiterstamm->Makr;	
	//olLine.NewAt(olLine.GetSize(), rlColumn);
	//rlColumn.Text = prpMitarbeiterstamm->Sken;	
	rlColumn.Text = prpMitarbeiterstamm->Lino;	
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpMitarbeiterstamm->Teld;	
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpMitarbeiterstamm->Telh;	
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpMitarbeiterstamm->Telp;	
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = "";
	if(prpMitarbeiterstamm->Doem.GetStatus() == COleDateTime::valid)
	{
		rlColumn.Text = prpMitarbeiterstamm->Doem.Format("%d.%m.%Y");	
	}
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = "";
	if(prpMitarbeiterstamm->Dodm.GetStatus() == COleDateTime::valid)
	{
		rlColumn.Text = prpMitarbeiterstamm->Dodm.Format("%d.%m.%Y");	
	}
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpMitarbeiterstamm->Ktou;	
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpMitarbeiterstamm->Zutf;	
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpMitarbeiterstamm->Zutb;	
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpMitarbeiterstamm->Zutw;	
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpMitarbeiterstamm->Zutn;	
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpMitarbeiterstamm->Tohr;	
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpMitarbeiterstamm->Rema;//Gsmn;	
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpMitarbeiterstamm->Lino;	
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpMitarbeiterstamm->Matr;	
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpMitarbeiterstamm->Rema;	
	olLine.NewAt(olLine.GetSize(), rlColumn);
	
	
	if (FindLine(prpMitarbeiterstamm->Urno, ilItem))
	{
        MITARBEITERSTAMMTABLE_LINEDATA *prlLine = &omLines[ilItem];

		prlLine->Urno = prpMitarbeiterstamm->Urno;	// Eindeutige Datensatz-Nr.
		prlLine->Lanm = prpMitarbeiterstamm->Lanm; 	// Name
		prlLine->Finm = prpMitarbeiterstamm->Finm; 	// Vorname
		prlLine->Shnm = prpMitarbeiterstamm->Shnm; 	// Kurzname
		prlLine->Perc = prpMitarbeiterstamm->Perc; 	// Kürzel
		prlLine->Peno = prpMitarbeiterstamm->Peno; 	// Personalnummer
		//prlLine->Makr = prpMitarbeiterstamm->Makr; 	// Mitarbeiterkreis
		//prlLine->Sken = prpMitarbeiterstamm->Sken; 	// Schichtkennzeichen
		prlLine->Teld = prpMitarbeiterstamm->Teld; 	// Telefonnr. dienstlich
		prlLine->Telh = prpMitarbeiterstamm->Telh; 	// Telefonnr. Handy
		prlLine->Telp = prpMitarbeiterstamm->Telp; 	// Telefonnr. privat
		prlLine->Doem = prpMitarbeiterstamm->Doem;	// Eintrittsdatum
		prlLine->Dodm = prpMitarbeiterstamm->Dodm;	// Austrittsdatum
		prlLine->Ktou = prpMitarbeiterstamm->Ktou; 	// Urlaubsansprüche
		prlLine->Zutf = prpMitarbeiterstamm->Zutf; 	// Zusätzliche Urlaubstage Freistellungstage
		prlLine->Zutb = prpMitarbeiterstamm->Zutb; 	// Zusätzliche Urlaubstage Schwerbehinderung
		prlLine->Zutw = prpMitarbeiterstamm->Zutw; 	// Zusätzliche Urlaubstage Wechselschicht
		prlLine->Zutn = prpMitarbeiterstamm->Zutn; 	// Zusätzliche Urlaubstage Nachtschicht
		prlLine->Tohr = prpMitarbeiterstamm->Tohr; 	// Soll an SAP-HR übermittelt werden
		prlLine->Rema = prpMitarbeiterstamm->Rema; 	// Bemerkungen
		prlLine->Gsmn = prpMitarbeiterstamm->Gsmn; 	// Name
		prlLine->Lino = prpMitarbeiterstamm->Lino; 	// Name
		prlLine->Matr = prpMitarbeiterstamm->Matr; 	// Name


		pomMitarbeiterstammTable->ChangeTextLine(ilItem, &olLine, (void *)prlLine);
		pomMitarbeiterstammTable->DisplayTable();
	}
	else
	{
		MakeLine(prpMitarbeiterstamm);
		if (FindLine(prpMitarbeiterstamm->Urno, ilItem))
		{
	        MITARBEITERSTAMMTABLE_LINEDATA *prlLine = &omLines[ilItem];
			if(prlLine != NULL)
			{
				pomMitarbeiterstammTable->AddTextLine(olLine, (void *)prlLine);
				pomMitarbeiterstammTable->DisplayTable();
			}
		}
	}
	olLine.DeleteAll();
}

//-----------------------------------------------------------------------------------------------

void MitarbeiterstammTableViewer::ProcessMitarbeiterstammDelete(STFDATA *prpMitarbeiterstamm)
{
	int ilItem;
	if (FindLine(prpMitarbeiterstamm->Urno, ilItem))
	{
		DeleteLine(ilItem);
		pomMitarbeiterstammTable->DeleteTextLine(ilItem);
		pomMitarbeiterstammTable->DisplayTable();
	}
}

//-----------------------------------------------------------------------------------------------

bool MitarbeiterstammTableViewer::IsPassFilter(STFDATA *prpMitarbeiterstamm)
{

	return true;
}

//-----------------------------------------------------------------------------------------------

int MitarbeiterstammTableViewer::CompareMitarbeiterstamm(MITARBEITERSTAMMTABLE_LINEDATA *prpMitarbeiterstamm1, MITARBEITERSTAMMTABLE_LINEDATA *prpMitarbeiterstamm2)
{
/*	int ilCase=0;
	int	ilCompareResult;
	if(prpMitarbeiterstamm1->Tifd == prpMitarbeiterstamm2->Tifd)
	{
		ilCompareResult= 0;
	}
	else
	{
		if(prpMitarbeiterstamm1->Tifd > prpMitarbeiterstamm2->Tifd)
		{
			ilCompareResult = 1;
		}
		else
		{
			ilCompareResult = -1;
		}
	}
	return ilCompareResult;
*/  
	return 0;
}

//-----------------------------------------------------------------------------------------------

void MitarbeiterstammTableViewer::DeleteLine(int ipLineno)
{
	omLines.DeleteAt(ipLineno);
}

//-----------------------------------------------------------------------------------------------

bool MitarbeiterstammTableViewer::FindLine(long lpUrno, int &rilLineno)
{
    for (rilLineno = 0; rilLineno < omLines.GetSize(); rilLineno++)
	{
        if (omLines[rilLineno].Urno == lpUrno)
            return true;
	}
    return false;
}
//-----------------------------------------------------------------------------------------------

void MitarbeiterstammTableViewer::DeleteAll()
{
    while (omLines.GetSize() > 0)
    DeleteLine(0);
}

//-----------------------------------------------------------------------------------------------
// Printing Routines
//-----------------------------------------------------------------------------------------------

void MitarbeiterstammTableViewer::PrintTableView()
{
	CString olFooter1,olFooter2;
	omTableName = LoadStg(IDS_STRING183);
	int ilOrientation = PRINT_LANDSCAPE;//  PRINT_PORTRAET  PRINT_LANDSCAPE
	pomPrint = new CCSPrint((CWnd*)this,ilOrientation,45);// FirstLine(P=200,L=200),LeftOffset(P=200,L=50)
	//pomPrint->imMaxLines = 38;  // (P=57,L=38)
	//omBitmap.LoadBitmap(IDB_HAJLOGO);
	//pomPrint->SetBitmaps(&omBitmap,&omBitmap);
	if (pomPrint != NULL)
	{
		if (pomPrint->InitializePrinter(ilOrientation) == TRUE)
		{
			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
			pomPrint->imLineNo = pomPrint->imMaxLines + 1;
			DOCINFO	rlDocInfo;
			memset(&rlDocInfo, 0, sizeof(DOCINFO));
			rlDocInfo.cbSize = sizeof( DOCINFO );
			rlDocInfo.lpszDocName = omTableName;	
			pomPrint->omCdc.StartDoc( &rlDocInfo );
			pomPrint->imPageNo = 0;
			int ilLines = omLines.GetSize();
			olFooter1.Format("%d %s",ilLines,omTableName);
			for(int i = 0; i < ilLines; i++ ) 
			{
				if(pomPrint->imLineNo >= pomPrint->imMaxLines)
				{
					if(pomPrint->imPageNo > 0)
					{
						olFooter2.Format("%s: %d",LoadStg(IDS_STRING222),pomPrint->imPageNo);
						pomPrint->PrintUIFFooter(olFooter1,"",olFooter2);
						pomPrint->omCdc.EndPage();
					}
					PrintTableHeader();
				}
				if(pomPrint->imLineNo == (pomPrint->imMaxLines-1) || i == (ilLines-1))
				{
					PrintTableLine(&omLines[i],true);
				}
				else
				{
					PrintTableLine(&omLines[i],false);
				}
			}
			olFooter2.Format("%s: %d",LoadStg(IDS_STRING222),pomPrint->imPageNo);
			pomPrint->PrintUIFFooter(olFooter1,"",olFooter2);
			pomPrint->omCdc.EndPage();
			pomPrint->omCdc.EndDoc();
		}
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
	delete pomPrint;
	//omBitmap.DeleteObject();
	pomPrint = NULL;
	}
}

//-----------------------------------------------------------------------------------------------

bool MitarbeiterstammTableViewer::PrintTableHeader()
{
	pomPrint->omCdc.StartPage();
	pomPrint->imPageNo++;
	pomPrint->imLineNo = 0;

	pomPrint->PrintUIFHeader(omTableName,CString(CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M")),pomPrint->imFirstLine-10);

	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;

	rlElement.Alignment   = PRINT_LEFT;
	rlElement.FrameLeft   = PRINT_NOFRAME;
	rlElement.FrameRight  = PRINT_NOFRAME;
	rlElement.FrameTop    = PRINT_NOFRAME;
	rlElement.FrameBottom = PRINT_FRAMEMEDIUM;
	rlElement.pFont       = &pomPrint->ogCourierNew_Bold_8;

	int ilSize = omHeaderDataArray.GetSize();
	for(int i=0;i<ilSize;i++)
	{
		//if(i!=1&&i!=2)// <= setzen, wenn Spalten nicht angezeigt werden sollen 
		{
			rlElement.Length = (int)(omHeaderDataArray[i].Length*dgCCSPrintFactor);
			if(rlElement.Length < igCCSPrintMinLength)rlElement.Length+=igCCSPrintMoreLength; 
			rlElement.Text   = omHeaderDataArray[i].Text;
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
		}
	}
	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();


	return true;
}

//-----------------------------------------------------------------------------------------------

bool MitarbeiterstammTableViewer::PrintTableLine(MITARBEITERSTAMMTABLE_LINEDATA *prpLine,bool bpLastLine)
{
	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;
	rlElement.pFont      = &pomPrint->ogCourierNew_Regular_8;
	int ilSize = omHeaderDataArray.GetSize();
	for(int i=0;i<ilSize;i++)
	{
		//if(i!=1&&i!=1)// <= setzen, wenn Spalten nicht angezeigt werden sollen 
		{
			rlElement.Length = (int)(omHeaderDataArray[i].Length*dgCCSPrintFactor);
			if(rlElement.Length < igCCSPrintMinLength)rlElement.Length+=igCCSPrintMoreLength; 
			rlElement.Alignment  = PRINT_LEFT;
			rlElement.FrameTop   = PRINT_FRAMETHIN;
			if(bpLastLine == true)
			{
				rlElement.FrameBottom = PRINT_FRAMETHIN;
			}
			else
			{
				rlElement.FrameBottom = PRINT_NOFRAME;
			}
			rlElement.FrameLeft  = PRINT_NOFRAME;
			rlElement.FrameRight = PRINT_NOFRAME;
			switch(i)
			{
			case 0:
				{
					rlElement.FrameLeft  = PRINT_FRAMETHIN;
					rlElement.Text = prpLine->Lanm;
				}
				break;
//			case 1:
//				rlElement.Text = prpLine->Lanm;
//				break;
			case 1:
				rlElement.Text = prpLine->Finm;
				break;
			case 2:
				rlElement.Text = prpLine->Shnm;
				break;
			case 3:
				rlElement.Text = prpLine->Perc;
				break;
			case 4:
				rlElement.Text = prpLine->Peno;
				break;
			case 5:
				//*** 04.10.99 SHA ***
				//rlElement.Text = prpLine->Makr;
				rlElement.Text = prpLine->Lino;
				break;
			case 6:
				rlElement.Text = prpLine->Sken;
				break;
			case 7:
				rlElement.Text = prpLine->Teld;
				break;
			case 8:
				rlElement.Text = prpLine->Telh;
				break;
			case 9:
				rlElement.Text = prpLine->Telp;
				break;
			case 10:
				rlElement.Text = prpLine->Doem.Format("%d.%m.%Y");
				break;
			case 11:
				rlElement.Text = prpLine->Dodm.Format("%d.%m.%Y");
				break;
			case 12:
				rlElement.Text = prpLine->Ktou;
				break;
			case 13:
				rlElement.Text = prpLine->Zutf;
				break;
			case 14:
				rlElement.Text = prpLine->Zutb;
				break;
			case 15:
				rlElement.Text = prpLine->Zutw;
				break;
			case 16:
				rlElement.Text = prpLine->Zutn;
				break;
			case 17:
				rlElement.Text = prpLine->Tohr;
				break;
			case 18:
				rlElement.Text = prpLine->Rema;
			case 19:
				rlElement.Text = prpLine->Gsmn;
			case 20:
				rlElement.Text = prpLine->Lino;
			case 21:
				rlElement.Text = prpLine->Matr;
				break;
			}
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
		}
	}
	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();
	return true;
}

//-----------------------------------------------------------------------------------------------
