// ErgAbfertigungsartenDlg.cpp : implementation file
//

#include "stdafx.h"
#include "BDPSUIF.h"
#include "ErgAbfertigungsartenDlg.h"
#include "PrivList.h"
#include "CedaGrnData.h"
#include "CedaACRData.h"
#include "CedaACTData.h"
#include "CedaAPTData.h"
#include "CedaHTYData.h"
#include "CedaSTYData.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// ErgAbfertigungsartenDlg dialog

ErgAbfertigungsartenDlg::ErgAbfertigungsartenDlg(SPHDATA *popSph,CWnd* pParent) : CDialog(ErgAbfertigungsartenDlg::IDD, pParent)
{
	pomSph = popSph;

	//{{AFX_DATA_INIT(ErgAbfertigungsartenDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void ErgAbfertigungsartenDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(ErgAbfertigungsartenDlg)
	DDX_Control(pDX, IDC_ART,	 m_ART);
	DDX_Control(pDX, IDC_REGN,	 m_REGN);
	DDX_Control(pDX, IDOK,		 m_OK);
	DDX_Control(pDX, IDC_DAYS_1, m_DAYS1);
	DDX_Control(pDX, IDC_DAYS_2, m_DAYS2);
	DDX_Control(pDX, IDC_DAYS_3, m_DAYS3);
	DDX_Control(pDX, IDC_DAYS_4, m_DAYS4);
	DDX_Control(pDX, IDC_DAYS_5, m_DAYS5);
	DDX_Control(pDX, IDC_DAYS_6, m_DAYS6);
	DDX_Control(pDX, IDC_DAYS_7, m_DAYS7);
	DDX_Control(pDX, IDC_DAYS_T, m_DAYST);
	DDX_Control(pDX, IDC_ARDE_A, m_ARDEA);
	DDX_Control(pDX, IDC_ARDE_D, m_ARDED);
	DDX_Control(pDX, IDC_ACT3,	 m_ACT3);
	DDX_Control(pDX, IDC_ACTM,	 m_ACTM);
	DDX_Control(pDX, IDC_ALCM,	 m_ALCM);
	DDX_Control(pDX, IDC_APC3,	 m_APC3);
	DDX_Control(pDX, IDC_APCM,	 m_APCM);
	DDX_Control(pDX, IDC_CDAT_D, m_CDATD);
	DDX_Control(pDX, IDC_CDAT_T, m_CDATT);
	DDX_Control(pDX, IDC_FLNC,	 m_FLNC);
	DDX_Control(pDX, IDC_FLNN,	 m_FLNN);
	DDX_Control(pDX, IDC_FLNS,	 m_FLNS);
	DDX_Control(pDX, IDC_LSTU_D, m_LSTUD);
	DDX_Control(pDX, IDC_LSTU_T, m_LSTUT);
	DDX_Control(pDX, IDC_STYP,	 m_STYP);
	DDX_Control(pDX, IDC_TTYP,	 m_TTYP);
	DDX_Control(pDX, IDC_USEC,	 m_USEC);
	DDX_Control(pDX, IDC_USEU,	 m_USEU);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(ErgAbfertigungsartenDlg, CDialog)
	//{{AFX_MSG_MAP(ErgAbfertigungsartenDlg)
	ON_BN_CLICKED(IDC_DAYS_1,	OnDays17)
	ON_BN_CLICKED(IDC_DAYS_2,	OnDays17)
	ON_BN_CLICKED(IDC_DAYS_3,	OnDays17)
	ON_BN_CLICKED(IDC_DAYS_4,	OnDays17)
	ON_BN_CLICKED(IDC_DAYS_5,	OnDays17)
	ON_BN_CLICKED(IDC_DAYS_6,	OnDays17)
	ON_BN_CLICKED(IDC_DAYS_7,	OnDays17)
	ON_BN_CLICKED(IDC_DAYS_T,	OnDaysT)
    ON_MESSAGE(WM_EDIT_KILLFOCUS, OnKillfocus)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// ErgAbfertigungsartenDlg message handlers
//----------------------------------------------------------------------------------------

BOOL ErgAbfertigungsartenDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	GRNDATA *prlGrn;

	m_Caption = LoadStg(IDS_STRING169) + " <" + m_Caption + ">";
	SetWindowText(m_Caption);

	CString olTmp = LoadStg(IDS_STRING15) + ":";
	m_ART.SetWindowText(olTmp);

	char clStat = ogPrivList.GetStat("ERGABFERTIGUNGSARTENDLG.m_OK");
	SetWndStatAll(clStat,m_OK);

	//------------------------------------
	m_CDATD.SetBKColor(SILVER);
	m_CDATD.SetInitText(pomSph->Cdat.Format("%d.%m.%Y"));
	m_CDATD.SetSecState(ogPrivList.GetStat("ERGABFERTIGUNGSARTENDLG.m_CDAT"));
	// - - - - - - - - - - - - - - - - - -
	m_CDATT.SetBKColor(SILVER);
	m_CDATT.SetInitText(pomSph->Cdat.Format("%H:%M"));
	m_CDATT.SetSecState(ogPrivList.GetStat("ERGABFERTIGUNGSARTENDLG.m_CDAT"));
	//------------------------------------
	m_LSTUD.SetBKColor(SILVER);
	m_LSTUD.SetInitText(pomSph->Lstu.Format("%d.%m.%Y"));
	m_LSTUD.SetSecState(ogPrivList.GetStat("ERGABFERTIGUNGSARTENDLG.m_LSTU"));
	// - - - - - - - - - - - - - - - - - -
	m_LSTUT.SetBKColor(SILVER);
	m_LSTUT.SetInitText(pomSph->Lstu.Format("%H:%M"));
	m_LSTUT.SetSecState(ogPrivList.GetStat("ERGABFERTIGUNGSARTENDLG.m_LSTU"));
	//------------------------------------
	m_USEC.SetBKColor(SILVER);
	m_USEC.SetInitText(pomSph->Usec);
	m_USEC.SetSecState(ogPrivList.GetStat("ERGABFERTIGUNGSARTENDLG.m_USEC"));
	//------------------------------------
	m_USEU.SetBKColor(SILVER);
	m_USEU.SetInitText(pomSph->Useu);
	m_USEU.SetSecState(ogPrivList.GetStat("ERGABFERTIGUNGSARTENDLG.m_USEU"));
	//------------------------------------//WHITE,YELLOW,SILVER,RED,LYELLOW
	m_FLNC.SetFormat("x|#x|#x|#");
	m_FLNC.SetTextLimit(0,3);
	m_FLNC.SetTextErrColor(RED);
	m_FLNC.SetInitText(pomSph->Flnc);
	m_FLNC.SetSecState(ogPrivList.GetStat("ERGABFERTIGUNGSARTENDLG.m_FLNN"));
	//------------------------------------
	m_FLNN.SetFormat("x|#x|#x|#x|#x|#");
	m_FLNN.SetTextLimit(0,5);
	m_FLNN.SetTextErrColor(RED);
	m_FLNN.SetInitText(pomSph->Flnn);
	m_FLNN.SetSecState(ogPrivList.GetStat("ERGABFERTIGUNGSARTENDLG.m_FLNN"));
	//------------------------------------
	m_FLNS.SetFormat("x|#");
	m_FLNS.SetTextLimit(0,1);
	m_FLNS.SetTextErrColor(RED);
	m_FLNS.SetInitText(pomSph->Flns);
	m_FLNS.SetSecState(ogPrivList.GetStat("ERGABFERTIGUNGSARTENDLG.m_FLNN"));
	//------------------------------------
	m_ALCM.SetFormat("x|#x|#x|#");
	m_ALCM.SetTextLimit(0,3);
	m_ALCM.SetTextErrColor(RED);
	prlGrn = ogGrnData.GetGrnByUrno(pomSph->Alcm);
	if(prlGrn != NULL)
	{
		m_ALCM.SetInitText(prlGrn->Grsn);
	}
	m_ALCM.SetSecState(ogPrivList.GetStat("ERGABFERTIGUNGSARTENDLG.m_ALCM"));
	//------------------------------------
	m_REGN.SetTypeToString("X(12)",12,0);
	m_REGN.SetTextErrColor(RED);
	m_REGN.SetInitText(pomSph->Regn);
	m_REGN.SetSecState(ogPrivList.GetStat("ERGABFERTIGUNGSARTENDLG.m_REGN"));
	//------------------------------------
	m_ACT3.SetFormat("x|#x|#x|#");
	m_ACT3.SetTextLimit(-1,-1,true);
	m_ACT3.SetTextErrColor(RED);
	m_ACT3.SetInitText(pomSph->Act3);
	m_ACT3.SetSecState(ogPrivList.GetStat("ERGABFERTIGUNGSARTENDLG.m_ACT3"));
	//------------------------------------
	m_ACTM.SetFormat("x|#x|#x|#");
	m_ACTM.SetTextLimit(0,3);
	m_ACTM.SetTextErrColor(RED);
	prlGrn = ogGrnData.GetGrnByUrno(pomSph->Actm);
	if(prlGrn != NULL)
	{
		m_ACTM.SetInitText(prlGrn->Grsn);
	}
	m_ACTM.SetSecState(ogPrivList.GetStat("ERGABFERTIGUNGSARTENDLG.m_ACTM"));
	//------------------------------------
	m_APC3.SetFormat("x|#x|#x|#");
	m_APC3.SetTextLimit(-1,-1,true);
	m_APC3.SetTextErrColor(RED);
	m_APC3.SetInitText(pomSph->Apc3);
	m_APC3.SetSecState(ogPrivList.GetStat("ERGABFERTIGUNGSARTENDLG.m_APC3"));
	//------------------------------------
	m_APCM.SetFormat("x|#x|#x|#");
	m_APCM.SetTextLimit(0,3);
	m_APCM.SetTextErrColor(RED);
	prlGrn = ogGrnData.GetGrnByUrno(pomSph->Apcm);
	if(prlGrn != NULL)
	{
		m_APCM.SetInitText(prlGrn->Grsn);
	}
	m_APCM.SetSecState(ogPrivList.GetStat("ERGABFERTIGUNGSARTENDLG.m_APCM"));
	//------------------------------------
	clStat = ogPrivList.GetStat("ERGABFERTIGUNGSARTENDLG.m_DAYS");
	SetWndStatAll(clStat,m_DAYS1);
	SetWndStatAll(clStat,m_DAYS2);
	SetWndStatAll(clStat,m_DAYS3);
	SetWndStatAll(clStat,m_DAYS4);
	SetWndStatAll(clStat,m_DAYS5);
	SetWndStatAll(clStat,m_DAYS6);
	SetWndStatAll(clStat,m_DAYS7);
	SetWndStatAll(clStat,m_DAYST);
	if(strchr(pomSph->Days,'1') != NULL) m_DAYS1.SetCheck(1);
	if(strchr(pomSph->Days,'2') != NULL) m_DAYS2.SetCheck(1);
	if(strchr(pomSph->Days,'3') != NULL) m_DAYS3.SetCheck(1);
	if(strchr(pomSph->Days,'4') != NULL) m_DAYS4.SetCheck(1);
	if(strchr(pomSph->Days,'5') != NULL) m_DAYS5.SetCheck(1);
	if(strchr(pomSph->Days,'6') != NULL) m_DAYS6.SetCheck(1);
	if(strchr(pomSph->Days,'7') != NULL) m_DAYS7.SetCheck(1);
	//------------------------------------
	m_STYP.SetFormat("x|#x|#");
	m_STYP.SetTextLimit(0,2);
	m_STYP.SetTextErrColor(RED);
	m_STYP.SetInitText(pomSph->Styp);
	m_STYP.SetSecState(ogPrivList.GetStat("ERGABFERTIGUNGSARTENDLG.m_STYP"));
	//------------------------------------
	m_TTYP.SetFormat("x|#x|#x|#x|#x|#");
	m_TTYP.SetTextLimit(1,5);
	m_TTYP.SetBKColor(YELLOW);
	m_TTYP.SetTextErrColor(RED);
	m_TTYP.SetInitText(pomSph->Ttyp);
	m_TTYP.SetSecState(ogPrivList.GetStat("ERGABFERTIGUNGSARTENDLG.m_TTYP"));
	//------------------------------------
	m_ARDEA.SetCheck(0);
	m_ARDED.SetCheck(0);
	if (pomSph->Arde[0] == 'A') m_ARDEA.SetCheck(1);
	else if (pomSph->Arde[0] == 'D') m_ARDED.SetCheck(1);
	else m_ARDEA.SetCheck(1);
	clStat = ogPrivList.GetStat("ERGABFERTIGUNGSARTENDLG.m_ARDE");
	SetWndStatAll(clStat,m_ARDEA);
	SetWndStatAll(clStat,m_ARDED);
	UpdateWindow();
	//------------------------------------
	return TRUE;  
}

//----------------------------------------------------------------------------------------

void ErgAbfertigungsartenDlg::OnOK() 
{
	AfxGetApp()->DoWaitCursor(1);
	CString olErrorText;
	bool ilStatus = true;
	if(m_FLNC.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING16) + ogNotFormat;
	}
	if(m_FLNN.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING17) + ogNotFormat;
	}
	if(m_FLNS.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING18) + ogNotFormat;
	}
	if(m_ALCM.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING19) + ogNotFormat;
	}
	if(m_REGN.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING182) + ogNotFormat;
	}
	if(m_ACT3.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING21) + ogNotFormat;
	}
	if(m_ACTM.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING22) + ogNotFormat;
	}
	if(m_APC3.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING23) + ogNotFormat;
	}
	if(m_APCM.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING24) + ogNotFormat;
	}
	if(m_STYP.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING25) + ogNotFormat;
	}
	if(m_TTYP.GetStatus() == false)
	{
		ilStatus = false;
		if(m_TTYP.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING15) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING15) + ogNotFormat;
		}
	}

	///////////////////////////////////////////////////////////////////

	if(m_DAYS1.GetCheck()==0&&m_DAYS2.GetCheck()==0&&m_DAYS3.GetCheck()==0&&m_DAYS4.GetCheck()==0&&m_DAYS5.GetCheck()==0&&m_DAYS6.GetCheck()==0&&m_DAYS7.GetCheck()==0)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING26);
	}
		
	int ilC = m_FLNC.GetWindowTextLength();
	int ilN = m_FLNN.GetWindowTextLength();
	int ilS = m_FLNS.GetWindowTextLength();
	
	if((ilC != 0 && ilN == 0)||(ilC == 0 && ilN != 0)||(ilS != 0 &&(ilN == 0 || ilC == 0)))
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING27);
	}

	//Konsistenz-Check//
	if(ilStatus == true)
	{
		CString olAlcm,olActm,olApcm,olRegn,olAct3,olApc3,olStyp,olTtyp;
		char clWhere[100];
		CCSPtrArray<GRNDATA> olGrnCPA;

		if(m_ALCM.GetWindowTextLength() != 0)
		{
			m_ALCM.GetWindowText(olAlcm);
			sprintf(clWhere,"WHERE GRSN='%s' AND TABN='ALT%s'",olAlcm,ogTableExt);
			if(ogGrnData.ReadSpecialData(&olGrnCPA,clWhere,"URNO",false) == false)
			{
				ilStatus = false;
				olErrorText += LoadStg(IDS_STRING28);
			}
			else
			{
				pomSph->Alcm = olGrnCPA[0].Urno;
			}
			olGrnCPA.DeleteAll();
		}
		else
		{
			pomSph->Alcm = 0;
		}

		if(m_ACTM.GetWindowTextLength() != 0)
		{
			m_ACTM.GetWindowText(olActm);
			sprintf(clWhere,"WHERE GRSN='%s' AND TABN='ACT%s'",olActm,ogTableExt);
			if(ogGrnData.ReadSpecialData(&olGrnCPA,clWhere,"URNO",false) == false)
			{
				ilStatus = false;
				olErrorText += LoadStg(IDS_STRING29);
			}
			else
			{
				pomSph->Actm = olGrnCPA[0].Urno;
			}
			olGrnCPA.DeleteAll();
		}
		else
		{
			pomSph->Actm = 0;
		}

		if(m_APCM.GetWindowTextLength() != 0)
		{
			m_APCM.GetWindowText(olApcm);
			sprintf(clWhere,"WHERE GRSN='%s' AND TABN='APT%s'",olApcm,ogTableExt);
			if(ogGrnData.ReadSpecialData(&olGrnCPA,clWhere,"URNO",false) == false)
			{
				ilStatus = false;
				olErrorText += LoadStg(IDS_STRING30);
			}
			else
			{
				pomSph->Apcm = olGrnCPA[0].Urno;
			}
			olGrnCPA.DeleteAll();
		}
		else
		{
			pomSph->Apcm = 0;
		}

		if(m_REGN.GetWindowTextLength() != 0)
		{
			m_REGN.GetWindowText(olRegn);
			sprintf(clWhere,"WHERE REGN='%s'",olRegn);
			if(ogACRData.ReadSpecialData(NULL,clWhere,"REGN",false) == false)
			{
				ilStatus = false;
				olErrorText += LoadStg(IDS_STRING31);
			}
		}
		
		if(m_ACT3.GetWindowTextLength() == 3)
		{
			m_ACT3.GetWindowText(olAct3);
			sprintf(clWhere,"WHERE ACT3='%s'",olAct3);
			if(ogACTData.ReadSpecialData(NULL,clWhere,"ACT3",false) == false)
			{
				ilStatus = false;
				olErrorText += LoadStg(IDS_STRING32);
			}
		}

		if(m_APC3.GetWindowTextLength() == 3)
		{
			m_APC3.GetWindowText(olApc3);
			sprintf(clWhere,"WHERE APC3='%s'",olApc3);
			if(ogAPTData.ReadSpecialData(NULL,clWhere,"APC3",false) == false)
			{
				ilStatus = false;
				olErrorText += LoadStg(IDS_STRING33);
			}
		}
		
		if(m_STYP.GetWindowTextLength() != 0)
		{
			m_STYP.GetWindowText(olStyp);
			sprintf(clWhere,"WHERE STYP='%s'",olStyp);
			if(ogSTYData.ReadSpecialData(NULL,clWhere,"STYP",false) == false)
			{
				ilStatus = false;
				olErrorText += LoadStg(IDS_STRING34);
			}
		}

		if(m_TTYP.GetWindowTextLength() != 0)
		{
			m_TTYP.GetWindowText(olTtyp);
			sprintf(clWhere,"WHERE HTYP='%s'",olTtyp);
			if(ogHTYData.ReadSpecialData(NULL,clWhere,"HTYP",false) == false)
			{
				ilStatus = false;
				olErrorText += LoadStg(IDS_STRING35);
			}
		}
	}

	///////////////////////////////////////////////////////////////////
	AfxGetApp()->DoWaitCursor(-1);
	if (ilStatus == true)
	{
		m_TTYP.GetWindowText(pomSph->Ttyp,6);
		m_STYP.GetWindowText(pomSph->Styp,3);
		m_APC3.GetWindowText(pomSph->Apc3,4);
		m_ACT3.GetWindowText(pomSph->Act3,4);
		m_REGN.GetWindowText(pomSph->Regn,13);
		m_FLNC.GetWindowText(pomSph->Flnc,4);
		m_FLNN.GetWindowText(pomSph->Flnn,6);
		m_FLNS.GetWindowText(pomSph->Flns,2);

		CString olDays;
		if(m_DAYS1.GetCheck()==1) olDays  = "1";
		if(m_DAYS2.GetCheck()==1) olDays += "2";
		if(m_DAYS3.GetCheck()==1) olDays += "3";
		if(m_DAYS4.GetCheck()==1) olDays += "4";
		if(m_DAYS5.GetCheck()==1) olDays += "5";
		if(m_DAYS6.GetCheck()==1) olDays += "6";
		if(m_DAYS7.GetCheck()==1) olDays += "7";
		strcpy(pomSph->Days,olDays);

		(m_ARDEA.GetCheck() == 1) ? pomSph->Arde[0] = 'A' : pomSph->Arde[0] = 'D';

		CDialog::OnOK();
	}
	else
	{
		Beep(440,70);
		MessageBox(olErrorText,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
		m_FLNC.SetFocus();
		m_FLNC.SetSel(0,-1);
  }

}

//----------------------------------------------------------------------------------------

void ErgAbfertigungsartenDlg::OnDays17() 
{
	if(m_DAYST.GetCheck() == 1)
	{
		m_DAYST.SetCheck(0);
	}
}

//----------------------------------------------------------------------------------------

void ErgAbfertigungsartenDlg::OnDaysT() 
{
	if(m_DAYST.GetCheck() == 1)
	{
		m_DAYS1.SetCheck(1);
		m_DAYS2.SetCheck(1);
		m_DAYS3.SetCheck(1);
		m_DAYS4.SetCheck(1);
		m_DAYS5.SetCheck(1);
		m_DAYS6.SetCheck(1);
		m_DAYS7.SetCheck(1);
	}
	if(m_DAYST.GetCheck() == 0)
	{
		m_DAYS1.SetCheck(0);
		m_DAYS2.SetCheck(0);
		m_DAYS3.SetCheck(0);
		m_DAYS4.SetCheck(0);
		m_DAYS5.SetCheck(0);
		m_DAYS6.SetCheck(0);
		m_DAYS7.SetCheck(0);
	}
}

//----------------------------------------------------------------------------------------

LONG ErgAbfertigungsartenDlg::OnKillfocus(UINT lParam, LONG wParam) 
{
	if(lParam == (UINT)m_FLNC.imID && m_FLNC.GetWindowTextLength() != 0) m_ALCM.SetInitText("");
	else if(lParam == (UINT)m_FLNN.imID && m_FLNN.GetWindowTextLength() != 0) m_ALCM.SetInitText("");
	else if(lParam == (UINT)m_FLNS.imID && m_FLNS.GetWindowTextLength() != 0) m_ALCM.SetInitText("");
	else if(lParam == (UINT)m_ALCM.imID && m_ALCM.GetWindowTextLength() != 0) 
	{
		m_FLNC.SetInitText("");
		m_FLNN.SetInitText("");
		m_FLNS.SetInitText("");
	}
	else if(lParam == (UINT)m_ACT3.imID && m_ACT3.GetWindowTextLength() != 0) m_ACTM.SetInitText("");
	else if(lParam == (UINT)m_ACTM.imID && m_ACTM.GetWindowTextLength() != 0) m_ACT3.SetInitText("");
	else if(lParam == (UINT)m_APC3.imID && m_APC3.GetWindowTextLength() != 0) m_APCM.SetInitText("");
	else if(lParam == (UINT)m_APCM.imID && m_APCM.GetWindowTextLength() != 0) m_APC3.SetInitText("");
	return 0L;
}

//----------------------------------------------------------------------------------------
