// DiensteUndAbwesenheitenDlg.cpp : implementation file
//

#include "stdafx.h"
#include "bdpsuif.h"
#include "DiensteUndAbwesenheitenDlg.h"
#include "AwDlg.h"
#include "PrivList.h"
#include "CedaBsdData.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// DiensteUndAbwesenheitenDlg dialog


DiensteUndAbwesenheitenDlg::DiensteUndAbwesenheitenDlg(ODADATA *popOda, CWnd* pParent /*=NULL*/) : CDialog(DiensteUndAbwesenheitenDlg::IDD, pParent)
{
	pomOda = popOda;
	//{{AFX_DATA_INIT(DiensteUndAbwesenheitenDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void DiensteUndAbwesenheitenDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(DiensteUndAbwesenheitenDlg)
	DDX_Control(pDX, IDC_FREE,	   m_Free);
	DDX_Control(pDX, IDC_R_TATP_0, m_R_Tatp0);
	DDX_Control(pDX, IDC_R_TATP_1, m_R_Tatp1);
	DDX_Control(pDX, IDC_R_TATP_2, m_R_Tatp2);
	DDX_Control(pDX, IDC_R_TATP_3, m_R_Tatp3);
	DDX_Control(pDX, IDC_R_TATP_4, m_R_Tatp4);
	DDX_Control(pDX, IDC_C_UPLN, m_C_Upln);
	DDX_Control(pDX, IDC_C_TSAP, m_C_Tsap);
	DDX_Control(pDX, IDOK,       m_OK);
	DDX_Control(pDX, IDC_CDAT_D, m_CDATD);
	DDX_Control(pDX, IDC_CDAT_T, m_CDATT);
	DDX_Control(pDX, IDC_CTRC,	 m_CTRC);
	DDX_Control(pDX, IDC_CTRC2,  m_CTRC2);
	DDX_Control(pDX, IDC_DURA,	 m_DURA);
	DDX_Control(pDX, IDC_LSTU_D, m_LSTUD);
	DDX_Control(pDX, IDC_LSTU_T, m_LSTUT);
	DDX_Control(pDX, IDC_REMA,	 m_REMA);
	DDX_Control(pDX, IDC_SDAC,	 m_SDAC);
	DDX_Control(pDX, IDC_SDAK,	 m_SDAK);
	DDX_Control(pDX, IDC_SDAN,	 m_SDAN);
	DDX_Control(pDX, IDC_SDAS,	 m_SDAS);
	DDX_Control(pDX, IDC_USEC,	 m_USEC);
	DDX_Control(pDX, IDC_USEU,	 m_USEU);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(DiensteUndAbwesenheitenDlg, CDialog)
	//{{AFX_MSG_MAP(DiensteUndAbwesenheitenDlg)
	ON_BN_CLICKED(IDC_B_AW_COT, OnBAwCot)
    ON_MESSAGE(WM_EDIT_KILLFOCUS, OnKillfocus)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// DiensteUndAbwesenheitenDlg message handlers
//----------------------------------------------------------------------------------------

BOOL DiensteUndAbwesenheitenDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	m_Caption = LoadStg(IDS_STRING168) + " <" + m_Caption + ">";
	SetWindowText(m_Caption);
	
	char clStat = ogPrivList.GetStat("DIENSTEUNDABWESENHEITENDLG.m_OK");
	SetWndStatAll(clStat,m_OK);

	//------------------------------------
	m_CDATD.SetBKColor(SILVER);
	m_CDATD.SetInitText(pomOda->Cdat.Format("%d.%m.%Y"));
	m_CDATD.SetSecState(ogPrivList.GetStat("DIENSTEUNDABWESENHEITENDLG.m_CDAT"));
	// - - - - - - - - - - - - - - - - - -
	m_CDATT.SetBKColor(SILVER);
	m_CDATT.SetInitText(pomOda->Cdat.Format("%H:%M"));
	m_CDATT.SetSecState(ogPrivList.GetStat("DIENSTEUNDABWESENHEITENDLG.m_CDAT"));
	//------------------------------------
	m_LSTUD.SetBKColor(SILVER);
	m_LSTUD.SetInitText(pomOda->Lstu.Format("%d.%m.%Y"));
	m_LSTUD.SetSecState(ogPrivList.GetStat("DIENSTEUNDABWESENHEITENDLG.m_LSTU"));
	// - - - - - - - - - - - - - - - - - -
	m_LSTUT.SetBKColor(SILVER);
	m_LSTUT.SetInitText(pomOda->Lstu.Format("%H:%M"));
	m_LSTUT.SetSecState(ogPrivList.GetStat("DIENSTEUNDABWESENHEITENDLG.m_LSTU"));
	//------------------------------------
	m_USEC.SetBKColor(SILVER);
	m_USEC.SetInitText(pomOda->Usec);
	m_USEC.SetSecState(ogPrivList.GetStat("DIENSTEUNDABWESENHEITENDLG.m_USEC"));
	//------------------------------------
	m_USEU.SetBKColor(SILVER);
	m_USEU.SetInitText(pomOda->Useu);
	m_USEU.SetSecState(ogPrivList.GetStat("DIENSTEUNDABWESENHEITENDLG.m_USEU"));
	//------------------------------------//WHITE,YELLOW,SILVER,RED,LYELLOW
	m_CTRC.SetFormat("x|#x|#x|#x|#x|#");
	m_CTRC.SetTextLimit(1,5);
	//m_CTRC.SetBKColor(YELLOW);  
	m_CTRC.SetTextErrColor(RED);
	m_CTRC.SetInitText(pomOda->Ctrc);
	m_CTRC.SetSecState(ogPrivList.GetStat("DIENSTEUNDABWESENHEITENDLG.m_CTRC"));
	//------------------------------------
	m_CTRC2.SetBKColor(SILVER);
	if(strcmp(pomOda->Ctrc, "") != 0)
	{
		char clWhere[100];
		CCSPtrArray<COTDATA> olCotList;
		sprintf(clWhere,"WHERE CTRC='%s'",pomOda->Ctrc);
		if(ogCotData.ReadSpecialData(&olCotList, clWhere, "CTRN", false) == true)
		{
			m_CTRC2.SetInitText(CString(olCotList[0].Ctrn));
		}
	}
	m_CTRC2.SetSecState(ogPrivList.GetStat("DIENSTEUNDABWESENHEITENDLG.m_CTRC2"));
	//------------------------------------
	m_DURA.SetTypeToInt(0, 9999);
	m_DURA.SetBKColor(YELLOW);  
	m_DURA.SetTextErrColor(RED);
	m_DURA.SetInitText(pomOda->Dura);
	m_DURA.SetSecState(ogPrivList.GetStat("DIENSTEUNDABWESENHEITENDLG.m_DURA"));
	//------------------------------------
	m_REMA.SetTypeToString("X(60)", 60, 0);
	m_REMA.SetTextErrColor(RED);
	m_REMA.SetInitText(pomOda->Rema);
	m_REMA.SetSecState(ogPrivList.GetStat("DIENSTEUNDABWESENHEITENDLG.m_REMA"));
	//------------------------------------
	m_SDAC.SetFormat("x|#x|#x|#x|#x|#");
	m_SDAC.SetTextLimit(1,5);
	m_SDAC.SetBKColor(YELLOW);  
	m_SDAC.SetTextErrColor(RED);
	m_SDAC.SetInitText(pomOda->Sdac);
	m_SDAC.SetSecState(ogPrivList.GetStat("DIENSTEUNDABWESENHEITENDLG.m_SDAC"));
	//------------------------------------
	m_SDAK.SetTypeToString("X(12)", 12, 1);
	m_SDAK.SetBKColor(YELLOW);  
	m_SDAK.SetTextErrColor(RED);
	m_SDAK.SetInitText(pomOda->Sdak);
	m_SDAK.SetSecState(ogPrivList.GetStat("DIENSTEUNDABWESENHEITENDLG.m_SDAK"));
	//------------------------------------
	m_SDAN.SetTypeToString("X(40)", 40, 1);
	m_SDAN.SetBKColor(YELLOW);  
	m_SDAN.SetTextErrColor(RED);
	m_SDAN.SetInitText(pomOda->Sdan);
	m_SDAN.SetSecState(ogPrivList.GetStat("DIENSTEUNDABWESENHEITENDLG.m_SDAN"));
	//------------------------------------
	m_SDAS.SetTypeToString("X(8)", 8, 1);
	m_SDAS.SetBKColor(YELLOW);  
	m_SDAS.SetTextErrColor(RED);
	m_SDAS.SetInitText(pomOda->Sdas);
	m_SDAS.SetSecState(ogPrivList.GetStat("DIENSTEUNDABWESENHEITENDLG.m_SDAS"));
	//------------------------------------
	m_R_Tatp0.SetCheck(0);
	m_R_Tatp1.SetCheck(0);
	m_R_Tatp2.SetCheck(0);
	if      (pomOda->Tatp[0] == 'A') m_R_Tatp1.SetCheck(1);
	else if (pomOda->Tatp[0] == 'R') m_R_Tatp2.SetCheck(1);
	else if (pomOda->Tatp[0] == 'S') m_R_Tatp3.SetCheck(1);
	else if (pomOda->Tatp[0] == 'N') m_R_Tatp4.SetCheck(1);
	else							 m_R_Tatp0.SetCheck(1);
	clStat = ogPrivList.GetStat("DIENSTEUNDABWESENHEITENDLG.m_TATP");
	SetWndStatAll(clStat,m_R_Tatp0);
	SetWndStatAll(clStat,m_R_Tatp1);
	SetWndStatAll(clStat,m_R_Tatp2);
	SetWndStatAll(clStat,m_R_Tatp3);
	SetWndStatAll(clStat,m_R_Tatp4);
	//------------------------------------
	m_C_Upln.SetCheck(0);
	if(pomOda->Upln[0] == 'Y') m_C_Upln.SetCheck(1);
	clStat = ogPrivList.GetStat("DIENSTEUNDABWESENHEITENDLG.m_UPLN");
	SetWndStatAll(clStat,m_C_Upln);
	//------------------------------------
	m_C_Tsap.SetCheck(0);
	if(pomOda->Tsap[0] == 'Y') m_C_Tsap.SetCheck(1);
	clStat = ogPrivList.GetStat("DIENSTEUNDABWESENHEITENDLG.m_TSAP");
	SetWndStatAll(clStat,m_C_Tsap);
	//------------------------------------
#ifdef CLIENT_HAJ
	//*** 08.09.99 SHA ***
	if (pomOda->Free[0]=='x')
		m_Free.SetCheck(1);
	else
		m_Free.SetCheck(0);
	clStat = ogPrivList.GetStat("DIENSTEUNDABWESENHEITENDLG.m_FREE");
	SetWndStatAll(clStat,m_Free);
	//------------------------------------
#endif

	return TRUE;
}

//----------------------------------------------------------------------------------------

void DiensteUndAbwesenheitenDlg::OnOK() 
{
	AfxGetApp()->DoWaitCursor(1);
	CString olErrorText;
	bool ilStatus = true;

	if(m_SDAC.GetStatus() == false) 
	{
		ilStatus = false;
		if(m_SDAC.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING237) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING237) + ogNotFormat;
		}
	}
	if(m_SDAK.GetStatus() == false) 
	{
		ilStatus = false;
		if(m_SDAK.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING301) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING301) + ogNotFormat;
		}
	}
	if(m_SDAN.GetStatus() == false) 
	{
		ilStatus = false;
		if(m_SDAN.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING229) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING229) + ogNotFormat;
		}
	}
	if(m_SDAS.GetStatus() == false) 
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING331) + ogNotFormat;
	}
	//*** 08.09.99 SHA ***
	/*
	if(m_CTRC.GetStatus() == false) 
	{
		ilStatus = false;
		if(m_CTRC.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING299) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING299) + ogNotFormat;
		}
	}*/

	//*** 07.09.99 SHA ***
	//*** NO ENTRY MAKES 0 MINUTES ***
	if(m_DURA.GetWindowTextLength() == 0) 
		m_DURA.SetInitText("0");
	if(m_DURA.GetStatus() == false) 
	{
		ilStatus = false;
		if(m_DURA.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING381) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING381) + ogNotFormat;
		}
	}
	if(m_REMA.GetStatus() == false) 
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING238) + ogNotFormat;
	}

	///////////////////////////////////////////////////////////////////
	//Konsistenz-Check//
	if(ilStatus == true)
	{
		CString olText;
		char clWhere[100];


		CCSPtrArray<BSDDATA> olBsdList;
		sprintf(clWhere,"WHERE BSDC='%s'",olText);
		if(ogBsdData.ReadSpecialData(&olBsdList,clWhere,"URNO,BSDC",false) == true)
		{
			ilStatus = false;
			olErrorText += LoadStg(IDS_STRING237) + LoadStg(IDS_EXISTINBSD);
		}
		olBsdList.DeleteAll();

		//*** 08.09.99 SHA ***
		/*
		CCSPtrArray<COTDATA> olCotList;
		m_CTRC.GetWindowText(olText);
		sprintf(clWhere,"WHERE CTRC='%s'",olText);
		if(ogCotData.ReadSpecialData(&olCotList, clWhere, "CTRC", false) ==false)
		{
			ilStatus = false;
			olErrorText += LoadStg(IDS_STRING349);
		}
		olCotList.DeleteAll();
		*/

		//Gültigkeit Code
		CCSPtrArray<ODADATA> olOdaCPA;
		m_SDAC.GetWindowText(olText);
		sprintf(clWhere,"WHERE SDAC='%s'",olText);
		if(ogOdaData.ReadSpecialData(&olOdaCPA,clWhere,"URNO,SDAC",false) == true)
		{
			if(olOdaCPA[0].Urno != pomOda->Urno)
			{
				ilStatus = false;
				olErrorText += LoadStg(IDS_STRING237) + LoadStg(IDS_EXIST);
			}
		}
		olOdaCPA.DeleteAll();

		//*** 09.09.99 SHA ***
		/*
		//Gültigkeit Code + Arbeitsvertragsart
		CString olCtrc = " ";
		if(m_CTRC.GetWindowTextLength() != 0)
		{
			m_CTRC.GetWindowText(olCtrc);
		}
		CCSPtrArray<ODADATA> olOdaCPA;
		m_SDAC.GetWindowText(olText);
		sprintf(clWhere,"WHERE SDAC='%s' AND CTRC='%s'", olText, olCtrc);
		if(ogOdaData.ReadSpecialData(&olOdaCPA,clWhere,"URNO",false) == true)
		{
			if(olOdaCPA[0].Urno != pomOda->Urno)
			{
				ilStatus = false;
				olErrorText += LoadStg(IDS_STRING617);
			}
		}
		olOdaCPA.DeleteAll();
		*/
	}
	///////////////////////////////////////////////////////////////////
	AfxGetApp()->DoWaitCursor(-1);
	if (ilStatus == true)
	{
		m_SDAC.GetWindowText(pomOda->Sdac,6);
		m_SDAK.GetWindowText(pomOda->Sdak,12);
		m_SDAN.GetWindowText(pomOda->Sdan,40);
		m_SDAS.GetWindowText(pomOda->Sdas,6);
		m_CTRC.GetWindowText(pomOda->Ctrc,6);
		m_DURA.GetWindowText(pomOda->Dura,4);

#ifdef CLIENT_HAJ
	//*** 08.09.99 SHA ***
	if (m_Free.GetCheck()==1)
		pomOda->Free[0]='x';
	else
		pomOda->Free[0]=' ';
#endif

		
		if		(m_R_Tatp1.GetCheck() == 1) pomOda->Tatp[0] = 'A';
		else if (m_R_Tatp2.GetCheck() == 1) pomOda->Tatp[0] = 'R';
		else if (m_R_Tatp3.GetCheck() == 1) pomOda->Tatp[0] = 'S';
		else if (m_R_Tatp4.GetCheck() == 1) pomOda->Tatp[0] = 'N';
		else							    pomOda->Tatp[0] = ' ';

  		if(m_C_Tsap.GetCheck() == 1) pomOda->Tsap[0] = 'Y';
		else						 pomOda->Tsap[0] = ' ';
		

		if(m_C_Upln.GetCheck() == 1) pomOda->Upln[0] = 'Y';
		else			 		     pomOda->Upln[0] = ' ';



		//wandelt Return in Blank//
		CString olTemp;
		m_REMA.GetWindowText(olTemp);
		for(int i=0;i>-1;)
		{
			i = olTemp.Find("\r\n");
			if(i != -1)
			{
				if(olTemp.GetLength() > (i+2))
					olTemp = olTemp.Left(i) + " " + olTemp.Mid(i+2);
				else
					olTemp = olTemp.Left(i);
			}
		}
		strcpy(pomOda->Rema,olTemp);
		////////////////////////////
		CDialog::OnOK();
	}	
	else
	{
		Beep(440,70);
		MessageBox(olErrorText,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
		m_SDAC.SetFocus();
		m_SDAC.SetSel(0,-1);
	}
}

//----------------------------------------------------------------------------------------

void DiensteUndAbwesenheitenDlg::OnBAwCot() 
{
	CStringArray olCol1, olCol2, olCol3;
	CCSPtrArray<COTDATA> olList;
	if(ogCotData.ReadSpecialData(&olList, "", "URNO,CTRC,CTRN", false) == true)
	{
		for(int i = 0; i < olList.GetSize(); i++)
		{
			COTDATA rlCot = olList[i];
			char pclUrno[20]="";
			sprintf(pclUrno, "%ld", olList[i].Urno);
			olCol3.Add(CString(pclUrno));
			olCol1.Add(CString(olList[i].Ctrc));
			olCol2.Add(CString(olList[i].Ctrn));
		}
		AwDlg *pomDlg = new AwDlg(&olCol1, &olCol2, &olCol3, LoadStg(IDS_STRING237), LoadStg(IDS_STRING229), this);
		if(pomDlg->DoModal() == IDOK)
		{
			m_CTRC.SetInitText(pomDlg->omReturnString, false);
			m_CTRC.SetFocus();
		}
		delete pomDlg;
	}
	else
	{
		MessageBox(LoadStg(IDS_STRING321), LoadStg(IDS_STRING149), (MB_ICONEXCLAMATION|MB_OK));
	}
	olList.DeleteAll();
}

//----------------------------------------------------------------------------------------

LONG DiensteUndAbwesenheitenDlg::OnKillfocus(UINT lParam, LONG wParam) 
{
	if(lParam == (UINT)m_CTRC.imID) 
	{
		if(m_CTRC.GetWindowTextLength() != 0) 
		{
			char clWhere[100];
			CCSPtrArray<COTDATA> olCotList;
			CString olTmp;m_CTRC.GetWindowText(olTmp);
			sprintf(clWhere,"WHERE CTRC='%s'",olTmp);
			if(ogCotData.ReadSpecialData(&olCotList, clWhere, "CTRC,CTRN", false) == true)
			{
				m_CTRC2.SetInitText(CString(olCotList[0].Ctrn));
			}
			else
			{
				m_CTRC2.SetInitText("");
			}
			olCotList.DeleteAll();
		}
		else
		{
			m_CTRC2.SetInitText("");
		}
	}
	return 0L;
}

//----------------------------------------------------------------------------------------

