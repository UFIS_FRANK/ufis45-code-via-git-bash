// VeryImpPers.cpp : implementation file
//

#include "stdafx.h"
#include "bdpsuif.h"
#include "VeryImpPers.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CVeryImpPers dialog
//---------------------------------------------------------------------------

CVeryImpPers::CVeryImpPers(VIPDATA *prpVip,CWnd* pParent /*=NULL*/)
	: CDialog(CVeryImpPers::IDD, pParent)
{
	pomVip = prpVip;

	//{{AFX_DATA_INIT(CVeryImpPers)

	//}}AFX_DATA_INIT
}


void CVeryImpPers::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CVeryImpPers)
	DDX_Control(pDX, IDOK, m_OK);
	DDX_Control(pDX, IDC_CDAT_D, m_CdatD);
	DDX_Control(pDX, IDC_CDAT_T, m_CdatT);
	DDX_Control(pDX, IDC_LSTU_D, m_LstuD);
	DDX_Control(pDX, IDC_LSTU_T, m_LstuT);
	DDX_Control(pDX, IDC_NOGR, m_NoGr);
	DDX_Control(pDX, IDC_NOPX, m_NoPx);
	DDX_Control(pDX, IDC_PAXN, m_Paxn);
	DDX_Control(pDX, IDC_PAXR, m_Paxr);
	DDX_Control(pDX, IDC_USEC, m_Usec);
	DDX_Control(pDX, IDC_USEU, m_Useu);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CVeryImpPers, CDialog)
	//{{AFX_MSG_MAP(CVeryImpPers)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CVeryImpPers message handlers
//---------------------------------------------------------------------------

BOOL CVeryImpPers::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	m_Caption = LoadStg(IDS_STRING203) + " <" + m_Caption + ">";
	SetWindowText(m_Caption);
	char clStat = ogPrivList.GetStat("VERYIMPPERSDLG.m_OK");
	SetWndStatAll(clStat,m_OK);

	//------------------------------------
	m_CdatD.SetBKColor(SILVER);
	m_CdatD.SetInitText(pomVip->Cdat.Format("%d.%m.%Y"));
	m_CdatD.SetSecState(ogPrivList.GetStat("VERYIMPPERSDLG.m_CDAT"));
	m_CdatT.SetBKColor(SILVER);
	m_CdatT.SetInitText(pomVip->Cdat.Format("%H:%M"));
	//m_CdatT.SetSecState(ogPrivList.GetStat("VERYIMPPERSDLG.m_CDAT"));
	//------------------------------------
	m_LstuD.SetBKColor(SILVER);
	m_LstuD.SetInitText(pomVip->Lstu.Format("%d.%m.%Y"));
	m_LstuD.SetSecState(ogPrivList.GetStat("VERYIMPPERSDLG.m_LSTU"));
	m_LstuT.SetBKColor(SILVER);
	m_LstuT.SetInitText(pomVip->Lstu.Format("%H:%M"));
	//m_LstuT.SetSecState(ogPrivList.GetStat("VERYIMPPERSDLG.m_LSTU"));
	//------------------------------------
	m_Usec.SetBKColor(SILVER);
	m_Usec.SetInitText(pomVip->Usec);
	//m_Usec.SetSecState(ogPrivList.GetStat("VERYIMPPERSDLG.m_USEC"));
	//------------------------------------
	m_Useu.SetBKColor(SILVER);
	m_Useu.SetInitText(pomVip->Useu);
	//m_Useu.SetSecState(ogPrivList.GetStat("VERYIMPPERSDLG.m_USEU"));
	//------------------------------------
	m_NoGr.SetFormat("x|#x|#x|#");
	m_NoGr.SetTextLimit(1,3);
	m_NoGr.SetBKColor(YELLOW);
	m_NoGr.SetTextErrColor(RED);
	m_NoGr.SetInitText(pomVip->Nogr);
	//m_NoGr.SetSecState(ogPrivList.GetStat("VERYIMPPERSDLG.m_NOGR"));
	//------------------------------------
	m_NoPx.SetFormat("x|#x|#x|#");
	m_NoPx.SetTextLimit(1,3);
	m_NoPx.SetBKColor(YELLOW);
	m_NoPx.SetTextErrColor(RED);
	m_NoPx.SetInitText(pomVip->Nopx);
	//m_NoPx.SetSecState(ogPrivList.GetStat("VERYIMPPERSDLG.m_NOPX"));
	//------------------------------------
	m_Paxn.SetTypeToString("X(64)",64,1);
	m_Paxn.SetTextErrColor(RED);
	m_Paxn.SetInitText(pomVip->Paxn);
	//m_Paxn.SetSecState(ogPrivList.GetStat("VERYIMPPERSDLG.m_PAXN"));
	//------------------------------------
	m_Paxr.SetTypeToString("X(64)",64,1);
	m_Paxr.SetTextErrColor(RED);
	m_Paxr.SetInitText(pomVip->Paxr);
	//m_Paxr.SetSecState(ogPrivList.GetStat("VERYIMPPERSDLG.m_PAXR"));
	//------------------------------------

	return TRUE;
}

//---------------------------------------------------------------------------

void CVeryImpPers::OnOK() 
{
	AfxGetApp()->DoWaitCursor(1);
	CString olErrorText;
	bool ilStatus = true;
	if(m_NoGr.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING599) + ogNotFormat;
	}
	if(m_NoPx.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING600) + ogNotFormat;// + CString(" 2")
	}
	if(m_Paxn.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING601) + ogNotFormat;
	}
	if(m_Paxr.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING602) + ogNotFormat;// + CString(" 2")
	}

	AfxGetApp()->DoWaitCursor(-1);
	if (ilStatus == true)
	{
		m_NoGr.GetWindowText(pomVip->Nogr,4);
		m_NoPx.GetWindowText(pomVip->Nopx,4);
		m_Paxn.GetWindowText(pomVip->Paxn,65);
		m_Paxr.GetWindowText(pomVip->Paxr,65);
		CDialog::OnOK();
	}
	else
	{
		Beep(440,70);
		MessageBox(olErrorText,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
		m_NoGr.SetFocus();
		m_NoGr.SetSel(0,-1);
	}
}

//---------------------------------------------------------------------------

void CVeryImpPers::OnCancel() 
{
	// TODO: Add extra cleanup here
	
	CDialog::OnCancel();
}

//---------------------------------------------------------------------------
