// AirportwetTableViewer.cpp 
//

#include "stdafx.h"
#include "AwiTableViewer.h"
#include "CcsDdx.h"
#include "resource.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif


// Local function prototype
static void AirportwetTableCf(void *popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

//-----------------------------------------------------------------------------------------------
// AirportwetTableViewer
//

AirportwetTableViewer::AirportwetTableViewer(CCSPtrArray<AWIDATA> *popData)
{
	pomData = popData;

	bmIsFromSearch = FALSE;
    pomAirPortWetTable = NULL;
    ogDdx.Register(this, AWI_CHANGE, CString("AirportwetTableViewer"), CString("Airportwet Update"), AirportwetTableCf);
    ogDdx.Register(this, AWI_NEW,    CString("AirportwetTableViewer"), CString("Airportwet New"),    AirportwetTableCf);
    ogDdx.Register(this, AWI_DELETE, CString("AirportwetTableViewer"), CString("Airportwet Delete"), AirportwetTableCf);
}

//-----------------------------------------------------------------------------------------------

AirportwetTableViewer::~AirportwetTableViewer()
{
	ogDdx.UnRegister(this, NOTUSED);
    DeleteAll();
	omHeaderDataArray.DeleteAll();
}

//-----------------------------------------------------------------------------------------------

void AirportwetTableViewer::Attach(CCSTable *popTable)
{
    pomAirPortWetTable = popTable;
}

//-----------------------------------------------------------------------------------------------

void AirportwetTableViewer::ChangeViewTo(const char *pcpViewName)
{
    DeleteAll();    
    MakeLines();
	UpdateDisplay();
}

//-----------------------------------------------------------------------------------------------

void AirportwetTableViewer::MakeLines()
{
	int ilAirportwetCount = pomData->GetSize();
	for (int ilLc = 0; ilLc < ilAirportwetCount; ilLc++)
	{
		AWIDATA *prlAirportwetData = &pomData->GetAt(ilLc);
		MakeLine(prlAirportwetData);
	}
}

//-----------------------------------------------------------------------------------------------

void AirportwetTableViewer::MakeLine(AWIDATA *prpAirportwet)
{

    //if( !IsPassFilter(prpAirportwet)) return;

    // Update viewer data for this shift record
    AIRPORTWETTABLE_LINEDATA rlAirportwet;

	rlAirportwet.Urno = prpAirportwet->Urno;
	rlAirportwet.Apc3 = prpAirportwet->Apc3;
	rlAirportwet.Apc4 = prpAirportwet->Apc4;
	rlAirportwet.Humi = prpAirportwet->Humi;
	rlAirportwet.Temp = prpAirportwet->Temp;
	rlAirportwet.Visi = prpAirportwet->Visi;
	rlAirportwet.Wcod = prpAirportwet->Wcod;
	rlAirportwet.Wdir = prpAirportwet->Wdir;
	rlAirportwet.Wind = prpAirportwet->Wind;
	rlAirportwet.Msgt = prpAirportwet->Msgt;
	rlAirportwet.Vafr = prpAirportwet->Vafr.Format("%d.%m.%Y %H:%M");
	rlAirportwet.Vato = prpAirportwet->Vafr.Format("%d.%m.%Y %H:%M");


	CreateLine(&rlAirportwet);
}

//-----------------------------------------------------------------------------------------------

void AirportwetTableViewer::CreateLine(AIRPORTWETTABLE_LINEDATA *prpAirportwet)
{
    int ilLineCount = omLines.GetSize();
    for (int ilLineno = ilLineCount; ilLineno > 0; ilLineno--)
        if (CompareAirportwet(prpAirportwet, &omLines[ilLineno-1]) >= 0)
	        break;							 // should be inserted after Lines[ilLineno-1]
	AIRPORTWETTABLE_LINEDATA rlAirportwet;
	rlAirportwet = *prpAirportwet;
    omLines.NewAt(ilLineno, rlAirportwet);
}

//-----------------------------------------------------------------------------------------------
// UpdateDisplay: Load data selected by filter conditions to the display by using "omTable"

void AirportwetTableViewer::UpdateDisplay()
{
	int ilLines = omLines.GetSize();
	const int ilColumns = 10;	//<===== Spaltenanzahl setzen (von 0 ab)
	omHeaderDataArray.DeleteAll();

	pomAirPortWetTable->SetShowSelection(TRUE);
	pomAirPortWetTable->ResetContent();

	TABLE_HEADER_COLUMN rlHeader;
	rlHeader.Alignment = COLALIGN_LEFT;
	rlHeader.Font = &ogCourier_Regular_10;

	int ilPos = 1;
	rlHeader.Length = 25; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING612),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 33; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING612),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 25; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING612),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 25; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING612),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 25; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING612),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 25; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING612),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 25; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING612),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 25; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING612),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 400; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING612),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 133; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING612),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 133; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING612),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);

	pomAirPortWetTable->SetHeaderFields(omHeaderDataArray);

	pomAirPortWetTable->SetDefaultSeparator();
	pomAirPortWetTable->SetTableEditable(FALSE);

	TABLE_COLUMN rlColumnData;
	rlColumnData.Font = &ogCourier_Regular_10;
	rlColumnData.BkColor = WHITE;
	rlColumnData.Alignment = COLALIGN_LEFT;
	
    for (int ilLineNo = 0; ilLineNo < ilLines; ilLineNo++)
	{
		CCSPtrArray<TABLE_COLUMN> olColList;

		rlColumnData.Text = omLines[ilLineNo].Apc3;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Apc4;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Humi;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Temp;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Visi;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Wcod;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Wdir;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Wind;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Msgt;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Vafr;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Vato;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		pomAirPortWetTable->AddTextLine(olColList,(void*)NULL);
		olColList.DeleteAll();
	}
	pomAirPortWetTable->DisplayTable();
}

//-----------------------------------------------------------------------------------------------

CString AirportwetTableViewer::Format(AIRPORTWETTABLE_LINEDATA *prpLine)
{
    CString s;
	// ???
    return s;
}

//-----------------------------------------------------------------------------------------------

bool AirportwetTableViewer::FindAirportwet(char *pcpAirportwetKeya, char *pcpAirportwetKeyd, int& ilItem)
{
	int ilCount = omLines.GetSize();
    for (ilItem = 0; ilItem < ilCount; ilItem++)
    {
/*		if ( (omLines[ilItem].Keya == pcpAirportwetKeya) &&
			 (omLines[ilItem].Keyd == pcpAirportwetKeyd)    )
			return TRUE;*/
	}
	return false;
}

//-----------------------------------------------------------------------------------------------

static void AirportwetTableCf(void *popInstance, int ipDDXType,
    void *vpDataPointer, CString &ropInstanceName)
{
    AirportwetTableViewer *polViewer = (AirportwetTableViewer *)popInstance;
    if (ipDDXType == AWI_CHANGE || ipDDXType == AWI_NEW) polViewer->ProcessAirportwetChange((AWIDATA *)vpDataPointer);
    if (ipDDXType == AWI_DELETE) polViewer->ProcessAirportwetDelete((AWIDATA *)vpDataPointer);
} 


//-----------------------------------------------------------------------------------------------

void AirportwetTableViewer::ProcessAirportwetChange(AWIDATA *prpAirportwet)
{
	int ilItem = 0;
	int ilColumnNo = 0;
	TABLE_COLUMN rlColumn;
	CCSPtrArray <TABLE_COLUMN> olLine;

	rlColumn.Lineno = ilItem;
	rlColumn.VerticalSeparator = SEPA_NONE;
	rlColumn.SeparatorType = SEPA_NONE;
	rlColumn.BkColor = WHITE;
	rlColumn.Font = &ogCourier_Regular_10;
	rlColumn.Alignment = COLALIGN_LEFT;

	rlColumn.Text = prpAirportwet->Apc3;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpAirportwet->Apc4;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpAirportwet->Humi;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpAirportwet->Temp;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpAirportwet->Visi;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpAirportwet->Wcod;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpAirportwet->Wdir;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpAirportwet->Wind;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpAirportwet->Msgt;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpAirportwet->Vafr.Format("%d.%m.%Y %H:%M");;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpAirportwet->Vato.Format("%d.%m.%Y %H:%M");;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	
	if (FindLine(prpAirportwet->Urno, ilItem))
	{
        AIRPORTWETTABLE_LINEDATA *prlLine = &omLines[ilItem];

		prlLine->Urno = prpAirportwet->Urno;
		prlLine->Apc3 = prpAirportwet->Apc3;
		prlLine->Apc4 = prpAirportwet->Apc4;
		prlLine->Humi = prpAirportwet->Humi;
		prlLine->Temp = prpAirportwet->Temp;
		prlLine->Visi = prpAirportwet->Visi;
		prlLine->Wcod = prpAirportwet->Wcod;
		prlLine->Wdir = prpAirportwet->Wdir;
		prlLine->Wind = prpAirportwet->Wind;
		prlLine->Msgt = prpAirportwet->Msgt;
		prlLine->Vafr = prpAirportwet->Vafr.Format("%d.%m.%Y %H:%M");
		prlLine->Vato = prpAirportwet->Vafr.Format("%d.%m.%Y %H:%M");



		pomAirPortWetTable->ChangeTextLine(ilItem, &olLine, (void *)prlLine);
		pomAirPortWetTable->DisplayTable();
	}
	else
	{
		MakeLine(prpAirportwet);
		if (FindLine(prpAirportwet->Urno, ilItem))
		{
	        AIRPORTWETTABLE_LINEDATA *prlLine = &omLines[ilItem];
			if(prlLine != NULL)
			{
				pomAirPortWetTable->AddTextLine(olLine, (void *)prlLine);
				pomAirPortWetTable->DisplayTable();
			}
		}
	}
	olLine.DeleteAll();
}

//-----------------------------------------------------------------------------------------------

void AirportwetTableViewer::ProcessAirportwetDelete(AWIDATA *prpAirportwet)
{
	int ilItem;
	if (FindLine(prpAirportwet->Urno, ilItem))
	{
		DeleteLine(ilItem);
		pomAirPortWetTable->DeleteTextLine(ilItem);
		pomAirPortWetTable->DisplayTable();
	}
}

//-----------------------------------------------------------------------------------------------

bool AirportwetTableViewer::IsPassFilter(AWIDATA *prpAirportwet)
{

	return true;
}

//-----------------------------------------------------------------------------------------------

int AirportwetTableViewer::CompareAirportwet(AIRPORTWETTABLE_LINEDATA *prpAirportwet1, AIRPORTWETTABLE_LINEDATA *prpAirportwet2)
{
/*	int ilCase=0;
	int	ilCompareResult;
	if(prpAirportwet1->Tifd == prpAirportwet2->Tifd)
	{
		ilCompareResult= 0;
	}
	else
	{
		if(prpAirportwet1->Tifd > prpAirportwet2->Tifd)
		{
			ilCompareResult = 1;
		}
		else
		{
			ilCompareResult = -1;
		}
	}
	return ilCompareResult;
*/  
	return 0;
}

//-----------------------------------------------------------------------------------------------

void AirportwetTableViewer::DeleteLine(int ipLineno)
{
	omLines.DeleteAt(ipLineno);
}

//-----------------------------------------------------------------------------------------------

bool AirportwetTableViewer::FindLine(long lpUrno, int &rilLineno)
{
    for (rilLineno = 0; rilLineno < omLines.GetSize(); rilLineno++)
	{
        if (omLines[rilLineno].Urno == lpUrno)
            return true;
	}
    return false;
}
//-----------------------------------------------------------------------------------------------

void AirportwetTableViewer::DeleteAll()
{
    while (omLines.GetSize() > 0)
    DeleteLine(0);
}

//-----------------------------------------------------------------------------------------------
// Printing Routines
//-----------------------------------------------------------------------------------------------

void AirportwetTableViewer::PrintTableView()
{
	CString olFooter1,olFooter2;
	omTableName = LoadStg(IDS_STRING201);
	int ilOrientation = PRINT_LANDSCAPE;//  PRINT_PORTRAET  PRINT_LANDSCAPE
	pomPrint = new CCSPrint((CWnd*)this,ilOrientation,45);// FirstLine(P=200,L=200),LeftOffset(P=200,L=50)
	//pomPrint->imMaxLines = 38;  // (P=57,L=38)
	//omBitmap.LoadBitmap(IDB_HAJLOGO);
	//pomPrint->SetBitmaps(&omBitmap,&omBitmap);
	if (pomPrint != NULL)
	{
		if (pomPrint->InitializePrinter(ilOrientation) == TRUE)
		{
			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
			pomPrint->imLineNo = pomPrint->imMaxLines + 1;
			DOCINFO	rlDocInfo;
			memset(&rlDocInfo, 0, sizeof(DOCINFO));
			rlDocInfo.cbSize = sizeof( DOCINFO );
			rlDocInfo.lpszDocName = omTableName;	
			pomPrint->omCdc.StartDoc( &rlDocInfo );
			pomPrint->imPageNo = 0;
			int ilLines = omLines.GetSize();
			olFooter1.Format("%d %s",ilLines,omTableName);
			for(int i = 0; i < ilLines; i++ ) 
			{
				if(pomPrint->imLineNo >= pomPrint->imMaxLines)
				{
					if(pomPrint->imPageNo > 0)
					{
						olFooter2.Format("%s: %d",LoadStg(IDS_STRING222),pomPrint->imPageNo);
						pomPrint->PrintUIFFooter(olFooter1,"",olFooter2);
						pomPrint->omCdc.EndPage();
					}
					PrintTableHeader();
				}
				if(pomPrint->imLineNo == (pomPrint->imMaxLines-1) || i == (ilLines-1))
				{
					PrintTableLine(&omLines[i],true);
				}
				else
				{
					PrintTableLine(&omLines[i],false);
				}
			}
			olFooter2.Format("%s: %d",LoadStg(IDS_STRING222),pomPrint->imPageNo);
			pomPrint->PrintUIFFooter(olFooter1,"",olFooter2);
			pomPrint->omCdc.EndPage();
			pomPrint->omCdc.EndDoc();
		}
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
	delete pomPrint;
	//omBitmap.DeleteObject();
	pomPrint = NULL;
	}
}

//-----------------------------------------------------------------------------------------------

bool AirportwetTableViewer::PrintTableHeader()
{
	pomPrint->omCdc.StartPage();
	pomPrint->imPageNo++;
	pomPrint->imLineNo = 0;

	pomPrint->PrintUIFHeader(omTableName,CString(CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M")),pomPrint->imFirstLine-10);

	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;

	rlElement.Alignment   = PRINT_LEFT;
	rlElement.FrameLeft   = PRINT_NOFRAME;
	rlElement.FrameRight  = PRINT_NOFRAME;
	rlElement.FrameTop    = PRINT_NOFRAME;
	rlElement.FrameBottom = PRINT_FRAMEMEDIUM;
	rlElement.pFont       = &pomPrint->ogCourierNew_Bold_8;

	int ilSize = omHeaderDataArray.GetSize();
	for(int i=0;i<ilSize;i++)
	{
		//if(i!=1&&i!=2)// <= setzen, wenn Spalten nicht angezeigt werden sollen 
		{
			rlElement.Length = (int)(omHeaderDataArray[i].Length*dgCCSPrintFactor);
			if(rlElement.Length < igCCSPrintMinLength)rlElement.Length+=igCCSPrintMoreLength; 
			rlElement.Text   = omHeaderDataArray[i].Text;
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
		}
	}
	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();


	return true;
}

//-----------------------------------------------------------------------------------------------

bool AirportwetTableViewer::PrintTableLine(AIRPORTWETTABLE_LINEDATA *prpLine,bool bpLastLine)
{
	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;
	rlElement.pFont      = &pomPrint->ogCourierNew_Regular_8;
	int ilSize = omHeaderDataArray.GetSize();
	for(int i=0;i<ilSize;i++)
	{
		//if(i!=1&&i!=1)// <= setzen, wenn Spalten nicht angezeigt werden sollen 
		{
			rlElement.Length = (int)(omHeaderDataArray[i].Length*dgCCSPrintFactor);
			if(rlElement.Length < igCCSPrintMinLength)rlElement.Length+=igCCSPrintMoreLength; 
			rlElement.Alignment  = PRINT_LEFT;
			rlElement.FrameTop   = PRINT_FRAMETHIN;
			if(bpLastLine == true)
			{
				rlElement.FrameBottom = PRINT_FRAMETHIN;
			}
			else
			{
				rlElement.FrameBottom = PRINT_NOFRAME;
			}
			rlElement.FrameLeft  = PRINT_NOFRAME;
			rlElement.FrameRight = PRINT_NOFRAME;

			switch(i)
			{
			case 0:
				rlElement.FrameLeft  = PRINT_FRAMETHIN;
				rlElement.Text		 = prpLine->Apc3;
				break;
			case 1:
				rlElement.Text		 = prpLine->Apc4;
				break;
			case 2:
				rlElement.Text		 = prpLine->Humi;
				break;
			case 3:
				rlElement.Text		 = prpLine->Temp;
				break;
			case 4:
				rlElement.Text		 = prpLine->Visi;
				break;
			case 5:
				rlElement.Text		 = prpLine->Wcod;
				break;
			case 6:
				rlElement.Text		 = prpLine->Wdir;
				break;
			case 7:
				rlElement.Text		 = prpLine->Wind;
				break;
			case 8:
				rlElement.Text		 = prpLine->Msgt;
				break;
			case 9:
				rlElement.Text		 = prpLine->Vafr;
				break;
			case 10:
				rlElement.FrameRight = PRINT_FRAMETHIN;
				rlElement.Text		 = prpLine->Vato;
				break;
			}
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
		}
	}
	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();
	return true;
}

//-----------------------------------------------------------------------------------------------
