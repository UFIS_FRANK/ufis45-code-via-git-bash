// CedaInitModuData.h

#ifndef __CEDAINITMODUDATA__
#define __CEDAINITMODUDATA__
 
#include "stdafx.h"
#include "basicdata.h"

//--Class declaratino-------------------------------------------------------------------------------------------------------

class CedaInitModuData: public CCSCedaData
{
public:

// Operations
	CedaInitModuData();
	~CedaInitModuData();

	bool SendInitModu();
	CString GetInitModuTxt1();
	CString GetInitModuTxt2();
	CString GetInitModuTxt3();
	CString GetInitModuTxt4();
	CString GetInitModuTxt5();
	CString GetInitModuTxt6();
	CString GetInitModuTxt7();
	CString GetInitModuTxt8();
	CString GetInitModuTxt9();
	CString GetInitModuTxt10();

// Variaben
	char pcmInitModuFieldList[200];

	CString om_Tab;
	CString om_Dlg;
	CString om_ShBut;
	CString om_ShTab;
	CString om_ButVie;
	CString om_ComVie;
	CString om_ButIns;
	CString om_ButCha;
	CString om_ButDel;
	CString om_ButCop;
	CString om_ButRep;
	CString om_ButPri;
	CString	om_ButExc;
	CString om_OK;
	CString om_T_ALT;
	CString om_T_ACT;
	CString om_T_ACR;
	CString om_T_APT;
	CString om_T_RWY;
	CString om_T_TWY;
	CString om_T_PST;
	CString om_T_GAT;
	CString om_T_CIC;
	CString om_T_BLT;
	CString om_T_EXT;
	CString om_T_DEN;
	CString om_T_MVT;
	CString om_T_NAT;
	CString om_T_HAG;
	CString om_T_WRO;
	CString om_T_HAT;
	CString om_T_STY;
	CString om_T_FID;
	CString om_T_SPH_N;
	CString om_T_STR;
	CString om_T_SEA;
	CString om_T_GHS;
	CString om_T_PER;
	CString om_T_ORG;
	CString om_T_COT;
	CString om_T_ASF;
	CString om_T_BSD;
	CString om_T_PFC;
	CString om_T_TEA;
	CString om_T_PRC;
	CString om_T_ODA;
	CString om_T_STF;
	CString om_T_GEG;
	CString om_T_WAY;
	CString om_T_SPH_H;
	CString om_T_CHT;
	CString om_T_TIP;
	CString om_T_HOL;
	CString om_T_WGP;
	CString om_T_PGP;
	CString om_T_AWI;
	CString om_T_CCC;
	CString om_T_VIP;
	CString om_T_AFM;
	CString om_T_ENT;

	CString om_F_CDAT;
	CString om_F_USEC;
	CString om_F_LSTU;
	CString om_F_USEU;
	CString om_F_VAFR;
	CString om_F_VATO;
	CString om_F_GRUP;
	CString om_F_NAFR;
	CString om_F_NATO;
	CString om_F_REMA;
	CString om_F_BEME;
	CString om_F_CODE;
	CString om_F_NAME;
	CString om_F_KNAM;
	CString om_F_NOAVC;
	CString om_F_NOAVD;
	CString om_F_NOAVN;
	CString om_F_TYPE;

};

//---------------------------------------------------------------------------------------------------------

extern CedaInitModuData ogInitModuData;


#endif //__CEDAINITMODUDATA__

