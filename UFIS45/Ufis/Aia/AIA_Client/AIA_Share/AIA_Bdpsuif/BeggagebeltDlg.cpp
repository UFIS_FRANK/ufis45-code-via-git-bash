// BeggagebeltDlg.cpp : implementation file
//

#include "stdafx.h"
#include "BDPSUIF.h"
#include "BeggagebeltDlg.h"
#include "NotAvailableDlg.h"
#include "PrivList.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// BeggagebeltDlg dialog


BeggagebeltDlg::BeggagebeltDlg(BLTDATA *popBLT,CWnd* pParent /*=NULL*/) : CDialog(BeggagebeltDlg::IDD, pParent)
{
	pomBLT = popBLT;
	
	pomTable = NULL;
	pomTable = new CCSTable;
	
	//{{AFX_DATA_INIT(BeggagebeltDlg)
	//}}AFX_DATA_INIT
}

BeggagebeltDlg::~BeggagebeltDlg()
{
	omBlkPtrA.DeleteAll();
	omDeleteBlkPtrA.DeleteAll();
	delete pomTable;
}

void BeggagebeltDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(BeggagebeltDlg)
	DDX_Control(pDX, IDC_NOAVNEW, m_NOAVNEW);
	DDX_Control(pDX, IDC_NOAVDEL, m_NOAVDEL);
	DDX_Control(pDX, IDC_NOAV,	 m_NOAVFRAME);
	DDX_Control(pDX, IDC_DEFD, m_DEFD);
	DDX_Control(pDX, IDC_MXF, m_MXF);
	DDX_Control(pDX, IDOK, m_OK);
	DDX_Control(pDX, IDC_CDAT_D, m_CDATD);
	DDX_Control(pDX, IDC_CDAT_T, m_CDATT);
	DDX_Control(pDX, IDC_LSTU_D, m_LSTUD);
	DDX_Control(pDX, IDC_LSTU_T, m_LSTUT);
	DDX_Control(pDX, IDC_USEU,	 m_USEU);
	DDX_Control(pDX, IDC_USEC,	 m_USEC);
	DDX_Control(pDX, IDC_BNAM,	 m_BNAM);
	DDX_Control(pDX, IDC_TELE,	 m_TELE);
	DDX_Control(pDX, IDC_VAFR_D, m_VAFRD);
	DDX_Control(pDX, IDC_VAFR_T, m_VAFRT);
	DDX_Control(pDX, IDC_VATO_D, m_VATOD);
	DDX_Control(pDX, IDC_VATO_T, m_VATOT);
	DDX_Control(pDX, IDC_GRUP,	 m_GRUP);
	DDX_Control(pDX, IDC_TERM,	 m_TERM);
	DDX_Control(pDX, IDC_STAT,	 m_STAT);
	DDX_Control(pDX, IDC_BLTT,	 m_BLTT);
	DDX_Control(pDX, IDC_HOME,	 m_HOME);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(BeggagebeltDlg, CDialog)
	//{{AFX_MSG_MAP(BeggagebeltDlg)
	ON_MESSAGE(WM_TABLE_LBUTTONDBLCLK,	OnTableLButtonDblclk)
	ON_BN_CLICKED(IDC_NOAVDEL, OnNoavDel)
	ON_BN_CLICKED(IDC_NOAVNEW, OnNoavNew)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// BeggagebeltDlg message handlers
//----------------------------------------------------------------------------------------

BOOL BeggagebeltDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	m_Caption = LoadStg(IDS_STRING179) + " <" + m_Caption + ">";
	SetWindowText(m_Caption);

	char clStat = ogPrivList.GetStat("BEGGAGEBELTDLG.m_OK");
	SetWndStatAll(clStat,m_OK);

	//------------------------------------
	m_CDATD.SetBKColor(SILVER);
	m_CDATD.SetInitText(pomBLT->Cdat.Format("%d.%m.%Y"));
	m_CDATD.SetSecState(ogPrivList.GetStat("BEGGAGEBELTDLG.m_CDAT"));
	m_CDATT.SetBKColor(SILVER);
	m_CDATT.SetInitText(pomBLT->Cdat.Format("%H:%M"));
	m_CDATT.SetSecState(ogPrivList.GetStat("BEGGAGEBELTDLG.m_CDAT"));
	//------------------------------------
	m_LSTUD.SetBKColor(SILVER);
	m_LSTUD.SetInitText(pomBLT->Lstu.Format("%d.%m.%Y"));
	m_LSTUD.SetSecState(ogPrivList.GetStat("BEGGAGEBELTDLG.m_LSTU"));
	m_LSTUT.SetBKColor(SILVER);
	m_LSTUT.SetInitText(pomBLT->Lstu.Format("%H:%M"));
	m_LSTUT.SetSecState(ogPrivList.GetStat("BEGGAGEBELTDLG.m_LSTU"));
	//------------------------------------
	m_USEC.SetBKColor(SILVER);
	m_USEC.SetInitText(pomBLT->Usec);
	m_USEC.SetSecState(ogPrivList.GetStat("BEGGAGEBELTDLG.m_USEC"));
	//------------------------------------
	m_USEU.SetBKColor(SILVER);
	m_USEU.SetInitText(pomBLT->Useu);
	m_USEU.SetSecState(ogPrivList.GetStat("BEGGAGEBELTDLG.m_USEU"));
	//------------------------------------
	m_GRUP.SetBKColor(SILVER);
	//m_GRUP.SetInitText(pomBLT->);
	m_GRUP.SetSecState(ogPrivList.GetStat("BEGGAGEBELTDLG.m_GRUP"));
	//------------------------------------//WHITE,YELLOW,SILVER,RED,LYELLOW
	m_BNAM.SetFormat("x|#x|#x|#x|#x|#");
	m_BNAM.SetTextLimit(1,5);
	m_BNAM.SetBKColor(YELLOW);
	m_BNAM.SetTextErrColor(RED);
	m_BNAM.SetInitText(pomBLT->Bnam);
	m_BNAM.SetSecState(ogPrivList.GetStat("BEGGAGEBELTDLG.m_BNAM"));
	//------------------------------------
	/*m_NAFRD.SetTypeToDate();
	m_NAFRD.SetTextErrColor(RED);
	m_NAFRD.SetInitText(pomBLT->Nafr.Format("%d.%m.%Y"));
	m_NAFRD.SetSecState(ogPrivList.GetStat("BEGGAGEBELTDLG.m_NAFR"));
	// - - - - - - - - - - - - - - - - - -
	m_NAFRT.SetTypeToTime();
	m_NAFRT.SetTextErrColor(RED);
	m_NAFRT.SetInitText(pomBLT->Nafr.Format("%H:%M"));
	m_NAFRT.SetSecState(ogPrivList.GetStat("BEGGAGEBELTDLG.m_NAFR"));
	//------------------------------------
	m_NATOD.SetTypeToDate();
	m_NATOD.SetTextErrColor(RED);
	m_NATOD.SetInitText(pomBLT->Nato.Format("%d.%m.%Y"));
	m_NATOD.SetSecState(ogPrivList.GetStat("BEGGAGEBELTDLG.m_NATO"));
	// - - - - - - - - - - - - - - - - - -
	m_NATOT.SetTypeToTime();
	m_NATOT.SetTextErrColor(RED);
	m_NATOT.SetInitText(pomBLT->Nato.Format("%H:%M"));
	m_NATOT.SetSecState(ogPrivList.GetStat("BEGGAGEBELTDLG.m_NATO"));
	//------------------------------------
	m_RESN.SetTypeToString("X(40)",40,0);
	m_RESN.SetTextErrColor(RED);
	m_RESN.SetInitText(pomBLT->Resn);
	m_RESN.SetSecState(ogPrivList.GetStat("BEGGAGEBELTDLG.m_RESN"));*/
	//------------------------------------
	m_STAT.SetTypeToString("X(3)",3,1);
	m_STAT.SetTextErrColor(RED);
	m_STAT.SetInitText(pomBLT->Stat);
	m_STAT.SetSecState(ogPrivList.GetStat("BEGGAGEBELTDLG.m_STAT"));
	//------------------------------------
	m_BLTT.SetTypeToString("X(2)",2,1);
	m_BLTT.SetTextErrColor(RED);
	m_BLTT.SetInitText(pomBLT->Bltt);
	m_BLTT.SetSecState(ogPrivList.GetStat("BEGGAGEBELTDLG.m_BLTT"));
	//------------------------------------
	m_HOME.SetTypeToString("X(3)",3,1);
	m_HOME.SetTextErrColor(RED);
	m_HOME.SetInitText(pomBLT->Home);
	m_HOME.SetSecState(ogPrivList.GetStat("BEGGAGEBELTDLG.m_HOME"));
	//------------------------------------
	m_TERM.SetFormat("x|#");
	m_TERM.SetTextLimit(0,1);
	m_TERM.SetTextErrColor(RED);
	m_TERM.SetInitText(pomBLT->Term);
	m_TERM.SetSecState(ogPrivList.GetStat("BEGGAGEBELTDLG.m_TERM"));
	//------------------------------------
	m_TELE.SetTypeToString("X(10)",10,0);
	m_TELE.SetTextErrColor(RED);
	m_TELE.SetInitText(pomBLT->Tele);
	m_TELE.SetSecState(ogPrivList.GetStat("BEGGAGEBELTDLG.m_TELE"));
	//------------------------------------
	m_VAFRD.SetTypeToDate(true);
	m_VAFRD.SetTextErrColor(RED);
	m_VAFRD.SetBKColor(YELLOW);
	m_VAFRD.SetInitText(pomBLT->Vafr.Format("%d.%m.%Y"));
	m_VAFRD.SetSecState(ogPrivList.GetStat("BEGGAGEBELTDLG.m_VAFR"));
	// - - - - - - - - - - - - - - - - - -
	m_VAFRT.SetTypeToTime(true);
	m_VAFRT.SetTextErrColor(RED);
	m_VAFRT.SetBKColor(YELLOW);
	m_VAFRT.SetInitText(pomBLT->Vafr.Format("%H:%M"));
	m_VAFRT.SetSecState(ogPrivList.GetStat("BEGGAGEBELTDLG.m_VAFR"));
	//------------------------------------
	m_VATOD.SetTypeToDate();
	m_VATOD.SetTextErrColor(RED);
	m_VATOD.SetInitText(pomBLT->Vato.Format("%d.%m.%Y"));
	m_VATOD.SetSecState(ogPrivList.GetStat("BEGGAGEBELTDLG.m_VATO"));
	// - - - - - - - - - - - - - - - - - -
	m_VATOT.SetTypeToTime();
	m_VATOT.SetTextErrColor(RED);
	m_VATOT.SetInitText(pomBLT->Vato.Format("%H:%M"));
	m_VATOT.SetSecState(ogPrivList.GetStat("BEGGAGEBELTDLG.m_VATO"));
	//------------------------------------

	m_MXF.SetFormat("#");
	m_MXF.SetTextLimit(0,1);
	m_MXF.SetTextErrColor(RED);
	m_MXF.SetInitText(pomBLT->Maxf);

	m_DEFD.SetFormat("###");
	m_DEFD.SetTextLimit(0,3);
	m_DEFD.SetTextErrColor(RED);
	m_DEFD.SetInitText(pomBLT->Defd);
	m_DEFD.SetSecState(ogPrivList.GetStat("BEGGAGEBELTDLG.m_DEFD"));

	CString olBurn;
	olBurn.Format("%d",pomBLT->Urno);
	MakeNoavTable("BLT", olBurn);

	return TRUE;
}

//----------------------------------------------------------------------------------------
void BeggagebeltDlg::OnNoavNew() 
{
		BLKDATA rlBlk;
		strcpy(rlBlk.Tabn, CString("BLT"));
		NotAvailableDlg *polDlg = new NotAvailableDlg(&rlBlk,this);
		m_BNAM.GetWindowText(polDlg->omName);
		if(polDlg->DoModal() == IDOK)
		{
			BLKDATA *prlBlk = new BLKDATA;
			*prlBlk = rlBlk;
			omBlkPtrA.Add(prlBlk);
			prlBlk->IsChanged = DATA_NEW;
			ChangeNoavTable(prlBlk, -1);
		}
		delete polDlg;
}

void BeggagebeltDlg::OnNoavDel() 
{
	int ilLineNo = pomTable->GetCurSel();
	if (ilLineNo == -1)
	{
		MessageBox(LoadStg(IDS_STRING148),LoadStg(IDS_STRING149),MB_ICONEXCLAMATION);
	}
	else
	{
		if (&omBlkPtrA[ilLineNo] != NULL)
		{
			BLKDATA *prlBlk = new BLKDATA;
			*prlBlk = omBlkPtrA[ilLineNo];
			prlBlk->IsChanged = DATA_DELETED;
			omDeleteBlkPtrA.Add(prlBlk);
			omBlkPtrA.DeleteAt(ilLineNo);
			ChangeNoavTable(NULL, ilLineNo);
		}
	}
}

void BeggagebeltDlg::OnOK() 
{
	AfxGetApp()->DoWaitCursor(1);
	CString olErrorText;
	bool ilStatus = true;
	if(m_BNAM.GetStatus() == false)
	{
		ilStatus = false;
		if(m_BNAM.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING288) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING288) + ogNotFormat;
		}
	}
	/*if(m_NAFRD.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING364) + LoadStg(IDS_STRING232) + ogNotFormat;
	}
	if(m_NAFRT.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING364) + LoadStg(IDS_STRING233) + ogNotFormat;
	}
	if(m_NATOD.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING365) + LoadStg(IDS_STRING232) + ogNotFormat;
	}
	if(m_NATOT.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING365) + LoadStg(IDS_STRING233) + ogNotFormat;
	}
	if(m_RESN.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING366) + ogNotFormat;
	}*/
	if(m_TERM.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING296) + ogNotFormat;
	}
	if(m_TELE.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING294) + ogNotFormat;
	}
	if(m_VAFRD.GetStatus() == false)
	{
		ilStatus = false;
		if(m_VAFRD.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING230) + LoadStg(IDS_STRING232) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING230) + LoadStg(IDS_STRING232) + ogNotFormat;
		}
	}
	if(m_VAFRT.GetStatus() == false)
	{
		ilStatus = false;
		if(m_VAFRT.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING230) + LoadStg(IDS_STRING233) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING230) + LoadStg(IDS_STRING233) + ogNotFormat;
		}
	}
	if(m_VATOD.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING231) + LoadStg(IDS_STRING232) + ogNotFormat;
	}
	if(m_VATOT.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING231) + LoadStg(IDS_STRING233) + ogNotFormat;
	}

	///////////////////////////////////////////////////////////////////////////
	CString /*olNafrd,olNafrt,olNatod,olNatot,*/ olVafrd,olVafrt,olVatod,olVatot;
	CTime olTmpTimeFr,olTmpTimeTo;
	//m_NAFRD.GetWindowText(olNafrd);
	//m_NAFRT.GetWindowText(olNafrt);
	//m_NATOD.GetWindowText(olNatod);
	//m_NATOT.GetWindowText(olNatot);
	m_VAFRD.GetWindowText(olVafrd);
	m_VAFRT.GetWindowText(olVafrt);
	m_VATOD.GetWindowText(olVatod);
	m_VATOT.GetWindowText(olVatot);

	if((m_VAFRD.GetWindowTextLength() != 0 && m_VAFRT.GetWindowTextLength() == 0) || (m_VAFRD.GetWindowTextLength() == 0 && m_VAFRT.GetWindowTextLength() != 0))
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING234);
	}
	if((m_VATOD.GetWindowTextLength() != 0 && m_VATOT.GetWindowTextLength() == 0) || (m_VATOD.GetWindowTextLength() == 0 && m_VATOT.GetWindowTextLength() != 0))
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING235);
	}
	if(m_VAFRD.GetStatus() == true && m_VAFRT.GetStatus() == true && m_VATOD.GetStatus() == true && m_VATOT.GetStatus() == true)
	{
		olTmpTimeFr = DateHourStringToDate(olVafrd,olVafrt);
		olTmpTimeTo = DateHourStringToDate(olVatod,olVatot);
		if(olTmpTimeTo != -1 && (olTmpTimeTo < olTmpTimeFr || olTmpTimeFr == -1))
		{
			ilStatus = false;
			olErrorText += LoadStg(IDS_STRING236);
		}
	}

	/*if((m_NAFRD.GetWindowTextLength() != 0 && m_NAFRT.GetWindowTextLength() == 0) || (m_NAFRD.GetWindowTextLength() == 0 && m_NAFRT.GetWindowTextLength() != 0))
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING291);
	}
	if((m_NATOD.GetWindowTextLength() != 0 && m_NATOT.GetWindowTextLength() == 0) || (m_NATOD.GetWindowTextLength() == 0 && m_NATOT.GetWindowTextLength() != 0))
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING292);
	}
	if(m_NAFRD.GetStatus() == true && m_NAFRT.GetStatus() == true && m_NATOD.GetStatus() == true && m_NATOT.GetStatus() == true)
	{
		olTmpTimeFr = DateHourStringToDate(olNafrd,olNafrt);
		olTmpTimeTo = DateHourStringToDate(olNatod,olNatot);
		if(olTmpTimeTo != -1 && (olTmpTimeTo < olTmpTimeFr || olTmpTimeFr == -1))
		{
			ilStatus = false;
			olErrorText += LoadStg(IDS_STRING293);
		}
	}*/

	//Konsistenz-Check//
	if(ilStatus == true)
	{
		CString olBnam;
		char clWhere[100];
		CCSPtrArray<BLTDATA> olBltCPA;

		m_BNAM.GetWindowText(olBnam);
		sprintf(clWhere,"WHERE BNAM='%s'",olBnam);
		if(ogBLTData.ReadSpecialData(&olBltCPA,clWhere,"URNO,BNAM",false) == true)
		{
			if(olBltCPA[0].Urno != pomBLT->Urno)
			{
				ilStatus = false;
				olErrorText += LoadStg(IDS_STRING288) + LoadStg(IDS_EXIST);
			}
		}
		olBltCPA.DeleteAll();
	}
	///////////////////////////////////////////////////////////////////////////
	AfxGetApp()->DoWaitCursor(-1);
	if (ilStatus == true)
	{
		m_BNAM.GetWindowText(pomBLT->Bnam,6);
		//pomBLT->Nafr = DateHourStringToDate(olNafrd,olNafrt);
		//pomBLT->Nato = DateHourStringToDate(olNatod,olNatot);
		//m_RESN.GetWindowText(pomBLT->Resn,41);
		m_TERM.GetWindowText(pomBLT->Term,2);
		m_TELE.GetWindowText(pomBLT->Tele,11);
		m_STAT.GetWindowText(pomBLT->Stat,3);
		m_BLTT.GetWindowText(pomBLT->Bltt,2);
		m_HOME.GetWindowText(pomBLT->Home,3);
		pomBLT->Vafr = DateHourStringToDate(olVafrd,olVafrt);
		pomBLT->Vato = DateHourStringToDate(olVatod,olVatot);

		m_MXF.GetWindowText(pomBLT->Maxf,2);
		m_DEFD.GetWindowText(pomBLT->Defd,4);
		CDialog::OnOK();
	}
	else
	{
		Beep(440,70);
		MessageBox(olErrorText,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
		m_BNAM.SetFocus();
		m_BNAM.SetSel(0,-1);
	}
}

//----------------------------------------------------------------------------------------

void BeggagebeltDlg::MakeNoavTable(CString opTabn, CString opBurn)
{
	CRect olRectBorder;
	m_NOAVFRAME.GetWindowRect( olRectBorder );
	ScreenToClient(olRectBorder);
	olRectBorder.DeflateRect(2,2);

    pomTable->SetTableData(this, olRectBorder.left, olRectBorder.right, olRectBorder.top, olRectBorder.bottom);
	pomTable->SetSelectMode(0);
	pomTable->SetShowSelection(TRUE);
	pomTable->ResetContent();

	CCSPtrArray <TABLE_HEADER_COLUMN> olHeaderDataArray;
	TABLE_HEADER_COLUMN rlHeader;
	rlHeader.Alignment = COLALIGN_LEFT;
	rlHeader.Font = &ogCourier_Regular_10;
	int ilPos = 1;
	rlHeader.Length = 133; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING101),ilPos++,true,'|');
	olHeaderDataArray.NewAt(olHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 133; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING101),ilPos++,true,'|');
	olHeaderDataArray.NewAt(olHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 58; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING101),ilPos++,true,'|');
	olHeaderDataArray.NewAt(olHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 42; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING101),ilPos++,true,'|');
	olHeaderDataArray.NewAt(olHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 42; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING101),ilPos++,true,'|');
	olHeaderDataArray.NewAt(olHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 8; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING101),ilPos++,true,'|');
	olHeaderDataArray.NewAt(olHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 332; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING101),ilPos++,true,'|');
	olHeaderDataArray.NewAt(olHeaderDataArray.GetSize(), rlHeader);

	pomTable->SetHeaderFields(olHeaderDataArray);
	pomTable->SetDefaultSeparator();
	olHeaderDataArray.DeleteAll();

	char clWhere[100];
	CCSPtrArray<BLKDATA> olBlkCPA;
	sprintf(clWhere,"WHERE TABN='%s' AND BURN='%s'",opTabn,opBurn);
	ogBlkData.ReadSpecialData(&omBlkPtrA,clWhere,"",false);
	int ilLines = omBlkPtrA.GetSize();

	pomTable->SetDefaultSeparator();
	pomTable->SetTableEditable(FALSE);

	TABLE_COLUMN rlColumnData;
	rlColumnData.Font = &ogCourier_Regular_10;
	rlColumnData.BkColor = WHITE;
	rlColumnData.Alignment = COLALIGN_LEFT;
	
    for (int ilLineNo = 0; ilLineNo < ilLines; ilLineNo++)
	{
		CCSPtrArray<TABLE_COLUMN> olColList;

		rlColumnData.Text = omBlkPtrA[ilLineNo].Nafr.Format("%d.%m.%Y %H:%M");
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omBlkPtrA[ilLineNo].Nato.Format("%d.%m.%Y %H:%M");
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omBlkPtrA[ilLineNo].Days;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		char pclTmp[10]="";
		rlColumnData.Text = "";
		if(strlen(omBlkPtrA[ilLineNo].Tifr) > 0)
		{
			sprintf(pclTmp, "%s:%s", CString(omBlkPtrA[ilLineNo].Tifr).Left(2), CString(omBlkPtrA[ilLineNo].Tifr).Right(2));
			rlColumnData.Text = pclTmp;
		}
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		rlColumnData.Text = "";
		if(strlen(omBlkPtrA[ilLineNo].Tito) > 0)
		{
			sprintf(pclTmp, "%s:%s", CString(omBlkPtrA[ilLineNo].Tito).Left(2), CString(omBlkPtrA[ilLineNo].Tito).Right(2));
			rlColumnData.Text = pclTmp;
		}
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		(*omBlkPtrA[ilLineNo].Type == 'X') ? rlColumnData.Text = cgYes : rlColumnData.Text = cgNo;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omBlkPtrA[ilLineNo].Resn;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		pomTable->AddTextLine(olColList,(void*)NULL);
		olColList.DeleteAll();
	}
	pomTable->DisplayTable();
}

void BeggagebeltDlg::ChangeNoavTable(BLKDATA *prpBlk, int ipLineNo)
{
	if(prpBlk != NULL)
	{
		TABLE_COLUMN rlColumn;
		CCSPtrArray <TABLE_COLUMN> olLine;

		rlColumn.Lineno = 0;
		rlColumn.VerticalSeparator = SEPA_NONE;
		rlColumn.SeparatorType = SEPA_NONE;
		rlColumn.BkColor = WHITE;
		rlColumn.Font = &ogCourier_Regular_10;
		rlColumn.Alignment = COLALIGN_LEFT;

		int ilColumnNo = 0;
		rlColumn.Text = prpBlk->Nafr.Format("%d.%m.%Y %H:%M");
		rlColumn.Columnno = ilColumnNo++;
		olLine.NewAt(olLine.GetSize(), rlColumn);
		rlColumn.Text = prpBlk->Nato.Format("%d.%m.%Y %H:%M"); 
		rlColumn.Columnno = ilColumnNo++;
		olLine.NewAt(olLine.GetSize(), rlColumn);
		rlColumn.Text = prpBlk->Days;
		rlColumn.Columnno = ilColumnNo++;
		olLine.NewAt(olLine.GetSize(), rlColumn);

		char pclTmp[10]="";
		rlColumn.Text = "";
		if(strlen(prpBlk->Tifr) > 0)
		{
			sprintf(pclTmp, "%s:%s", CString(prpBlk->Tifr).Left(2), CString(prpBlk->Tifr).Right(2));
			rlColumn.Text = pclTmp;
		}
		rlColumn.Columnno = ilColumnNo++;
		olLine.NewAt(olLine.GetSize(), rlColumn);

		rlColumn.Text = "";
		if(strlen(prpBlk->Tito) > 0)
		{
			sprintf(pclTmp, "%s:%s", CString(prpBlk->Tito).Left(2), CString(prpBlk->Tito).Right(2));
			rlColumn.Text = pclTmp;
		}
		rlColumn.Columnno = ilColumnNo++;
		olLine.NewAt(olLine.GetSize(), rlColumn);

		(*prpBlk->Type == 'X') ? rlColumn.Text = cgYes : rlColumn.Text = cgNo;
		rlColumn.Columnno = ilColumnNo++;
		olLine.NewAt(olLine.GetSize(), rlColumn);
		rlColumn.Text = prpBlk->Resn;
		rlColumn.Columnno = ilColumnNo++;
		olLine.NewAt(olLine.GetSize(), rlColumn);

		if(ipLineNo>-1)
		{
			pomTable->ChangeTextLine(ipLineNo, &olLine, (void *)prpBlk);
			pomTable->DisplayTable();
		}
		else
		{
			if(prpBlk != NULL)
			{
				pomTable->AddTextLine(olLine, (void *)prpBlk);
				pomTable->DisplayTable();
			}
		}
		olLine.DeleteAll();
	}
	else
	{
		if(ipLineNo>-1)
		{
			pomTable->DeleteTextLine(ipLineNo);
			pomTable->DisplayTable();
		}
	}
}

LONG BeggagebeltDlg::OnTableLButtonDblclk(UINT wParam, LONG lParam) 
{
	if(ogPrivList.GetStat("BEGGAGEBELTDLG.m_NOAVCHA") == '1')
	{
		int ilLineNo = pomTable->GetCurSel();
		if (ilLineNo == -1)
		{
			MessageBox(LoadStg(IDS_STRING148),LoadStg(IDS_STRING149),MB_ICONEXCLAMATION);
		}
		else
		{
			BLKDATA *prlBlk = &omBlkPtrA[ilLineNo];
			if (prlBlk != NULL)
			{
				BLKDATA rlBlk = *prlBlk;
				strcpy(rlBlk.Tabn, CString("BLT"));
				NotAvailableDlg *polDlg = new NotAvailableDlg(&rlBlk,this);
				m_BNAM.GetWindowText(polDlg->omName);
				if(polDlg->DoModal() == IDOK)
				{
					*prlBlk = rlBlk;
					if(prlBlk->IsChanged != DATA_NEW) prlBlk->IsChanged = DATA_CHANGED;
					ChangeNoavTable(prlBlk, ilLineNo);
				}
				delete polDlg;
			}
		}
	}
	return 0L;
}
