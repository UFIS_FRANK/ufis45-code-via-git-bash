// CedaBsdData.cpp
 
#include "stdafx.h"
#include "CedaBsdData.h"
#include "resource.h"
#include "resrc1.h"


void ProcessBsdCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

//--CEDADATA-----------------------------------------------------------------------------------------------

CedaBsdData::CedaBsdData() : CCSCedaData(&ogCommHandler)
{
	// Create an array of CEDARECINFO for BSDDataStruct
	BEGIN_CEDARECINFO(BSDDATA,BSDDataRecInfo)
		FIELD_CHAR_TRIM	(Bewc,"BEWC")
		FIELD_CHAR_TRIM	(Bkd1,"BKD1")
		FIELD_CHAR_TRIM	(Bkf1,"BKF1")
		FIELD_CHAR_TRIM	(Bkr1,"BKR1")
		FIELD_CHAR_TRIM	(Bkt1,"BKT1")
		FIELD_CHAR_TRIM	(Bsdc,"BSDC")
		FIELD_CHAR_TRIM	(Bsdk,"BSDK")
		FIELD_CHAR_TRIM	(Bsdn,"BSDN")
		FIELD_CHAR_TRIM	(Bsds,"BSDS")
		FIELD_DATE		(Cdat,"CDAT")
		FIELD_CHAR_TRIM	(Ctrc,"CTRC")
		FIELD_CHAR_TRIM	(Esbg,"ESBG")
		FIELD_CHAR_TRIM	(Lsen,"LSEN")
		FIELD_DATE		(Lstu,"LSTU")
		FIELD_CHAR_TRIM	(Prfl,"PRFL")
		FIELD_CHAR_TRIM	(Rema,"REMA")
		FIELD_CHAR_TRIM	(Sdu1,"SDU1")
		FIELD_CHAR_TRIM	(Sex1,"SEX1")
		FIELD_CHAR_TRIM	(Ssh1,"SSH1")
		FIELD_CHAR_TRIM	(Type,"TYPE")
		FIELD_LONG		(Urno,"URNO")
		FIELD_CHAR_TRIM	(Usec,"USEC")
		FIELD_CHAR_TRIM	(Useu,"USEU")
//#ifdef CLIENT_ZRH
		FIELD_CHAR_TRIM	(Rgsb,"RGSB")
		FIELD_DATE		(Vafr,"VAFR")
		FIELD_DATE		(Vato,"VATO")
		FIELD_CHAR_TRIM	(Fctc,"FCTC")
		FIELD_CHAR_TRIM	(Dpt1,"DPT1")
		FIELD_CHAR_TRIM	(Fdel,"FDEL")
//#endif
	END_CEDARECINFO //(BSDDataStruct)

	// FIELD_LONG, FIELD_DATE 
	// Copy the record structure
	for (int i=0; i< sizeof(BSDDataRecInfo)/sizeof(BSDDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&BSDDataRecInfo[i],sizeof(CEDARECINFO));
		omRecInfo.Add(prpCedaRecInfo);
	} // end for

	// initialize field names
	strcpy(pcmTableName,"BSD");
//	strcpy(pcmListOfFields,"BEWC,BKD1,BKF1,BKR1,BKT1,BSDC,BSDK,BSDN,BSDS,CDAT,CTRC,ESBG,LSEN,LSTU,PRFL,REMA,SDU1,SEX1,SSH1,TYPE,URNO,USEC,USEU");
//#ifdef CLIENT_ZRH
	strcpy(pcmListOfFields,"BEWC,BKD1,BKF1,BKR1,BKT1,BSDC,BSDK,BSDN,BSDS,CDAT,CTRC,ESBG,LSEN,LSTU,PRFL,REMA,SDU1,SEX1,SSH1,TYPE,URNO,USEC,USEU,RGSB,VAFR,VATO,FCTC,DPT1,FDEL");
//#endif
	pcmFieldList = pcmListOfFields; //because in CedaData pcmFieldList is just a pointer

	omData.RemoveAll();
}

//---------------------------------------------------------------------------------------------------------

void CedaBsdData::GetDataInfo(CStringArray &ropFields, CStringArray &ropDesription, CStringArray &ropType)
{
	ropFields.Add("BEWC");
	ropFields.Add("BKD1");
	ropFields.Add("BKF1");
	ropFields.Add("BKR1");
	ropFields.Add("BKT1");
	ropFields.Add("BSDC");
	ropFields.Add("BSDK");
	ropFields.Add("BSDN");
	ropFields.Add("BSDS");
	ropFields.Add("CDAT");
	ropFields.Add("CTRC");
	ropFields.Add("ESBG");
	ropFields.Add("LSEN");
	ropFields.Add("LSTU");
	ropFields.Add("PRFL");
	ropFields.Add("REMA");
	ropFields.Add("SDU1");
	ropFields.Add("SEX1");
	ropFields.Add("SSH1");
	ropFields.Add("TYPE");
	ropFields.Add("URNO");
	ropFields.Add("USEC");
	ropFields.Add("USEU");

	ropFields.Add("RGSB");
	ropFields.Add("VAFR");
	ropFields.Add("VATO");
	ropFields.Add("FCTC");
	ropFields.Add("DPT1");
	ropFields.Add("FDEL");

	ropDesription.Add(LoadStg(IDS_STRING341));
	ropDesription.Add(LoadStg(IDS_STRING340));
	ropDesription.Add(LoadStg(IDS_STRING338));
	ropDesription.Add(LoadStg(IDS_STRING446));
	ropDesription.Add(LoadStg(IDS_STRING339));
	ropDesription.Add(LoadStg(IDS_STRING237));
	ropDesription.Add(LoadStg(IDS_STRING330));
	ropDesription.Add(LoadStg(IDS_STRING229));
	ropDesription.Add(LoadStg(IDS_STRING331));
	ropDesription.Add(LoadStg(IDS_STRING343));
	ropDesription.Add(LoadStg(IDS_STRING332));
	ropDesription.Add(LoadStg(IDS_STRING333));
	ropDesription.Add(LoadStg(IDS_STRING334));
	ropDesription.Add(LoadStg(IDS_STRING344));
	ropDesription.Add(LoadStg(IDS_STRING345));
	ropDesription.Add(LoadStg(IDS_STRING342));
	ropDesription.Add(LoadStg(IDS_STRING335));
	ropDesription.Add(LoadStg(IDS_STRING336));
	ropDesription.Add(LoadStg(IDS_STRING337));
	ropDesription.Add(LoadStg(IDS_STRING447));
	ropDesription.Add(LoadStg(IDS_STRING346));
	ropDesription.Add(LoadStg(IDS_STRING347));
	ropDesription.Add(LoadStg(IDS_STRING348));

	ropDesription.Add(LoadStg(IDS_STRING348));
	ropDesription.Add(LoadStg(IDS_STRING230));
	ropDesription.Add(LoadStg(IDS_STRING231));
	ropDesription.Add(LoadStg(IDS_STRING1011));
	ropDesription.Add(LoadStg(IDS_STRING1012));
	ropDesription.Add(LoadStg(IDS_STRING1013));

	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("Date");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("Date");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");

}

//--REGISTER----------------------------------------------------------------------------------------------

void CedaBsdData::Register(void)
{
	ogDdx.Register((void *)this,BC_BSD_CHANGE,	CString("BSDDATA"), CString("Bsd-changed"),	ProcessBsdCf);
	ogDdx.Register((void *)this,BC_BSD_NEW,		CString("BSDDATA"), CString("Bsd-new"),		ProcessBsdCf);
	ogDdx.Register((void *)this,BC_BSD_DELETE,	CString("BSDDATA"), CString("Bsd-deleted"),	ProcessBsdCf);
}

//--~CEDADATA----------------------------------------------------------------------------------------------

CedaBsdData::~CedaBsdData(void)
{
	omRecInfo.DeleteAll();
	ClearAll();
}

//--CLEAR-ALL----------------------------------------------------------------------------------------------

void CedaBsdData::ClearAll(bool bpWithRegistration)
{
    omUrnoMap.RemoveAll();
    omData.DeleteAll();
	if(bpWithRegistration)
	{
		ogDdx.UnRegister(this,NOTUSED);
	}
}

//--READ-ALL-----------------------------------------------------------------------------------------------

bool CedaBsdData::Read(char *pspWhere /*NULL*/)
{
    // Select data from the database
	bool ilRc = true;
    omUrnoMap.RemoveAll();
    omData.DeleteAll();
	if(pspWhere == NULL)
	{	
		ilRc = CedaAction("RT");
	}
	else
	{
		ilRc = CedaAction("RT", pspWhere);
	}
	if (ilRc != true)
	{
		return ilRc;
	}
	// Load data from CedaData into the dynamic array of record
	for (int ilLc = 0; ilRc == true; ilLc++)
	{
		BSDDATA *prlBsd = new BSDDATA;
		if ((ilRc = GetFirstBufferRecord(prlBsd)) == true)
		{
			omData.Add(prlBsd);//Update omData
			omUrnoMap.SetAt((void *)prlBsd->Urno,prlBsd);
		}
		else
		{
			delete prlBsd;
		}
	}
    return true;
}

//--INSERT-------------------------------------------------------------------------------------------------

bool CedaBsdData::Insert(BSDDATA *prpBsd)
{
	prpBsd->IsChanged = DATA_NEW;
	if(Save(prpBsd) == false) return false; //Update Database
	InsertInternal(prpBsd);
    return true;
}

//--INSERT-INTERNAL--------------------------------------------------------------------------------------

bool CedaBsdData::InsertInternal(BSDDATA *prpBsd)
{
	ogDdx.DataChanged((void *)this, BSD_NEW,(void *)prpBsd ); //Update Viewer
	omData.Add(prpBsd);//Update omData
	omUrnoMap.SetAt((void *)prpBsd->Urno,prpBsd);
    return true;
}

//--DELETE-------------------------------------------------------------------------------------------------

bool CedaBsdData::Delete(long lpUrno)
{
	BSDDATA *prlBsd = GetBsdByUrno(lpUrno);
	if (prlBsd != NULL)
	{
		prlBsd->IsChanged = DATA_DELETED;
		if(Save(prlBsd) == false) return false; //Update Database
		DeleteInternal(prlBsd);
	}
    return true;
}

//--DELETE-INTERNAL----------------------------------------------------------------------------------------

bool CedaBsdData::DeleteInternal(BSDDATA *prpBsd)
{
	ogDdx.DataChanged((void *)this,BSD_DELETE,(void *)prpBsd); //Update Viewer
	omUrnoMap.RemoveKey((void *)prpBsd->Urno);
	int ilBsdCount = omData.GetSize();
	for (int ilLc = 0; ilLc < ilBsdCount; ilLc++)
	{
		if (omData[ilLc].Urno == prpBsd->Urno)
		{
			omData.DeleteAt(ilLc);//Update omData
			break;
		}
	}
    return true;
}

//--UPDATE-------------------------------------------------------------------------------------------------

bool CedaBsdData::Update(BSDDATA *prpBsd)
{
	if (GetBsdByUrno(prpBsd->Urno) != NULL)
	{
		if (prpBsd->IsChanged == DATA_UNCHANGED)
		{
			prpBsd->IsChanged = DATA_CHANGED;
		}
		if(Save(prpBsd) == false) return false; //Update Database
		UpdateInternal(prpBsd);
	}
    return true;
}

//--UPDATE-INTERNAL--------------------------------------------------------------------------------------------

bool CedaBsdData::UpdateInternal(BSDDATA *prpBsd)
{
	BSDDATA *prlBsd = GetBsdByUrno(prpBsd->Urno);
	if (prlBsd != NULL)
	{
		*prlBsd = *prpBsd; //Update omData
		ogDdx.DataChanged((void *)this,BSD_CHANGE,(void *)prlBsd); //Update Viewer
	}
    return true;
}

//--GET-BY-URNO--------------------------------------------------------------------------------------------

BSDDATA *CedaBsdData::GetBsdByUrno(long lpUrno)
{
	BSDDATA  *prlBsd;
	if (omUrnoMap.Lookup((void *)lpUrno,(void *& )prlBsd) == TRUE)
	{
		return prlBsd;
	}
	return NULL;
}

//--READSPECIALDATA-------------------------------------------------------------------------------------

bool CedaBsdData::ReadSpecialData(CCSPtrArray<BSDDATA> *popBsd,char *pspWhere,char *pspFieldList,bool ipSYS/*=true*/)
{
	bool ilRc = true;
	char pclFieldList[256] = " ";

	if(strlen(pspFieldList) > 0) 
	{
		strcpy(pclFieldList, pspFieldList);
	}
	if(ipSYS == true) 
	{
		if (CedaAction("SYS","BSD",pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	else
	{
		if (CedaAction("RT","BSD",pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	if(popBsd != NULL)
	{
		for (int ilLc = 0; ilRc == true; ilLc++)
		{
			BSDDATA *prpBsd = new BSDDATA;
			if ((ilRc = GetBufferRecord(ilLc,prpBsd,CString(pclFieldList))) == true)
			{
				popBsd->Add(prpBsd);
			}
			else
			{
				delete prpBsd;
			}
		}
		if(popBsd->GetSize() == 0) return false;
	}
    return true;
}

//--SAVE---------------------------------------------------------------------------------------------------

bool CedaBsdData::Save(BSDDATA *prpBsd)
{
	bool ilRc = true;
	CString olListOfData;
	char pclSelection[124];
	char pclData[524];

	if (prpBsd->IsChanged == DATA_UNCHANGED)
	{
		return true; // no change, nothing to do
	}
	switch(prpBsd->IsChanged)
	{
	case DATA_NEW:
		MakeCedaData(&omRecInfo,olListOfData,prpBsd);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("IRT","","",pclData);
		prpBsd->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_CHANGED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpBsd->Urno);
		MakeCedaData(&omRecInfo,olListOfData,prpBsd);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("URT",pclSelection,"",pclData);
		prpBsd->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_DELETED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpBsd->Urno);
		ilRc = CedaAction("DRT",pclSelection);
		break;
	}
    return ilRc;
}

//--PROCESS-CF---------------------------------------------------------------------------------------------

void  ProcessBsdCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	ogBsdData.ProcessBc(ipDDXType,vpDataPointer,ropInstanceName);
}

//--PROCESS-BC---------------------------------------------------------------------------------------------

void  CedaBsdData::ProcessBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	struct BcStruct *prlBsdData;
	prlBsdData = (struct BcStruct *) vpDataPointer;
	BSDDATA *prlBsd;
	if(ipDDXType == BC_BSD_NEW)
	{
		prlBsd = new BSDDATA;
		GetRecordFromItemList(prlBsd,prlBsdData->Fields,prlBsdData->Data);
		InsertInternal(prlBsd);
	}
	if(ipDDXType == BC_BSD_CHANGE)
	{
		long llUrno = GetUrnoFromSelection(prlBsdData->Selection);
		prlBsd = GetBsdByUrno(llUrno);
		if(prlBsd != NULL)
		{
			GetRecordFromItemList(prlBsd,prlBsdData->Fields,prlBsdData->Data);
			UpdateInternal(prlBsd);
		}
	}
	if(ipDDXType == BC_BSD_DELETE)
	{
		long llUrno;
		CString olSelection = (CString)prlBsdData->Selection;
		if (olSelection.Find('\'') != -1)
		{
			llUrno = GetUrnoFromSelection(prlBsdData->Selection);
		}
		else
		{
			int ilFirst = olSelection.Find("=")+2;
			int ilLast  = olSelection.GetLength();
			llUrno = atol(olSelection.Mid(ilFirst,ilLast-ilFirst));
		}
		prlBsd = GetBsdByUrno(llUrno);
		if (prlBsd != NULL)
		{
			DeleteInternal(prlBsd);
		}
	}
}

//---------------------------------------------------------------------------------------------------------
