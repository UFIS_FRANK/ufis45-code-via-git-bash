// CedaEXTData.h

#ifndef __CEDAEXTDATA__
#define __CEDAEXTDATA__
 
#include "stdafx.h"
#include "basicdata.h"

//---------------------------------------------------------------------------------------------------------
// Record structure declaration

struct EXTDATA 
{
	char 	 Enam[7]; 	// Gep�ckband Name
	CTime	 Cdat; 		// Erstellungsdatum
	CTime	 Lstu; 		// Datum letzte �nderung
	CTime	 Nafr; 		// Nicht verf�gbar von
	CTime	 Nato; 		// Nicht verf�gbar bis
	char 	 Resn[42];	// Grund der Sperrung
	char 	 Prfl[3]; 	// Protokollierungskennzeichen
	char 	 Tele[12]; 	// Telefonnummer
	long 	 Urno; 		// Eindeutige Datensatz-Nr.
	char 	 Usec[34]; 	// Anwender (Ersteller)
	char 	 Useu[34]; 	// Anwender (letzte �nderung)
	CTime	 Vafr; 		// G�ltig von
	CTime	 Vato; 		// G�ltig bis
	char 	 Term[3]; 	// Terminal
	char 	 Home[4]; 	// Terminal

	//DataCreated by this class
	int      IsChanged;

	EXTDATA(void)
	{
		memset(this,'\0',sizeof(*this));
		Cdat=-1;
		Lstu=-1;
		Nafr=-1;
		Nato=-1;
		Vafr=-1;
		Vato=-1;
	}

}; // end EXTDataStrukt


//---------------------------------------------------------------------------------------------------------
// Class declaratino


class CedaEXTData: public CCSCedaData
{
// Attributes
public:
    CMapPtrToPtr omUrnoMap;

    CCSPtrArray<EXTDATA> omData;

// Operations
public:
    CedaEXTData();
	~CedaEXTData();
	virtual void GetDataInfo(CStringArray &ropFields, CStringArray &ropDesription, CStringArray &ropType);
	void Register(void);
	void ClearAll(void);

    bool Read(char *pcpWhere = NULL);
	bool ReadSpecialData(CCSPtrArray<EXTDATA> *popExt,char *pspWhere,char *pspFieldList,bool ipSYS = true);
	bool InsertEXT(EXTDATA *prpEXT,BOOL bpSendDdx = TRUE);
	bool InsertEXTInternal(EXTDATA *prpEXT);
	bool UpdateEXT(EXTDATA *prpEXT,BOOL bpSendDdx = TRUE);
	bool UpdateEXTInternal(EXTDATA *prpEXT);
	bool DeleteEXT(long lpUrno);
	bool DeleteEXTInternal(EXTDATA *prpEXT);
	EXTDATA  *GetEXTByUrno(long lpUrno);
	bool SaveEXT(EXTDATA *prpEXT);
	char pcmEXTFieldList[2048];
	void ProcessEXTBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

	// Private methods
private:
    void PrepareEXTData(EXTDATA *prpEXTData);
};

//---------------------------------------------------------------------------------------------------------

#endif //__CEDAEXTDATA__
