#ifndef __TIMEPARAMETERSTABLEVIEWER_H__
#define __TIMEPARAMETERSTABLEVIEWER_H__

#include "stdafx.h"
#include "CedaTipData.h"
#include "CCSTable.h"
#include "CViewer.h"
#include "CCSPrint.h"


struct TIMEPARAMETERSTABLE_LINEDATA
{
	long	 Urno;
	CString  Tipc;
	CString	 Tipt;
	CString	 Tipu;
	CString	 Tipv;
	CString	 Vafr;
	CString	 Vato;
};

/////////////////////////////////////////////////////////////////////////////
// TimeParametersTableViewer

	  
class TimeParametersTableViewer : public CViewer
{
// Constructions
public:
    TimeParametersTableViewer(CCSPtrArray<TIPDATA> *popData);
    ~TimeParametersTableViewer();

    void Attach(CCSTable *popAttachWnd);
    virtual void ChangeViewTo(const char *pcpViewName);
// Internal data processing routines
private:
	bool IsPassFilter(TIPDATA *prpTimeParameters);
	int CompareTimeParameters(TIMEPARAMETERSTABLE_LINEDATA *prpTimeParameters1, TIMEPARAMETERSTABLE_LINEDATA *prpTimeParameters2);
    void MakeLines();
	void MakeLine(TIPDATA *prpTimeParameters);
	bool FindLine(long lpUrno, int &rilLineno);
// Operations
public:
	void DeleteAll();
	void CreateLine(TIMEPARAMETERSTABLE_LINEDATA *prpTimeParameters);
	void DeleteLine(int ipLineno);
// Window refreshing routines
public:
	void UpdateDisplay();
	CString Format(TIMEPARAMETERSTABLE_LINEDATA *prpLine);
	void ProcessTimeParametersChange(TIPDATA *prpTimeParameters);
	void ProcessTimeParametersDelete(TIPDATA *prpTimeParameters);
	bool FindTimeParameters(char *prpTimeParametersKeya, char *prpTimeParametersKeyd, int& ilItem);
// Attributes used for filtering condition
private:
	bool bmIsFromSearch;
// Attributes
private:
    CCSTable *pomTimeParametersTable;
	CCSPtrArray<TIPDATA> *pomData;
// Methods which handle changes (from Data Distributor)
public:
    CCSPtrArray<TIMEPARAMETERSTABLE_LINEDATA> omLines;
///////
	CCSPtrArray<TABLE_HEADER_COLUMN> omHeaderDataArray;
//Print 
	void PrintTableView();
	bool PrintTableLine(TIMEPARAMETERSTABLE_LINEDATA *prpLine,bool bpLastLine);
	bool PrintTableHeader();
	CCSPrint *pomPrint;
	//CBitmap omBitmap;
	CString omTableName;

};

#endif //__TIMEPARAMETERSTABLEVIEWER_H__
