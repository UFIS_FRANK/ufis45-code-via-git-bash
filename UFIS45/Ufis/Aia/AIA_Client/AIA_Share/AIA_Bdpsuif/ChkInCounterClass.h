#if !defined(AFX_CHKINCOUNTERCLASS_H__6FB70430_EE67_11D2_AD3D_004095436A98__INCLUDED_)
#define AFX_CHKINCOUNTERCLASS_H__6FB70430_EE67_11D2_AD3D_004095436A98__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// ChkInCounterClass.h : header file
//
#include "PrivList.h"
#include "CCSEdit.h"
#include "CCSComboBox.h"
#include "CedaCccData.h"


/////////////////////////////////////////////////////////////////////////////
// CChkInCounterClass dialog

class CChkInCounterClass : public CDialog
{
// Construction
public:
	CChkInCounterClass(CCCDATA *prpCcc,CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CChkInCounterClass)
	enum { IDD = IDD_CHECKINCLASS };
	CButton	m_OK;
	CCSEdit	m_CdatD;
	CCSEdit	m_CdatT;
	CCSEdit	m_Cicc;
	CCSEdit	m_Cicn;
	CCSEdit	m_LstuD;
	CCSEdit	m_LstuT;
	CCSEdit	m_Usec;
	CCSEdit	m_Useu;
	CCSEdit	m_VaFrD;
	CCSEdit	m_VaFrT;
	CCSEdit	m_VaToD;
	CCSEdit	m_VaToT;
	CString m_Caption;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CChkInCounterClass)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CChkInCounterClass)
	virtual void OnCancel();
	virtual void OnOK();
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
private:
		CCCDATA *pomCcc;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CHKINCOUNTERCLASS_H__6FB70430_EE67_11D2_AD3D_004095436A98__INCLUDED_)
