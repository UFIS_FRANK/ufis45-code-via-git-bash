// InfoDlg.cpp : implementation file
//

#include "stdafx.h"
#include "BDPSUIF.h"
#include "InfoDlg.h"
#include "CCSCedadata.h"
#include "CCSCedacom.h"
#include "BasicData.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// InfoDlg dialog
//-----------------------------------------------------------------------------------------

InfoDlg::InfoDlg(CWnd* pParent /*=NULL*/)
	: CDialog(InfoDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(InfoDlg)
	//}}AFX_DATA_INIT
}


void InfoDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(InfoDlg)
	DDX_Control(pDX, IDC_USERNAME,		m_Username);
	DDX_Control(pDX, IDC_SERVER,		m_Server);
	DDX_Control(pDX, IDC_E_COPYRIGHT1,	m_COPYRIGHT1);
	DDX_Control(pDX, IDC_E_COPYRIGHT2,	m_COPYRIGHT2);
	DDX_Control(pDX, IDC_E_UFIS1,		m_UFIS1);
	DDX_Control(pDX, IDC_E_UFIS2,		m_UFIS2);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(InfoDlg, CDialog)
	//{{AFX_MSG_MAP(InfoDlg)
	ON_WM_PAINT()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// InfoDlg message handlers
//-----------------------------------------------------------------------------------------

BOOL InfoDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

	m_Username.SetWindowText(cgUserName);

	CString olServerText="";
	olServerText  += ogCommHandler.pcmRealHostName;
	olServerText  += " / ";
	olServerText  += ogCommHandler.pcmRealHostType;

	m_Server.SetWindowText(olServerText);

	m_COPYRIGHT1.SetEditFont(&ogMSSansSerif_Bold_12);
	m_COPYRIGHT2.SetEditFont(&ogMSSansSerif_Bold_12);
	m_UFIS1.SetEditFont(&ogMSSansSerif_Bold_12);
	m_UFIS2.SetEditFont(&ogMSSansSerif_Bold_12);
	m_COPYRIGHT1.SetBKColor(SILVER);
	m_COPYRIGHT2.SetBKColor(SILVER);
	m_UFIS1.SetBKColor(SILVER);
	m_UFIS2.SetBKColor(SILVER);


	if(ogHome != "SHA")
	{
		m_UFIS1.SetInitText(LoadStg(IDS_BDPSUIF1));
		m_UFIS2.SetInitText(LoadStg(IDS_BDPSUIF2));
		m_COPYRIGHT1.SetInitText(LoadStg(IDS_COPYRIGHT1));
		m_COPYRIGHT2.SetInitText(LoadStg(IDS_COPYRIGHT2));
	}
	else
	{
		m_UFIS1.SetInitText(LoadStg(IDS_BDPSUIF1));
		m_UFIS2.SetInitText(LoadStg(IDS_BDPSUIF2));
		m_COPYRIGHT1.SetInitText("");
		m_COPYRIGHT2.SetInitText("");
	}

	return TRUE;  
}

//-----------------------------------------------------------------------------------------

void InfoDlg::OnPaint() 
{
	CPaintDC dc(this);
	
	CDC olDC;
    olDC.CreateCompatibleDC( &dc );
    CBitmap olBitmap;

	if(ogHome != "SHA")
	{
		olBitmap.LoadBitmap( IDB_ABB ); //ABB-Logo
	}
	else
	{
		olBitmap.LoadBitmap( IDB_EMPTY ); 
	}

    olDC.SelectObject( &olBitmap );
    dc.BitBlt( 19, 96, 76, 52, &olDC, 0, 0, SRCCOPY );
    olDC.DeleteDC( );
}

//-----------------------------------------------------------------------------------------
