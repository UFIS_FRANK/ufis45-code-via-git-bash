// HandlingtypeTableViewer.cpp 
//

#include "stdafx.h"
#include "HandlingtypeTableViewer.h"
#include "CcsDdx.h"
#include "resource.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif


// Local function prototype
static void HandlingtypeTableCf(void *popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

//-----------------------------------------------------------------------------------------------
// HandlingtypeTableViewer
//

HandlingtypeTableViewer::HandlingtypeTableViewer(CCSPtrArray<HTYDATA> *popData)
{
	pomData = popData;

	bmIsFromSearch = FALSE;
    pomHandlingtypeTable = NULL;
    ogDdx.Register(this, HTY_CHANGE, CString("HANDLINGTYPETABLEVIEWER"), CString("Handlingtype Update/new"), HandlingtypeTableCf);
    ogDdx.Register(this, HTY_DELETE, CString("HANDLINGTYPETABLEVIEWER"), CString("Handlingtype Delete"), HandlingtypeTableCf);
}

//-----------------------------------------------------------------------------------------------

HandlingtypeTableViewer::~HandlingtypeTableViewer()
{
	ogDdx.UnRegister(this, NOTUSED);
    DeleteAll();
	omHeaderDataArray.DeleteAll();
}

//-----------------------------------------------------------------------------------------------

void HandlingtypeTableViewer::Attach(CCSTable *popTable)
{
    pomHandlingtypeTable = popTable;
}

//-----------------------------------------------------------------------------------------------

void HandlingtypeTableViewer::ChangeViewTo(const char *pcpViewName)
{
    DeleteAll();    
    MakeLines();
	UpdateDisplay();
}

//-----------------------------------------------------------------------------------------------

void HandlingtypeTableViewer::MakeLines()
{
	int ilHandlingtypeCount = pomData->GetSize();
	for (int ilLc = 0; ilLc < ilHandlingtypeCount; ilLc++)
	{
		HTYDATA *prlHandlingtypeData = &pomData->GetAt(ilLc);
		MakeLine(prlHandlingtypeData);
	}
}

//-----------------------------------------------------------------------------------------------

void HandlingtypeTableViewer::MakeLine(HTYDATA *prpHandlingtype)
{

    //if( !IsPassFilter(prpHandlingtype)) return;

    // Update viewer data for this shift record
    HANDLINGTYPETABLE_LINEDATA rlHandlingtype;

	rlHandlingtype.Urno = prpHandlingtype->Urno; 
	rlHandlingtype.Htyp = prpHandlingtype->Htyp; 
	rlHandlingtype.Hnam = prpHandlingtype->Hnam; 
	rlHandlingtype.Vafr = prpHandlingtype->Vafr.Format("%d.%m.%Y %H:%M"); 
	rlHandlingtype.Vato = prpHandlingtype->Vato.Format("%d.%m.%Y %H:%M"); 
	
	CreateLine(&rlHandlingtype);
}

//-----------------------------------------------------------------------------------------------

void HandlingtypeTableViewer::CreateLine(HANDLINGTYPETABLE_LINEDATA *prpHandlingtype)
{
    int ilLineCount = omLines.GetSize();
    for (int ilLineno = ilLineCount; ilLineno > 0; ilLineno--)
        if (CompareHandlingtype(prpHandlingtype, &omLines[ilLineno-1]) >= 0)
	        break;							 // should be inserted after Lines[ilLineno-1]
	HANDLINGTYPETABLE_LINEDATA rlHandlingtype;
	rlHandlingtype = *prpHandlingtype;
    omLines.NewAt(ilLineno, rlHandlingtype);
}

//-----------------------------------------------------------------------------------------------
// UpdateDisplay: Load data selected by filter conditions to the display by using "omTable"

void HandlingtypeTableViewer::UpdateDisplay()
{
	int ilLines = omLines.GetSize();
	const int ilColumns = 3;	//<===== Spaltenanzahl setzen (von 0 ab)
	omHeaderDataArray.DeleteAll();

	pomHandlingtypeTable->SetShowSelection(TRUE);
	pomHandlingtypeTable->ResetContent();

	TABLE_HEADER_COLUMN rlHeader;
	rlHeader.Alignment = COLALIGN_LEFT;
	rlHeader.Font = &ogCourier_Regular_10;

	int ilPos = 1;
	rlHeader.Length = 17; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING270),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 249; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING270),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 133; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING270),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 133; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING270),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);

	pomHandlingtypeTable->SetHeaderFields(omHeaderDataArray);
	pomHandlingtypeTable->SetDefaultSeparator();
	pomHandlingtypeTable->SetTableEditable(FALSE);

	TABLE_COLUMN rlColumnData;
	rlColumnData.Font = &ogCourier_Regular_10;
	rlColumnData.BkColor = WHITE;
	rlColumnData.Alignment = COLALIGN_LEFT;
	
    for (int ilLineNo = 0; ilLineNo < ilLines; ilLineNo++)
	{
		CCSPtrArray<TABLE_COLUMN> olColList;

		rlColumnData.Text = omLines[ilLineNo].Htyp;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Hnam;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Vafr;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Vato;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		pomHandlingtypeTable->AddTextLine(olColList,(void*)NULL);
		olColList.DeleteAll();
	}
	pomHandlingtypeTable->DisplayTable();
}

//-----------------------------------------------------------------------------------------------

CString HandlingtypeTableViewer::Format(HANDLINGTYPETABLE_LINEDATA *prpLine)
{
    CString s;
	// ???
    return s;
}

//-----------------------------------------------------------------------------------------------

bool HandlingtypeTableViewer::FindHandlingtype(char *pcpHandlingtypeKeya, char *pcpHandlingtypeKeyd, int& ilItem)
{
	int ilCount = omLines.GetSize();
    for (ilItem = 0; ilItem < ilCount; ilItem++)
    {
/*		if ( (omLines[ilItem].Keya == pcpHandlingtypeKeya) &&
			 (omLines[ilItem].Keyd == pcpHandlingtypeKeyd)    )
			return TRUE;*/
	}
	return false;
}

//-----------------------------------------------------------------------------------------------

static void HandlingtypeTableCf(void *popInstance, int ipDDXType,
    void *vpDataPointer, CString &ropInstanceName)
{
    HandlingtypeTableViewer *polViewer = (HandlingtypeTableViewer *)popInstance;
    if (ipDDXType == HTY_CHANGE) polViewer->ProcessHandlingtypeChange((HTYDATA *)vpDataPointer);
    if (ipDDXType == HTY_DELETE) polViewer->ProcessHandlingtypeDelete((HTYDATA *)vpDataPointer);
} 


//-----------------------------------------------------------------------------------------------

void HandlingtypeTableViewer::ProcessHandlingtypeChange(HTYDATA *prpHandlingtype)
{
	int ilItem = 0;
	int ilColumnNo = 0;
	TABLE_COLUMN rlColumn;
	CCSPtrArray <TABLE_COLUMN> olLine;

	rlColumn.Lineno = ilItem;
	rlColumn.VerticalSeparator = SEPA_NONE;
	rlColumn.SeparatorType = SEPA_NONE;
	rlColumn.BkColor = WHITE;
	rlColumn.Font = &ogCourier_Regular_10;
	rlColumn.Alignment = COLALIGN_LEFT;

	rlColumn.Text = prpHandlingtype->Htyp;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpHandlingtype->Hnam;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpHandlingtype->Vafr.Format("%d.%m.%Y %H:%M");
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpHandlingtype->Vato.Format("%d.%m.%Y %H:%M");
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);

	if (FindLine(prpHandlingtype->Urno, ilItem))
	{
        HANDLINGTYPETABLE_LINEDATA *prlLine = &omLines[ilItem];

		prlLine->Urno = prpHandlingtype->Urno;
		prlLine->Htyp = prpHandlingtype->Htyp;
		prlLine->Hnam = prpHandlingtype->Hnam;
		prlLine->Vafr = prpHandlingtype->Vafr.Format("%d.%m.%Y %H:%M");
		prlLine->Vato = prpHandlingtype->Vato.Format("%d.%m.%Y %H:%M");

		pomHandlingtypeTable->ChangeTextLine(ilItem, &olLine, (void *)prlLine);
		pomHandlingtypeTable->DisplayTable();
	}
	else
	{
		MakeLine(prpHandlingtype);
		if (FindLine(prpHandlingtype->Urno, ilItem))
		{
	        HANDLINGTYPETABLE_LINEDATA *prlLine = &omLines[ilItem];
			if(prlLine != NULL)
			{
				pomHandlingtypeTable->AddTextLine(olLine, (void *)prlLine);
				pomHandlingtypeTable->DisplayTable();
			}
		}
	}
	olLine.DeleteAll();
}

//-----------------------------------------------------------------------------------------------

void HandlingtypeTableViewer::ProcessHandlingtypeDelete(HTYDATA *prpHandlingtype)
{
	int ilItem;
	if (FindLine(prpHandlingtype->Urno, ilItem))
	{
		DeleteLine(ilItem);
		pomHandlingtypeTable->DeleteTextLine(ilItem);
		pomHandlingtypeTable->DisplayTable();
	}
}

//-----------------------------------------------------------------------------------------------

bool HandlingtypeTableViewer::IsPassFilter(HTYDATA *prpHandlingtype)
{

	return true;
}

//-----------------------------------------------------------------------------------------------

int HandlingtypeTableViewer::CompareHandlingtype(HANDLINGTYPETABLE_LINEDATA *prpHandlingtype1, HANDLINGTYPETABLE_LINEDATA *prpHandlingtype2)
{
/*	int ilCase=0;
	int	ilCompareResult;
	if(prpHandlingtype1->Tifd == prpHandlingtype2->Tifd)
	{
		ilCompareResult= 0;
	}
	else
	{
		if(prpHandlingtype1->Tifd > prpHandlingtype2->Tifd)
		{
			ilCompareResult = 1;
		}
		else
		{
			ilCompareResult = -1;
		}
	}
	return ilCompareResult;
*/  
	return 0;
}

//-----------------------------------------------------------------------------------------------

void HandlingtypeTableViewer::DeleteLine(int ipLineno)
{
	omLines.DeleteAt(ipLineno);
}

//-----------------------------------------------------------------------------------------------

bool HandlingtypeTableViewer::FindLine(long lpUrno, int &rilLineno)
{
    for (rilLineno = 0; rilLineno < omLines.GetSize(); rilLineno++)
	{
        if (omLines[rilLineno].Urno == lpUrno)
            return true;
	}
    return false;
}
//-----------------------------------------------------------------------------------------------

void HandlingtypeTableViewer::DeleteAll()
{
    while (omLines.GetSize() > 0)
    DeleteLine(0);
}

//-----------------------------------------------------------------------------------------------
// Printing Routines
//-----------------------------------------------------------------------------------------------

void HandlingtypeTableViewer::PrintTableView()
{
	CString olFooter1,olFooter2;
	omTableName = LoadStg(IDS_STRING161);
	int ilOrientation = PRINT_PORTRAET;//  PRINT_PORTRAET  PRINT_LANDSCAPE
	pomPrint = new CCSPrint((CWnd*)this,ilOrientation,45);// FirstLine(P=200,L=200),LeftOffset(P=200,L=50)
	//pomPrint->imMaxLines = 38;  // (P=57,L=38)
	//omBitmap.LoadBitmap(IDB_HAJLOGO);
	//pomPrint->SetBitmaps(&omBitmap,&omBitmap);
	if (pomPrint != NULL)
	{
		if (pomPrint->InitializePrinter(ilOrientation) == TRUE)
		{
			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
			pomPrint->imLineNo = pomPrint->imMaxLines + 1;
			DOCINFO	rlDocInfo;
			memset(&rlDocInfo, 0, sizeof(DOCINFO));
			rlDocInfo.cbSize = sizeof( DOCINFO );
			rlDocInfo.lpszDocName = omTableName;	
			pomPrint->omCdc.StartDoc( &rlDocInfo );
			pomPrint->imPageNo = 0;
			int ilLines = omLines.GetSize();
			olFooter1.Format("%d %s",ilLines,omTableName);
			for(int i = 0; i < ilLines; i++ ) 
			{
				if(pomPrint->imLineNo >= pomPrint->imMaxLines)
				{
					if(pomPrint->imPageNo > 0)
					{
						olFooter2.Format("%s: %d",LoadStg(IDS_STRING222),pomPrint->imPageNo);
						pomPrint->PrintUIFFooter(olFooter1,"",olFooter2);
						pomPrint->omCdc.EndPage();
					}
					PrintTableHeader();
				}
				if(pomPrint->imLineNo == (pomPrint->imMaxLines-1) || i == (ilLines-1))
				{
					PrintTableLine(&omLines[i],true);
				}
				else
				{
					PrintTableLine(&omLines[i],false);
				}
			}
			olFooter2.Format("%s: %d",LoadStg(IDS_STRING222),pomPrint->imPageNo);
			pomPrint->PrintUIFFooter(olFooter1,"",olFooter2);
			pomPrint->omCdc.EndPage();
			pomPrint->omCdc.EndDoc();
		}
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
	delete pomPrint;
	//omBitmap.DeleteObject();
	pomPrint = NULL;
	}
}

//-----------------------------------------------------------------------------------------------

bool HandlingtypeTableViewer::PrintTableHeader()
{
	pomPrint->omCdc.StartPage();
	pomPrint->imPageNo++;
	pomPrint->imLineNo = 0;

	pomPrint->PrintUIFHeader(omTableName,CString(CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M")),pomPrint->imFirstLine-10);

	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;

	rlElement.Alignment   = PRINT_LEFT;
	rlElement.FrameLeft   = PRINT_NOFRAME;
	rlElement.FrameRight  = PRINT_NOFRAME;
	rlElement.FrameTop    = PRINT_NOFRAME;
	rlElement.FrameBottom = PRINT_FRAMEMEDIUM;
	rlElement.pFont       = &pomPrint->ogCourierNew_Bold_8;

	int ilSize = omHeaderDataArray.GetSize();
	for(int i=0;i<ilSize;i++)
	{
		//if(i!=1&&i!=2)// <= setzen, wenn Spalten nicht angezeigt werden sollen 
		{
			rlElement.Length = (int)(omHeaderDataArray[i].Length*dgCCSPrintFactor);
			if(rlElement.Length < igCCSPrintMinLength)rlElement.Length+=igCCSPrintMoreLength; 
			rlElement.Text   = omHeaderDataArray[i].Text;
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
		}
	}
	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();


	return true;
}

//-----------------------------------------------------------------------------------------------

bool HandlingtypeTableViewer::PrintTableLine(HANDLINGTYPETABLE_LINEDATA *prpLine,bool bpLastLine)
{
	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;
	rlElement.pFont      = &pomPrint->ogCourierNew_Regular_8;
	int ilSize = omHeaderDataArray.GetSize();
	for(int i=0;i<ilSize;i++)
	{
		//if(i!=1&&i!=1)// <= setzen, wenn Spalten nicht angezeigt werden sollen 
		{
			rlElement.Length = (int)(omHeaderDataArray[i].Length*dgCCSPrintFactor);
			if(rlElement.Length < igCCSPrintMinLength)rlElement.Length+=igCCSPrintMoreLength; 
			rlElement.Alignment  = PRINT_LEFT;
			rlElement.FrameTop   = PRINT_FRAMETHIN;
			if(bpLastLine == true)
			{
				rlElement.FrameBottom = PRINT_FRAMETHIN;
			}
			else
			{
				rlElement.FrameBottom = PRINT_NOFRAME;
			}
			rlElement.FrameLeft  = PRINT_NOFRAME;
			rlElement.FrameRight = PRINT_NOFRAME;
			switch(i)
			{
			case 0:
				{
					rlElement.FrameLeft  = PRINT_FRAMETHIN;
					rlElement.Text		= prpLine->Htyp;
				}
				break;
			case 1:
				{
					rlElement.Text		= prpLine->Hnam;
				}
				break;
			case 2:
				{
					rlElement.Text		= prpLine->Vafr;
				}
				break;
			case 3:
				{
					rlElement.FrameRight = PRINT_FRAMETHIN;
					rlElement.Text		= prpLine->Vato;
				}
				break;
			}
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
		}
	}
	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();
	return true;
}

//-----------------------------------------------------------------------------------------------
