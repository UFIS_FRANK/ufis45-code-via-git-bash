// CedaOrgData.h

#ifndef __CEDAORGDATA__
#define __CEDAORGDATA__
 
#include "stdafx.h"
#include "basicdata.h"

//---------------------------------------------------------------------------------------------------------

struct ORGDATA 
{
	CTime 	 Cdat; 		// Erstellungsdatum
	char 	 Dpt1[7]; 	// Code
	char 	 Dpt2[7]; 	// �bergeordnete Einheit. Code
	char 	 Dptn[42]; 	// Bezeichnung
	CTime 	 Lstu;		// Datum letzte �nderung
	char 	 Prfl[3]; 	// Protokollierungskennzeichen
	char 	 Rema[62]; 	// Bemerkung
	long 	 Urno;		// Eindeutige Datensatz-Nr.
	char 	 Usec[34]; 	// Anwender (Ersteller)
	char 	 Useu[34]; 	// Anwender (letzte �nderung)

	//DataCreated by this class
	int		 IsChanged;	// Check whether Data has Changed f�r Relaese

	ORGDATA(void)
	{ 
		memset(this,'\0',sizeof(*this));
		IsChanged	=	DATA_UNCHANGED;
		Cdat		=	TIMENULL;
		Lstu		=	TIMENULL;
	}

}; // end OrgDataStrukt

//---------------------------------------------------------------------------------------------------------
// Class declaratino

class CedaOrgData: public CCSCedaData
{
// Attributes
public:
    CMapPtrToPtr omUrnoMap;

    CCSPtrArray<ORGDATA> omData;

	char pcmListOfFields[2048];

// Operations
public:
    CedaOrgData();
	~CedaOrgData();
	virtual void GetDataInfo(CStringArray &ropFields, CStringArray &ropDesription, CStringArray &ropType);
	void Register(void);
	void ClearAll(bool bpWithRegistration = true);
    bool Read(char *pspWhere = NULL);
	bool Insert(ORGDATA *prpOrg);
	bool InsertInternal(ORGDATA *prpOrg);
	bool Update(ORGDATA *prpOrg);
	bool UpdateInternal(ORGDATA *prpOrg);
	bool Delete(long lpUrno);
	bool DeleteInternal(ORGDATA *prpOrg);
	bool ReadSpecialData(CCSPtrArray<ORGDATA> *popOrg,char *pspWhere,char *pspFieldList,bool ipSYS=true);
	bool Save(ORGDATA *prpOrg);
	void ProcessBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);
	ORGDATA  *GetOrgByUrno(long lpUrno);


	// Private methods
private:
    void PrepareOrgData(ORGDATA *prpOrgData);

};

//---------------------------------------------------------------------------------------------------------


#endif //__CEDAORGDATA__
