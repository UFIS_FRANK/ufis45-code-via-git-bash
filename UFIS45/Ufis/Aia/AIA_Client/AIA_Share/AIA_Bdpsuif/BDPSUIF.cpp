// BDPSUIF.cpp : Defines the class behaviors for the application.
//

#include "stdafx.h"
#include "BDPSUIF.h"
#include "UFIS.h"
#include "LoginDlg.h"
#include "PrivList.h"
#include "RegisterDlg.h"
#include "Stammdaten.h"
#include "CViewer.h"
#include "CedaBasicData.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

char cgUserName[34];
CStringArray  ogCmdLineStghArray;


static void InitBDPSTableDefaultView()
{
	CViewer olViewer;
	CStringArray olPossibleFilters;

	// Read the default value from the database in the server
	olPossibleFilters.RemoveAll();
	olPossibleFilters.Add("UNIFILTER");
	// Initialize the default view for the gate diagram
	olViewer.SetViewerKey("UIF_AIRLINE");
	CString olLastView = olViewer.SelectView();	// remember the current view
	olViewer.CreateView("<Default>", olPossibleFilters);
	olViewer.SelectView("<Default>");
	if (olLastView != "")
		olViewer.SelectView(olLastView);	// restore the previously selected view

	CViewer olViewer1;

	// Read the default value from the database in the server
	olPossibleFilters.RemoveAll();
	olPossibleFilters.Add("UNIFILTER");
	// Initialize the default view for the gate diagram
	olViewer1.SetViewerKey("UIF_AIRCRAFT");
	olLastView = olViewer1.SelectView();	// remember the current view
	olViewer1.CreateView("<Default>", olPossibleFilters);
	olViewer1.SelectView("<Default>");
	if (olLastView != "")
		olViewer1.SelectView(olLastView);	// restore the previously selected view

	CViewer olViewer2;

	// Read the default value from the database in the server
	olPossibleFilters.RemoveAll();
	olPossibleFilters.Add("UNIFILTER");
	// Initialize the default view for the gate diagram
	olViewer2.SetViewerKey("UIF_LFZREGISTR");
	olLastView = olViewer2.SelectView();	// remember the current view
	olViewer2.CreateView("<Default>", olPossibleFilters);
	olViewer2.SelectView("<Default>");
	if (olLastView != "")
		olViewer2.SelectView(olLastView);	// restore the previously selected view

	CViewer olViewer3;

	// Read the default value from the database in the server
	olPossibleFilters.RemoveAll();
	olPossibleFilters.Add("UNIFILTER");
	// Initialize the default view for the gate diagram
	olViewer3.SetViewerKey("UIF_AIRPORT");
	olLastView = olViewer3.SelectView();	// remember the current view
	olViewer3.CreateView("<Default>", olPossibleFilters);
	olViewer3.SelectView("<Default>");
	if (olLastView != "")
		olViewer3.SelectView(olLastView);	// restore the previously selected view

	CViewer olViewer4;

	// Read the default value from the database in the server
	olPossibleFilters.RemoveAll();
	olPossibleFilters.Add("UNIFILTER");
	// Initialize the default view for the gate diagram
	olViewer4.SetViewerKey("UIF_RUNWAY");
	olLastView = olViewer4.SelectView();	// remember the current view
	olViewer4.CreateView("<Default>", olPossibleFilters);
	olViewer4.SelectView("<Default>");
	if (olLastView != "")
		olViewer4.SelectView(olLastView);	// restore the previously selected view

	CViewer olViewer5;

	// Read the default value from the database in the server
	olPossibleFilters.RemoveAll();
	olPossibleFilters.Add("UNIFILTER");
	// Initialize the default view for the gate diagram
	olViewer5.SetViewerKey("UIF_TAXIWAY");
	olLastView = olViewer5.SelectView();	// remember the current view
	olViewer5.CreateView("<Default>", olPossibleFilters);
	olViewer5.SelectView("<Default>");
	if (olLastView != "")
		olViewer5.SelectView(olLastView);	// restore the previously selected view

	CViewer olViewer6;

	// Read the default value from the database in the server
	olPossibleFilters.RemoveAll();
	olPossibleFilters.Add("UNIFILTER");
	// Initialize the default view for the gate diagram
	olViewer6.SetViewerKey("UIF_POSITION");
	olLastView = olViewer6.SelectView();	// remember the current view
	olViewer6.CreateView("<Default>", olPossibleFilters);
	olViewer6.SelectView("<Default>");
	if (olLastView != "")
		olViewer6.SelectView(olLastView);	// restore the previously selected view

	CViewer olViewer7;

	// Read the default value from the database in the server
	olPossibleFilters.RemoveAll();
	olPossibleFilters.Add("UNIFILTER");
	// Initialize the default view for the gate diagram
	olViewer7.SetViewerKey("UIF_GATE");
	olLastView = olViewer7.SelectView();	// remember the current view
	olViewer7.CreateView("<Default>", olPossibleFilters);
	olViewer7.SelectView("<Default>");
	if (olLastView != "")
		olViewer7.SelectView(olLastView);	// restore the previously selected view

	CViewer olViewer8;

	// Read the default value from the database in the server
	olPossibleFilters.RemoveAll();
	olPossibleFilters.Add("UNIFILTER");
	// Initialize the default view for the gate diagram
	olViewer8.SetViewerKey("UIF_CHECKINCOUNTER");
	olLastView = olViewer8.SelectView();	// remember the current view
	olViewer8.CreateView("<Default>", olPossibleFilters);
	olViewer8.SelectView("<Default>");
	if (olLastView != "")
		olViewer8.SelectView(olLastView);	// restore the previously selected view

	CViewer olViewer9;

	// Read the default value from the database in the server
	olPossibleFilters.RemoveAll();
	olPossibleFilters.Add("UNIFILTER");
	// Initialize the default view for the gate diagram
	olViewer9.SetViewerKey("UIF_BEGGAGEBELT");
	olLastView = olViewer9.SelectView();	// remember the current view
	olViewer9.CreateView("<Default>", olPossibleFilters);
	olViewer9.SelectView("<Default>");
	if (olLastView != "")
		olViewer9.SelectView(olLastView);	// restore the previously selected view

	CViewer olViewer10;

	// Read the default value from the database in the server
	olPossibleFilters.RemoveAll();
	olPossibleFilters.Add("UNIFILTER");
	// Initialize the default view for the gate diagram
	olViewer10.SetViewerKey("UIF_EXIT");
	olLastView = olViewer10.SelectView();	// remember the current view
	olViewer10.CreateView("<Default>", olPossibleFilters);
	olViewer10.SelectView("<Default>");
	if (olLastView != "")
		olViewer10.SelectView(olLastView);	// restore the previously selected view

	CViewer olViewer11;

	// Read the default value from the database in the server
	olPossibleFilters.RemoveAll();
	olPossibleFilters.Add("UNIFILTER");
	// Initialize the default view for the gate diagram
	olViewer11.SetViewerKey("UIF_DELAYCODE");
	olLastView = olViewer11.SelectView();	// remember the current view
	olViewer11.CreateView("<Default>", olPossibleFilters);
	olViewer11.SelectView("<Default>");
	if (olLastView != "")
		olViewer11.SelectView(olLastView);	// restore the previously selected view

	CViewer olViewer12;

	// Read the default value from the database in the server
	olPossibleFilters.RemoveAll();
	olPossibleFilters.Add("UNIFILTER");
	// Initialize the default view for the gate diagram
	olViewer12.SetViewerKey("UIF_TELEXADRESS");
	olLastView = olViewer12.SelectView();	// remember the current view
	olViewer12.CreateView("<Default>", olPossibleFilters);
	olViewer12.SelectView("<Default>");
	if (olLastView != "")
		olViewer12.SelectView(olLastView);	// restore the previously selected view

	CViewer olViewer13;

	// Read the default value from the database in the server
	olPossibleFilters.RemoveAll();
	olPossibleFilters.Add("UNIFILTER");
	// Initialize the default view for the gate diagram
	olViewer13.SetViewerKey("UIF_TRAFFICTYPE");
	olLastView = olViewer13.SelectView();	// remember the current view
	olViewer13.CreateView("<Default>", olPossibleFilters);
	olViewer13.SelectView("<Default>");
	if (olLastView != "")
		olViewer13.SelectView(olLastView);	// restore the previously selected view

	CViewer olViewer14;

	// Read the default value from the database in the server
	olPossibleFilters.RemoveAll();
	olPossibleFilters.Add("UNIFILTER");
	// Initialize the default view for the gate diagram
	olViewer14.SetViewerKey("UIF_HANDLINGAGENT");
	olLastView = olViewer14.SelectView();	// remember the current view
	olViewer14.CreateView("<Default>", olPossibleFilters);
	olViewer14.SelectView("<Default>");
	if (olLastView != "")
		olViewer14.SelectView(olLastView);	// restore the previously selected view

	CViewer olViewer15;

	// Read the default value from the database in the server
	olPossibleFilters.RemoveAll();
	olPossibleFilters.Add("UNIFILTER");
	// Initialize the default view for the gate diagram
	olViewer15.SetViewerKey("UIF_WAITINGROOM");
	olLastView = olViewer15.SelectView();	// remember the current view
	olViewer15.CreateView("<Default>", olPossibleFilters);
	olViewer15.SelectView("<Default>");
	if (olLastView != "")
		olViewer15.SelectView(olLastView);	// restore the previously selected view

	CViewer olViewer16;

	// Read the default value from the database in the server
	olPossibleFilters.RemoveAll();
	olPossibleFilters.Add("UNIFILTER");
	// Initialize the default view for the gate diagram
	olViewer16.SetViewerKey("UIF_HANDLINGTYPE");
	olLastView = olViewer16.SelectView();	// remember the current view
	olViewer16.CreateView("<Default>", olPossibleFilters);
	olViewer16.SelectView("<Default>");
	if (olLastView != "")
		olViewer16.SelectView(olLastView);	// restore the previously selected view

	CViewer olViewer17;

	// Read the default value from the database in the server
	olPossibleFilters.RemoveAll();
	olPossibleFilters.Add("UNIFILTER");
	// Initialize the default view for the gate diagram
	olViewer17.SetViewerKey("UIF_SERVICETYPE");
	olLastView = olViewer17.SelectView();	// remember the current view
	olViewer17.CreateView("<Default>", olPossibleFilters);
	olViewer17.SelectView("<Default>");
	if (olLastView != "")
		olViewer17.SelectView(olLastView);	// restore the previously selected view

	CViewer olViewer18;

	// Read the default value from the database in the server
	olPossibleFilters.RemoveAll();
	olPossibleFilters.Add("UNIFILTER");
	// Initialize the default view for the gate diagram
	olViewer18.SetViewerKey("UIF_FIDSCOMMAND");
	olLastView = olViewer18.SelectView();	// remember the current view
	olViewer18.CreateView("<Default>", olPossibleFilters);
	olViewer18.SelectView("<Default>");
	if (olLastView != "")
		olViewer18.SelectView(olLastView);	// restore the previously selected view

	CViewer olViewer19;

	// Read the default value from the database in the server
	olPossibleFilters.RemoveAll();
	olPossibleFilters.Add("UNIFILTER");
	// Initialize the default view for the gate diagram
	olViewer19.SetViewerKey("UIF_ERGVERKEHRSAR");
	olLastView = olViewer19.SelectView();	// remember the current view
	olViewer19.CreateView("<Default>", olPossibleFilters);
	olViewer19.SelectView("<Default>");
	if (olLastView != "")
		olViewer19.SelectView(olLastView);	// restore the previously selected view

	CViewer olViewer20;

	// Read the default value from the database in the server
	olPossibleFilters.RemoveAll();
	olPossibleFilters.Add("UNIFILTER");
	// Initialize the default view for the gate diagram
	olViewer20.SetViewerKey("UIF_STDARDVERKETT");
	olLastView = olViewer20.SelectView();	// remember the current view
	olViewer20.CreateView("<Default>", olPossibleFilters);
	olViewer20.SelectView("<Default>");
	if (olLastView != "")
		olViewer20.SelectView(olLastView);	// restore the previously selected view

	CViewer olViewer21;

	// Read the default value from the database in the server
	olPossibleFilters.RemoveAll();
	olPossibleFilters.Add("UNIFILTER");
	// Initialize the default view for the gate diagram
	olViewer21.SetViewerKey("UIF_FLUGPLANSAISON");
	olLastView = olViewer21.SelectView();	// remember the current view
	olViewer21.CreateView("<Default>", olPossibleFilters);
	olViewer21.SelectView("<Default>");
	if (olLastView != "")
		olViewer21.SelectView(olLastView);	// restore the previously selected view

	CViewer olViewer22;

	// Read the default value from the database in the server
	olPossibleFilters.RemoveAll();
	olPossibleFilters.Add("UNIFILTER");
	// Initialize the default view for the gate diagram
	olViewer22.SetViewerKey("UIF_LEISTUNGSKATA");
	olLastView = olViewer22.SelectView();	// remember the current view
	olViewer22.CreateView("<Default>", olPossibleFilters);
	olViewer22.SelectView("<Default>");
	if (olLastView != "")
		olViewer22.SelectView(olLastView);	// restore the previously selected view

	CViewer olViewer23;

	// Read the default value from the database in the server
	olPossibleFilters.RemoveAll();
	olPossibleFilters.Add("UNIFILTER");
	// Initialize the default view for the gate diagram
	olViewer23.SetViewerKey("UIF_QUALIFIKATIONEN");
	olLastView = olViewer23.SelectView();	// remember the current view
	olViewer23.CreateView("<Default>", olPossibleFilters);
	olViewer23.SelectView("<Default>");
	if (olLastView != "")
		olViewer23.SelectView(olLastView);	// restore the previously selected view

	CViewer olViewer24;

	// Read the default value from the database in the server
	olPossibleFilters.RemoveAll();
	olPossibleFilters.Add("UNIFILTER");
	// Initialize the default view for the gate diagram
	olViewer24.SetViewerKey("UIF_ORGANISATIONSEINHEITEN");
	olLastView = olViewer24.SelectView();	// remember the current view
	olViewer24.CreateView("<Default>", olPossibleFilters);
	olViewer24.SelectView("<Default>");
	if (olLastView != "")
		olViewer24.SelectView(olLastView);	// restore the previously selected view

	CViewer olViewer25;

	// Read the default value from the database in the server
	olPossibleFilters.RemoveAll();
	olPossibleFilters.Add("UNIFILTER");
	// Initialize the default view for the gate diagram
	olViewer25.SetViewerKey("UIF_ARBEITSVERTRAGSARTEN");
	olLastView = olViewer25.SelectView();	// remember the current view
	olViewer25.CreateView("<Default>", olPossibleFilters);
	olViewer25.SelectView("<Default>");
	if (olLastView != "")
		olViewer25.SelectView(olLastView);	// restore the previously selected view

	CViewer olViewer26;

	// Read the default value from the database in the server
	olPossibleFilters.RemoveAll();
	olPossibleFilters.Add("UNIFILTER");
	// Initialize the default view for the gate diagram
	olViewer26.SetViewerKey("UIF_BEWERTUNGSFAKTOREN");
	olLastView = olViewer26.SelectView();	// remember the current view
	olViewer26.CreateView("<Default>", olPossibleFilters);
	olViewer26.SelectView("<Default>");
	if (olLastView != "")
		olViewer26.SelectView(olLastView);	// restore the previously selected view

	CViewer olViewer27;

	// Read the default value from the database in the server
	olPossibleFilters.RemoveAll();
	olPossibleFilters.Add("UNIFILTER");
	// Initialize the default view for the gate diagram
	olViewer27.SetViewerKey("UIF_BASISSCHICHTEN");
	olLastView = olViewer27.SelectView();	// remember the current view
	olViewer27.CreateView("<Default>", olPossibleFilters);
	olViewer27.SelectView("<Default>");
	if (olLastView != "")
		olViewer27.SelectView(olLastView);	// restore the previously selected view

	CViewer olViewer28;

	// Read the default value from the database in the server
	olPossibleFilters.RemoveAll();
	olPossibleFilters.Add("UNIFILTER");
	// Initialize the default view for the gate diagram
	olViewer28.SetViewerKey("UIF_FUNKTIONEN");
	olLastView = olViewer28.SelectView();	// remember the current view
	olViewer28.CreateView("<Default>", olPossibleFilters);
	olViewer28.SelectView("<Default>");
	if (olLastView != "")
		olViewer28.SelectView(olLastView);	// restore the previously selected view
	
	CViewer olViewer29;

	// Read the default value from the database in the server
	olPossibleFilters.RemoveAll();
	olPossibleFilters.Add("UNIFILTER");
	// Initialize the default view for the gate diagram
	olViewer29.SetViewerKey("UIF_FAHRGEMEINSCHAFTEN");
	olLastView = olViewer29.SelectView();	// remember the current view
	olViewer29.CreateView("<Default>", olPossibleFilters);
	olViewer29.SelectView("<Default>");
	if (olLastView != "")
		olViewer29.SelectView(olLastView);	// restore the previously selected view

	CViewer olViewer30;

	// Read the default value from the database in the server
	olPossibleFilters.RemoveAll();
	olPossibleFilters.Add("UNIFILTER");
	// Initialize the default view for the gate diagram
	olViewer30.SetViewerKey("UIF_REDUKTIONEN");
	olLastView = olViewer30.SelectView();	// remember the current view
	olViewer30.CreateView("<Default>", olPossibleFilters);
	olViewer30.SelectView("<Default>");
	if (olLastView != "")
		olViewer30.SelectView(olLastView);	// restore the previously selected view

	
	CViewer olViewer31;

	// Read the default value from the database in the server
	olPossibleFilters.RemoveAll();
	olPossibleFilters.Add("UNIFILTER");
	// Initialize the default view for the gate diagram
	olViewer31.SetViewerKey("UIF_DIENSTEUNDABWESENHEITEN");
	olLastView = olViewer31.SelectView();	// remember the current view
	olViewer31.CreateView("<Default>", olPossibleFilters);
	olViewer31.SelectView("<Default>");
	if (olLastView != "")
		olViewer31.SelectView(olLastView);	// restore the previously selected view
	
	CViewer olViewer32;

	// Read the default value from the database in the server
	olPossibleFilters.RemoveAll();
	olPossibleFilters.Add("UNIFILTER");
	// Initialize the default view for the gate diagram
	olViewer32.SetViewerKey("UIF_MITARBEITERSTAMM");
	olLastView = olViewer32.SelectView();	// remember the current view
	olViewer32.CreateView("<Default>", olPossibleFilters);
	olViewer32.SelectView("<Default>");
	if (olLastView != "")
		olViewer32.SelectView(olLastView);	// restore the previously selected view

	CViewer olViewer33;

	// Read the default value from the database in the server
	olPossibleFilters.RemoveAll();
	olPossibleFilters.Add("UNIFILTER");
	// Initialize the default view for the gate diagram
	olViewer33.SetViewerKey("UIF_GERAETEGRUPPEN");
	olLastView = olViewer33.SelectView();	// remember the current view
	olViewer33.CreateView("<Default>", olPossibleFilters);
	olViewer33.SelectView("<Default>");
	if (olLastView != "")
		olViewer33.SelectView(olLastView);	// restore the previously selected view

	CViewer olViewer34;

	// Read the default value from the database in the server
	olPossibleFilters.RemoveAll();
	olPossibleFilters.Add("UNIFILTER");
	// Initialize the default view for the gate diagram
	olViewer34.SetViewerKey("UIF_WEGEZEITEN");
	olLastView = olViewer34.SelectView();	// remember the current view
	olViewer34.CreateView("<Default>", olPossibleFilters);
	olViewer34.SelectView("<Default>");
	if (olLastView != "")
		olViewer34.SelectView(olLastView);	// restore the previously selected view

	CViewer olViewer35;

	// Read the default value from the database in the server
	olPossibleFilters.RemoveAll();
	olPossibleFilters.Add("UNIFILTER");
	// Initialize the default view for the gate diagram
	olViewer35.SetViewerKey("UIF_ERGABFERTIGUNGSARTEN");
	olLastView = olViewer35.SelectView();	// remember the current view
	olViewer35.CreateView("<Default>", olPossibleFilters);
	olViewer35.SelectView("<Default>");
	if (olLastView != "")
		olViewer35.SelectView(olLastView);	// restore the previously selected view

	CViewer olViewer36;

	// Read the default value from the database in the server
	olPossibleFilters.RemoveAll();
	olPossibleFilters.Add("UNIFILTER");
	// Initialize the default view for the gate diagram
	olViewer36.SetViewerKey("UIF_ORGANIZER");
	olLastView = olViewer36.SelectView();	// remember the current view
	olViewer36.CreateView("<Default>", olPossibleFilters);
	olViewer36.SelectView("<Default>");
	if (olLastView != "")
		olViewer36.SelectView(olLastView);	// restore the previously selected view

	CViewer olViewer37;

	// Read the default value from the database in the server
	olPossibleFilters.RemoveAll();
	olPossibleFilters.Add("UNIFILTER");
	// Initialize the default view for the gate diagram
	olViewer37.SetViewerKey("UIF_TIMEPARAMETERS");
	olLastView = olViewer37.SelectView();	// remember the current view
	olViewer37.CreateView("<Default>", olPossibleFilters);
	olViewer37.SelectView("<Default>");
	if (olLastView != "")
		olViewer37.SelectView(olLastView);	// restore the previously selected view

	CViewer olViewer38;
	// Read the default value from the database in the server
	olPossibleFilters.RemoveAll();
	olPossibleFilters.Add("UNIFILTER");
	// Initialize the default view for the gate diagram
	olViewer38.SetViewerKey("UIF_HOLIDAY");
	olLastView = olViewer38.SelectView();	// remember the current view
	olViewer38.CreateView("<Default>", olPossibleFilters);
	olViewer38.SelectView("<Default>");
	if (olLastView != "")
		olViewer38.SelectView(olLastView);	// restore the previously selected view

	CViewer olViewer39;
	// Read the default value from the database in the server
	olPossibleFilters.RemoveAll();
	olPossibleFilters.Add("UNIFILTER");
	// Initialize the default view for the gate diagram
	olViewer39.SetViewerKey("UIF_ARBEITSGRUPPEN");
	olLastView = olViewer39.SelectView();	// remember the current view
	olViewer39.CreateView("<Default>", olPossibleFilters);
	olViewer39.SelectView("<Default>");
	if (olLastView != "")
		olViewer39.SelectView(olLastView);	// restore the previously selected view

	CViewer olViewer40;
	// Read the default value from the database in the server
	olPossibleFilters.RemoveAll();
	olPossibleFilters.Add("UNIFILTER");
	// Initialize the default view for the gate diagram
	olViewer40.SetViewerKey("UIF_PLANUNGSGRUPPEN");
	olLastView = olViewer40.SelectView();	// remember the current view
	olViewer40.CreateView("<Default>", olPossibleFilters);
	olViewer40.SelectView("<Default>");
	if (olLastView != "")
		olViewer40.SelectView(olLastView);	// restore the previously selected view

	CViewer olViewer41;
	// Read the default value from the database in the server
	olPossibleFilters.RemoveAll();
	olPossibleFilters.Add("UNIFILTER");
	// Initialize the default view for the gate diagram
	olViewer41.SetViewerKey("UIF_AIRPORTWET");
	olLastView = olViewer41.SelectView();	// remember the current view
	olViewer41.CreateView("<Default>", olPossibleFilters);
	olViewer41.SelectView("<Default>");
	if (olLastView != "")
		olViewer41.SelectView(olLastView);	// restore the previously selected view

	CViewer olViewer42;
	// Read the default value from the database in the server
	olPossibleFilters.RemoveAll();
	olPossibleFilters.Add("UNIFILTER");
	// Initialize the default view for the gate diagram
	olViewer42.SetViewerKey("UIF_COUNTERCLASS");
	olLastView = olViewer42.SelectView();	// remember the current view
	olViewer42.CreateView("<Default>", olPossibleFilters);
	olViewer42.SelectView("<Default>");
	if (olLastView != "")
		olViewer42.SelectView(olLastView);	// restore the previously selected view

	CViewer olViewer43;
	// Read the default value from the database in the server
	olPossibleFilters.RemoveAll();
	olPossibleFilters.Add("UNIFILTER");
	// Initialize the default view for the gate diagram
	olViewer43.SetViewerKey("UIF_VERYIMPPERS");
	olLastView = olViewer43.SelectView();	// remember the current view
	olViewer43.CreateView("<Default>", olPossibleFilters);
	olViewer43.SelectView("<Default>");
	if (olLastView != "")
		olViewer43.SelectView(olLastView);	// restore the previously selected view

	CViewer olViewer44;
	// Read the default value from the database in the server
	olPossibleFilters.RemoveAll();
	olPossibleFilters.Add("UNIFILTER");
	// Initialize the default view for the gate diagram
	olViewer44.SetViewerKey("UIF_AIRCRAFTFAM");
	olLastView = olViewer44.SelectView();	// remember the current view
	olViewer44.CreateView("<Default>", olPossibleFilters);
	olViewer44.SelectView("<Default>");
	if (olLastView != "")
		olViewer44.SelectView(olLastView);	// restore the previously selected view

	CViewer olViewer45;
	// Read the default value from the database in the server
	olPossibleFilters.RemoveAll();
	olPossibleFilters.Add("UNIFILTER");
	// Initialize the default view for the gate diagram
	olViewer45.SetViewerKey("UIF_ENGINETYPE");
	olLastView = olViewer45.SelectView();	// remember the current view
	olViewer45.CreateView("<Default>", olPossibleFilters);
	olViewer45.SelectView("<Default>");
	if (olLastView != "")
		olViewer45.SelectView(olLastView);	// restore the previously selected view

}

/////////////////////////////////////////////////////////////////////////////
// CBDPSUIFApp

BEGIN_MESSAGE_MAP(CBDPSUIFApp, CWinApp)
	//{{AFX_MSG_MAP(CBDPSUIFApp)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG
	ON_COMMAND(ID_HELP, OnSpecialHelp)
END_MESSAGE_MAP()
//	ON_COMMAND(ID_HELP, CWinApp::OnHelp)
//	ON_COMMAND(ID_HELP, OnSpecialHelp)

/////////////////////////////////////////////////////////////////////////////
// CBDPSUIFApp construction

CBDPSUIFApp::CBDPSUIFApp()
{
	// TODO: add construction code here,
	// Place all significant initialization in InitInstance
}


CBDPSUIFApp::~CBDPSUIFApp()
{
	DeleteBrushes();
	//ExitProcess(0);
}

/////////////////////////////////////////////////////////////////////////////
// The one and only CBDPSUIFApp object

CBDPSUIFApp theApp;


/////////////////////////////////////////////////////////////////////////////
// CBDPSUIFApp initialization

BOOL CBDPSUIFApp::InitInstance()
{
	// Standard initialization
	char pclViewEditFilter[64];
	char pclViewEditSort[64];

	ogNoData  = LoadStg(ST_NODATA);
	ogNotFormat = LoadStg(ST_NOTFORMAT);
	

	// If you are not using these features and wish to reduce the size
	//  of your final executable, you should remove from the following
	//  the specific initialization routines you do not need.

#ifdef _AFXDLL
	Enable3dControls();			// Call this when using MFC in a shared DLL
#else
	Enable3dControlsStatic();	// Call this when linking to MFC statically
#endif

	if (!AfxOleInit())
	{
		return FALSE;
	}

	//SHA20001108 GRID
	GXInit();

	//Parameter�bergabe///////////////////////////////////////////////////
	ogCmdLineStghArray.RemoveAll();
	CString olCmdLine =  LPCTSTR(m_lpCmdLine);
	//m_lpCmdLine => "AppName,UserID,Password,Tablelist"
	if(olCmdLine.GetLength() == 0)
	{
		//dafault f�r BDPS-UIF
		ogCmdLineStghArray.Add(ogAppName);
		ogCmdLineStghArray.Add("");
		ogCmdLineStghArray.Add("");
		ogCmdLineStghArray.Add("ALT-ACT-ACR-APT-RWY-TWY-PST-GAT-CIC-BLT-EXT-DEN-MVT-NAT-HAG-WRO-HTY-STY-FID-STR-SPH-SEA-GHS-GEG-PER-PEF-GRM-GRN-ORG-PFC-COT-ASF-BSS-BSD-ODA-TEA-STF-HTC-WAY-PRC-CHT-TIP-BLK-HOL-WGP-SWG-PGP-AWI-CCC-VIP-AFM-ENT");//Alle
	}
	else if (olCmdLine.Find("Resource:") > -1)	// called from FIPS
	{
		if(ExtractItemList(olCmdLine,&ogCmdLineStghArray) != 4)
		{
			MessageBox(NULL,LoadStg(IDS_STRING363),"BDPS-UIF",MB_ICONERROR);
			return FALSE;
		}
		else
		{
			CString olPara4 = ogCmdLineStghArray.GetAt(3);
			ogCmdLineStghArray.SetAt(3,"ALT-ACT-ACR-APT-RWY-TWY-PST-GAT-CIC-BLT-EXT-DEN-MVT-NAT-HAG-WRO-HTY-STY-FID-STR-SPH-SEA-GHS-GEG-PER-PEF-GRM-GRN-ORG-PFC-COT-ASF-BSS-BSD-ODA-TEA-STF-HTC-WAY-PRC-CHT-TIP-BLK-HOL-WGP-SWG-PGP-AWI-CCC-VIP-AFM-ENT");
			ogCmdLineStghArray.Add(olPara4);
		}
				
	}
	else
	{
		if(ExtractItemList(olCmdLine,&ogCmdLineStghArray) != 4)
		{
			MessageBox(NULL,LoadStg(IDS_STRING363),"BDPS-UIF",MB_ICONERROR);
			return FALSE;
		}
		else
		{
			ogCmdLineStghArray.SetAt(3,"ALT-ACT-ACR-APT-RWY-TWY-PST-GAT-CIC-BLT-EXT-DEN-MVT-NAT-HAG-WRO-HTY-STY-FID-STR-SPH-SEA-GHS-GEG-PER-PEF-GRM-GRN-ORG-PFC-COT-ASF-BSS-BSD-ODA-TEA-STF-HTC-WAY-PRC-CHT-TIP-BLK-HOL-WGP-SWG-PGP-AWI-CCC-VIP-AFM-ENT");
		}
	}
	//Parameter�bergabe Ende////////////////////////////////////////////////

    if (ogCommHandler.Initialize() != true)  // connection error?
    {
        AfxMessageBox(CString("CedaCom:\n") + ogCommHandler.LastError());
        return FALSE;
    }

  	InitFont();
	CreateBrushes();


	// INIT Tablenames and Homeairport
	char pclConfigPath[256];

    if (getenv("CEDA") == NULL)
        strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
    else
        strcpy(pclConfigPath, getenv("CEDA"));


    GetPrivateProfileString("GLOBAL", "HOMEAIRPORT", "HAJ", pcgHome, sizeof pcgHome, pclConfigPath);
    GetPrivateProfileString("GLOBAL", "TABLEEXTENSION", "HAJ", pcgTableExt, sizeof pcgTableExt, pclConfigPath);
	GetPrivateProfileString(ogAppName, "EDIT_VIEW_FILTER", "FALSE", pclViewEditFilter, sizeof pclViewEditFilter, pclConfigPath);
    GetPrivateProfileString(ogAppName, "EDIT_VIEW_SORT"  , "FALSE", pclViewEditSort, sizeof pclViewEditSort, pclConfigPath);

	strcpy(CCSCedaData::pcmTableExt,    pcgTableExt);
	strcpy(CCSCedaData::pcmApplName,    pcgAppName);
    strcpy(CCSCedaData::pcmHomeAirport, pcgHome);

	if(strcmp(pclViewEditFilter, "TRUE") == 0)
		bgViewEditFilter = true;
	else
		bgViewEditFilter = false;

	if(strcmp(pclViewEditSort, "TRUE") == 0)
		bgViewEditSort = true;
	else
		bgViewEditSort = false;

	ogHome = pcgHome;
	ogTableExt = pcgTableExt;

	//INIT END/////////////////////////

	ogBcHandle.SetCloMessage(LoadStg(IDS_STRING709));

	ogBcHandle.AddTableCommand(CString("ALT") + ogTableExt, CString("IRT"), BC_ALT_CHANGE, false);
	ogBcHandle.AddTableCommand(CString("ALT") + ogTableExt, CString("URT"), BC_ALT_CHANGE, false);
	ogBcHandle.AddTableCommand(CString("ALT") + ogTableExt, CString("DRT"), BC_ALT_DELETE, false);

	ogBcHandle.AddTableCommand(CString("ACT") + ogTableExt, CString("IRT"), BC_ACT_CHANGE, false);
	ogBcHandle.AddTableCommand(CString("ACT") + ogTableExt, CString("URT"), BC_ACT_CHANGE, false);
	ogBcHandle.AddTableCommand(CString("ACT") + ogTableExt, CString("DRT"), BC_ACT_DELETE, false);

	ogBcHandle.AddTableCommand(CString("ACR") + ogTableExt, CString("IRT"), BC_ACR_CHANGE, false);
	ogBcHandle.AddTableCommand(CString("ACR") + ogTableExt, CString("URT"), BC_ACR_CHANGE, false);
	ogBcHandle.AddTableCommand(CString("ACR") + ogTableExt, CString("DRT"), BC_ACR_DELETE, false);

	ogBcHandle.AddTableCommand(CString("APT") + ogTableExt, CString("IRT"), BC_APT_CHANGE, false);
	ogBcHandle.AddTableCommand(CString("APT") + ogTableExt, CString("URT"), BC_APT_CHANGE, false);
	ogBcHandle.AddTableCommand(CString("APT") + ogTableExt, CString("DRT"), BC_APT_DELETE, false);

	ogBcHandle.AddTableCommand(CString("RWY") + ogTableExt, CString("IRT"), BC_RWY_CHANGE, false);
	ogBcHandle.AddTableCommand(CString("RWY") + ogTableExt, CString("URT"), BC_RWY_CHANGE, false);
	ogBcHandle.AddTableCommand(CString("RWY") + ogTableExt, CString("DRT"), BC_RWY_DELETE, false);

	ogBcHandle.AddTableCommand(CString("TWY") + ogTableExt, CString("IRT"), BC_TWY_CHANGE, false);
	ogBcHandle.AddTableCommand(CString("TWY") + ogTableExt, CString("URT"), BC_TWY_CHANGE, false);
	ogBcHandle.AddTableCommand(CString("TWY") + ogTableExt, CString("DRT"), BC_TWY_DELETE, false);

	ogBcHandle.AddTableCommand(CString("PST") + ogTableExt, CString("IRT"), BC_PST_CHANGE, false);
	ogBcHandle.AddTableCommand(CString("PST") + ogTableExt, CString("URT"), BC_PST_CHANGE, false);
	ogBcHandle.AddTableCommand(CString("PST") + ogTableExt, CString("DRT"), BC_PST_DELETE, false);

	ogBcHandle.AddTableCommand(CString("GAT") + ogTableExt, CString("IRT"), BC_GAT_CHANGE, false);
	ogBcHandle.AddTableCommand(CString("GAT") + ogTableExt, CString("URT"), BC_GAT_CHANGE, false);
	ogBcHandle.AddTableCommand(CString("GAT") + ogTableExt, CString("DRT"), BC_GAT_DELETE, false);

	ogBcHandle.AddTableCommand(CString("CIC") + ogTableExt, CString("IRT"), BC_CIC_CHANGE, false);
	ogBcHandle.AddTableCommand(CString("CIC") + ogTableExt, CString("URT"), BC_CIC_CHANGE, false);
	ogBcHandle.AddTableCommand(CString("CIC") + ogTableExt, CString("DRT"), BC_CIC_DELETE, false);

	ogBcHandle.AddTableCommand(CString("BLT") + ogTableExt, CString("IRT"), BC_BLT_CHANGE, false);
	ogBcHandle.AddTableCommand(CString("BLT") + ogTableExt, CString("URT"), BC_BLT_CHANGE, false);
	ogBcHandle.AddTableCommand(CString("BLT") + ogTableExt, CString("DRT"), BC_BLT_DELETE, false);

	ogBcHandle.AddTableCommand(CString("EXT") + ogTableExt, CString("IRT"), BC_EXT_CHANGE, false);
	ogBcHandle.AddTableCommand(CString("EXT") + ogTableExt, CString("URT"), BC_EXT_CHANGE, false);
	ogBcHandle.AddTableCommand(CString("EXT") + ogTableExt, CString("DRT"), BC_EXT_DELETE, false);

	ogBcHandle.AddTableCommand(CString("DEN") + ogTableExt, CString("IRT"), BC_DEN_CHANGE, false);
	ogBcHandle.AddTableCommand(CString("DEN") + ogTableExt, CString("URT"), BC_DEN_CHANGE, false);
	ogBcHandle.AddTableCommand(CString("DEN") + ogTableExt, CString("DRT"), BC_DEN_DELETE, false);

	ogBcHandle.AddTableCommand(CString("MVT") + ogTableExt, CString("IRT"), BC_MVT_CHANGE, false);
	ogBcHandle.AddTableCommand(CString("MVT") + ogTableExt, CString("URT"), BC_MVT_CHANGE, false);
	ogBcHandle.AddTableCommand(CString("MVT") + ogTableExt, CString("DRT"), BC_MVT_DELETE, false);

	ogBcHandle.AddTableCommand(CString("NAT") + ogTableExt, CString("IRT"), BC_NAT_CHANGE, false);
	ogBcHandle.AddTableCommand(CString("NAT") + ogTableExt, CString("URT"), BC_NAT_CHANGE, false);
	ogBcHandle.AddTableCommand(CString("NAT") + ogTableExt, CString("DRT"), BC_NAT_DELETE, false);

	ogBcHandle.AddTableCommand(CString("HAG") + ogTableExt, CString("IRT"), BC_HAG_CHANGE, false);
	ogBcHandle.AddTableCommand(CString("HAG") + ogTableExt, CString("URT"), BC_HAG_CHANGE, false);
	ogBcHandle.AddTableCommand(CString("HAG") + ogTableExt, CString("DRT"), BC_HAG_DELETE, false);

	ogBcHandle.AddTableCommand(CString("WRO") + ogTableExt, CString("IRT"), BC_WRO_CHANGE, false);
	ogBcHandle.AddTableCommand(CString("WRO") + ogTableExt, CString("URT"), BC_WRO_CHANGE, false);
	ogBcHandle.AddTableCommand(CString("WRO") + ogTableExt, CString("DRT"), BC_WRO_DELETE, false);

	ogBcHandle.AddTableCommand(CString("HTY") + ogTableExt, CString("IRT"), BC_HTY_CHANGE, false);
	ogBcHandle.AddTableCommand(CString("HTY") + ogTableExt, CString("URT"), BC_HTY_CHANGE, false);
	ogBcHandle.AddTableCommand(CString("HTY") + ogTableExt, CString("DRT"), BC_HTY_DELETE, false);

	ogBcHandle.AddTableCommand(CString("STY") + ogTableExt, CString("IRT"), BC_STY_CHANGE, false);
	ogBcHandle.AddTableCommand(CString("STY") + ogTableExt, CString("URT"), BC_STY_CHANGE, false);
	ogBcHandle.AddTableCommand(CString("STY") + ogTableExt, CString("DRT"), BC_STY_DELETE, false);

	ogBcHandle.AddTableCommand(CString("FID") + ogTableExt, CString("IRT"), BC_FID_CHANGE, false);
	ogBcHandle.AddTableCommand(CString("FID") + ogTableExt, CString("URT"), BC_FID_CHANGE, false);
	ogBcHandle.AddTableCommand(CString("FID") + ogTableExt, CString("DRT"), BC_FID_DELETE, false);

	ogBcHandle.AddTableCommand(CString("SEA") + ogTableExt, CString("IRT"), BC_SEA_NEW, false);
	ogBcHandle.AddTableCommand(CString("SEA") + ogTableExt, CString("URT"), BC_SEA_CHANGE, false);
	ogBcHandle.AddTableCommand(CString("SEA") + ogTableExt, CString("DRT"), BC_SEA_DELETE, false);

	ogBcHandle.AddTableCommand(CString("STR") + ogTableExt, CString("IRT"), BC_STR_NEW, false);
	ogBcHandle.AddTableCommand(CString("STR") + ogTableExt, CString("URT"), BC_STR_CHANGE, false);
	ogBcHandle.AddTableCommand(CString("STR") + ogTableExt, CString("DRT"), BC_STR_DELETE, false);

	ogBcHandle.AddTableCommand(CString("SPH") + ogTableExt, CString("IRT"), BC_SPH_NEW, false);
	ogBcHandle.AddTableCommand(CString("SPH") + ogTableExt, CString("URT"), BC_SPH_CHANGE, false);
	ogBcHandle.AddTableCommand(CString("SPH") + ogTableExt, CString("DRT"), BC_SPH_DELETE, false);
	
	ogBcHandle.AddTableCommand(CString("GHS") + ogTableExt, CString("IRT"), BC_GHS_NEW, false);
	ogBcHandle.AddTableCommand(CString("GHS") + ogTableExt, CString("URT"), BC_GHS_CHANGE, false);
	ogBcHandle.AddTableCommand(CString("GHS") + ogTableExt, CString("DRT"), BC_GHS_DELETE, false);

	ogBcHandle.AddTableCommand(CString("GEG") + ogTableExt, CString("IRT"), BC_GEG_NEW, false);
	ogBcHandle.AddTableCommand(CString("GEG") + ogTableExt, CString("URT"), BC_GEG_CHANGE, false);
	ogBcHandle.AddTableCommand(CString("GEG") + ogTableExt, CString("DRT"), BC_GEG_DELETE, false);

	ogBcHandle.AddTableCommand(CString("PER") + ogTableExt, CString("IRT"), BC_PER_NEW, false);
	ogBcHandle.AddTableCommand(CString("PER") + ogTableExt, CString("URT"), BC_PER_CHANGE, false);
	ogBcHandle.AddTableCommand(CString("PER") + ogTableExt, CString("DRT"), BC_PER_DELETE, false);
	
	ogBcHandle.AddTableCommand(CString("PEF") + ogTableExt, CString("IRT"), BC_PEF_NEW, false);
	ogBcHandle.AddTableCommand(CString("PEF") + ogTableExt, CString("URT"), BC_PEF_CHANGE, false);
	ogBcHandle.AddTableCommand(CString("PEF") + ogTableExt, CString("DRT"), BC_PEF_DELETE, false);
	
	ogBcHandle.AddTableCommand(CString("GRM") + ogTableExt, CString("IRT"), BC_GRM_NEW, false);
	ogBcHandle.AddTableCommand(CString("GRM") + ogTableExt, CString("URT"), BC_GRM_CHANGE, false);
	ogBcHandle.AddTableCommand(CString("GRM") + ogTableExt, CString("DRT"), BC_GRM_DELETE, false);
	
	ogBcHandle.AddTableCommand(CString("GRN") + ogTableExt, CString("IRT"), BC_GRN_NEW, false);
	ogBcHandle.AddTableCommand(CString("GRN") + ogTableExt, CString("URT"), BC_GRN_CHANGE, false);
	ogBcHandle.AddTableCommand(CString("GRN") + ogTableExt, CString("DRT"), BC_GRN_DELETE, false);

	ogBcHandle.AddTableCommand(CString("ORG") + ogTableExt, CString("IRT"), BC_ORG_NEW, false);
	ogBcHandle.AddTableCommand(CString("ORG") + ogTableExt, CString("URT"), BC_ORG_CHANGE, false);
	ogBcHandle.AddTableCommand(CString("ORG") + ogTableExt, CString("DRT"), BC_ORG_DELETE, false);

	ogBcHandle.AddTableCommand(CString("PFC") + ogTableExt, CString("IRT"), BC_PFC_NEW, false);
	ogBcHandle.AddTableCommand(CString("PFC") + ogTableExt, CString("URT"), BC_PFC_CHANGE, false);
	ogBcHandle.AddTableCommand(CString("PFC") + ogTableExt, CString("DRT"), BC_PFC_DELETE, false);

	ogBcHandle.AddTableCommand(CString("COT") + ogTableExt, CString("IRT"), BC_COT_NEW, false);
	ogBcHandle.AddTableCommand(CString("COT") + ogTableExt, CString("URT"), BC_COT_CHANGE, false);
	ogBcHandle.AddTableCommand(CString("COT") + ogTableExt, CString("DRT"), BC_COT_DELETE, false);
	
	ogBcHandle.AddTableCommand(CString("ASF") + ogTableExt, CString("IRT"), BC_ASF_NEW, false);
	ogBcHandle.AddTableCommand(CString("ASF") + ogTableExt, CString("URT"), BC_ASF_CHANGE, false);
	ogBcHandle.AddTableCommand(CString("ASF") + ogTableExt, CString("DRT"), BC_ASF_DELETE, false);

	ogBcHandle.AddTableCommand(CString("BSS") + ogTableExt, CString("IRT"), BC_BSS_NEW, false);
	ogBcHandle.AddTableCommand(CString("BSS") + ogTableExt, CString("URT"), BC_BSS_CHANGE, false);
	ogBcHandle.AddTableCommand(CString("BSS") + ogTableExt, CString("DRT"), BC_BSS_DELETE, false);

	ogBcHandle.AddTableCommand(CString("BSD") + ogTableExt, CString("IRT"), BC_BSD_NEW, false);
	ogBcHandle.AddTableCommand(CString("BSD") + ogTableExt, CString("URT"), BC_BSD_CHANGE, false);
	ogBcHandle.AddTableCommand(CString("BSD") + ogTableExt, CString("DRT"), BC_BSD_DELETE, false);
	
	ogBcHandle.AddTableCommand(CString("ODA") + ogTableExt, CString("IRT"), BC_ODA_NEW, false);
	ogBcHandle.AddTableCommand(CString("ODA") + ogTableExt, CString("URT"), BC_ODA_CHANGE, false);
	ogBcHandle.AddTableCommand(CString("ODA") + ogTableExt, CString("DRT"), BC_ODA_DELETE, false);
	
	ogBcHandle.AddTableCommand(CString("TEA") + ogTableExt, CString("IRT"), BC_TEA_NEW, false);
	ogBcHandle.AddTableCommand(CString("TEA") + ogTableExt, CString("URT"), BC_TEA_CHANGE, false);
	ogBcHandle.AddTableCommand(CString("TEA") + ogTableExt, CString("DRT"), BC_TEA_DELETE, false);
	
	ogBcHandle.AddTableCommand(CString("STF") + ogTableExt, CString("IRT"), BC_STF_NEW, false);
	ogBcHandle.AddTableCommand(CString("STF") + ogTableExt, CString("URT"), BC_STF_CHANGE, false);
	ogBcHandle.AddTableCommand(CString("STF") + ogTableExt, CString("DRT"), BC_STF_DELETE, false);

	ogBcHandle.AddTableCommand(CString("WAY") + ogTableExt, CString("IRT"), BC_WAY_NEW, false);
	ogBcHandle.AddTableCommand(CString("WAY") + ogTableExt, CString("URT"), BC_WAY_CHANGE, false);
	ogBcHandle.AddTableCommand(CString("WAY") + ogTableExt, CString("DRT"), BC_WAY_DELETE, false);

	ogBcHandle.AddTableCommand(CString("PRC") + ogTableExt, CString("IRT"), BC_PRC_NEW, false);
	ogBcHandle.AddTableCommand(CString("PRC") + ogTableExt, CString("URT"), BC_PRC_CHANGE, false);
	ogBcHandle.AddTableCommand(CString("PRC") + ogTableExt, CString("DRT"), BC_PRC_DELETE, false);

	ogBcHandle.AddTableCommand(CString("CHT") + ogTableExt, CString("IRT"), BC_CHT_NEW, false);
	ogBcHandle.AddTableCommand(CString("CHT") + ogTableExt, CString("URT"), BC_CHT_CHANGE, false);
	ogBcHandle.AddTableCommand(CString("CHT") + ogTableExt, CString("DRT"), BC_CHT_DELETE, false);

	ogBcHandle.AddTableCommand(CString("TIP") + ogTableExt, CString("IRT"), BC_TIP_NEW, false);
	ogBcHandle.AddTableCommand(CString("TIP") + ogTableExt, CString("URT"), BC_TIP_CHANGE, false);
	ogBcHandle.AddTableCommand(CString("TIP") + ogTableExt, CString("DRT"), BC_TIP_DELETE, false);

	ogBcHandle.AddTableCommand(CString("BLK") + ogTableExt, CString("IRT"), BC_BLK_NEW, false);
	ogBcHandle.AddTableCommand(CString("BLK") + ogTableExt, CString("URT"), BC_BLK_CHANGE, false);
	ogBcHandle.AddTableCommand(CString("BLK") + ogTableExt, CString("DRT"), BC_BLK_DELETE, false);

	ogBcHandle.AddTableCommand(CString("HOL") + ogTableExt, CString("IRT"), BC_HOL_NEW, false);
	ogBcHandle.AddTableCommand(CString("HOL") + ogTableExt, CString("URT"), BC_HOL_CHANGE, false);
	ogBcHandle.AddTableCommand(CString("HOL") + ogTableExt, CString("DRT"), BC_HOL_DELETE, false);

	ogBcHandle.AddTableCommand(CString("WGP") + ogTableExt, CString("IRT"), BC_WGP_NEW, false);
	ogBcHandle.AddTableCommand(CString("WGP") + ogTableExt, CString("URT"), BC_WGP_CHANGE, false);
	ogBcHandle.AddTableCommand(CString("WGP") + ogTableExt, CString("DRT"), BC_WGP_DELETE, false);

	ogBcHandle.AddTableCommand(CString("PGP") + ogTableExt, CString("IRT"), BC_PGP_NEW, false);
	ogBcHandle.AddTableCommand(CString("PGP") + ogTableExt, CString("URT"), BC_PGP_CHANGE, false);
	ogBcHandle.AddTableCommand(CString("PGP") + ogTableExt, CString("DRT"), BC_PGP_DELETE, false);

	ogBcHandle.AddTableCommand(CString("AWI") + ogTableExt, CString("IRT"), BC_AWI_NEW, false);
	ogBcHandle.AddTableCommand(CString("AWI") + ogTableExt, CString("URT"), BC_AWI_CHANGE, false);
	ogBcHandle.AddTableCommand(CString("AWI") + ogTableExt, CString("DRT"), BC_AWI_DELETE, false);

	ogBcHandle.AddTableCommand(CString("CCC") + ogTableExt, CString("IRT"), BC_CCC_NEW, false);
	ogBcHandle.AddTableCommand(CString("CCC") + ogTableExt, CString("URT"), BC_CCC_CHANGE, false);
	ogBcHandle.AddTableCommand(CString("CCC") + ogTableExt, CString("DRT"), BC_CCC_DELETE, false);

	ogBcHandle.AddTableCommand(CString("VIP") + ogTableExt, CString("IRT"), BC_VIP_NEW, false);
	ogBcHandle.AddTableCommand(CString("VIP") + ogTableExt, CString("URT"), BC_VIP_CHANGE, false);
	ogBcHandle.AddTableCommand(CString("VIP") + ogTableExt, CString("DRT"), BC_VIP_DELETE, false);

	ogBcHandle.AddTableCommand(CString("AFM") + ogTableExt, CString("IRT"), BC_AFM_NEW, false);
	ogBcHandle.AddTableCommand(CString("AFM") + ogTableExt, CString("URT"), BC_AFM_CHANGE, false);
	ogBcHandle.AddTableCommand(CString("AFM") + ogTableExt, CString("DRT"), BC_AFM_DELETE, false);

	ogBcHandle.AddTableCommand(CString("ENT") + ogTableExt, CString("IRT"), BC_ENT_NEW, false);
	ogBcHandle.AddTableCommand(CString("ENT") + ogTableExt, CString("URT"), BC_ENT_CHANGE, false);
	ogBcHandle.AddTableCommand(CString("ENT") + ogTableExt, CString("DRT"), BC_ENT_DELETE, false);

	//*** 07.09.99 SHA *** (1)
		
	//////////////////////////////////////

	CLoginDialog olLoginDlg(pcgTableExt,ogAppName,ogCommHandler.pcmReqId);
	if(ogCmdLineStghArray.GetAt(0) != ogAppName)
	{
		if(olLoginDlg.Login(ogCmdLineStghArray.GetAt(1),ogCmdLineStghArray.GetAt(2)) == true)
		{
			

			//*** 10.09.99 SHA ***
			//*** F�R UFISCLASSES ***
			ogCfgData.ReadUfisCedaConfig();

			ogCfgData.ReadCfgData();

			InitBDPSTableDefaultView();
			CStammdaten olStammdatenDlg;
			olStammdatenDlg.DoModal();
		}
	}
	else
	{
		if( olLoginDlg.DoModal() != IDCANCEL )
		{
						
			//*** 10.09.99 SHA ***
			//*** F�R UFISCLASSES ***
			ogCfgData.ReadUfisCedaConfig();

			ogCfgData.ReadCfgData();
			
			InitBDPSTableDefaultView();
			int ilStartApp = IDOK;
			if(ogPrivList.GetStat("InitModu") == '1')
			{
				RegisterDlg olRegisterDlg;
				ilStartApp = olRegisterDlg.DoModal();
			}
			if(ilStartApp == IDOK)
			{
				//*** 07.09.99 SHA ***
				//*** MOVED FROM POS. (1) ***
				// for referenz check in CheckReferenz
				ogBCD.SetTableExtension(ogTableExt);
				ogBCD.SetHomeAirport(ogHome);
				ogBCD.SetObject("DSR");
				ogBCD.SetObject("GSP");
				ogBCD.SetObject("ROS");
				ogBCD.SetObject("SDT");
				ogBCD.SetObject("GRM");

				CStammdaten olStammdatenDlg;
				olStammdatenDlg.DoModal();	
			}
		}
	}


	return FALSE;
}
bool CBDPSUIFApp::OnSpecialHelp()
{
	AfxGetApp()->WinHelp(0,HELP_CONTENTS);
	return true;
}