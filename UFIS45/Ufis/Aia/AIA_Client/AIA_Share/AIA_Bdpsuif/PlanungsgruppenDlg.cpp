// PlanungsgruppenDlg.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "bdpsuif.h"
#include "PlanungsgruppenDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld PlanungsgruppenDlg 

PlanungsgruppenDlg::PlanungsgruppenDlg(PGPDATA *prpPgp, CWnd* pParent /*=NULL*/)
	:CDialog(PlanungsgruppenDlg::IDD, pParent)
{
	pomPgp = prpPgp;
	
	pomTable = new CCSTable;

	//{{AFX_DATA_INIT(PlanungsgruppenDlg)
	//}}AFX_DATA_INIT
}
//------------------------------------------------------------------------------------------------------------------------


PlanungsgruppenDlg::~PlanungsgruppenDlg()
{
	delete pomTable;
}
//------------------------------------------------------------------------------------------------------------------------


void PlanungsgruppenDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(PlanungsgruppenDlg)
	DDX_Control(pDX, IDC_TYPE, m_TYPE);
	DDX_Control(pDX, IDOK, m_OK);
	DDX_Control(pDX, IDC_HELP_ORG, m_HELP_ORG);
	DDX_Control(pDX, IDC_CDAT_D, m_CDAT_D);
	DDX_Control(pDX, IDC_CDAT_T, m_CDAT_T);
	DDX_Control(pDX, IDC_LSTU_D, m_LSTU_D);
	DDX_Control(pDX, IDC_LSTU_T, m_LSTU_T);
	DDX_Control(pDX, IDC_USEC, m_USEC);
	DDX_Control(pDX, IDC_USEU, m_USEU);
	DDX_Control(pDX, IDC_REMA, m_REMA);
	DDX_Control(pDX, IDC_PLPC, m_PGPC);
	DDX_Control(pDX, IDC_PLPN, m_PGPN);
	DDX_Control(pDX, IDC_COLTESTFIELD, m_ColTestField);
	//}}AFX_DATA_MAP

}


//------------------------------------------------------------------------------------------------------------------------
// Message Handler
//------------------------------------------------------------------------------------------------------------------------

BEGIN_MESSAGE_MAP(PlanungsgruppenDlg, CDialog)
	//{{AFX_MSG_MAP(PlanungsgruppenDlg)
	ON_BN_CLICKED(IDC_B1, OnB1)
	ON_BN_CLICKED(IDC_B2, OnB2)
	ON_BN_CLICKED(IDC_B3, OnB3)
	ON_BN_CLICKED(IDC_B4, OnB4)
	ON_BN_CLICKED(IDC_B5, OnB5)
	ON_MESSAGE(WM_TABLE_IPEDIT_KILLFOCUS, OnTableIPEditKillfocus)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()



BOOL PlanungsgruppenDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	m_Caption = LoadStg(IDS_STRING200) + " <" + m_Caption + ">";
	SetWindowText(m_Caption);
	
// Checking Privileges (see BDPS-SEC)
	char clStat = ogPrivList.GetStat("PLANUNGSGRUPPENDLG.m_OK");
	SetWndStatAll(clStat,m_OK);
	if (pomPgp != NULL)
	{
		m_CDAT_D.SetBKColor(SILVER);
		m_CDAT_D.SetInitText(pomPgp->Cdat.Format("%d.%m.%Y"));
		//m_CDAT_D.SetSecState(ogPrivList.GetStat("PLANUNGSGRUPPENDLG.m_CDAT"));
		// - - - - - - - - - - - - - - - - - -
		m_CDAT_T.SetBKColor(SILVER);
		m_CDAT_T.SetInitText(pomPgp->Cdat.Format("%H:%M"));
		//m_CDAT_T.SetSecState(ogPrivList.GetStat("PLANUNGSGRUPPENDLG.m_CDAT"));
		//------------------------------------
		m_LSTU_D.SetBKColor(SILVER);
		m_LSTU_D.SetInitText(pomPgp->Lstu.Format("%d.%m.%Y"));
		//m_LSTU_D.SetSecState(ogPrivList.GetStat("PLANUNGSGRUPPENDLG.m_LSTU"));
		// - - - - - - - - - - - - - - - - - -
		m_LSTU_T.SetBKColor(SILVER);
		m_LSTU_T.SetInitText(pomPgp->Lstu.Format("%H:%M"));
		//m_LSTU_T.SetSecState(ogPrivList.GetStat("PLANUNGSGRUPPENDLG.m_LSTU"));
		//------------------------------------
		m_USEC.SetBKColor(SILVER);
		m_USEC.SetInitText(pomPgp->Usec);
		//m_USEC.SetSecState(ogPrivList.GetStat("PLANUNGSGRUPPENDLG.m_USEC"));
		//------------------------------------
		m_USEU.SetBKColor(SILVER);
		m_USEU.SetInitText(pomPgp->Useu);
		//m_USEU.SetSecState(ogPrivList.GetStat("PLANUNGSGRUPPENDLG.m_USEU"));
		//------------------------------------
		m_PGPC.SetBKColor(YELLOW);
		m_PGPC.SetFormat("x|#x|#x|#x|#x|#");
		m_PGPC.SetTextLimit(0,5);
		m_PGPC.SetTextErrColor(RED);
		m_PGPC.SetInitText(pomPgp->Pgpc);
		//m_PGPC.SetSecState(ogPrivList.GetStat("PLANUNGSGRUPPENDLG.m_PGPC"));
		//------------------------------------
		m_PGPN.SetBKColor(YELLOW);
		m_PGPN.SetTypeToString("X(40)",40,1);
		m_PGPN.SetTextErrColor(RED);
		m_PGPN.SetInitText(pomPgp->Pgpn);
		//m_PGPN.SetSecState(ogPrivList.GetStat("PLANUNGSGRUPPENDLG.m_PGPN"));
		//------------------------------------
		m_TYPE.SetBKColor(YELLOW);
		m_TYPE.AddString(LoadStg(IDS_STRING177));
		m_TYPE.SetCurSel(0);
		//char clStat2 = '1';
		//char clStat1 = ogPrivList.GetStat("PLANUNGSGRUPPENDLG.m_TYPE");
		//SetWndStatPrio_1(clStat1,clStat2,m_TYPE);
		//------------------------------------
		m_REMA.SetTypeToString("X(60)",60,0);
		m_REMA.SetTextErrColor(RED);
		m_REMA.SetInitText(pomPgp->Rema);
		//m_REMA.SetSecState(ogPrivList.GetStat("PLANUNGSGRUPPENDLG.m_REMA"));
	}
	m_ColTestField.SetTypeToInt(0,9999);

	InitTables();

//	m_TYPE.InvalidateRect(NULL);
//	m_TYPE.UpdateWindow();
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}
//------------------------------------------------------------------------------------------------------------------------


void PlanungsgruppenDlg::OnOK() 
{
	AfxGetApp()->DoWaitCursor(1);	// start wait cursor (hourglass)
	CString olErrorText;
	bool blStatus = true;

// Checking filling state of edit fields and table
	// code-field has to be filled in
	if(m_PGPC.GetWindowTextLength() == 0)
	{
		olErrorText += LoadStg(IDS_STRING237) +  ogNoData;
		blStatus = false;
	}

	if(m_PGPC.GetStatus() == false)
	{
		blStatus = false;
		if(!(m_PGPC.GetWindowTextLength() == 0))
		{
			olErrorText += LoadStg(IDS_STRING237) + ogNotFormat;
		}
	}

	// name field has to be filled in
	if(m_PGPN.GetWindowTextLength() == 0)
	{
		olErrorText += LoadStg(IDS_STRING229) +  ogNoData;
		blStatus = false;
	}

	if(m_PGPN.GetStatus() == false)
	{
		blStatus = false;
		if(!(m_PGPN.GetWindowTextLength() == 0))
		{
			olErrorText += LoadStg(IDS_STRING229) + ogNotFormat;
		}
	}
	if(m_TYPE.GetWindowTextLength() == 0)
	{
		blStatus = false;
		olErrorText += LoadStg(IDS_STRING595)+ ogNoData;
	}

	// I don't think it should be mandatory to fill in the remark field, so I commented it out (MNE, Jan 21, 1999)  
	/*if(m_REMA.GetStatus() == false)
	{
		blStatus = false;
		olErrorText += LoadStg(IDS_STRING238) + ogNotFormat;
	}*/

	///////////////////////////////////////////////////////////////////
	//Konsistenz-Check (Code PGPC mustn't be used twice)
	if(blStatus == true)
	{
		char clWhere[100];
		CString olTmpCode;
		CCSPtrArray <PGPDATA> olPgp;
		m_PGPC.GetWindowText(olTmpCode);
		sprintf(clWhere,"WHERE PGPC='%s'",olTmpCode);
		if(ogPgpData.ReadSpecialData(&olPgp,clWhere,"URNO,PGPC",false) == true)
		{
			if(olPgp[0].Urno != pomPgp->Urno)
			{
				blStatus = false;
				olErrorText += LoadStg(IDS_STRING237) + LoadStg(IDS_EXIST);
			}
		}
		olPgp.DeleteAll();
	}
	
	///////////////////////////////////////////////////////////////////
	AfxGetApp()->DoWaitCursor(-1);
	CString olCol1, olCol2, olCol3;
	olCol1 = olCol2 = olCol3 = CString("");
	CCSPtrArray <TABLE_COLUMN> olCols;
	char clWhere[100];

	if (blStatus == true)
	{
		bool bmExit = false;

		for (int i = 0; i <= 50 && !bmExit; i++)
		{
			char pclZeile[10];
			pomTable->GetTextLineColumns(&olCols, i);
			
			// first line mustn't be empty
			if (i == 0 && olCols[0].Text == CString(""))
			{
				blStatus = false;
				sprintf(pclZeile, "%d", i+1);
				olErrorText += LoadStg(IDS_STRING200) + LoadStg(IDS_STRING85) + CString(pclZeile) + LoadStg(IDS_STRING86) + CString("1") + LoadStg(IDS_STRING87);
			}
			// if first column valid, empty following columns get default value ("1")
			if (olCols[0].Text != CString(""))
			{
				sprintf(clWhere,"WHERE FCTC='%s'",olCols[0].Text);
				if(ogPfcData.ReadSpecialData(NULL,clWhere,"FCTC",false) == false)
				{
					blStatus = false;
					olErrorText += LoadStg(IDS_STRING117) + olCols[0].Text + LoadStg(IDS_STRING118) + CString("\n");
				}

				// Check Col1 and Col2 at valid values
				m_ColTestField.SetWindowText(olCols[1].Text);
				if(m_ColTestField.GetStatus() == false)
				{
					blStatus = false;
					sprintf(pclZeile, "%d:", i+1);
					olErrorText += LoadStg(IDS_STRING113) + CString(pclZeile) + LoadStg(IDS_STRING114) + CString("\n");
				}
				m_ColTestField.SetWindowText(olCols[2].Text);
				if(m_ColTestField.GetStatus() == false)
				{
					blStatus = false;
					sprintf(pclZeile, "%d:", i+1);
					olErrorText += LoadStg(IDS_STRING113) + CString(pclZeile) + LoadStg(IDS_STRING115) + CString("\n");
				}
				// End Check Col1 and Col2 at valid values
				if(blStatus == true)
				{
					if (olCols[1].Text == CString(""))
					{
						olCols[1].Text = CString("1");
					}
					
					if (olCols[2].Text != CString("") && atol(olCols[2].Text) < atol(olCols[1].Text))
					{
						blStatus = false;
						sprintf(pclZeile, "%d:", i+1);
						olErrorText += LoadStg(IDS_STRING200) + LoadStg(IDS_STRING85) + CString(pclZeile) + LoadStg(IDS_STRING112) + CString("\n");
					}

					olCol1 = olCol1 + CString("|") + olCols[0].Text;
					olCol2 = olCol2 + CString("|") + olCols[1].Text;
					olCol3 = olCol3 + CString("|") + olCols[2].Text;
				}
			}
			else	// if first column not valid: exit for-loop
			{
				bmExit = true;
			}
			olCols.RemoveAll();
		} 
		if(olCol1.GetLength() > 1)
			olCol1 = olCol1.Mid(1);
		if(olCol2.GetLength() > 1)
			olCol2 = olCol2.Mid(1);
		if(olCol3.GetLength() > 1)
			olCol3 = olCol3.Mid(1);
	}

	if (blStatus)
	{
		CString olType;
		m_TYPE.GetWindowText(olType);

		CString olName;
		m_PGPN.GetWindowText(olName);

		CString olCode;
		m_PGPC.GetWindowText(olCode);
	
		strcpy(pomPgp->Type, GetCodeShort(olType));
		strcpy(pomPgp->Pgpn, olName);
		strcpy(pomPgp->Pgpc, olCode);
		strcpy(pomPgp->Pgpm, olCol1);
		strcpy(pomPgp->Minm, olCol2);
		strcpy(pomPgp->Maxm, olCol3);
	
		// wandelt Return in Blank //
		CString olTemp;
		m_REMA.GetWindowText(olTemp);
		for(int i = 0; i > -1;)
		{
			i = olTemp.Find("\r\n");
			if(i != -1)
			{
				if(olTemp.GetLength() > (i+2))
					olTemp = olTemp.Left(i) + " " + olTemp.Mid(i+2);
				else
					olTemp = olTemp.Left(i);
			}
		}
		strcpy(pomPgp->Rema,olTemp);
		////////////////////////////
		CDialog::OnOK();
	}
	else
	{
		Beep(440,70);
		MessageBox(olErrorText,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
		m_PGPC.SetFocus();
		m_PGPC.SetSel(0,-1);
	}
}
//------------------------------------------------------------------------------------------------------------------------


void PlanungsgruppenDlg::OnCancel() 
{
	// TODO: Add extra cleanup here
	
	CDialog::OnCancel();
}
//------------------------------------------------------------------------------------------------------------------------


void PlanungsgruppenDlg::OnB1() 
{
	OnBf_Button(0);
}
//------------------------------------------------------------------------------------------------------------------------


void PlanungsgruppenDlg::OnB2() 
{
	OnBf_Button(1);
}
//------------------------------------------------------------------------------------------------------------------------


void PlanungsgruppenDlg::OnB3() 
{
	OnBf_Button(2);
}
//------------------------------------------------------------------------------------------------------------------------


void PlanungsgruppenDlg::OnB4() 
{
	OnBf_Button(3);
}
//------------------------------------------------------------------------------------------------------------------------


void PlanungsgruppenDlg::OnB5() 
{
	OnBf_Button(4);
}


//------------------------------------------------------------------------------------------------------------------------
// Implementierung
//------------------------------------------------------------------------------------------------------------------------
void PlanungsgruppenDlg::InitTables()
{
	int ili;
	CRect olRect;
	m_HELP_ORG.GetWindowRect(olRect);
	ScreenToClient(olRect);
	olRect.DeflateRect(2,2);

	int ilTab = olRect.right - olRect.left - 15; 
	pomTable->SetHeaderSpacing(0);
	pomTable->SetMiniTable();
	pomTable->SetTableEditable(true);
    pomTable->SetTableData(this, olRect.left, olRect.right, olRect.top, olRect.bottom);
	pomTable->SetSelectMode(0);

	CCSPtrArray <TABLE_HEADER_COLUMN> omHeaderDataArray;
	TABLE_HEADER_COLUMN *prlHeader[3];
	pomTable->SetShowSelection(false);
	pomTable->ResetContent();

	prlHeader[0] = new TABLE_HEADER_COLUMN;
	prlHeader[0]->Alignment = COLALIGN_CENTER;
	prlHeader[0]->Length = (int)(ilTab/3 + 20); 
	prlHeader[0]->Font = &ogCourier_Regular_8;
	prlHeader[0]->Text = CString(""); 

	prlHeader[1] = new TABLE_HEADER_COLUMN;
	prlHeader[1]->Alignment = COLALIGN_CENTER;
	prlHeader[1]->Length = (int)(ilTab/3 - 13); 
	prlHeader[1]->Font = &ogCourier_Regular_8;
	prlHeader[1]->Text = CString("");

	prlHeader[2] = new TABLE_HEADER_COLUMN;
	prlHeader[2]->Alignment = COLALIGN_CENTER;
	prlHeader[2]->Length = (int)(ilTab/3 - 17); 
	prlHeader[2]->Font = &ogCourier_Regular_8;
	prlHeader[2]->Text = CString("");

	for(ili = 0; ili < 3; ili++)
	{
		omHeaderDataArray.Add(prlHeader[ili]);
	}
	pomTable->SetHeaderFields(omHeaderDataArray);
	pomTable->SetDefaultSeparator();
	pomTable->SetTableEditable(true);
	pomTable->DisplayTable();
	omHeaderDataArray.DeleteAll();


// Set attributes for table
	CCSEDIT_ATTRIB rlAttribC1,
				   rlAttribC2,
				   rlAttribC3;

	rlAttribC1.Format = "X(5)";
	rlAttribC1.TextMinLenght = 0;
	rlAttribC1.TextMaxLenght = 5;
	rlAttribC1.Style = ES_UPPERCASE;
	
	rlAttribC2.TextMinLenght = 0;
	rlAttribC2.TextMaxLenght = 5;
	rlAttribC2.Type = KT_INT;
	rlAttribC2.RangeFrom = 0;
	rlAttribC2.RangeTo = 32767;
	rlAttribC2.ErrColor = RGB(200,0,0);

	rlAttribC3.TextMinLenght = 0;
	rlAttribC3.TextMaxLenght = 5;
	rlAttribC3.Type = KT_INT;
	rlAttribC3.RangeFrom = 0;
	rlAttribC3.RangeTo = 32767;
	rlAttribC3.ErrColor = RGB(200,0,0);


	CCSPtrArray<TABLE_COLUMN> olColList;
	TABLE_COLUMN rlColumnData;
	rlColumnData.Font = &ogCourier_Regular_8;//ogMS_Sans_Serif_8;
	

	// There has to be an item within a field of CCSTable to get valid return values from "ItemFromPoint()"
	// This method can be called regardless of the value of pomPgp. If pomPgp was NULL, there wouldn't be any items within e
	// the table. That's why I fill some fields with empty strings:
	for (int ilLC = 0; ilLC < 51; ilLC++)
	{
		rlColumnData.Font = &ogCourier_Regular_8;
		rlColumnData.SeparatorType = SEPA_NONE;
		rlColumnData.VerticalSeparator = SEPA_LIKEVERTICAL;
		rlColumnData.Text = CString("");
		rlColumnData.blIsEditable = true;
		rlColumnData.EditAttrib = rlAttribC1;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		rlColumnData.Font = &ogCourier_Regular_8;
		rlColumnData.SeparatorType = SEPA_NONE;
		rlColumnData.VerticalSeparator = SEPA_LIKEVERTICAL;
		rlColumnData.Text = CString("");
		rlColumnData.blIsEditable = true;
		rlColumnData.EditAttrib = rlAttribC2;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		rlColumnData.Font = &ogCourier_Regular_8;
		rlColumnData.SeparatorType = SEPA_NONE;
		rlColumnData.VerticalSeparator = SEPA_LIKEVERTICAL;
		rlColumnData.Text = CString("");
		rlColumnData.blIsEditable = true;
		rlColumnData.EditAttrib = rlAttribC3;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		pomTable->AddTextLine(olColList, (void*)(NULL));
		olColList.DeleteAll();
	}

	
	
	if (pomPgp != NULL)
	{	
		CString olPgpm;
		CString olMinm;
		CString olMaxm;

		CStringArray rlPgpm;
		CStringArray rlMinm;
		CStringArray rlMaxm;

		olPgpm = pomPgp->Pgpm;
		olMinm = pomPgp->Minm;
		olMaxm = pomPgp->Maxm;
		
		char clSeparator;
		clSeparator = '|';

		ExtractItemList(olPgpm, &rlPgpm, clSeparator);
		ExtractItemList(olMinm, &rlMinm, clSeparator);
		ExtractItemList(olMaxm, &rlMaxm, clSeparator);

		int ilLineNo = 0;

		// now we can fill the table with real data:
		for (int ilLC = 0; ilLC < rlPgpm.GetSize() && ilLC < rlMinm.GetSize(); ilLC++)
		{
			// Column 1 (Members)
			rlColumnData.SeparatorType = SEPA_NONE;
			rlColumnData.VerticalSeparator = SEPA_LIKEVERTICAL;
			rlColumnData.Text = rlPgpm.GetAt(ilLC);
			rlColumnData.blIsEditable = true;
			rlColumnData.EditAttrib = rlAttribC1;
			olColList.NewAt(olColList.GetSize(), rlColumnData);

			// Column 2 (Min)
			rlColumnData.SeparatorType = SEPA_NONE;
			rlColumnData.VerticalSeparator = SEPA_LIKEVERTICAL;
			rlColumnData.Text = rlMinm.GetAt(ilLC);
			rlColumnData.blIsEditable = true;
			rlColumnData.EditAttrib = rlAttribC2;
			olColList.NewAt(olColList.GetSize(), rlColumnData);

			// Column 3 (Max)
			rlColumnData.SeparatorType = SEPA_NONE;
			rlColumnData.VerticalSeparator = SEPA_LIKEVERTICAL;
			if(ilLC < rlMaxm.GetSize())
				rlColumnData.Text = rlMaxm.GetAt(ilLC);
			else
				rlColumnData.Text = "";
			rlColumnData.blIsEditable = true;
			rlColumnData.EditAttrib = rlAttribC3;
			olColList.NewAt(olColList.GetSize(), rlColumnData);
			
			// Changing table data
			pomTable->ChangeTextLine(ilLineNo, &olColList, (void*)(NULL));
			olColList.DeleteAll();
			ilLineNo++;
		}
	}
	pomTable->DisplayTable();
	pomTable->SetColumnEditable(0, true);
	pomTable->SetColumnEditable(1, true);
	pomTable->SetColumnEditable(2, true);
}
//------------------------------------------------------------------------------------------------------------------------


void PlanungsgruppenDlg::OnBf_Button(int ipColY)
{
	CStringArray olCol1, olCol2, olCol3;
	CCSPtrArray<PFCDATA> olList;
	AfxGetApp()->DoWaitCursor(1);
	if(ogPfcData.ReadSpecialData(&olList, "", "URNO,FCTC,FCTN", false) == true)
	{
		AfxGetApp()->DoWaitCursor(-1);
		for(int i = 0; i < olList.GetSize(); i++)
		{
			char pclUrno[20]="";
			sprintf(pclUrno, "%ld", olList[i].Urno);
			olCol3.Add(CString(pclUrno));
			olCol1.Add(CString(olList[i].Fctc));
			olCol2.Add(CString(olList[i].Fctn));
		}
		AwDlg *pomDlg = new AwDlg(&olCol1, &olCol2, &olCol3, LoadStg(IDS_STRING237), LoadStg(IDS_STRING229), this);
		if(pomDlg->DoModal() == IDOK)
		{
			int ilColX = -1, ilColY = -1;
			CCSTableListBox *pomListBox = pomTable->pomListBox;

			if(pomListBox->GetVisibleLeftTopColumn(ilColX, ilColY) == true)
			{
				ilColY += ipColY;
				pomTable->SetIPValue(ilColY, 0, pomDlg->omReturnString);
				pomTable->imCurrentColumn = 0;
				pomTable->MakeInplaceEdit(ilColY);
			}
		}
		delete pomDlg;
	}
	else
	{
		MessageBox(LoadStg(IDS_STRING321), LoadStg(IDS_STRING149), (MB_ICONEXCLAMATION|MB_OK));
	}
	olList.DeleteAll();

	AfxGetApp()->DoWaitCursor(-1);
}
//------------------------------------------------------------------------------------------------------------------------


CString PlanungsgruppenDlg::GetCodeShort(CString opCode)
{
	CString olReturnString;
	olReturnString = "";

	if(opCode == LoadStg(IDS_STRING177)) //Funktionen 
	{
		olReturnString = "PFC";
	}
/*	else if(opCode == LoadStg())
	{
		olReturnString = "???";
	}*/

	return olReturnString;
}
//------------------------------------------------------------------------------------------------------------------------


LONG PlanungsgruppenDlg::OnTableIPEditKillfocus( UINT wParam, LPARAM lParam)
{
	CCSIPEDITNOTIFY *prlNotify = (CCSIPEDITNOTIFY*)lParam;

	if(prlNotify->Text.IsEmpty())
		return 0L;
	
	if(prlNotify->SourceTable == pomTable) 
	{
		if(prlNotify->Column == 0)
		{
			CedaPfcData olData;
			CCSPtrArray <PFCDATA> omData;
			char pclWhere[128];
			sprintf(pclWhere,"WHERE FCTC='%s'", prlNotify->Text);
			prlNotify->UserStatus = true;
			prlNotify->Status = olData.ReadSpecialData(&omData, pclWhere, "FCTC", false);
			omData.DeleteAll();
			return 0L;
		}
	}
	return 0L;
}


