#ifndef __LEISTUNGSKATALOGTABLEVIEWER_H__
#define __LEISTUNGSKATALOGTABLEVIEWER_H__

#include "stdafx.h"
#include "CedaGhsData.h"
#include "CCSTable.h"
#include "CViewer.h"
#include "CCSPrint.h"


struct LEISTUNGSKATALOGTABLE_LINEDATA
{
	long	Urno;
	CString	Lknm;
	CString	Lkcc;
	CString	Lkco;
	CString	Lkar;
	CString	Atrn;
	CString	Lkan;
	CString	Lkvb;
	CString	Lknb;
	CString	Lkda;
	CString	Lkbz;
	CString	Lkam;
	CString	Perm;
	CString	Vrgc;
	CString	Lkst;
	CString	Disp;
	CString	Lkty;
	CString	Lkhc;
};

/////////////////////////////////////////////////////////////////////////////
// LeistungskatalogTableViewer

class LeistungskatalogTableViewer : public CViewer
{
// Constructions
public:
    LeistungskatalogTableViewer(CCSPtrArray<GHSDATA> *popData);
    ~LeistungskatalogTableViewer();

    void Attach(CCSTable *popAttachWnd);
    virtual void ChangeViewTo(const char *pcpViewName);
// Internal data processing routines
private:
	bool IsPassFilter(GHSDATA *prpLeistungskatalog);
	int CompareLeistungskatalog(LEISTUNGSKATALOGTABLE_LINEDATA *prpLeistungskatalog1, LEISTUNGSKATALOGTABLE_LINEDATA *prpLeistungskatalog2);
    void MakeLines();
	void MakeLine(GHSDATA *prpLeistungskatalog);
	bool FindLine(long lpUrno, int &rilLineno);
// Operations
public:
	void DeleteAll();
	void CreateLine(LEISTUNGSKATALOGTABLE_LINEDATA *prpLeistungskatalog);
	void DeleteLine(int ipLineno);
// Window refreshing routines
public:
	void UpdateDisplay();
	CString Format(LEISTUNGSKATALOGTABLE_LINEDATA *prpLine);
	void ProcessLeistungskatalogChange(GHSDATA *prpLeistungskatalog);
	void ProcessLeistungskatalogDelete(GHSDATA *prpLeistungskatalog);
	bool FindLeistungskatalog(char *prpLeistungskatalogKeya, char *prpLeistungskatalogKeyd, int& ilItem);
// Attributes used for filtering condition
private:
	bool bmIsFromSearch;
// Attributes
private:
    CCSTable *pomLeistungskatalogTable;
	CCSPtrArray<GHSDATA> *pomData;
// Methods which handle changes (from Data Distributor)
public:
    CCSPtrArray<LEISTUNGSKATALOGTABLE_LINEDATA> omLines;
///////
	CCSPtrArray<TABLE_HEADER_COLUMN> omHeaderDataArray;
//Print 
	void PrintTableView();
	bool PrintTableLine(LEISTUNGSKATALOGTABLE_LINEDATA *prpLine,bool bpLastLine);
	bool PrintTableHeader();
	CCSPrint *pomPrint;
	//CBitmap omBitmap;
	CString omTableName;

};

#endif //__LEISTUNGSKATALOGTABLEVIEWER_H__
