#if !defined(AFX_FIDSCOMMANDDLG_H__526E8E32_33ED_11D1_B39E_0000C016B067__INCLUDED_)
#define AFX_FIDSCOMMANDDLG_H__526E8E32_33ED_11D1_B39E_0000C016B067__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// FidsCommandDlg.h : header file
//
#include "CedaFIDData.h"
#include "CCSEdit.h"

/////////////////////////////////////////////////////////////////////////////
// FidsCommandDlg dialog

class FidsCommandDlg : public CDialog
{
// Construction
public:
	FidsCommandDlg(FIDDATA *popFID,CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(FidsCommandDlg)
	enum { IDD = IDD_FIDSCOMMANDDLG };
	CButton	m_OK;
	CString	m_Caption;
	CCSEdit	m_CDATD;
	CCSEdit	m_CDATT;
	CCSEdit	m_LSTUD;
	CCSEdit	m_LSTUT;
	CCSEdit	m_USEU;
	CCSEdit	m_USEC;
	CCSEdit	m_VAFRD;
	CCSEdit	m_VAFRT;
	CCSEdit	m_VATOD;
	CCSEdit	m_VATOT;
	CCSEdit	m_BEME;
	CCSEdit	m_CODE;
	CCSEdit	m_BEMD;
	CCSEdit	m_BET3;
	CCSEdit	m_BET4;
	CCSEdit	m_REMI;
	CCSEdit	m_REMT;
	CButton	m_BLKC;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(FidsCommandDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(FidsCommandDlg)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

private:

	FIDDATA *pomFID;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_FIDSCOMMANDDLG_H__526E8E32_33ED_11D1_B39E_0000C016B067__INCLUDED_)
