// CedaTeaData.cpp
 
#include "stdafx.h"
#include "CedaTeaData.h"
#include "resource.h"


void ProcessTeaCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

//--CEDADATA-----------------------------------------------------------------------------------------------

CedaTeaData::CedaTeaData() : CCSCedaData(&ogCommHandler)
{
	// Create an array of CEDARECINFO for TEADataStruct
	BEGIN_CEDARECINFO(TEADATA,TEADataRecInfo)
		FIELD_DATE		(Cdat,"CDAT")
		FIELD_CHAR_TRIM	(Fgmc,"FGMC")
		FIELD_CHAR_TRIM	(Fgmn,"FGMN")
		FIELD_DATE		(Lstu,"LSTU")
		FIELD_CHAR_TRIM	(Prfl,"PRFL")
		FIELD_CHAR_TRIM	(Rema,"REMA")
		FIELD_LONG		(Urno,"URNO")
		FIELD_CHAR_TRIM	(Usec,"USEC")
		FIELD_CHAR_TRIM	(Useu,"USEU")

	END_CEDARECINFO //(TEADataStruct)

	// FIELD_LONG, FIELD_DATE 
	// Copy the record structure
	for (int i=0; i< sizeof(TEADataRecInfo)/sizeof(TEADataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&TEADataRecInfo[i],sizeof(CEDARECINFO));
		omRecInfo.Add(prpCedaRecInfo);
	} // end for

	// initialize field names
	strcpy(pcmTableName,"TEA");
	strcpy(pcmListOfFields,"CDAT,FGMC,FGMN,LSTU,PRFL,REMA,URNO,USEC,USEU");
	pcmFieldList = pcmListOfFields; //because in CedaData pcmFieldList is just a pointer

	omData.RemoveAll();
}

//---------------------------------------------------------------------------------------------------------

void CedaTeaData::GetDataInfo(CStringArray &ropFields, CStringArray &ropDesription, CStringArray &ropType)
{
	ropFields.Add("CDAT");
	ropFields.Add("FGMC");
	ropFields.Add("FGMN");
	ropFields.Add("LSTU");
	ropFields.Add("PRFL");
	ropFields.Add("REMA");
	ropFields.Add("URNO");
	ropFields.Add("USEC");
	ropFields.Add("USEU");

	ropDesription.Add(LoadStg(IDS_STRING343));
	ropDesription.Add(LoadStg(IDS_STRING237));
	ropDesription.Add(LoadStg(IDS_STRING229));
	ropDesription.Add(LoadStg(IDS_STRING344));
	ropDesription.Add(LoadStg(IDS_STRING345));
	ropDesription.Add(LoadStg(IDS_STRING342));
	ropDesription.Add(LoadStg(IDS_STRING346));
	ropDesription.Add(LoadStg(IDS_STRING347));
	ropDesription.Add(LoadStg(IDS_STRING348));

	ropType.Add("Date");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("Date");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");

}

//--REGISTER----------------------------------------------------------------------------------------------

void CedaTeaData::Register(void)
{
	ogDdx.Register((void *)this,BC_TEA_CHANGE,	CString("TEADATA"), CString("Tea-changed"),	ProcessTeaCf);
	ogDdx.Register((void *)this,BC_TEA_NEW,		CString("TEADATA"), CString("Tea-new"),		ProcessTeaCf);
	ogDdx.Register((void *)this,BC_TEA_DELETE,	CString("TEADATA"), CString("Tea-deleted"),	ProcessTeaCf);
}

//--~CEDADATA----------------------------------------------------------------------------------------------

CedaTeaData::~CedaTeaData(void)
{
	omRecInfo.DeleteAll();
	ClearAll();
}

//--CLEAR-ALL----------------------------------------------------------------------------------------------

void CedaTeaData::ClearAll(bool bpWithRegistration)
{
    omUrnoMap.RemoveAll();
    omData.DeleteAll();
	if(bpWithRegistration)
	{
		ogDdx.UnRegister(this,NOTUSED);
	}
}

//--READ-ALL-----------------------------------------------------------------------------------------------

bool CedaTeaData::Read(char *pspWhere /*NULL*/)
{
    // Select data from the database
	bool ilRc = true;
    omUrnoMap.RemoveAll();
    omData.DeleteAll();
	if(pspWhere == NULL)
	{	
		ilRc = CedaAction("RT");
	}
	else
	{
		ilRc = CedaAction("RT", pspWhere);
	}
	if (ilRc != true)
	{
		return ilRc;
	}
	// Load data from CedaData into the dynamic array of record
	for (int ilLc = 0; ilRc == true; ilLc++)
	{
		TEADATA *prlTea = new TEADATA;
		if ((ilRc = GetFirstBufferRecord(prlTea)) == true)
		{
			omData.Add(prlTea);//Update omData
			omUrnoMap.SetAt((void *)prlTea->Urno,prlTea);
		}
		else
		{
			delete prlTea;
		}
	}
    return true;
}

//--INSERT-------------------------------------------------------------------------------------------------

bool CedaTeaData::Insert(TEADATA *prpTea)
{
	prpTea->IsChanged = DATA_NEW;
	if(Save(prpTea) == false) return false; //Update Database
	InsertInternal(prpTea);
    return true;
}

//--INSERT-INTERNAL--------------------------------------------------------------------------------------

bool CedaTeaData::InsertInternal(TEADATA *prpTea)
{
	ogDdx.DataChanged((void *)this, TEA_NEW,(void *)prpTea ); //Update Viewer
	omData.Add(prpTea);//Update omData
	omUrnoMap.SetAt((void *)prpTea->Urno,prpTea);
    return true;
}

//--DELETE-------------------------------------------------------------------------------------------------

bool CedaTeaData::Delete(long lpUrno)
{
	TEADATA *prlTea = GetTeaByUrno(lpUrno);
	if (prlTea != NULL)
	{
		prlTea->IsChanged = DATA_DELETED;
		if(Save(prlTea) == false) return false; //Update Database
		DeleteInternal(prlTea);
	}
    return true;
}

//--DELETE-INTERNAL----------------------------------------------------------------------------------------

bool CedaTeaData::DeleteInternal(TEADATA *prpTea)
{
	ogDdx.DataChanged((void *)this,TEA_DELETE,(void *)prpTea); //Update Viewer
	omUrnoMap.RemoveKey((void *)prpTea->Urno);
	int ilTeaCount = omData.GetSize();
	for (int ilLc = 0; ilLc < ilTeaCount; ilLc++)
	{
		if (omData[ilLc].Urno == prpTea->Urno)
		{
			omData.DeleteAt(ilLc);//Update omData
			break;
		}
	}
    return true;
}

//--UPDATE-------------------------------------------------------------------------------------------------

bool CedaTeaData::Update(TEADATA *prpTea)
{
	if (GetTeaByUrno(prpTea->Urno) != NULL)
	{
		if (prpTea->IsChanged == DATA_UNCHANGED)
		{
			prpTea->IsChanged = DATA_CHANGED;
		}
		if(Save(prpTea) == false) return false; //Update Database
		UpdateInternal(prpTea);
	}
    return true;
}

//--UPDATE-INTERNAL--------------------------------------------------------------------------------------------

bool CedaTeaData::UpdateInternal(TEADATA *prpTea)
{
	TEADATA *prlTea = GetTeaByUrno(prpTea->Urno);
	if (prlTea != NULL)
	{
		*prlTea = *prpTea; //Update omData
		ogDdx.DataChanged((void *)this,TEA_CHANGE,(void *)prlTea); //Update Viewer
	}
    return true;
}

//--GET-BY-URNO--------------------------------------------------------------------------------------------

TEADATA *CedaTeaData::GetTeaByUrno(long lpUrno)
{
	TEADATA  *prlTea;
	if (omUrnoMap.Lookup((void *)lpUrno,(void *& )prlTea) == TRUE)
	{
		return prlTea;
	}
	return NULL;
}

//--READSPECIALDATA-------------------------------------------------------------------------------------

bool CedaTeaData::ReadSpecialData(CCSPtrArray<TEADATA> *popTea,char *pspWhere,char *pspFieldList,bool ipSYS/*=true*/)
{
	bool ilRc = true;
	char pclFieldList[256] = " ";

	if(strlen(pspFieldList) > 0) 
	{
		strcpy(pclFieldList, pspFieldList);
	}
	if(ipSYS == true) 
	{
		if (CedaAction("SYS","TEA",pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	else
	{
		if (CedaAction("RT","TEA",pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	if(popTea != NULL)
	{
		for (int ilLc = 0; ilRc == true; ilLc++)
		{
			TEADATA *prpTea = new TEADATA;
			if ((ilRc = GetBufferRecord(ilLc,prpTea,CString(pclFieldList))) == true)
			{
				popTea->Add(prpTea);
			}
			else
			{
				delete prpTea;
			}
		}
		if(popTea->GetSize() == 0) return false;
	}
    return true;
}

//--SAVE---------------------------------------------------------------------------------------------------

bool CedaTeaData::Save(TEADATA *prpTea)
{
	bool ilRc = true;
	CString olListOfData;
	char pclSelection[124];
	char pclData[524];

	if (prpTea->IsChanged == DATA_UNCHANGED)
	{
		return true; // no change, nothing to do
	}
	switch(prpTea->IsChanged)
	{
	case DATA_NEW:
		MakeCedaData(&omRecInfo,olListOfData,prpTea);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("IRT","","",pclData);
		prpTea->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_CHANGED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpTea->Urno);
		MakeCedaData(&omRecInfo,olListOfData,prpTea);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("URT",pclSelection,"",pclData);
		prpTea->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_DELETED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpTea->Urno);
		ilRc = CedaAction("DRT",pclSelection);
		break;
	}
    return ilRc;
}

//--PROCESS-CF---------------------------------------------------------------------------------------------

void  ProcessTeaCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	ogTeaData.ProcessBc(ipDDXType,vpDataPointer,ropInstanceName);
}

//--PROCESS-BC---------------------------------------------------------------------------------------------

void  CedaTeaData::ProcessBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	struct BcStruct *prlTeaData;
	prlTeaData = (struct BcStruct *) vpDataPointer;
	TEADATA *prlTea;
	if(ipDDXType == BC_TEA_NEW)
	{
		prlTea = new TEADATA;
		GetRecordFromItemList(prlTea,prlTeaData->Fields,prlTeaData->Data);
		InsertInternal(prlTea);
	}
	if(ipDDXType == BC_TEA_CHANGE)
	{
		long llUrno = GetUrnoFromSelection(prlTeaData->Selection);
		prlTea = GetTeaByUrno(llUrno);
		if(prlTea != NULL)
		{
			GetRecordFromItemList(prlTea,prlTeaData->Fields,prlTeaData->Data);
			UpdateInternal(prlTea);
		}
	}
	if(ipDDXType == BC_TEA_DELETE)
	{
		long llUrno;
		CString olSelection = (CString)prlTeaData->Selection;
		if (olSelection.Find('\'') != -1)
		{
			llUrno = GetUrnoFromSelection(prlTeaData->Selection);
		}
		else
		{
			int ilFirst = olSelection.Find("=")+2;
			int ilLast  = olSelection.GetLength();
			llUrno = atol(olSelection.Mid(ilFirst,ilLast-ilFirst));
		}
		prlTea = GetTeaByUrno(llUrno);
		if (prlTea != NULL)
		{
			DeleteInternal(prlTea);
		}
	}
}

//---------------------------------------------------------------------------------------------------------
