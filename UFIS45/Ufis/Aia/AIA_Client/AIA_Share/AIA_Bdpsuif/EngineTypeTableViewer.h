#ifndef __EngineTypeTableViewer_H__
#define __EngineTypeTableViewer_H__

#include "stdafx.h"
#include "CedaEntData.h"
#include "CCSTable.h"
#include "CViewer.h"
#include "CCSPrint.h"


struct ENGINETYPETABLE_LINEDATA
{
	long 	 Urno; 	// Eindeutige Datensatz-Nr.
	CString  Entc; 	// Code
	CString  Enam; 	// Bezeichnung

};

/////////////////////////////////////////////////////////////////////////////
// EngineTypeTableViewer

	  
class EngineTypeTableViewer : public CViewer
{
// Constructions
public:
    EngineTypeTableViewer(CCSPtrArray<ENTDATA> *popData);
    ~EngineTypeTableViewer();

    void Attach(CCSTable *popAttachWnd);
    virtual void ChangeViewTo(const char *pcpViewName);
// Internal data processing routines
private:
	bool IsPassFilter(ENTDATA *prpEngineType);
	int CompareEngineType(ENGINETYPETABLE_LINEDATA *prpEngineType1, ENGINETYPETABLE_LINEDATA *prpEngineType2);
    void MakeLines();
	void MakeLine(ENTDATA *prpEngineType);
	bool FindLine(long lpUrno, int &rilLineno);
// Operations
public:
	void DeleteAll();
	void CreateLine(ENGINETYPETABLE_LINEDATA *prpEngineType);
	void DeleteLine(int ipLineno);
// Window refreshing routines
public:
	void UpdateDisplay();
	CString Format(ENGINETYPETABLE_LINEDATA *prpLine);
	void ProcessEngineTypeChange(ENTDATA *prpEngineType);
	void ProcessEngineTypeDelete(ENTDATA *prpEngineType);
	bool FindEngineType(char *prpEngineTypeKeya, char *prpEngineTypeKeyd, int& ilItem);
// Attributes used for filtering condition
private:
	bool bmIsFromSearch;
// Attributes
private:
    CCSTable *pomEngineTypeTable;
	CCSPtrArray<ENTDATA> *pomData;
// Methods which handle changes (from Data Distributor)
public:
    CCSPtrArray<ENGINETYPETABLE_LINEDATA> omLines;
///////
	CCSPtrArray<TABLE_HEADER_COLUMN> omHeaderDataArray;
//Print 
	void PrintTableView();
	bool PrintTableLine(ENGINETYPETABLE_LINEDATA *prpLine,bool bpLastLine);
	bool PrintTableHeader();
	CCSPrint *pomPrint;
	//CBitmap omBitmap;
	CString omTableName;

};

#endif //__EngineTypeTableViewer_H__
