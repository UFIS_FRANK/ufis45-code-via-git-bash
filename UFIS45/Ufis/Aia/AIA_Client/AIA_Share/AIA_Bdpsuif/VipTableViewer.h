#ifndef __VeryImpPersTableViewer_H__
#define __VeryImpPersTableViewer_H__

#include "stdafx.h"
#include "CedaVipData.h"
#include "CCSTable.h"
#include "CViewer.h"
#include "CCSPrint.h"


struct VERYIMPPERSTABLE_LINEDATA
{
	long 	 Urno; 	// Eindeutige Datensatz-Nr.
	long	 Flnu; 	// Code
	CString  Nogr; 	// Bezeichnung
	CString  Nopx; 	// Bemerkungen
	CString  Paxn; 	// Bemerkungen
	CString  Paxr; 	// Bemerkungen
};

/////////////////////////////////////////////////////////////////////////////
// VeryImpPersTableViewer

	  
class VeryImpPersTableViewer : public CViewer
{
// Constructions
public:
    VeryImpPersTableViewer(CCSPtrArray<VIPDATA> *popData);
    ~VeryImpPersTableViewer();

    void Attach(CCSTable *popAttachWnd);
    virtual void ChangeViewTo(const char *pcpViewName);
// Internal data processing routines
private:
	bool IsPassFilter(VIPDATA *prpVeryImpPers);
	int CompareVeryImpPers(VERYIMPPERSTABLE_LINEDATA *prpVeryImpPers1, VERYIMPPERSTABLE_LINEDATA *prpVeryImpPers2);
    void MakeLines();
	void MakeLine(VIPDATA *prpVeryImpPers);
	bool FindLine(long lpUrno, int &rilLineno);
// Operations
public:
	void DeleteAll();
	void CreateLine(VERYIMPPERSTABLE_LINEDATA *prpVeryImpPers);
	void DeleteLine(int ipLineno);
// Window refreshing routines
public:
	void UpdateDisplay();
	CString Format(VERYIMPPERSTABLE_LINEDATA *prpLine);
	void ProcessVeryImpPersChange(VIPDATA *prpVeryImpPers);
	void ProcessVeryImpPersDelete(VIPDATA *prpVeryImpPers);
	bool FindVeryImpPers(char *prpVeryImpPersKeya, char *prpVeryImpPersKeyd, int& ilItem);
// Attributes used for filtering condition
private:
	bool bmIsFromSearch;
// Attributes
private:
    CCSTable *pomVeryImpPersTable;
	CCSPtrArray<VIPDATA> *pomData;
// Methods which handle changes (from Data Distributor)
public:
    CCSPtrArray<VERYIMPPERSTABLE_LINEDATA> omLines;
///////
	CCSPtrArray<TABLE_HEADER_COLUMN> omHeaderDataArray;
//Print 
	void PrintTableView();
	bool PrintTableLine(VERYIMPPERSTABLE_LINEDATA *prpLine,bool bpLastLine);
	bool PrintTableHeader();
	CCSPrint *pomPrint;
	//CBitmap omBitmap;
	CString omTableName;

};

#endif //__VeryImpPersTableViewer_H__
