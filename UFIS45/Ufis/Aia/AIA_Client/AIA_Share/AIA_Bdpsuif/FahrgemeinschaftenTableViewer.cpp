// FahrgemeinschaftenTableViewer.cpp 
//

#include "stdafx.h"
#include "FahrgemeinschaftenTableViewer.h"
#include "CcsDdx.h"
#include "resource.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif


// Local function prototype
static void FahrgemeinschaftenTableCf(void *popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

//-----------------------------------------------------------------------------------------------
// FahrgemeinschaftenTableViewer
//

FahrgemeinschaftenTableViewer::FahrgemeinschaftenTableViewer(CCSPtrArray<TEADATA> *popData)
{
	pomData = popData;

	bmIsFromSearch = FALSE;
    pomFahrgemeinschaftenTable = NULL;
    ogDdx.Register(this, TEA_CHANGE, CString("FahrgemeinschaftenTableViewer"), CString("Fahrgemeinschaften Update"), FahrgemeinschaftenTableCf);
    ogDdx.Register(this, TEA_NEW,    CString("FahrgemeinschaftenTableViewer"), CString("Fahrgemeinschaften New"),    FahrgemeinschaftenTableCf);
    ogDdx.Register(this, TEA_DELETE, CString("FahrgemeinschaftenTableViewer"), CString("Fahrgemeinschaften Delete"), FahrgemeinschaftenTableCf);
}

//-----------------------------------------------------------------------------------------------

FahrgemeinschaftenTableViewer::~FahrgemeinschaftenTableViewer()
{
	ogDdx.UnRegister(this, NOTUSED);
    DeleteAll();
	omHeaderDataArray.DeleteAll();
}

//-----------------------------------------------------------------------------------------------

void FahrgemeinschaftenTableViewer::Attach(CCSTable *popTable)
{
    pomFahrgemeinschaftenTable = popTable;
}

//-----------------------------------------------------------------------------------------------

void FahrgemeinschaftenTableViewer::ChangeViewTo(const char *pcpViewName)
{
    DeleteAll();    
    MakeLines();
	UpdateDisplay();
}

//-----------------------------------------------------------------------------------------------

void FahrgemeinschaftenTableViewer::MakeLines()
{
	int ilFahrgemeinschaftenCount = pomData->GetSize();
	for (int ilLc = 0; ilLc < ilFahrgemeinschaftenCount; ilLc++)
	{
		TEADATA *prlFahrgemeinschaftenData = &pomData->GetAt(ilLc);
		MakeLine(prlFahrgemeinschaftenData);
	}
}

//-----------------------------------------------------------------------------------------------

void FahrgemeinschaftenTableViewer::MakeLine(TEADATA *prpFahrgemeinschaften)
{

    //if( !IsPassFilter(prpFahrgemeinschaften)) return;

    // Update viewer data for this shift record
    FAHRGEMEINSCHAFTENTABLE_LINEDATA rlFahrgemeinschaften;

	rlFahrgemeinschaften.Urno = prpFahrgemeinschaften->Urno;
	rlFahrgemeinschaften.Fgmc = prpFahrgemeinschaften->Fgmc;
	rlFahrgemeinschaften.Fgmn = prpFahrgemeinschaften->Fgmn;
	rlFahrgemeinschaften.Rema = prpFahrgemeinschaften->Rema;

	CreateLine(&rlFahrgemeinschaften);
}

//-----------------------------------------------------------------------------------------------

void FahrgemeinschaftenTableViewer::CreateLine(FAHRGEMEINSCHAFTENTABLE_LINEDATA *prpFahrgemeinschaften)
{
    int ilLineCount = omLines.GetSize();
    for (int ilLineno = ilLineCount; ilLineno > 0; ilLineno--)
        if (CompareFahrgemeinschaften(prpFahrgemeinschaften, &omLines[ilLineno-1]) >= 0)
	        break;							 // should be inserted after Lines[ilLineno-1]
	FAHRGEMEINSCHAFTENTABLE_LINEDATA rlFahrgemeinschaften;
	rlFahrgemeinschaften = *prpFahrgemeinschaften;
    omLines.NewAt(ilLineno, rlFahrgemeinschaften);
}

//-----------------------------------------------------------------------------------------------
// UpdateDisplay: Load data selected by filter conditions to the display by using "omTable"

void FahrgemeinschaftenTableViewer::UpdateDisplay()
{
	int ilLines = omLines.GetSize();
	const int ilColumns = 2;	//<===== Spaltenanzahl setzen (von 0 ab)
	omHeaderDataArray.DeleteAll();

	pomFahrgemeinschaftenTable->SetShowSelection(TRUE);
	pomFahrgemeinschaftenTable->ResetContent();

	TABLE_HEADER_COLUMN rlHeader;
	rlHeader.Alignment = COLALIGN_LEFT;
	rlHeader.Font = &ogCourier_Regular_10;

	int ilPos = 1;
	rlHeader.Length = 42; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING266),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 332; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING266),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 498; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING266),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	pomFahrgemeinschaftenTable->SetHeaderFields(omHeaderDataArray);

	pomFahrgemeinschaftenTable->SetDefaultSeparator();
	pomFahrgemeinschaftenTable->SetTableEditable(FALSE);

	TABLE_COLUMN rlColumnData;
	rlColumnData.Font = &ogCourier_Regular_10;
	rlColumnData.BkColor = WHITE;
	rlColumnData.Alignment = COLALIGN_LEFT;
	
    for (int ilLineNo = 0; ilLineNo < ilLines; ilLineNo++)
	{
		CCSPtrArray<TABLE_COLUMN> olColList;

		rlColumnData.Text = omLines[ilLineNo].Fgmc;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Fgmn;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Rema;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		pomFahrgemeinschaftenTable->AddTextLine(olColList,(void*)NULL);
		olColList.DeleteAll();
	}
	pomFahrgemeinschaftenTable->DisplayTable();
}

//-----------------------------------------------------------------------------------------------

CString FahrgemeinschaftenTableViewer::Format(FAHRGEMEINSCHAFTENTABLE_LINEDATA *prpLine)
{
    CString s;
	// ???
    return s;
}

//-----------------------------------------------------------------------------------------------

bool FahrgemeinschaftenTableViewer::FindFahrgemeinschaften(char *pcpFahrgemeinschaftenKeya, char *pcpFahrgemeinschaftenKeyd, int& ilItem)
{
	int ilCount = omLines.GetSize();
    for (ilItem = 0; ilItem < ilCount; ilItem++)
    {
/*		if ( (omLines[ilItem].Keya == pcpFahrgemeinschaftenKeya) &&
			 (omLines[ilItem].Keyd == pcpFahrgemeinschaftenKeyd)    )
			return TRUE;*/
	}
	return false;
}

//-----------------------------------------------------------------------------------------------

static void FahrgemeinschaftenTableCf(void *popInstance, int ipDDXType,
    void *vpDataPointer, CString &ropInstanceName)
{
    FahrgemeinschaftenTableViewer *polViewer = (FahrgemeinschaftenTableViewer *)popInstance;
    if (ipDDXType == TEA_CHANGE || ipDDXType == TEA_NEW) polViewer->ProcessFahrgemeinschaftenChange((TEADATA *)vpDataPointer);
    if (ipDDXType == TEA_DELETE) polViewer->ProcessFahrgemeinschaftenDelete((TEADATA *)vpDataPointer);
} 


//-----------------------------------------------------------------------------------------------

void FahrgemeinschaftenTableViewer::ProcessFahrgemeinschaftenChange(TEADATA *prpFahrgemeinschaften)
{
	int ilItem = 0;
	int ilColumnNo = 0;
	TABLE_COLUMN rlColumn;
	CCSPtrArray <TABLE_COLUMN> olLine;

	rlColumn.Lineno = ilItem;
	rlColumn.VerticalSeparator = SEPA_NONE;
	rlColumn.SeparatorType = SEPA_NONE;
	rlColumn.BkColor = WHITE;
	rlColumn.Font = &ogCourier_Regular_10;
	rlColumn.Alignment = COLALIGN_LEFT;

	rlColumn.Text = prpFahrgemeinschaften->Fgmc;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpFahrgemeinschaften->Fgmn;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpFahrgemeinschaften->Rema;
	olLine.NewAt(olLine.GetSize(), rlColumn);

	
	if (FindLine(prpFahrgemeinschaften->Urno, ilItem))
	{
        FAHRGEMEINSCHAFTENTABLE_LINEDATA *prlLine = &omLines[ilItem];

		prlLine->Urno = prpFahrgemeinschaften->Urno;
		prlLine->Fgmc = prpFahrgemeinschaften->Fgmc;
		prlLine->Fgmn = prpFahrgemeinschaften->Fgmn;
		prlLine->Rema = prpFahrgemeinschaften->Rema;


		pomFahrgemeinschaftenTable->ChangeTextLine(ilItem, &olLine, (void *)prlLine);
		pomFahrgemeinschaftenTable->DisplayTable();
	}
	else
	{
		MakeLine(prpFahrgemeinschaften);
		if (FindLine(prpFahrgemeinschaften->Urno, ilItem))
		{
	        FAHRGEMEINSCHAFTENTABLE_LINEDATA *prlLine = &omLines[ilItem];
			if(prlLine != NULL)
			{
				pomFahrgemeinschaftenTable->AddTextLine(olLine, (void *)prlLine);
				pomFahrgemeinschaftenTable->DisplayTable();
			}
		}
	}
	olLine.DeleteAll();
}

//-----------------------------------------------------------------------------------------------

void FahrgemeinschaftenTableViewer::ProcessFahrgemeinschaftenDelete(TEADATA *prpFahrgemeinschaften)
{
	int ilItem;
	if (FindLine(prpFahrgemeinschaften->Urno, ilItem))
	{
		DeleteLine(ilItem);
		pomFahrgemeinschaftenTable->DeleteTextLine(ilItem);
		pomFahrgemeinschaftenTable->DisplayTable();
	}
}

//-----------------------------------------------------------------------------------------------

bool FahrgemeinschaftenTableViewer::IsPassFilter(TEADATA *prpFahrgemeinschaften)
{

	return true;
}

//-----------------------------------------------------------------------------------------------

int FahrgemeinschaftenTableViewer::CompareFahrgemeinschaften(FAHRGEMEINSCHAFTENTABLE_LINEDATA *prpFahrgemeinschaften1, FAHRGEMEINSCHAFTENTABLE_LINEDATA *prpFahrgemeinschaften2)
{
/*	int ilCase=0;
	int	ilCompareResult;
	if(prpFahrgemeinschaften1->Tifd == prpFahrgemeinschaften2->Tifd)
	{
		ilCompareResult= 0;
	}
	else
	{
		if(prpFahrgemeinschaften1->Tifd > prpFahrgemeinschaften2->Tifd)
		{
			ilCompareResult = 1;
		}
		else
		{
			ilCompareResult = -1;
		}
	}
	return ilCompareResult;
*/  
	return 0;
}

//-----------------------------------------------------------------------------------------------

void FahrgemeinschaftenTableViewer::DeleteLine(int ipLineno)
{
	omLines.DeleteAt(ipLineno);
}

//-----------------------------------------------------------------------------------------------

bool FahrgemeinschaftenTableViewer::FindLine(long lpUrno, int &rilLineno)
{
    for (rilLineno = 0; rilLineno < omLines.GetSize(); rilLineno++)
	{
        if (omLines[rilLineno].Urno == lpUrno)
            return true;
	}
    return false;
}
//-----------------------------------------------------------------------------------------------

void FahrgemeinschaftenTableViewer::DeleteAll()
{
    while (omLines.GetSize() > 0)
    DeleteLine(0);
}

//-----------------------------------------------------------------------------------------------
// Printing Routines
//-----------------------------------------------------------------------------------------------

void FahrgemeinschaftenTableViewer::PrintTableView()
{
	CString olFooter1,olFooter2;
	omTableName = LoadStg(IDS_STRING171);
	int ilOrientation = PRINT_LANDSCAPE;//  PRINT_PORTRAET  PRINT_LANDSCAPE
	pomPrint = new CCSPrint((CWnd*)this,ilOrientation,45);// FirstLine(P=200,L=200),LeftOffset(P=200,L=50)
	//pomPrint->imMaxLines = 38;  // (P=57,L=38)
	//omBitmap.LoadBitmap(IDB_HAJLOGO);
	//pomPrint->SetBitmaps(&omBitmap,&omBitmap);
	if (pomPrint != NULL)
	{
		if (pomPrint->InitializePrinter(ilOrientation) == TRUE)
		{
			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
			pomPrint->imLineNo = pomPrint->imMaxLines + 1;
			DOCINFO	rlDocInfo;
			memset(&rlDocInfo, 0, sizeof(DOCINFO));
			rlDocInfo.cbSize = sizeof( DOCINFO );
			rlDocInfo.lpszDocName = omTableName;	
			pomPrint->omCdc.StartDoc( &rlDocInfo );
			pomPrint->imPageNo = 0;
			int ilLines = omLines.GetSize();
			olFooter1.Format("%d %s",ilLines,omTableName);
			for(int i = 0; i < ilLines; i++ ) 
			{
				if(pomPrint->imLineNo >= pomPrint->imMaxLines)
				{
					if(pomPrint->imPageNo > 0)
					{
						olFooter2.Format("%s: %d",LoadStg(IDS_STRING222),pomPrint->imPageNo);
						pomPrint->PrintUIFFooter(olFooter1,"",olFooter2);
						pomPrint->omCdc.EndPage();
					}
					PrintTableHeader();
				}
				if(pomPrint->imLineNo == (pomPrint->imMaxLines-1) || i == (ilLines-1))
				{
					PrintTableLine(&omLines[i],true);
				}
				else
				{
					PrintTableLine(&omLines[i],false);
				}
			}
			olFooter2.Format("%s: %d",LoadStg(IDS_STRING222),pomPrint->imPageNo);
			pomPrint->PrintUIFFooter(olFooter1,"",olFooter2);
			pomPrint->omCdc.EndPage();
			pomPrint->omCdc.EndDoc();
		}
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
	delete pomPrint;
	//omBitmap.DeleteObject();
	pomPrint = NULL;
	}
}

//-----------------------------------------------------------------------------------------------

bool FahrgemeinschaftenTableViewer::PrintTableHeader()
{
	pomPrint->omCdc.StartPage();
	pomPrint->imPageNo++;
	pomPrint->imLineNo = 0;

	pomPrint->PrintUIFHeader(omTableName,CString(CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M")),pomPrint->imFirstLine-10);

	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;

	rlElement.Alignment   = PRINT_LEFT;
	rlElement.FrameLeft   = PRINT_NOFRAME;
	rlElement.FrameRight  = PRINT_NOFRAME;
	rlElement.FrameTop    = PRINT_NOFRAME;
	rlElement.FrameBottom = PRINT_FRAMEMEDIUM;
	rlElement.pFont       = &pomPrint->ogCourierNew_Bold_8;

	int ilSize = omHeaderDataArray.GetSize();
	for(int i=0;i<ilSize;i++)
	{
		//if(i!=1&&i!=2)// <= setzen, wenn Spalten nicht angezeigt werden sollen 
		{
			rlElement.Length = (int)(omHeaderDataArray[i].Length*dgCCSPrintFactor);
			if(rlElement.Length < igCCSPrintMinLength)rlElement.Length+=igCCSPrintMoreLength; 
			rlElement.Text   = omHeaderDataArray[i].Text;
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
		}
	}
	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();


	return true;
}

//-----------------------------------------------------------------------------------------------

bool FahrgemeinschaftenTableViewer::PrintTableLine(FAHRGEMEINSCHAFTENTABLE_LINEDATA *prpLine,bool bpLastLine)
{
	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;
	rlElement.pFont      = &pomPrint->ogCourierNew_Regular_8;
	int ilSize = omHeaderDataArray.GetSize();
	for(int i=0;i<ilSize;i++)
	{
		//if(i!=1&&i!=1)// <= setzen, wenn Spalten nicht angezeigt werden sollen 
		{
			rlElement.Length = (int)(omHeaderDataArray[i].Length*dgCCSPrintFactor);
			if(rlElement.Length < igCCSPrintMinLength)rlElement.Length+=igCCSPrintMoreLength; 
			rlElement.Alignment  = PRINT_LEFT;
			rlElement.FrameTop   = PRINT_FRAMETHIN;
			if(bpLastLine == true)
			{
				rlElement.FrameBottom = PRINT_FRAMETHIN;
			}
			else
			{
				rlElement.FrameBottom = PRINT_NOFRAME;
			}
			rlElement.FrameLeft  = PRINT_NOFRAME;
			rlElement.FrameRight = PRINT_NOFRAME;

			switch(i)
			{
			case 0:
				rlElement.FrameLeft  = PRINT_FRAMETHIN;
				rlElement.Text		 = prpLine->Fgmc;
				break;
			case 1:
				rlElement.Text		 = prpLine->Fgmn;
				break;
			case 2:
				rlElement.FrameRight = PRINT_FRAMETHIN;
				rlElement.Text		 = prpLine->Rema;
				break;
			}
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
		}
	}
	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();
	return true;
}

//-----------------------------------------------------------------------------------------------
