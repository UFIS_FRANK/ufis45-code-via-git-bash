#if !defined(AFX_TELEXADRESSDLG_H__526E8E31_33ED_11D1_B39E_0000C016B067__INCLUDED_)
#define AFX_TELEXADRESSDLG_H__526E8E31_33ED_11D1_B39E_0000C016B067__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// TelexadressDlg.h : header file
//
#include "CedaMVTData.h"
#include "CCSEdit.h"

/////////////////////////////////////////////////////////////////////////////
// TelexadressDlg dialog

class TelexadressDlg : public CDialog
{
// Construction
public:
	TelexadressDlg(MVTDATA *popMVT,CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(TelexadressDlg)
	enum { IDD = IDD_TELEXADRESSDLG };
	CButton	m_OK;
	CButton	m_TEMA;
	CButton	m_SMVT;
	CString	m_Caption;
	CCSEdit	m_CDATD;
	CCSEdit	m_CDATT;
	CCSEdit	m_LSTUD;
	CCSEdit	m_LSTUT;
	CCSEdit	m_USEU;
	CCSEdit	m_USEC;
	CCSEdit	m_VAFRD;
	CCSEdit	m_VAFRT;
	CCSEdit	m_VATOD;
	CCSEdit	m_VATOT;
	CCSEdit	m_ALC3;
	CCSEdit	m_BEME;
	CCSEdit	m_TEAD;
	CCSEdit	m_APC3;
	CCSEdit	m_PAYE;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(TelexadressDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(TelexadressDlg)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

private:

	MVTDATA *pomMVT;

};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_TELEXADRESSDLG_H__526E8E31_33ED_11D1_B39E_0000C016B067__INCLUDED_)
