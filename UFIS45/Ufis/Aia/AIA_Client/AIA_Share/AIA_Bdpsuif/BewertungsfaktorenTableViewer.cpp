// BewertungsfaktorenTableViewer.cpp 
//

#include "stdafx.h"
#include "BewertungsfaktorenTableViewer.h"
#include "CcsDdx.h"
#include "resource.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif


// Local function prototype
static void BewertungsfaktorenTableCf(void *popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

//-----------------------------------------------------------------------------------------------
// BewertungsfaktorenTableViewer
//

BewertungsfaktorenTableViewer::BewertungsfaktorenTableViewer(CCSPtrArray<ASFDATA> *popData)
{
	pomData = popData;

	bmIsFromSearch = FALSE;
    pomBewertungsfaktorenTable = NULL;
    ogDdx.Register(this, ASF_CHANGE, CString("BEWERTUNGSFAKTORENTABLEVIEWER"), CString("Bewertungsfaktoren Update"), BewertungsfaktorenTableCf);
    ogDdx.Register(this, ASF_NEW,    CString("BEWERTUNGSFAKTORENTABLEVIEWER"), CString("Bewertungsfaktoren New"),    BewertungsfaktorenTableCf);
    ogDdx.Register(this, ASF_DELETE, CString("BEWERTUNGSFAKTORENTABLEVIEWER"), CString("Bewertungsfaktoren Delete"), BewertungsfaktorenTableCf);
}

//-----------------------------------------------------------------------------------------------

BewertungsfaktorenTableViewer::~BewertungsfaktorenTableViewer()
{
	ogDdx.UnRegister(this, NOTUSED);
    DeleteAll();
	omHeaderDataArray.DeleteAll();
}

//-----------------------------------------------------------------------------------------------

void BewertungsfaktorenTableViewer::Attach(CCSTable *popTable)
{
    pomBewertungsfaktorenTable = popTable;
}

//-----------------------------------------------------------------------------------------------

void BewertungsfaktorenTableViewer::ChangeViewTo(const char *pcpViewName)
{
    DeleteAll();    
    MakeLines();
	UpdateDisplay();
}

//-----------------------------------------------------------------------------------------------

void BewertungsfaktorenTableViewer::MakeLines()
{
	int ilBewertungsfaktorenCount = pomData->GetSize();
	for (int ilLc = 0; ilLc < ilBewertungsfaktorenCount; ilLc++)
	{
		ASFDATA *prlBewertungsfaktorenData = &pomData->GetAt(ilLc);
		MakeLine(prlBewertungsfaktorenData);
	}
}

//-----------------------------------------------------------------------------------------------

void BewertungsfaktorenTableViewer::MakeLine(ASFDATA *prpBewertungsfaktoren)
{

    //if( !IsPassFilter(prpBewertungsfaktoren)) return;

    // Update viewer data for this shift record
    BEWERTUNGSFAKTORENTABLE_LINEDATA rlBewertungsfaktoren;

	rlBewertungsfaktoren.Urno = prpBewertungsfaktoren->Urno;
	rlBewertungsfaktoren.Bewc = prpBewertungsfaktoren->Bewc;
	rlBewertungsfaktoren.Bewf = prpBewertungsfaktoren->Bewf;
	rlBewertungsfaktoren.Bewn = prpBewertungsfaktoren->Bewn;
	rlBewertungsfaktoren.Rema = prpBewertungsfaktoren->Rema;

	CreateLine(&rlBewertungsfaktoren);
}

//-----------------------------------------------------------------------------------------------

void BewertungsfaktorenTableViewer::CreateLine(BEWERTUNGSFAKTORENTABLE_LINEDATA *prpBewertungsfaktoren)
{
    int ilLineCount = omLines.GetSize();
    for (int ilLineno = ilLineCount; ilLineno > 0; ilLineno--)
        if (CompareBewertungsfaktoren(prpBewertungsfaktoren, &omLines[ilLineno-1]) >= 0)
	        break;							 // should be inserted after Lines[ilLineno-1]
	BEWERTUNGSFAKTORENTABLE_LINEDATA rlBewertungsfaktoren;
	rlBewertungsfaktoren = *prpBewertungsfaktoren;
    omLines.NewAt(ilLineno, rlBewertungsfaktoren);
}

//-----------------------------------------------------------------------------------------------
// UpdateDisplay: Load data selected by filter conditions to the display by using "omTable"

void BewertungsfaktorenTableViewer::UpdateDisplay()
{
	int ilLines = omLines.GetSize();
	const int ilColumns = 2;	//<===== Spaltenanzahl setzen (von 0 ab)
	omHeaderDataArray.DeleteAll();

	pomBewertungsfaktorenTable->SetShowSelection(TRUE);
	pomBewertungsfaktorenTable->ResetContent();

	TABLE_HEADER_COLUMN rlHeader;
	rlHeader.Alignment = COLALIGN_LEFT;
	rlHeader.Font = &ogCourier_Regular_10;

	int ilPos = 1;
	rlHeader.Length = 42; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING257),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 332; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING257),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 83; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING257),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 498; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING257),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);

	pomBewertungsfaktorenTable->SetHeaderFields(omHeaderDataArray);

	pomBewertungsfaktorenTable->SetDefaultSeparator();
	pomBewertungsfaktorenTable->SetTableEditable(FALSE);

	TABLE_COLUMN rlColumnData;
	rlColumnData.Font = &ogCourier_Regular_10;
	rlColumnData.BkColor = WHITE;
	rlColumnData.Alignment = COLALIGN_LEFT;
	
    for (int ilLineNo = 0; ilLineNo < ilLines; ilLineNo++)
	{
		CCSPtrArray<TABLE_COLUMN> olColList;

		rlColumnData.Text = omLines[ilLineNo].Bewc;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Bewn;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Bewf;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Rema;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		pomBewertungsfaktorenTable->AddTextLine(olColList,(void*)NULL);
		olColList.DeleteAll();
	}
	pomBewertungsfaktorenTable->DisplayTable();
}

//-----------------------------------------------------------------------------------------------

CString BewertungsfaktorenTableViewer::Format(BEWERTUNGSFAKTORENTABLE_LINEDATA *prpLine)
{
    CString s;
	// ???
    return s;
}

//-----------------------------------------------------------------------------------------------

bool BewertungsfaktorenTableViewer::FindBewertungsfaktoren(char *pcpBewertungsfaktorenKeya, char *pcpBewertungsfaktorenKeyd, int& ilItem)
{
	int ilCount = omLines.GetSize();
    for (ilItem = 0; ilItem < ilCount; ilItem++)
    {
/*		if ( (omLines[ilItem].Keya == pcpBewertungsfaktorenKeya) &&
			 (omLines[ilItem].Keyd == pcpBewertungsfaktorenKeyd)    )
			return TRUE;*/
	}
	return false;
}

//-----------------------------------------------------------------------------------------------

static void BewertungsfaktorenTableCf(void *popInstance, int ipDDXType,
    void *vpDataPointer, CString &ropInstanceName)
{
    BewertungsfaktorenTableViewer *polViewer = (BewertungsfaktorenTableViewer *)popInstance;
    if (ipDDXType == ASF_CHANGE || ipDDXType == ASF_NEW) polViewer->ProcessBewertungsfaktorenChange((ASFDATA *)vpDataPointer);
    if (ipDDXType == ASF_DELETE) polViewer->ProcessBewertungsfaktorenDelete((ASFDATA *)vpDataPointer);
} 


//-----------------------------------------------------------------------------------------------

void BewertungsfaktorenTableViewer::ProcessBewertungsfaktorenChange(ASFDATA *prpBewertungsfaktoren)
{
	int ilItem = 0;
	int ilColumnNo = 0;
	TABLE_COLUMN rlColumn;
	CCSPtrArray <TABLE_COLUMN> olLine;

	rlColumn.Lineno = ilItem;
	rlColumn.VerticalSeparator = SEPA_NONE;
	rlColumn.SeparatorType = SEPA_NONE;
	rlColumn.BkColor = WHITE;
	rlColumn.Font = &ogCourier_Regular_10;
	rlColumn.Alignment = COLALIGN_LEFT;


	rlColumn.Text = prpBewertungsfaktoren->Bewc;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpBewertungsfaktoren->Bewn;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpBewertungsfaktoren->Bewf;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpBewertungsfaktoren->Rema;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	
	if (FindLine(prpBewertungsfaktoren->Urno, ilItem))
	{
        BEWERTUNGSFAKTORENTABLE_LINEDATA *prlLine = &omLines[ilItem];


		prlLine->Urno = prpBewertungsfaktoren->Urno;
		prlLine->Bewc = prpBewertungsfaktoren->Bewc;
		prlLine->Bewf = prpBewertungsfaktoren->Bewf;
		prlLine->Bewn = prpBewertungsfaktoren->Bewn;
		prlLine->Rema = prpBewertungsfaktoren->Rema;

		pomBewertungsfaktorenTable->ChangeTextLine(ilItem, &olLine, (void *)prlLine);
		pomBewertungsfaktorenTable->DisplayTable();
	}
	else
	{
		MakeLine(prpBewertungsfaktoren);
		if (FindLine(prpBewertungsfaktoren->Urno, ilItem))
		{
	        BEWERTUNGSFAKTORENTABLE_LINEDATA *prlLine = &omLines[ilItem];
			if(prlLine != NULL)
			{
				pomBewertungsfaktorenTable->AddTextLine(olLine, (void *)prlLine);
				pomBewertungsfaktorenTable->DisplayTable();
			}
		}
	}
	olLine.DeleteAll();
}

//-----------------------------------------------------------------------------------------------

void BewertungsfaktorenTableViewer::ProcessBewertungsfaktorenDelete(ASFDATA *prpBewertungsfaktoren)
{
	int ilItem;
	if (FindLine(prpBewertungsfaktoren->Urno, ilItem))
	{
		DeleteLine(ilItem);
		pomBewertungsfaktorenTable->DeleteTextLine(ilItem);
		pomBewertungsfaktorenTable->DisplayTable();
	}
}

//-----------------------------------------------------------------------------------------------

bool BewertungsfaktorenTableViewer::IsPassFilter(ASFDATA *prpBewertungsfaktoren)
{

	return true;
}

//-----------------------------------------------------------------------------------------------

int BewertungsfaktorenTableViewer::CompareBewertungsfaktoren(BEWERTUNGSFAKTORENTABLE_LINEDATA *prpBewertungsfaktoren1, BEWERTUNGSFAKTORENTABLE_LINEDATA *prpBewertungsfaktoren2)
{
/*	int ilCase=0;
	int	ilCompareResult;
	if(prpBewertungsfaktoren1->Tifd == prpBewertungsfaktoren2->Tifd)
	{
		ilCompareResult= 0;
	}
	else
	{
		if(prpBewertungsfaktoren1->Tifd > prpBewertungsfaktoren2->Tifd)
		{
			ilCompareResult = 1;
		}
		else
		{
			ilCompareResult = -1;
		}
	}
	return ilCompareResult;
*/  
	return 0;
}

//-----------------------------------------------------------------------------------------------

void BewertungsfaktorenTableViewer::DeleteLine(int ipLineno)
{
	omLines.DeleteAt(ipLineno);
}

//-----------------------------------------------------------------------------------------------

bool BewertungsfaktorenTableViewer::FindLine(long lpUrno, int &rilLineno)
{
    for (rilLineno = 0; rilLineno < omLines.GetSize(); rilLineno++)
	{
        if (omLines[rilLineno].Urno == lpUrno)
            return true;
	}
    return false;
}
//-----------------------------------------------------------------------------------------------

void BewertungsfaktorenTableViewer::DeleteAll()
{
    while (omLines.GetSize() > 0)
    DeleteLine(0);
}

//-----------------------------------------------------------------------------------------------
// Printing Routines
//-----------------------------------------------------------------------------------------------

void BewertungsfaktorenTableViewer::PrintTableView()
{
	CString olFooter1,olFooter2;
	omTableName = LoadStg(IDS_STRING165);
	int ilOrientation = PRINT_LANDSCAPE;//  PRINT_PORTRAET  PRINT_LANDSCAPE
	pomPrint = new CCSPrint((CWnd*)this,ilOrientation,45);// FirstLine(P=200,L=200),LeftOffset(P=200,L=50)
	//pomPrint->imMaxLines = 38;  // (P=57,L=38)
	//omBitmap.LoadBitmap(IDB_HAJLOGO);
	//pomPrint->SetBitmaps(&omBitmap,&omBitmap);
	if (pomPrint != NULL)
	{
		if (pomPrint->InitializePrinter(ilOrientation) == TRUE)
		{
			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
			pomPrint->imLineNo = pomPrint->imMaxLines + 1;
			DOCINFO	rlDocInfo;
			memset(&rlDocInfo, 0, sizeof(DOCINFO));
			rlDocInfo.cbSize = sizeof( DOCINFO );
			rlDocInfo.lpszDocName = omTableName;	
			pomPrint->omCdc.StartDoc( &rlDocInfo );
			pomPrint->imPageNo = 0;
			int ilLines = omLines.GetSize();
			olFooter1.Format("%d %s",ilLines,omTableName);
			for(int i = 0; i < ilLines; i++ ) 
			{
				if(pomPrint->imLineNo >= pomPrint->imMaxLines)
				{
					if(pomPrint->imPageNo > 0)
					{
						olFooter2.Format("%s: %d",LoadStg(IDS_STRING222),pomPrint->imPageNo);
						pomPrint->PrintUIFFooter(olFooter1,"",olFooter2);
						pomPrint->omCdc.EndPage();
					}
					PrintTableHeader();
				}
				if(pomPrint->imLineNo == (pomPrint->imMaxLines-1) || i == (ilLines-1))
				{
					PrintTableLine(&omLines[i],true);
				}
				else
				{
					PrintTableLine(&omLines[i],false);
				}
			}
			olFooter2.Format("%s: %d",LoadStg(IDS_STRING222),pomPrint->imPageNo);
			pomPrint->PrintUIFFooter(olFooter1,"",olFooter2);
			pomPrint->omCdc.EndPage();
			pomPrint->omCdc.EndDoc();
		}
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
	delete pomPrint;
	//omBitmap.DeleteObject();
	pomPrint = NULL;
	}
}

//-----------------------------------------------------------------------------------------------

bool BewertungsfaktorenTableViewer::PrintTableHeader()
{
	pomPrint->omCdc.StartPage();
	pomPrint->imPageNo++;
	pomPrint->imLineNo = 0;

	pomPrint->PrintUIFHeader(omTableName,CString(CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M")),pomPrint->imFirstLine-10);

	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;

	rlElement.Alignment   = PRINT_LEFT;
	rlElement.FrameLeft   = PRINT_NOFRAME;
	rlElement.FrameRight  = PRINT_NOFRAME;
	rlElement.FrameTop    = PRINT_NOFRAME;
	rlElement.FrameBottom = PRINT_FRAMEMEDIUM;
	rlElement.pFont       = &pomPrint->ogCourierNew_Bold_8;

	int ilSize = omHeaderDataArray.GetSize();
	for(int i=0;i<ilSize;i++)
	{
		//if(i!=1&&i!=2)// <= setzen, wenn Spalten nicht angezeigt werden sollen 
		{
			rlElement.Length = (int)(omHeaderDataArray[i].Length*dgCCSPrintFactor);
			if(rlElement.Length < igCCSPrintMinLength)rlElement.Length+=igCCSPrintMoreLength; 
			rlElement.Text   = omHeaderDataArray[i].Text;
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
		}
	}
	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();


	return true;
}

//-----------------------------------------------------------------------------------------------

bool BewertungsfaktorenTableViewer::PrintTableLine(BEWERTUNGSFAKTORENTABLE_LINEDATA *prpLine,bool bpLastLine)
{
	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;
	rlElement.pFont      = &pomPrint->ogCourierNew_Regular_8;
	int ilSize = omHeaderDataArray.GetSize();
	for(int i=0;i<ilSize;i++)
	{
		//if(i!=1&&i!=1)// <= setzen, wenn Spalten nicht angezeigt werden sollen 
		{
			rlElement.Length = (int)(omHeaderDataArray[i].Length*dgCCSPrintFactor);
			if(rlElement.Length < igCCSPrintMinLength)rlElement.Length+=igCCSPrintMoreLength; 
			rlElement.Alignment  = PRINT_LEFT;
			rlElement.FrameTop   = PRINT_FRAMETHIN;
			if(bpLastLine == true)
			{
				rlElement.FrameBottom = PRINT_FRAMETHIN;
			}
			else
			{
				rlElement.FrameBottom = PRINT_NOFRAME;
			}
			rlElement.FrameLeft  = PRINT_NOFRAME;
			rlElement.FrameRight = PRINT_NOFRAME;

			switch(i)
			{
			case 0:
				rlElement.FrameLeft  = PRINT_FRAMETHIN;
				rlElement.Text		 = prpLine->Bewc;
				break;
			case 1:
				rlElement.Text		 = prpLine->Bewn;
				break;
			case 2:
				rlElement.Text		 = prpLine->Bewf;
				break;
			case 3:
				rlElement.FrameRight = PRINT_FRAMETHIN;
				rlElement.Text		 = prpLine->Rema;
				break;
			}
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
		}
	}
	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();
	return true;
}

//-----------------------------------------------------------------------------------------------
