// CedaWayData.h

#ifndef __CEDAPWAYDATA__
#define __CEDAPWAYDATA__
 
#include "stdafx.h"
#include "basicdata.h"

//---------------------------------------------------------------------------------------------------------

struct WAYDATA 
{
	CTime 	 Cdat;		// Erstellungsdatum
	CTime 	 Lstu;		// Datum letzte �nderung
	char 	 Pobe[12]; 	// Ausgangsposition
	char 	 Poen[12]; 	// Zielposition
	char 	 Prfl[3]; 	// Protokollierungskennzeichen
	char 	 Ttgo[6]; 	// Wegezeit
	long 	 Urno;		// Eindeutige Datensatz-Nr.
	char 	 Usec[34]; 	// Anwender (Ersteller)
	char 	 Useu[34]; 	// Anwender (letzte �nderung)
	char 	 Wtgo[7]; 	// Wegstrecke
	char 	 Wtyp[3]; 	// Wegetyp (z.B. Schlepp, Gang zu Fu�, Ger�t)
	char 	 Tybe[3]; 	// von (Guppe/Pos/Start- u. Landebahn)
	char 	 Tyen[3]; 	// nach (Guppe/Pos/Start- u. Landebahn)
	char 	 Home[4]; 	// nach (Guppe/Pos/Start- u. Landebahn)

	//DataCreated by this class
	int      IsChanged;

	WAYDATA(void)
	{
		memset(this,'\0',sizeof(*this));
		Cdat=-1;
		Lstu=-1;
	}

}; // end WAYDATA

//---------------------------------------------------------------------------------------------------------
// Class declaratino

class CedaWayData: public CCSCedaData
{
// Attributes
public:
    CMapPtrToPtr omUrnoMap;

    CCSPtrArray<WAYDATA> omData;

	char pcmListOfFields[2048];

// OWayations
public:
    CedaWayData();
	~CedaWayData();
	virtual void GetDataInfo(CStringArray &ropFields, CStringArray &ropDesription, CStringArray &ropType);
	void Register(void);
	void ClearAll(bool bpWithRegistration = true);
    bool Read(char *pspWhere = NULL);
	bool Insert(WAYDATA *prpWay);
	bool InsertInternal(WAYDATA *prpWay);
	bool Update(WAYDATA *prpWay);
	bool UpdateInternal(WAYDATA *prpWay);
	bool Delete(long lpUrno);
	bool DeleteInternal(WAYDATA *prpWay);
	bool ReadSpecialData(CCSPtrArray<WAYDATA> *popWay,char *pspWhere,char *pspFieldList,bool ipSYS=true);
	bool Save(WAYDATA *prpWay);
	void ProcessBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);
	WAYDATA  *GetWayByUrno(long lpUrno);


	// Private methods
private:
    void PrepareWayData(WAYDATA *prpWayData);

};

//---------------------------------------------------------------------------------------------------------


#endif //__CEDAPWAYDATA__
