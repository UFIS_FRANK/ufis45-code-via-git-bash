// WegezeitenDlg.cpp : implementation file
//

#include "stdafx.h"
#include "bdpsuif.h"
#include "WegezeitenDlg.h"
#include "PrivList.h"
#include "CedaPstData.h"
#include "CedaRwyData.h"
#include "CedaTwyData.h"
#include "CedaGrnData.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// WegezeitenDlg dialog


WegezeitenDlg::WegezeitenDlg(WAYDATA *popWay, CWnd* pParent /*=NULL*/)
	: CDialog(WegezeitenDlg::IDD, pParent)
{
	pomWay = popWay;
	//{{AFX_DATA_INIT(WegezeitenDlg)
	//}}AFX_DATA_INIT
}


void WegezeitenDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(WegezeitenDlg)
	DDX_Control(pDX, IDC_TYEN_P, m_TYENP);
	DDX_Control(pDX, IDC_TYEN_R, m_TYENR);
	DDX_Control(pDX, IDC_TYEN_G, m_TYENG);
	DDX_Control(pDX, IDC_TYBE_P, m_TYBEP);
	DDX_Control(pDX, IDC_TYBE_R, m_TYBER);
	DDX_Control(pDX, IDC_TYBE_G, m_TYBEG);
	DDX_Control(pDX, IDC_WTYP,   m_WTYP);
	DDX_Control(pDX, IDC_POBE_G, m_POBEG);
	DDX_Control(pDX, IDC_POEN_G, m_POENG);
	DDX_Control(pDX, IDOK,		 m_OK);
	DDX_Control(pDX, IDC_LSTU_D, m_LSTUD);
	DDX_Control(pDX, IDC_GRUP,	 m_GRUP);
	DDX_Control(pDX, IDC_LSTU_T, m_LSTUT);
	DDX_Control(pDX, IDC_CDAT_T, m_CDATT);
	DDX_Control(pDX, IDC_CDAT_D, m_CDATD);
	DDX_Control(pDX, IDC_POBE,	 m_POBE);
	DDX_Control(pDX, IDC_POEN,	 m_POEN);
	DDX_Control(pDX, IDC_TTGO,	 m_TTGO);
	DDX_Control(pDX, IDC_USEC,	 m_USEC);
	DDX_Control(pDX, IDC_USEU,	 m_USEU);
	DDX_Control(pDX, IDC_WTGO_S, m_WTGOS);
	DDX_Control(pDX, IDC_WTGO_T, m_WTGOT);
	DDX_Control(pDX, IDC_HOME, m_HOME);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(WegezeitenDlg, CDialog)
	//{{AFX_MSG_MAP(WegezeitenDlg)
	ON_BN_CLICKED(IDC_TYEN_P, OnTyen)
	ON_BN_CLICKED(IDC_TYEN_R, OnTyen)
	ON_BN_CLICKED(IDC_TYEN_G, OnTyen)
	ON_BN_CLICKED(IDC_TYBE_P, OnTybe)
	ON_BN_CLICKED(IDC_TYBE_R, OnTybe)
	ON_BN_CLICKED(IDC_TYBE_G, OnTybe)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// WegezeitenDlg message handlers
//----------------------------------------------------------------------------------------

BOOL WegezeitenDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	m_Caption = LoadStg(IDS_STRING196) + " <" + m_Caption + ">";
	SetWindowText(m_Caption);
	
	char clStat = ogPrivList.GetStat("WEGEZEITENDLG.m_OK");
	SetWndStatAll(clStat,m_OK);

	//------------------------------------
	m_CDATD.SetBKColor(SILVER);
	m_CDATD.SetInitText(pomWay->Cdat.Format("%d.%m.%Y"));
	m_CDATD.SetSecState(ogPrivList.GetStat("WEGEZEITENDLG.m_CDAT"));
	// - - - - - - - - - - - - - - - - - -
	m_CDATT.SetBKColor(SILVER);
	m_CDATT.SetInitText(pomWay->Cdat.Format("%H:%M"));
	m_CDATT.SetSecState(ogPrivList.GetStat("WEGEZEITENDLG.m_CDAT"));
	//------------------------------------
	m_LSTUD.SetBKColor(SILVER);
	m_LSTUD.SetInitText(pomWay->Lstu.Format("%d.%m.%Y"));
	m_LSTUD.SetSecState(ogPrivList.GetStat("WEGEZEITENDLG.m_LSTU"));
	// - - - - - - - - - - - - - - - - - -
	m_LSTUT.SetBKColor(SILVER);
	m_LSTUT.SetInitText(pomWay->Lstu.Format("%H:%M"));
	m_LSTUT.SetSecState(ogPrivList.GetStat("WEGEZEITENDLG.m_LSTU"));
	//------------------------------------
	m_USEC.SetBKColor(SILVER);
	m_USEC.SetInitText(pomWay->Usec);
	m_USEC.SetSecState(ogPrivList.GetStat("WEGEZEITENDLG.m_USEC"));
	//------------------------------------
	m_USEU.SetBKColor(SILVER);
	m_USEU.SetInitText(pomWay->Useu);
	m_USEU.SetSecState(ogPrivList.GetStat("WEGEZEITENDLG.m_USEU"));
	//------------------------------------
	m_GRUP.SetBKColor(SILVER);
	//m_GRUP.SetInitText(pomGAT->);
	m_GRUP.SetSecState(ogPrivList.GetStat("WEGEZEITENDLG.m_GRUP"));
	//------------------------------------//WHITE,YELLOW,SILVER,RED,LYELLOW
	m_HOME.SetTypeToString("X(3)",3,1);
	m_HOME.SetTextErrColor(RED);
	m_HOME.SetInitText(pomWay->Home);
	m_HOME.SetSecState(ogPrivList.GetStat("WEGEZEITENDLG.m_HOME"));
	//------------------------------------
	char clStat1;
	char clStat2 = '0';
	m_POBEG.SetBKColor(YELLOW);
	m_POENG.SetBKColor(YELLOW);
	clStat1 = ogPrivList.GetStat("WEGEZEITENDLG.m_POBE");
	SetWndStatPrio_1(clStat1,clStat2,m_POBEG);
	clStat1 = ogPrivList.GetStat("WEGEZEITENDLG.m_POEN");
	SetWndStatPrio_1(clStat1,clStat2,m_POENG);
	m_POBE.SetSecState(ogPrivList.GetStat("WEGEZEITENDLG.m_POBE"));
	m_POEN.SetSecState(ogPrivList.GetStat("WEGEZEITENDLG.m_POEN"));
	m_POBE.SetTextErrColor(RED);
	m_POEN.SetTextErrColor(RED);
	//------------------------------------
	m_POBEG.SetFont(&ogCourier_Regular_9);
	m_POENG.SetFont(&ogCourier_Regular_9);

	GetGruppnames();
	//------------------------------------
	m_TYBEP.SetCheck(0);
	m_TYBER.SetCheck(0);
	m_TYBEG.SetCheck(0);

	if (pomWay->Tybe[0] == 'P')
	{
		cmOldTybe = 'P';
		m_TYBEP.SetCheck(1);
		m_POBE.SetFormat("x|#x|#x|#x|#x|#");
		m_POBE.SetTextLimit(1,5);
		m_POBE.SetBKColor(YELLOW);  
		m_POBE.SetInitText(pomWay->Pobe);
	}
	else if (pomWay->Tybe[0] == 'R')
	{
		cmOldTybe = 'R';
		m_TYBER.SetCheck(1);
		m_POBE.SetFormat("x|#x|#x|#x|#");
		m_POBE.SetTextLimit(1,4);
		m_POBE.SetBKColor(YELLOW);  
		m_POBE.SetInitText(pomWay->Pobe);
	}
	else if (pomWay->Tybe[0] == 'G')
	{
		cmOldTybe = 'G';
		m_TYBEG.SetCheck(1);
		m_POBEG.EnableWindow(TRUE);
		m_POBE.SetBKColor(YELLOW);  
		m_POBE.EnableWindow(FALSE);
		SetCurSelByUrno(&m_POBEG,pomWay->Pobe);
		//m_POBEG.SetBKColor(YELLOW);
	}
	else
	{
		cmOldTybe = 'P';
		m_TYBEP.SetCheck(1);
		m_POBE.SetFormat("x|#x|#x|#x|#x|#");
		m_POBE.SetTextLimit(1,5);
		m_POBE.SetBKColor(YELLOW);  
		m_POBE.SetInitText(pomWay->Pobe);
	}
	clStat = ogPrivList.GetStat("WEGEZEITENDLG.m_POBE");
	SetWndStatAll(clStat,m_TYBEP);
	SetWndStatAll(clStat,m_TYBER);
	SetWndStatAll(clStat,m_TYBEG);
	//------------------------------------
	m_TYENP.SetCheck(0);
	m_TYENR.SetCheck(0);
	m_TYENG.SetCheck(0);
	if (pomWay->Tyen[0] == 'P')
	{
		cmOldTyen = 'P';
		m_TYENP.SetCheck(1);
		m_POEN.SetFormat("x|#x|#x|#x|#x|#");
		m_POEN.SetTextLimit(1,5);
		m_POEN.SetBKColor(YELLOW);  
		m_POEN.SetInitText(pomWay->Poen);
	}
	else if (pomWay->Tyen[0] == 'R')
	{
		cmOldTyen = 'R';
		m_TYENR.SetCheck(1);
		m_POEN.SetFormat("x|#x|#x|#x|#");
		m_POEN.SetTextLimit(1,4);
		m_POEN.SetBKColor(YELLOW);  
		m_POEN.SetInitText(pomWay->Poen);
	}
	else if (pomWay->Tyen[0] == 'G')
	{
		cmOldTyen = 'G';
		m_TYENG.SetCheck(1);
		m_POENG.EnableWindow(TRUE);
		m_POEN.SetBKColor(YELLOW);  
		m_POEN.EnableWindow(FALSE);
		SetCurSelByUrno(&m_POENG,pomWay->Poen);
		//m_POENG.SetBKColor(YELLOW);
	}
	else 
	{
		cmOldTyen = 'P';
		m_TYENP.SetCheck(1);
		m_POEN.SetFormat("x|#x|#x|#x|#x|#");
		m_POEN.SetTextLimit(1,5);
		m_POEN.SetBKColor(YELLOW);  
		m_POEN.SetInitText(pomWay->Poen);
	}
	clStat = ogPrivList.GetStat("WEGEZEITENDLG.m_POEN");
	SetWndStatAll(clStat,m_TYENP);
	SetWndStatAll(clStat,m_TYENR);
	SetWndStatAll(clStat,m_TYENG);
	//------------------------------------
	m_WTGOS.SetTypeToInt(0,9999);
	m_WTGOS.SetTextErrColor(RED);
	m_WTGOT.SetFormat("x|#x|#x|#x|#x|#");
	m_WTGOT.SetTextLimit(0,5);
	m_WTGOT.SetTextErrColor(RED);
	if (pomWay->Tybe[0] == 'R'||pomWay->Tyen[0] == 'R')
	{
		m_WTGOT.SetInitText(pomWay->Wtgo);		
		if(ogPrivList.GetStat("WEGEZEITENDLG.m_WTGO")=='1')
		{
			m_WTGOS.SetSecState('0');
			m_WTGOT.SetSecState('1');
		}
		else
		{
			m_WTGOS.SetSecState(ogPrivList.GetStat("WEGEZEITENDLG.m_WTGO"));
			m_WTGOT.SetSecState(ogPrivList.GetStat("WEGEZEITENDLG.m_WTGO"));
		}
	}
	else 
	{
		m_WTGOS.SetInitText(pomWay->Wtgo);		
		if(ogPrivList.GetStat("WEGEZEITENDLG.m_WTGO")=='1')
		{
			m_WTGOS.SetSecState('1');
			m_WTGOT.SetSecState('0');
		}
		else
		{
			m_WTGOS.SetSecState(ogPrivList.GetStat("WEGEZEITENDLG.m_WTGO"));
			m_WTGOT.SetSecState(ogPrivList.GetStat("WEGEZEITENDLG.m_WTGO"));
		}
	}
	//------------------------------------
	m_TTGO.SetTypeToInt(0,9999);
	m_TTGO.SetTextErrColor(RED);
	m_TTGO.SetInitText(pomWay->Ttgo);		
	m_TTGO.SetSecState(ogPrivList.GetStat("WEGEZEITENDLG.m_TTGO"));
	//------------------------------------
	clStat = ogPrivList.GetStat("WEGEZEITENDLG.m_WTYP");
	SetWndStatAll(clStat,m_WTYP);
	m_WTYP.AddString(LoadStg(IDS_STRING432));
	m_WTYP.AddString(LoadStg(IDS_STRING433));
	m_WTYP.AddString(LoadStg(IDS_STRING434));
	if(pomWay->Wtyp[0] == 'G') m_WTYP.SetCurSel(0);
	else if(pomWay->Wtyp[0] == 'R') m_WTYP.SetCurSel(1);
	else if(pomWay->Wtyp[0] == 'F') m_WTYP.SetCurSel(2);
	else m_WTYP.SetCurSel(0);
	//------------------------------------
	m_POBE.SetFocus();

	return TRUE;
}

//----------------------------------------------------------------------------------------

void WegezeitenDlg::OnOK() 
{
	AfxGetApp()->DoWaitCursor(1);
	CString olErrorText;
	bool ilStatus = true;

	if(m_TYBEP.GetCheck() == 1)
	{
		if(m_POBE.GetStatus() == false)
		{
			ilStatus = false;
			if(m_POBE.GetWindowTextLength() == 0)
			{
				olErrorText += LoadStg(IDS_STRING420) + LoadStg(IDS_STRING422) +  ogNoData;
			}
			else
			{
				olErrorText += LoadStg(IDS_STRING420) + LoadStg(IDS_STRING422) + ogNotFormat;
			}
		}
	}
	if(m_TYBER.GetCheck() == 1)
	{
		if(m_POBE.GetStatus() == false)
		{
			ilStatus = false;
			if(m_POBE.GetWindowTextLength() == 0)
			{
				olErrorText += LoadStg(IDS_STRING420) + LoadStg(IDS_STRING423) +  ogNoData;
			}
			else
			{
				olErrorText += LoadStg(IDS_STRING420) + LoadStg(IDS_STRING423) + ogNotFormat;
			}
		}
	}
	if(m_TYBEG.GetCheck() == 1)
	{
		if(m_POBEG.GetWindowTextLength() == 0)
		{
			ilStatus = false;
			olErrorText += LoadStg(IDS_STRING420) + LoadStg(IDS_STRING424) +  ogNoData;
		}
	}
	if(m_TYENP.GetCheck() == 1)
	{
		if(m_POEN.GetStatus() == false)
		{
			ilStatus = false;
			if(m_POEN.GetWindowTextLength() == 0)
			{
				olErrorText += LoadStg(IDS_STRING421) + LoadStg(IDS_STRING422) +  ogNoData;
			}
			else
			{
				olErrorText += LoadStg(IDS_STRING421) + LoadStg(IDS_STRING422) + ogNotFormat;
			}
		}
	}
	if(m_TYENR.GetCheck() == 1)
	{
		if(m_POEN.GetStatus() == false)
		{
			ilStatus = false;
			if(m_POEN.GetWindowTextLength() == 0)
			{
				olErrorText += LoadStg(IDS_STRING421) + LoadStg(IDS_STRING423) +  ogNoData;
			}
			else
			{
				olErrorText += LoadStg(IDS_STRING421) + LoadStg(IDS_STRING423) + ogNotFormat;
			}
		}

	}
	if(m_TYENG.GetCheck() == 1)
	{
		if(m_POENG.GetWindowTextLength() == 0)
		{
			ilStatus = false;
			olErrorText += LoadStg(IDS_STRING421) + LoadStg(IDS_STRING424) +  ogNoData;
		}
	}
	if(m_TYBER.GetCheck() == 1 || m_TYENR.GetCheck() == 1)
	{
		if(m_WTGOT.GetStatus() == false)
		{
			ilStatus = false;
			olErrorText += LoadStg(IDS_STRING425) + ogNotFormat;
		}
	}
	else
	{
		if(m_WTGOS.GetStatus() == false)
		{
			ilStatus = false;
			olErrorText += LoadStg(IDS_STRING426) + ogNotFormat;
		}
	}
	if(m_TTGO.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING427) + ogNotFormat;
	}

	///////////////////////////////////////////////////////////////////
	//Konsistenz-Check//
	if(ilStatus == true)
	{
		char clWhere[100];
		CString olTmpTXT;

		if(m_TYBEP.GetCheck() == 1)
		{
			m_POBE.GetWindowText(olTmpTXT);
			sprintf(clWhere,"WHERE PNAM='%s'",olTmpTXT);
			if(ogPSTData.ReadSpecialData(NULL,clWhere,"PNAM",false) == false)
			{
				ilStatus = false;
				olErrorText += LoadStg(IDS_STRING428);
			}
		}
		if(m_TYBER.GetCheck() == 1)
		{
			m_POBE.GetWindowText(olTmpTXT);
			sprintf(clWhere,"WHERE RNAM='%s'",olTmpTXT);
			if(ogRWYData.ReadSpecialData(NULL,clWhere,"RNAM",false) == false)
			{
				ilStatus = false;
				olErrorText += LoadStg(IDS_STRING429);
			}
		}

		if(m_TYENP.GetCheck() == 1)
		{
			m_POEN.GetWindowText(olTmpTXT);
			sprintf(clWhere,"WHERE PNAM='%s'",olTmpTXT);
			if(ogPSTData.ReadSpecialData(NULL,clWhere,"PNAM",false) == false)
			{
				ilStatus = false;
				olErrorText += LoadStg(IDS_STRING430);
			}
		}
		if(m_TYENR.GetCheck() == 1)
		{
			m_POEN.GetWindowText(olTmpTXT);
			sprintf(clWhere,"WHERE RNAM='%s'",olTmpTXT);
			if(ogRWYData.ReadSpecialData(NULL,clWhere,"RNAM",false) == false)
			{
				ilStatus = false;
				olErrorText += LoadStg(IDS_STRING431);
			}
		}
		if(m_TYENR.GetCheck() == 1 || m_TYBER.GetCheck() == 1)
		{
			m_WTGOT.GetWindowText(olTmpTXT);
			sprintf(clWhere,"WHERE TNAM='%s'",olTmpTXT);
			if(ogTWYData.ReadSpecialData(NULL,clWhere,"TNAM",false) == false)
			{
				ilStatus = false;
				olErrorText += LoadStg(IDS_STRING100);
			}
		}
	}
	///////////////////////////////////////////////////////////////////
	AfxGetApp()->DoWaitCursor(-1);
	if (ilStatus == true)
	{
		CString olWtyp;
		m_WTYP.GetWindowText(olWtyp);
		if(olWtyp == LoadStg(IDS_STRING432)) pomWay->Wtyp[0] = 'G';
		if(olWtyp == LoadStg(IDS_STRING433)) pomWay->Wtyp[0] = 'R';
		if(olWtyp == LoadStg(IDS_STRING434)) pomWay->Wtyp[0] = 'F';

		if(m_TYBEP.GetCheck() == 1)
		{
			pomWay->Tybe[0] = 'P';
			m_POBE.GetWindowText(pomWay->Pobe,6);
		}
		if(m_TYBER.GetCheck() == 1)
		{
			pomWay->Tybe[0] = 'R';
			m_POBE.GetWindowText(pomWay->Pobe,5);
		}
		if(m_TYBEG.GetCheck() == 1)
		{
			pomWay->Tybe[0] = 'G';
			strcpy(pomWay->Pobe, GetUrnoBySetCurSel(&m_POBEG));
		}
		if(m_TYENP.GetCheck() == 1)
		{
			pomWay->Tyen[0] = 'P';
			m_POEN.GetWindowText(pomWay->Poen,6);
		}
		if(m_TYENR.GetCheck() == 1)
		{
			pomWay->Tyen[0] = 'R';
			m_POEN.GetWindowText(pomWay->Poen,5);
		}
		if(m_TYENG.GetCheck() == 1)
		{
			pomWay->Tyen[0] = 'G';
			strcpy(pomWay->Poen, GetUrnoBySetCurSel(&m_POENG));
		}
		if(m_TYENR.GetCheck() == 1 || m_TYBER.GetCheck() == 1)
		{
			m_WTGOT.GetWindowText(pomWay->Wtgo,6);
		}
		else
		{
			m_WTGOS.GetWindowText(pomWay->Wtgo,6);
		}

		m_TTGO.GetWindowText(pomWay->Ttgo,5);
		m_HOME.GetWindowText(pomWay->Home,4);

		CDialog::OnOK();
	}
	else
	{
		Beep(440,70);
		MessageBox(olErrorText,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
		if(m_TYBEG.GetCheck() == 1)
		{
			m_POBEG.SetFocus();
		}
		else
		{
			m_POBE.SetFocus();
		}
	}	
}

//----------------------------------------------------------------------------------------

void WegezeitenDlg::OnTybe() 
{
	char clStat;
	if(cmOldTybe !='P' && m_TYBEP.GetCheck() == 1)
	{
		if(ogPrivList.GetStat("WEGEZEITENDLG.m_WTGO")=='1' && cmOldTyen != 'R' && cmOldTybe == 'R')
		{
			m_WTGOT.SetSecState('0');
			m_WTGOS.SetSecState('1');
		}
		if(cmOldTybe =='G')
		{
			clStat = ogPrivList.GetStat("WEGEZEITENDLG.m_POBE");
			SetWndStatPrio_1(clStat,'0',m_POBEG);
			if(clStat!='-' && clStat!='0') m_POBE.SetSecState('1');
			//m_POBEG.SetBKColor(SILVER);  

		}
		m_POBE.SetFormat("x|#x|#x|#x|#x|#");
		m_POBE.SetTextLimit(1,5);
		cmOldTybe ='P';
	}
	else if(cmOldTybe !='R' && m_TYBER.GetCheck() == 1)
	{
		if(ogPrivList.GetStat("WEGEZEITENDLG.m_WTGO")=='1' && cmOldTyen != 'R')
		{
			m_WTGOT.SetSecState('1');
			m_WTGOS.SetSecState('0');
		}
		if(cmOldTybe =='G')
		{
			clStat = ogPrivList.GetStat("WEGEZEITENDLG.m_POBE");
			SetWndStatPrio_1(clStat,'0',m_POBEG);
			if(clStat!='-' && clStat!='0') m_POBE.SetSecState('1');
			//m_POBEG.SetBKColor(SILVER);  

		}
		m_POBE.SetFormat("x|#x|#x|#x|#");
		m_POBE.SetTextLimit(1,4);
		cmOldTybe ='R';
	}
	else if(cmOldTybe !='G' && m_TYBEG.GetCheck() == 1)
	{
		if(ogPrivList.GetStat("WEGEZEITENDLG.m_WTGO")=='1' && cmOldTyen != 'R' && cmOldTybe == 'R')
		{
			m_WTGOT.SetSecState('0');
			m_WTGOS.SetSecState('1');
		}
		clStat = ogPrivList.GetStat("WEGEZEITENDLG.m_POBE");
		SetWndStatPrio_1(clStat,'1',m_POBEG);
		if(clStat!='-') m_POBE.SetSecState('0');
		//m_POBEG.SetBKColor(YELLOW);  

		cmOldTybe ='G';
	}
	m_POBE.InvalidateRect(NULL);
	m_POBE.UpdateWindow();
	m_POBEG.InvalidateRect(NULL);
	m_POBEG.UpdateWindow();
	m_WTGOT.InvalidateRect(NULL);
	m_WTGOT.UpdateWindow();
	m_WTGOS.InvalidateRect(NULL);
	m_WTGOS.UpdateWindow();

}

//----------------------------------------------------------------------------------------

void WegezeitenDlg::OnTyen() 
{
	char clStat;
	if(cmOldTyen !='P' && m_TYENP.GetCheck() == 1)
	{
		if(ogPrivList.GetStat("WEGEZEITENDLG.m_WTGO")=='1' && cmOldTyen == 'R' && cmOldTybe != 'R')
		{
			m_WTGOT.SetSecState('0');
			m_WTGOS.SetSecState('1');
		}
		if(cmOldTyen =='G')
		{
			clStat = ogPrivList.GetStat("WEGEZEITENDLG.m_POEN");
			SetWndStatPrio_1(clStat,'0',m_POENG);
			//m_POENG.SetBKColor(SILVER);
			if(clStat!='-' && clStat!='0') m_POEN.SetSecState('1');
		}
		m_POEN.SetFormat("x|#x|#x|#x|#x|#");
		m_POEN.SetTextLimit(1,5);
		cmOldTyen ='P';
	}
	else if(cmOldTyen !='R'  && m_TYENR.GetCheck() == 1)
	{
		if(ogPrivList.GetStat("WEGEZEITENDLG.m_WTGO")=='1' && cmOldTybe != 'R')
		{
			m_WTGOT.SetSecState('1');
			m_WTGOS.SetSecState('0');
		}
		if(cmOldTyen =='G')
		{
			clStat = ogPrivList.GetStat("WEGEZEITENDLG.m_POEN");
			SetWndStatPrio_1(clStat,'0',m_POENG);
			if(clStat!='-' && clStat!='0') m_POEN.SetSecState('1');
			//m_POENG.SetBKColor(SILVER);

		}
		m_POEN.SetFormat("x|#x|#x|#x|#");
		m_POEN.SetTextLimit(1,4);
		cmOldTyen ='R';
	}
	else if(cmOldTyen !='G' && m_TYENG.GetCheck() == 1)
	{
		if(ogPrivList.GetStat("WEGEZEITENDLG.m_WTGO")=='1' && cmOldTyen == 'R' && cmOldTybe != 'R')
		{
			m_WTGOT.SetSecState('0');
			m_WTGOS.SetSecState('1');
		}
		clStat = ogPrivList.GetStat("WEGEZEITENDLG.m_POEN");
		SetWndStatPrio_1(clStat,'1',m_POENG);
		if(clStat!='-') m_POEN.SetSecState('0');
		//m_POENG.SetBKColor(YELLOW);

		cmOldTyen ='G';
	}
	m_POEN.InvalidateRect(NULL);
	m_POEN.UpdateWindow();
	m_POENG.InvalidateRect(NULL);
	m_POENG.UpdateWindow();
	m_WTGOT.InvalidateRect(NULL);
	m_WTGOT.UpdateWindow();
	m_WTGOS.InvalidateRect(NULL);
	m_WTGOS.UpdateWindow();
	
}

//----------------------------------------------------------------------------------------

bool WegezeitenDlg::GetGruppnames() 
{
	CString olGrupp;
	CCSPtrArray<GRNDATA> olGrnCPA;
	char clWhere[100];
	sprintf(clWhere,"WHERE TABN='PST%s'",ogTableExt);

	if(ogGrnData.ReadSpecialData(&olGrnCPA,clWhere,"URNO,GRSN,GRPN",false) == true)
	{
		int ilGrnSize = olGrnCPA.GetSize();
		for(int i=0;i<ilGrnSize;i++)
		{
			olGrupp.Format("%-3s  %-12s       URNO=%i",olGrnCPA[i].Grsn,olGrnCPA[i].Grpn,olGrnCPA[i].Urno);
			m_POBEG.AddString(olGrupp);
			m_POENG.AddString(olGrupp);
		}
		olGrnCPA.DeleteAll();
		return true;
	}
	else
	{
		olGrnCPA.DeleteAll();
		return false;
	}
}

//----------------------------------------------------------------------------------------

void WegezeitenDlg::SetCurSelByUrno(CComboBox* popCBox,CString opUrno)
{
	CString olGrupp,olTmp;
	olGrupp.Format("URNO=%s",opUrno);
	int ilCount = popCBox->GetCount();
	for(int i=0; i<ilCount; i++)
	{
		popCBox->GetLBText(i,olTmp);
		if((olTmp.Find(olGrupp))!=-1)
		{
			popCBox->SetCurSel(i);
			break;
		}
	}
}

//----------------------------------------------------------------------------------------

CString WegezeitenDlg::GetUrnoBySetCurSel(CComboBox* popCBox)
{
	CString olGrupp,olUrno = "0";
	popCBox->GetWindowText(olGrupp);

	if(olGrupp.GetLength() > (olGrupp.Find("URNO=")+5))
		olUrno = olGrupp.Mid(olGrupp.Find("URNO=")+5);
	return olUrno;
}

//----------------------------------------------------------------------------------------
