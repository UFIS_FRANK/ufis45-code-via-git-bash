// PsUnifilter.cpp : implementation file
//

#include "stdafx.h"
#include "CCSGlobl.h"
#include "PSUniFilterPage.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CPsUniFilter property page

IMPLEMENT_DYNCREATE(CPsUniFilter, CPropertyPage)

CPsUniFilter::CPsUniFilter() : CPropertyPage(CPsUniFilter::IDD)
{
	//{{AFX_DATA_INIT(CPsUniFilter)
	m_EditBedingung = _T("");
	m_EditFilter = _T("");
	//}}AFX_DATA_INIT
	pomCedaData = NULL;
}

CPsUniFilter::~CPsUniFilter()
{
}

void CPsUniFilter::SetCalledFrom(CString opCalledFrom)
{
	omCalledFrom = opCalledFrom;

	     if(omCalledFrom == LoadStg(IDS_STRING173)) pomCedaData = (CCSCedaData *)&ogALTData;
	else if(omCalledFrom == LoadStg(IDS_STRING176)) pomCedaData = (CCSCedaData *)&ogACTData;
	else if(omCalledFrom == LoadStg(IDS_STRING182)) pomCedaData = (CCSCedaData *)&ogACRData;
	else if(omCalledFrom == LoadStg(IDS_STRING174)) pomCedaData = (CCSCedaData *)&ogAPTData;
	else if(omCalledFrom == LoadStg(IDS_STRING190)) pomCedaData = (CCSCedaData *)&ogRWYData;
	else if(omCalledFrom == LoadStg(IDS_STRING191)) pomCedaData = (CCSCedaData *)&ogTWYData;
	else if(omCalledFrom == LoadStg(IDS_STRING185)) pomCedaData = (CCSCedaData *)&ogPSTData;
	else if(omCalledFrom == LoadStg(IDS_STRING178)) pomCedaData = (CCSCedaData *)&ogGATData;
	else if(omCalledFrom == LoadStg(IDS_STRING166)) pomCedaData = (CCSCedaData *)&ogCICData;
	else if(omCalledFrom == LoadStg(IDS_STRING179)) pomCedaData = (CCSCedaData *)&ogBLTData;
	else if(omCalledFrom == LoadStg(IDS_STRING163)) pomCedaData = (CCSCedaData *)&ogEXTData;
	else if(omCalledFrom == LoadStg(IDS_STRING167)) pomCedaData = (CCSCedaData *)&ogDENData;
	else if(omCalledFrom == LoadStg(IDS_STRING192)) pomCedaData = (CCSCedaData *)&ogMVTData;
	else if(omCalledFrom == LoadStg(IDS_STRING194)) pomCedaData = (CCSCedaData *)&ogNATData;
	else if(omCalledFrom == LoadStg(IDS_STRING160)) pomCedaData = (CCSCedaData *)&ogHAGData;
	else if(omCalledFrom == LoadStg(IDS_STRING195)) pomCedaData = (CCSCedaData *)&ogWROData;
	else if(omCalledFrom == LoadStg(IDS_STRING161)) pomCedaData = (CCSCedaData *)&ogHTYData;
	else if(omCalledFrom == LoadStg(IDS_STRING188)) pomCedaData = (CCSCedaData *)&ogSTYData;
	else if(omCalledFrom == LoadStg(IDS_STRING172)) pomCedaData = (CCSCedaData *)&ogFIDData;
	else if(omCalledFrom == LoadStg(IDS_STRING170)) pomCedaData = (CCSCedaData *)&ogSphData; //the same table SPH
	else if(omCalledFrom == LoadStg(IDS_STRING169)) pomCedaData = (CCSCedaData *)&ogSphData; //the same table SPH
	else if(omCalledFrom == LoadStg(IDS_STRING189)) pomCedaData = (CCSCedaData *)&ogStrData;
	else if(omCalledFrom == LoadStg(IDS_STRING175)) pomCedaData = (CCSCedaData *)&ogSeaData;
	else if(omCalledFrom == LoadStg(IDS_STRING181)) pomCedaData = (CCSCedaData *)&ogGhsData;
	else if(omCalledFrom == LoadStg(IDS_STRING186)) pomCedaData = (CCSCedaData *)&ogPerData;
	else if(omCalledFrom == LoadStg(IDS_STRING184)) pomCedaData = (CCSCedaData *)&ogOrgData;
	else if(omCalledFrom == LoadStg(IDS_STRING162)) pomCedaData = (CCSCedaData *)&ogCotData;
	else if(omCalledFrom == LoadStg(IDS_STRING165)) pomCedaData = (CCSCedaData *)&ogAsfData;
	else if(omCalledFrom == LoadStg(IDS_STRING164)) pomCedaData = (CCSCedaData *)&ogBsdData;
	else if(omCalledFrom == LoadStg(IDS_STRING177)) pomCedaData = (CCSCedaData *)&ogPfcData;
	else if(omCalledFrom == LoadStg(IDS_STRING171)) pomCedaData = (CCSCedaData *)&ogTeaData;
	else if(omCalledFrom == LoadStg(IDS_STRING187)) pomCedaData = (CCSCedaData *)&ogPrcData;
	else if(omCalledFrom == LoadStg(IDS_STRING168)) pomCedaData = (CCSCedaData *)&ogOdaData;
	else if(omCalledFrom == LoadStg(IDS_STRING183)) pomCedaData = (CCSCedaData *)&ogStfData;
	else if(omCalledFrom == LoadStg(IDS_STRING180)) pomCedaData = (CCSCedaData *)&ogGegData;
	else if(omCalledFrom == LoadStg(IDS_STRING196)) pomCedaData = (CCSCedaData *)&ogWayData;
	else if(omCalledFrom == LoadStg(IDS_STRING193)) pomCedaData = (CCSCedaData *)&ogChtData;
	else if(omCalledFrom == LoadStg(IDS_STRING197)) pomCedaData = (CCSCedaData *)&ogTipData;
	else if(omCalledFrom == LoadStg(IDS_STRING198)) pomCedaData = (CCSCedaData *)&ogHolData;
	else if(omCalledFrom == LoadStg(IDS_STRING199)) pomCedaData = (CCSCedaData *)&ogWgpData;
	else if(omCalledFrom == LoadStg(IDS_STRING200)) pomCedaData = (CCSCedaData *)&ogPgpData;
	else if(omCalledFrom == LoadStg(IDS_STRING201)) pomCedaData = (CCSCedaData *)&ogAwiData;
	else if(omCalledFrom == LoadStg(IDS_STRING202)) pomCedaData = (CCSCedaData *)&ogCccData;
	else if(omCalledFrom == LoadStg(IDS_STRING203)) pomCedaData = (CCSCedaData *)&ogVipData;
	else if(omCalledFrom == LoadStg(IDS_STRING618)) pomCedaData = (CCSCedaData *)&ogAFMData;
	else if(omCalledFrom == LoadStg(IDS_STRING619)) pomCedaData = (CCSCedaData *)&ogENTData;
}

void CPsUniFilter::SetCaption(const char *pcpCaption)
{
	m_psp.pszTitle = pcpCaption;
	m_psp.dwFlags |= PSP_USETITLE;
}

BOOL CPsUniFilter::OnCommand(WPARAM wParam, LPARAM lParam) 
{
	if (HIWORD(wParam) == BN_CLICKED || HIWORD(wParam) == EN_SETFOCUS)
		CancelToClose();
	return CPropertyPage::OnCommand(wParam, lParam);
}

void CPsUniFilter::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CPsUniFilter)
	DDX_Control(pDX, IDC_COMBO_Datenfeld, m_ComboDatenfeld);
	DDX_Control(pDX, IDC_COMBO_Operator, m_ComboOperator);
	DDX_Control(pDX, IDC_EDIT_Bedingung, m_CtrlBedingung);
	//DDX_Control(pDX, IDC_EDIT_Filter, E_EditFilter);
	DDX_Text(pDX, IDC_EDIT_Filter, m_EditFilter);
	DDX_Control(pDX, IDC_EDIT_Filter, m_Where);
	//DDX_Radio(pDX, IDC_RADIO_AND, m_ButtonConnect);
	DDX_Control(pDX, IDC_RADIO_AND, m_And);
	DDX_Control(pDX, IDC_RADIO_OR, m_Or);
	//}}AFX_DATA_MAP
	if (pDX->m_bSaveAndValidate == FALSE)
	{
		SetData();
	}
	if (pDX->m_bSaveAndValidate == TRUE)
	{
		GetData();
	}
}


BEGIN_MESSAGE_MAP(CPsUniFilter, CPropertyPage)
	//{{AFX_MSG_MAP(CPsUniFilter)
	ON_BN_CLICKED(IDC_BUTTON_Neu, OnBUTTONNeu)
	ON_BN_CLICKED(IDC_BUTTON_Setzen, OnBUTTONSetzen)
	ON_CBN_SELCHANGE(IDC_COMBO_Datenfeld, OnSelchangeCOMBODatenfeld)
	ON_BN_CLICKED(IDC_RADIO_AND, OnRadioAnd)
	ON_BN_CLICKED(IDC_RADIO_OR, OnRadioOr)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CPsUniFilter message handlers

void CPsUniFilter::OnBUTTONNeu() 
{
	omFilter.Empty();
	m_Where.SetWindowText("");
}

void CPsUniFilter::OnBUTTONSetzen() 
{
	// TODO: Add your control notification handler code here
	// TODO: Add your control notification handler code here
	CString olFilter;
	//CString olTabelle, olDatenfeld, olOperator, olValue;

	// TODO: Add your control notification handler code here
	//UpdateData(TRUE);
	CString olOperator;
	CString olField;
	CString olValue;
	CString olPercent;
	m_ComboOperator.GetWindowText(olOperator);
	int ilIdx = m_ComboDatenfeld.GetCurSel();
	if(ilIdx != LB_ERR)
	{
		olField = omFieldArray[ilIdx];
	}
	else
	{
		return;
	}
	m_CtrlBedingung.GetWindowText(olValue);

	if(ilIdx != LB_ERR)
	{
		if(omTypeArray[ilIdx] == CString("Date"))
		{
			if(ReverseDateString(olValue) == false)
			{
				MessageBox(LoadStg(IDS_STRING223), LoadStg(IDS_STRING149), (MB_ICONEXCLAMATION| MB_OK));
				return;
			}
		}
	}
	omConnection == "";
	if(m_And.GetCheck() == 1)
	{
		omConnection = "AND";
	}
	else if(m_Or.GetCheck() == 1)
	{
		omConnection = "OR";
	}

	if(omConnection.IsEmpty())
		return;
	if(olOperator == " LIKE ")
	{
		olPercent = CString("%");
	}
	olFilter = olField + olOperator + CString("'") + olValue + olPercent + CString("'");

	CWnd *polWnd = GetDlgItem(IDC_EDIT_Filter);
	if(polWnd != NULL)
		polWnd->GetWindowText(m_EditFilter);
	if(m_EditFilter.IsEmpty())
	{
		if(olField.IsEmpty() || olValue.IsEmpty() || olOperator.IsEmpty())
		{
			return;
		}
		else
		{
			m_EditFilter = olFilter;
		}
	}
	else if (m_EditFilter.GetLength() > 0)
	{
		if(olField.IsEmpty() || olValue.IsEmpty() || olOperator.IsEmpty())
		{
			return;
		}
		else
		{
			m_EditFilter+= " " + omConnection + " " + olFilter;
		}
	} // end if

	if(polWnd != NULL)
	{
		polWnd->SetWindowText(m_EditFilter);
		CEdit *polEdit = (CEdit*) GetDlgItem(IDC_EDIT_Filter);
		if (polEdit)
			polEdit->LineScroll(polEdit->GetLineCount(), 0 );
	}

	//UpdateData(FALSE);

}

void CPsUniFilter::OnSelchangeCOMBOTabelle() 
{
	// TODO: Add your control notification handler code here
}

void CPsUniFilter::OnSelchangeCOMBODatenfeld() 
{
	// TODO: Add your control notification handler code here
} 

void CPsUniFilter::InitMask()
{
	SetFieldAndDescriptionArrays();
	int ilCount = omFieldArray.GetSize();
	for(int i = 0; i < ilCount; i++)
	{
		char pclText[200]=""; 
		sprintf(pclText, "%s...%s", omFieldArray[i], omDescArray[i]);
		m_ComboDatenfeld.AddString(pclText);
	}
	ilCount = omOperators.GetSize();
	for(i = 0; i < ilCount; i++)
	{
		m_ComboOperator.AddString(omOperators[i]);
	}
} 

BOOL CPsUniFilter::OnInitDialog() 
{
	CPropertyPage::OnInitDialog();
	
 	m_ComboDatenfeld.SetFont(&ogCourier_Regular_8);
	InitMask();
	m_And.SetCheck(1);

	CEdit *polEdit = (CEdit*) GetDlgItem(IDC_EDIT_Filter);//CWndReadOnly
	if (polEdit)
		polEdit->SetReadOnly(!bgViewEditFilter);

	return TRUE;  
}

void CPsUniFilter::OnRadioAnd() 
{
	omConnection = "AND";	
}

void CPsUniFilter::OnRadioOr() 
{
	omConnection = "OR";
}

void CPsUniFilter::SetData()
{
	m_EditFilter.Empty();
	for(int i = 0; i< omValues.GetSize(); i++)
	{
		m_EditFilter += omValues[i];
	}
	int ilIdx;
	while((ilIdx = m_EditFilter.Find('@')) != -1)
	{
		m_EditFilter.SetAt(ilIdx, ' ');
	}
	m_EditFilter.Replace('?','\'');
	m_EditFilter.TrimLeft(); m_EditFilter.TrimRight();
	CWnd *polWnd = GetDlgItem(IDC_EDIT_Filter);
	if(polWnd != NULL)
		polWnd->SetWindowText(m_EditFilter);
	
}
void CPsUniFilter::GetData()
{
	omValues.RemoveAll();
	if(!m_EditFilter.IsEmpty())
	{
		m_EditFilter += CString("@");
	}
	omValues.Add(m_EditFilter);
}

void CPsUniFilter::SetFieldAndDescriptionArrays()
{
	omFieldArray.RemoveAll();
	omDescArray.RemoveAll();
	omTypeArray.RemoveAll();

	if(pomCedaData != NULL)
	{
		pomCedaData->GetDataInfo(omFieldArray, omDescArray, omTypeArray);
	}

	omOperators.Add(" = ");
	omOperators.Add(" <= ");
	omOperators.Add(" >= ");
	omOperators.Add(" < ");
	omOperators.Add(" > ");
	omOperators.Add(" <> ");
	omOperators.Add(" LIKE ");


}

bool CPsUniFilter::ReverseDateString(CString &ropSource)
{
	
	if(ropSource.GetLength() < 4)
		return false;


	//Eventuelle Punkte und Doppelpunkte rausfiltern
	StripSign(ropSource, '.');
	StripSign(ropSource, ' ');
	StripSign(ropSource, ':');
	if(ropSource.Find('%') == -1)
	{

		//String auf 14 Stellen mit 0 auff�llen
		int ilDiff = 15 - ropSource.GetLength();
		for(int i = 0; i < ilDiff; i++)
		{
			ropSource += "0";
		}

		if(ropSource.GetLength() < 12)
			return false;


		if(atoi(ropSource.Mid(4,4)) < 1970)
			return false;
		if(atoi(ropSource.Mid(2,2)) > 12)
			return false;
		if(atoi(ropSource.Mid(0,2)) > 31)
			return false;
		if(atoi(ropSource.Mid(8,2)) > 23)
			return false;
		if(atoi(ropSource.Mid(10,2)) > 59)
			return false;
		ropSource = ropSource.Mid(4,4) + ropSource.Mid(2,2) + ropSource.Mid(0,2) +
				  ropSource.Mid(8,2) + ropSource.Mid(10,2) + ropSource.Right(2);
	}
	return true;
}

void CPsUniFilter::StripSign(CString &ropString, char cpChar)
{
	CString olSubString = ropString;
	ropString.Empty();
	if(!olSubString.IsEmpty())
	{
		int pos;
		int olPos = 0;
		bool blEnd = false;
		CString olText;
		while(blEnd == false)
		{
			pos = olSubString.Find(cpChar);
			if(pos == -1)
			{
				blEnd = true;
				olText = olSubString;
			}
			else
			{
				olText = olSubString.Mid(0, olSubString.Find(cpChar));
				olSubString = olSubString.Mid(olSubString.Find(cpChar)+1, olSubString.GetLength( )-olSubString.Find(cpChar)+1);
			}
			ropString += olText;
		}
	}
}

