// CedaStfData.cpp
 
#include "stdafx.h"
#include "CedaStfData.h"
#include "resource.h"

void ProcessStfCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

//--CEDADATA-----------------------------------------------------------------------------------------------

CedaStfData::CedaStfData() : CCSCedaData(&ogCommHandler)
{
	// Create an array of CEDARECINFO for STFDataStruct
	BEGIN_CEDARECINFO(STFDATA,STFDataRecInfo)
		FIELD_DATE		(Cdat,"CDAT")
		FIELD_OLEDATE	(Dodm,"DODM")
		FIELD_OLEDATE	(Doem,"DOEM")
		FIELD_CHAR_TRIM	(Finm,"FINM")
		FIELD_CHAR_TRIM	(Gsmn,"GSMN")
		FIELD_CHAR_TRIM	(Ktou,"KTOU")
		FIELD_CHAR_TRIM	(Lanm,"LANM")
		FIELD_CHAR_TRIM	(Lino,"LINO")
		FIELD_DATE		(Lstu,"LSTU")
		FIELD_CHAR_TRIM	(Makr,"MAKR")
		FIELD_CHAR_TRIM	(Matr,"MATR")
		FIELD_CHAR_TRIM	(Peno,"PENO")
		FIELD_CHAR_TRIM	(Perc,"PERC")
		FIELD_CHAR_TRIM	(Prfl,"PRFL")
		FIELD_CHAR_TRIM	(Rema,"REMA")
		FIELD_CHAR_TRIM	(Shnm,"SHNM")
		FIELD_CHAR_TRIM	(Sken,"SKEN")
		FIELD_CHAR_TRIM	(Teld,"TELD")
		FIELD_CHAR_TRIM	(Telh,"TELH")
		FIELD_CHAR_TRIM	(Telp,"TELP")
		FIELD_CHAR_TRIM	(Tohr,"TOHR")
		FIELD_LONG		(Urno,"URNO")
		FIELD_CHAR_TRIM	(Usec,"USEC")
		FIELD_CHAR_TRIM	(Useu,"USEU")
		FIELD_CHAR_TRIM	(Zutb,"ZUTB")
		FIELD_CHAR_TRIM	(Zutf,"ZUTF")
		FIELD_CHAR_TRIM	(Zutn,"ZUTN")
		FIELD_CHAR_TRIM	(Zutw,"ZUTW")

	END_CEDARECINFO //(STFDataStruct)

	// FIELD_LONG, FIELD_DATE 
	// Copy the record structure
	for (int i=0; i< sizeof(STFDataRecInfo)/sizeof(STFDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&STFDataRecInfo[i],sizeof(CEDARECINFO));
		omRecInfo.Add(prpCedaRecInfo);
	} // end for

	// initialize field names
	strcpy(pcmTableName,"STF");
	strcpy(pcmListOfFields,"CDAT,DODM,DOEM,FINM,GSMN,KTOU,LANM,LINO,LSTU,MAKR,MATR,PENO,PERC,"
						   "PRFL,REMA,SHNM,SKEN,TELD,TELH,TELP,TOHR,URNO,USEC,USEU,"
						   "ZUTB,ZUTF,ZUTN,ZUTW");
	pcmFieldList = pcmListOfFields; //because in CedaData pcmFieldList is just a pointer

	omData.RemoveAll();
}

//---------------------------------------------------------------------------------------------------------

void CedaStfData::GetDataInfo(CStringArray &ropFields, CStringArray &ropDesription, CStringArray &ropType)
{
	ropFields.Add("CDAT");
	ropFields.Add("DODM");
	ropFields.Add("DOEM");
	ropFields.Add("FINM");
	ropFields.Add("GSMN");
	ropFields.Add("KTOU");
	ropFields.Add("LANM");
	ropFields.Add("LINO");
	ropFields.Add("LSTU");
	ropFields.Add("MAKR");
	ropFields.Add("MATR");
	ropFields.Add("PENO");
	ropFields.Add("PERC");
	ropFields.Add("PRFL");
	ropFields.Add("REMA");
	ropFields.Add("SHNM");
	ropFields.Add("SKEN");
	ropFields.Add("TELD");
	ropFields.Add("TELH");
	ropFields.Add("TELP");
	ropFields.Add("TOHR");
	ropFields.Add("URNO");
	ropFields.Add("USEC");
	ropFields.Add("USEU");
	ropFields.Add("ZUTB");
	ropFields.Add("ZUTF");
	ropFields.Add("ZUTN");
	ropFields.Add("ZUTW");

	ropDesription.Add(LoadStg(IDS_STRING343));
	ropDesription.Add(LoadStg(IDS_STRING65));
	ropDesription.Add(LoadStg(IDS_STRING67));
	ropDesription.Add(LoadStg(IDS_STRING69));
	ropDesription.Add(LoadStg(IDS_STRING638));
	ropDesription.Add(LoadStg(IDS_STRING70));
	ropDesription.Add(LoadStg(IDS_STRING288));
	ropDesription.Add(LoadStg(IDS_STRING639));
	ropDesription.Add(LoadStg(IDS_STRING344));
	ropDesription.Add(LoadStg(IDS_STRING523));
	ropDesription.Add(LoadStg(IDS_STRING640));
	ropDesription.Add(LoadStg(IDS_STRING71));
	ropDesription.Add(LoadStg(IDS_STRING72));
	ropDesription.Add(LoadStg(IDS_STRING345));
	ropDesription.Add(LoadStg(IDS_STRING342));
	ropDesription.Add(LoadStg(IDS_STRING301));
	ropDesription.Add(LoadStg(IDS_STRING524));
	ropDesription.Add(LoadStg(IDS_STRING73));
	ropDesription.Add(LoadStg(IDS_STRING74));
	ropDesription.Add(LoadStg(IDS_STRING75));
	ropDesription.Add(LoadStg(IDS_STRING525));
	ropDesription.Add(LoadStg(IDS_STRING346));
	ropDesription.Add(LoadStg(IDS_STRING347));
	ropDesription.Add(LoadStg(IDS_STRING348));
	ropDesription.Add(LoadStg(IDS_STRING76));
	ropDesription.Add(LoadStg(IDS_STRING77));
	ropDesription.Add(LoadStg(IDS_STRING78));
	ropDesription.Add(LoadStg(IDS_STRING79));

	ropType.Add("Date");
	ropType.Add("Date");
	ropType.Add("Date");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("Date");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");

}

//--REGISTER----------------------------------------------------------------------------------------------

void CedaStfData::Register(void)
{
	ogDdx.Register((void *)this,BC_STF_CHANGE,	CString("STFDATA"), CString("Stf-changed"),	ProcessStfCf);
	ogDdx.Register((void *)this,BC_STF_NEW,		CString("STFDATA"), CString("Stf-new"),		ProcessStfCf);
	ogDdx.Register((void *)this,BC_STF_DELETE,	CString("STFDATA"), CString("Stf-deleted"),	ProcessStfCf);
}

//--~CEDADATA----------------------------------------------------------------------------------------------

CedaStfData::~CedaStfData(void)
{
	omRecInfo.DeleteAll();
	ClearAll();
}

//--CLEAR-ALL----------------------------------------------------------------------------------------------

void CedaStfData::ClearAll(bool bpWithRegistration)
{
    omUrnoMap.RemoveAll();
    omData.DeleteAll();
	if(bpWithRegistration)
	{
		ogDdx.UnRegister(this,NOTUSED);
	}
}

//--READ-ALL-----------------------------------------------------------------------------------------------

bool CedaStfData::Read(char *pspWhere /*NULL*/)
{
    // Select data from the database
	bool ilRc = true;
    omUrnoMap.RemoveAll();
    omData.DeleteAll();
	if(pspWhere == NULL)
	{	
		ilRc = CedaAction("RT");
	}
	else
	{
		ilRc = CedaAction("RT", pspWhere);
	}
	if (ilRc != true)
	{
		return ilRc;
	}
	// Load data from CedaData into the dynamic array of record
	for (int ilLc = 0; ilRc == true; ilLc++)
	{
		STFDATA *prlStf = new STFDATA;
		if ((ilRc = GetFirstBufferRecord(prlStf)) == true)
		{
			omData.Add(prlStf);//Update omData
			omUrnoMap.SetAt((void *)prlStf->Urno,prlStf);
		}
		else
		{
			delete prlStf;
		}
	}
    return true;
}

//--INSERT-------------------------------------------------------------------------------------------------

bool CedaStfData::Insert(STFDATA *prpStf)
{
	prpStf->IsChanged = DATA_NEW;
	if(Save(prpStf) == false) return false; //Update Database
	InsertInternal(prpStf);
    return true;
}

//--INSERT-INTERNAL--------------------------------------------------------------------------------------

bool CedaStfData::InsertInternal(STFDATA *prpStf)
{
	ogDdx.DataChanged((void *)this, STF_NEW,(void *)prpStf ); //Update Viewer
	omData.Add(prpStf);//Update omData
	omUrnoMap.SetAt((void *)prpStf->Urno,prpStf);
    return true;
}

//--DELETE-------------------------------------------------------------------------------------------------

bool CedaStfData::Delete(long lpUrno)
{
	STFDATA *prlStf = GetStfByUrno(lpUrno);
	if (prlStf != NULL)
	{
		prlStf->IsChanged = DATA_DELETED;
		//*** 29.09.99 SHA ***
		//if(Save(prlStf) == false) return false; //Update Database
		Save(prlStf);
		DeleteInternal(prlStf);
	}
    return true;
}

//--DELETE-INTERNAL----------------------------------------------------------------------------------------

bool CedaStfData::DeleteInternal(STFDATA *prpStf)
{
	ogDdx.DataChanged((void *)this,STF_DELETE,(void *)prpStf); //Update Viewer
	omUrnoMap.RemoveKey((void *)prpStf->Urno);
	int ilStfCount = omData.GetSize();
	for (int ilLc = 0; ilLc < ilStfCount; ilLc++)
	{
		if (omData[ilLc].Urno == prpStf->Urno)
		{
			omData.DeleteAt(ilLc);//Update omData
			break;
		}
	}
    return true;
}

//--UPDATE-------------------------------------------------------------------------------------------------

bool CedaStfData::Update(STFDATA *prpStf)
{
	if (GetStfByUrno(prpStf->Urno) != NULL)
	{
		if (prpStf->IsChanged == DATA_UNCHANGED)
		{
			prpStf->IsChanged = DATA_CHANGED;
		}
		if(Save(prpStf) == false) return false; //Update Database
		UpdateInternal(prpStf);
	}
    return true;
}

//--UPDATE-INTERNAL--------------------------------------------------------------------------------------------

bool CedaStfData::UpdateInternal(STFDATA *prpStf)
{
	STFDATA *prlStf = GetStfByUrno(prpStf->Urno);
	if (prlStf != NULL)
	{
		*prlStf = *prpStf; //Update omData
		ogDdx.DataChanged((void *)this,STF_CHANGE,(void *)prlStf); //Update Viewer
	}
    return true;
}

//--GET-BY-URNO--------------------------------------------------------------------------------------------

STFDATA *CedaStfData::GetStfByUrno(long lpUrno)
{
	STFDATA  *prlStf;
	if (omUrnoMap.Lookup((void *)lpUrno,(void *& )prlStf) == TRUE)
	{
		return prlStf;
	}
	return NULL;
}

//--READSPECIALDATA-------------------------------------------------------------------------------------

bool CedaStfData::ReadSpecialData(CCSPtrArray<STFDATA> *popStf,char *pspWhere,char *pspFieldList,bool ipSYS/*=true*/)
{
	bool ilRc = true;
	char pclFieldList[256] = " ";

	if(strlen(pspFieldList) > 0) 
	{
		strcpy(pclFieldList, pspFieldList);
	}
	if(ipSYS == true) 
	{
		if (CedaAction("SYS","STF",pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	else
	{
		if (CedaAction("RT","STF",pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	if(popStf != NULL)
	{
		for (int ilLc = 0; ilRc == true; ilLc++)
		{
			STFDATA *prpStf = new STFDATA;
			if ((ilRc = GetBufferRecord(ilLc,prpStf,CString(pclFieldList))) == true)
			{
				popStf->Add(prpStf);
			}
			else
			{
				delete prpStf;
			}
		}
		if(popStf->GetSize() == 0) return false;
	}
    return true;
}

//--SAVE---------------------------------------------------------------------------------------------------

bool CedaStfData::Save(STFDATA *prpStf)
{
	bool ilRc = true;
	CString olListOfData;
	char pclSelection[124];
	char pclData[524];

	if (prpStf->IsChanged == DATA_UNCHANGED)
	{
		return true; // no change, nothing to do
	}
	switch(prpStf->IsChanged)
	{
	case DATA_NEW:
		MakeCedaData(&omRecInfo,olListOfData,prpStf);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("IRT","","",pclData);
		prpStf->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_CHANGED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpStf->Urno);
		MakeCedaData(&omRecInfo,olListOfData,prpStf);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("URT",pclSelection,"",pclData);
		prpStf->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_DELETED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpStf->Urno);
		ilRc = CedaAction("DRT",pclSelection);
		break;
	}
    return ilRc;
}

//--PROCESS-CF---------------------------------------------------------------------------------------------

void  ProcessStfCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	ogStfData.ProcessBc(ipDDXType,vpDataPointer,ropInstanceName);
}

//--PROCESS-BC---------------------------------------------------------------------------------------------

void  CedaStfData::ProcessBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	struct BcStruct *prlStfData;
	prlStfData = (struct BcStruct *) vpDataPointer;
	STFDATA *prlStf;
	if(ipDDXType == BC_STF_NEW)
	{
		prlStf = new STFDATA;
		GetRecordFromItemList(prlStf,prlStfData->Fields,prlStfData->Data);
		InsertInternal(prlStf);
	}
	if(ipDDXType == BC_STF_CHANGE)
	{
		long llUrno = GetUrnoFromSelection(prlStfData->Selection);
		prlStf = GetStfByUrno(llUrno);
		if(prlStf != NULL)
		{
			GetRecordFromItemList(prlStf,prlStfData->Fields,prlStfData->Data);
			UpdateInternal(prlStf);
		}
	}
	if(ipDDXType == BC_STF_DELETE)
	{
		long llUrno;
		CString olSelection = (CString)prlStfData->Selection;
		if (olSelection.Find('\'') != -1)
		{
			llUrno = GetUrnoFromSelection(prlStfData->Selection);
		}
		else
		{
			int ilFirst = olSelection.Find("=")+2;
			int ilLast  = olSelection.GetLength();
			llUrno = atol(olSelection.Mid(ilFirst,ilLast-ilFirst));
		}
		prlStf = GetStfByUrno(llUrno);
		if (prlStf != NULL)
		{
			DeleteInternal(prlStf);
		}
	}
}

//---------------------------------------------------------------------------------------------------------
