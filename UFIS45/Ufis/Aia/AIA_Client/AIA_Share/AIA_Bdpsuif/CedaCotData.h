// CedaCotData.h

#ifndef __CEDAPCOTDATA__
#define __CEDAPCOTDATA__
 
#include "stdafx.h"
#include "basicdata.h"

//---------------------------------------------------------------------------------------------------------

struct COTDATA 
{
	CTime 	 Cdat; 		// Erstellungsdatum
	char 	 Ctrc[7]; 	// Code
	char 	 Ctrn[42]; 	// Bezeichnung
	char 	 Dptc[7]; 	// Organisationseinheit Code
	CTime 	 Lstu;		// Datum letzte Änderung
	char 	 Misl[6]; 	// Minimale Schichtlänge
	char 	 Mxsl[6]; 	// Maximale Schichtlänge
	char 	 Nowd[3]; 	// Arbeitstage pro Woche
	char 	 Prfl[3]; 	// Protokollierungskennzeichen
	char 	 Rema[62]; 	// Bemerkung
	long 	 Urno;		// Eindeutige Datensatz-Nr.
	char 	 Usec[34]; 	// Anwender (Ersteller)
	char 	 Useu[34]; 	// Anwender (letzte Änderung)
	char 	 Whpw[6]; 	// Arbeitstunden pro Wochen
	char 	 Wrkd[9]; 	// Wochenarbeitstage
	char 	 Sbpa[3]; 	// Pausen bezahlt//*** 07.09.99 SHA ***

	//DataCreated by this class
	int      IsChanged;

	COTDATA(void)
	{ 
		memset(this,'\0',sizeof(*this));
		Cdat=-1;
		Lstu=-1;
	}


}; // end COTDATA

//---------------------------------------------------------------------------------------------------------
// Class declaratino

class CedaCotData: public CCSCedaData
{
// Attributes
public:
    CMapPtrToPtr omUrnoMap;

    CCSPtrArray<COTDATA> omData;

	char pcmListOfFields[2048];

// OCotations
public:
    CedaCotData();
	~CedaCotData();
	virtual void GetDataInfo(CStringArray &ropFields, CStringArray &ropDesription, CStringArray &ropType);
	void Register(void);
	void ClearAll(bool bpWithRegistration = true);
    bool Read(char *pspWhere = NULL);
	bool Insert(COTDATA *prpCot);
	bool InsertInternal(COTDATA *prpCot);
	bool Update(COTDATA *prpCot);
	bool UpdateInternal(COTDATA *prpCot);
	bool Delete(long lpUrno);
	bool DeleteInternal(COTDATA *prpCot);
	bool ReadSpecialData(CCSPtrArray<COTDATA> *popCot,char *pspWhere,char *pspFieldList,bool ipSYS=true);
	bool Save(COTDATA *prpCot);
	void ProcessBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);
	COTDATA  *GetCotByUrno(long lpUrno);


	// Private methods
private:
    void PrepareCotData(COTDATA *prpCotData);

};

//---------------------------------------------------------------------------------------------------------


#endif //__CEDAPCOTDATA__
