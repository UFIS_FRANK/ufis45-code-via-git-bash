// CedaHolData.cpp
 
#include "stdafx.h"
#include "CedaHolData.h"
#include "resource.h"


void ProcessHolCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

//--CEDADATA-----------------------------------------------------------------------------------------------

CedaHolData::CedaHolData() : CCSCedaData(&ogCommHandler)
{
	// Create an array of CEDARECINFO for HOLDATA
	BEGIN_CEDARECINFO(HOLDATA,HolDataRecInfo)
		FIELD_LONG		(Urno,"URNO")
		FIELD_DATE		(Cdat,"CDAT")
		FIELD_DATE		(Lstu,"LSTU")
		FIELD_CHAR_TRIM	(Usec,"USEC")
		FIELD_CHAR_TRIM	(Useu,"USEU")
		FIELD_CHAR_TRIM	(Prfl,"PRFL")
		FIELD_CHAR_TRIM	(Type,"TYPE")
		FIELD_CHAR_TRIM	(Rema,"REMA")
		FIELD_DATE		(Hday,"HDAY")
	END_CEDARECINFO //(HOLDATA)

	// Copy the record structure
	for (int i=0; i< sizeof(HolDataRecInfo)/sizeof(HolDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&HolDataRecInfo[i],sizeof(CEDARECINFO));
		omRecInfo.Add(prpCedaRecInfo);
	} // end for

	// initialize field names
	strcpy(pcmTableName,"HOL");
	strcpy(pcmListOfFields,"URNO,CDAT,LSTU,USEC,USEU,PRFL,TYPE,REMA,HDAY");
	pcmFieldList = pcmListOfFields; //because in CedaData pcmFieldList is just a pointer

	omData.RemoveAll();
}

//---------------------------------------------------------------------------------------------------------

void CedaHolData::GetDataInfo(CStringArray &ropFields, CStringArray &ropDesription, CStringArray &ropType)
{
	ropFields.Add("URNO");
	ropFields.Add("CDAT");
	ropFields.Add("LSTU");
	ropFields.Add("USEC");
	ropFields.Add("USEU");
	ropFields.Add("PRFL");
	ropFields.Add("TYPE");
	ropFields.Add("REMA");
	ropFields.Add("HDAY");

	ropDesription.Add(LoadStg(IDS_STRING346));
	ropDesription.Add(LoadStg(IDS_STRING343));
	ropDesription.Add(LoadStg(IDS_STRING344));
	ropDesription.Add(LoadStg(IDS_STRING347));
	ropDesription.Add(LoadStg(IDS_STRING348));
	ropDesription.Add(LoadStg(IDS_STRING345));

	ropDesription.Add(LoadStg(IDS_STRING106));
	ropDesription.Add(LoadStg(IDS_STRING501));
	ropDesription.Add(LoadStg(IDS_STRING107));

	ropType.Add("String");
	ropType.Add("Date");
	ropType.Add("Date");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("Date");
}

//--REGISTER----------------------------------------------------------------------------------------------

void CedaHolData::Register(void)
{
	ogDdx.Register((void *)this,BC_HOL_CHANGE,	CString("HOLDATA"), CString("Hol-changed"),	ProcessHolCf);
	ogDdx.Register((void *)this,BC_HOL_NEW,		CString("HOLDATA"), CString("Hol-new"),		ProcessHolCf);
	ogDdx.Register((void *)this,BC_HOL_DELETE,	CString("HOLDATA"), CString("Hol-deleted"),	ProcessHolCf);
}

//--~CEDADATA----------------------------------------------------------------------------------------------

CedaHolData::~CedaHolData(void)
{
	omRecInfo.DeleteAll();
	ClearAll();
}

//--CLEAR-ALL----------------------------------------------------------------------------------------------

void CedaHolData::ClearAll(bool bpWithRegistration)
{
    omUrnoMap.RemoveAll();
    omData.DeleteAll();
	if(bpWithRegistration)
	{
		ogDdx.UnRegister(this,NOTUSED);
	}
}

//--READ-ALL-----------------------------------------------------------------------------------------------

bool CedaHolData::Read(char *pspWhere /*NULL*/)
{
    // Select data from the database
	bool ilRc = true;
    omUrnoMap.RemoveAll();
    omData.DeleteAll();
	if(pspWhere == NULL)
	{	
		ilRc = CedaAction("RT");
	}
	else
	{
		ilRc = CedaAction("RT", pspWhere);
	}
	if (ilRc != true)
	{
		return ilRc;
	}
	// Load data from CedaData into the dynamic array of record
	for (int ilLc = 0; ilRc == true; ilLc++)
	{
		HOLDATA *prlHol = new HOLDATA;
		if ((ilRc = GetFirstBufferRecord(prlHol)) == true)
		{
			omData.Add(prlHol);//Update omData
			omUrnoMap.SetAt((void *)prlHol->Urno,prlHol);
		}
		else
		{
			delete prlHol;
		}
	}
    return true;
}

//--INSERT-------------------------------------------------------------------------------------------------

bool CedaHolData::Insert(HOLDATA *prpHol)
{
	prpHol->IsChanged = DATA_NEW;
	if(Save(prpHol) == false) return false; //Update Database
	InsertInternal(prpHol);
    return true;
}

//--INSERT-INTERNAL--------------------------------------------------------------------------------------

bool CedaHolData::InsertInternal(HOLDATA *prpHol)
{
	ogDdx.DataChanged((void *)this, HOL_NEW,(void *)prpHol ); //Update Viewer
	omData.Add(prpHol);//Update omData
	omUrnoMap.SetAt((void *)prpHol->Urno,prpHol);
    return true;
}

//--DELETE-------------------------------------------------------------------------------------------------

bool CedaHolData::Delete(long lpUrno)
{
	HOLDATA *prlHol = GetHolByUrno(lpUrno);
	if (prlHol != NULL)
	{
		prlHol->IsChanged = DATA_DELETED;
		if(Save(prlHol) == false) return false; //Update Database
		DeleteInternal(prlHol);
	}
    return true;
}

//--DELETE-INTERNAL----------------------------------------------------------------------------------------

bool CedaHolData::DeleteInternal(HOLDATA *prpHol)
{
	ogDdx.DataChanged((void *)this,HOL_DELETE,(void *)prpHol); //Update Viewer
	omUrnoMap.RemoveKey((void *)prpHol->Urno);
	int ilHolCount = omData.GetSize();
	for (int ilLc = 0; ilLc < ilHolCount; ilLc++)
	{
		if (omData[ilLc].Urno == prpHol->Urno)
		{
			omData.DeleteAt(ilLc);//Update omData
			break;
		}
	}
    return true;
}

//--UPDATE-------------------------------------------------------------------------------------------------

bool CedaHolData::Update(HOLDATA *prpHol)
{
	if (GetHolByUrno(prpHol->Urno) != NULL)
	{
		if (prpHol->IsChanged == DATA_UNCHANGED)
		{
			prpHol->IsChanged = DATA_CHANGED;
		}
		if(Save(prpHol) == false) return false; //Update Database
		UpdateInternal(prpHol);
	}
    return true;
}

//--UPDATE-INTERNAL--------------------------------------------------------------------------------------------

bool CedaHolData::UpdateInternal(HOLDATA *prpHol)
{
	HOLDATA *prlHol = GetHolByUrno(prpHol->Urno);
	if (prlHol != NULL)
	{
		*prlHol = *prpHol; //Update omData
		ogDdx.DataChanged((void *)this,HOL_CHANGE,(void *)prlHol); //Update Viewer
	}
    return true;
}

//--GET-BY-URNO--------------------------------------------------------------------------------------------

HOLDATA *CedaHolData::GetHolByUrno(long lpUrno)
{
	HOLDATA  *prlHol;
	if (omUrnoMap.Lookup((void *)lpUrno,(void *& )prlHol) == TRUE)
	{
		return prlHol;
	}
	return NULL;
}

//--READSPECIALDATA-------------------------------------------------------------------------------------

bool CedaHolData::ReadSpecialData(CCSPtrArray<HOLDATA> *popHol,char *pspWhere,char *pspFieldList,bool ipSYS/*=true*/)
{
	bool ilRc = true;
	char pclFieldList[256] = " ";

	if(strlen(pspFieldList) > 0) 
	{
		strcpy(pclFieldList, pspFieldList);
	}
	if(ipSYS == true) 
	{
		if (CedaAction("SYS","HOL",pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	else
	{
		if (CedaAction("RT","HOL",pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	if(popHol != NULL)
	{
		for (int ilLc = 0; ilRc == true; ilLc++)
		{
			HOLDATA *prpHol = new HOLDATA;
			if ((ilRc = GetBufferRecord(ilLc,prpHol,CString(pclFieldList))) == true)
			{
				popHol->Add(prpHol);
			}
			else
			{
				delete prpHol;
			}
		}
		if(popHol->GetSize() == 0) return false;
	}
    return true;
}

//--SAVE---------------------------------------------------------------------------------------------------

bool CedaHolData::Save(HOLDATA *prpHol)
{
	bool ilRc = true;
	CString olListOfData;
	char pclSelection[124];
	char pclData[524];

	if (prpHol->IsChanged == DATA_UNCHANGED)
	{
		return true; // no change, nothing to do
	}
	switch(prpHol->IsChanged)
	{
	case DATA_NEW:
		MakeCedaData(&omRecInfo,olListOfData,prpHol);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("IRT","","",pclData);
		prpHol->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_CHANGED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpHol->Urno);
		MakeCedaData(&omRecInfo,olListOfData,prpHol);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("URT",pclSelection,"",pclData);
		prpHol->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_DELETED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpHol->Urno);
		ilRc = CedaAction("DRT",pclSelection);
		break;
	}
    return ilRc;
}

//--PROCESS-CF---------------------------------------------------------------------------------------------

void  ProcessHolCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	ogHolData.ProcessBc(ipDDXType,vpDataPointer,ropInstanceName);
}

//--PROCESS-BC---------------------------------------------------------------------------------------------

void  CedaHolData::ProcessBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	struct BcStruct *prlHolData;
	prlHolData = (struct BcStruct *) vpDataPointer;
	HOLDATA *prlHol;
	if(ipDDXType == BC_HOL_NEW)
	{
		prlHol = new HOLDATA;
		GetRecordFromItemList(prlHol,prlHolData->Fields,prlHolData->Data);
		InsertInternal(prlHol);
	}
	if(ipDDXType == BC_HOL_CHANGE)
	{
		long llUrno = GetUrnoFromSelection(prlHolData->Selection);
		prlHol = GetHolByUrno(llUrno);
		if(prlHol != NULL)
		{
			GetRecordFromItemList(prlHol,prlHolData->Fields,prlHolData->Data);
			UpdateInternal(prlHol);
		}
	}
	if(ipDDXType == BC_HOL_DELETE)
	{
		long llUrno;
		CString olSelection = (CString)prlHolData->Selection;
		if (olSelection.Find('\'') != -1)
		{
			llUrno = GetUrnoFromSelection(prlHolData->Selection);
		}
		else
		{
			int ilFirst = olSelection.Find("=")+2;
			int ilLast  = olSelection.GetLength();
			llUrno = atol(olSelection.Mid(ilFirst,ilLast-ilFirst));
		}
		prlHol = GetHolByUrno(llUrno);
		if (prlHol != NULL)
		{
			DeleteInternal(prlHol);
		}
	}
}

//---------------------------------------------------------------------------------------------------------
