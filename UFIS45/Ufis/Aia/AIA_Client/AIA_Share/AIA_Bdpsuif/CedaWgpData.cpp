// CedaWgpData.cpp
 
#include "stdafx.h"
#include "CedaWgpData.h"
#include "resource.h"


void ProcessWgpCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

//--CEDADATA-----------------------------------------------------------------------------------------------

CedaWgpData::CedaWgpData() : CCSCedaData(&ogCommHandler)
{
	// Create an array of CEDARECINFO for WGPDataStruct
	BEGIN_CEDARECINFO(WGPDATA,WGPDataRecInfo)
		FIELD_DATE		(Cdat,"CDAT")
		FIELD_DATE		(Lstu,"LSTU")
		FIELD_CHAR_TRIM	(Prfl,"PRFL")
		FIELD_CHAR_TRIM	(Rema,"REMA")
		FIELD_LONG		(Urno,"URNO")
		FIELD_CHAR_TRIM	(Usec,"USEC")
		FIELD_CHAR_TRIM	(Useu,"USEU")
		FIELD_CHAR_TRIM	(Wgpc,"WGPC")
		FIELD_CHAR_TRIM	(Wgpn,"WGPN")
		FIELD_LONG		(Pgpu,"PGPU")

	END_CEDARECINFO //(WGPDataStruct)

	// FIELD_LONG, FIELD_DATE 
	// Copy the record structure
	for (int i=0; i< sizeof(WGPDataRecInfo)/sizeof(WGPDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&WGPDataRecInfo[i],sizeof(CEDARECINFO));
		omRecInfo.Add(prpCedaRecInfo);
	} // end for

	// initialize field names
	strcpy(pcmTableName,"WGP");
	strcpy(pcmListOfFields,"CDAT,LSTU,PRFL,REMA,URNO,USEC,USEU,WGPC,WGPN,PGPU");
	pcmFieldList = pcmListOfFields; //because in CedaData pcmFieldList is just a pointer

	omData.RemoveAll();
}

//---------------------------------------------------------------------------------------------------------

void CedaWgpData::GetDataInfo(CStringArray &ropFields, CStringArray &ropDesription, CStringArray &ropType)
{
	ropFields.Add("CDAT");
	ropFields.Add("LSTU");
	ropFields.Add("PRFL");
	ropFields.Add("REMA");
	ropFields.Add("URNO");
	ropFields.Add("USEC");
	ropFields.Add("USEU");
	ropFields.Add("WGPC");
	ropFields.Add("WGPN");
	ropFields.Add("PGPU");

	ropDesription.Add(LoadStg(IDS_STRING343));
	ropDesription.Add(LoadStg(IDS_STRING344));
	ropDesription.Add(LoadStg(IDS_STRING345));
	ropDesription.Add(LoadStg(IDS_STRING342));
	ropDesription.Add(LoadStg(IDS_STRING346));
	ropDesription.Add(LoadStg(IDS_STRING347));
	ropDesription.Add(LoadStg(IDS_STRING348));
	ropDesription.Add(LoadStg(IDS_STRING237));
	ropDesription.Add(LoadStg(IDS_STRING229));
	ropDesription.Add(LoadStg(IDS_STRING200));

	ropType.Add("Date");
	ropType.Add("Date");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");

}

//--REGISTER----------------------------------------------------------------------------------------------

void CedaWgpData::Register(void)
{
	ogDdx.Register((void *)this,BC_WGP_CHANGE,	CString("WGPDATA"), CString("Wgp-changed"),	ProcessWgpCf);
	ogDdx.Register((void *)this,BC_WGP_NEW,		CString("WGPDATA"), CString("Wgp-new"),		ProcessWgpCf);
	ogDdx.Register((void *)this,BC_WGP_DELETE,	CString("WGPDATA"), CString("Wgp-deleted"),	ProcessWgpCf);
}

//--~CEDADATA----------------------------------------------------------------------------------------------

CedaWgpData::~CedaWgpData(void)
{
	omRecInfo.DeleteAll();
	ClearAll();
}

//--CLEAR-ALL----------------------------------------------------------------------------------------------

void CedaWgpData::ClearAll(bool bpWithRegistration)
{
    omUrnoMap.RemoveAll();
    omData.DeleteAll();
	if(bpWithRegistration)
	{
		ogDdx.UnRegister(this,NOTUSED);
	}
}

//--READ-ALL-----------------------------------------------------------------------------------------------

bool CedaWgpData::Read(char *pspWhere /*NULL*/)
{
    // Select data from the database
	bool ilRc = true;
    omUrnoMap.RemoveAll();
    omData.DeleteAll();
	if(pspWhere == NULL)
	{	
		ilRc = CedaAction("RT");
	}
	else
	{
		ilRc = CedaAction("RT", pspWhere);
	}
	if (ilRc != true)
	{
		return ilRc;
	}
	// Load data from CedaData into the dynamic array of record
	for (int ilLc = 0; ilRc == true; ilLc++)
	{
		WGPDATA *prlWgp = new WGPDATA;
		if ((ilRc = GetFirstBufferRecord(prlWgp)) == true)
		{
			omData.Add(prlWgp);//Update omData
			omUrnoMap.SetAt((void *)prlWgp->Urno,prlWgp);
		}
		else
		{
			delete prlWgp;
		}
	}
    return true;
}

//--INSERT-------------------------------------------------------------------------------------------------

bool CedaWgpData::Insert(WGPDATA *prpWgp)
{
	prpWgp->IsChanged = DATA_NEW;
	if(Save(prpWgp) == false) return false; //Update Database
	InsertInternal(prpWgp);
    return true;
}

//--INSERT-INTERNAL--------------------------------------------------------------------------------------

bool CedaWgpData::InsertInternal(WGPDATA *prpWgp)
{
	ogDdx.DataChanged((void *)this, WGP_NEW,(void *)prpWgp ); //Update Viewer
	omData.Add(prpWgp);//Update omData
	omUrnoMap.SetAt((void *)prpWgp->Urno,prpWgp);
    return true;
}

//--DELETE-------------------------------------------------------------------------------------------------

bool CedaWgpData::Delete(long lpUrno)
{
	WGPDATA *prlWgp = GetWgpByUrno(lpUrno);
	if (prlWgp != NULL)
	{
		prlWgp->IsChanged = DATA_DELETED;
		if(Save(prlWgp) == false) return false; //Update Database
		DeleteInternal(prlWgp);
	}
    return true;
}

//--DELETE-INTERNAL----------------------------------------------------------------------------------------

bool CedaWgpData::DeleteInternal(WGPDATA *prpWgp)
{
	ogDdx.DataChanged((void *)this,WGP_DELETE,(void *)prpWgp); //Update Viewer
	omUrnoMap.RemoveKey((void *)prpWgp->Urno);
	int ilWgpCount = omData.GetSize();
	for (int ilLc = 0; ilLc < ilWgpCount; ilLc++)
	{
		if (omData[ilLc].Urno == prpWgp->Urno)
		{
			omData.DeleteAt(ilLc);//Update omData
			break;
		}
	}
    return true;
}

//--UPDATE-------------------------------------------------------------------------------------------------

bool CedaWgpData::Update(WGPDATA *prpWgp)
{
	if (GetWgpByUrno(prpWgp->Urno) != NULL)
	{
		if (prpWgp->IsChanged == DATA_UNCHANGED)
		{
			prpWgp->IsChanged = DATA_CHANGED;
		}
		if(Save(prpWgp) == false) return false; //Update Database
		UpdateInternal(prpWgp);
	}
    return true;
}

//--UPDATE-INTERNAL--------------------------------------------------------------------------------------------

bool CedaWgpData::UpdateInternal(WGPDATA *prpWgp)
{
	WGPDATA *prlWgp = GetWgpByUrno(prpWgp->Urno);
	if (prlWgp != NULL)
	{
		*prlWgp = *prpWgp; //Update omData
		ogDdx.DataChanged((void *)this,WGP_CHANGE,(void *)prlWgp); //Update Viewer
	}
    return true;
}

//--GET-BY-URNO--------------------------------------------------------------------------------------------

WGPDATA *CedaWgpData::GetWgpByUrno(long lpUrno)
{
	WGPDATA  *prlWgp;
	if (omUrnoMap.Lookup((void *)lpUrno,(void *& )prlWgp) == TRUE)
	{
		return prlWgp;
	}
	return NULL;
}

//--READSPECIALDATA-------------------------------------------------------------------------------------

bool CedaWgpData::ReadSpecialData(CCSPtrArray<WGPDATA> *popWgp,char *pspWhere,char *pspFieldList,bool ipSYS/*=true*/)
{
	bool ilRc = true;
	char pclFieldList[256] = " ";

	if(strlen(pspFieldList) > 0) 
	{
		strcpy(pclFieldList, pspFieldList);
	}
	if(ipSYS == true) 
	{
		if (CedaAction("SYS","WGP",pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	else
	{
		if (CedaAction("RT","WGP",pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	if(popWgp != NULL)
	{
		for (int ilLc = 0; ilRc == true; ilLc++)
		{
			WGPDATA *prpWgp = new WGPDATA;
			if ((ilRc = GetBufferRecord(ilLc,prpWgp,CString(pclFieldList))) == true)
			{
				popWgp->Add(prpWgp);
			}
			else
			{
				delete prpWgp;
			}
		}
		if(popWgp->GetSize() == 0) return false;
	}
    return true;
}

//--SAVE---------------------------------------------------------------------------------------------------

bool CedaWgpData::Save(WGPDATA *prpWgp)
{
	bool ilRc = true;
	CString olListOfData;
	char pclSelection[124];
	char pclData[524];

	if (prpWgp->IsChanged == DATA_UNCHANGED)
	{
		return true; // no change, nothing to do
	}
	switch(prpWgp->IsChanged)
	{
	case DATA_NEW:
		MakeCedaData(&omRecInfo,olListOfData,prpWgp);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("IRT","","",pclData);
		prpWgp->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_CHANGED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpWgp->Urno);
		MakeCedaData(&omRecInfo,olListOfData,prpWgp);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("URT",pclSelection,"",pclData);
		prpWgp->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_DELETED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpWgp->Urno);
		ilRc = CedaAction("DRT",pclSelection);
		break;
	}
    return ilRc;
}

//--PROCESS-CF---------------------------------------------------------------------------------------------

void  ProcessWgpCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	ogWgpData.ProcessBc(ipDDXType,vpDataPointer,ropInstanceName);
}

//--PROCESS-BC---------------------------------------------------------------------------------------------

void  CedaWgpData::ProcessBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	struct BcStruct *prlWgpData;
	prlWgpData = (struct BcStruct *) vpDataPointer;
	WGPDATA *prlWgp;
	if(ipDDXType == BC_WGP_NEW)
	{
		prlWgp = new WGPDATA;
		GetRecordFromItemList(prlWgp,prlWgpData->Fields,prlWgpData->Data);
		InsertInternal(prlWgp);
	}
	if(ipDDXType == BC_WGP_CHANGE)
	{
		long llUrno = GetUrnoFromSelection(prlWgpData->Selection);
		prlWgp = GetWgpByUrno(llUrno);
		if(prlWgp != NULL)
		{
			GetRecordFromItemList(prlWgp,prlWgpData->Fields,prlWgpData->Data);
			UpdateInternal(prlWgp);
		}
	}
	if(ipDDXType == BC_WGP_DELETE)
	{
		long llUrno;
		CString olSelection = (CString)prlWgpData->Selection;
		if (olSelection.Find('\'') != -1)
		{
			llUrno = GetUrnoFromSelection(prlWgpData->Selection);
		}
		else
		{
			int ilFirst = olSelection.Find("=")+2;
			int ilLast  = olSelection.GetLength();
			llUrno = atol(olSelection.Mid(ilFirst,ilLast-ilFirst));
		}
		prlWgp = GetWgpByUrno(llUrno);
		if (prlWgp != NULL)
		{
			DeleteInternal(prlWgp);
		}
	}
}

//---------------------------------------------------------------------------------------------------------
