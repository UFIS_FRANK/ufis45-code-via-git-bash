// CedaGegData.h

#ifndef __CEDAGEGDATA__
#define __CEDAGEGDATA__
 
#include "stdafx.h"
#include "basicdata.h"

//---------------------------------------------------------------------------------------------------------

struct GEGDATA 
{
	CTime	 Cdat; 		// Erstellungsdatum
	char 	 Gcat[5]; 	// Kategorie
	char 	 Gcde[7]; 	// Code
	char 	 Gnam[42]; 	// Bezeichnung
	char 	 Gsnm[42]; 	// Bezeichnung Kurzname
	CTime	 Lstu; 		// Datum letzte �nderung
	char 	 Prfl[3]; 	// Protokollierungskennzeichen
	char 	 Rema[62]; 	// Bemerkung
	long 	 Urno;		// Eindeutige Datensatz-Nr.
	char 	 Usec[34]; 	// Anwender (Ersteller)
	char 	 Useu[34]; 	// Anwender (letzte �nderung)

	//DataCreated by this class
	int		 IsChanged;	// Check whether Data has Changed f�r Relaese

	GEGDATA(void)
	{ 
		memset(this,'\0',sizeof(*this));
		IsChanged	=	DATA_UNCHANGED;
		Cdat		=	TIMENULL;
		Lstu		=	TIMENULL;
	}

}; // end GegDataStrukt

//---------------------------------------------------------------------------------------------------------
// Class declaratino

class CedaGegData: public CCSCedaData
{
// Attributes
public:
    CMapPtrToPtr omUrnoMap;

    CCSPtrArray<GEGDATA> omData;

	char pcmListOfFields[2048];

// Operations
public:
    CedaGegData();
	~CedaGegData();
	virtual void GetDataInfo(CStringArray &ropFields, CStringArray &ropDesription, CStringArray &ropType);
	void Register(void);
	void ClearAll(bool bpWithRegistration = true);
    bool Read(char *pspWhere = NULL);
	bool Insert(GEGDATA *prpGeg);
	bool InsertInternal(GEGDATA *prpGeg);
	bool Update(GEGDATA *prpGeg);
	bool UpdateInternal(GEGDATA *prpGeg);
	bool Delete(long lpUrno);
	bool DeleteInternal(GEGDATA *prpGeg);
	bool ReadSpecialData(CCSPtrArray<GEGDATA> *popGeg,char *pspWhere,char *pspFieldList,bool ipSYS=true);
	bool Save(GEGDATA *prpGeg);
	void ProcessBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);
	GEGDATA  *GetGegByUrno(long lpUrno);

	// Private methods
private:
    void PrepareGegData(GEGDATA *prpGegData);

};

//---------------------------------------------------------------------------------------------------------


#endif //__CEDAGEGDATA__
