// CedaPerData.cpp
 
#include "stdafx.h"
#include "CedaPerData.h"
#include "resource.h"


void ProcessPerCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

//--CEDADATA-----------------------------------------------------------------------------------------------

CedaPerData::CedaPerData() : CCSCedaData(&ogCommHandler)
{
    BEGIN_CEDARECINFO(PERDATA, PerDataRecInfo)
		FIELD_DATE		(Cdat,"CDAT")
		FIELD_DATE		(Lstu,"LSTU")
		FIELD_CHAR_TRIM	(Prfl,"PRFL")
		FIELD_CHAR_TRIM	(Prio,"PRIO")
		FIELD_CHAR_TRIM	(Prmc,"PRMC")
		FIELD_CHAR_TRIM	(Prmn,"PRMN")
		FIELD_CHAR_TRIM	(Rema,"REMA")
		FIELD_LONG		(Urno,"URNO")
		FIELD_CHAR_TRIM	(Usec,"USEC")
		FIELD_CHAR_TRIM	(Useu,"USEU")
		//*** 19.08.99 SHA ***
		//FIELD_CHAR_TRIM	(Act3,"ACT3")
		//FIELD_CHAR_TRIM	(Act5,"ACT5")
		//FIELD_CHAR_TRIM	(Alc2,"ALC2")
		//FIELD_CHAR_TRIM	(Alc3,"ALC3")
	END_CEDARECINFO

    // Copy the record structure
    for (int i = 0; i < sizeof(PerDataRecInfo)/sizeof(PerDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&PerDataRecInfo[i],sizeof(CEDARECINFO));
        omRecInfo.Add(prpCedaRecInfo);
	}
    // Initialize table names and field names
    strcpy(pcmTableName,"PER");
    sprintf(pcmListOfFields,"CDAT,LSTU,PRFL,PRIO,PRMC,PRMN,REMA,URNO,USEC,USEU");
	//sprintf(pcmListOfFields,"CDAT,LSTU,PRFL,PRIO,PRMC,PRMN,REMA,URNO,USEC,USEU,ACT3,ACT5,ALC2,ALC3");


	pcmFieldList = pcmListOfFields; //because in CedaData pcmFieldList is just a pointer

	omData.RemoveAll();
}

//---------------------------------------------------------------------------------------------------------

void CedaPerData::GetDataInfo(CStringArray &ropFields, CStringArray &ropDesription, CStringArray &ropType)
{
	ropFields.Add("CDAT");
	ropFields.Add("LSTU");
	ropFields.Add("PRFL");
	ropFields.Add("PRIO");
	ropFields.Add("PRMC");
	ropFields.Add("PRMN");
	ropFields.Add("REMA");
	ropFields.Add("URNO");
	ropFields.Add("USEC");
	ropFields.Add("USEU");
	//*** 19.08.99 SHA ***
	//ropFields.Add("ACT3");
	//ropFields.Add("ACT5");
	//ropFields.Add("ALC2");
	//ropFields.Add("ALC3");

	ropDesription.Add(LoadStg(IDS_STRING343));
	ropDesription.Add(LoadStg(IDS_STRING344));
	ropDesription.Add(LoadStg(IDS_STRING345));
	ropDesription.Add(LoadStg(IDS_STRING637));
	ropDesription.Add(LoadStg(IDS_STRING237));
	ropDesription.Add(LoadStg(IDS_STRING229));
	ropDesription.Add(LoadStg(IDS_STRING238));
	ropDesription.Add(LoadStg(IDS_STRING346));
	ropDesription.Add(LoadStg(IDS_STRING347));
	ropDesription.Add(LoadStg(IDS_STRING348));
	//*** 19.08.99 SHA ***
	//ropDesription.Add("ACT3");
	//ropDesription.Add("ACT5");
	//ropDesription.Add("ALC2");
	//ropDesription.Add("ALC3");

	ropType.Add("Date");
	ropType.Add("Date");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	//*** 19.08.99 SHA ***
	//ropType.Add("String");
	//ropType.Add("String");
	//ropType.Add("String");
	//ropType.Add("String");


}

//--REGISTER----------------------------------------------------------------------------------------------

void CedaPerData::Register(void)
{
	ogDdx.Register((void *)this,BC_PER_CHANGE,	CString("PERDATA"), CString("Per-changed"),	ProcessPerCf);
	ogDdx.Register((void *)this,BC_PER_NEW,		CString("PERDATA"), CString("Per-new"),		ProcessPerCf);
	ogDdx.Register((void *)this,BC_PER_DELETE,	CString("PERDATA"), CString("Per-deleted"),	ProcessPerCf);
}

//--~CEDADATA----------------------------------------------------------------------------------------------

CedaPerData::~CedaPerData(void)
{
	omRecInfo.DeleteAll();
	ClearAll();
}

//--CLEAR-ALL----------------------------------------------------------------------------------------------

void CedaPerData::ClearAll(bool bpWithRegistration)
{
    omUrnoMap.RemoveAll();
    omData.DeleteAll();
	if(bpWithRegistration)
	{
		ogDdx.UnRegister(this,NOTUSED);
	}
}

//--READ-ALL-----------------------------------------------------------------------------------------------

bool CedaPerData::Read(char *pspWhere /*NULL*/)
{
    // Select data from the database
	bool ilRc = true;
    omUrnoMap.RemoveAll();
    omData.DeleteAll();
	if(pspWhere == NULL)
	{	
		ilRc = CedaAction("RT");
	}
	else
	{
		ilRc = CedaAction("RT", pspWhere);
	}
	if (ilRc != true)
	{
		return ilRc;
	}
	// Load data from CedaData into the dynamic array of record
	for (int ilLc = 0; ilRc == true; ilLc++)
	{
		PERDATA *prlPer = new PERDATA;
		if ((ilRc = GetFirstBufferRecord(prlPer)) == true)
		{
			omData.Add(prlPer);//Update omData
			omUrnoMap.SetAt((void *)prlPer->Urno,prlPer);
		}
		else
		{
			delete prlPer;
		}
	}
    return true;
}

//--INSERT-------------------------------------------------------------------------------------------------

bool CedaPerData::Insert(PERDATA *prpPer)
{
	prpPer->IsChanged = DATA_NEW;
	if(Save(prpPer) == false) return false; //Update Database
	InsertInternal(prpPer);
    return true;
}

//--INSERT-INTERNAL--------------------------------------------------------------------------------------

bool CedaPerData::InsertInternal(PERDATA *prpPer)
{
	ogDdx.DataChanged((void *)this, PER_NEW,(void *)prpPer ); //Update Viewer
	omData.Add(prpPer);//Update omData
	omUrnoMap.SetAt((void *)prpPer->Urno,prpPer);
    return true;
}

//--DELETE-------------------------------------------------------------------------------------------------

bool CedaPerData::Delete(long lpUrno)
{
	PERDATA *prlPer = GetPerByUrno(lpUrno);
	if (prlPer != NULL)
	{
		prlPer->IsChanged = DATA_DELETED;
		if(Save(prlPer) == false) return false; //Update Database
		DeleteInternal(prlPer);
	}
    return true;
}

//--DELETE-INTERNAL----------------------------------------------------------------------------------------

bool CedaPerData::DeleteInternal(PERDATA *prpPer)
{
	ogDdx.DataChanged((void *)this,PER_DELETE,(void *)prpPer); //Update Viewer
	omUrnoMap.RemoveKey((void *)prpPer->Urno);
	int ilPerCount = omData.GetSize();
	for (int ilLc = 0; ilLc < ilPerCount; ilLc++)
	{
		if (omData[ilLc].Urno == prpPer->Urno)
		{
			omData.DeleteAt(ilLc);//Update omData
			break;
		}
	}
    return true;
}

//--UPDATE-------------------------------------------------------------------------------------------------

bool CedaPerData::Update(PERDATA *prpPer)
{
	if (GetPerByUrno(prpPer->Urno) != NULL)
	{
		if (prpPer->IsChanged == DATA_UNCHANGED)
		{
			prpPer->IsChanged = DATA_CHANGED;
		}
		if(Save(prpPer) == false) return false; //Update Database
		UpdateInternal(prpPer);
	}
    return true;
}

//--UPDATE-INTERNAL--------------------------------------------------------------------------------------------

bool CedaPerData::UpdateInternal(PERDATA *prpPer)
{
	PERDATA *prlPer = GetPerByUrno(prpPer->Urno);
	if (prlPer != NULL)
	{
		*prlPer = *prpPer; //Update omData
		ogDdx.DataChanged((void *)this,PER_CHANGE,(void *)prlPer); //Update Viewer
	}
    return true;
}

//--GET-BY-URNO--------------------------------------------------------------------------------------------

PERDATA *CedaPerData::GetPerByUrno(long lpUrno)
{
	PERDATA  *prlPer;
	if (omUrnoMap.Lookup((void *)lpUrno,(void *& )prlPer) == TRUE)
	{
		return prlPer;
	}
	return NULL;
}

//--READSPECIALDATA-------------------------------------------------------------------------------------

bool CedaPerData::ReadSpecialData(CCSPtrArray<PERDATA> *popPer,char *pspWhere,char *pspFieldList,bool ipSYS/*=true*/)
{
	bool ilRc = true;
	char pclFieldList[256] = " ";

	if(strlen(pspFieldList) > 0) 
	{
		strcpy(pclFieldList, pspFieldList);
	}
	if(ipSYS == true) 
	{
		if (CedaAction("SYS","PER",pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	else
	{
		if (CedaAction("RT","PER",pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	if(popPer != NULL)
	{
		for (int ilLc = 0; ilRc == true; ilLc++)
		{
			PERDATA *prpPer = new PERDATA;
			if ((ilRc = GetBufferRecord(ilLc,prpPer,CString(pclFieldList))) == true)
			{
				popPer->Add(prpPer);
			}
			else
			{
				delete prpPer;
			}
		}
		if(popPer->GetSize() == 0) return false;
	}
    return true;
}

//--SAVE---------------------------------------------------------------------------------------------------

bool CedaPerData::Save(PERDATA *prpPer)
{
	bool ilRc = true;
	CString olListOfData;
	char pclSelection[124];
	char pclData[524];

	if (prpPer->IsChanged == DATA_UNCHANGED)
	{
		return true; // no change, nothing to do
	}
	switch(prpPer->IsChanged)
	{
	case DATA_NEW:
		MakeCedaData(&omRecInfo,olListOfData,prpPer);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("IRT","","",pclData);
		prpPer->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_CHANGED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpPer->Urno);
		MakeCedaData(&omRecInfo,olListOfData,prpPer);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("URT",pclSelection,"",pclData);
		prpPer->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_DELETED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpPer->Urno);
		ilRc = CedaAction("DRT",pclSelection);
		break;
	}
    return ilRc;
}

//--PROCESS-CF---------------------------------------------------------------------------------------------

void  ProcessPerCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	ogPerData.ProcessBc(ipDDXType,vpDataPointer,ropInstanceName);
}

//--PROCESS-BC---------------------------------------------------------------------------------------------

void  CedaPerData::ProcessBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	struct BcStruct *prlPerData;
	prlPerData = (struct BcStruct *) vpDataPointer;
	PERDATA *prlPer;
	if(ipDDXType == BC_PER_NEW)
	{
		prlPer = new PERDATA;
		GetRecordFromItemList(prlPer,prlPerData->Fields,prlPerData->Data);
		InsertInternal(prlPer);
	}
	if(ipDDXType == BC_PER_CHANGE)
	{
		long llUrno = GetUrnoFromSelection(prlPerData->Selection);
		prlPer = GetPerByUrno(llUrno);
		if(prlPer != NULL)
		{
			GetRecordFromItemList(prlPer,prlPerData->Fields,prlPerData->Data);
			UpdateInternal(prlPer);
		}
	}
	if(ipDDXType == BC_PER_DELETE)
	{
		long llUrno;
		CString olSelection = (CString)prlPerData->Selection;
		if (olSelection.Find('\'') != -1)
		{
			llUrno = GetUrnoFromSelection(prlPerData->Selection);
		}
		else
		{
			int ilFirst = olSelection.Find("=")+2;
			int ilLast  = olSelection.GetLength();
			llUrno = atol(olSelection.Mid(ilFirst,ilLast-ilFirst));
		}
		prlPer = GetPerByUrno(llUrno);
		if (prlPer != NULL)
		{
			DeleteInternal(prlPer);
		}
	}
}

//---------------------------------------------------------------------------------------------------------
