// FunktionenDlg.cpp : implementation file
//

#include "stdafx.h"
#include "bdpsuif.h"
#include "FunktionenDlg.h"
#include "PrivList.h"
#include "AwDlg.h"
#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// FunktionenDlg dialog


FunktionenDlg::FunktionenDlg(PFCDATA *popPfc, CWnd* pParent /*=NULL*/)
	: CDialog(FunktionenDlg::IDD, pParent)
{
	pomPfc = popPfc;
	//{{AFX_DATA_INIT(FunktionenDlg)
	//}}AFX_DATA_INIT
}


void FunktionenDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(FunktionenDlg)
	DDX_Control(pDX, IDOK, m_OK);
	DDX_Control(pDX, IDC_CDAT_D, m_CDATD);
	DDX_Control(pDX, IDC_CDAT_T, m_CDATT);
	DDX_Control(pDX, IDC_DPTC, m_DPTC);
	DDX_Control(pDX, IDC_FCTC, m_FCTC);
	DDX_Control(pDX, IDC_FCTN, m_FCTN);
	DDX_Control(pDX, IDC_LSTU_D, m_LSTUD);
	DDX_Control(pDX, IDC_LSTU_T, m_LSTUT);
	DDX_Control(pDX, IDC_PRIO, m_PRIO);
	DDX_Control(pDX, IDC_REMA, m_REMA);
	DDX_Control(pDX, IDC_USEU, m_USEU);
	DDX_Control(pDX, IDC_USEC, m_USEC);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(FunktionenDlg, CDialog)
	//{{AFX_MSG_MAP(FunktionenDlg)
	ON_BN_CLICKED(IDC_B_ORG_AW, OnBOrgAw)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// FunktionenDlg message handlers
//----------------------------------------------------------------------------------------

BOOL FunktionenDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

	m_Caption = LoadStg(IDS_STRING177) + " <" + m_Caption + ">";
	SetWindowText(m_Caption);
	
	char clStat = ogPrivList.GetStat("FUNKTIONENDLG.m_OK");
	SetWndStatAll(clStat,m_OK);

	//------------------------------------
	m_CDATD.SetBKColor(SILVER);
	m_CDATD.SetInitText(pomPfc->Cdat.Format("%d.%m.%Y"));
	m_CDATD.SetSecState(ogPrivList.GetStat("FUNKTIONENDLG.m_CDAT"));
	// - - - - - - - - - - - - - - - - - -
	m_CDATT.SetBKColor(SILVER);
	m_CDATT.SetInitText(pomPfc->Cdat.Format("%H:%M"));
	m_CDATT.SetSecState(ogPrivList.GetStat("FUNKTIONENDLG.m_CDAT"));
	//------------------------------------
	m_LSTUD.SetBKColor(SILVER);
	m_LSTUD.SetInitText(pomPfc->Lstu.Format("%d.%m.%Y"));
	m_LSTUD.SetSecState(ogPrivList.GetStat("FUNKTIONENDLG.m_LSTU"));
	// - - - - - - - - - - - - - - - - - -
	m_LSTUT.SetBKColor(SILVER);
	m_LSTUT.SetInitText(pomPfc->Lstu.Format("%H:%M"));
	m_LSTUT.SetSecState(ogPrivList.GetStat("FUNKTIONENDLG.m_LSTU"));
	//------------------------------------
	m_USEC.SetBKColor(SILVER);
	m_USEC.SetInitText(pomPfc->Usec);
	m_USEC.SetSecState(ogPrivList.GetStat("FUNKTIONENDLG.m_USEC"));
	//------------------------------------
	m_USEU.SetBKColor(SILVER);
	m_USEU.SetInitText(pomPfc->Useu);
	m_USEU.SetSecState(ogPrivList.GetStat("FUNKTIONENDLG.m_USEU"));
	//------------------------------------//WHITE,YELLOW,SILVER,RED,LYELLOW
	m_DPTC.SetFormat("x|#x|#x|#x|#x|#");
	m_DPTC.SetTextLimit(1,5);
	m_DPTC.SetBKColor(YELLOW);  
	m_DPTC.SetTextErrColor(RED);
	m_DPTC.SetInitText(pomPfc->Dptc);
	m_DPTC.SetSecState(ogPrivList.GetStat("FUNKTIONENDLG.m_DPTC"));
	//------------------------------------
	m_FCTC.SetFormat("x|#x|#x|#x|#x|#");
	m_FCTC.SetTextLimit(1,5);
	m_FCTC.SetBKColor(YELLOW);  
	m_FCTC.SetTextErrColor(RED);
	m_FCTC.SetInitText(pomPfc->Fctc);
	m_FCTC.SetSecState(ogPrivList.GetStat("FUNKTIONENDLG.m_FCTC"));
	//------------------------------------
	m_FCTN.SetTypeToString("X(40)",40,1);
	m_FCTN.SetBKColor(YELLOW);  
	m_FCTN.SetTextErrColor(RED);
	m_FCTN.SetInitText(pomPfc->Fctn);
	m_FCTN.SetSecState(ogPrivList.GetStat("FUNKTIONENDLG.m_FCTN"));
	//------------------------------------
	m_PRIO.SetTypeToString("X(6)",6,1);
	m_PRIO.SetBKColor(YELLOW);  
	m_PRIO.SetTextErrColor(RED);
	m_PRIO.SetInitText(pomPfc->Prio);
	m_PRIO.SetSecState(ogPrivList.GetStat("FUNKTIONENDLG.m_PRIO"));
	//------------------------------------
	m_REMA.SetTypeToString("X(60)",60,0);
	m_REMA.SetTextErrColor(RED);
	m_REMA.SetInitText(pomPfc->Rema);
	m_REMA.SetSecState(ogPrivList.GetStat("FUNKTIONENDLG.m_REMA"));
	//------------------------------------
	return TRUE;
}

//----------------------------------------------------------------------------------------

void FunktionenDlg::OnOK() 
{
	AfxGetApp()->DoWaitCursor(1);
	CString olErrorText;
	bool ilStatus = true;
	if(m_FCTC.GetStatus() == false)
	{
		ilStatus = false;
		if(m_FCTC.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING237) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING237) + ogNotFormat;
		}
	}
	if(m_DPTC.GetStatus() == false)
	{
		ilStatus = false;
		if(m_DPTC.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING41) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING41) + ogNotFormat;
		}
	}
	if(m_FCTN.GetStatus() == false)
	{
		ilStatus = false;
		if(m_FCTN.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING229) + ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING229) + ogNotFormat;
		}
	}
	if(m_PRIO.GetStatus() == false)
	{
		ilStatus = false;
		if(m_PRIO.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING229) + ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING229) + ogNotFormat;
		}
	}
	if(m_REMA.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING238) + ogNotFormat;
	}

	///////////////////////////////////////////////////////////////////
	//Konsistenz-Check//
	if(ilStatus == true)
	{
		CString olDptc;//
		CString olFctc;//
		char clWhere[100];
		CCSPtrArray<ORGDATA> olOrgCPA;
		CCSPtrArray<PFCDATA> olPfcCPA;

		if(m_DPTC.GetWindowTextLength() != 0)
		{
			m_DPTC.GetWindowText(olDptc);
			sprintf(clWhere,"WHERE DPT1='%s'",olDptc);
			if(ogOrgData.ReadSpecialData(&olOrgCPA,clWhere,"URNO",false) == false)
			{
				ilStatus = false;
				olErrorText += LoadStg(IDS_STRING42);
			}
			olOrgCPA.DeleteAll();
		}
		if(m_FCTC.GetWindowTextLength() != 0)
		{
			m_FCTC.GetWindowText(olFctc);
			sprintf(clWhere,"WHERE FCTC='%s'",olFctc);
			if(ogPfcData.ReadSpecialData(&olPfcCPA,clWhere,"URNO",false) == true)
			{
				if(olPfcCPA[0].Urno != pomPfc->Urno)
				{
					ilStatus = false;
					olErrorText += LoadStg(IDS_STRING237) + LoadStg(IDS_EXIST);
				}
			}
			olPfcCPA.DeleteAll();
		}
	}
	///////////////////////////////////////////////////////////////////
	AfxGetApp()->DoWaitCursor(-1);
	if (ilStatus == true)
	{
		m_DPTC.GetWindowText(pomPfc->Dptc,6);
		m_FCTC.GetWindowText(pomPfc->Fctc,6);
		m_FCTN.GetWindowText(pomPfc->Fctn,41);
		m_PRIO.GetWindowText(pomPfc->Prio,4);

		//wandelt Return in Blank//
		CString olTemp;
		m_REMA.GetWindowText(olTemp);
		for(int i=0;i>-1;)
		{
			i = olTemp.Find("\r\n");
			if(i != -1)
			{
				if(olTemp.GetLength() > (i+2))
					olTemp = olTemp.Left(i) + " " + olTemp.Mid(i+2);
				else
					olTemp = olTemp.Left(i);
			}
		}
		strcpy(pomPfc->Rema,olTemp);
		////////////////////////////

		CDialog::OnOK();
	}
	else
	{
		Beep(440,70);
		MessageBox(olErrorText,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
		m_FCTC.SetFocus();
	}	
}

//----------------------------------------------------------------------------------------

void FunktionenDlg::OnBOrgAw() 
{
	CStringArray olCol1, olCol2, olCol3;
	CCSPtrArray<ORGDATA> olList;
	if(ogOrgData.ReadSpecialData(&olList, "", "URNO,DPT1,DPTN", false) == true)
	{
		for(int i = 0; i < olList.GetSize(); i++)
		{
			char pclUrno[20]="";
			sprintf(pclUrno, "%ld", olList[i].Urno);
			olCol3.Add(CString(pclUrno));
			olCol1.Add(CString(olList[i].Dpt1));
			olCol2.Add(CString(olList[i].Dptn));
		}
		AwDlg *pomDlg = new AwDlg(&olCol1, &olCol2, &olCol3, LoadStg(IDS_STRING237), LoadStg(IDS_STRING229), this);
		if(pomDlg->DoModal() == IDOK)
		{
			m_DPTC.SetWindowText(pomDlg->omReturnString);
			m_DPTC.SetFocus();
		}
		delete pomDlg;
	}
	else
	{
		MessageBox(LoadStg(IDS_STRING321), LoadStg(IDS_STRING149), (MB_ICONEXCLAMATION|MB_OK));
	}
	olList.DeleteAll();
}

//----------------------------------------------------------------------------------------
