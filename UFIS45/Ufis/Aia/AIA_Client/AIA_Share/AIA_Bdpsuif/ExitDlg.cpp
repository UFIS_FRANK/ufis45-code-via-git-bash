// ExitDlg.cpp : implementation file
//

#include "stdafx.h"
#include "BDPSUIF.h"
#include "ExitDlg.h"
#include "PrivList.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// ExitDlg dialog


ExitDlg::ExitDlg(EXTDATA *popEXT,CWnd* pParent /*=NULL*/) : CDialog(ExitDlg::IDD, pParent)
{
	pomEXT = popEXT;
	
	//{{AFX_DATA_INIT(ExitDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void ExitDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(ExitDlg)
	DDX_Control(pDX, IDOK, m_OK);
	DDX_Control(pDX, IDC_CDAT_D, m_CDATD);
	DDX_Control(pDX, IDC_CDAT_T, m_CDATT);
	DDX_Control(pDX, IDC_LSTU_D, m_LSTUD);
	DDX_Control(pDX, IDC_LSTU_T, m_LSTUT);
	DDX_Control(pDX, IDC_NAFR_D, m_NAFRD);
	DDX_Control(pDX, IDC_NAFR_T, m_NAFRT);
	DDX_Control(pDX, IDC_NATO_D, m_NATOD);
	DDX_Control(pDX, IDC_NATO_T, m_NATOT);
	DDX_Control(pDX, IDC_USEU,	 m_USEU);
	DDX_Control(pDX, IDC_USEC,	 m_USEC);
	DDX_Control(pDX, IDC_RESN,	 m_RESN);
	DDX_Control(pDX, IDC_ENAM,	 m_ENAM);
	DDX_Control(pDX, IDC_TELE,	 m_TELE);
	DDX_Control(pDX, IDC_VAFR_D, m_VAFRD);
	DDX_Control(pDX, IDC_VAFR_T, m_VAFRT);
	DDX_Control(pDX, IDC_VATO_D, m_VATOD);
	DDX_Control(pDX, IDC_VATO_T, m_VATOT);
	DDX_Control(pDX, IDC_GRUP,	 m_GRUP);
	DDX_Control(pDX, IDC_TERM,	 m_TERM);
	DDX_Control(pDX, IDC_HOME,	 m_HOME);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(ExitDlg, CDialog)
	//{{AFX_MSG_MAP(ExitDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// ExitDlg message handlers
//----------------------------------------------------------------------------------------

BOOL ExitDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	m_Caption = LoadStg(IDS_STRING163) + " <" + m_Caption + ">";
	SetWindowText(m_Caption);

	char clStat = ogPrivList.GetStat("EXITDLG.m_OK");
	SetWndStatAll(clStat,m_OK);

	//------------------------------------
	m_CDATD.SetBKColor(SILVER);
	m_CDATD.SetInitText(pomEXT->Cdat.Format("%d.%m.%Y"));
	m_CDATD.SetSecState(ogPrivList.GetStat("EXITDLG.m_CDAT"));
	m_CDATT.SetBKColor(SILVER);
	m_CDATT.SetInitText(pomEXT->Cdat.Format("%H:%M"));
	m_CDATT.SetSecState(ogPrivList.GetStat("EXITDLG.m_CDAT"));
	//------------------------------------
	m_LSTUD.SetBKColor(SILVER);
	m_LSTUD.SetInitText(pomEXT->Lstu.Format("%d.%m.%Y"));
	m_LSTUD.SetSecState(ogPrivList.GetStat("EXITDLG.m_LSTU"));
	m_LSTUT.SetBKColor(SILVER);
	m_LSTUT.SetInitText(pomEXT->Lstu.Format("%H:%M"));
	m_LSTUT.SetSecState(ogPrivList.GetStat("EXITDLG.m_LSTU"));
	//------------------------------------
	m_USEC.SetBKColor(SILVER);
	m_USEC.SetInitText(pomEXT->Usec);
	m_USEC.SetSecState(ogPrivList.GetStat("EXITDLG.m_USEC"));
	//------------------------------------
	m_USEU.SetBKColor(SILVER);
	m_USEU.SetInitText(pomEXT->Useu);
	m_USEU.SetSecState(ogPrivList.GetStat("EXITDLG.m_USEU"));
	//------------------------------------
	m_GRUP.SetBKColor(SILVER);
	//m_GRUP.SetInitText(pomEXT->);
	m_GRUP.SetSecState(ogPrivList.GetStat("EXITDLG.m_GRUP"));
	//------------------------------------//WHITE,YELLOW,SILVER,RED,LYELLOW
	m_ENAM.SetFormat("x|#x|#x|#x|#x|#");
	m_ENAM.SetTextLimit(1,5);
	m_ENAM.SetBKColor(YELLOW);
	m_ENAM.SetTextErrColor(RED);
	m_ENAM.SetInitText(pomEXT->Enam);
	m_ENAM.SetSecState(ogPrivList.GetStat("EXITDLG.m_ENAM"));
	//------------------------------------
	m_NAFRD.SetTypeToDate();
	m_NAFRD.SetTextErrColor(RED);
	m_NAFRD.SetInitText(pomEXT->Nafr.Format("%d.%m.%Y"));
	m_NAFRD.SetSecState(ogPrivList.GetStat("EXITDLG.m_NAFR"));
	// - - - - - - - - - - - - - - - - - -
	m_NAFRT.SetTypeToTime();
	m_NAFRT.SetTextErrColor(RED);
	m_NAFRT.SetInitText(pomEXT->Nafr.Format("%H:%M"));
	m_NAFRT.SetSecState(ogPrivList.GetStat("EXITDLG.m_NAFR"));
	//------------------------------------
	m_NATOD.SetTypeToDate();
	m_NATOD.SetTextErrColor(RED);
	m_NATOD.SetInitText(pomEXT->Nato.Format("%d.%m.%Y"));
	m_NATOD.SetSecState(ogPrivList.GetStat("EXITDLG.m_NATO"));
	// - - - - - - - - - - - - - - - - - -
	m_NATOT.SetTypeToTime();
	m_NATOT.SetTextErrColor(RED);
	m_NATOT.SetInitText(pomEXT->Nato.Format("%H:%M"));
	m_NATOT.SetSecState(ogPrivList.GetStat("EXITDLG.m_NATO"));
	//------------------------------------
	m_RESN.SetTypeToString("X(40)",40,0);
	m_RESN.SetTextErrColor(RED);
	m_RESN.SetInitText(pomEXT->Resn);
	m_RESN.SetSecState(ogPrivList.GetStat("EXITDLG.m_RESN"));
	//------------------------------------
	m_TERM.SetFormat("x|#");
	m_TERM.SetTextLimit(0,1);
	m_TERM.SetTextErrColor(RED);
	m_TERM.SetInitText(pomEXT->Term);
	m_TERM.SetSecState(ogPrivList.GetStat("EXITDLG.m_TERM"));
	//------------------------------------
	m_TELE.SetTypeToString("X(10)",10,0);
	m_TELE.SetTextErrColor(RED);
	m_TELE.SetInitText(pomEXT->Tele);
	m_TELE.SetSecState(ogPrivList.GetStat("EXITDLG.m_TELE"));
	//------------------------------------
	m_HOME.SetTypeToString("X(4)",4,1);
	m_HOME.SetTextErrColor(RED);
	m_HOME.SetInitText(pomEXT->Home);
	m_HOME.SetSecState(ogPrivList.GetStat("EXITDLG.m_HOME"));
	//------------------------------------
	m_VAFRD.SetTypeToDate(true);
	m_VAFRD.SetTextErrColor(RED);
	m_VAFRD.SetBKColor(YELLOW);
	m_VAFRD.SetInitText(pomEXT->Vafr.Format("%d.%m.%Y"));
	m_VAFRD.SetSecState(ogPrivList.GetStat("EXITDLG.m_VAFR"));
	// - - - - - - - - - - - - - - - - - -
	m_VAFRT.SetTypeToTime(true);
	m_VAFRT.SetTextErrColor(RED);
	m_VAFRT.SetBKColor(YELLOW);
	m_VAFRT.SetInitText(pomEXT->Vafr.Format("%H:%M"));
	m_VAFRT.SetSecState(ogPrivList.GetStat("EXITDLG.m_VAFR"));
	//------------------------------------
	m_VATOD.SetTypeToDate();
	m_VATOD.SetTextErrColor(RED);
	m_VATOD.SetInitText(pomEXT->Vato.Format("%d.%m.%Y"));
	m_VATOD.SetSecState(ogPrivList.GetStat("EXITDLG.m_VATO"));
	// - - - - - - - - - - - - - - - - - -
	m_VATOT.SetTypeToTime();
	m_VATOT.SetTextErrColor(RED);
	m_VATOT.SetInitText(pomEXT->Vato.Format("%H:%M"));
	m_VATOT.SetSecState(ogPrivList.GetStat("EXITDLG.m_VATO"));
	//------------------------------------
	return TRUE;
}

//----------------------------------------------------------------------------------------

void ExitDlg::OnOK() 
{
	AfxGetApp()->DoWaitCursor(1);
	CString olErrorText;
	bool ilStatus = true;
	if(m_ENAM.GetStatus() == false)
	{
		ilStatus = false;
		if(m_ENAM.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING288) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING288) + ogNotFormat;
		}
	}
	if(m_NAFRD.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING364) + LoadStg(IDS_STRING232) + ogNotFormat;
	}
	if(m_NAFRT.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING364) + LoadStg(IDS_STRING233) + ogNotFormat;
	}
	if(m_NATOD.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING365) + LoadStg(IDS_STRING232) + ogNotFormat;
	}
	if(m_NATOT.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING365) + LoadStg(IDS_STRING233) + ogNotFormat;
	}
	if(m_RESN.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING366) + ogNotFormat;
	}
	if(m_TERM.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING296) + ogNotFormat;
	}
	if(m_TELE.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING294) + ogNotFormat;
	}
	if(m_VAFRD.GetStatus() == false)
	{
		ilStatus = false;
		if(m_VAFRD.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING230) + LoadStg(IDS_STRING232) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING230) + LoadStg(IDS_STRING232) + ogNotFormat;
		}
	}
	if(m_VAFRT.GetStatus() == false)
	{
		ilStatus = false;
		if(m_VAFRT.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING230) + LoadStg(IDS_STRING233) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING230) + LoadStg(IDS_STRING233) + ogNotFormat;
		}
	}
	if(m_VATOD.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING231) + LoadStg(IDS_STRING232) + ogNotFormat;
	}
	if(m_VATOT.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING231) + LoadStg(IDS_STRING233) + ogNotFormat;
	}

	///////////////////////////////////////////////////////////////////////////
	CString olNafrd,olNafrt,olNatod,olNatot,olVafrd,olVafrt,olVatod,olVatot;
	CTime olTmpTimeFr,olTmpTimeTo;
	m_NAFRD.GetWindowText(olNafrd);
	m_NAFRT.GetWindowText(olNafrt);
	m_NATOD.GetWindowText(olNatod);
	m_NATOT.GetWindowText(olNatot);
	m_VAFRD.GetWindowText(olVafrd);
	m_VAFRT.GetWindowText(olVafrt);
	m_VATOD.GetWindowText(olVatod);
	m_VATOT.GetWindowText(olVatot);

	if((m_VAFRD.GetWindowTextLength() != 0 && m_VAFRT.GetWindowTextLength() == 0) || (m_VAFRD.GetWindowTextLength() == 0 && m_VAFRT.GetWindowTextLength() != 0))
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING234);
	}
	if((m_VATOD.GetWindowTextLength() != 0 && m_VATOT.GetWindowTextLength() == 0) || (m_VATOD.GetWindowTextLength() == 0 && m_VATOT.GetWindowTextLength() != 0))
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING235);
	}
	if(m_VAFRD.GetStatus() == true && m_VAFRT.GetStatus() == true && m_VATOD.GetStatus() == true && m_VATOT.GetStatus() == true)
	{
		olTmpTimeFr = DateHourStringToDate(olVafrd,olVafrt);
		olTmpTimeTo = DateHourStringToDate(olVatod,olVatot);
		if(olTmpTimeTo != -1 && (olTmpTimeTo < olTmpTimeFr || olTmpTimeFr == -1))
		{
			ilStatus = false;
			olErrorText += LoadStg(IDS_STRING236);
		}
	}

	if((m_NAFRD.GetWindowTextLength() != 0 && m_NAFRT.GetWindowTextLength() == 0) || (m_NAFRD.GetWindowTextLength() == 0 && m_NAFRT.GetWindowTextLength() != 0))
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING291);
	}
	if((m_NATOD.GetWindowTextLength() != 0 && m_NATOT.GetWindowTextLength() == 0) || (m_NATOD.GetWindowTextLength() == 0 && m_NATOT.GetWindowTextLength() != 0))
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING292);
	}
	if(m_NAFRD.GetStatus() == true && m_NAFRT.GetStatus() == true && m_NATOD.GetStatus() == true && m_NATOT.GetStatus() == true)
	{
		olTmpTimeFr = DateHourStringToDate(olNafrd,olNafrt);
		olTmpTimeTo = DateHourStringToDate(olNatod,olNatot);
		if(olTmpTimeTo != -1 && (olTmpTimeTo < olTmpTimeFr || olTmpTimeFr == -1))
		{
			ilStatus = false;
			olErrorText += LoadStg(IDS_STRING293);
		}
	}

	//Konsistenz-Check//
	if(ilStatus == true)
	{
		CString olEnam;
		char clWhere[100];
		CCSPtrArray<EXTDATA> olExtCPA;

		m_ENAM.GetWindowText(olEnam);
		sprintf(clWhere,"WHERE ENAM='%s'",olEnam);
		if(ogEXTData.ReadSpecialData(&olExtCPA,clWhere,"URNO,ENAM",false) == true)
		{
			if(olExtCPA[0].Urno != pomEXT->Urno)
			{
				ilStatus = false;
				olErrorText += LoadStg(IDS_STRING288) + LoadStg(IDS_EXIST);
			}
		}
		olExtCPA.DeleteAll();
	}

	///////////////////////////////////////////////////////////////////////////
	AfxGetApp()->DoWaitCursor(-1);
	if (ilStatus == true)
	{
		m_ENAM.GetWindowText(pomEXT->Enam,6);
		pomEXT->Nafr = DateHourStringToDate(olNafrd,olNafrt);
		pomEXT->Nato = DateHourStringToDate(olNatod,olNatot);
		m_RESN.GetWindowText(pomEXT->Resn,41);
		m_TERM.GetWindowText(pomEXT->Term,2);
		m_TELE.GetWindowText(pomEXT->Tele,10);
		m_HOME.GetWindowText(pomEXT->Home,3);
		pomEXT->Vafr = DateHourStringToDate(olVafrd,olVafrt);
		pomEXT->Vato = DateHourStringToDate(olVatod,olVatot);
		CDialog::OnOK();
	}
	else
	{
		Beep(440,70);
		MessageBox(olErrorText,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
		m_ENAM.SetFocus();
		m_ENAM.SetSel(0,-1);
	}
}

//----------------------------------------------------------------------------------------

