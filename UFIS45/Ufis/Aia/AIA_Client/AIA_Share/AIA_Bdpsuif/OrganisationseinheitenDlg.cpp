// OrganisationseinheitenDlg.cpp : implementation file
//

#include "stdafx.h"
#include "BDPSUIF.h"
#include "OrganisationseinheitenDlg.h"
#include "PrivList.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// OrganisationseinheitenDlg dialog


OrganisationseinheitenDlg::OrganisationseinheitenDlg(ORGDATA *popOrg,CWnd* pParent) : CDialog(OrganisationseinheitenDlg::IDD, pParent)
{
	pomOrg = popOrg;

	//{{AFX_DATA_INIT(OrganisationseinheitenDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void OrganisationseinheitenDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(OrganisationseinheitenDlg)
	DDX_Control(pDX, IDOK,		 m_OK);
	DDX_Control(pDX, IDC_USEU,	 m_USEU);
	DDX_Control(pDX, IDC_USEC,	 m_USEC);
	DDX_Control(pDX, IDC_CDAT_D, m_CDATD);
	DDX_Control(pDX, IDC_CDAT_T, m_CDATT);
	DDX_Control(pDX, IDC_LSTU_D, m_LSTUD);
	DDX_Control(pDX, IDC_LSTU_T, m_LSTUT);
	DDX_Control(pDX, IDC_GRUP,	 m_GRUP);
	DDX_Control(pDX, IDC_DPT1,	 m_DPT1);
	DDX_Control(pDX, IDC_DPT2,	 m_DPT2);
	DDX_Control(pDX, IDC_DPTN,	 m_DPTN);
	DDX_Control(pDX, IDC_REMA,	 m_REMA);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(OrganisationseinheitenDlg, CDialog)
	//{{AFX_MSG_MAP(OrganisationseinheitenDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// OrganisationseinheitenDlg message handlers
//----------------------------------------------------------------------------------------

BOOL OrganisationseinheitenDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	m_Caption = LoadStg(IDS_STRING184) + " <" + m_Caption + ">";
	SetWindowText(m_Caption);
	
	char clStat = ogPrivList.GetStat("ORGANISATIONSEINHEITENDLG.m_OK");
	SetWndStatAll(clStat,m_OK);

	//------------------------------------
	m_CDATD.SetBKColor(SILVER);
	m_CDATD.SetInitText(pomOrg->Cdat.Format("%d.%m.%Y"));
	m_CDATD.SetSecState(ogPrivList.GetStat("ORGANISATIONSEINHEITENDLG.m_CDAT"));
	// - - - - - - - - - - - - - - - - - -
	m_CDATT.SetBKColor(SILVER);
	m_CDATT.SetInitText(pomOrg->Cdat.Format("%H:%M"));
	m_CDATT.SetSecState(ogPrivList.GetStat("ORGANISATIONSEINHEITENDLG.m_CDAT"));
	//------------------------------------
	m_LSTUD.SetBKColor(SILVER);
	m_LSTUD.SetInitText(pomOrg->Lstu.Format("%d.%m.%Y"));
	m_LSTUD.SetSecState(ogPrivList.GetStat("ORGANISATIONSEINHEITENDLG.m_LSTU"));
	// - - - - - - - - - - - - - - - - - -
	m_LSTUT.SetBKColor(SILVER);
	m_LSTUT.SetInitText(pomOrg->Lstu.Format("%H:%M"));
	m_LSTUT.SetSecState(ogPrivList.GetStat("ORGANISATIONSEINHEITENDLG.m_LSTU"));
	//------------------------------------
	m_USEC.SetBKColor(SILVER);
	m_USEC.SetInitText(pomOrg->Usec);
	m_USEC.SetSecState(ogPrivList.GetStat("ORGANISATIONSEINHEITENDLG.m_USEC"));
	//------------------------------------
	m_USEU.SetBKColor(SILVER);
	m_USEU.SetInitText(pomOrg->Useu);
	m_USEU.SetSecState(ogPrivList.GetStat("ORGANISATIONSEINHEITENDLG.m_USEU"));
	//------------------------------------
	m_GRUP.SetBKColor(SILVER);
	//m_GRUP.SetInitText(pomGAT->);
	m_GRUP.SetSecState(ogPrivList.GetStat("ORGANISATIONSEINHEITENDLG.m_GRUP"));
	//------------------------------------//WHITE,YELLOW,SILVER,RED,LYELLOW
	m_DPT1.SetFormat("X(5)");
	m_DPT1.SetTextLimit(1,5);
	m_DPT1.SetBKColor(YELLOW);  
	m_DPT1.SetTextErrColor(RED);
	m_DPT1.SetInitText(pomOrg->Dpt1);
	m_DPT1.SetSecState(ogPrivList.GetStat("ORGANISATIONSEINHEITENDLG.m_DPT1"));
	//------------------------------------
	m_DPT2.SetFormat("X(5)");
	m_DPT2.SetTextLimit(0,5);
	m_DPT2.SetTextErrColor(RED);
	m_DPT2.SetInitText(pomOrg->Dpt2);
	m_DPT2.SetSecState(ogPrivList.GetStat("ORGANISATIONSEINHEITENDLG.m_DPT2"));
	//------------------------------------
	m_DPTN.SetTypeToString("X(40)",40,1);
	m_DPTN.SetBKColor(YELLOW);  
	m_DPTN.SetTextErrColor(RED);
	m_DPTN.SetInitText(pomOrg->Dptn);
	m_DPTN.SetSecState(ogPrivList.GetStat("ORGANISATIONSEINHEITENDLG.m_DPTN"));
	//------------------------------------
	m_REMA.SetTypeToString("X(60)",60,0);
	m_REMA.SetTextErrColor(RED);
	m_REMA.SetInitText(pomOrg->Rema);
	m_REMA.SetSecState(ogPrivList.GetStat("ORGANISATIONSEINHEITENDLG.m_REMA"));
	//------------------------------------
	return TRUE; 
}

//----------------------------------------------------------------------------------------

void OrganisationseinheitenDlg::OnOK() 
{
	AfxGetApp()->DoWaitCursor(1);
	CString olErrorText;
	bool ilStatus = true;
	if(m_DPT1.GetStatus() == false)
	{
		ilStatus = false;
		if(m_DPT1.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING237) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING237) + ogNotFormat;
		}
	}
	if(m_DPT2.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING92) + ogNotFormat;
	}
	if(m_DPTN.GetStatus() == false)
	{
		ilStatus = false;
		if(m_DPTN.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING229) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING229) + ogNotFormat;
		}
	}
	if(m_REMA.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING238) + ogNotFormat;
	}

	///////////////////////////////////////////////////////////////////
	//Konsistenz-Check//
	if(ilStatus == true)
	{
		CString olDpt1,olDpt2;
		char clWhere[100];

		CCSPtrArray<ORGDATA> olOrgCPA;
		m_DPT1.GetWindowText(olDpt1);
		sprintf(clWhere,"WHERE DPT1='%s'",olDpt1);
		if(ogOrgData.ReadSpecialData(&olOrgCPA,clWhere,"URNO,DPT1",false) == true)
		{
			if(olOrgCPA[0].Urno != pomOrg->Urno)
			{
				ilStatus = false;
				olErrorText += LoadStg(IDS_STRING237) + LoadStg(IDS_EXIST);
			}
		}
		olOrgCPA.DeleteAll();
	}
	
	///////////////////////////////////////////////////////////////////
	AfxGetApp()->DoWaitCursor(-1);
	if (ilStatus == true)
	{
		m_DPT1.GetWindowText(pomOrg->Dpt1,6);
		m_DPT2.GetWindowText(pomOrg->Dpt2,6);
		m_DPTN.GetWindowText(pomOrg->Dptn,41);
	
		//wandelt Return in Blank//
		CString olTemp;
		m_REMA.GetWindowText(olTemp);
		for(int i=0;i>-1;)
		{
			i = olTemp.Find("\r\n");
			if(i != -1)
			{
				if(olTemp.GetLength() > (i+2))
					olTemp = olTemp.Left(i) + " " + olTemp.Mid(i+2);
				else
					olTemp = olTemp.Left(i);
			}
		}
		strcpy(pomOrg->Rema,olTemp);
		////////////////////////////

		CDialog::OnOK();
	}
	else
	{
		Beep(440,70);
		MessageBox(olErrorText,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
		m_DPT1.SetFocus();
		m_DPT1.SetSel(0,-1);
  }
}
//----------------------------------------------------------------------------------------
