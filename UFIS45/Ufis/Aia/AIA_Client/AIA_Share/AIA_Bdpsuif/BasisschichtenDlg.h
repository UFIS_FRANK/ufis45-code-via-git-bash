#if !defined(AFX_BASISSCHICHTENDLG_H__546136B1_7519_11D1_B430_0000B45A33F5__INCLUDED_)
#define AFX_BASISSCHICHTENDLG_H__546136B1_7519_11D1_B430_0000B45A33F5__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// BasisschichtenDlg.h : header file
//

#include "CedaSphData.h"
#include "CCSEdit.h"
#include "CedaAsfData.h"
#include "CedaCotData.h"
#include "CedaBsdData.h"
/////////////////////////////////////////////////////////////////////////////
// BasisschichtenDlg dialog

class BasisschichtenDlg : public CDialog
{
// Construction
public:
	BasisschichtenDlg(BSDDATA *popBsd, bool bpIsDynamic = true, CWnd* pParent = NULL);   // standard constructor

	BSDDATA *pomBsd;
	bool bmIsDynamic;

	CString omOldCode;
	bool bmChangeAction;
	CStatic *pomStatus;
// Dialog Data
	//{{AFX_DATA(BasisschichtenDlg)
	enum { IDD = IDD_BASISSCHICHTENDLG };
	CButton	m_BKR1_S;
	CButton	m_BKR1_A;
	CButton	m_OK;
	CButton m_DYN;
	CButton m_STAT;
	CButton m_EXTRA;
	CCSEdit	m_BEWC;
	CCSEdit	m_BEWC_ALL;
	CCSEdit	m_BKD1;
	CCSEdit	m_BKF1;
	CCSEdit	m_BKT1;
	CCSEdit	m_BSDC;
	CCSEdit	m_BSDK;
	CCSEdit	m_BSDN;
	CCSEdit	m_BSDS;
	CCSEdit	m_CDATD;
	CCSEdit	m_CDATT;
	CCSEdit	m_CTRC1;
	CCSEdit	m_ESBG;
	CCSEdit	m_LSEN;
	CCSEdit	m_LSTUD;
	CCSEdit	m_LSTUT;
	CCSEdit	m_REMA;
	CCSEdit	m_SDU1;
	CCSEdit	m_SEX1;
	CCSEdit	m_SSH1;
	CCSEdit	m_USEC;
	CCSEdit	m_USEU;
	CString m_Caption;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(BasisschichtenDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(BasisschichtenDlg)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
    afx_msg LONG OnKillfocus(UINT wParam, LONG lParam);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_BASISSCHICHTENDLG_H__546136B1_7519_11D1_B430_0000B45A33F5__INCLUDED_)
