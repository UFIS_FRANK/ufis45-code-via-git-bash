// LFZRegiExt.cpp : implementation file
//

#include "stdafx.h"
#include "bdpsuif.h"
#include "LFZRegiExt.h"
#include "ccsglobl.h"

//~~~~~ 10.08.99 SHA : 
#include "PrivList.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CLFZRegiExt dialog


CLFZRegiExt::CLFZRegiExt(CWnd* pParent /*=NULL*/)
	: CDialog(CLFZRegiExt::IDD, pParent)
{
	//{{AFX_DATA_INIT(CLFZRegiExt)
	m_RegnExt = _T("");
	m_Cnam = _T("");
	m_Type = _T("");
	m_Seri = _T("");
	m_Frst = _T("");
	m_Conf = _T("");
	m_Powd = _T("");
	m_Nois = _T("");
	m_Rest = _T("");
	m_Efis = _T("");
	m_Enty = _T("");
	m_Stxt = _T("");
	m_Exrg = _T("");
	m_Scod = _T("");
	m_Year = _T("");
	m_Pnum = _T("");
	m_Gall = _T("");
	m_Enna = _T("");
	m_Fuse = _T("");
	m_Icao = _T("");
	m_Cont = _T("");
	m_Iata = _T("");
	m_Deli = _T("");
	m_Lsdf = _T("");
	m_Lcod = _T("");
	m_Odat = _T("");
	m_Lsdb = FALSE;
	m_Lsfb = FALSE;
	m_Lstb = FALSE;
	m_Oope = FALSE;
	m_Oopt = FALSE;
	m_Oord = FALSE;
	m_Opbb = FALSE;
	m_Opbf = FALSE;
	m_Opwb = FALSE;
	m_Wobo = FALSE;
	m_Wfub = FALSE;
	m_Regb = FALSE;
	m_Cvtd = FALSE;
	m_Strd = FALSE;
	m_Ladp_d = _T("");
	m_Ladp_t = _T("");
	//}}AFX_DATA_INIT
}


void CLFZRegiExt::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CLFZRegiExt)
	DDX_Control(pDX, IDC_LADP_T, m_cLadp_t);
	DDX_Control(pDX, IDC_LADP_D, m_cLadp_d);
	DDX_Control(pDX, IDC_LSDB, m_cLsdb);
	DDX_Control(pDX, IDC_ODAT, m_cOdat);
	DDX_Control(pDX, IDC_LCOD, m_cLcod);
	DDX_Control(pDX, IDC_LSDF, m_cLsdf);
	DDX_Control(pDX, IDC_DELI, m_cDeli);
	DDX_Control(pDX, IDC_IATA, m_cIata);
	DDX_Control(pDX, IDC_CONT, m_cCont);
	DDX_Control(pDX, IDC_ICAO, m_cIcao);
	DDX_Control(pDX, IDC_FUSE, m_cFuse);
	DDX_Control(pDX, IDC_ENNA, m_cEnna);
	DDX_Control(pDX, IDC_GALL, m_cGall);
	DDX_Control(pDX, IDC_PNUM, m_cPnum);
	DDX_Control(pDX, IDC_YEAR, m_cYear);
	DDX_Control(pDX, IDC_SCOD, m_cScod);
	DDX_Control(pDX, IDC_EXRG, m_cExrg);
	DDX_Control(pDX, IDC_STXT, m_cStxt);
	DDX_Control(pDX, IDC_ENTY, m_cEnty);
	DDX_Control(pDX, IDC_EFIS, m_cEfis);
	DDX_Control(pDX, IDC_REST, m_cRest);
	DDX_Control(pDX, IDC_NOIS, m_cNois);
	DDX_Control(pDX, IDC_POWD, m_cPowd);
	DDX_Control(pDX, IDC_CONF, m_cConf);
	DDX_Control(pDX, IDC_FRST, m_cFrst);
	DDX_Control(pDX, IDC_SERI, m_cSeri);
	DDX_Control(pDX, IDC_TYPE, m_cType);
	DDX_Control(pDX, IDC_CNAM, m_cCnam);
	DDX_Control(pDX, IDC_REGN_EXT, m_cRegnExt);
	DDX_Text(pDX, IDC_REGN_EXT, m_RegnExt);
	DDX_Text(pDX, IDC_CNAM, m_Cnam);
	DDX_Text(pDX, IDC_TYPE, m_Type);
	DDX_Text(pDX, IDC_SERI, m_Seri);
	DDX_Text(pDX, IDC_FRST, m_Frst);
	DDX_Text(pDX, IDC_CONF, m_Conf);
	DDX_Text(pDX, IDC_POWD, m_Powd);
	DDX_Text(pDX, IDC_NOIS, m_Nois);
	DDX_Text(pDX, IDC_REST, m_Rest);
	DDX_Text(pDX, IDC_EFIS, m_Efis);
	DDX_Text(pDX, IDC_ENTY, m_Enty);
	DDX_Text(pDX, IDC_STXT, m_Stxt);
	DDX_Text(pDX, IDC_EXRG, m_Exrg);
	DDX_Text(pDX, IDC_SCOD, m_Scod);
	DDX_Text(pDX, IDC_YEAR, m_Year);
	DDX_Text(pDX, IDC_PNUM, m_Pnum);
	DDX_Text(pDX, IDC_GALL, m_Gall);
	DDX_Text(pDX, IDC_ENNA, m_Enna);
	DDX_Text(pDX, IDC_FUSE, m_Fuse);
	DDX_Text(pDX, IDC_ICAO, m_Icao);
	DDX_Text(pDX, IDC_CONT, m_Cont);
	DDX_Text(pDX, IDC_IATA, m_Iata);
	DDX_Text(pDX, IDC_DELI, m_Deli);
	DDX_Text(pDX, IDC_LSDF, m_Lsdf);
	DDX_Text(pDX, IDC_LCOD, m_Lcod);
	DDX_Text(pDX, IDC_ODAT, m_Odat);
	DDX_Check(pDX, IDC_LSDB, m_Lsdb);
	DDX_Check(pDX, IDC_LSFB, m_Lsfb);
	DDX_Check(pDX, IDC_LSTB, m_Lstb);
	DDX_Check(pDX, IDC_OOPE, m_Oope);
	DDX_Check(pDX, IDC_OOPT, m_Oopt);
	DDX_Check(pDX, IDC_OORD, m_Oord);
	DDX_Check(pDX, IDC_OPBB, m_Opbb);
	DDX_Check(pDX, IDC_OPBF, m_Opbf);
	DDX_Check(pDX, IDC_OPWB, m_Opwb);
	DDX_Check(pDX, IDC_WOBO, m_Wobo);
	DDX_Check(pDX, IDC_WFUB, m_Wfub);
	DDX_Check(pDX, IDC_REGB, m_Regb);
	DDX_Check(pDX, IDC_CVTD, m_Cvtd);
	DDX_Check(pDX, IDC_STRD, m_Strd);
	DDX_Text(pDX, IDC_LADP_D, m_Ladp_d);
	DDX_Text(pDX, IDC_LADP_T, m_Ladp_t);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CLFZRegiExt, CDialog)
	//{{AFX_MSG_MAP(CLFZRegiExt)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CLFZRegiExt message handlers

BOOL CLFZRegiExt::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here


	m_cRegnExt.SetBKColor(SILVER);
	m_cRegnExt.SetSecState(ogPrivList.GetStat("LFZREGISTRATIONDLG.m_ACTI"));

	m_cCnam.SetTypeToString("X(40)",40,0);
	m_cCnam.SetTextErrColor(RED);
	m_cCnam.SetSecState(ogPrivList.GetStat("LFZREGISTRATIONDLG.m_CNAM"));

	m_cType.SetTypeToString("X(40)",40,0);
	m_cType.SetTextErrColor(RED);
	m_cType.SetSecState(ogPrivList.GetStat("LFZREGISTRATIONDLG.m_TYPE"));

	m_cSeri.SetTypeToString("X(16)",16,0);
	m_cSeri.SetTextErrColor(RED);
	m_cSeri.SetSecState(ogPrivList.GetStat("LFZREGISTRATIONDLG.m_SERI"));

	m_cFrst.SetTypeToString("X(3)",3,0);
	m_cFrst.SetTextErrColor(RED);
	m_cFrst.SetSecState(ogPrivList.GetStat("LFZREGISTRATIONDLG.m_FRST"));

	m_cConf.SetTypeToString("X(16)",16,0);
	m_cConf.SetTextErrColor(RED);
	m_cConf.SetSecState(ogPrivList.GetStat("LFZREGISTRATIONDLG.m_CONF"));

	m_cPowd.SetTypeToString("X(3)",3,0);
	m_cPowd.SetTextErrColor(RED);
	m_cPowd.SetSecState(ogPrivList.GetStat("LFZREGISTRATIONDLG.m_POWD"));

	m_cNois.SetTypeToString("X(1)",1,0);
	m_cNois.SetTextErrColor(RED);
	m_cNois.SetSecState(ogPrivList.GetStat("LFZREGISTRATIONDLG.m_NOIS"));

	m_cRest.SetTypeToString("X(3)",3,0);
	m_cRest.SetTextErrColor(RED);
	m_cRest.SetSecState(ogPrivList.GetStat("LFZREGISTRATIONDLG.m_REST"));

	m_cExrg.SetTypeToString("X(11)",11,0);
	m_cExrg.SetTextErrColor(RED);
	m_cExrg.SetSecState(ogPrivList.GetStat("LFZREGISTRATIONDLG.m_EXRG"));

	m_cScod.SetTypeToString("X(10)",10,0);
	m_cScod.SetTextErrColor(RED);
	m_cScod.SetSecState(ogPrivList.GetStat("LFZREGISTRATIONDLG.m_SCOD"));

	m_cYear.SetTypeToString("X(4)",4,0);
	m_cYear.SetTextErrColor(RED);
	m_cYear.SetSecState(ogPrivList.GetStat("LFZREGISTRATIONDLG.m_YEAR"));

	m_cPnum.SetTypeToString("X(10)",10,0);
	m_cPnum.SetTextErrColor(RED);
	m_cPnum.SetSecState(ogPrivList.GetStat("LFZREGISTRATIONDLG.m_PNUM"));

	m_cGall.SetTypeToString("X(32)",32,0);
	m_cGall.SetTextErrColor(RED);
	m_cGall.SetSecState(ogPrivList.GetStat("LFZREGISTRATIONDLG.m_GALL"));

	m_cEfis.SetTypeToString("X(3)",3,0);
	m_cEfis.SetTextErrColor(RED);
	m_cEfis.SetSecState(ogPrivList.GetStat("LFZREGISTRATIONDLG.m_EFIS"));

	m_cStxt.SetTypeToString("X(20)",20,0);
	m_cStxt.SetTextErrColor(RED);
	m_cStxt.SetSecState(ogPrivList.GetStat("LFZREGISTRATIONDLG.m_STXT"));

	m_cEnna.SetTypeToString("X(10)",10,0);
	m_cEnna.SetTextErrColor(RED);
	m_cEnna.SetSecState(ogPrivList.GetStat("LFZREGISTRATIONDLG.m_ENNA"));

	m_cFuse.SetTypeToString("X(5)",10,0);
	m_cFuse.SetTextErrColor(RED);
	m_cFuse.SetSecState(ogPrivList.GetStat("LFZREGISTRATIONDLG.m_FUSE"));

	m_cLadp_d.SetTypeToDate();
	m_cLadp_d.SetTextErrColor(RED);
	m_cLadp_d.SetSecState(ogPrivList.GetStat("LFZREGISTRATIONDLG.m_LADP"));

	m_cLadp_t.SetTypeToTime();
	m_cLadp_t.SetTextErrColor(RED);
	m_cLadp_t.SetSecState(ogPrivList.GetStat("LFZREGISTRATIONDLG.m_LADP"));

	m_cLcod.SetTypeToString("X(4)",4,0);
	m_cLcod.SetTextErrColor(RED);
	m_cLcod.SetSecState(ogPrivList.GetStat("LFZREGISTRATIONDLG.m_LCOD"));

	m_cLsdf.SetTypeToString("X(20)",20,0);
	m_cLsdf.SetTextErrColor(RED);
	m_cLsdf.SetSecState(ogPrivList.GetStat("LFZREGISTRATIONDLG.m_LSDF"));

	m_cOdat.SetTypeToString("X(4)",4,0);
	m_cOdat.SetTextErrColor(RED);
	m_cOdat.SetSecState(ogPrivList.GetStat("LFZREGISTRATIONDLG.m_ODAT"));

	m_cIcao.SetTypeToString("X(4)",4,0);
	m_cIcao.SetTextErrColor(RED);
	m_cIcao.SetSecState(ogPrivList.GetStat("LFZREGISTRATIONDLG.m_ICAO"));

	m_cCont.SetTypeToString("X(1)",1,0);
	m_cCont.SetTextErrColor(RED);
	m_cCont.SetSecState(ogPrivList.GetStat("LFZREGISTRATIONDLG.m_CONT"));

	m_cIata.SetTypeToString("X(4)",4,0);
	m_cIata.SetTextErrColor(RED);
	m_cIata.SetSecState(ogPrivList.GetStat("LFZREGISTRATIONDLG.m_IATA"));

	m_cDeli.SetTypeToString("X(4)",4,0);
	m_cDeli.SetTextErrColor(RED);
	m_cDeli.SetSecState(ogPrivList.GetStat("LFZREGISTRATIONDLG.m_DELI"));

	//*** 07.09.99 SHA ***
	//*** SWITCH TO EDITABLE ***
	//m_cEnty.SetBKColor(SILVER);
	m_cEnty.SetTypeToString("X(5)",5,0);
	m_cEnty.SetTextErrColor(RED);
	//m_cEnty.SetSecState(ogPrivList.GetStat("LFZREGISTRATIONDLG.m_ENTY"));

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}
