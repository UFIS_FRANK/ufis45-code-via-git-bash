// CedaSorData.h

#ifndef __CEDAPSORDATA__
#define __CEDAPSORDATA__
 
#include "stdafx.h"
#include "CCSCedaData.h"
#include "basicdata.h"
#include <afxdisp.h>
//---------------------------------------------------------------------------------------------------------

struct SORDATA 
{
	long Urno;			// Eindeutige Datensatz-Nr.
	long Surn;			// Referenz auf STF.URNO
	char Code[7];		// Codereferenz ORG.CODE
	COleDateTime Vpfr;	// G�ltig von
	COleDateTime Vpto;	// G�ltig bis

	//DataCreated by this class
	int      IsChanged;

	//long, CTime
	SORDATA(void)
	{ 
		memset(this,'\0',sizeof(*this));
		//Vpfr=COleDateTime(COleDateTime((time_t)-1)),Vpto=COleDateTime(COleDateTime((time_t)-1));; //<zB.(FIELD_DATE Felder)
		IsChanged = DATA_UNCHANGED;
	}

}; // end SORDataStruct

//---------------------------------------------------------------------------------------------------------
// Class declaratino

class CedaSorData: public CCSCedaData
{
// Attributes
public:
    CMapPtrToPtr omUrnoMap;

    CCSPtrArray<SORDATA> omData;

	char pcmListOfFields[2048];

// OSorations
public:
    CedaSorData();
	~CedaSorData();
	virtual void GetDataInfo(CStringArray &ropFields, CStringArray &ropDesription, CStringArray &ropType);
	void Register(void);
	void ClearAll(bool bpWithRegistration = true);
    bool Read(char *pspWhere = NULL);
	bool Insert(SORDATA *prpSor);
	bool InsertInternal(SORDATA *prpSor);
	bool Update(SORDATA *prpSor);
	bool UpdateInternal(SORDATA *prpSor);
	bool Delete(long lpUrno);
	bool DeleteInternal(SORDATA *prpSor);
	bool ReadSpecialData(CCSPtrArray<SORDATA> *popSor,char *pspWhere,char *pspFieldList,bool ipSYS=true);
	bool Save(SORDATA *prpSor);
	void ProcessBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);
	SORDATA  *GetSorByUrno(long lpUrno);


	// Private methods
private:
    void PrepareSorData(SORDATA *prpSorData);

};

//---------------------------------------------------------------------------------------------------------


#endif //__CEDAPSORDATA__
