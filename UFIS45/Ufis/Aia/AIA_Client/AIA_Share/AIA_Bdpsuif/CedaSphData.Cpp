// CedaSphData.cpp
 
#include "stdafx.h"
#include "CedaSphData.h"
#include "resource.h"
#include "resrc1.h"


void ProcessSphCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

//--CEDADATA-----------------------------------------------------------------------------------------------

CedaSphData::CedaSphData() : CCSCedaData(&ogCommHandler)
{
    BEGIN_CEDARECINFO(SPHDATA, SphDataRecInfo)
		FIELD_CHAR_TRIM	(Act3,"ACT3")
		FIELD_LONG		(Actm,"ACTM")
		FIELD_LONG		(Alcm,"ALCM")
		FIELD_CHAR_TRIM	(Apc3,"APC3")
		FIELD_LONG		(Apcm,"APCM")
		FIELD_CHAR_TRIM	(Arde,"ARDE")
		FIELD_DATE		(Cdat,"CDAT")
		FIELD_CHAR_TRIM	(Days,"DAYS")
		FIELD_CHAR_TRIM	(Flnc,"FLNC")
		FIELD_CHAR_TRIM	(Flnn,"FLNN")
		FIELD_CHAR_TRIM	(Flns,"FLNS")
		FIELD_DATE		(Lstu,"LSTU")
		FIELD_CHAR_TRIM	(Prfl,"PRFL")
		FIELD_CHAR_TRIM	(Regn,"REGN")
		FIELD_CHAR_TRIM	(Styp,"STYP")
		FIELD_CHAR_TRIM	(Ttyp,"TTYP")
		FIELD_LONG		(Urno,"URNO")
		FIELD_CHAR_TRIM	(Usec,"USEC")
		FIELD_CHAR_TRIM	(Useu,"USEU")
		FIELD_CHAR_TRIM	(Eart,"EART")
	END_CEDARECINFO

    // Copy the record structure
    for (int i = 0; i < sizeof(SphDataRecInfo)/sizeof(SphDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&SphDataRecInfo[i],sizeof(CEDARECINFO));
        omRecInfo.Add(prpCedaRecInfo);
	}
    // Initialize table names and field names
    strcpy(pcmTableName,"SPH");
    sprintf(pcmListOfFields,"ACT3,ACTM,ALCM,APC3,APCM,ARDE,CDAT,DAYS,FLNC,FLNN,FLNS,LSTU,PRFL,REGN,STYP,TTYP,URNO,USEC,USEU,EART");

	pcmFieldList = pcmListOfFields; //because in CedaData pcmFieldList is just a pointer

	omData.RemoveAll();
}

//---------------------------------------------------------------------------------------------------------

void CedaSphData::GetDataInfo(CStringArray &ropFields, CStringArray &ropDesription, CStringArray &ropType)
{
	ropFields.Add("ACT3");
	ropFields.Add("ACTM");
	ropFields.Add("ALCM");
	ropFields.Add("APC3");
	ropFields.Add("APCM");
	ropFields.Add("ARDE");
	ropFields.Add("CDAT");
	ropFields.Add("DAYS");
	ropFields.Add("FLNC");
	ropFields.Add("FLNN");
	ropFields.Add("FLNS");
	ropFields.Add("LSTU");
	ropFields.Add("PRFL");
	ropFields.Add("REGN");
	ropFields.Add("STYP");
	ropFields.Add("TTYP");
	ropFields.Add("URNO");
	ropFields.Add("USEC");
	ropFields.Add("USEU");
	ropFields.Add("EART");

	ropDesription.Add(LoadStg(IDS_STRING398));
	ropDesription.Add(LoadStg(IDS_STRING22));
	ropDesription.Add(LoadStg(IDS_STRING19));
	ropDesription.Add(LoadStg(IDS_STRING23));
	ropDesription.Add(LoadStg(IDS_STRING24));
	ropDesription.Add(LoadStg(IDS_STRING20));
	ropDesription.Add(LoadStg(IDS_STRING343));
	ropDesription.Add(LoadStg(IDS_STRING483));
	ropDesription.Add(LoadStg(IDS_STRING16));
	ropDesription.Add(LoadStg(IDS_STRING17));
	ropDesription.Add(LoadStg(IDS_STRING18));
	ropDesription.Add(LoadStg(IDS_STRING344));
	ropDesription.Add(LoadStg(IDS_STRING345));
	ropDesription.Add(LoadStg(IDS_STRING182));
	ropDesription.Add(LoadStg(IDS_STRING25));
	ropDesription.Add(LoadStg(IDS_STRING484));
	ropDesription.Add(LoadStg(IDS_STRING346));
	ropDesription.Add(LoadStg(IDS_STRING347));
	ropDesription.Add(LoadStg(IDS_STRING348));
	ropDesription.Add(LoadStg(IDS_STRING1024));

	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("Date");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("Date");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");

}

//--REGISTER----------------------------------------------------------------------------------------------

void CedaSphData::Register(void)
{
	ogDdx.Register((void *)this,BC_SPH_CHANGE,	CString("SPHDATA"), CString("Sph-changed"),	ProcessSphCf);
	ogDdx.Register((void *)this,BC_SPH_NEW,		CString("SPHDATA"), CString("Sph-new"),		ProcessSphCf);
	ogDdx.Register((void *)this,BC_SPH_DELETE,	CString("SPHDATA"), CString("Sph-deleted"),	ProcessSphCf);
}

//--~CEDADATA----------------------------------------------------------------------------------------------

CedaSphData::~CedaSphData(void)
{
	omRecInfo.DeleteAll();
	ClearAll();
}

//--CLEAR-ALL----------------------------------------------------------------------------------------------

void CedaSphData::ClearAll(bool bpWithRegistration)
{
    omUrnoMap.RemoveAll();
    omData.DeleteAll();
	if(bpWithRegistration)
	{
		ogDdx.UnRegister(this,NOTUSED);
	}
}

//--READ-ALL-----------------------------------------------------------------------------------------------

bool CedaSphData::Read(char *pspWhere /*NULL*/)
{
    // Select data from the database
	bool ilRc = true;
    omUrnoMap.RemoveAll();
    omData.DeleteAll();
	if(pspWhere == NULL)
	{	
		ilRc = CedaAction("RT");
	}
	else
	{
		ilRc = CedaAction("RT", pspWhere);
	}
	if (ilRc != true)
	{
		return ilRc;
	}
	// Load data from CedaData into the dynamic array of record
	for (int ilLc = 0; ilRc == true; ilLc++)
	{
		SPHDATA *prlSph = new SPHDATA;
		if ((ilRc = GetFirstBufferRecord(prlSph)) == true)
		{
			omData.Add(prlSph);//Update omData
			omUrnoMap.SetAt((void *)prlSph->Urno,prlSph);
		}
		else
		{
			delete prlSph;
		}
	}
    return true;
}

//--INSERT-------------------------------------------------------------------------------------------------

bool CedaSphData::Insert(SPHDATA *prpSph)
{
	prpSph->IsChanged = DATA_NEW;
	if(Save(prpSph) == false) return false; //Update Database
	InsertInternal(prpSph);
    return true;
}

//--INSERT-INTERNAL--------------------------------------------------------------------------------------

bool CedaSphData::InsertInternal(SPHDATA *prpSph)
{
	ogDdx.DataChanged((void *)this, SPH_NEW,(void *)prpSph ); //Update Viewer
	omData.Add(prpSph);//Update omData
	omUrnoMap.SetAt((void *)prpSph->Urno,prpSph);
    return true;
}

//--DELETE-------------------------------------------------------------------------------------------------

bool CedaSphData::Delete(long lpUrno)
{
	SPHDATA *prlSph = GetSphByUrno(lpUrno);
	if (prlSph != NULL)
	{
		prlSph->IsChanged = DATA_DELETED;
		if(Save(prlSph) == false) return false; //Update Database
		DeleteInternal(prlSph);
	}
    return true;
}

//--DELETE-INTERNAL----------------------------------------------------------------------------------------

bool CedaSphData::DeleteInternal(SPHDATA *prpSph)
{
	ogDdx.DataChanged((void *)this,SPH_DELETE,(void *)prpSph); //Update Viewer
	omUrnoMap.RemoveKey((void *)prpSph->Urno);
	int ilSphCount = omData.GetSize();
	for (int ilLc = 0; ilLc < ilSphCount; ilLc++)
	{
		if (omData[ilLc].Urno == prpSph->Urno)
		{
			omData.DeleteAt(ilLc);//Update omData
			break;
		}
	}
    return true;
}

//--UPDATE-------------------------------------------------------------------------------------------------

bool CedaSphData::Update(SPHDATA *prpSph)
{
	if (GetSphByUrno(prpSph->Urno) != NULL)
	{
		if (prpSph->IsChanged == DATA_UNCHANGED)
		{
			prpSph->IsChanged = DATA_CHANGED;
		}
		if(Save(prpSph) == false) return false; //Update Database
		UpdateInternal(prpSph);
	}
    return true;
}

//--UPDATE-INTERNAL--------------------------------------------------------------------------------------------

bool CedaSphData::UpdateInternal(SPHDATA *prpSph)
{
	SPHDATA *prlSph = GetSphByUrno(prpSph->Urno);
	if (prlSph != NULL)
	{
		*prlSph = *prpSph; //Update omData
		ogDdx.DataChanged((void *)this,SPH_CHANGE,(void *)prlSph); //Update Viewer
	}
    return true;
}

//--GET-BY-URNO--------------------------------------------------------------------------------------------

SPHDATA *CedaSphData::GetSphByUrno(long lpUrno)
{
	SPHDATA  *prlSph;
	if (omUrnoMap.Lookup((void *)lpUrno,(void *& )prlSph) == TRUE)
	{
		return prlSph;
	}
	return NULL;
}

//--READSPECIALDATA-------------------------------------------------------------------------------------

bool CedaSphData::ReadSpecialData(CCSPtrArray<SPHDATA> *popSph,char *pspWhere,char *pspFieldList,bool ipSYS/*=true*/)
{
	bool ilRc = true;
	char pclFieldList[256] = " ";

	if(strlen(pspFieldList) > 0) 
	{
		strcpy(pclFieldList, pspFieldList);
	}
	if(ipSYS == true) 
	{
		if (CedaAction("SYS","SPH",pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	else
	{
		if (CedaAction("RT","SPH",pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	if(popSph != NULL)
	{
		for (int ilLc = 0; ilRc == true; ilLc++)
		{
			SPHDATA *prpSph = new SPHDATA;
			if ((ilRc = GetBufferRecord(ilLc,prpSph,CString(pclFieldList))) == true)
			{
				popSph->Add(prpSph);
			}
			else
			{
				delete prpSph;
			}
		}
		if(popSph->GetSize() == 0) return false;
	}
    return true;
}

//--SAVE---------------------------------------------------------------------------------------------------

bool CedaSphData::Save(SPHDATA *prpSph)
{
	bool ilRc = true;
	CString olListOfData;
	char pclSelection[124];
	char pclData[524];

	if (prpSph->IsChanged == DATA_UNCHANGED)
	{
		return true; // no change, nothing to do
	}
	switch(prpSph->IsChanged)
	{
	case DATA_NEW:
		MakeCedaData(&omRecInfo,olListOfData,prpSph);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("IRT","","",pclData);
		prpSph->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_CHANGED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpSph->Urno);
		MakeCedaData(&omRecInfo,olListOfData,prpSph);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("URT",pclSelection,"",pclData);
		prpSph->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_DELETED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpSph->Urno);
		ilRc = CedaAction("DRT",pclSelection);
		break;
	}
    return ilRc;
}

//--PROCESS-CF---------------------------------------------------------------------------------------------

void  ProcessSphCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	ogSphData.ProcessBc(ipDDXType,vpDataPointer,ropInstanceName);
}

//--PROCESS-BC---------------------------------------------------------------------------------------------

void  CedaSphData::ProcessBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	struct BcStruct *prlSphData;
	prlSphData = (struct BcStruct *) vpDataPointer;
	SPHDATA *prlSph;
	if(ipDDXType == BC_SPH_NEW)
	{
		prlSph = new SPHDATA;
		GetRecordFromItemList(prlSph,prlSphData->Fields,prlSphData->Data);
		InsertInternal(prlSph);
	}
	if(ipDDXType == BC_SPH_CHANGE)
	{
		long llUrno = GetUrnoFromSelection(prlSphData->Selection);
		prlSph = GetSphByUrno(llUrno);
		if(prlSph != NULL)
		{
			GetRecordFromItemList(prlSph,prlSphData->Fields,prlSphData->Data);
			UpdateInternal(prlSph);
		}
	}
	if(ipDDXType == BC_SPH_DELETE)
	{
		long llUrno;
		CString olSelection = (CString)prlSphData->Selection;
		if (olSelection.Find('\'') != -1)
		{
			llUrno = GetUrnoFromSelection(prlSphData->Selection);
		}
		else
		{
			int ilFirst = olSelection.Find("=")+2;
			int ilLast  = olSelection.GetLength();
			llUrno = atol(olSelection.Mid(ilFirst,ilLast-ilFirst));
		}
		prlSph = GetSphByUrno(llUrno);
		if (prlSph != NULL)
		{
			DeleteInternal(prlSph);
		}
	}
}

//---------------------------------------------------------------------------------------------------------
