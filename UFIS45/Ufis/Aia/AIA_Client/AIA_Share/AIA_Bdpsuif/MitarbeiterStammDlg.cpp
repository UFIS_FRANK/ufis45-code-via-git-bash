//*** LAST CHANGE OF FILE ***
//*** 01.11.99 SHA ***
//*** 02.11.99 SHA ***

// MitarbeiterStammDlg.cpp : implementation file
//

#include "stdafx.h"
#include "bdpsuif.h"
#include "MitarbeiterStammDlg.h"
#include "PrivList.h"
#include "AwDlg.h"
//*** 27.08.99 SHA ***
#include "CedaActData.h"
#include "CedaAltData.h"
//*** 01.11.99 SHA ***
#include "CedaSPFData.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// MitarbeiterStammDlg dialog
//----------------------------------------------------------------------------------------

MitarbeiterStammDlg::MitarbeiterStammDlg(STFDATA *popStf, 
										 CCSPtrArray<SORDATA> *popSorData,
										 CCSPtrArray<SPFDATA> *popSpfData,
										 CCSPtrArray<SPEDATA> *popSpeData,
										 CCSPtrArray<SCODATA> *popScoData,
										 CCSPtrArray<STEDATA> *popSteData,
										 CCSPtrArray<SWGDATA> *popSwgData,
										 CWnd* pParent /*=NULL*/)
	: CDialog(MitarbeiterStammDlg::IDD, pParent)
{

	pomSorData = popSorData;
	pomSpfData = popSpfData;
	pomSpeData = popSpeData;
	pomScoData = popScoData;
	pomSteData = popSteData;
	pomSwgData = popSwgData;
	
	pomStf = popStf;

	pomOrgTable = new CCSTable;
	pomPfcTable = new CCSTable;
	pomPerTable = new CCSTable;
	pomCotTable = new CCSTable;
	pomTeaTable = new CCSTable;
	pomWgpTable = new CCSTable;

	//{{AFX_DATA_INIT(MitarbeiterStammDlg)
	//}}AFX_DATA_INIT
}

void MitarbeiterStammDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(MitarbeiterStammDlg)
	DDX_Control(pDX, IDC_REMARK, m_REMA);
	DDX_Control(pDX, IDC_HELP_GRP, m_HELP_GRP);
	DDX_Control(pDX, IDOK, m_OK);
	DDX_Control(pDX, IDC_TOHR, m_THOR);
	DDX_Control(pDX, IDC_HELP_TEA, m_HELP_TEA);
	DDX_Control(pDX, IDC_HELP_PFC, m_HELP_PFC);
	DDX_Control(pDX, IDC_HELP_PER, m_HELP_PER);
	DDX_Control(pDX, IDC_HELP_ORG, m_HELP_ORG);
	DDX_Control(pDX, IDC_HELP_COT, m_HELP_COT);
	DDX_Control(pDX, IDC_CDAT_D2, m_CDATD);
	DDX_Control(pDX, IDC_CDAT_T2, m_CDATT);
	DDX_Control(pDX, IDC_DODM_D, m_DODDM_D);
	DDX_Control(pDX, IDC_DODM_T, m_DDODM_T);
	DDX_Control(pDX, IDC_DOEM_D, m_DOEM_D);
	DDX_Control(pDX, IDC_DOEM_T, m_DOEM_T);
	DDX_Control(pDX, IDC_FINM, m_FINM);
	DDX_Control(pDX, IDC_GSMN, m_GSMN);
	DDX_Control(pDX, IDC_KOTU, m_KTOU);
	DDX_Control(pDX, IDC_LNAM, m_LANM);
	DDX_Control(pDX, IDC_LINO, m_LINO);
	DDX_Control(pDX, IDC_MATR, m_MATR);
	DDX_Control(pDX, IDC_PENO, m_PENO);
	DDX_Control(pDX, IDC_PERC, m_PERC);
	DDX_Control(pDX, IDC_SHNM, m_SHNM);
	DDX_Control(pDX, IDC_TELD, m_TELD);
	DDX_Control(pDX, IDC_TELH, m_TELH);
	DDX_Control(pDX, IDC_TELP, m_TELP);
	DDX_Control(pDX, IDC_USEC2, m_USEC);
	DDX_Control(pDX, IDC_USEU2, m_USEU);
	DDX_Control(pDX, IDC_ZUTB, m_ZUTB);
	DDX_Control(pDX, IDC_ZUTF, m_ZUTF);
	DDX_Control(pDX, IDC_ZUTN, m_ZUTN);
	DDX_Control(pDX, IDC_ZUTW, m_ZUTW);
	DDX_Control(pDX, IDC_LSTU_D2, m_LSTUD);
	DDX_Control(pDX, IDC_LSTU_T2, m_LSTUT);
	//}}AFX_DATA_MAP

	/*
	start MNE Jan 21, 1999	(moved this stuff into constructor)

	pomOrgTable = NULL;
	pomPfcTable = NULL;
	pomPerTable = NULL;
	pomCotTable = NULL;
	pomTeaTable = NULL;
	pomWgpTable = NULL;

	pomOrgTable = new CCSTable;
	pomPfcTable = new CCSTable;
	pomPerTable = new CCSTable;
	pomCotTable = new CCSTable;
	pomTeaTable = new CCSTable;
	pomWgpTable = new CCSTable;
	
	end MNE
	*/
}

MitarbeiterStammDlg::~MitarbeiterStammDlg()
{
		if(pomOrgTable != NULL)
			delete pomOrgTable;
		if(pomPfcTable != NULL)
			delete pomPfcTable;
		if(pomPerTable != NULL)
			delete pomPerTable;
		if(pomCotTable != NULL)
			delete pomCotTable;
		if(pomTeaTable != NULL)
			delete pomTeaTable;
		if(pomWgpTable != NULL)
			delete pomWgpTable;

		//*** 30.08.99 SHA ***
		olAct.DeleteAll();
		olAlt.DeleteAll();

}

BEGIN_MESSAGE_MAP(MitarbeiterStammDlg, CDialog)
	//{{AFX_MSG_MAP(MitarbeiterStammDlg)
    ON_MESSAGE(WM_EDIT_KILLFOCUS, OnKillfocus)
	ON_BN_CLICKED(IDC_BF1, OnBf1)
	ON_BN_CLICKED(IDC_BF2, OnBf2)
	ON_BN_CLICKED(IDC_BF3, OnBf3)
	ON_BN_CLICKED(IDC_BF4, OnBf4)
	ON_BN_CLICKED(IDC_BF5, OnBf5)
	ON_BN_CLICKED(IDC_BG1, OnBg1)
	ON_BN_CLICKED(IDC_BG2, OnBg2)
	ON_BN_CLICKED(IDC_BG3, OnBg3)
	ON_BN_CLICKED(IDC_BG4, OnBg4)
	ON_BN_CLICKED(IDC_BG5, OnBg5)
	ON_BN_CLICKED(IDC_BO1, OnBo1)
	ON_BN_CLICKED(IDC_BO2, OnBo2)
	ON_BN_CLICKED(IDC_BO3, OnBo3)
	ON_BN_CLICKED(IDC_BO4, OnBo4)
	ON_BN_CLICKED(IDC_BO5, OnBo5)
	ON_BN_CLICKED(IDC_BQ1, OnBq1)
	ON_BN_CLICKED(IDC_BQ2, OnBq2)
	ON_BN_CLICKED(IDC_BQ3, OnBq3)
	ON_BN_CLICKED(IDC_BQ4, OnBq4)
	ON_BN_CLICKED(IDC_BQ5, OnBq5)
	ON_BN_CLICKED(IDC_BA1, OnBa1)
	ON_BN_CLICKED(IDC_BA2, OnBa2)
	ON_BN_CLICKED(IDC_BA3, OnBa3)
	ON_BN_CLICKED(IDC_BA4, OnBa4)
	ON_BN_CLICKED(IDC_BA5, OnBa5)
	ON_MESSAGE(WM_TABLE_IPEDIT_KILLFOCUS, OnTableIPEditKillfocus)
	ON_BN_CLICKED(IDC_BGRP1, OnBgrp1)
	ON_BN_CLICKED(IDC_BGRP2, OnBgrp2)
	ON_BN_CLICKED(IDC_BGRP3, OnBgrp3)
	ON_BN_CLICKED(IDC_BGRP4, OnBgrp4)
	ON_BN_CLICKED(IDC_BGRP5, OnBgrp5)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// MitarbeiterStammDlg message handlers
//----------------------------------------------------------------------------------------

BOOL MitarbeiterStammDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	m_Caption = LoadStg(IDS_STRING183) + " <" + m_Caption + ">";
	SetWindowText(m_Caption);
	
	char clStat = ogPrivList.GetStat("FAHRGEMEINSCHAFTENDLG.m_OK");
	SetWndStatAll(clStat,m_OK);

	//*** 02.11.99 SHA ***
	//*** DMY ***
	//long x;
	//x = SearchFctc("ff");

	//------------------------------------
	m_CDATD.SetBKColor(SILVER);
	m_CDATD.SetInitText(pomStf->Cdat.Format("%d.%m.%Y"));
	//m_CDATD.SetSecState(ogPrivList.GetStat("FAHRGEMEINSCHAFTENDLG.m_CDAT"));
	// - - - - - - - - - - - - - - - - - -
	m_CDATT.SetBKColor(SILVER);
	m_CDATT.SetInitText(pomStf->Cdat.Format("%H:%M"));
	//m_CDATT.SetSecState(ogPrivList.GetStat("FAHRGEMEINSCHAFTENDLG.m_CDAT"));
	//------------------------------------
	m_LSTUD.SetBKColor(SILVER);
	m_LSTUD.SetInitText(pomStf->Lstu.Format("%d.%m.%Y"));
	//m_LSTUD.SetSecState(ogPrivList.GetStat("FAHRGEMEINSCHAFTENDLG.m_LSTU"));
	// - - - - - - - - - - - - - - - - - -
	m_LSTUT.SetBKColor(SILVER);
	m_LSTUT.SetInitText(pomStf->Lstu.Format("%H:%M"));
	//m_LSTUT.SetSecState(ogPrivList.GetStat("FAHRGEMEINSCHAFTENDLG.m_LSTU"));
	//------------------------------------
	m_USEC.SetBKColor(SILVER);
	m_USEC.SetInitText(pomStf->Usec);
	//m_USEC.SetSecState(ogPrivList.GetStat("FAHRGEMEINSCHAFTENDLG.m_USEC"));
	//------------------------------------
	m_USEU.SetBKColor(SILVER);
	m_USEU.SetInitText(pomStf->Useu);
	//m_USEU.SetSecState(ogPrivList.GetStat("FAHRGEMEINSCHAFTENDLG.m_USEU"));
	//------------------------------------//WHITE,YELLOW,SILVER,RED,LYELLOW
	m_DODDM_D.SetTypeToDate(false);
	m_DODDM_D.SetTextErrColor(RED);
	if(pomStf->Dodm.GetStatus() == COleDateTime::valid)
		m_DODDM_D.SetInitText(pomStf->Dodm.Format("%d.%m.%Y"));
	else
		m_DODDM_D.SetInitText("");
	//------------------------------------
	m_DDODM_T.SetTypeToTime(false);
	m_DDODM_T.SetTextErrColor(RED);
	if(pomStf->Dodm.GetStatus() == COleDateTime::valid)
	{
		m_DDODM_T.SetInitText(pomStf->Dodm.Format("%H:%M"));
	}
	else
	{
		m_DDODM_T.SetInitText("");
	}
	//------------------------------------
	m_DOEM_D.SetTypeToDate(true);
	m_DOEM_D.SetBKColor(YELLOW);  
	m_DOEM_D.SetTextErrColor(RED);
	if(pomStf->Doem.GetStatus() == COleDateTime::valid)
		m_DOEM_D.SetInitText(pomStf->Doem.Format("%d.%m.%Y"));
	else
		m_DOEM_D.SetInitText("");
	//------------------------------------
	m_DOEM_T.SetTypeToTime(true);
	m_DOEM_T.SetBKColor(YELLOW);  
	m_DOEM_T.SetTextErrColor(RED);
	if(pomStf->Doem.GetStatus() == COleDateTime::valid)
		m_DOEM_T.SetInitText(pomStf->Doem.Format("%H:%M"));
	else
		m_DOEM_T.SetInitText("");

	//------------------------------------
	m_FINM.SetTypeToString("X(20)", 20, 1);
	m_FINM.SetBKColor(YELLOW);  
	m_FINM.SetTextErrColor(RED);
	m_FINM.SetInitText(pomStf->Finm);
	//------------------------------------
	m_GSMN.SetTypeToString("X(20)", 20, 1);
	m_GSMN.SetBKColor(YELLOW);  
	m_GSMN.SetTextErrColor(RED);
	m_GSMN.SetInitText(pomStf->Gsmn);
	//------------------------------------
	m_KTOU.SetTypeToString("#####", 5, 0);
	//m_KTOU.SetBKColor(YELLOW);  
	m_KTOU.SetTextErrColor(RED);
	m_KTOU.SetInitText(pomStf->Ktou);
	//------------------------------------
	m_LANM.SetTypeToString("X(40)", 40, 1);
	m_LANM.SetBKColor(YELLOW);  
	m_LANM.SetTextErrColor(RED);
	m_LANM.SetInitText(pomStf->Lanm);
	//------------------------------------
	m_LINO.SetTypeToString("X(20)", 20, 1);
//	m_LINO.SetBKColor(YELLOW);  
	m_LINO.SetTextErrColor(RED);
	m_LINO.SetInitText(pomStf->Lino);
	//------------------------------------
	m_MATR.SetTypeToString("#####", 5, 0);
	//m_KTOU.SetBKColor(YELLOW);  
	m_MATR.SetTextErrColor(RED);
	m_MATR.SetInitText(pomStf->Matr);
	//------------------------------------
	m_PENO.SetTypeToString("X(20)", 20, 1);
	m_PENO.SetBKColor(YELLOW);  
	m_PENO.SetTextErrColor(RED);
	m_PENO.SetInitText(pomStf->Peno);
	//------------------------------------
	m_PERC.SetTypeToString("X(3)", 3, 1);
	m_PERC.SetBKColor(YELLOW);  
	m_PERC.SetTextErrColor(RED);
	m_PERC.SetInitText(pomStf->Perc);
	//------------------------------------
	m_SHNM.SetTypeToString("X(10)", 10, 1);
	m_SHNM.SetBKColor(YELLOW);  
	m_SHNM.SetTextErrColor(RED);
	m_SHNM.SetInitText(pomStf->Shnm);
	//------------------------------------
	m_TELD.SetTypeToString("#(20)", 20, 0);
	//m_TELD.SetBKColor(YELLOW);  
	m_TELD.SetTextErrColor(RED);
	m_TELD.SetInitText(pomStf->Teld);
	//------------------------------------
	m_TELH.SetTypeToString("#(20)", 20, 0);
	//m_TELH.SetBKColor(YELLOW);  
	m_TELH.SetTextErrColor(RED);
	m_TELH.SetInitText(pomStf->Telh);
	//------------------------------------
	m_TELP.SetTypeToString("#(20)", 20, 0);
	//m_TELP.SetBKColor(YELLOW);  
	m_TELP.SetTextErrColor(RED);
	m_TELP.SetInitText(pomStf->Telp);
	//------------------------------------
	m_ZUTB.SetTypeToString("#####", 5, 0);
	//m_ZUTB.SetBKColor(YELLOW);  
	m_ZUTB.SetTextErrColor(RED);
	m_ZUTB.SetInitText(pomStf->Zutb);
	//------------------------------------
	m_ZUTF.SetTypeToString("#####", 5, 0);
	//m_ZUTF.SetBKColor(YELLOW);  
	m_ZUTF.SetTextErrColor(RED);
	m_ZUTF.SetInitText(pomStf->Zutf);
	//------------------------------------
	m_ZUTN.SetTypeToString("#####", 5, 0);
	//m_ZUTN.SetBKColor(YELLOW);  
	m_ZUTN.SetTextErrColor(RED);
	m_ZUTN.SetInitText(pomStf->Zutn);
	//------------------------------------
	m_ZUTW.SetTypeToString("#####", 5, 0);
	//m_ZUTW.SetBKColor(YELLOW);  
	m_ZUTW.SetTextErrColor(RED);
	m_ZUTW.SetInitText(pomStf->Zutw);
	//------------------------------------
	//*** 09.09.99 SHA ***
	m_REMA.SetTypeToString("X(60)", 60, 0);
	m_REMA.SetTextErrColor(RED);
	m_REMA.SetInitText(pomStf->Rema);
	//------------------------------------

	if(strcmp(pomStf->Tohr, "J") == 0)
	{
		m_THOR.SetCheck(1);
	}
	else
	{
		m_THOR.SetCheck(0);
	}
	InitTables();

	return TRUE;  
}

//----------------------------------------------------------------------------------------

void MitarbeiterStammDlg::InitTables()
{
	int ili;
	CRect olRectBorder;
	m_HELP_ORG.GetWindowRect( olRectBorder );
	ScreenToClient(olRectBorder);
	olRectBorder.DeflateRect(2,2);

	int ilTab = (int)((olRectBorder.right - olRectBorder.left)/3);
	ilTab -= 10;
	pomOrgTable->SetHeaderSpacing(0);
	pomOrgTable->SetMiniTable();
	pomOrgTable->SetTableEditable(true);
    pomOrgTable->SetTableData(this, olRectBorder.left, olRectBorder.right, olRectBorder.top, olRectBorder.bottom);
	pomOrgTable->SetSelectMode(0);

	CCSPtrArray <TABLE_HEADER_COLUMN> omHeaderDataArray;
	TABLE_HEADER_COLUMN *prlHeader[9];//*** 27.08.99 SHA *** EX 5
	pomOrgTable->SetShowSelection(false);
	pomOrgTable->ResetContent();

	prlHeader[0] = new TABLE_HEADER_COLUMN;
	prlHeader[0]->Alignment = COLALIGN_CENTER;
	prlHeader[0]->Length = ilTab-20; 
	prlHeader[0]->Font = &ogCourier_Regular_10;
	prlHeader[0]->Text = CString(""); 

	prlHeader[1] = new TABLE_HEADER_COLUMN;
	prlHeader[1]->Alignment = COLALIGN_CENTER;
	prlHeader[1]->Length = ilTab+12; 
	prlHeader[1]->Font = &ogCourier_Regular_10;
	prlHeader[1]->Text = CString("");

	prlHeader[2] = new TABLE_HEADER_COLUMN;
	prlHeader[2]->Alignment = COLALIGN_CENTER;
	prlHeader[2]->Length = ilTab+8; 
	prlHeader[2]->Font = &ogCourier_Regular_10;
	prlHeader[2]->Text = CString("");

	prlHeader[3] = new TABLE_HEADER_COLUMN;
	prlHeader[3]->Alignment = COLALIGN_CENTER;
	prlHeader[3]->Length = 0; 
	prlHeader[3]->Font = &ogCourier_Regular_10;
	prlHeader[3]->Text = CString("");

	for(ili = 0; ili < 4; ili++)
	{
		omHeaderDataArray.Add(prlHeader[ili]);
	}
	pomOrgTable->SetHeaderFields(omHeaderDataArray);
	pomOrgTable->SetDefaultSeparator();
	pomOrgTable->SetTableEditable(true);
	omHeaderDataArray.DeleteAll();
	pomOrgTable->DisplayTable();

//-------------------------Tabelle 2

	m_HELP_PFC.GetWindowRect( olRectBorder );
	ScreenToClient(olRectBorder);
	olRectBorder.DeflateRect(2,2);
	ilTab = (int)((olRectBorder.right - olRectBorder.left)/3);
	ilTab -= 10;
	pomPfcTable->SetHeaderSpacing(0);
	pomPfcTable->SetMiniTable();
	pomPfcTable->SetTableEditable(true);
    pomPfcTable->SetTableData(this, olRectBorder.left, olRectBorder.right, olRectBorder.top, olRectBorder.bottom);
	pomPfcTable->SetSelectMode(0);

	pomPfcTable->SetShowSelection(false);
	pomPfcTable->ResetContent();
	
	int ilElements = 0;

#ifdef CLIENT_HAJ

	prlHeader[0] = new TABLE_HEADER_COLUMN;
	prlHeader[0]->Alignment = COLALIGN_CENTER;
	prlHeader[0]->Length = ilTab-10; 
	prlHeader[0]->Font = &ogCourier_Regular_8;
	prlHeader[0]->Text = CString(""); 

	prlHeader[1] = new TABLE_HEADER_COLUMN;
	prlHeader[1]->Alignment = COLALIGN_CENTER;
	prlHeader[1]->Length = ilTab+7; 
	prlHeader[1]->Font = &ogCourier_Regular_10;
	prlHeader[1]->Text = CString("");

	prlHeader[2] = new TABLE_HEADER_COLUMN;
	prlHeader[2]->Alignment = COLALIGN_CENTER;
	prlHeader[2]->Length = ilTab+3; 
	prlHeader[2]->Font = &ogCourier_Regular_10;
	prlHeader[2]->Text = CString("");

	prlHeader[3] = new TABLE_HEADER_COLUMN;
	prlHeader[3]->Alignment = COLALIGN_CENTER;
	prlHeader[3]->Length = 0; 
	prlHeader[3]->Font = &ogCourier_Regular_10;
	prlHeader[3]->Text = CString("");
	ilElements = 4;

#else
	prlHeader[0] = new TABLE_HEADER_COLUMN;
	prlHeader[0]->Alignment = COLALIGN_CENTER;
	prlHeader[0]->Length = 55;//ilTab-10; 
	prlHeader[0]->Font = &ogCourier_Regular_8;
	prlHeader[0]->Text = CString(""); 

	prlHeader[1] = new TABLE_HEADER_COLUMN;
	prlHeader[1]->Alignment = COLALIGN_CENTER;
	prlHeader[1]->Length = 75;//ilTab+7; 
	prlHeader[1]->Font = &ogCourier_Regular_10;
	prlHeader[1]->Text = CString("");

	prlHeader[2] = new TABLE_HEADER_COLUMN;
	prlHeader[2]->Alignment = COLALIGN_CENTER;
	prlHeader[2]->Length = 75;//ilTab+3; 
	prlHeader[2]->Font = &ogCourier_Regular_10;
	prlHeader[2]->Text = CString("");

	prlHeader[3] = new TABLE_HEADER_COLUMN;
	prlHeader[3]->Alignment = COLALIGN_CENTER;
	prlHeader[3]->Length = 0; 
	prlHeader[3]->Font = &ogCourier_Regular_10;
	prlHeader[3]->Text = CString("");

	prlHeader[4] = new TABLE_HEADER_COLUMN;		//*** ACT3 ***
	prlHeader[4]->Alignment = COLALIGN_CENTER;
	prlHeader[4]->Length = 65; 
	prlHeader[4]->Font = &ogCourier_Regular_10;
	prlHeader[4]->Text = CString("");

	prlHeader[5] = new TABLE_HEADER_COLUMN;		//*** ACT5 ***
	prlHeader[5]->Alignment = COLALIGN_CENTER;
	prlHeader[5]->Length = 65; 
	prlHeader[5]->Font = &ogCourier_Regular_10;
	prlHeader[5]->Text = CString("");

	prlHeader[6] = new TABLE_HEADER_COLUMN;		//*** ALC2 ***
	prlHeader[6]->Alignment = COLALIGN_CENTER;
	prlHeader[6]->Length = 65; 
	prlHeader[6]->Font = &ogCourier_Regular_10;
	prlHeader[6]->Text = CString("");

	prlHeader[7] = new TABLE_HEADER_COLUMN;		//*** ALC3 ***
	prlHeader[7]->Alignment = COLALIGN_CENTER;
	prlHeader[7]->Length = 65; 
	prlHeader[7]->Font = &ogCourier_Regular_10;
	prlHeader[7]->Text = CString("");
	ilElements = 8;
#endif

	for(ili = 0; ili < ilElements ; ili++)
	{
		omHeaderDataArray.Add(prlHeader[ili]);
	}
	pomPfcTable->SetHeaderFields(omHeaderDataArray);
	pomPfcTable->SetDefaultSeparator();
	pomPfcTable->SetTableEditable(true);
	omHeaderDataArray.DeleteAll();
	pomPfcTable->DisplayTable();
	omHeaderDataArray.DeleteAll();

//-------------------------Tabelle 3

	m_HELP_PER.GetWindowRect( olRectBorder );
	ScreenToClient(olRectBorder);
	olRectBorder.DeflateRect(2,2);
	ilTab = (int)((olRectBorder.right - olRectBorder.left)/3);
	ilTab -= 10;
	pomPerTable->SetHeaderSpacing(0);
	pomPerTable->SetMiniTable();
	pomPerTable->SetTableEditable(true);
    pomPerTable->SetTableData(this, olRectBorder.left, olRectBorder.right, olRectBorder.top, olRectBorder.bottom);
	pomPerTable->SetSelectMode(0);


	pomPerTable->SetShowSelection(false);
	pomPerTable->ResetContent();
	

	prlHeader[0] = new TABLE_HEADER_COLUMN;
	prlHeader[0]->Alignment = COLALIGN_CENTER;
	prlHeader[0]->Length = 55;//ilTab-20; 
	prlHeader[0]->Font = &ogCourier_Regular_10;
	prlHeader[0]->Text = CString(""); 

	prlHeader[1] = new TABLE_HEADER_COLUMN;
	prlHeader[1]->Alignment = COLALIGN_CENTER;
	prlHeader[1]->Length = 75;//ilTab-15;//+12; 
	prlHeader[1]->Font = &ogCourier_Regular_10;
	prlHeader[1]->Text = CString("");

	prlHeader[2] = new TABLE_HEADER_COLUMN;
	prlHeader[2]->Alignment = COLALIGN_CENTER;
	prlHeader[2]->Length = 75;//ilTab-15;//+8; 
	prlHeader[2]->Font = &ogCourier_Regular_10;
	prlHeader[2]->Text = CString("");

	prlHeader[3] = new TABLE_HEADER_COLUMN;
	prlHeader[3]->Alignment = COLALIGN_CENTER;
	prlHeader[3]->Length = 0; 
	prlHeader[3]->Font = &ogCourier_Regular_10;
	prlHeader[3]->Text = CString("");



	for(ili = 0; ili < 4; ili++)
	{
		omHeaderDataArray.Add(prlHeader[ili]);
	}
	pomPerTable->SetHeaderFields(omHeaderDataArray);
	pomPerTable->SetDefaultSeparator();
	pomPerTable->SetTableEditable(true);
	omHeaderDataArray.DeleteAll();
	pomPerTable->DisplayTable();
	omHeaderDataArray.DeleteAll();//sssssssssssssscvccccccccccccccccccccccccc

//-------------------------Tabelle 4

	m_HELP_COT.GetWindowRect( olRectBorder );
	ScreenToClient(olRectBorder);
	olRectBorder.DeflateRect(2,2);
	ilTab = (int)((olRectBorder.right - olRectBorder.left)/3);
	ilTab -= 10;
	pomCotTable->SetHeaderSpacing(0);
	pomCotTable->SetMiniTable();
	pomCotTable->SetTableEditable(true);
    pomCotTable->SetTableData(this, olRectBorder.left, olRectBorder.right, olRectBorder.top, olRectBorder.bottom);
	pomCotTable->SetSelectMode(0);


	pomCotTable->SetShowSelection(false);
	pomCotTable->ResetContent();
	

	prlHeader[0] = new TABLE_HEADER_COLUMN;
	prlHeader[0]->Alignment = COLALIGN_CENTER;
	prlHeader[0]->Length = ilTab-20; 
	prlHeader[0]->Font = &ogCourier_Regular_10;
	prlHeader[0]->Text = CString(""); 

	prlHeader[1] = new TABLE_HEADER_COLUMN;
	prlHeader[1]->Alignment = COLALIGN_CENTER;
	prlHeader[1]->Length = ilTab+12; 
	prlHeader[1]->Font = &ogCourier_Regular_10;
	prlHeader[1]->Text = CString("");

	prlHeader[2] = new TABLE_HEADER_COLUMN;
	prlHeader[2]->Alignment = COLALIGN_CENTER;
	prlHeader[2]->Length = ilTab+8; 
	prlHeader[2]->Font = &ogCourier_Regular_10;
	prlHeader[2]->Text = CString("");

	prlHeader[3] = new TABLE_HEADER_COLUMN;
	prlHeader[3]->Alignment = COLALIGN_CENTER;
	prlHeader[3]->Length = 0; 
	prlHeader[3]->Font = &ogCourier_Regular_10;
	prlHeader[3]->Text = CString("");
	for(ili = 0; ili < 4; ili++)
	{
		omHeaderDataArray.Add(prlHeader[ili]);
	}
	pomCotTable->SetHeaderFields(omHeaderDataArray);
	pomCotTable->SetDefaultSeparator();
	pomCotTable->SetTableEditable(true);
	omHeaderDataArray.DeleteAll();
	pomCotTable->DisplayTable();
	omHeaderDataArray.DeleteAll();

//-------------------------Tabelle 5
/*
	m_HELP_COT.GetWindowRect( olRectBorder );
	ScreenToClient(olRectBorder);
	olRectBorder.DeflateRect(2,2);
	ilTab = (int)((olRectBorder.right - olRectBorder.left)/3);
	ilTab -= 10;
	pomCotTable->SetHeaderSpacing(0);
	pomCotTable->SetMiniTable();
	pomCotTable->SetTableEditable(true);
    pomCotTable->SetTableData(this, olRectBorder.left, olRectBorder.right, olRectBorder.top, olRectBorder.bottom);
	pomCotTable->SetSelectMode(0);


	pomCotTable->SetShowSelection(false);
	pomCotTable->ResetContent();

	prlHeader[0] = new TABLE_HEADER_COLUMN;
	prlHeader[0]->Alignment = COLALIGN_CENTER;
	prlHeader[0]->Length = ilTab-20; 
	prlHeader[0]->Font = &ogCourier_Regular_10;
	prlHeader[0]->Text = CString(""); 

	prlHeader[1] = new TABLE_HEADER_COLUMN;
	prlHeader[1]->Alignment = COLALIGN_CENTER;
	prlHeader[1]->Length = ilTab+12; 
	prlHeader[1]->Font = &ogCourier_Regular_10;
	prlHeader[1]->Text = CString("");

	prlHeader[2] = new TABLE_HEADER_COLUMN;
	prlHeader[2]->Alignment = COLALIGN_CENTER;
	prlHeader[2]->Length = ilTab+8; 
	prlHeader[2]->Font = &ogCourier_Regular_10;
	prlHeader[2]->Text = CString("");

	prlHeader[3] = new TABLE_HEADER_COLUMN;
	prlHeader[3]->Alignment = COLALIGN_CENTER;
	prlHeader[3]->Length = 0; 
	prlHeader[3]->Font = &ogCourier_Regular_10;
	prlHeader[3]->Text = CString("");
	for(ili = 0; ili < 4; ili++)
	{
		omHeaderDataArray.Add(prlHeader[ili]);
	}
	pomCotTable->SetHeaderFields(omHeaderDataArray);
	pomCotTable->SetDefaultSeparator();
	pomCotTable->SetTableEditable(true);
	omHeaderDataArray.DeleteAll();
	pomCotTable->DisplayTable();
	omHeaderDataArray.DeleteAll();
*/
//-------------------------Tabelle 5

	m_HELP_TEA.GetWindowRect( olRectBorder );
	ScreenToClient(olRectBorder);
	olRectBorder.DeflateRect(2,2);
	ilTab = (int)((olRectBorder.right - olRectBorder.left)/3);
	ilTab -= 10;
	pomTeaTable->SetHeaderSpacing(0);
	pomTeaTable->SetMiniTable();
	pomTeaTable->SetTableEditable(true);
    pomTeaTable->SetTableData(this, olRectBorder.left, olRectBorder.right, olRectBorder.top, olRectBorder.bottom);
	pomTeaTable->SetSelectMode(0);


	pomTeaTable->SetShowSelection(false);
	pomTeaTable->ResetContent();
	

	prlHeader[0] = new TABLE_HEADER_COLUMN;
	prlHeader[0]->Alignment = COLALIGN_CENTER;
	prlHeader[0]->Length = ilTab-20; 
	prlHeader[0]->Font = &ogCourier_Regular_10;
	prlHeader[0]->Text = CString(""); 

	prlHeader[1] = new TABLE_HEADER_COLUMN;
	prlHeader[1]->Alignment = COLALIGN_CENTER;
	prlHeader[1]->Length = ilTab+12; 
	prlHeader[1]->Font = &ogCourier_Regular_10;
	prlHeader[1]->Text = CString("");

	prlHeader[2] = new TABLE_HEADER_COLUMN;
	prlHeader[2]->Alignment = COLALIGN_CENTER;
	prlHeader[2]->Length = ilTab+8; 
	prlHeader[2]->Font = &ogCourier_Regular_10;
	prlHeader[2]->Text = CString("");

	prlHeader[3] = new TABLE_HEADER_COLUMN;
	prlHeader[3]->Alignment = COLALIGN_CENTER;
	prlHeader[3]->Length = 0; 
	prlHeader[3]->Font = &ogCourier_Regular_10;
	prlHeader[3]->Text = CString("");

	for(ili = 0; ili < 4; ili++)
	{
		omHeaderDataArray.Add(prlHeader[ili]);
	}
	pomTeaTable->SetHeaderFields(omHeaderDataArray);
	pomTeaTable->SetDefaultSeparator();
	pomTeaTable->SetTableEditable(true);
	omHeaderDataArray.DeleteAll();
	pomTeaTable->DisplayTable();
	omHeaderDataArray.DeleteAll();

//-------------------------Tabelle 6

	m_HELP_GRP.GetWindowRect( olRectBorder );
	ScreenToClient(olRectBorder);
	olRectBorder.DeflateRect(2,2);
	ilTab = (int)((olRectBorder.right - olRectBorder.left)/3);
	ilTab -= 10;
	pomWgpTable->SetHeaderSpacing(0);
	pomWgpTable->SetMiniTable();
	pomWgpTable->SetTableEditable(true);
    pomWgpTable->SetTableData(this, olRectBorder.left, olRectBorder.right, olRectBorder.top, olRectBorder.bottom);
	pomWgpTable->SetSelectMode(0);


	pomWgpTable->SetShowSelection(false);
	pomWgpTable->ResetContent();
	

	prlHeader[0] = new TABLE_HEADER_COLUMN;
	prlHeader[0]->Alignment = COLALIGN_CENTER;
	prlHeader[0]->Length = ilTab-20; 
	prlHeader[0]->Font = &ogCourier_Regular_10;
	prlHeader[0]->Text = CString(""); 

	prlHeader[1] = new TABLE_HEADER_COLUMN;
	prlHeader[1]->Alignment = COLALIGN_CENTER;
	prlHeader[1]->Length = ilTab+12; 
	prlHeader[1]->Font = &ogCourier_Regular_10;
	prlHeader[1]->Text = CString("");

	prlHeader[2] = new TABLE_HEADER_COLUMN;
	prlHeader[2]->Alignment = COLALIGN_CENTER;
	prlHeader[2]->Length = ilTab+8; 
	prlHeader[2]->Font = &ogCourier_Regular_10;
	prlHeader[2]->Text = CString("");

	prlHeader[3] = new TABLE_HEADER_COLUMN;
	prlHeader[3]->Alignment = COLALIGN_CENTER;
	prlHeader[3]->Length = 0; 
	prlHeader[3]->Font = &ogCourier_Regular_10;
	prlHeader[3]->Text = CString("");

	for(ili = 0; ili < 4; ili++)
	{
		omHeaderDataArray.Add(prlHeader[ili]);
	}
	pomWgpTable->SetHeaderFields(omHeaderDataArray);
	pomWgpTable->SetDefaultSeparator();
	pomWgpTable->SetTableEditable(true);
	omHeaderDataArray.DeleteAll();
	pomWgpTable->DisplayTable();
	omHeaderDataArray.DeleteAll();

//-------------------------------------------------------------------------
// Attribute f�r die einzelnen Tables setzen
// Tabelle ORG

	CCSEDIT_ATTRIB rlAttribC1;
	CCSEDIT_ATTRIB rlAttribC2;
	CCSEDIT_ATTRIB rlAttribC3;
	CCSEDIT_ATTRIB rlAttribC4;
	CCSEDIT_ATTRIB rlAttribC5;
	CCSEDIT_ATTRIB rlAttribC6;
	CCSEDIT_ATTRIB rlAttribC7;
	CCSEDIT_ATTRIB rlAttribC8;
	
	rlAttribC1.Format = "X(5)";
	rlAttribC1.TextMinLenght = 0;
	rlAttribC1.TextMaxLenght = 5;
	rlAttribC1.Style = ES_UPPERCASE;
	
	rlAttribC2.Type = KT_DATE;
	rlAttribC2.ChangeDay	= true;
	rlAttribC2.TextMaxLenght = 10;

	rlAttribC3.Type = KT_DATE;
	rlAttribC3.ChangeDay	= true;
	rlAttribC3.TextMaxLenght = 10;



	CCSPtrArray<TABLE_COLUMN> olColList;
	TABLE_COLUMN rlColumnData;
	rlColumnData.SeparatorType = SEPA_NONE;
	rlColumnData.Font = &ogMSSansSerif_Bold_8;//ogMS_Sans_Serif_8;
	rlColumnData.VerticalSeparator = SEPA_NORMAL;
	rlColumnData.HorizontalSeparator = SEPA_NONE;
	rlColumnData.Alignment = COLALIGN_CENTER;

	for(int i = 0; i < pomSorData->GetSize(); i++)
	{
		rlColumnData.SeparatorType = SEPA_NONE;
		rlColumnData.VerticalSeparator = SEPA_LIKEVERTICAL;
		rlColumnData.Text = CString(pomSorData->GetAt(i).Code);
		rlColumnData.EditAttrib = rlAttribC1;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = CString("");
		if(pomSorData->GetAt(i).Vpfr.GetStatus() == COleDateTime::valid)
			rlColumnData.Text = CString(pomSorData->GetAt(i).Vpfr.Format("%d.%m.%Y"));
		rlColumnData.EditAttrib = rlAttribC2;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = CString("");
		if(pomSorData->GetAt(i).Vpto.GetStatus() == COleDateTime::valid)
			rlColumnData.Text = CString(pomSorData->GetAt(i).Vpto.Format("%d.%m.%Y"));
		rlColumnData.EditAttrib = rlAttribC3;
		rlColumnData.SeparatorType = SEPA_NONE;
		rlColumnData.VerticalSeparator = SEPA_NONE;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		if(pomSorData->GetAt(i).Urno != 0)
		{
			char pclText[20];
			sprintf(pclText, "%ld", pomSorData->GetAt(i).Urno);
			rlColumnData.Text = CString(pclText);
		}
		else
		{
			rlColumnData.Text = CString("");
		}
		rlColumnData.SeparatorType = SEPA_NONE;
		rlColumnData.VerticalSeparator = SEPA_NONE;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		pomOrgTable->AddTextLine(olColList, (void*)(NULL));
		olColList.DeleteAll();
	}
	for(i = pomSorData->GetSize(); i < 50; i++)
	{
		rlColumnData.SeparatorType = SEPA_NONE;
		rlColumnData.VerticalSeparator = SEPA_LIKEVERTICAL;
		rlColumnData.Text = CString("");
		rlColumnData.EditAttrib = rlAttribC1;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = CString("");
		rlColumnData.EditAttrib = rlAttribC2;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = CString("");
		rlColumnData.EditAttrib = rlAttribC3;
		rlColumnData.SeparatorType = SEPA_NONE;
		rlColumnData.VerticalSeparator = SEPA_NONE;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = CString("");
		rlColumnData.SeparatorType = SEPA_NONE;
		rlColumnData.VerticalSeparator = SEPA_NONE;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		pomOrgTable->AddTextLine(olColList, (void*)(NULL));
		olColList.DeleteAll();
	}
	pomOrgTable->DisplayTable();
	pomOrgTable->SetColumnEditable(3, false);
//-------------------------------------------------------------------------
// Attribute f�r die einzelnen Tables setzen
// Tabelle PFC

	rlAttribC1.Format = "X(5)";
	rlAttribC1.TextMinLenght = 0;
	rlAttribC1.TextMaxLenght = 5;
	rlAttribC1.Style = ES_UPPERCASE;
	
	rlAttribC2.Type = KT_DATE;
	rlAttribC2.ChangeDay	= true;
	rlAttribC2.TextMaxLenght = 10;

	rlAttribC3.Type = KT_DATE;
	rlAttribC3.ChangeDay	= true;
	rlAttribC3.TextMaxLenght = 10;

#ifndef CLIENT_HAJ
	//*** 27.08.99 SHA ***
	rlAttribC4.Format = "X(3)";
	rlAttribC4.TextMinLenght = 0;
	rlAttribC4.TextMaxLenght = 3;
	rlAttribC4.Style = ES_UPPERCASE;

	rlAttribC5.Format = "X(5)";
	rlAttribC5.TextMinLenght = 0;
	rlAttribC5.TextMaxLenght = 5;
	rlAttribC5.Style = ES_UPPERCASE;

	rlAttribC6.Format = "X(2)";
	rlAttribC6.TextMinLenght = 0;
	rlAttribC6.TextMaxLenght = 2;
	rlAttribC6.Style = ES_UPPERCASE;

	rlAttribC7.Format = "X(3)";
	rlAttribC7.TextMinLenght = 0;
	rlAttribC7.TextMaxLenght = 3;
	rlAttribC7.Style = ES_UPPERCASE;
#endif

	rlColumnData.SeparatorType = SEPA_NONE;
	rlColumnData.Font = &ogMSSansSerif_Bold_8;//ogMS_Sans_Serif_8;
	rlColumnData.VerticalSeparator = SEPA_NORMAL;
	rlColumnData.HorizontalSeparator = SEPA_NONE;
	rlColumnData.Alignment = COLALIGN_CENTER;

	for(i = 0; i < pomSpfData->GetSize(); i++)
	{
		rlColumnData.SeparatorType = SEPA_NONE;
		rlColumnData.VerticalSeparator = SEPA_LIKEVERTICAL;
		rlColumnData.Text = CString(pomSpfData->GetAt(i).Code);
		rlColumnData.EditAttrib = rlAttribC1;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = CString("");
		if(pomSpfData->GetAt(i).Vpfr.GetStatus() == COleDateTime::valid)
			rlColumnData.Text = CString(pomSpfData->GetAt(i).Vpfr.Format("%d.%m.%Y"));
		rlColumnData.EditAttrib = rlAttribC2;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = CString("");
		if(pomSpfData->GetAt(i).Vpto.GetStatus() == COleDateTime::valid)
			rlColumnData.Text = CString(pomSpfData->GetAt(i).Vpto.Format("%d.%m.%Y"));
		rlColumnData.EditAttrib = rlAttribC3;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		if(pomSpfData->GetAt(i).Urno != 0)
		{
			char pclText[20];
			sprintf(pclText, "%ld", pomSpfData->GetAt(i).Urno);
			rlColumnData.Text = CString(pclText);
		}
		else
		{
			rlColumnData.Text = CString("");
		}
		rlColumnData.SeparatorType = SEPA_NONE;
		rlColumnData.VerticalSeparator = SEPA_NONE;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

#ifndef CLIENT_HAJ
		//*** 27.08.99 SHA ***
		rlColumnData.Text = CString(pomSpfData->GetAt(i).Act3);
		rlColumnData.EditAttrib = rlAttribC4;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		rlColumnData.Text = CString(pomSpfData->GetAt(i).Act5);
		rlColumnData.EditAttrib = rlAttribC5;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		rlColumnData.Text = CString(pomSpfData->GetAt(i).Alc2);
		rlColumnData.EditAttrib = rlAttribC6;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		rlColumnData.Text = CString(pomSpfData->GetAt(i).Alc3);
		rlColumnData.EditAttrib = rlAttribC7;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
#endif

		pomPfcTable->AddTextLine(olColList, (void*)(NULL));
		olColList.DeleteAll();
	}
	for(i = pomSpfData->GetSize(); i < 50; i++)
	{
		rlColumnData.SeparatorType = SEPA_NONE;
		rlColumnData.VerticalSeparator = SEPA_LIKEVERTICAL;
		rlColumnData.Text = CString("");
		rlColumnData.EditAttrib = rlAttribC1;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = CString("");
		rlColumnData.EditAttrib = rlAttribC2;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = CString("");
		rlColumnData.EditAttrib = rlAttribC3;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = CString("");
		rlColumnData.SeparatorType = SEPA_NONE;
		rlColumnData.VerticalSeparator = SEPA_NONE;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

#ifndef CLIENT_HAJ
		//*** 27.08.99 SHA ***
		rlColumnData.Text = CString("");
		rlColumnData.EditAttrib = rlAttribC4;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		rlColumnData.Text = CString("");
		rlColumnData.EditAttrib = rlAttribC5;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		rlColumnData.Text = CString("");
		rlColumnData.EditAttrib = rlAttribC6;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		rlColumnData.Text = CString("");
		rlColumnData.EditAttrib = rlAttribC7;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
#endif

		pomPfcTable->AddTextLine(olColList, (void*)(NULL));
		olColList.DeleteAll();
	}
	pomPfcTable->DisplayTable();
	pomPfcTable->SetColumnEditable(3, false);

#ifndef CLIENT_HAJ
	pomPfcTable->SetColumnEditable(4, false);
	pomPfcTable->SetColumnEditable(5, false);
	pomPfcTable->SetColumnEditable(6, false);
	pomPfcTable->SetColumnEditable(7, false);
#endif

//-------------------------------------------------------------------------
// Attribute f�r die einzelnen Tables setzen
// Tabelle PER

	rlAttribC1.Format = "X(5)";
	rlAttribC1.TextMinLenght = 0;
	rlAttribC1.TextMaxLenght = 5;
	rlAttribC1.Style = ES_UPPERCASE;
	
	rlAttribC2.Type = KT_DATE;
	rlAttribC2.ChangeDay	= true;
	rlAttribC2.TextMaxLenght = 10;

	rlAttribC3.Type = KT_DATE;
	rlAttribC3.ChangeDay	= true;
	rlAttribC3.TextMaxLenght = 10;





	rlColumnData.SeparatorType = SEPA_NONE;
	rlColumnData.Font = &ogMSSansSerif_Bold_8;//ogMS_Sans_Serif_8;
	rlColumnData.VerticalSeparator = SEPA_NORMAL;
	rlColumnData.HorizontalSeparator = SEPA_NONE;
	rlColumnData.Alignment = COLALIGN_CENTER;

	for(i = 0; i < pomSpeData->GetSize(); i++)
	{
		rlColumnData.SeparatorType = SEPA_NONE;
		rlColumnData.VerticalSeparator = SEPA_LIKEVERTICAL;
		rlColumnData.Text = CString(pomSpeData->GetAt(i).Code);
		rlColumnData.EditAttrib = rlAttribC1;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = CString("");
		if(pomSpeData->GetAt(i).Vpfr.GetStatus() == COleDateTime::valid)
			rlColumnData.Text = CString(pomSpeData->GetAt(i).Vpfr.Format("%d.%m.%Y"));
		rlColumnData.EditAttrib = rlAttribC2;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = CString("");
		if(pomSpeData->GetAt(i).Vpto.GetStatus() == COleDateTime::valid)
			rlColumnData.Text = CString(pomSpeData->GetAt(i).Vpto.Format("%d.%m.%Y"));
		rlColumnData.EditAttrib = rlAttribC3;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		if(pomSpeData->GetAt(i).Urno != 0)
		{
			char pclText[20];
			sprintf(pclText, "%ld", pomSpeData->GetAt(i).Urno);
			rlColumnData.Text = CString(pclText);
		}
		else
		{
			rlColumnData.Text = CString("");
		}
		rlColumnData.SeparatorType = SEPA_NONE;
		rlColumnData.VerticalSeparator = SEPA_NONE;
		olColList.NewAt(olColList.GetSize(), rlColumnData);


		pomPerTable->AddTextLine(olColList, (void*)(NULL));
		olColList.DeleteAll();
	}
	for(i = pomSpeData->GetSize(); i < 50; i++)
	{
		rlColumnData.SeparatorType = SEPA_NONE;
		rlColumnData.VerticalSeparator = SEPA_LIKEVERTICAL;
		rlColumnData.Text = CString("");
		rlColumnData.EditAttrib = rlAttribC1;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = CString("");
		rlColumnData.EditAttrib = rlAttribC2;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = CString("");
		rlColumnData.EditAttrib = rlAttribC3;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		rlColumnData.Text = CString("");
		rlColumnData.SeparatorType = SEPA_NONE;
		rlColumnData.VerticalSeparator = SEPA_NONE;
		olColList.NewAt(olColList.GetSize(), rlColumnData);


		pomPerTable->AddTextLine(olColList, (void*)(NULL));
		olColList.DeleteAll();
	}
	pomPerTable->DisplayTable();
	pomPerTable->SetColumnEditable(3, false);


//-------------------------------------------------------------------------
// Attribute f�r die einzelnen Tables setzen
// Tabelle COT

	rlAttribC1.Format = "X(5)";
	rlAttribC1.TextMinLenght = 0;
	rlAttribC1.TextMaxLenght = 5;
	rlAttribC1.Style = ES_UPPERCASE;
	
	rlAttribC2.Type = KT_DATE;
	rlAttribC2.ChangeDay	= true;
	rlAttribC2.TextMaxLenght = 10;

	rlAttribC3.Type = KT_DATE;
	rlAttribC3.ChangeDay	= true;
	rlAttribC3.TextMaxLenght = 10;

	rlColumnData.SeparatorType = SEPA_NONE;
	rlColumnData.Font = &ogMSSansSerif_Bold_8;//ogMS_Sans_Serif_8;
	rlColumnData.VerticalSeparator = SEPA_NORMAL;
	rlColumnData.HorizontalSeparator = SEPA_NONE;
	rlColumnData.Alignment = COLALIGN_CENTER;

	for(i = 0; i < pomScoData->GetSize(); i++)
	{
		rlColumnData.SeparatorType = SEPA_NONE;
		rlColumnData.VerticalSeparator = SEPA_LIKEVERTICAL;
		rlColumnData.Text = CString(pomScoData->GetAt(i).Code);
		rlColumnData.EditAttrib = rlAttribC1;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = CString("");
		if(pomScoData->GetAt(i).Vpfr.GetStatus() == COleDateTime::valid)
			rlColumnData.Text = CString(pomScoData->GetAt(i).Vpfr.Format("%d.%m.%Y"));
		rlColumnData.EditAttrib = rlAttribC2;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = CString("");
		if(pomScoData->GetAt(i).Vpto.GetStatus() == COleDateTime::valid)
			rlColumnData.Text = CString(pomScoData->GetAt(i).Vpto.Format("%d.%m.%Y"));
		rlColumnData.EditAttrib = rlAttribC3;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		if(pomScoData->GetAt(i).Urno != 0)
		{
			char pclText[20];
			sprintf(pclText, "%ld", pomScoData->GetAt(i).Urno);
			rlColumnData.Text = CString(pclText);
		}
		else
		{
			rlColumnData.Text = CString("");
		}
		rlColumnData.SeparatorType = SEPA_NONE;
		rlColumnData.VerticalSeparator = SEPA_NONE;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		pomCotTable->AddTextLine(olColList, (void*)(NULL));
		olColList.DeleteAll();
	}
	for(i = pomScoData->GetSize(); i < 50; i++)
	{
		rlColumnData.SeparatorType = SEPA_NONE;
		rlColumnData.VerticalSeparator = SEPA_LIKEVERTICAL;
		rlColumnData.Text = CString("");
		rlColumnData.EditAttrib = rlAttribC1;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = CString("");
		rlColumnData.EditAttrib = rlAttribC2;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = CString("");
		rlColumnData.EditAttrib = rlAttribC3;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = CString("");
		rlColumnData.SeparatorType = SEPA_NONE;
		rlColumnData.VerticalSeparator = SEPA_NONE;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		pomCotTable->AddTextLine(olColList, (void*)(NULL));
		olColList.DeleteAll();
	}
	pomCotTable->DisplayTable();
	pomCotTable->SetColumnEditable(3, false);

//-------------------------------------------------------------------------
// Attribute f�r die einzelnen Tables setzen
// Tabelle TEA

	rlAttribC1.Format = "X(5)";
	rlAttribC1.TextMinLenght = 0;
	rlAttribC1.TextMaxLenght = 5;
	rlAttribC1.Style = ES_UPPERCASE;
	
	rlAttribC2.Type = KT_DATE;
	rlAttribC2.ChangeDay	= true;
	rlAttribC2.TextMaxLenght = 10;

	rlAttribC3.Type = KT_DATE;
	rlAttribC3.ChangeDay	= true;
	rlAttribC3.TextMaxLenght = 10;

	rlColumnData.SeparatorType = SEPA_NONE;
	rlColumnData.Font = &ogMSSansSerif_Bold_8;//ogMS_Sans_Serif_8;
	rlColumnData.VerticalSeparator = SEPA_NORMAL;
	rlColumnData.HorizontalSeparator = SEPA_NONE;
	rlColumnData.Alignment = COLALIGN_CENTER;

	for(i = 0; i < pomSteData->GetSize(); i++)
	{
		rlColumnData.SeparatorType = SEPA_NONE;
		rlColumnData.VerticalSeparator = SEPA_LIKEVERTICAL;
		rlColumnData.Text = CString(pomSteData->GetAt(i).Code);
		rlColumnData.EditAttrib = rlAttribC1;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = CString("");
		if(pomSteData->GetAt(i).Vpfr.GetStatus() == COleDateTime::valid)
			rlColumnData.Text = CString(pomSteData->GetAt(i).Vpfr.Format("%d.%m.%Y"));
		rlColumnData.EditAttrib = rlAttribC2;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = CString("");
		if(pomSteData->GetAt(i).Vpto.GetStatus() == COleDateTime::valid)
			rlColumnData.Text = CString(pomSteData->GetAt(i).Vpto.Format("%d.%m.%Y"));
		rlColumnData.EditAttrib = rlAttribC3;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		if(pomSteData->GetAt(i).Urno != 0)
		{
			char pclText[20];
			sprintf(pclText, "%ld", pomSteData->GetAt(i).Urno);
			rlColumnData.Text = CString(pclText);
		}
		else
		{
			rlColumnData.Text = CString("");
		}
		
		rlColumnData.SeparatorType = SEPA_NONE;
		rlColumnData.VerticalSeparator = SEPA_NONE;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		pomTeaTable->AddTextLine(olColList, (void*)(NULL));
		olColList.DeleteAll();
	}
	for(i = pomSteData->GetSize(); i < 50; i++)
	{
		rlColumnData.SeparatorType = SEPA_NONE;
		rlColumnData.VerticalSeparator = SEPA_LIKEVERTICAL;
		rlColumnData.Text = CString("");
		rlColumnData.EditAttrib = rlAttribC1;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = CString("");
		rlColumnData.EditAttrib = rlAttribC2;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = CString("");
		rlColumnData.EditAttrib = rlAttribC3;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = CString("");
		rlColumnData.SeparatorType = SEPA_NONE;
		rlColumnData.VerticalSeparator = SEPA_NONE;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		pomTeaTable->AddTextLine(olColList, (void*)(NULL));
		olColList.DeleteAll(); 
	} 
	pomTeaTable->DisplayTable();
	pomTeaTable->SetColumnEditable(3, false);
//-------------------------------------------------------------------------
// Attribute f�r die einzelnen Tables setzen
// Tabelle WPG

	rlAttribC1.Format = "X(5)";
	rlAttribC1.TextMinLenght = 0;
	rlAttribC1.TextMaxLenght = 5;
	rlAttribC1.Style = ES_UPPERCASE;
	
	rlAttribC2.Type = KT_DATE;
	rlAttribC2.ChangeDay	= true;
	rlAttribC2.TextMaxLenght = 10;

	rlAttribC3.Type = KT_DATE;
	rlAttribC3.ChangeDay	= true;
	rlAttribC3.TextMaxLenght = 10;

	rlColumnData.SeparatorType = SEPA_NONE;
	rlColumnData.Font = &ogMSSansSerif_Bold_8;//ogMS_Sans_Serif_8;
	rlColumnData.VerticalSeparator = SEPA_NORMAL;
	rlColumnData.HorizontalSeparator = SEPA_NONE;
	rlColumnData.Alignment = COLALIGN_CENTER;

	for(i = 0; i < pomSwgData->GetSize(); i++)
	{
		rlColumnData.SeparatorType = SEPA_NONE;
		rlColumnData.VerticalSeparator = SEPA_LIKEVERTICAL;
		rlColumnData.Text = CString(pomSwgData->GetAt(i).Code);
		rlColumnData.EditAttrib = rlAttribC1;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = CString("");
		if(pomSwgData->GetAt(i).Vpfr.GetStatus() == COleDateTime::valid)
			rlColumnData.Text = CString(pomSwgData->GetAt(i).Vpfr.Format("%d.%m.%Y"));
		rlColumnData.EditAttrib = rlAttribC2;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = CString("");
		if(pomSwgData->GetAt(i).Vpto.GetStatus() == COleDateTime::valid)
			rlColumnData.Text = CString(pomSwgData->GetAt(i).Vpto.Format("%d.%m.%Y"));
		rlColumnData.EditAttrib = rlAttribC3;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		if(pomSwgData->GetAt(i).Urno != 0)
		{
			char pclText[20];
			sprintf(pclText, "%ld", pomSwgData->GetAt(i).Urno);
			rlColumnData.Text = CString(pclText);
		}
		else
		{
			rlColumnData.Text = CString("");
		}
		
		rlColumnData.SeparatorType = SEPA_NONE;
		rlColumnData.VerticalSeparator = SEPA_NONE;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		pomWgpTable->AddTextLine(olColList, (void*)(NULL));
		olColList.DeleteAll();
	}
	for(i = pomSwgData->GetSize(); i < 50; i++)
	{
		rlColumnData.SeparatorType = SEPA_NONE;
		rlColumnData.VerticalSeparator = SEPA_LIKEVERTICAL;
		rlColumnData.Text = CString("");
		rlColumnData.EditAttrib = rlAttribC1;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = CString("");
		rlColumnData.EditAttrib = rlAttribC2;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = CString("");
		rlColumnData.EditAttrib = rlAttribC3;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = CString("");
		rlColumnData.SeparatorType = SEPA_NONE;
		rlColumnData.VerticalSeparator = SEPA_NONE;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		pomWgpTable->AddTextLine(olColList, (void*)(NULL));
		olColList.DeleteAll(); 
	} 
	pomWgpTable->DisplayTable();
	pomWgpTable->SetColumnEditable(3, false);


#ifndef CLIENT_HAJ
	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	//*** 27.08.99 SHA ***

	CString cDmy;

	if (ogACTData.ReadSpecialData(&olAct,"","URNO,ACT3,ACT5",false)== true)
	{
		for(int iAct = 0; iAct < olAct.GetSize(); iAct++)
		{
			cDmy.Format("%s(%s)",olAct[iAct].Act3,olAct[iAct].Act5);
			olActArray.Add(cDmy);
		}
	}
	if (ogALTData.ReadSpecialData(&olAlt,"","URNO,ALC2,ALC3",false)== true)
	{
		for(int iAlt = 0; iAlt < olAlt.GetSize(); iAlt++)
		{
			cDmy.Format("%s(%s)",olAlt[iAlt].Alc2,olAlt[iAlt].Alc3);
			olAltArray.Add(cDmy);
		}
	}

	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#endif

}

//----------------------------------------------------------------------------------------

void MitarbeiterStammDlg::OnOK() 
{
	AfxGetApp()->DoWaitCursor(1);
	CString olErrorText;
	bool ilStatus = true;
	if(m_FINM.GetStatus() == false) 
	{
		ilStatus = false;
		if(m_FINM.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING69) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING69) + ogNotFormat;
		}
	}
	//*** 13.09.99 SHA ***
	/*
	if(m_GSMN.GetStatus() == false) 
	{
		ilStatus = false;
		if(m_GSMN.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING638) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING638) + ogNotFormat;
		}
	}*/

	if(m_KTOU.GetStatus() == false) 
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING70) + ogNotFormat;
	}
	if(m_LANM.GetStatus() == false) 
	{
		ilStatus = false;
		if(m_LANM.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING288) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING288) + ogNotFormat;
		}
	}
/*	if(m_LINO.GetStatus() == false) 
	{
		ilStatus = false;
		if(m_LINO.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING639) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING639) + ogNotFormat;
		}
	}
	if(m_MATR.GetStatus() == false) 
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING70) + ogNotFormat;
	}
*/	if(m_PENO.GetStatus() == false) 
	{
		ilStatus = false;
		if(m_PENO.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING71) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING71) + ogNotFormat;
		}
	}
	if(m_PERC.GetStatus() == false) 
	{
		ilStatus = false;
		if(m_PERC.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING72) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING72) + ogNotFormat;
		}
	}
	if(m_SHNM.GetStatus() == false) 
	{
		ilStatus = false;
		if(m_SHNM.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING301) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING301) + ogNotFormat;
		}
	}
	if(m_DOEM_D.GetStatus() == false) 
	{
		ilStatus = false;
		if(m_DOEM_D.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING67) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING67) + ogNotFormat;
		}
	}
	if(m_DOEM_T.GetStatus() == false) 
	{
		ilStatus = false;
		if(m_DOEM_T.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING68) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING68) + ogNotFormat;
		}
	}
	if(m_DODDM_D.GetStatus() == false) 
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING65) + ogNotFormat;
	}
	if(m_DDODM_T.GetStatus() == false) 
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING66) + ogNotFormat;
	}
	if(m_TELD.GetStatus() == false) 
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING73) + ogNotFormat;
	}
	if(m_TELH.GetStatus() == false) 
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING74) + ogNotFormat;
	}
	if(m_TELP.GetStatus() == false) 
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING75) + ogNotFormat;
	}
	if(m_ZUTB.GetStatus() == false) 
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING76) + ogNotFormat;
	}
	if(m_ZUTF.GetStatus() == false) 
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING77) + ogNotFormat;
	}
	if(m_ZUTN.GetStatus() == false) 
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING78) + ogNotFormat;
	}
	if(m_ZUTW.GetStatus() == false) 
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING79) + ogNotFormat;
	}

	CString olTableErrorText;
	int i;

	for(i = 0; i < 50; i++)
	{
		int ilFilledCells = 0;
		bool blLineOK = true;
		char pclZeile[10], pclSpalte[10];
		CCSPtrArray<TABLE_COLUMN> olLine;
		//////////////////////////////////////////////////////////////////////////////////////////
		pomOrgTable->GetTextLineColumns(&olLine, i);
		for(int k = 0; k < olLine.GetSize()-1; k++)
		{
			if(olLine[k].Text != "")
			{
				ilFilledCells++;
			}
			sprintf(pclZeile, "%d", i+1);
			blLineOK = pomOrgTable->GetCellStatus(i, k);
			if(blLineOK == false)
			{
				ilStatus = false;
				sprintf(pclSpalte, "%d", k+1);
				if(k == 0 || k == 1 || k == 2)
				{
					olTableErrorText += LoadStg(IDS_STRING80) + LoadStg(IDS_STRING85) + CString(pclZeile) + LoadStg(IDS_STRING86) + CString(pclSpalte) + LoadStg(IDS_STRING87);
				}
			}
		}
		if(ilFilledCells != 0 && ilFilledCells < 2 && olLine[1].Text != CString(""))
		{
			ilStatus = false;
			olTableErrorText += LoadStg(IDS_STRING80) + LoadStg(IDS_STRING85) + CString(pclZeile) + LoadStg(IDS_STRING88);
		}

		//*** 30.09.99 SHA ***
		//*** CHECK IF THERE IS ANY ORG WITHOUT VALID FROM DATE ***
		if (olLine[0].Text != CString("") && olLine[1].Text == CString(""))
		{
			olTableErrorText += CString("<") + CString(pclZeile) + CString(">") + LoadStg(IDS_STRING723);
			ilStatus = false;
		}

		if(olLine[1].Text != CString("") && olLine[2].Text != CString(""))
		{
			COleDateTime olFrom = OleDateStringToDate(olLine[1].Text);
			COleDateTime olTo = OleDateStringToDate(olLine[2].Text);


			if(olFrom.GetStatus() == COleDateTime::invalid || olTo.GetStatus() == COleDateTime::invalid)
			{
				ilStatus = false;
				olTableErrorText += LoadStg(IDS_STRING80) + LoadStg(IDS_STRING89) + CString(pclZeile);
			}
			else
			{
				if(olTo < olFrom)
				{
					ilStatus = false;
					olTableErrorText += LoadStg(IDS_STRING80) + LoadStg(IDS_STRING85) + CString(pclZeile) + LoadStg(IDS_STRING90);
					pomOrgTable->SetTextColumnColor(i, 1, COLORREF(RED), COLORREF(WHITE));
					pomOrgTable->SetTextColumnColor(i, 2, COLORREF(RED), COLORREF(WHITE));
				}
			}
		}
		olLine.RemoveAll();
	}
	for(i = 0; i < 50; i++)
	{
		bool blLineOK = true;
		int ilFilledCells = 0;
		char pclZeile[10], pclSpalte[10];
		CCSPtrArray<TABLE_COLUMN> olLine;
		pomPfcTable->GetTextLineColumns(&olLine, i);
		for( int k = 0; k < olLine.GetSize()-1; k++)
		{
			if(olLine[k].Text != "")
			{
				ilFilledCells++;
			}
			sprintf(pclZeile, "%d", i+1);
			blLineOK = pomPfcTable->GetCellStatus(i, k);
			if(blLineOK == false)
			{
				ilStatus = false;
				sprintf(pclSpalte, "%d", k+1);
				if(k == 0 || k == 1 || k == 2)
				{
					olTableErrorText += LoadStg(IDS_STRING81) + LoadStg(IDS_STRING85) + CString(pclZeile) + LoadStg(IDS_STRING86) + CString(pclSpalte) + LoadStg(IDS_STRING87);
				}
			}
		}
		if(ilFilledCells != 0 && ilFilledCells < 2 && olLine[1].Text != CString(""))
		{
			ilStatus = false;
			olTableErrorText += LoadStg(IDS_STRING81) + LoadStg(IDS_STRING85) + CString(pclZeile) + LoadStg(IDS_STRING88);
		}

		//*** 30.09.99 SHA ***
		//*** CHECK IF THERE IS ANY FUNCTION WITHOUT VALID FROM DATE ***
		if (olLine[0].Text != CString("") && olLine[1].Text == CString(""))
		{
			olTableErrorText += CString("<") + CString(pclZeile) + CString(">") + LoadStg(IDS_STRING724);
			ilStatus = false;
		}

		if(olLine[1].Text != CString("") && olLine[2].Text != CString(""))
		{
			COleDateTime olFrom = OleDateStringToDate(olLine[1].Text);
			COleDateTime olTo = OleDateStringToDate(olLine[2].Text);
			if(olFrom.GetStatus() == COleDateTime::invalid || olTo.GetStatus() == COleDateTime::invalid)
			{
				ilStatus = false;
				olTableErrorText += LoadStg(IDS_STRING81) + LoadStg(IDS_STRING89) + CString(pclZeile);
			}
			else
			{
				if(olTo < olFrom)
				{
					ilStatus = false;
					olTableErrorText += LoadStg(IDS_STRING81) + LoadStg(IDS_STRING85) + CString(pclZeile) + LoadStg(IDS_STRING90);
					pomPfcTable->SetTextColumnColor(i, 1, COLORREF(RED), COLORREF(WHITE));
					pomPfcTable->SetTextColumnColor(i, 2, COLORREF(RED), COLORREF(WHITE));
				}
			}
		}
		olLine.RemoveAll();
	}
	for( i = 0; i < 50; i++)
	{
		int ilFilledCells = 0;
		bool blLineOK = true;
		char pclZeile[10], pclSpalte[10];
		CCSPtrArray<TABLE_COLUMN> olLine;
		olLine.DeleteAll();
		pomPerTable->GetTextLineColumns(&olLine, i);
		for(int k = 0; k < olLine.GetSize()-1; k++)
		{
			if(olLine[k].Text != "")
			{
				ilFilledCells++;
			}
			sprintf(pclZeile, "%d", i+1);
			blLineOK = pomPerTable->GetCellStatus(i, k);
			if(blLineOK == false)
			{
				ilStatus = false;
				sprintf(pclSpalte, "%d", k+1);
				if(k == 0 || k == 1 || k == 2)
				{
					olTableErrorText += LoadStg(IDS_STRING82) + LoadStg(IDS_STRING85) + CString(pclZeile) + LoadStg(IDS_STRING86) + CString(pclSpalte) + LoadStg(IDS_STRING87);
				}
			}
		}
		if(ilFilledCells != 0 && ilFilledCells < 2 && olLine[1].Text != CString(""))
		{
			ilStatus = false;
			olTableErrorText += LoadStg(IDS_STRING82) + LoadStg(IDS_STRING85) + CString(pclZeile) + LoadStg(IDS_STRING88);
		}

		//*** 30.09.99 SHA ***
		//*** CHECK IF THERE IS ANY QUALIFICATION WITHOUT VALID FROM DATE ***
		if (olLine[0].Text != CString("") && olLine[1].Text == CString(""))
		{
			olTableErrorText += CString("<") + CString(pclZeile) + CString(">") + LoadStg(IDS_STRING725);
			ilStatus = false;
		}


		if(olLine[1].Text != CString("") && olLine[2].Text != CString(""))
		{
			COleDateTime olFrom = OleDateStringToDate(olLine[1].Text);
			COleDateTime olTo = OleDateStringToDate(olLine[2].Text);
			if(olFrom.GetStatus() == COleDateTime::invalid || olTo.GetStatus() == COleDateTime::invalid)
			{
				ilStatus = false;
				olTableErrorText += LoadStg(IDS_STRING82) + LoadStg(IDS_STRING89) + CString(pclZeile);
			}
			else
			{
				if(olTo < olFrom)
				{
					ilStatus = false;
					olTableErrorText += LoadStg(IDS_STRING82) + LoadStg(IDS_STRING85) + CString(pclZeile) + LoadStg(IDS_STRING90);
					pomPerTable->SetTextColumnColor(i, 1, COLORREF(RED), COLORREF(WHITE));
					pomPerTable->SetTextColumnColor(i, 2, COLORREF(RED), COLORREF(WHITE));
				}
			}
		}
		olLine.RemoveAll();
	}
	for( i = 0; i < 50; i++)
	{
		bool blLineOK = true;
		char pclZeile[10], pclSpalte[10];
		int ilFilledCells = 0;
		CCSPtrArray<TABLE_COLUMN> olLine;
		olLine.DeleteAll();
		pomCotTable->GetTextLineColumns(&olLine, i);
		for(int k = 0; k < olLine.GetSize()-1; k++)
		{
			if(olLine[k].Text != "")
			{
				ilFilledCells++;
			}
			sprintf(pclZeile, "%d", i+1);
			blLineOK = pomCotTable->GetCellStatus(i, k);
			if(blLineOK == false)
			{
				ilStatus = false;
				sprintf(pclSpalte, "%d", k+1);
				if(k == 0 || k == 1 || k == 2)
				{
					olTableErrorText += LoadStg(IDS_STRING83) + LoadStg(IDS_STRING85) + CString(pclZeile) + LoadStg(IDS_STRING86) + CString(pclSpalte) + LoadStg(IDS_STRING87);
				}
			}
		}
		if(ilFilledCells != 0 && ilFilledCells < 2 && olLine[1].Text != CString(""))
		{
			ilStatus = false;
			olTableErrorText += LoadStg(IDS_STRING83) + LoadStg(IDS_STRING85) + CString(pclZeile) + LoadStg(IDS_STRING88);
		}
		if(olLine[1].Text != CString("") && olLine[2].Text != CString(""))
		{
			COleDateTime olFrom = OleDateStringToDate(olLine[1].Text);
			COleDateTime olTo = OleDateStringToDate(olLine[2].Text);
			if(olFrom.GetStatus() == COleDateTime::invalid || olTo.GetStatus() == COleDateTime::invalid)
			{
				ilStatus = false;
				olTableErrorText += LoadStg(IDS_STRING83) + LoadStg(IDS_STRING89) + CString(pclZeile);
			}
			else
			{
				if(olTo < olFrom)
				{
					ilStatus = false;
					olTableErrorText += LoadStg(IDS_STRING83) + LoadStg(IDS_STRING85) + CString(pclZeile) + LoadStg(IDS_STRING90);
					pomCotTable->SetTextColumnColor(i, 1, COLORREF(RED), COLORREF(WHITE));
					pomCotTable->SetTextColumnColor(i, 2, COLORREF(RED), COLORREF(WHITE));
				}
			}
		}
		olLine.RemoveAll();
	}
	for( i = 0; i < 50; i++)
	{

		int ilFilledCells = 0;
		bool blLineOK = true;
		char pclZeile[10], pclSpalte[10];
		CCSPtrArray<TABLE_COLUMN> olLine;
		pomTeaTable->GetTextLineColumns(&olLine, i);

		for(int k = 0; k < olLine.GetSize()-1; k++)
		{
			if(olLine[k].Text != "")
			{
				ilFilledCells++;
			}
			sprintf(pclZeile, "%d", i+1);
			blLineOK = pomTeaTable->GetCellStatus(i, k);
			if(blLineOK == false)
			{
				ilStatus = false;
				sprintf(pclSpalte, "%d", k+1);
				if(k == 0 || k == 1 || k == 2)
				{
					olTableErrorText += LoadStg(IDS_STRING84) + LoadStg(IDS_STRING85) + CString(pclZeile) + LoadStg(IDS_STRING86) + CString(pclSpalte) + LoadStg(IDS_STRING87);
				}
			}
		}
		if(ilFilledCells != 0 && ilFilledCells < 2 && olLine[1].Text != CString(""))
		{
			ilStatus = false;
			olTableErrorText += LoadStg(IDS_STRING84) + LoadStg(IDS_STRING85) + CString(pclZeile) + LoadStg(IDS_STRING88);
		}
		if(olLine[1].Text != CString("") && olLine[2].Text != CString(""))
		{
			COleDateTime olFrom = OleDateStringToDate(olLine[1].Text);
			COleDateTime olTo = OleDateStringToDate(olLine[2].Text);
			if(olFrom.GetStatus() == COleDateTime::invalid || olTo.GetStatus() == COleDateTime::invalid)
			{
				ilStatus = false;
				olTableErrorText += LoadStg(IDS_STRING84) + LoadStg(IDS_STRING89) + CString(pclZeile);
			}
			else
			{
				if(olTo < olFrom)
				{
					ilStatus = false;
					olTableErrorText += LoadStg(IDS_STRING84) + LoadStg(IDS_STRING85) + CString(pclZeile) + LoadStg(IDS_STRING90);
					pomTeaTable->SetTextColumnColor(i, 1, COLORREF(RED), COLORREF(WHITE));
					pomTeaTable->SetTextColumnColor(i, 2, COLORREF(RED), COLORREF(WHITE));
				}
			}
		}
		olLine.RemoveAll();
	}	

	for( i = 0; i < 50; i++)
	{

		int ilFilledCells = 0;
		bool blLineOK = true;
		char pclZeile[10], pclSpalte[10];
		CCSPtrArray<TABLE_COLUMN> olLine;
		pomWgpTable->GetTextLineColumns(&olLine, i);

		for(int k = 0; k < olLine.GetSize()-1; k++)
		{
			if(olLine[k].Text != "")
			{
				ilFilledCells++;
			}
			sprintf(pclZeile, "%d", i+1);
			blLineOK = pomWgpTable->GetCellStatus(i, k);
			if(blLineOK == false)
			{
				ilStatus = false;
				sprintf(pclSpalte, "%d", k+1);
				if(k == 0 || k == 1 || k == 2)
				{
					olTableErrorText += LoadStg(IDS_STRING116) + LoadStg(IDS_STRING85) + CString(pclZeile) + LoadStg(IDS_STRING86) + CString(pclSpalte) + LoadStg(IDS_STRING87);
				}
			}
		}
		if(ilFilledCells != 0 && ilFilledCells < 2 && olLine[1].Text != CString(""))
		{
			ilStatus = false;
			olTableErrorText += LoadStg(IDS_STRING116) + LoadStg(IDS_STRING85) + CString(pclZeile) + LoadStg(IDS_STRING88);
		}
		if(olLine[1].Text != CString("") && olLine[2].Text != CString(""))
		{
			COleDateTime olFrom = OleDateStringToDate(olLine[1].Text);
			COleDateTime olTo = OleDateStringToDate(olLine[2].Text);
			if(olFrom.GetStatus() == COleDateTime::invalid || olTo.GetStatus() == COleDateTime::invalid)
			{
				ilStatus = false;
				olTableErrorText += LoadStg(IDS_STRING116) + LoadStg(IDS_STRING89) + CString(pclZeile);
			}
			else
			{
				if(olTo < olFrom)
				{
					ilStatus = false;
					olTableErrorText += LoadStg(IDS_STRING116) + LoadStg(IDS_STRING85) + CString(pclZeile) + LoadStg(IDS_STRING90);
					pomWgpTable->SetTextColumnColor(i, 1, COLORREF(RED), COLORREF(WHITE));
					pomWgpTable->SetTextColumnColor(i, 2, COLORREF(RED), COLORREF(WHITE));
				}
			}
		}
		olLine.RemoveAll();
	}	

	int ilSorCount = 0;
	int ilSpfCount = 0;
	int ilSpeCount = 0;
	int ilScoCount = 0;
	int ilSteCount = 0;
	int ilSwgCount = 0;
	if(ilStatus == true)
	{
		for( i = 0; i < 50; i++)
		{
			bool blLineOK = true;
			CCSPtrArray<TABLE_COLUMN> olLine;
			//////////////////////////////////////////////////////////////////////////////////////////
			pomOrgTable->GetTextLineColumns(&olLine, i);
			for(int k = 0; k < olLine.GetSize(); k++)
			{
				blLineOK = pomOrgTable->GetCellStatus(i, k);
			}
			if(blLineOK == true)
			{
				if(olLine.GetSize() > 2)
				{
					if((olLine[0].Text != CString("")) && (olLine[1].Text != CString("")))
					{
						if(olLine[3].Text == CString(""))
						{
							SORDATA *prlSor = new SORDATA;
							strcpy(prlSor->Code, olLine[0].Text);
							prlSor->Vpfr = OleDateStringToDate(olLine[1].Text);
							prlSor->Vpto = OleDateStringToDate(olLine[2].Text);
							prlSor->IsChanged = DATA_NEW;
							pomSorData->Add(prlSor);
						}
						else
						{
							int ilTmpUrno = atoi(olLine[3].Text);
							//Nach �nderungen Checken
							bool blFound = false;
							for(int j = 0; j < pomSorData->GetSize(); j ++)
							{
								if(ilTmpUrno == pomSorData->GetAt(j).Urno)
								{
									SORDATA *prlSor = ogSorData.GetSorByUrno(pomSorData->GetAt(j).Urno);
									CString olTmpVpfr = pomSorData->GetAt(j).Vpfr.Format("%d.%m.%Y");
									CString olTmpVpto = pomSorData->GetAt(j).Vpto.Format("%d.%m.%Y");
									if((olLine[0].Text != CString(pomSorData->GetAt(j).Code))  ||
									   (olLine[1].Text != olTmpVpfr) || (olLine[2].Text != olTmpVpto))
									{
										if(prlSor != NULL)
										{
											strcpy(prlSor->Code, olLine[0].Text);
											prlSor->Vpfr = OleDateStringToDate(olLine[1].Text);
											prlSor->Vpto = OleDateStringToDate(olLine[2].Text);
											prlSor->IsChanged = DATA_CHANGED;
											ogSorData.UpdateInternal(prlSor);
											blFound = true;
											break;
										}
									}
									else
									{
										prlSor->IsChanged = DATA_UNCHANGED;
										blFound = true;
										break;
									}
								}
							}
						}
					}//Alle Columns sind gef�llt
					else
					{
						if((olLine[0].Text == CString("")) && (olLine[1].Text == CString("")))
						{
							if(olLine[3].Text != CString(""))
							{
								int ilTmpUrno = atoi(olLine[3].Text);
								bool blFound = false;
								for(int j = 0; j < pomSorData->GetSize(); j ++)
								{
									if(ilTmpUrno == pomSorData->GetAt(j).Urno)
									{
										SORDATA *prlSor = ogSorData.GetSorByUrno(pomSorData->GetAt(j).Urno);
										if(prlSor != NULL)
										{
											prlSor->IsChanged = DATA_DELETED;
										}
										break;
									}
								}
							}
						}
					}
				}
			}
			else
			{
				ilSorCount++;
			}
			olLine.RemoveAll();
			//////////////////////////////////////////////////////////////////////////////////////////
			pomPfcTable->GetTextLineColumns(&olLine, i);
			for(k = 0; k < olLine.GetSize(); k++)
			{
				blLineOK = pomPfcTable->GetCellStatus(i, k);
			}
			if(blLineOK == true)
			{
				if(olLine.GetSize() > 2)
				{
					if((olLine[0].Text != CString("")) && (olLine[1].Text != CString("")))
					{
						if(olLine[3].Text == CString(""))
						{
							SPFDATA *prlSpf = new SPFDATA;
							strcpy(prlSpf->Code, olLine[0].Text);
							prlSpf->Vpfr = OleDateStringToDate(olLine[1].Text);
							prlSpf->Vpto = OleDateStringToDate(olLine[2].Text);
#ifndef CLIENT_HAJ
							//*** 30.08.99 SHA ***
							strcpy(prlSpf->Act3, olLine[4].Text);
							strcpy(prlSpf->Act5, olLine[5].Text);
							strcpy(prlSpf->Alc2, olLine[6].Text);
							strcpy(prlSpf->Alc3, olLine[7].Text);

							//*** 02.11.99 SHA ***
							CString olTmp0 = olLine[0].Text;
							CString olTmp5 = olLine[5].Text;
							CString olTmp7 = olLine[7].Text;

							if (olTmp5 == CString(" ")) //*** SPACE ! ***
								olTmp5 = "*";
							if (olTmp7 == CString(" ")) //*** SPACE ! ***
								olTmp7 = "*";
							
							CString olCheck = olTmp0 + "_" +  olTmp5 + "_" + olTmp7;

							int ilTmpCount = SearchFctc(olCheck);

							if (ilTmpCount==0) SaveNewFctc(olCheck);
							//*** END 02.11.99 SHA ***


#endif

							prlSpf->IsChanged = DATA_NEW;
							pomSpfData->Add(prlSpf);
						}
						else
						{
							int ilTmpUrno = atoi(olLine[3].Text);
							//Nach �nderungen Checken
							bool blFound = false;
							for(int j = 0; j < pomSpfData->GetSize(); j ++)
							{
								if(ilTmpUrno == pomSpfData->GetAt(j).Urno)
								{
									SPFDATA *prlSpf = ogSpfData.GetSpfByUrno(pomSpfData->GetAt(j).Urno);
									CString olTmpVpfr = pomSpfData->GetAt(j).Vpfr.Format("%d.%m.%Y");
									CString olTmpVpto = pomSpfData->GetAt(j).Vpto.Format("%d.%m.%Y");

#ifdef CLIENT_HAJ
									if((olLine[0].Text != CString(pomSpfData->GetAt(j).Code))  ||
									   (olLine[1].Text != olTmpVpfr) || (olLine[2].Text != olTmpVpto))
									{
										if(prlSpf != NULL)
										{
											strcpy(prlSpf->Code, olLine[0].Text);
											prlSpf->Vpfr = OleDateStringToDate(olLine[1].Text);
											prlSpf->Vpto = OleDateStringToDate(olLine[2].Text);

											prlSpf->IsChanged = DATA_CHANGED;
											ogSpfData.UpdateInternal(prlSpf);
											blFound = true;
											break;
										}
									}
									else
									{
										prlSpf->IsChanged = DATA_UNCHANGED;
										blFound = true;
										break;
									}
#else
									//*** 30.08.99 SHA ***
									CString olTmpAct3 = pomSpfData->GetAt(j).Act3;
									CString olTmpAct5 = pomSpfData->GetAt(j).Act5;
									CString olTmpAlc2 = pomSpfData->GetAt(j).Alc2;
									CString olTmpAlc3 = pomSpfData->GetAt(j).Alc3;
									
									if((olLine[0].Text != CString(pomSpfData->GetAt(j).Code))  ||
									   (olLine[1].Text != olTmpVpfr) || (olLine[2].Text != olTmpVpto) ||
									   (olLine[4].Text != olTmpAct3) || (olLine[5].Text != olTmpAct5) || 
									   (olLine[6].Text != olTmpAlc2) || (olLine[7].Text != olTmpAlc3)									   
									   )
									{
										if(prlSpf != NULL)
										{
											strcpy(prlSpf->Code, olLine[0].Text);
											prlSpf->Vpfr = OleDateStringToDate(olLine[1].Text);
											prlSpf->Vpto = OleDateStringToDate(olLine[2].Text);

											//*** 30.08.99 SHA ***
											strcpy(prlSpf->Act3, olLine[4].Text);
											strcpy(prlSpf->Act5, olLine[5].Text);
											strcpy(prlSpf->Alc2, olLine[6].Text);
											strcpy(prlSpf->Alc3, olLine[7].Text);

											
											//*** 02.11.99 SHA ***
											CString olTmp0 = olLine[0].Text;
											CString olTmp5 = olLine[5].Text;
											CString olTmp7 = olLine[7].Text;

											if (olTmp5 == CString(" ")) //*** SPACE ! ***
												olTmp5 = "*";
											if (olTmp7 == CString(" ")) //*** SPACE ! ***
												olTmp7 = "*";
											/*
											CString olCheck = olTmp0 + "_" +  olTmp5 + "_" + olTmp7;

											int ilTmpCount = SearchFctc(olCheck);

											if (ilTmpCount==0) SaveNewFctc(olCheck);
											//*** END 02.11.99 SHA ***
*/
											prlSpf->IsChanged = DATA_CHANGED;
											ogSpfData.UpdateInternal(prlSpf);
											blFound = true;
											break;
										}
									}
									else
									{
										prlSpf->IsChanged = DATA_UNCHANGED;
										blFound = true;
										break;
									}
#endif
								}
							}
						}
					}//Alle Columns sind gef�llt
					else
					{
						if((olLine[0].Text == CString("")) && (olLine[1].Text == CString("")))
						{
							if(olLine[3].Text != CString(""))
							{
								int ilTmpUrno = atoi(olLine[3].Text);
								bool blFound = false;
								for(int j = 0; j < pomSpfData->GetSize(); j ++)
								{
									if(ilTmpUrno == pomSpfData->GetAt(j).Urno)
									{
										SPFDATA *prlSpf = ogSpfData.GetSpfByUrno(pomSpfData->GetAt(j).Urno);
										if(prlSpf != NULL)
										{ 
											prlSpf->IsChanged = DATA_DELETED;
										}
										break;
									}
								}
							}
						}
					}
				}
			}
			else
			{
				ilSpfCount++;
			}
			olLine.RemoveAll();
			//////////////////////////////////////////////////////////////////////////////////////////
			pomPerTable->GetTextLineColumns(&olLine, i);
			for(k = 0; k < olLine.GetSize(); k++)
			{
				blLineOK = pomPerTable->GetCellStatus(i, k);
			}
			if(blLineOK == true)
			{
				if(olLine.GetSize() > 2)
				{
					if((olLine[0].Text != CString("")) && (olLine[1].Text != CString("")))
					{
						if(olLine[3].Text == CString(""))
						{
							SPEDATA *prlSpe = new SPEDATA;
							strcpy(prlSpe->Code, olLine[0].Text);
							prlSpe->Vpfr = OleDateStringToDate(olLine[1].Text);
							prlSpe->Vpto = OleDateStringToDate(olLine[2].Text);

							prlSpe->IsChanged = DATA_NEW;
							pomSpeData->Add(prlSpe);
						}
						else
						{
							int ilTmpUrno = atoi(olLine[3].Text);
							//Nach �nderungen Checken
							bool blFound = false;
							for(int j = 0; j < pomSpeData->GetSize(); j ++)
							{
								if(ilTmpUrno == pomSpeData->GetAt(j).Urno)
								{
									SPEDATA *prlSpe = ogSpeData.GetSpeByUrno(pomSpeData->GetAt(j).Urno);
									CString olTmpVpfr = pomSpeData->GetAt(j).Vpfr.Format("%d.%m.%Y");
									CString olTmpVpto = pomSpeData->GetAt(j).Vpto.Format("%d.%m.%Y");

									if((olLine[0].Text != CString(pomSpeData->GetAt(j).Code))  ||
									   (olLine[1].Text != olTmpVpfr) || (olLine[2].Text != olTmpVpto))
									{
										if(prlSpe != NULL)
										{
											strcpy(prlSpe->Code, olLine[0].Text);
											prlSpe->Vpfr = OleDateStringToDate(olLine[1].Text);
											prlSpe->Vpto = OleDateStringToDate(olLine[2].Text);

											
											prlSpe->IsChanged = DATA_CHANGED;
											ogSpeData.UpdateInternal(prlSpe);
											blFound = true;
											break;
										}
									}
									else
									{
										prlSpe->IsChanged = DATA_UNCHANGED;
										blFound = true;
										break;
									}
								}
							}
						}
					}//Alle Columns sind gef�llt
					else
					{
						if((olLine[0].Text == CString("")) && (olLine[1].Text == CString("")) /*&& (olLine[2].Text == CString(""))*/)
						{
							if(olLine[3].Text != CString(""))
							{
								int ilTmpUrno = atoi(olLine[3].Text);
								bool blFound = false;
								for(int j = 0; j < pomSpeData->GetSize(); j ++)
								{
									if(ilTmpUrno == pomSpeData->GetAt(j).Urno)
									{
										SPEDATA *prlSpe = ogSpeData.GetSpeByUrno(pomSpeData->GetAt(j).Urno);
										if(prlSpe != NULL)
										{
											prlSpe->IsChanged = DATA_DELETED;
										}
										break;
									}
								}
							}
						}
					}
				}
			}
			else
			{
				ilSpeCount++;
			}
			olLine.RemoveAll();
			//////////////////////////////////////////////////////////////////////////////////////////
			pomCotTable->GetTextLineColumns(&olLine, i);
			for(k = 0; k < olLine.GetSize(); k++)
			{
				blLineOK = pomCotTable->GetCellStatus(i, k);
			}
			if(blLineOK == true)
			{
				if(olLine.GetSize() > 2)
				{
					if((olLine[0].Text != CString("")) && (olLine[1].Text != CString("")))
					{
						if(olLine[3].Text == CString(""))
						{
							SCODATA *prlSco = new SCODATA;
							strcpy(prlSco->Code, olLine[0].Text);
							prlSco->Vpfr = OleDateStringToDate(olLine[1].Text);
							prlSco->Vpto = OleDateStringToDate(olLine[2].Text);
							prlSco->IsChanged = DATA_NEW;
							pomScoData->Add(prlSco);
						}
						else
						{
							int ilTmpUrno = atoi(olLine[3].Text);
							//Nach �nderungen Checken
							bool blFound = false;
							for(int j = 0; j < pomScoData->GetSize(); j ++)
							{
								if(ilTmpUrno == pomScoData->GetAt(j).Urno)
								{
									SCODATA *prlSco = ogScoData.GetScoByUrno(pomScoData->GetAt(j).Urno);
									CString olTmpVpfr = pomScoData->GetAt(j).Vpfr.Format("%d.%m.%Y");
									CString olTmpVpto = pomScoData->GetAt(j).Vpto.Format("%d.%m.%Y");
									if((olLine[0].Text != CString(pomScoData->GetAt(j).Code))  ||
									   (olLine[1].Text != olTmpVpfr) || (olLine[2].Text != olTmpVpto))
									{
										if(prlSco != NULL)
										{
											strcpy(prlSco->Code, olLine[0].Text);
											prlSco->Vpfr = OleDateStringToDate(olLine[1].Text);
											prlSco->Vpto = OleDateStringToDate(olLine[2].Text);
											prlSco->IsChanged = DATA_CHANGED;
											ogScoData.UpdateInternal(prlSco);
											blFound = true;
											break;
										}
									}
									else
									{
										prlSco->IsChanged = DATA_UNCHANGED;
										blFound = true;
										break;
									}
								}
							}
						}
					}//Alle Columns sind gef�llt
					else
					{
						if((olLine[0].Text == CString("")) && (olLine[1].Text == CString("")))
						{
							if(olLine[3].Text != CString(""))
							{
								int ilTmpUrno = atoi(olLine[3].Text);
								bool blFound = false;
								for(int j = 0; j < pomScoData->GetSize(); j ++)
								{
									if(ilTmpUrno == pomScoData->GetAt(j).Urno)
									{
										SCODATA *prlSco = ogScoData.GetScoByUrno(pomScoData->GetAt(j).Urno);
										if(prlSco != NULL)
										{
											prlSco->IsChanged = DATA_DELETED;
										}
										break;
									}
								}
							}
						}
					}
				}
			}
			else
			{
				ilScoCount++;
			}
			olLine.RemoveAll();
			//////////////////////////////////////////////////////////////////////////////////////////

			pomTeaTable->GetTextLineColumns(&olLine, i);
			for(k = 0; k < olLine.GetSize(); k++)
			{
				blLineOK = pomTeaTable->GetCellStatus(i, k);
			}
			if(blLineOK == true)
			{
				if(olLine.GetSize() > 2)
				{
					if((olLine[0].Text != CString("")) && (olLine[1].Text != CString("")))
					{
						if(olLine[3].Text == CString(""))
						{
							STEDATA *prlSte = new STEDATA;
							strcpy(prlSte->Code, olLine[0].Text);
							prlSte->Vpfr = OleDateStringToDate(olLine[1].Text);
							prlSte->Vpto = OleDateStringToDate(olLine[2].Text);
							prlSte->IsChanged = DATA_NEW;
							pomSteData->Add(prlSte);
						}
						else
						{
							int ilTmpUrno = atoi(olLine[3].Text);
							//Nach �nderungen Checken
							bool blFound = false;
							for(int j = 0; j < pomSteData->GetSize(); j ++)
							{
								if(ilTmpUrno == pomSteData->GetAt(j).Urno)
								{
									STEDATA *prlSte = ogSteData.GetSteByUrno(pomSteData->GetAt(j).Urno);
									CString olTmpVpfr = pomSteData->GetAt(j).Vpfr.Format("%d.%m.%Y");
									CString olTmpVpto = pomSteData->GetAt(j).Vpto.Format("%d.%m.%Y");
									if((olLine[0].Text != CString(pomSteData->GetAt(j).Code))  ||
									   (olLine[1].Text != olTmpVpfr) || (olLine[2].Text != olTmpVpto))
									{
										if(prlSte != NULL)
										{
											strcpy(prlSte->Code, olLine[0].Text);
											prlSte->Vpfr = OleDateStringToDate(olLine[1].Text);
											prlSte->Vpto = OleDateStringToDate(olLine[2].Text);
											prlSte->IsChanged = DATA_CHANGED;
											ogSteData.UpdateInternal(prlSte);
											blFound = true;
											break;
										}
									}
									else
									{
										prlSte->IsChanged = DATA_UNCHANGED;
										blFound = true;
										break;
									}
								}
							}
						}
					}//Alle Columns sind gef�llt
					else
					{
						if((olLine[0].Text == CString("")) && (olLine[1].Text == CString("")))
						{
							if(olLine[3].Text != CString(""))
							{
								int ilTmpUrno = atoi(olLine[3].Text);
								bool blFound = false;
								for(int j = 0; j < pomSteData->GetSize(); j ++)
								{
									if(ilTmpUrno == pomSteData->GetAt(j).Urno)
									{
										STEDATA *prlSte = ogSteData.GetSteByUrno(pomSteData->GetAt(j).Urno);
										if(prlSte != NULL)
										{
											prlSte->IsChanged = DATA_DELETED;
										}
										break;
									}
								}
							}
						}
					}
				}
			}
			else
			{
				ilSteCount++;
			}
			olLine.RemoveAll();
			//////////////////////////////////////////////////////////////////////////////////////////
			pomWgpTable->GetTextLineColumns(&olLine, i);
			for(k = 0; k < olLine.GetSize(); k++)
			{
				blLineOK = pomWgpTable->GetCellStatus(i, k);
			}
			if(blLineOK == true)
			{
				if(olLine.GetSize() > 2)
				{
					if((olLine[0].Text != CString("")) && (olLine[1].Text != CString("")))
					{
						if(olLine[3].Text == CString(""))
						{
							SWGDATA *prlSwg = new SWGDATA;
							strcpy(prlSwg->Code, olLine[0].Text);
							prlSwg->Vpfr = OleDateStringToDate(olLine[1].Text);
							prlSwg->Vpto = OleDateStringToDate(olLine[2].Text);
							prlSwg->IsChanged = DATA_NEW;
							pomSwgData->Add(prlSwg);
						}
						else
						{
							int ilTmpUrno = atoi(olLine[3].Text);
							//Nach �nderungen Checken
							bool blFound = false;
							for(int j = 0; j < pomSwgData->GetSize(); j ++)
							{
								if(ilTmpUrno == pomSwgData->GetAt(j).Urno)
								{
									SWGDATA *prlSwg = ogSwgData.GetSwgByUrno(pomSwgData->GetAt(j).Urno);
									CString olTmpVpfr = pomSwgData->GetAt(j).Vpfr.Format("%d.%m.%Y");
									CString olTmpVpto = pomSwgData->GetAt(j).Vpto.Format("%d.%m.%Y");
									if((olLine[0].Text != CString(pomSwgData->GetAt(j).Code))  ||
									   (olLine[1].Text != olTmpVpfr) || (olLine[2].Text != olTmpVpto))
									{
										if(prlSwg != NULL)
										{
											strcpy(prlSwg->Code, olLine[0].Text);
											prlSwg->Vpfr = OleDateStringToDate(olLine[1].Text);
											prlSwg->Vpto = OleDateStringToDate(olLine[2].Text);
											prlSwg->IsChanged = DATA_CHANGED;
											ogSwgData.UpdateInternal(prlSwg);
											blFound = true;
											break;
										}
									}
									else
									{
										prlSwg->IsChanged = DATA_UNCHANGED;
										blFound = true;
										break;
									}
								}
							}
						}
					}//Alle Columns sind gef�llt
					else
					{
						if((olLine[0].Text == CString("")) && (olLine[1].Text == CString("")))
						{
							if(olLine[3].Text != CString(""))
							{
								int ilTmpUrno = atoi(olLine[3].Text);
								bool blFound = false;
								for(int j = 0; j < pomSwgData->GetSize(); j ++)
								{
									if(ilTmpUrno == pomSwgData->GetAt(j).Urno)
									{
										SWGDATA *prlSwg = ogSwgData.GetSwgByUrno(pomSwgData->GetAt(j).Urno);
										if(prlSwg != NULL)
										{
											prlSwg->IsChanged = DATA_DELETED;
										}
										break;
									}
								}
							}
						}
					}
				}
			}
			else
			{
				ilSwgCount++;
			}
			olLine.RemoveAll();
		}
	}
	bool blSoftStatus = true;
	if(ilSorCount != 0)
	{
		blSoftStatus = false;
		olErrorText += CString("\n") + LoadStg(IDS_STRING80) + LoadStg(IDS_STRING91);
	}
	if(ilSpfCount != 0)
	{
		blSoftStatus = false;
		olErrorText += CString("\n") + LoadStg(IDS_STRING81) + LoadStg(IDS_STRING91);
	}
	if(ilSpeCount != 0)
	{
		blSoftStatus = false;
		olErrorText += CString("\n") + LoadStg(IDS_STRING82) + LoadStg(IDS_STRING91);
	}
	if(ilScoCount != 0)
	{
		blSoftStatus = false;
		olErrorText += CString("\n") + LoadStg(IDS_STRING83) + LoadStg(IDS_STRING91);
	}
	if(ilSteCount != 0)
	{
		blSoftStatus = false;
		olErrorText += CString("\n") + LoadStg(IDS_STRING84) + LoadStg(IDS_STRING91);
	}
	if(ilSwgCount != 0)
	{
		blSoftStatus = false;
		olErrorText += CString("\n") + LoadStg(IDS_STRING116) + LoadStg(IDS_STRING91);
	}

	if(ilStatus == true)
	{
		CString olText;
		CedaStfData olData;
		CCSPtrArray<STFDATA> omData;
		char pclWhere[128];
		m_PERC.GetWindowText(olText);
		sprintf(pclWhere,"WHERE PERC='%s'", olText);
		if(olData.ReadSpecialData(&omData, pclWhere, "URNO", false) == true)
		{	
			if(pomStf->Urno != omData[0].Urno)
			{
				olErrorText += LoadStg(IDS_STRING72) + LoadStg(IDS_EXIST);
				ilStatus = false;
			}
		}
		omData.DeleteAll();
		m_PENO.GetWindowText(olText);
		sprintf(pclWhere,"WHERE PENO='%s'", olText);
		if(olData.ReadSpecialData(&omData, pclWhere, "URNO", false) == true)
		{	
			if(pomStf->Urno != omData[0].Urno)
			{
				olErrorText += LoadStg(IDS_STRING71) + LoadStg(IDS_EXIST);
				ilStatus = false;
			}
		}
		omData.DeleteAll();
	}

	////////////////////////////////////////////////////////////////////////
	AfxGetApp()->DoWaitCursor(-1);
	if(ilStatus == true)
	{
		CString olText;
		char plcTmp[100]="";
		COleDateTime olDate; 
		CTime        olTime;
		m_DODDM_D.GetWindowText(olText); 
		olDate = OleDateStringToDate(olText);
		m_DDODM_T.GetWindowText(olText); 
		olTime = HourStringToDate(olText);
		if(olDate.GetStatus() == COleDateTime::valid && olTime != TIMENULL)
		{
			CString olS = olDate.Format("%d.%m.%Y") + olTime.Format("%H:%M");
			pomStf->Dodm = COleDateTime(olDate.GetYear(),olDate.GetMonth(),olDate.GetDay(),olTime.GetHour(), olTime.GetMinute(), 0);
			olS = pomStf->Dodm.Format("%d.%m.%Y-%H:%M");
			olS = "";
		}
		else
		{
			pomStf->Dodm.SetStatus(COleDateTime::null);
		}
		m_DOEM_D.GetWindowText(olText); 
		olDate = OleDateStringToDate(olText);
		m_DOEM_T.GetWindowText(olText); 
		olTime = HourStringToDate(olText);
		if(olDate.GetStatus() == COleDateTime::valid && olTime != TIMENULL)
		{
			CString olS = olDate.Format("%d.%m.%Y") + olTime.Format("%H:%M");
			pomStf->Doem = COleDateTime(olDate.GetYear(),olDate.GetMonth(),olDate.GetDay(),olTime.GetHour(), olTime.GetMinute(), 0);
			olS = pomStf->Doem.Format("%d.%m.%Y-%H:%M");
			olS = "";
		}
		else
		{
			pomStf->Doem.SetStatus(COleDateTime::null);
		}

		m_FINM.GetWindowText(olText); 
		strcpy(pomStf->Finm, olText);
		m_GSMN.GetWindowText(olText); 
		strcpy(pomStf->Gsmn, olText);
		m_KTOU.GetWindowText(olText); 
		strcpy(pomStf->Ktou, olText);
		m_LANM.GetWindowText(olText); 
		strcpy(pomStf->Lanm, olText);
		m_LINO.GetWindowText(olText); 
		strcpy(pomStf->Lino, olText);
		m_MATR.GetWindowText(olText); 
		strcpy(pomStf->Matr, olText);
		m_PENO.GetWindowText(olText); 
		strcpy(pomStf->Peno, olText);
		m_PERC.GetWindowText(olText); 
		strcpy(pomStf->Perc, olText);
		m_SHNM.GetWindowText(olText); 
		strcpy(pomStf->Shnm, olText);
		m_TELD.GetWindowText(olText); 
		strcpy(pomStf->Teld, olText);
		m_TELH.GetWindowText(olText); 
		strcpy(pomStf->Telh, olText);
		m_TELP.GetWindowText(olText); 
		strcpy(pomStf->Telp, olText);
		m_ZUTB.GetWindowText(olText); 
		strcpy(pomStf->Zutb, olText);
		m_ZUTF.GetWindowText(olText); 
		strcpy(pomStf->Zutf, olText);
		m_ZUTN.GetWindowText(olText); 
		strcpy(pomStf->Zutn, olText);
		m_ZUTW.GetWindowText(olText); 
		strcpy(pomStf->Zutw, olText);
		m_REMA.GetWindowText(olText); 
		strcpy(pomStf->Rema, olText);

		if(m_THOR.GetCheck() == 1) 
			strcpy(pomStf->Tohr, "J");
		else
			strcpy(pomStf->Tohr, "N");
		CDialog::OnOK();
	}
	else
	{
		Beep(440,70);
		olErrorText += CString("\n") + olTableErrorText;
		MessageBox(olErrorText,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
	}
}

//----------------------------------------------------------------------------------------

LONG MitarbeiterStammDlg::OnTableIPEditKillfocus( UINT wParam, LPARAM lParam)
{
	CCSIPEDITNOTIFY *prlNotify = (CCSIPEDITNOTIFY*)lParam;

	if(prlNotify->Text.IsEmpty())
		return 0L;
	if(prlNotify->SourceTable == pomOrgTable) 
	{
		if(prlNotify->Column == 0)
		{
			CedaOrgData olData;
			CCSPtrArray<ORGDATA> omData;
			char pclWhere[128];
			sprintf(pclWhere,"WHERE DPT1='%s'", prlNotify->Text);
			prlNotify->UserStatus = true;
			prlNotify->Status = olData.ReadSpecialData(&omData, pclWhere, "DPT1", false);
			omData.DeleteAll();
			return 0L;
		}
	}
	if(prlNotify->SourceTable == pomPfcTable) 
	{
		if(prlNotify->Column == 0)
		{
			CedaPfcData olData;
			CCSPtrArray<PFCDATA> omData;
			char pclWhere[128];
			sprintf(pclWhere,"WHERE FCTC='%s'", prlNotify->Text);
			prlNotify->UserStatus = true;
			prlNotify->Status = olData.ReadSpecialData(&omData, pclWhere, "FCTC", false);
			omData.DeleteAll();
			return 0L;
		}
	}
	if(prlNotify->SourceTable == pomPerTable) 
	{
		if(prlNotify->Column == 0)
		{
			CedaPerData olData;
			CCSPtrArray<PERDATA> omData;
			char pclWhere[128];
			sprintf(pclWhere,"WHERE PRMC='%s'", prlNotify->Text);
			prlNotify->UserStatus = true;
			prlNotify->Status = olData.ReadSpecialData(&omData, pclWhere, "PRMC", false);
			omData.DeleteAll();
			return 0L;
		}
	}
	if(prlNotify->SourceTable == pomCotTable) 
	{
		if(prlNotify->Column == 0)
		{
			CedaCotData olData;
			CCSPtrArray<COTDATA> omData;
			char pclWhere[128];
			sprintf(pclWhere,"WHERE CTRC='%s'", prlNotify->Text);
			prlNotify->UserStatus = true;
			prlNotify->Status = olData.ReadSpecialData(&omData, pclWhere, "CTRC", false);
			omData.DeleteAll();
			return 0L;
		}
	}
	if(prlNotify->SourceTable == pomTeaTable) 
	{
		if(prlNotify->Column == 0)
		{
			CedaTeaData olData;
			CCSPtrArray<TEADATA> omData;
			char pclWhere[128];
			sprintf(pclWhere,"WHERE FGMC='%s'", prlNotify->Text);
			prlNotify->UserStatus = true;
			prlNotify->Status = olData.ReadSpecialData(&omData, pclWhere, "FGMC", false);
			omData.DeleteAll();
			return 0L;
		}
	}
	if(prlNotify->SourceTable == pomWgpTable) 
	{
		if(prlNotify->Column == 0)
		{
			CedaWgpData olData;
			CCSPtrArray<WGPDATA> omData;
			char pclWhere[128];
			sprintf(pclWhere,"WHERE WGPC='%s'", prlNotify->Text);
			prlNotify->UserStatus = true;
			prlNotify->Status = olData.ReadSpecialData(&omData, pclWhere, "WGPC", false);
			omData.DeleteAll();
			return 0L;
		}
	}
	return 0L;
}

//----------------------------------------------------------------------------------------

LONG MitarbeiterStammDlg::OnKillfocus(UINT lParam, LONG wParam) 
{
	CString olText;

	return 0L;
}

//----------------------------------------------------------------------------------------

void MitarbeiterStammDlg::OnBf1() 
{
	OnBf_Button(0);
}

void MitarbeiterStammDlg::OnBf2() 
{
	OnBf_Button(1);
}

void MitarbeiterStammDlg::OnBf3() 
{
	OnBf_Button(2);
}

void MitarbeiterStammDlg::OnBf4() 
{
	OnBf_Button(3);
}

void MitarbeiterStammDlg::OnBf5() 
{
	OnBf_Button(4);
}

//----------------------------------------------------------------------------------------

void MitarbeiterStammDlg::OnBf_Button(int ipColY)
{
	CStringArray olCol1, olCol2, olCol3;
	CCSPtrArray<PFCDATA> olList;
	AfxGetApp()->DoWaitCursor(1);
	AfxGetApp()->DoWaitCursor(1);
	if(ogPfcData.ReadSpecialData(&olList, "", "URNO,FCTC,FCTN", false) == true)
	{
		AfxGetApp()->DoWaitCursor(-1);
		for(int i = 0; i < olList.GetSize(); i++)
		{
			char pclUrno[20]="";
			sprintf(pclUrno, "%ld", olList[i].Urno);
			olCol3.Add(CString(pclUrno));
			olCol1.Add(CString(olList[i].Fctc));
			olCol2.Add(CString(olList[i].Fctn));
		}
		AwDlg *pomDlg = new AwDlg(&olCol1, &olCol2, &olCol3, LoadStg(IDS_STRING237), LoadStg(IDS_STRING229), this);

#ifndef CLIENT_HAJ
		//*** 30.08.99 SHA ***		
		pomDlg->obShowCodes = TRUE;
		pomDlg->ogAct = &olActArray;
		pomDlg->ogAlt = &olAltArray;
#endif

		if(pomDlg->DoModal() == IDOK)
		{
			int ilColX = -1, ilColY = -1;
			if(((CCSTableListBox*)(pomPfcTable->GetCTableListBox()))->GetVisibleLeftTopColumn(ilColX, ilColY) == true)
			{
#ifndef CLIENT_HAJ
				//*** 30.08.99 SHA ***
				//*** Break out the two codes [3LC(5LC)] ACT ***
				CString olCode1,olCode2;
				olCode1 = pomDlg->olReturnAct;
				olCode2 = olCode1.Mid(olCode1.Find("(")+1);
				olCode2 = olCode2.Left(olCode2.Find(")"));
				olCode1 = olCode1.Left(olCode1.Find("("));
				if (olCode1.IsEmpty())
					olCode1 = " ";
				if (olCode2.IsEmpty())
					olCode2 = " ";
				//*** Break out the two codes [2LC(3LC)] ALC ***
				CString olCode3,olCode4;
				olCode3 = pomDlg->olReturnAlc;
				olCode4 = olCode3.Mid(olCode3.Find("(")+1);
				olCode4 = olCode4.Left(olCode4.Find(")"));
				olCode3 = olCode3.Left(olCode3.Find("("));
				if (olCode3.IsEmpty())
					olCode3 = " ";
				if (olCode4.IsEmpty())
					olCode4 = " ";
#endif

				ilColY += ipColY;
				pomPfcTable->SetIPValue(ilColY, 0, pomDlg->omReturnString);

#ifndef CLIENT_HAJ
				//*** ACT ***
				pomPfcTable->SetIPValue(ilColY, 4, olCode1);
				pomPfcTable->SetIPValue(ilColY, 5, olCode2);
				//*** ALC ***
				pomPfcTable->SetIPValue(ilColY, 6, olCode3);
				pomPfcTable->SetIPValue(ilColY, 7, olCode4);
#endif
				
				pomPfcTable->imCurrentColumn = 0;
				pomPfcTable->MakeInplaceEdit(ilColY);
			}
		}
		delete pomDlg;
	}
	else
	{
		MessageBox(LoadStg(IDS_STRING321), LoadStg(IDS_STRING149), (MB_ICONEXCLAMATION|MB_OK));
	}
	olList.DeleteAll();

	AfxGetApp()->DoWaitCursor(-1);
}

//----------------------------------------------------------------------------------------

void MitarbeiterStammDlg::OnBg1() 
{
	OnBg_Button(0);
}

void MitarbeiterStammDlg::OnBg2() 
{
	OnBg_Button(1);
}

void MitarbeiterStammDlg::OnBg3() 
{
	OnBg_Button(2);
}

void MitarbeiterStammDlg::OnBg4() 
{
	OnBg_Button(3);
}

void MitarbeiterStammDlg::OnBg5() 
{
	OnBg_Button(4);
}

//----------------------------------------------------------------------------------------

void MitarbeiterStammDlg::OnBg_Button(int ipColY)
{
	CStringArray olCol1, olCol2, olCol3;
	CCSPtrArray<TEADATA> olList;
	AfxGetApp()->DoWaitCursor(1);
	AfxGetApp()->DoWaitCursor(1);
	if(ogTeaData.ReadSpecialData(&olList, "", "URNO,FGMC,FGMN", false) == true)
	{
		AfxGetApp()->DoWaitCursor(-1);
		for(int i = 0; i < olList.GetSize(); i++)
		{
			char pclUrno[20]="";
			sprintf(pclUrno, "%ld", olList[i].Urno);
			olCol3.Add(CString(pclUrno));
			olCol1.Add(CString(olList[i].Fgmc));
			olCol2.Add(CString(olList[i].Fgmn));
		}
		AwDlg *pomDlg = new AwDlg(&olCol1, &olCol2, &olCol3, LoadStg(IDS_STRING237), LoadStg(IDS_STRING229), this);
		if(pomDlg->DoModal() == IDOK)
		{
			int ilColX = -1, ilColY = -1;
			if(((CCSTableListBox*)(pomTeaTable->GetCTableListBox()))->GetVisibleLeftTopColumn(ilColX, ilColY) == true)
			{
				ilColY += ipColY;
				pomTeaTable->SetIPValue(ilColY, 0, pomDlg->omReturnString);
				pomTeaTable->imCurrentColumn = 0;
				pomTeaTable->MakeInplaceEdit(ilColY);
			}
		}
		delete pomDlg;
	}
	else
	{
		MessageBox(LoadStg(IDS_STRING321), LoadStg(IDS_STRING149), (MB_ICONEXCLAMATION|MB_OK));
	}
	olList.DeleteAll();

	AfxGetApp()->DoWaitCursor(-1);
}

//----------------------------------------------------------------------------------------

void MitarbeiterStammDlg::OnBo1() 
{
	OnBo_Button(0);
}

void MitarbeiterStammDlg::OnBo2() 
{
	OnBo_Button(1);
}

void MitarbeiterStammDlg::OnBo3() 
{
	OnBo_Button(2);
}

void MitarbeiterStammDlg::OnBo4() 
{
	OnBo_Button(3);
}

void MitarbeiterStammDlg::OnBo5() 
{
	OnBo_Button(4);
}

//----------------------------------------------------------------------------------------

void MitarbeiterStammDlg::OnBo_Button(int ipColY)
{
	CStringArray olCol1, olCol2, olCol3;
	CCSPtrArray<ORGDATA> olList;
	AfxGetApp()->DoWaitCursor(1);
	AfxGetApp()->DoWaitCursor(1);
	if(ogOrgData.ReadSpecialData(&olList, "", "URNO,DPT1,DPTN", false) == true)
	{
		AfxGetApp()->DoWaitCursor(-1);
		for(int i = 0; i < olList.GetSize(); i++)
		{
			char pclUrno[20]="";
			sprintf(pclUrno, "%ld", olList[i].Urno);
			olCol3.Add(CString(pclUrno));
			olCol1.Add(CString(olList[i].Dpt1));
			olCol2.Add(CString(olList[i].Dptn));
		}
		AwDlg *pomDlg = new AwDlg(&olCol1, &olCol2, &olCol3, LoadStg(IDS_STRING237), LoadStg(IDS_STRING229), this);
		if(pomDlg->DoModal() == IDOK)
		{
			int ilColX = -1, ilColY = -1;
			if(((CCSTableListBox*)(pomOrgTable->GetCTableListBox()))->GetVisibleLeftTopColumn(ilColX, ilColY) == true)
			{
				ilColY += ipColY;
				pomOrgTable->SetIPValue(ilColY, 0, pomDlg->omReturnString);
				pomOrgTable->imCurrentColumn = 0;
				pomOrgTable->MakeInplaceEdit(ilColY);
			}
		}
		delete pomDlg;
	}
	else
	{
		MessageBox(LoadStg(IDS_STRING321), LoadStg(IDS_STRING149), (MB_ICONEXCLAMATION|MB_OK));
	}
	olList.DeleteAll();

	AfxGetApp()->DoWaitCursor(-1);
}

//----------------------------------------------------------------------------------------

void MitarbeiterStammDlg::OnBq1() 
{
	OnBq_Button(0);
}

void MitarbeiterStammDlg::OnBq2() 
{
	OnBq_Button(1);
}

void MitarbeiterStammDlg::OnBq3() 
{
	OnBq_Button(2);
}

void MitarbeiterStammDlg::OnBq4() 
{
	OnBq_Button(3);
}

void MitarbeiterStammDlg::OnBq5() 
{
	OnBq_Button(4);
}

//----------------------------------------------------------------------------------------

void MitarbeiterStammDlg::OnBq_Button(int ipColY)
{
	CStringArray olCol1, olCol2, olCol3;
	CCSPtrArray<PERDATA> olList;

	CString cDmy;
	AfxGetApp()->DoWaitCursor(1);
	AfxGetApp()->DoWaitCursor(1);
	if(ogPerData.ReadSpecialData(&olList, "", "URNO,PRMC,PRMN", false) == true)

	{
		AfxGetApp()->DoWaitCursor(-1);
		for(int i = 0; i < olList.GetSize(); i++)
		{
			char pclUrno[20]="";
			sprintf(pclUrno, "%ld", olList[i].Urno);
			olCol3.Add(CString(pclUrno));
			olCol1.Add(CString(olList[i].Prmc));
			olCol2.Add(CString(olList[i].Prmn));
		}

		AwDlg *pomDlg = new AwDlg(&olCol1, &olCol2, &olCol3, LoadStg(IDS_STRING237), LoadStg(IDS_STRING229), this);


		if(pomDlg->DoModal() == IDOK)
		{
			int ilColX = -1, ilColY = -1;
			if(((CCSTableListBox*)(pomPerTable->GetCTableListBox()))->GetVisibleLeftTopColumn(ilColX, ilColY) == true)
			{

				ilColY += ipColY;
				pomPerTable->SetIPValue(ilColY, 0, pomDlg->omReturnString);

				pomPerTable->imCurrentColumn = 0;
				pomPerTable->MakeInplaceEdit(ilColY);
			}
		}
		delete pomDlg;
	}
	else
	{
		MessageBox(LoadStg(IDS_STRING321), LoadStg(IDS_STRING149), (MB_ICONEXCLAMATION|MB_OK));
	}
	olList.DeleteAll();

	AfxGetApp()->DoWaitCursor(-1);
}

//----------------------------------------------------------------------------------------

void MitarbeiterStammDlg::OnBa1() 
{
	OnBa_Button(0);
}

void MitarbeiterStammDlg::OnBa2() 
{
	OnBa_Button(1);
}

void MitarbeiterStammDlg::OnBa3() 
{
	OnBa_Button(2);
}

void MitarbeiterStammDlg::OnBa4() 
{
	OnBa_Button(3);
}

void MitarbeiterStammDlg::OnBa5() 
{
	OnBa_Button(4);
}

//----------------------------------------------------------------------------------------

void MitarbeiterStammDlg::OnBa_Button(int ipColY) 
{
	CStringArray olCol1, olCol2, olCol3;
	CCSPtrArray<COTDATA> olList;
	AfxGetApp()->DoWaitCursor(1);

	AfxGetApp()->DoWaitCursor(1);
	if(ogCotData.ReadSpecialData(&olList, "", "URNO,CTRC,CTRN", false) == true)
	{
		AfxGetApp()->DoWaitCursor(-1);
		for(int i = 0; i < olList.GetSize(); i++)
		{
			char pclUrno[20]="";
			sprintf(pclUrno, "%ld", olList[i].Urno);
			olCol3.Add(CString(pclUrno));
			olCol1.Add(CString(olList[i].Ctrc));
			olCol2.Add(CString(olList[i].Ctrn));
		}
		AwDlg *pomDlg = new AwDlg(&olCol1, &olCol2, &olCol3, LoadStg(IDS_STRING237), LoadStg(IDS_STRING229), this);
		if(pomDlg->DoModal() == IDOK)
		{
			int ilColX = -1, ilColY = -1;
			if(((CCSTableListBox*)(pomCotTable->GetCTableListBox()))->GetVisibleLeftTopColumn(ilColX, ilColY) == true)
			{
				ilColY += ipColY;
				pomCotTable->SetIPValue(ilColY, 0, pomDlg->omReturnString);
				pomCotTable->imCurrentColumn = 0;
				pomCotTable->MakeInplaceEdit(ilColY);
			}
		}
		delete pomDlg;
	}
	else
	{
		MessageBox(LoadStg(IDS_STRING321), LoadStg(IDS_STRING149), (MB_ICONEXCLAMATION|MB_OK));
	}
	olList.DeleteAll();

	AfxGetApp()->DoWaitCursor(-1);
}

//----------------------------------------------------------------------------------------

void MitarbeiterStammDlg::OnBgrp1() 
{
	OnBgrp_Button(0);
	
}

void MitarbeiterStammDlg::OnBgrp2() 
{
	OnBgrp_Button(1);
	
}

void MitarbeiterStammDlg::OnBgrp3() 
{
	OnBgrp_Button(2);
	
}

void MitarbeiterStammDlg::OnBgrp4() 
{
	OnBgrp_Button(3);
	
}

void MitarbeiterStammDlg::OnBgrp5() 
{
	OnBgrp_Button(4);
	
}

void MitarbeiterStammDlg::OnBgrp_Button(int ipColY)
{
	CStringArray olCol1, olCol2, olCol3;
	CCSPtrArray<WGPDATA> olList;
	AfxGetApp()->DoWaitCursor(1);
	AfxGetApp()->DoWaitCursor(1);
	if(ogWgpData.ReadSpecialData(&olList, "", "URNO,WGPC,WGPN", false) == true)
	{
		AfxGetApp()->DoWaitCursor(-1);
		for(int i = 0; i < olList.GetSize(); i++)
		{
			char pclUrno[20]="";
			sprintf(pclUrno, "%ld", olList[i].Urno);
			olCol3.Add(CString(pclUrno));
			olCol1.Add(CString(olList[i].Wgpc));
			olCol2.Add(CString(olList[i].Wgpn));
		}
		AwDlg *pomDlg = new AwDlg(&olCol1, &olCol2, &olCol3, LoadStg(IDS_STRING237), LoadStg(IDS_STRING229), this);
		if(pomDlg->DoModal() == IDOK)
		{
			int ilColX = -1, ilColY = -1;
			if(((CCSTableListBox*)(pomWgpTable->GetCTableListBox()))->GetVisibleLeftTopColumn(ilColX, ilColY) == true)
			{
				ilColY += ipColY;
				pomWgpTable->SetIPValue(ilColY, 0, pomDlg->omReturnString);
				pomWgpTable->imCurrentColumn = 0;
				pomWgpTable->MakeInplaceEdit(ilColY);
			}
		}
		delete pomDlg;
	}
	else
	{
		MessageBox(LoadStg(IDS_STRING321), LoadStg(IDS_STRING149), (MB_ICONEXCLAMATION|MB_OK));
	}
	olList.DeleteAll();

	AfxGetApp()->DoWaitCursor(-1);
}

//----------------------------------------------------------------------------------------

//*** 02.11.99 SHA                        ***
//*** NEW SYSTEM OF SAVING QUALIFICATIONS ***
//*** ITALIAN REQUIREMENT !               ***
//*** e.g. QUALIFIKATION: TDL_MD8_AZ      ***
long MitarbeiterStammDlg::SearchFctc(CString ilFctc)
{

	long ilCount;
	char clWhere[100];

	sprintf(clWhere,"WHERE FCTC='%s'",ilFctc);
	ogPerData.Read(clWhere);
	
	ilCount = ogPerData.omData.GetSize();

	return ilCount;
}

//*** 02.11.99 SHA ***
void MitarbeiterStammDlg::SaveNewFctc(CString ilFctc)
{
	
	PERDATA *prlPer = new PERDATA;

	sprintf(prlPer->Prmc,"%s", ilFctc);
	sprintf(prlPer->Prmn,"%s", ilFctc);
	sprintf(prlPer->Usec,"%s", "AUTO_STF_DIALOG");
	prlPer->IsChanged = DATA_NEW;
	prlPer->Urno = ogBasicData.GetNextUrno();
	prlPer->Cdat = CTime::GetCurrentTime();

	ogPerData.Save(prlPer);

	delete prlPer;

}