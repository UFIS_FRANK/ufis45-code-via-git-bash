#ifndef __FahrgemeinschaftenTableViewer_H__
#define __FahrgemeinschaftenTableViewer_H__

#include "stdafx.h"
#include "CedaTeaData.h"
#include "CCSTable.h"
#include "CViewer.h"
#include "CCSPrint.h"


struct FAHRGEMEINSCHAFTENTABLE_LINEDATA
{
	long 	 Urno; 	// Eindeutige Datensatz-Nr.
	CString  Fgmc; 	// Code
	CString  Fgmn; 	// Bezeichnung
	CString  Rema; 	// Bemerkungen

};

/////////////////////////////////////////////////////////////////////////////
// FahrgemeinschaftenTableViewer

	  
class FahrgemeinschaftenTableViewer : public CViewer
{
// Constructions
public:
    FahrgemeinschaftenTableViewer(CCSPtrArray<TEADATA> *popData);
    ~FahrgemeinschaftenTableViewer();

    void Attach(CCSTable *popAttachWnd);
    virtual void ChangeViewTo(const char *pcpViewName);
// Internal data processing routines
private:
	bool IsPassFilter(TEADATA *prpFahrgemeinschaften);
	int CompareFahrgemeinschaften(FAHRGEMEINSCHAFTENTABLE_LINEDATA *prpFahrgemeinschaften1, FAHRGEMEINSCHAFTENTABLE_LINEDATA *prpFahrgemeinschaften2);
    void MakeLines();
	void MakeLine(TEADATA *prpFahrgemeinschaften);
	bool FindLine(long lpUrno, int &rilLineno);
// Operations
public:
	void DeleteAll();
	void CreateLine(FAHRGEMEINSCHAFTENTABLE_LINEDATA *prpFahrgemeinschaften);
	void DeleteLine(int ipLineno);
// Window refreshing routines
public:
	void UpdateDisplay();
	CString Format(FAHRGEMEINSCHAFTENTABLE_LINEDATA *prpLine);
	void ProcessFahrgemeinschaftenChange(TEADATA *prpFahrgemeinschaften);
	void ProcessFahrgemeinschaftenDelete(TEADATA *prpFahrgemeinschaften);
	bool FindFahrgemeinschaften(char *prpFahrgemeinschaftenKeya, char *prpFahrgemeinschaftenKeyd, int& ilItem);
// Attributes used for filtering condition
private:
	bool bmIsFromSearch;
// Attributes
private:
    CCSTable *pomFahrgemeinschaftenTable;
	CCSPtrArray<TEADATA> *pomData;
// Methods which handle changes (from Data Distributor)
public:
    CCSPtrArray<FAHRGEMEINSCHAFTENTABLE_LINEDATA> omLines;
///////
	CCSPtrArray<TABLE_HEADER_COLUMN> omHeaderDataArray;
//Print 
	void PrintTableView();
	bool PrintTableLine(FAHRGEMEINSCHAFTENTABLE_LINEDATA *prpLine,bool bpLastLine);
	bool PrintTableHeader();
	CCSPrint *pomPrint;
	//CBitmap omBitmap;
	CString omTableName;

};

#endif //__FahrgemeinschaftenTableViewer_H__
