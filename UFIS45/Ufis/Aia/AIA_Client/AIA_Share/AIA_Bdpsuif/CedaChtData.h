// CedaChtData.h

#ifndef __CEDACHTDATA__
#define __CEDACHTDATA__
 
#include "stdafx.h"
#include "basicdata.h"

//---------------------------------------------------------------------------------------------------------

struct CHTDATA 
{
	CTime 	 Cdat; 		// Erstellungsdatum
	CTime 	 Lstu; 		// Datum letzte �nderung
	char 	 Chtc[6]; 	// Veranstalter K�rzel
	char 	 Chtn[62]; 	// Veranstalter Name
	long 	 Urno; 		// Eindeutige Datensatz-Nr.
	char 	 Usec[34]; 	// Anwender (Ersteller)
	char 	 Useu[34]; 	// Anwender (letzte �nderung)
	CTime 	 Vafr; 		// G�ltig von
	CTime 	 Vato; 		// G�ltig bis

	//DataCreated by this class
	int		 IsChanged;	// Check whether Data has Changed f�r Relaese

	CHTDATA(void)
	{ 
		memset(this,'\0',sizeof(*this));
		IsChanged	=	DATA_UNCHANGED;
		Cdat		=	TIMENULL;
		Lstu		=	TIMENULL;
		Vafr		=	TIMENULL;
		Vato		=	TIMENULL;
	}

}; // end ChtDataStrukt

//---------------------------------------------------------------------------------------------------------
// Class declaratino

class CedaChtData: public CCSCedaData
{
// Attributes
public:
    CMapPtrToPtr omUrnoMap;

    CCSPtrArray<CHTDATA> omData;

	char pcmListOfFields[2048];

// Operations
public:
    CedaChtData();
	~CedaChtData();
	virtual void GetDataInfo(CStringArray &ropFields, CStringArray &ropDesription, CStringArray &ropType);
	void Register(void);
	void ClearAll(bool bpWithRegistration = true);
    bool Read(char *pspWhere = NULL);
	bool Insert(CHTDATA *prpCht);
	bool InsertInternal(CHTDATA *prpCht);
	bool Update(CHTDATA *prpCht);
	bool UpdateInternal(CHTDATA *prpCht);
	bool Delete(long lpUrno);
	bool DeleteInternal(CHTDATA *prpCht);
	bool ReadSpecialData(CCSPtrArray<CHTDATA> *popCht,char *pspWhere,char *pspFieldList,bool ipSYS = true);
	bool Save(CHTDATA *prpCht);
	void ProcessBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);
	CHTDATA  *GetChtByUrno(long lpUrno);

	// Private methods
private:
    void PrepareChtData(CHTDATA *prpChtData);

};

//---------------------------------------------------------------------------------------------------------

#endif //__CEDACHTDATA__
