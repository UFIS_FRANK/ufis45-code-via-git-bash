#ifndef __MitarbeiterstammTableViewer_H__
#define __MitarbeiterstammTableViewer_H__

#include "stdafx.h"
#include "CedaStfData.h"
#include "CCSTable.h"
#include "CViewer.h"
#include "CCSPrint.h"


struct MITARBEITERSTAMMTABLE_LINEDATA
{
	long 	 Urno;	// Eindeutige Datensatz-Nr.
	CString  Lanm; 	// Name
	CString  Finm; 	// Vorname
	CString  Shnm; 	// Kurzname
	CString  Perc; 	// Kürzel
	CString  Peno; 	// Personalnummer
	CString  Makr; 	// Mitarbeiterkreis
	CString  Sken; 	// Schichtkennzeichen
	CString  Teld; 	// Telefonnr. dienstlich
	CString  Telh; 	// Telefonnr. Handy
	CString  Telp; 	// Telefonnr. privat
	COleDateTime 	 Doem;	// Eintrittsdatum
	COleDateTime 	 Dodm;	// Austrittsdatum
	CString  Ktou; 	// Urlaubsansprüche
	CString  Zutf; 	// Zusätzliche Urlaubstage Freistellungstage
	CString  Zutb; 	// Zusätzliche Urlaubstage Schwerbehinderung
	CString  Zutw; 	// Zusätzliche Urlaubstage Wechselschicht
	CString  Zutn; 	// Zusätzliche Urlaubstage Nachtschicht
	CString  Tohr; 	// Soll an SAP-HR übermittelt werden
	CString  Gsmn; 	// Name
	CString  Lino; 	// Name
	CString  Matr; 	// Name
	CString  Rema; 	// Bemerkungen
};

/////////////////////////////////////////////////////////////////////////////
// MitarbeiterstammTableViewer

	  
class MitarbeiterstammTableViewer : public CViewer
{
// Constructions
public:
    MitarbeiterstammTableViewer(CCSPtrArray<STFDATA> *popData);
    ~MitarbeiterstammTableViewer();

    void Attach(CCSTable *popAttachWnd);
    virtual void ChangeViewTo(const char *pcpViewName);
// Internal data processing routines
private:
	bool IsPassFilter(STFDATA *prpMitarbeiterstamm);
	int CompareMitarbeiterstamm(MITARBEITERSTAMMTABLE_LINEDATA *prpMitarbeiterstamm1, MITARBEITERSTAMMTABLE_LINEDATA *prpMitarbeiterstamm2);
    void MakeLines();
	void MakeLine(STFDATA *prpMitarbeiterstamm);
	bool FindLine(long lpUrno, int &rilLineno);
// Operations
public:
	void DeleteAll();
	void CreateLine(MITARBEITERSTAMMTABLE_LINEDATA *prpMitarbeiterstamm);
	void DeleteLine(int ipLineno);
// Window refreshing routines
public:
	void UpdateDisplay();
	CString Format(MITARBEITERSTAMMTABLE_LINEDATA *prpLine);
	void ProcessMitarbeiterstammChange(STFDATA *prpMitarbeiterstamm);
	void ProcessMitarbeiterstammDelete(STFDATA *prpMitarbeiterstamm);
	bool FindMitarbeiterstamm(char *prpMitarbeiterstammKeya, char *prpMitarbeiterstammKeyd, int& ilItem);
// Attributes used for filtering condition
private:
	bool bmIsFromSearch;
// Attributes
private:
    CCSTable *pomMitarbeiterstammTable;
	CCSPtrArray<STFDATA> *pomData;
// Methods which handle changes (from Data Distributor)
public:
    CCSPtrArray<MITARBEITERSTAMMTABLE_LINEDATA> omLines;
///////
	CCSPtrArray<TABLE_HEADER_COLUMN> omHeaderDataArray;
//Print 
	void PrintTableView();
	bool PrintTableLine(MITARBEITERSTAMMTABLE_LINEDATA *prpLine,bool bpLastLine);
	bool PrintTableHeader();
	CCSPrint *pomPrint;
	//CBitmap omBitmap;
	CString omTableName;

};

#endif //__MitarbeiterstammTableViewer_H__
