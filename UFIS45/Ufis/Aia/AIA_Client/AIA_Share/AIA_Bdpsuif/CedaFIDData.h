// CedaFIDData.h

#ifndef __CEDAFIDDATA__
#define __CEDAFIDDATA__
 
#include "stdafx.h"
#include "basicdata.h"

//---------------------------------------------------------------------------------------------------------
// Record structure declaration

struct FIDDATA 
{
	CTime	 Cdat;		// Erstellungsdatum
	CTime	 Lstu; 		// Datum letzte �nderung
	char 	 Prfl[3]; 	// Protokollierungskennzeichen
	long 	 Urno; 		// Eindeutige Datensatz-Nr.
	char 	 Usec[34]; 	// Anwender (Ersteller)
	char 	 Useu[34]; 	// Anwender (letzte �nderung)
	CTime	 Vafr;		// G�ltig von
	CTime	 Vato;		// G�ltig bis
	char 	 Code[6]; 	// Code der Bemerkung
	char 	 Beme[22]; 	// FIDS-Bemerkungstext 1
	char 	 Bemd[22]; 	// FIDS-Bemerkungstext 2
	char 	 Bet3[22]; 	// FIDS-Bemerkungstext 3
	char 	 Bet4[22]; 	// FIDS-Bemerkungstext 4
	char 	 Remi[6]; 	
	char 	 Remt[3]; 	
	char 	 Blkc[3]; 	

	//DataCreated by this class
	int		 IsChanged;

	FIDDATA(void)
	{ 
		memset(this,'\0',sizeof(*this));
		Cdat=-1;
		Lstu=-1;
		Vafr=-1;
		Vato=-1;
	}

}; // end FIDDataStrukt


//---------------------------------------------------------------------------------------------------------
// Class declaratino

class CedaFIDData: public CCSCedaData
{
// Attributes
public:
    CMapPtrToPtr omUrnoMap;

    CCSPtrArray<FIDDATA> omData;

// Operations
public:
    CedaFIDData();
	~CedaFIDData();

	virtual void GetDataInfo(CStringArray &ropFields, CStringArray &ropDesription, CStringArray &ropType);
	void Register(void);
	void ClearAll(void);

    bool Read(char *pcpWhere = NULL);
	bool ReadSpecialData(CCSPtrArray<FIDDATA> *popFid,char *pspWhere,char *pspFieldList,bool ipSYS=true);
	bool InsertFID(FIDDATA *prpFID,BOOL bpSendDdx = TRUE);
	bool InsertFIDInternal(FIDDATA *prpFID);
	bool UpdateFID(FIDDATA *prpFID,BOOL bpSendDdx = TRUE);
	bool UpdateFIDInternal(FIDDATA *prpFID);
	bool DeleteFID(long lpUrno);
	bool DeleteFIDInternal(FIDDATA *prpFID);
	FIDDATA  *GetFIDByUrno(long lpUrno);
	bool SaveFID(FIDDATA *prpFID);
	char pcmFIDFieldList[2048];
	void ProcessFIDBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

	// Private methods
private:
    void PrepareFIDData(FIDDATA *prpFIDData);
};

//---------------------------------------------------------------------------------------------------------

#endif //__CEDAFIDDATA__
