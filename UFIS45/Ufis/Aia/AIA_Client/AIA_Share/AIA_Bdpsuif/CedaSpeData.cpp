// CedaSpeData.cpp
 
#include "stdafx.h"
#include "CedaSpeData.h"
#include "resource.h"


void ProcessSpeCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName);


//--CEDADATA-----------------------------------------------------------------------------------------------

CedaSpeData::CedaSpeData() : CCSCedaData(&ogCommHandler)
{
	// Create an array of CEDARECINFO for SPEDataStruct
	BEGIN_CEDARECINFO(SPEDATA,SPEDataRecInfo)
		FIELD_LONG		(Urno,"URNO")
		FIELD_LONG		(Surn,"SURN")
		FIELD_CHAR_TRIM	(Code,"CODE")
		FIELD_OLEDATE	(Vpfr,"VPFR")
		FIELD_OLEDATE	(Vpto,"VPTO")
		END_CEDARECINFO //(SPEDataStruct)

	// FIELD_LONG, FIELD_DATE 
	// Copy the record structure
	for (int i=0; i< sizeof(SPEDataRecInfo)/sizeof(SPEDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&SPEDataRecInfo[i],sizeof(CEDARECINFO));
		omRecInfo.Add(prpCedaRecInfo);
	} // end for

	// initialize field names
	strcpy(pcmTableName,"SPE");
	strcpy(pcmListOfFields,"URNO,SURN,CODE,VPFR,VPTO");
	
	pcmFieldList = pcmListOfFields; //because in CedaData pcmFieldList is just a pointer

	omData.RemoveAll();
}

//---------------------------------------------------------------------------------------------------------

void CedaSpeData::GetDataInfo(CStringArray &ropFields, CStringArray &ropDesription, CStringArray &ropType)
{
	ropFields.Add("URNO");
	ropFields.Add("SURN");
	ropFields.Add("CODE");
	ropFields.Add("VPFR");
	ropFields.Add("VPTO");

	ropDesription.Add(LoadStg(IDS_STRING346));
	ropDesription.Add(LoadStg(IDS_STRING464));
	ropDesription.Add(LoadStg(IDS_STRING465));
	ropDesription.Add(LoadStg(IDS_STRING230));
	ropDesription.Add(LoadStg(IDS_STRING231));

	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("Date");
	ropType.Add("Date");


}
//--REGISTER----------------------------------------------------------------------------------------------

void CedaSpeData::Register(void)
{
	ogDdx.Register((void *)this,BC_SPE_CHANGE,	CString("SPEDATA"), CString("Spe-changed"),	ProcessSpeCf);
	ogDdx.Register((void *)this,BC_SPE_NEW,		CString("SPEDATA"), CString("Spe-new"),		ProcessSpeCf);
	ogDdx.Register((void *)this,BC_SPE_DELETE,	CString("SPEDATA"), CString("Spe-deleted"),	ProcessSpeCf);
}

//--~CEDADATA----------------------------------------------------------------------------------------------

CedaSpeData::~CedaSpeData(void)
{
	omRecInfo.DeleteAll();
	ClearAll();
}

//--CLEAR-ALL----------------------------------------------------------------------------------------------

void CedaSpeData::ClearAll(bool bpWithRegistration)
{
    omUrnoMap.RemoveAll();
    omData.DeleteAll();
	if(bpWithRegistration)
	{
		ogDdx.UnRegister(this,NOTUSED);
	}
}

//--READ-ALL-----------------------------------------------------------------------------------------------

bool CedaSpeData::Read(char *pspWhere /*NULL*/)
{
    // Select data from the database
	bool ilRc = true;
    omUrnoMap.RemoveAll();
    omData.DeleteAll();
	if(pspWhere == NULL)
	{	
		ilRc = CedaAction("RT");
	}
	else
	{
		ilRc = CedaAction("RT", pspWhere);
	}
	if (ilRc != true)
	{
		return ilRc;
	}
	// Load data from CedaData into the dynamic array of record
	for (int ilLc = 0; ilRc == true; ilLc++)
	{
		SPEDATA *prlSpe = new SPEDATA;
		if ((ilRc = GetFirstBufferRecord(prlSpe)) == true)
		{
			omData.Add(prlSpe);//Update omData
			omUrnoMap.SetAt((void *)prlSpe->Urno,prlSpe);
		}
		else
		{
			delete prlSpe;
		}
	}
    return true;
}

//--INSERT-------------------------------------------------------------------------------------------------

bool CedaSpeData::Insert(SPEDATA *prpSpe)
{
	prpSpe->IsChanged = DATA_NEW;
	if(Save(prpSpe) == false) return false; //Update Database
	InsertInternal(prpSpe);
    return true;
}

//--INSERT-INTERNAL--------------------------------------------------------------------------------------

bool CedaSpeData::InsertInternal(SPEDATA *prpSpe)
{
	ogDdx.DataChanged((void *)this, SPE_NEW,(void *)prpSpe ); //Update Viewer
	omData.Add(prpSpe);//Update omData
	omUrnoMap.SetAt((void *)prpSpe->Urno,prpSpe);
    return true;
}

//--DELETE-------------------------------------------------------------------------------------------------

bool CedaSpeData::Delete(long lpUrno)
{
	SPEDATA *prlSpe = GetSpeByUrno(lpUrno);
	if (prlSpe != NULL)
	{
		prlSpe->IsChanged = DATA_DELETED;
		if(Save(prlSpe) == false) return false; //Update Database
		DeleteInternal(prlSpe);
	}
    return true;
}

//--DELETE-INTERNAL----------------------------------------------------------------------------------------

bool CedaSpeData::DeleteInternal(SPEDATA *prpSpe)
{
	ogDdx.DataChanged((void *)this,SPE_DELETE,(void *)prpSpe); //Update Viewer
	omUrnoMap.RemoveKey((void *)prpSpe->Urno);
	int ilSpeCount = omData.GetSize();
	for (int ilLc = 0; ilLc < ilSpeCount; ilLc++)
	{
		if (omData[ilLc].Urno == prpSpe->Urno)
		{
			omData.DeleteAt(ilLc);//Update omData
			break;
		}
	}
    return true;
}

//--UPDATE-------------------------------------------------------------------------------------------------

bool CedaSpeData::Update(SPEDATA *prpSpe)
{
	if (GetSpeByUrno(prpSpe->Urno) != NULL)
	{
		if (prpSpe->IsChanged == DATA_UNCHANGED)
		{
			prpSpe->IsChanged = DATA_CHANGED;
		}
		if(Save(prpSpe) == false) return false; //Update Database
		UpdateInternal(prpSpe);
	}
    return true;
}

//--UPDATE-INTERNAL--------------------------------------------------------------------------------------------

bool CedaSpeData::UpdateInternal(SPEDATA *prpSpe)
{
	SPEDATA *prlSpe = GetSpeByUrno(prpSpe->Urno);
	if (prlSpe != NULL)
	{
		*prlSpe = *prpSpe; //Update omData
		ogDdx.DataChanged((void *)this,SPE_CHANGE,(void *)prlSpe); //Update Viewer
	}
    return true;
}

//--GET-BY-URNO--------------------------------------------------------------------------------------------

SPEDATA *CedaSpeData::GetSpeByUrno(long lpUrno)
{
	SPEDATA  *prlSpe;
	if (omUrnoMap.Lookup((void *)lpUrno,(void *& )prlSpe) == TRUE)
	{
		return prlSpe;
	}
	return NULL;
}

//--READSPECIALDATA-------------------------------------------------------------------------------------

bool CedaSpeData::ReadSpecialData(CCSPtrArray<SPEDATA> *popSpe,char *pspWhere,char *pspFieldList,bool ipSYS/*=true*/)
{
	bool ilRc = true;
	char pclFieldList[256] = " ";

	if(strlen(pspFieldList) > 0) 
	{
		strcpy(pclFieldList, pspFieldList);
	}
	if(ipSYS == true) 
	{
		if (CedaAction("SYS","SPE",pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	else
	{
		if (CedaAction("RT","SPE",pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	if(popSpe != NULL)
	{
		for (int ilLc = 0; ilRc == true; ilLc++)
		{
			SPEDATA *prpSpe = new SPEDATA;
			if ((ilRc = GetBufferRecord(ilLc,prpSpe,CString(pclFieldList))) == true)
			{
				popSpe->Add(prpSpe);
			}
			else
			{
				delete prpSpe;
			}
		}
		if(popSpe->GetSize() == 0) return false;
	}
    return true;
}

//--SAVE---------------------------------------------------------------------------------------------------

bool CedaSpeData::Save(SPEDATA *prpSpe)
{
	bool ilRc = true;
	CString olListOfData;
	char pclSelection[124];
	char pclData[524];

	if (prpSpe->IsChanged == DATA_UNCHANGED)
	{
		return true; // no change, nothing to do
	}
	switch(prpSpe->IsChanged)
	{
	case DATA_NEW:
		MakeCedaData(&omRecInfo,olListOfData,prpSpe);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("IRT","","",pclData);
		prpSpe->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_CHANGED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpSpe->Urno);
		MakeCedaData(&omRecInfo,olListOfData,prpSpe);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("URT",pclSelection,"",pclData);
		prpSpe->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_DELETED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpSpe->Urno);
		ilRc = CedaAction("DRT",pclSelection);
		break;
	}
    return ilRc;
}

//--PROCESS-CF---------------------------------------------------------------------------------------------

void  ProcessSpeCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	ogSpeData.ProcessBc(ipDDXType,vpDataPointer,ropInstanceName);
}

//--PROCESS-BC---------------------------------------------------------------------------------------------

void  CedaSpeData::ProcessBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	struct BcStruct *prlSpeData;
	prlSpeData = (struct BcStruct *) vpDataPointer;
	SPEDATA *prlSpe;
	if(ipDDXType == BC_SPE_NEW)
	{
		prlSpe = new SPEDATA;
		GetRecordFromItemList(prlSpe,prlSpeData->Fields,prlSpeData->Data);
		InsertInternal(prlSpe);
	}
	if(ipDDXType == BC_SPE_CHANGE)
	{
		long llUrno = GetUrnoFromSelection(prlSpeData->Selection);
		prlSpe = GetSpeByUrno(llUrno);
		if(prlSpe != NULL)
		{
			GetRecordFromItemList(prlSpe,prlSpeData->Fields,prlSpeData->Data);
			UpdateInternal(prlSpe);
		}
	}
	if(ipDDXType == BC_SPE_DELETE)
	{
		long llUrno;
		CString olSelection = (CString)prlSpeData->Selection;
		if (olSelection.Find('\'') != -1)
		{
			llUrno = GetUrnoFromSelection(prlSpeData->Selection);
		}
		else
		{
			int ilFirst = olSelection.Find("=")+2;
			int ilLast  = olSelection.GetLength();
			llUrno = atol(olSelection.Mid(ilFirst,ilLast-ilFirst));
		}
		prlSpe = GetSpeByUrno(llUrno);
		if (prlSpe != NULL)
		{
			DeleteInternal(prlSpe);
		}
	}
}

//---------------------------------------------------------------------------------------------------------
