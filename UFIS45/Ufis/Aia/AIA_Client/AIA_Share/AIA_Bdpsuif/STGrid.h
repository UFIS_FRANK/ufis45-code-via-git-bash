// STGrid.h: interface for the CSTGrid class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_STGRID_H__81A0D371_1352_11D3_97D3_00001C0411B3__INCLUDED_)
#define AFX_STGRID_H__81A0D371_1352_11D3_97D3_00001C0411B3__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class CSTGrid : public CGXGridWnd  
{
protected:
	
	afx_msg  void  OnLButtonDown(UINT nFlags, CPoint point);
	DECLARE_MESSAGE_MAP()

public: 
	BOOL mDirty;
	void SetGrdID(long iID);
	int ClearMarks();
	int FindRow(CString cValue,int iCol,BOOL bMark, BOOL bSelect);
	BOOL SubClassDlgItem(UINT nID, CWnd *pParent);
	
	virtual	BOOL OnLButtonClickedRowCol(ROWCOL nRow, ROWCOL nCol, UINT nFlags, CPoint pt);

	virtual BOOL OnGridKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags);

	virtual BOOL LButtonUp(UINT nFlags, CPoint point, UINT nHitState);


	virtual BOOL OnDeleteCell();
	BOOL OnEndEditing(ROWCOL nRow, ROWCOL nCol);

	CSTGrid(); 
	virtual ~CSTGrid();

	
	CStringArray		olHagCodes;
	CStringArray		olHagNames;
	CStringArray		olHagPhones;
	CStringArray		olHagFax;


private:
	long mGrdID;
 
	BOOL bmSortAscend;
	BOOL bmSortNumerical;
};

#endif // !defined(AFX_STGRID_H__81A0D371_1352_11D3_97D3_00001C0411B3__INCLUDED_)
