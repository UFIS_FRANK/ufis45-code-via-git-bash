#ifndef __AircraftFamTableViewer_H__
#define __AircraftFamTableViewer_H__

#include "stdafx.h"
#include "CedaAfmData.h"
#include "CCSTable.h"
#include "CViewer.h"
#include "CCSPrint.h"


struct AIRCRAFTFAMTABLE_LINEDATA
{
	long 	 Urno; 	// Eindeutige Datensatz-Nr.
	CString  Afmc; 	// Code
	CString  Anam; 	// Bezeichnung

};

/////////////////////////////////////////////////////////////////////////////
// AircraftFamTableViewer

	  
class AircraftFamTableViewer : public CViewer
{
// Constructions
public:
    AircraftFamTableViewer(CCSPtrArray<AFMDATA> *popData);
    ~AircraftFamTableViewer();

    void Attach(CCSTable *popAttachWnd);
    virtual void ChangeViewTo(const char *pcpViewName);
// Internal data processing routines
private:
	bool IsPassFilter(AFMDATA *prpAircraftFam);
	int CompareAircraftFam(AIRCRAFTFAMTABLE_LINEDATA *prpAircraftFam1, AIRCRAFTFAMTABLE_LINEDATA *prpAircraftFam2);
    void MakeLines();
	void MakeLine(AFMDATA *prpAircraftFam);
	bool FindLine(long lpUrno, int &rilLineno);
// Operations
public:
	void DeleteAll();
	void CreateLine(AIRCRAFTFAMTABLE_LINEDATA *prpAircraftFam);
	void DeleteLine(int ipLineno);
// Window refreshing routines
public:
	void UpdateDisplay();
	CString Format(AIRCRAFTFAMTABLE_LINEDATA *prpLine);
	void ProcessAircraftFamChange(AFMDATA *prpAircraftFam);
	void ProcessAircraftFamDelete(AFMDATA *prpAircraftFam);
	bool FindAircraftFam(char *prpAircraftFamKeya, char *prpAircraftFamKeyd, int& ilItem);
// Attributes used for filtering condition
private:
	bool bmIsFromSearch;
// Attributes
private:
    CCSTable *pomAircraftFamTable;
	CCSPtrArray<AFMDATA> *pomData;
// Methods which handle changes (from Data Distributor)
public:
    CCSPtrArray<AIRCRAFTFAMTABLE_LINEDATA> omLines;
///////
	CCSPtrArray<TABLE_HEADER_COLUMN> omHeaderDataArray;
//Print 
	void PrintTableView();
	bool PrintTableLine(AIRCRAFTFAMTABLE_LINEDATA *prpLine,bool bpLastLine);
	bool PrintTableHeader();
	CCSPrint *pomPrint;
	//CBitmap omBitmap;
	CString omTableName;

};

#endif //__AircraftFamTableViewer_H__
