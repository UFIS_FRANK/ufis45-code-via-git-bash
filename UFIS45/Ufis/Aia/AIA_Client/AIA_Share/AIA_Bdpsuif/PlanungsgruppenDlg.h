// PlanungsgruppenDlg.h : Header-Datei
//

#ifndef AFX_PLANUNGSGRUPPENDLG_H__A9495301_AAE6_11D2_8E51_0000C002916B__INCLUDED_
#define AFX_PLANUNGSGRUPPENDLG_H__A9495301_AAE6_11D2_8E51_0000C002916B__INCLUDED_

#include "PrivList.h"
#include "CCSGlobl.h"
#include "CCSTable.h"
#include "CCSEdit.h"
#include "CCSComboBox.h"
#include "AwDlg.h"
#include "CedaPgpData.h"
#include "CedaPfcdata.h"
#include "CedaSpfdata.h"

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld PlanungsgruppenDlg 

class PlanungsgruppenDlg : public CDialog
{


// Konstruktion
public:
	PlanungsgruppenDlg::PlanungsgruppenDlg(PGPDATA *prpPgp, CWnd* pParent=NULL);   // Standardkonstruktor
	~PlanungsgruppenDlg();						

// Dialogfelddaten
	//{{AFX_DATA(PlanungsgruppenDlg)
	enum { IDD = IDD_PLANUNGSGRUPPENDLG };
	CCSComboBox	m_TYPE;
	CButton	m_OK;
	CStatic	m_HELP_ORG;
	CString m_Caption;
	CCSEdit	m_CDAT_D;
	CCSEdit	m_CDAT_T;
	CCSEdit	m_LSTU_D;
	CCSEdit	m_LSTU_T;
	CCSEdit	m_USEC;
	CCSEdit	m_USEU;
	CCSEdit	m_REMA;
	CCSEdit	m_PGPC;
	CCSEdit	m_PGPN;
	CCSEdit	m_ColTestField;
	//}}AFX_DATA


	CBrush omBrush;
// Überschreibungen
	// Vom Klassen-Assistenten generierte virtuelle Funktionsüberschreibungen
	//{{AFX_VIRTUAL(PlanungsgruppenDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	//}}AFX_VIRTUAL

// Implementierung
protected:

	CCSTable *pomTable;
	PGPDATA *pomPgp;
	
	CCSPtrArray <PFCDATA> *pomPfcData;
	CCSPtrArray <SPFDATA> *pomSpfData;

	void InitTables();
	void OnBf_Button(int ipColY);
	CString GetCodeShort(CString opCode);
	LONG OnTableIPEditKillfocus( UINT wParam, LPARAM lParam);

	// Generierte Nachrichtenzuordnungsfunktionen
	//{{AFX_MSG(PlanungsgruppenDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnB1();
	afx_msg void OnB2();
	afx_msg void OnB3();
	afx_msg void OnB4();
	afx_msg void OnB5();
	virtual void OnOK();
	virtual void OnCancel();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}

#endif // AFX_PLANUNGSGRUPPENDLG_H__A9495301_AAE6_11D2_8E51_0000C002916B__INCLUDED_
