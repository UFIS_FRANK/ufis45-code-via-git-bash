// AirportDlg.cpp : implementation file
//

#include "stdafx.h"
#include "BDPSUIF.h"
#include "AirportDlg.h"
#include "PrivList.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// AirportDlg dialog


AirportDlg::AirportDlg(APTDATA *popAPT,CWnd* pParent /*=NULL*/) : CDialog(AirportDlg::IDD, pParent)
{
	pomAPT = popAPT;
	
	//{{AFX_DATA_INIT(AirportDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void AirportDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(AirportDlg)
	DDX_Control(pDX, IDOK, m_OK);
	DDX_Control(pDX, IDC_CDAT_D, m_CDATD);
	DDX_Control(pDX, IDC_CDAT_T, m_CDATT);
	DDX_Control(pDX, IDC_LSTU_D, m_LSTUD);
	DDX_Control(pDX, IDC_LSTU_T, m_LSTUT);
	DDX_Control(pDX, IDC_USEU,	 m_USEU);
	DDX_Control(pDX, IDC_USEC,	 m_USEC);
	DDX_Control(pDX, IDC_APC3,	 m_APC3);
	DDX_Control(pDX, IDC_APC4,	 m_APC4);
	DDX_Control(pDX, IDC_APFN,	 m_APFN);
	DDX_Control(pDX, IDC_APSN,	 m_APSN);
	DDX_Control(pDX, IDC_APN2,	 m_APN2);
	DDX_Control(pDX, IDC_APN3,	 m_APN3);
	DDX_Control(pDX, IDC_APN4,	 m_APN4);
	DDX_Control(pDX, IDC_ETOF,	 m_ETOF);
	DDX_Control(pDX, IDC_GRUP,	 m_GRUP);
	DDX_Control(pDX, IDC_LAND,	 m_LAND);
	DDX_Control(pDX, IDC_TDI1,	 m_TDI1);
	DDX_Control(pDX, IDC_TDI2,	 m_TDI2);
	DDX_Control(pDX, IDC_TICH_D, m_TICHD);
	DDX_Control(pDX, IDC_TICH_T, m_TICHT);
	DDX_Control(pDX, IDC_VAFR_D, m_VAFRD);
	DDX_Control(pDX, IDC_VAFR_T, m_VAFRT);
	DDX_Control(pDX, IDC_VATO_D1, m_VATOD);
	DDX_Control(pDX, IDC_VATO_T1, m_VATOT);
	DDX_Control(pDX, IDC_APTT,	 m_APTT);
	DDX_Control(pDX, IDC_HOME,	 m_HOME);
	DDX_Control(pDX, IDC_TDIS,	 m_TDIS);
	DDX_Control(pDX, IDC_TDIW,	 m_TDIW);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(AirportDlg, CDialog)
	//{{AFX_MSG_MAP(AirportDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// AirportDlg message handlers
//----------------------------------------------------------------------------------------

BOOL AirportDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	m_Caption = LoadStg(IDS_STRING174) + " <" + m_Caption + ">";
	SetWindowText(m_Caption);
	char clStat = ogPrivList.GetStat("AIRPORTDLG.m_OK");
	SetWndStatAll(clStat,m_OK);

	//------------------------------------
	m_CDATD.SetBKColor(SILVER);
	m_CDATD.SetInitText(pomAPT->Cdat.Format("%d.%m.%Y"));
	m_CDATD.SetSecState(ogPrivList.GetStat("AIRPORTDLG.m_CDAT"));
	m_CDATT.SetBKColor(SILVER);
	m_CDATT.SetInitText(pomAPT->Cdat.Format("%H:%M"));
	m_CDATT.SetSecState(ogPrivList.GetStat("AIRPORTDLG.m_CDAT"));
	//------------------------------------
	m_LSTUD.SetBKColor(SILVER);
	m_LSTUD.SetInitText(pomAPT->Lstu.Format("%d.%m.%Y"));
	m_LSTUD.SetSecState(ogPrivList.GetStat("AIRPORTDLG.m_LSTU"));
	m_LSTUT.SetBKColor(SILVER);
	m_LSTUT.SetInitText(pomAPT->Lstu.Format("%H:%M"));
	m_LSTUT.SetSecState(ogPrivList.GetStat("AIRPORTDLG.m_LSTU"));
	//------------------------------------
	m_USEC.SetBKColor(SILVER);
	m_USEC.SetInitText(pomAPT->Usec);
	m_USEC.SetSecState(ogPrivList.GetStat("AIRPORTDLG.m_USEC"));
	//------------------------------------
	m_USEU.SetBKColor(SILVER);
	m_USEU.SetInitText(pomAPT->Useu);
	m_USEU.SetSecState(ogPrivList.GetStat("AIRPORTDLG.m_USEU"));
	//------------------------------------
	m_GRUP.SetBKColor(SILVER);
	//m_GRUP.SetInitText(pomAPT->);
	m_GRUP.SetSecState(ogPrivList.GetStat("AIRPORTDLG.m_GRUP"));
	//------------------------------------//WHITE,YELLOW,SILVER,RED,LYELLOW
	m_APC3.SetFormat("x|#x|#x|#");
	m_APC3.SetTextLimit(-1,-1,true);
	m_APC3.SetTextErrColor(RED);
	m_APC3.SetInitText(pomAPT->Apc3);
	m_APC3.SetSecState(ogPrivList.GetStat("AIRPORTDLG.m_APC3"));
	//------------------------------------
	m_APC4.SetFormat("x|#x|#x|#x|#");
	m_APC4.SetBKColor(YELLOW);  
	m_APC4.SetTextErrColor(RED);
	m_APC4.SetInitText(pomAPT->Apc4);
	m_APC4.SetSecState(ogPrivList.GetStat("AIRPORTDLG.m_APC4"));
	//------------------------------------
	m_APFN.SetTypeToString("X(32)",32,1);
	m_APFN.SetBKColor(YELLOW);
	m_APFN.SetTextErrColor(RED);
	m_APFN.SetInitText(pomAPT->Apfn);
	m_APFN.SetSecState(ogPrivList.GetStat("AIRPORTDLG.m_APFN"));
	//------------------------------------
	m_APSN.SetTypeToString("X(20)",20,1);
	m_APSN.SetBKColor(YELLOW);
	m_APSN.SetTextErrColor(RED);
	m_APSN.SetInitText(pomAPT->Apsn);
	m_APSN.SetSecState(ogPrivList.GetStat("AIRPORTDLG.m_APSN"));
	//------------------------------------
	m_APN2.SetTypeToString("X(20)",20,0);
	m_APN2.SetTextErrColor(RED);
	m_APN2.SetInitText(pomAPT->Apn2);
	m_APN2.SetSecState(ogPrivList.GetStat("AIRPORTDLG.m_APN2"));
	//------------------------------------
	m_APN3.SetTypeToString("X(20)",20,0);
	m_APN3.SetTextErrColor(RED);
	m_APN3.SetInitText(pomAPT->Apn3);
	m_APN3.SetSecState(ogPrivList.GetStat("AIRPORTDLG.m_APN3"));
	//------------------------------------
	m_APN4.SetTypeToString("X(20)",20,0);
	m_APN4.SetTextErrColor(RED);
	m_APN4.SetInitText(pomAPT->Apn4);
	m_APN4.SetSecState(ogPrivList.GetStat("AIRPORTDLG.m_APN4"));
	//------------------------------------
	//m_LAND.SetFormat("XX");
	m_LAND.SetTypeToString("X(2)",2,1);
	m_LAND.SetBKColor(YELLOW);  
	m_LAND.SetTextErrColor(RED);
	m_LAND.SetInitText(pomAPT->Land);
	m_LAND.SetSecState(ogPrivList.GetStat("AIRPORTDLG.m_LAND"));
	//------------------------------------
	m_APTT.SetFormat("X");
	m_APTT.SetBKColor(YELLOW);  
	m_APTT.SetTextErrColor(RED);
	m_APTT.SetInitText(pomAPT->Aptt);
	m_APTT.SetSecState(ogPrivList.GetStat("AIRPORTDLG.m_APTT"));
	//------------------------------------
	m_ETOF.SetTypeToInt(0,9999);
	m_ETOF.SetTextErrColor(RED);
	m_ETOF.SetInitText(pomAPT->Etof);
	m_ETOF.SetSecState(ogPrivList.GetStat("AIRPORTDLG.m_ETOF"));
	//------------------------------------
	m_TICHD.SetTypeToDate();
	m_TICHD.SetTextErrColor(RED);
	m_TICHD.SetInitText(pomAPT->Tich.Format("%d.%m.%Y"));
	m_TICHD.SetSecState(ogPrivList.GetStat("AIRPORTDLG.m_TICH"));
	// - - - - - - - - - - - - - - - - - - 
	m_TICHT.SetTypeToTime();
	m_TICHT.SetTextErrColor(RED);
	m_TICHT.SetInitText(pomAPT->Tich.Format("%H:%M"));
	m_TICHT.SetSecState(ogPrivList.GetStat("AIRPORTDLG.m_TICH"));
	//------------------------------------
	m_TDI1.SetTypeToInt(-9999,9999);
	m_TDI1.SetTextErrColor(RED);
	m_TDI1.SetInitText(pomAPT->Tdi1);
	m_TDI1.SetSecState(ogPrivList.GetStat("AIRPORTDLG.m_TDI1"));
	//------------------------------------
	m_TDI2.SetTypeToInt(-9999,9999);
	m_TDI2.SetTextErrColor(RED);
	m_TDI2.SetInitText(pomAPT->Tdi2);
	m_TDI2.SetSecState(ogPrivList.GetStat("AIRPORTDLG.m_TDI2"));
	//------------------------------------
	m_VAFRD.SetTypeToDate(true);
	m_VAFRD.SetTextErrColor(RED);
	m_VAFRD.SetBKColor(YELLOW);
	m_VAFRD.SetInitText(pomAPT->Vafr.Format("%d.%m.%Y"));
	m_VAFRD.SetSecState(ogPrivList.GetStat("AIRPORTDLG.m_VAFR"));
	// - - - - - - - - - - - - - - - - - -
	m_VAFRT.SetTypeToTime(true);
	m_VAFRT.SetTextErrColor(RED);
	m_VAFRT.SetBKColor(YELLOW);
	m_VAFRT.SetInitText(pomAPT->Vafr.Format("%H:%M"));
	m_VAFRT.SetSecState(ogPrivList.GetStat("AIRPORTDLG.m_VAFR"));
	//------------------------------------
	m_VATOD.SetTypeToDate();
	m_VATOD.SetTextErrColor(RED);
	m_VATOD.SetInitText(pomAPT->Vato.Format("%d.%m.%Y"));
	m_VATOD.SetSecState(ogPrivList.GetStat("AIRPORTDLG.m_VATO"));
	// - - - - - - - - - - - - - - - - - -
	m_VATOT.SetTypeToTime();
	m_VATOT.SetTextErrColor(RED);
	m_VATOT.SetInitText(pomAPT->Vato.Format("%H:%M"));
	m_VATOT.SetSecState(ogPrivList.GetStat("AIRPORTDLG.m_VATO"));
	//------------------------------------
	m_HOME.SetTypeToString("X(3)",3,1);
	m_HOME.SetTextErrColor(RED);
	m_HOME.SetInitText(pomAPT->Home);
	m_HOME.SetSecState(ogPrivList.GetStat("AIRPORTDLG.m_HOME"));
	//------------------------------------
	m_TDIS.SetTypeToInt(-9999,9999);
	m_TDIS.SetTextErrColor(RED);
	m_TDIS.SetInitText(pomAPT->Tdis);
	m_TDIS.SetSecState(ogPrivList.GetStat("AIRPORTDLG.m_TDIS"));
	//------------------------------------
	m_TDIW.SetTypeToInt(-9999,9999);
	m_TDIW.SetTextErrColor(RED);
	m_TDIW.SetInitText(pomAPT->Tdiw);
	m_TDIW.SetSecState(ogPrivList.GetStat("AIRPORTDLG.m_TDIW"));

	return TRUE;  
}

//----------------------------------------------------------------------------------------

void AirportDlg::OnOK() 
{
	AfxGetApp()->DoWaitCursor(1);
	CString olErrorText;
	bool ilStatus = true;
	if(m_APC3.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING290) + ogNotFormat;
	}
	if(m_APC4.GetStatus() == false)
	{
		ilStatus = false;
		if(m_APC4.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING300) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING300) + ogNotFormat;
		}
	}
	if(m_APFN.GetStatus() == false)
	{
		ilStatus = false;
		if(m_APFN.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING288) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING288) + ogNotFormat;
		}
	}
	if(m_APSN.GetStatus() == false)
	{
		ilStatus = false;
		if(m_APSN.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING301) + CString(" 1") +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING301) + CString(" 1") + ogNotFormat;
		}
	}
	if(m_APN2.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING301) + CString(" 2") + ogNotFormat;
	}
	if(m_APN3.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING301) + CString(" 3") + ogNotFormat;
	}
	if(m_APN4.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING301) + CString(" 4") + ogNotFormat;
	}
	if(m_LAND.GetStatus() == false)
	{
		ilStatus = false;
		if(m_LAND.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING302) +  ogNoData;
		}
		//else
		//{
		//	olErrorText += LoadStg(IDS_STRING302) + ogNotFormat;
		//}
	}
	if(m_APTT.GetStatus() == false)
	{
		ilStatus = false;
		if(m_APTT.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING303) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING303) + ogNotFormat;
		}
	}
	if(m_ETOF.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING304) + ogNotFormat;
	}
	if(m_TICHD.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING305) + LoadStg(IDS_STRING232) + ogNotFormat;
	}
	if(m_TICHT.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING305) + LoadStg(IDS_STRING233) + ogNotFormat;
	}
	if(m_TDI1.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING306) + ogNotFormat;
	}
	if(m_TDI2.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING307) + ogNotFormat;
	}
	if(m_VAFRD.GetStatus() == false)
	{
		ilStatus = false;
		if(m_VAFRD.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING230) + LoadStg(IDS_STRING232) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING230) + LoadStg(IDS_STRING232) + ogNotFormat;
		}
	}
	if(m_VAFRT.GetStatus() == false)
	{
		ilStatus = false;
		if(m_VAFRT.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING230) + LoadStg(IDS_STRING233) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING230) + LoadStg(IDS_STRING233) + ogNotFormat;
		}
	}
	if(m_VATOD.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING231) + LoadStg(IDS_STRING232) + ogNotFormat;
	}
	if(m_VATOT.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING231) + LoadStg(IDS_STRING233) + ogNotFormat;
	}

	if(m_TDIS.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING729) + ogNotFormat;
	}
	if(m_TDIW.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING730) + ogNotFormat;
	}

	///////////////////////////////////////////////////////////////////////////
	CString olVafrd,olVafrt,olVatod,olVatot;
	CTime olTmpTimeFr,olTmpTimeTo;
	m_VAFRD.GetWindowText(olVafrd);
	m_VAFRT.GetWindowText(olVafrt);
	m_VATOD.GetWindowText(olVatod);
	m_VATOT.GetWindowText(olVatot);

	if((m_VAFRD.GetWindowTextLength() != 0 && m_VAFRT.GetWindowTextLength() == 0) || (m_VAFRD.GetWindowTextLength() == 0 && m_VAFRT.GetWindowTextLength() != 0))
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING234);
	}
	if((m_VATOD.GetWindowTextLength() != 0 && m_VATOT.GetWindowTextLength() == 0) || (m_VATOD.GetWindowTextLength() == 0 && m_VATOT.GetWindowTextLength() != 0))
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING235);
	}
	if(m_VAFRD.GetStatus() == true && m_VAFRT.GetStatus() == true && m_VATOD.GetStatus() == true && m_VATOT.GetStatus() == true)
	{
		olTmpTimeFr = DateHourStringToDate(olVafrd,olVafrt);
		olTmpTimeTo = DateHourStringToDate(olVatod,olVatot);
		if(olTmpTimeTo != -1 && (olTmpTimeTo < olTmpTimeFr || olTmpTimeFr == -1))
		{
			ilStatus = false;
			olErrorText += LoadStg(IDS_STRING236);
		}
	}

	if((m_TICHD.GetWindowTextLength() != 0 && m_TICHT.GetWindowTextLength() == 0) || (m_TICHD.GetWindowTextLength() == 0 && m_TICHT.GetWindowTextLength() != 0))
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING308);
	}
	//Konsistenz-Check//
	if(ilStatus == true)
	{
		CString olApc3,olApc4, olAptt;
		char clWhere[100];
		CCSPtrArray<APTDATA> olAptCPA;

		m_APC3.GetWindowText(olApc3);
		if(olApc3.GetLength() == 3)
		{
			sprintf(clWhere,"WHERE APC3='%s'",olApc3);
			if(ogAPTData.ReadSpecialData(&olAptCPA,clWhere,"URNO,APC3",false) == true)
			{
				if(olAptCPA[0].Urno != pomAPT->Urno)
				{
					ilStatus = false;
					olErrorText += LoadStg(IDS_STRING290) + LoadStg(IDS_EXIST);
				}
			}
			olAptCPA.DeleteAll();
		}

		m_APC4.GetWindowText(olApc4);
		sprintf(clWhere,"WHERE APC4='%s'",olApc4);
		if(ogAPTData.ReadSpecialData(&olAptCPA,clWhere,"URNO,APC4",false) == true)
		{
			if(olAptCPA[0].Urno != pomAPT->Urno)
			{
				ilStatus = false;
				olErrorText += LoadStg(IDS_STRING300) + LoadStg(IDS_EXIST);
			}
		}
		olAptCPA.DeleteAll();

		m_APTT.GetWindowText(olAptt);
		if ( (olAptt.Find('S') != 0) && (olAptt.Find('D') != 0) && (olAptt.Find('I') != 0) )
		{
			ilStatus = false;
			olErrorText += LoadStg(IDS_STRING710) + LoadStg(IDS_EXIST);
		}

	}
	///////////////////////////////////////////////////////////////////////////
	AfxGetApp()->DoWaitCursor(-1);
	if (ilStatus == true)
	{
		m_APC3.GetWindowText(pomAPT->Apc3,4);
		m_APC4.GetWindowText(pomAPT->Apc4,5);
		m_APFN.GetWindowText(pomAPT->Apfn,33);
		m_APSN.GetWindowText(pomAPT->Apsn,21);
		m_APN2.GetWindowText(pomAPT->Apn2,21);
		m_APN3.GetWindowText(pomAPT->Apn3,21);
		m_APN4.GetWindowText(pomAPT->Apn4,21);
		m_LAND.GetWindowText(pomAPT->Land,3);
		m_APTT.GetWindowText(pomAPT->Aptt,2);
		m_ETOF.GetWindowText(pomAPT->Etof,5);
		m_TDI1.GetWindowText(pomAPT->Tdi1,5);
		m_TDI2.GetWindowText(pomAPT->Tdi2,5);
		m_HOME.GetWindowText(pomAPT->Home,4);
		CString olTichd,olTicht;
		m_TICHD.GetWindowText(olTichd);
		m_TICHT.GetWindowText(olTicht);
		pomAPT->Tich = DateHourStringToDate(olTichd,olTicht);
		pomAPT->Vafr = DateHourStringToDate(olVafrd,olVafrt);
		pomAPT->Vato = DateHourStringToDate(olVatod,olVatot);
		m_TDIS.GetWindowText(pomAPT->Tdis,5);
		m_TDIW.GetWindowText(pomAPT->Tdiw,5);

		CDialog::OnOK();
	}
	else
	{
		Beep(440,70);
		MessageBox(olErrorText,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
		m_APC3.SetFocus();
		m_APC3.SetSel(0,-1);
	}
}

//----------------------------------------------------------------------------------------
