// AirlineDlg.cpp : implementation file
//

#include "stdafx.h"
#include "BDPSUIF.h"
#include "AirlineDlg.h"
#include "PrivList.h"


#include "CedaBasicData.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


/////////////////////////////////////////////////////////////////////////////
// AirlineDlg dialog


AirlineDlg::AirlineDlg(ALTDATA *popALT,CWnd* pParent /*=NULL*/) : CDialog(AirlineDlg::IDD, pParent)
{
	pomALT = popALT;
	
	//{{AFX_DATA_INIT(AirlineDlg)
	//}}AFX_DATA_INIT
}


void AirlineDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(AirlineDlg)
	DDX_Control(pDX, IDOK,		 m_OK);
	DDX_Control(pDX, IDCANCEL,	 m_CANCEL);
	DDX_Control(pDX, IDC_CASH_K, m_CASHK);
	DDX_Control(pDX, IDC_CASH_B, m_CASHB);
	DDX_Control(pDX, IDC_CASH_U, m_CASHU);
	DDX_Control(pDX, IDC_ALC2,	 m_ALC2);
	DDX_Control(pDX, IDC_ALC3,	 m_ALC3);
	DDX_Control(pDX, IDC_ALFN,	 m_ALFN);
	DDX_Control(pDX, IDC_CDAT_D, m_CDATD);
	DDX_Control(pDX, IDC_CDAT_T, m_CDATT);
	DDX_Control(pDX, IDC_LSTU_D, m_LSTUD);
	DDX_Control(pDX, IDC_LSTU_T, m_LSTUT);
	DDX_Control(pDX, IDC_USEU,	 m_USEU);
	DDX_Control(pDX, IDC_USEC,	 m_USEC);
	DDX_Control(pDX, IDC_GRUP,	 m_GRUP);
	DDX_Control(pDX, IDC_VAFR_D, m_VAFRD);
	DDX_Control(pDX, IDC_VAFR_T, m_VAFRT);
	DDX_Control(pDX, IDC_VATO_D, m_VATOD);
	DDX_Control(pDX, IDC_VATO_T, m_VATOT);
	DDX_Control(pDX, IDC_TERM,	 m_TERM);
	DDX_Control(pDX, IDC_AKEY,	 m_AKEY);
	DDX_Control(pDX, IDC_RPRT,	 m_RPRT);
	DDX_Control(pDX, IDC_WRKO,	 m_WRKO);
	DDX_Control(pDX, IDC_ADMD,	 m_ADMD);
	DDX_Control(pDX, IDC_ADD1,	 m_ADD1);
	DDX_Control(pDX, IDC_ADD2,	 m_ADD2);
	DDX_Control(pDX, IDC_ADD3,	 m_ADD3);
	DDX_Control(pDX, IDC_ADD4,	 m_ADD4);
	DDX_Control(pDX, IDC_BASE,	 m_BASE);
	DDX_Control(pDX, IDC_CONT,	 m_CONT);
	DDX_Control(pDX, IDC_CTRY,	 m_CTRY);
	DDX_Control(pDX, IDC_EMPS,	 m_EMPS);
	DDX_Control(pDX, IDC_EXEC,	 m_EXEC);
	DDX_Control(pDX, IDC_FOND,	 m_FOND);
	DDX_Control(pDX, IDC_IANO,	 m_IANO);
	DDX_Control(pDX, IDC_IATA,	 m_IATA);
	DDX_Control(pDX, IDC_ICAL,	 m_ICAL);
	DDX_Control(pDX, IDC_ICAO,	 m_ICAO);
	DDX_Control(pDX, IDC_LCOD,	 m_LCOD);
	DDX_Control(pDX, IDC_PHON,	 m_PHON);
	DDX_Control(pDX, IDC_SITA,	 m_SITA);
	DDX_Control(pDX, IDC_STXT,	 m_STXT);
	DDX_Control(pDX, IDC_TFAX,	 m_TFAX);
	DDX_Control(pDX, IDC_TELX,	 m_TELX);
	DDX_Control(pDX, IDC_WEBS,	 m_WEBS);
	DDX_Control(pDX, IDC_HOME,	 m_HOME);
	DDX_Control(pDX, IDC_DOIN,	 m_DOIN);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(AirlineDlg, CDialog)
	//{{AFX_MSG_MAP(AirlineDlg)
    ON_MESSAGE(WM_EDIT_KILLFOCUS, OnKillfocus)
	ON_BN_CLICKED(IDC_ADD, OnAdd)
	ON_BN_CLICKED(IDC_DELETE, OnDelete)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// AirlineDlg message handlers
//----------------------------------------------------------------------------------------

BOOL AirlineDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	m_Caption = LoadStg(IDS_STRING173) + " <" + m_Caption + ">";
	SetWindowText(m_Caption);
	char clStat = ogPrivList.GetStat("AIRLINEDLG.m_OK");
	SetWndStatAll(clStat,m_OK);

	//------------------------------------
	m_CDATD.SetBKColor(SILVER);
	m_CDATD.SetInitText(pomALT->Cdat.Format("%d.%m.%Y"));
	m_CDATD.SetSecState(ogPrivList.GetStat("AIRLINEDLG.m_CDAT"));
	m_CDATT.SetBKColor(SILVER);
	m_CDATT.SetInitText(pomALT->Cdat.Format("%H:%M"));
	m_CDATT.SetSecState(ogPrivList.GetStat("AIRLINEDLG.m_CDAT"));
	//------------------------------------
	m_LSTUD.SetBKColor(SILVER);
	m_LSTUD.SetInitText(pomALT->Lstu.Format("%d.%m.%Y"));
	m_LSTUD.SetSecState(ogPrivList.GetStat("AIRLINEDLG.m_LSTU"));
	m_LSTUT.SetBKColor(SILVER);
	m_LSTUT.SetInitText(pomALT->Lstu.Format("%H:%M"));
	m_LSTUT.SetSecState(ogPrivList.GetStat("AIRLINEDLG.m_LSTU"));
	//------------------------------------
	m_USEC.SetBKColor(SILVER);
	m_USEC.SetInitText(pomALT->Usec);
	m_USEC.SetSecState(ogPrivList.GetStat("AIRLINEDLG.m_USEC"));
	//------------------------------------
	m_USEU.SetBKColor(SILVER);
	m_USEU.SetInitText(pomALT->Useu);
	m_USEU.SetSecState(ogPrivList.GetStat("AIRLINEDLG.m_USEU"));
	//------------------------------------
	m_GRUP.SetBKColor(SILVER);
	//m_GRUP.SetInitText(???);
	m_GRUP.SetSecState(ogPrivList.GetStat("AIRLINEDLG.m_GRUP"));
	//------------------------------------//WHITE,YELLOW,SILVER,RED,LYELLOW
	m_ALFN.SetTypeToString("X(32)",32,1);
//	m_ALFN.SetBKColor(YELLOW);
	m_ALFN.SetTextErrColor(RED);
	m_ALFN.SetInitText(pomALT->Alfn);
	m_ALFN.SetSecState(ogPrivList.GetStat("AIRLINEDLG.m_ALFN"));
	//------------------------------------
	m_RPRT.SetTypeToString("X(10)",10,1);
//	m_RPRT.SetBKColor(YELLOW);
	m_RPRT.SetTextErrColor(RED);
	m_RPRT.SetInitText(pomALT->Rprt);
	m_RPRT.SetSecState(ogPrivList.GetStat("AIRLINEDLG.m_RPRT"));
	//------------------------------------
	m_WRKO.SetTypeToString("X(10)",10,1);
//	m_WRKO.SetBKColor(YELLOW);
	m_WRKO.SetTextErrColor(RED);
	m_WRKO.SetInitText(pomALT->Wrko);
	m_WRKO.SetSecState(ogPrivList.GetStat("AIRLINEDLG.m_WRKO"));
	//------------------------------------
	m_ADMD.SetTypeToString("X(10)",10,1);
	m_ADMD.SetTextErrColor(RED);
	m_ADMD.SetInitText(pomALT->Admd);
	m_ADMD.SetSecState(ogPrivList.GetStat("AIRLINEDLG.m_ADMD"));
	//------------------------------------
	m_HOME.SetTypeToString("X(3)",3,1);
	m_HOME.SetTextErrColor(RED);
	m_HOME.SetInitText(pomALT->Home);
	m_HOME.SetSecState(ogPrivList.GetStat("AIRLINEDLG.m_HOME"));
	//------------------------------------
	m_DOIN.SetTypeToString("X(1)",1,1);
	m_DOIN.SetTextErrColor(RED);
	m_DOIN.SetInitText(pomALT->Doin);
	m_DOIN.SetSecState(ogPrivList.GetStat("AIRLINEDLG.m_DOIN"));
	//------------------------------------
	m_ALC2.SetFormat("x|#x|#");
	//m_ALC2.SetBKColor(YELLOW);
	m_ALC2.SetBKColor(WHITE);
	m_ALC2.SetTextErrColor(RED);
	m_ALC2.SetInitText(pomALT->Alc2);
	m_ALC2.SetSecState(ogPrivList.GetStat("AIRLINEDLG.m_ALC2"));
	//------------------------------------
	m_ALC3.SetFormat("x|#x|#x|#");
	m_ALC3.SetBKColor(YELLOW);
	m_ALC3.SetTextErrColor(RED);
	m_ALC3.SetInitText(pomALT->Alc3);
	m_ALC3.SetSecState(ogPrivList.GetStat("AIRLINEDLG.m_ALC3"));
	//------------------------------------
	m_AKEY.SetFormat("x|#x|#x|#x|#x|#");
	m_AKEY.SetTextLimit(1,5);
	//m_AKEY.SetBKColor(YELLOW);
	m_AKEY.SetBKColor(WHITE);
	m_AKEY.SetTextErrColor(RED);
	m_AKEY.SetInitText(pomALT->Akey);
	m_AKEY.SetSecState(ogPrivList.GetStat("AIRLINEDLG.m_AKEY"));
	//------------------------------------
	m_CASHK.SetCheck(0);
	m_CASHB.SetCheck(0);
	m_CASHU.SetCheck(0);
	if (pomALT->Cash[0] == 'K')		 m_CASHK.SetCheck(1);
	else if (pomALT->Cash[0] == 'B') m_CASHB.SetCheck(1);
	else m_CASHU.SetCheck(1);
	clStat = ogPrivList.GetStat("AIRLINEDLG.m_CASH");
	SetWndStatAll(clStat,m_CASHK);
	SetWndStatAll(clStat,m_CASHB);
	SetWndStatAll(clStat,m_CASHU);
	//------------------------------------
	//m_TERM.SetFormat("x|#','x|#','x|#','x|#','x|#','x|#','x|#','x|#','x|#','x|#");
	m_TERM.SetFormat("x|#','x|#','x|#','x|#','x|#','x|#','x|#','x|#','x|#");
	m_TERM.SetTextLimit(0,18);
	m_TERM.SetBKColor(WHITE);
	m_TERM.SetTextErrColor(RED);
	m_TERM.SetInitText(pomALT->Term);
	m_TERM.SetSecState(ogPrivList.GetStat("AIRLINEDLG.m_TERM"));
	//------------------------------------
	m_VAFRD.SetTypeToDate(true);
	m_VAFRD.SetTextErrColor(RED);
	m_VAFRD.SetBKColor(YELLOW);
	m_VAFRD.SetInitText(pomALT->Vafr.Format("%d.%m.%Y"));
	m_VAFRD.SetSecState(ogPrivList.GetStat("AIRLINEDLG.m_VAFR"));
	// - - - - - - - - - - - - - - - - - -
	m_VAFRT.SetTypeToTime(true);
	m_VAFRT.SetTextErrColor(RED);
	m_VAFRT.SetBKColor(YELLOW);
	m_VAFRT.SetInitText(pomALT->Vafr.Format("%H:%M"));
	m_VAFRT.SetSecState(ogPrivList.GetStat("AIRLINEDLG.m_VAFR"));
	//------------------------------------
	m_VATOD.SetTypeToDate();
	m_VATOD.SetTextErrColor(RED);
	m_VATOD.SetInitText(pomALT->Vato.Format("%d.%m.%Y"));
	m_VATOD.SetSecState(ogPrivList.GetStat("AIRLINEDLG.m_VATO"));
	// - - - - - - - - - - - - - - - - - -
	m_VATOT.SetTypeToTime();
	m_VATOT.SetTextErrColor(RED);
	m_VATOT.SetInitText(pomALT->Vato.Format("%H:%M"));
	m_VATOT.SetSecState(ogPrivList.GetStat("AIRLINEDLG.m_VATO"));
	//------------------------------------
	m_ADD1.SetTypeToString("X(60)",60,1);
	m_ADD1.SetTextErrColor(RED);
	m_ADD1.SetInitText(pomALT->Add1);
	m_ADD1.SetSecState(ogPrivList.GetStat("AIRLINEDLG.m_ADD1"));
	//------------------------------------
	m_ADD2.SetTypeToString("X(60)",60,1);
	m_ADD2.SetTextErrColor(RED);
	m_ADD2.SetInitText(pomALT->Add2);
	m_ADD2.SetSecState(ogPrivList.GetStat("AIRLINEDLG.m_ADD2"));
	//------------------------------------
	m_ADD3.SetTypeToString("X(40)",40,1);
	m_ADD3.SetTextErrColor(RED);
	m_ADD3.SetInitText(pomALT->Add3);
	m_ADD3.SetSecState(ogPrivList.GetStat("AIRLINEDLG.m_ADD3"));
	//------------------------------------
	m_ADD4.SetTypeToString("X(40)",40,1);
	m_ADD4.SetTextErrColor(RED);
	m_ADD4.SetInitText(pomALT->Add4);
	m_ADD4.SetSecState(ogPrivList.GetStat("AIRLINEDLG.m_ADD4"));
	//------------------------------------
	m_BASE.SetTypeToString("X(60)",60,1);
	m_BASE.SetTextErrColor(RED);
	m_BASE.SetInitText(pomALT->Base);
	m_BASE.SetSecState(ogPrivList.GetStat("AIRLINEDLG.m_BASE"));
	//------------------------------------
	m_CONT.SetTypeToString("X(1)",1,1);
	m_CONT.SetTextErrColor(RED);
	m_CONT.SetInitText(pomALT->Cont);
	m_CONT.SetSecState(ogPrivList.GetStat("AIRLINEDLG.m_CONT"));
	//------------------------------------
	m_CTRY.SetTypeToString("X(10)",10,1);
	m_CTRY.SetTextErrColor(RED);
	m_CTRY.SetInitText(pomALT->Ctry);
	m_CTRY.SetSecState(ogPrivList.GetStat("AIRLINEDLG.m_CTRY"));
	//------------------------------------
	m_EMPS.SetTypeToString("X(5)",5,1);
	m_EMPS.SetTextErrColor(RED);
	m_EMPS.SetInitText(pomALT->Emps);
	m_EMPS.SetSecState(ogPrivList.GetStat("AIRLINEDLG.m_EMPS"));
	//------------------------------------
	m_EXEC.SetTypeToString("X(32)",32,1);
	m_EXEC.SetTextErrColor(RED);
	m_EXEC.SetInitText(pomALT->Exec);
	m_EXEC.SetSecState(ogPrivList.GetStat("AIRLINEDLG.m_EXEC"));
	//------------------------------------
	m_FOND.SetTypeToString("X(4)",4,1);
	m_FOND.SetTextErrColor(RED);
	m_FOND.SetInitText(pomALT->Fond);
	m_FOND.SetSecState(ogPrivList.GetStat("AIRLINEDLG.m_FOND"));
	//------------------------------------
	m_IANO.SetTypeToString("X(4)",4,1);
	m_IANO.SetTextErrColor(RED);
	m_IANO.SetInitText(pomALT->Iano);
	m_IANO.SetSecState(ogPrivList.GetStat("AIRLINEDLG.m_IANO"));
	//------------------------------------
	m_IATA.SetTypeToString("X(2)",2,1);
	m_IATA.SetTextErrColor(RED);
	m_IATA.SetInitText(pomALT->Iata);
	m_IATA.SetSecState(ogPrivList.GetStat("AIRLINEDLG.m_IATA"));
	//------------------------------------
	m_ICAL.SetTypeToString("X(12)",12,1);
	m_ICAL.SetTextErrColor(RED);
	m_ICAL.SetInitText(pomALT->Ical);
	m_ICAL.SetSecState(ogPrivList.GetStat("AIRLINEDLG.m_ICAL"));
	//------------------------------------
	m_ICAO.SetTypeToString("X(3)",3,1);
	m_ICAO.SetTextErrColor(RED);
	m_ICAO.SetInitText(pomALT->Icao);
	m_ICAO.SetSecState(ogPrivList.GetStat("AIRLINEDLG.m_ICAO"));
	//------------------------------------
	m_LCOD.SetTypeToString("X(4)",4,1);
	m_LCOD.SetTextErrColor(RED);
	m_LCOD.SetInitText(pomALT->Lcod);
	m_LCOD.SetSecState(ogPrivList.GetStat("AIRLINEDLG.m_LCOD"));
	//------------------------------------
	m_PHON.SetTypeToString("X(16)",16,1);
	m_PHON.SetTextErrColor(RED);
	m_PHON.SetInitText(pomALT->Phon);
	m_PHON.SetSecState(ogPrivList.GetStat("AIRLINEDLG.m_PHON"));
	//------------------------------------
	//m_SITA.SetTypeToString("X(7)",7,1);
	m_SITA.SetFormat("x|#x|#x|#x|#x|#x|#x|#");
	m_SITA.SetTextErrColor(RED);
	m_SITA.SetInitText(pomALT->Sita);
	m_SITA.SetSecState(ogPrivList.GetStat("AIRLINEDLG.m_SITA"));
	//------------------------------------
	m_STXT.SetTypeToString("X(20)",20,1);
	m_STXT.SetTextErrColor(RED);
	m_STXT.SetInitText(pomALT->Text);
	m_STXT.SetSecState(ogPrivList.GetStat("AIRLINEDLG.m_STXT"));
	//------------------------------------
	m_TFAX.SetTypeToString("X(16)",16,1);
	m_TFAX.SetTextErrColor(RED);
	m_TFAX.SetInitText(pomALT->Tfax);
	m_TFAX.SetSecState(ogPrivList.GetStat("AIRLINEDLG.m_TFAX"));
	//------------------------------------
	m_TELX.SetTypeToString("X(16)",16,1);
	m_TELX.SetTextErrColor(RED);
	m_TELX.SetInitText(pomALT->Telx);
	m_TELX.SetSecState(ogPrivList.GetStat("AIRLINEDLG.m_TELX"));
	//------------------------------------
	m_WEBS.SetTypeToString("X(40)",40,1);
	m_WEBS.SetTextErrColor(RED);
	m_WEBS.SetInitText(pomALT->Webs);
	m_WEBS.SetSecState(ogPrivList.GetStat("AIRLINEDLG.m_WEBS"));
	//



	//20001108SHA GRID ---------------------------------------
	olGrdDel = new CSTGrid;
	olGrdDel->SubClassDlgItem(IDC_GRID, this);
	olGrdDel->Initialize();
	olGrdDel->SetColCount(8);	

	olGrdDel->GetParam()->EnableTrackRowHeight(FALSE);		//disable rowsizing
	olGrdDel->GetParam()->EnableMoveRows(FALSE);			//disable rowmoving
	olGrdDel->GetParam()->EnableSelection(GX_SELNONE);	

	olGrdDel->HideCols(8,8);
	olGrdDel->HideCols(7,7);
	olGrdDel->SetColWidth(1,1,120);
	olGrdDel->SetColWidth(6,6,120);

	

	olGrdDel->SetValueRange(CGXRange(0,1),"Task");
	olGrdDel->SetValueRange(CGXRange(0,2),"Code");
	olGrdDel->SetValueRange(CGXRange(0,3),"Name");
	olGrdDel->SetValueRange(CGXRange(0,4),"Phone");
	olGrdDel->SetValueRange(CGXRange(0,5),"Fax");
	olGrdDel->SetValueRange(CGXRange(0,6),"Remark");
	olGrdDel->SetValueRange(CGXRange(0,7),"Urno");
	olGrdDel->SetValueRange(CGXRange(0,8),"STATUS");
	

	
	FillHandlingAgents();	
	FillHandlingTypes();	


	
	// FILL WITH EXISTING DATA FROM HAITAB
	CString clWhere;
	CString clDmy;

	clWhere.Format("WHERE ALTU=%d",pomALT->Urno);
	ogBCD.SetObject("HAI");
	ogBCD.Read("HAI",clWhere);	

	int ilDataCount=ogBCD.GetDataCount("HAI"); //number of rows
	olGrdDel->SetRowCount(ilDataCount);	


	int i;
	for(i = 0; i < ilDataCount; i++)
	{
		RecordSet olRecord;
		BOOL blRead=ogBCD.GetRecord("HAI", i,  olRecord);

		clDmy = olRecord.Values[ogBCD.GetFieldIndex("HAI","HSNA")];
		olGrdDel->SetValueRange(CGXRange(i+1,2),clDmy);

		//Fill detail columns
		FillAgentData(clDmy,i+1);

		clDmy = olRecord.Values[ogBCD.GetFieldIndex("HAI","URNO")];
		olGrdDel->SetValueRange(CGXRange(i+1,7),clDmy);

		clDmy = olRecord.Values[ogBCD.GetFieldIndex("HAI","TASK")];
		olGrdDel->SetValueRange(CGXRange(i+1,1),clDmy);
	
		clDmy = olRecord.Values[ogBCD.GetFieldIndex("HAI","REMA")];
		olGrdDel->SetValueRange(CGXRange(i+1,6),clDmy);

	}
	

	// SET COMBOBOX WITH HAG-CODES
	if (ilDataCount!=0)
	{
	olGrdDel->SetStyleRange(CGXRange(1,2,ilDataCount,2),CGXStyle().
		SetControl(GX_IDS_CTRL_COMBOBOX).
		SetChoiceList(olHagCombo));	

	olGrdDel->SetStyleRange(CGXRange(1,1,ilDataCount,1),CGXStyle().
		SetControl(GX_IDS_CTRL_COMBOBOX).
		SetChoiceList(omHtyCombo));	
	}



	return TRUE;  

}
//----------------------------------------------------------------------------------------
void AirlineDlg::FillAgentData(CString olCode, int iRow)
{
	int i;

	for(i = 0; i < olHagCodes.GetSize(); i++)
	{
		if (olHagCodes.GetAt(i)==olCode)
		{
			CString cl;
			cl =olHagNames.GetAt(i);
			olGrdDel->SetValueRange(CGXRange(iRow,3),olHagNames.GetAt(i));
			olGrdDel->SetValueRange(CGXRange(iRow,4),olHagPhones.GetAt(i));
			olGrdDel->SetValueRange(CGXRange(iRow,5),olHagFax.GetAt(i));

			break;
		}
	}
}
//----------------------------------------------------------------------------------------
void AirlineDlg::FillHandlingAgents()
{

	CString clWhere;
	int i;

	//clWhere.Format("HSNA+");
	ogBCD.SetObject("HAG");
	ogBCD.SetSort("HAG","HSNA+",true); 
	ogBCD.Read("HAG");	
	int ilDataCount=ogBCD.GetDataCount("HAG");	

	olHagCombo="";
	for(i = 0; i < ilDataCount; i++)
	{
		RecordSet olRecord;
		BOOL blRead=ogBCD.GetRecord("HAG", i,  olRecord);
		olHagCombo = olHagCombo + olRecord.Values[ogBCD.GetFieldIndex("HAG","HSNA")] + "\n";
		
		olHagCodes.Add(olRecord.Values[ogBCD.GetFieldIndex("HAG","HSNA")]);
		olGrdDel->olHagCodes.Add(olRecord.Values[ogBCD.GetFieldIndex("HAG","HSNA")]);

		olHagNames.Add(olRecord.Values[ogBCD.GetFieldIndex("HAG","HNAM")]);
		olGrdDel->olHagNames.Add(olRecord.Values[ogBCD.GetFieldIndex("HAG","HNAM")]);

		olHagPhones.Add(olRecord.Values[ogBCD.GetFieldIndex("HAG","TELE")]);
		olGrdDel->olHagPhones.Add(olRecord.Values[ogBCD.GetFieldIndex("HAG","TELE")]);
		
		olHagFax.Add(olRecord.Values[ogBCD.GetFieldIndex("HAG","FAXN")]);
		olGrdDel->olHagFax.Add(olRecord.Values[ogBCD.GetFieldIndex("HAG","FAXN")]);
	}

}

//----------------------------------------------------------------------------------------
void AirlineDlg::FillHandlingTypes()
{

	CString clWhere;
	int i;

	//clWhere.Format("HSNA+");
	ogBCD.SetObject("HTY");
	ogBCD.SetSort("HTY","HNAM+",true); 
	ogBCD.Read("HTY");	
	int ilDataCount=ogBCD.GetDataCount("HTY");	

	omHtyCombo="";
	for(i = 0; i < ilDataCount; i++)
	{
		RecordSet olRecord;
		BOOL blRead=ogBCD.GetRecord("HTY", i,  olRecord);
		omHtyCombo = omHtyCombo + olRecord.Values[ogBCD.GetFieldIndex("HTY","HNAM")] + "\n";
		
	}

}

//----------------------------------------------------------------------------------------

void AirlineDlg::OnOK() 
{
	AfxGetApp()->DoWaitCursor(1);
	CString olErrorText;
	bool ilStatus = true;
/*	if(m_RPRT.GetStatus() == false)
	{
		ilStatus = false;
		if(m_RPRT.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING288) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING288) + ogNotFormat;
		}
	}
	if(m_WRKO.GetStatus() == false)
	{
		ilStatus = false;
		if(m_WRKO.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING288) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING288) + ogNotFormat;
		}
	}
	if(m_ADMD.GetStatus() == false)
	{

		ilStatus = false;
		if(m_ADMD.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING288) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING288) + ogNotFormat;
		}
	}
	if(m_ALFN.GetStatus() == false)
	{
		ilStatus = false;
		if(m_ALFN.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING288) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING288) + ogNotFormat;
		}
	}
*/	if(m_ALC2.GetStatus() == false)
	{
		if(m_ALC2.GetWindowTextLength() != 0)
		{
			ilStatus = false;
			olErrorText += LoadStg(IDS_STRING289) + ogNotFormat;
		}
	}

	if(m_ALC3.GetStatus() == false)
	{
		if(m_ALC3.GetWindowTextLength() != 0)
		{
			ilStatus = false;
			olErrorText += LoadStg(IDS_STRING290) + ogNotFormat;
		}
	}

	//if(m_ALC3.GetWindowTextLength() == 0 && m_ALC2.GetWindowTextLength() == 0)
	if(m_ALC3.GetWindowTextLength() == 0)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING297);
	}
	if(m_AKEY.GetStatus() == false)
	{
		if(m_AKEY.GetWindowTextLength() == 0)
		{
			//olErrorText += LoadStg(IDS_STRING109) +  ogNoData;
			//ilStatus = false;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING109) + ogNotFormat;
			ilStatus = false;
		}
	}
	if(m_TERM.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING295) + ogNotFormat;
	}
	if(m_VAFRD.GetStatus() == false)
	{
		ilStatus = false;
		if(m_VAFRD.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING230) + LoadStg(IDS_STRING232) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING230) + LoadStg(IDS_STRING232) + ogNotFormat;
		}
	}
	if(m_VAFRT.GetStatus() == false)
	{
		ilStatus = false;
		if(m_VAFRT.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING230) + LoadStg(IDS_STRING233) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING230) + LoadStg(IDS_STRING233) + ogNotFormat;
		}
	}
	if(m_VATOD.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING231) + LoadStg(IDS_STRING232) + ogNotFormat;
	}
	if(m_VATOT.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING231) + LoadStg(IDS_STRING233) + ogNotFormat;
	}

	///////////////////////////////////////////////////////////////////
	CString olVafrd,olVafrt,olVatod,olVatot;
	CTime olTmpTimeFr,olTmpTimeTo;
	m_VAFRD.GetWindowText(olVafrd);
	m_VAFRT.GetWindowText(olVafrt);
	m_VATOD.GetWindowText(olVatod);
	m_VATOT.GetWindowText(olVatot);

	if((m_VAFRD.GetWindowTextLength() != 0 && m_VAFRT.GetWindowTextLength() == 0) || (m_VAFRD.GetWindowTextLength() == 0 && m_VAFRT.GetWindowTextLength() != 0))
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING234);
	}
	if((m_VATOD.GetWindowTextLength() != 0 && m_VATOT.GetWindowTextLength() == 0) || (m_VATOD.GetWindowTextLength() == 0 && m_VATOT.GetWindowTextLength() != 0))
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING235);
	}
	if(m_VAFRD.GetStatus() == true && m_VAFRT.GetStatus() == true && m_VATOD.GetStatus() == true && m_VATOT.GetStatus() == true)
	{
		olTmpTimeFr = DateHourStringToDate(olVafrd,olVafrt);
		olTmpTimeTo = DateHourStringToDate(olVatod,olVatot);
		if(olTmpTimeTo != -1 && (olTmpTimeTo < olTmpTimeFr || olTmpTimeFr == -1))
		{
			ilStatus = false;
			olErrorText += LoadStg(IDS_STRING236);
		}
	}
	//Konsistenz-Check//
	if(ilStatus == true)
	{
		CString olAkey;
		CTime olVafr,olVato;
		char clWhere[100];
		CCSPtrArray<ALTDATA> olAltCPA;

		// bch 13.12.2006 PRF 7657: ALC3 is now mandatory and unique, the rest is no longer checked
		CString olAlc3 = "";
		m_ALC3.GetWindowText(olAlc3);
		sprintf(clWhere,"WHERE ALC3='%s'", olAlc3);
		if(ogALTData.ReadSpecialData(&olAltCPA,clWhere,"URNO,ALC2,ALC3,VAFR,VATO",false) == true)
		{
			if(olAltCPA[0].Urno != pomALT->Urno)
			{
				ilStatus = false;
				// This record can't be saved. An Airline with the same 3-Letter-Code or with the same combination of 2-Letter-Code and 3-Letter-Code already exists
				olErrorText += LoadStg(IDS_STRING1025);
			}
		}
		olAltCPA.DeleteAll();

//		m_AKEY.GetWindowText(olAkey);
//
//		olVafr = DateHourStringToDate(olVafrd,olVafrt);
//		olVato = DateHourStringToDate(olVatod,olVatot);
//
//		sprintf(clWhere,"WHERE AKEY='%s'",olAkey);
//		if(ogALTData.ReadSpecialData(&olAltCPA,clWhere,"URNO,AKEY,VAFR,VATO",false) == true)
//		{
//			CString olTmpAkey;
//			int ilSize = olAltCPA.GetSize();
//			for(int i=0; i<ilSize; i++)
//			{
//				olTmpAkey = olAltCPA[i].Akey;
//				if(olAltCPA[i].Urno != pomALT->Urno && olTmpAkey == olAkey)
//				{
//					if(olVato != -1 && olAltCPA[i].Vato != -1)
//					{
//						if(olAltCPA[i].Vafr <= olVato && olAltCPA[i].Vato >= olVafr)
//						{
//							ilStatus = false;
//						}
//					}
//					else if(olVato == -1 && olAltCPA[i].Vato != -1)
//					{
//						if(olAltCPA[i].Vafr <= olVafr && olAltCPA[i].Vato >= olVafr)
//						{
//							ilStatus = false;
//						}
//
//					}
//					else if(olVato != -1 && olAltCPA[i].Vato == -1)
//					{
//						if(olVafr <= olAltCPA[i].Vafr && olVato >= olAltCPA[i].Vafr)
//						{
//							ilStatus = false;
//						}
//					}
//					else //(olVato == -1 && olAltCPA[i].Vato == -1)
//					{
//						if(olVafr == olAltCPA[i].Vafr)
//						{
//							ilStatus = false;
//						}
//					}
//				}
//			}
//			if(ilStatus == false)
//			{
//				olErrorText += LoadStg(IDS_STRING110);
//				i = ilSize;
//			}
//		}
//		olAltCPA.DeleteAll();
	}

	///////////////////////////////////////////////////////////////////
	AfxGetApp()->DoWaitCursor(-1);
	if (ilStatus == true)
	{
		CString olTmp;
		m_TERM.GetWindowText(olTmp);

#ifdef	_DEBUG
		afxDump << "olTmp.GetLength():" << olTmp.GetLength() <<"\n";
#endif
		if(olTmp.Right(1) == ",")
		{
			olTmp = olTmp.Left(olTmp.GetLength()-1);
		}
		sprintf(pomALT->Term,"%s",olTmp);

		m_ALC2.GetWindowText(pomALT->Alc2,3);
		m_ALC3.GetWindowText(pomALT->Alc3,4);
		m_ALFN.GetWindowText(pomALT->Alfn,33);
		m_AKEY.GetWindowText(pomALT->Akey,6);
		m_RPRT.GetWindowText(pomALT->Rprt,10);
		m_WRKO.GetWindowText(pomALT->Wrko,10);
		m_ADMD.GetWindowText(pomALT->Admd,10);
		m_ADD1.GetWindowText(pomALT->Add1,60);
		m_ADD2.GetWindowText(pomALT->Add2,60);
		m_ADD3.GetWindowText(pomALT->Add3,40);
		m_ADD4.GetWindowText(pomALT->Add4,40);
		m_BASE.GetWindowText(pomALT->Base,60);
		m_CONT.GetWindowText(pomALT->Cont,2);
		m_CTRY.GetWindowText(pomALT->Ctry,10);
		m_EMPS.GetWindowText(pomALT->Emps,5);
		m_EXEC.GetWindowText(pomALT->Exec,30);
		m_FOND.GetWindowText(pomALT->Fond,5);
		m_IANO.GetWindowText(pomALT->Iano,4);
		m_IATA.GetWindowText(pomALT->Iata,2);
		m_ICAL.GetWindowText(pomALT->Ical,12);
		m_ICAO.GetWindowText(pomALT->Icao,3);
		m_LCOD.GetWindowText(pomALT->Lcod,4);
		m_PHON.GetWindowText(pomALT->Phon,16);
		m_SITA.GetWindowText(pomALT->Sita,8);
		m_STXT.GetWindowText(pomALT->Text,20);
		m_TELX.GetWindowText(pomALT->Telx,16);
		m_TFAX.GetWindowText(pomALT->Tfax,16);
		m_WEBS.GetWindowText(pomALT->Webs,40);
		m_HOME.GetWindowText(pomALT->Home,4);
		m_DOIN.GetWindowText(pomALT->Doin,2);

		if (m_CASHK.GetCheck() == 1) pomALT->Cash[0] = 'K';
		else if (m_CASHB.GetCheck() == 1) pomALT->Cash[0] = 'B';
		else pomALT->Cash[0] = ' ';

		pomALT->Vafr = DateHourStringToDate(olVafrd,olVafrt);
		pomALT->Vato = DateHourStringToDate(olVatod,olVatot);


		//NOW SAVE THE HAG-DATA
		CString clDmy;
		bool result;

		long iRows=olGrdDel->GetRowCount();
		//ogBCD.SetObject("HAI");
		int i;

		for(i = 1; i <= iRows; i++)
		{
			CString clUrno=olGrdDel->GetValueRowCol(i,7);
			CString clAltu;
			clAltu.Format("%d",pomALT->Urno);
			CString clHsna=olGrdDel->GetValueRowCol(i,2);
			CString clTask=olGrdDel->GetValueRowCol(i,1);
			CString clRema=olGrdDel->GetValueRowCol(i,6);
			CString clStatus=olGrdDel->GetValueRowCol(i,8);

			if (clStatus=="NEW" || clStatus=="UPD" )
			{
				if (clStatus=="UPD" )
				{
					clDmy.Format("WHERE URNO=%s",clUrno);
					ogBCD.DeleteByWhere("HAI",clDmy);
				}
				RecordSet olRecord(ogBCD.GetFieldCount("HAI"));

				olRecord.Values[ogBCD.GetFieldIndex("HAI","URNO")]=clUrno;
				olRecord.Values[ogBCD.GetFieldIndex("HAI","ALTU")]=clAltu;
				olRecord.Values[ogBCD.GetFieldIndex("HAI","HSNA")]=clHsna;
				olRecord.Values[ogBCD.GetFieldIndex("HAI","REMA")]=clRema;
				olRecord.Values[ogBCD.GetFieldIndex("HAI","TASK")]=clTask;
				olRecord.Values[ogBCD.GetFieldIndex("HAI","FLNU")]="0";

				result=ogBCD.InsertRecord("HAI",olRecord,TRUE);
			}
		}
		result=ogBCD.Save("HAI");

		//*** DELETE THE SELECTED ***
		for (i=0; i<olDeleteUrno.GetSize(); i++)
		{
			clDmy.Format("WHERE URNO=%s",olDeleteUrno[i]);
			ogBCD.DeleteByWhere("HAI",clDmy);
		}

		CDialog::OnOK();
	}
	else
	{
		Beep(440,70);
		MessageBox(olErrorText,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
		m_ALFN.SetFocus();
		m_ALFN.SetSel(0,-1);
	}
}

//----------------------------------------------------------------------------------------

LONG AirlineDlg::OnKillfocus(UINT lParam, LONG wParam) 
{
//	if(lParam == (UINT)m_ALC2.imID && m_ALC2.GetWindowTextLength() > 0)
//	{
//		m_ALC3.SetBKColor(WHITE);
//	}
//	if(lParam == (UINT)m_ALC2.imID && m_ALC2.GetWindowTextLength() == 0)
//	{
//		m_ALC3.SetBKColor(YELLOW);
//	}
//	if(lParam == (UINT)m_ALC3.imID && m_ALC3.GetWindowTextLength() > 0)
//	{
//		m_ALC2.SetBKColor(WHITE);
//	}
//	if(lParam == (UINT)m_ALC3.imID && m_ALC3.GetWindowTextLength() == 0)
//	{
//		m_ALC2.SetBKColor(YELLOW);
//	}
	if(lParam == (UINT)m_ALC3.imID && m_AKEY.GetWindowTextLength() == 0)
	{
		CString olTmp;
		m_ALC3.GetWindowText(olTmp);
		m_AKEY.SetInitText(olTmp);
	}
	return 0L;
}

//----------------------------------------------------------------------------------------


void AirlineDlg::OnAdd() 
{
	long iRows=olGrdDel->GetRowCount()+1;
	olGrdDel->InsertRows(iRows,1);

	
	olGrdDel->SetValueRange(CGXRange(iRows,7),ogBasicData.GetNextUrno());
	olGrdDel->SetValueRange(CGXRange(iRows,8),"NEW");

	olGrdDel->SetStyleRange(CGXRange(iRows,2),CGXStyle().
		SetControl(GX_IDS_CTRL_COMBOBOX).
		SetChoiceList(olHagCombo));	
	olGrdDel->SetStyleRange(CGXRange(iRows,1),CGXStyle().
		SetControl(GX_IDS_CTRL_COMBOBOX).
		SetChoiceList(omHtyCombo));	
	

}

void AirlineDlg::OnDelete() 
{


	
	//*** DELETE FROM SCREEN, DB DELETE ONOK() ***
	ROWCOL iCol,iRow;
	olGrdDel->GetCurrentCell(iRow,iCol);

	if (iRow>0)
	{
#ifdef	_DEBUG
		afxDump << "iRow:" << iRow <<"\n";
#endif
		CString clUrno=olGrdDel->GetValueRowCol(iRow,7);

		olDeleteUrno.Add(clUrno);

		olGrdDel->RemoveRows(iRow,iRow);

		olGrdDel->SetCurrentCell(0,0);
	}
	
}
