#if !defined(AFX_STANDARDVERKETTUNGENDLG_H__32F75441_55BF_11D1_B3C2_0000C016B067__INCLUDED_)
#define AFX_STANDARDVERKETTUNGENDLG_H__32F75441_55BF_11D1_B3C2_0000C016B067__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// StandardverkettungenDlg.h : header file
//
#include "CedaStrData.h"
#include "CCSEdit.h"

/////////////////////////////////////////////////////////////////////////////
// StandardverkettungenDlg dialog

class StandardverkettungenDlg : public CDialog
{
// Construction
public:
	StandardverkettungenDlg(STRDATA *popStr,CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(StandardverkettungenDlg)
	enum { IDD = IDD_STANDARDVERKETTUNGENDLG };
	CButton	m_OK;
	CString	m_Caption;
	CCSEdit	m_CDATD;
	CCSEdit	m_CDATT;
	CCSEdit	m_LSTUD;
	CCSEdit	m_LSTUT;
	CCSEdit	m_USEC;
	CCSEdit	m_USEU;
	CCSEdit	m_VAFRD;
	CCSEdit	m_VAFRT;
	CCSEdit	m_VATOD;
	CCSEdit	m_VATOT;
	CButton	m_DAYA1;
	CButton	m_DAYA2;
	CButton	m_DAYA3;
	CButton	m_DAYA4;
	CButton	m_DAYA5;
	CButton	m_DAYA6;
	CButton	m_DAYA7;
	CButton	m_DAYAT;
	CButton	m_DAYD1;
	CButton	m_DAYD2;
	CButton	m_DAYD3;
	CButton	m_DAYD4;
	CButton	m_DAYD5;
	CButton	m_DAYD6;
	CButton	m_DAYD7;
	CButton	m_DAYDT;
	CCSEdit	m_ACT3;
	CCSEdit	m_ACTM;
	CCSEdit	m_FLCA;
	CCSEdit	m_FLNA;
	CCSEdit	m_FLCD;
	CCSEdit	m_FLSA;
	CCSEdit	m_FLND;
	CCSEdit	m_FLSD;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(StandardverkettungenDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(StandardverkettungenDlg)
	virtual void OnOK();
	virtual BOOL OnInitDialog();
	afx_msg void OnDaya17();
	afx_msg void OnDayd17();
	afx_msg void OnDayaT();
	afx_msg void OnDaydT();
    afx_msg LONG OnKillfocus(UINT wParam, LONG lParam);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

private:

	STRDATA *pomStr;

};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_STANDARDVERKETTUNGENDLG_H__32F75441_55BF_11D1_B3C2_0000C016B067__INCLUDED_)
