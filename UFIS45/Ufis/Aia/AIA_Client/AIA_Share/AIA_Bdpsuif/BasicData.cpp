// basicdat.cpp CBasicData class for providing general used methods

#include "stdafx.h"
#include "BasicData.h"
#include "CedaBasicData.h"
#include "resource.h"
#include "Bdpsuif.h"

#define N_URNOS_AT_ONCE 50
 

static int CompareArray( const CString **e1, const CString **e2);

//------------------------------------------------------------------------------------

CString LoadStg(UINT nID)
{
	CString olString = "";
	olString.LoadString(nID);
	int i = olString.Find("*REM*");
	if(i>-1)
		olString = olString.Left(i);
	return olString;
}

//------------------------------------------------------------------------------------

int GetItemCount(CString olList, char cpTrenner  )
{
	CStringArray olStrArray;
	return ExtractItemList(olList,&olStrArray,cpTrenner);

}

CString GetListItem(CString &opList, int ipPos, bool bpCut, char cpTrenner )
{
	CStringArray olStrArray;
	int ilAnz = ExtractItemList(opList, &olStrArray, cpTrenner);
	CString olReturn;

	if(ipPos == -1)
		ipPos = ilAnz;

	if((ipPos <= ilAnz) && (ipPos > 0))
		olReturn = olStrArray[ipPos - 1];
	if(bpCut)
	{
		opList = "";
		for(int ilLc = 0; ilLc < ilAnz; ilLc++)
		{
			if(ilLc != (ipPos - 1))
			{
				opList = opList + olStrArray[ilLc] + cpTrenner;
			}
		}
	}
	if(bpCut)
		opList = opList.Left(opList.GetLength() - 1);
	return olReturn;
}



CString DeleteListItem(CString &opList, CString olItem)
{
	CStringArray olStrArray;
	int ilAnz = ExtractItemList(opList, &olStrArray);
	opList = "";
	for(int ilLc = 0; ilLc < ilAnz; ilLc++)
	{
			if(olStrArray[ilLc] != olItem)
				opList = opList + olStrArray[ilLc] + ",";
	}
	opList = opList.Left(opList.GetLength() - 1);
	return opList;
}

CString SortItemList(CString opSubString, char cpTrenner)
{
	CString *polText;
	CCSPtrArray<CString> olArray;

	BOOL blEnd = FALSE;
	
	if(!opSubString.IsEmpty())
	{
		int pos;
		int olPos = 0;
		while(blEnd == FALSE)
		{
			polText = new CString;
			pos = opSubString.Find(cpTrenner);
			if(pos == -1)
			{
				blEnd = TRUE;
				*polText = opSubString;
			}
			else
			{
				*polText = opSubString.Mid(0, opSubString.Find(cpTrenner));
				opSubString = opSubString.Mid(opSubString.Find(cpTrenner)+1, opSubString.GetLength( )-opSubString.Find(cpTrenner)+1);
			}
			olArray.Add(polText);
		}
	}

	CString olSortedString;
	olArray.Sort(CompareArray);
	for(int i=0; i<olArray.GetSize(); i++)
	{
		olSortedString += olArray[i] + cpTrenner;
	}
	
	olArray.DeleteAll();
	return olSortedString.Left(olSortedString.GetLength()-1);
}

CBasicData::CBasicData(void)
{
	char pclTmpText[512];
	char pclConfigPath[512];
    if (getenv("CEDA") == NULL)
        strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
    else
        strcpy(pclConfigPath, getenv("CEDA"));

	imNextOrder = 4711;
	*pcmCedaCmd = '\0';


	omDiaStartTime = CTime::GetCurrentTime();
	omDiaStartTime -= CTimeSpan(0, 1, 0, 0);
	omDiaEndTime = omDiaStartTime + CTimeSpan(0, 6, 0, 0);
	

	// now reading the ceda commands
	char pclComandBuf[24];
	omDefaultComands.Empty();
	omActualComands.Empty();
 
    GetPrivateProfileString(pcgAppName, "EIOHDL", "LLF",
		pclTmpText, sizeof pclTmpText, pclConfigPath);
	sprintf(pclComandBuf," %3s ","LLF");
	omDefaultComands += pclComandBuf;
	sprintf(pclComandBuf," %3s ",pclTmpText);
	omActualComands += pclComandBuf;
}



CBasicData::~CBasicData(void)
{
}


long CBasicData::GetNextUrno(void)
{
	bool	olRc = true;
	long				llNextUrno = 0L;
	

	if (omUrnos.GetSize() == 0)
	{
		olRc = GetNurnos(N_URNOS_AT_ONCE);
	}

	if (omUrnos.GetSize() > 0)
	{
		llNextUrno = omUrnos[0];
		omUrnos.RemoveAt(0);

		if ( (llNextUrno != 0L) && (olRc == true) )
		{
			return(llNextUrno);
		}
	}
	::MessageBox(NULL,LoadStg(IDS_STRING444),LoadStg(IDS_STRING445),MB_OK);
	return -1;	
}


bool CBasicData::GetNurnos(int ipNrOfUrnos)
{
	bool	ilRc = false;
	char 	pclTmpDataBuf[12*N_URNOS_AT_ONCE];

	sprintf(pclTmpDataBuf, "%d", ipNrOfUrnos);

	ilRc = CedaAction("GMU", "", "", pclTmpDataBuf);

	if (ilRc == true)
	{
		for ( int ilItemNo=1; (ilItemNo <= N_URNOS_AT_ONCE) ; ilItemNo++ )
		{
			char pclTmpBuf[64];

			GetItem(ilItemNo, pclTmpDataBuf, pclTmpBuf);

			long llNewUrno = atol(pclTmpBuf);

			omUrnos.Add(llNewUrno);
		}
	}

	return ilRc;
}


int CBasicData::GetNextOrderNo()
{
	imNextOrder++;

	return (imNextOrder);
}


char *CBasicData::GetCedaCommand(CString opCmdType)
{ 

	int ilIndex;

	if ((ilIndex = omDefaultComands.Find(opCmdType)) != -1)
	{
		strcpy(pcmCedaComand,omActualComands.Mid(ilIndex,3));
	}
	else
	{
		strcpy(pcmCedaComand,opCmdType);
	}
	return pcmCedaComand;
}


void CBasicData::GetDiagramStartTime(CTime &opStart, CTime &opEnd)
{
	opStart = omDiaStartTime;
	opEnd = omDiaEndTime;
	return;
}


void CBasicData::SetDiagramStartTime(CTime opDiagramStartTime, CTime opDiagramEndTime)
{
	omDiaStartTime = opDiagramStartTime;
	omDiaEndTime = opDiagramEndTime;
}


void CBasicData::SetWorkstationName(CString opWsName)
{
	omWorkstationName = CString("WKS234");//opWsName;
}


CString CBasicData::GetWorkstationName()
{
	return omWorkstationName;
}


bool CBasicData::GetWindowPosition(CRect& rlPos,CString olMonitor)
{

	int XResolution = 1024;
	int YResolution = 768;
	if (ogCfgData.rmUserSetup.RESO[0] == '8')
	{
		XResolution = 800;
		YResolution = 600;
	}
	else
	{
		if (ogCfgData.rmUserSetup.RESO[0] == '1')
		{
			if (ogCfgData.rmUserSetup.RESO[1] == '0')
			{
				XResolution = 1024;
				YResolution = 768;
			}
		}
		else
		{
			XResolution = 1280;
			YResolution = 1024;
		}
	}
  

	int ilMonitor;
	if (olMonitor[0] == 'L')
		ilMonitor = 0;
	if (olMonitor[0] == 'M')
		ilMonitor = 1;
	if (olMonitor[0] == 'R')
		ilMonitor = 2;
	
	rlPos.top = ilMonitor == 0 ? 56 : 0;
	rlPos.bottom = YResolution;
	rlPos.left = XResolution * ilMonitor;
	rlPos.right = XResolution * (ilMonitor+1);

	return true;
}

static int CompareArray( const CString **e1, const CString **e2)
{
	return (strcmp((**e1),(**e2)));
}

BOOL CBasicData::LoadBitmapFromBMPFile( LPTSTR szFileName, HBITMAP *phBitmap,HPALETTE *phPalette )
{
     BITMAP  bm;

     *phBitmap = NULL;
     *phPalette = NULL;

     // Use LoadImage() to get the image loaded into a DIBSection
     *phBitmap = (HBITMAP)LoadImage( NULL, szFileName, IMAGE_BITMAP, 0, 0,
                 LR_CREATEDIBSECTION | LR_LOADFROMFILE );
     if( *phBitmap == NULL )
       return FALSE;

     // Get the color depth of the DIBSection
     GetObject(*phBitmap, sizeof(BITMAP), &bm );
     // If the DIBSection is 256 color or less, it has a color table
     if( ( bm.bmBitsPixel * bm.bmPlanes ) <= 8 )
     {
       HDC           hMemDC;
       HBITMAP       hOldBitmap;
       RGBQUAD       rgb[256];
       LPLOGPALETTE  pLogPal;
       WORD          i;

       // Create a memory DC and select the DIBSection into it
       hMemDC = CreateCompatibleDC( NULL );
       hOldBitmap = (HBITMAP)SelectObject( hMemDC, *phBitmap );
       // Get the DIBSection's color table
       GetDIBColorTable( hMemDC, 0, 256, rgb );
       // Create a palette from the color table
       pLogPal = (LOGPALETTE*)malloc( sizeof(LOGPALETTE) + (256*sizeof(PALETTEENTRY)) );
       pLogPal->palVersion = 0x300;
       pLogPal->palNumEntries = 256;
       for(i=0;i<256;i++)
       {
         pLogPal->palPalEntry[i].peRed = rgb[i].rgbRed;
         pLogPal->palPalEntry[i].peGreen = rgb[i].rgbGreen;
         pLogPal->palPalEntry[i].peBlue = rgb[i].rgbBlue;
         pLogPal->palPalEntry[i].peFlags = 0;
       }
       *phPalette = CreatePalette( pLogPal );
       // Clean up
       free( pLogPal );
       SelectObject( hMemDC, hOldBitmap );
       DeleteDC( hMemDC );
     }
     else   // It has no color table, so use a halftone palette
     {
       HDC    hRefDC;

       hRefDC = GetDC( NULL );
       *phPalette = CreateHalftonePalette( hRefDC );
       ReleaseDC( NULL, hRefDC );
     }
     return TRUE;
}

bool CBasicData::AddBlockingImages(CImageList& ropImageList)
{
	if (!ropImageList.Create(16,16,ILC_COLOR4,10,10))
		return false;



	char pclConfigPath[256];

    if (getenv("CEDA") == NULL)
        strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
    else
        strcpy(pclConfigPath, getenv("CEDA"));

	char pclBitmapCount[24];
    GetPrivateProfileString("GATPOS", "BlockedBitmapCount", "0", pclBitmapCount, sizeof pclBitmapCount, pclConfigPath);
	int ilBitmapCount = atoi(pclBitmapCount);
	for (int i = 0; i < ilBitmapCount; i++)
	{
		char pclBlockedBitmap[256];
		char pclBitmap[256];
		sprintf(pclBitmap,"BlockedBitmap%d",i+1);
	    GetPrivateProfileString("GATPOS",pclBitmap, "", pclBlockedBitmap, sizeof pclBlockedBitmap, pclConfigPath);

		HBITMAP hBitmap;
		HPALETTE hPalette;

		if (!this->LoadBitmapFromBMPFile(pclBlockedBitmap,&hBitmap,&hPalette))
			return false;

		CBitmap* polBitmap1 = new CBitmap;
		polBitmap1->Attach(hBitmap);

		if (ropImageList.Add(polBitmap1,(CBitmap *)NULL) < 0)
		{
			delete polBitmap1;
			return false;
		}

		polBitmap1->Detach();
		delete polBitmap1;
	}

	return true;
}

bool CBasicData::AddBlockingImages(AatBitmapComboBox& ropComboBox)
{
	char pclConfigPath[256];

    if (getenv("CEDA") == NULL)
        strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
    else
        strcpy(pclConfigPath, getenv("CEDA"));

	char pclBitmapCount[24];
    GetPrivateProfileString("GATPOS", "BlockedBitmapCount", "0", pclBitmapCount, sizeof pclBitmapCount, pclConfigPath);
	int ilBitmapCount = atoi(pclBitmapCount);
	for (int i = 0; i < ilBitmapCount; i++)
	{
		char pclBlockedBitmap[256];
		char pclBitmap[256];
		sprintf(pclBitmap,"BlockedBitmap%d",i+1);
	    GetPrivateProfileString("GATPOS",pclBitmap, "", pclBlockedBitmap, sizeof pclBlockedBitmap, pclConfigPath);

		if (ropComboBox.AddBitmap(pclBlockedBitmap) == CB_ERR)
			return false;
	}

	return true;
}

bool CBasicData::GetCmdLineResource(CString& ropTable,long& rlpUrno)
{
	if (ogCmdLineStghArray.GetSize() < 5)
		return false;
	char pclTable[128];
	if (sscanf(ogCmdLineStghArray.GetAt(4),"Resource:%3s:%ld",&pclTable[0],&rlpUrno) != 2)
		return false;
	else
	{
		ropTable = pclTable;
		return true;
	}
}
