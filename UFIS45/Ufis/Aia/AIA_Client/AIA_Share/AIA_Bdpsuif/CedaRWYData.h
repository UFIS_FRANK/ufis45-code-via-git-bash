// CedaRWYData.h

#ifndef __CEDARWYDATA__
#define __CEDARWYDATA__
 
#include "stdafx.h"
#include "basicdata.h"

//---------------------------------------------------------------------------------------------------------
// Record structure declaration

struct RWYDATA 
{
	long 	 Urno; 		// Eindeutige Datensatz-Nr.
	CTime	 Cdat;		// Erstellungsdatum
	char 	 Usec[34]; 	// Anwender (Ersteller)
	CTime	 Lstu;	 	// Datum letzte �nderung
	char 	 Prfl[3]; 	// Protokollierungskennzeichen
	char 	 Useu[34]; 	// Anwender (letzte �nderung)
	char 	 Rnam[6]; 	// Start- und Landebahn Bezeichnung
	char 	 Rnum[3]; 	// Start- und Landebahn Aliasname
	char 	 Rtyp[3]; 	// Start- und/oder Landebahn
	CTime	 Nafr; 		// Nicht verf�gbar vom
	CTime	 Nato; 		// Nicht verf�gbar bis
	char 	 Resn[42]; 	// Grund f�r die Sperrung
	CTime	 Vafr;	 	// G�ltig von
	CTime	 Vato;	 	// G�ltig bis
	char 	 Home[4]; 	// Grund f�r die Sperrung

	//DataCreated by this class
	int      IsChanged;

	RWYDATA(void)
	{
		memset(this,'\0',sizeof(*this));
		Cdat=-1;
		Lstu=-1;
		Nafr=-1;
		Nato=-1;
		Vafr=-1;
		Vato=-1;
	}

}; // end RWYDataStrukt


//---------------------------------------------------------------------------------------------------------
// Class declaratino


class CedaRWYData: public CCSCedaData
{
// Attributes
public:
    CMapPtrToPtr omUrnoMap;

    CCSPtrArray<RWYDATA> omData;

	char pcmRWYFieldList[2048];

// Operations
public:
    CedaRWYData();
	~CedaRWYData();
	virtual void GetDataInfo(CStringArray &ropFields, CStringArray &ropDesription, CStringArray &ropType);
	void Register(void);
	
	void ClearAll(void);

    bool Read(char *pcpWhere = NULL);
	bool ReadSpecialData(CCSPtrArray<RWYDATA> *popRwy,char *pspWhere,char *pspFieldList,bool ipSYS=true);
	bool InsertRWY(RWYDATA *prpRWY,BOOL bpSendDdx = TRUE);
	bool InsertRWYInternal(RWYDATA *prpRWY);
	bool UpdateRWY(RWYDATA *prpRWY,BOOL bpSendDdx = TRUE);
	bool UpdateRWYInternal(RWYDATA *prpRWY);
	bool DeleteRWY(long lpUrno);
	bool DeleteRWYInternal(RWYDATA *prpRWY);
	RWYDATA  *GetRWYByUrno(long lpUrno);
	bool SaveRWY(RWYDATA *prpRWY);
	void ProcessRWYBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

	// Private methods
private:
    void PrepareRWYData(RWYDATA *prpRWYData);
};

//---------------------------------------------------------------------------------------------------------

#endif //__CEDARWYDATA__
