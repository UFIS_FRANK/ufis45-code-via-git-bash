// FlugplansaisonDlg.cpp : implementation file
//

#include "stdafx.h"
#include "BDPSUIF.h"
#include "FlugplansaisonDlg.h"
#include "PrivList.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// FlugplansaisonDlg dialog


FlugplansaisonDlg::FlugplansaisonDlg(SEADATA *popSea,CWnd* pParent) : CDialog(FlugplansaisonDlg::IDD, pParent)
{
	pomSea = popSea;

	//{{AFX_DATA_INIT(FlugplansaisonDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void FlugplansaisonDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(FlugplansaisonDlg)
	DDX_Control(pDX, IDOK,		 m_OK);
	DDX_Control(pDX, IDC_CDAT_D, m_CDATD);
	DDX_Control(pDX, IDC_CDAT_T, m_CDATT);
	DDX_Control(pDX, IDC_LSTU_D, m_LSTUD);
	DDX_Control(pDX, IDC_LSTU_T, m_LSTUT);
	DDX_Control(pDX, IDC_USEC,	 m_USEC);
	DDX_Control(pDX, IDC_USEU,	 m_USEU);
	DDX_Control(pDX, IDC_SEAS,	 m_SEAS);
	DDX_Control(pDX, IDC_VPFR_D, m_VPFRD);
	DDX_Control(pDX, IDC_VPFR_T, m_VPFRT);
	DDX_Control(pDX, IDC_VPTO_D, m_VPTOD);
	DDX_Control(pDX, IDC_VPTO_T, m_VPTOT);
	DDX_Control(pDX, IDC_BEME,	 m_BEME);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(FlugplansaisonDlg, CDialog)
	//{{AFX_MSG_MAP(FlugplansaisonDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// FlugplansaisonDlg message handlers
//----------------------------------------------------------------------------------------

BOOL FlugplansaisonDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	m_Caption = LoadStg(IDS_STRING175) + " <" + m_Caption + ">";
	SetWindowText(m_Caption);
	
	char clStat = ogPrivList.GetStat("FLUGPLANSAISONDLG.m_OK");
	SetWndStatAll(clStat,m_OK);

	//------------------------------------
	m_CDATD.SetBKColor(SILVER);
	m_CDATD.SetInitText(pomSea->Cdat.Format("%d.%m.%Y"));
	m_CDATD.SetSecState(ogPrivList.GetStat("FLUGPLANSAISONDLG.m_CDAT"));
	// - - - - - - - - - - - - - - - - - -
	m_CDATT.SetBKColor(SILVER);
	m_CDATT.SetInitText(pomSea->Cdat.Format("%H:%M"));
	m_CDATT.SetSecState(ogPrivList.GetStat("FLUGPLANSAISONDLG.m_CDAT"));
	//------------------------------------
	m_LSTUD.SetBKColor(SILVER);
	m_LSTUD.SetInitText(pomSea->Lstu.Format("%d.%m.%Y"));
	m_LSTUD.SetSecState(ogPrivList.GetStat("FLUGPLANSAISONDLG.m_LSTU"));
	// - - - - - - - - - - - - - - - - - -
	m_LSTUT.SetBKColor(SILVER);
	m_LSTUT.SetInitText(pomSea->Lstu.Format("%H:%M"));
	m_LSTUT.SetSecState(ogPrivList.GetStat("FLUGPLANSAISONDLG.m_LSTU"));
	//------------------------------------
	m_USEC.SetBKColor(SILVER);
	m_USEC.SetInitText(pomSea->Usec);
	m_USEC.SetSecState(ogPrivList.GetStat("FLUGPLANSAISONDLG.m_USEC"));
	//------------------------------------
	m_USEU.SetBKColor(SILVER);
	m_USEU.SetInitText(pomSea->Useu);
	m_USEU.SetSecState(ogPrivList.GetStat("FLUGPLANSAISONDLG.m_USEU"));
	//------------------------------------//WHITE,YELLOW,SILVER,RED,LYELLOW
	m_SEAS.SetFormat("x|#x|#x|#x|#x|#x|#");
	m_SEAS.SetTextLimit(1,6);
	m_SEAS.SetBKColor(YELLOW);  
	m_SEAS.SetTextErrColor(RED);
	m_SEAS.SetInitText(pomSea->Seas);
	m_SEAS.SetSecState(ogPrivList.GetStat("FLUGPLANSAISONDLG.m_SEAS"));
	//------------------------------------
	m_BEME.SetTypeToString("X(20)",20,0);
	m_BEME.SetTextErrColor(RED);
	m_BEME.SetInitText(pomSea->Beme);
	m_BEME.SetSecState(ogPrivList.GetStat("FLUGPLANSAISONDLG.m_BEME"));
	//------------------------------------
	m_VPFRD.SetTypeToDate(true);
	m_VPFRD.SetTextErrColor(RED);
	m_VPFRD.SetBKColor(YELLOW);
	m_VPFRD.SetInitText(pomSea->Vpfr.Format("%d.%m.%Y"));
	m_VPFRD.SetSecState(ogPrivList.GetStat("FLUGPLANSAISONDLG.m_VPFR"));
	// - - - - - - - - - - - - - - - - - -
	m_VPFRT.SetTypeToTime(true);
	m_VPFRT.SetTextErrColor(RED);
	m_VPFRT.SetBKColor(YELLOW);
	m_VPFRT.SetInitText(pomSea->Vpfr.Format("%H:%M"));
	m_VPFRT.SetSecState(ogPrivList.GetStat("FLUGPLANSAISONDLG.m_VPFR"));
	//------------------------------------
	m_VPTOD.SetTypeToDate(true);
	m_VPTOD.SetTextErrColor(RED);
	m_VPTOD.SetBKColor(YELLOW);
	m_VPTOD.SetInitText(pomSea->Vpto.Format("%d.%m.%Y"));
	m_VPTOD.SetSecState(ogPrivList.GetStat("FLUGPLANSAISONDLG.m_VPTO"));
	// - - - - - - - - - - - - - - - - - -
	m_VPTOT.SetTypeToTime(true);
	m_VPTOT.SetTextErrColor(RED);
	m_VPTOT.SetBKColor(YELLOW);
	m_VPTOT.SetInitText(pomSea->Vpto.Format("%H:%M"));
	m_VPTOT.SetSecState(ogPrivList.GetStat("FLUGPLANSAISONDLG.m_VPTO"));
	//------------------------------------

	return TRUE;  
}

//----------------------------------------------------------------------------------------

void FlugplansaisonDlg::OnOK() 
{
	AfxGetApp()->DoWaitCursor(1);
	CString olErrorText;
	bool ilStatus = true;
	if(m_SEAS.GetStatus() == false)
	{
		ilStatus = false;
		if(m_SEAS.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING40) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING40) + ogNotFormat;
		}
	}
	if(m_BEME.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING238) + ogNotFormat;
	}
	if(m_VPFRD.GetStatus() == false)
	{
		ilStatus = false;
		if(m_VPFRD.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING230) + LoadStg(IDS_STRING232) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING230) + LoadStg(IDS_STRING232) + ogNotFormat;
		}
	}
	if(m_VPFRT.GetStatus() == false)
	{
		ilStatus = false;
		if(m_VPFRT.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING230) + LoadStg(IDS_STRING233) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING230) + LoadStg(IDS_STRING233) + ogNotFormat;
		}
	}
	if(m_VPTOD.GetStatus() == false)
	{
		ilStatus = false;
		if(m_VPTOD.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING231) + LoadStg(IDS_STRING232) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING231) + LoadStg(IDS_STRING232) + ogNotFormat;
		}
	}
	if(m_VPTOT.GetStatus() == false)
	{
		ilStatus = false;
		if(m_VPTOT.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING231) + LoadStg(IDS_STRING233) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING231) + LoadStg(IDS_STRING233) + ogNotFormat;
		}
	}

	///////////////////////////////////////////////////////////////////
	CString olVpfrd,olVpfrt,olVptod,olVptot;
	CTime olTmpTimeFr,olTmpTimeTo;
	m_VPFRD.GetWindowText(olVpfrd);
	m_VPFRT.GetWindowText(olVpfrt);
	m_VPTOD.GetWindowText(olVptod);
	m_VPTOT.GetWindowText(olVptot);

	if(m_VPFRD.GetStatus() == true && m_VPFRT.GetStatus() == true && m_VPTOD.GetStatus() == true && m_VPTOT.GetStatus() == true)
	{
		olTmpTimeFr = DateHourStringToDate(olVpfrd,olVpfrt);
		olTmpTimeTo = DateHourStringToDate(olVptod,olVptot);
		if(olTmpTimeTo != -1 && (olTmpTimeTo < olTmpTimeFr || olTmpTimeFr == -1))
		{
			ilStatus = false;
			olErrorText += LoadStg(IDS_STRING236);
		}
	}

	//Konsistenz-Check//
	if(ilStatus == true)
	{
		CString olSeas;
		char clWhere[100];
		CCSPtrArray<SEADATA> olSeaCPA;

		m_SEAS.GetWindowText(olSeas);
		sprintf(clWhere,"WHERE SEAS='%s'",olSeas);
		if(ogSeaData.ReadSpecialData(&olSeaCPA,clWhere,"URNO,SEAS",false) == true)
		{
			if(olSeaCPA[0].Urno != pomSea->Urno)
			{
				ilStatus = false;
				olErrorText += LoadStg(IDS_STRING40) + LoadStg(IDS_EXIST);
			}
		}
		olSeaCPA.DeleteAll();
	}

	///////////////////////////////////////////////////////////////////
	AfxGetApp()->DoWaitCursor(-1);
	if (ilStatus == true)
	{
		m_SEAS.GetWindowText(pomSea->Seas,7);
		m_BEME.GetWindowText(pomSea->Beme,21);
		pomSea->Vpfr = DateHourStringToDate(olVpfrd,olVpfrt);
		pomSea->Vpto = DateHourStringToDate(olVptod,olVptot);

		CDialog::OnOK();
	}
	else
	{
		Beep(440,70);
		MessageBox(olErrorText,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
		m_SEAS.SetFocus();
		m_SEAS.SetSel(0,-1);
  }

}

//----------------------------------------------------------------------------------------


