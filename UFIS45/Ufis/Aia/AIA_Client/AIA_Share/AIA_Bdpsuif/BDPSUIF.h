// BDPSUIF.h : main header file for the BDPSUIF application
//

#if !defined(AFX_BDPSUIF_H__C988B314_1F85_11D1_82CD_0080AD1DC701__INCLUDED_)
#define AFX_BDPSUIF_H__C988B314_1F85_11D1_82CD_0080AD1DC701__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols
#include "resrc1.h"

/////////////////////////////////////////////////////////////////////////////
// CBDPSUIFApp:
// See BDPSUIF.cpp for the implementation of this class
//

class CBDPSUIFApp : public CWinApp
{
public:
	CBDPSUIFApp();
	~CBDPSUIFApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CBDPSUIFApp)
	public:
	virtual BOOL InitInstance();
	bool OnSpecialHelp();
	//}}AFX_VIRTUAL

// Implementation

	//{{AFX_MSG(CBDPSUIFApp)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

extern char cgUserName[34];
extern CStringArray  ogCmdLineStghArray;

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_BDPSUIF_H__C988B314_1F85_11D1_82CD_0080AD1DC701__INCLUDED_)
