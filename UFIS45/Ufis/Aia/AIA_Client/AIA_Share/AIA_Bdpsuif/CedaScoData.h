// CedaScoData.h

#ifndef __CEDAPSCODATA__
#define __CEDAPSCODATA__
 
#include "stdafx.h"
#include "basicdata.h"
#include <afxdisp.h>

//---------------------------------------------------------------------------------------------------------

struct SCODATA 
{
	long Urno;			// Eindeutige Datensatz-Nr.
	long Surn;			// Referenz auf STF.URNO
	char Code[7];		// Codereferenz ORG.CODE
	COleDateTime Vpfr;	// G�ltig von
	COleDateTime Vpto;	// G�ltig bis

	//DataCreated by this class
	int      IsChanged;

	//long, CTime
	SCODATA(void)
	{ memset(this,'\0',sizeof(*this));
	  //Vpfr=-1,Vpto=-1; //<zB.(FIELD_DATE Felder)
	}

}; // end SCODataStruct

//---------------------------------------------------------------------------------------------------------
// Class declaratino

class CedaScoData: public CCSCedaData
{
// Attributes
public:
    CMapPtrToPtr omUrnoMap;

    CCSPtrArray<SCODATA> omData;

	char pcmListOfFields[2048];

// OScoations
public:
    CedaScoData();
	~CedaScoData();
	virtual void GetDataInfo(CStringArray &ropFields, CStringArray &ropDesription, CStringArray &ropType);
	void Register(void);
	void ClearAll(bool bpWithRegistration = true);
    bool Read(char *pspWhere = NULL);
	bool Insert(SCODATA *prpSco);
	bool InsertInternal(SCODATA *prpSco);
	bool Update(SCODATA *prpSco);
	bool UpdateInternal(SCODATA *prpSco);
	bool Delete(long lpUrno);
	bool DeleteInternal(SCODATA *prpSco);
	bool ReadScocialData(CCSPtrArray<SCODATA> *popSco,char *pspWhere,char *pspFieldList,bool ipSYS=true);
	bool Save(SCODATA *prpSco);
	void ProcessBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);
	SCODATA  *GetScoByUrno(long lpUrno);


	// Private methods
private:
    void PrepareScoData(SCODATA *prpScoData);

};

//---------------------------------------------------------------------------------------------------------


#endif //__CEDAPSCODATA__
