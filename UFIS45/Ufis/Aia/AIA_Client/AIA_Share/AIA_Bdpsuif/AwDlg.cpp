// AwDlg.cpp : implementation file
//

#include "stdafx.h"
#include "CCSGlobl.h"
#include "bdpsuif.h"
#include "AwDlg.h"
#include "BasicData.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// AwDlg dialog


AwDlg::AwDlg(CStringArray *popColumn1, 
			 CStringArray *popColumn2, 
			 CStringArray *popUrnos, 
			 CString opHeader1,
			 CString opHeader2,
			 CWnd* pParent /*=NULL*/)
	: CDialog(AwDlg::IDD, pParent)
{
	pomColumn1 = popColumn1;
	pomColumn2 = popColumn2;
	omHeader1 = opHeader1;
	omHeader2 = opHeader2;
	pomUrnos = popUrnos;
	pomTable = NULL;
	pomTable = new CCSTable;

	//*** 30.08.99 SHA ***
	obShowCodes= FALSE;

	lmReturnUrno = 0;
	//{{AFX_DATA_INIT(AwDlg)
	//}}AFX_DATA_INIT
}

AwDlg::~AwDlg()
{
	delete pomTable;
}
void AwDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(AwDlg)
	DDX_Control(pDX, IDC_LISTALC, m_Alc);
	DDX_Control(pDX, IDC_LISTACT, m_Act);
	DDX_Control(pDX, IDOK, m_OK);
	DDX_Control(pDX, IDC_FRAME, m_Frame);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(AwDlg, CDialog)
	//{{AFX_MSG_MAP(AwDlg)
	ON_MESSAGE(WM_TABLE_LBUTTONDBLCLK,	OnTableLButtonDblclk)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// AwDlg message handlers

void AwDlg::OnOK() 
{
	CCSPtrArray<TABLE_COLUMN> olLine;
	int ilLineNo = pomTable->GetCurSel();
	if (ilLineNo == -1)
	{
		MessageBox(LoadStg(IDS_STRING148),LoadStg(IDS_STRING149),MB_ICONEXCLAMATION);
		return;
	}
	pomTable->GetTextLineColumns(&olLine, ilLineNo);
	
	if(olLine.GetSize() == 2 || olLine.GetSize() == 1)
	{
		omReturnString = olLine[0].Text;
	}
	else if(olLine.GetSize() == 3)
	{
		char pclUrno[20]="";
		omReturnString = olLine[0].Text;
		sprintf(pclUrno, "%ld", olLine[2].Text);
		lmReturnUrno = atoi(pclUrno);		
	}

	if (obShowCodes)
	{
		//*** 27.08.99 SHA ***
		if (m_Act.GetCurSel()!=-1)
		{
			m_Act.GetText(m_Act.GetCurSel(),olReturnAct);
		}
		else
			olReturnAct = " ";

		if (m_Alc.GetCurSel()!=-1)
		{
			m_Alc.GetText(m_Alc.GetCurSel(),olReturnAlc);
		}
		else
			olReturnAlc = " ";
	}

	CDialog::OnOK();
}

BOOL AwDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	CRect olRectBorder;
	m_Frame.GetWindowRect( olRectBorder );
	ScreenToClient(olRectBorder);
	olRectBorder.DeflateRect(2,2);
	CCSPtrArray<TABLE_COLUMN> olColList;
	TABLE_COLUMN rlColumnData;

	int ilTab = (int)((olRectBorder.right - olRectBorder.left)/4);
//	pomTable->SetHeaderSpacing(0);
    pomTable->SetTableData(this, olRectBorder.left, olRectBorder.right, olRectBorder.top, olRectBorder.bottom);
	pomTable->SetSelectMode(0);

	CCSPtrArray <TABLE_HEADER_COLUMN> omHeaderDataArray;
	TABLE_HEADER_COLUMN *prlHeader[5];
//	pomTable->SetShowSelection(false);
	pomTable->ResetContent();

	prlHeader[0] = new TABLE_HEADER_COLUMN;
	prlHeader[0]->Alignment = COLALIGN_LEFT;
	prlHeader[0]->Length = ilTab; 
	prlHeader[0]->Font = &ogCourier_Regular_10;
	prlHeader[0]->Text = omHeader1; 

	prlHeader[1] = new TABLE_HEADER_COLUMN;
	prlHeader[1]->Alignment = COLALIGN_LEFT;
	prlHeader[1]->Length = ilTab*4; 
	prlHeader[1]->Font = &ogCourier_Regular_10;
	prlHeader[1]->Text = omHeader2;

	prlHeader[2] = new TABLE_HEADER_COLUMN;
	prlHeader[2]->Alignment = COLALIGN_LEFT;
	prlHeader[2]->Length = 0; 
	prlHeader[2]->Font = &ogCourier_Regular_10;
	prlHeader[2]->Text = omHeader2;

	for(int ili = 0; ili < 3; ili++)
	{
		omHeaderDataArray.Add(prlHeader[ili]);
	}
	pomTable->SetHeaderFields(omHeaderDataArray);
	omHeaderDataArray.DeleteAll();
	pomTable->SetDefaultSeparator();
	omHeaderDataArray.DeleteAll();
	for(int i = 0; i < pomColumn1->GetSize(); i++)
	{
		rlColumnData.SeparatorType = SEPA_NONE;
		rlColumnData.VerticalSeparator = SEPA_LIKEVERTICAL;
		rlColumnData.Text = pomColumn1->GetAt(i);
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		if(i < pomColumn2->GetSize())
		{
			rlColumnData.Text = pomColumn2->GetAt(i);
			olColList.NewAt(olColList.GetSize(), rlColumnData);
		}
		if(i < pomUrnos->GetSize())
		{
			rlColumnData.Text = pomUrnos->GetAt(i);
			olColList.NewAt(olColList.GetSize(), rlColumnData);
		}
		pomTable->AddTextLine(olColList, (void*)(NULL));
		olColList.DeleteAll();
	}
	pomTable->DisplayTable();


	if (obShowCodes)
	{
		
		//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
		//*** 27.08.99 SHA ***
		GetDlgItem(IDC_LISTACT)->SetFont(&ogCourier_Regular_10);
		if (ogAct->GetSize()>0)
		{
			for (int iAct=0; iAct<ogAct->GetSize();iAct++)
			{
				m_Act.AddString(ogAct->GetAt(iAct));
			}
		}
		GetDlgItem(IDC_LISTALC)->SetFont(&ogCourier_Regular_10);
		if (ogAlt->GetSize()>0)
		{
			for (int iAlt=0; iAlt<ogAlt->GetSize();iAlt++)
			{
				m_Alc.AddString(ogAlt->GetAt(iAlt));
			}
		}
		
		//*** 30.09.99 SHA ***
		//*** DEFAULTAIRLINE FOR THE QUALIFICATION ***
		//*** ALITALIA REQUIREMENT ***
		char pclDefaultAirline[10];
		char pclConfigPath[256];

		if (getenv("CEDA") == NULL)
			strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
		else
			strcpy(pclConfigPath, getenv("CEDA"));

		GetPrivateProfileString("BDPS-UIF", "DEFAULTAIRLINE", "###", pclDefaultAirline, sizeof pclDefaultAirline, pclConfigPath);
		
		if (pclDefaultAirline[1]!='#')
			m_Alc.SelectString(0,pclDefaultAirline);

		//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	}
	else
	{
		CRect olRect;
		GetWindowRect(olRect);
		MoveWindow(olRect.left,olRect.top,olRect.right - olRect.left -220,olRect.bottom - olRect.top);
		GetDlgItem(IDC_LISTACT)->ShowWindow(FALSE);
		GetDlgItem(IDC_LISTALC)->ShowWindow(FALSE);
	}

	
	
	return TRUE; 
}

LONG AwDlg::OnTableLButtonDblclk(UINT wParam, LONG lParam) 
{
	OnOK();
	return 0L;
}
