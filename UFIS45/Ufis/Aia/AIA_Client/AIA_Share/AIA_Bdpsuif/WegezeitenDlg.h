#if !defined(AFX_WEGEZEITENDLG_H__7E5FEA51_7397_11D1_B42E_0000B45A33F5__INCLUDED_)
#define AFX_WEGEZEITENDLG_H__7E5FEA51_7397_11D1_B42E_0000B45A33F5__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// WegezeitenDlg.h : header file
//

#include "CCSEdit.h"
#include "CCSComboBox.h"
#include "CedaWayData.h"

/////////////////////////////////////////////////////////////////////////////
// WegezeitenDlg dialog

class WegezeitenDlg : public CDialog
{
// Construction
public:
	WegezeitenDlg(WAYDATA *popWay, CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(WegezeitenDlg)
	enum { IDD = IDD_WEGEZEITENDLG };
	CString		m_Caption;
	CButton		m_TYENP;
	CButton		m_TYENR;
	CButton		m_TYENG;
	CButton		m_TYBEP;
	CButton		m_TYBER;
	CButton		m_TYBEG;
	CComboBox	m_WTYP;
	CCSComboBox	m_POENG;
	CCSComboBox	m_POBEG;
	CCSEdit		m_LSTUD;
	CCSEdit		m_GRUP;
	CCSEdit		m_LSTUT;
	CCSEdit		m_CDATT;
	CCSEdit		m_CDATD;
	CCSEdit		m_POBE;
	CCSEdit		m_POEN;
	CCSEdit		m_TTGO;
	CCSEdit		m_USEC;
	CCSEdit		m_USEU;
	CCSEdit		m_WTGO;
	CCSEdit		m_WTGOS;
	CCSEdit		m_WTGOT;
	CCSEdit		m_HOME;
	CButton		m_OK;
	//}}AFX_DATA

	WAYDATA *pomWay;
	char cmOldTybe;
	char cmOldTyen;
	bool GetGruppnames();
	void SetCurSelByUrno(CComboBox* popCBox,CString opUrno);
	CString GetUrnoBySetCurSel(CComboBox* popCBox);


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(WegezeitenDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(WegezeitenDlg)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	afx_msg void OnTybe();
	afx_msg void OnTyen();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_WEGEZEITENDLG_H__7E5FEA51_7397_11D1_B42E_0000B45A33F5__INCLUDED_)
