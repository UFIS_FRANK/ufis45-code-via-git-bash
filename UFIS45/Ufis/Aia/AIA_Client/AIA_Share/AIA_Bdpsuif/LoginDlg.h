// LoginDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CLoginDialog dialog

#ifndef __LOGINDLG__
#define __LOGINDLG__

#include "resource.h"
#include "CCSEdit.h"
#include "CCSGlobl.h"

#define MAX_LOGIN 3

class CLoginDialog : public CDialog 
{
// Construction
public:

	//Constructor.
    CLoginDialog(const char *pcpHomeAirport, const char *pcpAppl, const char *pcpWks, CWnd* pParent = NULL );     // standard constructor

// Dialog Data
    //{{AFX_DATA(CLoginDialog)
	enum { IDD = IDD_LOGIN };
	CCSEdit	m_UsernameCtrl;
	CCSEdit	m_PasswordCtrl;
	//}}AFX_DATA

	CString omUsername,omPassword; // entered by the user
	CString omErrTxt;
	bool Login(const char *pcpUsername, const char *pcpPassword);

private:
	int	imLoginCount;
	char pcmHomeAirport[20], pcmAppl[20], pcmWks[50];

// Implementation
protected:
    virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

    // Generated message map functions
    //{{AFX_MSG(CLoginDialog)
    afx_msg void OnPaint();
	virtual void OnOK();
	virtual void OnCancel();
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
    DECLARE_MESSAGE_MAP()
};


#endif // __LOGINDLG__
