#if !defined(AFX_GATEDLG_H__56BF0B47_2049_11D1_B38A_0000C016B067__INCLUDED_)
#define AFX_GATEDLG_H__56BF0B47_2049_11D1_B38A_0000C016B067__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// GateDlg.h : header file
//
#include "CedaGATData.h"
#include "CCSEdit.h"
#include "CedaBlkData.h"
#include "CCSTable.h"

/////////////////////////////////////////////////////////////////////////////
// GateDlg dialog

class GateDlg : public CDialog
{
// Construction
public:
	GateDlg(GATDATA *popGAT,CWnd* pParent = NULL);   // standard constructor
	~GateDlg();

	CCSPtrArray<BLKDATA> omBlkPtrA;
	CCSPtrArray<BLKDATA> omDeleteBlkPtrA;

// Dialog Data
	//{{AFX_DATA(GateDlg)
	enum { IDD = IDD_GATEDLG };
	CButton	m_NOAVNEW;
	CButton	m_NOAVDEL;
	CStatic	m_NOAVFRAME;
	CCSEdit	m_DEFD;
	CButton	m_OK;
	CButton	m_BUSG;
	CString	m_Caption;
	CCSEdit	m_CDATD;
	CCSEdit	m_CDATT;
	CCSEdit	m_LSTUD;
	CCSEdit	m_LSTUT;
	CCSEdit	m_USEU;
	CCSEdit	m_USEC;
	CCSEdit	m_VAFRD;
	CCSEdit	m_VAFRT;
	CCSEdit	m_VATOD;
	CCSEdit	m_VATOT;
	CCSEdit	m_BNAFD;
	CCSEdit	m_BNAFT;
	CCSEdit	m_BNATD;
	CCSEdit	m_BNATT;
	CCSEdit	m_GNAM;
	CCSEdit	m_GRUP;
	CCSEdit	m_NOBR;
	CCSEdit	m_RBAB;
	CCSEdit	m_RESB;
	CCSEdit	m_RGA1;
	CCSEdit	m_RGA2;
	CCSEdit	m_TELE;
	CCSEdit	m_TERM;
	CCSEdit	m_HOME;
	CCSEdit	m_GTID;
	CCSEdit	m_GTYP;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(GateDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(GateDlg)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	afx_msg LONG OnTableLButtonDblclk(UINT wParam, LONG lParam);
	afx_msg void OnNoavDel();
	afx_msg void OnNoavNew();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

private:

	GATDATA *pomGAT;

	CCSTable	*pomTable;

	void MakeNoavTable(CString opTabn, CString opBurn);
	void ChangeNoavTable(BLKDATA *prpBlk, int ipLineNo);
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_GATEDLG_H__56BF0B47_2049_11D1_B38A_0000C016B067__INCLUDED_)
