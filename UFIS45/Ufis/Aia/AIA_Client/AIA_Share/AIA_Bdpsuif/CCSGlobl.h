// CCSGlobl.h: interface for the CCSGlobl class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_CCSGLOBL_H__55D90F34_191F_11D1_82BE_0080AD1DC701__INCLUDED_)
#define AFX_CCSGLOBL_H__55D90F34_191F_11D1_82BE_0080AD1DC701__INCLUDED_

#ifndef __CCSGLOBL_H__
#define __CCSGLOBL_H__

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000


#include "CCSDefines.h"

/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
// Global Variable Section


class CCSBcHandle;
class CCSCedaCom;
class CCSDdx;
class CCSLog;
class CCSBcHandle;
class CBasicData;
class CedaBasicData;

class CedaCfgData;
class CedaWROData;
class CedaTWYData;
class CedaSTYData;
class CedaRWYData;
class CedaPSTData;
class CedaNATData;
class CedaMVTData;
class CedaHTYData;
class CedaHAGData;
class CedaGATData;
class CedaFIDData;
class CedaEXTData;
class CedaDENData;
class CedaCICData;
class CedaBLTData;
class CedaAPTData;
class CedaALTData;
class CedaACTData;
class CedaACRData;
class CedaSeaData;
class CedaSphData;
class CedaStrData;
class CedaGhsData;
class CedaGegData;
class CedaPerData;
class CedaGrmData;
class CedaGrnData;
class CedaOrgData;
class CedaPfcData;
class CedaCotData;
class CedaAsfData;
class CedaBsdData;
class CedaOdaData;
class CedaTeaData;
class CedaStfData;
class CedaWayData;
class CedaPrcData;
class CedaSorData;
class CedaSpfData;
class CedaSpeData;
class CedaScoData;
class CedaSteData;
class CedaChtData;
class CedaTipData;
class CedaBlkData;
class CedaHolData;
class CedaWgpData;
class CedaSwgData;
class CedaPgpData;

class CedaAwiData;
class CedaCccData;
class CedaVipData;
class CedaAfmData;
class CedaEntData;


class PrivList;

extern CCSBcHandle ogBcHandle;
extern CCSCedaCom ogCommHandler;  // The one and only CedaCom object
extern CCSDdx ogDdx;
extern CCSLog ogLog;
extern CBasicData ogBasicData;

extern CedaBasicData ogBCD;

extern CedaCfgData ogCfgData;
extern CedaWROData ogWROData;
extern CedaTWYData ogTWYData;
extern CedaSTYData ogSTYData;
extern CedaRWYData ogRWYData;
extern CedaPSTData ogPSTData;
extern CedaNATData ogNATData;
extern CedaMVTData ogMVTData;
extern CedaHTYData ogHTYData;
extern CedaHAGData ogHAGData;
extern CedaGATData ogGATData;
extern CedaFIDData ogFIDData;
extern CedaEXTData ogEXTData;
extern CedaDENData ogDENData;
extern CedaCICData ogCICData;
extern CedaBLTData ogBLTData;
extern CedaAPTData ogAPTData;
extern CedaALTData ogALTData;
extern CedaACTData ogACTData;
extern CedaACRData ogACRData;
extern CedaSeaData ogSeaData;
extern CedaSphData ogSphData;
extern CedaStrData ogStrData;
extern CedaGhsData ogGhsData;
extern CedaGegData ogGegData;
extern CedaPerData ogPerData;
extern CedaGrmData ogGrmData;
extern CedaGrnData ogGrnData;
extern CedaOrgData ogOrgData;
extern CedaPfcData ogPfcData;
extern CedaCotData ogCotData;
extern CedaAsfData ogAsfData;
extern CedaBsdData ogBsdData;
extern CedaOdaData ogOdaData;
extern CedaTeaData ogTeaData;
extern CedaStfData ogStfData;
extern CedaWayData ogWayData;
extern CedaPrcData ogPrcData;
extern CedaSorData ogSorData;
extern CedaSpfData ogSpfData;
extern CedaSpeData ogSpeData;
extern CedaScoData ogScoData;
extern CedaSteData ogSteData;
extern CedaSwgData ogSwgData;
extern CedaChtData ogChtData;
extern CedaTipData ogTipData;
extern CedaBlkData ogBlkData;
extern CedaHolData ogHolData;
extern CedaWgpData ogWgpData;
extern CedaPgpData ogPgpData;
extern CedaAwiData ogAwiData;
extern CedaCccData ogCccData;
extern CedaVipData ogVipData;
extern CedaAfmData ogAFMData;
extern CedaEntData ogENTData;

extern PrivList ogPrivList;

extern const char *pcgAppName;


enum enumColorIndexes
{
	IDX_GRAY=2,IDX_GREEN,IDX_RED,IDX_BLUE,IDX_SILVER,IDX_MAROON,
	IDX_OLIVE,IDX_NAVY,IDX_PURPLE,IDX_TEAL,IDX_LIME,
	IDX_YELLOW,IDX_FUCHSIA,IDX_AQUA, IDX_WHITE,IDX_BLACK,IDX_ORANGE
};

enum DLG_ACTION 
{ 
    DLG_INSERT,DLG_UPDATE
   
};  

//enum enumBarType{BAR_FLIGHT,BAR_SPECIAL,BKBAR,GEDBAR,BAR_BREAK,BAR_ABSENT,BAR_SHADOW};


#define MAXCOLORS 64
#define FIRSTCONFLICTCOLOR 31

// Symbolic colors (helper constants for CGateDiagramDialog -- testing purpose only)
#define BLACK   RGB(  0,   0,   0)
#define MAROON  RGB(128,   0,   0)          // dark red
#define GREEN   RGB(  0, 128,   0)          // dark green
#define OLIVE   RGB(128, 128,   0)          // dark yellow
#define NAVY    RGB(  0,   0, 128)          // dark blue
#define PURPLE  RGB(128,   0, 128)          // dark magenta
#define TEAL    RGB(  0, 128, 128)          // dark cyan
#define GRAY    RGB(128, 128, 128)          // dark gray
#define SILVER  RGB(192, 192, 192)          // light gray
#define RED     RGB(255,   0,   0)
#define ORANGE  RGB(255, 132,   0)
#define LIME    RGB(  0, 255,   0)          // green
#define YELLOW  RGB(255, 255, 128)          // light yellow (yello=255,255,0)
#define BLUE    RGB(  0,   0, 255)
#define FUCHSIA RGB(255,   0, 255)          // magenta
#define AQUA    RGB(  0, 255, 255)          // cyan
#define WHITE   RGB(255, 255, 255)

extern COLORREF ogColors[];
extern CBrush *ogBrushs[];
extern CString ogAppName;

extern COLORREF lgBkColor;
extern COLORREF lgTextColor;
extern COLORREF lgHilightColor;

extern enum enumRecordState egRecordState;

extern BOOL bgIsInitialized;

// CFont : translate from logical point to physical pixel
#define PT_TO_PIXELS(dc, pt)    (- MulDiv( pt, (dc).GetDeviceCaps( LOGPIXELSY ), 72 ))


/////////////////////////////////////////////////////////////////////////////
// IDs


#define IDC_PREV            0x4008
#define IDC_NEXT            0x4009
#define IDC_INPLACEEDIT		0x400c


/////////////////////////////////////////////////////////////////////////////
// Messages


// Message and constants which are used to handshake viewer and the attached diagram
#define WM_POSITIONCHILD				(WM_USER + 201)
#define WM_UPDATEDIAGRAM				(WM_USER + 202)
#define UD_UPDATEGROUP                  (WM_USER + 203)
#define UD_DELETELINE                   (WM_USER + 204)
#define UD_DELETEGROUP                  (WM_USER + 205)
#define UD_INSERTLINE                   (WM_USER + 206)
#define UD_INSERTGROUP                  (WM_USER + 207)
#define UD_UPDATELINEHEIGHT             (WM_USER + 208)
#define UD_UPDATELINE					(WM_USER + 209)
#define UD_UPDATEDLG					(WM_USER + 210)
#define WM_REASSIGNFINISHED				(WM_USER + 211)
#define WM_ATTENTIONSETCOLOR			(WM_USER + 212)
#define UD_REASSIGNFINISHED				(WM_USER + 212)

#define WM_EVALUATE_CMDLINE				(WM_USER + 220)


/////////////////////////////////////////////////////////////////////////////
// Font variable

extern CFont ogSmallFonts_Regular_6;
extern CFont ogSmallFonts_Regular_7;
extern CFont ogMSSansSerif_Regular_8;
extern CFont ogMSSansSerif_Bold_8;
extern CFont ogMSSansSerif_Bold_12;

extern CFont ogCourier_Bold_10;
extern CFont ogCourier_Regular_10;
extern CFont ogCourier_Regular_8;
extern CFont ogCourier_Regular_9;

extern CFont ogScalingFonts[4];
extern int igFontIndex1;
extern int igFontIndex2;


void InitFont();
void DeleteBrushes();
void CreateBrushes();


enum{MS_SANS6, MS_SANS8, MS_SANS12, MS_SANS16, 
     MS_SANS6BOLD, MS_SANS8BOLD, MS_SANS12BOLD, MS_SANS16BOLD};

struct FONT_INDEXES
{
	int VerticalScale;
	int Chart;
	FONT_INDEXES(void)
	{VerticalScale=0;Chart=0;}
};


/////////////////////////////////////////////////////////////////////////////
// Color and brush variables

extern COLORREF ogColors[];
extern CBrush *ogBrushs[];

/////////////////////////////////////////////////////////////////////////////



struct TIMEFRAMEDATA
{
	CTime StartTime;
	CTime EndTime;
	TIMEFRAMEDATA(void)
	{StartTime=TIMENULL;EndTime=TIMENULL;}
};


/////////////////////////////////////////////////////////////////////////////
// application globals
extern char pcgHome[4]; 
extern char pcgTableExt[4]; 

extern CString ogHome;
extern CString ogTableExt;

extern bool bgNoScroll;
extern bool bgOnline;
extern bool bgIsButtonListMovable;

extern bool bgViewEditFilter;
extern bool bgViewEditSort;

class CInitialLoadDlg;
extern CInitialLoadDlg *pogInitialLoad;

/////////////////////////////////////////////////////////////////////////////
// IDs

//***** moved from "ccsglobl.h" by Damkerng 04/30/96 18:30
//***** moved from "tscall.cpp" by Pichate May 08,96 18:15
#define IDC_TIMESCALE       0x4001

//***** moved from "ccsglobl.h" by Damkerng 04/30/96 18:30
//***** moved from "vscale.cpp" by Pichate May 08,96 18:15
#define IDC_VERTICALSCALE   0x4002

//***** moved from "ccsglobl.h" by Damkerng 04/30/96 18:30
//***** moved from "gbar.cpp" by Pichate May 08,96 18:15
#define IDC_GANTTBAR        0x4003

//#define IDD_DIAGRAM         0x4004
#define IDD_CHART           0x4005
#define IDC_CHARTBUTTON     0x4006
#define IDD_GANTT           0x4007

#define IDC_PREV            0x4008
#define IDC_NEXT            0x4009
#define IDC_ARRIVAL         0x400a
#define IDC_DEPARTURE       0x400b
#define IDC_INPLACEEDIT		0x400c

#define IDD_GANTTCHART      0x4101


/////////////////////////////////////////////////////////////////////////////
// Messages

// Message sent from PrePlanTable to the parent window when closed
// Id 24-Sep-96
// Fix some bug here, since CCI diagram and Gate diagram shares the same message number.
// This surely will crash the machine or destroy our memory blocks if the user terminate
// the diagram with Alt-F4.
#define WM_STAFFTABLE_EXIT          (WM_APP + 20)	/* tables */
#define WM_GEQTABLE_EXIT            (WM_APP + 21)	/* tables */
#define WM_INFTABLE_EXIT            (WM_APP + 22)	/* tables */
#define WM_HWDIA_EXIT				(WM_APP + 23)	/* tables */
#define WM_FLIGHTDIA_EXIT           (WM_APP + 24)	/* tables */
#define WM_STAFFDIA_EXIT            (WM_APP + 25)	/* tables */
#define WM_CONFTABLE_EXIT           (WM_APP + 26)	/* tables */
#define WM_FLIGHTTABLE_EXIT         (WM_APP + 27)	/* tables */
#define WM_MAGNETICTABLE_EXIT       (WM_APP + 28)	/* tables */
#define WM_ATTENTIONTABLE_EXIT      (WM_APP + 29)	/* tables */
#define WM_CONFLICTTABLE_EXIT       (WM_APP + 30)	/* tables */
#define WM_LOCKWINDOWUPDATE         (WM_APP + 31)	/* tables */
#define WM_ZOLLTABLE_EXIT			(WM_APP + 32)	/* tables */





/////////////////////////////////////////////////////////////////////////////
// Drag Information Type

enum 
{
	DIT_AFLIGHT,	// source: Flugdatenansicht
	DIT_DFLIGHT		// source: Flugdatenansicht
};

// DIT 0 - 49



enum
{
	DLG_NEW,
	DLG_COPY,
	DLG_CHANGE
};


enum enumDDXTypes
{
    UNDO_CHANGE, 
    BC_CFG_INSERT,BC_CFG_CHANGE,CFG_CHANGE,CFG_INSERT,CFG_DELETE,
    // from here, there are defines for your project
	BC_ALT_CHANGE,BC_ALT_DELETE,ALT_CHANGE,ALT_DELETE,    
	BC_ACT_CHANGE,BC_ACT_DELETE,ACT_CHANGE,ACT_DELETE,    
	BC_ACR_CHANGE,BC_ACR_DELETE,ACR_CHANGE,ACR_DELETE,
	BC_APT_CHANGE,BC_APT_DELETE,APT_CHANGE,APT_DELETE,    
	BC_RWY_CHANGE,BC_RWY_DELETE,RWY_CHANGE,RWY_DELETE,    
	BC_TWY_CHANGE,BC_TWY_DELETE,TWY_CHANGE,TWY_DELETE,   
	BC_PST_CHANGE,BC_PST_DELETE,PST_CHANGE,PST_DELETE, 
	BC_GAT_CHANGE,BC_GAT_DELETE,GAT_CHANGE,GAT_DELETE,
	BC_CIC_CHANGE,BC_CIC_DELETE,CIC_CHANGE,CIC_DELETE,   
	BC_BLT_CHANGE,BC_BLT_DELETE,BLT_CHANGE,BLT_DELETE,   
	BC_EXT_CHANGE,BC_EXT_DELETE,EXT_CHANGE,EXT_DELETE,   
	BC_DEN_CHANGE,BC_DEN_DELETE,DEN_CHANGE,DEN_DELETE,   
	BC_MVT_CHANGE,BC_MVT_DELETE,MVT_CHANGE,MVT_DELETE,   
	BC_HAG_CHANGE,BC_HAG_DELETE,HAG_CHANGE,HAG_DELETE,    
	BC_NAT_CHANGE,BC_NAT_DELETE,NAT_CHANGE,NAT_DELETE,   
	BC_WRO_CHANGE,BC_WRO_DELETE,WRO_CHANGE,WRO_DELETE,   
	BC_HTY_CHANGE,BC_HTY_DELETE,HTY_CHANGE,HTY_DELETE,   
	BC_STY_CHANGE,BC_STY_DELETE,STY_CHANGE,STY_DELETE,   
	BC_FID_CHANGE,BC_FID_DELETE,FID_CHANGE,FID_DELETE,   
	BC_SEA_CHANGE,BC_SEA_DELETE,BC_SEA_NEW,SEA_CHANGE,SEA_DELETE,SEA_NEW,
	BC_SPH_CHANGE,BC_SPH_DELETE,BC_SPH_NEW,SPH_CHANGE,SPH_DELETE,SPH_NEW,
	BC_STR_CHANGE,BC_STR_DELETE,BC_STR_NEW,STR_CHANGE,STR_DELETE,STR_NEW,
	BC_GHS_CHANGE,BC_GHS_DELETE,BC_GHS_NEW,GHS_CHANGE,GHS_DELETE,GHS_NEW,
	BC_GEG_CHANGE,BC_GEG_DELETE,BC_GEG_NEW,GEG_CHANGE,GEG_DELETE,GEG_NEW,
	BC_PER_CHANGE,BC_PER_DELETE,BC_PER_NEW,PER_CHANGE,PER_DELETE,PER_NEW,
	BC_PEF_CHANGE,BC_PEF_DELETE,BC_PEF_NEW,PEF_CHANGE,PEF_DELETE,PEF_NEW,
	BC_GRM_CHANGE,BC_GRM_DELETE,BC_GRM_NEW,GRM_CHANGE,GRM_DELETE,GRM_NEW,
	BC_GRN_CHANGE,BC_GRN_DELETE,BC_GRN_NEW,GRN_CHANGE,GRN_DELETE,GRN_NEW,
	BC_ORG_CHANGE,BC_ORG_DELETE,BC_ORG_NEW,ORG_CHANGE,ORG_DELETE,ORG_NEW,
	BC_PFC_CHANGE,BC_PFC_DELETE,BC_PFC_NEW,PFC_CHANGE,PFC_DELETE,PFC_NEW,
	BC_COT_CHANGE,BC_COT_DELETE,BC_COT_NEW,COT_CHANGE,COT_DELETE,COT_NEW,
	BC_ASF_CHANGE,BC_ASF_DELETE,BC_ASF_NEW,ASF_CHANGE,ASF_DELETE,ASF_NEW,
	BC_BSS_CHANGE,BC_BSS_DELETE,BC_BSS_NEW,BSS_CHANGE,BSS_DELETE,BSS_NEW,
	BC_BSD_CHANGE,BC_BSD_DELETE,BC_BSD_NEW,BSD_CHANGE,BSD_DELETE,BSD_NEW,
	BC_ODA_CHANGE,BC_ODA_DELETE,BC_ODA_NEW,ODA_CHANGE,ODA_DELETE,ODA_NEW,
	BC_TEA_CHANGE,BC_TEA_DELETE,BC_TEA_NEW,TEA_CHANGE,TEA_DELETE,TEA_NEW,
	BC_WGP_CHANGE,BC_WGP_DELETE,BC_WGP_NEW,WGP_CHANGE,WGP_DELETE,WGP_NEW,
	BC_STF_CHANGE,BC_STF_DELETE,BC_STF_NEW,STF_CHANGE,STF_DELETE,STF_NEW,
	BC_WAY_CHANGE,BC_WAY_DELETE,BC_WAY_NEW,WAY_CHANGE,WAY_DELETE,WAY_NEW,
	BC_PRC_CHANGE,BC_PRC_DELETE,BC_PRC_NEW,PRC_CHANGE,PRC_DELETE,PRC_NEW,
	BC_SOR_CHANGE,BC_SOR_DELETE,BC_SOR_NEW,SOR_CHANGE,SOR_DELETE,SOR_NEW,
	BC_SPF_CHANGE,BC_SPF_DELETE,BC_SPF_NEW,SPF_CHANGE,SPF_DELETE,SPF_NEW,
	BC_SPE_CHANGE,BC_SPE_DELETE,BC_SPE_NEW,SPE_CHANGE,SPE_DELETE,SPE_NEW,
	BC_SCO_CHANGE,BC_SCO_DELETE,BC_SCO_NEW,SCO_CHANGE,SCO_DELETE,SCO_NEW,
	BC_STE_CHANGE,BC_STE_DELETE,BC_STE_NEW,STE_CHANGE,STE_DELETE,STE_NEW,
	BC_SWG_CHANGE,BC_SWG_DELETE,BC_SWG_NEW,SWG_CHANGE,SWG_DELETE,SWG_NEW,
	BC_CHT_CHANGE,BC_CHT_DELETE,BC_CHT_NEW,CHT_CHANGE,CHT_DELETE,CHT_NEW,	
	BC_TIP_CHANGE,BC_TIP_DELETE,BC_TIP_NEW,TIP_CHANGE,TIP_DELETE,TIP_NEW,	
	BC_BLK_CHANGE,BC_BLK_DELETE,BC_BLK_NEW,BLK_CHANGE,BLK_DELETE,BLK_NEW,	
	BC_HOL_CHANGE,BC_HOL_DELETE,BC_HOL_NEW,HOL_CHANGE,HOL_DELETE,HOL_NEW,
	BC_PGP_CHANGE,BC_PGP_DELETE,BC_PGP_NEW,PGP_CHANGE,PGP_DELETE,PGP_NEW,
	BC_AWI_CHANGE,BC_AWI_DELETE,BC_AWI_NEW,AWI_CHANGE,AWI_DELETE,AWI_NEW,
	BC_CCC_CHANGE,BC_CCC_DELETE,BC_CCC_NEW,CCC_CHANGE,CCC_DELETE,CCC_NEW,
	BC_VIP_CHANGE,BC_VIP_DELETE,BC_VIP_NEW,VIP_CHANGE,VIP_DELETE,VIP_NEW,
	BC_AFM_CHANGE,BC_AFM_DELETE,BC_AFM_NEW,AFM_CHANGE,AFM_DELETE,AFM_NEW,
	BC_ENT_CHANGE,BC_ENT_DELETE,BC_ENT_NEW,ENT_CHANGE,ENT_DELETE,ENT_NEW
};

// end globals
/////////////////////////////////////////////////////////////////////////////

#define SetWndStatAll(clStat, plWnd)\
	if(clStat=='1') {plWnd.ShowWindow(SW_SHOW);plWnd.EnableWindow(TRUE);}\
	else if(clStat=='0') {plWnd.ShowWindow(SW_SHOW);plWnd.EnableWindow(FALSE);}\
	else if(clStat=='-') plWnd.ShowWindow(SW_HIDE);

#define SetWndStatPrio_1(clStat1,clStat2, plWnd)\
	if(clStat1=='-'||clStat2=='-') plWnd.ShowWindow(SW_HIDE);\
	else if(clStat1=='0'||clStat2=='0') {plWnd.ShowWindow(SW_SHOW);plWnd.EnableWindow(FALSE);}\
	else if(clStat1=='1'||clStat2=='1') {plWnd.ShowWindow(SW_SHOW);plWnd.EnableWindow(TRUE);}

extern char cgYes;
extern char cgNo;
extern CString ogAnsicht;
extern CString ogNoData;
extern CString ogNotFormat;



#endif //__CCSGLOBL_H__
#endif // !defined(AFX_CCSGLOBL_H__55D90F34_191F_11D1_82BE_0080AD1DC701__INCLUDED_)
