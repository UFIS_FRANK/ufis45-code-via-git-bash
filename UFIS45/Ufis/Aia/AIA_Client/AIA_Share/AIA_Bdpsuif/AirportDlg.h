#if !defined(AFX_AIRPORTDLG_H__56BF0B49_2049_11D1_B38A_0000C016B067__INCLUDED_)
#define AFX_AIRPORTDLG_H__56BF0B49_2049_11D1_B38A_0000C016B067__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// AirportDlg.h : header file
//
#include "CedaAPTData.h"
#include "CCSEdit.h"

/////////////////////////////////////////////////////////////////////////////
// AirportDlg dialog

class AirportDlg : public CDialog
{
// Construction
public:
	AirportDlg(APTDATA *popAPT,CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(AirportDlg)
	enum { IDD = IDD_AIRPORTDLG };
	CButton	m_OK;
	CString	m_Caption;
	CCSEdit	m_CDATD;
	CCSEdit	m_CDATT;
	CCSEdit	m_LSTUD;
	CCSEdit	m_LSTUT;
	CCSEdit	m_USEU;
	CCSEdit	m_USEC;
	CCSEdit	m_APC3;
	CCSEdit	m_APC4;
	CCSEdit	m_APFN;
	CCSEdit	m_APSN;
	CCSEdit	m_APN2;
	CCSEdit	m_APN3;
	CCSEdit	m_APN4;
	CCSEdit	m_ETOF;
	CCSEdit	m_GRUP;
	CCSEdit	m_LAND;
	CCSEdit	m_TDI1;
	CCSEdit	m_TDI2;
	CCSEdit	m_TICHD;
	CCSEdit	m_TICHT;
	CCSEdit	m_VAFRD;
	CCSEdit	m_VAFRT;
	CCSEdit	m_VATOD;
	CCSEdit	m_VATOT;
	CCSEdit	m_APTT;
	CCSEdit	m_HOME;
	CCSEdit	m_TDIS;
	CCSEdit	m_TDIW;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(AirportDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(AirportDlg)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

private:

	APTDATA *pomAPT;

};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_AIRPORTDLG_H__56BF0B49_2049_11D1_B38A_0000C016B067__INCLUDED_)
