// CedaGrnData.h

#ifndef __CEDAGRNDATA__
#define __CEDAGRNDATA__
 
#include "stdafx.h"
#include "basicdata.h"

//---------------------------------------------------------------------------------------------------------

struct GRNDATA 
{
	char 	 Appl[10]; 	// Anwendungsname
	CTime 	 Cdat; 		// Erstellungsdatum
	char 	 Fldn[8]; 	// Feldnameauf den sich die Gruppierung bezieht
	char 	 Grpn[14]; 	// Gruppenname
	CTime 	 Lstu;		// Datum letzte �nderung
	char 	 Prfl[3]; 	// Protokollierungskennzeichen
	char 	 Tabn[8]; 	// Tabellenname auf den sich die Gruppierung bezieht
	long 	 Urno;		// Eindeutige Datensatz-Nr.
	char 	 Usec[34]; 	// Anwender (Ersteller)
	char 	 Useu[34]; 	// Anwender (letzte �nderung)
	char	 Grds[34];	// Beschreibung
	char	 Grsn[5];	// Kurzname

	//DataCreated by this class
	int		 IsChanged;	// Check whether Data has Changed f�r Relaese

	GRNDATA(void)
	{ 
		memset(this,'\0',sizeof(*this));
		IsChanged	=	DATA_UNCHANGED;
		Cdat		=	TIMENULL;
		Lstu		=	TIMENULL;
	}

}; // end GrnDataStrukt

//---------------------------------------------------------------------------------------------------------
// Class declaratino

class CedaGrnData: public CCSCedaData
{
// Attributes
public:
    CMapPtrToPtr omUrnoMap;

    CCSPtrArray<GRNDATA> omData;

	char pcmListOfFields[2048];

// Operations
public:
    CedaGrnData();
	~CedaGrnData();
	virtual void GetDataInfo(CStringArray &ropFields, CStringArray &ropDesription, CStringArray &ropType);
	void Register(void);
	void ClearAll(bool bpWithRegistration = true);
    bool Read(char *pspWhere = NULL);
	bool Insert(GRNDATA *prpGrn);
	bool InsertInternal(GRNDATA *prpGrn);
	bool Update(GRNDATA *prpGrn);
	bool UpdateInternal(GRNDATA *prpGrn);
	bool Delete(long lpUrno);
	bool DeleteInternal(GRNDATA *prpGrn);
	bool ReadSpecialData(CCSPtrArray<GRNDATA> *popGrn,char *pspWhere,char *pspFieldList,bool ipSYS = true);
	bool Save(GRNDATA *prpGrn);
	void ProcessBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);
	GRNDATA  *GetGrnByUrno(long lpUrno);

	// Private methods
private:
    void PrepareGrnData(GRNDATA *prpGrnData);

};

//---------------------------------------------------------------------------------------------------------

#endif //__CEDAGRNDATA__
