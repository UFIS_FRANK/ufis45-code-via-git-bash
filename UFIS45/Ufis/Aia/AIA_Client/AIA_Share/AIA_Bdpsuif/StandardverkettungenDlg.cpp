// StandardverkettungenDlg.cpp : implementation file
//

#include "stdafx.h"
#include "BDPSUIF.h"
#include "StandardverkettungenDlg.h"
#include "CedaGrnData.h"
#include "CedaACTData.h"
#include "PrivList.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// StandardverkettungenDlg dialog


StandardverkettungenDlg::StandardverkettungenDlg(STRDATA *popStr,CWnd* pParent) : CDialog(StandardverkettungenDlg::IDD, pParent)
{
	pomStr = popStr;

	//{{AFX_DATA_INIT(StandardverkettungenDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void StandardverkettungenDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(StandardverkettungenDlg)
	DDX_Control(pDX, IDOK,		 m_OK);
	DDX_Control(pDX, IDC_CDAT_D, m_CDATD);
	DDX_Control(pDX, IDC_CDAT_T, m_CDATT);
	DDX_Control(pDX, IDC_LSTU_D, m_LSTUD);
	DDX_Control(pDX, IDC_LSTU_T, m_LSTUT);
	DDX_Control(pDX, IDC_USEC,	 m_USEC);
	DDX_Control(pDX, IDC_USEU,	 m_USEU);
	DDX_Control(pDX, IDC_VAFR_D, m_VAFRD);
	DDX_Control(pDX, IDC_VAFR_T, m_VAFRT);
	DDX_Control(pDX, IDC_VATO_D, m_VATOD);
	DDX_Control(pDX, IDC_VATO_T, m_VATOT);
	DDX_Control(pDX, IDC_DAYA_1, m_DAYA1);
	DDX_Control(pDX, IDC_DAYA_2, m_DAYA2);
	DDX_Control(pDX, IDC_DAYA_3, m_DAYA3);
	DDX_Control(pDX, IDC_DAYA_4, m_DAYA4);
	DDX_Control(pDX, IDC_DAYA_5, m_DAYA5);
	DDX_Control(pDX, IDC_DAYA_6, m_DAYA6);
	DDX_Control(pDX, IDC_DAYA_7, m_DAYA7);
	DDX_Control(pDX, IDC_DAYA_T, m_DAYAT);
	DDX_Control(pDX, IDC_DAYD_1, m_DAYD1);
	DDX_Control(pDX, IDC_DAYD_2, m_DAYD2);
	DDX_Control(pDX, IDC_DAYD_3, m_DAYD3);
	DDX_Control(pDX, IDC_DAYD_4, m_DAYD4);
	DDX_Control(pDX, IDC_DAYD_5, m_DAYD5);
	DDX_Control(pDX, IDC_DAYD_6, m_DAYD6);
	DDX_Control(pDX, IDC_DAYD_7, m_DAYD7);
	DDX_Control(pDX, IDC_DAYD_T, m_DAYDT);
	DDX_Control(pDX, IDC_ACT3,	 m_ACT3);
	DDX_Control(pDX, IDC_ACTM,	 m_ACTM);
	DDX_Control(pDX, IDC_FLCA,	 m_FLCA);
	DDX_Control(pDX, IDC_FLNA,	 m_FLNA);
	DDX_Control(pDX, IDC_FLCD,	 m_FLCD);
	DDX_Control(pDX, IDC_FLSA,	 m_FLSA);
	DDX_Control(pDX, IDC_FLND,	 m_FLND);
	DDX_Control(pDX, IDC_FLSD,	 m_FLSD);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(StandardverkettungenDlg, CDialog)
	//{{AFX_MSG_MAP(StandardverkettungenDlg)
	ON_BN_CLICKED(IDC_DAYA_1,	OnDaya17)
	ON_BN_CLICKED(IDC_DAYA_2,	OnDaya17)
	ON_BN_CLICKED(IDC_DAYA_3,	OnDaya17)
	ON_BN_CLICKED(IDC_DAYA_4,	OnDaya17)
	ON_BN_CLICKED(IDC_DAYA_5,	OnDaya17)
	ON_BN_CLICKED(IDC_DAYA_6,	OnDaya17)
	ON_BN_CLICKED(IDC_DAYA_7,	OnDaya17)
	ON_BN_CLICKED(IDC_DAYA_T,	OnDayaT)
	ON_BN_CLICKED(IDC_DAYD_1,	OnDayd17)
	ON_BN_CLICKED(IDC_DAYD_2,	OnDayd17)
	ON_BN_CLICKED(IDC_DAYD_3,	OnDayd17)
	ON_BN_CLICKED(IDC_DAYD_4,	OnDayd17)
	ON_BN_CLICKED(IDC_DAYD_5,	OnDayd17)
	ON_BN_CLICKED(IDC_DAYD_6,	OnDayd17)
	ON_BN_CLICKED(IDC_DAYD_7,	OnDayd17)
	ON_BN_CLICKED(IDC_DAYD_T,	OnDaydT)
    ON_MESSAGE(WM_EDIT_KILLFOCUS, OnKillfocus)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// StandardverkettungenDlg message handlers
//----------------------------------------------------------------------------------------

BOOL StandardverkettungenDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	GRNDATA *prlGrn;

	m_Caption = LoadStg(IDS_STRING189) + " <" + m_Caption + ">";
	SetWindowText(m_Caption);
	
	char clStat = ogPrivList.GetStat("STANDARDVERKETTUNGENDLG.m_OK");
	SetWndStatAll(clStat,m_OK);

	//------------------------------------
	m_CDATD.SetBKColor(SILVER);
	m_CDATD.SetInitText(pomStr->Cdat.Format("%d.%m.%Y"));
	m_CDATD.SetSecState(ogPrivList.GetStat("STANDARDVERKETTUNGENDLG.m_CDAT"));
	// - - - - - - - - - - - - - - - - - -
	m_CDATT.SetBKColor(SILVER);
	m_CDATT.SetInitText(pomStr->Cdat.Format("%H:%M"));
	m_CDATT.SetSecState(ogPrivList.GetStat("STANDARDVERKETTUNGENDLG.m_CDAT"));
	//------------------------------------
	m_LSTUD.SetBKColor(SILVER);
	m_LSTUD.SetInitText(pomStr->Lstu.Format("%d.%m.%Y"));
	m_LSTUD.SetSecState(ogPrivList.GetStat("STANDARDVERKETTUNGENDLG.m_LSTU"));
	// - - - - - - - - - - - - - - - - - -
	m_LSTUT.SetBKColor(SILVER);
	m_LSTUT.SetInitText(pomStr->Lstu.Format("%H:%M"));
	m_LSTUT.SetSecState(ogPrivList.GetStat("STANDARDVERKETTUNGENDLG.m_LSTU"));
	//------------------------------------
	m_USEC.SetBKColor(SILVER);
	m_USEC.SetInitText(pomStr->Usec);
	m_USEC.SetSecState(ogPrivList.GetStat("STANDARDVERKETTUNGENDLG.m_USEC"));
	//------------------------------------
	m_USEU.SetBKColor(SILVER);
	m_USEU.SetInitText(pomStr->Useu);
	m_USEU.SetSecState(ogPrivList.GetStat("STANDARDVERKETTUNGENDLG.m_USEU"));
	//------------------------------------//WHITE,YELLOW,SILVER,RED,LYELLOW
	m_ACT3.SetFormat("x|#x|#x|#");
	m_ACT3.SetTextLimit(-1,-1,true);
	m_ACT3.SetTextErrColor(RED);
	m_ACT3.SetInitText(pomStr->Act3);
	m_ACT3.SetSecState(ogPrivList.GetStat("STANDARDVERKETTUNGENDLG.m_ACT3"));
	//------------------------------------
	m_ACTM.SetFormat("x|#x|#x|#");
	m_ACTM.SetTextLimit(0,3);
	m_ACTM.SetTextErrColor(RED);
	prlGrn = ogGrnData.GetGrnByUrno(pomStr->Actm);
	if(prlGrn != NULL)
	{
		m_ACTM.SetInitText(prlGrn->Grsn);
	}
	m_ACTM.SetSecState(ogPrivList.GetStat("STANDARDVERKETTUNGENDLG.m_ACTM"));
	//------------------------------------
	m_FLCA.SetFormat("x|#x|#x|#");
	m_FLCA.SetTextLimit(1,3);
	m_FLCA.SetBKColor(YELLOW);
	m_FLCA.SetTextErrColor(RED);
	m_FLCA.SetInitText(pomStr->Flca);
	m_FLCA.SetSecState(ogPrivList.GetStat("STANDARDVERKETTUNGENDLG.m_FLNA"));
	//------------------------------------
	m_FLNA.SetFormat("x|#x|#x|#x|#x|#");
	m_FLNA.SetTextLimit(1,5);
	m_FLNA.SetBKColor(YELLOW);
	m_FLNA.SetTextErrColor(RED);
	m_FLNA.SetInitText(pomStr->Flna);
	m_FLNA.SetSecState(ogPrivList.GetStat("STANDARDVERKETTUNGENDLG.m_FLNA"));
	//------------------------------------
	m_FLSA.SetFormat("x|#");
	m_FLSA.SetTextLimit(0,1);
	m_FLSA.SetTextErrColor(RED);
	m_FLSA.SetInitText(pomStr->Flsa);
	m_FLSA.SetSecState(ogPrivList.GetStat("STANDARDVERKETTUNGENDLG.m_FLNA"));
	//------------------------------------
	clStat = ogPrivList.GetStat("STANDARDVERKETTUNGENDLG.m_DAYA");
	SetWndStatAll(clStat,m_DAYA1);
	SetWndStatAll(clStat,m_DAYA2);
	SetWndStatAll(clStat,m_DAYA3);
	SetWndStatAll(clStat,m_DAYA4);
	SetWndStatAll(clStat,m_DAYA5);
	SetWndStatAll(clStat,m_DAYA6);
	SetWndStatAll(clStat,m_DAYA7);
	SetWndStatAll(clStat,m_DAYAT);
	if(strchr(pomStr->Daya,'1') != NULL) m_DAYA1.SetCheck(1);
	if(strchr(pomStr->Daya,'2') != NULL) m_DAYA2.SetCheck(1);
	if(strchr(pomStr->Daya,'3') != NULL) m_DAYA3.SetCheck(1);
	if(strchr(pomStr->Daya,'4') != NULL) m_DAYA4.SetCheck(1);
	if(strchr(pomStr->Daya,'5') != NULL) m_DAYA5.SetCheck(1);
	if(strchr(pomStr->Daya,'6') != NULL) m_DAYA6.SetCheck(1);
	if(strchr(pomStr->Daya,'7') != NULL) m_DAYA7.SetCheck(1);
	if(CString(pomStr->Daya).Find("1234567") != -1) m_DAYAT.SetCheck(1);
	//------------------------------------
	m_FLCD.SetFormat("x|#x|#x|#");
	m_FLCD.SetTextLimit(1,3);
//	m_FLCD.SetBKColor(YELLOW);
	m_FLCD.SetTextErrColor(RED);
	m_FLCD.SetInitText(pomStr->Flcd);
	m_FLCD.SetSecState(ogPrivList.GetStat("STANDARDVERKETTUNGENDLG.m_FLND"));
	//------------------------------------
	m_FLND.SetFormat("x|#x|#x|#x|#x|#");
	m_FLND.SetTextLimit(1,5);
	m_FLND.SetTextErrColor(RED);
//	m_FLND.SetBKColor(YELLOW);
	m_FLND.SetInitText(pomStr->Flnd);
	m_FLND.SetSecState(ogPrivList.GetStat("STANDARDVERKETTUNGENDLG.m_FLND"));
	//------------------------------------
	m_FLSD.SetFormat("x|#");
	m_FLSD.SetTextLimit(0,1);
	m_FLSD.SetTextErrColor(RED);
	m_FLSD.SetInitText(pomStr->Flsd);
	m_FLSD.SetSecState(ogPrivList.GetStat("STANDARDVERKETTUNGENDLG.m_FLND"));
	//------------------------------------
	clStat = ogPrivList.GetStat("STANDARDVERKETTUNGENDLG.m_DAYD");
	SetWndStatAll(clStat,m_DAYD1);
	SetWndStatAll(clStat,m_DAYD2);
	SetWndStatAll(clStat,m_DAYD3);
	SetWndStatAll(clStat,m_DAYD4);
	SetWndStatAll(clStat,m_DAYD5);
	SetWndStatAll(clStat,m_DAYD6);
	SetWndStatAll(clStat,m_DAYD7);
	SetWndStatAll(clStat,m_DAYDT);
	if(strchr(pomStr->Dayd,'1') != NULL) m_DAYD1.SetCheck(1);
	if(strchr(pomStr->Dayd,'2') != NULL) m_DAYD2.SetCheck(1);
	if(strchr(pomStr->Dayd,'3') != NULL) m_DAYD3.SetCheck(1);
	if(strchr(pomStr->Dayd,'4') != NULL) m_DAYD4.SetCheck(1);
	if(strchr(pomStr->Dayd,'5') != NULL) m_DAYD5.SetCheck(1);
	if(strchr(pomStr->Dayd,'6') != NULL) m_DAYD6.SetCheck(1);
	if(strchr(pomStr->Dayd,'7') != NULL) m_DAYD7.SetCheck(1);
	//------------------------------------
	m_VAFRD.SetTypeToDate(true);
	m_VAFRD.SetTextErrColor(RED);
	m_VAFRD.SetBKColor(YELLOW);
	m_VAFRD.SetInitText(pomStr->Vafr.Format("%d.%m.%Y"));
	m_VAFRD.SetSecState(ogPrivList.GetStat("STANDARDVERKETTUNGENDLG.m_VAFR"));
	// - - - - - - - - - - - - - - - - - -
	m_VAFRT.SetTypeToTime(true);
	m_VAFRT.SetTextErrColor(RED);
	m_VAFRT.SetBKColor(YELLOW);
	m_VAFRT.SetInitText(pomStr->Vafr.Format("%H:%M"));
	m_VAFRT.SetSecState(ogPrivList.GetStat("STANDARDVERKETTUNGENDLG.m_VAFR"));
	//------------------------------------
	m_VATOD.SetTypeToDate(true);
	m_VATOD.SetTextErrColor(RED);
	m_VATOD.SetBKColor(YELLOW);
	m_VATOD.SetInitText(pomStr->Vato.Format("%d.%m.%Y"));
	m_VATOD.SetSecState(ogPrivList.GetStat("STANDARDVERKETTUNGENDLG.m_VATO"));
	// - - - - - - - - - - - - - - - - - -
	m_VATOT.SetTypeToTime(true);
	m_VATOT.SetTextErrColor(RED);
	m_VATOT.SetBKColor(YELLOW);
	m_VATOT.SetInitText(pomStr->Vato.Format("%H:%M"));
	m_VATOT.SetSecState(ogPrivList.GetStat("STANDARDVERKETTUNGENDLG.m_VATO"));
	//------------------------------------
	return TRUE;  
}

//----------------------------------------------------------------------------------------

void StandardverkettungenDlg::OnOK() 
{
	AfxGetApp()->DoWaitCursor(1);
	CString olErrorText;
	bool ilStatus = true;
	if(m_ACT3.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING240) + ogNotFormat;
	}
	if(m_ACTM.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING22) + ogNotFormat;
	}
	if(m_FLCA.GetStatus() == false)
	{
		ilStatus = false;
		if(m_FLCA.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING412) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING412) + ogNotFormat;
		}
	}
	if(m_FLNA.GetStatus() == false)
	{
		ilStatus = false;
		if(m_FLNA.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING413) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING413) + ogNotFormat;
		}
	}
	if(m_FLSA.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING414) + ogNotFormat;
	}
/*	if(m_FLCD.GetStatus() == false)
	{
		ilStatus = false;
		if(m_FLCD.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING415) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING415) + ogNotFormat;
		}
	}
	if(m_FLND.GetStatus() == false)
	{
		ilStatus = false;
		if(m_FLND.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING416) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING416) + ogNotFormat;
		}
	}
*/	if(m_FLSD.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING17) + ogNotFormat;
	}
	if(m_VAFRD.GetStatus() == false)
	{
		ilStatus = false;
		if(m_VAFRD.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING230) + LoadStg(IDS_STRING232) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING230) + LoadStg(IDS_STRING232) + ogNotFormat;
		}
	}
	if(m_VAFRT.GetStatus() == false)
	{
		ilStatus = false;
		if(m_VAFRT.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING230) + LoadStg(IDS_STRING233) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING230) + LoadStg(IDS_STRING233) + ogNotFormat;
		}
	}
	if(m_VATOD.GetStatus() == false)
	{
		ilStatus = false;
		if(m_VATOD.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING231) + LoadStg(IDS_STRING232) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING231) + LoadStg(IDS_STRING232) + ogNotFormat;
		}
	}
	if(m_VATOT.GetStatus() == false)
	{
		ilStatus = false;
		if(m_VATOT.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING231) + LoadStg(IDS_STRING233) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING231) + LoadStg(IDS_STRING233) + ogNotFormat;
		}
	}

	///////////////////////////////////////////////////////////////////

	CString olVafrd,olVafrt,olVatod,olVatot;
	CTime olTmpTimeFr,olTmpTimeTo;
	m_VAFRD.GetWindowText(olVafrd);
	m_VAFRT.GetWindowText(olVafrt);
	m_VATOD.GetWindowText(olVatod);
	m_VATOT.GetWindowText(olVatot);

	if(m_VAFRD.GetStatus() == true && m_VAFRT.GetStatus() == true && m_VATOD.GetStatus() == true && m_VATOT.GetStatus() == true)
	{
		olTmpTimeFr = DateHourStringToDate(olVafrd,olVafrt);
		olTmpTimeTo = DateHourStringToDate(olVatod,olVatot);
		if(olTmpTimeTo != -1 && (olTmpTimeTo < olTmpTimeFr || olTmpTimeFr == -1))
		{
			ilStatus = false;
			olErrorText += LoadStg(IDS_STRING236);
		}
	}

	if(m_DAYA1.GetCheck()==0&&m_DAYA2.GetCheck()==0&&m_DAYA3.GetCheck()==0&&m_DAYA4.GetCheck()==0&&m_DAYA5.GetCheck()==0&&m_DAYA6.GetCheck()==0&&m_DAYA7.GetCheck()==0)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING418);
	}
	if(m_DAYD1.GetCheck()==0&&m_DAYD2.GetCheck()==0&&m_DAYD3.GetCheck()==0&&m_DAYD4.GetCheck()==0&&m_DAYD5.GetCheck()==0&&m_DAYD6.GetCheck()==0&&m_DAYD7.GetCheck()==0)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING419);
	}

	//Konsistenz-Check//
	if(ilStatus == true)
	{
		CString olActm,olAct3;
		char clWhere[100];
		CCSPtrArray<GRNDATA> olGrnCPA;


		if(m_ACTM.GetWindowTextLength() != 0)
		{
			m_ACTM.GetWindowText(olActm);
			sprintf(clWhere,"WHERE GRSN='%s' AND TABN='ACT%s'",olActm,ogTableExt);
			if(ogGrnData.ReadSpecialData(&olGrnCPA,clWhere,"URNO",false) == false)
			{
				ilStatus = false;
				olErrorText += LoadStg(IDS_STRING29);
			}
			else
			{
				pomStr->Actm = olGrnCPA[0].Urno;
			}
			olGrnCPA.DeleteAll();
		}
		else
		{
			pomStr->Actm = 0;
		}
		
		if(m_ACT3.GetWindowTextLength() != 0)
		{
			m_ACT3.GetWindowText(olAct3);
			sprintf(clWhere,"WHERE ACT3='%s'",olAct3);
			if(ogACTData.ReadSpecialData(NULL,clWhere,"ACT3",false) == false)
			{
				ilStatus = false;
				olErrorText += LoadStg(IDS_STRING32);
			}
		}
	}

	///////////////////////////////////////////////////////////////////
	AfxGetApp()->DoWaitCursor(-1);
	if (ilStatus == true)
	{
		m_ACT3.GetWindowText(pomStr->Act3,4);
		m_FLCA.GetWindowText(pomStr->Flca,4);
		m_FLNA.GetWindowText(pomStr->Flna,6);
		m_FLSA.GetWindowText(pomStr->Flsa,2);
		m_FLCD.GetWindowText(pomStr->Flcd,4);
		m_FLND.GetWindowText(pomStr->Flnd,6);
		m_FLSD.GetWindowText(pomStr->Flsd,2);

		CString olDaya;
		if(m_DAYA1.GetCheck()==1) olDaya  = "1";
		if(m_DAYA2.GetCheck()==1) olDaya += "2";
		if(m_DAYA3.GetCheck()==1) olDaya += "3";
		if(m_DAYA4.GetCheck()==1) olDaya += "4";
		if(m_DAYA5.GetCheck()==1) olDaya += "5";
		if(m_DAYA6.GetCheck()==1) olDaya += "6";
		if(m_DAYA7.GetCheck()==1) olDaya += "7";
		strcpy(pomStr->Daya,olDaya);
		CString olDayd;
		if(m_DAYD1.GetCheck()==1) olDayd  = "1";
		if(m_DAYD2.GetCheck()==1) olDayd += "2";
		if(m_DAYD3.GetCheck()==1) olDayd += "3";
		if(m_DAYD4.GetCheck()==1) olDayd += "4";
		if(m_DAYD5.GetCheck()==1) olDayd += "5";
		if(m_DAYD6.GetCheck()==1) olDayd += "6";
		if(m_DAYD7.GetCheck()==1) olDayd += "7";
		strcpy(pomStr->Dayd,olDayd);

		pomStr->Vafr = DateHourStringToDate(olVafrd,olVafrt);
		pomStr->Vato = DateHourStringToDate(olVatod,olVatot);


		CDialog::OnOK();
	}
	else
	{
		Beep(440,70);
		MessageBox(olErrorText,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
		m_ACT3.SetFocus();
		m_ACT3.SetSel(0,-1);
  }
}

//----------------------------------------------------------------------------------------

void StandardverkettungenDlg::OnDaya17() 
{
	if(m_DAYAT.GetCheck() == 1)
	{
		m_DAYAT.SetCheck(0);
	}
}

//----------------------------------------------------------------------------------------

void StandardverkettungenDlg::OnDayd17() 
{
	if(m_DAYDT.GetCheck() == 1)
	{
		m_DAYDT.SetCheck(0);
	}
}

//----------------------------------------------------------------------------------------

void StandardverkettungenDlg::OnDayaT() 
{
	if(m_DAYAT.GetCheck() == 1)
	{
		m_DAYA1.SetCheck(1);
		m_DAYA2.SetCheck(1);
		m_DAYA3.SetCheck(1);
		m_DAYA4.SetCheck(1);
		m_DAYA5.SetCheck(1);
		m_DAYA6.SetCheck(1);
		m_DAYA7.SetCheck(1);
	}
	if(m_DAYAT.GetCheck() == 0)
	{
		m_DAYA1.SetCheck(0);
		m_DAYA2.SetCheck(0);
		m_DAYA3.SetCheck(0);
		m_DAYA4.SetCheck(0);
		m_DAYA5.SetCheck(0);
		m_DAYA6.SetCheck(0);
		m_DAYA7.SetCheck(0);
	}
}

//----------------------------------------------------------------------------------------

void StandardverkettungenDlg::OnDaydT() 
{
	if(m_DAYDT.GetCheck() == 1)
	{
		m_DAYD1.SetCheck(1);
		m_DAYD2.SetCheck(1);
		m_DAYD3.SetCheck(1);
		m_DAYD4.SetCheck(1);
		m_DAYD5.SetCheck(1);
		m_DAYD6.SetCheck(1);
		m_DAYD7.SetCheck(1);
	}
	if(m_DAYDT.GetCheck() == 0)
	{
		m_DAYD1.SetCheck(0);
		m_DAYD2.SetCheck(0);
		m_DAYD3.SetCheck(0);
		m_DAYD4.SetCheck(0);
		m_DAYD5.SetCheck(0);
		m_DAYD6.SetCheck(0);
		m_DAYD7.SetCheck(0);
	}
}

//----------------------------------------------------------------------------------------

LONG StandardverkettungenDlg::OnKillfocus(UINT lParam, LONG wParam) 
{
	if(lParam == (UINT)m_ACT3.imID && m_ACT3.GetWindowTextLength() != 0) m_ACTM.SetInitText("");
	else if(lParam == (UINT)m_ACTM.imID && m_ACTM.GetWindowTextLength() != 0) m_ACT3.SetInitText("");
	return 0L;
}

//----------------------------------------------------------------------------------------

