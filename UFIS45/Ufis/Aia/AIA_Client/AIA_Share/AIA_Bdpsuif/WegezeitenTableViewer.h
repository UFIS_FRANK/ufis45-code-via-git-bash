#ifndef __WEGEZEITENTABLEVIEWER_H__
#define __WEGEZEITENTABLEVIEWER_H__

#include "stdafx.h"
#include "CedaWayData.h"
#include "CCSTable.h"
#include "CViewer.h"
#include "CCSPrint.h"
#include "CedaGrnData.h"


struct WEGEZEITENTABLE_LINEDATA
{
	long 		 Urno; 	// Eindeutige Datensatz-Nr.
	CString 	 Pobe; 	// Ausgangsposition
	CString 	 Tybe; 	// von P/G/S
	CString 	 Poen; 	// Zielposition
	CString 	 Tyen; 	// nach P/G/S
	CString 	 Ttgo; 	// Wegezeit
	CString 	 Wtgo; 	// Wegstrecke
	CString 	 Wtyp; 	// Wegetyp (z.B. Schlepp, Gang zu Fu�, Ger�t)
	CString 	 Home; 	// Wegetyp (z.B. Schlepp, Gang zu Fu�, Ger�t)
};

/////////////////////////////////////////////////////////////////////////////
// WegezeitenTableViewer

class WegezeitenTableViewer : public CViewer
{
// Constructions
public:
    WegezeitenTableViewer(CCSPtrArray<WAYDATA> *popData);
    ~WegezeitenTableViewer();

    void Attach(CCSTable *popAttachWnd);
    virtual void ChangeViewTo(const char *pcpViewName);
// Internal data processing routines
private:
	bool IsPassFilter(WAYDATA *prpWegezeiten);
	int CompareWegezeiten(WEGEZEITENTABLE_LINEDATA *prpWegezeiten1, WEGEZEITENTABLE_LINEDATA *prpWegezeiten2);
    void MakeLines();
	void MakeLine(WAYDATA *prpWegezeiten);
	bool FindLine(long lpUrno, int &rilLineno);
// Operations
public:
	void DeleteAll();
	void CreateLine(WEGEZEITENTABLE_LINEDATA *prpWegezeiten);
	void DeleteLine(int ipLineno);
// Window refreshing routines
public:
	void UpdateDisplay();
	CString Format(WEGEZEITENTABLE_LINEDATA *prpLine);
	void ProcessWegezeitenChange(WAYDATA *prpWegezeiten);
	void ProcessWegezeitenDelete(WAYDATA *prpWegezeiten);
	bool FindWegezeiten(char *prpWegezeitenKeya, char *prpWegezeitenKeyd, int& ilItem);
// Attributes used for filtering condition
private:
	bool bmIsFromSearch;
// Attributes
private:
    CCSTable *pomWegezeitenTable;
	CCSPtrArray<WAYDATA> *pomData;
// Methods which handle changes (from Data Distributor)
public:
    CCSPtrArray<WEGEZEITENTABLE_LINEDATA> omLines;
///////
	CCSPtrArray<TABLE_HEADER_COLUMN> omHeaderDataArray;
//Print 
	void PrintTableView();
	bool PrintTableLine(WEGEZEITENTABLE_LINEDATA *prpLine,bool bpLastLine);
	bool PrintTableHeader();
	CCSPrint *pomPrint;
	//CBitmap omBitmap;
	CString omTableName;
	CMapPtrToPtr omGrnUrnoMap;
	CCSPtrArray<GRNDATA> omGrnCPA;

};

#endif //__WEGEZEITENTABLEVIEWER_H__
