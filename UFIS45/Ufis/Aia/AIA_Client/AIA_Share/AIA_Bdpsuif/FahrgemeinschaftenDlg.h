#if !defined(AFX_FAHRGEMEINSCHAFTENDLG_H__AB36A832_7540_11D1_B430_0000B45A33F5__INCLUDED_)
#define AFX_FAHRGEMEINSCHAFTENDLG_H__AB36A832_7540_11D1_B430_0000B45A33F5__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// FahrgemeinschaftenDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// FahrgemeinschaftenDlg dialog
#include "CedaSphData.h"
#include "CCSEdit.h"
#include "CedaTeaData.h"

class FahrgemeinschaftenDlg : public CDialog
{
// Construction
public:
	FahrgemeinschaftenDlg(TEADATA *popTea, CWnd* pParent = NULL);   // standard constructor

	TEADATA *pomTea;
// Dialog Data
	//{{AFX_DATA(FahrgemeinschaftenDlg)
	enum { IDD = IDD_FAHRGEMEINSCHAFTENDLG };
	CButton	m_OK;
	CCSEdit	m_CDATD;
	CCSEdit	m_CDATT;
	CCSEdit	m_FGMC;
	CCSEdit	m_FGMN;
	CCSEdit	m_LSTUD;
	CCSEdit	m_LSTUT;
	CCSEdit	m_REMA;
	CCSEdit	m_USEC;
	CCSEdit	m_USEU;
	CString m_Caption;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(FahrgemeinschaftenDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(FahrgemeinschaftenDlg)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_FAHRGEMEINSCHAFTENDLG_H__AB36A832_7540_11D1_B430_0000B45A33F5__INCLUDED_)
