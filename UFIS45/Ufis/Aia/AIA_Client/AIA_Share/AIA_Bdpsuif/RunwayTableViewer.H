#ifndef __RUNWAYTABLEVIEWER_H__
#define __RUNWAYTABLEVIEWER_H__

#include "stdafx.h"
#include "CedaRWYData.h"
#include "CCSTable.h"
#include "CViewer.h"
#include "CCSPrint.h"

struct RUNWAYTABLE_LINEDATA
{
	long	Urno;
	CString	Rnam;
	CString	Rnum;
	CString	RtypS;
	CString	RtypL;
	CString	Nafr;
	CString	Nato;
	CString	Resn;
	CString	Vafr;
	CString	Vato;
	CString	Home;
};

/////////////////////////////////////////////////////////////////////////////
// RunwayTableViewer

class RunwayTableViewer : public CViewer
{
// Constructions
public:
    RunwayTableViewer(CCSPtrArray<RWYDATA> *popData);
    ~RunwayTableViewer();

    void Attach(CCSTable *popAttachWnd);
    virtual void ChangeViewTo(const char *pcpViewName);
// Internal data processing routines
private:
	BOOL IsPassFilter(RWYDATA *prpRunway);
	int CompareRunway(RUNWAYTABLE_LINEDATA *prpRunway1, RUNWAYTABLE_LINEDATA *prpRunway2);
    void MakeLines();
	void MakeLine(RWYDATA *prpRunway);
	BOOL FindLine(long lpUrno, int &rilLineno);
// Operations
public:
	void DeleteAll();
	void CreateLine(RUNWAYTABLE_LINEDATA *prpRunway);
	void DeleteLine(int ipLineno);
// Window refreshing routines
public:
	void UpdateDisplay();
	CString Format(RUNWAYTABLE_LINEDATA *prpLine);
	void ProcessRunwayChange(RWYDATA *prpRunway);
	void ProcessRunwayDelete(RWYDATA *prpRunway);
	BOOL FindRunway(char *prpRunwayKeya, char *prpRunwayKeyd, int& ilItem);
// Attributes used for filtering condition
private:
	BOOL bmIsFromSearch;
// Attributes
private:
    CCSTable *pomRunwayTable;
	CCSPtrArray<RWYDATA> *pomData;
// Methods which handle changes (from Data Distributor)
public:
    CCSPtrArray<RUNWAYTABLE_LINEDATA> omLines;
///////
	CCSPtrArray<TABLE_HEADER_COLUMN> omHeaderDataArray;
//Print 
	void PrintTableView();
	bool PrintTableLine(RUNWAYTABLE_LINEDATA *prpLine,bool bpLastLine);
	bool PrintTableHeader();
	CCSPrint *pomPrint;
	//CBitmap omBitmap;
	CString omTableName;

};

#endif //__RUNWAYTABLEVIEWER_H__
