// CedaACTData.h

#ifndef __CEDAACTDATA__
#define __CEDAACTDATA__
 
#include "stdafx.h"
#include "basicdata.h"

//---------------------------------------------------------------------------------------------------------
// Record structure declaration

struct ACTDATA 
{
	long	 Urno; 		// Eindeutige Datensatz-Nr.
	CTime	 Cdat;		// Erstellungsdatum
	char 	 Usec[34]; 	// Anwender (Ersteller)
	CTime	 Lstu;	 	// Datum letzte �nderung
	char 	 Prfl[3]; 	// Protokollierungskennzeichen
	char 	 Useu[34]; 	// Anwender (letzte �nderung)
	char 	 Act3[5]; 	// Flugzeug-Typ 3-Letter Code (IATA)
	char 	 Act5[7]; 	// Flugzeug-Typ 5-Letter Code (ICAO)
	char 	 Acti[7]; 	// Modifizierter ICAO Code
	char 	 Acfn[32]; 	// Flugzeug-Bezeichnung
	char 	 Acws[7]; 	// Spannweite
	char 	 Acle[7]; 	// L�nge
	char 	 Ache[7]; 	// H�he
	char 	 AcwsHelp[7]; 	// Spannweite
	char 	 AcleHelp[7]; 	// L�nge
	char 	 AcheHelp[7]; 	// H�he
	char 	 Seat[5]; 	// Anzahl Sitzpl�tze
	char 	 Seaf[5]; 	// Anzahl Sitzpl�tze
	char 	 Seab[5]; 	// Anzahl Sitzpl�tze
	char 	 Seae[5]; 	// Anzahl Sitzpl�tze
	char 	 Enty[7]; 	// Antriebsart
	char 	 Enno[3]; 	// Anzahl Triebwerke
	CTime	 Vafr;	 	// G�ltig von
	CTime	 Vato;	 	// G�ltig bis
	char 	 Nads[3]; 	// kein Andocksystem verwenden
	char 	 Afmc[6]; 	// Anzahl Sitzpl�tze
	char 	 Altc[4]; 	// Anzahl Sitzpl�tze
	char 	 Modc[7]; 	// Anzahl Sitzpl�tze
	char	 Acbt[2];
	char	 Ming[5];	//*** 04.10.99 SHA ***
						//*** Minimum Ground Time (REQ. FROM SHANGHAI) ***


	//DataCreated by this class
	int		 IsChanged;

	ACTDATA(void)
	{ 
		memset(this,'\0',sizeof(*this));
		Cdat=-1;
		Lstu=-1;
		Vafr=-1;
		Vato=-1;
	}

}; // end ACTDataStrukt

//---------------------------------------------------------------------------------------------------------
// Class declaratino

class CedaACTData: public CCSCedaData
{
// Attributes
public:
    CMapPtrToPtr omUrnoMap;

    CCSPtrArray<ACTDATA> omData;

// Operations
public:
    CedaACTData();
	~CedaACTData();
	
	virtual void GetDataInfo(CStringArray &ropFields, CStringArray &ropDesription, CStringArray &ropType);
	void Register(void);
	void ClearAll(bool bpWithRegistration = true);

    bool Read(char *pcpWhere = NULL);
	bool ReadSpecialData(CCSPtrArray<ACTDATA> *popAct,char *pspWhere,char *pspFieldList,bool ipSYS = true);
	bool InsertACT(ACTDATA *prpACT,BOOL bpSendDdx = TRUE);
	bool InsertACTInternal(ACTDATA *prpACT);
	bool UpdateACT(ACTDATA *prpACT,BOOL bpSendDdx = TRUE);
	bool UpdateACTInternal(ACTDATA *prpACT);
	bool DeleteACT(long lpUrno);
	bool DeleteACTInternal(ACTDATA *prpACT);
	ACTDATA  *GetACTByUrno(long lpUrno);
	bool SaveACT(ACTDATA *prpACT);
	char pcmACTFieldList[2048];
	void ProcessACTBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);
	void  CutZeros(ACTDATA *prpACT);
	void  AddZeros(ACTDATA *prpACT);


	// Private methods
private:
    void PrepareACTData(ACTDATA *prpACTData);
};
void	EliminateColons(char *);

//---------------------------------------------------------------------------------------------------------

#endif //__CEDAACTDATA__
