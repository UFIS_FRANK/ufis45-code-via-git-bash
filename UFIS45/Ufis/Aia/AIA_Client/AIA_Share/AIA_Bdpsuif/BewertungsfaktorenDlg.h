#if !defined(AFX_BEWERTUNGSFAKTORENDLG_H__780A8AF4_64B4_11D1_B3D1_0000C016B067__INCLUDED_)
#define AFX_BEWERTUNGSFAKTORENDLG_H__780A8AF4_64B4_11D1_B3D1_0000C016B067__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// BewertungsfaktorenDlg.h : header file
//

#include "CCSEdit.h"
#include "CedaAsfData.h"
/////////////////////////////////////////////////////////////////////////////
// BewertungsfaktorenDlg dialog

class BewertungsfaktorenDlg : public CDialog
{
// Construction
public:
	BewertungsfaktorenDlg(ASFDATA *popAsf, CWnd* pParent = NULL);   // standard constructor

	ASFDATA *pomAsf;
// Dialog Data
	//{{AFX_DATA(BewertungsfaktorenDlg)
	enum { IDD = IDD_BEWERTUNGSFAKTOREN };
	CButton	m_OK;
	CCSEdit	m_BEWC;
	CCSEdit	m_BEWF;
	CCSEdit	m_BEWN;
	CCSEdit	m_REMA;
	CCSEdit	m_USEC;
	CCSEdit	m_USEU;
	CString m_Caption;
	CCSEdit	m_CDATD;
	CCSEdit	m_CDATT;
	CCSEdit	m_LSTUD;
	CCSEdit	m_LSTUT;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(BewertungsfaktorenDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(BewertungsfaktorenDlg)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_BEWERTUNGSFAKTORENDLG_H__780A8AF4_64B4_11D1_B3D1_0000C016B067__INCLUDED_)
