// AircraftFamTableViewer.cpp 
//

#include "stdafx.h"
#include "AircraftFamTableViewer.h"
#include "CcsDdx.h"
#include "resource.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif


// Local function prototype
static void AircraftFamTableCf(void *popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

//-----------------------------------------------------------------------------------------------
// AircraftFamTableViewer
//

AircraftFamTableViewer::AircraftFamTableViewer(CCSPtrArray<AFMDATA> *popData)
{
	pomData = popData;

	bmIsFromSearch = FALSE;
    pomAircraftFamTable = NULL;
    ogDdx.Register(this, AFM_CHANGE, CString("AircraftFamTableViewer"), CString("AircraftFam Update"), AircraftFamTableCf);
    ogDdx.Register(this, AFM_NEW,    CString("AircraftFamTableViewer"), CString("AircraftFam New"),    AircraftFamTableCf);
    ogDdx.Register(this, AFM_DELETE, CString("AircraftFamTableViewer"), CString("AircraftFam Delete"), AircraftFamTableCf);
}

//-----------------------------------------------------------------------------------------------

AircraftFamTableViewer::~AircraftFamTableViewer()
{
	ogDdx.UnRegister(this, NOTUSED);
    DeleteAll();
	omHeaderDataArray.DeleteAll();
}

//-----------------------------------------------------------------------------------------------

void AircraftFamTableViewer::Attach(CCSTable *popTable)
{
    pomAircraftFamTable = popTable;
}

//-----------------------------------------------------------------------------------------------

void AircraftFamTableViewer::ChangeViewTo(const char *pcpViewName)
{
    DeleteAll();    
    MakeLines();
	UpdateDisplay();
}

//-----------------------------------------------------------------------------------------------

void AircraftFamTableViewer::MakeLines()
{
	int ilAircraftFamCount = pomData->GetSize();
	for (int ilLc = 0; ilLc < ilAircraftFamCount; ilLc++)
	{
		AFMDATA *prlAircraftFamData = &pomData->GetAt(ilLc);
		MakeLine(prlAircraftFamData);
	}
}

//-----------------------------------------------------------------------------------------------

void AircraftFamTableViewer::MakeLine(AFMDATA *prpAircraftFam)
{

    //if( !IsPassFilter(prpAircraftFam)) return;

    // Update viewer data for this shift record
    AIRCRAFTFAMTABLE_LINEDATA rlAircraftFam;

	rlAircraftFam.Urno = prpAircraftFam->Urno;
	rlAircraftFam.Afmc = prpAircraftFam->Afmc;
	rlAircraftFam.Anam = prpAircraftFam->Anam;

	CreateLine(&rlAircraftFam);
}

//-----------------------------------------------------------------------------------------------

void AircraftFamTableViewer::CreateLine(AIRCRAFTFAMTABLE_LINEDATA *prpAircraftFam)
{
    int ilLineCount = omLines.GetSize();
    for (int ilLineno = ilLineCount; ilLineno > 0; ilLineno--)
        if (CompareAircraftFam(prpAircraftFam, &omLines[ilLineno-1]) >= 0)
	        break;							 // should be inserted after Lines[ilLineno-1]
	AIRCRAFTFAMTABLE_LINEDATA rlAircraftFam;
	rlAircraftFam = *prpAircraftFam;
    omLines.NewAt(ilLineno, rlAircraftFam);
}

//-----------------------------------------------------------------------------------------------
// UpdateDisplay: Load data selected by filter conditions to the display by using "omTable"

void AircraftFamTableViewer::UpdateDisplay()
{
	int ilLines = omLines.GetSize();
	const int ilColumns = 1;	//<===== Spaltenanzahl setzen (von 0 ab)
	omHeaderDataArray.DeleteAll();

	pomAircraftFamTable->SetShowSelection(TRUE);
	pomAircraftFamTable->ResetContent();

	TABLE_HEADER_COLUMN rlHeader;
	rlHeader.Alignment = COLALIGN_LEFT;
	rlHeader.Font = &ogCourier_Regular_10;

	int ilPos = 1;
	rlHeader.Length = 42; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING641),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 332; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING641),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
/*	rlHeader.Length = 498; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING155),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
*/	pomAircraftFamTable->SetHeaderFields(omHeaderDataArray);

	pomAircraftFamTable->SetDefaultSeparator();
	pomAircraftFamTable->SetTableEditable(FALSE);

	TABLE_COLUMN rlColumnData;
	rlColumnData.Font = &ogCourier_Regular_10;
	rlColumnData.BkColor = WHITE;
	rlColumnData.Alignment = COLALIGN_LEFT;
	
    for (int ilLineNo = 0; ilLineNo < ilLines; ilLineNo++)
	{
		CCSPtrArray<TABLE_COLUMN> olColList;

		rlColumnData.Text = omLines[ilLineNo].Afmc;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Anam;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		pomAircraftFamTable->AddTextLine(olColList,(void*)NULL);
		olColList.DeleteAll();
	}
	pomAircraftFamTable->DisplayTable();
}

//-----------------------------------------------------------------------------------------------

CString AircraftFamTableViewer::Format(AIRCRAFTFAMTABLE_LINEDATA *prpLine)
{
    CString s;
	// ???
    return s;
}

//-----------------------------------------------------------------------------------------------

bool AircraftFamTableViewer::FindAircraftFam(char *pcpAircraftFamKeya, char *pcpAircraftFamKeyd, int& ilItem)
{
	int ilCount = omLines.GetSize();
    for (ilItem = 0; ilItem < ilCount; ilItem++)
    {
/*		if ( (omLines[ilItem].Keya == pcpAircraftFamKeya) &&
			 (omLines[ilItem].Keyd == pcpAircraftFamKeyd)    )
			return TRUE;*/
	}
	return false;
}

//-----------------------------------------------------------------------------------------------

static void AircraftFamTableCf(void *popInstance, int ipDDXType,
    void *vpDataPointer, CString &ropInstanceName)
{
    AircraftFamTableViewer *polViewer = (AircraftFamTableViewer *)popInstance;
    if (ipDDXType == AFM_CHANGE || ipDDXType == AFM_NEW) polViewer->ProcessAircraftFamChange((AFMDATA *)vpDataPointer);
    if (ipDDXType == AFM_DELETE) polViewer->ProcessAircraftFamDelete((AFMDATA *)vpDataPointer);
} 


//-----------------------------------------------------------------------------------------------

void AircraftFamTableViewer::ProcessAircraftFamChange(AFMDATA *prpAircraftFam)
{
	int ilItem = 0;
	int ilColumnNo = 0;
	TABLE_COLUMN rlColumn;
	CCSPtrArray <TABLE_COLUMN> olLine;

	rlColumn.Lineno = ilItem;
	rlColumn.VerticalSeparator = SEPA_NONE;
	rlColumn.SeparatorType = SEPA_NONE;
	rlColumn.BkColor = WHITE;
	rlColumn.Font = &ogCourier_Regular_10;
	rlColumn.Alignment = COLALIGN_LEFT;

	rlColumn.Text = prpAircraftFam->Afmc;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpAircraftFam->Anam;
	olLine.NewAt(olLine.GetSize(), rlColumn);

	
	if (FindLine(prpAircraftFam->Urno, ilItem))
	{
        AIRCRAFTFAMTABLE_LINEDATA *prlLine = &omLines[ilItem];

		prlLine->Urno = prpAircraftFam->Urno;
		prlLine->Afmc = prpAircraftFam->Afmc;
		prlLine->Anam = prpAircraftFam->Anam;


		pomAircraftFamTable->ChangeTextLine(ilItem, &olLine, (void *)prlLine);
		pomAircraftFamTable->DisplayTable();
	}
	else
	{
		MakeLine(prpAircraftFam);
		if (FindLine(prpAircraftFam->Urno, ilItem))
		{
	        AIRCRAFTFAMTABLE_LINEDATA *prlLine = &omLines[ilItem];
			if(prlLine != NULL)
			{
				pomAircraftFamTable->AddTextLine(olLine, (void *)prlLine);
				pomAircraftFamTable->DisplayTable();
			}
		}
	}
	olLine.DeleteAll();
}

//-----------------------------------------------------------------------------------------------

void AircraftFamTableViewer::ProcessAircraftFamDelete(AFMDATA *prpAircraftFam)
{
	int ilItem;
	if (FindLine(prpAircraftFam->Urno, ilItem))
	{
		DeleteLine(ilItem);
		pomAircraftFamTable->DeleteTextLine(ilItem);
		pomAircraftFamTable->DisplayTable();
	}
}

//-----------------------------------------------------------------------------------------------

bool AircraftFamTableViewer::IsPassFilter(AFMDATA *prpAircraftFam)
{

	return true;
}

//-----------------------------------------------------------------------------------------------

int AircraftFamTableViewer::CompareAircraftFam(AIRCRAFTFAMTABLE_LINEDATA *prpAircraftFam1, AIRCRAFTFAMTABLE_LINEDATA *prpAircraftFam2)
{
/*	int ilCase=0;
	int	ilCompareResult;
	if(prpAircraftFam1->Tifd == prpAircraftFam2->Tifd)
	{
		ilCompareResult= 0;
	}
	else
	{
		if(prpAircraftFam1->Tifd > prpAircraftFam2->Tifd)
		{
			ilCompareResult = 1;
		}
		else
		{
			ilCompareResult = -1;
		}
	}
	return ilCompareResult;
*/  
	return 0;
}

//-----------------------------------------------------------------------------------------------

void AircraftFamTableViewer::DeleteLine(int ipLineno)
{
	omLines.DeleteAt(ipLineno);
}

//-----------------------------------------------------------------------------------------------

bool AircraftFamTableViewer::FindLine(long lpUrno, int &rilLineno)
{
    for (rilLineno = 0; rilLineno < omLines.GetSize(); rilLineno++)
	{
        if (omLines[rilLineno].Urno == lpUrno)
            return true;
	}
    return false;
}
//-----------------------------------------------------------------------------------------------

void AircraftFamTableViewer::DeleteAll()
{
    while (omLines.GetSize() > 0)
    DeleteLine(0);
}

//-----------------------------------------------------------------------------------------------
// Printing Routines
//-----------------------------------------------------------------------------------------------

void AircraftFamTableViewer::PrintTableView()
{
	CString olFooter1,olFooter2;
	omTableName = LoadStg(IDS_STRING618);
	int ilOrientation = PRINT_LANDSCAPE;//  PRINT_PORTRAET  PRINT_LANDSCAPE
	pomPrint = new CCSPrint((CWnd*)this,ilOrientation,45);// FirstLine(P=200,L=200),LeftOffset(P=200,L=50)
	//pomPrint->imMaxLines = 38;  // (P=57,L=38)
	//omBitmap.LoadBitmap(IDB_HAJLOGO);
	//pomPrint->SetBitmaps(&omBitmap,&omBitmap);
	if (pomPrint != NULL)
	{
		if (pomPrint->InitializePrinter(ilOrientation) == TRUE)
		{
			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
			pomPrint->imLineNo = pomPrint->imMaxLines + 1;
			DOCINFO	rlDocInfo;
			memset(&rlDocInfo, 0, sizeof(DOCINFO));
			rlDocInfo.cbSize = sizeof( DOCINFO );
			rlDocInfo.lpszDocName = omTableName;	
			pomPrint->omCdc.StartDoc( &rlDocInfo );
			pomPrint->imPageNo = 0;
			int ilLines = omLines.GetSize();
			olFooter1.Format("%d %s",ilLines,omTableName);
			for(int i = 0; i < ilLines; i++ ) 
			{
				if(pomPrint->imLineNo >= pomPrint->imMaxLines)
				{
					if(pomPrint->imPageNo > 0)
					{
						olFooter2.Format("%s: %d",LoadStg(IDS_STRING222),pomPrint->imPageNo);
						pomPrint->PrintUIFFooter(olFooter1,"",olFooter2);
						pomPrint->omCdc.EndPage();
					}
					PrintTableHeader();
				}
				if(pomPrint->imLineNo == (pomPrint->imMaxLines-1) || i == (ilLines-1))
				{
					PrintTableLine(&omLines[i],true);
				}
				else
				{
					PrintTableLine(&omLines[i],false);
				}
			}
			olFooter2.Format("%s: %d",LoadStg(IDS_STRING222),pomPrint->imPageNo);
			pomPrint->PrintUIFFooter(olFooter1,"",olFooter2);
			pomPrint->omCdc.EndPage();
			pomPrint->omCdc.EndDoc();
		}
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
	delete pomPrint;
	//omBitmap.DeleteObject();
	pomPrint = NULL;
	}
}

//-----------------------------------------------------------------------------------------------

bool AircraftFamTableViewer::PrintTableHeader()
{
	pomPrint->omCdc.StartPage();
	pomPrint->imPageNo++;
	pomPrint->imLineNo = 0;

	pomPrint->PrintUIFHeader(omTableName,CString(CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M")),pomPrint->imFirstLine-10);

	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;

	rlElement.Alignment   = PRINT_LEFT;
	rlElement.FrameLeft   = PRINT_NOFRAME;
	rlElement.FrameRight  = PRINT_NOFRAME;
	rlElement.FrameTop    = PRINT_NOFRAME;
	rlElement.FrameBottom = PRINT_FRAMEMEDIUM;
	rlElement.pFont       = &pomPrint->ogCourierNew_Bold_8;

	int ilSize = omHeaderDataArray.GetSize();
	for(int i=0;i<ilSize;i++)
	{
		//if(i!=1&&i!=2)// <= setzen, wenn Spalten nicht angezeigt werden sollen 
		{
			rlElement.Length = (int)(omHeaderDataArray[i].Length*dgCCSPrintFactor);
			if(rlElement.Length < igCCSPrintMinLength)rlElement.Length+=igCCSPrintMoreLength; 
			rlElement.Text   = omHeaderDataArray[i].Text;
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
		}
	}
	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();


	return true;
}

//-----------------------------------------------------------------------------------------------

bool AircraftFamTableViewer::PrintTableLine(AIRCRAFTFAMTABLE_LINEDATA *prpLine,bool bpLastLine)
{
	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;
	rlElement.pFont      = &pomPrint->ogCourierNew_Regular_8;
	int ilSize = omHeaderDataArray.GetSize();
	for(int i=0;i<ilSize;i++)
	{
		//if(i!=1&&i!=1)// <= setzen, wenn Spalten nicht angezeigt werden sollen 
		{
			rlElement.Length = (int)(omHeaderDataArray[i].Length*dgCCSPrintFactor);
			if(rlElement.Length < igCCSPrintMinLength)rlElement.Length+=igCCSPrintMoreLength; 
			rlElement.Alignment  = PRINT_LEFT;
			rlElement.FrameTop   = PRINT_FRAMETHIN;
			if(bpLastLine == true)
			{
				rlElement.FrameBottom = PRINT_FRAMETHIN;
			}
			else
			{
				rlElement.FrameBottom = PRINT_NOFRAME;
			}
			rlElement.FrameLeft  = PRINT_NOFRAME;
			rlElement.FrameRight = PRINT_NOFRAME;

			switch(i)
			{
			case 0:
				rlElement.FrameLeft  = PRINT_FRAMETHIN;
				rlElement.Text		 = prpLine->Afmc;
				break;
			case 1:
				rlElement.FrameRight = PRINT_FRAMETHIN;
				rlElement.Text		 = prpLine->Anam;
				break;
			}
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
		}
	}
	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();
	return true;
}

//-----------------------------------------------------------------------------------------------
