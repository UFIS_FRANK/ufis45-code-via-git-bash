// ArbeitsgruppenDlg.cpp : implementation file
//

#include "stdafx.h"
#include "bdpsuif.h"
#include "ArbeitsgruppenDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CArbeitsgruppenDlg dialog
//----------------------------------------------------------------------------------------


CArbeitsgruppenDlg::CArbeitsgruppenDlg(WGPDATA *popWgp,CWnd* pParent /*=NULL*/)
	: CDialog(CArbeitsgruppenDlg::IDD, pParent)
{
	pomWgp = popWgp;

	//{{AFX_DATA_INIT(CArbeitsgruppenDlg)
	//}}AFX_DATA_INIT
}


void CArbeitsgruppenDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CArbeitsgruppenDlg)
	DDX_Control(pDX, IDC_PGP, m_PGP);
	DDX_Control(pDX, IDOK, m_OK);
	DDX_Control(pDX, IDC_CDAT_D, m_CDATD);
	DDX_Control(pDX, IDC_CDAT_T, m_CDATT);
	DDX_Control(pDX, IDC_LSTU_D, m_LSTUD);
	DDX_Control(pDX, IDC_LSTU_T, m_LSTUT);
	DDX_Control(pDX, IDC_REMA, m_REMA);
	DDX_Control(pDX, IDC_USEC, m_USEC);
	DDX_Control(pDX, IDC_USEU, m_USEU);
	DDX_Control(pDX, IDC_WGPC, m_WGPC);
	DDX_Control(pDX, IDC_WGPN, m_WGPN);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CArbeitsgruppenDlg, CDialog)
	//{{AFX_MSG_MAP(CArbeitsgruppenDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CArbeitsgruppenDlg message handlers
//----------------------------------------------------------------------------------------

BOOL CArbeitsgruppenDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

	m_Caption = LoadStg(IDS_STRING199) + " <" + m_Caption + ">";
	SetWindowText(m_Caption);
	
	char clStat = ogPrivList.GetStat("ARBEITSGRUPPENDLG.m_OK");
	SetWndStatAll(clStat,m_OK);

	//------------------------------------
	m_CDATD.SetBKColor(SILVER);
	m_CDATD.SetInitText(pomWgp->Cdat.Format("%d.%m.%Y"));
	//m_CDATD.SetSecState(ogPrivList.GetStat("ARBEITSGRUPPENDLG.m_CDAT"));
	// - - - - - - - - - - - - - - - - - -
	m_CDATT.SetBKColor(SILVER);
	m_CDATT.SetInitText(pomWgp->Cdat.Format("%H:%M"));
	//m_CDATT.SetSecState(ogPrivList.GetStat("ARBEITSGRUPPENDLG.m_CDAT"));
	//------------------------------------
	m_LSTUD.SetBKColor(SILVER);
	m_LSTUD.SetInitText(pomWgp->Lstu.Format("%d.%m.%Y"));
	//m_LSTUD.SetSecState(ogPrivList.GetStat("ARBEITSGRUPPENDLG.m_LSTU"));
	// - - - - - - - - - - - - - - - - - -
	m_LSTUT.SetBKColor(SILVER);
	m_LSTUT.SetInitText(pomWgp->Lstu.Format("%H:%M"));
	//m_LSTUT.SetSecState(ogPrivList.GetStat("ARBEITSGRUPPENDLG.m_LSTU"));
	//------------------------------------
	m_USEC.SetBKColor(SILVER);
	m_USEC.SetInitText(pomWgp->Usec);
	//m_USEC.SetSecState(ogPrivList.GetStat("ARBEITSGRUPPENDLG.m_USEC"));
	//------------------------------------
	m_USEU.SetBKColor(SILVER);
	m_USEU.SetInitText(pomWgp->Useu);
	//m_USEU.SetSecState(ogPrivList.GetStat("ARBEITSGRUPPENDLG.m_USEU"));
	//------------------------------------//WHITE,YELLOW,SILVER,RED,LYELLOW
	m_WGPC.SetFormat("x|#x|#x|#x|#x|#");
	m_WGPC.SetTextLimit(1,5);
	m_WGPC.SetBKColor(YELLOW);  
	m_WGPC.SetTextErrColor(RED);
	m_WGPC.SetInitText(pomWgp->Wgpc);
	//m_WGPC.SetSecState(ogPrivList.GetStat("ARBEITSGRUPPENDLG.m_WGPC"));
	//------------------------------------
	m_WGPN.SetTypeToString("X(40)",40,1);
	m_WGPN.SetBKColor(YELLOW);  
	m_WGPN.SetTextErrColor(RED);
	m_WGPN.SetInitText(pomWgp->Wgpn);
	//m_WGPN.SetSecState(ogPrivList.GetStat("ARBEITSGRUPPENDLG.m_WGPN"));
	//------------------------------------
	m_REMA.SetTypeToString("X(60)",60,0);
	m_REMA.SetTextErrColor(RED);
	m_REMA.SetInitText(pomWgp->Rema);
	//m_REMA.SetSecState(ogPrivList.GetStat("ARBEITSGRUPPENDLG.m_REMA"));
	//------------------------------------
	CCSPtrArray <PGPDATA> olPgpData;
	m_PGP.SetBKColor(YELLOW);
	char pclWhere[1024];
	*pclWhere = '\0';
	ogPgpData.ReadSpecialData(&olPgpData, pclWhere, "URNO,PGPC,PGPN", false); // Get all PGP items and display them
	for (int ilLC = 0; ilLC < olPgpData.GetSize(); ilLC++)
	{
		CString olTmp = olPgpData[ilLC].Pgpc + CString(" (") + olPgpData[ilLC].Pgpn + CString(")");
		m_PGP.AddString(olTmp);
	}
	int ilResult = 0;
	if (pomWgp->Pgpu > 0)	
	{
		// There's something in the database that in the end might prove to be an Urno
		for (int ilLC = 0; ilLC < olPgpData.GetSize(); ilLC++)	
		{
			// Try to find it in PgpData
			if (olPgpData[ilLC].Urno == pomWgp->Pgpu)
			{
				//If there's a suiting string in the list: set it
				if (m_PGP.SelectString(-1, olPgpData[ilLC].Pgpc) == CB_ERR)	
				{
					// Setting the item returned CB_ERR: Tell the user and clear the text field
					CString olErrorText = "";
					olErrorText += LoadStg(IDS_STRING117) + CString(" (") + olPgpData[ilLC].Pgpc + CString(")");
					MessageBox(olErrorText);
					m_PGP.Clear();
				}
			}
		}
	}
	else
	{
		//m_PGP.SetCurSel(0);
		m_PGP.Clear();
	}
	//char clStat2 = '1';
	//char clStat1 = ogPrivList.GetStat("ARBEITSGRUPPENDLG.m_PGP");
	//SetWndStatPrio_1(clStat1,clStat2,m_PGP);
	olPgpData.DeleteAll();
	//------------------------------------

	return TRUE;
}

//----------------------------------------------------------------------------------------

void CArbeitsgruppenDlg::OnOK() 
{
	AfxGetApp()->DoWaitCursor(1);
	CString olErrorText;
	bool ilStatus = true;
	if(m_WGPC.GetStatus() == false)
	{
		ilStatus = false;
		if(m_WGPC.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING237) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING237) + ogNotFormat;
		}
	}
	if(m_WGPN.GetStatus() == false)
	{
		ilStatus = false;
		if(m_WGPN.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING229) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING229) + ogNotFormat;
		}
	}
	if(m_REMA.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING238) + ogNotFormat;
	}

	if(m_PGP.GetWindowTextLength() == 0)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING596)+ ogNoData;
	}

	///////////////////////////////////////////////////////////////////
	//Konsistenz-Check//
	if(ilStatus == true)
	{
		CString olText;//
		if(m_WGPC.GetWindowTextLength() != 0)
		{
			CCSPtrArray<WGPDATA> olWgpCPA;
			char clWhere[100];
			m_WGPC.GetWindowText(olText);
			sprintf(clWhere,"WHERE WGPC='%s'",olText);
			if(ogWgpData.ReadSpecialData(&olWgpCPA,clWhere,"URNO",false) == true)
			{
				if(olWgpCPA[0].Urno != pomWgp->Urno)
				{
					ilStatus = false;
					olErrorText += LoadStg(IDS_STRING237) + LoadStg(IDS_EXIST);
				}
			}
			olWgpCPA.DeleteAll();
		}
	}
	///////////////////////////////////////////////////////////////////
	AfxGetApp()->DoWaitCursor(-1);
	if (ilStatus == true)
	{
		m_WGPC.GetWindowText(pomWgp->Wgpc,6);
		m_WGPN.GetWindowText(pomWgp->Wgpn,40);
		
		//wandelt Return in Blank//
		CString olTemp;
		m_REMA.GetWindowText(olTemp);
		for(int i=0;i>-1;)
		{
			i = olTemp.Find("\r\n");
			if(i != -1)
			{
				if(olTemp.GetLength() > (i+2))
					olTemp = olTemp.Left(i) + " " + olTemp.Mid(i+2);
				else
					olTemp = olTemp.Left(i);
			}
		}
		strcpy(pomWgp->Rema,olTemp);
		
		// Get Urno of Planning Group //
		int ilSel = m_PGP.GetCurSel();
		if (ilSel != CB_ERR)
		{
			m_PGP.GetLBText(ilSel, olTemp);
		}
		// take PGPN suffix off string (rest: PGPC)
		int ilPos = olTemp.Find(" (");
		olTemp = olTemp.Left(ilPos);
		olTemp.TrimRight();
		
		// find PGPC in PGP table and retrieve related urno
		char pclWhere[1024];
		sprintf(pclWhere, "WHERE PGPC = '%s'", olTemp);
		CCSPtrArray <PGPDATA> olPgp;
		ogPgpData.ReadSpecialData(&olPgp, pclWhere, "URNO,PGPC,PGPN", false);
		if (olPgp.GetSize() != 0)
		{
			pomWgp->Pgpu = olPgp[0].Urno;
		}
		olPgp.DeleteAll();
		////////////////////////////
		CDialog::OnOK();
	}
	else
	{
		Beep(440,70);
		MessageBox(olErrorText,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
		m_WGPC.SetFocus();
	}
	return;
}

//----------------------------------------------------------------------------------------
