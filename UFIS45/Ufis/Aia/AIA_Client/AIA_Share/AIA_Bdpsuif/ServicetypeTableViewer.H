#ifndef __SERVICETYPETABLEVIEWER_H__
#define __SERVICETYPETABLEVIEWER_H__

#include "stdafx.h"
#include "CedaSTYData.h"
#include "CCSTable.h"
#include "CViewer.h"
#include "CCSPrint.h"


struct SERVICETYPETABLE_LINEDATA
{
	long	Urno;
	CString	Styp;
	CString	Snam;
	CString	Vafr;
	CString	Vato;
};

/////////////////////////////////////////////////////////////////////////////
// ServicetypeTableViewer

class ServicetypeTableViewer : public CViewer
{
// Constructions
public:
    ServicetypeTableViewer(CCSPtrArray<STYDATA> *popData);
    ~ServicetypeTableViewer();

    void Attach(CCSTable *popAttachWnd);
    virtual void ChangeViewTo(const char *pcpViewName);
// Internal data processing routines
private:
	bool IsPassFilter(STYDATA *prpServicetype);
	int CompareServicetype(SERVICETYPETABLE_LINEDATA *prpServicetype1, SERVICETYPETABLE_LINEDATA *prpServicetype2);
    void MakeLines();
	void MakeLine(STYDATA *prpServicetype);
	bool FindLine(long lpUrno, int &rilLineno);
// Operations
public:
	void DeleteAll();
	void CreateLine(SERVICETYPETABLE_LINEDATA *prpServicetype);
	void DeleteLine(int ipLineno);
// Window refreshing routines
public:
	void UpdateDisplay();
	CString Format(SERVICETYPETABLE_LINEDATA *prpLine);
	void ProcessServicetypeChange(STYDATA *prpServicetype);
	void ProcessServicetypeDelete(STYDATA *prpServicetype);
	bool FindServicetype(char *prpServicetypeKeya, char *prpServicetypeKeyd, int& ilItem);
// Attributes used for filtering condition
private:
	bool bmIsFromSearch;
// Attributes
private:
    CCSTable *pomServicetypeTable;
	CCSPtrArray<STYDATA> *pomData;
// Methods which handle changes (from Data Distributor)
public:
    CCSPtrArray<SERVICETYPETABLE_LINEDATA> omLines;
///////
	CCSPtrArray<TABLE_HEADER_COLUMN> omHeaderDataArray;
//Print 
	void PrintTableView();
	bool PrintTableLine(SERVICETYPETABLE_LINEDATA *prpLine,bool bpLastLine);
	bool PrintTableHeader();
	CCSPrint *pomPrint;
	//CBitmap omBitmap;
	CString omTableName;

};

#endif //__SERVICETYPETABLEVIEWER_H__
