// CedaHTYData.h

#ifndef __CEDAHTYDATA__
#define __CEDAHTYDATA__
 
#include "stdafx.h"
#include "basicdata.h"

//---------------------------------------------------------------------------------------------------------
// Record structure declaration

struct HTYDATA 
{
	CTime	 Cdat; 		// Erstellungsdatum
	CTime	 Lstu; 		// Datum letzte �nderung
	char 	 Prfl[3]; 	// Protokollierungskennzeichen
	char 	 Hnam[32]; 	// Abfertigungsart Name
	char 	 Htyp[4]; 	// Abfertigungsart
	long 	 Urno; 		// Eindeutige Datensatz-Nr.
	char 	 Usec[34]; 	// Anwender (Ersteller)
	char 	 Useu[34]; 	// Anwender (letzte �nderung)
	CTime	 Vafr; 		// G�ltig von
	CTime	 Vato; 		// G�ltig bis

	//DataCreated by this class
	int		 IsChanged;

	HTYDATA(void)
	{ 
		memset(this,'\0',sizeof(*this));
		Cdat=-1;
		Lstu=-1;
		Vafr=-1;
		Vato=-1;
	}

}; // end HTYDataStrukt


//---------------------------------------------------------------------------------------------------------
// Class declaratino

class CedaHTYData: public CCSCedaData
{
// Attributes
public:
    CMapPtrToPtr omUrnoMap;

    CCSPtrArray<HTYDATA> omData;

	char pcmHTYFieldList[2048];

// Operations
public:
    CedaHTYData();
	~CedaHTYData();

	virtual void GetDataInfo(CStringArray &ropFields, CStringArray &ropDesription, CStringArray &ropType);
	void Register(void);
	void ClearAll(void);

    bool Read(char *pcpWhere = NULL);
	bool ReadSpecialData(CCSPtrArray<HTYDATA> *popHty,char *pspWhere,char *pspFieldList,bool ipSYS = true);
	bool InsertHTY(HTYDATA *prpHTY,BOOL bpSendDdx = TRUE);
	bool InsertHTYInternal(HTYDATA *prpHTY);
	bool UpdateHTY(HTYDATA *prpHTY,BOOL bpSendDdx = TRUE);
	bool UpdateHTYInternal(HTYDATA *prpHTY);
	bool DeleteHTY(long lpUrno);
	bool DeleteHTYInternal(HTYDATA *prpHTY);
	HTYDATA  *GetHTYByUrno(long lpUrno);
	bool SaveHTY(HTYDATA *prpHTY);
	void ProcessHTYBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

	// Private methods
private:
    void PrepareHTYData(HTYDATA *prpHTYData);
};

//---------------------------------------------------------------------------------------------------------

#endif //__CEDAHTYDATA__
