//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by BDPSUIF.rc
//
#define IDS_STRING726                   726
#define IDS_STRING727                   727
#define IDS_STRING728                   728
#define IDS_STRING729                   729
#define IDS_STRING730                   730
#define IDS_STRING889                   889
#define IDS_STRING992                   992
#define IDS_STRING993                   993
#define IDS_STRING994                   994
#define IDS_STRING995                   995
#define IDS_STRING996                   996
#define IDS_STRING997                   997
#define IDS_STRING998                   998
#define IDS_STRING999                   999
#define IDS_STRING1000                  1000
#define IDS_STRING1001                  1001
#define IDS_STRING1002                  1002
#define IDS_STRING1003                  1003
#define IDS_STRING1004                  1004
#define IDS_STRING1005                  1005
#define IDS_STRING1006                  1006
#define IDS_STRING1007                  1007
#define IDS_STRING1008                  1008
#define IDS_STRING1009                  1009
#define IDS_STRING1010                  1010
#define IDS_STRING1011                  1011
#define IDS_STRING1012                  1012
#define IDS_STRING1013                  1013
#define IDS_STRING1014                  1014
#define IDS_STRING1015                  1015
#define IDS_STRING1016                  1016
#define IDS_STRING1017                  1017
#define IDS_STRING1018                  1018
#define IDS_STRING1019                  1019
#define IDS_STRING1020                  1020
#define IDS_STRING1021                  1021
#define IDS_STRING1022                  1022
#define IDS_STRING1023                  1023
#define IDS_STRING1024                  1024
#define IDS_STRING1025                  1025
#define IDC_TDIS                        1029
#define IDC_TDIW                        1031
#define IDC_SEAF2                       1032
#define IDC_SEAB2                       1033
#define IDC_SEAE2                       1034
#define IDC_MAXF                        1035
#define IDC_BUTTON2                     1239
#define IDC_DELETE                      1239
#define IDC_MXF                         1241
#define IDC_DEFD                        1242
#define IDC_GRID                        1243
#define IDC_ADD                         1244
#define IDC_BLOCKTYPE                   1245
#define IDC_EXCEL                       1246
#define IDC_BLOCKING_BITMAP             1336
#define MID_EXCEL                       32781

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        223
#define _APS_NEXT_COMMAND_VALUE         32782
#define _APS_NEXT_CONTROL_VALUE         1247
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
