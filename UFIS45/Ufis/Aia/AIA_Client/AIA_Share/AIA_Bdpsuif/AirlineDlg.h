#if !defined(AFX_AIRLINEDLG_H__771D42C4_1DEB_11D1_B387_0000C016B067__INCLUDED_)
#define AFX_AIRLINEDLG_H__771D42C4_1DEB_11D1_B387_0000C016B067__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#include "CedaALTData.h"
#include "CCSEdit.h"

//20001108SHA GRID
#include "stgrid.h"
/////////////////////////////////////////////////////////////////////////////
// AirlineDlg dialog

class AirlineDlg : public CDialog
{
// Construction
public:
	AirlineDlg(ALTDATA *popALT,CWnd* pParent = NULL);

// Dialog Data
	//{{AFX_DATA(AirlineDlg)
	enum { IDD = IDD_AIRLINEDLG };
	CButton	m_OK;
	CButton	m_CANCEL;
	CButton	m_CASHK;
	CButton	m_CASHB;
	CButton	m_CASHU;
	CString	m_Caption;
	CCSEdit	m_ALC2;
	CCSEdit	m_ALC3;
	CCSEdit	m_ALFN;
	CCSEdit	m_RPRT;
	CCSEdit	m_WRKO;
	CCSEdit	m_ADMD;
	CCSEdit	m_CDATD;
	CCSEdit	m_CDATT;
	CCSEdit	m_LSTUD;
	CCSEdit	m_LSTUT;
	CCSEdit	m_USEU;
	CCSEdit	m_USEC;
	CCSEdit	m_GRUP;
	CCSEdit	m_VAFRD;
	CCSEdit	m_VAFRT;
	CCSEdit	m_VATOD;
	CCSEdit	m_VATOT;
	CCSEdit	m_TERM;
	CCSEdit	m_AKEY;
	CCSEdit	m_ADD1;
	CCSEdit	m_ADD2;
	CCSEdit	m_ADD3;
	CCSEdit	m_ADD4;
	CCSEdit	m_BASE;
	CCSEdit	m_CONT;
	CCSEdit	m_CTRY;
	CCSEdit	m_EMPS;
	CCSEdit	m_EXEC;
	CCSEdit	m_FOND;
	CCSEdit	m_IANO;
	CCSEdit	m_IATA;
	CCSEdit	m_ICAL;
	CCSEdit	m_ICAO;
	CCSEdit	m_LCOD;
	CCSEdit	m_PHON;
	CCSEdit	m_SITA;
	CCSEdit	m_STXT;
	CCSEdit	m_TFAX;
	CCSEdit	m_TELX;
	CCSEdit	m_WEBS;
	CCSEdit	m_HOME;
	CCSEdit	m_DOIN;
	//}}AFX_DATA


	//20001108SHA GRID
	CSTGrid			*olGrdDel;
	CStringArray	olHag;
	CString			olHagCombo;
	CString			omHtyCombo;
	
	CStringArray		olHagCodes;
	CStringArray		olHagNames;
	CStringArray		olHagPhones;
	CStringArray		olHagFax;

	CStringArray	olDeleteUrno;
	void FillHandlingAgents();
	void FillHandlingTypes();
	void FillAgentData(CString olCode, int iRow);

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(AirlineDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(AirlineDlg)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
    afx_msg LONG OnKillfocus(UINT wParam, LONG lParam);
	afx_msg void OnAdd();
	afx_msg void OnDelete();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

private:

	ALTDATA *pomALT;


};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_AIRLINEDLG_H__771D42C4_1DEB_11D1_B387_0000C016B067__INCLUDED_)
