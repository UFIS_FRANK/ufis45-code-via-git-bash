// FidsCommandDlg.cpp : implementation file
//

#include "stdafx.h"
#include "BDPSUIF.h"
#include "FidsCommandDlg.h"
#include "PrivList.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// FidsCommandDlg dialog


FidsCommandDlg::FidsCommandDlg(FIDDATA *popFID,CWnd* pParent /*=NULL*/) : CDialog(FidsCommandDlg::IDD, pParent)
{
	pomFID = popFID;

	//{{AFX_DATA_INIT(FidsCommandDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void FidsCommandDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(FidsCommandDlg)
	DDX_Control(pDX, IDOK, m_OK);
	DDX_Control(pDX, IDC_CDAT_D, m_CDATD);
	DDX_Control(pDX, IDC_CDAT_T, m_CDATT);
	DDX_Control(pDX, IDC_LSTU_D, m_LSTUD);
	DDX_Control(pDX, IDC_LSTU_T, m_LSTUT);
	DDX_Control(pDX, IDC_USEU,	 m_USEU);
	DDX_Control(pDX, IDC_USEC,	 m_USEC);
	DDX_Control(pDX, IDC_VAFR_D, m_VAFRD);
	DDX_Control(pDX, IDC_VAFR_T, m_VAFRT);
	DDX_Control(pDX, IDC_VATO_D, m_VATOD);
	DDX_Control(pDX, IDC_VATO_T, m_VATOT);
	DDX_Control(pDX, IDC_CODE,	 m_CODE);
	DDX_Control(pDX, IDC_REMI,	 m_REMI);
	DDX_Control(pDX, IDC_REMT,	 m_REMT);
	DDX_Control(pDX, IDC_BLKC,	 m_BLKC);
	DDX_Control(pDX, IDC_BEME,	 m_BEME);
	DDX_Control(pDX, IDC_BEMD,	 m_BEMD);
	DDX_Control(pDX, IDC_BET3,	 m_BET3);
	DDX_Control(pDX, IDC_BET4,	 m_BET4);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(FidsCommandDlg, CDialog)
	//{{AFX_MSG_MAP(FidsCommandDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// FidsCommandDlg message handlers
//----------------------------------------------------------------------------------------

BOOL FidsCommandDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	m_Caption = LoadStg(IDS_STRING172) + " <" + m_Caption + ">";
	SetWindowText(m_Caption);

	char clStat = ogPrivList.GetStat("FIDSCOMMANDDLG.m_OK");
	SetWndStatAll(clStat,m_OK);

	//------------------------------------
	m_CDATD.SetBKColor(SILVER);
	m_CDATD.SetInitText(pomFID->Cdat.Format("%d.%m.%Y"));
	m_CDATD.SetSecState(ogPrivList.GetStat("FIDSCOMMANDDLG.m_CDAT"));
	m_CDATT.SetBKColor(SILVER);
	m_CDATT.SetInitText(pomFID->Cdat.Format("%H:%M"));
	m_CDATT.SetSecState(ogPrivList.GetStat("FIDSCOMMANDDLG.m_CDAT"));
	//------------------------------------
	m_LSTUD.SetBKColor(SILVER);
	m_LSTUD.SetInitText(pomFID->Lstu.Format("%d.%m.%Y"));
	m_LSTUD.SetSecState(ogPrivList.GetStat("FIDSCOMMANDDLG.m_LSTU"));
	m_LSTUT.SetBKColor(SILVER);
	m_LSTUT.SetInitText(pomFID->Lstu.Format("%H:%M"));
	m_LSTUT.SetSecState(ogPrivList.GetStat("FIDSCOMMANDDLG.m_LSTU"));
	//------------------------------------
	m_USEC.SetBKColor(SILVER);
	m_USEC.SetInitText(pomFID->Usec);
	m_USEC.SetSecState(ogPrivList.GetStat("FIDSCOMMANDDLG.m_USEC"));
	//------------------------------------
	m_USEU.SetBKColor(SILVER);
	m_USEU.SetInitText(pomFID->Useu);
	m_USEU.SetSecState(ogPrivList.GetStat("FIDSCOMMANDDLG.m_USEU"));
	//------------------------------------//WHITE,YELLOW,SILVER,RED,LYELLOW
	m_CODE.SetFormat("x|#x|#x|#x|#");
	m_CODE.SetTextLimit(1,4);
	m_CODE.SetBKColor(YELLOW);  
	m_CODE.SetTextErrColor(RED);
	m_CODE.SetInitText(pomFID->Code);
	m_CODE.SetSecState(ogPrivList.GetStat("FIDSCOMMANDDLG.m_CODE"));
	//------------------------------------
	m_REMI.SetFormat("x|#x|#x|#x|#");
	m_REMI.SetBKColor(YELLOW);  
	m_REMI.SetTextLimit(1,4);
	m_REMI.SetTextErrColor(RED);
	m_REMI.SetInitText(pomFID->Remi);
	m_REMI.SetSecState(ogPrivList.GetStat("FIDSCOMMANDDLG.m_REMI"));
	//------------------------------------
	m_REMT.SetFormat("x|#");
	m_REMT.SetBKColor(YELLOW);  
	m_REMT.SetTextErrColor(RED);
	m_REMT.SetInitText(pomFID->Remt);
	m_REMT.SetSecState(ogPrivList.GetStat("FIDSCOMMANDDLG.m_REMT"));
	//------------------------------------
	if (pomFID->Blkc[0] == 'X') m_BLKC.SetCheck(1);
	clStat = ogPrivList.GetStat("FIDSCOMMANDDLG.m_BLKC");
	SetWndStatAll(clStat,m_BLKC);
	//------------------------------------
	m_BEME.SetTypeToString("X(20)",20,1);
	m_BEME.SetBKColor(YELLOW);  
	m_BEME.SetTextErrColor(RED);
	m_BEME.SetInitText(pomFID->Beme);
	m_BEME.SetSecState(ogPrivList.GetStat("FIDSCOMMANDDLG.m_BEME"));
	//------------------------------------
	m_BEMD.SetTypeToString("X(20)",20,0);
	m_BEMD.SetTextErrColor(RED);
	m_BEMD.SetInitText(pomFID->Bemd);
	m_BEMD.SetSecState(ogPrivList.GetStat("FIDSCOMMANDDLG.m_BEMD"));
	//------------------------------------
	m_BET3.SetTypeToString("X(20)",20,0);
	m_BET3.SetTextErrColor(RED);
	m_BET3.SetInitText(pomFID->Bet3);
	m_BET3.SetSecState(ogPrivList.GetStat("FIDSCOMMANDDLG.m_BET3"));
	//------------------------------------
	m_BET4.SetTypeToString("X(20)",20,0);
	m_BET4.SetTextErrColor(RED);
	m_BET4.SetInitText(pomFID->Bet4);
	m_BET4.SetSecState(ogPrivList.GetStat("FIDSCOMMANDDLG.m_BET4"));
	//------------------------------------
	m_VAFRD.SetTypeToDate(true);
	m_VAFRD.SetTextErrColor(RED);
	m_VAFRD.SetBKColor(YELLOW);
	m_VAFRD.SetInitText(pomFID->Vafr.Format("%d.%m.%Y"));
	m_VAFRD.SetSecState(ogPrivList.GetStat("FIDSCOMMANDDLG.m_VAFR"));
	// - - - - - - - - - - - - - - - - - -
	m_VAFRT.SetTypeToTime(true);
	m_VAFRT.SetTextErrColor(RED);
	m_VAFRT.SetBKColor(YELLOW);
	m_VAFRT.SetInitText(pomFID->Vafr.Format("%H:%M"));
	m_VAFRT.SetSecState(ogPrivList.GetStat("FIDSCOMMANDDLG.m_VAFR"));
	//------------------------------------
	m_VATOD.SetTypeToDate();
	m_VATOD.SetTextErrColor(RED);
	m_VATOD.SetInitText(pomFID->Vato.Format("%d.%m.%Y"));
	m_VATOD.SetSecState(ogPrivList.GetStat("FIDSCOMMANDDLG.m_VATO"));
	// - - - - - - - - - - - - - - - - - -
	m_VATOT.SetTypeToTime();
	m_VATOT.SetTextErrColor(RED);
	m_VATOT.SetInitText(pomFID->Vato.Format("%H:%M"));
	m_VATOT.SetSecState(ogPrivList.GetStat("FIDSCOMMANDDLG.m_VATO"));
	//------------------------------------
	return TRUE;  
}

//----------------------------------------------------------------------------------------

void FidsCommandDlg::OnOK() 
{
	AfxGetApp()->DoWaitCursor(1);
	CString olErrorText;
	bool ilStatus = true;
	if(m_CODE.GetStatus() == false)
	{
		ilStatus = false;
		if(m_CODE.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING237) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING237) + ogNotFormat;
		}
	}
	if(m_REMI.GetStatus() == false)
	{
		ilStatus = false;
		if(m_REMI.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING583) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING583) + ogNotFormat;
		}
	}
	if(m_REMT.GetStatus() == false)
	{
		ilStatus = false;
		if(m_REMT.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING584) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING584) + ogNotFormat;
		}
	}
	if(m_BEME.GetStatus() == false)
	{
		ilStatus = false;
		if(m_BEME.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING37) + CString(" 1") +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING37) + CString(" 1") + ogNotFormat;
		}
	}
	if(m_BEMD.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING37) + CString(" 2") + ogNotFormat;
	}
	if(m_BET3.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING37) + CString(" 3") + ogNotFormat;
	}
	if(m_BET4.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING37) + CString(" 4") + ogNotFormat;
	}
	if(m_VAFRD.GetStatus() == false)
	{
		ilStatus = false;
		if(m_VAFRD.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING230) + LoadStg(IDS_STRING232) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING230) + LoadStg(IDS_STRING232) + ogNotFormat;
		}
	}
	if(m_VAFRT.GetStatus() == false)
	{
		ilStatus = false;
		if(m_VAFRT.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING230) + LoadStg(IDS_STRING233) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING230) + LoadStg(IDS_STRING233) + ogNotFormat;
		}
	}
	if(m_VATOD.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING231) + LoadStg(IDS_STRING232) + ogNotFormat;
	}
	if(m_VATOT.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING231) + LoadStg(IDS_STRING233) + ogNotFormat;
	}

	///////////////////////////////////////////////////////////////////////////
	CString olVafrd,olVafrt,olVatod,olVatot;
	CTime olTmpTimeFr,olTmpTimeTo;
	m_VAFRD.GetWindowText(olVafrd);
	m_VAFRT.GetWindowText(olVafrt);
	m_VATOD.GetWindowText(olVatod);
	m_VATOT.GetWindowText(olVatot);

	if((m_VAFRD.GetWindowTextLength() != 0 && m_VAFRT.GetWindowTextLength() == 0) || (m_VAFRD.GetWindowTextLength() == 0 && m_VAFRT.GetWindowTextLength() != 0))
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING234);
	}
	if((m_VATOD.GetWindowTextLength() != 0 && m_VATOT.GetWindowTextLength() == 0) || (m_VATOD.GetWindowTextLength() == 0 && m_VATOT.GetWindowTextLength() != 0))
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING235);
	}
	if(m_VAFRD.GetStatus() == true && m_VAFRT.GetStatus() == true && m_VATOD.GetStatus() == true && m_VATOT.GetStatus() == true)
	{
		olTmpTimeFr = DateHourStringToDate(olVafrd,olVafrt);
		olTmpTimeTo = DateHourStringToDate(olVatod,olVatot);
		if(olTmpTimeTo != -1 && (olTmpTimeTo < olTmpTimeFr || olTmpTimeFr == -1))
		{
			ilStatus = false;
			olErrorText += LoadStg(IDS_STRING236);
		}
	}

	//Konsistenz-Check//
	if(ilStatus == true)
	{
		CString olCode;
		char clWhere[100];
		CCSPtrArray<FIDDATA> olFidCPA;

		m_CODE.GetWindowText(olCode);
		sprintf(clWhere,"WHERE CODE='%s'",olCode);
		if(ogFIDData.ReadSpecialData(&olFidCPA,clWhere,"URNO,CODE",false) == true)
		{
			if(olFidCPA[0].Urno != pomFID->Urno)
			{
				ilStatus = false;
				olErrorText += LoadStg(IDS_STRING237) + LoadStg(IDS_EXIST);
			}
		}
		olFidCPA.DeleteAll();
	}

	///////////////////////////////////////////////////////////////////////////
	AfxGetApp()->DoWaitCursor(-1);
	if (ilStatus == true)
	{
		m_CODE.GetWindowText(pomFID->Code,5);
		m_REMI.GetWindowText(pomFID->Remi,6);
		m_REMT.GetWindowText(pomFID->Remt,3);
		m_BEME.GetWindowText(pomFID->Beme,22);
		m_BEMD.GetWindowText(pomFID->Bemd,22);
		m_BET3.GetWindowText(pomFID->Bet3,22);
		m_BET4.GetWindowText(pomFID->Bet4,22);

		pomFID->Vafr = DateHourStringToDate(olVafrd,olVafrt);
		pomFID->Vato = DateHourStringToDate(olVatod,olVatot);
		(m_BLKC.GetCheck() == 1) ? pomFID->Blkc[0] = 'X' : pomFID->Blkc[0] = ' ';

		CDialog::OnOK();
	}
	else
	{
		Beep(440,70);
		MessageBox(olErrorText,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
		m_CODE.SetFocus();
		m_CODE.SetSel(0,-1);
	}
}

//----------------------------------------------------------------------------------------
