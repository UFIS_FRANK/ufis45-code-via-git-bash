// DiensteUndAbwesenheitenTableViewer.cpp 
//

#include "stdafx.h"
#include "DiensteUndAbwesenheitenTableViewer.h"
#include "CcsDdx.h"
#include "resource.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif


// Local function prototype
static void DiensteUndAbwesenheitenTableCf(void *popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

//-----------------------------------------------------------------------------------------------
// DiensteUndAbwesenheitenTableViewer
//

DiensteUndAbwesenheitenTableViewer::DiensteUndAbwesenheitenTableViewer(CCSPtrArray<ODADATA> *popData)
{
	pomData = popData;

	bmIsFromSearch = FALSE;
    pomDiensteUndAbwesenheitenTable = NULL;
    ogDdx.Register(this, ODA_CHANGE, CString("DiensteUndAbwesenheitenTableViewer"), CString("DiensteUndAbwesenheiten Update"), DiensteUndAbwesenheitenTableCf);
    ogDdx.Register(this, ODA_NEW,    CString("DiensteUndAbwesenheitenTableViewer"), CString("DiensteUndAbwesenheiten New"),    DiensteUndAbwesenheitenTableCf);
    ogDdx.Register(this, ODA_DELETE, CString("DiensteUndAbwesenheitenTableViewer"), CString("DiensteUndAbwesenheiten Delete"), DiensteUndAbwesenheitenTableCf);
}

//-----------------------------------------------------------------------------------------------

DiensteUndAbwesenheitenTableViewer::~DiensteUndAbwesenheitenTableViewer()
{
	ogDdx.UnRegister(this, NOTUSED);
    DeleteAll();
	omHeaderDataArray.DeleteAll();
}

//-----------------------------------------------------------------------------------------------

void DiensteUndAbwesenheitenTableViewer::Attach(CCSTable *popTable)
{
    pomDiensteUndAbwesenheitenTable = popTable;
}

//-----------------------------------------------------------------------------------------------

void DiensteUndAbwesenheitenTableViewer::ChangeViewTo(const char *pcpViewName)
{
    DeleteAll();    
    MakeLines();
	UpdateDisplay();
}

//-----------------------------------------------------------------------------------------------

void DiensteUndAbwesenheitenTableViewer::MakeLines()
{
	int ilDiensteUndAbwesenheitenCount = pomData->GetSize();
	for (int ilLc = 0; ilLc < ilDiensteUndAbwesenheitenCount; ilLc++)
	{
		ODADATA *prlDiensteUndAbwesenheitenData = &pomData->GetAt(ilLc);
		MakeLine(prlDiensteUndAbwesenheitenData);
	}
}

//-----------------------------------------------------------------------------------------------

void DiensteUndAbwesenheitenTableViewer::MakeLine(ODADATA *prpDiensteUndAbwesenheiten)
{

    //if( !IsPassFilter(prpDiensteUndAbwesenheiten)) return;

    // Update viewer data for this shift record
    DIENSTEUNDABWESENHEITENTABLE_LINEDATA rlDiensteUndAbwesenheiten;


	rlDiensteUndAbwesenheiten.Urno = prpDiensteUndAbwesenheiten->Urno;
	rlDiensteUndAbwesenheiten.Sdac = prpDiensteUndAbwesenheiten->Sdac;
	rlDiensteUndAbwesenheiten.Sdan = prpDiensteUndAbwesenheiten->Sdan;
	rlDiensteUndAbwesenheiten.Sdak = prpDiensteUndAbwesenheiten->Sdak;
	rlDiensteUndAbwesenheiten.Sdas = prpDiensteUndAbwesenheiten->Sdas;
	rlDiensteUndAbwesenheiten.Dptc = prpDiensteUndAbwesenheiten->Dptc;
	rlDiensteUndAbwesenheiten.Ctrc = prpDiensteUndAbwesenheiten->Ctrc;
	rlDiensteUndAbwesenheiten.Dura = prpDiensteUndAbwesenheiten->Dura;
	rlDiensteUndAbwesenheiten.Rema = prpDiensteUndAbwesenheiten->Rema;

	CreateLine(&rlDiensteUndAbwesenheiten);
}

//-----------------------------------------------------------------------------------------------

void DiensteUndAbwesenheitenTableViewer::CreateLine(DIENSTEUNDABWESENHEITENTABLE_LINEDATA *prpDiensteUndAbwesenheiten)
{
    int ilLineCount = omLines.GetSize();
    for (int ilLineno = ilLineCount; ilLineno > 0; ilLineno--)
        if (CompareDiensteUndAbwesenheiten(prpDiensteUndAbwesenheiten, &omLines[ilLineno-1]) >= 0)
	        break;							 // should be inserted after Lines[ilLineno-1]
	DIENSTEUNDABWESENHEITENTABLE_LINEDATA rlDiensteUndAbwesenheiten;
	rlDiensteUndAbwesenheiten = *prpDiensteUndAbwesenheiten;
    omLines.NewAt(ilLineno, rlDiensteUndAbwesenheiten);
}

//-----------------------------------------------------------------------------------------------
// UpdateDisplay: Load data selected by filter conditions to the display by using "omTable"

void DiensteUndAbwesenheitenTableViewer::UpdateDisplay()
{
	int ilLines = omLines.GetSize();
	const int ilColumns = 7;	//<===== Spaltenanzahl setzen (von 0 ab)
	omHeaderDataArray.DeleteAll();

	pomDiensteUndAbwesenheitenTable->SetShowSelection(TRUE);
	pomDiensteUndAbwesenheitenTable->ResetContent();

	TABLE_HEADER_COLUMN rlHeader;
	rlHeader.Alignment = COLALIGN_LEFT;
	rlHeader.Font = &ogCourier_Regular_10;

	int ilPos = 1;
	rlHeader.Length = 42; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING261),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 332; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING261),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 100; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING261),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 50; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING261),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 42; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING261),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 42; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING261),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 33; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING261),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 450; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING261),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);

	pomDiensteUndAbwesenheitenTable->SetHeaderFields(omHeaderDataArray);
	pomDiensteUndAbwesenheitenTable->SetDefaultSeparator();
	pomDiensteUndAbwesenheitenTable->SetTableEditable(FALSE);

	TABLE_COLUMN rlColumnData;
	rlColumnData.Font = &ogCourier_Regular_10;
	rlColumnData.BkColor = WHITE;
	rlColumnData.Alignment = COLALIGN_LEFT;
	
    for (int ilLineNo = 0; ilLineNo < ilLines; ilLineNo++)
	{
		CCSPtrArray<TABLE_COLUMN> olColList;

		rlColumnData.Text = omLines[ilLineNo].Sdac;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Sdan;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Sdak;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Sdas;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Ctrc;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Dptc;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Alignment = COLALIGN_RIGHT;
		rlColumnData.Text = omLines[ilLineNo].Dura;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Alignment = COLALIGN_LEFT;
		rlColumnData.Text = omLines[ilLineNo].Rema;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		pomDiensteUndAbwesenheitenTable->AddTextLine(olColList,(void*)NULL);
		olColList.DeleteAll();
	}
	pomDiensteUndAbwesenheitenTable->DisplayTable();
}

//-----------------------------------------------------------------------------------------------

CString DiensteUndAbwesenheitenTableViewer::Format(DIENSTEUNDABWESENHEITENTABLE_LINEDATA *prpLine)
{
    CString s;
	// ???
    return s;
}

//-----------------------------------------------------------------------------------------------

bool DiensteUndAbwesenheitenTableViewer::FindDiensteUndAbwesenheiten(char *pcpDiensteUndAbwesenheitenKeya, char *pcpDiensteUndAbwesenheitenKeyd, int& ilItem)
{
	int ilCount = omLines.GetSize();
    for (ilItem = 0; ilItem < ilCount; ilItem++)
    {
/*		if ( (omLines[ilItem].Keya == pcpDiensteUndAbwesenheitenKeya) &&
			 (omLines[ilItem].Keyd == pcpDiensteUndAbwesenheitenKeyd)    )
			return TRUE;*/
	}
	return false;
}

//-----------------------------------------------------------------------------------------------

static void DiensteUndAbwesenheitenTableCf(void *popInstance, int ipDDXType,
    void *vpDataPointer, CString &ropInstanceName)
{
    DiensteUndAbwesenheitenTableViewer *polViewer = (DiensteUndAbwesenheitenTableViewer *)popInstance;
    if (ipDDXType == ODA_CHANGE || ipDDXType == ODA_NEW) polViewer->ProcessDiensteUndAbwesenheitenChange((ODADATA *)vpDataPointer);
    if (ipDDXType == ODA_DELETE) polViewer->ProcessDiensteUndAbwesenheitenDelete((ODADATA *)vpDataPointer);
} 


//-----------------------------------------------------------------------------------------------

void DiensteUndAbwesenheitenTableViewer::ProcessDiensteUndAbwesenheitenChange(ODADATA *prpDiensteUndAbwesenheiten)
{
	int ilItem = 0;
	int ilColumnNo = 0;
	TABLE_COLUMN rlColumn;
	CCSPtrArray <TABLE_COLUMN> olLine;

	rlColumn.Lineno = ilItem;
	rlColumn.VerticalSeparator = SEPA_NONE;
	rlColumn.SeparatorType = SEPA_NONE;
	rlColumn.BkColor = WHITE;
	rlColumn.Font = &ogCourier_Regular_10;
	rlColumn.Alignment = COLALIGN_LEFT;

	rlColumn.Text = prpDiensteUndAbwesenheiten->Sdac;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpDiensteUndAbwesenheiten->Sdan;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpDiensteUndAbwesenheiten->Sdak;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpDiensteUndAbwesenheiten->Sdas;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpDiensteUndAbwesenheiten->Ctrc;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpDiensteUndAbwesenheiten->Dptc;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Alignment = COLALIGN_RIGHT;
	rlColumn.Text = prpDiensteUndAbwesenheiten->Dura;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Alignment = COLALIGN_LEFT;
	rlColumn.Text = prpDiensteUndAbwesenheiten->Rema;
	olLine.NewAt(olLine.GetSize(), rlColumn);

	
	if (FindLine(prpDiensteUndAbwesenheiten->Urno, ilItem))
	{
        DIENSTEUNDABWESENHEITENTABLE_LINEDATA *prlLine = &omLines[ilItem];

		prlLine->Urno = prpDiensteUndAbwesenheiten->Urno;
		prlLine->Sdac = prpDiensteUndAbwesenheiten->Sdac;
		prlLine->Sdan = prpDiensteUndAbwesenheiten->Sdan;
		prlLine->Sdak = prpDiensteUndAbwesenheiten->Sdak;
		prlLine->Sdas = prpDiensteUndAbwesenheiten->Sdas;
		prlLine->Dptc = prpDiensteUndAbwesenheiten->Dptc;
		prlLine->Ctrc = prpDiensteUndAbwesenheiten->Ctrc;
		prlLine->Dura = prpDiensteUndAbwesenheiten->Dura;
		prlLine->Rema = prpDiensteUndAbwesenheiten->Rema;


		pomDiensteUndAbwesenheitenTable->ChangeTextLine(ilItem, &olLine, (void *)prlLine);
		pomDiensteUndAbwesenheitenTable->DisplayTable();
	}
	else
	{
		MakeLine(prpDiensteUndAbwesenheiten);
		if (FindLine(prpDiensteUndAbwesenheiten->Urno, ilItem))
		{
	        DIENSTEUNDABWESENHEITENTABLE_LINEDATA *prlLine = &omLines[ilItem];
			if(prlLine != NULL)
			{
				pomDiensteUndAbwesenheitenTable->AddTextLine(olLine, (void *)prlLine);
				pomDiensteUndAbwesenheitenTable->DisplayTable();
			}
		}
	}
	olLine.DeleteAll();
}

//-----------------------------------------------------------------------------------------------

void DiensteUndAbwesenheitenTableViewer::ProcessDiensteUndAbwesenheitenDelete(ODADATA *prpDiensteUndAbwesenheiten)
{
	int ilItem;
	if (FindLine(prpDiensteUndAbwesenheiten->Urno, ilItem))
	{
		DeleteLine(ilItem);
		pomDiensteUndAbwesenheitenTable->DeleteTextLine(ilItem);
		pomDiensteUndAbwesenheitenTable->DisplayTable();
	}
}

//-----------------------------------------------------------------------------------------------

bool DiensteUndAbwesenheitenTableViewer::IsPassFilter(ODADATA *prpDiensteUndAbwesenheiten)
{

	return true;
}

//-----------------------------------------------------------------------------------------------

int DiensteUndAbwesenheitenTableViewer::CompareDiensteUndAbwesenheiten(DIENSTEUNDABWESENHEITENTABLE_LINEDATA *prpDiensteUndAbwesenheiten1, DIENSTEUNDABWESENHEITENTABLE_LINEDATA *prpDiensteUndAbwesenheiten2)
{
/*	int ilCase=0;
	int	ilCompareResult;
	if(prpDiensteUndAbwesenheiten1->Tifd == prpDiensteUndAbwesenheiten2->Tifd)
	{
		ilCompareResult= 0;
	}
	else
	{
		if(prpDiensteUndAbwesenheiten1->Tifd > prpDiensteUndAbwesenheiten2->Tifd)
		{
			ilCompareResult = 1;
		}
		else
		{
			ilCompareResult = -1;
		}
	}
	return ilCompareResult;
*/  
	return 0;
}

//-----------------------------------------------------------------------------------------------

void DiensteUndAbwesenheitenTableViewer::DeleteLine(int ipLineno)
{
	omLines.DeleteAt(ipLineno);
}

//-----------------------------------------------------------------------------------------------

bool DiensteUndAbwesenheitenTableViewer::FindLine(long lpUrno, int &rilLineno)
{
    for (rilLineno = 0; rilLineno < omLines.GetSize(); rilLineno++)
	{
        if (omLines[rilLineno].Urno == lpUrno)
            return true;
	}
    return false;
}
//-----------------------------------------------------------------------------------------------

void DiensteUndAbwesenheitenTableViewer::DeleteAll()
{
    while (omLines.GetSize() > 0)
    DeleteLine(0);
}

//-----------------------------------------------------------------------------------------------
// Printing Routines
//-----------------------------------------------------------------------------------------------

void DiensteUndAbwesenheitenTableViewer::PrintTableView()
{
	CString olFooter1,olFooter2;
	omTableName = LoadStg(IDS_STRING168);
	int ilOrientation = PRINT_LANDSCAPE;//  PRINT_PORTRAET  PRINT_LANDSCAPE
	pomPrint = new CCSPrint((CWnd*)this,ilOrientation,45);// FirstLine(P=200,L=200),LeftOffset(P=200,L=50)
	//pomPrint->imMaxLines = 38;  // (P=57,L=38)
	//omBitmap.LoadBitmap(IDB_HAJLOGO);
	//pomPrint->SetBitmaps(&omBitmap,&omBitmap);
	if (pomPrint != NULL)
	{
		if (pomPrint->InitializePrinter(ilOrientation) == TRUE)
		{
			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
			pomPrint->imLineNo = pomPrint->imMaxLines + 1;
			DOCINFO	rlDocInfo;
			memset(&rlDocInfo, 0, sizeof(DOCINFO));
			rlDocInfo.cbSize = sizeof( DOCINFO );
			rlDocInfo.lpszDocName = omTableName;	
			pomPrint->omCdc.StartDoc( &rlDocInfo );
			pomPrint->imPageNo = 0;
			int ilLines = omLines.GetSize();
			olFooter1.Format("%d %s",ilLines,omTableName);
			for(int i = 0; i < ilLines; i++ ) 
			{
				if(pomPrint->imLineNo >= pomPrint->imMaxLines)
				{
					if(pomPrint->imPageNo > 0)
					{
						olFooter2.Format("%s: %d",LoadStg(IDS_STRING222),pomPrint->imPageNo);
						pomPrint->PrintUIFFooter(olFooter1,"",olFooter2);
						pomPrint->omCdc.EndPage();
					}
					PrintTableHeader();
				}
				if(pomPrint->imLineNo == (pomPrint->imMaxLines-1) || i == (ilLines-1))
				{
					PrintTableLine(&omLines[i],true);
				}
				else
				{
					PrintTableLine(&omLines[i],false);
				}
			}
			olFooter2.Format("%s: %d",LoadStg(IDS_STRING222),pomPrint->imPageNo);
			pomPrint->PrintUIFFooter(olFooter1,"",olFooter2);
			pomPrint->omCdc.EndPage();
			pomPrint->omCdc.EndDoc();
		}
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
	delete pomPrint;
	//omBitmap.DeleteObject();
	pomPrint = NULL;
	}
}

//-----------------------------------------------------------------------------------------------

bool DiensteUndAbwesenheitenTableViewer::PrintTableHeader()
{
	pomPrint->omCdc.StartPage();
	pomPrint->imPageNo++;
	pomPrint->imLineNo = 0;

	pomPrint->PrintUIFHeader(omTableName,CString(CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M")),pomPrint->imFirstLine-10);

	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;

	rlElement.Alignment   = PRINT_LEFT;
	rlElement.FrameLeft   = PRINT_NOFRAME;
	rlElement.FrameRight  = PRINT_NOFRAME;
	rlElement.FrameTop    = PRINT_NOFRAME;
	rlElement.FrameBottom = PRINT_FRAMEMEDIUM;
	rlElement.pFont       = &pomPrint->ogCourierNew_Bold_8;

	int ilSize = omHeaderDataArray.GetSize();
	for(int i=0;i<ilSize;i++)
	{
		//if(i!=1&&i!=2)// <= setzen, wenn Spalten nicht angezeigt werden sollen 
		{
			rlElement.Length = (int)(omHeaderDataArray[i].Length*dgCCSPrintFactor);
			if(rlElement.Length < igCCSPrintMinLength)rlElement.Length+=igCCSPrintMoreLength; 
			rlElement.Text   = omHeaderDataArray[i].Text;
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
		}
	}
	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();


	return true;
}

//-----------------------------------------------------------------------------------------------

bool DiensteUndAbwesenheitenTableViewer::PrintTableLine(DIENSTEUNDABWESENHEITENTABLE_LINEDATA *prpLine,bool bpLastLine)
{
	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;
	rlElement.pFont      = &pomPrint->ogCourierNew_Regular_8;
	int ilSize = omHeaderDataArray.GetSize();
	for(int i=0;i<ilSize;i++)
	{
		//if(i!=1&&i!=1)// <= setzen, wenn Spalten nicht angezeigt werden sollen 
		{
			rlElement.Length = (int)(omHeaderDataArray[i].Length*dgCCSPrintFactor);
			if(rlElement.Length < igCCSPrintMinLength)rlElement.Length+=igCCSPrintMoreLength; 
			rlElement.Alignment  = PRINT_LEFT;
			rlElement.FrameTop   = PRINT_FRAMETHIN;
			if(bpLastLine == true)
			{
				rlElement.FrameBottom = PRINT_FRAMETHIN;
			}
			else
			{
				rlElement.FrameBottom = PRINT_NOFRAME;
			}
			rlElement.FrameLeft  = PRINT_NOFRAME;
			rlElement.FrameRight = PRINT_NOFRAME;


			switch(i)
			{
			case 0:
				rlElement.FrameLeft  = PRINT_FRAMETHIN;
				rlElement.Text = prpLine->Sdac;
				break;
			case 1:
				rlElement.Text = prpLine->Sdan;
				break;
			case 2:
				rlElement.Text = prpLine->Sdak;
				break;
			case 3:
				rlElement.Text = prpLine->Sdas;
				break;
			case 4:
				rlElement.Text = prpLine->Ctrc;
				break;
			case 5:
				rlElement.Text = prpLine->Dptc;
				break;
			case 6:
				rlElement.Alignment = PRINT_RIGHT;
				rlElement.Text = prpLine->Dura;
				break;
			case 7:
				rlElement.FrameRight = PRINT_FRAMETHIN;
				rlElement.Text = prpLine->Rema;
				break;
			}
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
		}
	}
	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();
	return true;
}

//-----------------------------------------------------------------------------------------------
