#ifndef __ArbeitsgruppenTableViewer_H__
#define __ArbeitsgruppenTableViewer_H__

#include "stdafx.h"
#include "CedaWgpData.h"
#include "CCSTable.h"
#include "CViewer.h"
#include "CCSPrint.h"


struct ARBEITSGRUPPENTABLE_LINEDATA
{
	long 	 Urno; 	// Eindeutige Datensatz-Nr.
	CString  Wgpc; 	// Code
	CString  Wgpn; 	// Bezeichnung
	CString  Rema; 	// Bemerkungen

};

/////////////////////////////////////////////////////////////////////////////
// ArbeitsgruppenTableViewer

	  
class ArbeitsgruppenTableViewer : public CViewer
{
// Constructions
public:
    ArbeitsgruppenTableViewer(CCSPtrArray<WGPDATA> *popData);
    ~ArbeitsgruppenTableViewer();

    void Attach(CCSTable *popAttachWnd);
    virtual void ChangeViewTo(const char *pcpViewName);
// Internal data processing routines
private:
	bool IsPassFilter(WGPDATA *prpArbeitsgruppen);
	int CompareArbeitsgruppen(ARBEITSGRUPPENTABLE_LINEDATA *prpArbeitsgruppen1, ARBEITSGRUPPENTABLE_LINEDATA *prpArbeitsgruppen2);
    void MakeLines();
	void MakeLine(WGPDATA *prpArbeitsgruppen);
	bool FindLine(long lpUrno, int &rilLineno);
// Operations
public:
	void DeleteAll();
	void CreateLine(ARBEITSGRUPPENTABLE_LINEDATA *prpArbeitsgruppen);
	void DeleteLine(int ipLineno);
// Window refreshing routines
public:
	void UpdateDisplay();
	CString Format(ARBEITSGRUPPENTABLE_LINEDATA *prpLine);
	void ProcessArbeitsgruppenChange(WGPDATA *prpArbeitsgruppen);
	void ProcessArbeitsgruppenDelete(WGPDATA *prpArbeitsgruppen);
	bool FindArbeitsgruppen(char *prpArbeitsgruppenKeya, char *prpArbeitsgruppenKeyd, int& ilItem);
// Attributes used for filtering condition
private:
	bool bmIsFromSearch;
// Attributes
private:
    CCSTable *pomArbeitsGruppenTable;
	CCSPtrArray<WGPDATA> *pomData;
// Methods which handle changes (from Data Distributor)
public:
    CCSPtrArray<ARBEITSGRUPPENTABLE_LINEDATA> omLines;
///////
	CCSPtrArray<TABLE_HEADER_COLUMN> omHeaderDataArray;
//Print 
	void PrintTableView();
	bool PrintTableLine(ARBEITSGRUPPENTABLE_LINEDATA *prpLine,bool bpLastLine);
	bool PrintTableHeader();
	CCSPrint *pomPrint;
	//CBitmap omBitmap;
	CString omTableName;

};

#endif //__ArbeitsgruppenTableViewer_H__
