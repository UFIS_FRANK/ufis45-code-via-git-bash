// CheckinCounterDlg.cpp : implementation file
//

#include "stdafx.h"
#include "BDPSUIF.h"
#include "CheckinCounterDlg.h"
#include "NotAvailableDlg.h"
#include "CedaWROData.h"
#include "PrivList.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CheckinCounterDlg dialog
//----------------------------------------------------------------------------------------

CheckinCounterDlg::CheckinCounterDlg(CICDATA *popCIC,CWnd* pParent /*=NULL*/) : CDialog(CheckinCounterDlg::IDD, pParent)
{
	pomCIC = popCIC;
	pomTable = NULL;
	pomTable = new CCSTable;

	//{{AFX_DATA_INIT(CheckinCounterDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}

CheckinCounterDlg::~CheckinCounterDlg()
{
	omBlkPtrA.DeleteAll();
	omDeleteBlkPtrA.DeleteAll();
	delete pomTable;

}

void CheckinCounterDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CheckinCounterDlg)
	DDX_Control(pDX, IDC_NOAVNEW, m_NOAVNEW);
	DDX_Control(pDX, IDC_NOAVDEL, m_NOAVDEL);
	DDX_Control(pDX, IDC_NOAV,	 m_NOAVFRAME);
	DDX_Control(pDX, IDOK,		 m_OK);
	DDX_Control(pDX, IDC_CDAT_D, m_CDATD);
	DDX_Control(pDX, IDC_CDAT_T, m_CDATT);
	DDX_Control(pDX, IDC_LSTU_D, m_LSTUD);
	DDX_Control(pDX, IDC_LSTU_T, m_LSTUT);
	DDX_Control(pDX, IDC_USEU,	 m_USEU);
	DDX_Control(pDX, IDC_USEC,	 m_USEC);
	DDX_Control(pDX, IDC_VAFR_D, m_VAFRD);
	DDX_Control(pDX, IDC_VAFR_T, m_VAFRT);
	DDX_Control(pDX, IDC_VATO_D, m_VATOD);
	DDX_Control(pDX, IDC_VATO_T, m_VATOT);
	DDX_Control(pDX, IDC_CNAM,	 m_CNAM);
	DDX_Control(pDX, IDC_GRUP,	 m_GRUP);
	DDX_Control(pDX, IDC_EDPE,	 m_EDPE);
	DDX_Control(pDX, IDC_RGBL,	 m_RGBL);
	DDX_Control(pDX, IDC_TELE,	 m_TELE);
	DDX_Control(pDX, IDC_TERM,	 m_TERM);
	DDX_Control(pDX, IDC_HALL,	 m_HALL);
	DDX_Control(pDX, IDC_CICR,	 m_CICR);
	DDX_Control(pDX, IDC_HOME,	 m_HOME);
	DDX_Control(pDX, IDC_CBAZ,	 m_CBAZ);
	DDX_Control(pDX, IDC_CICL,	 m_CICL);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CheckinCounterDlg, CDialog)
	//{{AFX_MSG_MAP(CheckinCounterDlg)
	ON_MESSAGE(WM_TABLE_LBUTTONDBLCLK,	OnTableLButtonDblclk)
	ON_BN_CLICKED(IDC_NOAVDEL, OnNoavDel)
	ON_BN_CLICKED(IDC_NOAVNEW, OnNoavNew)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CheckinCounterDlg message handlers
//----------------------------------------------------------------------------------------

BOOL CheckinCounterDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	AfxGetApp()->DoWaitCursor(1);
	m_Caption = LoadStg(IDS_STRING166) + " <" + m_Caption + ">";
	SetWindowText(m_Caption);

	char clStat = ogPrivList.GetStat("CHECKINCOUNTERDLG.m_OK");
	SetWndStatAll(clStat,m_OK);


	clStat = ogPrivList.GetStat("CHECKINCOUNTERDLG.m_NOAVNEW");
	SetWndStatAll(clStat,m_NOAVNEW);
	clStat = ogPrivList.GetStat("CHECKINCOUNTERDLG.m_NOAVDEL");
	SetWndStatAll(clStat,m_NOAVDEL);

	//------------------------------------
	m_CDATD.SetBKColor(SILVER);
	m_CDATD.SetInitText(pomCIC->Cdat.Format("%d.%m.%Y"));
	m_CDATD.SetSecState(ogPrivList.GetStat("CHECKINCOUNTERDLG.m_CDAT"));
	m_CDATT.SetBKColor(SILVER);
	m_CDATT.SetInitText(pomCIC->Cdat.Format("%H:%M"));
	m_CDATT.SetSecState(ogPrivList.GetStat("CHECKINCOUNTERDLG.m_CDAT"));
	//------------------------------------
	m_LSTUD.SetBKColor(SILVER);
	m_LSTUD.SetInitText(pomCIC->Lstu.Format("%d.%m.%Y"));
	m_LSTUD.SetSecState(ogPrivList.GetStat("CHECKINCOUNTERDLG.m_LSTU"));
	m_LSTUT.SetBKColor(SILVER);
	m_LSTUT.SetInitText(pomCIC->Lstu.Format("%H:%M"));
	m_LSTUT.SetSecState(ogPrivList.GetStat("CHECKINCOUNTERDLG.m_LSTU"));
	//------------------------------------
	m_USEC.SetBKColor(SILVER);
	m_USEC.SetInitText(pomCIC->Usec);
	m_USEC.SetSecState(ogPrivList.GetStat("CHECKINCOUNTERDLG.m_USEC"));
	//------------------------------------
	m_USEU.SetBKColor(SILVER);
	m_USEU.SetInitText(pomCIC->Useu);
	m_USEU.SetSecState(ogPrivList.GetStat("CHECKINCOUNTERDLG.m_USEU"));
	//------------------------------------
	m_GRUP.SetBKColor(SILVER);
	//m_GRUP.SetInitText(pomCIC->);
	m_GRUP.SetSecState(ogPrivList.GetStat("CHECKINCOUNTERDLG.m_GRUP"));
	//------------------------------------//WHITE,YELLOW,SILVER,RED,LYELLOW
	m_CNAM.SetFormat("x|#x|#x|#x|#x|#");
	m_CNAM.SetTextLimit(1,5);
	m_CNAM.SetBKColor(YELLOW);
	m_CNAM.SetTextErrColor(RED);
	m_CNAM.SetInitText(pomCIC->Cnam);
	m_CNAM.SetSecState(ogPrivList.GetStat("CHECKINCOUNTERDLG.m_CNAM"));
	//------------------------------------
	m_EDPE.SetTypeToString("X(10)",10,0);
	m_EDPE.SetTextErrColor(RED);
	m_EDPE.SetInitText(pomCIC->Edpe);
	m_EDPE.SetSecState(ogPrivList.GetStat("CHECKINCOUNTERDLG.m_EDPE"));
	//------------------------------------
	m_RGBL.SetFormat("x|#x|#x|#x|#x|#");
	m_RGBL.SetTextLimit(0,5);
	m_RGBL.SetTextErrColor(RED);
	m_RGBL.SetInitText(pomCIC->Rgbl);
	m_RGBL.SetSecState(ogPrivList.GetStat("CHECKINCOUNTERDLG.m_RGBL"));
	//------------------------------------
	m_TERM.SetFormat("x|#");
	m_TERM.SetTextLimit(0,1);
	m_TERM.SetTextErrColor(RED);
	m_TERM.SetInitText(pomCIC->Term);
	m_TERM.SetSecState(ogPrivList.GetStat("CHECKINCOUNTERDLG.m_TERM"));
	//------------------------------------
	m_HALL.SetFormat("x|#x|#x|#x|#x|#x|#");
	m_HALL.SetTextLimit(0,6);
	m_HALL.SetTextErrColor(RED);
	m_HALL.SetInitText(pomCIC->Hall);
	m_HALL.SetSecState(ogPrivList.GetStat("CHECKINCOUNTERDLG.m_HALL"));
	//------------------------------------
	m_TELE.SetTypeToString("X(10)",10,0);
	m_TELE.SetTextErrColor(RED);
	m_TELE.SetInitText(pomCIC->Tele);
	m_TELE.SetSecState(ogPrivList.GetStat("CHECKINCOUNTERDLG.m_TELE"));
	//------------------------------------
	m_VAFRD.SetTypeToDate(true);
	m_VAFRD.SetTextErrColor(RED);
	m_VAFRD.SetBKColor(YELLOW);
	m_VAFRD.SetInitText(pomCIC->Vafr.Format("%d.%m.%Y"));
	m_VAFRD.SetSecState(ogPrivList.GetStat("CHECKINCOUNTERDLG.m_VAFR"));
	// - - - - - - - - - - - - - - - - - -
	m_VAFRT.SetTypeToTime(true);
	m_VAFRT.SetTextErrColor(RED);
	m_VAFRT.SetBKColor(YELLOW);
	m_VAFRT.SetInitText(pomCIC->Vafr.Format("%H:%M"));
	m_VAFRT.SetSecState(ogPrivList.GetStat("CHECKINCOUNTERDLG.m_VAFR"));
	//------------------------------------
	m_VATOD.SetTypeToDate();
	m_VATOD.SetTextErrColor(RED);
	m_VATOD.SetInitText(pomCIC->Vato.Format("%d.%m.%Y"));
	m_VATOD.SetSecState(ogPrivList.GetStat("CHECKINCOUNTERDLG.m_VATO"));
	// - - - - - - - - - - - - - - - - - -
	m_VATOT.SetTypeToTime();
	m_VATOT.SetTextErrColor(RED);
	m_VATOT.SetInitText(pomCIC->Vato.Format("%H:%M"));
	m_VATOT.SetSecState(ogPrivList.GetStat("CHECKINCOUNTERDLG.m_VATO"));
	//------------------------------------
	m_CICR.SetFormat("x|#x|#x|#");
	m_CICR.SetTextLimit(0,3);
	m_CICR.SetTextErrColor(RED);
	m_CICR.SetInitText(pomCIC->Cicr);
	m_CICR.SetSecState(ogPrivList.GetStat("CHECKINCOUNTERDLG.m_CICR"));
	//------------------------------------
	m_CICL.SetTypeToString("X(3)",3,1);
	m_CICL.SetTextErrColor(RED);
	m_CICL.SetInitText(pomCIC->Cicl);
	m_CICL.SetSecState(ogPrivList.GetStat("CHECKINCOUNTERDLG.m_CICL"));
	//------------------------------------
	m_CBAZ.SetTypeToString("X(3)",3,1);
	m_CBAZ.SetTextErrColor(RED);
	m_CBAZ.SetInitText(pomCIC->Cbaz);
	m_CBAZ.SetSecState(ogPrivList.GetStat("CHECKINCOUNTERDLG.m_CBAZ"));
	//------------------------------------
	m_HOME.SetTypeToString("X(3)",3,1);
	m_HOME.SetTextErrColor(RED);
	m_HOME.SetInitText(pomCIC->Home);
	m_HOME.SetSecState(ogPrivList.GetStat("CHECKINCOUNTERDLG.m_HOME"));
	//------------------------------------

	CString olBurn;
	olBurn.Format("%d",pomCIC->Urno);
	MakeNoavTable("CIC", olBurn);

	//------------------------------------
	AfxGetApp()->DoWaitCursor(-1);
	return TRUE;
}

//----------------------------------------------------------------------------------------

void CheckinCounterDlg::OnOK() 
{
	AfxGetApp()->DoWaitCursor(1);
	CString olErrorText;
	bool ilStatus = true;
	if(m_CNAM.GetStatus() == false)
	{
		ilStatus = false;
		if(m_CNAM.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING288) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING288) + ogNotFormat;
		}
	}
	if(m_EDPE.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING373) + ogNotFormat;
	}
	if(m_RGBL.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING374) + ogNotFormat;
	}
	if(m_TERM.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING296) + ogNotFormat;
	}
	if(m_HALL.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING105) + ogNotFormat;
	}
	if(m_TELE.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING294) + ogNotFormat;
	}
	if(m_CICR.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING591) + ogNotFormat;
	}
	if(m_VAFRD.GetStatus() == false)
	{
		ilStatus = false;
		if(m_VAFRD.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING230) + LoadStg(IDS_STRING232) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING230) + LoadStg(IDS_STRING232) + ogNotFormat;
		}
	}
	if(m_VAFRT.GetStatus() == false)
	{
		ilStatus = false;
		if(m_VAFRT.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING230) + LoadStg(IDS_STRING233) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING230) + LoadStg(IDS_STRING233) + ogNotFormat;
		}
	}
	if(m_VATOD.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING231) + LoadStg(IDS_STRING232) + ogNotFormat;
	}
	if(m_VATOT.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING231) + LoadStg(IDS_STRING233) + ogNotFormat;
	}

	///////////////////////////////////////////////////////////////////////////
	CString olVafrd,olVafrt,olVatod,olVatot;
	CTime olTmpTimeFr,olTmpTimeTo;
	m_VAFRD.GetWindowText(olVafrd);
	m_VAFRT.GetWindowText(olVafrt);
	m_VATOD.GetWindowText(olVatod);
	m_VATOT.GetWindowText(olVatot);

	if((m_VAFRD.GetWindowTextLength() != 0 && m_VAFRT.GetWindowTextLength() == 0) || (m_VAFRD.GetWindowTextLength() == 0 && m_VAFRT.GetWindowTextLength() != 0))
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING234);
	}
	if((m_VATOD.GetWindowTextLength() != 0 && m_VATOT.GetWindowTextLength() == 0) || (m_VATOD.GetWindowTextLength() == 0 && m_VATOT.GetWindowTextLength() != 0))
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING235);
	}
	if(m_VAFRD.GetStatus() == true && m_VAFRT.GetStatus() == true && m_VATOD.GetStatus() == true && m_VATOT.GetStatus() == true)
	{
		olTmpTimeFr = DateHourStringToDate(olVafrd,olVafrt);
		olTmpTimeTo = DateHourStringToDate(olVatod,olVatot);
		if(olTmpTimeTo != -1 && (olTmpTimeTo < olTmpTimeFr || olTmpTimeFr == -1))
		{
			ilStatus = false;
			olErrorText += LoadStg(IDS_STRING236);
		}
	}

	//Konsistenz-Check//
	if(ilStatus == true)
	{
		CString olRgbl,olCnam;
		char clWhere[100];

		m_RGBL.GetWindowText(olRgbl);
		if(olRgbl.GetLength() != 0)
		{
			sprintf(clWhere,"WHERE WNAM='%s'",olRgbl);
			if(ogWROData.ReadSpecialData(NULL,clWhere,"WNAM",false) == false)
			{
				ilStatus = false;
				olErrorText += LoadStg(IDS_STRING375);
			}
		}

		CCSPtrArray<CICDATA> olCicCPA;
		m_CNAM.GetWindowText(olCnam);
		sprintf(clWhere,"WHERE CNAM='%s'",olCnam);
		if(ogCICData.ReadSpecialData(&olCicCPA,clWhere,"URNO,CNAM",false) == true)
		{
			if(olCicCPA[0].Urno != pomCIC->Urno)
			{
				ilStatus = false;
				olErrorText += LoadStg(IDS_STRING288) + LoadStg(IDS_EXIST);
			}
		}
		olCicCPA.DeleteAll();
	}
	///////////////////////////////////////////////////////////////////////////
	AfxGetApp()->DoWaitCursor(-1);
	if (ilStatus == true)
	{
		m_RGBL.GetWindowText(pomCIC->Rgbl,6);
		m_CNAM.GetWindowText(pomCIC->Cnam,6);
		m_EDPE.GetWindowText(pomCIC->Edpe,11);
		m_TERM.GetWindowText(pomCIC->Term,2);
		m_HALL.GetWindowText(pomCIC->Hall,7);
		m_TELE.GetWindowText(pomCIC->Tele,11);
		m_CICR.GetWindowText(pomCIC->Cicr,4);
		m_CBAZ.GetWindowText(pomCIC->Cbaz,4);
		m_CICL.GetWindowText(pomCIC->Cicl,4);
		m_HOME.GetWindowText(pomCIC->Home,4);
		pomCIC->Vafr = DateHourStringToDate(olVafrd,olVafrt);
		pomCIC->Vato = DateHourStringToDate(olVatod,olVatot);
		CDialog::OnOK();
	}
	else
	{
		Beep(440,70);
		MessageBox(olErrorText,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
		m_CNAM.SetFocus();
		m_CNAM.SetSel(0,-1);
	}
}

//----------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------

void CheckinCounterDlg::MakeNoavTable(CString opTabn, CString opBurn)
{
	CRect olRectBorder;
	m_NOAVFRAME.GetWindowRect( olRectBorder );
	ScreenToClient(olRectBorder);
	olRectBorder.DeflateRect(2,2);

    pomTable->SetTableData(this, olRectBorder.left, olRectBorder.right, olRectBorder.top, olRectBorder.bottom);
	pomTable->SetSelectMode(0);
	pomTable->SetShowSelection(TRUE);
	pomTable->ResetContent();

	CCSPtrArray <TABLE_HEADER_COLUMN> olHeaderDataArray;
	TABLE_HEADER_COLUMN rlHeader;
	rlHeader.Alignment = COLALIGN_LEFT;
	rlHeader.Font = &ogCourier_Regular_10;
	int ilPos = 1;
	rlHeader.Length = 133; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING101),ilPos++,true,'|');
	olHeaderDataArray.NewAt(olHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 133; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING101),ilPos++,true,'|');
	olHeaderDataArray.NewAt(olHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 58; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING101),ilPos++,true,'|');
	olHeaderDataArray.NewAt(olHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 42; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING101),ilPos++,true,'|');
	olHeaderDataArray.NewAt(olHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 42; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING101),ilPos++,true,'|');
	olHeaderDataArray.NewAt(olHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 8; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING101),ilPos++,true,'|');
	olHeaderDataArray.NewAt(olHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 332; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING101),ilPos++,true,'|');
	olHeaderDataArray.NewAt(olHeaderDataArray.GetSize(), rlHeader);

	pomTable->SetHeaderFields(olHeaderDataArray);
	pomTable->SetDefaultSeparator();
	olHeaderDataArray.DeleteAll();

	char clWhere[100];
	CCSPtrArray<BLKDATA> olBlkCPA;
	sprintf(clWhere,"WHERE TABN='%s' AND BURN='%s'",opTabn,opBurn);
	ogBlkData.ReadSpecialData(&omBlkPtrA,clWhere,"",false);
	int ilLines = omBlkPtrA.GetSize();

	pomTable->SetDefaultSeparator();
	pomTable->SetTableEditable(FALSE);

	TABLE_COLUMN rlColumnData;
	rlColumnData.Font = &ogCourier_Regular_10;
	rlColumnData.BkColor = WHITE;
	rlColumnData.Alignment = COLALIGN_LEFT;
	
    for (int ilLineNo = 0; ilLineNo < ilLines; ilLineNo++)
	{
		CCSPtrArray<TABLE_COLUMN> olColList;

		rlColumnData.Text = omBlkPtrA[ilLineNo].Nafr.Format("%d.%m.%Y %H:%M");
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omBlkPtrA[ilLineNo].Nato.Format("%d.%m.%Y %H:%M");
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omBlkPtrA[ilLineNo].Days;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		char pclTmp[10]="";
		rlColumnData.Text = "";
		if(strlen(omBlkPtrA[ilLineNo].Tifr) > 0)
		{
			sprintf(pclTmp, "%s:%s", CString(omBlkPtrA[ilLineNo].Tifr).Left(2), CString(omBlkPtrA[ilLineNo].Tifr).Right(2));
			rlColumnData.Text = pclTmp;
		}
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		rlColumnData.Text = "";
		if(strlen(omBlkPtrA[ilLineNo].Tito) > 0)
		{
			sprintf(pclTmp, "%s:%s", CString(omBlkPtrA[ilLineNo].Tito).Left(2), CString(omBlkPtrA[ilLineNo].Tito).Right(2));
			rlColumnData.Text = pclTmp;
		}
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		(*omBlkPtrA[ilLineNo].Type == 'X') ? rlColumnData.Text = cgYes : rlColumnData.Text = cgNo;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omBlkPtrA[ilLineNo].Resn;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		pomTable->AddTextLine(olColList,(void*)NULL);
		olColList.DeleteAll();
	}
	pomTable->DisplayTable();
}

//----------------------------------------------------------------------------------------

void CheckinCounterDlg::ChangeNoavTable(BLKDATA *prpBlk, int ipLineNo)
{
	if(prpBlk != NULL)
	{
		TABLE_COLUMN rlColumn;
		CCSPtrArray <TABLE_COLUMN> olLine;

		rlColumn.Lineno = 0;
		rlColumn.VerticalSeparator = SEPA_NONE;
		rlColumn.SeparatorType = SEPA_NONE;
		rlColumn.BkColor = WHITE;
		rlColumn.Font = &ogCourier_Regular_10;
		rlColumn.Alignment = COLALIGN_LEFT;

		int ilColumnNo = 0;
		rlColumn.Text = prpBlk->Nafr.Format("%d.%m.%Y %H:%M");
		rlColumn.Columnno = ilColumnNo++;
		olLine.NewAt(olLine.GetSize(), rlColumn);
		rlColumn.Text = prpBlk->Nato.Format("%d.%m.%Y %H:%M"); 
		rlColumn.Columnno = ilColumnNo++;
		olLine.NewAt(olLine.GetSize(), rlColumn);
		rlColumn.Text = prpBlk->Days;
		rlColumn.Columnno = ilColumnNo++;
		olLine.NewAt(olLine.GetSize(), rlColumn);

		char pclTmp[10]="";
		rlColumn.Text = "";
		if(strlen(prpBlk->Tifr) > 0)
		{
			sprintf(pclTmp, "%s:%s", CString(prpBlk->Tifr).Left(2), CString(prpBlk->Tifr).Right(2));
			rlColumn.Text = pclTmp;
		}
		rlColumn.Columnno = ilColumnNo++;
		olLine.NewAt(olLine.GetSize(), rlColumn);

		rlColumn.Text = "";
		if(strlen(prpBlk->Tito) > 0)
		{
			sprintf(pclTmp, "%s:%s", CString(prpBlk->Tito).Left(2), CString(prpBlk->Tito).Right(2));
			rlColumn.Text = pclTmp;
		}
		rlColumn.Columnno = ilColumnNo++;
		olLine.NewAt(olLine.GetSize(), rlColumn);

		(*prpBlk->Type == 'X') ? rlColumn.Text = cgYes : rlColumn.Text = cgNo;
		rlColumn.Columnno = ilColumnNo++;
		olLine.NewAt(olLine.GetSize(), rlColumn);
		rlColumn.Text = prpBlk->Resn;
		rlColumn.Columnno = ilColumnNo++;
		olLine.NewAt(olLine.GetSize(), rlColumn);

		if(ipLineNo>-1)
		{
			pomTable->ChangeTextLine(ipLineNo, &olLine, (void *)prpBlk);
			pomTable->DisplayTable();
		}
		else
		{
			if(prpBlk != NULL)
			{
				pomTable->AddTextLine(olLine, (void *)prpBlk);
				pomTable->DisplayTable();
			}
		}
		olLine.DeleteAll();
	}
	else
	{
		if(ipLineNo>-1)
		{
			pomTable->DeleteTextLine(ipLineNo);
			pomTable->DisplayTable();
		}
	}
}

//----------------------------------------------------------------------------------------

LONG CheckinCounterDlg::OnTableLButtonDblclk(UINT wParam, LONG lParam) 
{
	if(ogPrivList.GetStat("CHECKINCOUNTERDLG.m_NOAVCHA") == '1')
	{
		int ilLineNo = pomTable->GetCurSel();
		if (ilLineNo == -1)
		{
			MessageBox(LoadStg(IDS_STRING148),LoadStg(IDS_STRING149),MB_ICONEXCLAMATION);
		}
		else
		{
			BLKDATA *prlBlk = &omBlkPtrA[ilLineNo];
			if (prlBlk != NULL)
			{
				BLKDATA rlBlk = *prlBlk;
				strcpy(rlBlk.Tabn, CString("CIC"));
				NotAvailableDlg *polDlg = new NotAvailableDlg(&rlBlk,this);
				m_CNAM.GetWindowText(polDlg->omName);
				if(polDlg->DoModal() == IDOK)
				{
					*prlBlk = rlBlk;
					if(prlBlk->IsChanged != DATA_NEW) prlBlk->IsChanged = DATA_CHANGED;
					ChangeNoavTable(prlBlk, ilLineNo);
				}
				delete polDlg;
			}
		}
	}
	return 0L;
}

//----------------------------------------------------------------------------------------

void CheckinCounterDlg::OnNoavNew() 
{
		BLKDATA rlBlk;
		NotAvailableDlg *polDlg = new NotAvailableDlg(&rlBlk,this);
		m_CNAM.GetWindowText(polDlg->omName);
		if(polDlg->DoModal() == IDOK)
		{
			BLKDATA *prlBlk = new BLKDATA;
			strcpy(rlBlk.Tabn, CString("CIC"));
			*prlBlk = rlBlk;
			omBlkPtrA.Add(prlBlk);
			prlBlk->IsChanged = DATA_NEW;
			ChangeNoavTable(prlBlk, -1);
		}
		delete polDlg;
}

//----------------------------------------------------------------------------------------

void CheckinCounterDlg::OnNoavDel() 
{
	int ilLineNo = pomTable->GetCurSel();
	if (ilLineNo == -1)
	{
		MessageBox(LoadStg(IDS_STRING148),LoadStg(IDS_STRING149),MB_ICONEXCLAMATION);
	}
	else
	{
		if (&omBlkPtrA[ilLineNo] != NULL)
		{
			BLKDATA *prlBlk = new BLKDATA;
			*prlBlk = omBlkPtrA[ilLineNo];
			prlBlk->IsChanged = DATA_DELETED;
			omDeleteBlkPtrA.Add(prlBlk);
			omBlkPtrA.DeleteAt(ilLineNo);
			ChangeNoavTable(NULL, ilLineNo);
		}
	}
}

//----------------------------------------------------------------------------------------
