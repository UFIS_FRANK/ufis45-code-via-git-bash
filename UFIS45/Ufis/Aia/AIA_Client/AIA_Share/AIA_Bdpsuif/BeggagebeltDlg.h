#if !defined(AFX_BEGGAGEBELTDLG_H__E14A38E1_30C3_11D1_B39C_0000C016B067__INCLUDED_)
#define AFX_BEGGAGEBELTDLG_H__E14A38E1_30C3_11D1_B39C_0000C016B067__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// BeggagebeltDlg.h : header file
//
#include "CedaBLTData.h"
#include "CedaBlkData.h"
#include "CCSEdit.h"
#include "CCSTable.h"

/////////////////////////////////////////////////////////////////////////////
// BeggagebeltDlg dialog

class BeggagebeltDlg : public CDialog
{
// Construction
public:
	BeggagebeltDlg(BLTDATA *popBLT,CWnd* pParent = NULL);   // standard constructor
	~BeggagebeltDlg();

	CCSPtrArray<BLKDATA> omBlkPtrA;
	CCSPtrArray<BLKDATA> omDeleteBlkPtrA;

// Dialog Data
	//{{AFX_DATA(BeggagebeltDlg)
	enum { IDD = IDD_BEGGAGEBELTDLG };
	CButton	m_NOAVNEW;
	CButton	m_NOAVDEL;
	CStatic	m_NOAVFRAME;
	CCSEdit	m_DEFD;
	CCSEdit	m_MXF;
	CButton	m_OK;
	CString	m_Caption;
	CCSEdit	m_CDATD;
	CCSEdit	m_CDATT;
	CCSEdit	m_LSTUD;
	CCSEdit	m_LSTUT;
	CCSEdit	m_USEU;
	CCSEdit	m_USEC;
	CCSEdit	m_BNAM;
	CCSEdit	m_TELE;
	CCSEdit	m_VAFRD;
	CCSEdit	m_VAFRT;
	CCSEdit	m_VATOD;
	CCSEdit	m_VATOT;
	CCSEdit	m_GRUP;
	CCSEdit	m_TERM;
	CCSEdit	m_STAT;
	CCSEdit	m_BLTT;
	CCSEdit	m_HOME;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(BeggagebeltDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(BeggagebeltDlg)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	afx_msg LONG OnTableLButtonDblclk(UINT wParam, LONG lParam);
	afx_msg void OnNoavDel();
	afx_msg void OnNoavNew();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

private:

	BLTDATA *pomBLT;
	CCSTable	*pomTable;

	void MakeNoavTable(CString opTabn, CString opBurn);
	void ChangeNoavTable(BLKDATA *prpBlk, int ipLineNo);


};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_BEGGAGEBELTDLG_H__E14A38E1_30C3_11D1_B39C_0000C016B067__INCLUDED_)
