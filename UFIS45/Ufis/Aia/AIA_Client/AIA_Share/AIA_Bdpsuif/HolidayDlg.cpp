// HolidayDlg.cpp : implementation file
//

#include "stdafx.h"
#include "BDPSUIF.h"
#include "HolidayDlg.h"
#include "PrivList.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// HolidayDlg dialog


HolidayDlg::HolidayDlg(HOLDATA *popHol,CWnd* pParent /*=NULL*/) : CDialog(HolidayDlg::IDD, pParent)
{
	pomHol = popHol;

	//{{AFX_DATA_INIT(HolidayDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void HolidayDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(HolidayDlg)
	DDX_Control(pDX, IDOK, m_OK);
	DDX_Control(pDX, IDC_CDAT_D, m_CDATD);
	DDX_Control(pDX, IDC_CDAT_T, m_CDATT);
	DDX_Control(pDX, IDC_LSTU_D, m_LSTUD);
	DDX_Control(pDX, IDC_LSTU_T, m_LSTUT);
	DDX_Control(pDX, IDC_USEU,	 m_USEU);
	DDX_Control(pDX, IDC_USEC,	 m_USEC);
	DDX_Control(pDX, IDC_REMA,	 m_REMA);
	DDX_Control(pDX, IDC_TYPE,	 m_TYPE);
	DDX_Control(pDX, IDC_HDAY,	 m_HDAY);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(HolidayDlg, CDialog)
	//{{AFX_MSG_MAP(HolidayDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// HolidayDlg message handlers
//----------------------------------------------------------------------------------------

BOOL HolidayDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	m_Caption = LoadStg(IDS_STRING198) + " <" + m_Caption + ">";
	SetWindowText(m_Caption);

	char clStat = ogPrivList.GetStat("HOLIDAYDLG.m_OK");
	SetWndStatAll(clStat,m_OK);

	//------------------------------------
	m_CDATD.SetBKColor(SILVER);
	m_CDATD.SetInitText(pomHol->Cdat.Format("%d.%m.%Y"));
	//m_CDATD.SetSecState(ogPrivList.GetStat("HOLIDAYDLG.m_CDAT"));
	m_CDATT.SetBKColor(SILVER);
	m_CDATT.SetInitText(pomHol->Cdat.Format("%H:%M"));
	//m_CDATT.SetSecState(ogPrivList.GetStat("HOLIDAYDLG.m_CDAT"));
	//------------------------------------
	m_LSTUD.SetBKColor(SILVER);
	m_LSTUD.SetInitText(pomHol->Lstu.Format("%d.%m.%Y"));
	//m_LSTUD.SetSecState(ogPrivList.GetStat("HOLIDAYDLG.m_LSTU"));
	m_LSTUT.SetBKColor(SILVER);
	m_LSTUT.SetInitText(pomHol->Lstu.Format("%H:%M"));
	//m_LSTUT.SetSecState(ogPrivList.GetStat("HOLIDAYDLG.m_LSTU"));
	//------------------------------------
	m_USEC.SetBKColor(SILVER);
	m_USEC.SetInitText(pomHol->Usec);
	//m_USEC.SetSecState(ogPrivList.GetStat("HOLIDAYDLG.m_USEC"));
	//------------------------------------
	m_USEU.SetBKColor(SILVER);
	m_USEU.SetInitText(pomHol->Useu);
	//m_USEU.SetSecState(ogPrivList.GetStat("HOLIDAYDLG.m_USEU"));
	//------------------------------------//WHITE,YELLOW,SILVER,RED,LYELLOW
	m_HDAY.SetTypeToDate(true);
	m_HDAY.SetTextErrColor(RED);
	m_HDAY.SetBKColor(YELLOW);
	m_HDAY.SetInitText(pomHol->Hday.Format("%d.%m.%Y"));
	//m_HDAY.SetSecState(ogPrivList.GetStat("HOLIDAYDLG.m_HDAY"));
	//------------------------------------
	m_TYPE.SetFormat("x|#");
	m_TYPE.SetBKColor(YELLOW);
	m_TYPE.SetTextErrColor(RED);
	m_TYPE.SetInitText(pomHol->Type);
	//m_TYPE.SetSecState(ogPrivList.GetStat("HOLIDAYDLG.m_TYPE"));
	//------------------------------------
	m_REMA.SetTypeToString("X(40)",40,1);
	m_REMA.SetBKColor(YELLOW);
	m_REMA.SetTextErrColor(RED);
	m_REMA.SetInitText(pomHol->Rema);
	//m_REMA.SetSecState(ogPrivList.GetStat("HOLIDAYDLG.m_REMA"));
	//------------------------------------
	return TRUE;
}

//----------------------------------------------------------------------------------------

void HolidayDlg::OnOK() 
{
	AfxGetApp()->DoWaitCursor(1);
	CString olErrorText;
	bool ilStatus = true;

	if(m_HDAY.GetStatus() == false)
	{
		ilStatus = false;
		if(m_HDAY.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING107) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING107) + ogNotFormat;
		}
	}
	if(m_TYPE.GetStatus() == false)
	{
		ilStatus = false;
		if(m_TYPE.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING106) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING106) + ogNotFormat;
		}
	}
	if(m_REMA.GetStatus() == false)
	{
		ilStatus = false;
		if(m_REMA.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING501) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING501) + ogNotFormat;
		}
	}

	///////////////////////////////////////////////////////////////////////////

	//Konsistenz-Check//
	if(ilStatus == true)
	{
		CString olHday;
		CTime olTmpTime;
		char clWhere[100];
		CCSPtrArray<HOLDATA> olHolCPA;

		m_HDAY.GetWindowText(olHday);
		olTmpTime = DateHourStringToDate(olHday,CString("00:00"));
		olHday = olTmpTime.Format("%Y%m%d000000");
		sprintf(clWhere,"WHERE HDAY='%s'",olHday);
		if(ogHolData.ReadSpecialData(&olHolCPA,clWhere,"URNO",false) == true)
		{
			if(olHolCPA[0].Urno != pomHol->Urno)
			{
				ilStatus = false;
				olErrorText += LoadStg(IDS_STRING107) + LoadStg(IDS_EXIST);
			}
		}
		olHolCPA.DeleteAll();
	}

	///////////////////////////////////////////////////////////////////////////
	AfxGetApp()->DoWaitCursor(-1);
	if (ilStatus == true)
	{
		CString olDay;
		m_HDAY.GetWindowText(olDay);
		pomHol->Hday = DateHourStringToDate(olDay,CString("00:00"));

		m_TYPE.GetWindowText(pomHol->Type,2);
		m_REMA.GetWindowText(pomHol->Rema,41);

		CDialog::OnOK();
	}
	else
	{
		Beep(440,70);
		MessageBox(olErrorText,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
		m_HDAY.SetFocus();
		m_HDAY.SetSel(0,-1);
	}
}

//----------------------------------------------------------------------------------------
