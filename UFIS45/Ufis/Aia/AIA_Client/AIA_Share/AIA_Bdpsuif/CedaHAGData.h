// CedaHAGData.h

#ifndef __CEDAHAGDATA__
#define __CEDAHAGDATA__
 
#include "stdafx.h"
#include "basicdata.h"

//---------------------------------------------------------------------------------------------------------
// Record structure declaration

struct HAGDATA 
{
	long 	 Urno; 		// Eindeutige Datensatz-Nr.
	CTime	 Cdat;		// Erstellungsdatum
	char 	 Usec[34]; 	// Anwender (Ersteller)
	CTime	 Lstu;	 	// Datum letzte �nderung
	char 	 Prfl[3]; 	// Protokollierungskennzeichen
	char 	 Useu[34]; 	// Anwender (letzte �nderung)
	char 	 Hnam[42]; 	// Name Abfertigungsagent
	char 	 Hsna[7]; 	// Abfertigungsagent Kurzzeichen
	char 	 Tele[12]; 	// Telefonnummer
	char 	 Faxn[12]; 	// FAX-Nummer
	CTime	 Vafr;	 	// G�ltig von
	CTime	 Vato;	 	// G�ltig bis

	//DataCreated by this class
	int		 IsChanged;

	HAGDATA(void)
	{ 
		memset(this,'\0',sizeof(*this));
		Cdat=-1;
		Lstu=-1;
		Vafr=-1;
		Vato=-1;
	}

}; // end HAGDataStrukt


//---------------------------------------------------------------------------------------------------------
// Class declaratino


class CedaHAGData: public CCSCedaData
{
// Attributes
public:
    CMapPtrToPtr omUrnoMap;

    CCSPtrArray<HAGDATA> omData;
	char pcmHAGFieldList[2048];

// Operations
public:
    CedaHAGData();
	~CedaHAGData();

	virtual void GetDataInfo(CStringArray &ropFields, CStringArray &ropDesription, CStringArray &ropType);
	void Register(void);
	void ClearAll(void);

    bool Read(char *pcpWhere = NULL);
	bool ReadSpecialData(CCSPtrArray<HAGDATA> *popHag,char *pspWhere,char *pspFieldList,bool ipSYS=true);
	bool InsertHAG(HAGDATA *prpHAG,BOOL bpSendDdx = TRUE);
	bool InsertHAGInternal(HAGDATA *prpHAG);
	bool UpdateHAG(HAGDATA *prpHAG,BOOL bpSendDdx = TRUE);
	bool UpdateHAGInternal(HAGDATA *prpHAG);
	bool DeleteHAG(long lpUrno);
	bool DeleteHAGInternal(HAGDATA *prpHAG);
	HAGDATA  *GetHAGByUrno(long lpUrno);
	bool SaveHAG(HAGDATA *prpHAG);
	void ProcessHAGBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

	// Private methods
private:
    void PrepareHAGData(HAGDATA *prpHAGData);
};

//---------------------------------------------------------------------------------------------------------

#endif //__CEDAHAGDATA__
