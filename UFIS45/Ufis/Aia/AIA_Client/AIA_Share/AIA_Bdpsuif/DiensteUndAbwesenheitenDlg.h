#if !defined(AFX_DIENSTEUNDABWESENHEITENDLG_H__AB36A834_7540_11D1_B430_0000B45A33F5__INCLUDED_)
#define AFX_DIENSTEUNDABWESENHEITENDLG_H__AB36A834_7540_11D1_B430_0000B45A33F5__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// DiensteUndAbwesenheitenDlg.h : header file
//
#include "CedaOdaData.h"
#include "CedaCotData.h"
#include "CCSEdit.h"
/////////////////////////////////////////////////////////////////////////////
// DiensteUndAbwesenheitenDlg dialog

class DiensteUndAbwesenheitenDlg : public CDialog
{
// Construction
public:
	DiensteUndAbwesenheitenDlg(ODADATA *popOda, CWnd* pParent = NULL);   // standard constructor

	ODADATA *pomOda;
// Dialog Data
	//{{AFX_DATA(DiensteUndAbwesenheitenDlg)
	enum { IDD = IDD_DIENSTEDLG };
	CButton	m_Free;
	CButton	m_R_Tatp0;
	CButton	m_R_Tatp1;
	CButton	m_R_Tatp2;
	CButton	m_R_Tatp3;
	CButton	m_R_Tatp4;
	CButton	m_C_Upln;
	CButton	m_C_Tsap;
	CButton	m_OK;
	CCSEdit	m_CDATD;
	CCSEdit	m_CDATT;
	CCSEdit	m_CTRC;
	CCSEdit	m_CTRC2;
	CCSEdit	m_DURA;
	CCSEdit	m_LSTUD;
	CCSEdit	m_LSTUT;
	CCSEdit	m_REMA;
	CCSEdit	m_SDAC;
	CCSEdit	m_SDAK;
	CCSEdit	m_SDAN;
	CCSEdit	m_SDAS;
	CCSEdit	m_USEC;
	CCSEdit	m_USEU;
	CString m_Caption;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(DiensteUndAbwesenheitenDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(DiensteUndAbwesenheitenDlg)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	afx_msg void OnBAwCot();
    afx_msg LONG OnKillfocus(UINT wParam, LONG lParam);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DIENSTEUNDABWESENHEITENDLG_H__AB36A834_7540_11D1_B430_0000B45A33F5__INCLUDED_)
