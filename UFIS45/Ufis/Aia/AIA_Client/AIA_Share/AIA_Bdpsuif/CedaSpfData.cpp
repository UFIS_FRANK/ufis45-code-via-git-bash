// CedaSpfData.cpp
 
#include "stdafx.h"
#include "CedaSpfData.h"
#include "resource.h"


void ProcessSpfCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName);


//--CEDADATA-----------------------------------------------------------------------------------------------

CedaSpfData::CedaSpfData() : CCSCedaData(&ogCommHandler)
{
	// Create an array of CEDARECINFO for SPFDataStruct
	BEGIN_CEDARECINFO(SPFDATA,SPFDataRecInfo)
		FIELD_LONG		(Urno,"URNO")
		FIELD_LONG		(Surn,"SURN")
		FIELD_CHAR_TRIM	(Code,"CODE")
		FIELD_OLEDATE	(Vpfr,"VPFR")
		FIELD_OLEDATE	(Vpto,"VPTO")
#ifndef CLIENT_HAJ
		//*** 27.08.99 SHA ***
		FIELD_CHAR_TRIM	(Act3,"ACT3")
		FIELD_CHAR_TRIM	(Act5,"ACT5")
		FIELD_CHAR_TRIM	(Alc2,"ALC2")
		FIELD_CHAR_TRIM	(Alc3,"ALC3")
#endif
		END_CEDARECINFO //(SPFDataStruct)

	// FIELD_LONG, FIELD_DATE 
	// Copy the record structure
	for (int i=0; i< sizeof(SPFDataRecInfo)/sizeof(SPFDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&SPFDataRecInfo[i],sizeof(CEDARECINFO));
		omRecInfo.Add(prpCedaRecInfo);
	} // end for

	// initialize field names
	strcpy(pcmTableName,"SPF");

#ifdef CLIENT_HAJ
	strcpy(pcmListOfFields,"URNO,SURN,CODE,VPFR,VPTO");
#else
	strcpy(pcmListOfFields,"URNO,SURN,CODE,VPFR,VPTO,ACT3,ACT5,ALC2,ALC3");
#endif
	pcmFieldList = pcmListOfFields; //because in CedaData pcmFieldList is just a pointer

	omData.RemoveAll();
}

//---------------------------------------------------------------------------------------------------------

void CedaSpfData::GetDataInfo(CStringArray &ropFields, CStringArray &ropDesription, CStringArray &ropType)
{
	ropFields.Add("URNO");
	ropFields.Add("SURN");
	ropFields.Add("CODE");
	ropFields.Add("VPFR");
	ropFields.Add("VPTO");
	//*** 27.08.99 SHA ***
	ropFields.Add("ACT3");
	ropFields.Add("ACT5");
	ropFields.Add("ALC2");
	ropFields.Add("ALC3");

	ropDesription.Add(LoadStg(IDS_STRING346));
	ropDesription.Add(LoadStg(IDS_STRING464));
	ropDesription.Add(LoadStg(IDS_STRING465));
	ropDesription.Add(LoadStg(IDS_STRING230));
	ropDesription.Add(LoadStg(IDS_STRING231));
	//*** 27.08.99 SHA ***
	ropDesription.Add("ACT3");
	ropDesription.Add("ACT5");
	ropDesription.Add("ALC2");
	ropDesription.Add("ALC3");

	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("Date");
	ropType.Add("Date");
	//*** 19.08.99 SHA ***
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");

}

//--REGISTER----------------------------------------------------------------------------------------------

void CedaSpfData::Register(void)
{
	ogDdx.Register((void *)this,BC_SPF_CHANGE,	CString("SPFDATA"), CString("Spf-changed"),	ProcessSpfCf);
	ogDdx.Register((void *)this,BC_SPF_NEW,		CString("SPFDATA"), CString("Spf-new"),		ProcessSpfCf);
	ogDdx.Register((void *)this,BC_SPF_DELETE,	CString("SPFDATA"), CString("Spf-deleted"),	ProcessSpfCf);
}

//--~CEDADATA----------------------------------------------------------------------------------------------

CedaSpfData::~CedaSpfData(void)
{
	omRecInfo.DeleteAll();
	ClearAll();
}

//--CLEAR-ALL----------------------------------------------------------------------------------------------

void CedaSpfData::ClearAll(bool bpWithRegistration)
{
    omUrnoMap.RemoveAll();
    omData.DeleteAll();
	if(bpWithRegistration)
	{
		ogDdx.UnRegister(this,NOTUSED);
	}
}

//--READ-ALL-----------------------------------------------------------------------------------------------

bool CedaSpfData::Read(char *pspWhere /*NULL*/)
{
    // Select data from the database
	bool ilRc = true;
    omUrnoMap.RemoveAll();
    omData.DeleteAll();
	if(pspWhere == NULL)
	{	
		ilRc = CedaAction("RT");
	}
	else
	{
		ilRc = CedaAction("RT", pspWhere);
	}
	if (ilRc != true)
	{
		return ilRc;
	}
	// Load data from CedaData into the dynamic array of record
	for (int ilLc = 0; ilRc == true; ilLc++)
	{
		SPFDATA *prlSpf = new SPFDATA;
		if ((ilRc = GetFirstBufferRecord(prlSpf)) == true)
		{
			omData.Add(prlSpf);//Update omData
			omUrnoMap.SetAt((void *)prlSpf->Urno,prlSpf);
		}
		else
		{
			delete prlSpf;
		}
	}
    return true;
}

//--INSERT-------------------------------------------------------------------------------------------------

bool CedaSpfData::Insert(SPFDATA *prpSpf)
{
	prpSpf->IsChanged = DATA_NEW;
	if(Save(prpSpf) == false) return false; //Update Database
	InsertInternal(prpSpf);
    return true;
}

//--INSERT-INTERNAL--------------------------------------------------------------------------------------

bool CedaSpfData::InsertInternal(SPFDATA *prpSpf)
{
	ogDdx.DataChanged((void *)this, SPF_NEW,(void *)prpSpf ); //Update Viewer
	omData.Add(prpSpf);//Update omData
	omUrnoMap.SetAt((void *)prpSpf->Urno,prpSpf);
    return true;
}

//--DELETE-------------------------------------------------------------------------------------------------

bool CedaSpfData::Delete(long lpUrno)
{
	SPFDATA *prlSpf = GetSpfByUrno(lpUrno);
	if (prlSpf != NULL)
	{
		prlSpf->IsChanged = DATA_DELETED;
		if(Save(prlSpf) == false) return false; //Update Database
		DeleteInternal(prlSpf);
	}
    return true;
}

//--DELETE-INTERNAL----------------------------------------------------------------------------------------

bool CedaSpfData::DeleteInternal(SPFDATA *prpSpf)
{
	ogDdx.DataChanged((void *)this,SPF_DELETE,(void *)prpSpf); //Update Viewer
	omUrnoMap.RemoveKey((void *)prpSpf->Urno);
	int ilSpfCount = omData.GetSize();
	for (int ilLc = 0; ilLc < ilSpfCount; ilLc++)
	{
		if (omData[ilLc].Urno == prpSpf->Urno)
		{
			omData.DeleteAt(ilLc);//Update omData
			break;
		}
	}
    return true;
}

//--UPDATE-------------------------------------------------------------------------------------------------

bool CedaSpfData::Update(SPFDATA *prpSpf)
{
	if (GetSpfByUrno(prpSpf->Urno) != NULL)
	{
		if (prpSpf->IsChanged == DATA_UNCHANGED)
		{
			prpSpf->IsChanged = DATA_CHANGED;
		}
		if(Save(prpSpf) == false) return false; //Update Database
		UpdateInternal(prpSpf);
	}
    return true;
}

//--UPDATE-INTERNAL--------------------------------------------------------------------------------------------

bool CedaSpfData::UpdateInternal(SPFDATA *prpSpf)
{
	SPFDATA *prlSpf = GetSpfByUrno(prpSpf->Urno);
	if (prlSpf != NULL)
	{
		*prlSpf = *prpSpf; //Update omData
		ogDdx.DataChanged((void *)this,SPF_CHANGE,(void *)prlSpf); //Update Viewer
	}
    return true;
}

//--GET-BY-URNO--------------------------------------------------------------------------------------------

SPFDATA *CedaSpfData::GetSpfByUrno(long lpUrno)
{
	SPFDATA  *prlSpf;
	if (omUrnoMap.Lookup((void *)lpUrno,(void *& )prlSpf) == TRUE)
	{
		return prlSpf;
	}
	return NULL;
}

//--READSPECIALDATA-------------------------------------------------------------------------------------

bool CedaSpfData::ReadSpecialData(CCSPtrArray<SPFDATA> *popSpf,char *pspWhere,char *pspFieldList,bool ipSYS/*=true*/)
{
	bool ilRc = true;
	char pclFieldList[256] = " ";

	if(strlen(pspFieldList) > 0) 
	{
		strcpy(pclFieldList, pspFieldList);
	}
	if(ipSYS == true) 
	{
		if (CedaAction("SYS","SPF",pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	else
	{
		if (CedaAction("RT","SPF",pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	if(popSpf != NULL)
	{
		for (int ilLc = 0; ilRc == true; ilLc++)
		{
			SPFDATA *prpSpf = new SPFDATA;
			if ((ilRc = GetBufferRecord(ilLc,prpSpf,CString(pclFieldList))) == true)
			{
				popSpf->Add(prpSpf);
			}
			else
			{
				delete prpSpf;
			}
		}
		if(popSpf->GetSize() == 0) return false;
	}
    return true;
}

//--SAVE---------------------------------------------------------------------------------------------------

bool CedaSpfData::Save(SPFDATA *prpSpf)
{
	bool ilRc = true;
	CString olListOfData;
	char pclSelection[124];
	char pclData[524];

	if (prpSpf->IsChanged == DATA_UNCHANGED)
	{
		return true; // no change, nothing to do
	}
	switch(prpSpf->IsChanged)
	{
	case DATA_NEW:
		MakeCedaData(&omRecInfo,olListOfData,prpSpf);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("IRT","","",pclData);
		prpSpf->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_CHANGED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpSpf->Urno);
		MakeCedaData(&omRecInfo,olListOfData,prpSpf);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("URT",pclSelection,"",pclData);
		prpSpf->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_DELETED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpSpf->Urno);
		ilRc = CedaAction("DRT",pclSelection);
		break;
	}
    return ilRc;
}

//--PROCESS-CF---------------------------------------------------------------------------------------------

void  ProcessSpfCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	ogSpfData.ProcessBc(ipDDXType,vpDataPointer,ropInstanceName);
}

//--PROCESS-BC---------------------------------------------------------------------------------------------

void  CedaSpfData::ProcessBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	struct BcStruct *prlSpfData;
	prlSpfData = (struct BcStruct *) vpDataPointer;
	SPFDATA *prlSpf;
	if(ipDDXType == BC_SPF_NEW)
	{
		prlSpf = new SPFDATA;
		GetRecordFromItemList(prlSpf,prlSpfData->Fields,prlSpfData->Data);
		InsertInternal(prlSpf);
	}
	if(ipDDXType == BC_SPF_CHANGE)
	{
		long llUrno = GetUrnoFromSelection(prlSpfData->Selection);
		prlSpf = GetSpfByUrno(llUrno);
		if(prlSpf != NULL)
		{
			GetRecordFromItemList(prlSpf,prlSpfData->Fields,prlSpfData->Data);
			UpdateInternal(prlSpf);
		}
	}
	if(ipDDXType == BC_SPF_DELETE)
	{
		long llUrno;
		CString olSelection = (CString)prlSpfData->Selection;
		if (olSelection.Find('\'') != -1)
		{
			llUrno = GetUrnoFromSelection(prlSpfData->Selection);
		}
		else
		{
			int ilFirst = olSelection.Find("=")+2;
			int ilLast  = olSelection.GetLength();
			llUrno = atol(olSelection.Mid(ilFirst,ilLast-ilFirst));
		}
		prlSpf = GetSpfByUrno(llUrno);
		if (prlSpf != NULL)
		{
			DeleteInternal(prlSpf);
		}
	}
}

//---------------------------------------------------------------------------------------------------------
