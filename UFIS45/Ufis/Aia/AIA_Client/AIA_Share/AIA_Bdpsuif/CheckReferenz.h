// CheckReferenz.h

#ifndef _CHECKREFERENZ_
#define _CHECKREFERENZ_
 
#include "stdafx.h"

enum TableID
{
	_BSD,
	_ODA
};
//---------------------------------------------------------------------------------------------------------
// Class declaratino

class CheckReferenz
{
// Attributes
public:

// Operations
public:
    CheckReferenz();
	~CheckReferenz();
	
	//Reurn: >0 = Record exist, 0 = Record don't exist
	int Check(int ipTableID, long lpUrno, CString opCode);

// Private methods
private:

};

//---------------------------------------------------------------------------------------------------------

#endif //_CHECKREFERENZ_
