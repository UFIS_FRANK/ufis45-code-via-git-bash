// CedaSteData.h

#ifndef __CEDAPSTEDATA__
#define __CEDAPSTEDATA__
 
#include "stdafx.h"
#include "basicdata.h"
#include <afxdisp.h>

//---------------------------------------------------------------------------------------------------------

struct STEDATA 
{
	long			Urno;	 // Eindeutige Datensatz-Nr.
	long			Surn;	 // Referenz auf STF.URNO
	char			Code[7]; // Codereferenz ORG.CODE
	COleDateTime	Vpfr;	 // G�ltig von
	COleDateTime	Vpto;	 // G�ltig bis

	//DataCreated by this class
	int      IsChanged;

	//long, CTime
	STEDATA(void)
	{ memset(this,'\0',sizeof(*this));
	  //Vpfr=-1,Vpto=-1; //<zB.(FIELD_DATE Felder)
	}

}; // end STEDataStruct

//---------------------------------------------------------------------------------------------------------
// Class declaratino

class CedaSteData: public CCSCedaData
{
// Attributes
public:
    CMapPtrToPtr omUrnoMap;

    CCSPtrArray<STEDATA> omData;

	char pcmListOfFields[2048];

// OSteations
public:
    CedaSteData();
	~CedaSteData();
	virtual void GetDataInfo(CStringArray &ropFields, CStringArray &ropDesription, CStringArray &ropType);
	void Register(void);
	void ClearAll(bool bpWithRegistration = true);
    bool Read(char *pspWhere = NULL);
	bool Insert(STEDATA *prpSte);
	bool InsertInternal(STEDATA *prpSte);
	bool Update(STEDATA *prpSte);
	bool UpdateInternal(STEDATA *prpSte);
	bool Delete(long lpUrno);
	bool DeleteInternal(STEDATA *prpSte);
	bool ReadStecialData(CCSPtrArray<STEDATA> *popSte,char *pspWhere,char *pspFieldList,bool ipSYS=true);
	bool Save(STEDATA *prpSte);
	void ProcessBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);
	STEDATA  *GetSteByUrno(long lpUrno);


	// Private methods
private:
    void PrepareSteData(STEDATA *prpSteData);

};

//---------------------------------------------------------------------------------------------------------


#endif //__CEDAPSTEDATA__
