#ifndef __FLUGPLANSAISONTABLEVIEWER_H__
#define __FLUGPLANSAISONTABLEVIEWER_H__

#include "stdafx.h"
#include "CedaSeaData.h"
#include "CCSTable.h"
#include "CViewer.h"
#include "CCSPrint.h"


struct FLUGPLANSAISONTABLE_LINEDATA
{
	long	Urno;
	CString	Beme;
	CString	Seas;
	CString	Vpfr;
	CString	Vpto;
};

/////////////////////////////////////////////////////////////////////////////
// FlugplansaisonTableViewer

class FlugplansaisonTableViewer : public CViewer
{
// Constructions
public:
    FlugplansaisonTableViewer(CCSPtrArray<SEADATA> *popData);
    ~FlugplansaisonTableViewer();

    void Attach(CCSTable *popAttachWnd);
    virtual void ChangeViewTo(const char *pcpViewName);
// Internal data processing routines
private:
	bool IsPassFilter(SEADATA *prpFlugplansaison);
	int CompareFlugplansaison(FLUGPLANSAISONTABLE_LINEDATA *prpFlugplansaison1, FLUGPLANSAISONTABLE_LINEDATA *prpFlugplansaison2);
    void MakeLines();
	void MakeLine(SEADATA *prpFlugplansaison);
	bool FindLine(long lpUrno, int &rilLineno);
// Operations
public:
	void DeleteAll();
	void CreateLine(FLUGPLANSAISONTABLE_LINEDATA *prpFlugplansaison);
	void DeleteLine(int ipLineno);
// Window refreshing routines
public:
	void UpdateDisplay();
	CString Format(FLUGPLANSAISONTABLE_LINEDATA *prpLine);
	void ProcessFlugplansaisonChange(SEADATA *prpFlugplansaison);
	void ProcessFlugplansaisonDelete(SEADATA *prpFlugplansaison);
	bool FindFlugplansaison(char *prpFlugplansaisonKeya, char *prpFlugplansaisonKeyd, int& ilItem);
// Attributes used for filtering condition
private:
	bool bmIsFromSearch;
// Attributes
private:
    CCSTable *pomFlugplansaisonTable;
	CCSPtrArray<SEADATA> *pomData;
// Methods which handle changes (from Data Distributor)
public:
    CCSPtrArray<FLUGPLANSAISONTABLE_LINEDATA> omLines;
///////
	CCSPtrArray<TABLE_HEADER_COLUMN> omHeaderDataArray;
//Print 
	void PrintTableView();
	bool PrintTableLine(FLUGPLANSAISONTABLE_LINEDATA *prpLine,bool bpLastLine);
	bool PrintTableHeader();
	CCSPrint *pomPrint;
	//CBitmap omBitmap;
	CString omTableName;

};

#endif //__FLUGPLANSAISONTABLEVIEWER_H__
