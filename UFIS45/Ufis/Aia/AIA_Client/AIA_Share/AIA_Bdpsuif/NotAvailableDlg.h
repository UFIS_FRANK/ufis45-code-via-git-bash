#ifndef AFX_NOTAVAILABLEDLG_H__1FA7A531_43D0_11D2_802A_004095434A85__INCLUDED_
#define AFX_NOTAVAILABLEDLG_H__1FA7A531_43D0_11D2_802A_004095434A85__INCLUDED_

// NotAvailableDlg.h : Header-Datei
//
#include "CedaBlkData.h"
#include "CCSEdit.h"
#include <AatBitmapComboBox.h>

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld NotAvailableDlg 

class NotAvailableDlg : public CDialog
{
// Konstruktion
public:
	NotAvailableDlg(BLKDATA *popBlk,CWnd* pParent = NULL);   // standard constructor

	CString omName;

// Dialogfelddaten
	//{{AFX_DATA(NotAvailableDlg)
	enum { IDD = IDD_NOTAVAILABLEDLG };
	CButton	m_BLOCKTYPE;
	CButton	m_OK;
	CButton	m_CHANGEY;
	CButton	m_CHANGEN;
	CButton	m_DAYS_T;
	CButton	m_DAYS_7;
	CButton	m_DAYS_6;
	CButton	m_DAYS_5;
	CButton	m_DAYS_4;
	CButton	m_DAYS_2;
	CButton	m_DAYS_3;
	CButton	m_DAYS_1;
	CCSEdit	m_NAME;
	CCSEdit	m_NAFRD;
	CCSEdit	m_NAFRT;
	CCSEdit	m_NATOD;
	CCSEdit	m_NATOT;
	CCSEdit	m_RESN;
	CCSEdit	m_TIFR;
	CCSEdit	m_TITO;
	AatBitmapComboBox	m_BlockingBitmap;
	//}}AFX_DATA


// Überschreibungen
	// Vom Klassen-Assistenten generierte virtuelle Funktionsüberschreibungen
	//{{AFX_VIRTUAL(NotAvailableDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	//}}AFX_VIRTUAL

// Implementierung
protected:

	// Generierte Nachrichtenzuordnungsfunktionen
	//{{AFX_MSG(NotAvailableDlg)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	afx_msg void OnDaysT();
	afx_msg void OnDays17();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

private:

	BLKDATA *pomBlk;

};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio fügt zusätzliche Deklarationen unmittelbar vor der vorhergehenden Zeile ein.

#endif // AFX_NOTAVAILABLEDLG_H__1FA7A531_43D0_11D2_802A_004095434A85__INCLUDED_
