#if !defined(AFX_EXITDLG_H__B35519F1_3320_11D1_B39D_0000C016B067__INCLUDED_)
#define AFX_EXITDLG_H__B35519F1_3320_11D1_B39D_0000C016B067__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// ExitDlg.h : header file
//
#include "CedaEXTData.h"
#include "CCSEdit.h"
/////////////////////////////////////////////////////////////////////////////
// ExitDlg dialog

class ExitDlg : public CDialog
{
// Construction
public:
	ExitDlg(EXTDATA *popEXT,CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(ExitDlg)
	enum { IDD = IDD_EXITDLG };
	CButton	m_OK;
	CString	m_Caption;
	CCSEdit	m_CDATD;
	CCSEdit	m_CDATT;
	CCSEdit	m_LSTUD;
	CCSEdit	m_LSTUT;
	CCSEdit	m_NAFRD;
	CCSEdit	m_NAFRT;
	CCSEdit	m_NATOD;
	CCSEdit	m_NATOT;
	CCSEdit	m_USEU;
	CCSEdit	m_USEC;
	CCSEdit	m_RESN;
	CCSEdit	m_ENAM;
	CCSEdit	m_TELE;
	CCSEdit	m_VAFRD;
	CCSEdit	m_VAFRT;
	CCSEdit	m_VATOD;
	CCSEdit	m_VATOT;
	CCSEdit	m_GRUP;
	CCSEdit	m_TERM;
	CCSEdit	m_HOME;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(ExitDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(ExitDlg)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

private:

	EXTDATA *pomEXT;

};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_EXITDLG_H__B35519F1_3320_11D1_B39D_0000C016B067__INCLUDED_)
