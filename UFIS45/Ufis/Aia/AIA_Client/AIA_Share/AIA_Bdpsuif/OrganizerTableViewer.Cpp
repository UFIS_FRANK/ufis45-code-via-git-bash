// OrganizerTableViewer.cpp 
//

#include "stdafx.h"
#include "OrganizerTableViewer.h"
#include "CCSDdx.h"
#include "resource.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif


// Local function prototype
static void OrganizerTableCf(void *popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

//-----------------------------------------------------------------------------------------------
// OrganizerTableViewer
//

OrganizerTableViewer::OrganizerTableViewer(CCSPtrArray<CHTDATA> *popData)
{
	pomData = popData;

	bmIsFromSearch = FALSE;
    pomOrganizerTable = NULL;
    ogDdx.Register(this, CHT_NEW,	 CString("ORGANIZERTABLEVIEWER"), CString("Organizer New"),	 OrganizerTableCf);
    ogDdx.Register(this, CHT_CHANGE, CString("ORGANIZERTABLEVIEWER"), CString("Organizer Update"), OrganizerTableCf);
    ogDdx.Register(this, CHT_DELETE, CString("ORGANIZERTABLEVIEWER"), CString("Organizer Delete"), OrganizerTableCf);
}

//-----------------------------------------------------------------------------------------------

OrganizerTableViewer::~OrganizerTableViewer()
{
	ogDdx.UnRegister(this, NOTUSED);
    DeleteAll();
	omHeaderDataArray.DeleteAll();
}

//-----------------------------------------------------------------------------------------------

void OrganizerTableViewer::Attach(CCSTable *popTable)
{
    pomOrganizerTable = popTable;
}

//-----------------------------------------------------------------------------------------------

void OrganizerTableViewer::ChangeViewTo(const char *pcpViewName)
{
    DeleteAll();    
    MakeLines();
	UpdateDisplay();
}

//-----------------------------------------------------------------------------------------------

void OrganizerTableViewer::MakeLines()
{
	int ilOrganizerCount = pomData->GetSize();
	for (int ilLc = 0; ilLc < ilOrganizerCount; ilLc++)
	{
		CHTDATA *prlOrganizerData = &pomData->GetAt(ilLc);
		MakeLine(prlOrganizerData);
	}
}

//-----------------------------------------------------------------------------------------------

void OrganizerTableViewer::MakeLine(CHTDATA *prpOrganizer)
{

    //if( !IsPassFilter(prpOrganizer)) return;

    // Update viewer data for this shift record
    ORGANIZERTABLE_LINEDATA rlOrganizer;

	rlOrganizer.Urno = prpOrganizer->Urno; 
	rlOrganizer.Chtn = prpOrganizer->Chtn; 
	rlOrganizer.Chtc = prpOrganizer->Chtc; 
	rlOrganizer.Vafr = prpOrganizer->Vafr.Format("%d.%m.%Y %H:%M");
	rlOrganizer.Vato = prpOrganizer->Vato.Format("%d.%m.%Y %H:%M");

	CreateLine(&rlOrganizer);
}

//-----------------------------------------------------------------------------------------------

void OrganizerTableViewer::CreateLine(ORGANIZERTABLE_LINEDATA *prpOrganizer)
{
    int ilLineCount = omLines.GetSize();
    for (int ilLineno = ilLineCount; ilLineno > 0; ilLineno--)
        if (CompareOrganizer(prpOrganizer, &omLines[ilLineno-1]) >= 0)
	        break;							 // should be inserted after Lines[ilLineno-1]
	ORGANIZERTABLE_LINEDATA rlOrganizer;
	rlOrganizer = *prpOrganizer;
    omLines.NewAt(ilLineno, rlOrganizer);
}

//-----------------------------------------------------------------------------------------------
// UpdateDisplay: Load data selected by filter conditions to the display by using "omTable"

void OrganizerTableViewer::UpdateDisplay()
{
	int ilLines = omLines.GetSize();
	const int ilColumns = 3;	//<===== Spaltenanzahl setzen (von 0 ab)
	omHeaderDataArray.DeleteAll();

	pomOrganizerTable->SetShowSelection(TRUE);
	pomOrganizerTable->ResetContent();

	TABLE_HEADER_COLUMN rlHeader;
	rlHeader.Alignment = COLALIGN_LEFT;
	rlHeader.Font = &ogCourier_Regular_10;

	int ilPos = 1;
	rlHeader.Length = 33; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING276),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 498; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING276),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 133; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING276),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 133; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING276),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);

	pomOrganizerTable->SetHeaderFields(omHeaderDataArray);
	pomOrganizerTable->SetDefaultSeparator();
	pomOrganizerTable->SetTableEditable(FALSE);

	TABLE_COLUMN rlColumnData;
	rlColumnData.Font = &ogCourier_Regular_10;
	rlColumnData.BkColor = WHITE;
	rlColumnData.Alignment = COLALIGN_LEFT;
	
    for (int ilLineNo = 0; ilLineNo < ilLines; ilLineNo++)
	{
		CCSPtrArray<TABLE_COLUMN> olColList;

		rlColumnData.Text = omLines[ilLineNo].Chtc;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Chtn;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Vafr;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Vato;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		pomOrganizerTable->AddTextLine(olColList,(void*)NULL);
		olColList.DeleteAll();
	}
	pomOrganizerTable->DisplayTable();
}

//-----------------------------------------------------------------------------------------------

CString OrganizerTableViewer::Format(ORGANIZERTABLE_LINEDATA *prpLine)
{
    CString s;
	// ???
    return s;
}

//-----------------------------------------------------------------------------------------------

bool OrganizerTableViewer::FindOrganizer(char *pcpOrganizerKeya, char *pcpOrganizerKeyd, int& ilItem)
{
	int ilCount = omLines.GetSize();
    for (ilItem = 0; ilItem < ilCount; ilItem++)
    {
/*		if ( (omLines[ilItem].Keya == pcpOrganizerKeya) &&
			 (omLines[ilItem].Keyd == pcpOrganizerKeyd)    )
			return true;*/
	}
	return false;
}

//-----------------------------------------------------------------------------------------------

static void OrganizerTableCf(void *popInstance, int ipDDXType,
    void *vpDataPointer, CString &ropInstanceName)
{
    OrganizerTableViewer *polViewer = (OrganizerTableViewer *)popInstance;
    if (ipDDXType == CHT_CHANGE || ipDDXType == CHT_NEW) polViewer->ProcessOrganizerChange((CHTDATA *)vpDataPointer);
    if (ipDDXType == CHT_DELETE) polViewer->ProcessOrganizerDelete((CHTDATA *)vpDataPointer);
} 


//-----------------------------------------------------------------------------------------------

void OrganizerTableViewer::ProcessOrganizerChange(CHTDATA *prpOrganizer)
{
	int ilItem = 0;
	int ilColumnNo = 0;
	TABLE_COLUMN rlColumn;
	CCSPtrArray <TABLE_COLUMN> olLine;

	rlColumn.Lineno = ilItem;
	rlColumn.VerticalSeparator = SEPA_NONE;
	rlColumn.SeparatorType = SEPA_NONE;
	rlColumn.BkColor = WHITE;
	rlColumn.Font = &ogCourier_Regular_10;
	rlColumn.Alignment = COLALIGN_LEFT;

	rlColumn.Text = prpOrganizer->Chtc;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpOrganizer->Chtn;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpOrganizer->Vafr.Format("%d.%m.%Y %H:%M"); 
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpOrganizer->Vato.Format("%d.%m.%Y %H:%M");
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);


	if (FindLine(prpOrganizer->Urno, ilItem))
	{
        ORGANIZERTABLE_LINEDATA *prlLine = &omLines[ilItem];

		prlLine->Urno = prpOrganizer->Urno;
		prlLine->Chtn = prpOrganizer->Chtn;
		prlLine->Chtc = prpOrganizer->Chtc;
		prlLine->Vafr = prpOrganizer->Vafr.Format("%d.%m.%Y %H:%M");
		prlLine->Vato = prpOrganizer->Vato.Format("%d.%m.%Y %H:%M");

		pomOrganizerTable->ChangeTextLine(ilItem, &olLine, (void *)prlLine);
		pomOrganizerTable->DisplayTable();
	}
	else
	{
		MakeLine(prpOrganizer);
		if (FindLine(prpOrganizer->Urno, ilItem))
		{
	        ORGANIZERTABLE_LINEDATA *prlLine = &omLines[ilItem];
			if(prlLine != NULL)
			{
				pomOrganizerTable->AddTextLine(olLine, (void *)prlLine);
				pomOrganizerTable->DisplayTable();
			}
		}
	}
	olLine.DeleteAll();
}

//-----------------------------------------------------------------------------------------------

void OrganizerTableViewer::ProcessOrganizerDelete(CHTDATA *prpOrganizer)
{
	int ilItem;
	if (FindLine(prpOrganizer->Urno, ilItem))
	{
		DeleteLine(ilItem);
		pomOrganizerTable->DeleteTextLine(ilItem);
		pomOrganizerTable->DisplayTable();
	}
}

//-----------------------------------------------------------------------------------------------

bool OrganizerTableViewer::IsPassFilter(CHTDATA *prpOrganizer)
{

	return true;
}

//-----------------------------------------------------------------------------------------------

int OrganizerTableViewer::CompareOrganizer(ORGANIZERTABLE_LINEDATA *prpOrganizer1, ORGANIZERTABLE_LINEDATA *prpOrganizer2)
{
/*	int ilCase=0;
	int	ilCompareResult;
	if(prpOrganizer1->Tifd == prpOrganizer2->Tifd)
	{
		ilCompareResult= 0;
	}
	else
	{
		if(prpOrganizer1->Tifd > prpOrganizer2->Tifd)
		{
			ilCompareResult = 1;
		}
		else
		{
			ilCompareResult = -1;
		}
	}
	return ilCompareResult;
*/  
	return 0;
}

//-----------------------------------------------------------------------------------------------

void OrganizerTableViewer::DeleteLine(int ipLineno)
{
	omLines.DeleteAt(ipLineno);
}

//-----------------------------------------------------------------------------------------------

bool OrganizerTableViewer::FindLine(long lpUrno, int &rilLineno)
{
    for (rilLineno = 0; rilLineno < omLines.GetSize(); rilLineno++)
	{
        if (omLines[rilLineno].Urno == lpUrno)
            return true;
	}
    return false;
}
//-----------------------------------------------------------------------------------------------

void OrganizerTableViewer::DeleteAll()
{
    while (omLines.GetSize() > 0)
        DeleteLine(0);
}

//-----------------------------------------------------------------------------------------------
// Printing Routines
//-----------------------------------------------------------------------------------------------

void OrganizerTableViewer::PrintTableView()
{
	CString olFooter1,olFooter2;
	omTableName = LoadStg(IDS_STRING193);
	int ilOrientation = PRINT_LANDSCAPE;//  PRINT_PORTRAET  PRINT_LANDSCAPE
	pomPrint = new CCSPrint((CWnd*)this,ilOrientation,45);// FirstLine(P=200,L=200),LeftOffset(P=200,L=50)
	//pomPrint->imMaxLines = 38;  // (P=57,L=38)
	//omBitmap.LoadBitmap(IDB_HAJLOGO);
	//pomPrint->SetBitmaps(&omBitmap,&omBitmap);
	if (pomPrint != NULL)
	{
		if (pomPrint->InitializePrinter(ilOrientation) == TRUE)
		{
			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
			pomPrint->imLineNo = pomPrint->imMaxLines + 1;
			DOCINFO	rlDocInfo;
			memset(&rlDocInfo, 0, sizeof(DOCINFO));
			rlDocInfo.cbSize = sizeof( DOCINFO );
			rlDocInfo.lpszDocName = omTableName;	
			pomPrint->omCdc.StartDoc( &rlDocInfo );
			pomPrint->imPageNo = 0;
			int ilLines = omLines.GetSize();
			olFooter1.Format("%d %s",ilLines,omTableName);
			for(int i = 0; i < ilLines; i++ ) 
			{
				if(pomPrint->imLineNo >= pomPrint->imMaxLines)
				{
					if(pomPrint->imPageNo > 0)
					{
						olFooter2.Format("%s: %d",LoadStg(IDS_STRING222),pomPrint->imPageNo);
						pomPrint->PrintUIFFooter(olFooter1,"",olFooter2);
						pomPrint->omCdc.EndPage();
					}
					PrintTableHeader();
				}
				if(pomPrint->imLineNo == (pomPrint->imMaxLines-1) || i == (ilLines-1))
				{
					PrintTableLine(&omLines[i],true);
				}
				else
				{
					PrintTableLine(&omLines[i],false);
				}
			}
			olFooter2.Format("%s: %d",LoadStg(IDS_STRING222),pomPrint->imPageNo);
			pomPrint->PrintUIFFooter(olFooter1,"",olFooter2);
			pomPrint->omCdc.EndPage();
			pomPrint->omCdc.EndDoc();
		}
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
	delete pomPrint;
	//omBitmap.DeleteObject();
	pomPrint = NULL;
	}
}

//-----------------------------------------------------------------------------------------------

bool OrganizerTableViewer::PrintTableHeader()
{
	pomPrint->omCdc.StartPage();
	pomPrint->imPageNo++;
	pomPrint->imLineNo = 0;

	pomPrint->PrintUIFHeader(omTableName,CString(CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M")),pomPrint->imFirstLine-10);

	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;

	rlElement.Alignment   = PRINT_LEFT;
	rlElement.FrameLeft   = PRINT_NOFRAME;
	rlElement.FrameRight  = PRINT_NOFRAME;
	rlElement.FrameTop    = PRINT_NOFRAME;
	rlElement.FrameBottom = PRINT_FRAMEMEDIUM;
	rlElement.pFont       = &pomPrint->ogCourierNew_Bold_8;

	int ilSize = omHeaderDataArray.GetSize();
	for(int i=0;i<ilSize;i++)
	{
		//if(i!=1&&i!=2)// <= setzen, wenn Spalten nicht angezeigt werden sollen 
		{
			rlElement.Length = (int)(omHeaderDataArray[i].Length*dgCCSPrintFactor);
			if(rlElement.Length < igCCSPrintMinLength)rlElement.Length+=igCCSPrintMoreLength; 
			rlElement.Text   = omHeaderDataArray[i].Text;
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
		}
	}
	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();


	return true;
}

//-----------------------------------------------------------------------------------------------

bool OrganizerTableViewer::PrintTableLine(ORGANIZERTABLE_LINEDATA *prpLine,bool bpLastLine)
{
	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;
	rlElement.pFont      = &pomPrint->ogCourierNew_Regular_8;
	int ilSize = omHeaderDataArray.GetSize();
	for(int i=0;i<ilSize;i++)
	{
		//if(i!=1&&i!=2)// <= setzen, wenn Spalten nicht angezeigt werden sollen 
		{
			rlElement.Length = (int)(omHeaderDataArray[i].Length*dgCCSPrintFactor);
			if(rlElement.Length < igCCSPrintMinLength)rlElement.Length+=igCCSPrintMoreLength; 
			rlElement.Alignment  = PRINT_LEFT;
			rlElement.FrameTop   = PRINT_FRAMETHIN;
			if(bpLastLine == true)
			{
				rlElement.FrameBottom = PRINT_FRAMETHIN;
			}
			else
			{
				rlElement.FrameBottom = PRINT_NOFRAME;
			}
			rlElement.FrameLeft  = PRINT_NOFRAME;
			rlElement.FrameRight = PRINT_NOFRAME;
			switch(i)
			{
				case 0:
					{
						rlElement.FrameLeft  = PRINT_FRAMETHIN;
						rlElement.Text		= prpLine->Chtc;
					}
					break;
				case 1:
					{
						rlElement.Text		= prpLine->Chtn;
					}
					break;
				case 2:
					{
						rlElement.Text		= prpLine->Vafr;
					}
					break;
				case 3:
					{
						rlElement.FrameRight = PRINT_FRAMETHIN;
						rlElement.Text		= prpLine->Vato;
					}
					break;
			}
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
		}
	}
	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();
	return true;
}

//-----------------------------------------------------------------------------------------------
