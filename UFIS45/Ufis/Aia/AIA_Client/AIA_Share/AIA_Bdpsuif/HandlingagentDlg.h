#if !defined(AFX_HANDLINGAGENTDLG_H__56BF0B46_2049_11D1_B38A_0000C016B067__INCLUDED_)
#define AFX_HANDLINGAGENTDLG_H__56BF0B46_2049_11D1_B38A_0000C016B067__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// HandlingagentDlg.h : header file
//
#include "CedaHAGData.h"
#include "CCSEdit.h"

/////////////////////////////////////////////////////////////////////////////
// HandlingagentDlg dialog

class HandlingagentDlg : public CDialog
{
// Construction
public:
	HandlingagentDlg(HAGDATA *popHAG,CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(HandlingagentDlg)
	enum { IDD = IDD_HANDLINGAGENTDLG };
	CButton	m_OK;
	CString	m_Caption;
	CCSEdit	m_CDATD;
	CCSEdit	m_CDATT;
	CCSEdit	m_FAXN;
	CCSEdit	m_HNAM;
	CCSEdit	m_HSNA;
	CCSEdit	m_LSTUD;
	CCSEdit	m_LSTUT;
	CCSEdit	m_USEU;
	CCSEdit	m_USEC;
	CCSEdit	m_TELE;
	CCSEdit	m_VAFRD;
	CCSEdit	m_VAFRT;
	CCSEdit	m_VATOD;
	CCSEdit	m_VATOT;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(HandlingagentDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(HandlingagentDlg)
	virtual void OnOK();
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

private:

	HAGDATA *pomHAG;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_HANDLINGAGENTDLG_H__56BF0B46_2049_11D1_B38A_0000C016B067__INCLUDED_)
