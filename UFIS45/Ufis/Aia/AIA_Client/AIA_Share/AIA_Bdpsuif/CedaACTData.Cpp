// CedaACTData.cpp
 
#include "stdafx.h"
#include "CedaACTData.h"
#include "resource.h"
#include "resrc1.h"


// Local function prototype
static void ProcessACTCf(void *popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

//--CEDADATA-----------------------------------------------------------------------------------------------

CedaACTData::CedaACTData() : CCSCedaData(&ogCommHandler)
{
    // Create an array of CEDARECINFO for ACTDATA
	BEGIN_CEDARECINFO(ACTDATA,ACTDataRecInfo)
		FIELD_LONG		(Urno,"URNO")
		FIELD_DATE		(Cdat,"CDAT")
		FIELD_CHAR_TRIM	(Usec,"USEC")
		FIELD_DATE		(Lstu,"LSTU")
		FIELD_CHAR_TRIM	(Prfl,"PRFL")
		FIELD_CHAR_TRIM	(Useu,"USEU")
		FIELD_CHAR_TRIM	(Act3,"ACT3")
		FIELD_CHAR_TRIM	(Act5,"ACT5")
		FIELD_CHAR_TRIM	(Acti,"ACTI")
		FIELD_CHAR_TRIM	(Acfn,"ACFN")
		FIELD_CHAR_TRIM	(Acws,"ACWS")
		FIELD_CHAR_TRIM	(Acle,"ACLE")
		FIELD_CHAR_TRIM	(Ache,"ACHE")
//		FIELD_CHAR_TRIM	(Acws,"ACWS")
//		FIELD_CHAR_TRIM	(Acle,"ACLE")
//		FIELD_CHAR_TRIM	(Ache,"ACHE")
		FIELD_CHAR_TRIM	(Seat,"SEAT")
		FIELD_CHAR_TRIM	(Seaf,"SEAF")
		FIELD_CHAR_TRIM	(Seab,"SEAB")
		FIELD_CHAR_TRIM	(Seae,"SEAE")
		FIELD_CHAR_TRIM	(Enty,"ENTY")
		FIELD_CHAR_TRIM	(Enno,"ENNO")
		FIELD_DATE		(Vafr,"VAFR")
		FIELD_DATE		(Vato,"VATO")
		FIELD_CHAR_TRIM	(Nads,"NADS")
		FIELD_CHAR_TRIM	(Afmc,"AFMC")
		FIELD_CHAR_TRIM	(Altc,"ALTC")
		FIELD_CHAR_TRIM	(Modc,"MODC")
		FIELD_CHAR_TRIM	(Acbt,"ACBT")
//*** 04.10.99 SHA ***
#ifdef CLIENT_SHA
		FIELD_CHAR_TRIM	(Ming,"MING")
#endif
	END_CEDARECINFO //(ACTDATA)


	// Copy the record structure
	for (int i=0; i< sizeof(ACTDataRecInfo)/sizeof(ACTDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&ACTDataRecInfo[i],sizeof(CEDARECINFO));
		omRecInfo.Add(prpCedaRecInfo);
	} // end for

	// initialize field names
	strcpy(pcmTableName,"ACT");
	//strcpy(pcmACTFieldList,"URNO,CDAT,USEC,LSTU,PRFL,USEU,ACT3,ACT5,ACTI,ACFN,ACWS,ACLE,ACHE,SEAT,SEAF,SEAB,SEAE,ENTY,ENNO,VAFR,VATO,NADS,AFMC,ALTC,MODC");
	strcpy(pcmACTFieldList,"URNO,CDAT,USEC,LSTU,PRFL,USEU,ACT3,ACT5,ACTI,ACFN,ACWS,ACLE,ACHE,SEAT,SEAF,SEAB,SEAE,ENTY,ENNO,VAFR,VATO,NADS,AFMC,ALTC,MODC,ACBT");

//*** 04.10.99 SHA ***
#ifdef CLIENT_SHA
	strcpy(pcmACTFieldList,"URNO,CDAT,USEC,LSTU,PRFL,USEU,ACT3,ACT5,ACTI,ACFN,ACWS,ACLE,ACHE,SEAT,SEAF,SEAB,SEAE,ENTY,ENNO,VAFR,VATO,NADS,AFMC,ALTC,MODC,ACBT,MING");
#endif
	
	
	pcmFieldList = pcmACTFieldList;
	omData.SetSize(0,1000);
	//omUrnoMap.InitHashTable(2001);
}

void CedaACTData::GetDataInfo(CStringArray &ropFields, CStringArray &ropDesription, CStringArray &ropType)
{
	ropFields.Add("ACFN");
	ropFields.Add("ACHE");
	ropFields.Add("ACLE");
	ropFields.Add("ACT3");
	ropFields.Add("ACT5");
	ropFields.Add("ACTI");
	ropFields.Add("ACWS");
	ropFields.Add("CDAT");
	ropFields.Add("ENNO");
	ropFields.Add("ENTY");
	ropFields.Add("LSTU");
	ropFields.Add("NADS");
	ropFields.Add("PRFL");
	ropFields.Add("SEAT");
	ropFields.Add("SEAF");
	ropFields.Add("SEAB");
	ropFields.Add("SEAE");
	ropFields.Add("URNO");
	ropFields.Add("USEC");
	ropFields.Add("USEU");
	ropFields.Add("VAFR");
	ropFields.Add("VATO");
	ropFields.Add("AFMC");
	ropFields.Add("ALTC");
	ropFields.Add("MODC");
	ropFields.Add("ACBT");
#ifdef CLIENT_SHA
	ropFields.Add("MING");
#endif

	ropDesription.Add(LoadStg(IDS_STRING439));
	ropDesription.Add(LoadStg(IDS_STRING247));
	ropDesription.Add(LoadStg(IDS_STRING246));
	ropDesription.Add(LoadStg(IDS_STRING440));
	ropDesription.Add(LoadStg(IDS_STRING441));
	ropDesription.Add(LoadStg(IDS_STRING442));
	ropDesription.Add(LoadStg(IDS_STRING245));
	ropDesription.Add(LoadStg(IDS_STRING343));
	ropDesription.Add(LoadStg(IDS_STRING249));
	ropDesription.Add(LoadStg(IDS_STRING248));
	ropDesription.Add(LoadStg(IDS_STRING344));
	ropDesription.Add(LoadStg(IDS_STRING443));
	ropDesription.Add(LoadStg(IDS_STRING345));
	ropDesription.Add(LoadStg(IDS_STRING444));
	ropDesription.Add(LoadStg(IDS_STRING624));
	ropDesription.Add(LoadStg(IDS_STRING625));
	ropDesription.Add(LoadStg(IDS_STRING626));
	ropDesription.Add(LoadStg(IDS_STRING346));
	ropDesription.Add(LoadStg(IDS_STRING347));
	ropDesription.Add(LoadStg(IDS_STRING348));
	ropDesription.Add(LoadStg(IDS_STRING230));
	ropDesription.Add(LoadStg(IDS_STRING231));
	ropDesription.Add(LoadStg(IDS_STRING627));
	ropDesription.Add(LoadStg(IDS_STRING628));
	ropDesription.Add(LoadStg(IDS_STRING629));
	ropDesription.Add(LoadStg(IDS_STRING993));
#ifdef CLIENT_SHA
	ropDesription.Add(LoadStg(IDS_STRING682));
#endif

	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("Date");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("Date");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("Date");
	ropType.Add("Date");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
#ifdef CLIENT_SHA
	ropType.Add("String");
#endif

}

//---------------------------------------------------------------------------------------------------------

void CedaACTData::Register(void)
{
	ogDdx.Register((void *)this,BC_ACT_CHANGE,CString("ACTDATA"), CString("ACT-changed"),ProcessACTCf);
	ogDdx.Register((void *)this,BC_ACT_DELETE,CString("ACTDATA"), CString("ACT-deleted"),ProcessACTCf);
}

//--~CEDADATA----------------------------------------------------------------------------------------------

CedaACTData::~CedaACTData(void)
{
	omRecInfo.DeleteAll();
	ClearAll();
}

//--CLEAR-ALL----------------------------------------------------------------------------------------------

void CedaACTData::ClearAll(bool bpWithRegistration)
{
    omUrnoMap.RemoveAll();
    omData.DeleteAll();
	if(bpWithRegistration)
	{
		ogDdx.UnRegister(this,NOTUSED);
	}
}

//--READ-ALL-----------------------------------------------------------------------------------------------

bool CedaACTData::Read(char *pspWhere)
{
    // Select data from the database
	bool ilRc = true;
    omUrnoMap.RemoveAll();
    omData.DeleteAll();
	if(pspWhere != NULL)
	{
		ilRc = CedaAction(ogBasicData.GetCedaCommand("RT"), pspWhere);
	}
	else
	{
		ilRc = CedaAction(ogBasicData.GetCedaCommand("RT"));
	}
	
	if (ilRc != true)
	{
		return false;
	}
	// Load data from CedaData into the dynamic array of record
	for (int ilLc = 0; ilRc == true; ilLc++)
	{
		ACTDATA *prpACT = new ACTDATA;
		if ((ilRc = GetFirstBufferRecord(prpACT)) == true)
		{
			prpACT->IsChanged = DATA_UNCHANGED;

			CutZeros(prpACT);

			omData.Add(prpACT);//Update omData
			omUrnoMap.SetAt((void *)prpACT->Urno,prpACT);
		}
		else
		{
			delete prpACT;
		}
	}
    return true;
}

//--INSERT-------------------------------------------------------------------------------------------------

bool CedaACTData::InsertACT(ACTDATA *prpACT,BOOL bpSendDdx)
{
	prpACT->IsChanged = DATA_NEW;
	AddZeros(prpACT);
	if(SaveACT(prpACT) == false) return false; //Update Database
	InsertACTInternal(prpACT);
    return true;
}

//--INSERT-INTERNAL--------------------------------------------------------------------------------------

bool CedaACTData::InsertACTInternal(ACTDATA *prpACT)
{
	CutZeros(prpACT);
	ogDdx.DataChanged((void *)this, ACT_CHANGE,(void *)prpACT ); //Update Viewer
	omData.Add(prpACT);//Update omData
	omUrnoMap.SetAt((void *)prpACT->Urno,prpACT);
    return true;
}

//--DELETE-------------------------------------------------------------------------------------------------

bool CedaACTData::DeleteACT(long lpUrno)
{
	ACTDATA *prlACT = GetACTByUrno(lpUrno);
	if (prlACT != NULL)
	{
		prlACT->IsChanged = DATA_DELETED;
		if(SaveACT(prlACT) == false) return false; //Update Database
		DeleteACTInternal(prlACT);
	}
    return true;
}

//--DELETE-INTERNAL----------------------------------------------------------------------------------------

bool CedaACTData::DeleteACTInternal(ACTDATA *prpACT)
{
	ogDdx.DataChanged((void *)this,ACT_DELETE,(void *)prpACT); //Update Viewer
	omUrnoMap.RemoveKey((void *)prpACT->Urno);
	int ilACTCount = omData.GetSize();
	for (int ilLc = 0; ilLc < ilACTCount; ilLc++)
	{
		if (omData[ilLc].Urno == prpACT->Urno)
		{
			omData.DeleteAt(ilLc);//Update omData
			break;
		}
	}
    return true;
}

//--PREPARE-DATA-------------------------------------------------------------------------------------------

void CedaACTData::PrepareACTData(ACTDATA *prpACT)
{
	// TODO: add code here
}

//--UPDATE-------------------------------------------------------------------------------------------------

bool CedaACTData::UpdateACT(ACTDATA *prpACT,BOOL bpSendDdx)
{
	if (GetACTByUrno(prpACT->Urno) != NULL)
	{
		if (prpACT->IsChanged == DATA_UNCHANGED)
		{
			prpACT->IsChanged = DATA_CHANGED;
		}
		AddZeros(prpACT);
		if(SaveACT(prpACT) == false) return false; //Update Database
		UpdateACTInternal(prpACT);
	}
    return true;
}

//--UPDATE-INTERNAL--------------------------------------------------------------------------------------------

bool CedaACTData::UpdateACTInternal(ACTDATA *prpACT)
{
	ACTDATA *prlACT = GetACTByUrno(prpACT->Urno);
	if (prlACT != NULL)
	{
		CutZeros(prpACT);
		*prlACT = *prpACT; //Update omData
		ogDdx.DataChanged((void *)this,ACT_CHANGE,(void *)prlACT); //Update Viewer
	}
    return true;
}

//--GET-BY-URNO--------------------------------------------------------------------------------------------

ACTDATA *CedaACTData::GetACTByUrno(long lpUrno)
{
	ACTDATA  *prlACT;
	if (omUrnoMap.Lookup((void *)lpUrno,(void *& )prlACT) == TRUE)
	{
		return prlACT;
	}
	return NULL;
}

//--READSPECIALDATA-------------------------------------------------------------------------------------

bool CedaACTData::ReadSpecialData(CCSPtrArray<ACTDATA> *popAct,char *pspWhere,char *pspFieldList,bool ipSYS/*=true*/)
{
	bool ilRc = true;
	char pclFieldList[256] = " ";

	if(strlen(pspFieldList) > 0) 
	{
		strcpy(pclFieldList, pspFieldList);
	}
	if(ipSYS == true) 
	{
		if (CedaAction("SYS","ACT",pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	else
	{
		if (CedaAction("RT","ACT",pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	if(popAct != NULL)
	{
		for (int ilLc = 0; ilRc == true; ilLc++)
		{
			ACTDATA *prpAct = new ACTDATA;
			if ((ilRc = GetBufferRecord(ilLc,prpAct,CString(pclFieldList))) == true)
			{
				popAct->Add(prpAct);
			}
			else
			{
				delete prpAct;
			}
		}
		if(popAct->GetSize() == 0) return false;
	}
    return true;
}

//--SAVE---------------------------------------------------------------------------------------------------

bool CedaACTData::SaveACT(ACTDATA *prpACT)
{
	bool ilRc = true;
	CString olListOfData;
	char pclSelection[124];
	char pclData[524];

	if (prpACT->IsChanged == DATA_UNCHANGED)
	{
		return true; // no change, nothing to do
	}

	switch(prpACT->IsChanged)
	{
	case DATA_NEW:
		MakeCedaData(&omRecInfo,olListOfData,prpACT);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("IRT","","",pclData);
		prpACT->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_CHANGED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpACT->Urno);
		MakeCedaData(&omRecInfo,olListOfData,prpACT);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("URT",pclSelection,"",pclData);
		prpACT->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_DELETED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpACT->Urno);
		ilRc = CedaAction("DRT",pclSelection);
		break;
	}

    return ilRc;
}

//--PROCESS-CF---------------------------------------------------------------------------------------------

void  ProcessACTCf(void * popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	switch(ipDDXType)
	{
	case BC_ACT_CHANGE :
	case BC_ACT_DELETE :
		((CedaACTData *)popInstance)->ProcessACTBc(ipDDXType,vpDataPointer,ropInstanceName);
		break;
	}
}

//--PROCESS-BC---------------------------------------------------------------------------------------------

void  CedaACTData::ProcessACTBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	struct BcStruct *prlACTData;
	long llUrno;
	prlACTData = (struct BcStruct *) vpDataPointer;
	CString olSelection = (CString)prlACTData->Selection;
	if (olSelection.Find('\'') != -1)
	{
		llUrno = GetUrnoFromSelection(prlACTData->Selection);
	}
	else
	{
		int ilFirst = olSelection.Find("=")+2;
		int ilLast  = olSelection.GetLength();
		llUrno = atol(olSelection.Mid(ilFirst,ilLast-ilFirst));
	}

	ACTDATA *prlACT;
	prlACT = GetACTByUrno(llUrno);
	if(ipDDXType == BC_ACT_CHANGE)
	{
		if (prlACT != NULL)
		{
			GetRecordFromItemList(prlACT,prlACTData->Fields,prlACTData->Data);
			UpdateACTInternal(prlACT);
		}
		else
		{
			prlACT = new ACTDATA;
			GetRecordFromItemList(prlACT,prlACTData->Fields,prlACTData->Data);
			InsertACTInternal(prlACT);
		}
	}
	if(ipDDXType == BC_ACT_DELETE)
	{
		if (prlACT != NULL)
		{
			DeleteACTInternal(prlACT);
		}
	}
}

//---------------------------------------------------------------------------------------------------------

void  CedaACTData::CutZeros(ACTDATA *prpACT)
{
//	if(prpACT->Ache > 0) sprintf(prpACT->AcheHelp,"%.2f",((float) prpACT->Ache / 100));
//	if(prpACT->Acle > 0) sprintf(prpACT->AcleHelp,"%.2f",((float) prpACT->Acle / 100));
//	if(prpACT->Acws > 0) sprintf(prpACT->AcwsHelp,"%.2f",((float) prpACT->Acws / 100));
	if(strlen(prpACT->Ache) != 0) sprintf(prpACT->Ache,"%.2f",atof(prpACT->Ache));
	if(strlen(prpACT->Acle) != 0) sprintf(prpACT->Acle,"%.2f",atof(prpACT->Acle));
	if(strlen(prpACT->Acws) != 0) sprintf(prpACT->Acws,"%.2f",atof(prpACT->Acws));
	if(strlen(prpACT->Seat) != 0) sprintf(prpACT->Seat,"%d",  atoi(prpACT->Seat));
}

//---------------------------------------------------------------------------------------------------------

void  CedaACTData::AddZeros(ACTDATA *prpACT)
{
//	if(strlen(prpACT->AcheHelp) != 0) prpACT->Ache = atol(prpACT->AcheHelp);
//	if(strlen(prpACT->AcleHelp) != 0) prpACT->Acle = atol(prpACT->AcleHelp);
//	if(strlen(prpACT->AcwsHelp) != 0) prpACT->Acws = atol(prpACT->AcwsHelp);
	if(strlen(prpACT->Ache) != 0) sprintf(prpACT->Ache,"%05.2f",atof(prpACT->Ache));
	if(strlen(prpACT->Acle) != 0) sprintf(prpACT->Acle,"%05.2f",atof(prpACT->Acle));
	if(strlen(prpACT->Acws) != 0) sprintf(prpACT->Acws,"%05.2f",atof(prpACT->Acws));
	if(strlen(prpACT->Seat) != 0) sprintf(prpACT->Seat,"%03d",atoi(prpACT->Seat));
}

//---------------------------------------------------------------------------------------------------------
