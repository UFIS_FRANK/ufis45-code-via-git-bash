// LoginDlg.cpp : implementation file
//

#include "stdafx.h"
#include "CCSCedadata.h"
#include "CCSCedacom.h"
#include "LoginDlg.h"
#include "PrivList.h"
#include "BasicData.h"
#include "BDPSUIF.h"


#ifdef _DEBUG
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CLoginDialog dialog
//-----------------------------------------------------------------------------------------

CLoginDialog::CLoginDialog(const char *pcpHomeAirport, const char *pcpAppl, const char *pcpWks, CWnd* pParent /*=NULL*/)
    : CDialog(CLoginDialog::IDD, pParent)
{
    //{{AFX_DATA_INIT(CLoginDialog)
	//}}AFX_DATA_INIT

	strcpy(pcmHomeAirport,pcpHomeAirport);
	strcpy(pcmAppl,pcpAppl);
	strcpy(pcmWks,pcpWks);
}

void CLoginDialog::DoDataExchange(CDataExchange* pDX)
{
    CDialog::DoDataExchange(pDX);
    //{{AFX_DATA_MAP(CLoginDialog)
	DDX_Control(pDX, IDC_USERNAME, m_UsernameCtrl);
	DDX_Control(pDX, IDC_PASSWORD, m_PasswordCtrl);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CLoginDialog, CDialog)
    //{{AFX_MSG_MAP(CLoginDialog)
    ON_WM_PAINT()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// CLoginDialog message handlers
//-----------------------------------------------------------------------------------------

BOOL CLoginDialog::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	imLoginCount = 0;

	CString olCaption = LoadStg(IDS_STRING399);
	olCaption  += ogCommHandler.pcmRealHostName;
	olCaption  += " / ";
	olCaption  += ogCommHandler.pcmRealHostType;
	SetWindowText(olCaption  );

	m_UsernameCtrl.SetTypeToString("X(32)",32,1);
	m_UsernameCtrl.SetBKColor(YELLOW);
	m_UsernameCtrl.SetTextErrColor(RED);

	m_PasswordCtrl.SetTypeToString("X(32)",32,1);
	m_PasswordCtrl.SetBKColor(YELLOW);
	m_PasswordCtrl.SetTextErrColor(RED);

	m_UsernameCtrl.SetFocus();

	return FALSE;  
}

//-----------------------------------------------------------------------------------------

void CLoginDialog::OnPaint() 
{
    CPaintDC dc(this); // device context for painting
    
    // TODO: Add your message handler code here
    CBrush olBrush( RGB( 192, 192, 192 ) );
    CBrush *polOldBrush = dc.SelectObject( &olBrush );
    CRect olRect;
    GetClientRect( &olRect );
    dc.FillRect( &olRect, &olBrush );
    dc.SelectObject( polOldBrush );
    
    CDC olDC;
    olDC.CreateCompatibleDC( &dc );
    CBitmap olBitmap;
    olBitmap.LoadBitmap( IDB_LOGIN );
    olDC.SelectObject( &olBitmap );
    dc.BitBlt( 0, 140, 409, 52, &olDC, 0, 0, SRCCOPY );
    olDC.DeleteDC( );
    
    // Do not call CDialog::OnPaint() for painting messages
}

//-----------------------------------------------------------------------------------------

void CLoginDialog::OnOK() 
{
	bool blRc = true;
	CWnd *polFocusCtrl = &m_UsernameCtrl;
	omErrTxt.Empty();

	// get the username
	m_UsernameCtrl.GetWindowText(omUsername);
	m_PasswordCtrl.GetWindowText(omPassword);

	strcpy(cgUserName,omUsername);
	ogBasicData.omUserID = CString(cgUserName);
	ogCommHandler.SetUser(omUsername);
	strcpy(CCSCedaData::pcmUser, omUsername);
	strcpy(CCSCedaData::pcmReqId, ogCommHandler.pcmReqId);

	// check the username + password
	AfxGetApp()->DoWaitCursor(1);
	blRc = ogPrivList.Login(pcmHomeAirport,omUsername,omPassword,pcmAppl,pcmWks);
	AfxGetApp()->DoWaitCursor(-1);


	if( blRc )
	{
		CDialog::OnOK();
	}
	else
	{
		if( !strcmp(ogPrivList.omErrorMessage,"LOGINERROR INVALID_USER") ) { 
			omErrTxt = LoadStg(IDS_STRING400);
		}
		else if( !strcmp(ogPrivList.omErrorMessage,"LOGINERROR INVALID_APPLICATION") ) {
			omErrTxt = LoadStg(IDS_STRING401) + LoadStg(IDS_STRING410);
		}
		else if( !strcmp(ogPrivList.omErrorMessage,"LOGINERROR INVALID_PASSWORD") ) {
			omErrTxt = LoadStg(IDS_STRING402);
			polFocusCtrl = &m_PasswordCtrl;
		}
		else if( !strcmp(ogPrivList.omErrorMessage,"LOGINERROR EXPIRED_USER") ) {
			omErrTxt = LoadStg(IDS_STRING403) + LoadStg(IDS_STRING410);
		}
		else if( !strcmp(ogPrivList.omErrorMessage,"LOGINERROR EXPIRED_APPLICATION") ) {
			omErrTxt = LoadStg(IDS_STRING404) + LoadStg(IDS_STRING410);
		}
		else if( !strcmp(ogPrivList.omErrorMessage,"LOGINERROR EXPIRED_WORKSTATION") ) {
			omErrTxt = LoadStg(IDS_STRING405) + LoadStg(IDS_STRING410);
		}
		else if( !strcmp(ogPrivList.omErrorMessage,"LOGINERROR DISABLED_USER") ) {
			omErrTxt = LoadStg(IDS_STRING406) + LoadStg(IDS_STRING410);
		}
		else if( !strcmp(ogPrivList.omErrorMessage,"LOGINERROR DISABLED_APPLICATION") ) {
			omErrTxt = LoadStg(IDS_STRING407) + LoadStg(IDS_STRING410);
		}
		else if( !strcmp(ogPrivList.omErrorMessage,"LOGINERROR DISABLED_WORKSTATION") ) {
			omErrTxt = LoadStg(IDS_STRING408) + LoadStg(IDS_STRING410);
		}
		else if( !strcmp(ogPrivList.omErrorMessage,"LOGINERROR UNDEFINED_PROFILE") ) {
			omErrTxt = LoadStg(IDS_STRING409) + LoadStg(IDS_STRING410);
		}
		else {
			omErrTxt = LoadStg(IDS_STRING12) + LoadStg(IDS_STRING410);
		}

		MessageBox(omErrTxt,LoadStg(IDS_STRING411),MB_ICONINFORMATION);
	
		imLoginCount++;

		if( imLoginCount >= MAX_LOGIN )
			OnCancel();
		else
		{
			if( polFocusCtrl != NULL )
				polFocusCtrl->SetFocus(); // set the focus to the offending control
		}
	}
}

//-----------------------------------------------------------------------------------------

bool CLoginDialog::Login(const char *pcpUsername, const char *pcpPassword)
{
	bool blRc = true;

	omErrTxt.Empty();

	strcpy(cgUserName,pcpUsername);
	ogBasicData.omUserID = CString(cgUserName);
	ogCommHandler.SetUser(CString(cgUserName));
	strcpy(CCSCedaData::pcmUser, cgUserName);
	strcpy(CCSCedaData::pcmReqId, ogCommHandler.pcmReqId);

	// check the username + password
	AfxGetApp()->DoWaitCursor(1);
	blRc = ogPrivList.Login(pcmHomeAirport,pcpUsername,pcpPassword,pcmAppl,pcmWks);
	AfxGetApp()->DoWaitCursor(-1);

	if( ! blRc )
	{
		if( !strcmp(ogPrivList.omErrorMessage,"LOGINERROR INVALID_USER") ) { 
			omErrTxt = LoadStg(IDS_STRING400);
		}
		else if( !strcmp(ogPrivList.omErrorMessage,"LOGINERROR INVALID_APPLICATION") ) {
			omErrTxt = LoadStg(IDS_STRING401) + LoadStg(IDS_STRING410);
		}
		else if( !strcmp(ogPrivList.omErrorMessage,"LOGINERROR INVALID_PASSWORD") ) {
			omErrTxt = LoadStg(IDS_STRING402);
		}
		else if( !strcmp(ogPrivList.omErrorMessage,"LOGINERROR EXPIRED_USER") ) {
			omErrTxt = LoadStg(IDS_STRING403) + LoadStg(IDS_STRING410);
		}
		else if( !strcmp(ogPrivList.omErrorMessage,"LOGINERROR EXPIRED_APPLICATION") ) {
			omErrTxt = LoadStg(IDS_STRING404) + LoadStg(IDS_STRING410);
		}
		else if( !strcmp(ogPrivList.omErrorMessage,"LOGINERROR EXPIRED_WORKSTATION") ) {
			omErrTxt = LoadStg(IDS_STRING405) + LoadStg(IDS_STRING410);
		}
		else if( !strcmp(ogPrivList.omErrorMessage,"LOGINERROR DISABLED_USER") ) {
			omErrTxt = LoadStg(IDS_STRING406) + LoadStg(IDS_STRING410);
		}
		else if( !strcmp(ogPrivList.omErrorMessage,"LOGINERROR DISABLED_APPLICATION") ) {
			omErrTxt = LoadStg(IDS_STRING407) + LoadStg(IDS_STRING410);
		}
		else if( !strcmp(ogPrivList.omErrorMessage,"LOGINERROR DISABLED_WORKSTATION") ) {
			omErrTxt = LoadStg(IDS_STRING408) + LoadStg(IDS_STRING410);
		}
		else if( !strcmp(ogPrivList.omErrorMessage,"LOGINERROR UNDEFINED_PROFILE") ) {
			omErrTxt = LoadStg(IDS_STRING409) + LoadStg(IDS_STRING410);
		}
		else {
			omErrTxt = LoadStg(IDS_STRING12) + LoadStg(IDS_STRING410);
		}

		MessageBox(omErrTxt,LoadStg(IDS_STRING411),MB_ICONINFORMATION);
	}

	return blRc;
}

//-----------------------------------------------------------------------------------------

void CLoginDialog::OnCancel() 
{
	// TODO: Add extra cleanup here
	
	CDialog::OnCancel();
}

//-----------------------------------------------------------------------------------------



