// BewertungsfaktorenDlg.cpp : implementation file
//

#include "stdafx.h"
#include "BDPSUIF.h"
#include "BewertungsfaktorenDlg.h"
#include "PrivList.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// BewertungsfaktorenDlg dialog


BewertungsfaktorenDlg::BewertungsfaktorenDlg(ASFDATA *popAsf, CWnd* pParent /*=NULL*/)
	: CDialog(BewertungsfaktorenDlg::IDD, pParent)
{
	pomAsf = popAsf;
	//{{AFX_DATA_INIT(BewertungsfaktorenDlg)
	//}}AFX_DATA_INIT
}

void BewertungsfaktorenDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(BewertungsfaktorenDlg)
	DDX_Control(pDX, IDOK, m_OK);
	DDX_Control(pDX, IDC_BEWC, m_BEWC);
	DDX_Control(pDX, IDC_BEWF, m_BEWF);
	DDX_Control(pDX, IDC_BEWN, m_BEWN);
	DDX_Control(pDX, IDC_REMA, m_REMA);
	DDX_Control(pDX, IDC_USEC, m_USEC);
	DDX_Control(pDX, IDC_USEU, m_USEU);
	DDX_Control(pDX, IDC_CDAT_D, m_CDATD);
	DDX_Control(pDX, IDC_CDAT_T, m_CDATT);
	DDX_Control(pDX, IDC_LSTU_D, m_LSTUD);
	DDX_Control(pDX, IDC_LSTU_T, m_LSTUT);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(BewertungsfaktorenDlg, CDialog)
	//{{AFX_MSG_MAP(BewertungsfaktorenDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// BewertungsfaktorenDlg message handlers
//----------------------------------------------------------------------------------------

BOOL BewertungsfaktorenDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	m_Caption = LoadStg(IDS_STRING165) + " <" + m_Caption + ">";
	SetWindowText(m_Caption);
	
	char clStat = ogPrivList.GetStat("BEWERTUNGSFAKTORENDLG.m_OK");
	SetWndStatAll(clStat,m_OK);

	//------------------------------------
	m_CDATD.SetBKColor(SILVER);
	m_CDATD.SetInitText(pomAsf->Cdat.Format("%d.%m.%Y"));
	m_CDATD.SetSecState(ogPrivList.GetStat("BEWERTUNGSFAKTORENDLG.m_CDAT"));
	// - - - - - - - - - - - - - - - - - -
	m_CDATT.SetBKColor(SILVER);
	m_CDATT.SetInitText(pomAsf->Cdat.Format("%H:%M"));
	m_CDATT.SetSecState(ogPrivList.GetStat("BEWERTUNGSFAKTORENDLG.m_CDAT"));
	//------------------------------------
	m_LSTUD.SetBKColor(SILVER);
	m_LSTUD.SetInitText(pomAsf->Lstu.Format("%d.%m.%Y"));
	m_LSTUD.SetSecState(ogPrivList.GetStat("BEWERTUNGSFAKTORENDLG.m_LSTU"));
	// - - - - - - - - - - - - - - - - - -
	m_LSTUT.SetBKColor(SILVER);
	m_LSTUT.SetInitText(pomAsf->Lstu.Format("%H:%M"));
	m_LSTUT.SetSecState(ogPrivList.GetStat("BEWERTUNGSFAKTORENDLG.m_LSTU"));
	//------------------------------------
	m_USEC.SetBKColor(SILVER);
	m_USEC.SetInitText(pomAsf->Usec);
	m_USEC.SetSecState(ogPrivList.GetStat("BEWERTUNGSFAKTORENDLG.m_USEC"));
	//------------------------------------
	m_USEU.SetBKColor(SILVER);
	m_USEU.SetInitText(pomAsf->Useu);
	m_USEU.SetSecState(ogPrivList.GetStat("BEWERTUNGSFAKTORENDLG.m_USEU"));
	//------------------------------------//WHITE,YELLOW,SILVER,RED,LYELLOW
	m_BEWC.SetFormat("x|#x|#x|#x|#x|#");
	m_BEWC.SetTextLimit(1,5);
	m_BEWC.SetBKColor(YELLOW);  
	m_BEWC.SetTextErrColor(RED);
	m_BEWC.SetInitText(pomAsf->Bewc);
	m_BEWC.SetSecState(ogPrivList.GetStat("BEWERTUNGSFAKTORENDLG.m_BEWC"));
	//-------------------------------------
	m_BEWN.SetTypeToString("X(40)",40,1);
	m_BEWN.SetBKColor(YELLOW);  
	m_BEWN.SetTextErrColor(RED);
	m_BEWN.SetInitText(pomAsf->Bewn);
	m_BEWN.SetSecState(ogPrivList.GetStat("BEWERTUNGSFAKTORENDLG.m_BEWN"));
	//--------------------------------------
	m_BEWF.SetTypeToString("#(10)",10,1);
	m_BEWF.SetBKColor(YELLOW);  
	m_BEWF.SetTextErrColor(RED);
	m_BEWF.SetInitText(pomAsf->Bewf);
	m_BEWF.SetSecState(ogPrivList.GetStat("BEWERTUNGSFAKTORENDLG.m_BEWF"));
	//--------------------------------------
	m_REMA.SetFormat("X(60)");
	m_REMA.SetTextLimit(0,60);
	m_REMA.SetTextErrColor(RED);
	m_REMA.SetInitText(pomAsf->Rema);
	m_REMA.SetSecState(ogPrivList.GetStat("ARBEITSVERTRAGSARTENDLG.m_REMA"));
	//--------------------------------------
	return TRUE;
}

//----------------------------------------------------------------------------------------

void BewertungsfaktorenDlg::OnOK() 
{
	AfxGetApp()->DoWaitCursor(1);
	CString olErrorText;
	bool ilStatus = true;
	if(m_BEWC.GetStatus() == false)
	{
		ilStatus = false;
		if(m_BEWC.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING237) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING237) + ogNotFormat;
		}
	}
	if(m_BEWF.GetStatus() == false)
	{
		ilStatus = false;
		if(m_BEWF.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING298) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING298) + ogNotFormat;
		}
	}
	if(m_BEWN.GetStatus() == false)
	{
		ilStatus = false;
		if(m_BEWN.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING229) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING229) + ogNotFormat;
		}
	}
	if(m_REMA.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING238) + ogNotFormat;
	}

	///////////////////////////////////////////////////////////////////
	//Konsistenz-Check//
	if(ilStatus == true)
	{
		CString olBewc;
		char clWhere[100];

		CCSPtrArray<ASFDATA> olAsfCPA;
		m_BEWC.GetWindowText(olBewc);
		sprintf(clWhere,"WHERE BEWC='%s'",olBewc);
		if(ogAsfData.ReadSpecialData(&olAsfCPA,clWhere,"URNO,BEWC",false) == true)
		{
			if(olAsfCPA[0].Urno != pomAsf->Urno)
			{
				ilStatus = false;
				olErrorText += LoadStg(IDS_STRING237) + LoadStg(IDS_EXIST);
			}
		}
		olAsfCPA.DeleteAll();
	}
	
	///////////////////////////////////////////////////////////////////
	AfxGetApp()->DoWaitCursor(-1);
	if (ilStatus == true)
	{
		m_BEWC.GetWindowText(pomAsf->Bewc,6);
		m_BEWF.GetWindowText(pomAsf->Bewf,11);
		m_BEWN.GetWindowText(pomAsf->Bewn,61);
		//wandelt Return in Blank//
		CString olTemp;
		m_REMA.GetWindowText(olTemp);
		for(int i=0;i>-1;)
		{
			i = olTemp.Find("\r\n");
			if(i != -1)
			{
				if(olTemp.GetLength() > (i+2))
					olTemp = olTemp.Left(i) + " " + olTemp.Mid(i+2);
				else
					olTemp = olTemp.Left(i);
			}
		}
		strcpy(pomAsf->Rema,olTemp);
		////////////////////////////
		CDialog::OnOK();
	}
	else
	{
		Beep(440,70);
		MessageBox(olErrorText,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
		m_BEWC.SetFocus();
		m_BEWC.SetSel(0,-1);
  }
}

//----------------------------------------------------------------------------------------
