// CedaSTYData.h

#ifndef __CEDASTYDATA__
#define __CEDASTYDATA__
 
#include "stdafx.h"
#include "basicdata.h"

//---------------------------------------------------------------------------------------------------------
// Record structure declaration

struct STYDATA 
{
	CTime	 Cdat; 		// Erstellungsdatum
	CTime	 Lstu; 		// Datum letzte �nderung
	char 	 Prfl[3]; 	// Protokollierungskennzeichen
	char 	 Snam[32]; 	// Servicetyp Name
	char 	 Styp[4]; 	// Servicetyp
	long 	 Urno; 		// Eindeutige Datensatz-Nr.
	char 	 Usec[34]; 	// Anwender (Ersteller)
	char 	 Useu[34]; 	// Anwender (letzte �nderung)
	CTime	 Vafr; 		// G�ltig von
	CTime	 Vato; 		// G�ltig bis

	//DataCreated by this class
	int		 IsChanged;

	STYDATA(void)
	{ 
		memset(this,'\0',sizeof(*this));
		Cdat=-1;
		Lstu=-1;
		Vafr=-1;
		Vato=-1;
	}

}; // end STYDataStrukt


//---------------------------------------------------------------------------------------------------------
// Class declaratino

class CedaSTYData: public CCSCedaData
{
// Attributes
public:
    CMapPtrToPtr omUrnoMap;

    CCSPtrArray<STYDATA> omData;

	char pcmSTYFieldList[2048];

// Operations
public:
    CedaSTYData();
	~CedaSTYData();

	void Register(void);
	virtual void GetDataInfo(CStringArray &ropFields, CStringArray &ropDesription, CStringArray &ropType);

	void ClearAll(void);

    bool Read(char *pcpWhere = NULL);
	bool ReadSpecialData(CCSPtrArray<STYDATA> *popSty,char *pspWhere,char *pspFieldList,bool ipSYS = true);
	bool InsertSTY(STYDATA *prpSTY,BOOL bpSendDdx = TRUE);
	bool InsertSTYInternal(STYDATA *prpSTY);
	bool UpdateSTY(STYDATA *prpSTY,BOOL bpSendDdx = TRUE);
	bool UpdateSTYInternal(STYDATA *prpSTY);
	bool DeleteSTY(long lpUrno);
	bool DeleteSTYInternal(STYDATA *prpSTY);
	STYDATA  *GetSTYByUrno(long lpUrno);
	bool SaveSTY(STYDATA *prpSTY);
	void ProcessSTYBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

	// Private methods
private:
    void PrepareSTYData(STYDATA *prpSTYData);
};

//---------------------------------------------------------------------------------------------------------

#endif //__CEDASTYDATA__
