#if !defined(AFX_LEISTUNGSKATALOGDLG_H__2AE25B81_461E_11D1_B3B4_0000C016B067__INCLUDED_)
#define AFX_LEISTUNGSKATALOGDLG_H__2AE25B81_461E_11D1_B3B4_0000C016B067__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// LeistungskatalogDlg.h : header file
//
#include "CedaGhsData.h"
#include "CedaGegData.h"
#include "CedaPfcData.h"
#include "CedaPerData.h"
#include "CCSEdit.h"

/////////////////////////////////////////////////////////////////////////////
// LeistungskatalogDlg dialog

struct LIST_DATA
{
	long Urno;
	char Code[20];
};

class LeistungskatalogDlg : public CDialog
{
// Construction
public:
	LeistungskatalogDlg(GHSDATA *popGhs,CWnd* pParent = NULL);   // standard constructor
	~LeistungskatalogDlg();

	CCSPtrArray<LIST_DATA> omListData;
	CUIntArray omUrnos;
	CUIntArray omPerms;
	CCSPtrArray<LIST_DATA> omPermList;
	CCSPtrArray<PFCDATA> omPfcList;
	CCSPtrArray<GEGDATA> omGegList;

	void LoadPermList();
	void MakeUrnoList(char *pspVrgc);
	CString MakeUrnoList();
	void MakePermUrnoList(char *pspVrgc);
	CString MakePermUrnoList();

// Dialog Data
	//{{AFX_DATA(LeistungskatalogDlg)
	enum { IDD = IDD_LEISTUNGSKATALOGDLG };
	CButton	m_WTYP_FUSS;
	CButton	m_WTYP_FAHRT;
	CButton	m_WTYP_SCHLEPP;
	CListBox	m_PERM_CB;
	CListBox	m_VRGC;
	CButton	m_LKTYP;
	CButton	m_LKTYG;
	CButton	m_OK;
	CButton	m_DISP;
	CButton	m_LKBZN;
	CButton	m_LKBZF;
	CButton	m_LKBZT;
	CButton	m_LKST;
	CString	m_Caption;
	CCSEdit	m_LKHC;
	CCSEdit	m_ATRN;
	CCSEdit	m_CDATD;
	CCSEdit	m_CDATT;
	CCSEdit	m_LKAM;
	CCSEdit	m_LKAN;
	CCSEdit	m_LKAR;
	CCSEdit	m_LKCO;
	CCSEdit	m_LKDA;
	CCSEdit	m_LKNB;
	CCSEdit	m_LKNM;
	CCSEdit	m_PERM;
	CCSEdit	m_LKVB;
	CCSEdit	m_LSTUD;
	CCSEdit	m_LSTUT;
	CCSEdit	m_USEC;
	CCSEdit	m_USEU;
	CCSEdit	m_LKCC;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(LeistungskatalogDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(LeistungskatalogDlg)
	virtual void OnOK();
	virtual BOOL OnInitDialog();
	afx_msg void OnSelectPerm();
	afx_msg void OnLktyP();
	afx_msg void OnLktyG();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

private:

	GHSDATA *pomGhs;

};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_LEISTUNGSKATALOGDLG_H__2AE25B81_461E_11D1_B3B4_0000C016B067__INCLUDED_)
