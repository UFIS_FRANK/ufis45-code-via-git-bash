// TaxiwayDlg.cpp : implementation file
//

#include "stdafx.h"
#include "BDPSUIF.h"
#include "TaxiwayDlg.h"
#include "CedaRWYData.h"
#include "PrivList.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// TaxiwayDlg dialog


TaxiwayDlg::TaxiwayDlg(TWYDATA *popTWY,CWnd* pParent /*=NULL*/) : CDialog(TaxiwayDlg::IDD, pParent)
{
	pomTWY = popTWY;

	//{{AFX_DATA_INIT(TaxiwayDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void TaxiwayDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(TaxiwayDlg)
	DDX_Control(pDX, IDOK, m_OK);
	DDX_Control(pDX, IDC_CDAT_D, m_CDATD);
	DDX_Control(pDX, IDC_CDAT_T, m_CDATT);
	DDX_Control(pDX, IDC_LSTU_D, m_LSTUD);
	DDX_Control(pDX, IDC_LSTU_T, m_LSTUT);
	DDX_Control(pDX, IDC_NAFR_D, m_NAFRD);
	DDX_Control(pDX, IDC_NAFR_T, m_NAFRT);
	DDX_Control(pDX, IDC_NATO_D, m_NATOD);
	DDX_Control(pDX, IDC_NATO_T, m_NATOT);
	DDX_Control(pDX, IDC_USEU,	 m_USEU);
	DDX_Control(pDX, IDC_USEC,	 m_USEC);
	DDX_Control(pDX, IDC_RESN,	 m_RESN);
	DDX_Control(pDX, IDC_TNAM,	 m_TNAM);
	DDX_Control(pDX, IDC_RGRW,	 m_RGRW);
	DDX_Control(pDX, IDC_VAFR_D, m_VAFRD);
	DDX_Control(pDX, IDC_VAFR_T, m_VAFRT);
	DDX_Control(pDX, IDC_VATO_D, m_VATOD);
	DDX_Control(pDX, IDC_VATO_T, m_VATOT);
	DDX_Control(pDX, IDC_HOME, m_HOME);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(TaxiwayDlg, CDialog)
	//{{AFX_MSG_MAP(TaxiwayDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// TaxiwayDlg message handlers
//----------------------------------------------------------------------------------------

BOOL TaxiwayDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	m_Caption = LoadStg(IDS_STRING191) + " <" + m_Caption + ">";
	SetWindowText(m_Caption);

	char clStat = ogPrivList.GetStat("TAXIWAYDLG.m_OK");
	SetWndStatAll(clStat,m_OK);

	//------------------------------------
	m_CDATD.SetBKColor(SILVER);
	m_CDATD.SetInitText(pomTWY->Cdat.Format("%d.%m.%Y"));
	m_CDATD.SetSecState(ogPrivList.GetStat("TAXIWAYDLG.m_CDAT"));
	m_CDATT.SetBKColor(SILVER);
	m_CDATT.SetInitText(pomTWY->Cdat.Format("%H:%M"));
	m_CDATT.SetSecState(ogPrivList.GetStat("TAXIWAYDLG.m_CDAT"));
	//------------------------------------
	m_LSTUD.SetBKColor(SILVER);
	m_LSTUD.SetInitText(pomTWY->Lstu.Format("%d.%m.%Y"));
	m_LSTUD.SetSecState(ogPrivList.GetStat("TAXIWAYDLG.m_LSTU"));
	m_LSTUT.SetBKColor(SILVER);
	m_LSTUT.SetInitText(pomTWY->Lstu.Format("%H:%M"));
	m_LSTUT.SetSecState(ogPrivList.GetStat("TAXIWAYDLG.m_LSTU"));
	//------------------------------------
	m_USEC.SetBKColor(SILVER);
	m_USEC.SetInitText(pomTWY->Usec);
	m_USEC.SetSecState(ogPrivList.GetStat("TAXIWAYDLG.m_USEC"));
	//------------------------------------
	m_USEU.SetBKColor(SILVER);
	m_USEU.SetInitText(pomTWY->Useu);
	m_USEU.SetSecState(ogPrivList.GetStat("TAXIWAYDLG.m_USEU"));
	//------------------------------------//WHITE,YELLOW,SILVER,RED,LYELLOW
	m_TNAM.SetFormat("x|#x|#x|#x|#x|#");
	m_TNAM.SetTextLimit(1,5);
	m_TNAM.SetBKColor(YELLOW);
	m_TNAM.SetTextErrColor(RED);
	m_TNAM.SetInitText(pomTWY->Tnam);
	m_TNAM.SetSecState(ogPrivList.GetStat("TAXIWAYDLG.m_TNAM"));
	//------------------------------------
	m_RGRW.SetFormat("x|#x|#x|#x|#");
	m_RGRW.SetTextLimit(0,4);
	m_RGRW.SetTextErrColor(RED);
	m_RGRW.SetInitText(pomTWY->Rgrw);
	m_RGRW.SetSecState(ogPrivList.GetStat("TAXIWAYDLG.m_RGRW"));
	//------------------------------------
	m_NAFRD.SetTypeToDate();
	m_NAFRD.SetTextErrColor(RED);
	m_NAFRD.SetInitText(pomTWY->Nafr.Format("%d.%m.%Y"));
	m_NAFRD.SetSecState(ogPrivList.GetStat("TAXIWAYDLG.m_NAFR"));
	// - - - - - - - - - - - - - - - - - -
	m_NAFRT.SetTypeToTime();
	m_NAFRT.SetTextErrColor(RED);
	m_NAFRT.SetInitText(pomTWY->Nafr.Format("%H:%M"));
	m_NAFRT.SetSecState(ogPrivList.GetStat("TAXIWAYDLG.m_NAFR"));
	//------------------------------------
	m_NATOD.SetTypeToDate();
	m_NATOD.SetTextErrColor(RED);
	m_NATOD.SetInitText(pomTWY->Nato.Format("%d.%m.%Y"));
	m_NATOD.SetSecState(ogPrivList.GetStat("TAXIWAYDLG.m_NATO"));
	// - - - - - - - - - - - - - - - - - -
	m_NATOT.SetTypeToTime();
	m_NATOT.SetTextErrColor(RED);
	m_NATOT.SetInitText(pomTWY->Nato.Format("%H:%M"));
	m_NATOT.SetSecState(ogPrivList.GetStat("TAXIWAYDLG.m_NATO"));
	//------------------------------------
	m_HOME.SetTypeToString("X(5)",5,1);
	m_HOME.SetTextErrColor(RED);
	m_HOME.SetInitText(pomTWY->Home);
	m_HOME.SetSecState(ogPrivList.GetStat("TAXIWAYDLG.m_HOME"));
	//------------------------------------
	m_RESN.SetTypeToString("X(40)",40,0);
	m_RESN.SetTextErrColor(RED);
	m_RESN.SetInitText(pomTWY->Resn);
	m_RESN.SetSecState(ogPrivList.GetStat("TAXIWAYDLG.m_RESN"));
	//------------------------------------
	m_VAFRD.SetTypeToDate(true);
	m_VAFRD.SetTextErrColor(RED);
	m_VAFRD.SetBKColor(YELLOW);
	m_VAFRD.SetInitText(pomTWY->Vafr.Format("%d.%m.%Y"));
	m_VAFRD.SetSecState(ogPrivList.GetStat("TAXIWAYDLG.m_VAFR"));
	// - - - - - - - - - - - - - - - - - -
	m_VAFRT.SetTypeToTime(true);
	m_VAFRT.SetTextErrColor(RED);
	m_VAFRT.SetBKColor(YELLOW);
	m_VAFRT.SetInitText(pomTWY->Vafr.Format("%H:%M"));
	m_VAFRT.SetSecState(ogPrivList.GetStat("TAXIWAYDLG.m_VAFR"));
	//------------------------------------
	m_VATOD.SetTypeToDate();
	m_VATOD.SetTextErrColor(RED);
	m_VATOD.SetInitText(pomTWY->Vato.Format("%d.%m.%Y"));
	m_VATOD.SetSecState(ogPrivList.GetStat("TAXIWAYDLG.m_VATO"));
	// - - - - - - - - - - - - - - - - - -
	m_VATOT.SetTypeToTime();
	m_VATOT.SetTextErrColor(RED);
	m_VATOT.SetInitText(pomTWY->Vato.Format("%H:%M"));
	m_VATOT.SetSecState(ogPrivList.GetStat("TAXIWAYDLG.m_VATO"));
	//------------------------------------
	return TRUE;
}

//----------------------------------------------------------------------------------------

void TaxiwayDlg::OnOK() 
{
	AfxGetApp()->DoWaitCursor(1);
	CString olErrorText;
	bool ilStatus = true;
	if(m_TNAM.GetStatus() == false)
	{
		ilStatus = false;
		if(m_TNAM.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING288) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING288) + ogNotFormat;
		}
	}
	if(m_RGRW.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING95) + ogNotFormat;
	}
	if(m_NAFRD.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING364) + LoadStg(IDS_STRING232) + ogNotFormat;
	}
	if(m_NAFRT.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING364) + LoadStg(IDS_STRING233) + ogNotFormat;
	}
	if(m_NATOD.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING365) + LoadStg(IDS_STRING232) + ogNotFormat;
	}
	if(m_NATOT.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING365) + LoadStg(IDS_STRING233) + ogNotFormat;
	}
	if(m_RESN.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING366) + ogNotFormat;
	}
	if(m_VAFRD.GetStatus() == false)
	{
		ilStatus = false;
		if(m_VAFRD.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING230) + LoadStg(IDS_STRING232) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING230) + LoadStg(IDS_STRING232) + ogNotFormat;
		}
	}
	if(m_VAFRT.GetStatus() == false)
	{
		ilStatus = false;
		if(m_VAFRT.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING230) + LoadStg(IDS_STRING233) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING230) + LoadStg(IDS_STRING233) + ogNotFormat;
		}
	}
	if(m_VATOD.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING231) + LoadStg(IDS_STRING232) + ogNotFormat;
	}
	if(m_VATOT.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING231) + LoadStg(IDS_STRING233) + ogNotFormat;
	}

	///////////////////////////////////////////////////////////////////////////
	CString olNafrd,olNafrt,olNatod,olNatot,olVafrd,olVafrt,olVatod,olVatot;
	CTime olTmpTimeFr,olTmpTimeTo;
	m_NAFRD.GetWindowText(olNafrd);
	m_NAFRT.GetWindowText(olNafrt);
	m_NATOD.GetWindowText(olNatod);
	m_NATOT.GetWindowText(olNatot);
	m_VAFRD.GetWindowText(olVafrd);
	m_VAFRT.GetWindowText(olVafrt);
	m_VATOD.GetWindowText(olVatod);
	m_VATOT.GetWindowText(olVatot);

	if((m_VAFRD.GetWindowTextLength() != 0 && m_VAFRT.GetWindowTextLength() == 0) || (m_VAFRD.GetWindowTextLength() == 0 && m_VAFRT.GetWindowTextLength() != 0))
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING234);
	}
	if((m_VATOD.GetWindowTextLength() != 0 && m_VATOT.GetWindowTextLength() == 0) || (m_VATOD.GetWindowTextLength() == 0 && m_VATOT.GetWindowTextLength() != 0))
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING235);
	}
	if(m_VAFRD.GetStatus() == true && m_VAFRT.GetStatus() == true && m_VATOD.GetStatus() == true && m_VATOT.GetStatus() == true)
	{
		olTmpTimeFr = DateHourStringToDate(olVafrd,olVafrt);
		olTmpTimeTo = DateHourStringToDate(olVatod,olVatot);
		if(olTmpTimeTo != -1 && (olTmpTimeTo < olTmpTimeFr || olTmpTimeFr == -1))
		{
			ilStatus = false;
			olErrorText += LoadStg(IDS_STRING236);
		}
	}

	if((m_NAFRD.GetWindowTextLength() != 0 && m_NAFRT.GetWindowTextLength() == 0) || (m_NAFRD.GetWindowTextLength() == 0 && m_NAFRT.GetWindowTextLength() != 0))
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING291);
	}
	if((m_NATOD.GetWindowTextLength() != 0 && m_NATOT.GetWindowTextLength() == 0) || (m_NATOD.GetWindowTextLength() == 0 && m_NATOT.GetWindowTextLength() != 0))
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING292);
	}
	if(m_NAFRD.GetStatus() == true && m_NAFRT.GetStatus() == true && m_NATOD.GetStatus() == true && m_NATOT.GetStatus() == true)
	{
		olTmpTimeFr = DateHourStringToDate(olNafrd,olNafrt);
		olTmpTimeTo = DateHourStringToDate(olNatod,olNatot);
		if(olTmpTimeTo != -1 && (olTmpTimeTo < olTmpTimeFr || olTmpTimeFr == -1))
		{
			ilStatus = false;
			olErrorText += LoadStg(IDS_STRING293);
		}
	}

	//Konsistenz-Check//
	if(ilStatus == true)
	{
		CString olTnam,olRgrw;
		char clWhere[100];
		CCSPtrArray<TWYDATA> olTwyCPA;

		m_TNAM.GetWindowText(olTnam);
		sprintf(clWhere,"WHERE TNAM='%s'",olTnam);
		if(ogTWYData.ReadSpecialData(&olTwyCPA,clWhere,"URNO,TNAM",false) == true)
		{
			if(olTwyCPA[0].Urno != pomTWY->Urno)
			{
				ilStatus = false;
				olErrorText += LoadStg(IDS_STRING288) + LoadStg(IDS_EXIST);
			}
		}
		olTwyCPA.DeleteAll();

		if(m_RGRW.GetWindowTextLength() != 0)
		{
			m_RGRW.GetWindowText(olRgrw);
			sprintf(clWhere,"WHERE RNAM='%s'",olRgrw);
			if(ogRWYData.ReadSpecialData(NULL,clWhere,"RNAM",false) == false)
			{
				ilStatus = false;
				olErrorText += LoadStg(IDS_STRING96);
			}
		}
	}

	///////////////////////////////////////////////////////////////////////////
	AfxGetApp()->DoWaitCursor(-1);
	if (ilStatus == true)
	{
		m_TNAM.GetWindowText(pomTWY->Tnam,6);
		m_RGRW.GetWindowText(pomTWY->Rgrw,5);
		pomTWY->Nafr = DateHourStringToDate(olNafrd,olNafrt);
		pomTWY->Nato = DateHourStringToDate(olNatod,olNatot);
		m_RESN.GetWindowText(pomTWY->Resn,41);
		m_HOME.GetWindowText(pomTWY->Home,3);
		pomTWY->Vafr = DateHourStringToDate(olVafrd,olVafrt);
		pomTWY->Vato = DateHourStringToDate(olVatod,olVatot);
		CDialog::OnOK();
	}
	else
	{
		Beep(440,70);
		MessageBox(olErrorText,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
		m_TNAM.SetFocus();
		m_TNAM.SetSel(0,-1);
	}
}

//----------------------------------------------------------------------------------------
