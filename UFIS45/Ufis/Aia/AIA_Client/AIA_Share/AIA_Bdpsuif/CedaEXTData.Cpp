// CedaEXTData.cpp
 
#include "stdafx.h"
#include "CedaEXTData.h"
#include "resource.h"


// Local function prototype
static void ProcessEXTCf(void *popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

//--CEDADATA-----------------------------------------------------------------------------------------------

CedaEXTData::CedaEXTData() : CCSCedaData(&ogCommHandler)
{
    // Create an array of CEDARECINFO for EXTDATA
	BEGIN_CEDARECINFO(EXTDATA,EXTDataRecInfo)
		FIELD_CHAR_TRIM	(Enam,"ENAM")
		FIELD_DATE		(Cdat,"CDAT")
		FIELD_DATE		(Lstu,"LSTU")
		FIELD_DATE		(Nafr,"NAFR")
		FIELD_DATE		(Nato,"NATO")
		FIELD_CHAR_TRIM	(Resn,"RESN")
		FIELD_CHAR_TRIM	(Prfl,"PRFL")
		FIELD_CHAR_TRIM	(Tele,"TELE")
		FIELD_LONG		(Urno,"URNO")
		FIELD_CHAR_TRIM	(Usec,"USEC")
		FIELD_CHAR_TRIM	(Useu,"USEU")
		FIELD_DATE		(Vafr,"VAFR")
		FIELD_DATE		(Vato,"VATO")
		FIELD_CHAR_TRIM	(Term,"TERM")
		FIELD_CHAR_TRIM	(Home,"HOME")
	END_CEDARECINFO //(EXTDataAStruct)

	// Copy the record structure
	for (int i=0; i< sizeof(EXTDataRecInfo)/sizeof(EXTDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&EXTDataRecInfo[i],sizeof(CEDARECINFO));
		omRecInfo.Add(prpCedaRecInfo);
	} // end for

	// initialize field names
	strcpy(pcmTableName,"EXT");
	strcpy(pcmEXTFieldList,"ENAM,CDAT,LSTU,NAFR,NATO,RESN,PRFL,TELE,URNO,USEC,USEU,VAFR,VATO,TERM,HOME");
	pcmFieldList = pcmEXTFieldList;

	omData.SetSize(0,1000);
	//omUrnoMap.InitHashTable(2001);
}

//----------------------------------------------------------------------------------------------------

void CedaEXTData::GetDataInfo(CStringArray &ropFields, CStringArray &ropDesription, CStringArray &ropType)
{
	ropFields.Add("CDAT");
	ropFields.Add("ENAM");
	ropFields.Add("LSTU");
	ropFields.Add("NAFR");
	ropFields.Add("NATO");
	ropFields.Add("PRFL");
	ropFields.Add("RESN");
	ropFields.Add("TELE");
	ropFields.Add("TERM");
	ropFields.Add("URNO");
	ropFields.Add("USEC");
	ropFields.Add("USEU");
	ropFields.Add("VAFR");
	ropFields.Add("VATO");
	ropFields.Add("HOME");

	ropDesription.Add(LoadStg(IDS_STRING343));
	ropDesription.Add(LoadStg(IDS_STRING445));
	ropDesription.Add(LoadStg(IDS_STRING344));
	ropDesription.Add(LoadStg(IDS_STRING364));
	ropDesription.Add(LoadStg(IDS_STRING365));
	ropDesription.Add(LoadStg(IDS_STRING345));
	ropDesription.Add(LoadStg(IDS_STRING366));
	ropDesription.Add(LoadStg(IDS_STRING294));
	ropDesription.Add(LoadStg(IDS_STRING296));
	ropDesription.Add(LoadStg(IDS_STRING346));
	ropDesription.Add(LoadStg(IDS_STRING347));
	ropDesription.Add(LoadStg(IDS_STRING348));
	ropDesription.Add(LoadStg(IDS_STRING230));
	ropDesription.Add(LoadStg(IDS_STRING231));
	ropDesription.Add(LoadStg(IDS_STRING711));

	ropType.Add("Date");
	ropType.Add("String");
	ropType.Add("Date");
	ropType.Add("Date");
	ropType.Add("Date");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("Date");
	ropType.Add("Date");
	ropType.Add("String");

}

//----------------------------------------------------------------------------------------------------

void CedaEXTData::Register(void)
{
	ogDdx.Register((void *)this,BC_EXT_CHANGE,CString("EXTDATA"), CString("EXT-changed"),ProcessEXTCf);
	ogDdx.Register((void *)this,BC_EXT_DELETE,CString("EXTDATA"), CString("EXT-deleted"),ProcessEXTCf);
}

//--~CEDADATA----------------------------------------------------------------------------------------------

CedaEXTData::~CedaEXTData(void)
{
	omRecInfo.DeleteAll();
	ClearAll();
}

//--CLEAR-ALL----------------------------------------------------------------------------------------------

void CedaEXTData::ClearAll(void)
{
    omUrnoMap.RemoveAll();
    omData.DeleteAll();
	ogDdx.UnRegister(this,NOTUSED);
}

//--READ-ALL-----------------------------------------------------------------------------------------------

bool CedaEXTData::Read(char *pcpWhere)
{
    // Select data from the database
	bool ilRc = true;
    omUrnoMap.RemoveAll();
    omData.DeleteAll();
	if(pcpWhere != NULL)
		ilRc = CedaAction(ogBasicData.GetCedaCommand("RT"), pcpWhere);
	else
		ilRc = CedaAction(ogBasicData.GetCedaCommand("RT"));
	if (ilRc != true)
	{
		return false;
	}
	// Load data from CedaData into the dynamic array of record
	for (int ilLc = 0; ilRc == true; ilLc++)
	{
		EXTDATA *prpEXT = new EXTDATA;
		if ((ilRc = GetFirstBufferRecord(prpEXT)) == true)
		{
			prpEXT->IsChanged = DATA_UNCHANGED;
			omData.Add(prpEXT);//Update omData
			omUrnoMap.SetAt((void *)prpEXT->Urno,prpEXT);
		}
		else
		{
			delete prpEXT;
		}
	}
    return true;
}

//--INSERT-------------------------------------------------------------------------------------------------

bool CedaEXTData::InsertEXT(EXTDATA *prpEXT,BOOL bpSendDdx)
{
	prpEXT->IsChanged = DATA_NEW;
	if(SaveEXT(prpEXT) == false) return false; //Update Database
	InsertEXTInternal(prpEXT);
    return true;
}

//--INSERT-INTERNAL---------------------------------------------------------------------------------------

bool CedaEXTData::InsertEXTInternal(EXTDATA *prpEXT)
{
	//PrepareEXTData(prpEXT);
	ogDdx.DataChanged((void *)this, EXT_CHANGE,(void *)prpEXT ); //Update Viewer
	omData.Add(prpEXT);//Update omData
	omUrnoMap.SetAt((void *)prpEXT->Urno,prpEXT);
	return true;
}

//--DELETE-------------------------------------------------------------------------------------------------

bool CedaEXTData::DeleteEXT(long lpUrno)
{
	EXTDATA *prlEXT = GetEXTByUrno(lpUrno);
	if (prlEXT != NULL)
	{
		prlEXT->IsChanged = DATA_DELETED;
		if(SaveEXT(prlEXT) == false) return false; //Update Database
		DeleteEXTInternal(prlEXT);
	}
    return true;
}

//--DELETE-INTERNAL----------------------------------------------------------------------------------------

bool CedaEXTData::DeleteEXTInternal(EXTDATA *prpEXT)
{
	ogDdx.DataChanged((void *)this,EXT_DELETE,(void *)prpEXT); //Update Viewer
	omUrnoMap.RemoveKey((void *)prpEXT->Urno);
	int ilEXTCount = omData.GetSize();
	for (int ilLc = 0; ilLc < ilEXTCount; ilLc++)
	{
		if (omData[ilLc].Urno == prpEXT->Urno)
		{
			omData.DeleteAt(ilLc);//Update omData
			break;
		}
	}
    return true;
}

//--PREPARE-DATA-------------------------------------------------------------------------------------------

void CedaEXTData::PrepareEXTData(EXTDATA *prpEXT)
{
	// TODO: add code here
}

//--UPDATE-------------------------------------------------------------------------------------------------

bool CedaEXTData::UpdateEXT(EXTDATA *prpEXT,BOOL bpSendDdx)
{
	if (GetEXTByUrno(prpEXT->Urno) != NULL)
	{
		if (prpEXT->IsChanged == DATA_UNCHANGED)
		{
			prpEXT->IsChanged = DATA_CHANGED;
		}
		if(SaveEXT(prpEXT) == false) return false; //Update Database
		UpdateEXTInternal(prpEXT);
	}
    return true;
}

//--UPDATE-INTERNAL--------------------------------------------------------------------------------------------

bool CedaEXTData::UpdateEXTInternal(EXTDATA *prpEXT)
{
	EXTDATA *prlEXT = GetEXTByUrno(prpEXT->Urno);
	if (prlEXT != NULL)
	{
		*prlEXT = *prpEXT; //Update omData
		ogDdx.DataChanged((void *)this,EXT_CHANGE,(void *)prlEXT); //Update Viewer
	}
    return true;
}

//--GET-BY-URNO--------------------------------------------------------------------------------------------

EXTDATA *CedaEXTData::GetEXTByUrno(long lpUrno)
{
	EXTDATA  *prlEXT;
	if (omUrnoMap.Lookup((void *)lpUrno,(void *& )prlEXT) == TRUE)
	{
		return prlEXT;
	}
	return NULL;
}

//--READSPECIALDATA-------------------------------------------------------------------------------------

bool CedaEXTData::ReadSpecialData(CCSPtrArray<EXTDATA> *popExt,char *pspWhere,char *pspFieldList,bool ipSYS/*=true*/)
{
	bool ilRc = true;
	char pclFieldList[256] = " ";

	if(strlen(pspFieldList) > 0) 
	{
		strcpy(pclFieldList, pspFieldList);
	}
	if(ipSYS == true) 
	{
		if (CedaAction("SYS","EXT",pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	else
	{
		if (CedaAction("RT","EXT",pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	if(popExt != NULL)
	{
		for (int ilLc = 0; ilRc == true; ilLc++)
		{
			EXTDATA *prpExt = new EXTDATA;
			if ((ilRc = GetBufferRecord(ilLc,prpExt,CString(pclFieldList))) == true)
			{
				popExt->Add(prpExt);
			}
			else
			{
				delete prpExt;
			}
		}
		if(popExt->GetSize() == 0) return false;
	}
    return true;
}

//--SAVE---------------------------------------------------------------------------------------------------

bool CedaEXTData::SaveEXT(EXTDATA *prpEXT)
{
	bool ilRc = true;
	CString olListOfData;
	char pclSelection[124];
	char pclData[524];

	if (prpEXT->IsChanged == DATA_UNCHANGED)
	{
		return true; // no change, nothing to do
	}

	switch(prpEXT->IsChanged)
	{
	case DATA_NEW:
		MakeCedaData(&omRecInfo,olListOfData,prpEXT);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("IRT","","",pclData);
		prpEXT->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_CHANGED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpEXT->Urno);
		MakeCedaData(&omRecInfo,olListOfData,prpEXT);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("URT",pclSelection,"",pclData);
		prpEXT->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_DELETED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpEXT->Urno);
		ilRc = CedaAction("DRT",pclSelection);
		break;
	}

    return ilRc;
}

//--PROCESS-CF---------------------------------------------------------------------------------------------

void  ProcessEXTCf(void * popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	switch(ipDDXType)
	{
	case BC_EXT_CHANGE :
	case BC_EXT_DELETE :
		((CedaEXTData *)popInstance)->ProcessEXTBc(ipDDXType,vpDataPointer,ropInstanceName);
		break;
	}
}

//--PROCESS-BC---------------------------------------------------------------------------------------------

void  CedaEXTData::ProcessEXTBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	struct BcStruct *prlEXTData;
	long llUrno;
	prlEXTData = (struct BcStruct *) vpDataPointer;
	CString olSelection = (CString)prlEXTData->Selection;
	if (olSelection.Find('\'') != -1)
	{
		llUrno = GetUrnoFromSelection(prlEXTData->Selection);
	}
	else
	{
		int ilFirst = olSelection.Find("=")+2;
		int ilLast  = olSelection.GetLength();
		llUrno = atol(olSelection.Mid(ilFirst,ilLast-ilFirst));
	}

	EXTDATA *prlEXT;
	prlEXT = GetEXTByUrno(llUrno);
	if(ipDDXType == BC_EXT_CHANGE)
	{
		if (prlEXT != NULL)
		{
			GetRecordFromItemList(prlEXT,prlEXTData->Fields,prlEXTData->Data);
			UpdateEXTInternal(prlEXT);
		}
		else
		{
			prlEXT = new EXTDATA;
			GetRecordFromItemList(prlEXT,prlEXTData->Fields,prlEXTData->Data);
			InsertEXTInternal(prlEXT);
		}
	}
	if(ipDDXType == BC_EXT_DELETE)
	{
		if (prlEXT != NULL)
		{
			DeleteEXTInternal(prlEXT);
		}
	}
}

//---------------------------------------------------------------------------------------------------------
