// ArbeitsvertragsartenDlg.cpp : implementation file
//

#include "stdafx.h"
#include "BDPSUIF.h"
#include "ArbeitsvertragsartenDlg.h"
#include "CedaOrgData.h"
#include "PrivList.h"
#include "AwDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// ArbeitsvertragsartenDlg dialog

ArbeitsvertragsartenDlg::ArbeitsvertragsartenDlg(COTDATA *popCot, CWnd* pParent /*=NULL*/)
	: CDialog(ArbeitsvertragsartenDlg::IDD, pParent)
{
	pomCot = popCot;
	//{{AFX_DATA_INIT(ArbeitsvertragsartenDlg)
	//}}AFX_DATA_INIT
}

void ArbeitsvertragsartenDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(ArbeitsvertragsartenDlg)
	DDX_Control(pDX, IDC_SBPA, m_Sbpa);
	DDX_Control(pDX, IDOK, m_OK);
	DDX_Control(pDX, IDC_WRKD_T, m_WRKD_T);
	DDX_Control(pDX, IDC_WRKD_7, m_WRKD_7);
	DDX_Control(pDX, IDC_WRKD_6, m_WRKD_6);
	DDX_Control(pDX, IDC_WRKD_5, m_WRKD_5);
	DDX_Control(pDX, IDC_WRKD_4, m_WRKD_4);
	DDX_Control(pDX, IDC_WRKD_2, m_WRKD_2);
	DDX_Control(pDX, IDC_WRKD_3, m_WRKD_3);
	DDX_Control(pDX, IDC_WRKD_1, m_WRKD_1);
	DDX_Control(pDX, IDC_CDAT_D, m_CDATD);
	DDX_Control(pDX, IDC_CDAT_T, m_CDATT);
	DDX_Control(pDX, IDC_CTRC,   m_CTRC);
	DDX_Control(pDX, IDC_CTRN,   m_CTRN);
	DDX_Control(pDX, IDC_DPTC,   m_DPTC);
	DDX_Control(pDX, IDC_LSTU_D, m_LSTUD);
	DDX_Control(pDX, IDC_LSTU_T, m_LSTUT);
	DDX_Control(pDX, IDC_MISL,   m_MISL);
	DDX_Control(pDX, IDC_MXSL,   m_MXSL);
	DDX_Control(pDX, IDC_NOWD,   m_NOWD);
	DDX_Control(pDX, IDC_REMA,   m_REMA);
	DDX_Control(pDX, IDC_USEC,   m_USEC);
	DDX_Control(pDX, IDC_USEU,   m_USEU);
	DDX_Control(pDX, IDC_WHPW,   m_WHPW);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(ArbeitsvertragsartenDlg, CDialog)
	//{{AFX_MSG_MAP(ArbeitsvertragsartenDlg)
	ON_BN_CLICKED(IDC_WRKD_T,	OnWrkdT)
	ON_BN_CLICKED(IDC_WRKD_7,	OnWrkd17)
	ON_BN_CLICKED(IDC_WRKD_6,	OnWrkd17)
	ON_BN_CLICKED(IDC_WRKD_5,	OnWrkd17)
	ON_BN_CLICKED(IDC_WRKD_4,	OnWrkd17)
	ON_BN_CLICKED(IDC_WRKD_2,	OnWrkd17)
	ON_BN_CLICKED(IDC_WRKD_3,	OnWrkd17)
	ON_BN_CLICKED(IDC_WRKD_1,	OnWrkd17)
	ON_BN_CLICKED(IDC_B_ORG_AW, OnBOrgAw)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// ArbeitsvertragsartenDlg message handlers
//----------------------------------------------------------------------------------------

BOOL ArbeitsvertragsartenDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	m_Caption = LoadStg(IDS_STRING162) + " <" + m_Caption + ">";
	SetWindowText(m_Caption);
	
	char clStat = ogPrivList.GetStat("ARBEITSVERTRAGSARTENDLG.m_OK");
	SetWndStatAll(clStat,m_OK);

	//------------------------------------
	m_CDATD.SetBKColor(SILVER);
	m_CDATD.SetInitText(pomCot->Cdat.Format("%d.%m.%Y"));
	m_CDATD.SetSecState(ogPrivList.GetStat("ARBEITSVERTRAGSARTENDLG.m_CDAT"));
	// - - - - - - - - - - - - - - - - - -
	m_CDATT.SetBKColor(SILVER);
	m_CDATT.SetInitText(pomCot->Cdat.Format("%H:%M"));
	m_CDATT.SetSecState(ogPrivList.GetStat("ARBEITSVERTRAGSARTENDLG.m_CDAT"));
	//------------------------------------
	m_LSTUD.SetBKColor(SILVER);
	m_LSTUD.SetInitText(pomCot->Lstu.Format("%d.%m.%Y"));
	m_LSTUD.SetSecState(ogPrivList.GetStat("ARBEITSVERTRAGSARTENDLG.m_LSTU"));
	// - - - - - - - - - - - - - - - - - -
	m_LSTUT.SetBKColor(SILVER);
	m_LSTUT.SetInitText(pomCot->Lstu.Format("%H:%M"));
	m_LSTUT.SetSecState(ogPrivList.GetStat("ARBEITSVERTRAGSARTENDLG.m_LSTU"));
	//------------------------------------
	m_USEC.SetBKColor(SILVER);
	m_USEC.SetInitText(pomCot->Usec);
	m_USEC.SetSecState(ogPrivList.GetStat("ARBEITSVERTRAGSARTENDLG.m_USEC"));
	//------------------------------------
	m_USEU.SetBKColor(SILVER);
	m_USEU.SetInitText(pomCot->Useu);
	m_USEU.SetSecState(ogPrivList.GetStat("ARBEITSVERTRAGSARTENDLG.m_USEU"));
	//------------------------------------//WHITE,YELLOW,SILVER,RED,LYELLOW
	m_CTRC.SetFormat("x|#x|#x|#x|#x|#");
	m_CTRC.SetTextLimit(1,5);
	m_CTRC.SetBKColor(YELLOW);
	m_CTRC.SetTextErrColor(RED);
	m_CTRC.SetInitText(pomCot->Ctrc);
	m_CTRC.SetSecState(ogPrivList.GetStat("ARBEITSVERTRAGSARTENDLG.m_CTRC"));
	//------------------------------------
	m_CTRN.SetTypeToString("X(40)",40,1);
	m_CTRN.SetBKColor(YELLOW);
	m_CTRN.SetTextErrColor(RED);
	m_CTRN.SetInitText(pomCot->Ctrn);
	m_CTRN.SetSecState(ogPrivList.GetStat("ARBEITSVERTRAGSARTENDLG.m_CTRN"));
	//------------------------------------
	m_DPTC.SetFormat("x|#x|#x|#x|#x|#");
	m_DPTC.SetTextLimit(0,5);
	m_DPTC.SetTextErrColor(RED);
	m_DPTC.SetInitText(pomCot->Dptc);
	m_DPTC.SetSecState(ogPrivList.GetStat("ARBEITSVERTRAGSARTENDLG.m_DPTC"));
	//------------------------------------
	m_MISL.SetTypeToDouble(2,1);
	m_MISL.SetTextLimit(1,4);
	m_MISL.SetBKColor(YELLOW);
	m_MISL.SetTextErrColor(RED);
	m_MISL.SetInitText(pomCot->Misl);
	m_MISL.SetSecState(ogPrivList.GetStat("ARBEITSVERTRAGSARTENDLG.m_MISL"));
	//------------------------------------
	m_MXSL.SetTypeToDouble(2,1);
	m_MXSL.SetTextLimit(1,4);
	m_MXSL.SetBKColor(YELLOW);
	m_MXSL.SetTextErrColor(RED);
	m_MXSL.SetInitText(pomCot->Mxsl);
	m_MXSL.SetSecState(ogPrivList.GetStat("ARBEITSVERTRAGSARTENDLG.m_MXSL"));
	//------------------------------------
	m_NOWD.SetFormat("'1'|'2'|'3'|'4'|'5'|'6'|'7'");
	m_NOWD.SetTextLimit(1,1);
	m_NOWD.SetBKColor(YELLOW);
	m_NOWD.SetTextErrColor(RED);
	m_NOWD.SetInitText(pomCot->Nowd);
	m_NOWD.SetSecState(ogPrivList.GetStat("ARBEITSVERTRAGSARTENDLG.m_NOWD"));
	//------------------------------------
	m_REMA.SetTypeToString("X(60)",60,0);
	m_REMA.SetTextErrColor(RED);
	m_REMA.SetInitText(pomCot->Rema);
	m_REMA.SetSecState(ogPrivList.GetStat("ARBEITSVERTRAGSARTENDLG.m_REMA"));
	//------------------------------------
	m_WHPW.SetTypeToDouble(2,1);
	m_WHPW.SetTextLimit(1,4);
	m_WHPW.SetBKColor(YELLOW);
	m_WHPW.SetTextErrColor(RED);
	m_WHPW.SetInitText(pomCot->Whpw);
	m_WHPW.SetSecState(ogPrivList.GetStat("ARBEITSVERTRAGSARTENDLG.m_WHPW"));
	//------------------------------------
	clStat = ogPrivList.GetStat("ARBEITSVERTRAGSARTENDLG.m_WRKD");
	SetWndStatAll(clStat,m_WRKD_1);
	SetWndStatAll(clStat,m_WRKD_2);
	SetWndStatAll(clStat,m_WRKD_3);
	SetWndStatAll(clStat,m_WRKD_4);
	SetWndStatAll(clStat,m_WRKD_5);
	SetWndStatAll(clStat,m_WRKD_6);
	SetWndStatAll(clStat,m_WRKD_7);
	SetWndStatAll(clStat,m_WRKD_T);
	if(strchr(pomCot->Wrkd,'1') != NULL) m_WRKD_1.SetCheck(1);
	if(strchr(pomCot->Wrkd,'2') != NULL) m_WRKD_2.SetCheck(1);
	if(strchr(pomCot->Wrkd,'3') != NULL) m_WRKD_3.SetCheck(1);
	if(strchr(pomCot->Wrkd,'4') != NULL) m_WRKD_4.SetCheck(1);
	if(strchr(pomCot->Wrkd,'5') != NULL) m_WRKD_5.SetCheck(1);
	if(strchr(pomCot->Wrkd,'6') != NULL) m_WRKD_6.SetCheck(1);
	if(strchr(pomCot->Wrkd,'7') != NULL) m_WRKD_7.SetCheck(1);
	if(CString(pomCot->Wrkd).Find("1234567") != -1) m_WRKD_T.SetCheck(1);
	//------------------------------------
	SetWndStatAll('-',m_Sbpa);

#ifdef CLIENT_HAJ
	if (pomCot->Sbpa[0] =='x')
		m_Sbpa.SetCheck(0);
	else
		m_Sbpa.SetCheck(1);
	clStat = ogPrivList.GetStat("ARBEITSVERTRAGSARTENDLG.m_SBPA");
	SetWndStatAll(clStat,m_Sbpa);
#endif
	//------------------------------------

	return TRUE;  
}


//----------------------------------------------------------------------------------------

void ArbeitsvertragsartenDlg::OnOK() 
{
	AfxGetApp()->DoWaitCursor(1);
	CString olErrorText;
	bool ilStatus = true;

	if(m_CTRC.GetStatus() == false)
	{
		ilStatus = false;
		if(m_CTRC.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING237) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING237) + ogNotFormat;
		}
	}
	if(m_CTRN.GetStatus() == false)
	{
		ilStatus = false;
		if(m_CTRN.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING229) + ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING229) + ogNotFormat;
		}
	}
	if(m_DPTC.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING41) + ogNotFormat;
	}
	if(m_MISL.GetStatus() == false)
	{
		ilStatus = false;
		if(m_MISL.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING315) + ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING315) + ogNotFormat;
		}
	}
	if(m_MXSL.GetStatus() == false)
	{
		ilStatus = false;
		if(m_MXSL.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING316) + ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING316) + ogNotFormat;
		}
	}
	if(m_NOWD.GetStatus() == false)
	{
		ilStatus = false;
		if(m_NOWD.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING317) + ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING317) + ogNotFormat;
		}
	}
	if(m_WHPW.GetStatus() == false)
	{
		ilStatus = false;
		if(m_WHPW.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING318) + ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING318) + ogNotFormat;
		}
	}
	if(m_REMA.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING238) + ogNotFormat;
	}
	if(m_WRKD_1.GetCheck()==0&&m_WRKD_2.GetCheck()==0&&m_WRKD_3.GetCheck()==0&&m_WRKD_4.GetCheck()==0&&m_WRKD_5.GetCheck()==0&&m_WRKD_6.GetCheck()==0&&m_WRKD_7.GetCheck()==0)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING319);
	}
	///////////////////////////////////////////////////////////////////
	//Konsistenz-Check//
	if(ilStatus == true)
	{
		CString olDptc,olCtrc;
		char clWhere[100];
	
		if(m_DPTC.GetWindowTextLength() != 0)
		{
			CCSPtrArray<ORGDATA> olOrgCPA;
			m_DPTC.GetWindowText(olDptc);
			sprintf(clWhere,"WHERE DPT1='%s'",olDptc);
			if(ogOrgData.ReadSpecialData(&olOrgCPA,clWhere,"URNO",false) == false)
			{
				ilStatus = false;
				olErrorText += LoadStg(IDS_STRING320);
			}
			olOrgCPA.DeleteAll();
		}
		else
		{
			olDptc = " ";
		}

/*		//Gültigkeit Code + Org.Code
		if(m_CTRC.GetWindowTextLength() != 0)
		{
			CCSPtrArray<COTDATA> olData;

			CString olText;
			m_CTRC.GetWindowText(olCtrc);
			sprintf(clWhere,"WHERE CTRC='%s' AND DPTC = '%s'",olCtrc, olDptc);
			if(ogCotData.ReadSpecialData(&olData,clWhere,"URNO",false) == true)
			{
				if(pomCot->Urno != olData[0].Urno)
				{
					ilStatus = false;
					olErrorText += LoadStg(IDS_STRING616);
				}
			}
			olData.DeleteAll();
		}
*/
		//Gültigkeit nur Code
		if(m_CTRC.GetWindowTextLength() != 0)
		{
			CCSPtrArray<COTDATA> olData;

			CString olText;
			m_CTRC.GetWindowText(olCtrc);
			sprintf(clWhere,"WHERE CTRC='%s'",olCtrc);
			if(ogCotData.ReadSpecialData(&olData,clWhere,"URNO",false) == true)
			{
				if(pomCot->Urno != olData[0].Urno)
				{
					ilStatus = false;
					olErrorText += LoadStg(IDS_STRING237) + LoadStg(IDS_EXIST);
				}
			}
			olData.DeleteAll();
		}

	}
	///////////////////////////////////////////////////////////////////
	AfxGetApp()->DoWaitCursor(-1);
	if (ilStatus == true)
	{
		m_CTRC.GetWindowText(pomCot->Ctrc,6);
		m_CTRN.GetWindowText(pomCot->Ctrn,41);
		m_DPTC.GetWindowText(pomCot->Dptc,6);
		m_MISL.GetWindowText(pomCot->Misl,5);
		m_MXSL.GetWindowText(pomCot->Mxsl,5);
		m_NOWD.GetWindowText(pomCot->Nowd,2);
		m_WHPW.GetWindowText(pomCot->Whpw,5);

#ifdef CLIENT_HAJ
		//*** 08.09.99 SHA ***
		if (m_Sbpa.GetCheck() == 1)
			strcpy(pomCot->Sbpa," ");
		else
			strcpy(pomCot->Sbpa,"x");
#endif

		CString olDays;
		if(m_WRKD_1.GetCheck()==1) olDays  = "1";
		if(m_WRKD_2.GetCheck()==1) olDays += "2";
		if(m_WRKD_3.GetCheck()==1) olDays += "3";
		if(m_WRKD_4.GetCheck()==1) olDays += "4";
		if(m_WRKD_5.GetCheck()==1) olDays += "5";
		if(m_WRKD_6.GetCheck()==1) olDays += "6";
		if(m_WRKD_7.GetCheck()==1) olDays += "7";
		strcpy(pomCot->Wrkd,olDays);
		//wandelt Return in Blank//
		CString olTemp;
		m_REMA.GetWindowText(olTemp);
		for(int i=0;i>-1;)
		{
			i = olTemp.Find("\r\n");
			if(i != -1)
			{
				if(olTemp.GetLength() > (i+2))
					olTemp = olTemp.Left(i) + " " + olTemp.Mid(i+2);
				else
					olTemp = olTemp.Left(i);
			}
		}
		strcpy(pomCot->Rema,olTemp);
		////////////////////////////
		CDialog::OnOK();
	}
	else
	{
		Beep(440,70);
		MessageBox(olErrorText,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
		m_CTRC.SetFocus();
	}	
}

//----------------------------------------------------------------------------------------

void ArbeitsvertragsartenDlg::OnWrkdT() 
{
	if(m_WRKD_T.GetCheck() == 1)
	{
		m_WRKD_1.SetCheck(1);
		m_WRKD_2.SetCheck(1);
		m_WRKD_3.SetCheck(1);
		m_WRKD_4.SetCheck(1);
		m_WRKD_5.SetCheck(1);
		m_WRKD_6.SetCheck(1);
		m_WRKD_7.SetCheck(1);
	}
	if(m_WRKD_T.GetCheck() == 0)
	{
		m_WRKD_1.SetCheck(0);
		m_WRKD_2.SetCheck(0);
		m_WRKD_3.SetCheck(0);
		m_WRKD_4.SetCheck(0);
		m_WRKD_5.SetCheck(0);
		m_WRKD_6.SetCheck(0);
		m_WRKD_7.SetCheck(0);
	}
}

//----------------------------------------------------------------------------------------

void ArbeitsvertragsartenDlg::OnWrkd17() 
{
	if(m_WRKD_T.GetCheck() == 1)
	{
		m_WRKD_T.SetCheck(0);
	}
}

//----------------------------------------------------------------------------------------

void ArbeitsvertragsartenDlg::OnBOrgAw() 
{
	CStringArray olCol1, olCol2, olCol3;
	CCSPtrArray<ORGDATA> olList;
	if(ogOrgData.ReadSpecialData(&olList, "", "URNO,DPT1,DPTN", false) == true)
	{
		for(int i = 0; i < olList.GetSize(); i++)
		{
			char pclUrno[20]="";
			sprintf(pclUrno, "%ld", olList[i].Urno);
			olCol3.Add(CString(pclUrno));
			olCol1.Add(CString(olList[i].Dpt1));
			olCol2.Add(CString(olList[i].Dptn));
		}
		AwDlg *pomDlg = new AwDlg(&olCol1, &olCol2, &olCol3, LoadStg(IDS_STRING237), LoadStg(IDS_STRING229), this);
		if(pomDlg->DoModal() == IDOK)
		{
			m_DPTC.SetInitText(pomDlg->omReturnString);
			m_DPTC.SetFocus();
		}
		delete pomDlg;
	}
	else
	{
		MessageBox(LoadStg(IDS_STRING321), LoadStg(IDS_STRING149), (MB_ICONEXCLAMATION|MB_OK));
	}
	olList.DeleteAll();
}

//----------------------------------------------------------------------------------------
