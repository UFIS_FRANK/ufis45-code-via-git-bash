#ifndef __PlanungsgruppenTableViewer_H__
#define __PlanungsgruppenTableViewer_H__

#include "stdafx.h"
#include "CedaPgpData.h"
#include "CCSTable.h"
#include "CViewer.h"
#include "CCSPrint.h"



struct PLANUNGSGRUPPENTABLE_LINEDATA
{
	long 	 Urno;	// uniform resource number
	CTime 	 Cdat; 	// creation date
	CTime 	 Lstu;	// last update
	CString  Usec;	// user (creator)
	CString  Useu;	// user (last change)
	CString	 Prfl;	// Protokollierungserkennung
	CString  Pgpc;	// code
	CString  Type;  // type
	CString  Pgpn;  // name
	CString  Rema; 	// remark
	CString  Pgpm;  // members
	CString  Minm;	// min number of members
	CString  Maxm;  // max number of members
	CString  Grpm;	// group members (constructed from Pgpm, Minm and Maxm)

	PLANUNGSGRUPPENTABLE_LINEDATA(void)
	{
		//memset(this,'\0',sizeof(*this));
		Urno = -1;
		Cdat = -1;
		Lstu = -1;
	}
};

/////////////////////////////////////////////////////////////////////////////
// PlanungsgruppenTableViewer

	  
class PlanungsgruppenTableViewer : public CViewer
{
// Constructions
public:
    PlanungsgruppenTableViewer(CCSPtrArray <PGPDATA> *popData);
    ~PlanungsgruppenTableViewer();

    void Attach(CCSTable *popAttachWnd);
    virtual void ChangeViewTo(const char *pcpViewName);
// Internal data processing routines
private:
	bool IsPassFilter(PGPDATA *prpPlanungsgruppen);
	int ComparePlanungsgruppen(PLANUNGSGRUPPENTABLE_LINEDATA *prpPlanungsgruppen1, PLANUNGSGRUPPENTABLE_LINEDATA *prpPlanungsgruppen2);
    void MakeLines();
	void MakeLine(PGPDATA *prpPlanungsgruppen);
	bool FindLine(long lpUrno, int &rilLineno);
// Operations
public:
	void DeleteAll();
	void CreateLine(PLANUNGSGRUPPENTABLE_LINEDATA *prpPlanungsgruppen);
	void DeleteLine(int ipLineno);
// Window refreshing routines
public:
	void UpdateDisplay();
	CString Format(PLANUNGSGRUPPENTABLE_LINEDATA *prpLine);
	CString MakeGroupString(PGPDATA *prpPgp);
	void ProcessPlanungsgruppenChange(PGPDATA *prpPlanungsgruppen);
	void ProcessPlanungsgruppenDelete(PGPDATA *prpPlanungsgruppen);
	bool PlanungsgruppenTableViewer::FindPlanungsgruppen(long lpPgpUrno, char *pcpPgpName, int& ilItem);
// Attributes used for filtering condition
private:
	bool bmIsFromSearch;
// Attributes
private:
    CCSTable *pomPlanungsgruppenTable;
	CCSPtrArray<PGPDATA> *pomData;
// Methods which handle changes (from Data Distributor)
public:
    CCSPtrArray<PLANUNGSGRUPPENTABLE_LINEDATA> omLines;
///////
	CCSPtrArray<TABLE_HEADER_COLUMN> omHeaderDataArray;
//Print 
	void PrintTableView();
	bool PrintTableLine(PLANUNGSGRUPPENTABLE_LINEDATA *prpLine,bool bpLastLine);
	bool PrintTableHeader();
	CCSPrint *pomPrint;
	CString omTableName;

};

#endif //__PlanungsgruppenTableViewer_H__
