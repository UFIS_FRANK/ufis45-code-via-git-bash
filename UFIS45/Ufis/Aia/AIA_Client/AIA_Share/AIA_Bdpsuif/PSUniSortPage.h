#if !defined(AFX_CCSUNISORTPAGE_H__18568972_4EA3_11D1_9881_000001014864__INCLUDED_)
#define AFX_CCSUNISORTPAGE_H__18568972_4EA3_11D1_9881_000001014864__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#include "Ansicht.h"
#include "CedaALTData.h"
#include "CedaACTData.h"
#include "CedaACRData.h"
#include "CedaAPTData.h"
#include "CedaRWYData.h"
#include "CedaTWYData.h"
#include "CedaPSTData.h"
#include "CedaGATData.h"
#include "CedaCICData.h"
#include "CedaBLTData.h"
#include "CedaEXTData.h"
#include "CedaDENData.h"
#include "CedaMVTData.h"
#include "CedaNATData.h"
#include "CedaHAGData.h"
#include "CedaWROData.h"
#include "CedaHTYData.h"
#include "CedaSTYData.h"
#include "CedaFIDData.h"
#include "CedaSphData.h"
#include "CedaStrData.h"
#include "CedaSeaData.h"
#include "CedaGhsData.h"
#include "CedaPerData.h"
#include "CedaGrnData.h"
#include "CedaOrgData.h"
#include "CedaWayData.h"
#include "CedaChtData.h"
#include "CedaTipData.h"
#include "CedaHolData.h"
#include "CedaWgpData.h"
#include "CedaPgpData.h"
#include "CedaAwiData.h"
#include "CedaCccData.h"
#include "CedaVipData.h"
#include "CedaAFMData.h"
#include "CedaENTData.h"

/////////////////////////////////////////////////////////////////////////////
// CPSUniSortPage dialog

class CPSUniSortPage : public CPropertyPage//CBasePropSheetPage
{
	DECLARE_DYNCREATE(CPSUniSortPage)

// Construction
public:
	CPSUniSortPage();
	~CPSUniSortPage();
public:
// Dialog Data
	//{{AFX_DATA(CPSUniSortPage)
	enum { IDD = IDD_PSUNISORT_PAGE };
	CListBox	m_ListFields;
	CString	m_EditSort;
	CEdit E_SortText;
	//}}AFX_DATA
	CStringArray omDescArray;
	CStringArray omFieldArray;

	CStringArray omValues;
	CString omCalledFrom;

	CCSCedaData *pomCedaData;

	void SetData();
	void GetData();

	void SetFieldAndDescriptionArrays();
	void InitMask();
	void SetCalledFrom(CString opCalledFrom);
// Overrides
	// ClassWizard generate virtual function overrides
	//{{AFX_VIRTUAL(CPSUniSortPage)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL
	CString GetPrivList(){return("");};

// Implementation
protected:
	// Generated message map functions
	//{{AFX_MSG(CPSUniSortPage)
	afx_msg void OnButtonAsc();
	afx_msg void OnButtonDelete();
	afx_msg void OnButtonDesc();
	afx_msg void OnButtonNew();
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CCSUNISORTPAGE_H__18568972_4EA3_11D1_9881_000001014864__INCLUDED_)
