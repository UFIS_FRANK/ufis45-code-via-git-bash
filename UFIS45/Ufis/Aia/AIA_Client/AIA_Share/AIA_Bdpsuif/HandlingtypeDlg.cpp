// HandlingtypeDlg.cpp : implementation file
//

#include "stdafx.h"
#include "BDPSUIF.h"
#include "HandlingtypeDlg.h"
#include "PrivList.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// HandlingtypeDlg dialog


HandlingtypeDlg::HandlingtypeDlg(HTYDATA *popHTY,CWnd* pParent /*=NULL*/) : CDialog(HandlingtypeDlg::IDD, pParent)
{
	pomHTY = popHTY;

	//{{AFX_DATA_INIT(HandlingtypeDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void HandlingtypeDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(HandlingtypeDlg)
	DDX_Control(pDX, IDOK, m_OK);
	DDX_Control(pDX, IDC_CDAT_D, m_CDATD);
	DDX_Control(pDX, IDC_CDAT_T, m_CDATT);
	DDX_Control(pDX, IDC_LSTU_D, m_LSTUD);
	DDX_Control(pDX, IDC_LSTU_T, m_LSTUT);
	DDX_Control(pDX, IDC_USEU,	 m_USEU);
	DDX_Control(pDX, IDC_USEC,	 m_USEC);
	DDX_Control(pDX, IDC_HTYP,   m_HTYP);
	DDX_Control(pDX, IDC_HNAM,   m_HNAM);
	DDX_Control(pDX, IDC_VAFR_D, m_VAFRD);
	DDX_Control(pDX, IDC_VAFR_T, m_VAFRT);
	DDX_Control(pDX, IDC_VATO_D, m_VATOD);
	DDX_Control(pDX, IDC_VATO_T, m_VATOT);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(HandlingtypeDlg, CDialog)
	//{{AFX_MSG_MAP(HandlingtypeDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// HandlingtypeDlg message handlers
//----------------------------------------------------------------------------------------

BOOL HandlingtypeDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	m_Caption = LoadStg(IDS_STRING161) + " <" + m_Caption + ">";
	SetWindowText(m_Caption);

	char clStat = ogPrivList.GetStat("HANDLINGTYPEDLG.m_OK");
	SetWndStatAll(clStat,m_OK);

	//------------------------------------
	m_CDATD.SetBKColor(SILVER);
	m_CDATD.SetInitText(pomHTY->Cdat.Format("%d.%m.%Y"));
	m_CDATD.SetSecState(ogPrivList.GetStat("HANDLINGTYPEDLG.m_CDAT"));
	m_CDATT.SetBKColor(SILVER);
	m_CDATT.SetInitText(pomHTY->Cdat.Format("%H:%M"));
	m_CDATT.SetSecState(ogPrivList.GetStat("HANDLINGTYPEDLG.m_CDAT"));
	//------------------------------------
	m_LSTUD.SetBKColor(SILVER);
	m_LSTUD.SetInitText(pomHTY->Lstu.Format("%d.%m.%Y"));
	m_LSTUD.SetSecState(ogPrivList.GetStat("HANDLINGTYPEDLG.m_LSTU"));
	m_LSTUT.SetBKColor(SILVER);
	m_LSTUT.SetInitText(pomHTY->Lstu.Format("%H:%M"));
	m_LSTUT.SetSecState(ogPrivList.GetStat("HANDLINGTYPEDLG.m_LSTU"));
	//------------------------------------
	m_USEC.SetBKColor(SILVER);
	m_USEC.SetInitText(pomHTY->Usec);
	m_USEC.SetSecState(ogPrivList.GetStat("HANDLINGTYPEDLG.m_USEC"));
	//------------------------------------
	m_USEU.SetBKColor(SILVER);
	m_USEU.SetInitText(pomHTY->Useu);
	m_USEU.SetSecState(ogPrivList.GetStat("HANDLINGTYPEDLG.m_USEU"));
	//------------------------------------//WHITE,YELLOW,SILVER,RED,LYELLOW
	m_HTYP.SetFormat("x|#x|#");
	m_HTYP.SetTextLimit(1,2);
	m_HTYP.SetBKColor(YELLOW);  
	m_HTYP.SetTextErrColor(RED);
	m_HTYP.SetInitText(pomHTY->Htyp);
	m_HTYP.SetSecState(ogPrivList.GetStat("HANDLINGTYPEDLG.m_HTYP"));
	//------------------------------------
	m_HNAM.SetTypeToString("X(30)",30,1);
	m_HNAM.SetBKColor(YELLOW);
	m_HNAM.SetTextErrColor(RED);
	m_HNAM.SetInitText(pomHTY->Hnam);
	m_HNAM.SetSecState(ogPrivList.GetStat("HANDLINGTYPEDLG.m_HNAM"));
	//------------------------------------
	m_VAFRD.SetTypeToDate(true);
	m_VAFRD.SetTextErrColor(RED);
	m_VAFRD.SetBKColor(YELLOW);
	m_VAFRD.SetInitText(pomHTY->Vafr.Format("%d.%m.%Y"));
	m_VAFRD.SetSecState(ogPrivList.GetStat("HANDLINGTYPEDLG.m_VAFR"));
	// - - - - - - - - - - - - - - - - - -
	m_VAFRT.SetTypeToTime(true);
	m_VAFRT.SetTextErrColor(RED);
	m_VAFRT.SetBKColor(YELLOW);
	m_VAFRT.SetInitText(pomHTY->Vafr.Format("%H:%M"));
	m_VAFRT.SetSecState(ogPrivList.GetStat("HANDLINGTYPEDLG.m_VAFR"));
	//------------------------------------
	m_VATOD.SetTypeToDate();
	m_VATOD.SetTextErrColor(RED);
	m_VATOD.SetInitText(pomHTY->Vato.Format("%d.%m.%Y"));
	m_VATOD.SetSecState(ogPrivList.GetStat("HANDLINGTYPEDLG.m_VATO"));
	// - - - - - - - - - - - - - - - - - -
	m_VATOT.SetTypeToTime();
	m_VATOT.SetTextErrColor(RED);
	m_VATOT.SetInitText(pomHTY->Vato.Format("%H:%M"));
	m_VATOT.SetSecState(ogPrivList.GetStat("HANDLINGTYPEDLG.m_VATO"));
	//------------------------------------
	return TRUE;  
}

//----------------------------------------------------------------------------------------

void HandlingtypeDlg::OnOK() 
{
	AfxGetApp()->DoWaitCursor(1);
	CString olErrorText;
	bool ilStatus = true;
	if(m_HTYP.GetStatus() == false)
	{
		ilStatus = false;
		if(m_HTYP.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING237) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING237) + ogNotFormat;
		}
	}
	if(m_HNAM.GetStatus() == false)
	{
		ilStatus = false;
		if(m_HNAM.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING288) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING288) + ogNotFormat;
		}
	}
	if(m_VAFRD.GetStatus() == false)
	{
		ilStatus = false;
		if(m_VAFRD.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING230) + LoadStg(IDS_STRING232) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING230) + LoadStg(IDS_STRING232) + ogNotFormat;
		}
	}
	if(m_VAFRT.GetStatus() == false)
	{
		ilStatus = false;
		if(m_VAFRT.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING230) + LoadStg(IDS_STRING233) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING230) + LoadStg(IDS_STRING233) + ogNotFormat;
		}
	}
	if(m_VATOD.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING231) + LoadStg(IDS_STRING232) + ogNotFormat;
	}
	if(m_VATOT.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING231) + LoadStg(IDS_STRING233) + ogNotFormat;
	}

	///////////////////////////////////////////////////////////////////////////
	CString olVafrd,olVafrt,olVatod,olVatot;
	CTime olTmpTimeFr,olTmpTimeTo;
	m_VAFRD.GetWindowText(olVafrd);
	m_VAFRT.GetWindowText(olVafrt);
	m_VATOD.GetWindowText(olVatod);
	m_VATOT.GetWindowText(olVatot);

	if((m_VAFRD.GetWindowTextLength() != 0 && m_VAFRT.GetWindowTextLength() == 0) || (m_VAFRD.GetWindowTextLength() == 0 && m_VAFRT.GetWindowTextLength() != 0))
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING234);
	}
	if((m_VATOD.GetWindowTextLength() != 0 && m_VATOT.GetWindowTextLength() == 0) || (m_VATOD.GetWindowTextLength() == 0 && m_VATOT.GetWindowTextLength() != 0))
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING235);
	}
	if(m_VAFRD.GetStatus() == true && m_VAFRT.GetStatus() == true && m_VATOD.GetStatus() == true && m_VATOT.GetStatus() == true)
	{
		olTmpTimeFr = DateHourStringToDate(olVafrd,olVafrt);
		olTmpTimeTo = DateHourStringToDate(olVatod,olVatot);
		if(olTmpTimeTo != -1 && (olTmpTimeTo < olTmpTimeFr || olTmpTimeFr == -1))
		{
			ilStatus = false;
			olErrorText += LoadStg(IDS_STRING236);
		}
	}

	//Konsistenz-Check//
	if(ilStatus == true)
	{
		CString olHtyp;
		char clWhere[100];
		CCSPtrArray<HTYDATA> olHtyCPA;

		m_HTYP.GetWindowText(olHtyp);
		sprintf(clWhere,"WHERE HTYP='%s'",olHtyp);
		if(ogHTYData.ReadSpecialData(&olHtyCPA,clWhere,"URNO,HTYP",false) == true)
		{
			if(olHtyCPA[0].Urno != pomHTY->Urno)
			{
				ilStatus = false;
				olErrorText += LoadStg(IDS_STRING237) + LoadStg(IDS_EXIST);
			}
		}
		olHtyCPA.DeleteAll();
	}

	///////////////////////////////////////////////////////////////////////////
	AfxGetApp()->DoWaitCursor(-1);
	if (ilStatus == true)
	{
		m_HTYP.GetWindowText(pomHTY->Htyp,3);
		m_HNAM.GetWindowText(pomHTY->Hnam,31);
		pomHTY->Vafr = DateHourStringToDate(olVafrd,olVafrt);
		pomHTY->Vato = DateHourStringToDate(olVatod,olVatot);

		CDialog::OnOK();
	}
	else
	{
		Beep(440,70);
		MessageBox(olErrorText,LoadStg(IDS_STRING145),MB_ICONEXCLAMATION);
		m_HTYP.SetFocus();
		m_HTYP.SetSel(0,-1);
	}
}

//----------------------------------------------------------------------------------------

