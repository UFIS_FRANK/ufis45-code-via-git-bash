// CedaOdaData.h

#ifndef __CEDAPODADATA__
#define __CEDAPODADATA__
 
#include "stdafx.h"
#include "basicdata.h"

//---------------------------------------------------------------------------------------------------------

struct ODADATA 
{
	CTime	 Cdat;			// Erstellungsdatum
	char	 Ctrc[5+2];		// Vertragsart.code
	char	 Dptc[5+2];		// Organisationseinheit,code
	char	 Dura[4+2];		// Dauer (Ersatzstunden)
	CTime	 Lstu;			// Datum letzte �nderung
	char	 Prfl[1+2];		// Protokollierungskennung
	char	 Rema[60+2];	// Bemerkungen
	char	 Sdac[5+2];		// Code
	char	 Sdak[12+2];	// Kurzbezeichnung
	char	 Sdan[40+2];	// Bezeichnung
	char	 Sdas[6+2];		// SAP-Code
#ifdef CLIENT_SHA
	char	 Syme[8+2];		// Symbol Extern
	char	 Symi[8+2];		// Symbol Intern
#endif
	char	 Tatp[1+2];		// Auswirkung auf ZK
	char	 Tsap[1+2];		// Send to SAP
	char	 Type[2+2];		// Type Intern
	char	 Upln[1+2];		// in Urlaubsplanung
	long	 Urno;			// Eindeutige Datensatz-Nr.
	char	 Usec[32+2];	// Anwender (Ersteller)
	char	 Useu[32+2];	// Anwender (letzte �nderung)
	char	 Free[3];		// regul�r arbeitsfrei

  
	//DataCreated by this class
	int      IsChanged;

	//long, CTime
	ODADATA(void)
	{ 
		memset(this,'\0',sizeof(*this));
		Cdat=-1; 
		Lstu=-1;
	}

}; // end ODADataStruct

//---------------------------------------------------------------------------------------------------------
// Class declaratino

class CedaOdaData: public CCSCedaData
{
// Attributes
public:
    CMapPtrToPtr omUrnoMap;

    CCSPtrArray<ODADATA> omData;

	char pcmListOfFields[2048];

// OOdaations
public:
    CedaOdaData();
	~CedaOdaData();
	virtual void GetDataInfo(CStringArray &ropFields, CStringArray &ropDesription, CStringArray &ropType);
	void Register(void);
	void ClearAll(bool bpWithRegistration = true);
    bool Read(char *pspWhere = NULL);
	bool Insert(ODADATA *prpOda);
	bool InsertInternal(ODADATA *prpOda);
	bool Update(ODADATA *prpOda);
	bool UpdateInternal(ODADATA *prpOda);
	bool Delete(long lpUrno);
	bool DeleteInternal(ODADATA *prpOda);
	bool ReadSpecialData(CCSPtrArray<ODADATA> *popOda,char *pspWhere,char *pspFieldList,bool ipSYS=true);
	bool Save(ODADATA *prpOda);
	void ProcessBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);
	ODADATA  *GetOdaByUrno(long lpUrno);


	// Private methods
private:
    void PrepareOdaData(ODADATA *prpOdaData);

};

//---------------------------------------------------------------------------------------------------------


#endif //__CEDAPODADATA__
