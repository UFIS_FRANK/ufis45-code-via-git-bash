#if !defined(AFX_CCSPSUNIFILTER_H__601EC771_25CE_11D1_987D_000001014864__INCLUDED_)
#define AFX_CCSPSUNIFILTER_H__601EC771_25CE_11D1_987D_000001014864__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
//
#include "CCSEdit.h"
#include "Ansicht.h"
#include "CedaALTData.h"
#include "CedaACTData.h"
#include "CedaACRData.h"
#include "CedaAPTData.h"
#include "CedaRWYData.h"
#include "CedaTWYData.h"
#include "CedaPSTData.h"
#include "CedaGATData.h"
#include "CedaCICData.h"
#include "CedaBLTData.h"
#include "CedaEXTData.h"
#include "CedaDENData.h"
#include "CedaMVTData.h"
#include "CedaNATData.h"
#include "CedaHAGData.h"
#include "CedaWROData.h"
#include "CedaHTYData.h"
#include "CedaSTYData.h"
#include "CedaFIDData.h"
#include "CedaSphData.h"
#include "CedaStrData.h"
#include "CedaSeaData.h"
#include "CedaGhsData.h"
#include "CedaPerData.h"
#include "CedaGrnData.h"
#include "CedaOrgData.h"
#include "CedaWayData.h"
#include "CedaChtData.h"
#include "CedaTipData.h"
#include "CedaHolData.h"
#include "CedaWgpData.h"
#include "CedaPgpData.h"
#include "CedaAwiData.h"
#include "CedaCccData.h"
#include "CedaVipData.h"
#include "CedaAFMData.h"
#include "CedaENTData.h"

/////////////////////////////////////////////////////////////////////////////
// CPsUniFilter dialog

class CPsUniFilter : public CPropertyPage//CBasePropSheetPage
{
	DECLARE_DYNCREATE(CPsUniFilter)

// Construction
public:
	CPsUniFilter();
	~CPsUniFilter();


	void SetCaption(const char *pcpCaption);
	CStringArray omDescArray;
	CStringArray omFieldArray;
	CStringArray omTypeArray;
	CStringArray omOperators;
	CString omFilter;

	CStringArray omValues;
	CString omCalledFrom;
	CCSCedaData *pomCedaData;
// Dialog Data
	//{{AFX_DATA(CPsUniFilter)
	enum { IDD = IDD_PSUNIFILTER_PAGE };
	CComboBox	m_ComboDatenfeld;
	CComboBox	m_ComboOperator;
	CComboBox	m_ComboTabelle;
	CString	m_EditBedingung;
	CCSEdit	m_CtrlBedingung;
	CEdit m_Where;
	CEdit E_EditFilter;
	CString	m_EditFilter;
	CButton		m_And;
	CButton		m_Or;
	//}}AFX_DATA

	void InitMask();
	void SetFieldAndDescriptionArrays();
	void SetData();
	void GetData();
	void StripSign(CString &ropString, char cpChar);
	bool ReverseDateString(CString &ropSource);
	void SetCalledFrom(CString opCalledFrom);

// Overrides
	// ClassWizard generate virtual function overrides
	//{{AFX_VIRTUAL(CPsUniFilter)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnCommand(WPARAM wParam, LPARAM lParam);
	//}}AFX_VIRTUAL
	CString GetPrivList(){return("");};

// Implementation
protected:
	CString omConnection;
	bool blIsInit;
	
	// Generated message map functions
	//{{AFX_MSG(CPsUniFilter)
	afx_msg void OnBUTTONNeu();
	afx_msg void OnBUTTONSetzen();
	afx_msg void OnSelchangeCOMBOTabelle();
	afx_msg void OnSelchangeCOMBODatenfeld();
	virtual BOOL OnInitDialog();
	afx_msg void OnRadioAnd();
	afx_msg void OnRadioOr();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CCSPSUNIFILTER_H__601EC771_25CE_11D1_987D_000001014864__INCLUDED_)
