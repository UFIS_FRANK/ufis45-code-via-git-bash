// FlViewer.h
//
#ifndef __APPLTABLEVIEWER_H__
#define __APPLTABLEVIEWER_H__

#include "CCSPtrArray.h"
#include "CCSTable.h"
#include "CedaSecData.h"



struct APPLTAB_LINEDATA
{
	char			URNO[URNOLEN];	// URNO in SECTAB of the application
	char			USID[USIDLEN];	// USID of of the application
	INSTALLED_STAT	isInstalled;

	APPLTAB_LINEDATA(void)
	{
		memset(URNO,0,URNOLEN);
		memset(USID,0,USIDLEN);
		isInstalled = IS_INSTALLED;
	}
};


class ApplTableViewer
{
// Constructions
public:
    ApplTableViewer();
    ~ApplTableViewer();

//    void Attach(CCSTable *popAttachWnd);
    void ChangeViewTo(CMapStringToPtr &ropFfktMap);


// Internal data processing routines
public:

    void MakeLines(CMapStringToPtr &ropFfktMap);
	void MakeLine(SECDATA *prpSec,CMapStringToPtr &ropFfktMap);
	int CreateLine(APPLTAB_LINEDATA *);
	void DeleteAll();
	void DeleteLine(int);

	BOOL IsPassFilter(const char *);
	int CompareLine(APPLTAB_LINEDATA *,APPLTAB_LINEDATA *);
	int	 GetSize();
	void Update(char *pcpUsid);
	void UpdateDisplay();
	void Attach(CCSTable *popTable);

// Attributes
public:
	CCSTable	*pomTable;
    CCSPtrArray <APPLTAB_LINEDATA> omLines;
	char		pcmFsec[URNOLEN];
	void		SelectLine(const int ipLine);

};

#endif //__APPLTABLEVIEWER_H__
