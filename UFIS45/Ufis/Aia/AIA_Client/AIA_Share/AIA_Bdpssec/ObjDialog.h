// ObjDialog.h : header file
//
#include "ApplTableViewer.h"
#include "SubdTableViewer.h"
#include "FuncTableViewer.h"
#include "ProfileListViewer.h"
#include "CcsPrint.h"

/////////////////////////////////////////////////////////////////////////////
// ObjDialog dialog


#define NAMECOLUMNWIDTH 150
#define STATUSCOLUMNWIDTH 75
#define TYPECOLUMNWIDTH 90
#define MENUCOLUMNWIDTH 135
#define BUFLEN 100

#define PRINT_ALL 0
#define PRINT_APPL 1
#define PRINT_SUBD 2

class ObjDialog : public CDialog
{
// Construction
public:
	ObjDialog(CWnd* pParent = NULL);   // standard constructor
    ~ObjDialog();

// Dialog Data
	//{{AFX_DATA(ObjDialog)
	enum { IDD = IDD_OBJDIALOG };
	CStatic	m_ProfilesUsed;
	CStatic	m_ProfileInfo;
	CComboBoxEx	m_ProfileList;
	CButton	m_OK;
	CButton	m_Cancel;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(ObjDialog)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(ObjDialog)
	virtual BOOL OnInitDialog();
	afx_msg LONG OnTableLButtonDown(UINT ipParam, LONG lpParam);
	afx_msg LONG OnTableRButtonDown(UINT ipParam, LONG lpParam);
	afx_msg LONG OnTableSelChange(UINT ipParam, LONG lpParam);
	afx_msg LONG OnTableLButtonDblClk(UINT ipParam, LONG lpParam);
	virtual void OnOK();
	afx_msg void OnInstallModule();
	afx_msg void OnDeinstallModule();
	afx_msg void OnReinstallModule();
	afx_msg void OnEnableSelection();
	afx_msg void OnDisableSelection();
	afx_msg void OnHideSelection();
	afx_msg void OnEnableAll();
	afx_msg void OnDisableAll();
	afx_msg void OnHideAll();
	afx_msg void OnSelchangeProfilelist();
	afx_msg void OnPrintrights();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

	void UpdateView();

private:

	ProfileListViewer omProfileListViewer;
	ProfileListItem *pomProfileListItem;

	ApplTableViewer omApplTableViewer;
	CCSTable *pomApplTable;

	SubdTableViewer omSubdTableViewer;
	CCSTable *pomSubdTable;

	FuncTableViewer omFuncTableViewer;
	CCSTable *pomFuncTable;

	CImageList omImageList;
	
public:

	char		pcmFsec[URNOLEN];
	char		pcmFapp[URNOLEN];
//	char		pcmProf[URNOLEN];
	CString		omStatList; // list of URNO,STAT combinations to be updated
	bool		bmPersonalProf;
	bool		bmMaskChanged; 
	int			imLine; // for applications this is the line selected on the main screen
	void		InstallApplication(const int ipLine);
	void		ReinstallApplication(const int ipLine);
	void		DisplayProfile();
	void		CreateAndDisplayProfileList(int ipSelectProfile);
	CCSPrint	*pomPrint; // object used to print
	void		PrintFktRights(int ipSel);
	void		PrintPrvRights(int ipSel);
	bool		PrintLine(ProfileListObject *prpObj, int ipOrientation);


public:


};
