// BDPS_SEC.h : main header file for the BDPS_SEC application
//
#ifndef __ABOUTDLG__
#define __ABOUTDLG__

#include "resource.h"

class CAboutSecDlg : public CDialog
{
public:
	CAboutSecDlg();

// Dialog Data
	//{{AFX_DATA(CAboutSecDlg)
	enum { IDD = IDD_ABOUTBOX };
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAboutSecDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	//{{AFX_MSG(CAboutSecDlg)
		// No message handlers
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

#endif // __ABOUTDLG__