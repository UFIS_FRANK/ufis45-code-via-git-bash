#ifndef __CEDASEC_DATA__
#define __CEDASEC_DATA__

#include "CedaCalData.h"	// need CALDATA --> returned from create user
#include "CedaGrpData.h"	// need GRPDATA --> returned from create user

static void CedaSecDataCf(void *popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

#include "CCSGlobl.h"
#include "CCSCedaData.h"
/////////////////////////////////////////////////////////////////////////////
struct SECDATA {

	char	URNO[URNOLEN];
	char	USID[USIDLEN];
	char	NAME[NAMELEN];
	char	TYPE[TYPELEN];
	char	LANG[LANGLEN];
	char	STAT[STATLEN];
	char	REMA[REMALEN];

	int IsChanged;

	SECDATA(void)
	{
		strcpy(URNO, "");
		strcpy(USID, "");
		strcpy(NAME, "");
		strcpy(TYPE, "");
		strcpy(LANG, "");
		strcpy(STAT, "");
		strcpy(REMA, "");
		IsChanged = DATA_UNCHANGED;
	}

};

#define ORA_NOT_FOUND "ORA-01403"

class CedaSecData: public CCSCedaData
{

// Attributes
public:
    CCSPtrArray <SECDATA> omData;
	CMapStringToPtr omUrnoMap;
	CCSPtrArray <CEDARECINFO> omRetRecInfo; // contains record info for reading CRU return values


// Operations
public:
    CedaSecData();
	~CedaSecData();
	void ClearAll(void);

    bool Read();
	bool Add(SECDATA *prpSec);

	SECDATA* GetSecByUrno(const char *pcpUrno);
	SECDATA* GetSecByUsid(const char *pcpUSID, SEC_REC_TYPE rpCurrType);
	bool NoRecords(const char *pcpType);

	// NEWSEC - insert into DB function call
	bool NewSec( SECDATA  *prpSecRec, CCSPtrArray <CALDATA> &ropCalRecs, const int ipMODU, char *pcpErrorMessage);

	// NEWAPP - insert appl into DB
	bool NewApp( SECDATA  *prpSecRec, CCSPtrArray <CALDATA> &ropCalRecs, char *pcpErrorMessage);

	// UPDSEC - update DB function call
	bool UpdSec( SECDATA  *prpSecRec, char *pcpErrorMessage);

	// DELSEC - delete from DB function call
	bool DelSec( const char *pcpUrno, char *pcpErrorMessage);

	// DELAPP - delete appl from DB function call
	bool DelApp( const char *pcpUrno, char *pcpErrorMessage);

	// SPW - reset password
	bool SetPwd( const char *pcpUrno, const char *pcpPass, char *pcpErrorMessage );

	// update a record in omData
	void Update( SECDATA  *prpSec );

	// insert a record into omData
	void Insert( SECDATA  *prpSec);

	// delete a record from omData
	void Delete( const char  *pcpUrno);

};

extern CedaSecData ogCedaSecData;

#endif //__CEDASEC_DATA__
