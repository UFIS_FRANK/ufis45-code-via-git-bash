//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by BDPS_SEC.rc
//
#define ID_PRINTRIGHTS                  3
#define IDB_LOGIN                       10
#define IDM_ABOUTBOX                    0x0010
#define IDI_UFIS                        58
#define IDD_INITIALLOAD                 71
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_LOGIN                       101
#define WM_ENABLE_SELECTION             101
#define IDD_BDPS_SEC_DIALOG             102
#define WM_DISABLE_SELECTION            102
#define IDD_BUTTONLIST                  103
#define WM_HIDE_SELECTION               103
#define IDS_FREE1                       103
#define WM_INSTALL_MODULE               104
#define IDS_ACTIVATED                   104
#define WM_DEINSTALL_MODULE             105
#define IDS_DEACTIVATED                 105
#define WM_REINSTALL_MODULE             106
#define IDS_HIDDEN                      106
#define WM_ENABLE_ALL                   107
#define IDS_NO_LINE_SELECTED2           107
#define IDS_NO_LINE_SELECTED            107
#define WM_DISABLE_ALL                  108
#define IDS_DATABASE_ERROR              108
#define WM_HIDE_ALL                     109
#define IDS_DEFAULT_PASSWORD            109
#define WM_CLOSE_ALLOCDLG               110
#define IDS_NUM_DB_RECS_READ            110
#define WM_INSERT_DATE                  111
#define IDS_LOADING_TABLE               111
#define WM_DELETE_DATE                  112
#define IDS_TABLE_LOAD_ERROR            112
#define WM_UPDATE_CAL                   113
#define IDS_ALL_TABLES_LOADED           113
#define IDS_STRING114                   114
#define IDS_OBJLIST_SECTIONNAME         114
#define IDS_STRING115                   115
#define IDS_STRING116                   116
#define IDS_STRING117                   117
#define IDS_STRING118                   118
#define IDS_STRING119                   119
#define IDS_STRING120                   120
#define IDS_STRING121                   121
#define IDS_STRING122                   122
#define IDS_STRING123                   123
#define IDS_STRING124                   124
#define IDS_STRING125                   125
#define IDS_STRING126                   126
#define IDS_STRING127                   127
#define IDR_MAINFRAME                   128
#define IDD_EDIT_USER                   129
#define IDS_SECTABLEVIEWER_VALID        129
#define IDR_BDPSSEC_MENU                130
#define IDS_SECTABLEVIEWER_INVALID      130
#define IDB_TL_GREEN                    131
#define IDS_SECTABLEVIEWER_UNKNOWN      131
#define IDB_TL_RED                      132
#define IDS_SECTABLEVIEWER_NOPROFILES   132
#define IDD_OBJDIALOG                   133
#define IDB_TL_AMBER                    133
#define IDS_SECTABLEVIEWER_NOMODULES    133
#define IDD_ALLOCATED                   134
#define IDS_SECTABLEVIEWER_NOUSERS      134
#define IDI_VALIDFINALPROFILE           134
#define IDS_SECTABLEVIEWER_NOGROUPS     135
#define IDI_VALIDPROFILE                135
#define IDS_SECTABLEVIEWER_NOWKS        136
#define IDI_INVALIDPROFILE              136
#define IDS_SECTABLEVIEWER_NOWKSGROUPS  137
#define IDI_VALIDGROUP                  137
#define IDS_SECTABLEVIEWER_PROFILENAME  138
#define IDI_INVALIDGROUP                138
#define IDS_SECTABLEVIEWER_MODULENAME   139
#define IDI_ERRORRECURSION              139
#define IDS_SECTABLEVIEWER_USERNAME     140
#define IDI_INVALIDFINALPROFILE         140
#define IDS_SECTABLEVIEWER_GROUPNAME    141
#define IDI_ATTENTION_ICON              141
#define IDS_SECTABLEVIEWER_WKSNAME      142
#define IDD_PRINTRIGHTSDLG              142
#define IDS_SECTABLEVIEWER_WKSGROUPNAME 143
#define IDS_SECTABLEVIEWER_PROFILEHEADER 144
#define IDD_REGISTERDLG                 144
#define IDS_SECTABLEVIEWER_VALIDFROM    145
#define IDD_PRINTRIGHTSDLG1             145
#define IDS_SECTABLEVIEWER_VALIDTO      146
#define IDS_SECTABLEVIEWER_FREQ         147
#define IDS_SECTABLEVIEWER_STATUS       148
#define IDS_SECTABLEVIEWER_NAME         149
#define IDS_SECTABLEVIEWER_REMARK       150
#define IDS_STRING151                   151
#define IDS_STRING152                   152
#define IDS_STRING153                   153
#define IDS_STRING154                   154
#define IDS_STRING155                   155
#define IDS_STRING156                   156
#define IDB_EXCLAMATION                 156
#define IDS_STRING157                   157
#define IDS_STRING158                   158
#define IDS_STRING159                   159
#define IDS_OBJDIALOG_PROFILENAME       160
#define IDS_OBJDIALOG_MODULENAME        161
#define IDS_OBJDIALOG_USERNAME          162
#define IDS_OBJDIALOG_GROUPNAME         163
#define IDS_OBJDIALOG_WKSNAME           164
#define IDS_OBJDIALOG_WKSGROUPNAME      165
#define IDS_OBJDIALOG_NOTPERSONALPROFILE 166
#define IDS_OBJDIALOG_INSTALLAPPLTITLE  167
#define IDS_OBJDIALOG_INSTALLAPPL       168
#define IDS_OBJDIALOG_REINSTALLAPPLTITLE 169
#define IDS_OBJDIALOG_REINSTALLAPPL     170
#define IDS_OBJDIALOG_PROFILECAPTION    171
#define IDS_OBJDIALOG_MODULECAPTION     172
#define IDS_OBJDIALOG_USERCAPTION       173
#define IDS_OBJDIALOG_GROUPCAPTION      174
#define IDS_OBJDIALOG_WKSCAPTION        175
#define IDS_OBJDIALOG_WKSGROUPCAPTION   176
#define IDS_OBJDIALOG_MENUINSTALL       177
#define IDS_OBJDIALOG_MENUDEINSTALL     178
#define IDS_OBJDIALOG_MENUREINSTALL     179
#define IDS_OBJDIALOG_MENUHIDE          180
#define IDS_OBJDIALOG_MENUENABLE        181
#define IDS_OBJDIALOG_MENUDISABLE       182
#define IDS_STRING183                   183
#define IDS_STRING184                   184
#define IDS_STRING185                   185
#define IDS_STRING186                   186
#define IDS_STRING187                   187
#define IDS_STRING188                   188
#define IDS_STRING189                   189
#define IDS_STRING190                   190
#define IDS_STRING191                   191
#define IDS_SUBDTABLEVIEWER_SUBDIVISION 192
#define IDS_STRING193                   193
#define IDS_STRING194                   194
#define IDS_STRING195                   195
#define IDS_STRING196                   196
#define IDS_STRING197                   197
#define IDS_STRING198                   198
#define IDS_STRING199                   199
#define IDS_STRING200                   200
#define IDS_STRING201                   201
#define IDS_STRING202                   202
#define IDS_STRING203                   203
#define IDS_STRING204                   204
#define IDS_STRING205                   205
#define IDS_STRING206                   206
#define IDS_STRING207                   207
#define IDS_BUTTONLISTDLG_PROFILECAPTION 208
#define IDS_BUTTONLISTDLG_MODULECAPTION 209
#define IDS_BUTTONLISTDLG_USERCAPTION   210
#define IDS_BUTTONLISTDLG_GROUPCAPTION  211
#define IDS_BUTTONLISTDLG_WKSCAPTION    212
#define IDS_BUTTONLISTDLG_WKSGROUPCAPTION 213
#define IDS_BUTTONLISTDLG_NEWPASSWORD   214
#define IDS_BUTTONLISTDLG_NEWPASSWORDCAPTION 215
#define IDS_BUTTONLISTDLG_GETSECBYURNOERROR 216
#define IDS_BUTTONLISTDLG_GETCALBYFSECERROR 217
#define IDS_BUTTONLISTDLG_CONFIRMSETPASS 218
#define IDS_BUTTONLISTDLG_PASSWORDCAPTION 219
#define IDS_BUTTONLISTDLG_DELETELINE    220
#define IDS_BUTTONLISTDLG_DELETEPROFILE 221
#define IDS_BUTTONLISTDLG_DELETEMODULE  222
#define IDS_BUTTONLISTDLG_DELETEUSER    223
#define IDS_BUTTONLISTDLG_DELETEGROUP   224
#define IDS_BUTTONLISTDLG_DELETEWKS     225
#define IDS_BUTTONLISTDLG_DELETEWKSGROUP 226
#define IDS_BUTTONLISTDLG_CONFIRMEXIT   227
#define IDS_BUTTONLISTDLG_MENUINSERT    228
#define IDS_BUTTONLISTDLG_MENUUPDATE    229
#define IDS_BUTTONLISTDLG_MENUDELETE    230
#define IDS_BUTTONLISTDLG_MENURIGHTS    231
#define IDS_BUTTONLISTDLG_MENUALLOCATE  232
#define IDS_BUTTONLISTDLG_MENUPASSWORD  233
#define IDS_STRING234                   234
#define IDS_STRING235                   235
#define IDS_STRING236                   236
#define IDS_STRING237                   237
#define IDS_STRING238                   238
#define IDS_STRING239                   239
#define IDS_FUNCTABLEVIEWER_TYPE        240
#define IDS_FUNCTABLEVIEWER_FUNCTION    241
#define IDS_FUNCTABLEVIEWER_STATUS      242
#define IDS_STRING243                   243
#define IDS_STRING244                   244
#define IDS_STRING245                   245
#define IDS_STRING246                   246
#define IDS_STRING247                   247
#define IDS_STRING248                   248
#define IDS_STRING249                   249
#define IDS_STRING250                   250
#define IDS_STRING251                   251
#define IDS_STRING252                   252
#define IDS_STRING253                   253
#define IDS_STRING254                   254
#define IDS_STRING255                   255
#define IDS_EDITUSERDLG_PROFILENAME     256
#define IDS_EDITUSERDLG_PROFILEENABLED  257
#define IDS_EDITUSERDLG_PROFILEDISABLED 258
#define IDS_EDITUSERDLG_PROFILEINSERT   259
#define IDS_EDITUSERDLG_PROFILEUPDATE   260
#define IDS_EDITUSERDLG_MODULENAME      261
#define IDS_EDITUSERDLG_MODULEENABLED   262
#define IDS_EDITUSERDLG_MODULEDISABLED  263
#define IDS_EDITUSERDLG_MODULEINSERT    264
#define IDS_EDITUSERDLG_MODULEUPDATE    265
#define IDS_EDITUSERDLG_USERNAME        266
#define IDS_EDITUSERDLG_USERENABLED     267
#define IDS_EDITUSERDLG_USERDISABLED    268
#define IDS_EDITUSERDLG_USERINSERT      269
#define IDS_EDITUSERDLG_USERUPDATE      270
#define IDS_EDITUSERDLG_GROUPNAME       271
#define IDS_EDITUSERDLG_GROUPENABLED    272
#define IDS_EDITUSERDLG_GROUPDISABLED   273
#define IDS_EDITUSERDLG_GROUPINSERT     274
#define IDS_EDITUSERDLG_GROUPUPDATE     275
#define IDS_EDITUSERDLG_WKSNAME         276
#define IDS_EDITUSERDLG_WKSENABLED      277
#define IDS_EDITUSERDLG_WKSDISABLED     278
#define IDS_EDITUSERDLG_WKSINSERT       279
#define IDS_EDITUSERDLG_WKSUPDATE       280
#define IDS_EDITUSERDLG_WKSGROUPNAME    281
#define IDS_EDITUSERDLG_WKSGROUPENABLED 282
#define IDS_EDITUSERDLG_WKSGROUPDISABLED 283
#define IDS_EDITUSERDLG_WKSGROUPINSERT  284
#define IDS_EDITUSERDLG_WKSGROUPUPDATE  285
#define IDS_EDITUSERDLG_ERRORVAFRTIME   286
#define IDS_EDITUSERDLG_ERRORVAFRDATE   287
#define IDS_EDITUSERDLG_ERRORVATOTIME   288
#define IDS_EDITUSERDLG_ERRORVATODATE   289
#define IDS_EDITUSERDLG_ERROROLDERVAFR  290
#define IDS_EDITUSERDLG_ERRORUSIDLEN    291
#define IDS_EDITUSERDLG_ERRORDUPLICATEUSID 292
#define IDS_EDITUSERDLG_INSERTERRORCAPTION 293
#define IDS_EDITUSERDLG_INSERTDATE      294
#define IDS_EDITUSERDLG_DELETEDATE      295
#define IDS_STRING296                   296
#define IDS_STRING297                   297
#define IDS_STRING298                   298
#define IDS_STRING299                   299
#define IDS_STRING300                   300
#define IDS_STRING301                   301
#define IDS_STRING302                   302
#define IDS_STRING303                   303
#define IDS_CALTABLEVIEWER_VALIDFROM    304
#define IDS_CALTABLEVIEWER_VALIDTO      305
#define IDS_CALTABLEVIEWER_FREQ         306
#define IDS_STRING307                   307
#define IDS_STRING308                   308
#define IDS_STRING309                   309
#define IDS_APPLTABLEVIEWER_MODULENAME  310
#define IDS_STRING311                   311
#define IDS_STRING312                   312
#define IDS_STRING313                   313
#define IDS_STRING314                   314
#define IDS_STRING315                   315
#define IDS_STRING316                   316
#define IDS_STRING317                   317
#define IDS_STRING318                   318
#define IDS_STRING319                   319
#define IDS_ALLOCATEDDLG_SINGLESELCAPTION 320
#define IDS_ALLOCATEDDLG_MULTISELCAPTION 321
#define IDS_ALLOCATEDDLG_USERCAPTION    322
#define IDS_ALLOCATEDDLG_PROFILECAPTION 323
#define IDS_ALLOCATEDDLG_GROUPCAPTION   324
#define IDS_ALLOCATEDDLG_WKSCAPTION     325
#define IDS_ALLOCATEDDLG_WKSGROUPCAPTION 326
#define IDS_ALLOCATEDDLG_SHOWUSER       327
#define IDS_ALLOCATEDDLG_SHOWPROFILE    328
#define IDS_ALLOCATEDDLG_SHOWGROUP      329
#define IDS_ALLOCATEDDLG_SHOWWKS        330
#define IDS_ALLOCATEDDLG_SHOWWKSGROUP   331
#define IDS_ALLOCATEDDLG_USERNAME       332
#define IDS_ALLOCATEDDLG_USERS          333
#define IDS_ALLOCATEDDLG_PROFILENAME    334
#define IDS_ALLOCATEDDLG_PROFILES       335
#define IDS_ALLOCATEDDLG_GROUPNAME      336
#define IDS_ALLOCATEDDLG_GROUPS         337
#define IDS_ALLOCATEDDLG_WKSNAME        338
#define IDS_ALLOCATEDDLG_WKS            339
#define IDS_ALLOCATEDDLG_WKSGROUPNAME   340
#define IDS_ALLOCATEDDLG_WKSGROUPS      341
#define IDS_ALLOCATEDDLG_UPDATECHANGES  342
#define IDS_STRING343                   343
#define IDS_STRING344                   344
#define IDS_STRING345                   345
#define IDS_STRING346                   346
#define IDS_STRING347                   347
#define IDS_STRING348                   348
#define IDS_STRING349                   349
#define IDS_STRING350                   350
#define IDS_STRING351                   351
#define IDS_INVALID_USERNAME            352
#define IDS_INVALID_APPLICATION         353
#define IDS_INVALID_PASSWORD            354
#define IDS_EXPIRED_USERNAME            355
#define IDS_EXPIRED_APPLICATION         356
#define IDS_EXPIRED_WORKSTATION         357
#define IDS_DISABLED_USERNAME           358
#define IDS_DISABLED_APPLICATION        359
#define IDS_DISABLED_WORKSTATION        360
#define IDS_UNDEFINED_PROFILE           361
#define IDS_INVALIDLOGINCAPTION         362
#define IDS_LOGINCAPTION                363
#define IDS_NOTSYSADMIN                 364
#define IDS_REGISTER                    365
#define IDS_SECSTOSTART                 366
#define IDS_REGISTER2                   367
#define IDS_REGISTER3                   368
#define IDS_CANUSESECSUBD               369
#define IDS_CANUSESECFUAL               370
#define IDS_PROFILESUSED                371
#define IDS_CURRENTRIGHTS               372
#define IDS_STATDISABLED                373
#define IDS_OUTOFDATE                   374
#define IDS_NORIGHTS                    375
#define IDS_PROFILECAPTION              376
#define IDS_PERSONALPROFILE             377
#define IDS_PROFILEDISABLED             378
#define IDS_PROFILEOUTOFDATE            379
#define IDS_PROFILEASSOUTOFDATE         380
#define IDS_GROUPCAPTION                381
#define IDS_GROUPDISABLED               382
#define IDS_GROUPOUTOFDATE              383
#define IDS_GROUPASSOUTOFDATE           384
#define IDC_Title1                      1000
#define IDC_USID_HEAD                   1001
#define IDC_Title2                      1001
#define IDC_Title3                      1002
#define IDC_NAME_HEAD                   1002
#define IDC_USERNAME                    1003
#define IDC_Title4                      1003
#define IDC_REMA_HEAD                   1003
#define IDC_TEXT                        1003
#define IDC_PASSWORD                    1004
#define IDC_TIME                        1004
#define IDC_USID                        1005
#define IDC_NAME                        1006
#define ID_InsertButton                 1007
#define IDC_REMA                        1007
#define IDC_VAFRdate                    1010
#define ID_UpdateButton                 1011
#define ID_DeleteButton                 1013
#define IDC_VATOdate                    1014
#define ID_RightsButton                 1015
#define IDC_VAFRtime                    1016
#define ID_AllocateButton               1017
#define IDC_VATOtime                    1018
#define ID_ExitButton                   1018
#define ID_TestButton                   1019
#define IDC_STAT                        1020
#define ID_ModuleButton                 1021
#define ID_PasswordButton               1022
#define IDC_None                        1023
#define ID_UserButton                   1023
#define IDC_Standard                    1024
#define ID_ProfileButton                1024
#define IDC_DisableAll                  1025
#define ID_GroupButton                  1025
#define IDC_EnableAll                   1026
#define ID_WksButton                    1026
#define IDC_InitialRightsGroup          1027
#define ID_WksGroupButton               1027
#define ID_PrintButton                  1028
#define IDC_VAFR_HEAD                   1029
#define IDC_VAFR_HEAD2                  1030
#define IDC_STAT_HEAD                   1031
#define IDC_ShowAllocatedOnly           1032
#define IDC_FREQ_HEAD                   1032
#define IDC_AllocatedProfilesList       1033
#define IDC_STATUSBAR                   1033
#define IDC_AllocTitle                  1034
#define IDC_USIDCAPTION                 1035
#define IDC_AllocTitle2                 1035
#define IDC_PASSCAPTION                 1036
#define IDC_ListSelection               1036
#define IDC_FREQ_HEAD2                  1038
#define IDC_FREQ1                       1041
#define IDC_FREQ2                       1042
#define IDC_FREQ3                       1043
#define IDC_FREQ4                       1044
#define IDC_FREQ_ALL                    1045
#define IDC_FREQ5                       1046
#define IDC_FREQ6                       1047
#define IDC_FREQ7                       1048
#define IDC_CALGROUP                    1049
#define IDC_CAN_USE_BDSPSEC             1050
#define IDC_PROFILELIST                 1052
#define IDC_PROFILEINFO                 1053
#define IDC_PROFILESUSED                1054
#define IDC_ALL                         1055
#define IDC_ALL2                        1056
#define IDC_REGISTRIEREN                1058
#define IDC_ALL4                        1058
#define IDC_ABBRECHEN                   1059
#define IDC_STARTEN                     1060
#define IDC_MSGLIST                     1147
#define IDC_PROGRESS1                   1148
#define IDD_HELP_USING                  32773
#define IDD_HELP_INDEX                  32774
#define IDD_HELP_CONTENTS               32775
#define ID_Application                  32777
#define ID_User                         32778
#define ID_Profile                      32779
#define ID_ExitBDPSSEC                  32781
#define ID_Group                        32787
#define ID_Wks                          32788
#define ID_WksGroup                     32789

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        143
#define _APS_NEXT_COMMAND_VALUE         32790
#define _APS_NEXT_CONTROL_VALUE         1056
#define _APS_NEXT_SYMED_VALUE           114
#endif
#endif
