// SubdTableViewer.h
//
#ifndef __SUBDTABLEVIEWER_H__
#define __SUBDTABLEVIEWER_H__

#include "CCSPtrArray.h"
#include "CCSTable.h"
#include "CedaPrvData.h"
#include "CedaFktData.h"
#include "ProfileListViewer.h"


struct SUBDTAB_LINEDATA
{
	char	FAPP[URNOLEN];	// URNO in SECTAB of the application
	char	SUBD[SUBDLEN];	// USID of of the application

	SUBDTAB_LINEDATA(void)
	{
		memset(FAPP,0,URNOLEN);
		memset(SUBD,0,SUBDLEN);
	}
};


class SubdTableViewer
{
// Constructions
public:
    SubdTableViewer();
    ~SubdTableViewer();

    void ChangeViewTo(CCSPtrArray <ProfileListObject> &ropPrvData);
	void ChangeViewTo();


// Internal data processing routines
public:

    void MakePrvLines(CCSPtrArray <ProfileListObject> &ropPrvData);
	void MakePrvLine(ProfileListObject *prpObj);
    void MakeFktLines();
	void MakeFktLine(FKTDATA *prpFkt);
	int  CreateLine(SUBDTAB_LINEDATA *);
	void DeleteAll();
	void DeleteLine(int);
	void SelectLine(const int ipLine);

	bool IsPassFilter(const char *);
	bool IsThisFapp(const char *);
	bool AlreadyAdded(char *pcpFapp, char *pcpSubd);
	int  CompareLine(SUBDTAB_LINEDATA *,SUBDTAB_LINEDATA *);
	int	 GetSize();

	void Update(char *pcpUsid);
	void UpdateDisplay();
	void Attach(CCSTable *popTable);

// Attributes
public:
	CCSTable	*pomTable;
    CCSPtrArray <SUBDTAB_LINEDATA> omLines;
	CCSPtrArray <int> omIndicies; // indicies into omLines of the lines displayed in the SubdTable
	char		pcmFapp[URNOLEN]; // URNO of the application selected
	char		pcmFsec[URNOLEN]; // URNO of the user or profile selected
//	char		pcmProf[URNOLEN]; // FSEC field in SUBDTAB/ FREL field in GRPTAB
	char		*GetSubdByLine(const int ipLine);

};

#endif //__SUBDTABLEVIEWER_H__
