// ObjectListBox.cpp : implementation file
//

#include "stdafx.h"
#include "bdps_sec.h"
#include "ObjectListBox.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CObjectListBox

CObjectListBox::CObjectListBox(CWnd *popParent)
{
	pomParent = popParent;
}

CObjectListBox::~CObjectListBox()
{
}


BEGIN_MESSAGE_MAP(CObjectListBox, CListBox)
	//{{AFX_MSG_MAP(CObjectListBox)
		// NOTE - the ClassWizard will add and remove mapping macros here.
	ON_WM_RBUTTONDOWN()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CObjectListBox message handlers
void CObjectListBox::OnRButtonDown(UINT nFlags, CPoint point)
{
	int ilItem = GetItemFromPoint(point);

	if(ilItem != LB_ERR)
	{
		pomParent->SendMessage(WM_UPDATE_CAL, ilItem, 0);
	}
}

int CObjectListBox::GetItemFromPoint(CPoint point)
{
    CRect rcItem;
    for (int itemID = 0; itemID < GetCount(); itemID++)
    {
        GetItemRect(itemID, &rcItem);
        if (rcItem.PtInRect(point))
            return itemID;
    }
    return -1;
}
