// GrpTableViewer.cpp : implementation file
//
#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#include "stdafx.h"
#include "CCSGlobl.h"

#include "BDPS_SEC.h"
#include "GrpTableViewer.h"
#include "CedaSecData.h"
#include "CedaGrpData.h"
#include "basicdata.h"
#include "CCSDdx.h"

#include "MessList.h"


#ifdef _DEBUG
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif


// Local function prototype
static void GrpTableViewerCf(void *popInstance, int ipDDXType,
					   void *vpDataPointer, CString &ropInstanceName);




//*************************************************************************************
//
// GrpTableViewer() - constructor
//
//*************************************************************************************

GrpTableViewer::GrpTableViewer()
{
	ogDdx.Register((void *) this, SEC_VIEWER_UPDATE, CString("GRPTABLE"),CString("GRPTAB INSERT"), GrpTableViewerCf);
	bmAllocOnly = false; // show allocated objects only

}


//*************************************************************************************
//
// GrpTableViewer() - destructor
//
//*************************************************************************************

GrpTableViewer::~GrpTableViewer()
{
    DeleteAll();
}


//*************************************************************************************
//
// DeleteAll() - delete all lines (records) read from SECTAB
//
//*************************************************************************************

void GrpTableViewer::DeleteAll()
{

	int ilNumLines = omLines.GetSize();

	for(int ilLine=1; ilLine<=ilNumLines; ilLine++)
        DeleteLine(0);
}

//*************************************************************************************
//
// GetSize() - return the number of lines in omLines (and thus number of lines in CCSTable)
//
//*************************************************************************************

int GrpTableViewer::GetSize()
{
	return omLines.GetSize();
}

//*************************************************************************************
//
// DeleteLine() - delete a line (record copied from SECTAB) at ipLineno
//
//*************************************************************************************

void GrpTableViewer::DeleteLine(int ipLineno)
{
	
	omLines.DeleteAt(ipLineno);
}

//*************************************************************************************
//
// Attach() - receives a pointer to the CCSTable
//
//*************************************************************************************
/*
void GrpTableViewer::Attach(CCSTable *popTable)
{
    pomTable = popTable;
}
*/

//*************************************************************************************
//
// ChangeViewTo() - function responsible for updating the display
//
//*************************************************************************************

void GrpTableViewer::ChangeViewTo(void)
{
	// delete all lines defined
	DeleteAll();

	// get lines specified in piViewType from CedaSecData into local memory
	MakeLines();

	// draw the lines loaded in CCSTable
	UpdateDisplay();
}




//*************************************************************************************
//
// IsPassFilter() - pcpType='U','P' etc --> used to select either Users,Profiles etc
//                - pcpUrno=Used to make sure a group cannot be allocated to itself
//
//*************************************************************************************

BOOL GrpTableViewer::IsPassFilter(SECDATA  *prpSec)
{
	BOOL blRc = TRUE;

	if(strcmp(prpSec->TYPE,pcmType))
	{
		blRc = FALSE;
	}
	else if(!strcmp(prpSec->URNO,pcmUrno))
	{
		blRc = FALSE;
	}
	else if(!bgIsSuperUser && (!strcmp(prpSec->LANG,"1") || (*prpSec->TYPE == SEC_USER && !strcmp(prpSec->USID,ogSuperUser))))
	{	
		// only the superuser (UFIS$ADMIN) can display other system administrators and itself
		blRc = FALSE;
	}

	return blRc;
}


//*************************************************************************************
//
// CompareLine() - 
//
//*************************************************************************************

int GrpTableViewer::CompareLine(GRPTAB_LINEDATA *prpLine1, GRPTAB_LINEDATA *prpLine2)
{
	return strcmp(prpLine1->USID,prpLine2->USID);
}



/*
//*************************************************************************************
//
// FincLine() - 
//
//*************************************************************************************
BOOL GrpTableViewer::FindLine(const char *pcpUrno, int &rilLineno)
{
	int ilCount = omLines.GetSize();
	for(rilLineno = 0; rilLineno < ilCount; rilLineno++)
	{
		if(! strcmp(omLines[rilLineno].URNO,pcpUrno))
		{
			return TRUE;
		}
	}
	return FALSE;
}
*/
/////////////////////////////////////////////////////////////////////////////
// GrpTableViewer -- code specific to this class





//*************************************************************************************
//
// MakeLines() - loop through the data held in ogCedaSecData copying to local memory
//
//*************************************************************************************

void GrpTableViewer::MakeLines()
{
	int ilCount;

	// flag that indicates that the URNO of the line selected on the main screen is the FSEC field in GRPHAJ
	// this line says if the line clicked in the main screen was not a profile and the type allocated to was
	// either a profile or a group or a workstation group, then pcmUrno is FSEC in GRPTAB
	bmIsFsec = rgCurrType != SEC_PROFILE && (strchr(pcmType,SEC_PROFILE) != NULL || (strchr(pcmType,SEC_GROUP) != NULL && rgCurrType != SEC_GROUP) || (strchr(pcmType,SEC_WKSGROUP) != NULL && rgCurrType != SEC_WKSGROUP));

	ilCount = ogCedaSecData.omData.GetSize();
	for(int i = 0; i < ilCount; i++)
		MakeLine(&ogCedaSecData.omData[i]);
}

//*************************************************************************************
//
// MakeLine() - copy and process a line of data, create its field and display it
//
//*************************************************************************************

void GrpTableViewer::MakeLine(SECDATA  *prpSec)
{

	// filter out either profiles or users or modul
	if( !IsPassFilter(prpSec) ) 
		return;

	
	GRPTAB_LINEDATA rlLine;

	strcpy(rlLine.URNO,prpSec->URNO);
	strcpy(rlLine.USID,prpSec->USID);

	if( bmIsFsec ) // flag that states that the line selected on the main screen is FSEC in GRPTAB
		rlLine.wasAssigned = ogCedaGrpData.GetAssigned(rlLine.URNO,pcmUrno,rlLine.FGRP);
	else
		rlLine.wasAssigned = ogCedaGrpData.GetAssigned(pcmUrno,rlLine.URNO,rlLine.FGRP);
	
	rlLine.isAssigned = rlLine.wasAssigned;
	
	CreateLine(&rlLine);
}



//*************************************************************************************
//
// CreateLine() - add the line to omLines (array of pointers to each line)
//
//*************************************************************************************

int GrpTableViewer::CreateLine(GRPTAB_LINEDATA *prpNewLine)
{
    int ilLineCount = omLines.GetSize();

    for (int ilLineno = 0; ilLineno < ilLineCount; ilLineno++)
        if (CompareLine(prpNewLine, &omLines[ilLineno]) <= 0)
            break;  

	GRPTAB_LINEDATA  rlLine;
    rlLine = *prpNewLine;
	omLines.NewAt(ilLineno, rlLine);

	return ilLineCount;
}

/////////////////////////////////////////////////////////////////////////////
// GrpTableViewer - display drawing routines



//*************************************************************************************
//
// UpdateDisplay() - write local SECTAB data to the CCSTable
//
//*************************************************************************************

void GrpTableViewer::UpdateDisplay()
{
	// clear the list box
	pomList->ResetContent();

	// get and set the name of the user or profile selected
	SECDATA *prlSecRec = ogCedaSecData.GetSecByUrno(pcmUrno);
	pomTitle->SetWindowText(prlSecRec->USID);

	int	ilCurrLine = 0;
	int ilNumLines = omLines.GetSize();
	for (int ilLc = 0; ilLc < ilNumLines; ilLc++)
	{
		GRPTAB_LINEDATA *prlLine = &omLines[ilLc];

		if( !bmAllocOnly || prlLine->isAssigned )
		{
			pomList->AddString(prlLine->USID); // add usid
			if(prlLine->isAssigned)
				pomList->SetSel(ilCurrLine,TRUE);
			ilCurrLine++;
		}
	}


}


static void GrpTableViewerCf(void *popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
    GrpTableViewer *polThis = (GrpTableViewer *)popInstance;

	switch(ipDDXType) {

	case SEC_VIEWER_UPDATE:
        polThis->Update((char *)vpDataPointer);
		break;
	default:
		break;
	}
}


int GrpTableViewer::GetLineByUsid(const char *pcpUsid)
{
	int ilRetLine = -1;
	
	int ilLine, ilNumLines = omLines.GetSize();
	for(ilLine = 0; ilRetLine == -1 && ilLine < ilNumLines; ilLine++ )
		if( !strcmp(omLines[ilLine].USID,pcpUsid) )
			ilRetLine = ilLine;

	return ilRetLine;

} // end GetLineByUsid() */


// update the isAssigned flag using the usid
void GrpTableViewer::UpdateByUsid(char *pcpUsid)
{
	bool blNotFound = true;
	
	int ilLine, ilNumLines = omLines.GetSize();
	for(ilLine = 0; blNotFound && ilLine < ilNumLines; ilLine++ )
	{
		if( !strcmp(omLines[ilLine].USID,pcpUsid) )
		{
			omLines[ilLine].isAssigned = true;
			blNotFound = false;
		}
	}


} // end UpdateByUsid() */


void GrpTableViewer::Update(char *pcpUrno)
{
	// the name of the user or profile selected may have been changed
	if( !strcmp(pcmUrno,pcpUrno) )
	{
		// get and set the name of the user or profile selected
		SECDATA *prlSecRec = ogCedaSecData.GetSecByUrno(pcmUrno);
		pomTitle->SetWindowText(prlSecRec->USID);
	}
}

/*
void GrpTableViewer::Update(char *pcpUrno)
{
	int ilLine;
	bool blRc = true;

	if(FindLine(pcpUrno, ilLine) == TRUE)
	{
		// get the GRP rec
		SECDATA *prlSecRec;
		prlSecRec = ogCedaSecData.GetSecByUrno(pcpUrno);

		if( prlSecRec != NULL )
		{
			strcpy(omLines[ilLine].USID,prlSecRec->USID);

			if( !strcmp(pcmType,"P") )
				omLines[ilLine].wasAssigned = ogCedaGrpData.GetAssigned(pcpUrno,pcmUrno,omLines[ilLine].FGRP);
			else
				omLines[ilLine].wasAssigned = ogCedaGrpData.GetAssigned(pcmUrno,pcpUrno,omLines[ilLine].FGRP);
			
			omLines[ilLine].isAssigned = omLines[ilLine].wasAssigned;

			pomList->DeleteString(ilLine);
			pomList->InsertString(ilLine,prlSecRec->USID);
			if(omLines[ilLine].isAssigned)
				pomList->SetSel(ilLine,TRUE);
		}
	}


}
*/
/*
void GrpTableViewer::Insert(char *pcpUrno)
{
	// update the table
	ChangeViewTo();
}
*/
/*
void GrpTableViewer::Delete(char *pcpUrno)
{
	int ilLine;

	// delete the line from local mem
	if(FindLine(pcpUrno, ilLine) == TRUE)
	{
		DeleteLine(ilLine);

		// update the table
		ChangeViewTo();
	}

}
*/