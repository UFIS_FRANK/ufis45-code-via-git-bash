// CedaCalData.cpp - Read/update SECTAB
//
#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#include "stdafx.h"
#include "CCSGlobl.h"
#include "CedaCalData.h"
#include "CedaSecData.h"
#include "MessList.h"

//
// CedaCalData()
//
// Constructor
//
CedaCalData::CedaCalData()
{

    
	// Create an array of CEDARECINFO for CALDATA
    BEGIN_CEDARECINFO(CALDATA, CedaCalData)
        FIELD_CHAR_TRIM(URNO,"URNO")
        FIELD_CHAR_TRIM(FSEC,"FSEC")
        FIELD_DATE(VAFR,"VPFR")
        FIELD_DATE(VATO,"VPTO")
        FIELD_CHAR_TRIM(FREQ,"FREQ")
    END_CEDARECINFO

    // Copy the record structure
    for (int i = 0; i < sizeof(CedaCalData)/sizeof(CedaCalData[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&CedaCalData[i],sizeof(CEDARECINFO));
        omRecInfo.Add(prpCedaRecInfo);
	}


    // Initialize table name and field names
    sprintf(pcmTableName,"CAL%s",pcgTableExt);
    pcmFieldList = "URNO,FSEC,VAFR,VATO,FREQ";


} // end CedaCalData()


//
// ~CedaCalData()
//
// Destructor
//
CedaCalData::~CedaCalData(void)
{
	//ogCCSDdx.UnRegister(this,NOTUSED);

	omRecInfo.DeleteAll();
	ClearAll();

} // end ~CedaCalData()


//
// ClearAll()
//
// Deletes omData
//
void CedaCalData::ClearAll(void)
{
	omUrnoMap.RemoveAll();
	omFsecMap.RemoveAll();
    omData.DeleteAll();

} // end ClearAll()




//
// Read()
//
// Reads all records from SECTAB loading them into omData
//
bool CedaCalData::Read()
{
    sprintf(pcmTableName,"CAL%s",pcgTableExt);

	
	// the following is another CEDARECINFO structure allowing a different structure
	// to be read in GetBufferRecord() - this must be defined in a different function
	// to the other structure otherwise error !
    // Create an array of CEDARECINFO for RETDATA (return values from CRU)
    BEGIN_CEDARECINFO(RETDATA, CedaRetData)
        FIELD_CHAR_TRIM(TYPE,"TYPE")
        FIELD_CHAR_TRIM(FLD1,"FLD1")
        FIELD_CHAR_TRIM(FLD2,"FLD2")
        FIELD_CHAR_TRIM(FLD3,"FLD3")
        FIELD_CHAR_TRIM(FLD4,"FLD4")
        FIELD_CHAR_TRIM(FLD5,"FLD5")
        FIELD_CHAR_TRIM(FLD6,"FLD6")
        FIELD_CHAR_TRIM(FLD7,"FLD7")
        FIELD_CHAR_TRIM(FLD8,"FLD8")
    END_CEDARECINFO

    // Copy the record structure
    for (int i = 0; i < sizeof(CedaRetData)/sizeof(CedaRetData[0]); i++)
	{
		CEDARECINFO *prpCedaRetRecInfo = new CEDARECINFO;
		memcpy(prpCedaRetRecInfo,&CedaRetData[i],sizeof(CEDARECINFO));
        omRetRecInfo.Add(prpCedaRetRecInfo);
	}
	
	ogDdx.Register((void *)this, CAL_INSERT, CString("CALTABLE"),CString("CALTAB INSERT"), CedaCalDataCf);
	ogDdx.Register((void *)this, CAL_UPDATE, CString("CALTABLE"),CString("CALTAB UPDATE"), CedaCalDataCf);
	ogDdx.Register((void *)this, SEC_DELETE, CString("CALTABLE"),CString("SECTAB DELETE"), CedaCalDataCf);
	ogDdx.Register((void *)this, GRP_DELETE, CString("CALTABLE"),CString("GRPTAB DELETE"), CedaCalDataCf);
	ogDdx.Register((void *)this, SEC_DELAPP, CString("CALTABLE"),CString("SECTAB DELAPP"), CedaCalDataCf);
	ogDdx.Register((void *)this, CAL_DELETE, CString("CALTABLE"),CString("SECTAB DELAPP"), CedaCalDataCf);

	if (pogInitialLoad != NULL)
	{
		char pclTmp[200];
		sprintf(pclTmp,GetString(IDS_LOADING_TABLE),pcmTableName);
		pogInitialLoad->SetMessage(pclTmp);
	}

    // Select data from the database
	char pclWhere[512];
	bool blRc = true;
	char pclFields[512];
	strcpy(pclFields,pcmFieldList);
	char pclSort[512] = "";
	char pclData[1024] = "";
	char pclTable[20];
	memset(pclTable,0,20);
	strncpy(pclTable,pcmTableName,6);


	sprintf(pclWhere,"ORDER BY FSEC");
	char pclCom[50] = "RT";
//	blRc = CedaAction(pclCom, pclWhere);
	blRc = CedaAction(pclCom,pclTable,pclFields,pclWhere,pclSort,pclData);
	if (! blRc)
	{
		ogLog.Trace("LOADCAL","Read: Ceda-Error %d \n",blRc);

		// it's not an error if no data were found
		if(omLastErrorMessage.Find(ORA_NOT_FOUND) != -1 || omLastErrorMessage == "No Data Found")
			blRc = true;
	}
	else
	{
		// Load data from CedaData into the dynamic array of records
		for (int ilLc = 0; blRc; ilLc++)
		{
			CALDATA *prpCal = new CALDATA;
			if ((blRc = GetBufferRecord(ilLc,prpCal)) == true)
				Add(prpCal);
			else
				delete prpCal;
		}

		int ilSize = omData.GetSize();
		ogLog.Trace("LOADSEC"," %d users loaded from %s",ilSize,pcmTableName);
		if (pogInitialLoad != NULL)
		{
			char pclBuf[128];
			pogInitialLoad->SetProgress(20);
			sprintf(pclBuf,GetString(IDS_NUM_DB_RECS_READ),ilSize,pcmTableName);
			pogInitialLoad->SetMessage(CString(pclBuf));
			pogInitialLoad->UpdateWindow();
		}
		blRc = true;
	}

	return blRc;


} // Read()


// ----------------------------------------------------------------------------------------------
// NewCal()
//
// Insert records into CALTAB
//
// ropCalRecs		- CCSPtrArray of CALDATA records
// pcpErrorMessage	- on RCFail this buffer will contain an error message else blank
//
bool CedaCalData::NewCal( CCSPtrArray <CALDATA> &ropCalRecs, char *pcpErrorMessage)
{
	bool blRc = true;
	CString olListOfData;
	char pclData[BIGBUF];
	char pclFields[BIGBUF];
	char pclWhere[50];


	strcpy(pcpErrorMessage,""); // no errors yet


	int ilNumRecs = ropCalRecs.GetSize();
	memset(pclData,0,sizeof(pclData));
	bool blFirstRec = true;
	for(int ilCal = 0; ilCal < ilNumRecs; ilCal++ )
	{
		if( blFirstRec )
		{
			sprintf(pclData,"%s,%s,%s,%s",ropCalRecs[ilCal].FSEC,ropCalRecs[ilCal].VAFR.Format("%Y%m%d%H%M%S"),ropCalRecs[ilCal].VATO.Format("%Y%m%d%H%M%S"),ropCalRecs[ilCal].FREQ);
			blFirstRec = false;
		}
		else
			sprintf(pclData,"%s,%s,%s,%s,%s",pclData,ropCalRecs[ilCal].FSEC,ropCalRecs[ilCal].VAFR.Format("%Y%m%d%H%M%S"),ropCalRecs[ilCal].VATO.Format("%Y%m%d%H%M%S"),ropCalRecs[ilCal].FREQ);
	}

	strcpy(pclFields,"FSEC,VAFR,VATO,FREQ");
	strcpy(pclWhere,"WHERE NEWCAL");

	// Action,TableName,FieldList,Selection,Sort,Data,Dest (eg "RETURN")
	char pclSort[50] = "";
	blRc = CedaAction(pcgSecCommand,pcgTableExt,pclFields,pclWhere,pclSort,pclData);


	if( ! blRc )
	{
		if( omLastErrorMessage != CString(pclData) )
			strcpy(pcpErrorMessage,omLastErrorMessage);
		else
			sprintf(pcpErrorMessage,"Ceda error <%d> !",blRc);
	}
	else
	{
		// read the results from the data field
		RETDATA rlRetRec;

		for (int ilLc = 0; blRc; ilLc++)
		{
			
			if ((blRc = GetBufferRecord(&omRetRecInfo,ilLc,&rlRetRec)) == true)
			{
				// omRetRecInfo contains return values from CRU
				// these values can be:
				//
				// TYPE,FLD1,FLD2,FLD3,FLD4,FLD5,FLD6,FLD7,FLD8
				// --------------------------------------------
				//  CAL,URNO,FSEC,VAFR,VATO,FREQ					--> from CALTAB (create application (SECTAB))

				// AfxMessageBox(CString(rlRetRec.TYPE)+CString(",")+CString(rlRetRec.FLD1)+CString(",")+CString(rlRetRec.FLD2)+CString(",")+CString(rlRetRec.FLD3)+CString(",")+CString(rlRetRec.FLD4)+CString(",")+CString(rlRetRec.FLD5),0);

				if( ! strcmp( rlRetRec.TYPE , "CAL" ) )
				{
					// CAL record received
					CALDATA *prlCalRec = new CALDATA;
					strcpy(prlCalRec->URNO,rlRetRec.FLD1);
					strcpy(prlCalRec->FSEC,rlRetRec.FLD2);
					StoreDate(rlRetRec.FLD3,&prlCalRec->VAFR); // converts a value to CTime
					StoreDate(rlRetRec.FLD4,&prlCalRec->VATO); // converts a value to CTime
					strcpy(prlCalRec->FREQ,rlRetRec.FLD5);
					Add(prlCalRec);

					// update the main screen if this is a CAL that was inserted for SECTAB
					if( ogCedaSecData.GetSecByUrno(prlCalRec->FSEC) != NULL )
						ogDdx.DataChanged((void *)this, SEC_VIEWER_UPDATE,(void *)prlCalRec->FSEC );
				
				}
			}
		}

		blRc = true;
	}

	return blRc;

} // end UpdSec()


// ----------------------------------------------------------------------------------------------
// UpdCal()
//
// Update records in CALTAB
//
// ropCalRecs		- CCSPtrArray of CALDATA records
// pcpErrorMessage	- on RCFail this buffer will contain an error message else blank
//
bool CedaCalData::UpdCal( CCSPtrArray <CALDATA> &ropCalRecs, char *pcpErrorMessage)
{
	bool blRc = true;
	CString olListOfData;
	char pclData[BIGBUF];
	char pclFields[BIGBUF];
	char pclWhere[50];


	strcpy(pcpErrorMessage,""); // no errors yet


	int ilNumRecs = ropCalRecs.GetSize();
	memset(pclData,0,sizeof(pclData));
	bool blFirstRec = true;
	int ilCal;
	for(ilCal = 0; ilCal < ilNumRecs; ilCal++ )
	{
		if( blFirstRec )
		{
			sprintf(pclData,"%s,%s,%s,%s",ropCalRecs[ilCal].URNO,ropCalRecs[ilCal].VAFR.Format("%Y%m%d%H%M%S"),ropCalRecs[ilCal].VATO.Format("%Y%m%d%H%M%S"),ropCalRecs[ilCal].FREQ);
			blFirstRec = false;
		}
		else
			sprintf(pclData,"%s,%s,%s,%s,%s",pclData,ropCalRecs[ilCal].URNO,ropCalRecs[ilCal].VAFR.Format("%Y%m%d%H%M%S"),ropCalRecs[ilCal].VATO.Format("%Y%m%d%H%M%S"),ropCalRecs[ilCal].FREQ);
	}
	strcpy(pclFields,"URNO,VAFR,VATO,FREQ");
	strcpy(pclWhere,"WHERE UPDCAL");

	// Action,TableName,FieldList,Selection,Sort,Data,Dest (eg "RETURN")
	char pclSort[50] = "";
	blRc = CedaAction(pcgSecCommand,pcgTableExt,pclFields,pclWhere,pclSort,pclData);


	if( ! blRc )
	{
		if( omLastErrorMessage != CString(pclData) )
			strcpy(pcpErrorMessage,omLastErrorMessage);
		else
			sprintf(pcpErrorMessage,"Ceda error <%d> !",blRc);
	}
	else
	{
			// update local data
		for(ilCal = 0; ilCal < ilNumRecs; ilCal++ )
		{
			Update((CALDATA*) &ropCalRecs[ilCal]);

			// update the main screen
			if( ogCedaSecData.GetSecByUrno(ropCalRecs[ilCal].FSEC) != NULL )
				ogDdx.DataChanged((void *)this, SEC_VIEWER_UPDATE,(void *)ropCalRecs[ilCal].FSEC );
		}

		blRc = true;
	}

	return blRc;

} // end UpdCal()



// ----------------------------------------------------------------------------------------------
// DelCal()
//
// Delete records from CALTAB
//
// ropCalRecs		- CCSPtrArray of CALDATA records
// pcpErrorMessage	- on RCFail this buffer will contain an error message else blank
//
bool CedaCalData::DelCal( CCSPtrArray <CALDATA> &ropCalRecs, char *pcpErrorMessage)
{
	bool blRc = true;
	CString olListOfData;
	char pclData[BIGBUF];
	char pclFields[BIGBUF];
	char pclWhere[50];


	strcpy(pcpErrorMessage,""); // no errors yet


	int ilNumRecs = ropCalRecs.GetSize();
	memset(pclData,0,sizeof(pclData));
	bool blFirstRec = true;
	int ilCal;
	for(ilCal = 0; ilCal < ilNumRecs; ilCal++ )
	{
		if( blFirstRec )
		{
			sprintf(pclData,"%s",ropCalRecs[ilCal].URNO);
			blFirstRec = false;
		}
		else
			sprintf(pclData,"%s,%s",pclData,ropCalRecs[ilCal].URNO);
	}
	strcpy(pclFields,"URNO");
	strcpy(pclWhere,"WHERE DELCAL");

	// Action,TableName,FieldList,Selection,Sort,Data,Dest (eg "RETURN")
	char pclSort[50] = "";
	blRc = CedaAction(pcgSecCommand,pcgTableExt,pclFields,pclWhere,pclSort,pclData);


	if( ! blRc )
	{
		if( omLastErrorMessage != CString(pclData) )
			strcpy(pcpErrorMessage,omLastErrorMessage);
		else
			sprintf(pcpErrorMessage,"Ceda error <%d> !",blRc);
	}
	else
	{
			// update local data
		for(ilCal = 0; ilCal < ilNumRecs; ilCal++ )
		{
			CString olFsec(ropCalRecs[ilCal].FSEC);
			DeleteByUrno(ropCalRecs[ilCal].URNO);

			// update the main screen
			if( ogCedaSecData.GetSecByUrno(olFsec) != NULL )
				ogDdx.DataChanged((void *)this, SEC_VIEWER_UPDATE,(void *) (const char*) olFsec );
		}

		blRc = true;
	}

	return blRc;

} // end DelCal()




// ----------------------------------------------------------------------------------------------
// Add()

bool CedaCalData::Add(CALDATA *prpCal)
{

	omData.Add(prpCal);
	omUrnoMap.SetAt(prpCal->URNO,prpCal);
	omFsecMap.SetAt(prpCal->FSEC,prpCal);

    return true;

} // end Add()


static void CedaCalDataCf(void *popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
    CedaCalData *polThis = (CedaCalData *)popInstance;

	switch(ipDDXType) {

	case CAL_INSERT:
		polThis->Insert((CALDATA *)vpDataPointer);
		break;
	case CAL_UPDATE:
        polThis->Update((CALDATA *)vpDataPointer);
		break;
	case CAL_DELETE:
		// delete a CAL rec
        polThis->DeleteByFsec((char *)vpDataPointer);
		break;
	case SEC_DELETE:
		// SEC record deleted
        polThis->DeleteByFsec((char *)vpDataPointer);
		break;
	case SEC_DELAPP:
		// SEC application record deleted
        polThis->DeleteByFsec((char *)vpDataPointer);
		break;
	default:
		break;
	}
}

//
// Insert()
//
// Add a record to omData
//
// prpCal - CALDATA record to add
//
bool CedaCalData::Insert(CALDATA *prpCal)
{

	Add(prpCal);

    return true;

} // end Insert()


//
// Update()
//
// Update a record in omData
//
// prpCal - CALDATA record to update
//
bool CedaCalData::Update(CALDATA *prpCal)
{
	CALDATA *prlCal = GetCalByUrno(prpCal->URNO);

	if( prlCal != NULL )
		*prlCal = *prpCal;

    return prlCal != NULL ? true : false;

} // end Update()


//
// DeleteByUrno()
//
// delete a record from local data using pcpUrno as the key
//
bool CedaCalData::DeleteByUrno( const char  *pcpUrno )
{
	bool blNotFound = true;
	int ilCount = omData.GetSize();

	for(int ilLineNo = 0; blNotFound && ilLineNo < ilCount; ilLineNo++)
	{
		if(strcmp(omData[ilLineNo].URNO, pcpUrno) == 0)
		{
			omUrnoMap.RemoveKey(pcpUrno);
			omFsecMap.RemoveKey(omData[ilLineNo].FSEC);
			omData.DeleteAt(ilLineNo);
			blNotFound = false;
		}
	}

	return !blNotFound;
}


//
// DeleteByFsec()
//
// delete a record from local data using pcpFsec as the key
//
bool CedaCalData::DeleteByFsec( const char  *pcpFsec )
{
	bool blNotFound = true;
	int ilCount = omData.GetSize();

	for(int ilLineNo = 0; blNotFound && ilLineNo < ilCount; ilLineNo++)
	{
		if(strcmp(omData[ilLineNo].FSEC, pcpFsec) == 0)
		{
			omUrnoMap.RemoveKey(omData[ilLineNo].URNO);
			omFsecMap.RemoveKey(pcpFsec);
			omData.DeleteAt(ilLineNo);
			blNotFound = false;
		}
	}

	return !blNotFound;
}


// Given the FSEC URNO return a pointer to a CAL rec.
CALDATA* CedaCalData::GetCalByFsec(char *pcpFsec)
{
	CALDATA *prlCalRec = NULL;
	
	omFsecMap.Lookup(pcpFsec,(void *&) prlCalRec);

	return prlCalRec;

}


// Given the FSEC URNO return a the date strings in the following format:
// "Date" --> means one date only and is currently valid
// "Date *" --> many dates, the displayed one is the first found that is currently valid
// "*" --> one or many dates, none of which are currently valid
int CedaCalData::GetDatesByFsec(const char *pcpFsec, CString &ropVafr, CString &ropVato, CString &ropFreq)
{
	int ilCount = omData.GetSize();
	CTime olCurrTime = CTime::GetCurrentTime();

	
	ropVafr.Empty(); ropVato.Empty(); ropFreq.Empty();
	bool blDatesEmpty = true;

	int ilNumFound = 0;

	for(int ilLineNo = 0; ilLineNo < ilCount; ilLineNo++)
	{
		if(!strcmp(omData[ilLineNo].FSEC, pcpFsec))
		{
			ilNumFound++;
			if(blDatesEmpty && omData[ilLineNo].VAFR <= olCurrTime && omData[ilLineNo].VATO >= olCurrTime)
			{
				ropVafr = omData[ilLineNo].VAFR.Format("%d.%m.%Y %H:%M");
				ropVato = omData[ilLineNo].VATO.Format("%d.%m.%Y %H:%M");
				ropFreq = omData[ilLineNo].FREQ;
				blDatesEmpty = false;
			}
		}
	}

	CString olMany((blDatesEmpty) ? "*" : " *");
	if( ilNumFound > 1 || blDatesEmpty )
	{
		ropVafr += olMany;
		ropVato += olMany;
		ropFreq += olMany;
	}			

	return ilNumFound;
}

bool CedaCalData::DateIsValidForFsec(const char *pcpFsec)
{
	bool blIsValid = false;
	CTime olCurrTime = CTime::GetCurrentTime();

	int ilCount = omData.GetSize();
	for(int ilLineNo = 0; !blIsValid && ilLineNo < ilCount; ilLineNo++)
	{
		if(!strcmp(omData[ilLineNo].FSEC, pcpFsec))
		{
			if(omData[ilLineNo].VAFR <= olCurrTime && omData[ilLineNo].VATO >= olCurrTime)
			{
				blIsValid = true;
			}
		}
	}

	return blIsValid;
}


// Given the URNO return a pointer to a CAL rec.
CALDATA* CedaCalData::GetCalByUrno(char *pcpUrno)
{
	CALDATA *prlCalRec = NULL;
	
	omUrnoMap.Lookup(pcpUrno,(void *&) prlCalRec);

	return prlCalRec;

}

