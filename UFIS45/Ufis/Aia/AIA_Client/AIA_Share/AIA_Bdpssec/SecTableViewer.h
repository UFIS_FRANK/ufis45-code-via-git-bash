// FlViewer.h
//
#ifndef __SECTABLEVIEWER_H__
#define __SECTABLEVIEWER_H__

#include "CCSGlobl.h"
#include "CCSTable.h"
#include "CedaSecData.h"
#include "CCSPrint.h"


struct SECTAB_LINEDATA
{
	char	URNO[URNOLEN];
	CString USID;
	CString NAME;
	CString STAT;
	CString	VAFR;
	CString	VATO;
	CString	FREQ;
	CString REMA;
	CString PROF;
	bool IsSysAdmin;

	SECTAB_LINEDATA(void)
	{
		memset(URNO,0,URNOLEN);
		IsSysAdmin = false;
	}
};

#define USIDCOLUMNWIDTH 100
#define FULLNAMECOLUMNWIDTH 260
#define STATCOLUMNWIDTH 75
#define VAFRCOLUMNWIDTH 145
#define VATOCOLUMNWIDTH 145
#define FREQCOLUMNWIDTH 75
#define REMACOLUMNWIDTH 500
#define PROFCOLUMNWIDTH 260

class SecTableViewer
{
// Constructions
public:
    SecTableViewer();
    ~SecTableViewer();

    void Attach(CCSTable *popAttachWnd);
    void ChangeViewTo(void);


// Internal data processing routines
public:

    void MakeLines();
	void MakeLine(SECDATA *prpSec);
	int CreateLine(SECTAB_LINEDATA *);
	void DeleteAll();
	void DeleteLine(int);

	BOOL IsPassFilter(SECDATA  *prpSec);
	int CompareLine(SECTAB_LINEDATA *,SECTAB_LINEDATA *);
	void GetCalValues(char *pcpURNO,CString &ropVAFR,CString &ropVATO,CString &ropFREQ);
	int	 GetSize();
	int FindLine(const char *pcpUrno);
	void SelectLine(const char *pcpUrno);

	void Update(char *pcpUrno);
	void Insert(char *pcpUrno);
	void Delete(char *pcpUrno);

	void UpdateDisplay();

	void Print(CString opCaption, CWnd *popParent);
	bool PrintLine(SECTAB_LINEDATA *prpLine, int ipOrientation);
	CCSPrint *pomPrint;

// Attributes
public:
    CCSTable *pomTable;
    CCSPtrArray <SECTAB_LINEDATA> omLines;


private:
	// strings containing text displayed on the screen
	CString omEnabledMess; 
	CString omDisabledMess;
	CString omUnknownMess;
	bool bmDisplayProfile;

};

#endif //__SECTABLEVIEWER_H__
