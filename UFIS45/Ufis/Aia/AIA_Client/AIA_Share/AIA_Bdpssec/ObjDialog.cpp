// ObjDialog.cpp : implementation file
//

#include "stdafx.h"
#include "CCSGlobl.h"
#include "BDPS_SEC.h"

#include "CedaPrvData.h"
#include "MessList.h"
#include "ObjDialog.h"
#include "CcsPrint.h"
#include "ObjList.h"
#include "PrintRightsDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

// Local function prototype
static void ObjDialogCf(void *popInstance, int ipDDXType,
					   void *vpDataPointer, CString &ropInstanceName);

static int ByApplSubdFual(const ProfileListObject **ppp1, const ProfileListObject **ppp2);
static int ByApplSubdFual(const ProfileListObject **ppp1, const ProfileListObject **ppp2)
{
	int ilRc = strcmp((**ppp1).Sec->USID,(**ppp2).Sec->USID);
	if(ilRc == 0)
		ilRc = strcmp((**ppp1).Fkt->SUBD,(**ppp2).Fkt->SUBD);
	if(ilRc == 0)
		ilRc = strcmp((**ppp1).Fkt->FUAL,(**ppp2).Fkt->FUAL);
	return ilRc;
}


/////////////////////////////////////////////////////////////////////////////
// ObjDialog dialog


ObjDialog::ObjDialog(CWnd* pParent /*=NULL*/)
	: CDialog(ObjDialog::IDD, pParent)
{

	//{{AFX_DATA_INIT(ObjDialog)
	//}}AFX_DATA_INIT

	pomProfileListItem = NULL;

	pomApplTable = new CCSTable; // application list
	pomApplTable->SetStyle(WS_BORDER);
	pomApplTable->SetSelectMode(0);

	pomSubdTable = new CCSTable; // subdivision list
	pomSubdTable->SetStyle(WS_BORDER);
	pomSubdTable->SetSelectMode(0);

	pomFuncTable = new CCSTable; // function list
	pomFuncTable->SetStyle(WS_BORDER);
	pomFuncTable->SetSelectMode(LBS_MULTIPLESEL|LBS_EXTENDEDSEL);

	ogDdx.Register((void *) this, OBJ_DLG_CHANGE, CString("OBJDLG"),CString("OBJDLG CHANGE"), ObjDialogCf);
}

ObjDialog::~ObjDialog()
{
	delete pomApplTable;
	delete pomSubdTable;
	delete pomFuncTable;
}


void ObjDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(ObjDialog)
	DDX_Control(pDX, IDC_PROFILESUSED, m_ProfilesUsed);
	DDX_Control(pDX, IDC_PROFILEINFO, m_ProfileInfo);
	DDX_Control(pDX, IDC_PROFILELIST, m_ProfileList);
	DDX_Control(pDX, IDOK, m_OK);
	DDX_Control(pDX, IDCANCEL, m_Cancel);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(ObjDialog, CDialog)
	//{{AFX_MSG_MAP(ObjDialog)
	ON_MESSAGE(WM_TABLE_LBUTTONDOWN,OnTableLButtonDown)
	ON_MESSAGE(WM_TABLE_RBUTTONDOWN,OnTableRButtonDown)
	ON_MESSAGE(WM_TABLE_SELCHANGE,OnTableSelChange)
	ON_MESSAGE(WM_TABLE_LBUTTONDBLCLK,OnTableLButtonDblClk)
	ON_COMMAND(WM_INSTALL_MODULE,OnInstallModule)
	ON_COMMAND(WM_DEINSTALL_MODULE,OnDeinstallModule)
	ON_COMMAND(WM_REINSTALL_MODULE,OnReinstallModule)
	ON_COMMAND(WM_ENABLE_SELECTION,OnEnableSelection)
	ON_COMMAND(WM_DISABLE_SELECTION,OnDisableSelection)
	ON_COMMAND(WM_HIDE_SELECTION,OnHideSelection)
	ON_COMMAND(WM_ENABLE_ALL,OnEnableAll)
	ON_COMMAND(WM_DISABLE_ALL,OnDisableAll)
	ON_COMMAND(WM_HIDE_ALL,OnHideAll)
	ON_CBN_SELCHANGE(IDC_PROFILELIST, OnSelchangeProfilelist)
	ON_BN_CLICKED(ID_PRINTRIGHTS, OnPrintrights)
	//}}AFX_MSG_MAP
//	ON_WM_HELPINFO()
//	ON_WM_CONTEXTMENU()
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// ObjDialog message handlers


BOOL ObjDialog::OnInitDialog() 
{
	CDialog::OnInitDialog();

	// get the SEC rec for the selected line	
	SECDATA *prlSec = ogCedaSecData.GetSecByUrno(pcmFsec);


	// set titles, caption and USID field
	switch( rgCurrType )
	{
		case SEC_PROFILE:
			SetWindowText(GetString(IDS_OBJDIALOG_PROFILECAPTION));
			break;
		case SEC_GROUP:
			SetWindowText(GetString(IDS_OBJDIALOG_GROUPCAPTION));
			break;
		case SEC_WKS:
			SetWindowText(GetString(IDS_OBJDIALOG_WKSCAPTION));
			break;
		case SEC_WKSGROUP:
			SetWindowText(GetString(IDS_OBJDIALOG_WKSGROUPCAPTION));
			break;
		case SEC_APPL:
			SetWindowText(GetString(IDS_OBJDIALOG_MODULECAPTION));
			break;
		case SEC_USER:
		default:
			SetWindowText(GetString(IDS_OBJDIALOG_USERCAPTION));
			break;
	}

	omImageList.Create(16,16,ILC_COLOR,7,0);

	HICON rlIcon;
	omImageList.SetBkColor(GetSysColor(COLOR_WINDOW));
	rlIcon = ::LoadIcon(AfxGetResourceHandle(),MAKEINTRESOURCE(IDI_VALIDFINALPROFILE));
	omImageList.Add(rlIcon);
	rlIcon = ::LoadIcon(AfxGetResourceHandle(),MAKEINTRESOURCE(IDI_INVALIDFINALPROFILE));
	omImageList.Add(rlIcon);
	rlIcon = ::LoadIcon(AfxGetResourceHandle(),MAKEINTRESOURCE(IDI_VALIDGROUP));
	omImageList.Add(rlIcon);
	rlIcon = ::LoadIcon(AfxGetResourceHandle(),MAKEINTRESOURCE(IDI_INVALIDGROUP));
	omImageList.Add(rlIcon);
	rlIcon = ::LoadIcon(AfxGetResourceHandle(),MAKEINTRESOURCE(IDI_VALIDPROFILE));
	omImageList.Add(rlIcon);
	rlIcon = ::LoadIcon(AfxGetResourceHandle(),MAKEINTRESOURCE(IDI_INVALIDPROFILE));
	omImageList.Add(rlIcon);
	rlIcon = ::LoadIcon(AfxGetResourceHandle(),MAKEINTRESOURCE(IDI_ERRORRECURSION));
	omImageList.Add(rlIcon);
	m_ProfileList.SetImageList(&omImageList);

	omProfileListViewer.Attach(&m_ProfileList);

//	m_ProfileInfo.ModifyStyle(0,SS_ICON,0);

	// draw the 3 tables
	CRect rect;
	const int ilTop = 70, ilBottom = 590;

	// applications table
	rect.left = 20;
	rect.right = 179;
	rect.top = ilTop;
	rect.bottom = ilBottom;
	pomApplTable->SetTableData(this, rect.left, rect.right, rect.top, rect.bottom);
	omApplTableViewer.Attach(pomApplTable);

	// subdivisions table
	rect.left = 181;
	rect.right = 505;
	rect.top = ilTop;
	rect.bottom = ilBottom;
	pomSubdTable->SetTableData(this, rect.left, rect.right, rect.top, rect.bottom);
	omSubdTableViewer.Attach(pomSubdTable);

	// functions table
	rect.left = 507;
	rect.right = 1000;
	rect.top = ilTop;
	rect.bottom = ilBottom;
	pomFuncTable->SetTableData(this, rect.left, rect.right, rect.top, rect.bottom);
	omFuncTableViewer.Attach(pomFuncTable);


	// get and display the profile for the selected line
	CreateAndDisplayProfileList(0);


	return FALSE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE

  
}

// ipSelectProfile - zero based profile num to select
void ObjDialog::CreateAndDisplayProfileList(int ipSelectProfile)
{
	AfxGetApp()->DoWaitCursor(1);
	pomProfileListItem = NULL;
	if( rgCurrType == SEC_APPL )
	{
		bmPersonalProf = true;
		m_ProfileList.EnableWindow(FALSE);
		DisplayProfile();
	}
	else
	{
		m_ProfileList.EnableWindow(TRUE);
		m_ProfileList.ResetContent();
		omProfileListViewer.ChangeViewTo(pcmFsec);
		if(m_ProfileList.GetCount() >= ipSelectProfile)
			m_ProfileList.SetCurSel(ipSelectProfile);
		OnSelchangeProfilelist();
	}
	AfxGetApp()->DoWaitCursor(-1);
}

void ObjDialog::DisplayProfile(void)
{
	// the following flag is used in case a MessageBox was open when a ddx was received
	bmMaskChanged = true;

	if( !bmPersonalProf )
	{
		m_OK.EnableWindow(false);
	}
	else
	{
		m_OK.EnableWindow(true);
	}

	
	UpdateView();


} // end DisplayProfile()


void ObjDialog::UpdateView()
{
	AfxGetApp()->DoWaitCursor(1);

	strcpy(omApplTableViewer.pcmFsec,pcmFsec); // URNO of selected user or profile
	strcpy(omSubdTableViewer.pcmFsec,pcmFsec); // URNO of selected user or profile
	strcpy(omFuncTableViewer.pcmFsec,pcmFsec); // URNO of selected user or profile

	if(rgCurrType != SEC_APPL)
	{
		omApplTableViewer.ChangeViewTo(pomProfileListItem->FfktMap);
	}
	else
	{
		CMapStringToPtr olDummyFfktMap;
		omApplTableViewer.ChangeViewTo(olDummyFfktMap);
	}

	omApplTableViewer.SelectLine(imLine);
	
	if(omApplTableViewer.omLines.GetSize() > 0)
	{
		strcpy(omSubdTableViewer.pcmFapp,omApplTableViewer.omLines[imLine].URNO); // URNO of selected application
		strcpy(omFuncTableViewer.pcmFapp,omApplTableViewer.omLines[imLine].URNO); // URNO of selected application
	}
	else
	{
		strcpy(omSubdTableViewer.pcmFapp,"");
		strcpy(omFuncTableViewer.pcmFapp,"");
	}

	if(rgCurrType != SEC_APPL)
	{
		omSubdTableViewer.ChangeViewTo(pomProfileListItem->Objects);
	}
	else
	{
		omSubdTableViewer.ChangeViewTo();
	}
	
	strcpy(omFuncTableViewer.pcmSubd,omSubdTableViewer.GetSubdByLine(0)); // select SUBD

	if(rgCurrType != SEC_APPL)
	{
		omFuncTableViewer.ChangeViewTo(pomProfileListItem->Objects);
	}
	else
	{
		omFuncTableViewer.ChangeViewTo();
	}
	
	omFuncTableViewer.SelectLine(0);

	AfxGetApp()->DoWaitCursor(-1);

} // end UpdateView()


static void ObjDialogCf(void *popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
    ObjDialog *polThis = (ObjDialog *)popInstance;

	switch(ipDDXType) {

	case OBJ_DLG_CHANGE:
		// profile allocation has been changed for a user in the modeless zuweisen dialog (alloc dialog)
		// check if this user was updated if so redisplay the profile
		if( rgCurrType != SEC_APPL && rgCurrType != SEC_PROFILE && !strcmp(polThis->pcmFsec,(char *)vpDataPointer) )
			polThis->DisplayProfile();
		break;
	default:
		break;
	}
}

//
// OnTableRButtonDown()
//
// receives a WM_TABLE_RBUTTONDOWN message from CCSTable
// when the function SelectLine() is called
// if the right mouse button was pressed, displays context sensetive menus
//
// ipParam contains flags indicating which button was pressed
// lpParam contains x,y co-ord where the mouse button was clicked
//
LONG ObjDialog::OnTableRButtonDown(UINT ipParam, LONG lpParam)
{
	CCSTABLENOTIFY *polNotify = (CCSTABLENOTIFY *) lpParam;
	int ilLine = polNotify->Line;

	if( polNotify->SourceTable == pomApplTable )
	{
		if( !bmPersonalProf )
			MessageBox(GetString(IDS_OBJDIALOG_NOTPERSONALPROFILE),"",MB_ICONINFORMATION);
		else
		{
			if( rgCurrType != SEC_APPL && bmPersonalProf && omApplTableViewer.omLines.GetSize() > 0)
			{
				strcpy(omSubdTableViewer.pcmFapp,omApplTableViewer.omLines[ilLine].URNO);
				strcpy(omFuncTableViewer.pcmFapp,omApplTableViewer.omLines[ilLine].URNO);
				omApplTableViewer.SelectLine(ilLine);
				omSubdTableViewer.UpdateDisplay();
				strcpy(omFuncTableViewer.pcmSubd,omSubdTableViewer.GetSubdByLine(0));
				omFuncTableViewer.UpdateDisplay();
				omFuncTableViewer.SelectLine(0);


				if(omApplTableViewer.omLines[ilLine].isInstalled == IS_INSTALLED)
				{
					omFuncTableViewer.SelectLine(0);
					CMenu olMenu;
					olMenu.CreatePopupMenu();
					for (int i = olMenu.GetMenuItemCount(); --i >= 0;)
  						olMenu.RemoveMenu(i, MF_BYPOSITION);
  					olMenu.AppendMenu(MF_STRING|MF_GRAYED,WM_INSTALL_MODULE, GetString(IDS_OBJDIALOG_MENUINSTALL));
  					olMenu.AppendMenu(MF_STRING,WM_DEINSTALL_MODULE, GetString(IDS_OBJDIALOG_MENUDEINSTALL));
  					olMenu.AppendMenu(MF_STRING|MF_GRAYED,WM_REINSTALL_MODULE, GetString(IDS_OBJDIALOG_MENUREINSTALL));
					pomApplTable->ClientToScreen(&polNotify->Point);
					olMenu.TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON, polNotify->Point.x, polNotify->Point.y, this, NULL);
				}
				else if(omApplTableViewer.omLines[ilLine].isInstalled == ISNOT_INSTALLED)
				{
					omFuncTableViewer.SelectLine(0);
					CMenu olMenu;
					olMenu.CreatePopupMenu();
					for (int i = olMenu.GetMenuItemCount(); --i >= 0;)
  						olMenu.RemoveMenu(i, MF_BYPOSITION);
  					olMenu.AppendMenu(MF_STRING,WM_INSTALL_MODULE, GetString(IDS_OBJDIALOG_MENUINSTALL));
  					olMenu.AppendMenu(MF_STRING|MF_GRAYED,WM_DEINSTALL_MODULE, GetString(IDS_OBJDIALOG_MENUDEINSTALL));
  					olMenu.AppendMenu(MF_STRING|MF_GRAYED,WM_REINSTALL_MODULE, GetString(IDS_OBJDIALOG_MENUREINSTALL));
					pomApplTable->ClientToScreen(&polNotify->Point);
					olMenu.TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON, polNotify->Point.x, polNotify->Point.y, this, NULL);
				}
				else if(omApplTableViewer.omLines[ilLine].isInstalled == PARTIALLY_INSTALLED)
				{
					omFuncTableViewer.SelectLine(0);
					CMenu olMenu;
					olMenu.CreatePopupMenu();
					for (int i = olMenu.GetMenuItemCount(); --i >= 0;)
  						olMenu.RemoveMenu(i, MF_BYPOSITION);
  					olMenu.AppendMenu(MF_STRING|MF_GRAYED,WM_INSTALL_MODULE, GetString(IDS_OBJDIALOG_MENUINSTALL));
  					olMenu.AppendMenu(MF_STRING,WM_DEINSTALL_MODULE, GetString(IDS_OBJDIALOG_MENUDEINSTALL));
  					olMenu.AppendMenu(MF_STRING,WM_REINSTALL_MODULE, GetString(IDS_OBJDIALOG_MENUREINSTALL));
					pomApplTable->ClientToScreen(&polNotify->Point);
					olMenu.TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON, polNotify->Point.x, polNotify->Point.y, this, NULL);
				}
			}
		}
	}
	else if( polNotify->SourceTable == pomSubdTable )
	{
		if( !bmPersonalProf )
			MessageBox(GetString(IDS_OBJDIALOG_NOTPERSONALPROFILE),"",MB_ICONINFORMATION);
		else
		{
			omSubdTableViewer.SelectLine(ilLine);
			strcpy(omFuncTableViewer.pcmSubd,omSubdTableViewer.GetSubdByLine(ilLine));
			omFuncTableViewer.UpdateDisplay();
			omFuncTableViewer.SelectLine(0);

			CMenu olMenu;
			olMenu.CreatePopupMenu();
			for (int i = olMenu.GetMenuItemCount(); --i >= 0;)
  				olMenu.RemoveMenu(i, MF_BYPOSITION);
  			olMenu.AppendMenu(MF_STRING,WM_ENABLE_ALL, GetString(IDS_OBJDIALOG_MENUENABLE));
  			olMenu.AppendMenu(MF_STRING,WM_DISABLE_ALL, GetString(IDS_OBJDIALOG_MENUDISABLE));
  			olMenu.AppendMenu(MF_STRING,WM_HIDE_ALL, GetString(IDS_OBJDIALOG_MENUHIDE));
			pomSubdTable->ClientToScreen(&polNotify->Point);
			olMenu.TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON, polNotify->Point.x, polNotify->Point.y, this, NULL);
		}
	}
	else if( polNotify->SourceTable == pomFuncTable )
	{
		if( !bmPersonalProf )
			MessageBox(GetString(IDS_OBJDIALOG_NOTPERSONALPROFILE),"",MB_ICONINFORMATION);
		else
		{
			CMenu olMenu;
			olMenu.CreatePopupMenu();
			for (int i = olMenu.GetMenuItemCount(); --i >= 0;)
  				olMenu.RemoveMenu(i, MF_BYPOSITION);
  			olMenu.AppendMenu(MF_STRING,WM_ENABLE_SELECTION, GetString(IDS_OBJDIALOG_MENUENABLE));
  			olMenu.AppendMenu(MF_STRING,WM_DISABLE_SELECTION, GetString(IDS_OBJDIALOG_MENUDISABLE));
  			olMenu.AppendMenu(MF_STRING,WM_HIDE_SELECTION, GetString(IDS_OBJDIALOG_MENUHIDE));
			pomFuncTable->ClientToScreen(&polNotify->Point);
			olMenu.TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON, polNotify->Point.x, polNotify->Point.y, this, NULL);
		}
	}
	
	return 0L;
}

//
// OnTableLButtonDown()
//
// receives a WM_TABLE_LBUTTONDOWN message from CCSTable
// when the function SelectLine() is called
// if the left mouse button was pressed, calls OnTableSelChange()
//
// ipParam contains flags indicating which button was pressed
// lpParam contains x,y co-ord where the mouse button was clicked
//
LONG ObjDialog::OnTableLButtonDown(UINT ipParam, LONG lpParam)
{
	CCSTABLENOTIFY *polNotify = (CCSTABLENOTIFY *) lpParam;
	int ilLine = polNotify->Line;

	if( polNotify->SourceTable == pomApplTable )
	{
		if(omApplTableViewer.omLines.GetSize() > 0)
		{
			strcpy(omSubdTableViewer.pcmFapp,omApplTableViewer.omLines[ilLine].URNO);
			strcpy(omFuncTableViewer.pcmFapp,omApplTableViewer.omLines[ilLine].URNO);
		}
		else
		{
			strcpy(omSubdTableViewer.pcmFapp,"");
			strcpy(omFuncTableViewer.pcmFapp,"");
		}

		if( rgCurrType == SEC_APPL )
		{
//			if( omApplTableViewer.omLines.GetSize() > 0)
//				m_Title2.SetWindowText(omApplTableViewer.omLines[ilLine].USID);
//			else
//				m_Title2.SetWindowText("");
		}

		omSubdTableViewer.UpdateDisplay();

		strcpy(omFuncTableViewer.pcmSubd,omSubdTableViewer.GetSubdByLine(0));
		omFuncTableViewer.UpdateDisplay();
		omFuncTableViewer.SelectLine(0);

		if( bmPersonalProf && omApplTableViewer.omLines.GetSize() > 0)
		{
			if(omApplTableViewer.omLines[ilLine].isInstalled == IS_INSTALLED)
				omFuncTableViewer.SelectLine(0);
			else if(omApplTableViewer.omLines[ilLine].isInstalled == ISNOT_INSTALLED)
				InstallApplication(ilLine);
			else if(omApplTableViewer.omLines[ilLine].isInstalled == PARTIALLY_INSTALLED)
				ReinstallApplication(ilLine);
		}
	}
	else if( polNotify->SourceTable == pomSubdTable )
	{
		strcpy(omFuncTableViewer.pcmSubd,omSubdTableViewer.GetSubdByLine(ilLine));
		omFuncTableViewer.UpdateDisplay();
		omFuncTableViewer.SelectLine(0);
	}
	else {
	}

	return 0L;
}

//
// OnTableLButtonDblClk()
//
// receives a WM_TABLE_LBUTTONDBLCLK message from CCSTable
//
LONG ObjDialog::OnTableLButtonDblClk(UINT ipParam, LONG lpParam)
{
	CCSTABLENOTIFY *polNotify = (CCSTABLENOTIFY *) lpParam;

	// if this is the functions table and is a personal profile
	if( polNotify->SourceTable == pomFuncTable )
		if( bmPersonalProf )
			omFuncTableViewer.UpdateStatus(polNotify->Line);
		else
			MessageBox(GetString(IDS_OBJDIALOG_NOTPERSONALPROFILE),"",MB_ICONINFORMATION);

	return 0L;
}

//
// OnTableSelChange()
//
// receives a WM_TABLE_SELCHANGE message from CCSTable
//
// ipParam contains flags indicating which button was pressed
// lpParam contains x,y co-ord where the mouse button was clicked
//
LONG ObjDialog::OnTableSelChange(UINT ipParam, LONG lpParam)
{
//	AfxMessageBox("OnTableSelChange");

	return 0L;
}


void ObjDialog::OnInstallModule()
{
	char pclCedaErrorMessage[ERRMESSLEN];
	imLine = pomApplTable->GetCurrentLine();
	APPLTAB_LINEDATA *prlApplLine = (APPLTAB_LINEDATA *) pomApplTable->GetTextLineData(imLine);

	AfxGetApp()->DoWaitCursor(1); // hourGlass
	if(ogCedaPrvData.AddApp( omApplTableViewer.omLines[imLine].URNO, pcmFsec, pclCedaErrorMessage ) != true )
		MessageBox(GetString(IDS_DATABASE_ERROR),"",MB_ICONEXCLAMATION);
	else
	{
		int ilOldSel = m_ProfileList.GetCurSel();
		CreateAndDisplayProfileList(ilOldSel);
	}
	AfxGetApp()->DoWaitCursor(-1); // hourGlass
}

void ObjDialog::OnDeinstallModule()
{
	char pclCedaErrorMessage[ERRMESSLEN];
	imLine = pomApplTable->GetCurrentLine();
	APPLTAB_LINEDATA *prlApplLine = (APPLTAB_LINEDATA *) pomApplTable->GetTextLineData(imLine);

	AfxGetApp()->DoWaitCursor(1); // hourGlass
	if(ogCedaPrvData.RemApp( omApplTableViewer.omLines[imLine].URNO, pcmFsec, pclCedaErrorMessage ) != true )
		MessageBox(GetString(IDS_DATABASE_ERROR),"",MB_ICONEXCLAMATION);
	else
	{
		int ilOldSel = m_ProfileList.GetCurSel();
		CreateAndDisplayProfileList(ilOldSel);
	}
	AfxGetApp()->DoWaitCursor(-1); // hourGlass
}

void ObjDialog::OnReinstallModule()
{
	char pclCedaErrorMessage[ERRMESSLEN];
	imLine = pomApplTable->GetCurrentLine();
	APPLTAB_LINEDATA *prlApplLine = (APPLTAB_LINEDATA *) pomApplTable->GetTextLineData(imLine);

	AfxGetApp()->DoWaitCursor(1); // hourGlass
	if(ogCedaPrvData.UpdApp( omApplTableViewer.omLines[imLine].URNO, pcmFsec, pclCedaErrorMessage ) != true )
		MessageBox(GetString(IDS_DATABASE_ERROR),"",MB_ICONEXCLAMATION);
	else
	{
		int ilOldSel = m_ProfileList.GetCurSel();
		CreateAndDisplayProfileList(ilOldSel);
	}
	AfxGetApp()->DoWaitCursor(-1); // hourGlass
}

void ObjDialog::OnEnableSelection()
{
	int ilNumSelLines = pomFuncTable->pomListBox->GetSelCount();
	int *pipSelLines = new int[ilNumSelLines];

	pomFuncTable->pomListBox->GetSelItems(ilNumSelLines,pipSelLines);

	omFuncTableViewer.UpdateStatus(pipSelLines,ilNumSelLines,ENABLED_STAT);

	delete [] pipSelLines;
}

void ObjDialog::OnDisableSelection()
{
	int ilNumSelLines = pomFuncTable->pomListBox->GetSelCount();
	int *pipSelLines = new int[ilNumSelLines];

	pomFuncTable->pomListBox->GetSelItems(ilNumSelLines,pipSelLines);

	omFuncTableViewer.UpdateStatus(pipSelLines,ilNumSelLines,DISABLED_STAT);

	delete [] pipSelLines;
}

void ObjDialog::OnHideSelection()
{
	int ilNumSelLines = pomFuncTable->pomListBox->GetSelCount();
	int *pipSelLines = new int[ilNumSelLines];

	pomFuncTable->pomListBox->GetSelItems(ilNumSelLines,pipSelLines);

	omFuncTableViewer.UpdateStatus(pipSelLines,ilNumSelLines,HIDDEN_STAT);

	delete [] pipSelLines;
}

void ObjDialog::OnEnableAll()
{
	int ilNumLines = pomFuncTable->GetLinesCount();
	int *pipLines = new int[ilNumLines];

	for(int ilLc=0; ilLc<ilNumLines; ilLc++ )
		pipLines[ilLc] = ilLc;

	omFuncTableViewer.UpdateStatus(pipLines,ilNumLines,ENABLED_STAT);

	delete [] pipLines;
}

void ObjDialog::OnDisableAll()
{
	int ilNumLines = pomFuncTable->GetLinesCount();
	int *pipLines = new int[ilNumLines];

	for(int ilLc=0; ilLc<ilNumLines; ilLc++ )
		pipLines[ilLc] = ilLc;

	omFuncTableViewer.UpdateStatus(pipLines,ilNumLines,DISABLED_STAT);

	delete [] pipLines;
}

void ObjDialog::OnHideAll()
{
	int ilNumLines = pomFuncTable->GetLinesCount();
	int *pipLines = new int[ilNumLines];

	for(int ilLc=0; ilLc<ilNumLines; ilLc++ )
		pipLines[ilLc] = ilLc;

	omFuncTableViewer.UpdateStatus(pipLines,ilNumLines,HIDDEN_STAT);

	delete [] pipLines;
}



void ObjDialog::InstallApplication(const int ipLine)
{
	char pclCedaErrorMessage[ERRMESSLEN];

	bmMaskChanged = false;
	if( MessageBox(GetString(IDS_OBJDIALOG_INSTALLAPPL),GetString(IDS_OBJDIALOG_INSTALLAPPLTITLE),MB_YESNO|MB_ICONQUESTION) == IDYES )
	{
		if(!bmMaskChanged) // check that while the question was asked the mask wasn't updated by ddx
		{
			AfxGetApp()->DoWaitCursor(1); // hourGlass
			if(ogCedaPrvData.AddApp( omApplTableViewer.omLines[ipLine].URNO, pcmFsec, pclCedaErrorMessage ) != true )
				MessageBox(GetString(IDS_DATABASE_ERROR),"",MB_ICONEXCLAMATION);
			else
			{
				// get and display the profile for the selected line
				imLine = ipLine; // reselect the original module
				int ilOldSel = m_ProfileList.GetCurSel();
				CreateAndDisplayProfileList(ilOldSel);
			}
			AfxGetApp()->DoWaitCursor(-1); // hourGlass
		}
	}
}



void ObjDialog::ReinstallApplication(const int ipLine)
{
	char pclCedaErrorMessage[ERRMESSLEN];

	bmMaskChanged = false;
	if( MessageBox(GetString(IDS_OBJDIALOG_REINSTALLAPPL),GetString(IDS_OBJDIALOG_REINSTALLAPPLTITLE),MB_YESNO|MB_ICONQUESTION) == IDYES )
	{
		if(!bmMaskChanged) // check that while the question was asked the mask wasn't updated by ddx
		{
			AfxGetApp()->DoWaitCursor(1); // hourGlass
			if(ogCedaPrvData.UpdApp( omApplTableViewer.omLines[ipLine].URNO, pcmFsec, pclCedaErrorMessage ) != true )
				MessageBox(GetString(IDS_DATABASE_ERROR),"",MB_ICONEXCLAMATION);
			else
			{
				imLine = ipLine; // reselect the original module
				int ilOldSel = m_ProfileList.GetCurSel();
				CreateAndDisplayProfileList(ilOldSel);
			}
			AfxGetApp()->DoWaitCursor(-1); // hourGlass
		}
	}
}



void ObjDialog::OnOK() 
{
	// loop through the functions list creating a list of URNO,STAT fields to be updated
	int ilNumLines = omFuncTableViewer.omLines.GetSize();
	CString olComma(",");

	for(int ilLine=0; ilLine < ilNumLines; ilLine++ )
		if( strcmp(omFuncTableViewer.omLines[ilLine].newSTAT,omFuncTableViewer.omLines[ilLine].oldSTAT) )
			omStatList += CString(omFuncTableViewer.omLines[ilLine].URNO) + olComma + CString(omFuncTableViewer.omLines[ilLine].newSTAT) + olComma;

	
	CDialog::OnOK();
}

void ObjDialog::OnSelchangeProfilelist() 
{
	//m_ProfileList.SetEditSel(-1,-1);
	
	int ilSel = m_ProfileList.GetCurSel();
	pomProfileListItem = (ProfileListItem *) m_ProfileList.GetItemData(ilSel);
	if(pomProfileListItem != NULL)
	{
		CString olInfo;
		if(!pomProfileListItem->IsValid)
		{
			olInfo = pomProfileListItem->DESC;
		}
		void *prlDummy;
		if(omProfileListViewer.NoProfilesDefined() || (pomProfileListItem->FrelList.GetCount() == 1 && pomProfileListItem->FrelList.Lookup(pcmFsec,(void *)prlDummy)))
		{
			bmPersonalProf = true;
		}
		else
		{
			bmPersonalProf = false;
		}

		m_ProfileInfo.SetWindowText(olInfo);
//		omProfileListViewer.CreateProfile(pomProfileListItem->FrelList,omPrvList,omFfktMap);
		CString olFrel;
		void *pvlTmp;
		CString olProfileNames;
		for(POSITION rlPos = pomProfileListItem->FrelList.GetStartPosition(); rlPos != NULL; )
		{
			pomProfileListItem->FrelList.GetNextAssoc(rlPos,olFrel,(void *& )pvlTmp);
			SECDATA *prlSec = ogCedaSecData.GetSecByUrno(olFrel);
			if(prlSec != NULL)
			{
				if(olProfileNames.IsEmpty())
				{
					olProfileNames = GetString(IDS_PROFILESUSED);
				}
				else
				{
					olProfileNames += " + ";
				}
				olProfileNames += "'" + CString(prlSec->USID) + "'";
			}
		}
		m_ProfilesUsed.SetWindowText(olProfileNames);
	}
	DisplayProfile();
}



void ObjDialog::OnPrintrights() 
{
	CPrintRightsDlg olDlg;
	if(olDlg.DoModal() != IDCANCEL)
	{
		int ilSel = olDlg.m_Sel;

		AfxGetApp()->DoWaitCursor(1);

		if(rgCurrType != SEC_APPL)
		{
			PrintPrvRights(ilSel);
		}
		else
		{
			PrintFktRights(ilSel);
		}

		AfxGetApp()->DoWaitCursor(1);
	}
}

void ObjDialog::PrintPrvRights(int ipSel)
{
	CString olFooter1,olFooter2;
	CString olTitle1;
	int ilSel = m_ProfileList.GetCurSel();
	ProfileListItem *prlItem = (ProfileListItem *) m_ProfileList.GetItemData(ilSel);
	if(prlItem != NULL)
	{
		olTitle1 = prlItem->NAME;
	}

	CString olFapp = omSubdTableViewer.pcmFapp;
	CString olSubd = omFuncTableViewer.pcmSubd;
	CString olTitle2;
	if(ipSel != PRINT_ALL)
	{
		SECDATA *prlSec = ogCedaSecData.GetSecByUrno(olFapp);
		if(prlSec != NULL)
		{
			olTitle2 = prlSec->USID;
		}
		if(ipSel == PRINT_SUBD)
		{
			olTitle2 += CString(" (") + olSubd + CString(")");
		}
	}

	CCSPtrArray <ProfileListObject> olObjects;
	int ilNumObjs = prlItem->Objects.GetSize();
	for(int ilObj = 0; ilObj < ilNumObjs; ilObj++)
	{
		ProfileListObject *prlObj = &prlItem->Objects[ilObj];
		FKTDATA *prlFkt = prlItem->Objects[ilObj].Fkt;

		if(ipSel == PRINT_ALL ||
		  (ipSel == PRINT_APPL && olFapp == prlFkt->FAPP) || 
		  (ipSel == PRINT_SUBD && olSubd == prlFkt->SUBD))
		{
			olObjects.Add(prlObj);
		}
	}
	olObjects.Sort(ByApplSubdFual);

	int ilOrientation = PRINT_LANDSCAPE;//  PRINT_PORTRAET  PRINT_LANDSCAPE

	pomPrint = new CCSPrint((CWnd*)this,ilOrientation,45);

	if (pomPrint != NULL)
	{
		if (pomPrint->InitializePrinter(&ilOrientation) == TRUE)
		{
			int ilLinesPerPage = ilOrientation == PRINT_PORTRAET ? 57 : 38;

			pomPrint->imLineNo = ilLinesPerPage + 1;
			DOCINFO	rlDocInfo;
			memset(&rlDocInfo, 0, sizeof(DOCINFO));
			rlDocInfo.cbSize = sizeof( DOCINFO );
			rlDocInfo.lpszDocName = olTitle1;	
			pomPrint->omCdc.StartDoc( &rlDocInfo );
			pomPrint->imPageNo = 0;

			int ilLines = olObjects.GetSize();
			int ilTotalPages = (((int) ilLines-1) / (int) ilLinesPerPage) + 1;
			int ilPageCount = 1;

			pomPrint->imLineNo = 0;
			olFooter1 = CTime::GetCurrentTime().Format("%d.%m.%Y %H:%M");
			for(int ilCurrLine = 0; ilCurrLine < ilLines; ilCurrLine++ ) 
			{
				if(pomPrint->imLineNo >= ilLinesPerPage)
				{
					// end of page reached
					olFooter2.Format("%d/%d",ilPageCount++,ilTotalPages);
					pomPrint->PrintUIFFooter(olFooter1,"",olFooter2);
					pomPrint->omCdc.EndPage();
					pomPrint->imLineNo = 0;
				}

				if( pomPrint->imLineNo == 0 )
				{
					// print the header at the start and when the page has changed
					pomPrint->PrintUIFHeader(olTitle1,olTitle2,pomPrint->imFirstLine-10);
				}

				// print a line
				PrintLine(&olObjects[ilCurrLine], ilOrientation);
			}

			olFooter2.Format("%d/%d",ilPageCount++,ilTotalPages);
			pomPrint->PrintUIFFooter(olFooter1,"",olFooter2);
			pomPrint->omCdc.EndPage();
			pomPrint->omCdc.EndDoc();
		}

		delete pomPrint;
		pomPrint = NULL;
	}
}

// ipSel 0=All 1=WholeAppl 2=Subd
void ObjDialog::PrintFktRights(int ipSel)
{
	CString olFapp = omSubdTableViewer.pcmFapp;
	CString olSubd = omFuncTableViewer.pcmSubd;
	CString olTitle2;
	if(ipSel != PRINT_ALL)
	{
		SECDATA *prlSec = ogCedaSecData.GetSecByUrno(olFapp);
		if(prlSec != NULL)
		{
			olTitle2 = prlSec->USID;
		}
		if(ipSel == PRINT_SUBD)
		{
			olTitle2 += CString(" (") + olSubd + CString(")");
		}
	}

	CCSPtrArray <ProfileListObject> olObjects;
	int ilNumFkts = ogCedaFktData.omData.GetSize();
	for(int ilFkt = 0; ilFkt < ilNumFkts; ilFkt++)
	{
		FKTDATA *prlFkt = &ogCedaFktData.omData[ilFkt];
		if(ipSel == PRINT_ALL ||
		  (ipSel == PRINT_APPL && olFapp == prlFkt->FAPP) || 
		  (ipSel == PRINT_SUBD && olSubd == prlFkt->SUBD))
		{
			ProfileListObject *prlObj = new ProfileListObject;
			prlObj->Fkt = prlFkt;
			prlObj->Prv = NULL;
			prlObj->Sec = ogCedaSecData.GetSecByUrno(prlFkt->FAPP);
			if(prlObj->Sec != NULL)
				olObjects.Add(prlObj);
			else
				delete prlObj;
		}
	}
	olObjects.Sort(ByApplSubdFual);

	CString olFooter1,olFooter2;
	CString olTitle1;
	int ilOrientation = PRINT_LANDSCAPE;//  PRINT_PORTRAET  PRINT_LANDSCAPE

	pomPrint = new CCSPrint((CWnd*)this,ilOrientation,45);
	GetWindowText(olTitle1);
	if (pomPrint != NULL)
	{
		if (pomPrint->InitializePrinter(&ilOrientation) == TRUE)
		{
			int ilLinesPerPage = ilOrientation == PRINT_PORTRAET ? 57 : 38;

			pomPrint->imLineNo = ilLinesPerPage + 1;
			DOCINFO	rlDocInfo;
			memset(&rlDocInfo, 0, sizeof(DOCINFO));
			rlDocInfo.cbSize = sizeof( DOCINFO );
			rlDocInfo.lpszDocName = olTitle1;	
			pomPrint->omCdc.StartDoc( &rlDocInfo );
			pomPrint->imPageNo = 0;

			int ilLines = olObjects.GetSize();
//if(ilLines > 300) ilLines = 300;
			int ilTotalPages = (((int) ilLines-1) / (int) ilLinesPerPage) + 1;
			int ilPageCount = 1;

			pomPrint->imLineNo = 0;
			olFooter1 = CTime::GetCurrentTime().Format("%d.%m.%Y %H:%M");
			for(int ilCurrLine = 0; ilCurrLine < ilLines; ilCurrLine++ ) 
			{
				if(pomPrint->imLineNo >= ilLinesPerPage)
				{
					// end of page reached
					olFooter2.Format("%d/%d",ilPageCount++,ilTotalPages);
					pomPrint->PrintUIFFooter(olFooter1,"",olFooter2);
					pomPrint->omCdc.EndPage();
					pomPrint->imLineNo = 0;
				}

				if( pomPrint->imLineNo == 0 )
				{
					// print the header at the start and when the page has changed
					pomPrint->PrintUIFHeader(olTitle1,olTitle2,pomPrint->imFirstLine-10);
				}

				// print a line
				PrintLine(&olObjects[ilCurrLine], ilOrientation);
			}

			olFooter2.Format("%d/%d",ilPageCount++,ilTotalPages);
			pomPrint->PrintUIFFooter(olFooter1,"",olFooter2);
			pomPrint->omCdc.EndPage();
			pomPrint->omCdc.EndDoc();
		}

		delete pomPrint;
		pomPrint = NULL;
	}
	olObjects.DeleteAll();
}

bool ObjDialog::PrintLine(ProfileListObject *prpObj, int ipOrientation)
{
	bool blRc = false;

	// given "B" and "1" return "Button" and "Enabled"
	char pclTypeDesc[100], pclStatDesc[100];
	if(prpObj->Prv == NULL)
	{
		ogObjList.GetDesc(prpObj->Fkt->TYPE,prpObj->Fkt->STAT,pclTypeDesc,pclStatDesc);
	}
	else
	{
		ogObjList.GetDesc(prpObj->Fkt->TYPE,prpObj->Prv->STAT,pclTypeDesc,pclStatDesc);
	}

	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;
	int ilNumChars;

	rlElement.pFont      = &pomPrint->ogCourierNew_Regular_8;
	rlElement.Alignment  =  PRINT_LEFT;
	rlElement.FrameTop   =  PRINT_FRAMETHIN;
	rlElement.FrameBottom = PRINT_FRAMETHIN;
	rlElement.FrameLeft  =  PRINT_FRAMETHIN;
	rlElement.FrameRight =  PRINT_FRAMETHIN;

	ilNumChars = ipOrientation == PRINT_PORTRAET ? 20 : 30;
	rlElement.Length = (int)((ilNumChars*5)*dgCCSPrintFactor);
	rlElement.Text = CString(prpObj->Sec->USID);
	rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

	ilNumChars = ipOrientation == PRINT_PORTRAET ? 40 : 70;
	rlElement.Length = (int)((ilNumChars*5)*dgCCSPrintFactor);
	rlElement.Text = CString(prpObj->Fkt->SUBD);
	rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

	ilNumChars = 25;
	rlElement.Length = (int)((ilNumChars*5)*dgCCSPrintFactor);
	rlElement.Text = CString(pclTypeDesc);
	rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

	ilNumChars = ipOrientation == PRINT_PORTRAET ? 60 : 95;
	rlElement.Length = (int)((ilNumChars*5)*dgCCSPrintFactor);
	rlElement.Text = CString(prpObj->Fkt->FUAL);
	rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

	ilNumChars = 20;
	rlElement.Length = (int)((ilNumChars*5)*dgCCSPrintFactor);
	rlElement.Text = CString(pclStatDesc);
	rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();

	blRc = true;

	return blRc;
}
