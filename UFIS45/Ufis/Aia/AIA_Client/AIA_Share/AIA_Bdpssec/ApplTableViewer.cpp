// ApplTableViewer.cpp : implementation file
//

#include "stdafx.h"
#include "CCSGlobl.h"
#include "BDPS_SEC.h"
#include "ApplTableViewer.h"
#include "CedaSecData.h"
#include "CedaGrpData.h"
#include "CedaPrvData.h"
#include "CedaFktData.h"
#include "MessList.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif




//*************************************************************************************
//
// ApplTableViewer() - constructor
//
//*************************************************************************************

ApplTableViewer::ApplTableViewer()
{
 
}


//*************************************************************************************
//
// ApplTableViewer() - destructor
//
//*************************************************************************************

ApplTableViewer::~ApplTableViewer()
{
    DeleteAll();
}


//*************************************************************************************
//
// DeleteAll() - delete all lines (records) read from SECTAB
//
//*************************************************************************************

void ApplTableViewer::DeleteAll()
{

	int ilNumLines = omLines.GetSize();

	for(int ilLine=1; ilLine<=ilNumLines; ilLine++)
        DeleteLine(0);
}

//*************************************************************************************
//
// GetSize() - return the number of lines in omLines (and thus number of lines in CCSTable)
//
//*************************************************************************************

int ApplTableViewer::GetSize()
{
	return omLines.GetSize();
}

//*************************************************************************************
//
// DeleteLine() - delete a line (record copied from SECTAB) at ipLineno
//
//*************************************************************************************

void ApplTableViewer::DeleteLine(int ipLineno)
{
	
	omLines.DeleteAt(ipLineno);
}

//*************************************************************************************
//
// Attach() - receives a pointer to the CCSTable
//
//*************************************************************************************

void ApplTableViewer::Attach(CCSTable *popTable)
{
    pomTable = popTable;
}


//*************************************************************************************
//
// ChangeViewTo() - function responsible for updating the display
//
//*************************************************************************************

void ApplTableViewer::ChangeViewTo(CMapStringToPtr &ropFfktMap)
{
	// delete all lines defined
	DeleteAll();

	// get lines specified in piViewType from CedaSecData into local memory
	MakeLines(ropFfktMap);

	// draw the lines loaded in CCSTable
	UpdateDisplay();
}




//*************************************************************************************
//
// IsPassFilter() - SEC_APPL = 'A' - select applications only
//
//*************************************************************************************

BOOL ApplTableViewer::IsPassFilter(const char *pcpType)
{
	return strchr(pcpType,SEC_APPL) == NULL ? false : true;

}


//*************************************************************************************
//
// CompareLine() - 
//
//*************************************************************************************

int ApplTableViewer::CompareLine(APPLTAB_LINEDATA *prpLine1, APPLTAB_LINEDATA *prpLine2)
{
	return strcmp(prpLine1->USID,prpLine2->USID);
}


/////////////////////////////////////////////////////////////////////////////
// ApplTableViewer -- code specific to this class





//*************************************************************************************
//
// MakeLines() - loop through the data held in ogCedaSecData copying to local memory
//
//*************************************************************************************

void ApplTableViewer::MakeLines(CMapStringToPtr &ropFfktMap)
{
	int ilCount;
	
	ilCount = ogCedaSecData.omData.GetSize();
	for(int i = 0; i < ilCount; i++)
		MakeLine(&ogCedaSecData.omData[i],ropFfktMap);
}

//*************************************************************************************
//
// MakeLine() - copy and process a line of data, create its field and display it
//
//*************************************************************************************

void ApplTableViewer::MakeLine(SECDATA *prpSec, CMapStringToPtr &ropFfktMap)
{

	// filter out either profiles or users or modul
	if( !IsPassFilter(prpSec->TYPE) ) 
		return;

	
	APPLTAB_LINEDATA rlLine;

	strcpy(rlLine.URNO,prpSec->URNO);
	strcpy(rlLine.USID,prpSec->USID);

	rlLine.isInstalled = ISNOT_INSTALLED;

	// check if the user or profile has the current application installed
	if( rgCurrType != SEC_APPL )
	{
		CCSPtrArray <FKTDATA> olFktList;
		ogCedaFktData.GetFktListByFapp(prpSec->URNO,olFktList);
		bool blSame = true;
		bool blInstalled = false;
		int ilNumFkts = olFktList.GetSize();
		PRVDATA *prlPrv;
		for(int ilFkt = 0; blSame && ilFkt < ilNumFkts; ilFkt++)
		{
			FKTDATA *prlFkt = &olFktList[ilFkt];
			if(ropFfktMap.Lookup(prlFkt->URNO,(void *&)prlPrv))
			{
				blInstalled = true;
			}
			else
			{
				blSame = false;
			}
		}
		if(!blInstalled)
		{
			bool blNotFound = true;
			CString olUrno;
			PRVDATA *prlPrv;
			for(POSITION rlPos = ropFfktMap.GetStartPosition(); blNotFound && rlPos != NULL; )
			{
				ropFfktMap.GetNextAssoc(rlPos,olUrno,(void *& )prlPrv);
				if(!strcmp(prlPrv->FAPP,prpSec->URNO))
				{
					blNotFound = false;
				}
			}
			if(blNotFound)
			{
				rlLine.isInstalled = ISNOT_INSTALLED;
			}
			else
			{
				rlLine.isInstalled = PARTIALLY_INSTALLED;
			}
		}
		else if(!blSame)
		{
			rlLine.isInstalled = PARTIALLY_INSTALLED;
		}
		else
		{
			rlLine.isInstalled = IS_INSTALLED;
		}
//		GRPDATA *prlGrpRec = ogCedaGrpData.GetGrpByFsecType(pcmFsec,SEC_PROFILE);
//		if( prlGrpRec != NULL )
//			rlLine.isInstalled = ogCedaPrvData.ApplDefined(prlGrpRec->FREL,prpSec->URNO); // FSEC,FAPP
	}
	else
	{
		rlLine.isInstalled = IS_INSTALLED;
	}
	
	CreateLine(&rlLine);
}



//*************************************************************************************
//
// CreateLine() - add the line to omLines (array of pointers to each line)
//
//*************************************************************************************

int ApplTableViewer::CreateLine(APPLTAB_LINEDATA *prpNewLine)
{
    int ilLineCount = omLines.GetSize();

    for (int ilLineno = 0; ilLineno < ilLineCount; ilLineno++)
        if (CompareLine(prpNewLine, &omLines[ilLineno]) <= 0)
            break;  

	APPLTAB_LINEDATA  rlLine;
    rlLine = *prpNewLine;
	omLines.NewAt(ilLineno, rlLine);

	return ilLineCount;
}




/////////////////////////////////////////////////////////////////////////////
// ApplTableViewer - display drawing routines



//*************************************************************************************
//
// UpdateDisplay() - write local SECTAB data to the CCSTable
//
//*************************************************************************************

void ApplTableViewer::UpdateDisplay()
{
	// step 1 set table header
	pomTable->ResetContent();
	CCSPtrArray <TABLE_HEADER_COLUMN> omHeaderDataArray;

	pomTable->SetShowSelection(true);
	TABLE_HEADER_COLUMN rlHeader;
	rlHeader.Alignment = COLALIGN_LEFT;
	rlHeader.Font = &ogCourier_Bold_10;

	// USID header
	rlHeader.Length = 300; // ???
	rlHeader.Text = GetString(IDS_APPLTABLEVIEWER_MODULENAME);
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);


	pomTable->SetHeaderFields(omHeaderDataArray);
	omHeaderDataArray.RemoveAll();
	
//	pomTable->SetDefaultSeparator();
	
   // step 2 set linedata
	CCSPtrArray <TABLE_COLUMN> olColList;

	TABLE_COLUMN rlColumnData;
	rlColumnData.VerticalSeparator = SEPA_NONE;
	rlColumnData.SeparatorType = SEPA_NONE;
	rlColumnData.Font = &ogCourier_Regular_10;//&ogMSSansSerif_Bold_8;//&ogCourier_Regular_10;
	rlColumnData.Alignment = COLALIGN_LEFT;


	int ilNumLines = omLines.GetSize();
	for (int ilLc = 0; ilLc < ilNumLines; ilLc++)
	{
		APPLTAB_LINEDATA *prlLine = &omLines[ilLc];

		rlColumnData.Text = prlLine->USID;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		pomTable->AddTextLine(olColList, (void*)prlLine);

		// disable drag&drop for each line
		pomTable->SetTextLineDragEnable(ilLc,FALSE);

		olColList.DeleteAll();

	}

	pomTable->DisplayTable();

	
	for (int ilLine = 0; ilLine < ilNumLines; ilLine++)
	{
		APPLTAB_LINEDATA *prlLine = &omLines[ilLine];
		if(prlLine->isInstalled == IS_INSTALLED)
			pomTable->SetTextLineColor(ilLine,BLACK,WHITE);
		else if(prlLine->isInstalled == ISNOT_INSTALLED)
			pomTable->SetTextLineColor(ilLine,RED,WHITE);
		else if(prlLine->isInstalled == PARTIALLY_INSTALLED)
			pomTable->SetTextLineColor(ilLine,BLUE,WHITE);
	}

}

void ApplTableViewer::SelectLine(const int ipLine)
{
	if( ipLine < omLines.GetSize() )
		pomTable->SelectLine(ipLine);

}
