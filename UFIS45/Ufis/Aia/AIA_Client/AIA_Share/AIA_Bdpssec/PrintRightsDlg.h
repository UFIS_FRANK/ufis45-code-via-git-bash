#if !defined(AFX_PRINTRIGHTSDLG_H__B68AAB13_6744_11D3_9225_0000B4392C49__INCLUDED_)
#define AFX_PRINTRIGHTSDLG_H__B68AAB13_6744_11D3_9225_0000B4392C49__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// PrintRightsDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CPrintRightsDlg dialog

class CPrintRightsDlg : public CDialog
{
// Construction
public:
	CPrintRightsDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CPrintRightsDlg)
	enum { IDD = IDD_PRINTRIGHTSDLG };
	int		m_Sel;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CPrintRightsDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CPrintRightsDlg)
		// NOTE: the ClassWizard will add member functions here
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_PRINTRIGHTSDLG_H__B68AAB13_6744_11D3_9225_0000B4392C49__INCLUDED_)
