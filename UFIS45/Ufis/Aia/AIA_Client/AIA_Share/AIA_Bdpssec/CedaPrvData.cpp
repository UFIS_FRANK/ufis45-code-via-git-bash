//
// CedaPrvData.cpp - Read/update PRVTAB
//
//
#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#include "stdafx.h"
#include "CCSGlobl.h"
#include "CedaCalData.h"
#include "CedaGrpData.h"
#include "CedaFktData.h"
#include "CedaPrvData.h"
#include "MessList.h"

//
// CedaPrvData()
//
// constructor
//
CedaPrvData::CedaPrvData()
{
    // Create an array of CEDARECINFO for PRVDATA
    BEGIN_CEDARECINFO(PRVDATA, CedaPrvData)
        FIELD_CHAR_TRIM(URNO,"URNO")
        FIELD_CHAR_TRIM(FSEC,"FSEC")
        FIELD_CHAR_TRIM(FFKT,"FFKT")
        FIELD_CHAR_TRIM(FAPP,"FAPP")
        FIELD_CHAR_TRIM(STAT,"STAT")
    END_CEDARECINFO

    // Copy the record structure
    for (int i = 0; i < sizeof(CedaPrvData)/sizeof(CedaPrvData[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&CedaPrvData[i],sizeof(CEDARECINFO));
        omRecInfo.Add(prpCedaRecInfo);
	}

    // Initialize table name and field names
    sprintf(pcmTableName,"PRV%s",pcgTableExt);
    pcmFieldList = "URNO,FSEC,FFKT,FAPP,STAT";

} // end constructor()


//
// CedaPrvData()
//
// destructor
//
CedaPrvData::~CedaPrvData(void)
{
	omRecInfo.DeleteAll();
	ClearAll();

} // end destructor()


//
// ClearAll()
//
// Deletes all records held in omData
//
void CedaPrvData::ClearAll(void)
{
    omData.DeleteAll();
	omUrnoMap.RemoveAll();
	ClearFsecMap();

} // end ClearAll()


//
// Read()
//
// Read the database table PRVTAB for the username or profile name specified.
// Initially clear omData/omApplList and add records read to omData,
// Add a list of applications defined for this user/profile to omApplList.
//
// pcpUsid - username or profile name
//
bool CedaPrvData::Read(void)
{
    sprintf(pcmTableName,"PRV%s",pcgTableExt);

	
	ogDdx.Register((void *)this, PRV_INSERT, CString("PRVTABLE"),CString("PRVTAB INSERT"), CedaPrvDataCf);
	ogDdx.Register((void *)this, PRV_DELETE, CString("PRVTABLE"),CString("PRVTAB DELETE"), CedaPrvDataCf);
	ogDdx.Register((void *)this, SEC_DELAPP, CString("PRVTABLE"),CString("SECTAB DELAPP"), CedaPrvDataCf);
	ogDdx.Register((void *)this, SEC_DELETE, CString("PRVTABLE"),CString("SECTAB DELAPP"), CedaPrvDataCf);


	// the following is another CEDARECINFO structure allowing a different structure
	// to be read in GetBufferRecord() - this must be defined in a different function
	// to the other structure otherwise error !
    // Create an array of CEDARECINFO for RETDATA (return values from CRU)
    BEGIN_CEDARECINFO(RETDATA, CedaRetData)
        FIELD_CHAR_TRIM(TYPE,"TYPE")
        FIELD_CHAR_TRIM(FLD1,"FLD1")
        FIELD_CHAR_TRIM(FLD2,"FLD2")
        FIELD_CHAR_TRIM(FLD3,"FLD3")
        FIELD_CHAR_TRIM(FLD4,"FLD4")
        FIELD_CHAR_TRIM(FLD5,"FLD5")
        FIELD_CHAR_TRIM(FLD6,"FLD6")
        FIELD_CHAR_TRIM(FLD7,"FLD7")
        FIELD_CHAR_TRIM(FLD8,"FLD8")
    END_CEDARECINFO

    // Copy the record structure
    for (int i = 0; i < sizeof(CedaRetData)/sizeof(CedaRetData[0]); i++)
	{
		CEDARECINFO *prpCedaRetRecInfo = new CEDARECINFO;
		memcpy(prpCedaRetRecInfo,&CedaRetData[i],sizeof(CEDARECINFO));
        omRetRecInfo.Add(prpCedaRetRecInfo);
	}


	if (pogInitialLoad != NULL)
	{
		char pclTmp[200];
		sprintf(pclTmp,GetString(IDS_LOADING_TABLE),pcmTableName);
		pogInitialLoad->SetMessage(pclTmp);
	}

    // Select data from the database
	char pclWhere[512];
	bool blRc = true;

	sprintf(pclWhere,"ORDER BY FSEC,FAPP");
	char pclCom[50] = "RT";
	char pclFields[512];
	strcpy(pclFields,pcmFieldList);
	char pclSort[512] = "";
	char pclData[1024] = "";
	char pclTable[20];
	memset(pclTable,0,20);
	strncpy(pclTable,pcmTableName,6);
//	blRc = CedaAction("RT", pclWhere);
	blRc = CedaAction(pclCom,pclTable,pclFields,pclWhere,pclSort,pclData);
	if ( ! blRc )
	{
		ogLog.Trace("LOADPRV","Ceda-Error %s (%d) \n",omLastErrorMessage,blRc);

		// it's not an error if no data were found
		if(omLastErrorMessage.Find(ORA_NOT_FOUND) != -1 || omLastErrorMessage == "No Data Found")
			blRc = true;
	}
	else
	{
		// Load data from CedaData into the dynamic array of records
		for (int ilLc = 0; blRc; ilLc++)
		{

			PRVDATA *prpPrv = new PRVDATA;
			if ((blRc = GetBufferRecord(ilLc,prpPrv)) == true)
				Add(prpPrv);
			else
				delete prpPrv;
		}

		int ilSize = omData.GetSize();
		ogLog.Trace("LOADPRV"," %d group records loaded from %s",ilSize,pcmTableName);
		if (pogInitialLoad != NULL)
		{
			char pclBuf[128];
			pogInitialLoad->SetProgress(20);
			sprintf(pclBuf,GetString(IDS_NUM_DB_RECS_READ),ilSize,pcmTableName);
			pogInitialLoad->SetMessage(CString(pclBuf));
			pogInitialLoad->UpdateWindow();
		}

		blRc = true;
	}


	return blRc;

} // end Read()



// ----------------------------------------------------------------------------------------------
// SetPrv()
//
// Update STAT for records in PRVTAB
//
// pcpDataList		- one or many URNO,STAT
// pcpErrorMessage	- on RCFail this buffer will contain an error message else blank
//
bool CedaPrvData::SetPrv( const char *pcpDataField, char *pcpErrorMessage)
{
	bool blRc = true;
	CString olListOfData;
	char pclFields[BIGBUF];
	char pclWhere[50];
	int ilDataFieldLen = strlen(pcpDataField);

	char *pclData = new char[max(BIGBUF,ilDataFieldLen)];

	memset(pclData,0,ilDataFieldLen);
	strncpy(pclData,pcpDataField,ilDataFieldLen-1); // copy except final comma


	strcpy(pcpErrorMessage,""); // no errors yet


	strcpy(pclFields,"URNO,STAT");
	strcpy(pclWhere,"WHERE SETPRV");

	// Action,TableName,FieldList,Selection,Sort,Data,Dest (eg "RETURN")
	char pclSort[50] = "";
	blRc = CedaAction(pcgSecCommand,pcgTableExt,pclFields,pclWhere,pclSort,pclData);


	if( ! blRc )
	{
		if( omLastErrorMessage != CString(pclData) )
			strcpy(pcpErrorMessage,omLastErrorMessage);
		else
			sprintf(pcpErrorMessage,"Ceda error <%d> !",blRc);
	}
	else
	{
		// loop through the URNO/STAT list updating local PRV records
		char *pclCopy = new char[ilDataFieldLen+1];
		strcpy(pclCopy,pcpDataField);
		char *pclComma, *pclItem = pclCopy, pclUrno[URNOLEN], pclStat[STATLEN];
		
		while( (pclComma = strchr(pclItem,',')) != NULL )
		{
			memset(pclUrno,0,sizeof(pclUrno));
			memset(pclStat,0,sizeof(pclStat));

			strncpy(pclUrno,pclItem,strlen(pclItem)-strlen(pclComma));
			pclItem = pclComma + sizeof(char);

			pclComma = strchr(pclItem,',');
			strncpy(pclStat,pclItem,strlen(pclItem)-strlen(pclComma));
			pclItem = pclComma + sizeof(char);

			Update(pclUrno,pclStat);
		}


		delete [] pclCopy;
		blRc = true;
	}

	delete [] pclData;

	return blRc;

} // end SetPrv()

// ----------------------------------------------------------------------------------------------
// AddApp()
//
// Add an application to a profile
//
// pcpFapp			- application to add
// pcpFsec			- URNO of profile
// pcpErrorMessage	- on RCFail this buffer will contain an error message else blank
//
bool CedaPrvData::AddApp( char  *pcpFapp, char *pcpFsec, char *pcpErrorMessage)
{
	bool blRc = true;
	CString olListOfData;
	char pclData[BIGBUF];
	char pclFields[BIGBUF];
	char pclWhere[50];


	strcpy(pcpErrorMessage,""); // no errors yet


	sprintf(pclData,"%s,%s",pcpFapp,pcpFsec);
	strcpy(pclFields,"FAPP,FSEC");
	strcpy(pclWhere,"WHERE ADDAPP");

	// Action,TableName,FieldList,Selection,Sort,Data,Dest (eg "RETURN")
	char pclSort[50] = "";
	blRc = CedaAction(pcgSecCommand,pcgTableExt,pclFields,pclWhere,pclSort,pclData);


	if( ! blRc )
	{
		if( omLastErrorMessage != CString(pclData) )
			strcpy(pcpErrorMessage,omLastErrorMessage);
		else
			sprintf(pcpErrorMessage,"Ceda error <%d> !",blRc);
	}
	else
	{
		// read the results from the data field
		RETDATA rlRetRec;

		for (int ilLc = 0; blRc; ilLc++)
		{
			
			if ((blRc = GetBufferRecord(&omRetRecInfo,ilLc,&rlRetRec)) == true)
			{
				// omRetRecInfo contains return values, these values can be:
				//
				// TYPE,FLD1,FLD2,FLD3,FLD4,FLD5
				// -----------------------------
				//  CAL,URNO,FSEC,VAFR,VATO,FREQ		--> from CALTAB create personal profile (GRPTAB))
				//  GRP,URNO,FREL,FSEC,TYPE,STAT		--> from GRPTAB create personal profile
				//  PRV,URNO,FSEC,FFKT,FAPP,STAT		--> from PRVTAB create personal profile

				// AfxMessageBox(CString(rlRetRec.TYPE)+CString(",")+CString(rlRetRec.FLD1)+CString(",")+CString(rlRetRec.FLD2)+CString(",")+CString(rlRetRec.FLD3)+CString(",")+CString(rlRetRec.FLD4)+CString(",")+CString(rlRetRec.FLD5),0);

				if( ! strcmp( rlRetRec.TYPE , "CAL" ) )
				{
					// CAL record received
					CALDATA *prlCalRec = new CALDATA;
					strcpy(prlCalRec->URNO,rlRetRec.FLD1);
					strcpy(prlCalRec->FSEC,rlRetRec.FLD2);
					StoreDate(rlRetRec.FLD3,&prlCalRec->VAFR); // converts a value to CTime
					StoreDate(rlRetRec.FLD4,&prlCalRec->VATO); // converts a value to CTime
					ogDdx.DataChanged((void *)this, CAL_INSERT,(void *)prlCalRec );
				
				}
				else if( ! strcmp( rlRetRec.TYPE , "GRP" ) )
				{
					// group record recieved
					GRPDATA *prlGrpRec = new GRPDATA;
					strcpy(prlGrpRec->URNO,rlRetRec.FLD1);
					strcpy(prlGrpRec->FREL,rlRetRec.FLD2);
					strcpy(prlGrpRec->FSEC,rlRetRec.FLD3);
					strcpy(prlGrpRec->TYPE,rlRetRec.FLD4);
					strcpy(prlGrpRec->STAT,rlRetRec.FLD5);
					ogDdx.DataChanged((void *)this, GRP_INSERT,(void *)prlGrpRec );
				}
				else if( ! strcmp( rlRetRec.TYPE , "PRV" ) )
				{
					// profile record recieved
					PRVDATA *prlPrvRec = new PRVDATA;
					strcpy(prlPrvRec->URNO,rlRetRec.FLD1);
					strcpy(prlPrvRec->FSEC,rlRetRec.FLD2);
					strcpy(prlPrvRec->FFKT,rlRetRec.FLD3);
					strcpy(prlPrvRec->FAPP,rlRetRec.FLD4);
					strcpy(prlPrvRec->STAT,rlRetRec.FLD5);
					Add(prlPrvRec);
				}
			}
		}

		blRc = true;
	}

	return blRc;

} // end AddApp()

// ----------------------------------------------------------------------------------------------
// RemApp()
//
// Remove an application from a profile
//
// pcpFapp			- application to add
// pcpFsec			- URNO of profile
// pcpErrorMessage	- on RCFail this buffer will contain an error message else blank
//
bool CedaPrvData::RemApp( char  *pcpFapp, char *pcpFsec, char *pcpErrorMessage)
{
	bool blRc = true;
	CString olListOfData;
	char pclData[BIGBUF];
	char pclFields[BIGBUF];
	char pclWhere[50];


	strcpy(pcpErrorMessage,""); // no errors yet


	sprintf(pclData,"%s,%s",pcpFapp,pcpFsec);
	strcpy(pclFields,"FAPP,FSEC");
	strcpy(pclWhere,"WHERE REMAPP");

	// Action,TableName,FieldList,Selection,Sort,Data,Dest (eg "RETURN")
	char pclSort[50] = "";
	blRc = CedaAction(pcgSecCommand,pcgTableExt,pclFields,pclWhere,pclSort,pclData);


	if( ! blRc )
	{
		if( omLastErrorMessage != CString(pclData) )
			strcpy(pcpErrorMessage,omLastErrorMessage);
		else
			sprintf(pcpErrorMessage,"Ceda error <%d> !",blRc);
	}
	else
	{
		DeleteByFsecFapp(pcpFsec,pcpFapp);
		blRc = true;
	}

	return blRc;

} // end RemApp()

// ----------------------------------------------------------------------------------------------
// UpdApp()
//
// The application in a profile was incompletely installed so reinstall it
//
// pcpFapp			- application to add
// pcpFsec			- URNO of profile
// pcpErrorMessage	- on RCFail this buffer will contain an error message else blank
//
bool CedaPrvData::UpdApp( char  *pcpFapp, char *pcpFsec, char *pcpErrorMessage)
{
	bool blRc = true;
	CString olListOfData;
	char pclData[BIGBUF];
	char pclFields[BIGBUF];
	char pclWhere[50];


	strcpy(pcpErrorMessage,""); // no errors yet


	sprintf(pclData,"%s,%s",pcpFapp,pcpFsec);
	strcpy(pclFields,"FAPP,FSEC");
	strcpy(pclWhere,"WHERE UPDAPP");

	// Action,TableName,FieldList,Selection,Sort,Data,Dest (eg "RETURN")
	char pclSort[50] = "";
	blRc = CedaAction(pcgSecCommand,pcgTableExt,pclFields,pclWhere,pclSort,pclData);


	if( ! blRc )
	{
		if( omLastErrorMessage != CString(pclData) )
			strcpy(pcpErrorMessage,omLastErrorMessage);
		else
			sprintf(pcpErrorMessage,"Ceda error <%d> !",blRc);
	}
	else
	{
		// delete the current configuration in local memory
		DeleteByFsecFapp(pcpFsec,pcpFapp);
		
		
		// read the results from the data field
		RETDATA rlRetRec;

		for (int ilLc = 0; blRc; ilLc++)
		{
			
			if ((blRc = GetBufferRecord(&omRetRecInfo,ilLc,&rlRetRec)) == true)
			{
				// omRetRecInfo contains return values, these values can be:
				//
				// TYPE,FLD1,FLD2,FLD3,FLD4,FLD5
				// -----------------------------
				//  PRV,URNO,FSEC,FFKT,FAPP,STAT		--> from PRVTAB create personal profile

				if( ! strcmp( rlRetRec.TYPE , "PRV" ) )
				{
					// profile record recieved
					PRVDATA *prlPrvRec = new PRVDATA;
					strcpy(prlPrvRec->URNO,rlRetRec.FLD1);
					strcpy(prlPrvRec->FSEC,rlRetRec.FLD2);
					strcpy(prlPrvRec->FFKT,rlRetRec.FLD3);
					strcpy(prlPrvRec->FAPP,rlRetRec.FLD4);
					strcpy(prlPrvRec->STAT,rlRetRec.FLD5);
					Add(prlPrvRec);
				}
			}
		}

		blRc = true;
	}

	return blRc;

} // end UpdApp()

static void CedaPrvDataCf(void *popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
    CedaPrvData *polThis = (CedaPrvData *)popInstance;

	switch(ipDDXType) {

	case PRV_INSERT:
		polThis->Insert((PRVDATA *)vpDataPointer);
		break;
	case PRV_DELETE:
		// delete personal profiles
        polThis->DeleteByFsec((char *)vpDataPointer);
		break;
	case SEC_DELETE:
		// a user or profile was deleted so delete the profile
        polThis->DeleteByFsec((char *)vpDataPointer);
		break;
	case SEC_DELAPP:
		// application deleted so delete applications from profiles
        polThis->DeleteByFapp((char *)vpDataPointer);
		break;
	default:
		break;
	}
}

//
// Add()
//
// Adds a PRVDATA record to omData
//
// prpPrv - record to be added
//
bool CedaPrvData::Add(PRVDATA *prpPrv)
{
	omData.Add(prpPrv);
	omUrnoMap.SetAt(prpPrv->URNO,prpPrv);
	AddToFsecMap(prpPrv);

	return true;

} // end Add()


// Given the URNO return a pointer to a PRV rec.
PRVDATA* CedaPrvData::GetPrvByUrno(const char *pcpUrno)
{
	PRVDATA *prlPrvRec = NULL;
	
	omUrnoMap.Lookup(pcpUrno,(void *&) prlPrvRec);

	return prlPrvRec;
}


//
// Insert()
//
// Add a record to omData
//
// prpPrv - PRVDATA record to add
//
bool CedaPrvData::Insert(PRVDATA *prpPrv)
{

	omData.Add(prpPrv);
	omUrnoMap.SetAt(prpPrv->URNO,prpPrv);

    return true;

} // end Insert()


//
// Update()
//
// Update a record in omData
//
bool CedaPrvData::Update(const char *pcpUrno, const char *pcpStat)
{
	PRVDATA *prlPrv = GetPrvByUrno(pcpUrno);

	if( prlPrv != NULL )
		strcpy(prlPrv->STAT,pcpStat);


    return prlPrv != NULL ? true : false;

} // end Update()

//
// DeleteByUrno()
//
// delete a record from local data using pcpUrno as the key
//
bool CedaPrvData::DeleteByUrno( const char  *pcpUrno )
{
	bool blNotFound = true;
	int ilCount = omData.GetSize();

	for(int ilLineNo = 0; blNotFound && ilLineNo < ilCount; ilLineNo++)
	{
		if(strcmp(omData[ilLineNo].URNO, pcpUrno) == 0)
		{
			DeleteFromFsecMapByUrno(omData[ilLineNo].FSEC, omData[ilLineNo].URNO);
			omUrnoMap.RemoveKey(pcpUrno);
			omData.DeleteAt(ilLineNo);
			blNotFound = false;
		}
	}

	return !blNotFound;
}

//
// DeleteByFsec()
//
// delete records from local data using pcpFsec as the key
// deletes all records for the specified user/profile
//
bool CedaPrvData::DeleteByFsec( const char  *pcpFsec )
{
	bool blNotFound = true;
	int ilCount = omData.GetSize();

	if(DeleteFromFsecMapByFsec(pcpFsec))
	{
		for(int ilLineNo = ilCount-1; ilLineNo >= 0; ilLineNo--)
		{
			if(strcmp(omData[ilLineNo].FSEC, pcpFsec) == 0)
			{
				omUrnoMap.RemoveKey(omData[ilLineNo].URNO);
				omData.DeleteAt(ilLineNo);
				blNotFound = false;
			}
		}
	}

	return !blNotFound;
}

//
// DeleteByFapp()
//
// delete a record from local data using pcpFapp as the key
// deletes all records for the specified application
//
bool CedaPrvData::DeleteByFapp( const char  *pcpFapp )
{
	bool blNotFound = true;
	int ilCount = omData.GetSize();

	for(int ilLineNo = ilCount-1; ilLineNo >= 0; ilLineNo--)
	{
		if(strcmp(omData[ilLineNo].FAPP, pcpFapp) == 0)
		{
			DeleteFromFsecMapByUrno(omData[ilLineNo].FSEC, omData[ilLineNo].URNO);
			omUrnoMap.RemoveKey(omData[ilLineNo].URNO);
			omData.DeleteAt(ilLineNo);
			blNotFound = false;
		}
	}

	return !blNotFound;
}

//
// DeleteByFsecFapp()
//
// delete a record from local data using pcpFsec and pcpFapp as the key
// deletes all records for the specified user and application
//
bool CedaPrvData::DeleteByFsecFapp( const char *pcpFsec, const char  *pcpFapp )
{
	bool blNotFound = true;
	int ilCount = omData.GetSize();

	for(int ilLineNo = ilCount-1; ilLineNo >= 0; ilLineNo--)
	{
		if(strcmp(omData[ilLineNo].FSEC, pcpFsec) == 0 && 
			strcmp(omData[ilLineNo].FAPP, pcpFapp) == 0)
		{
			DeleteFromFsecMapByUrno(omData[ilLineNo].FSEC, omData[ilLineNo].URNO);
			omUrnoMap.RemoveKey(omData[ilLineNo].URNO);
			omData.DeleteAt(ilLineNo);
			blNotFound = false;
		}
	}

	return !blNotFound;
}


// return true if a user has the specified application installed
INSTALLED_STAT CedaPrvData::ApplDefined( const char *pcpFsec, const char *pcpFapp )
{
	bool blFound = true;
//	int ilCount = omData.GetSize();
	int ilNumFktRecs = 0, ilNumPrvRecs = 0, ilNumFktRecsFound = 0;
	INSTALLED_STAT rlStat = ISNOT_INSTALLED;

	// create a map of all functions in FKTTAB for this application
	ilNumFktRecs = ogCedaFktData.CreateApplMap( pcpFapp );

	CCSPtrArray <PRVDATA> olPrvList;
	GetPrvListByFsec(pcpFsec,olPrvList);
	int ilCount = olPrvList.GetSize();
	for(int ilLineNo = 0; blFound && ilLineNo < ilCount; ilLineNo++)
	{
		if(!strcmp(olPrvList[ilLineNo].FAPP, pcpFapp))
		{
			// something was found so it is at least partially installed
		    ilNumPrvRecs++;

			// check that this FFKT is in FKTTAB (returns FALSE if not found)
			if((blFound = ogCedaFktData.IsInApplMap(olPrvList[ilLineNo].FFKT)) == true)
				ilNumFktRecsFound++;
		}
	}

	if( ilNumFktRecs == 0 && ilNumPrvRecs == 0 )
		// nothing installed in either FKT or PRV so in effect this is fully installed
		rlStat = IS_INSTALLED;
	else if( ilNumPrvRecs == 0 )
		// nothing installed in the profile
		rlStat = ISNOT_INSTALLED;
	else if( ilNumFktRecs != ilNumFktRecsFound )
		// something found in the profile that's not in the FKTTAB
		rlStat = PARTIALLY_INSTALLED;
	else
		// the records in the profile match the records in FKTTAB for this application
		rlStat = IS_INSTALLED;

	return rlStat;
}

// FSEC -> URNO
//		-> URNO
//		-> URNO
//		-> URNO
//		-> URNO
// FSEC -> URNO
//		-> URNO
// etc
void CedaPrvData::AddToFsecMap(PRVDATA *prpPrv)
{
	CMapStringToPtr *polUrnoMap;
	if(omFsecMap.Lookup(prpPrv->FSEC,(void *&)polUrnoMap))
	{
		polUrnoMap->SetAt(prpPrv->URNO,prpPrv);
	}
	else
	{
		polUrnoMap = new CMapStringToPtr;
		polUrnoMap->SetAt(prpPrv->URNO,prpPrv);
		omFsecMap.SetAt(prpPrv->FSEC,polUrnoMap);
	}
}


void CedaPrvData::GetPrvListByFsec(const char *pcpFsec, CCSPtrArray <PRVDATA> &ropPrvList, bool bpReset /*true*/)
{
	if(bpReset)
	{
		ropPrvList.RemoveAll();
	}

	CMapStringToPtr *polUrnoMap;
	CString olUrno;
	PRVDATA *prlPrv;

	if(omFsecMap.Lookup(pcpFsec,(void *& )polUrnoMap))
	{
		for(POSITION rlPos = polUrnoMap->GetStartPosition(); rlPos != NULL; )
		{
			polUrnoMap->GetNextAssoc(rlPos,olUrno,(void *& )prlPrv);
			strcpy(prlPrv->tmpSTAT,prlPrv->STAT);
			ropPrvList.Add(prlPrv);
		}
	}
}

void CedaPrvData::ClearFsecMap()
{
	CMapStringToPtr *polUrnoMap;
	CString olUrno;
	for(POSITION rlPos = omFsecMap.GetStartPosition(); rlPos != NULL; )
	{
		omFsecMap.GetNextAssoc(rlPos,olUrno,(void *& )polUrnoMap);
		polUrnoMap->RemoveAll();
		delete polUrnoMap;
	}
	omFsecMap.RemoveAll();
}

// delete all records from fsec map for a fsec
bool CedaPrvData::DeleteFromFsecMapByFsec(const char *pcpFsec)
{
	bool blDeleted = false;
	CMapStringToPtr *polUrnoMap;
	CString olUrno;

	if(omFsecMap.Lookup(pcpFsec,(void *& )polUrnoMap))
	{
		polUrnoMap->RemoveAll();
		delete polUrnoMap;
		omFsecMap.RemoveKey(pcpFsec);
		blDeleted = true;
	}

	return blDeleted;
}

// delete single record from fsec map
void CedaPrvData::DeleteFromFsecMapByUrno(const char *pcpFsec, const char *pcpUrno)
{
	CMapStringToPtr *polUrnoMap;
	CString olUrno;

	if(omFsecMap.Lookup(pcpFsec,(void *& )polUrnoMap))
	{
		polUrnoMap->RemoveKey(pcpUrno);
		if(polUrnoMap->GetCount() <= 0)
		{
			delete polUrnoMap;
			omFsecMap.RemoveKey(pcpFsec);
		}
	}
}