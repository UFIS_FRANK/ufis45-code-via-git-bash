#include "stdafx.h"
#include "AboutSecDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


/////////////////////////////////////////////////////////////////////////////
// CAboutSecDlg dialog used for App About


CAboutSecDlg::CAboutSecDlg() : CDialog(CAboutSecDlg::IDD)
{
	//{{AFX_DATA_INIT(CAboutSecDlg)
	//}}AFX_DATA_INIT
}

void CAboutSecDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAboutSecDlg)
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CAboutSecDlg, CDialog)
	//{{AFX_MSG_MAP(CAboutSecDlg)
		// No message handlers
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()
