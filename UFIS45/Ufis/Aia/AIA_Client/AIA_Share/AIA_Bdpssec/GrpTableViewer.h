// FlViewer.h
//
#ifndef __GRPTABLEVIEWER_H__
#define __GRPTABLEVIEWER_H__

#include "CCSPtrArray.h"
#include "CedaSecData.h"


struct GRPTAB_LINEDATA
{
	char	FGRP[URNOLEN];	// URNO of GRPREC if this user/profile was assigned
	char	URNO[URNOLEN];	// URNO of either profile or user
	char	USID[USIDLEN];	// USID of either profile or user
	bool	wasAssigned;
	bool	isAssigned;

	GRPTAB_LINEDATA(void)
	{
		memset(FGRP,0,URNOLEN);
		memset(URNO,0,URNOLEN);
		memset(USID,0,USIDLEN);
		wasAssigned = false;
		isAssigned = false;
	}

};


class GrpTableViewer
{
// Constructions
public:
    GrpTableViewer();
    ~GrpTableViewer();

//    void Attach(CCSTable *popAttachWnd);
    void ChangeViewTo(void);


// Internal data processing routines
public:

    void MakeLines();
	void MakeLine(SECDATA *prpSec);
	int  CreateLine(GRPTAB_LINEDATA *);
	BOOL FindLine(const char *pcpUrno, int &rilLineno);
	void DeleteAll();
	void DeleteLine(int);

	BOOL IsPassFilter(SECDATA  *prpSec);
	int CompareLine(GRPTAB_LINEDATA *,GRPTAB_LINEDATA *);
	int	 GetSize();

	int GetLineByUsid(const char *pcpUsid);
	void UpdateByUsid(char *pcpUsid);
	void Insert(char *pcpUrno);
	void Update(char *pcpUrno);
	void Delete(char *pcpUrno);
	void UpdateDisplay();

// Attributes
public:
	CListBox	*pomList;  // list of users or profiles in AllocDlg
	CStatic		*pomTitle; // name of user or profile selected in AllocDlg
    CCSPtrArray <GRPTAB_LINEDATA> omLines;
	char		pcmUrno[URNOLEN];
	char		pcmType[TYPELEN]; 
	bool		bmAllocOnly;
	bool		bmIsFsec;

};

#endif //__GRPTABLE`VIEWER_H__
