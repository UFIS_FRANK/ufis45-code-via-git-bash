// SubdTableViewer.cpp : implementation file
//

#include "stdafx.h"
#include "CCSGlobl.h"

#include "BDPS_SEC.h"
#include "SubdTableViewer.h"
#include "CedaPrvData.h"
#include "CedaFktData.h"
#include "ProfileListViewer.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


// Local function prototype
static void SubdTableViewerCf(void *popInstance, int ipDDXType,
					   void *vpDataPointer, CString &ropInstanceName);




//*************************************************************************************
//
// SubdTableViewer() - constructor
//
//*************************************************************************************

SubdTableViewer::SubdTableViewer()
{
}


//*************************************************************************************
//
// SubdTableViewer() - destructor
//
//*************************************************************************************

SubdTableViewer::~SubdTableViewer()
{
    DeleteAll();
}


//*************************************************************************************
//
// DeleteAll() - delete all lines (records) read from SECTAB
//
//*************************************************************************************

void SubdTableViewer::DeleteAll()
{

	int ilNumLines = omLines.GetSize();

	for(int ilLine=1; ilLine<=ilNumLines; ilLine++)
        DeleteLine(0);

	omIndicies.DeleteAll(); // list of indicies into omLines of the lines displayed
}

//*************************************************************************************
//
// GetSize() - return the number of lines in omLines (and thus number of lines in CCSTable)
//
//*************************************************************************************

int SubdTableViewer::GetSize()
{
	return omLines.GetSize();
}

//*************************************************************************************
//
// DeleteLine() - delete a line (record copied from SECTAB) at ipLineno
//
//*************************************************************************************

void SubdTableViewer::DeleteLine(int ipLineno)
{
	
	omLines.DeleteAt(ipLineno);
}

//*************************************************************************************
//
// Attach() - receives a pointer to the CCSTable
//
//*************************************************************************************

void SubdTableViewer::Attach(CCSTable *popTable)
{
    pomTable = popTable;
}


//*************************************************************************************
//
// ChangeViewTo() - function responsible for updating the display
//
//*************************************************************************************

void SubdTableViewer::ChangeViewTo(CCSPtrArray <ProfileListObject> &ropPrvData)
{
	// delete all lines defined
	DeleteAll();

	// get lines specified in piViewType from CedaSecData into local memory
	MakePrvLines(ropPrvData);

	// draw the lines loaded in CCSTable
	UpdateDisplay();
}

void SubdTableViewer::ChangeViewTo()
{
	// delete all lines defined
	DeleteAll();

	// for applications load data from FKTTAB only
	MakeFktLines();

	// draw the lines loaded in CCSTable
	UpdateDisplay();
}


//*************************************************************************************
//
// IsPassFilter() - select for user/profile only
//
//*************************************************************************************

bool SubdTableViewer::IsPassFilter(const char *pcpFsec)
{
//	return strcmp(pcpFsec,pcmProf) ? false : true;
	return true;
}

//*************************************************************************************
//
// IsThisFapp() - display for application only
//
//*************************************************************************************

bool SubdTableViewer::IsThisFapp(const char *pcpFapp)
{
	return strcmp(pcpFapp,pcmFapp) ? false : true;
}



//*************************************************************************************
//
// CompareLine() - 
//
//*************************************************************************************

int SubdTableViewer::CompareLine(SUBDTAB_LINEDATA *prpLine1, SUBDTAB_LINEDATA *prpLine2)
{
	return strcmp(prpLine1->SUBD,prpLine2->SUBD);
}


/////////////////////////////////////////////////////////////////////////////
// SubdTableViewer -- code specific to this class





//*************************************************************************************
//
// MakeLines() - loop through the data held in ogCedaSecData copying to local memory
//
//*************************************************************************************

void SubdTableViewer::MakePrvLines(CCSPtrArray <ProfileListObject> &ropPrvData)
{
	int ilNumPrvs = ropPrvData.GetSize();
	for(int ilPrv = 0; ilPrv < ilNumPrvs; ilPrv++)
	{
		MakePrvLine(&ropPrvData[ilPrv]);
	}
}

void SubdTableViewer::MakeFktLines()
{
	int ilCount;
	
	ilCount = ogCedaFktData.omData.GetSize();
	for(int i = 0; i < ilCount; i++)
		MakeFktLine(&ogCedaFktData.omData[i]);
}


bool SubdTableViewer::AlreadyAdded(char *pcpFapp, char *pcpSubd)
{
    int ilLineCount = omLines.GetSize();
	bool blNotFound = true;

    for (int ilLineno = 0; blNotFound && ilLineno < ilLineCount; ilLineno++)
		if( ! strcmp(omLines[ilLineno].FAPP,pcpFapp) && ! strcmp(omLines[ilLineno].SUBD,pcpSubd) )
			blNotFound = false;

	return !blNotFound;
}

//*************************************************************************************
//
// MakeLine() - copy and process a line of data, create its field and display it
//
//*************************************************************************************

void SubdTableViewer::MakePrvLine(ProfileListObject  *prpObj)
{
	FKTDATA *prlFkt = prpObj->Fkt;
	PRVDATA *prlPrv = prpObj->Prv;

	// only need to add the SUBD once
	if(!AlreadyAdded(prlPrv->FAPP,prlFkt->SUBD))
	{
		SUBDTAB_LINEDATA rlLine;

		strcpy(rlLine.FAPP,prlPrv->FAPP);
		strcpy(rlLine.SUBD,prlFkt->SUBD);

		CreateLine(&rlLine);
	}
}


void SubdTableViewer::MakeFktLine(FKTDATA  *prpFkt)
{

	// filter out either profiles or users or modul
//	if( !IsPassFilter(prpPrv->FSEC)) 
//		return;

	// only need to add the SUBD once
//	FKTDATA *prlFkt = ogCedaFktData.GetFktByUrno(prpPrv->FFKT);
	if( AlreadyAdded(prpFkt->FAPP,prpFkt->SUBD) )
		return;

	
	SUBDTAB_LINEDATA rlLine;

	strcpy(rlLine.FAPP,prpFkt->FAPP);
	strcpy(rlLine.SUBD,prpFkt->SUBD);

	CreateLine(&rlLine);
}



//*************************************************************************************
//
// CreateLine() - add the line to omLines (array of pointers to each line)
//
//*************************************************************************************

int SubdTableViewer::CreateLine(SUBDTAB_LINEDATA *prpNewLine)
{
    int ilLineCount = omLines.GetSize();

    for (int ilLineno = 0; ilLineno < ilLineCount; ilLineno++)
        if (CompareLine(prpNewLine, &omLines[ilLineno]) <= 0)
            break;  

	SUBDTAB_LINEDATA  rlLine;
    rlLine = *prpNewLine;
	omLines.NewAt(ilLineno, rlLine);

	return ilLineCount;
}




/////////////////////////////////////////////////////////////////////////////
// SubdTableViewer - display drawing routines

void SubdTableViewer::SelectLine(const int ipLine)
{
	if( ipLine < omIndicies.GetSize() )
		pomTable->SelectLine(ipLine);
}




//*************************************************************************************
//
// UpdateDisplay() - write local SECTAB data to the CCSTable
//
//*************************************************************************************

void SubdTableViewer::UpdateDisplay()
{
	// step 1 set table header
	pomTable->ResetContent();
	CCSPtrArray <TABLE_HEADER_COLUMN> omHeaderDataArray;

	pomTable->SetShowSelection(TRUE);
	TABLE_HEADER_COLUMN rlHeader;
	rlHeader.Alignment = COLALIGN_LEFT;
	rlHeader.Font = &ogCourier_Bold_10;

	// USID header
	rlHeader.Length = 450; // ???
	rlHeader.Text = GetString(IDS_SUBDTABLEVIEWER_SUBDIVISION);
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);


	pomTable->SetHeaderFields(omHeaderDataArray);
	omHeaderDataArray.RemoveAll();
	
//	pomTable->SetDefaultSeparator();
	
   // step 2 set linedata
	CCSPtrArray <TABLE_COLUMN> olColList;

	TABLE_COLUMN rlColumnData;
	rlColumnData.VerticalSeparator = SEPA_NONE;
	rlColumnData.SeparatorType = SEPA_NONE;
	rlColumnData.Font = &ogCourier_Regular_10;//&ogMSSansSerif_Bold_8;//&ogCourier_Regular_10;
	rlColumnData.Alignment = COLALIGN_LEFT;


	int ilNumLines = omLines.GetSize();
	int ilLinesAdded = 0;
	omIndicies.DeleteAll(); // list of indicies into omLines of the lines displayed
	for (int ilLc = 0; ilLc < ilNumLines; ilLc++)
	{
		SUBDTAB_LINEDATA *prlLine = &omLines[ilLc];

		if( IsThisFapp(prlLine->FAPP) )
		{
			rlColumnData.Text = prlLine->SUBD;
			olColList.NewAt(olColList.GetSize(), rlColumnData);

			pomTable->AddTextLine(olColList, (void*)prlLine);

			// remember the index of this line for updating
			int *pilIndex = new int;
			*pilIndex = ilLc;
			omIndicies.Add(pilIndex);

			// disable drag&drop for each line
			pomTable->SetTextLineDragEnable(ilLinesAdded,FALSE);

			olColList.DeleteAll();

			ilLinesAdded++;
		}
	}

	pomTable->DisplayTable();

	if( omIndicies.GetSize() > 0 )
		pomTable->SelectLine(0);

}

// given the line number selected in the Subdivisions CCSTable, return the SUBD field
char* SubdTableViewer::GetSubdByLine(const int ipLine)
{

	if( omIndicies.GetSize() > 0 && ipLine < omLines.GetSize() )
		return omLines[omIndicies[ipLine]].SUBD;
	else
		return "";
}

