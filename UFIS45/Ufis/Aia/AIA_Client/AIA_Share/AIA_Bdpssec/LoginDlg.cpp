// LoginDlg.cpp : implementation file
//
#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#include "stdafx.h"
#include "CCSCedadata.h"
#include "CCSCedacom.h"
#include "CCSGlobl.h"
#include "LoginDlg.h"
#include "PrivList.h"
#include "CCSEdit.h"


#ifdef _DEBUG
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CLoginDialog dialog

//extern CCSBasicData ogBasicData;


CLoginDialog::CLoginDialog(const char *pcpHomeAirport, const char *pcpAppl, const char *pcpWorkstation, CWnd* pParent /*=NULL*/)
    : CDialog(CLoginDialog::IDD, pParent)
{
    //{{AFX_DATA_INIT(CLoginDialog)
	m_UsidCaption = _T("");
	//}}AFX_DATA_INIT

	strcpy(pcmHomeAirport,pcpHomeAirport);
	strcpy(pcmAppl,pcpAppl);
	strcpy(pcmWorkstation,pcpWorkstation);
	bmIsSuperUser = false;

	// default text
//	INVALID_USERNAME = "Der Benutzername existiert nicht !";
//	INVALID_APPLICATION = "Der Modulname existiert nicht ! \nBitte wenden Sie sich an ihren System Administrator.";
//	INVALID_PASSWORD = "Das Passwort ist ung�ltig !";
//	EXPIRED_USERNAME = "Der Benutzername ist entweder abgelaufen oder noch nicht g�ltig ! \nBitte wenden Sie sich an ihren System Administrator.";
//	EXPIRED_APPLICATION = "Der Modulname ist entweder abgelaufen oder noch nicht g�ltig ! \nBitte wenden Sie sich an ihren System Administrator.";
//	EXPIRED_WORKSTATION = "Die Workstation ist entweder abgelaufen oder noch nicht g�ltig ! \nBitte wenden Sie sich an ihren System Administrator.";
//	DISABLED_USERNAME = "Der Benutzer ist nicht aktiviert ! \nBitte wenden Sie sich an ihren System Administrator.";
//	DISABLED_APPLICATION = "Der Modul ist nicht aktiviert ! \nBitte wenden Sie sich an ihren System Administrator.";
//	DISABLED_WORKSTATION = "Die Workstation ist nicht aktiviert ! \nBitte wenden Sie sich an ihren System Administrator.";
//	UNDEFINED_PROFILE = "Der Benutzer hat zur Zeit keine Berechtigungen diese Applikation ! \nBitte wenden Sie sich an ihren System Administrator.";
//	MESSAGE_BOX_CAPTION = "Login Ung�ltig.";
//	USERNAME_CAPTION = "Benutzername:";
//	PASSWORD_CAPTION = "Passwort:";
//	OK_CAPTION = "&OK";
//	CANCEL_CAPTION = "&Abbrechen";
}

void CLoginDialog::DoDataExchange(CDataExchange* pDX)
{
    CDialog::DoDataExchange(pDX);
    //{{AFX_DATA_MAP(CLoginDialog)
	DDX_Control(pDX, IDC_USERNAME, m_UsernameCtrl);
	DDX_Control(pDX, IDC_PASSCAPTION, m_PassCaption);
	DDX_Control(pDX, IDOK, m_OkCaption);
	DDX_Control(pDX, IDCANCEL, m_CancelCaption);
	DDX_Control(pDX, IDC_PASSWORD, m_PasswordCtrl);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CLoginDialog, CDialog)
    //{{AFX_MSG_MAP(CLoginDialog)
    ON_WM_PAINT()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// CLoginDialog message handlers

void CLoginDialog::OnPaint() 
{
    CPaintDC dc(this); // device context for painting
    
    // TODO: Add your message handler code here
    CBrush olBrush( RGB( 192, 192, 192 ) );
    CBrush *polOldBrush = dc.SelectObject( &olBrush );
    CRect olRect;
    GetClientRect( &olRect );
    dc.FillRect( &olRect, &olBrush );
    dc.SelectObject( polOldBrush );
    
    CDC olDC;
    olDC.CreateCompatibleDC( &dc );
    CBitmap olBitmap;
    olBitmap.LoadBitmap( IDB_LOGIN );
    olDC.SelectObject( &olBitmap );
    dc.BitBlt( 0, 140, 409, 52, &olDC, 0, 0, SRCCOPY );
    olDC.DeleteDC( );
    
    // Do not call CDialog::OnPaint() for painting messages
}

void CLoginDialog::OnOK() 
{
	bool blRc = true;
	CString olUsername,olPassword,olWorkstation;
	CWnd *polFocusCtrl = &m_UsernameCtrl;
	char pclErrTxt[500];

	// get the username
	m_UsernameCtrl.GetWindowText(olUsername);
	m_PasswordCtrl.GetWindowText(olPassword);


	ogBasicData.omUserID = olUsername;
	ogCommHandler.SetUser(olUsername);
	strcpy(CCSCedaData::pcmUser, olUsername);
	strcpy(CCSCedaData::pcmReqId, ogCommHandler.pcmReqId);

	// check the username + password
	blRc = ogPrivList.Login(pcmHomeAirport,olUsername,olPassword,pcmAppl,pcmWorkstation);

	bool blValidLogin = false;

	if( blRc )
	{
		if(!strcmp(olUsername,ogSuperUser))
		{
			bmIsSuperUser = true;
		}
		blValidLogin = true;
//		else
//		{
//			if(ogPrivList.GetStat("BDPSSEC_CAN_USE_SEC") == '1')
//			{
//				blValidLogin = true;
//			}
//			else
//			{
//				strcpy(pclErrTxt,GetString(IDS_UNDEFINED_PROFILE));
//			}
//		}
	}
	else
	{
		if( !strcmp(ogPrivList.omErrorMessage,"LOGINERROR INVALID_USER") ) { 
			strcpy(pclErrTxt,GetString(IDS_INVALID_USERNAME));
		}
		else if( !strcmp(ogPrivList.omErrorMessage,"LOGINERROR INVALID_SUPERUSER") ) { 
			strcpy(pclErrTxt,GetString(IDS_UNDEFINED_PROFILE));
		}
		else if( !strcmp(ogPrivList.omErrorMessage,"LOGINERROR INVALID_APPLICATION") ) {
			strcpy(pclErrTxt,GetString(IDS_INVALID_APPLICATION));
		}
		else if( !strcmp(ogPrivList.omErrorMessage,"LOGINERROR INVALID_PASSWORD") ) {
			strcpy(pclErrTxt,GetString(IDS_INVALID_PASSWORD));
			polFocusCtrl = &m_PasswordCtrl;
		}
		else if( !strcmp(ogPrivList.omErrorMessage,"LOGINERROR EXPIRED_USER") ) {
			strcpy(pclErrTxt,GetString(IDS_EXPIRED_USERNAME));
		}
		else if( !strcmp(ogPrivList.omErrorMessage,"LOGINERROR EXPIRED_APPLICATION") ) {
			strcpy(pclErrTxt,GetString(IDS_EXPIRED_APPLICATION));
		}
		else if( !strcmp(ogPrivList.omErrorMessage,"LOGINERROR EXPIRED_WORKSTATION") ) {
			strcpy(pclErrTxt,GetString(IDS_EXPIRED_WORKSTATION));
		}
		else if( !strcmp(ogPrivList.omErrorMessage,"LOGINERROR DISABLED_USER") ) {
			strcpy(pclErrTxt,GetString(IDS_DISABLED_USERNAME));
		}
		else if( !strcmp(ogPrivList.omErrorMessage,"LOGINERROR DISABLED_APPLICATION") ) {
			strcpy(pclErrTxt,GetString(IDS_DISABLED_APPLICATION));
		}
		else if( !strcmp(ogPrivList.omErrorMessage,"LOGINERROR DISABLED_WORKSTATION") ) {
			strcpy(pclErrTxt,GetString(IDS_DISABLED_WORKSTATION));
		}
		else if( !strcmp(ogPrivList.omErrorMessage,"LOGINERROR UNDEFINED_PROFILE") ) {
			strcpy(pclErrTxt,GetString(IDS_UNDEFINED_PROFILE));
		}
		else {
			sprintf(pclErrTxt,"%s\n(omLastErrorMessage=<%s>)",GetString(IDS_DATABASE_ERROR),ogPrivList.omErrorMessage);
		}

	}

	if(blValidLogin)
	{
		CDialog::OnOK();
	}
	else
	{
		MessageBox(pclErrTxt,GetString(IDS_INVALIDLOGINCAPTION),MB_ICONINFORMATION);
		imLoginCount++;

		if( imLoginCount >= MAX_LOGIN )
			OnCancel();
		else
		{
			if( polFocusCtrl != NULL )
				polFocusCtrl->SetFocus(); // set the focus to the offending control
		}
	}

}


void CLoginDialog::OnCancel() 
{
	// TODO: Add extra cleanup here
	
	CDialog::OnCancel();
}




BOOL CLoginDialog::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	imLoginCount = 0;
//	m_UsidCaption.SetWindowText(USERNAME_CAPTION);
//	m_PassCaption.SetWindowText(PASSWORD_CAPTION);
//	m_OkCaption.SetWindowText(OK_CAPTION);
//	m_CancelCaption.SetWindowText(CANCEL_CAPTION);
	m_UsernameCtrl.SetFocus();

	m_UsernameCtrl.SetTypeToString("X(32)",32,1);
	m_UsernameCtrl.SetBKColor(YELLOW);
	m_UsernameCtrl.SetTextErrColor(RED);
	m_UsernameCtrl.SetFocus();

	m_PasswordCtrl.SetTypeToString("X(32)",32,1);
	m_PasswordCtrl.SetBKColor(YELLOW);
	m_PasswordCtrl.SetTextErrColor(RED);


	CString olCaption;
	olCaption.Format(GetString(IDS_LOGINCAPTION),pcgAppName,ogCommHandler.pcmRealHostName,ogCommHandler.pcmRealHostType);
	SetWindowText(olCaption);

	return FALSE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

