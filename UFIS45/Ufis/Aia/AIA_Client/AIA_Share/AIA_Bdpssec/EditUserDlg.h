// EditUserDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CEditUserDlg dialog

#ifndef __EDITUSERDLG__
#define __EDITUSERDLG__


#include "MessList.h"
#include "CCSPtrArray.h"
#include "CCSEdit.h"
#include "CedaCalData.h"
#include "CCSTable.h"
#include "CalTableViewer.h"





class CEditUserDlg : public CDialog
{
// Construction
public:
	CEditUserDlg(CWnd* pParent = NULL);   // standard constructor
	~CEditUserDlg();

// Dialog Data
	//{{AFX_DATA(CEditUserDlg)
	enum { IDD = IDD_EDIT_USER };
	CButton	m_CanUseSecCtrl;
	CButton	m_Freq7;
	CButton	m_Freq6;
	CButton	m_Freq5;
	CButton	m_Freq4;
	CButton	m_Freq3;
	CButton	m_Freq2;
	CButton	m_Freq1;
	CStatic	m_UsidCaptionCtrl;
	CStatic	m_StatHead;
	CButton	m_InitialRightsGroup;
	CButton	m_StatCtrl;
	CButton	m_OkButton;
	CButton	m_CancelButton;
	CButton	m_NoneCtrl;
	CButton	m_StandardCtrl;
	CButton	m_DisableAllCtrl;
	CButton	m_EnableAllCtrl;
	CString	m_USID;
	BOOL	m_STAT;
	CString	m_VAFRdate;
	CString	m_VAFRtime;
	CString	m_VATOdate;
	CString	m_VATOtime;
	int		m_ModuleType;
	CString	m_UsidCaption;
	CCSEdit	m_UsidCtrl;
	CCSEdit	m_VAFRdateCtrl;
	CCSEdit	m_VAFRtimeCtrl;
	CCSEdit	m_VATOdateCtrl;
	CCSEdit	m_VATOtimeCtrl;
	CCSEdit	m_NameCtrl;
	CCSEdit	m_RemaCtrl;
	CString	m_NAME;
	CString	m_REMA;
	BOOL	m_CanUseSec;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CEditUserDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL


// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CEditUserDlg)
	virtual void OnOK();
	virtual BOOL OnInitDialog();
	afx_msg void OnStat();
	afx_msg void OnFreq1();
	afx_msg void OnFreq2();
	afx_msg void OnFreq3();
	afx_msg void OnFreq4();
	afx_msg void OnFreq5();
	afx_msg void OnFreq6();
	afx_msg void OnFreq7();
	afx_msg void OnFreqAll();
	afx_msg LONG OnTableLButtonDown(UINT ipParam, LONG lpParam);
	afx_msg LONG OnTableRButtonDown(UINT ipParam, LONG lpParam);
	afx_msg LONG OnTableSelChange(UINT ipParam, LONG lpParam);
	afx_msg LONG OnEditKillFocus(UINT ipParam, LONG lpParam);
	afx_msg void OnInsertDate();
	afx_msg void OnDeleteDate();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()


private:
	bool FormatDate(CEdit &); // format date from ddmmyyyy to dd.mm.yyyy
	bool FormatTime(CEdit &); // format date from hhmm to hh:mm
	CBitmap omRedBitmap, omGreenBitmap, omAmberBitmap;
	void SelectDate(int ipLine);
	void UpdateDates(void);

public:
	CString	omWindowCaption; // "Insert User" or "Update Profile" etc.
	CString omEnabledStat, omDisabledStat;
	CString	omOldUsid; // on update, newUsid can be the same as oldUsid
	bool bmIsUpdate;
	CTime omVAFR, omVATO; // formatted versions of VAFR/VATO
	CString omUrno,omUsid,omName,omRema;
	CString omVafrDate,omVafrTime,omVatoDate,omVatoTime;
	CCSTable *pomCalTable;
	CalTableViewer omCalTableViewer;

};

#endif // __EDITUSERDLG__
