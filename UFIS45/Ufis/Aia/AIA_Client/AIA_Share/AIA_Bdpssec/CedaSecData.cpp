// CedaSecData.cpp - Read/update SECTAB
//
#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#include "stdafx.h"
#include "BDPS_SEC.h"
#include "CCSGlobl.h"
#include "CedaSecData.h"
#include "CedaCalData.h" // CALDATA rec is returned from create user
#include "CedaGrpData.h" // GRPDATA rec is returned from create user
#include "CedaPrvData.h" // PRVDATA rec is returned from create user
#include "CedaFktData.h" // FKTDATA rec is returned from create appl
#include "InitialLoadDlg.h"
#include "MessList.h"



// ----------------------------------------------------------------------------------------------
// Constructor

CedaSecData::CedaSecData()
{
    // Create an array of CEDARECINFO for SECDATA
    BEGIN_CEDARECINFO(SECDATA, CedaSecData)
        FIELD_CHAR_TRIM(URNO,"URNO")
        FIELD_CHAR_TRIM(USID,"USID")
        FIELD_CHAR_TRIM(NAME,"NAME")
        FIELD_CHAR_TRIM(TYPE,"TYPE")
        FIELD_CHAR_TRIM(LANG,"LANG")
        FIELD_CHAR_TRIM(STAT,"STAT")
        FIELD_CHAR_TRIM(REMA,"REMA")
    END_CEDARECINFO

    // Copy the record structure
    for (int i = 0; i < sizeof(CedaSecData)/sizeof(CedaSecData[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&CedaSecData[i],sizeof(CEDARECINFO));
        omRecInfo.Add(prpCedaRecInfo);
	}


    // Initialize table name and field names
    sprintf(pcmTableName,"SEC%s",pcgTableExt);
    pcmFieldList = "URNO,USID,NAME,TYPE,LANG,STAT,REMA";


} // end CedaSecData()


// ----------------------------------------------------------------------------------------------
// Destructor

CedaSecData::~CedaSecData(void)
{
	ogDdx.UnRegister(this,NOTUSED);
	omRecInfo.DeleteAll();
	omRetRecInfo.DeleteAll();
	ClearAll();

} // end ~CedaSecData()


// ----------------------------------------------------------------------------------------------
// ClearAll()

void CedaSecData::ClearAll(void)
{
    omData.DeleteAll();
	omUrnoMap.RemoveAll();

} // end ClearAll()



// ----------------------------------------------------------------------------------------------
// Read()

bool CedaSecData::Read()
{
    sprintf(pcmTableName,"SEC%s",pcgTableExt);


	// the following is another CEDARECINFO structure allowing a different structure
	// to be read in GetBufferRecord() - this must be defined in a different function
	// to the other structure otherwise error !
    // Create an array of CEDARECINFO for RETDATA (return values from CRU)
    BEGIN_CEDARECINFO(RETDATA, CedaRetData)
        FIELD_CHAR_TRIM(TYPE,"TYPE")
        FIELD_CHAR_TRIM(FLD1,"FLD1")
        FIELD_CHAR_TRIM(FLD2,"FLD2")
        FIELD_CHAR_TRIM(FLD3,"FLD3")
        FIELD_CHAR_TRIM(FLD4,"FLD4")
        FIELD_CHAR_TRIM(FLD5,"FLD5")
        FIELD_CHAR_TRIM(FLD6,"FLD6")
        FIELD_CHAR_TRIM(FLD7,"FLD7")
        FIELD_CHAR_TRIM(FLD8,"FLD8")
    END_CEDARECINFO

    // Copy the record structure
    for (int i = 0; i < sizeof(CedaRetData)/sizeof(CedaRetData[0]); i++)
	{
		CEDARECINFO *prpCedaRetRecInfo = new CEDARECINFO;
		memcpy(prpCedaRetRecInfo,&CedaRetData[i],sizeof(CEDARECINFO));
        omRetRecInfo.Add(prpCedaRetRecInfo);
	}

	if (pogInitialLoad != NULL)
	{
		char pclTmp[200];
		sprintf(pclTmp,GetString(IDS_LOADING_TABLE),pcmTableName);
		pogInitialLoad->SetMessage(pclTmp);
	}

    // Select data from the database
	char pclWhere[512];
	bool blRc = true;
	char pclFields[512];
	strcpy(pclFields,pcmFieldList);
	char pclSort[512] = "";
	char pclData[1024] = "";
	char pclTable[20];
	memset(pclTable,0,20);
	strncpy(pclTable,pcmTableName,6);


	sprintf(pclWhere,"ORDER BY TYPE,USID DESC");
	char pclCom[50] = "RT";
//	blRc = CedaAction(pclCom, pclWhere);
	blRc = CedaAction(pclCom,pclTable,pclFields,pclWhere,pclSort,pclData);
	if (! blRc)
	{
		ogLog.Trace("LOADSEC","Ceda-Error %s (%d)\n",omLastErrorMessage,blRc);

		// it's not an error if no data were found
		if(omLastErrorMessage.Find(ORA_NOT_FOUND) != -1 || omLastErrorMessage == "No Data Found")
			blRc = true;
	}
	else
	{
		// Load data from CedaData into the dynamic array of records
		for (int ilLc = 0; blRc; ilLc++)
		{

			SECDATA *prpSec = new SECDATA;
			if ((blRc = GetBufferRecord(ilLc,prpSec)) == true)
			{
				Add(prpSec);
			}
			else
			{
				delete prpSec;
			}
		}

		int ilSize = omData.GetSize();
		ogLog.Trace("LOADSEC"," %d records loaded from %s",ilSize,pcmTableName);
		if (pogInitialLoad != NULL)
		{
			char pclBuf[128];
			pogInitialLoad->SetProgress(20);
			sprintf(pclBuf,GetString(IDS_NUM_DB_RECS_READ),ilSize,pcmTableName);
			pogInitialLoad->SetMessage(CString(pclBuf));
			pogInitialLoad->UpdateWindow();
		}

		blRc = true;
	}

	return blRc;

} // Read()


// ----------------------------------------------------------------------------------------------
// Add()

bool CedaSecData::Add(SECDATA *prpSec)
{

	omData.Add(prpSec);
	omUrnoMap.SetAt(prpSec->URNO,prpSec);

    return true;

} // end Add()


// ----------------------------------------------------------------------------------------------
// GetSecByUrno()

SECDATA* CedaSecData::GetSecByUrno(const char *pcpUrno)
{
	SECDATA *prlSecRec = NULL;
	
	omUrnoMap.Lookup(pcpUrno,(void *&) prlSecRec);

	return prlSecRec;

} // end GetSecByUrno()

// returns true if there are no records of type pcpType
bool CedaSecData::NoRecords(const char *pcpType)
{
	bool blNotFound = true;
	
	int ilCount = omData.GetSize();
	for(int ilLineNo = 0; blNotFound && ilLineNo < ilCount; ilLineNo++)
		if(strcmp(omData[ilLineNo].TYPE, pcpType) == 0)
			blNotFound = false;

	return blNotFound;

} // end NoRecords()

// ----------------------------------------------------------------------------------------------
// GetSecByUsid()
//
// Used to check if a USID/TYPE combination already exists

SECDATA* CedaSecData::GetSecByUsid(const char *pcpUSID, SEC_REC_TYPE rpCurrType)
{
	SECDATA *prlSecRec = NULL;
	
	int ilCount = omData.GetSize();
	for(int ilLineNo = 0; prlSecRec == NULL && ilLineNo < ilCount; ilLineNo++)
		if(strcmp(omData[ilLineNo].USID, pcpUSID) == 0 && strchr(omData[ilLineNo].TYPE, rpCurrType))
			prlSecRec = (SECDATA *) &omData[ilLineNo];

	return prlSecRec;

} // end GetSecByUsid()


// ----------------------------------------------------------------------------------------------
// NewSec()
//
// Create a new user/profile in SECTAB,CALTAB,GRPTAB,PRVTAB
//
// prpSecRec		- record containing USID,NAME, etc , also receives the URNO of the created record
// prpCalRecs		- record containing calendar dates for the user and to receive URNOs of the created records
// ipMODU			- Create profile --> 0=None 1=Standard 2=DisableAll 3=EnableAll
// pcpErrorMessage	- on RCFail this buffer will contain an error message else blank
//
bool CedaSecData::NewSec( SECDATA  *prpSecRec, CCSPtrArray <CALDATA> &ropCalRecs, const int ipMODU, char *pcpErrorMessage)
{
	bool blRc = true;
	CString olListOfData;
	char *pclData;
	char pclFields[BIGBUF];
	char pclWhere[50];


	strcpy(pcpErrorMessage,""); // no errors yet

	CString olData;
	CString olUsid = prpSecRec->USID;
	MakeCedaString(olUsid);
	CString olName = prpSecRec->NAME;
	MakeCedaString(olName);
	CString olRema = prpSecRec->REMA;
	MakeCedaString(olRema);
//	olData.Format("%s,%s,%s,%s,%s,%s,%s,%d",prpSecRec->USID,prpSecRec->NAME,prpSecRec->TYPE,GetString(IDS_DEFAULT_PASSWORD),prpSecRec->STAT,prpSecRec->LANG,prpSecRec->REMA,ipMODU);
	olData.Format("%s,%s,%s,%s,%s,%s,%s,%d",olUsid,olName,prpSecRec->TYPE,GetString(IDS_DEFAULT_PASSWORD),prpSecRec->STAT,prpSecRec->LANG,olRema,ipMODU);

	
	int ilNumRecs = ropCalRecs.GetSize();
	char pclTmp[100];
	for(int ilCal = 0; ilCal < ilNumRecs; ilCal++ )
	{
		sprintf(pclTmp,",%s,%s,%s",ropCalRecs[ilCal].VAFR.Format("%Y%m%d%H%M%S"),ropCalRecs[ilCal].VATO.Format("%Y%m%d%H%M%S"),ropCalRecs[ilCal].FREQ);
		olData += CString(pclTmp);
	}
	
	pclData = new char[max(olData.GetLength()+1,BIGBUF)];
	strcpy(pclData,olData);
	
	strcpy(pclFields,"USID,NAME,TYPE,PASS,STAT,LANG,REMA,MODU,VAFR,VATO,FREQ");
	strcpy(pclWhere,"WHERE NEWSEC");

	// Action,TableName,FieldList,Selection,Sort,Data,Dest (eg "RETURN")
	char pclSort[50] = "";
	blRc = CedaAction(pcgSecCommand,pcgTableExt,pclFields,pclWhere,pclSort,pclData);


	if( ! blRc )
	{
		if( omLastErrorMessage != CString(pclData) )
			strcpy(pcpErrorMessage,omLastErrorMessage);
		else
			sprintf(pcpErrorMessage,"Ceda error <%d> !",blRc);
	}
	else
	{
		// read the results from the data field
		RETDATA rlRetRec;
		SECDATA *prlSecRec;

		for (int ilLc = 0; blRc; ilLc++)
		{
			
			if ((blRc = GetBufferRecord(&omRetRecInfo,ilLc,&rlRetRec)) == true)
			{
				// omRetRecInfo contains return values from CRU
				// these values can be:
				//
				// TYPE,FLD1,FLD2,FLD3,FLD4,FLD5
				// -----------------------------
				//  SEC,URNO							--> from create user in SECTAB
				//  CAL,URNO,FSEC,VAFR,VATO,FREQ		--> from CALTAB (create user (SECTAB) and create personal profile (GRPTAB))
				//  GRP,URNO,FREL,FSEC,TYPE,STAT		--> from GRPTAB create personal profile
				//  PRV,URNO,FSEC,FFKT,FAPP,STAT		--> from PRVTAB create personal profile

				// AfxMessageBox(CString(rlRetRec.TYPE)+CString(",")+CString(rlRetRec.FLD1)+CString(",")+CString(rlRetRec.FLD2)+CString(",")+CString(rlRetRec.FLD3)+CString(",")+CString(rlRetRec.FLD4)+CString(",")+CString(rlRetRec.FLD5),0);

				if( ! strcmp( rlRetRec.TYPE , "SEC" ) )
				{
					// new URNO in SECTAB received
					prlSecRec = new SECDATA;
					strcpy(prpSecRec->URNO,rlRetRec.FLD1);
					memcpy(prlSecRec,prpSecRec,sizeof(SECDATA));
					Insert(prlSecRec);
				}
				else if( ! strcmp( rlRetRec.TYPE , "CAL" ) )
				{
					// CAL record received
					CALDATA *prlCalRec = new CALDATA;
					strcpy(prlCalRec->URNO,rlRetRec.FLD1);
					strcpy(prlCalRec->FSEC,rlRetRec.FLD2);
					StoreDate(rlRetRec.FLD3,&prlCalRec->VAFR); // converts a value to CTime
					StoreDate(rlRetRec.FLD4,&prlCalRec->VATO); // converts a value to CTime
					strcpy(prlCalRec->FREQ,rlRetRec.FLD5);
					ogDdx.DataChanged((void *)this, CAL_INSERT,(void *)prlCalRec );
				
				}
				else if( ! strcmp( rlRetRec.TYPE , "GRP" ) )
				{
					// group record recieved
					GRPDATA *prlGrpRec = new GRPDATA;
					strcpy(prlGrpRec->URNO,rlRetRec.FLD1);
					strcpy(prlGrpRec->FREL,rlRetRec.FLD2);
					strcpy(prlGrpRec->FSEC,rlRetRec.FLD3);
					strcpy(prlGrpRec->TYPE,rlRetRec.FLD4);
					strcpy(prlGrpRec->STAT,rlRetRec.FLD5);
					ogDdx.DataChanged((void *)this, GRP_INSERT,(void *)prlGrpRec );
				}
				else if( ! strcmp( rlRetRec.TYPE , "PRV" ) )
				{
					// profile record recieved
					PRVDATA *prlPrvRec = new PRVDATA;
					strcpy(prlPrvRec->URNO,rlRetRec.FLD1);
					strcpy(prlPrvRec->FSEC,rlRetRec.FLD2);
					strcpy(prlPrvRec->FFKT,rlRetRec.FLD3);
					strcpy(prlPrvRec->FAPP,rlRetRec.FLD4);
					strcpy(prlPrvRec->STAT,rlRetRec.FLD5);
					ogDdx.DataChanged((void *)this, PRV_INSERT,(void *)prlPrvRec );
				}
			}
		}
		if( strlen(prlSecRec->URNO) > 0 )
			ogDdx.DataChanged((void *)this, SEC_VIEWER_INSERT,(void *)prlSecRec->URNO );

		blRc = true;
	}

	delete [] pclData;
	return blRc;

} // end NewSec()


// ----------------------------------------------------------------------------------------------
// NewApp()
//
// Create a new application in SECTAB,CALTAB,FKTTAB
//
// prpSecRec		- record containing USID,NAME, etc , also receives the URNO of the created record
// prpCalRecs		- record containing calendar dates for the user and to receive URNOs of the created records
// pcpErrorMessage	- on RCFail this buffer will contain an error message else blank
//
bool CedaSecData::NewApp( SECDATA  *prpSecRec, CCSPtrArray <CALDATA> &ropCalRecs, char *pcpErrorMessage)
{
	bool blRc = true;
	CString olListOfData;
	char *pclData;
	char pclFields[BIGBUF];
	char pclWhere[50];


	strcpy(pcpErrorMessage,""); // no errors yet

	CString olData;
	CString olUsid = prpSecRec->USID;
	MakeCedaString(olUsid);
	CString olName = prpSecRec->NAME;
	MakeCedaString(olName);
	CString olRema = prpSecRec->REMA;
	MakeCedaString(olRema);
//	olData.Format("%s,%s,%s,%s,%s",prpSecRec->USID,prpSecRec->NAME,prpSecRec->TYPE,prpSecRec->STAT,prpSecRec->REMA);
	olData.Format("%s,%s,%s,%s,%s",olUsid,olName,prpSecRec->TYPE,prpSecRec->STAT,olRema);

	
	int ilNumRecs = ropCalRecs.GetSize();
	char pclTmp[100];
	for(int ilCal = 0; ilCal < ilNumRecs; ilCal++ )
	{
		sprintf(pclTmp,",%s,%s,%s",ropCalRecs[ilCal].VAFR.Format("%Y%m%d%H%M%S"),ropCalRecs[ilCal].VATO.Format("%Y%m%d%H%M%S"),ropCalRecs[ilCal].FREQ);
		olData += CString(pclTmp);
	}
	
	pclData = new char[max(olData.GetLength()+1,BIGBUF)];
	strcpy(pclData,olData);

	strcpy(pclFields,"USID,NAME,TYPE,STAT,REMA,VAFR,VATO,FREQ");
	strcpy(pclWhere,"WHERE NEWAPP");

	// Action,TableName,FieldList,Selection,Sort,Data,Dest (eg "RETURN")
	char pclSort[50] = "";
	blRc = CedaAction(pcgSecCommand,pcgTableExt,pclFields,pclWhere,pclSort,pclData);


	if( ! blRc )
	{
		if( omLastErrorMessage != CString(pclData) )
			strcpy(pcpErrorMessage,omLastErrorMessage);
		else
			sprintf(pcpErrorMessage,"Ceda error <%d> !",blRc);
	}
	else
	{
		// read the results from the data field
		RETDATA rlRetRec;
		SECDATA *prlSecRec;

		for (int ilLc = 0; blRc; ilLc++)
		{
			
			if ((blRc = GetBufferRecord(&omRetRecInfo,ilLc,&rlRetRec)) == true)
			{
				// omRetRecInfo contains return values from CRU
				// these values can be:
				//
				// TYPE,FLD1,FLD2,FLD3,FLD4,FLD5,FLD6,FLD7,FLD8
				// --------------------------------------------
				//  SEC,URNO										--> from create application in SECTAB
				//  CAL,URNO,FSEC,VAFR,VATO,FREQ					--> from CALTAB (create application (SECTAB))
				//  FKT,URNO,FAPP,SUBD,SDAL,FUNC,FUAL,TYPE,STAT		--> from FKTTAB create ModuInit button

				// AfxMessageBox(CString(rlRetRec.TYPE)+CString(",")+CString(rlRetRec.FLD1)+CString(",")+CString(rlRetRec.FLD2)+CString(",")+CString(rlRetRec.FLD3)+CString(",")+CString(rlRetRec.FLD4)+CString(",")+CString(rlRetRec.FLD5),0);

				if( ! strcmp( rlRetRec.TYPE , "SEC" ) )
				{
					// new URNO in SECTAB received
					prlSecRec = new SECDATA;
					strcpy(prpSecRec->URNO,rlRetRec.FLD1);
					memcpy(prlSecRec,prpSecRec,sizeof(SECDATA));
					Insert(prlSecRec);
				}
				else if( ! strcmp( rlRetRec.TYPE , "CAL" ) )
				{
					// CAL record received
					CALDATA *prlCalRec = new CALDATA;
					strcpy(prlCalRec->URNO,rlRetRec.FLD1);
					strcpy(prlCalRec->FSEC,rlRetRec.FLD2);
					StoreDate(rlRetRec.FLD3,&prlCalRec->VAFR); // converts a value to CTime
					StoreDate(rlRetRec.FLD4,&prlCalRec->VATO); // converts a value to CTime
					strcpy(prlCalRec->FREQ,rlRetRec.FLD5);
					ogDdx.DataChanged((void *)this, CAL_INSERT,(void *)prlCalRec );
				
				}
				else if( ! strcmp( rlRetRec.TYPE , "FKT" ) )
				{
					// group record recieved
					FKTDATA *prlFktRec = new FKTDATA;
					strcpy(prlFktRec->URNO,rlRetRec.FLD1);
					strcpy(prlFktRec->FAPP,rlRetRec.FLD2);
					strcpy(prlFktRec->SUBD,rlRetRec.FLD3);
					strcpy(prlFktRec->SDAL,rlRetRec.FLD4);
					strcpy(prlFktRec->FUNC,rlRetRec.FLD5);
					strcpy(prlFktRec->FUAL,rlRetRec.FLD6);
					strcpy(prlFktRec->TYPE,rlRetRec.FLD7);
					strcpy(prlFktRec->STAT,rlRetRec.FLD8);
					ogDdx.DataChanged((void *)this, FKT_INSERT,(void *)prlFktRec );
				}
			}
		}
		if( strlen(prlSecRec->URNO) > 0 )
			ogDdx.DataChanged((void *)this, SEC_VIEWER_INSERT,(void *)prlSecRec->URNO );

		blRc = true;
	}

	delete [] pclData;
	return blRc;

} // end NewApp()


// ----------------------------------------------------------------------------------------------
// UpdSec()
//
// Update records in SECTAB
//
// prpSecRec		- record containing USID,NAME, etc
// pcpErrorMessage	- on RCFail this buffer will contain an error message else blank
//
bool CedaSecData::UpdSec( SECDATA *prpSecRec, char *pcpErrorMessage)
{
	bool blRc = true;
	CString olListOfData;
	char pclData[BIGBUF];
	char pclFields[BIGBUF];
	char pclWhere[50];


	strcpy(pcpErrorMessage,""); // no errors yet


	CString olUsid = prpSecRec->USID;
	MakeCedaString(olUsid);
	CString olName = prpSecRec->NAME;
	MakeCedaString(olName);
	CString olRema = prpSecRec->REMA;
	MakeCedaString(olRema);
	sprintf(pclData,"%s,%s,%s,%s,%s,%s",prpSecRec->URNO,olUsid,olName,prpSecRec->STAT,prpSecRec->LANG,olRema);
//	sprintf(pclData,"%s,%s,%s,%s,%s,%s",prpSecRec->URNO,prpSecRec->USID,prpSecRec->NAME,prpSecRec->STAT,prpSecRec->LANG,prpSecRec->REMA);
	strcpy(pclFields,"URNO,USID,NAME,STAT,LANG,REMA");
	strcpy(pclWhere,"WHERE UPDSEC");

	// Action,TableName,FieldList,Selection,Sort,Data,Dest (eg "RETURN")
	char pclSort[50] = "";
	blRc = CedaAction(pcgSecCommand,pcgTableExt,pclFields,pclWhere,pclSort,pclData);


	if( ! blRc )
	{
		if( omLastErrorMessage != CString(pclData) )
			strcpy(pcpErrorMessage,omLastErrorMessage);
		else
			sprintf(pcpErrorMessage,"Ceda error <%d> !",blRc);
	}
	else
	{
		// update local data
		Update(prpSecRec);

		// update the CALTAB
//		ogDdx.DataChanged((void *)this, CAL_UPDATE,(void *) prpCalRec );

		// update the main screen
		ogDdx.DataChanged((void *)this, SEC_VIEWER_UPDATE,(void *)prpSecRec->URNO );

		blRc = true;
	}

	return blRc;

} // end UpdSec()


// ----------------------------------------------------------------------------------------------
// DelSec()
//
// Delete a record from SECTAB (CALTAB,GRPTAB,PRVTAB)
//
// pcpUrno		- URNO of the record to be deleted
// pcpErrorMessage	- on RCFail this buffer will contain an error message else blank
//
bool CedaSecData::DelSec( const char *pcpUrno, char *pcpErrorMessage)
{
	bool blRc = true;
	CString olListOfData;
	char pclData[BIGBUF];
	char pclFields[BIGBUF];
	char pclWhere[50];


	strcpy(pcpErrorMessage,""); // no errors yet


	strcpy(pclData,pcpUrno);
	strcpy(pclFields,"URNO");
	strcpy(pclWhere,"WHERE DELSEC");

	// Action,TableName,FieldList,Selection,Sort,Data,Dest (eg "RETURN")
	char pclSort[50] = "";
	blRc = CedaAction(pcgSecCommand,pcgTableExt,pclFields,pclWhere,pclSort,pclData);


	if( ! blRc )
	{
		if( omLastErrorMessage != CString(pclData) )
			strcpy(pcpErrorMessage,omLastErrorMessage);
		else
			sprintf(pcpErrorMessage,"Ceda error <%d> !",blRc);
	}
	else
	{
		// make a copy of the URNO because it is deleted then used to update the screen
		char pclUrno[URNOLEN];
		strcpy(pclUrno,pcpUrno);

		// update the CALTAB,GRPTAB and PRVTAB
		ogDdx.DataChanged((void *)this, SEC_DELETE,(void *) pcpUrno );

		// update local data
		Delete(pcpUrno);

		// update the main screen
		ogDdx.DataChanged((void *)this, SEC_VIEWER_DELETE,(void *) pclUrno );


		blRc = true;
	}

	return blRc;

} // end DelSec()


// ----------------------------------------------------------------------------------------------
// DelApp()
//
// Delete a record from SECTAB (CALTAB,GRPTAB,PRVTAB)
//
// pcpUrno		- URNO of the record to be deleted
// pcpErrorMessage	- on RCFail this buffer will contain an error message else blank
//
bool CedaSecData::DelApp( const char *pcpUrno, char *pcpErrorMessage)
{
	bool blRc = true;
	CString olListOfData;
	char pclData[BIGBUF];
	char pclFields[BIGBUF];
	char pclWhere[50];


	strcpy(pcpErrorMessage,""); // no errors yet


	strcpy(pclData,pcpUrno);
	strcpy(pclFields,"URNO");
	strcpy(pclWhere,"WHERE DELAPP");

	// Action,TableName,FieldList,Selection,Sort,Data,Dest (eg "RETURN")
	char pclSort[50] = "";
	blRc = CedaAction(pcgSecCommand,pcgTableExt,pclFields,pclWhere,pclSort,pclData);


	if( ! blRc )
	{
		if( omLastErrorMessage != CString(pclData) )
			strcpy(pcpErrorMessage,omLastErrorMessage);
		else
			sprintf(pcpErrorMessage,"Ceda error <%d> !",blRc);
	}
	else
	{
		// make a copy of the URNO because it is deleted then used to update the screen
		char pclUrno[URNOLEN];
		strcpy(pclUrno,pcpUrno);

		// update the CALTAB,PRVTAB and FKTTAB
		ogDdx.DataChanged((void *)this, SEC_DELAPP,(void *) pcpUrno );

		// update local data
		Delete(pcpUrno);

		// update the main screen
		ogDdx.DataChanged((void *)this, SEC_VIEWER_DELETE,(void *) pclUrno );


		blRc = true;
	}

	return blRc;

} // end DelApp()


// ----------------------------------------------------------------------------------------------
// SetPwd()
//
// Sets the password for pcpUrno to the default password
//
// pcpUrno - URNO of user
// pcpPass - default password
// pcpErrorMessage - possible error message (on RCFail)
//
// RETURNS RCSuccess or RCFail
//
bool CedaSecData::SetPwd( const char *pcpUrno, const char *pcpPass, char *pcpErrorMessage )
{
	bool blRc = true;
	char pclData[BIGBUF];
	char pclFields[BIGBUF];
	char pclWhere[50];

	strcpy(pcpErrorMessage,""); // no errors yet

	sprintf(pclData,"%s,%s",pcpUrno,pcpPass);
	strcpy(pclFields,"URNO,PASS");
	strcpy(pclWhere,"WHERE SETPWD");

	// Action,TableName,FieldList,Selection,Sort,Data,Dest (eg "RETURN")
	char pclSort[50] = "";
	blRc = CedaAction(pcgSecCommand,pcgTableExt,pclFields,pclWhere,pclSort,pclData);

	if( ! blRc )
	{
		if( omLastErrorMessage != CString(pclData) )
			strcpy(pcpErrorMessage,omLastErrorMessage);
		else
			sprintf(pcpErrorMessage,"Ceda error <%d> !",blRc);
	}


	return blRc;

} // end SetPwd()


// ----------------------------------------------------------------------------------------------
// Insert()
//
// insert a record into local data
//
// prpSec - record containing USID,VPFR,VPTO,STAT,INIT
//
void CedaSecData::Insert( SECDATA  *prpSec)
{

	Add(prpSec);

}




// ----------------------------------------------------------------------------------------------
// Update()
//
// update a record in local data
//
// prpSec - record containing USID,VPFR,VPTO,STAT,INIT
// pcpUSID	on update the USID could have been changed, so this is the original
//
void CedaSecData::Update( SECDATA  *prpSec )
{
	SECDATA* prlSec = GetSecByUrno(prpSec->URNO);

	if( prlSec != NULL )
		*prlSec = *prpSec;	// copy the data

}




// ----------------------------------------------------------------------------------------------
// Delete()
//
// delete a record from local data using pcpUrno as the key
//
void CedaSecData::Delete( const char  *pcpUrno )
{
	bool blNotFound = true;
	int ilCount = omData.GetSize();
	for(int ilLineNo = 0; blNotFound && ilLineNo < ilCount; ilLineNo++)
	{
		if(strcmp(omData[ilLineNo].URNO, pcpUrno) == 0)
		{
			omUrnoMap.RemoveKey(pcpUrno);
			omData.DeleteAt(ilLineNo);
			blNotFound = false;
		}
	}
}


