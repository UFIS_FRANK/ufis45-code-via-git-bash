//
// CedaGrpData.cpp - Read/update relation records from PRVTAB
//
#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#include "stdafx.h"
#include "CCSGlobl.h"
#include "CedaGrpData.h"
#include "CedaCalData.h"
#include "MessList.h"


//
// CedaGrpData()
//
// Constructor
//
CedaGrpData::CedaGrpData()
{
    // Create an array of CEDARECINFO for GRPDATA
    BEGIN_CEDARECINFO(GRPDATA, CedaGrpData)
        FIELD_CHAR_TRIM(URNO,"URNO")
        FIELD_CHAR_TRIM(FREL,"FREL")
        FIELD_CHAR_TRIM(FSEC,"FSEC")
        FIELD_CHAR_TRIM(TYPE,"TYPE")
        FIELD_CHAR_TRIM(STAT,"STAT")
    END_CEDARECINFO

    // Copy the record structure
    for (int i = 0; i < sizeof(CedaGrpData)/sizeof(CedaGrpData[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&CedaGrpData[i],sizeof(CEDARECINFO));
        omRecInfo.Add(prpCedaRecInfo);
	}

    // Initialize table name and field names
    sprintf(pcmTableName,"GRP%s",pcgTableExt);
    pcmFieldList = "URNO,FREL,FSEC,TYPE,STAT";



} // end CedaGrpData()


//
// ~CedaGrpData()
//
// Destructor
//
CedaGrpData::~CedaGrpData(void)
{
	omRecInfo.DeleteAll();
	ClearAll();

} // end ~CedaGrpData()


//
// ClearAll()
//
// Delete omData
//
void CedaGrpData::ClearAll(void)
{
    omData.DeleteAll();
	omUrnoMap.RemoveAll();
	omFsecMap.RemoveAll();
	omFsecTypeMap.RemoveAll();
	omFrelMap.RemoveAll();
	omFrelFsecMap.RemoveAll();

} // end ClearAll()


//
// Read()
//
// Read all relational records from PRVTAB
// ie records that link users to profiles
//
bool CedaGrpData::Read(void)
{
    sprintf(pcmTableName,"GRP%s",pcgTableExt);

	
	ogDdx.Register((void *)this, GRP_INSERT, CString("GRPTABLE"),CString("GRPTAB INSERT"), CedaGrpDataCf);
	ogDdx.Register((void *)this, SEC_DELETE, CString("GRPTABLE"),CString("SECTAB DELETE"), CedaGrpDataCf);

	// the following is another CEDARECINFO structure allowing a different structure
	// to be read in GetBufferRecord() - this must be defined in a different function
	// to the other structure otherwise error !
    // Create an array of CEDARECINFO for RETDATA
    BEGIN_CEDARECINFO(RETDATA, CedaRetData)
        FIELD_CHAR_TRIM(TYPE,"TYPE")
        FIELD_CHAR_TRIM(FLD1,"FLD1")
        FIELD_CHAR_TRIM(FLD2,"FLD2")
        FIELD_CHAR_TRIM(FLD3,"FLD3")
        FIELD_CHAR_TRIM(FLD4,"FLD4")
        FIELD_CHAR_TRIM(FLD5,"FLD5")
        FIELD_CHAR_TRIM(FLD6,"FLD6")
        FIELD_CHAR_TRIM(FLD7,"FLD7")
        FIELD_CHAR_TRIM(FLD8,"FLD8")
    END_CEDARECINFO

    // Copy the record structure
    for (int i = 0; i < sizeof(CedaRetData)/sizeof(CedaRetData[0]); i++)
	{
		CEDARECINFO *prpCedaRetRecInfo = new CEDARECINFO;
		memcpy(prpCedaRetRecInfo,&CedaRetData[i],sizeof(CEDARECINFO));
        omRetRecInfo.Add(prpCedaRetRecInfo);
	}

	if (pogInitialLoad != NULL)
	{
		char pclTmp[200];
		sprintf(pclTmp,GetString(IDS_LOADING_TABLE),pcmTableName);
		pogInitialLoad->SetMessage(pclTmp);
	}

    // Select data from the database
	char pclWhere[512];
	bool blRc = true;
	char pclFields[512];
	strcpy(pclFields,pcmFieldList);
	char pclSort[512] = "";
	char pclData[1024] = "";
	char pclTable[20];
	memset(pclTable,0,20);
	strncpy(pclTable,pcmTableName,6);

	sprintf(pclWhere,"ORDER BY FREL");
	char pclCom[50] = "RT";
//	blRc = CedaAction(pclCom, where);
	blRc = CedaAction(pclCom,pclTable,pclFields,pclWhere,pclSort,pclData);
	if ( ! blRc )
	{
		ogLog.Trace("LOADGRP","Ceda-Error %s (%d) \n",omLastErrorMessage,blRc);

		// it's not an error if no data were found
		if(omLastErrorMessage.Find(ORA_NOT_FOUND) != -1 || omLastErrorMessage == "No Data Found")
			blRc = true;
	}
	else
	{
		// Load data from CedaData into the dynamic array of records
		for (int ilLc = 0; blRc; ilLc++)
		{
			GRPDATA *prpGrp = new GRPDATA;
			if ((blRc = GetBufferRecord(ilLc,prpGrp)) == true)
				Add(prpGrp);
			else
				delete prpGrp;
		}

		int ilSize = omData.GetSize();
		ogLog.Trace("LOADGRP"," %d group records loaded from %s",ilSize,pcmTableName);
		if (pogInitialLoad != NULL)
		{
			char pclBuf[128];
			pogInitialLoad->SetProgress(20);
			sprintf(pclBuf,GetString(IDS_NUM_DB_RECS_READ),ilSize,pcmTableName);
			pogInitialLoad->SetMessage(CString(pclBuf));
			pogInitialLoad->UpdateWindow();
		}

		blRc = true;
	}

	return blRc;


} // end Read()


//
// Add()
//
// Add a relational porfile record to omData
//
// prpGrp - relational profile record
//
bool CedaGrpData::Add(GRPDATA *prpGrp)
{
	omData.Add(prpGrp);
	omUrnoMap.SetAt(prpGrp->URNO,prpGrp);
	omFsecMap.SetAt(prpGrp->FSEC,prpGrp);
	omFrelMap.SetAt(prpGrp->FREL,prpGrp);

	CString olFsecType;
	olFsecType.Format("%s%c",prpGrp->FSEC,prpGrp->TYPE[0]);
	omFsecTypeMap.SetAt(olFsecType,prpGrp);

	CString olFrelFsec;
	olFrelFsec.Format("%s%s",prpGrp->FREL,prpGrp->FSEC);
	omFrelFsecMap.SetAt(olFrelFsec,prpGrp);

    return true;

} // end Add()

static void CedaGrpDataCf(void *popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
    CedaGrpData *polThis = (CedaGrpData *)popInstance;

	switch(ipDDXType) {

	case GRP_INSERT:
		polThis->Insert((GRPDATA *)vpDataPointer);
		break;
	case SEC_DELETE:
		// a user or profile was deleted so delete related group records and their associated cal records
        polThis->DeleteGroups((char *)vpDataPointer);
		break;
	default:
		break;
	}
}


// ----------------------------------------------------------------------------------------------
// NewRel()
//
// Create new relations in GRPTAB,CALTAB
//
// pcpDataField		- one or many FREL,FSEC,TYPE combinations
// pcpErrorMessage	- on RCFail this buffer will contain an error message else blank
//
bool CedaGrpData::NewRel( CCSPtrArray <GRPDATA> &ropGrpRecs, CCSPtrArray <CALDATA> &ropCalRecs, char *pcpErrorMessage)
{
	bool blRc = true;
	char pclFields[BIGBUF];
	char pclWhere[50];


	strcpy(pcpErrorMessage,""); // no errors yet

	// loop through groups adding them to the data list
	CString olDataList, olComma(",");
	olDataList.Empty();
	bool blNotFound;
	int ilGrp, ilCal, ilNumCals, ilNumGrps = ropGrpRecs.GetSize();
	for(ilGrp=0; ilGrp < ilNumGrps; ilGrp++ )
	{
		if(!olDataList.IsEmpty())
			olDataList += olComma;
		olDataList += ropGrpRecs[ilGrp].FREL + olComma + ropGrpRecs[ilGrp].FSEC + olComma + ropGrpRecs[ilGrp].TYPE;
		
		// loop through the CAL recs finding a date to insert along with the grouping
		ilNumCals = ropCalRecs.GetSize();
		blNotFound = true;
		for(ilCal = 0; blNotFound && ilCal < ilNumCals; ilCal++ )
		{
			// the GRP records have no URNO and the CAL recs no FSEC because they are not inserted yet
			// so I have used the URNO of the user/group etc that was in the allocList in the Zuweisen mask !
			if( !strcmp(ropGrpRecs[ilGrp].URNO,ropCalRecs[ilCal].FSEC) )
			{
				olDataList += olComma + ropCalRecs[ilCal].VAFR.Format("%Y%m%d%H%M%S") + olComma + ropCalRecs[ilCal].VATO.Format("%Y%m%d%H%M%S") + olComma + ropCalRecs[ilCal].FREQ;

				// remove the CAL rec from the list - there may be more than one CAL rec per insert
				// but here we just insert one (because the command receives only one CAL rec per
				// allocation), the rest of the CAL inserts are done in Zuweisen::OnOk in a single block
				ropCalRecs.DeleteAt(ilCal);
				// break out of the loop
				blNotFound = false;
			}
		}
	}

	int ilDataFieldLen = max(BIGBUF,olDataList.GetLength()+1);
	char *pclData = new char[ilDataFieldLen];
	memset(pclData,0,ilDataFieldLen);
	strcpy(pclData,olDataList);

	strcpy(pclFields,"FREL,FSEC,TYPE,VAFR,VATO,FREQ");
	strcpy(pclWhere,"WHERE NEWREL");

	// Action,TableName,FieldList,Selection,Sort,Data,Dest (eg "RETURN")
	char pclSort[50] = "";
	blRc = CedaAction(pcgSecCommand,pcgTableExt,pclFields,pclWhere,pclSort,pclData);

	if( ! blRc )
	{
		if( omLastErrorMessage != CString(pclData) )
			strcpy(pcpErrorMessage,omLastErrorMessage);
		else
			sprintf(pcpErrorMessage,"Ceda error <%d> !",blRc);
	}
	else
	{
		// read the results from the data field
		RETDATA rlRetRec;

		for (int ilLc = 0; blRc; ilLc++)
		{
			
			if ((blRc = GetBufferRecord(&omRetRecInfo,ilLc,&rlRetRec)) == true)
			{
				// omRetRecInfo contains return values from CRU
				// these values can be:
				//
				// TYPE,FLD1,FLD2,FLD3,FLD4,FLD5
				// -----------------------------
				//  CAL,URNO,FSEC,VAFR,VATO,FREQ
				//  GRP,URNO,FREL,FSEC,TYPE,STAT

				// AfxMessageBox(CString(rlSecRetRec.TYPE)+CString(",")+CString(rlSecRetRec.FLD1)+CString(",")+CString(rlSecRetRec.FLD2)+CString(",")+CString(rlSecRetRec.FLD3)+CString(",")+CString(rlSecRetRec.FLD4)+CString(",")+CString(rlSecRetRec.FLD5),0);

				if( ! strcmp( rlRetRec.TYPE , "CAL" ) )
				{
					// CAL record received
					CALDATA *prlCalRec = new CALDATA;
					strcpy(prlCalRec->URNO,rlRetRec.FLD1);
					strcpy(prlCalRec->FSEC,rlRetRec.FLD2);
					StoreDate(rlRetRec.FLD3,&prlCalRec->VAFR); // converts a value to CTime
					StoreDate(rlRetRec.FLD4,&prlCalRec->VATO); // converts a value to CTime
					strcpy(prlCalRec->FREQ,rlRetRec.FLD5);
					ogDdx.DataChanged((void *)this, CAL_INSERT,(void *)prlCalRec );
				
				}
				else if( ! strcmp( rlRetRec.TYPE , "GRP" ) )
				{
					// group record recieved
					GRPDATA *prlGrpRec = new GRPDATA;
					strcpy(prlGrpRec->URNO,rlRetRec.FLD1);
					strcpy(prlGrpRec->FREL,rlRetRec.FLD2);
					strcpy(prlGrpRec->FSEC,rlRetRec.FLD3);
					strcpy(prlGrpRec->TYPE,rlRetRec.FLD4);
					strcpy(prlGrpRec->STAT,rlRetRec.FLD5);

					// update the FSEC field of any CAL records that are still to be inserted
					blNotFound = true;
					ilNumGrps = ropGrpRecs.GetSize();
					for(ilGrp=0; blNotFound && ilGrp < ilNumGrps; ilGrp++ )
					{
						if( !strcmp(prlGrpRec->FREL,ropGrpRecs[ilGrp].FREL) && !strcmp(prlGrpRec->FSEC,ropGrpRecs[ilGrp].FSEC) )
						{
							ilNumCals = ropCalRecs.GetSize();
							for(ilCal = 0; ilCal < ilNumCals; ilCal++ )
								if( !strcmp(ropGrpRecs[ilGrp].URNO,ropCalRecs[ilCal].FSEC) )
									strcpy(ropCalRecs[ilCal].FSEC,prlGrpRec->URNO);
							blNotFound = false;
						}
					}

int i = ropCalRecs.GetSize();
					// the user assigned to the profile cannot have a personal profiles
					if(prlGrpRec->TYPE[0] == SEC_PROFILE)
						DeletePersonalProfile(prlGrpRec->FSEC);
i = ropCalRecs.GetSize();

					// insert the new group record
					Insert(prlGrpRec );

					// update the main screen
					if(prlGrpRec->TYPE[0] == SEC_PROFILE) // profile allocation so ...
						ogDdx.DataChanged((void *)this, SEC_VIEWER_UPDATE,(void *) prlGrpRec->FSEC ); // ... update main screeb
				}
			}
		}

		blRc = true;
	}

	if( pclData != NULL )
		delete pclData;
	
	return blRc;

} // end NewRel()



// ----------------------------------------------------------------------------------------------
// DelRel()
//
// Delete relations in GRPTAB,CALTAB
//
// pcpDataField		- one or many FREL,FSEC,TYPE combinations
// pcpErrorMessage	- on RCFail this buffer will contain an error message else blank
//
bool CedaGrpData::DelRel( const char *pcpDataField, char *pcpErrorMessage)
{
	bool blRc = true;
	char pclFields[BIGBUF];
	char pclWhere[50];
	int ilDataFieldLen = strlen(pcpDataField);
	char *pclData = new char[max(BIGBUF,ilDataFieldLen)];

	memset(pclData,0,ilDataFieldLen);
	strncpy(pclData,pcpDataField,ilDataFieldLen-1); // copy except final comma


	strcpy(pcpErrorMessage,""); // no errors yet


	strcpy(pclFields,"FREL,FSEC,TYPE");
	strcpy(pclWhere,"WHERE DELREL");

	// Action,TableName,FieldList,Selection,Sort,Data,Dest (eg "RETURN")
	char pclSort[50] = "";
	blRc = CedaAction(pcgSecCommand,pcgTableExt,pclFields,pclWhere,pclSort,pclData);


	if( ! blRc )
	{
		if( omLastErrorMessage != CString(pclData) )
			strcpy(pcpErrorMessage,omLastErrorMessage);
		else
			sprintf(pcpErrorMessage,"Ceda error <%d> !",blRc);
	}
	else
	{
		// loop through the URNO list deleting local GRP records and their CAL records
		char *pclCopy = new char[ilDataFieldLen+1];
		strcpy(pclCopy,pcpDataField);
		char *pclComma, *pclItem = pclCopy, pclUrno[URNOLEN], pclFsec[URNOLEN];
		GRPDATA *prlGrpRec;
		
		while( (pclComma = strchr(pclItem,',')) != NULL )
		{
			memset(pclUrno,0,sizeof(pclUrno));
			strncpy(pclUrno,pclItem,strlen(pclItem)-strlen(pclComma));
			pclItem = pclComma + sizeof(char);

			prlGrpRec = GetGrpByUrno(pclUrno);
			// prlGrpRec will be NULL if the profile was already deleted in DeletePersonalProfile() as a part of NewRel()
			if( prlGrpRec != NULL )
			{
				strcpy(pclFsec,prlGrpRec->FSEC); // copy FSEC before it is deleted
				char clAllocType = prlGrpRec->TYPE[0]; // copy the allocation type before it is deleted
				ogDdx.DataChanged((void *)this, CAL_DELETE,(void *)pclUrno ); // delete from CALTAB
				DeleteByUrno(pclUrno); // delete from GRPTAB
				if(clAllocType == SEC_PROFILE) // profile deallocation so ...
					ogDdx.DataChanged((void *)this, SEC_VIEWER_UPDATE,(void *)pclFsec ); // ... update main screen
			}
		}


		delete [] pclCopy;
		blRc = true;
	}

	delete [] pclData;
	
	return blRc;

} // end DelRel()




//
// Insert()
//
// Add a record to omData
//
// prpGrp - GRPDATA record to add
//
bool CedaGrpData::Insert(GRPDATA *prpGrp)
{
	omData.Add(prpGrp);
	omUrnoMap.SetAt(prpGrp->URNO,prpGrp);
	omFsecMap.SetAt(prpGrp->FSEC,prpGrp);
	omFrelMap.SetAt(prpGrp->FREL,prpGrp);

	CString olFsecType;
	olFsecType.Format("%s%c",prpGrp->FSEC,prpGrp->TYPE[0]);
	omFsecTypeMap.SetAt(olFsecType,prpGrp);

	CString olFrelFsec;
	olFrelFsec.Format("%s%s",prpGrp->FREL,prpGrp->FSEC);
	omFrelFsecMap.SetAt(olFrelFsec,prpGrp);

    return true;

} // end Insert()


//
// DeletedProfiles()
//
// A SECTAB record was deleted so delete any group records
// whose FREL or FSEC match the URNO and send GRP_DELETE
// so that associated CAL records are deleted
//
// pcpUrno - URNO of a deleted user or profile
//
void CedaGrpData::DeleteGroups( char  *pcpUrno )
{
	GRPDATA *prlGrpRec;

	// delete any FREL records
	while( (prlGrpRec = GetGrpByFrel(pcpUrno)) != NULL )
	{
		// delete record from CALTAB
		ogDdx.DataChanged((void *)this, CAL_DELETE,(void *) prlGrpRec->URNO );

		// delete the relation
		DeleteByUrno(prlGrpRec->URNO);
	}
	
	// delete any FSEC records
	while( (prlGrpRec = GetGrpByFsec(pcpUrno)) != NULL )
	{
		// delete CALTAB
		ogDdx.DataChanged((void *)this, CAL_DELETE,(void *) prlGrpRec->URNO );

		// delete the relation
		DeleteByUrno(prlGrpRec->URNO);
	}
	
} // end DeleteGroups() */

//
// DeletePersonalProfiles()
//
// Each user can only have one relation and only one profile
// so delete any existing relations for the user and if
// found, delete personal profile records from PRVTAB
//
// pcpUrno - FSEC in GRPTAB ie the URNO of a user.
//
void CedaGrpData::DeletePersonalProfile( char  *pcpUrno )
{
	GRPDATA *prlGrpRec;
	char	pclFsec[URNOLEN];


	// delete any relations currently defined for this user
	while( (prlGrpRec = GetGrpByFsecType(pcpUrno,SEC_PROFILE)) != NULL )
	{
		// delete the CAL rec
		ogDdx.DataChanged((void *)this, CAL_DELETE,(void *) prlGrpRec->URNO );

		// if this is a personal profile (FREL=FSEC) then delete the PRV records
		if( !strcmp(prlGrpRec->FREL,prlGrpRec->FSEC ) )
			ogDdx.DataChanged((void *)this, PRV_DELETE,(void *) prlGrpRec->FREL );

		// make a copy of FSEC before it is deleted
		strcpy(pclFsec,prlGrpRec->FSEC);

		// delete the relation
		DeleteByUrno(prlGrpRec->URNO);

		// update the main screen if this is a personal profile
		ogDdx.DataChanged((void *)this, SEC_VIEWER_UPDATE,(void *) pclFsec );
	}
	
	
} // end DeletePersonalProfiles() */


//
// DeleteByUrno()
//
// delete a record from local data using pcpUrno as the key
//
bool CedaGrpData::DeleteByUrno( const char  *pcpUrno )
{
	bool blNotFound = true;
	int ilCount = omData.GetSize();

	for(int ilLineNo = 0; blNotFound && ilLineNo < ilCount; ilLineNo++)
	{
		if(strcmp(omData[ilLineNo].URNO, pcpUrno) == 0)
		{
			omUrnoMap.RemoveKey(omData[ilLineNo].URNO);
			omFsecMap.RemoveKey(omData[ilLineNo].FSEC);
			omFrelMap.RemoveKey(omData[ilLineNo].FREL);

			CString olFsecType;
			olFsecType.Format("%s%c",omData[ilLineNo].FSEC,omData[ilLineNo].TYPE[0]);
			omFsecTypeMap.RemoveKey(olFsecType);

			CString olFrelFsec;
			olFrelFsec.Format("%s%s",omData[ilLineNo].FREL,omData[ilLineNo].FSEC);
			omFrelFsecMap.RemoveKey(olFrelFsec);

			omData.DeleteAt(ilLineNo);

			blNotFound = false;
		}
	}

	return !blNotFound;
}


//
// DeleteByFsec()
//
// delete all records from local data using pcpFsec as the key
//
bool CedaGrpData::DeleteByFsec( const char  *pcpFsec )
{
	bool blNotFound = true;
	int ilCount = omData.GetSize();

	for(int ilLineNo = ilCount-1; ilLineNo >= 0; ilLineNo--)
	{
		if(strcmp(omData[ilLineNo].FSEC, pcpFsec) == 0)
		{
			omUrnoMap.RemoveKey(omData[ilLineNo].URNO);
			omFsecMap.RemoveKey(omData[ilLineNo].FSEC);
			omFrelMap.RemoveKey(omData[ilLineNo].FREL);

			CString olFsecType;
			olFsecType.Format("%s%c",omData[ilLineNo].FSEC,omData[ilLineNo].TYPE[0]);
			omFsecTypeMap.RemoveKey(olFsecType);

			CString olFrelFsec;
			olFrelFsec.Format("%s%s",omData[ilLineNo].FREL,omData[ilLineNo].FSEC);
			omFrelFsecMap.RemoveKey(olFrelFsec);

			omData.DeleteAt(ilLineNo);

			blNotFound = false;
		}
	}

	return !blNotFound;
}


//
// DeleteByFrel()
//
// delete all records from local data using pcpFrel as the key
//
bool CedaGrpData::DeleteByFrel( const char  *pcpFrel )
{
	bool blNotFound = true;
	int ilCount = omData.GetSize();

	for(int ilLineNo = ilCount-1; ilLineNo >= 0; ilLineNo--)
	{
		if(strcmp(omData[ilLineNo].FREL, pcpFrel) == 0)
		{
			omUrnoMap.RemoveKey(omData[ilLineNo].URNO);
			omFsecMap.RemoveKey(omData[ilLineNo].FSEC);
			omFrelMap.RemoveKey(omData[ilLineNo].FREL);

			CString olFsecType;
			olFsecType.Format("%s%c",omData[ilLineNo].FSEC,omData[ilLineNo].TYPE);
			omFsecTypeMap.RemoveKey(olFsecType);

			CString olFrelFsec;
			olFrelFsec.Format("%s%s",omData[ilLineNo].FREL,omData[ilLineNo].FSEC);
			omFrelFsecMap.RemoveKey(olFrelFsec);
			
			omData.DeleteAt(ilLineNo);

			blNotFound = false;
		}
	}

	return !blNotFound;
}

// ----------------------------------------------------------------------------------------------
// GetGrpByUrno()

GRPDATA* CedaGrpData::GetGrpByUrno(const char *pcpUrno)
{
	GRPDATA *prlGrpRec = NULL;
	
	omUrnoMap.Lookup(pcpUrno,(void *&) prlGrpRec);

	return prlGrpRec;

} // end GetGrpByUrno()


// ----------------------------------------------------------------------------------------------
// GetAssigned()
//
// If FREL and FSEC combination is in the GRPTAB return true and the URNO of the record found

bool CedaGrpData::GetAssigned(const char *pcpFrel, const char *pcpFsec, char *pcpUrno)
{
	bool blAssigned = false;

	CString olFrelFsec;
	olFrelFsec.Format("%s%s",pcpFrel,pcpFsec);

	GRPDATA *prlGrpRec = NULL;

	if( omFrelFsecMap.Lookup(olFrelFsec,(void *&) prlGrpRec) )
	{
		strcpy(pcpUrno,prlGrpRec->URNO);
		blAssigned = true;
	}

	return blAssigned;

} // end GetAssigned()


// ----------------------------------------------------------------------------------------------
// GetGrpByFrel()
GRPDATA* CedaGrpData::GetGrpByFrel(const char *pcpFrel)
{
	GRPDATA *prlGrpRec = NULL;
	
	omFrelMap.Lookup(pcpFrel,(void *&) prlGrpRec);

	return prlGrpRec;

} // end GetGrpByFrel()

void CedaGrpData::GetGroupsByFsec(const char *pcpFsec, CCSPtrArray <GRPDATA> &ropGroups, bool bpReset /*true*/)
{
	if(bpReset)
	{
		ropGroups.RemoveAll();
	}

	int ilNumGroups = omData.GetSize();
	for(int ilGroup = 0; ilGroup < ilNumGroups; ilGroup++)
	{
		if(!strcmp(pcpFsec,omData[ilGroup].FSEC))
		{
			ropGroups.Add(&omData[ilGroup]);
		}
	}
}

// ----------------------------------------------------------------------------------------------
// GetGrpByFsec()

GRPDATA* CedaGrpData::GetGrpByFsec(const char *pcpFsec)
{
	GRPDATA *prlGrpRec = NULL;
	
	omFsecMap.Lookup(pcpFsec,(void *&) prlGrpRec);

	return prlGrpRec;

} // end GetGrpByFsec()


// ----------------------------------------------------------------------------------------------
// GetGrpByFsecType()

GRPDATA* CedaGrpData::GetGrpByFsecType(const char *pcpFsec,SEC_REC_TYPE rpType)
{
	GRPDATA *prlGrpRec = NULL;
	CString olFsecType;
	olFsecType.Format("%s%c",pcpFsec,rpType);
	
	omFsecTypeMap.Lookup(olFsecType,(void *&) prlGrpRec);

	return prlGrpRec;

} // end GetGrpByFsecType()

