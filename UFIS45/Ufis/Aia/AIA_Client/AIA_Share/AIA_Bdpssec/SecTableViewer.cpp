// SecTableViewer.cpp : implementation file
//

#include "stdafx.h"
#include "CCSGlobl.h"
#include "BDPS_SEC.h"
#include "SecTableViewer.h"
#include "AllocatedDlg.h"
#include "MessList.h"
#include "CcsPrint.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


// Local function prototype
static void SecTableViewerCf(void *popInstance, int ipDDXType,
					   void *vpDataPointer, CString &ropInstanceName);




//*************************************************************************************
//
// SecTableViewer() - constructor
//
//*************************************************************************************

SecTableViewer::SecTableViewer()
{
    pomTable = NULL;

	ogDdx.Register((void *) this, SEC_VIEWER_INSERT, CString("SECTABLE"),CString("SECTAB INSERT"), SecTableViewerCf);
	ogDdx.Register((void *) this, SEC_VIEWER_UPDATE, CString("SECTABLE"),CString("SECTAB UPDATE"), SecTableViewerCf);
	ogDdx.Register((void *) this, SEC_VIEWER_DELETE, CString("SECTABLE"),CString("SECTAB DELETE"), SecTableViewerCf);

	// set some text to be displayed
	omEnabledMess = GetString(IDS_SECTABLEVIEWER_VALID);
	omDisabledMess = GetString(IDS_SECTABLEVIEWER_INVALID);
	omUnknownMess = GetString(IDS_SECTABLEVIEWER_UNKNOWN);

}


//*************************************************************************************
//
// SecTableViewer() - destructor
//
//*************************************************************************************

SecTableViewer::~SecTableViewer()
{
    DeleteAll();
}


//*************************************************************************************
//
// DeleteAll() - delete all lines (records) read from SECTAB
//
//*************************************************************************************

void SecTableViewer::DeleteAll()
{

	int ilNumLines = omLines.GetSize();

	for(int ilLine=1; ilLine<=ilNumLines; ilLine++)
        DeleteLine(0);
}

//*************************************************************************************
//
// GetSize() - return the number of lines in omLines (and thus number of lines in CCSTable)
//
//*************************************************************************************

int SecTableViewer::GetSize()
{
	return omLines.GetSize();
}

//*************************************************************************************
//
// DeleteLine() - delete a line (record copied from SECTAB) at ipLineno
//
//*************************************************************************************

void SecTableViewer::DeleteLine(int ipLineno)
{
	omLines.DeleteAt(ipLineno);
}

//*************************************************************************************
//
// Attach() - receives a pointer to the CCSTable
//
//*************************************************************************************

void SecTableViewer::Attach(CCSTable *popTable)
{
    pomTable = popTable;
}


//*************************************************************************************
//
// ChangeViewTo() - function responsible for updating the display
//
//*************************************************************************************

void SecTableViewer::ChangeViewTo(void)
{
	if( rgCurrType != SEC_APPL && rgCurrType != SEC_PROFILE )
		bmDisplayProfile = true;
	else
		bmDisplayProfile = false;

	
	// delete all lines defined
	DeleteAll();

	// get lines specified in piViewType from CedaSecData into local memory
	MakeLines();

	// draw the lines loaded in CCSTable
	UpdateDisplay();
}




//*************************************************************************************
//
// IsPassFilter() - rgCurrType='U','P' etc --> used to select either Users,Profiles etc
//
//*************************************************************************************

BOOL SecTableViewer::IsPassFilter(SECDATA  *prpSec)
{
	BOOL blRc = TRUE;

	if(*prpSec->TYPE != rgCurrType)
	{
		blRc = FALSE;
	}
	else if(!bgIsSuperUser && (!strcmp(prpSec->LANG,"1") || (*prpSec->TYPE == SEC_USER && !strcmp(prpSec->USID,ogSuperUser))))
	{
		// only the superuser (UFIS$ADMIN) can display other system administrators and itself
		blRc = FALSE;
	}

	return blRc;
}


//*************************************************************************************
//
// CompareLine() - 
//
//*************************************************************************************

int SecTableViewer::CompareLine(SECTAB_LINEDATA *prpLine1, SECTAB_LINEDATA *prpLine2)
{
	int  ilCompareResult = 0;

	// Implementation of sorting
	return (prpLine1->USID == prpLine2->USID)? 0: (prpLine1->USID >  prpLine2->USID)? 1: -1;

}


/////////////////////////////////////////////////////////////////////////////
// SecTableViewer -- code specific to this class





//*************************************************************************************
//
// MakeLines() - loop through the data held in ogCedaSecData copying to local memory
//
//*************************************************************************************

void SecTableViewer::MakeLines()
{
	int ilCount;
	
	ilCount = ogCedaSecData.omData.GetSize();
	for(int i = 0; i < ilCount; i++)
		MakeLine(&ogCedaSecData.omData[i]);
}

//*************************************************************************************
//
// MakeLine() - copy and process a line of data, create its field and display it
//
//*************************************************************************************

void SecTableViewer::MakeLine(SECDATA  *prpSec)
{

	// filter out either profiles or users or modul
	if( !IsPassFilter(prpSec)) 
		return;

	
	SECTAB_LINEDATA rlLine;

	strcpy(rlLine.URNO,prpSec->URNO);
	rlLine.USID = prpSec->USID;
	rlLine.NAME = prpSec->NAME;
	rlLine.REMA = prpSec->REMA;

	ogCedaCalData.GetDatesByFsec(rlLine.URNO,rlLine.VAFR,rlLine.VATO,rlLine.FREQ);	

	if( strcmp(prpSec->STAT,"1") == 0 )
		rlLine.STAT = omEnabledMess; // if STAT is '1' set text displayed to "Enabled"
	else if( strcmp(prpSec->STAT,"0") == 0 )
		rlLine.STAT = omDisabledMess; // if STAT is '0' set text displayed to "Disabled"
	else
		rlLine.STAT = omUnknownMess; // else set text displayed to "Unknown"

	// for users get any relational profiles
	if( bmDisplayProfile )
	{
		// get the relational record from GRPTAB
		GRPDATA *prlGrpRec = ogCedaGrpData.GetGrpByFsecType(rlLine.URNO,SEC_PROFILE);

		// if not personal profile...
		if( prlGrpRec != NULL && strcmp(prlGrpRec->FREL,prlGrpRec->FSEC) )
		{
			// get the profile name from SECTAB
			SECDATA *prlPrfRec = ogCedaSecData.GetSecByUrno(prlGrpRec->FREL);
			if( prlPrfRec != NULL )
				rlLine.PROF = prlPrfRec->USID;
		}
	}

	if(!strcmp(prpSec->LANG,"1") || !strcmp(prpSec->USID,ogSuperUser))
	{
		rlLine.IsSysAdmin = true;
	}
	else
	{
		rlLine.IsSysAdmin = false;
	}

	CreateLine(&rlLine);
}



//*************************************************************************************
//
// CreateLine() - add the line to omLines (array of pointers to each line)
//
//*************************************************************************************

int SecTableViewer::CreateLine(SECTAB_LINEDATA *prpSec)
{
    int ilLineCount = omLines.GetSize();

    for (int ilLineno = 0; ilLineno < ilLineCount; ilLineno++)
        if (CompareLine(prpSec, &omLines[ilLineno]) <= 0)
            break;  

	SECTAB_LINEDATA  rlLine;
    rlLine = *prpSec;
	omLines.NewAt(ilLineno, rlLine);

	return ilLineCount;
}




/////////////////////////////////////////////////////////////////////////////
// SecTableViewer - display drawing routines



//*************************************************************************************
//
// UpdateDisplay() - write local SECTAB data to the CCSTable
// returns false if there are no lines to display, else true
//
//*************************************************************************************

void SecTableViewer::UpdateDisplay()
{
	CString		olTitle1;
	CString		olNoDataMsg;

	// set the header and dialog caption
	switch(rgCurrType)
	{
		case SEC_PROFILE:
			olTitle1 = GetString(IDS_SECTABLEVIEWER_PROFILENAME);
			olNoDataMsg = GetString(IDS_SECTABLEVIEWER_NOPROFILES);
			break;
		case SEC_APPL:
			olTitle1 = GetString(IDS_SECTABLEVIEWER_MODULENAME);
			olNoDataMsg = GetString(IDS_SECTABLEVIEWER_NOMODULES);
			break;
		case SEC_GROUP:
			olTitle1 = GetString(IDS_SECTABLEVIEWER_GROUPNAME);
			olNoDataMsg = GetString(IDS_SECTABLEVIEWER_NOGROUPS);
			break;
		case SEC_WKS:
			olTitle1 = GetString(IDS_SECTABLEVIEWER_WKSNAME);
			olNoDataMsg = GetString(IDS_SECTABLEVIEWER_NOWKS);
			break;
		case SEC_WKSGROUP:
			olTitle1 = GetString(IDS_SECTABLEVIEWER_WKSGROUPNAME);
			olNoDataMsg = GetString(IDS_SECTABLEVIEWER_NOWKSGROUPS);
			break;
		case SEC_USER:
		default:
			olTitle1 = GetString(IDS_SECTABLEVIEWER_USERNAME);
			olNoDataMsg = GetString(IDS_SECTABLEVIEWER_NOUSERS);
	}
	// step 1 set table header
	pomTable->ResetContent();
	CCSPtrArray <TABLE_HEADER_COLUMN> omHeaderDataArray;

	pomTable->SetShowSelection(TRUE);
	TABLE_HEADER_COLUMN rlHeader;
	rlHeader.Alignment = COLALIGN_LEFT;
	rlHeader.Font = &ogCourier_Bold_10;

	// USID header
	rlHeader.Length = USIDCOLUMNWIDTH; 
	rlHeader.Text = olTitle1;
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);

	// NAME header
	rlHeader.Length = FULLNAMECOLUMNWIDTH; 
	rlHeader.Text = GetString(IDS_SECTABLEVIEWER_NAME);
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);

	// STAT header
	rlHeader.Length = STATCOLUMNWIDTH; 
	rlHeader.Text = GetString(IDS_SECTABLEVIEWER_STATUS);
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);

	// VAFR header
	rlHeader.Length = VAFRCOLUMNWIDTH; 
	rlHeader.Text = GetString(IDS_SECTABLEVIEWER_VALIDFROM);
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);

	// VATO header
	rlHeader.Length = VATOCOLUMNWIDTH; 
	rlHeader.Text = GetString(IDS_SECTABLEVIEWER_VALIDTO);
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);

	// VATO header
	rlHeader.Length = FREQCOLUMNWIDTH; 
	rlHeader.Text = GetString(IDS_SECTABLEVIEWER_FREQ);
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);


	if( bmDisplayProfile )
	{
		// PROF header
		rlHeader.Length = PROFCOLUMNWIDTH; 
		rlHeader.Text = GetString(IDS_SECTABLEVIEWER_PROFILEHEADER);
		omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	}

	// REMA header
	rlHeader.Length = REMACOLUMNWIDTH; 
	rlHeader.Text = GetString(IDS_SECTABLEVIEWER_REMARK);
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);

	pomTable->SetHeaderFields(omHeaderDataArray);
	omHeaderDataArray.RemoveAll();
	
	pomTable->SetDefaultSeparator();
	
   // step 2 set linedata
	CCSPtrArray<TABLE_COLUMN> olColList;

	TABLE_COLUMN rlColumnData;
	rlColumnData.VerticalSeparator = SEPA_NONE;
	rlColumnData.SeparatorType = SEPA_NONE;
	rlColumnData.Font = &ogCourier_Regular_10;//&ogMSSansSerif_Bold_8;//&ogCourier_Regular_10;
	rlColumnData.Alignment = COLALIGN_LEFT;


	int ilNumLines = omLines.GetSize();
	for (int ilLc = 0; ilLc < ilNumLines; ilLc++)
	{
		SECTAB_LINEDATA *prlLine = &omLines[ilLc];

		if(prlLine->IsSysAdmin)
		{
			rlColumnData.BkColor = AQUA;
		}
		else
		{
			rlColumnData.BkColor = WHITE;
		}

		rlColumnData.Text = prlLine->USID;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		rlColumnData.Text = prlLine->NAME;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		rlColumnData.Text = prlLine->STAT;;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		rlColumnData.Text = prlLine->VAFR;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		rlColumnData.Text = prlLine->VATO;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		rlColumnData.Text = prlLine->FREQ;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		if( bmDisplayProfile ) {
			rlColumnData.Text = prlLine->PROF;
			olColList.NewAt(olColList.GetSize(), rlColumnData);
		}

		rlColumnData.Text = prlLine->REMA;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		pomTable->AddTextLine(olColList, (void*)prlLine);

		// disable drag&drop for each line
		pomTable->SetTextLineDragEnable(ilLc,FALSE);

		olColList.DeleteAll();
	}

	pomTable->DisplayTable();


	// if there are data then select the first line, else display a message
	if( ilNumLines > 0 )
		pomTable->SelectLine(0);
	else
	{
		// if the alloc dialog is open and there are no more records then close it
		if( pogAllocDlg != NULL )
			pogAllocDlg->SendCancel();

		pomTable->MessageBox(olNoDataMsg,"",MB_ICONINFORMATION);
	}

}

int SecTableViewer::FindLine(const char *pcpUrno)
{
	int ilLine = -1;

	int ilCount = omLines.GetSize();
	for(int ilLc = 0; ilLine == -1 && ilLc < ilCount; ilLc++)
	{
		if(!strcmp(omLines[ilLc].URNO,pcpUrno))
		{
			ilLine = ilLc;
		}
	}
	return ilLine;
}


static void SecTableViewerCf(void *popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
    SecTableViewer *polThis = (SecTableViewer *)popInstance;

	switch(ipDDXType) {

	case SEC_VIEWER_INSERT:
		polThis->Insert((char *)vpDataPointer);
		break;
	case SEC_VIEWER_UPDATE:
        polThis->Update((char *)vpDataPointer);
		break;
	case SEC_VIEWER_DELETE:
        polThis->Delete((char *)vpDataPointer);
		break;
	default:
		break;
	}
}


void SecTableViewer::SelectLine(const char *pcpUrno)
{
	int ilLine = FindLine(pcpUrno);

	// select the line indexed by pcpUsid
	if(ilLine -1)
	{
		pomTable->SelectLine(ilLine);
	}
}


void SecTableViewer::Update(char *pcpUrno)
{
	int ilSelLine = pomTable->GetCurrentLine();
	
	int ilLine = FindLine(pcpUrno);
	bool blRc = true;

	if(ilLine != -1)
	{
		// get the SEC rec
		SECDATA *prlSecRec;
		prlSecRec = ogCedaSecData.GetSecByUrno(pcpUrno);

		if( prlSecRec != NULL )
		{
			SECTAB_LINEDATA *prlLine = &omLines[ilLine];

			// read the current line characteristics
			CCSPtrArray <TABLE_COLUMN> rlLine;
			pomTable->GetTextLineColumns(&rlLine, ilLine);

			// update columns 0 to 4
			rlLine[0].Text = prlSecRec->USID;
			prlLine->USID = prlSecRec->USID;
			rlLine[1].Text = prlSecRec->NAME;
			prlLine->NAME = prlSecRec->NAME;


			// get the celendar dates
			ogCedaCalData.GetDatesByFsec(pcpUrno,rlLine[3].Text,rlLine[4].Text,rlLine[5].Text);	
			prlLine->VAFR = rlLine[3].Text;
			prlLine->VATO = rlLine[4].Text;
			prlLine->FREQ = rlLine[5].Text;

			COLORREF rlBkColor;
			if(!strcmp(prlSecRec->LANG,"1") || !strcmp(prlSecRec->USID,ogSuperUser))
			{
				rlBkColor = AQUA;
				prlLine->IsSysAdmin = true;
			}
			else
			{
				rlBkColor = WHITE;
				prlLine->IsSysAdmin = false;
			}
			int ilNumCols = rlLine.GetSize();
			for(int ilCol = 0; ilCol < ilNumCols; ilCol++)
			{
				rlLine[ilCol].BkColor = rlBkColor;
			}

			// update column 3 (STAT)
			if( strcmp(prlSecRec->STAT,"1") == 0 )
				rlLine[2].Text = omEnabledMess; // if STAT is '1' set text displayed to "Enabled"
			else if( strcmp(prlSecRec->STAT,"0") == 0 )
				rlLine[2].Text = omDisabledMess; // if STAT is '0' set text displayed to "Disabled"
			else
				rlLine[2].Text = omUnknownMess; // else set text displayed to "Unknown"

			prlLine->STAT = rlLine[2].Text;

			if( bmDisplayProfile )
			{
				rlLine[6].Text = "";
				GRPDATA *prlGrpRec = ogCedaGrpData.GetGrpByFsecType(pcpUrno,SEC_PROFILE);

				// if a user has a non-personal profile, update the profile field
				if( prlGrpRec != NULL && strcmp(prlGrpRec->FREL,prlGrpRec->FSEC))
				{
					SECDATA *prlPrfRec = ogCedaSecData.GetSecByUrno(prlGrpRec->FREL);
					if( prlPrfRec != NULL )
						rlLine[6].Text = prlPrfRec->USID;
				}
				rlLine[7].Text = prlSecRec->REMA;

				prlLine->PROF = rlLine[6].Text;
				prlLine->REMA = rlLine[7].Text;
			}
			else
			{
				rlLine[6].Text = prlSecRec->REMA;
				prlLine->REMA = rlLine[6].Text;
			}


			// refresh the table
			pomTable->DisplayTable();
		}
	}

	// reselect the originally selected line
	//SelectLine(pcpUrno);
//	pomTable->SelectLine(ilSelLine);


}

void SecTableViewer::Insert(char *pcpUrno)
{
	// update the table
	ChangeViewTo();

	// select the line of the newly inserted USID
	SelectLine(pcpUrno);

}

void SecTableViewer::Delete(char *pcpUrno)
{
	int ilLine = FindLine(pcpUrno);

	// delete the line from local mem
	if(ilLine != -1)
	{
		DeleteLine(ilLine);
	}

	// update the table
	ChangeViewTo();

	int ilNumLines = omLines.GetSize();

	// reselect the line
	if( ilNumLines > 0 )
	{
		if(ilLine < ilNumLines)
			pomTable->SelectLine(ilLine);
		else
			pomTable->SelectLine(ilLine-1);
	}
}

void SecTableViewer::Print(CString opCaption, CWnd *popParent)
{
	CString olFooter1,olFooter2;
	int ilOrientation = PRINT_LANDSCAPE;//  PRINT_PORTRAET  PRINT_LANDSCAPE

	pomPrint = NULL;
	pomPrint = new CCSPrint(popParent,ilOrientation,45);// FirstLine(P=200,L=200),LeftOffset(P=200,L=50)

	if (pomPrint != NULL)
	{
		if (pomPrint->InitializePrinter(&ilOrientation) == TRUE)
		{
			int ilLinesPerPage = ilOrientation == PRINT_PORTRAET ? 57 : 38;

			pomPrint->imLineNo = ilLinesPerPage + 1;
			DOCINFO	rlDocInfo;
			memset(&rlDocInfo, 0, sizeof(DOCINFO));
			rlDocInfo.cbSize = sizeof( DOCINFO );
			rlDocInfo.lpszDocName = opCaption;	
			pomPrint->omCdc.StartDoc( &rlDocInfo );
			pomPrint->imPageNo = 0;

			int ilLines = 	omLines.GetSize();
			int ilTotalPages = (((int) ilLines-1) / (int) ilLinesPerPage) + 1;
			int ilPageCount = 1;

			pomPrint->imLineNo = 0;
			olFooter1 = CTime::GetCurrentTime().Format("%d.%m.%Y %H:%M");
			for(int ilCurrLine = 0; ilCurrLine < ilLines; ilCurrLine++ ) 
			{
				if(pomPrint->imLineNo >= ilLinesPerPage)
				{
					// end of page reached
					olFooter2.Format("%d/%d",ilPageCount++,ilTotalPages);
					pomPrint->PrintUIFFooter(olFooter1,"",olFooter2);
					pomPrint->omCdc.EndPage();
					pomPrint->imLineNo = 0;
				}

				if( pomPrint->imLineNo == 0 )
				{
					// print the header at the start and when the page has changed
					pomPrint->PrintUIFHeader(opCaption,"",pomPrint->imFirstLine-10);
				}

				// print a line
				PrintLine(&omLines[ilCurrLine], ilOrientation);
			}

			olFooter2.Format("%d/%d",ilPageCount++,ilTotalPages);
			pomPrint->PrintUIFFooter(olFooter1,"",olFooter2);
			pomPrint->omCdc.EndPage();
			pomPrint->omCdc.EndDoc();
		}

		delete pomPrint;
		pomPrint = NULL;
	}

	AfxGetApp()->DoWaitCursor(1);
}

bool SecTableViewer::PrintLine(SECTAB_LINEDATA *prpLine, int ipOrientation)
{
	bool blRc = false;

	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;
	int ilNumChars;

	rlElement.pFont      = &pomPrint->ogCourierNew_Regular_8;
	rlElement.Alignment  =  PRINT_LEFT;
	if(prpLine->IsSysAdmin)
	{
		rlElement.FrameTop   =  PRINT_FRAMETHICK;
		rlElement.FrameBottom = PRINT_FRAMETHICK;
		rlElement.FrameLeft  =  PRINT_FRAMETHICK;
		rlElement.FrameRight =  PRINT_FRAMETHICK;
	}
	else
	{
		rlElement.FrameTop   =  PRINT_FRAMETHIN;
		rlElement.FrameBottom = PRINT_FRAMETHIN;
		rlElement.FrameLeft  =  PRINT_FRAMETHIN;
		rlElement.FrameRight =  PRINT_FRAMETHIN;
	}

	ilNumChars = ipOrientation == PRINT_LANDSCAPE ? 40 : 35;
	rlElement.Length = (int)((ilNumChars*5)*dgCCSPrintFactor);
	rlElement.Text = CString(prpLine->USID);
	rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

	if(ipOrientation == PRINT_LANDSCAPE)
	{
		ilNumChars = 65;
		rlElement.Length = (int)((ilNumChars*5)*dgCCSPrintFactor);
		rlElement.Text = CString(prpLine->NAME);
		rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
	}

	ilNumChars = 20;
	rlElement.Length = (int)((ilNumChars*5)*dgCCSPrintFactor);
	rlElement.Text = CString(prpLine->STAT);
	rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

	ilNumChars = 30;
	rlElement.Length = (int)((ilNumChars*5)*dgCCSPrintFactor);
	rlElement.Text = CString(prpLine->VAFR);
	rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

	ilNumChars = 30;
	rlElement.Length = (int)((ilNumChars*5)*dgCCSPrintFactor);
	rlElement.Text = CString(prpLine->VATO);
	rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

	ilNumChars = 16;
	rlElement.Length = (int)((ilNumChars*5)*dgCCSPrintFactor);
	rlElement.Text = CString(prpLine->FREQ);
	rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

	ilNumChars = ipOrientation == PRINT_LANDSCAPE ? 40 : 35;
	rlElement.Length = (int)((ilNumChars*5)*dgCCSPrintFactor);
	rlElement.Text = CString(prpLine->PROF);
	rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);


	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();

	blRc = true;

	return blRc;
}
