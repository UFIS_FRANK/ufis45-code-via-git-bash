// CalTableViewer.cpp : implementation file
//
#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#include "stdafx.h"
#include "CCSGlobl.h"
#include "BDPS_SEC.h"
#include "CalTableViewer.h"
#include "CedaCalData.h"
#include "MessList.h"


#ifdef _DEBUG
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif




//*************************************************************************************
//
// CalTableViewer() - constructor
//
//*************************************************************************************

CalTableViewer::CalTableViewer()
{
	// init vars
    DeleteAll();
	omFsecList.Empty();
	bmAllocOnly = false; // show allocated objects only
 
}


//*************************************************************************************
//
// CalTableViewer() - destructor
//
//*************************************************************************************

CalTableViewer::~CalTableViewer()
{
    DeleteAll();
}


//*************************************************************************************
//
// DeleteAll() - delete all lines (records) read from SECTAB
//
//*************************************************************************************

void CalTableViewer::DeleteAll()
{

	int ilNumLines = omLines.GetSize();

	for(int ilLine=1; ilLine<=ilNumLines; ilLine++)
        DeleteLine(0);

	omDeletedList.RemoveAll();
	omUpdatedList.DeleteAll();
	omInsertedList.DeleteAll();
}

//*************************************************************************************
//
// GetSize() - return the number of lines in omLines (and thus number of lines in CCSTable)
//
//*************************************************************************************

int CalTableViewer::GetSize()
{
	return omLines.GetSize();
}

//*************************************************************************************
//
// DeleteLine() - delete a line (record copied from SECTAB) at ipLineno
//
//*************************************************************************************

void CalTableViewer::DeleteLine(int ipLineno)
{
	
	omLines.DeleteAt(ipLineno);
}

//*************************************************************************************
//
// Attach() - receives a pointer to the CCSTable
//
//*************************************************************************************

void CalTableViewer::Attach(CCSTable *popTable)
{
    pomTable = popTable;
}


//*************************************************************************************
//
// ChangeViewTo() - function responsible for updating the display
//
//*************************************************************************************

void CalTableViewer::ChangeViewTo(void)
{
	// delete all lines defined
	DeleteAll();

	// get lines specified in piViewType from CedaSecData into local memory
	MakeLines();

	// draw the lines loaded in CCSTable
//	UpdateDisplay();
}




//*************************************************************************************
//
// IsPassFilter() - SEC_APPL = 'A' - select applications only
// omFsecList - list of URNOs in the format "|URNO|URNO|URNO|...|"
//
//*************************************************************************************

BOOL CalTableViewer::IsPassFilter(const char *pcpFsec)
{
	return strstr(omFsecList,pcpFsec) == NULL ? false : true;

}


//*************************************************************************************
//
// CompareLine() - 
//
//*************************************************************************************

int CalTableViewer::CompareLine(CALTAB_LINEDATA *prpLine1, CALTAB_LINEDATA *prpLine2)
{
	return prpLine1->VAFR > prpLine2->VAFR ? 1 : 0; // ordered by start date
}


/////////////////////////////////////////////////////////////////////////////
// CalTableViewer -- code specific to this class





//*************************************************************************************
//
// MakeLines() - loop through the data held in ogCedaSecData copying to local memory
//
//*************************************************************************************

void CalTableViewer::MakeLines()
{
	int ilCount;
	
	ilCount = ogCedaCalData.omData.GetSize();
	for(int i = 0; i < ilCount; i++)
		MakeLine(&ogCedaCalData.omData[i]);
}

//*************************************************************************************
//
// MakeLine() - copy and process a line of data, create its field and display it
//
//*************************************************************************************

void CalTableViewer::MakeLine(CALDATA  *prpCal)
{

	// filter out either profiles or users or modul
	if( !IsPassFilter(prpCal->FSEC) ) 
		return;

	
	CALTAB_LINEDATA rlLine;

	strcpy(rlLine.URNO,prpCal->URNO);
	strcpy(rlLine.FSEC,prpCal->FSEC);
	rlLine.VAFR = prpCal->VAFR;
	rlLine.VATO = prpCal->VATO;
	strcpy(rlLine.FREQ,prpCal->FREQ);

	rlLine.stat = DATA_UNCHANGED;

	
	CreateLine(&rlLine);
}



//*************************************************************************************
//
// CreateLine() - add the line to omLines (array of pointers to each line)
//
//*************************************************************************************

int CalTableViewer::CreateLine(CALTAB_LINEDATA *prpNewLine)
{
    int ilLineCount = omLines.GetSize();

    for (int ilLineno = 0; ilLineno < ilLineCount; ilLineno++)
        if (CompareLine(prpNewLine, &omLines[ilLineno]) <= 0)
            break;  

	CALTAB_LINEDATA  rlLine;
    rlLine = *prpNewLine;
	omLines.NewAt(ilLineno, rlLine);

	return ilLineCount;
}




/////////////////////////////////////////////////////////////////////////////
// CalTableViewer - display drawing routines



//*************************************************************************************
//
// UpdateDisplay() - write local SECTAB data to the CCSTable
//
//*************************************************************************************

void CalTableViewer::UpdateDisplay(int *pipInsertedLineNum)
{
	// step 1 set table header
	pomTable->ResetContent();
	CCSPtrArray <TABLE_HEADER_COLUMN> omHeaderDataArray;

	pomTable->SetShowSelection(true);
	TABLE_HEADER_COLUMN rlHeader;
	rlHeader.Alignment = COLALIGN_LEFT;
	rlHeader.Font = &ogCourier_Bold_10;

	// VAFR header
	rlHeader.Length = 130; // ???
	rlHeader.Text = GetString(IDS_CALTABLEVIEWER_VALIDFROM);
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);

	// VATO header
	rlHeader.Length = 130; // ???
	rlHeader.Text = GetString(IDS_CALTABLEVIEWER_VALIDTO);
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);

	// FREQ header
	rlHeader.Length = 80; // ???
	rlHeader.Text = GetString(IDS_CALTABLEVIEWER_FREQ);
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);


	pomTable->SetHeaderFields(omHeaderDataArray);
	omHeaderDataArray.RemoveAll();
	
	
   // step 2 set linedata
	CCSPtrArray <TABLE_COLUMN> olColList;

	TABLE_COLUMN rlColumnData;
	rlColumnData.VerticalSeparator = SEPA_NONE;
	rlColumnData.SeparatorType = SEPA_NONE;
	rlColumnData.Font = &ogCourier_Regular_10;//&ogMSSansSerif_Bold_8;//&ogCourier_Regular_10;
	rlColumnData.Alignment = COLALIGN_LEFT;


	// no lines displayed yet
	omDisplayedLines.DeleteAll();

	int ilNumLines = omLines.GetSize();
	int ilNumAlloc = 0;
	for (int ilLc = 0; ilLc < ilNumLines; ilLc++)
	{
		CALTAB_LINEDATA *prlLine = &omLines[ilLc];

		// there can be multiple calendar dates in omLines, when only one
		// of these is selected, then display by FSEC
//		if( omSelectedFsec.IsEmpty() || !strcmp(prlLine->FSEC,omSelectedFsec) )
		if( !strcmp(prlLine->FSEC,omSelectedFsec) )
		{
//			// return the line number of an inserted line
//			if( pipInsertedLineNum != NULL && ilLc == *pipInsertedLineNum )
//				*pipInsertedLineNum = imNumCalFieldsFound;
			
			rlColumnData.Text = prlLine->VAFR.Format("%d.%m.%Y %H:%M");
			olColList.NewAt(olColList.GetSize(), rlColumnData);

			rlColumnData.Text = prlLine->VATO.Format("%d.%m.%Y %H:%M");
			olColList.NewAt(olColList.GetSize(), rlColumnData);

			rlColumnData.Text = prlLine->FREQ;
			olColList.NewAt(olColList.GetSize(), rlColumnData);

			pomTable->AddTextLine(olColList, (void*)prlLine);

			// disable drag&drop for each line
			pomTable->SetTextLineDragEnable(GetNumLinesDisplayed(),FALSE);

			// indicies into omLines of the lines displayed
			int *pilLineAdded = new int;
			if( bmAllocOnly )
				*pilLineAdded = ilNumAlloc++;
			else
				*pilLineAdded = ilLc;

			omDisplayedLines.Add((void *)pilLineAdded);

			olColList.DeleteAll();
		}
	}

	pomTable->DisplayTable();


}

int CalTableViewer::GetLineNum(const int ipLine)
{
	int pilLine;

	if(omDisplayedLines.GetSize() > 0)
		pilLine = omDisplayedLines[ipLine];
	else
		pilLine = -1;

	return pilLine;

}

int CalTableViewer::GetNumLinesDisplayed(void)
{
	return omDisplayedLines.GetSize();
}

void CalTableViewer::Delete(const int ipLine)
{
	int ilLine = GetLineNum(ipLine);

	if( ilLine >= 0 )
	{
		if( omLines[ilLine].stat != DATA_NEW ) // if this was new insert, then don't delete it from DB
		{
			omDeletedList.Add(ogCedaCalData.GetCalByUrno(omLines[ilLine].URNO));
		}

		omLines.DeleteAt(ilLine);

		// refresh the table
		UpdateDisplay();
	}
}


// returns the line num of the inserted record
int CalTableViewer::Insert(const char *pcpVafrDate, const char *pcpVafrTime,
							const char *pcpVatoDate, const char *pcpVatoTime,
							const char *pcpFreq, const char *pcpFsec)
{
	CALTAB_LINEDATA rlLine;

	strcpy(rlLine.URNO,"");
	strcpy(rlLine.FSEC,pcpFsec);
	rlLine.VAFR = DateHourStringToDate(CString(pcpVafrDate),CString(pcpVafrTime));
	rlLine.VATO = DateHourStringToDate(CString(pcpVatoDate),CString(pcpVatoTime));
	strcpy(rlLine.FREQ,pcpFreq);

	rlLine.stat = DATA_NEW;

	
	int ilNewLine = CreateLine(&rlLine);

	// refresh the table
	UpdateDisplay(&ilNewLine);

	// search for the newly inserted line and return its line number, need to search the CCSTable because when a
	// profile is selected with many users allocated, there are more records in omLines than displayed in the table

	int ilNumLines = omLines.GetSize();
	ilNewLine = -1;
	for( int ilLine = 0; ilLine < ilNumLines && ilNewLine == -1; ilLine++)
	{
		if(omLines[ilLine].VAFR.GetTime() == rlLine.VAFR.GetTime() && omLines[ilLine].VATO.GetTime() == rlLine.VATO.GetTime() && !strcmp(omLines[ilLine].FREQ,rlLine.FREQ))
			ilNewLine = ilLine;
	}
	/*
	int ilNumLines = pomTable->omLines.GetSize();
	ilNewLine = -1;
	CCSPtrArray <TABLE_COLUMN> rlTabLine;
	CString olVafr = rlLine.VAFR.Format("%d.%m.%Y %H:%M"), olVato = rlLine.VATO.Format("%d.%m.%Y %H:%M"), olFreq = rlLine.FREQ;
	for( int ilLine = 0; ilLine < ilNumLines && ilNewLine == -1; ilLine++)
	{
		pomTable->GetTextLineColumns(&rlTabLine, ilLine);

		if( rlTabLine[0].Text == olVafr && rlTabLine[1].Text == olVato && rlTabLine[2].Text == olFreq )
			ilNewLine = ilLine;
	}
	*/
	if(ilNewLine < 0 || ilNewLine >= pomTable->omLines.GetSize())
		ilNewLine = 0;

	return ilNewLine;
}

void CalTableViewer::Update(const char *pcpVafrDate, const char *pcpVafrTime,
							const char *pcpVatoDate, const char *pcpVatoTime,
							const char *pcpFreq, int ipLine )
{
	int ilLine = GetLineNum(ipLine);

	if( ilLine >= 0 )
	{
		CTime olVafr = DateHourStringToDate(CString(pcpVafrDate),CString(pcpVafrTime));
		CTime olVato = DateHourStringToDate(CString(pcpVatoDate),CString(pcpVatoTime));
		bool blUpdated = false;
		
		if(olVafr != omLines[ilLine].VAFR)
		{
			omLines[ilLine].VAFR = olVafr;
			blUpdated = true;
		}

		if(olVato != omLines[ilLine].VATO)
		{
			omLines[ilLine].VATO = olVato;
			blUpdated = true;
		}

		// ??????
		if(strcmp(omLines[ilLine].FREQ,pcpFreq))
		{
			strcpy(omLines[ilLine].FREQ,pcpFreq);
			blUpdated = true;
		}

		if( blUpdated )
		{
			if(omLines[ilLine].stat == DATA_UNCHANGED)
				omLines[ilLine].stat = DATA_CHANGED;

			CCSPtrArray <TABLE_COLUMN> rlLine;
			pomTable->GetTextLineColumns(&rlLine, ipLine);
			rlLine[0].Text = omLines[ilLine].VAFR.Format("%d.%m.%Y %H:%M");
			rlLine[1].Text = omLines[ilLine].VATO.Format("%d.%m.%Y %H:%M");
			rlLine[2].Text = omLines[ilLine].FREQ;

			// refresh the table
			pomTable->DisplayTable();

			// select the line of the newly updated USID
			pomTable->SelectLine(ipLine);
		}
	}

}


bool CalTableViewer::SelectLine(char *pcpVafrDate, char *pcpVafrTime,
								char *pcpVatoDate, char *pcpVatoTime,
								char *pcpFreq, int ipLine )
{
	bool blRc = false;

	if( ipLine >= 0 && ipLine < omDisplayedLines.GetSize() )
	{
		int ilLine = GetLineNum(ipLine);

		pomTable->SelectLine(ipLine);

		strcpy(pcpVafrDate,omLines[ilLine].VAFR.Format("%d.%m.%Y"));
		strcpy(pcpVafrTime,omLines[ilLine].VAFR.Format("%H:%M"));
		strcpy(pcpVatoDate,omLines[ilLine].VATO.Format("%d.%m.%Y"));
		strcpy(pcpVatoTime,omLines[ilLine].VATO.Format("%H:%M"));
		strcpy(pcpFreq,omLines[ilLine].FREQ);
		blRc = true;
	}

	return blRc;
}


void CalTableViewer::CreateCommands(void)
{
	int ilNumDates = omLines.GetSize();
	omUpdatedList.DeleteAll();
	omInsertedList.DeleteAll();

	for( int ilDate=0; ilDate < ilNumDates; ilDate++)
	{
		if(omLines[ilDate].stat == DATA_CHANGED)
		{
			CALDATA *prlCal = new CALDATA;
			strcpy(prlCal->URNO,omLines[ilDate].URNO);
			strcpy(prlCal->FSEC,omLines[ilDate].FSEC);
			prlCal->VAFR = omLines[ilDate].VAFR;
			prlCal->VATO = omLines[ilDate].VATO;
			strcpy(prlCal->FREQ,omLines[ilDate].FREQ);
			omUpdatedList.Add(prlCal);
		}
		else if(omLines[ilDate].stat == DATA_NEW)
		{
			CALDATA *prlCal = new CALDATA;
			strcpy(prlCal->FSEC,omLines[ilDate].FSEC);
			prlCal->VAFR = omLines[ilDate].VAFR;
			prlCal->VATO = omLines[ilDate].VATO;
			strcpy(prlCal->FREQ,omLines[ilDate].FREQ);
			omInsertedList.Add(prlCal);
		}
		else
		{
		}
	}
}