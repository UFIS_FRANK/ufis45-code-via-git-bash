// CBDPS_SECApp.cpp : Defines the class behaviors for the application.
//
#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#include "stdafx.h"
#include "BDPS_SEC.h"
#include "CCSGlobl.h"
#include "LoginDlg.h"
#include "ButtonListDlg.h"
#include "InitialLoadDlg.h"
#include "RegisterDlg.h"

#include "CedaSecData.h"
#include "CedaCalData.h"
#include "CedaGrpData.h"
#include "CedaPrvData.h"
#include "CedaFktData.h"

#include "MessList.h"
#include "ObjList.h"
#include "AboutSecDlg.h"


/////////////////////////////////////////////////////////////////////////////

//

// CBDPS_SECApp

BEGIN_MESSAGE_MAP(CBDPS_SECApp, CWinApp)
	//{{AFX_MSG_MAP(CBDPS_SECApp)
	//}}AFX_MSG_MAP
	ON_COMMAND(ID_HELP, OnHelpContents)
	ON_COMMAND(IDD_HELP_CONTENTS, OnHelpContents)
//	ON_COMMAND(IDD_HELP_INDEX, OnHelpIndex)
	ON_COMMAND(IDD_HELP_USING, CWinApp::OnHelpUsing)
	ON_COMMAND(IDD_ABOUTBOX, OnAppAbout)
END_MESSAGE_MAP()





/////////////////////////////////////////////////////////////////////////////
// CBDPS_SECApp construction

CBDPS_SECApp::CBDPS_SECApp()
{

} // end constructor



bool CBDPS_SECApp::OnHelpContents()
{
	AfxGetApp()->WinHelp(0,HELP_CONTENTS);
	return true;
}

bool CBDPS_SECApp::OnHelpIndex()
{
	AfxGetApp()->WinHelp(0,HELP_INDEX);
	return true;
}

bool CBDPS_SECApp::OnHelpOnHelp()
{
	AfxGetApp()->WinHelp(0,HELP_HELPONHELP);
	return true;
}

/////////////////////////////////////////////////////////////////////////////
// The one and only CBDPS_SECApp object

CBDPS_SECApp theApp;
CButtonListDialog *pogMainScreenDlg;


/////////////////////////////////////////////////////////////////////////////
// CBDPS_SECApp initialization



void InitLogging(void)
{
	char pclConfigPath[142];
	char pclLogConfig[142];

    // retrieve the configuration path from OS environment
    if (getenv("CEDA") == NULL)
        strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
    else
        strcpy(pclConfigPath, getenv("CEDA"));

	// Search the config file specified in pclConfigPath (CEDA.INI)
	// search in the section "CCSLOG" for the Keyword "LOGFILE"
	// return the log file in pclLogConfig
	// parameter 3 is the default if the search is not successful
    GetPrivateProfileString("CCSLOG", "LOGFILE", "C:\\UFIS\\SYSTEM\\PEPLOG.CFG",
            pclLogConfig, sizeof pclLogConfig, pclConfigPath);
	ogLog.Load(pclLogConfig);
	ogLog.Trace("START1","Test Start %d",1);
	ogLog.Trace("START2","Test Start %d",2);

	ogLog.Debug("START1",CCSLog::Most,(const char *)"Test Debug Most Start %d",1);
	ogLog.Debug("START2",CCSLog::Some,(const char *)"Test Debug Some Start %d",2);
	ogLog.Debug("START3",CCSLog::None,(const char *)"Test Debug None Start %d",3);
	ogLog.Error("START1","Test Error Start %d",1);

} // end InitLogging




BOOL CBDPS_SECApp::InitInstance()
{


	if (!AfxOleInit())
	{
		return FALSE;
	}

	SetDialogBkColor();        // Set dialog background color to gray
	LoadStdProfileSettings();  // Load standard INI file options (including MRU)


#ifdef _AFXDLL
	Enable3dControls();			// Call this when using MFC in a shared DLL
#else
	Enable3dControlsStatic();	// Call this when linking to MFC statically
#endif


	// read the configuration file for home airport and message file name
	ReadCfgFile(pcgAppName);

	strncpy(CCSCedaData::pcmTableExt, pcgTableExt,3);
	strncpy(CCSCedaData::pcmHomeAirport, pcgHome,3);
	strcpy(CCSCedaData::pcmApplName, pcgAppName);

	// load error and informational messages into ogMessList
	if( ! ogMessList.Load(pcmSecCfgFilename) )
		AfxMessageBox(CString("Error opening the message file!\n")+ogMessList.pcmLastError);

	// load object (buttons,fields etc) descriptions and statuses
	if( ! ogObjList.Load(pcmSecCfgFilename) )
		AfxMessageBox(CString("Error opening the object description file!\n")+ogObjList.pcmLastError);

	// Check condition of the communication with the server
    if (ogCommHandler.Initialize() != true)  // connection error?
    {
        AfxMessageBox(CString("CedaCom:\n") + ogCommHandler.LastError());
        return FALSE;
    }


    // allow the user to log in
    CLoginDialog olLoginDlg(pcgOldHome,pcgAppName,pcgWks);

	CString olNewLine("\n");
/*
	olLoginDlg.INVALID_USERNAME = GetString(IDS_INVALID_USERNAME MESS);
	olLoginDlg.INVALID_PASSWORD = GetString(IDS_INVALID_PASSWORD MESS);
	olLoginDlg.EXPIRED_USERNAME = GetString(IDS_EXPIRED_USERNAME MESS) + olNewLine + GetString(IDS_CONTACT_SYS ADMIN);
	olLoginDlg.DISABLED_USERNAME = GetString(IDS_DISABLED_USERNAME MESS) + olNewLine + GetString(IDS_CONTACT_SYS ADMIN);
	olLoginDlg.EXPIRED_PROFILE = GetString(IDS_EXPIRED_PROFILE MESS) + olNewLine + GetString(IDS_CONTACT_SYS ADMIN);
	olLoginDlg.DISABLED_PROFILE = GetString(IDS_DISABLED_PROFILE MESS) + olNewLine + GetString(IDS_CONTACT_SYS ADMIN);
	olLoginDlg.UNDEFINED_PROFILE = GetString(IDS_UNDEFINED_PROFILE MESS) + olNewLine + GetString(IDS_CONTACT_SYS ADMIN);
	olLoginDlg.MESSAGE_BOX_CAPTION = GetString(IDS_MESSAGE_BOX CAPTION);
	olLoginDlg.USERNAME_CAPTION = GetString(IDS_USERNAME_CAPTION);
	olLoginDlg.PASSWORD_CAPTION = GetString(IDS_PASSWORD_CAPTION);
	olLoginDlg.OK_CAPTION = GetString(IDS_OK_CAPTION);
	olLoginDlg.CANCEL_CAPTION = GetString(IDS_CANCEL_CAPTION);
*/
	// display the login mask
	if (olLoginDlg.DoModal() == IDCANCEL)
	{
	    return FALSE;
	}

	// Character mapping !!!
	ogPrivList.ReadUfisCedaConfig();

// uncomment the following to allow registration of SEC
//	else
//	{
//		int ilStartApp = IDOK;
//		if(ogPrivList.GetStat("InitModu") == '1')
//		{
//			RegisterDlg olRegisterDlg;
//			ilStartApp = olRegisterDlg.DoModal();
//		}
//		if(ilStartApp != IDOK)
//		{
//			return FALSE;
//		}
//	}

	bgIsSuperUser = olLoginDlg.bmIsSuperUser;

	
	InitLogging(); // read the config file and start writing to log

	
	InitFont();
	CreateBrushes();

    
	
	bool blRc = InitialLoad(); // load database tables - with progress bar

	if( blRc )
	{
		CButtonListDialog olMainScreenDlg;    // create the main screen dialog
	//???	olMainScreenDlg.omAdminUsid = CString(pcmAdminUsid); // name of the system adminstrator (cannot be deleted or it's name changed)
		pogMainScreenDlg = &olMainScreenDlg;  // global pointer to main screen so it can be used in other dialogs
		m_pMainWnd = &olMainScreenDlg;



		int nResponse = olMainScreenDlg.DoModal();
	}

	// Since the dialog has been closed, return FALSE so that we exit the
	//  application, rather than start the application's message pump.
	return FALSE;

} // end InitInstance()





bool CBDPS_SECApp::InitialLoad(void)
{
	bool blRc = true;

	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));

	// create, init and display the progress bar
	pogInitialLoad = new InitialLoadDlg();
	CString olErr;
	
	if( blRc )
	{
		blRc = ogCedaSecData.Read();
		if( ! blRc )
		{
			olErr.Format("%s\n(omLastErrorMessage=<%s>)",GetString(IDS_DATABASE_ERROR),ogCedaSecData.omLastErrorMessage);
			AfxMessageBox(olErr);
		}
	}

	if( blRc )
	{
		blRc = ogCedaCalData.Read();
		if( ! blRc )
		{
			olErr.Format("%s\n(omLastErrorMessage=<%s>)",GetString(IDS_DATABASE_ERROR),ogCedaCalData.omLastErrorMessage);
			AfxMessageBox(olErr);
		}
	}

	if( blRc )
	{
		blRc = ogCedaGrpData.Read();
		if( ! blRc )
		{
			olErr.Format("%s\n(omLastErrorMessage=<%s>)",GetString(IDS_DATABASE_ERROR),ogCedaGrpData.omLastErrorMessage);
			AfxMessageBox(olErr);
		}
	}

	if( blRc )
	{
		blRc = ogCedaPrvData.Read();
		if( ! blRc )
		{
			olErr.Format("%s\n(omLastErrorMessage=<%s>)",GetString(IDS_DATABASE_ERROR),ogCedaPrvData.omLastErrorMessage);
			AfxMessageBox(olErr);
		}
	}

	if( blRc )
	{
		blRc = ogCedaFktData.Read();
		if( ! blRc )
		{
			olErr.Format("%s\n(omLastErrorMessage=<%s>)",GetString(IDS_DATABASE_ERROR),ogCedaFktData.omLastErrorMessage);
			AfxMessageBox(olErr);
		}
	}


	pogInitialLoad->SetMessage(GetString(IDS_ALL_TABLES_LOADED));
	pogInitialLoad->DestroyWindow();
	pogInitialLoad = NULL;


	return blRc;

} // end InitialLoad()


void CBDPS_SECApp::SetLandscape()
{
	// Get default printer settings.
	PRINTDLG   pd;
	pd.lStructSize = (DWORD) sizeof(PRINTDLG);
	if (GetPrinterDeviceDefaults(&pd))
	{
		// Lock memory handle.
		DEVMODE FAR* pDevMode =
			(DEVMODE FAR*)::GlobalLock(m_hDevMode);
		if (pDevMode)
		{
			// Change printer settings in here.
			pDevMode->dmOrientation = DMORIENT_LANDSCAPE;

			// Unlock memory handle.
			::GlobalUnlock(m_hDevMode);
		}
	}

} // end SetLandscape()


/////////////////////////////////////////////////////////////////////////////////////////////
// Destructor
//
CBDPS_SECApp::~CBDPS_SECApp()
{
	DeleteBrushes();

} // end destructor



void CBDPS_SECApp::ReadCfgFile(const char *pcpThisAppl)
{
	char pclConfigFile[142];

    // retrieve the configuration path from OS environment
    if (getenv("CEDA") == NULL)
        strcpy(pclConfigFile, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
    else
        strcpy(pclConfigFile, getenv("CEDA"));


	// Search the config file specified in pclConfigPath (CEDA.INI)
	// param 1 = section to search for,  param 2 = keyword to search for
	// param 3 = default value, params 4/5 = var to receive the value + size
	// param 6 = config file name

    // get the filename containing a list of all messages for BDPS-SEC
    // and a list object (buttons,fields etc) descriptions and statuses
	GetPrivateProfileString(pcpThisAppl, "INIFILE", "C:\\UFIS\\SYSTEM\\BDPS_SEC.CFG",
							pcmSecCfgFilename, sizeof(pcmSecCfgFilename), pclConfigFile);


	// get the home airport from [BDPS-SEC] segment
	// this is only used so that BDPS-SEC is backwards compatible
    GetPrivateProfileString(pcpThisAppl, "HOMEAIRPORT", "TAB",pcgOldHome, sizeof(pcgOldHome), pclConfigFile);

	char pclValidToDays[20];

	// get the valid to date (days after valid from)
    GetPrivateProfileString(pcpThisAppl, "VALIDTO_DAYS", "365",
							pclValidToDays, sizeof(pclValidToDays), pclConfigFile);

	igValidToDays = atoi(pclValidToDays);
	if(igValidToDays <= 0)
		igValidToDays = 365;

    GetPrivateProfileString("GLOBAL", "HOMEAIRPORT", "TAB", pcgHome, sizeof pcgHome, pclConfigFile);
    GetPrivateProfileString("GLOBAL", "TABLEEXTENSION", "", pcgTableExt, sizeof pcgTableExt, pclConfigFile);
	if(!strcmp(pcgTableExt,""))
	{
		strcpy(pcgTableExt,pcgOldHome);
	}
	strcat(pcgTableExt," ");


} // end InitLogging

void CBDPS_SECApp::OnAppAbout()
{
	CAboutSecDlg aboutDlg;
	aboutDlg.DoModal();
}
