#ifndef __CEDAGRP_DATA__
#define __CEDAGRP_DATA__


#include "CCSGlobl.h"
#include "CCSCedaData.h"
#include "CedaCalData.h"


static void CedaGrpDataCf(void *popInstance, int ipDDXType,
						   void *vpDataPointer, CString &ropInstanceName);


/////////////////////////////////////////////////////////////////////////////
struct GRPDATA {

	char	URNO[URNOLEN]; // unique record number
	char	FREL[URNOLEN]; // SEC.URNO of profile/group/wksgrp
	char	FSEC[URNOLEN]; // SEC.URNO to user/wks etc
	char	TYPE[TYPELEN]; // 'G' or 'P'
	char	STAT[STATLEN]; // is the assign currently enabled
	int		IsChanged;

	GRPDATA(void)
	{
		strcpy(URNO, "");
		strcpy(FREL, "");
		strcpy(FSEC, "");
		strcpy(TYPE, "");
		strcpy(STAT, "");
		IsChanged = DATA_UNCHANGED;
	}

};


#define ORA_NOT_FOUND "ORA-01403"

class CedaGrpData: public CCSCedaData
{

// Attributes
public:

    CCSPtrArray <GRPDATA> omData;
	CMapStringToPtr omUrnoMap;
	CMapStringToPtr omFsecMap;
	CMapStringToPtr omFrelMap;
	CMapStringToPtr omFrelFsecMap;
	CMapStringToPtr omFsecTypeMap;
	CCSPtrArray <CEDARECINFO> omRetRecInfo; // contains record info for reading CRU return values

// Operations
public:

    CedaGrpData();
	~CedaGrpData();

	void	ClearAll(void);
	bool	Read(void);
	bool	Add(GRPDATA *prpGrp);
	bool	NewRel( CCSPtrArray <GRPDATA> &ropGrpRecs, CCSPtrArray <CALDATA> &ropCalRecs, char *pcpErrorMessage);
	bool	DelRel( const char *pcpDataField, char *pcpErrorMessage);
	bool	Insert( GRPDATA *prpGrp );
	void	DeleteGroups( char  *pcpUrno );
	void	DeletePersonalProfile( char  *pcpUrno );
	bool	DeleteByUrno( const char *pcpUrno);
	bool	DeleteByFsec( const char *pcpFsec);
	bool	DeleteByFrel( const char *pcpFrel);
	GRPDATA* GetGrpByUrno(const char *pcpUrno);
	GRPDATA* GetGrpByFrel(const char *pcpFrel);
	GRPDATA* GetGrpByFsec(const char *pcpFsec);
	GRPDATA* GetGrpByFsecType(const char *pcpFsec,SEC_REC_TYPE rpType);
	void	GetGroupsByFsec(const char *pcpFsec, CCSPtrArray <GRPDATA> &ropGroups, bool bpReset=true);
	bool	GetAssigned(const char *pcpFrel, const char *pcpFsec, char *pcpUrno);

};

extern CedaGrpData ogCedaGrpData;

#endif //__CEDAGRP_DATA__
