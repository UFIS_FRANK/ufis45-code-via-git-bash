// AllocatedDlg.cpp : implementation file
//

#include "stdafx.h"
#include "CCSGlobl.h"
#include "BDPS_SEC.h"
#include "AllocatedDlg.h"
#include "MessList.h"
#include "CalTableViewer.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif



/////////////////////////////////////////////////////////////////////////////
// CAllocatedDlg dialog


CAllocatedDlg::CAllocatedDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CAllocatedDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CAllocatedDlg)
	//}}AFX_DATA_INIT

	ASSERT(pParent != NULL);

	pomCalTable = NULL;
	m_AllocatedProfilesList = NULL;

	m_AllocatedProfilesList = new CObjectListBox((CWnd*)this);
	m_pParent = pParent;
	m_nID = CAllocatedDlg::IDD;

	imCurrSel = -1; // force InitList to do something

	pomCalTable = new CCSTable; // calendar list
	pomCalTable->SetStyle(WS_BORDER);
	pomCalTable->SetSelectMode(0);

	memset(pcmUrno,0,sizeof(pcmUrno));
	memset(pcmPrevUrno,0,sizeof(pcmPrevUrno));

}

//
// ~CAllocatedDlg()
//
// Destructor
//
CAllocatedDlg::~CAllocatedDlg(void)
{
	omSelData.DeleteAll();
	if(pomCalTable != NULL)
	{
		delete pomCalTable;
	}
	if(m_AllocatedProfilesList != NULL)
	{
		delete m_AllocatedProfilesList;
	}

} // end ~CAllocatedDlg()


BOOL CAllocatedDlg::Create(void)
{
	return CDialog::Create(m_nID,m_pParent);
}


void CAllocatedDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAllocatedDlg)
	DDX_Control(pDX, IDC_FREQ_ALL, m_FreqAll);
	DDX_Control(pDX, IDC_CALGROUP, m_CalGroup);
	DDX_Control(pDX, IDC_FREQ7, m_Freq7);
	DDX_Control(pDX, IDC_FREQ6, m_Freq6);
	DDX_Control(pDX, IDC_FREQ5, m_Freq5);
	DDX_Control(pDX, IDC_FREQ4, m_Freq4);
	DDX_Control(pDX, IDC_FREQ3, m_Freq3);
	DDX_Control(pDX, IDC_FREQ2, m_Freq2);
	DDX_Control(pDX, IDC_FREQ1, m_Freq1);
	DDX_Control(pDX, IDC_ListSelection, m_ListSelectionCtrl);
	DDX_Control(pDX, IDC_AllocTitle2, m_Title2);
	DDX_Control(pDX, IDC_AllocTitle, m_Title1);
	DDX_Control(pDX, IDOK, m_OkButton);
	DDX_Control(pDX, IDCANCEL, m_CancelButton);
	DDX_Control(pDX, IDC_ShowAllocatedOnly, m_ShowAllocatedOnly);
	DDX_Control(pDX, IDC_AllocatedProfilesList, *m_AllocatedProfilesList);
	DDX_Control(pDX, IDC_VAFRdate, m_VAFRdateCtrl);
	DDX_Control(pDX, IDC_VAFRtime, m_VAFRtimeCtrl);
	DDX_Control(pDX, IDC_VATOdate, m_VATOdateCtrl);
	DDX_Control(pDX, IDC_VATOtime, m_VATOtimeCtrl);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CAllocatedDlg, CDialog)
	//{{AFX_MSG_MAP(CAllocatedDlg)
	ON_LBN_SELCHANGE(IDC_AllocatedProfilesList, OnSelchangeAllocatedProfilesList)
	ON_BN_CLICKED(IDC_ShowAllocatedOnly, OnShowAllocatedOnly)
	ON_CBN_SELCHANGE(IDC_ListSelection, OnSelchangeListSelection)
	ON_BN_CLICKED(IDC_FREQ_ALL, OnFreqAll)
	ON_BN_CLICKED(IDC_FREQ1, OnFreq1)
	ON_BN_CLICKED(IDC_FREQ2, OnFreq2)
	ON_BN_CLICKED(IDC_FREQ3, OnFreq3)
	ON_BN_CLICKED(IDC_FREQ4, OnFreq4)
	ON_BN_CLICKED(IDC_FREQ5, OnFreq5)
	ON_BN_CLICKED(IDC_FREQ6, OnFreq6)
	ON_BN_CLICKED(IDC_FREQ7, OnFreq7)
	ON_MESSAGE(WM_TABLE_LBUTTONDOWN,OnTableLButtonDown)
	ON_MESSAGE(WM_TABLE_RBUTTONDOWN,OnTableRButtonDown)
	ON_MESSAGE(WM_TABLE_SELCHANGE,OnTableSelChange)
	ON_MESSAGE(WM_EDIT_KILLFOCUS,OnEditKillFocus)
	ON_COMMAND(WM_INSERT_DATE,OnInsertDate)
	ON_COMMAND(WM_DELETE_DATE,OnDeleteDate)
	ON_MESSAGE(WM_UPDATE_CAL,OnUpdateCal)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CAllocatedDlg message handlers

// called when a new line is selected on the main screen
// reset the list of allocated objects (profiles,users,groups etc)
// load group data for pcpUrno
BOOL CAllocatedDlg::OnInitDialog(const char *pcpUrno) 
{
	CDialog::OnInitDialog();

	// check if something has been changed if so ask the user if they want to update the DB
	CheckChange();
	
	// copy the URNO of the new line selected on the main screen
	strcpy(pcmUrno,pcpUrno);
	
	omGrpTableViewer.pomList = m_AllocatedProfilesList; // point the groupTableViewer to my list box
	omGrpTableViewer.pomTitle = &m_Title2;
	strcpy(omGrpTableViewer.pcmUrno,pcmUrno);

	CRect rect;
	rect.left = 18;
	rect.right = 388;
	rect.top = 432;
	rect.bottom = 552;
	pomCalTable->SetTableData(this, rect.left, rect.right, rect.top, rect.bottom);
	omCalTableViewer.Attach(pomCalTable);

	m_VAFRdateCtrl.SetTypeToDate(true);
	m_VAFRdateCtrl.SetBKColor(YELLOW);
	m_VAFRdateCtrl.SetTextErrColor(RED);

	m_VAFRtimeCtrl.SetTypeToTime(true);
	m_VAFRtimeCtrl.SetBKColor(YELLOW);
	m_VAFRtimeCtrl.SetTextErrColor(RED);

	m_VATOdateCtrl.SetTypeToDate(true);
	m_VATOdateCtrl.SetBKColor(YELLOW);
	m_VATOdateCtrl.SetTextErrColor(RED);

	m_VATOtimeCtrl.SetTypeToTime(true);
	m_VATOtimeCtrl.SetBKColor(YELLOW);
	m_VATOtimeCtrl.SetTextErrColor(RED);
	
	omSelData.DeleteAll();

	m_ListSelectionCtrl.ResetContent();
	switch(rgCurrType)
	{
		case SEC_PROFILE:
			m_ListSelectionCtrl.AddString(GetString(IDS_ALLOCATEDDLG_USERS));
			AddSel(false,SEC_USER,SEC_PROFILE);
			m_ListSelectionCtrl.AddString(GetString(IDS_ALLOCATEDDLG_GROUPS));
			AddSel(false,SEC_GROUP,SEC_PROFILE);
			m_ListSelectionCtrl.AddString(GetString(IDS_ALLOCATEDDLG_WKS));
			AddSel(false,SEC_WKS,SEC_PROFILE);
			m_ListSelectionCtrl.AddString(GetString(IDS_ALLOCATEDDLG_WKSGROUPS));
			AddSel(false,SEC_WKSGROUP,SEC_PROFILE);
			break;
		case SEC_GROUP:
			m_ListSelectionCtrl.AddString(GetString(IDS_ALLOCATEDDLG_PROFILES));
			AddSel(true,SEC_PROFILE,SEC_PROFILE);
			m_ListSelectionCtrl.AddString(GetString(IDS_ALLOCATEDDLG_USERS));
			AddSel(false,SEC_USER,SEC_GROUP);
			m_ListSelectionCtrl.AddString(GetString(IDS_ALLOCATEDDLG_GROUPS));
			AddSel(false,SEC_GROUP,SEC_GROUP);
			break;
		case SEC_WKS:
			m_ListSelectionCtrl.AddString(GetString(IDS_ALLOCATEDDLG_PROFILES));
			AddSel(true,SEC_PROFILE,SEC_PROFILE);
			m_ListSelectionCtrl.AddString(GetString(IDS_ALLOCATEDDLG_WKSGROUPS));
			AddSel(false,SEC_WKSGROUP,SEC_GROUP);
			break;
		case SEC_WKSGROUP:
			m_ListSelectionCtrl.AddString(GetString(IDS_ALLOCATEDDLG_PROFILES));
			AddSel(true,SEC_PROFILE,SEC_PROFILE);
			m_ListSelectionCtrl.AddString(GetString(IDS_ALLOCATEDDLG_WKS));
			AddSel(false,SEC_WKS,SEC_GROUP);
			m_ListSelectionCtrl.AddString(GetString(IDS_ALLOCATEDDLG_WKSGROUPS));
			AddSel(false,SEC_WKSGROUP,SEC_GROUP);
			break;
		case SEC_USER:
		default:
			m_ListSelectionCtrl.AddString(GetString(IDS_ALLOCATEDDLG_PROFILES));
			AddSel(true,SEC_PROFILE,SEC_PROFILE);
			m_ListSelectionCtrl.AddString(GetString(IDS_ALLOCATEDDLG_GROUPS));
			AddSel(false,SEC_GROUP,SEC_GROUP);
			break;
	}

	// if the type of data displayed on the main screen has changed or this is the first InitDialog() after
	// creation then reset the allocation type in the comboBox
	if( rgCurrType != rmLastType || imCurrSel < 0 )
		imCurrSel = 0;

	m_ListSelectionCtrl.SetCurSel(imCurrSel); // select the first in the list
	rmLastType = rgCurrType;

	// init the list of profiles/groups/users etc depending on what is selected in the comboBox
	InitList();

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

// check if something has been changed if so ask the user if they want to update the DB
void CAllocatedDlg::CheckChange()
{
	bool blChanged = false;
	int ilLine, ilNumLines = omGrpTableViewer.omLines.GetSize();
	for(ilLine = 0; !blChanged && ilLine < ilNumLines; ilLine++ )
		blChanged = omGrpTableViewer.omLines[ilLine].isAssigned != omGrpTableViewer.omLines[ilLine].wasAssigned;

	if( blChanged )
	{
		CString omCaption;
		GetWindowText(omCaption);
		if(MessageBox(GetString(IDS_ALLOCATEDDLG_UPDATECHANGES),omCaption,MB_ICONINFORMATION|MB_YESNO) == IDYES)
			OnOK();
	}
}

// called when the type of object displayed in the alloc list is changed
void CAllocatedDlg::InitList()
{
	char pclTmp1[100],pclTmp2[100];
	int ilCurrSel = m_ListSelectionCtrl.GetCurSel(); // get the value selected in the comboBox


	if( ilCurrSel != LB_ERR )
	{
		// mirror GrpTableViewer with the actual selected lines
		UpdateSelFlag();


		// update the type selection field
		imCurrSel = ilCurrSel;
		m_ListSelectionCtrl.SetCurSel(imCurrSel); // refresh the comboBox

		// load and display list for the type (profile/user/group...)
		UpdateView(omSelData[imCurrSel].type);

		int ilNumSelectedLines = m_AllocatedProfilesList->GetSelCount();
		int ilNumLines = omGrpTableViewer.omLines.GetSize();


		// set single or multiple select
		if( omSelData[imCurrSel].singleSel )
		{
			// initialize the selected line pointer
			if( ilNumSelectedLines > 0 )
				m_AllocatedProfilesList->GetSelItems(1,&imSelectedLine);
			else
				imSelectedLine = -1; // no lines selected
			sprintf(pclTmp1,"%s ",GetString(IDS_ALLOCATEDDLG_SINGLESELCAPTION));
		}
		else
			sprintf(pclTmp1,"%s ",GetString(IDS_ALLOCATEDDLG_MULTISELCAPTION));
		
		// initialize main caption
		switch( omSelData[imCurrSel].type )
		{
			case SEC_PROFILE:
				strcat(pclTmp1,GetString(IDS_ALLOCATEDDLG_PROFILECAPTION));
				sprintf(pclTmp2,GetString(IDS_ALLOCATEDDLG_SHOWPROFILE),ilNumSelectedLines,ilNumLines);
				break;
			case SEC_GROUP:
				strcat(pclTmp1,GetString(IDS_ALLOCATEDDLG_GROUPCAPTION));
				sprintf(pclTmp2,GetString(IDS_ALLOCATEDDLG_SHOWGROUP),ilNumSelectedLines,ilNumLines);
				break;
			case SEC_WKS:
				strcat(pclTmp1,GetString(IDS_ALLOCATEDDLG_WKSCAPTION));
				sprintf(pclTmp2,GetString(IDS_ALLOCATEDDLG_SHOWWKS),ilNumSelectedLines,ilNumLines);
				break;
			case SEC_WKSGROUP:
				strcat(pclTmp1,GetString(IDS_ALLOCATEDDLG_WKSGROUPCAPTION));
				sprintf(pclTmp2,GetString(IDS_ALLOCATEDDLG_SHOWWKSGROUP),ilNumSelectedLines,ilNumLines);
				break;
			case SEC_USER:
			default:
				strcat(pclTmp1,GetString(IDS_ALLOCATEDDLG_USERCAPTION));
				sprintf(pclTmp2,GetString(IDS_ALLOCATEDDLG_SHOWUSER),ilNumSelectedLines,ilNumLines);
				break;
		}
		SetWindowText(pclTmp1);
		m_ShowAllocatedOnly.SetWindowText(pclTmp2);

		// initialize titles
		switch( rgCurrType )
		{
			case SEC_PROFILE:
				m_Title1.SetWindowText(GetString(IDS_ALLOCATEDDLG_PROFILENAME));
				break;
			case SEC_GROUP:
				m_Title1.SetWindowText(GetString(IDS_ALLOCATEDDLG_GROUPNAME));
				break;
			case SEC_WKS:
				m_Title1.SetWindowText(GetString(IDS_ALLOCATEDDLG_WKSNAME));
				break;
			case SEC_WKSGROUP:
				m_Title1.SetWindowText(GetString(IDS_ALLOCATEDDLG_WKSGROUPNAME));
				break;
			case SEC_USER:
			default:
				m_Title1.SetWindowText(GetString(IDS_ALLOCATEDDLG_USERNAME));
				break;
		}


		// load all calendar data
		LoadCalData();

		// display the calendar data
		DisplayCalData();

	}
}



void CAllocatedDlg::OnOK() 
{
	char pclCedaErrorMessage[ERRMESSLEN];
	bool blRc = true;
	
	UpdateSelFlag(); // mirror GrpTableViewer with the actual selected lines
	omCalTableViewer.CreateCommands(); // creates 3 lists of CAL records inserted,updated and deleted

	AfxGetApp()->DoWaitCursor(1); // hourGlass

	

	// create a list of new relations
	omNewRelations.DeleteAll();
	omDelRel.Empty();
	CString olComma(","),olType(omSelData[imCurrSel].allocType);

	// set a flag which states if the URNO of the selected line is the FREL field in the GRP table
	// this line says if the line clicked in the main screen was not a profile and the type allocated to was
	// either a profile or a group or a workstation group, then pcmUrno is FREL in GRPTAB
	bool blIsFrel = omSelData[imCurrSel].type != SEC_PROFILE && ( rgCurrType == SEC_PROFILE || rgCurrType == SEC_WKSGROUP || rgCurrType == SEC_GROUP );



	GRPTAB_LINEDATA *prlGrpRec;
	int ilLine, ilNumLines = omGrpTableViewer.omLines.GetSize();
	for(ilLine = 0; ilLine < ilNumLines; ilLine++ )
	{
		prlGrpRec = &omGrpTableViewer.omLines[ilLine];

		// check if is allocated but wasn't before
		if(prlGrpRec->isAssigned == true && prlGrpRec->wasAssigned == false)
		{
			GRPDATA *prlNewGrp = new GRPDATA;
			
			strcpy(prlNewGrp->URNO,prlGrpRec->URNO); // need this to link group recs to cal recs (not the real URNO)
			strcpy(prlNewGrp->TYPE,olType);

			if( blIsFrel )
			{
				strcpy(prlNewGrp->FREL,pcmUrno);
				strcpy(prlNewGrp->FSEC,prlGrpRec->URNO);
			}
			else
			{
				strcpy(prlNewGrp->FREL,prlGrpRec->URNO);
				strcpy(prlNewGrp->FSEC,pcmUrno);
			}

			omNewRelations.Add(prlNewGrp);
			prlGrpRec->wasAssigned = prlGrpRec->isAssigned;
		}

		// check if was allocated but no longer
		if(prlGrpRec->wasAssigned == true && prlGrpRec->isAssigned == false)
		{
			omDelRel += prlGrpRec->FGRP + olComma;
			prlGrpRec->wasAssigned = prlGrpRec->isAssigned;
		}
	
	}
	
	bool blCalDataChanged = false;

	// create new relations (deletes (some) CAL dates from omInsertedList as the relations are added)
	if( blRc && omNewRelations.GetSize() > 0)
	{
		if(ogCedaGrpData.NewRel( omNewRelations, omCalTableViewer.omInsertedList, pclCedaErrorMessage ) != true )
		{
			MessageBox(GetString(IDS_DATABASE_ERROR),"",MB_ICONEXCLAMATION);
			blRc = false;
		}
		blCalDataChanged = true;
	}

	// delete deallocated relations
	if( blRc && !omDelRel.IsEmpty() )
	{
		if(ogCedaGrpData.DelRel( omDelRel, pclCedaErrorMessage ) != true )
		{
			MessageBox(GetString(IDS_DATABASE_ERROR),"",MB_ICONEXCLAMATION);
			blRc = false;
		}
		blCalDataChanged = true;
	}
			
	// profile alloc changed so make sure the rights viewer is updated
	if( omNewRelations.GetSize() > 0 || ! omDelRel.IsEmpty() )
		ogDdx.DataChanged((void *)this, OBJ_DLG_CHANGE,(void *)pcmUrno );



	// insert new CAL recs
	if( blRc && omCalTableViewer.omInsertedList.GetSize() > 0 )
	{
		if(ogCedaCalData.NewCal( omCalTableViewer.omInsertedList, pclCedaErrorMessage) != true )
		{
			MessageBox(GetString(IDS_DATABASE_ERROR),"",MB_ICONEXCLAMATION);
			blRc = false;
		}
		blCalDataChanged = true;
	}

	// update existing CAL recs
	if( blRc && omCalTableViewer.omUpdatedList.GetSize() > 0 )
	{
		if(ogCedaCalData.UpdCal( omCalTableViewer.omUpdatedList, pclCedaErrorMessage ) != true )
		{
			MessageBox(GetString(IDS_DATABASE_ERROR),"",MB_ICONEXCLAMATION);
			blRc = false;
		}
		blCalDataChanged = true;
	}

	// remove deleted CAL recs
	if( blRc && omCalTableViewer.omDeletedList.GetSize() > 0 )
	{
		if(ogCedaCalData.DelCal( omCalTableViewer.omDeletedList, pclCedaErrorMessage ) != true )
		{
			MessageBox(GetString(IDS_DATABASE_ERROR),"",MB_ICONEXCLAMATION);
			blRc = false;
		}
		blCalDataChanged = true;
	}


	// if calendar data was changed refresh the list of dates displayed
	if(blCalDataChanged)
	{
		int ilCurrLine = pomCalTable->GetCurSel(); // remember the selected line
//		omCalTableViewer.ChangeViewTo(); // update viewer data with changes
//		omCalTableViewer.UpdateDisplay(); // redisplay the viewer data
OnInitDialog(pcmUrno);
		SelectDate(ilCurrLine); // reselect the line
	}

	
	AfxGetApp()->DoWaitCursor(-1); // hourGlass

}

void CAllocatedDlg::SendCancel() 
{
	OnCancel();
}

void CAllocatedDlg::OnCancel() 
{
	DestroyWindow();
}

void CAllocatedDlg::PostNcDestroy() 
{
	delete this;
	pogAllocDlg = NULL;
	
}


//-------------------------------------------------------------------------------------------------------------------//
// Alloc list handling routines

void CAllocatedDlg::OnShowAllocatedOnly() 
{
	UpdateSelFlag(); // mirror GrpTableViewer with the actual selected lines
	omGrpTableViewer.bmAllocOnly = !omGrpTableViewer.bmAllocOnly;
	omGrpTableViewer.UpdateDisplay();
	omCalTableViewer.bmAllocOnly = !omCalTableViewer.bmAllocOnly;
	omCalTableViewer.UpdateDisplay();
}

//
// UpdateSelFlag()
//
// function to mirror GrpTableViewer flags with the listBox
//
void CAllocatedDlg::UpdateSelFlag()
{
	// count how many lines are selected
	int ilNumSelectedLines = m_AllocatedProfilesList->GetSelCount();

	// allocate an integer buffer to hold the lines numbers selected
	int *pilSelLines = new int[ilNumSelectedLines];

	// get a list of the selected lines
	m_AllocatedProfilesList->GetSelItems(ilNumSelectedLines,pilSelLines);


	// initially deselect all lines in the alloc array
	int ilLine, ilNumLines = omGrpTableViewer.omLines.GetSize();
	for(ilLine = 0; ilLine < ilNumLines; ilLine++ )	
		omGrpTableViewer.omLines[ilLine].isAssigned = false;

	char pclUsid[USIDLEN];
	for(ilLine = 0; ilLine < ilNumSelectedLines; ilLine++ )
	{
		m_AllocatedProfilesList->GetText(pilSelLines[ilLine],pclUsid);
		omGrpTableViewer.UpdateByUsid(pclUsid);
	}

	// free allocated memory
	delete [] pilSelLines;

}

//
// OnSelchangeAllocatedProfilesList()
//
// Handle new selection in the alloc list
//
void CAllocatedDlg::OnSelchangeAllocatedProfilesList() 
{
	int ilLine, ilNumSelectedLines;
	int *pilSelLines;
	
	// if the list box contains a list of profiles, only one of them can
	// be selected at any one time
	if( omSelData[imCurrSel].singleSel )
	{
		// get the number of selected lines
		ilNumSelectedLines = m_AllocatedProfilesList->GetSelCount();

		// if there is more than one selected line
		switch( ilNumSelectedLines )
		{
			case 0: // nothing selected
				imSelectedLine = -1;
				break;
			case 1: // single line selected
				m_AllocatedProfilesList->GetSelItems(ilNumSelectedLines,&imSelectedLine);
				break;
			default: // two (or more) lines selected

				pilSelLines = new int[ilNumSelectedLines];

				// get a list of the selected lines
				m_AllocatedProfilesList->GetSelItems(ilNumSelectedLines,pilSelLines);

				int ilNewSelLine = -1;
				for( ilLine = 0; ilLine < ilNumSelectedLines; ilLine++ )
				{
					if(pilSelLines[ilLine] == imSelectedLine)
						// selected line has been re-clicked so unselect it
						m_AllocatedProfilesList->SetSel(imSelectedLine,FALSE);
					else
						// remember the new selected line
						ilNewSelLine = pilSelLines[ilLine];
				}

				imSelectedLine = ilNewSelLine;

				// free allocated memory
				delete [] pilSelLines;

		} // end switch

	}


	// reset the zuordnung text
	ilNumSelectedLines = m_AllocatedProfilesList->GetSelCount();
	int ilNumLines = omGrpTableViewer.omLines.GetSize();
	char pclTmp[100];

	// set num of allocated caption
	switch( omSelData[imCurrSel].type )
	{
		case SEC_PROFILE:
			sprintf(pclTmp,GetString(IDS_ALLOCATEDDLG_SHOWPROFILE),ilNumSelectedLines,ilNumLines);
			break;
		case SEC_GROUP:
			sprintf(pclTmp,GetString(IDS_ALLOCATEDDLG_SHOWGROUP),ilNumSelectedLines,ilNumLines);
			break;
		case SEC_WKS:
			sprintf(pclTmp,GetString(IDS_ALLOCATEDDLG_SHOWWKS),ilNumSelectedLines,ilNumLines);
			break;
		case SEC_WKSGROUP:
			sprintf(pclTmp,GetString(IDS_ALLOCATEDDLG_SHOWWKSGROUP),ilNumSelectedLines,ilNumLines);
			break;
		case SEC_USER:
		default:
			sprintf(pclTmp,GetString(IDS_ALLOCATEDDLG_SHOWPROFILE),ilNumSelectedLines,ilNumLines);
			break;
	}
	m_ShowAllocatedOnly.SetWindowText(pclTmp);


	// work out which was the last selected line
	int ilLastSelLine = -1;
	pilSelLines = new int[ilNumSelectedLines];
	m_AllocatedProfilesList->GetSelItems(ilNumSelectedLines,pilSelLines);
	for( ilLine = 0; ilLastSelLine == -1 && ilLine < ilNumSelectedLines; ilLine++ )
		if(!omGrpTableViewer.omLines[pilSelLines[ilLine]].isAssigned)
			ilLastSelLine = pilSelLines[ilLine];
	delete [] pilSelLines;


	UpdateSelFlag(); // mirror GrpTableViewer with the actual selected lines


	// display the calendar fields
	DisplayCalData(ilLastSelLine);

}

//
// OnUpdateCal()
//
// handle right button click from the alloc listBox
// display calendar fields for the line selected only
//
void CAllocatedDlg::OnUpdateCal(LONG ipLineNo)
{
	long ilLine = (long)ipLineNo;

	// ilLine is not a direct index into omGrpTableViewer.omLines[] when the showAllocOnly box is checked
	CString omSelUsid;
	m_AllocatedProfilesList->GetText(ilLine,omSelUsid);
	ilLine = omGrpTableViewer.GetLineByUsid(omSelUsid);

	// if the line clicked on is selected then display its' calendar data
	if( ilLine >= 0 && omGrpTableViewer.omLines[ilLine].isAssigned)
		DisplayCalData(ilLine);
}



//-------------------------------------------------------------------------------------------------------------------//
// Objects ComboBox handling routines

//
// OnSelchangeListSelection()
//
// New object type selected in the comboBox so reload the alloc list and calendar dates
//
void CAllocatedDlg::OnSelchangeListSelection() 
{
	// check if something has been changed if so ask the user if they want to update the DB
	CheckChange();

	// init the list of profiles/groups/users etc depending on what is selected in the comboBox
	InitList();
	
}

// comboBox data
// bpSingleSel --> true if single-selection else multi-selection
// rpType --> type of list displayed eg. groups,profiles etc
// rpAllocType --> type of allocation: either 'P' for profile or 'G' for group
void CAllocatedDlg::AddSel(bool bpSingleSel, SEC_REC_TYPE rpType, SEC_REC_TYPE rpAllocType)
{
	SELDATA *pomSelRec = new SELDATA;
	pomSelRec->singleSel = bpSingleSel;
	pomSelRec->type = rpType;
	pomSelRec->allocType = rpAllocType;
	omSelData.Add(pomSelRec);
}

void CAllocatedDlg::UpdateView(SEC_REC_TYPE rpType)
{

	AfxGetApp()->DoWaitCursor(1);

	sprintf(omGrpTableViewer.pcmType,"%c\0",rpType); // list the users or profiles or groups etc

	omGrpTableViewer.ChangeViewTo();
	
	AfxGetApp()->DoWaitCursor(-1);

}


//-------------------------------------------------------------------------------------------------------------------//
// Calendar handling routines

//
// LoadCalData()
//
// loop through the alloc list getting the group URNOs
// and load the calendar dates
//
void CAllocatedDlg::LoadCalData(void)
{
	// update the calendar fields
	omCalTableViewer.omSelectedFsec.Empty();
	omCalTableViewer.omFsecList = CString("|");
	int ilNumLines = omGrpTableViewer.omLines.GetSize();
	for(int ilLine = 0; ilLine < ilNumLines; ilLine++ )
	{
		if(omGrpTableViewer.omLines[ilLine].isAssigned)
		{
			// list of all selected profiles - cal data will be loaded for all of these
			omCalTableViewer.omFsecList += omGrpTableViewer.omLines[ilLine].FGRP + CString("|");

			// display cal data for the first selected line found
			if(omCalTableViewer.omSelectedFsec.IsEmpty())
			{
				if( omGrpTableViewer.omLines[ilLine].wasAssigned )
					// Grp was assigned when loaded so it has a real URNO (can use it as FSEC in CAL records)
					omCalTableViewer.omSelectedFsec = omGrpTableViewer.omLines[ilLine].FGRP;
				else
					// Grp was not assigned when loaded so we have no real URNO to connect GRPs and CALs, use the URNO of the user/group etc
					omCalTableViewer.omSelectedFsec = omGrpTableViewer.omLines[ilLine].URNO;

				m_CalGroup.SetWindowText(omGrpTableViewer.omLines[ilLine].USID);
			}

		}
	}
	omCalTableViewer.ChangeViewTo();

}

//
// DisplayCalData()
//
// display the Calendar data for the line specified	
//
void CAllocatedDlg::DisplayCalData(const int ipLine)
{
	// get the num selected lines in the alloc list
	int ilNumSelectedLines = m_AllocatedProfilesList->GetSelCount();

	int ilNumLines = omGrpTableViewer.omLines.GetSize();
	omCalTableViewer.omSelectedFsec.Empty();

	if( ipLine < 0 )
	{
		// no line specified so display cal fields for the first selected line
		for(int ilLine = 0; omCalTableViewer.omSelectedFsec.IsEmpty() && ilLine < ilNumLines; ilLine++ )
		{
			if(omGrpTableViewer.omLines[ilLine].isAssigned)
			{
				if( omGrpTableViewer.omLines[ilLine].wasAssigned )
					// Grp was assigned when loaded so it has a real URNO (can use it as FSEC in CAL records)
					omCalTableViewer.omSelectedFsec = omGrpTableViewer.omLines[ilLine].FGRP;
				else
					// Grp was not assigned when loaded so we have no real URNO to connect GRPs and CALs, use the URNO of the user/group etc
					omCalTableViewer.omSelectedFsec = omGrpTableViewer.omLines[ilLine].URNO;

				m_CalGroup.SetWindowText(omGrpTableViewer.omLines[ilLine].USID);
			}
		}
	}
	else // get the URNO of the line specified and display data for that URNO
	{
		if( omGrpTableViewer.omLines[ipLine].wasAssigned )
			// Grp was assigned when loaded so it has a real URNO (can use it as FSEC in CAL records)
			omCalTableViewer.omSelectedFsec = omGrpTableViewer.omLines[ipLine].FGRP;
		else
			// Grp was not assigned when loaded so we have no real URNO to connect GRPs and CALs, use the URNO of the user/group etc
			omCalTableViewer.omSelectedFsec = omGrpTableViewer.omLines[ipLine].URNO;

		m_CalGroup.SetWindowText(omGrpTableViewer.omLines[ipLine].USID);
	}




	// display the data for the selected line (omSelectedFsec)
	omCalTableViewer.UpdateDisplay();

	if(ilNumSelectedLines > 0)
	{
		bmEditFieldsEnabled = true;

		// if no calendar recs were found but a line is selected then this is a
		// newly selected line, so insert a calendar date for it
		if(omCalTableViewer.GetNumLinesDisplayed() <= 0)
			InsertDate(omCalTableViewer.omSelectedFsec);
	}
	else
	{
		bmEditFieldsEnabled = false;
	}

	EnableEditFields(bmEditFieldsEnabled);
	
	if(ilNumSelectedLines > 0)
		SelectDate(0); // select the first line in the date list

}


//
// OnTableRButtonDown()
//
// Display context sensitive menu to allow inserts and deletes of celendar dates
//
// receives a WM_TABLE_RBUTTONDOWN message from CCSTable
// when the function SelectLine() is called
// if the right mouse button was pressed, displays context sensetive menus
//
// ipParam contains flags indicating which button was pressed
// lpParam contains x,y co-ord where the mouse button was clicked
//
LONG CAllocatedDlg::OnTableRButtonDown(UINT ipParam, LONG lpParam)
{
	CCSTABLENOTIFY *polNotify = (CCSTABLENOTIFY *) lpParam;
	int ilLine = polNotify->Line;

	if( polNotify->SourceTable == pomCalTable )
	{
		if( bmEditFieldsEnabled )
		{
			int ilNumDates = pomCalTable->GetLinesCount();
			CMenu olMenu;
			olMenu.CreatePopupMenu();
			for (int i = olMenu.GetMenuItemCount(); --i >= 0;)
  				olMenu.RemoveMenu(i, MF_BYPOSITION);
			olMenu.AppendMenu(MF_STRING,WM_INSERT_DATE, GetString(IDS_EDITUSERDLG_INSERTDATE));
			if(ilNumDates > 1)
	  			olMenu.AppendMenu(MF_STRING,WM_DELETE_DATE, GetString(IDS_EDITUSERDLG_DELETEDATE));
			else
	  			olMenu.AppendMenu(MF_STRING|MF_GRAYED,WM_DELETE_DATE, GetString(IDS_EDITUSERDLG_DELETEDATE));
			pomCalTable->ClientToScreen(&polNotify->Point);
			olMenu.TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON, polNotify->Point.x, polNotify->Point.y, this, NULL);
		}
	}
	
	return 0L;
}


//
// OnTableLButtonDown()
//
// New date selected in the list so update edit fields
//
// receives a WM_TABLE_LBUTTONDOWN message from CCSTable
// when the function SelectLine() is called
// if the left mouse button was pressed, calls OnTableSelChange()
//
// ipParam contains flags indicating which button was pressed
// lpParam contains x,y co-ord where the mouse button was clicked
//
LONG CAllocatedDlg::OnTableLButtonDown(UINT ipParam, LONG lpParam)
{
	CCSTABLENOTIFY *polNotify = (CCSTABLENOTIFY *) lpParam;
	int ilLine = polNotify->Line;

	if( polNotify->SourceTable == pomCalTable )
	{
		if( bmEditFieldsEnabled )
		{
			SelectDate(pomCalTable->GetCurrentLine());
		}
	}

	return 0L;
}


//
// OnTableSelChange()
//
// receives a WM_TABLE_SELCHANGE message from CCSTable
//
// ipParam contains flags indicating which button was pressed
// lpParam contains x,y co-ord where the mouse button was clicked
//
LONG CAllocatedDlg::OnTableSelChange(UINT ipParam, LONG lpParam)
{
//	AfxMessageBox("OnTableSelChange");

	return 0L;
}



//
// OnEditLostFocus()
//
// receives a WM_EDIT_KILLFOCUS message from CCSEdit field
// Update the celendar list with the contents of the edit boxes
//
//
LONG CAllocatedDlg::OnEditKillFocus(UINT ipParam, LONG lpParam)
{
	CCSEDITNOTIFY *polNotify = (CCSEDITNOTIFY *) lpParam;

	if( polNotify->Status )
	{
		if( polNotify->SourceControl == &m_VAFRdateCtrl || polNotify->SourceControl == &m_VAFRtimeCtrl ||
			polNotify->SourceControl == &m_VATOdateCtrl || polNotify->SourceControl == &m_VATOtimeCtrl) 
			UpdateDates();
	}

	return 0L;
}

//
// OnInsertDate()
//
// Insert a new date for the line selected
//
void CAllocatedDlg::OnInsertDate()
{
	InsertDate(omCalTableViewer.omSelectedFsec);
}

//
// InsertDate()
//
// pcpUrno is the URNO can be either the URNO of the group record
// or if this is a new allocation where the group record has no URNO
// then this is the URNO of the line selected (ie FSEC in the group record)
void CAllocatedDlg::InsertDate(const char *pcpUrno)
{
	CString olVafrDate,olVafrTime,olVatoDate,olVatoTime,olFreq;

	CTime olCurrTime = CTime::GetCurrentTime();
	olVafrDate = olCurrTime.Format("%d.%m.%Y");
	olVafrTime = "00:00";
	olCurrTime += CTimeSpan( (LONG) igValidToDays, 0, 0, 0 ); // add n days to VAFR to create VATO
	olVatoDate = olCurrTime.Format("%d.%m.%Y");
	olVatoTime = "00:00";
	olFreq = "1234567";

	int ilNewLine = omCalTableViewer.Insert(olVafrDate,olVafrTime,olVatoDate,olVatoTime,olFreq,pcpUrno);
	SelectDate(ilNewLine);
}

//
// OnDeleteDate()
//
// Delete the date selected
//
void CAllocatedDlg::OnDeleteDate()
{
	int ilLine = pomCalTable->GetCurrentLine();

	omCalTableViewer.Delete(ilLine);

	if(ilLine < pomCalTable->GetLinesCount())
		SelectDate(ilLine);
	else
		SelectDate(ilLine-1);
}



void CAllocatedDlg::EnableEditFields(bool bpEnable)
{
	m_VAFRdateCtrl.EnableWindow(bpEnable);
	m_VAFRtimeCtrl.EnableWindow(bpEnable);
	m_VATOdateCtrl.EnableWindow(bpEnable);
	m_VATOtimeCtrl.EnableWindow(bpEnable);
	m_FreqAll.EnableWindow(bpEnable);
	m_Freq1.EnableWindow(bpEnable);
	m_Freq2.EnableWindow(bpEnable);
	m_Freq3.EnableWindow(bpEnable);
	m_Freq4.EnableWindow(bpEnable);
	m_Freq5.EnableWindow(bpEnable);
	m_Freq6.EnableWindow(bpEnable);
	m_Freq7.EnableWindow(bpEnable);

	if( !bpEnable )
	{
		m_CalGroup.SetWindowText("");
		m_Freq1.SetCheck(0);
		m_Freq2.SetCheck(0);
		m_Freq3.SetCheck(0);
		m_Freq4.SetCheck(0);
		m_Freq5.SetCheck(0);
		m_Freq6.SetCheck(0);
		m_Freq7.SetCheck(0);
	}
}
void CAllocatedDlg::OnFreqAll() 
{
	m_Freq1.SetCheck(1);
	m_Freq2.SetCheck(1);
	m_Freq3.SetCheck(1);
	m_Freq4.SetCheck(1);
	m_Freq5.SetCheck(1);
	m_Freq6.SetCheck(1);
	m_Freq7.SetCheck(1);
	UpdateDates();	
}

void CAllocatedDlg::OnFreq1() 
{
	UpdateDates();	
}

void CAllocatedDlg::OnFreq2() 
{
	UpdateDates();	
}

void CAllocatedDlg::OnFreq3() 
{
	UpdateDates();	
}

void CAllocatedDlg::OnFreq4() 
{
	UpdateDates();	
}

void CAllocatedDlg::OnFreq5() 
{
	UpdateDates();	
}

void CAllocatedDlg::OnFreq6() 
{
	UpdateDates();	
}

void CAllocatedDlg::OnFreq7() 
{
	UpdateDates();	
}

void CAllocatedDlg::SelectDate(int ipLine)
{	
	char pclVafrDate[DATELEN],pclVafrTime[DATELEN],pclVatoDate[DATELEN],pclVatoTime[DATELEN],pclFreq[FREQLEN];
	
	if(omCalTableViewer.SelectLine(pclVafrDate,pclVafrTime,pclVatoDate,pclVatoTime,pclFreq,ipLine))
	{
		m_VAFRdateCtrl.SetWindowText(pclVafrDate);
		m_VAFRtimeCtrl.SetWindowText(pclVafrTime);
		m_VATOdateCtrl.SetWindowText(pclVatoDate);
		m_VATOtimeCtrl.SetWindowText(pclVatoTime);
		if( strchr(pclFreq,'1') )
			m_Freq1.SetCheck(1);
		else
			m_Freq1.SetCheck(0);
		if( strchr(pclFreq,'2') )
			m_Freq2.SetCheck(1);
		else
			m_Freq2.SetCheck(0);
		if( strchr(pclFreq,'3') )
			m_Freq3.SetCheck(1);
		else
			m_Freq3.SetCheck(0);
		if( strchr(pclFreq,'4') )
			m_Freq4.SetCheck(1);
		else
			m_Freq4.SetCheck(0);
		if( strchr(pclFreq,'5') )
			m_Freq5.SetCheck(1);
		else
			m_Freq5.SetCheck(0);
		if( strchr(pclFreq,'6') )
			m_Freq6.SetCheck(1);
		else
			m_Freq6.SetCheck(0);
		if( strchr(pclFreq,'7') )
			m_Freq7.SetCheck(1);
		else
			m_Freq7.SetCheck(0);
	}
}


void CAllocatedDlg::UpdateDates(void)
{
	if(m_VAFRdateCtrl.GetStatus() && m_VAFRtimeCtrl.GetStatus() && m_VATOdateCtrl.GetStatus() && m_VATOtimeCtrl.GetStatus())
	{
		CString olVafrDate,olVafrTime,olVatoDate,olVatoTime,olFreq;

		olFreq.Empty();
		if(m_Freq1.GetCheck() == 1 )
			olFreq += "1";
		if(m_Freq2.GetCheck() == 1 )
			olFreq += "2";
		if(m_Freq3.GetCheck() == 1 )
			olFreq += "3";
		if(m_Freq4.GetCheck() == 1 )
			olFreq += "4";
		if(m_Freq5.GetCheck() == 1 )
			olFreq += "5";
		if(m_Freq6.GetCheck() == 1 )
			olFreq += "6";
		if(m_Freq7.GetCheck() == 1 )
			olFreq += "7";

		m_VAFRdateCtrl.GetWindowText(olVafrDate);
		m_VAFRtimeCtrl.GetWindowText(olVafrTime);
		m_VATOdateCtrl.GetWindowText(olVatoDate);
		m_VATOtimeCtrl.GetWindowText(olVatoTime);

		omCalTableViewer.Update(olVafrDate,olVafrTime,olVatoDate,olVatoTime,olFreq,pomCalTable->GetCurrentLine());
	}
}
