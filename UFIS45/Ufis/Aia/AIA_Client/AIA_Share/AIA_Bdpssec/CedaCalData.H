#ifndef __CEDACAL_DATA__
#define __CEDACAL_DATA__



static void CedaCalDataCf(void *popInstance, int ipDDXType,
						   void *vpDataPointer, CString &ropInstanceName);

#include "CCSGlobl.h"
#include "CCSCedaData.h"
/////////////////////////////////////////////////////////////////////////////
struct CALDATA {

	char	URNO[URNOLEN];
	char	FSEC[URNOLEN];
	CTime	VAFR;
	CTime	VATO;
	char	FREQ[FREQLEN];

	int IsChanged;

	CALDATA(void)
	{
		strcpy(URNO, "");
		strcpy(FSEC, "");
		VAFR = TIMENULL;
		VATO = TIMENULL;
		strcpy(FREQ, "");
		IsChanged = DATA_UNCHANGED;
	}

};


#define ORA_NOT_FOUND "ORA-01403"

class CedaCalData: public CCSCedaData
{

// Attributes
public:
    CCSPtrArray <CALDATA> omData;
	CMapStringToPtr omUrnoMap;
	CMapStringToPtr omFsecMap;
	CCSPtrArray <CEDARECINFO> omRetRecInfo; // contains record info for reading CRU return values

// Operations
public:
    CedaCalData();
	~CedaCalData();

	void ClearAll(void);

    bool Read();
	bool NewCal( CCSPtrArray <CALDATA> &ropCalRecs, char *pcpErrorMessage);
	bool UpdCal( CCSPtrArray <CALDATA> &ropCalRecs, char *pcpErrorMessage);
	bool DelCal( CCSPtrArray <CALDATA> &ropCalRecs, char *pcpErrorMessage);
	bool Add(CALDATA *prpCal);
	bool Insert(CALDATA *prpCal);
	bool Update(CALDATA *prpCal);
	bool DeleteByUrno( const char *pcpUrno);
	bool DeleteByFsec( const char *pcpFsec);

	CALDATA* GetCalByUrno(char *pcpUrno);
	CALDATA* GetCalByFsec(char *pcpFsec);
	int GetDatesByFsec(const char *pcpFsec, CString &ropVafr, CString &ropVato, CString &ropFreq);
	bool DateIsValidForFsec(const char *pcpFsec);

};

extern CedaCalData ogCedaCalData;

#endif //__CEDACAL_DATA__
