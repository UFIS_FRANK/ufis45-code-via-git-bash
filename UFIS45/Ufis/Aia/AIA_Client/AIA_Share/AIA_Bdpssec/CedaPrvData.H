#ifndef __CEDAPRVDATA__
#define __CEDAPRVDATA__



static void CedaPrvDataCf(void *popInstance, int ipDDXType,
						   void *vpDataPointer, CString &ropInstanceName);

#include "CCSGlobl.h"
#include "CCSCedaData.h"

/////////////////////////////////////////////////////////////////////////////
struct PRVDATA {

	char	URNO[URNOLEN];
	char	FSEC[URNOLEN];
	char	FFKT[URNOLEN];
	char	FAPP[URNOLEN];
	char	STAT[STATLEN];

	char	tmpSTAT[STATLEN];
	int IsChanged;

	PRVDATA(void)
	{
		strcpy(URNO, "");
		strcpy(FSEC, "");
		strcpy(FFKT, "");
		strcpy(FAPP, "");
		strcpy(STAT, "");
		strcpy(tmpSTAT, "");
		IsChanged = DATA_UNCHANGED;
	}

};

#define ORA_NOT_FOUND "ORA-01403"


class CedaPrvData: public CCSCedaData
{

// Attributes
public:

    CCSPtrArray <PRVDATA> omData;
	CCSPtrArray <CEDARECINFO> omRetRecInfo; // contains record info for reading CRU return values
	CMapStringToPtr omUrnoMap;
	CMapStringToPtr omFsecMap;
	char pcmErrorMessage[ERRMESSLEN];


// Operations
public:

    CedaPrvData();
	~CedaPrvData();

	void ClearAll(void);
	bool Read(void);
	bool Add(PRVDATA *prpPrv);
	PRVDATA* GetPrvByUrno(const char *pcpUrno);
	bool SetPrv( const char *pcpDataList, char *pcpErrorMessage);
	bool AddApp( char  *pcpFapp, char *pcpFsec, char *pcpErrorMessage);
	bool RemApp( char  *pcpFapp, char *pcpFsec, char *pcpErrorMessage);
	bool UpdApp( char  *pcpFapp, char *pcpFsec, char *pcpErrorMessage);
	bool Insert(PRVDATA *prpPrv);
	bool Update(const char *pcpUrno, const char *pcpStat);
	bool DeleteByUrno( const char  *pcpUrno );
	bool DeleteByFsec( const char  *pcpFsec );
	bool DeleteByFapp( const char  *pcpFapp );
	bool DeleteByFsecFapp( const char *pcpFsec, const char  *pcpFapp );
	INSTALLED_STAT ApplDefined( const char *pcpFsec, const char *pcpFapp );

	void AddToFsecMap(PRVDATA *prpPrv);
	void GetPrvListByFsec(const char *pcpFsec, CCSPtrArray <PRVDATA> &ropPrvList, bool bpReset=true);
	void ClearFsecMap();
	bool DeleteFromFsecMapByFsec(const char *pcpFsec);
	void DeleteFromFsecMapByUrno(const char *pcpFsec, const char *pcpUrno);

};

extern CedaPrvData ogCedaPrvData;

#endif //__CEDAPRVDATA__
