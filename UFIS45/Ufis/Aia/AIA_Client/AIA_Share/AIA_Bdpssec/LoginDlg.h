// LoginDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CLoginDialog dialog

#ifndef __LOGINDLG__
#define __LOGINDLG__
#include "resource.h"
#include "CCSEdit.h"

#define MAX_LOGIN 3

class CLoginDialog : public CDialog 
{
// Construction
public:

	//@ManDoc: Constructor.
    CLoginDialog(const char *pcpHomeAirport, const char *pcpAppl, const char *pcpWorkstation, CWnd* pParent = NULL );     // standard constructor
	bool bmIsSuperUser;
// Dialog Data
    //{{AFX_DATA(CLoginDialog)
	enum { IDD = IDD_LOGIN };
	CCSEdit	m_UsernameCtrl;
	CStatic	m_PassCaption;
	CButton	m_OkCaption;
	CButton	m_CancelCaption;
	CCSEdit	m_PasswordCtrl;
	CString	m_UsidCaption;
	//}}AFX_DATA


	// to change the error messages, change these strings
//	CString INVALID_USERNAME;
//	CString INVALID_APPLICATION;
//	CString INVALID_PASSWORD;
//	CString EXPIRED_USERNAME;
//	CString EXPIRED_APPLICATION;
//	CString EXPIRED_WORKSTATION;
//	CString DISABLED_USERNAME;
//	CString DISABLED_APPLICATION;
//	CString DISABLED_WORKSTATION;
//	CString UNDEFINED_PROFILE;
//	CString MESSAGE_BOX_CAPTION;
//	CString USERNAME_CAPTION;
//	CString PASSWORD_CAPTION;
//	CString OK_CAPTION;
//	CString CANCEL_CAPTION;

private:
	int	imLoginCount;
	char pcmHomeAirport[20], pcmAppl[20], pcmWorkstation[20];

// Implementation
protected:
    virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

    // Generated message map functions
    //{{AFX_MSG(CLoginDialog)
    afx_msg void OnPaint();
	virtual void OnOK();
	virtual void OnCancel();
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
    DECLARE_MESSAGE_MAP()
};


#endif // __LOGINDLG__
