// EditUserDlg.cpp : implementation file
//
#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#include "stdafx.h"
#include "CCSGlobl.h"
#include "BDPS_SEC.h"

#include "MessList.h"
#include "CedaSecData.h"
#include "EditUserDlg.h"
#include "CCSEdit.h"
#include "CCSTable.h"
#include "CalTableViewer.h"


extern char *pogMainScreenDlg;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CEditUserDlg dialog


CEditUserDlg::CEditUserDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CEditUserDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CEditUserDlg)
	m_USID = _T("");
	m_STAT = FALSE;
	m_VAFRdate = _T("");
	m_VAFRtime = _T("");
	m_VATOdate = _T("");
	m_VATOtime = _T("");
	m_ModuleType = -1;
	m_UsidCaption = _T("");
	m_NAME = _T("");
	m_REMA = _T("");
	m_CanUseSec = FALSE;
	//}}AFX_DATA_INIT

	pomCalTable = new CCSTable; // calendar list
	pomCalTable->SetStyle(WS_BORDER);
	pomCalTable->SetSelectMode(0);
}

//
// ~CEditUserDlg()
//
// Destructor
//
CEditUserDlg::~CEditUserDlg(void)
{
	
} // end ~CEditUserDlg()

void CEditUserDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CEditUserDlg)
	DDX_Control(pDX, IDC_CAN_USE_BDSPSEC, m_CanUseSecCtrl);
	DDX_Control(pDX, IDC_FREQ7, m_Freq7);
	DDX_Control(pDX, IDC_FREQ6, m_Freq6);
	DDX_Control(pDX, IDC_FREQ5, m_Freq5);
	DDX_Control(pDX, IDC_FREQ4, m_Freq4);
	DDX_Control(pDX, IDC_FREQ3, m_Freq3);
	DDX_Control(pDX, IDC_FREQ2, m_Freq2);
	DDX_Control(pDX, IDC_FREQ1, m_Freq1);
	DDX_Control(pDX, IDC_USID_HEAD, m_UsidCaptionCtrl);
	DDX_Control(pDX, IDC_STAT_HEAD, m_StatHead);
	DDX_Control(pDX, IDC_InitialRightsGroup, m_InitialRightsGroup);
	DDX_Control(pDX, IDC_STAT, m_StatCtrl);
	DDX_Control(pDX, IDOK, m_OkButton);
	DDX_Control(pDX, IDCANCEL, m_CancelButton);
	DDX_Control(pDX, IDC_None, m_NoneCtrl);
	DDX_Control(pDX, IDC_Standard, m_StandardCtrl);
	DDX_Control(pDX, IDC_DisableAll, m_DisableAllCtrl);
	DDX_Control(pDX, IDC_EnableAll, m_EnableAllCtrl);
	DDX_Text(pDX, IDC_USID, m_USID);
	DDV_MaxChars(pDX, m_USID, 32);
	DDX_Check(pDX, IDC_STAT, m_STAT);
	DDX_Text(pDX, IDC_VAFRdate, m_VAFRdate);
	DDV_MaxChars(pDX, m_VAFRdate, 10);
	DDX_Text(pDX, IDC_VAFRtime, m_VAFRtime);
	DDV_MaxChars(pDX, m_VAFRtime, 5);
	DDX_Text(pDX, IDC_VATOdate, m_VATOdate);
	DDV_MaxChars(pDX, m_VATOdate, 10);
	DDX_Text(pDX, IDC_VATOtime, m_VATOtime);
	DDV_MaxChars(pDX, m_VATOtime, 5);
	DDX_Radio(pDX, IDC_None, m_ModuleType);
	DDX_Text(pDX, IDC_USID_HEAD, m_UsidCaption);
	DDX_Control(pDX, IDC_USID, m_UsidCtrl);
	DDX_Control(pDX, IDC_VAFRdate, m_VAFRdateCtrl);
	DDX_Control(pDX, IDC_VAFRtime, m_VAFRtimeCtrl);
	DDX_Control(pDX, IDC_VATOdate, m_VATOdateCtrl);
	DDX_Control(pDX, IDC_VATOtime, m_VATOtimeCtrl);
	DDX_Control(pDX, IDC_NAME, m_NameCtrl);
	DDX_Control(pDX, IDC_REMA, m_RemaCtrl);
	DDX_Text(pDX, IDC_NAME, m_NAME);
	DDX_Text(pDX, IDC_REMA, m_REMA);
	DDX_Check(pDX, IDC_CAN_USE_BDSPSEC, m_CanUseSec);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CEditUserDlg, CDialog)
	//{{AFX_MSG_MAP(CEditUserDlg)
	ON_BN_CLICKED(IDC_STAT, OnStat)
	ON_BN_CLICKED(IDC_FREQ1, OnFreq1)
	ON_BN_CLICKED(IDC_FREQ2, OnFreq2)
	ON_BN_CLICKED(IDC_FREQ3, OnFreq3)
	ON_BN_CLICKED(IDC_FREQ4, OnFreq4)
	ON_BN_CLICKED(IDC_FREQ5, OnFreq5)
	ON_BN_CLICKED(IDC_FREQ6, OnFreq6)
	ON_BN_CLICKED(IDC_FREQ7, OnFreq7)
	ON_BN_CLICKED(IDC_FREQ_ALL, OnFreqAll)
	ON_MESSAGE(WM_TABLE_LBUTTONDOWN,OnTableLButtonDown)
	ON_MESSAGE(WM_TABLE_RBUTTONDOWN,OnTableRButtonDown)
	ON_MESSAGE(WM_TABLE_SELCHANGE,OnTableSelChange)
	ON_MESSAGE(WM_EDIT_KILLFOCUS,OnEditKillFocus)
	ON_COMMAND(WM_INSERT_DATE,OnInsertDate)
	ON_COMMAND(WM_DELETE_DATE,OnDeleteDate)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


BOOL CEditUserDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

	m_UsidCtrl.SetTypeToString("X(32)",32,1);
	m_UsidCtrl.SetBKColor(YELLOW);
	m_UsidCtrl.SetTextErrColor(RED);
	m_UsidCtrl.SetWindowText(omUsid);
	m_UsidCtrl.SetFocus();

	m_NameCtrl.SetTypeToString("X(32)",32,0);
	m_NameCtrl.SetBKColor(WHITE);
	m_NameCtrl.SetTextErrColor(RED);
	m_NameCtrl.SetWindowText(omName);

	m_VAFRdateCtrl.SetTypeToDate(true);
	m_VAFRdateCtrl.SetBKColor(YELLOW);
	m_VAFRdateCtrl.SetTextErrColor(RED);

	m_VAFRtimeCtrl.SetTypeToTime(true);
	m_VAFRtimeCtrl.SetBKColor(YELLOW);
	m_VAFRtimeCtrl.SetTextErrColor(RED);

	m_VATOdateCtrl.SetTypeToDate(true);
	m_VATOdateCtrl.SetBKColor(YELLOW);
	m_VATOdateCtrl.SetTextErrColor(RED);

	m_VATOtimeCtrl.SetTypeToTime(true);
	m_VATOtimeCtrl.SetBKColor(YELLOW);
	m_VATOtimeCtrl.SetTextErrColor(RED);

	m_RemaCtrl.SetTypeToString("X(80)",80,0);
	m_RemaCtrl.SetBKColor(WHITE);
	m_RemaCtrl.SetTextErrColor(RED);
	m_RemaCtrl.SetWindowText(omRema);



	omGreenBitmap.LoadBitmap(IDB_TL_GREEN);
	omAmberBitmap.LoadBitmap(IDB_TL_AMBER);
	omRedBitmap.LoadBitmap(IDB_TL_RED);



	CRect rect;
	rect.left = 13;
	rect.right = 383;
	rect.top = 310;
	rect.bottom = 430;
	pomCalTable->SetTableData(this, rect.left, rect.right, rect.top, rect.bottom);
	omCalTableViewer.Attach(pomCalTable);

	omCalTableViewer.omSelectedFsec = omUrno;
	omCalTableViewer.omFsecList = CString("|") + omUrno + CString("|");
	omCalTableViewer.ChangeViewTo();
	omCalTableViewer.UpdateDisplay();
	if(omCalTableViewer.omLines.GetSize() <= 0)
		OnInsertDate();
	else
		SelectDate(0); // select the first line in the date list

	if( bmIsUpdate || rgCurrType == SEC_APPL )
	{
		// for update or application disable the "Initial Rights" buttons
		m_NoneCtrl.EnableWindow(false);
		m_StandardCtrl.EnableWindow(false);
		m_DisableAllCtrl.EnableWindow(false);
		m_EnableAllCtrl.EnableWindow(false);
	}

	
	// set the username field and dialog caption
	switch( rgCurrType ) {
		case SEC_PROFILE:
			m_UsidCaptionCtrl.SetWindowText(GetString(IDS_EDITUSERDLG_PROFILENAME));
			omEnabledStat = GetString(IDS_EDITUSERDLG_PROFILEENABLED);
			omDisabledStat = GetString(IDS_EDITUSERDLG_PROFILEDISABLED);
			if( bmIsUpdate )
				omWindowCaption = GetString(IDS_EDITUSERDLG_PROFILEUPDATE);
			else
				omWindowCaption = GetString(IDS_EDITUSERDLG_PROFILEINSERT);
			break;
		case SEC_GROUP:
			m_UsidCaptionCtrl.SetWindowText(GetString(IDS_EDITUSERDLG_GROUPNAME));
			omEnabledStat = GetString(IDS_EDITUSERDLG_GROUPENABLED);
			omDisabledStat = GetString(IDS_EDITUSERDLG_GROUPDISABLED);
			if( bmIsUpdate )
				omWindowCaption = GetString(IDS_EDITUSERDLG_GROUPUPDATE);
			else
				omWindowCaption = GetString(IDS_EDITUSERDLG_GROUPINSERT);
			break;
		case SEC_WKS:
			m_UsidCaptionCtrl.SetWindowText(GetString(IDS_EDITUSERDLG_WKSNAME));
			omEnabledStat = GetString(IDS_EDITUSERDLG_WKSENABLED);
			omDisabledStat = GetString(IDS_EDITUSERDLG_WKSDISABLED);
			if( bmIsUpdate )
				omWindowCaption = GetString(IDS_EDITUSERDLG_WKSUPDATE);
			else
				omWindowCaption = GetString(IDS_EDITUSERDLG_WKSINSERT);
			break;
		case SEC_WKSGROUP:
			m_UsidCaptionCtrl.SetWindowText(GetString(IDS_EDITUSERDLG_WKSGROUPNAME));
			omEnabledStat = GetString(IDS_EDITUSERDLG_WKSGROUPENABLED);
			omDisabledStat = GetString(IDS_EDITUSERDLG_WKSGROUPDISABLED);
			if( bmIsUpdate )
				omWindowCaption = GetString(IDS_EDITUSERDLG_WKSGROUPUPDATE);
			else
				omWindowCaption = GetString(IDS_EDITUSERDLG_WKSGROUPINSERT);
			break;
		case SEC_APPL:
			m_UsidCaptionCtrl.SetWindowText(GetString(IDS_EDITUSERDLG_MODULENAME));
			omEnabledStat = GetString(IDS_EDITUSERDLG_MODULEENABLED);
			omDisabledStat = GetString(IDS_EDITUSERDLG_MODULEDISABLED);
			if( bmIsUpdate )
				omWindowCaption = GetString(IDS_EDITUSERDLG_MODULEUPDATE);
			else
				omWindowCaption = GetString(IDS_EDITUSERDLG_MODULEINSERT);
			break;
		case SEC_USER:
		default:
			m_UsidCaptionCtrl.SetWindowText(GetString(IDS_EDITUSERDLG_USERNAME));
			omEnabledStat = GetString(IDS_EDITUSERDLG_USERENABLED);
			omDisabledStat = GetString(IDS_EDITUSERDLG_USERDISABLED);
			if( bmIsUpdate )
				omWindowCaption = GetString(IDS_EDITUSERDLG_USERUPDATE);
			else
				omWindowCaption = GetString(IDS_EDITUSERDLG_USERINSERT);
	}


	SetWindowText(omWindowCaption); // "Insert User" or "Update Profile" etc

	if(rgCurrType == SEC_USER && bgIsSuperUser)
	{
		m_CanUseSecCtrl.ShowWindow(SW_SHOW);
	}
	else
	{
		m_CanUseSecCtrl.ShowWindow(SW_HIDE);
	}

	if( m_STAT )
	{
		m_StatHead.SetWindowText(omEnabledStat);
		SendDlgItemMessage(IDC_STAT,BM_SETIMAGE,IMAGE_BITMAP,(long)(HBITMAP) omGreenBitmap);
	}
	else
	{
		m_StatHead.SetWindowText(omDisabledStat);
		SendDlgItemMessage(IDC_STAT,BM_SETIMAGE,IMAGE_BITMAP,(long)(HBITMAP) omRedBitmap);
	}


	return FALSE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CEditUserDlg::OnOK() 
{
	UpdateDates();	

	CString olErrText;
	CString olNewLine("\n");
	CWnd	*polFocusWnd = NULL;
	bool blStatus = true;


	// check the USID
	CString omUsid;
	m_UsidCtrl.GetWindowText(omUsid);
	if( m_UsidCtrl.GetStatus() == false)
	{
		olErrText += GetString(IDS_EDITUSERDLG_ERRORUSIDLEN) + olNewLine;
		blStatus = false;
		polFocusWnd = &m_UsidCtrl;
	}
	else
	{
		// Check for duplicate USID:-
		//		On update omOldUsid contains the old USID, if the old and new USID's
		//		are the same then don't need to check for a duplicate USID.
		//		On Insert or Copy the old USID was blank, so duplicate always checked
		if( (!bmIsUpdate || omOldUsid != omUsid) && ogCedaSecData.GetSecByUsid(omUsid,rgCurrType) != NULL )
		{
			olErrText += GetString(IDS_EDITUSERDLG_ERRORDUPLICATEUSID) + olNewLine;
			blStatus = false;
			polFocusWnd = &m_UsidCtrl;
		}
	}


	// if any errors have occured, tell the user and don't exit the mask
	if( ! blStatus )
	{
		// set the focus to the erroneous control
		if( polFocusWnd != NULL )
			polFocusWnd->SetFocus();


		// display an error message
		MessageBox(olErrText,GetString(IDS_EDITUSERDLG_INSERTERRORCAPTION),MB_ICONSTOP);
	}
	else
	{
		omCalTableViewer.CreateCommands(); // creates 3 lists of records inserted,updated and deleted
		CDialog::OnOK();
	}
}



// format date from ddmmyyyy to dd.mm.yyyy
// return true if the date is valid else false
bool CEditUserDlg::FormatDate(CEdit &opDate)
{

	char clTmp[11];
	int ilLen,ilLoop1,ilLoop2;
	CString	olTmp;	// we need CString functionality

	opDate.GetWindowText(olTmp); // copy text from the edit field to a string
	ilLen = olTmp.GetLength();

	// remove all chars except numbers and full-stop
	memset(clTmp,0,sizeof(clTmp));
	for( ilLoop1 = 0, ilLoop2 = 0; ilLoop1 < ilLen; ilLoop1++ )
		if( (olTmp[ilLoop1] >= 48 && olTmp[ilLoop1] <= 57) || olTmp[ilLoop1] == '.' )
			clTmp[ilLoop2++] = olTmp[ilLoop1];

	ilLen = olTmp.GetLength();

	
	if( ilLen == 8 && olTmp.Find('.') == -1 )
	{
		sprintf(clTmp,"%s.%s.%s",olTmp.Left(2),olTmp.Mid(2,2),olTmp.Right(4));
		opDate.SetWindowText(clTmp);
	}


	olTmp = clTmp;
	
	// check if the date format is valid
//???	CCSTime olCheckTime;
//???	if( olTmp.GetLength() != 10 || olCheckTime.DateStringToDate(olTmp) == TIMENULL )
	if( olTmp.GetLength() != 10 || DateStringToDate(olTmp) == TIMENULL )
		return false;
	else
		return true;
}



bool CEditUserDlg::FormatTime(CEdit &opDate)
{

	char clTmp[11];
	int ilLen;
	memset(clTmp,'\0',sizeof(clTmp));

	CString	olTmp;	// we need CString functionality

	opDate.GetWindowText(olTmp); // copy text from the edit field to a string
	ilLen = olTmp.GetLength();

	// remove all chars except numbers and colon
	memset(clTmp,0,sizeof(clTmp));
	for( int ilLoop = 0, ilLoop2 = 0; ilLoop < ilLen; ilLoop++ )
		if( (olTmp[ilLoop] >= 48 && olTmp[ilLoop] <= 57) || olTmp[ilLoop] == ':' )
			clTmp[ilLoop2++] = olTmp[ilLoop];

	olTmp = CString(clTmp);
	ilLen = olTmp.GetLength();
	int ilColonPos = olTmp.Find(':');


	if( ilColonPos == -1 )
	{
		// no colon found
		switch( ilLen )
		{
			case 0:
				// format date from "" to 00:00
				strcpy(clTmp,"00:00");
				break;
			case 1:
				// format date from 1 to 01:00
				sprintf(clTmp,"0%s:00",olTmp.Left(1));
				break;
			case 2:
				// format date from 12 to 12:00
				sprintf(clTmp,"%s:00",olTmp.Left(2));
				break;
			case 3:
				// format date from 123 to 01:23
				sprintf(clTmp,"0%s:%s",olTmp.Left(1),olTmp.Right(2));
				break;
			case 4:
				// format date from 1230 to 12:30
				sprintf(clTmp,"%s:%s",olTmp.Left(2),olTmp.Right(2));
				break;
			default:
				;
		}
	}
	else
	{
		if( ilLen != 5 )
		{
			char pclHours[10],pclMins[10];

			if( ilColonPos > 0 )
				strcpy(pclHours,olTmp.Left(ilColonPos));

			if( ilColonPos < (ilLen-1) )
				strcpy(pclMins,olTmp.Right((ilLen - ilColonPos - 1)));

			int ilHoursLen =  strlen(pclHours);
			if( ilHoursLen != 2 )
			{
				if( ilHoursLen == 1 )
				{
					char clH = pclHours[0];
					sprintf(pclHours,"0%c",clH);
				}
				else
					strcpy(pclHours,"00");
			}
			int ilMinsLen =  strlen(pclMins);
			if( ilMinsLen != 2 )
			{
				if( ilMinsLen == 1 )
					strcat(pclMins,"0");
				else
					strcpy(pclMins,"00");
			}
			sprintf(clTmp,"%s:%s",pclHours,pclMins);
		}
	}

	olTmp = clTmp;

	// if new format then update the edit box
	if( strlen(clTmp) > 0 )
		opDate.SetWindowText(clTmp);
	

	// check if the time format is valid
//???	CCSTime olCheckTime;
//???	if( olTmp.GetLength() != 5 || olCheckTime.HourStringToDate(olTmp) == TIMENULL )
	if( olTmp.GetLength() != 5 || HourStringToDate(olTmp) == TIMENULL )
		return false;
	else
		return true;

}


void CEditUserDlg::OnStat() 
{
	if( m_StatCtrl.GetCheck() ) {
//		SendDlgItemMessage(IDC_STAT,BM_SETIMAGE,IMAGE_BITMAP,(long)(HBITMAP) omAmberBitmap);
		SendDlgItemMessage(IDC_STAT,BM_SETIMAGE,IMAGE_BITMAP,(long)(HBITMAP) omGreenBitmap);
		m_StatHead.SetWindowText(omEnabledStat);
	}
	else {
//		SendDlgItemMessage(IDC_STAT,BM_SETIMAGE,IMAGE_BITMAP,(long)(HBITMAP) omAmberBitmap);
		SendDlgItemMessage(IDC_STAT,BM_SETIMAGE,IMAGE_BITMAP,(long)(HBITMAP) omRedBitmap);
		m_StatHead.SetWindowText(omDisabledStat);
	}
}




void CEditUserDlg::OnFreq1() 
{
	UpdateDates();	
}

void CEditUserDlg::OnFreq2() 
{
	UpdateDates();
}

void CEditUserDlg::OnFreq3() 
{
	UpdateDates();
}

void CEditUserDlg::OnFreq4() 
{
	UpdateDates();
}

void CEditUserDlg::OnFreq5() 
{
	UpdateDates();
}

void CEditUserDlg::OnFreq6() 
{
	UpdateDates();
}

void CEditUserDlg::OnFreq7() 
{
	UpdateDates();
}

void CEditUserDlg::OnFreqAll() 
{
	m_Freq1.SetCheck(1);
	m_Freq2.SetCheck(1);
	m_Freq3.SetCheck(1);
	m_Freq4.SetCheck(1);
	m_Freq5.SetCheck(1);
	m_Freq6.SetCheck(1);
	m_Freq7.SetCheck(1);
	UpdateDates();
}


void CEditUserDlg::SelectDate(int ipLine)
{	
	char pclVafrDate[DATELEN],pclVafrTime[DATELEN],pclVatoDate[DATELEN],pclVatoTime[DATELEN],pclFreq[FREQLEN];
	
	if(omCalTableViewer.SelectLine(pclVafrDate,pclVafrTime,pclVatoDate,pclVatoTime,pclFreq,ipLine))
	{
		m_VAFRdateCtrl.SetWindowText(pclVafrDate);
		m_VAFRtimeCtrl.SetWindowText(pclVafrTime);
		m_VATOdateCtrl.SetWindowText(pclVatoDate);
		m_VATOtimeCtrl.SetWindowText(pclVatoTime);
		if( strchr(pclFreq,'1') )
			m_Freq1.SetCheck(1);
		else
			m_Freq1.SetCheck(0);
		if( strchr(pclFreq,'2') )
			m_Freq2.SetCheck(1);
		else
			m_Freq2.SetCheck(0);
		if( strchr(pclFreq,'3') )
			m_Freq3.SetCheck(1);
		else
			m_Freq3.SetCheck(0);
		if( strchr(pclFreq,'4') )
			m_Freq4.SetCheck(1);
		else
			m_Freq4.SetCheck(0);
		if( strchr(pclFreq,'5') )
			m_Freq5.SetCheck(1);
		else
			m_Freq5.SetCheck(0);
		if( strchr(pclFreq,'6') )
			m_Freq6.SetCheck(1);
		else
			m_Freq6.SetCheck(0);
		if( strchr(pclFreq,'7') )
			m_Freq7.SetCheck(1);
		else
			m_Freq7.SetCheck(0);
	}
}

void CEditUserDlg::UpdateDates(void)
{
	if(m_VAFRdateCtrl.GetStatus() && m_VAFRtimeCtrl.GetStatus() && m_VATOdateCtrl.GetStatus() && m_VATOtimeCtrl.GetStatus())
	{
		CString olVafrDate,olVafrTime,olVatoDate,olVatoTime,olFreq;

		olFreq.Empty();
		if(m_Freq1.GetCheck() == 1 )
			olFreq += "1";
		if(m_Freq2.GetCheck() == 1 )
			olFreq += "2";
		if(m_Freq3.GetCheck() == 1 )
			olFreq += "3";
		if(m_Freq4.GetCheck() == 1 )
			olFreq += "4";
		if(m_Freq5.GetCheck() == 1 )
			olFreq += "5";
		if(m_Freq6.GetCheck() == 1 )
			olFreq += "6";
		if(m_Freq7.GetCheck() == 1 )
			olFreq += "7";

		m_VAFRdateCtrl.GetWindowText(olVafrDate);
		m_VAFRtimeCtrl.GetWindowText(olVafrTime);
		m_VATOdateCtrl.GetWindowText(olVatoDate);
		m_VATOtimeCtrl.GetWindowText(olVatoTime);

		omCalTableViewer.Update(olVafrDate,olVafrTime,olVatoDate,olVatoTime,olFreq,pomCalTable->GetCurrentLine());
	}
}

//
// OnTableRButtonDown()
//
// receives a WM_TABLE_RBUTTONDOWN message from CCSTable
// when the function SelectLine() is called
// if the right mouse button was pressed, displays context sensetive menus
//
// ipParam contains flags indicating which button was pressed
// lpParam contains x,y co-ord where the mouse button was clicked
//
LONG CEditUserDlg::OnTableRButtonDown(UINT ipParam, LONG lpParam)
{
	CCSTABLENOTIFY *polNotify = (CCSTABLENOTIFY *) lpParam;
	int ilLine = polNotify->Line;

	if( polNotify->SourceTable == pomCalTable )
	{
		int ilNumDates = pomCalTable->GetLinesCount();
		CMenu olMenu;
		olMenu.CreatePopupMenu();
		for (int i = olMenu.GetMenuItemCount(); --i >= 0;)
  			olMenu.RemoveMenu(i, MF_BYPOSITION);
		olMenu.AppendMenu(MF_STRING,WM_INSERT_DATE, GetString(IDS_EDITUSERDLG_INSERTDATE));
		if(ilNumDates > 1)
	  		olMenu.AppendMenu(MF_STRING,WM_DELETE_DATE, GetString(IDS_EDITUSERDLG_DELETEDATE));
		else
	  		olMenu.AppendMenu(MF_STRING|MF_GRAYED,WM_DELETE_DATE, GetString(IDS_EDITUSERDLG_DELETEDATE));
		pomCalTable->ClientToScreen(&polNotify->Point);
		olMenu.TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON, polNotify->Point.x, polNotify->Point.y, this, NULL);
	}
	
	return 0L;
}

//
// OnTableLButtonDown()
//
// receives a WM_TABLE_LBUTTONDOWN message from CCSTable
// when the function SelectLine() is called
// if the left mouse button was pressed, calls OnTableSelChange()
//
// ipParam contains flags indicating which button was pressed
// lpParam contains x,y co-ord where the mouse button was clicked
//
LONG CEditUserDlg::OnTableLButtonDown(UINT ipParam, LONG lpParam)
{
	CCSTABLENOTIFY *polNotify = (CCSTABLENOTIFY *) lpParam;
	int ilLine = polNotify->Line;

	if( polNotify->SourceTable == pomCalTable )
	{
		SelectDate(pomCalTable->GetCurrentLine());
	}

	return 0L;
}


//
// OnTableSelChange()
//
// receives a WM_TABLE_SELCHANGE message from CCSTable
//
// ipParam contains flags indicating which button was pressed
// lpParam contains x,y co-ord where the mouse button was clicked
//
LONG CEditUserDlg::OnTableSelChange(UINT ipParam, LONG lpParam)
{
//	AfxMessageBox("OnTableSelChange");

	return 0L;
}


//
// OnEditLostFocus()
//
// receives a WM_EDIT_KILLFOCUS message from CCSEdit field
//
LONG CEditUserDlg::OnEditKillFocus(UINT ipParam, LONG lpParam)
{
	CCSEDITNOTIFY *polNotify = (CCSEDITNOTIFY *) lpParam;

	if( polNotify->Status )
	{
		if( polNotify->SourceControl == &m_VAFRdateCtrl || polNotify->SourceControl == &m_VAFRtimeCtrl ||
			polNotify->SourceControl == &m_VATOdateCtrl || polNotify->SourceControl == &m_VATOtimeCtrl) 
			UpdateDates();
	}

	return 0L;
}




void CEditUserDlg::OnInsertDate()
{
	CString olVafrDate,olVafrTime,olVatoDate,olVatoTime,olFreq;

	CTime olCurrTime = CTime::GetCurrentTime();
	olVafrDate = olCurrTime.Format("%d.%m.%Y");
	olVafrTime = "00:00";
	olCurrTime += CTimeSpan( (LONG) igValidToDays, 0, 0, 0 ); // add n days to VAFR to create VATO
	olVatoDate = olCurrTime.Format("%d.%m.%Y");
	olVatoTime = "00:00";
	olFreq = "1234567";

	int ilNewLine = omCalTableViewer.Insert(olVafrDate,olVafrTime,olVatoDate,olVatoTime,olFreq,omUrno);
	SelectDate(ilNewLine);
}

void CEditUserDlg::OnDeleteDate()
{
	int ilLine = pomCalTable->GetCurrentLine();

	omCalTableViewer.Delete(ilLine);

	if(ilLine < pomCalTable->GetLinesCount())
		SelectDate(ilLine);
	else
		SelectDate(ilLine-1);
}
