// PrintRightsDlg.cpp : implementation file
//

#include "stdafx.h"
#include "bdps_sec.h"
#include "PrintRightsDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CPrintRightsDlg dialog


CPrintRightsDlg::CPrintRightsDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CPrintRightsDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CPrintRightsDlg)
	m_Sel = 1;
	//}}AFX_DATA_INIT
}


void CPrintRightsDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CPrintRightsDlg)
	DDX_Radio(pDX, IDC_ALL, m_Sel);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CPrintRightsDlg, CDialog)
	//{{AFX_MSG_MAP(CPrintRightsDlg)
		// NOTE: the ClassWizard will add message map macros here
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CPrintRightsDlg message handlers
