// FlViewer.h
//
#ifndef __CALTABLEVIEWER_H__
#define __CALTABLEVIEWER_H__

#include "CCSPtrArray.h"
#include "CCSTable.h"
#include "CedaCalData.h"



struct CALTAB_LINEDATA
{
	char			URNO[URNOLEN];
	char			FSEC[URNOLEN];
	CTime			VAFR;
	CTime			VATO;
	char			FREQ[FREQLEN];
	int				stat; //{DATA_UNCHANGED,DATA_NEW,DATA_CHANGED,DATA_DELETED}

	CALTAB_LINEDATA(void)
	{
		memset(URNO,0,URNOLEN);
		memset(FSEC,0,URNOLEN);
		memset(FREQ,0,FREQLEN);
		VAFR = TIMENULL;
		VATO = TIMENULL;
		stat = DATA_UNCHANGED;
	}
};


class CalTableViewer
{
// Constructions
public:
    CalTableViewer();
    ~CalTableViewer();

//    void Attach(CCSTable *popAttachWnd);
    void ChangeViewTo(void);


// Internal data processing routines
public:

    void MakeLines();
	void MakeLine(CALDATA *prpCal);
	int CreateLine(CALTAB_LINEDATA *);
	void DeleteAll();
	void DeleteLine(int);

	BOOL IsPassFilter(const char *);
	int CompareLine(CALTAB_LINEDATA *,CALTAB_LINEDATA *);
	int	 GetSize();
	void Update(char *pcpUsid);
	void UpdateDisplay(int *pipInsertedLineNum = NULL);
	void Attach(CCSTable *popTable);
	void Delete(const int ipLine);
	int Insert(const char *pcpVafrDate, const char *pcpVafrTime, const char *pcpVatoDate, const char *pcpVatoTime, const char *pcpFreq, const char *pcpFsec);
	void Update(const char *pcpVafrDate, const char *pcpVafrTime, const char *pcpVatoDate, const char *pcpVatoTime, const char *pcpFreq, int ipLine );
	bool SelectLine(char *pcpVafrDate, char *pcpVafrTime,char *pcpVatoDate, char *pcpVatoTime,char *pcpFreq, int ipLine );
	void CreateCommands(void);

// Attributes
public:
	CCSTable	*pomTable;
    CCSPtrArray <CALTAB_LINEDATA> omLines;
	CCSPtrArray <int> omDisplayedLines; // indicies into omLines of displayed lines
	CString		omFsecList;
	CString		omSelectedFsec;
	bool		bmAllocOnly;
	CCSPtrArray <CALDATA> omDeletedList;
	CCSPtrArray <CALDATA> omUpdatedList;
	CCSPtrArray <CALDATA> omInsertedList;
	int GetNumLinesDisplayed(void);
	int GetLineNum(const int ipLine);
};

#endif //__CALTABLEVIEWER_H__
