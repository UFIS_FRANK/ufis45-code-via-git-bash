rem Copies all generated files from the SCM-Structue into the Install-Shield directory.


rem set drive to J: (IS 6.2) or K: (IS 8.0) 
set drive=k:

rem Copy ini- and config-files
copy c:\ufis_bin\ceda.ini %drive%\aia_nrq_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\bdps_sec.cfg %drive%\aia_nrq_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\loatab*.* %drive%\aia_nrq_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\telexpool.ini %drive%\aia_nrq_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\importtool.ini %drive%\aia_nrq_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\configgen.cfg %drive%\aia_nrq_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\texttable.txt %drive%\aia_nrq_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\staffpage.ini %drive%\aia_nrq_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\remotectrl.ini %drive%\aia_nrq_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\datachange.ini %drive%\aia_nrq_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\*.bmp %drive%\aia_nrq_build\ufis_client_appl\system\*.*

rem Copy applications
copy c:\ufis_bin\debug\bdps_sec.exe %drive%\aia_nrq_build\ufis_client_appl\applications\*.*
copy c:\ufis_bin\debug\bdpspass.exe %drive%\aia_nrq_build\ufis_client_appl\applications\*.*
copy c:\ufis_bin\debug\bdpsuif.exe %drive%\aia_nrq_build\ufis_client_appl\applications\*.*
copy c:\ufis_bin\debug\fips.exe %drive%\aia_nrq_build\ufis_client_appl\applications\*.*
copy c:\ufis_bin\debug\rules.exe %drive%\aia_nrq_build\ufis_client_appl\applications\*.*
copy c:\ufis_bin\debug\cuteif.exe %drive%\aia_nrq_build\ufis_client_appl\applications\*.*
copy c:\ufis_bin\release\fidas.exe %drive%\aia_nrq_build\ufis_client_appl\applications\*.*
copy c:\ufis_bin\release\telexpool.exe %drive%\aia_nrq_build\ufis_client_appl\applications\*.*
copy c:\ufis_bin\release\loadtabviewer.exe %drive%\aia_nrq_build\ufis_client_appl\applications\*.*
copy c:\ufis_bin\release\importflights.exe %drive%\aia_nrq_build\ufis_client_appl\applications\*.*
copy c:\ufis_bin\release\daco3tool.exe %drive%\aia_nrq_build\ufis_client_appl\applications\*.*
copy c:\ufis_bin\release\configgen.exe %drive%\aia_nrq_build\ufis_client_appl\applications\*.*
copy c:\ufis_bin\release\export.exe %drive%\aia_nrq_build\ufis_client_appl\applications\*.*
copy c:\ufis_bin\release\remotectrl.exe %drive%\aia_nrq_build\ufis_client_appl\applications\*.*
copy c:\ufis_bin\release\ruleschecker.exe %drive%\aia_nrq_build\ufis_client_appl\applications\*.*
copy c:\ufis_bin\release\staffpage.exe %drive%\aia_nrq_build\ufis_client_appl\applications\*.*
copy c:\ufis_bin\release\logtab_viewer.exe %drive%\aia_nrq_build\ufis_client_appl\applications\*.*

REM Copy system-files
copy c:\ufis_bin\release\bcproxy.exe %drive%\aia_nrq_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\bcproxy.tlb %drive%\aia_nrq_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\bcserv32.exe %drive%\aia_nrq_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\ufis32.dll %drive%\aia_nrq_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\ufisappmng.exe %drive%\aia_nrq_build\ufis_client_appl\system\*.*
copy c:\ufis\system\ufisappmng.tlb %drive%\aia_nrq_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\tab.ocx %drive%\aia_nrq_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\tab.tlb %drive%\aia_nrq_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\ufiscom.ocx %drive%\aia_nrq_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\ufiscom.tlb %drive%\aia_nrq_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\ugantt.ocx %drive%\aia_nrq_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\ugantt.tlb %drive%\aia_nrq_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\ucom.ocx %drive%\aia_nrq_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\ucom.tlb %drive%\aia_nrq_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\aatlogin.ocx %drive%\aia_nrq_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\aatlogin.tlb %drive%\aia_nrq_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\ufisbroadcasts.ocx %drive%\aia_nrq_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\ufisbroadcasts.tlb %drive%\aia_nrq_build\ufis_client_appl\system\*.*

Rem Copy .NET components
copy c:\ufis_bin\release\*lib.dll %drive%\aia_nrq_build\ufis_client_appl\applications\datachanges\*.*
copy c:\ufis_bin\release\ufis.utils.dll %drive%\aia_nrq_build\ufis_client_appl\applications\datachanges\*.*
copy c:\ufis_bin\release\data_changes.exe %drive%\aia_nrq_build\ufis_client_appl\applications\datachanges\*.*

rem Copy Help-Files
rem copy c:\ufis_bin\Help\*.chm %drive%\aia_nrq_build\ufis_client_appl\help\*.*


rem Copy ReleaseNotes
rem copy c:\ufis_bin\releasenotes.zip %drive%\aia_nrq_build\ufis_client_appl\rel_notes\*.*
