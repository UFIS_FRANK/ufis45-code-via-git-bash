Rem BEGIN
date /T
time /T 

Echo Build AIA_NRQ Clients > con


Rem ---Check for folder c:\ufis\system---
IF EXIST c:\ufis\system\nul GOTO cont3
Echo Create folder C:\ufis\system > con
md c:\ufis\system
:cont3

Rem ---Check for folder c:\ufis_Bin\Runtimedlls---
IF EXIST C:\Ufis_Bin\Runtimedlls\nul GOTO cont4
Echo Create folder C:\ufis_Bin\Runtimedlls > con
md C:\Ufis_Bin\Runtimedlls
:cont4

IF EXIST C:\tmp\nul goto cont5
Echo Create folder C:\tmp > con
md C:\tmp
:cont5

ECHO Project AIA Next Requirements 4.5  > C:\Ufis_Bin\AIA45.txt

mkdir c:\Ufis_Bin\ClassLib
mkdir c:\Ufis_Bin\ClassLib\Include
mkdir c:\Ufis_Bin\ClassLib\Include\jm
mkdir C:\Ufis_Bin\ForaignLibs
mkdir C:\Ufis_Bin\Help

copy ..\..\..\_Standard\_Standard_client\_Standard_share\_Standard_Regularexpressionlib\lib\*.lib C:\Ufis_Bin\ForaignLibs
copy ..\..\..\_Standard\_Standard_client\_Standard_share\_Standard_Regularexpressionlib\lib\*.pdb C:\Ufis_Bin\ForaignLibs
copy ..\..\..\_Standard\_Standard_client\_Standard_share\_Standard_Regularexpressionlib\lib\*.dll C:\Ufis_Bin\Runtimedlls
copy ..\..\..\_Standard\_Standard_client\_Standard_configuration\Clientruntime\*.* C:\Ufis_Bin\Runtimedlls
copy ..\..\..\_Standard\_Standard_client\_Standard_share\_Standard_Regularexpressionlib\include\*.h c:\Ufis_Bin\ClassLib\Include
copy ..\..\..\_Standard\_Standard_client\_Standard_share\_Standard_Regularexpressionlib\include\jm\*.h c:\Ufis_Bin\ClassLib\Include\jm
copy ..\..\..\_Standard\_Standard_client\_Standard_share\_Standard_ClassLib\*.h c:\Ufis_Bin\ClassLib\Include
copy ..\..\..\_Standard\_Standard_client\_Standard_share\_Standard_Wrapper\*.h c:\Ufis_Bin\ClassLib\Include

copy ..\..\..\_Standard\_Standard_client\_Standard_share\_Standard_Classlib\*.h c:\Ufis_Bin\ClassLib\Include
copy ..\..\..\_Standard\_Standard_client\_Standard_share\_Standard_Classlib\Classlib.rc c:\Ufis_Bin\ClassLib\Include

copy ..\..\..\_Standard\_Standard_client\_Standard_share\_Standard_HtmlHelp\lib\*.lib C:\Ufis_Bin\ForaignLibs
copy ..\..\..\_Standard\_Standard_client\_Standard_share\_Standard_HtmlHelp\include\*.h c:\Ufis_Bin\ClassLib\Include
copy ..\..\..\_Standard\_Standard_client\_Standard_help\*.chm c:\Ufis_Bin\help


copy ..\AIA_configuration\ceda.ini c:\Ufis_Bin
copy ..\AIA_configuration\telexpool.ini c:\Ufis_Bin
copy ..\AIA_configuration\Bdps_sec.cfg c:\Ufis_Bin
copy ..\AIA_configuration\LoaTab*.* c:\Ufis_Bin
copy ..\AIA_Configuration\StaffPage.ini c:\Ufis_Bin
copy ..\AIA_Configuration\RemoteCtrl.ini c:\Ufis_Bin
copy ..\AIA_Configuration\ConfigGen.cfg c:\Ufis_Bin
copy ..\AIA_Configuration\TextTable.txt c:\Ufis_Bin
copy ..\AIA_configuration\DATACHANGE.ini c:\Ufis_Bin

copy ..\AIA_configuration\*.bmp c:\Ufis_Bin

copy copy_for_install.bat c:\Ufis_Bin


REM ------------------Cients build from _Standard-Project-----------------------------------

REM ------------------------Share-----------------------------------------------------------

cd ..\..\..\_Standard\_Standard_client

ECHO Build Ufis32.dll... > con
cd _Standard_share\_Standard_Ufis32dll
msdev Ufis32.dsp /MAKE "Ufis32 - Debug" /REBUILD
msdev Ufis32.dsp /MAKE "Ufis32 - Release" /REBUILD
If exist c:\Ufis_Bin\Debug\Ufis32.ilk del c:\Ufis_Bin\Debug\Ufis32.ilk
If exist c:\Ufis_Bin\Debug\Ufis32.pdb del c:\Ufis_Bin\Debug\Ufis32.pdb
If exist c:\Ufis_Bin\Debug\Ufis32.pdb del c:\Ufis_Bin\Debug\Ufis32.exp
If exist c:\Ufis_Bin\Release\Ufis32.ilk del c:\Ufis_Bin\Release\Ufis32.ilk
If exist c:\Ufis_Bin\Release\Ufis32.pdb del c:\Ufis_Bin\Release\Ufis32.pdb
If exist c:\Ufis_Bin\Release\Ufis32.pdb del c:\Ufis_Bin\Release\Ufis32.exp
copy c:\ufis_bin\release\Ufis32.dll c:\ufis_bin\debug\*.*
copy c:\ufis_bin\release\Ufis32.lib c:\ufis_bin\debug\*.*
cd ..\..


cd ..\..\AIA\AIA_Client

ECHO Build Classlib... > con
cd AIA_Share\AIA_ClassLib
msdev CCSClass.dsp /MAKE "CCSClass - Win32 Debug" /REBUILD
msdev CCSClass.dsp /MAKE "CCSClass - Win32 Release" /REBUILD
msdev CCSClass.dsp /MAKE "CCSClass - Win32 Dll Debug" /REBUILD
msdev CCSClass.dsp /MAKE "CCSClass - Win32 Dll Release" /REBUILD
cd ..\..

cd ..\..\_Standard\_Standard_Client


ECHO Build BcServ... > con
cd _Standard_share\_Standard_BcServ
msdev BcServ32.dsp /MAKE "BcServ - Release" /REBUILD
If exist c:\Ufis_Bin\Release\BcServ32.ilk del c:\Ufis_Bin\Release\BcServ32.ilk
If exist c:\Ufis_Bin\Release\BcServ32.pdb del c:\Ufis_Bin\Release\BcServ32.pdb
cd ..\..

ECHO Build BcProxy... > con
cd _Standard_share\_Standard_BcProxy
Rem ---remove read-only state---
attrib *.* -r
msdev BcProxy.dsp /MAKE "BcProxy - Win32 Release" /REBUILD
If exist c:\Ufis_Bin\Release\BcProxy.ilk del c:\Ufis_Bin\Release\BcProxy.ilk
If exist c:\Ufis_Bin\Release\BcProxy.pdb del c:\Ufis_Bin\Release\BcProxy.pdb
cd ..\..

ECHO Build Tabocx... > con
cd _Standard_share\_Standard_TABocx
msdev TAB.dsp /MAKE "TAB - Win32 Release" /REBUILD
If exist c:\Ufis_Bin\Release\TAB.ilk del c:\Ufis_Bin\Release\TAB.ilk
If exist c:\Ufis_Bin\Release\TAB.pdb del c:\Ufis_Bin\Release\TAB.pdb
cd ..\..

ECHO Build UfisCedaocx... > con
cd _Standard_share\_Standard_Ufiscedaocx
msdev UfisCom.dsp /MAKE "UfisCom - Win32 Release" /REBUILD
If exist c:\Ufis_Bin\Release\UfisCom.ilk del c:\Ufis_Bin\Release\UfisCom.ilk
If exist c:\Ufis_Bin\Release\UfisCom.pdb del c:\Ufis_Bin\Release\UfisCom.pdb
cd ..\..

ECHO Build UComocx... > con
cd _Standard_Share\_Standard_UCom
msdev UCom.dsp /MAKE "UCom - Win32 Release" /REBUILD
If exist c:\Ufis_Bin\Release\UCom.ilk del c:\Ufis_Bin\Release\UCom.ilk
If exist c:\Ufis_Bin\Release\UCom.pdb del c:\Ufis_Bin\Release\UCom.pdb
cd ..\..

ECHO Build Ganttocx... > con
cd _Standard_share\_Standard_Ganttocx
msdev UGantt.dsp /MAKE "UGantt - Win32 Release" /REBUILD
If exist c:\Ufis_Bin\Release\UGantt.ilk del c:\Ufis_Bin\Release\UGantt.ilk
If exist c:\Ufis_Bin\Release\UGantt.pdb del c:\Ufis_Bin\Release\UGantt.pdb
cd ..\..

ECHO Build AatLoginocx... > con
cd _Standard_Share\_Standard_Login
msdev AatLogin.dsp /MAKE "AatLogin - Win32 Release" /REBUILD
If exist c:\Ufis_Bin\Release\AatLogin.ilk del c:\Ufis_Bin\Release\AatLogin.ilk
If exist c:\Ufis_Bin\Release\AatLogin.pdb del c:\Ufis_Bin\Release\AatLogin.pdb
cd ..\..

ECHO Build UfisAppMng... > con
cd _Standard_share\_Standard_UfisApManager
Rem ---remove read-only state---
attrib *.* -r
msdev UfisAppMng.dsp /MAKE "UfisAppMng - Win32 Release" /REBUILD
copy C:\Ufis_Bin\Release\UfisAppMng.tlb c:\ufis\system
If exist c:\Ufis_Bin\Release\UfisAppMng.ilk del c:\Ufis_Bin\Release\UfisAppMng.ilk
If exist c:\Ufis_Bin\Release\UfisAppMng.pdb del c:\Ufis_Bin\Release\UfisAppMng.pdb
cd ..\..

ECHO Build UfisApplMgr... > con
cd _Standard_share\_Standard_UfisApplMgr
Rem ---remove read-only state---
attrib *.* -r
msdev UfisApplMgr.dsp /MAKE "UfisApplMgr - Win32 Release" /REBUILD
copy C:\Ufis_Bin\Release\UFISApplMgr.tlb c:\ufis\system
If exist c:\Ufis_Bin\Release\UfisApplMgr.ilk del c:\Ufis_Bin\Release\UfisApplMgr.ilk
If exist c:\Ufis_Bin\Release\UfisApplMgr.pdb del c:\Ufis_Bin\Release\UfisApplMgr.pdb
cd ..\..

cd ..\..\AIA\AIA_Client

ECHO Build Fips... > con
cd AIA_Flight\AIA_Fips
msdev FPMS.dsp /MAKE "FPMS - Win32 Debug" /REBUILD
If exist c:\Ufis_Bin\Debug\FIPS.ilk del c:\Ufis_Bin\Debug\FIPS.ilk
If exist c:\Ufis_Bin\Debug\FIPS.pdb del c:\Ufis_Bin\Debug\FIPS.pdb
cd ..\..

GOTO end

ECHO Build BdpsSec... > con
cd _Standard_Share\_Standard_Bdpssec
msdev Bdps_sec.dsp /MAKE "Bdps_sec - Win32 Debug" /REBUILD
If exist c:\Ufis_Bin\Debug\Bdps_sec.ilk del c:\Ufis_Bin\Debug\Bdps_sec.ilk
If exist c:\Ufis_Bin\Debug\Bdps_sec.pdb del c:\Ufis_Bin\Debug\Bdps_sec.pdb
cd ..\..


REM ------------------------Fids------------------------------------------------------------

ECHO Build Fidas... > con
cd _Standard_Fids\_Standard_Fidas
msdev Fidas.dsp /MAKE "Fidas - Win32 Release" /REBUILD
If exist c:\Ufis_Bin\Release\Fidas.ilk del c:\Ufis_Bin\Release\Fidas.ilk
If exist c:\Ufis_Bin\Release\Fidas.pdb del c:\Ufis_Bin\Release\Fidas.pdb
cd ..\..

REM ------------------Cients build from Customer-Project------------------------------------
REM ------------------------Share-----------------------------------------------------------

cd ..\..\AIA\AIA_Client

ECHO Build Ufisbroadcastocx... > con
cd AIA_Share\AIA_Ufisbroadcastocx
msdev Ufisbroadcasts.dsp /MAKE "Ufisbroadcasts - Win32 Release" /REBUILD
If exist c:\Ufis_Bin\Release\Ufisbroadcasts.ilk del c:\Ufis_Bin\Release\Ufisbroadcasts.ilk
If exist c:\Ufis_Bin\Release\Ufisbroadcasts.pdb del c:\Ufis_Bin\Release\Ufisbroadcasts.pdb
cd ..\..

ECHO Build BdpsPass... > con
cd AIA_Share\AIA_Bdpspass
msdev Bdpspass.dsp /MAKE "Bdpspass - Win32 Debug" /REBUILD
If exist c:\Ufis_Bin\Debug\Bdpspass.ilk del c:\Ufis_Bin\Debug\Bdpspass.ilk
If exist c:\Ufis_Bin\Debug\Bdpspass.pdb del c:\Ufis_Bin\Debug\Bdpspass.pdb
cd ..\..

ECHO Build BdpsUif... > con
cd AIA_Share\AIA_Bdpsuif
msdev Bdpsuif.dsp /MAKE "Bdpsuif - Win32 Debug" /REBUILD
If exist c:\Ufis_Bin\Debug\Bdpsuif.ilk del c:\Ufis_Bin\Debug\Bdpsuif.ilk
If exist c:\Ufis_Bin\Debug\Bdpsuif.pdb del c:\Ufis_Bin\Debug\Bdpsuif.pdb
cd ..\..


REM ------------------------Flight--------------------------------------

ECHO Build FipsRules... > con
cd AIA_Flight\AIA_Ruleseditorflight
msdev Rules.dsp /MAKE "Rules - Win32 Debug" /REBUILD
If exist c:\Ufis_Bin\Debug\Rules.ilk del c:\Ufis_Bin\Debug\Rules.ilk
If exist c:\Ufis_Bin\Debug\Rules.pdb del c:\Ufis_Bin\Debug\Rules.pdb
cd ..\..

ECHO Build Fips... > con
cd AIA_Flight\AIA_Fips
msdev FPMS.dsp /MAKE "FPMS - Win32 Debug" /REBUILD
If exist c:\Ufis_Bin\Debug\FIPS.ilk del c:\Ufis_Bin\Debug\FIPS.ilk
If exist c:\Ufis_Bin\Debug\FIPS.pdb del c:\Ufis_Bin\Debug\FIPS.pdb
cd ..\..

ECHO Build FipsCute... > con
cd AIA_Flight\AIA_Fipscute
msdev CuteIF.dsp /MAKE "CuteIF - Win32 Debug" /REBUILD
If exist c:\Ufis_Bin\Debug\CuteIF.ilk del c:\Ufis_Bin\Debug\CuteIF.ilk
If exist c:\Ufis_Bin\Debug\CuteIF.pdb del c:\Ufis_Bin\Debug\CuteIF.pdb
cd ..\..



REM Visal Basic Clients have to be build
REM ------------------------------------

REM --------Build from Project ---------------

REM ImportFlights - build from AIA\ImportExportTool
ECHO Build ImportFlights... > con
cd AIA_Flight\AIA_ImportExportTool
vb6 /make ImportFlights.vbp /out C:\ufis_bin\vbappl.log
cd ..\..

REM Export - build from AIA\Flight\ImportExportTool\VB6ExportTool
ECHO Build ExportTool... > con
cd AIA_Flight\AIA_ImportExportTool\VB6_ExportTool
vb6 /make Export.vbp /out C:\ufis_bin\vbappl.log
cd ..\..\..

REM ConfigGen - build from AIA\Flight\ImportExportTool\ConfigGen
ECHO Build ConfigGen... > con
cd AIA_Flight\AIA_ImportExportTool\ConfigGen
vb6 /make ConfigGen.vbp /out C:\ufis_bin\vbappl.log
cd ..\..\..

REM DaCo3Tool - build from AIA\Flight\DaCo3Tool
ECHO Build DaCo3Tool... > con
cd AIA_Flight\AIA_DaCo3Tool
vb6 /make DaCo3Tool.vbp /out C:\ufis_bin\vbappl.log
cd ..\..

REM StaffPage - build from AIA\Flight\Staffpages\Staffpage
ECHO Build ExportTool... > con
cd AIA_Flight\AIA_Staffpages\StaffPage
vb6 /make Staffpage.vbp /out C:\ufis_bin\vbappl.log
cd ..\..\..

REM Remote Control - build from AIA\Flight\Staffpages\RemoteCtrl
ECHO Build Remote Control... > con
cd AIA_Flight\AIA_Staffpages\RemoteCtrl
vb6 /make RemoteCtrl.vbp /out C:\ufis_bin\vbappl.log
cd ..\..\..

REM RulesChecker - build from AIA\Share\ConsistencyChecker
ECHO Build Consistency Checker... > con
cd AIA_Share\AIA_ConsistencyChecker
vb6 /make RulesChecker.vbp /out C:\ufis_bin\vbappl.log
cd ..\..\..\..

REM --------- Build from Standard -----------------

REM Logging Table Viewer - build from _Standard\_Standard_Share\_Standard_Loggingtableviewer
ECHO Build LoggingTableViewer... > con
cd _Standard\_Standard_Client\_Standard_Share\_Standard_Loggingtableviewer
vb6 /make LogTab_Viewer.vbp /out C:\ufis_bin\vbappl.log
cd ..\..

REM LoadTabViewer - build from _Standard\_Standard_Flight\_Standard_Loatabviewer
ECHO Build LoadTabViewer... > con
cd _Standard_Flight\_Standard_LoaTabViewer
vb6 /make LoadTabViewer.vbp /out C:\ufis_bin\vbappl.log
cd ..\..

REM TelexPool - build from _Standard\_Standard_Flight\_Standard_Telexpool
ECHO Build Telexpool... > con
cd _Standard_Flight\_Standard_Telexpool
vb6 /make Telexpool.vbp /out C:\ufis_bin\vbappl.log
cd ..\..


REM C# Clients have to be build
REM ---------------------------

ECHO Build Ufis.Utils.dll... > con
cd _Standard_UfisAddOn\_Standard_Utilities
devenv Utilities.sln /Rebuild Release /project ufis.utils
cd ..\..

ECHO Build LocationChanges... > con
cd _Standard_UfisAddOn\_Standard_DataChanges\Data_Changes
devenv Data_Changes.sln /Rebuild Release /project Data_Changes
cd ..\..


REM Back to AIA_ClientMake
cd ..\..\..\AIA\AIA_Client\AIA_ClientMake


ECHO ...Done > con

:end
Rem END
time/T

