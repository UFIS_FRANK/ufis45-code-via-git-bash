VERSION 5.00
Begin VB.Form VBHdlcChatter 
   Caption         =   "VBChatter X25 Hdlc"
   ClientHeight    =   4725
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   6345
   LinkTopic       =   "Form1"
   ScaleHeight     =   4725
   ScaleWidth      =   6345
   StartUpPosition =   3  'Windows Default
   Begin VB.ListBox List1 
      Height          =   1035
      ItemData        =   "Form1.frx":0000
      Left            =   330
      List            =   "Form1.frx":0002
      TabIndex        =   7
      Top             =   1650
      Width           =   5745
   End
   Begin VB.CommandButton DisconnectButton 
      Caption         =   "Disconnect"
      Height          =   525
      Left            =   3270
      TabIndex        =   4
      Top             =   3780
      Width           =   2715
   End
   Begin VB.CommandButton ConnectButton 
      Caption         =   "Connect"
      Height          =   525
      Left            =   300
      TabIndex        =   3
      Top             =   3780
      Width           =   2685
   End
   Begin VB.TextBox Text1 
      Height          =   345
      Left            =   330
      TabIndex        =   2
      Top             =   570
      Width           =   5685
   End
   Begin VB.CommandButton ReceiveButton 
      Caption         =   "Check for receive"
      Height          =   495
      Left            =   3300
      TabIndex        =   1
      Top             =   2970
      Width           =   2685
   End
   Begin VB.CommandButton SendButton 
      Caption         =   "Submit"
      Height          =   495
      Left            =   330
      TabIndex        =   0
      Top             =   2970
      Width           =   2655
   End
   Begin VB.Label Label2 
      Caption         =   "Received Messages:"
      Height          =   255
      Left            =   330
      TabIndex        =   6
      Top             =   1050
      Width           =   2055
   End
   Begin VB.Label Label1 
      Caption         =   "Message to send:"
      Height          =   285
      Left            =   330
      TabIndex        =   5
      Top             =   210
      Width           =   1935
   End
End
Attribute VB_Name = "VBHdlcChatter"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
' * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
'
'   VBHdlcChatter
'
'   VBHdlcChatter application used to test the X25-Hdlc connection
'
'   ABB Airport Technologies GmbH   2000
'
'  * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
'
'   Change history:
'   --------------
'
'   05/06/2000      cla AAT/E       Initial version
'
' * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

Dim X25 As New X25HDLCLib.Hdlc

'
' Constants
' This is of course not the original command for the Eicon card
' will be translated by the COM Object
'

Const ConnectCmd = 1
Const SendCmd = 2
Const ReceiveCmd = 3
Const DisconnectCmd = 4

' * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
'
'   SendButton_Click():
'   Action on pressing the send button
'
' * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
Private Sub SendButton_Click()
    Dim cmd As Integer
    Dim length As Long
    Dim data As String
    Dim pos As Integer
    Dim str As String
    Dim info As Long
    Dim ret As Long
    
    cmd = SendCmd
    
    If Text1.Text = "" Then
        Return
    End If
    
    data = Text1.Text
    length = Len(data)
    X25.SubmitNCB cmd, data, length
    
    pos = InStr(1, data, "HDLC_ERROR:", vbTextCompare)
    If pos > 0 Then
        MsgBox data
    End If
    
    X25.PollNCB info, ret, length, str
    str = str & vbNewLine & "  Info: " & info & vbNewLine & " ReturCode: " & ret & vbNewLine & " Length: " & length
    MsgBox str
End Sub

' * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
'
'   ReceiveButton_Click():
'   Action on pressing the send button
'
' * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
Private Sub ReceiveButton_Click()
    Dim info As Long
    Dim ret As Long
    Dim length As Long
    Dim str As String
    Dim bl As Boolean
    Dim data As String
    Dim cmd As Integer
    Dim tmpStr As String
    Dim infoStr As String
    
    cmd = ReceiveCmd
    
    data = ""
    length = Len(data)
    X25.SubmitNCB cmd, data, length
    
    MsgBox data
    
    X25.PollNCB info, ret, length, str
    
    tmpStr = str
    tmpStr = tmpStr
    If ret = 0 And tmpStr <> "" Then ' Add items in valid case
        List1.AddItem tmpStr, 0
    End If
    infoStr = " Info: " & info & vbNewLine & " ReturCode: " & ret & vbNewLine & " Length: " & length
    'str = str & vbNewLine & "  Info: " & info & vbNewLine & " ReturCode: " & ret & vbNewLine & " Length: " & length
    MsgBox infoStr
End Sub

Private Sub ConnectButton_Click()
    Dim info As Long
    Dim ret As Long
    Dim length As Long
    Dim str As String
    Dim bl As Boolean
    Dim data As String
    Dim cmd As Integer
    
    cmd = ConnectCmd
    data = ""
    length = Len(data)
    X25.SubmitNCB cmd, data, length
    
    MsgBox data
    X25.PollNCB info, ret, length, str
    str = str & vbNewLine & "  Info: " & info & vbNewLine & " ReturCode: " & ret & vbNewLine & " Length: " & length
    MsgBox str

End Sub

Private Sub DisconnectButton_Click()
    Dim info As Long
    Dim ret As Long
    Dim length As Long
    Dim str As String
    Dim bl As Boolean
    Dim data As String
    Dim cmd As Integer
    
    cmd = DisconnectCmd
    data = ""
    length = Len(data)
    X25.SubmitNCB cmd, data, length
    
    MsgBox data
    X25.PollNCB info, ret, length, str
    str = str & vbNewLine & "  Info: " & info & vbNewLine & " ReturCode: " & ret & vbNewLine & " Length: " & length
    MsgBox str

    X25.CloseSession
    
End Sub

Private Sub Form_Load()
    Dim info As Long
    Dim ret As Long
    Dim length As Long
    Dim str As String
    Dim bl As Boolean
    Dim data As String
    Dim cmd As Integer
    
    bl = False
    X25.OpenSesion bl
    If bl = False Then
        MsgBox "Connect to palas faild"
    End If
    
    
    'X25.PollNCB info, ret, length, str
    'str = str & vbNewLine & "  Info: " & info & vbNewLine & " ReturCode: " & ret & vbNewLine & " Length: " & length
    'MsgBox str
        
   
    
End Sub
