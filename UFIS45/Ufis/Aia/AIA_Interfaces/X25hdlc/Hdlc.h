// Hdlc.h : Deklaration von CHdlc

#ifndef __HDLC_H_
#define __HDLC_H_

#include "resource.h"       // Hauptsymbole

/////////////////////////////////////////////////////////////////////////////
// CHdlc
class ATL_NO_VTABLE CHdlc : 
	public CComObjectRootEx<CComSingleThreadModel>,
	public CComCoClass<CHdlc, &CLSID_Hdlc>,
	public IDispatchImpl<IHdlc, &IID_IHdlc, &LIBID_X25HDLCLib>
{
public:
	CHdlc()
	{
	}

DECLARE_REGISTRY_RESOURCEID(IDR_HDLC)

DECLARE_PROTECT_FINAL_CONSTRUCT()

BEGIN_COM_MAP(CHdlc)
	COM_INTERFACE_ENTRY(IHdlc)
	COM_INTERFACE_ENTRY(IDispatch)
END_COM_MAP()

// IHdlc
public:
	STDMETHOD(CloseSession)();
	STDMETHOD(PollNCB)(/*[in,out]*/ long * info, /*[in,out]*/ long * ret_code, /*[in,out]*/ long * length, /*[in,out]*/ BSTR * databuf);
	STDMETHOD(SubmitNCB)(/*[in]*/ short cmd, /*[in,out]*/ BSTR * data, /*[in,out]*/ long * length);
	STDMETHOD(OpenSesion)(/*[out]*/ VARIANT_BOOL *ret);
};

#endif //__HDLC_H_
