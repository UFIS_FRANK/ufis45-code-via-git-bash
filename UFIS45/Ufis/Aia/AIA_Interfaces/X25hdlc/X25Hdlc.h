/* this ALWAYS GENERATED file contains the definitions for the interfaces */


/* File created by MIDL compiler version 5.01.0164 */
/* at Tue Apr 03 15:59:29 2001
 */
/* Compiler settings for D:\Sandbox\Ufis\Interfaces\X25hdlc\X25Hdlc.idl:
    Oicf (OptLev=i2), W1, Zp8, env=Win32, ms_ext, c_ext
    error checks: allocation ref bounds_check enum stub_data 
*/
//@@MIDL_FILE_HEADING(  )


/* verify that the <rpcndr.h> version is high enough to compile this file*/
#ifndef __REQUIRED_RPCNDR_H_VERSION__
#define __REQUIRED_RPCNDR_H_VERSION__ 440
#endif

#include "rpc.h"
#include "rpcndr.h"

#ifndef __RPCNDR_H_VERSION__
#error this stub requires an updated version of <rpcndr.h>
#endif // __RPCNDR_H_VERSION__

#ifndef COM_NO_WINDOWS_H
#include "windows.h"
#include "ole2.h"
#endif /*COM_NO_WINDOWS_H*/

#ifndef __X25Hdlc_h__
#define __X25Hdlc_h__

#ifdef __cplusplus
extern "C"{
#endif 

/* Forward Declarations */ 

#ifndef __IHdlc_FWD_DEFINED__
#define __IHdlc_FWD_DEFINED__
typedef interface IHdlc IHdlc;
#endif 	/* __IHdlc_FWD_DEFINED__ */


#ifndef __Hdlc_FWD_DEFINED__
#define __Hdlc_FWD_DEFINED__

#ifdef __cplusplus
typedef class Hdlc Hdlc;
#else
typedef struct Hdlc Hdlc;
#endif /* __cplusplus */

#endif 	/* __Hdlc_FWD_DEFINED__ */


/* header files for imported files */
#include "oaidl.h"
#include "ocidl.h"

void __RPC_FAR * __RPC_USER MIDL_user_allocate(size_t);
void __RPC_USER MIDL_user_free( void __RPC_FAR * ); 

#ifndef __IHdlc_INTERFACE_DEFINED__
#define __IHdlc_INTERFACE_DEFINED__

/* interface IHdlc */
/* [unique][helpstring][dual][uuid][object] */ 


EXTERN_C const IID IID_IHdlc;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("13C298EE-0F44-11D4-B8FC-0800690F80F1")
    IHdlc : public IDispatch
    {
    public:
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE OpenSesion( 
            /* [out] */ VARIANT_BOOL __RPC_FAR *ret) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SubmitNCB( 
            /* [in] */ short cmd,
            /* [out][in] */ BSTR __RPC_FAR *data,
            /* [out][in] */ long __RPC_FAR *length) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE PollNCB( 
            /* [out][in] */ long __RPC_FAR *info,
            /* [out][in] */ long __RPC_FAR *ret_code,
            /* [out][in] */ long __RPC_FAR *length,
            /* [out][in] */ BSTR __RPC_FAR *databuf) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE CloseSession( void) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IHdlcVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE __RPC_FAR *QueryInterface )( 
            IHdlc __RPC_FAR * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void __RPC_FAR *__RPC_FAR *ppvObject);
        
        ULONG ( STDMETHODCALLTYPE __RPC_FAR *AddRef )( 
            IHdlc __RPC_FAR * This);
        
        ULONG ( STDMETHODCALLTYPE __RPC_FAR *Release )( 
            IHdlc __RPC_FAR * This);
        
        HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetTypeInfoCount )( 
            IHdlc __RPC_FAR * This,
            /* [out] */ UINT __RPC_FAR *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetTypeInfo )( 
            IHdlc __RPC_FAR * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo __RPC_FAR *__RPC_FAR *ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetIDsOfNames )( 
            IHdlc __RPC_FAR * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR __RPC_FAR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID __RPC_FAR *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *Invoke )( 
            IHdlc __RPC_FAR * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS __RPC_FAR *pDispParams,
            /* [out] */ VARIANT __RPC_FAR *pVarResult,
            /* [out] */ EXCEPINFO __RPC_FAR *pExcepInfo,
            /* [out] */ UINT __RPC_FAR *puArgErr);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *OpenSesion )( 
            IHdlc __RPC_FAR * This,
            /* [out] */ VARIANT_BOOL __RPC_FAR *ret);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *SubmitNCB )( 
            IHdlc __RPC_FAR * This,
            /* [in] */ short cmd,
            /* [out][in] */ BSTR __RPC_FAR *data,
            /* [out][in] */ long __RPC_FAR *length);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *PollNCB )( 
            IHdlc __RPC_FAR * This,
            /* [out][in] */ long __RPC_FAR *info,
            /* [out][in] */ long __RPC_FAR *ret_code,
            /* [out][in] */ long __RPC_FAR *length,
            /* [out][in] */ BSTR __RPC_FAR *databuf);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *CloseSession )( 
            IHdlc __RPC_FAR * This);
        
        END_INTERFACE
    } IHdlcVtbl;

    interface IHdlc
    {
        CONST_VTBL struct IHdlcVtbl __RPC_FAR *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IHdlc_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IHdlc_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IHdlc_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IHdlc_GetTypeInfoCount(This,pctinfo)	\
    (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo)

#define IHdlc_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo)

#define IHdlc_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)

#define IHdlc_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)


#define IHdlc_OpenSesion(This,ret)	\
    (This)->lpVtbl -> OpenSesion(This,ret)

#define IHdlc_SubmitNCB(This,cmd,data,length)	\
    (This)->lpVtbl -> SubmitNCB(This,cmd,data,length)

#define IHdlc_PollNCB(This,info,ret_code,length,databuf)	\
    (This)->lpVtbl -> PollNCB(This,info,ret_code,length,databuf)

#define IHdlc_CloseSession(This)	\
    (This)->lpVtbl -> CloseSession(This)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IHdlc_OpenSesion_Proxy( 
    IHdlc __RPC_FAR * This,
    /* [out] */ VARIANT_BOOL __RPC_FAR *ret);


void __RPC_STUB IHdlc_OpenSesion_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IHdlc_SubmitNCB_Proxy( 
    IHdlc __RPC_FAR * This,
    /* [in] */ short cmd,
    /* [out][in] */ BSTR __RPC_FAR *data,
    /* [out][in] */ long __RPC_FAR *length);


void __RPC_STUB IHdlc_SubmitNCB_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IHdlc_PollNCB_Proxy( 
    IHdlc __RPC_FAR * This,
    /* [out][in] */ long __RPC_FAR *info,
    /* [out][in] */ long __RPC_FAR *ret_code,
    /* [out][in] */ long __RPC_FAR *length,
    /* [out][in] */ BSTR __RPC_FAR *databuf);


void __RPC_STUB IHdlc_PollNCB_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IHdlc_CloseSession_Proxy( 
    IHdlc __RPC_FAR * This);


void __RPC_STUB IHdlc_CloseSession_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IHdlc_INTERFACE_DEFINED__ */



#ifndef __X25HDLCLib_LIBRARY_DEFINED__
#define __X25HDLCLib_LIBRARY_DEFINED__

/* library X25HDLCLib */
/* [helpstring][version][uuid] */ 


EXTERN_C const IID LIBID_X25HDLCLib;

EXTERN_C const CLSID CLSID_Hdlc;

#ifdef __cplusplus

class DECLSPEC_UUID("85A029B6-0F30-11D4-B8FB-0800690F80F1")
Hdlc;
#endif
#endif /* __X25HDLCLib_LIBRARY_DEFINED__ */

/* Additional Prototypes for ALL interfaces */

unsigned long             __RPC_USER  BSTR_UserSize(     unsigned long __RPC_FAR *, unsigned long            , BSTR __RPC_FAR * ); 
unsigned char __RPC_FAR * __RPC_USER  BSTR_UserMarshal(  unsigned long __RPC_FAR *, unsigned char __RPC_FAR *, BSTR __RPC_FAR * ); 
unsigned char __RPC_FAR * __RPC_USER  BSTR_UserUnmarshal(unsigned long __RPC_FAR *, unsigned char __RPC_FAR *, BSTR __RPC_FAR * ); 
void                      __RPC_USER  BSTR_UserFree(     unsigned long __RPC_FAR *, BSTR __RPC_FAR * ); 

/* end of Additional Prototypes */

#ifdef __cplusplus
}
#endif

#endif
