/*	ecncb.h
 *
 *	Copyright (C) Eicon Technology Corporation, 1988-1996.
 *	This module contains Proprietary Information of
 *	Eicon Technology Corporation, and should be treated
 *	as confidential.
 *
 *	ecncb.h - ECNCB programming interface structures and constants.
 * 
*/

#ifndef _ECNCB_

#define _ECNCB_

#include <windows.h>

#define ECNCBAPI __stdcall

#if defined __cpluplus
//extern "C" {
#endif

//---------------------------------------------------------------------------------------
// CONSTANTS
//---------------------------------------------------------------------------------------

/* Special lsn and cid values */

#define ECNCB_ALL_LSN            0
#define ECNCB_INVALID_LSN       -1
#define ECNCB_PENDING            0xff  /* NCB status code */
#define ECNCB_NOLSN              0xff  /* Illegal lsn value */
#define ECNCB_INVALID_LANA       0     /* invalid lana number in the config file. */

//
// NCB commands
//

#define ECNCB_NCBCMD_MASK        0x7f  /* Command number mask */
#define ECNCB_NCBCMD_CALL        0x10  /* Call */
#define ECNCB_NCBCMD_LISTEN      0x11  /* Listen */
#define ECNCB_NCBCMD_HANGUP      0x12  /* Hangup */
#define ECNCB_NCBCMD_SEND        0x14  /* Send */
#define ECNCB_NCBCMD_RECEIVE     0x15  /* Receive */
#define ECNCB_NCBCMD_MDDIAL      0x20  /* Modem Dial */
#define ECNCB_NCBCMD_MDANSWER    0x21  /* Modem Answer */
#define ECNCB_NCBCMD_MDHANGUP    0x22  /* Modem Hangup */
#define ECNCB_NCBCMD_MDNUMBER    0x23  /* Modem Number */
#define ECNCB_NCBCMD_ADDNAME     0x30
#define ECNCB_NCBCMD_DELNAME     0x31
#define ECNCB_NCBCMD_RESET       0x32  /* Reset */
#define ECNCB_NCBCMD_STATS       0x33  /* Statistics */
#define ECNCB_NCBCMD_MDSTATUS    0x33  /* Modem Status */
#define ECNCB_NCBCMD_STATUS      0x34  /* Status */
#define ECNCB_NCBCMD_CANCEL      0x35  /* Cancel */
#define ECNCB_NCBCMD_ADDGRP      0x36
#define ECNCB_NCBCMD_UNLINK      0x70
#define ECNCB_NCBCMD_TRACE       0x79  /* Trace enable */
#define ECNCB_NCBCMD_IPC         0x7b  /* IPC (remote commands) */
#define ECNCB_NCBCMD_ILLEGAL     0x7f  /* Illegal command */

#define ECNCB_NCBCMD_NOWAITMODE	 0x80  /* Asynchronous command mode */


//
// NCB Info field bits 
//

#define ECNCB_NCBINFO_INTR       0x01  /* Interrupt received */
#define ECNCB_NCBINFO_VCRESET    0x02  /* Virtual circuit reset */
#define ECNCB_NCBINFO_XON        0x04  /* Flow control ON */
#define ECNCB_NCBINFO_MBIT       0x10  /* More bit */
#define ECNCB_NCBINFO_ENDEND     0x20  /* End-to-end confirm */
#define ECNCB_NCBINFO_DBIT       0x40  /* Delivery bit */
#define ECNCB_NCBINFO_QBIT       0x80  /* Qualified bit */

#define ECNCB_NCBINFO_XOFF                   (ECNCB_NCBINFO_ENDEND|ECNCB_NCBINFO_XON)
#define ECNCB_NCBINFO_SEND_CONFIRM           ECNCB_NCBINFO_ENDEND
#define ECNCB_NCBINFO_SEND_EXPEDITED         ECNCB_NCBINFO_INTR
#define ECNCB_NCBINFO_SEND_EXPEDITED_CONFIRM (ECNCB_NCBINFO_ENDEND|ECNCB_NCBINFO_INTR)
#define ECNCB_NCBINFO_SEND_RESET             ECNCB_NCBINFO_VCRESET
#define ECNCB_NCBINFO_SEND_RESET_CONFIRM     (ECNCB_NCBINFO_ENDEND|ECNCB_NCBINFO_VCRESET)
#define ECNCB_NCBINFO_LISTEN_QUALIFIED       ECNCB_QUALIFIED_BIT

//
// NCB internal lengths 
//

#define	ECNCB_NCB_RESSIZE         14  /* NCB reserved field size */
#define	ECNCB_NCB_NAMELEN         16  /* NCB name field length */


//
// NCB Return codes for application requests
//

#define	ECNCB_NCBERR_BAD_LENGTH             0x01
#define	ECNCB_NCBERR_RET_BAD_CMD            0x03
#define ECNCB_NCBERR_MORE_DATA              0x06
#define ECNCB_NCBERR_BAD_LSN                0x08
#define ECNCB_NCBERR_NO_VC                  0x09
#define ECNCB_NCBERR_CALL_CLEARED           0x0A
#define ECNCB_NCBERR_CMD_CANCELLED          0x0B
#define ECNCB_NCBERR_SESS_TABLE_FULL        0x11
#define ECNCB_NCBERR_CALL_REJECTED          0x12
#define ECNCB_NCBERR_VC_IN_USE              0x12
#define ECNCB_NCBERR_BAD_PROTOCOL           0x14
#define ECNCB_NCBERR_PORT_FAIL              0x15
#define ECNCB_NCBERR_CIRCUIT_IN_USE         0x16
#define ECNCB_NCBERR_BAD_PUNAME             0x17
#define ECNCB_NCBERR_CLEAR_RESET_RECEIVED   0x18
#define ECNCB_NCBERR_TRACE_NOT_ACTIVE       0x19
#define ECNCB_NCBERR_INSUFFICIENT_MEMORY    0x1a
#define ECNCB_NCBERR_TOO_MANY_CMD           0x22
#define	ECNCB_NCBERR_BAD_LANA_NUM           0x23
#define	ECNCB_NCBERR_NONE_DONE              0x40
#define	ECNCB_NCBERR_DONE_TIMEOUT           0x41
#define	ECNCB_NCBERR_BAD_DONE_PARAM         0x42

#define ECNCB_NCBERR_MALFUNC                0x50
#define	ECNCB_NCBERR_LOADED                 0x55
#define ECNCB_NCBERR_DUMP                   0xF9
#define ECNCB_NCBERR_SNARK                  0xFA
#define	ECNCB_NCBERR_HALT                   0xFD
#define ECNCB_NCBERR_NO_DRIVER              0xFE
#define ECNCB_NCBERR_PENDING                0xFF

// NCB error codes that can be returned by Eclan

#define ECNCB_NCBERR_CLIENT_NOT_READY       0x70
#define ECNCB_NCBERR_SERVER_NOT_READY       0x71
#define ECNCB_NCBERR_SERVER_NOT_FOUND	    0x72
#define ECNCB_NCBERR_INV_USERID             0x73
#define ECNCB_NCBERR_INV_PASSWORD           0x74
#define ECNCB_NCBERR_INV_PROTOCOL           0x75
#define ECNCB_NCBERR_INV_ADDRESS            0x76
#define ECNCB_NCBERR_INV_SERVER_TYPE        0x77
#define ECNCB_NCBERR_ADDRESS_ALREADY_SET    0x78
#define ECNCB_NCBERR_SERVERTABLE_FULL       0x79
#define ECNCB_NCBERR_BUF_OVLF	            0x80
#define ECNCB_NCBERR_QUERY                  0x81


//
// ECNCB error codes (Retrieved with EcGetLastError())
//

#define ECNCB_NOERROR                     0
#define ECNCB_SUCCESS                     0

// ECNCB uses error codes above 0x4000

// The following errors are equivalent to W32Ecbios error
// codes.
#define ECNCB_ERR_INVALID_SESSION         0x0001
#define ECNCB_ERR_INSUFFICIENT_RESOURCES  0x0002
#define ECNCB_ERR_INTERNAL                0x0003
#define ECNCB_ERR_DRIVER_UNAVAILABLE      0x0004
#define ECNCB_ERR_WAIT_TIMEOUT            0x0008

// The following errors (> 0x4800) are generated only in
// ECNCB.
#define ECNCB_ERR_POSTFCT_UNEXPECTED      0x4801
#define ECNCB_ERR_WIN32                   0x4802
#define ECNCB_ERR_SESSION_IS_CLOSING      0x4803
#define ECNCB_ERR_WAIT_IO_COMPLETION      0x4804
#define ECNCB_ERR_DRIVER_INTERNAL         0x4805
#define ECNCB_ERR_INVALID_GATEWAY         0x4806
#define ECNCB_ERR_INVALID_PARAM           0x4808
#define ECNCB_ERR_BAD_PARAM_VALUE         0x4809
#define ECNCB_ERR_OPERATION_DISALLOWED    0x480a

// Mapped W32ECBIOS error codes that are transparently returned by ECNCB.
// They may also be set by ECNCB itself.

#define ECNCB_ERR_NO_MEMORY          0x0002 // ECBIOS_NO_MEMORY        - No memory available for command
#define ECNCB_ERR_SYS                0x0003 // ECBIOS_SYS_ERR          - A system error has occurred
#define ECNCB_ERR_NO_ECLAN           0x0004 // ECBIOS_NO_ECLANW        - Could not find Eclan running
#define ECNCB_ERR_NCB_IN_USE         0x000A // ECBIOS_NCB_IN_USE       - This NCB is in use by WECBIOS
#define ECNCB_ERR_NCB_NOT_IN_USE     0x000B // ECBIOS_NCB_NOT_IN_USE   - The NCB is not in internal lists
#define ECNCB_ERR_NCB_INACCESSIBLE   0x000C // ECBIOS_NCB_INACCESSIBLE - The NCB, or buffer, invalid
#define ECNCB_ERR_NCB_NO_XSPACE      0x000D // ECBIOS_NCB_NOXSPACE     - The NCB does not have extra space
#define ECNCB_ERR_NCB_BAD_OFFSET     0x000E // ECBIOS_NCB_BAD_OFFSET   - The offset is out of range
#define ECNCB_ERR_IN_POSTFCT         0x000F // ECBIOS_IN_CALLBACK      - Attempt to issue Wait NCB from a post function
#define ECNCB_ERR_BUF_INACCESSIBLE   0x0010 // ECBIOS_BUF_INACCESSIBLE - The buffer is invalid

// Exclusive W32ECBIOS error codes.  They cannot be generated by internally
// by ECNCB but they can be set by W32ECBIOS

#define ECNCB_ERR_PROTOCOL_NOT_LOADED  0x0070  /* Specified prot is not configured */
#define ECNCB_ERR_START_IPC_FAILED     0x0072  /* IPC failed to start            */
#define ECNCB_ERR_IPC_MEM_OVERFLOW     0x0073  /* Buffer larger than size of IPC */

#define ECNCB_ERR_CFG_MAXSESSION       0x0091  /* Max session invalid in config  */
#define ECNCB_ERR_CFG_MAXSERVER        0x0092  /* Max server invalid in config   */
#define ECNCB_ERR_CFG_PROTOCOL         0x0093  /* Protocol invalid in config     */
#define ECNCB_ERR_CFG_FILENOTFOUND     0x0094  /* Provider config file not found */
#define ECNCB_ERR_CFG_INSTALLATION     0x0095  /* Missing product component or   */
                                               /* product not configured         */
#define ECNCB_ERR_CFG_IPCTIMEOUT       0x0096  /* Time out invalid in config     */
#define ECNCB_ERR_CFG_IPCSIZE          0x0097  /* IPC size invalid in config     */
#define ECNCB_ERR_CFG_IPCSTARTTIME     0x0098  /* IPC timer invalid in config    */
#define ECNCB_ERR_INCOMPATIBLE_VERSION 0x0099  /* DLL not same version           */

//
// ECNCB Session flags
//

#define ECNCB_SESSION_ECSERVER     0x0001
#define ECNCB_USE_DEFAULT_GATEWAY  0x0002
#define ECNCB_USE_LOCAL_CARD       0x0004

//
// ECNCB invalid session value
//

#define INVALID_ECSESSION_VALUE 0

//
// ECNCB Submit priority 
//

#define ECNCB_PRIORITY_DEFAULT  0
#define ECNCB_PRIORITY_NORMAL   50
#define ECNCB_PRIORITY_LOW      20
#define ECNCB_PRIORITY_HIGH     80

//
// ECNCB Parameters

#define ECNCB_PARAM_NCB_PRIORITY        1
#define ECNCB_PARAM_USERREF             2
#define ECNCB_PARAM_ECLAN_LOAD_TIMEOUT  3

#define ECNCB_LAST_VALID_PARAM          3

//
// ECLAN Client definitions
//

#define	ECNCB_NAMELEN           65
#define	ECNCB_STRLEN            17
#define	ECNCB_ADDRLEN           32
#define	ECNCB_PROTLEN           80

#define	ECNCB_GATEWAY_NONSECURE 0x01
#define	ECNCB_GATEWAY_SECURE    0x02

#define	ECNCB_NO_PROT           0
#define	ECNCB_TCPIP_PROT        1
#define	ECNCB_SPXIPX_PROT       2

#define ECNCB_PROTOCOL_TCPIP    __TEXT("TCPIP")
#define ECNCB_PROTOCOL_SPX      __TEXT("SPX")

//
//---------------------------------------------------------------------------------------
// ECNCB TYPE definitions
//---------------------------------------------------------------------------------------

#pragma pack ( 1 )  /* The following structures should remain the same
                     *  regardless of the Operating System/CPU/LAN in
                     *  which they are used.
                     */

struct ECNCB_PARAM{
  WORD  wParam;
  DWORD dwValue;
};

/*
 *	Structure for selecting Eicon gateways
 */
struct ECSERVER
{
  char     serverName[ECNCB_NAMELEN];   /* Target Server Name              */
  char     userName[ECNCB_NAMELEN];     /* ICS SECURE User Name            */
  char     password[ECNCB_NAMELEN];     /* ICS SECURE or V3R1 Password     */
  unsigned char	protId;                 /* ECNCB_NO_PROT or               */
                                        /* ECNCB_TCPIP_PROT or            */
                                        /* ECNCB_SPX_PROT                 */
  unsigned char netAddress[ECNCB_ADDRLEN]; /* Network order (NON Intel)   */
  char     serverType;                  /* ECNCB_GATEWAY_SECURE,  ECNCB_GATEWAY_NONSECURE */
  unsigned int	timeout;                /* Ignore -- future use            */
  int      returnCode;
};
#define PECSERVER ECSERVER*


// Header Structure for discovering Eicon gateways

struct ECDISCOVER
{
  PECSERVER    pEcServer;  /* Points to begining of array or W32ECSERVER struct */
  UINT         NumServer;  /* Number of W32ECSERVER structures passed -- Number of  */
                           /*  filled W32ECSERVER structures on return */
  char         ServerType; /* ECNCB_GATEWAY_SECURE, ECNCB_GATEWAY_NONSECURE	*/
  char         Protocols[ECNCB_PROTLEN]; /* Null terminated strings identifying protocol priority */
  char*        SLPServer;  /* SLP address -- TCP/IP only */
};

#define PECDISCOVER ECDISCOVER*


#ifndef _ECNCB_INTERNAL

struct CALLNAME{ 
  struct GatewayCall{
    WORD   MustBeZero;
    LPTSTR GatewayName;
    LPTSTR CallName;
  };
  BYTE cname[16];		// protocol name
};

#define LPCALLNAME  CALLNAME*

struct	ECNCB {
  BYTE     command;
  BYTE     retcode;
  BYTE     lsn;
  BYTE     info;
  PVOID    buf;
  WORD     len;
  CALLNAME cname;
  BYTE     aname[16];		// Application name
  BYTE     sto;
  BYTE     rto;
  PVOID    post;
  BYTE     lana;
  BYTE     complete;
  WORD     cid;
  BYTE     reserved[12];
};


#define PECNCB ECNCB*	

#define ECSESSION DWORD

#pragma pack ( )


//
// ECNCB API
//

extern "C" {
ECSESSION ECNCBAPI EcOpenNcbSession
( 
 PVOID    pEcGateway,     // If non-null, information about the Eicon Gateway to select
 DWORD    dwSessionFlags, // Session qualifiers
 DWORD    dwUserRef       // User reference given to NCB post routine
);
}

extern "C" {
BOOL ECNCBAPI EcCloseNcbSession
(
 ECSESSION hEcSession,  // A handle returned by EcOpenNcbSession
 DWORD     dwCloseFlags // CloseSession qualifiers
);
}
extern "C" {
BOOL ECNCBAPI EcSubmitNcb
(
 ECSESSION hEcSession,  // A handle returned by EcOpenNcbSession
 PECNCB pNcb,           // A pointer to the ncb to submit
 BYTE   bPri,           // Priority the NCB should be given
 HANDLE hEvent          // An event to signal when the ncb completes
);
}
extern "C" {
PECNCB ECNCBAPI EcWaitForNcbCompletionEx
(
 ECSESSION hEcSession,  // A handle returned by EcOpenNcbSession
 DWORD dwTimeout,       // Time-out interval in milliseconds
 BOOL bAlertable        // Alertable wait flag
);
}
#define EcWaitForNcbCompletion(a,b) EcWaitForNcbCompletionEx(a, b, FALSE)

extern "C" {
DWORD ECNCBAPI EcGetLastError
( 
 void
);
}
DWORD ECNCBAPI EcSetSessionParams
( 
 ECSESSION hEcSession,  // a handle returned by EcOpenNcbSession or INVALID_ECSESSION_HANDLE
 ECNCB_PARAM *pEcParam, // a list of parameter/value pair
 WORD wNumParams        // the number of parameters in the list
);

DWORD  ECNCBAPI EcGetSessionParams
( 
 ECSESSION hEcSession,  // a handle returned by EcOpenNcbSession or INVALID_ECSESSION_HANDLE
 ECNCB_PARAM *pEcParam, // a list of parameter/value pair
 WORD wNumParams        // the number of parameters in the list
);

BOOL ECNCBAPI EcDiscoverNcbServer
(
 PECDISCOVER pEcDiscover 
);

#endif //_ECNCB_INTERNAL

#ifdef __cplusplus
//}
#endif


#endif // _ECNCB_
