// Hdlc.cpp : Implementierung von CHdlc
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 * X25-HCLC COM component for Eicon cards - AIA project
 *
 * COM class CHdlc
 *
 * ABB Airport Technologies 2000
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 *	Change history:
 *	--------------
 *
 *	xx/xx/2000		mwo,cla		AAT/E	Initial version
 *	03/06/2000		cla			AAT/E	Error fixing bPort new - malloc problems
 *										cmd fixed in SubmitNCB
 *										Send and receive function fixed
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#include "stdafx.h"
#include "X25Hdlc.h"
#include "Hdlc.h"
#include <comdef.h>
#include	"Ecncb.h"

/////////////////////////////////////////////////////////////////////////////
// CHdlc
/*
 *	DEFINES
 */
#define DEF_PORT	1		// Default port # used
#define DATA_LEN	255		// Data buffer size

#define HDLC		"HDLC            "	// HDLC Protocol
#define APP_NAME	"HDLCCDI2        "	// Application name

// NCB no wait commands for HDLC
#define NCB_CONNECT		0x90	// Connect (Activate HDLC link)
#define NCB_DISCONNECT	0x92	// Disconnect (Deactivate HDLC link)
#define NCB_SEND		0x94	// Send data
#define NCB_RECEIVE		0x95	// Receive data

// Special timeout values
#define	TO_IMM		 0			// No timeout, return immediately


/*
 *	GLOBAL VARIABLES
 */
BYTE		bPort;				// Port # used
BYTE		bLSN = 0;			// LSN returned by the card
ECSESSION	hEcSession;			// NCB Session handle



STDMETHODIMP CHdlc::OpenSesion(VARIANT_BOOL *ret)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())
	hEcSession = EcOpenNcbSession(NULL, 0, 0);
	
	*ret = -1; //return value = false
	// Could not open a session
	if (hEcSession == INVALID_ECSESSION_VALUE)
	{
		//Berni, 0 und -1 wird in Visual Basic korrekt nach true und false umgesetzt
		*ret = 0; //return value = false
	}

	//	*ret = 0;//0 = false / -1 = true
	// ZU ERLEDIGEN: Implementierungscode hier hinzuf�gen

	return S_OK;
}

//------------------------------------------------------------------------------------
// hallo berni:
// es gibt auch hier wiederum die m�glichkeit �ber den anfang von data
// den grundatzzustand herauszufinden
// der beginn "HDLC_ERROR: [<nr>]" ein fehler [<nr>] ist der fehlercode,
// wenn kein '[' und ']' vorkommt, dann gab es sonst ein problem , z.b. malloc
// der beginn "HDLC_OK:" alles ok
//------------------------------------------------------------------------------------
STDMETHODIMP CHdlc::SubmitNCB(short cmd, BSTR * data, long *length)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	BYTE olCmd;
	//olCmd = (BYTE)cmd;
	_bstr_t olData( *data, false );
	olData = *data;
	*data = olData;
	char	*szDataBuf = NULL;	// Data buffer to Send/Receive
	char	*pclMessage = NULL;//[512]="";
	ECNCB	*pNCB = NULL;		// NCB pointer

	bPort = DEF_PORT;

	/*
	 * cla 03/06/00: 
	 * malloc is turned to new C++ is used
	 * do not mix malloc and new
	 * pclMessage = (char *)malloc(sizeof(char) * (512+1));
	 * pNCB = (ECNCB *)malloc(sizeof(ECNCB));
	 */
	
	pclMessage = new char[512+1];

	// Allocate memory for the NCB.
	pNCB = new ECNCB();

	if (pNCB == NULL)
	{	
		sprintf(pclMessage, "HDLC_ERROR: SubmitNCB: Unable to allocate memory for NCB.");
		olData = pclMessage;
		*data = olData;
		//printf("\n==>SubmitNCB: Unable to allocate memory for NCB.\n\n");
		return S_OK;
	}
	
	// Initialize NCB before submiting it.
	memset(pNCB, 0, sizeof(ECNCB));
	pNCB->command = static_cast<BYTE>(cmd);
	pNCB->retcode = ECNCB_PENDING;		// Set to 0xff
	memcpy(pNCB->cname.cname, HDLC, ECNCB_NCB_NAMELEN);
	memcpy(pNCB->aname, APP_NAME, ECNCB_NCB_NAMELEN);
	pNCB->lana = bPort;
	pNCB->complete = ECNCB_PENDING;	// Set to 0xff

	/*
	 * This switch is just a convience solution for the VB guys
	 * avoid dealing with hex values
	 */
	switch (cmd)
	{
		case 1: // Connect
			olCmd = NCB_CONNECT;
		break;

		case 2: // Send data
			olCmd = NCB_SEND;
		break;
	
		case 3: // Receive data
			olCmd = NCB_RECEIVE;
		break;

		case 4: // Disconnect
			olCmd = NCB_DISCONNECT;
		break;
		
		case 5: // Poll completed req.
			//PollNCB();
		break;
	
		default:
			//printf("\n==> Invalid Option selected.\n\n");
		break;
	} // end switch
	

	switch (olCmd)	// Initialize fields with specific values depending on the NCB cmd.
	{

		case NCB_CONNECT:	// Connect request
			pNCB->command = static_cast<BYTE>(NCB_CONNECT);
		break;

		case NCB_DISCONNECT:	// Disconnect request
			pNCB->command = static_cast<BYTE>(NCB_DISCONNECT);
			pNCB->lsn = bLSN;
		break;

		case NCB_SEND:		// Send request
			pNCB->command = static_cast<BYTE>(NCB_SEND);
			// Allocate memory for the Send data buffer
			/*
			 * malloc changed to new
			 * szDataBuf = (char *)malloc(sizeof(char) * (olData.length()+1));
			 * Bla bla see receive ...
			 */
			szDataBuf = (char *)malloc(sizeof(char) * (olData.length()+1));
			//szDataBuf = new char[olData.length()+1];
			/*
			 * char* operator converting BSTR to char*
			 */
			//szDataBuf = (char*)olData;
			memset(szDataBuf, 0, olData.length()+1);
			strcpy(szDataBuf,(char*)olData);

			if (szDataBuf == NULL)
			{	
				olData = "HDLC_ERROR: SubmitNCB: Unable to allocate memory for Send data buffer.";
				//printf("\n==>SubmitNCB: Unable to allocate memory for Send data buffer.\n\n");
				*data = olData;
				//free(pNCB);
				delete pNCB;

				return S_OK;
			}
			
//			printf("\n==> Type data to send (max. %d bytes): ", *length);
//			gets(szDataBuf); //muss raus
//			fflush(stdin);   //muss raus

			pNCB->lsn = bLSN;
			pNCB->buf = szDataBuf;
			pNCB->len = olData.length();//strlen(szDataBuf);
			//printf("==> Data length = %d bytes.", pNCB->len);
			szDataBuf = NULL;
		break;

		case NCB_RECEIVE:
			pNCB->command = static_cast<BYTE>(NCB_RECEIVE);

			// Allocate memory for the Receive data buffer
			//szDataBuf = (char *)malloc(sizeof(char) * (DATA_LEN+1));
			/*
			 * I keep the the malloc here - dangerous but less than
			 * do a new here and the eicon driver will do a free!
			 */
			szDataBuf = (char *)malloc(sizeof(char) * (DATA_LEN+1));

			if (szDataBuf == NULL)
			{	
				sprintf(pclMessage, "HDLC_ERROR: SubmitNCB: Unable to allocate memory for Receive data buffer.");
				olData =  pclMessage;
				//printf("\n==>SubmitNCB: Unable to allocate memory for Receive data buffer.\n\n");
				*data = olData;
				//free(pNCB);
				//free(pclMessage);
				delete[] pNCB;
				delete[] pclMessage;
				return S_OK;
			}
			
			memset(szDataBuf, 0, DATA_LEN+1);
			
			pNCB->lsn = bLSN;
			pNCB->buf = szDataBuf;
			pNCB->len = DATA_LEN+1;

			//szDataBuf = NULL;
		break;

		default:
			sprintf(pclMessage, "HDLC_ERROR: [%d] SubmitNCB: Invalid Cmd ", cmd);
			olData = pclMessage;
			*data = olData;
			//printf("\n==>SubmitNCB: Invalid Cmd 0x%02X\n\n", cmd);
//			free(pNCB);
			delete pNCB;
//			free(pclMessage);
			delete[] pclMessage;
			return S_OK;
		break;

	} // end switch

	int size = sizeof(ECNCB);
	// Submit the NCB
	if ( EcSubmitNcb(hEcSession, pNCB, ECNCB_PRIORITY_DEFAULT, NULL) == FALSE )
	{
		sprintf(pclMessage, "HDLC_ERROR: [%d] SubmitNCB: EcSubmitNcb failed with error ", EcGetLastError());
		olData = pclMessage;
		*data = olData;
		//free(pclMessage);
		delete[] pclMessage;
		//printf("\n==>SubmitNCB: EcSubmitNcb failed with error 0x%04X\n\n", EcGetLastError());
		if (pNCB->buf != NULL)	// In case it is a Send/Receive NCB.
			free(pNCB->buf);
		delete pNCB;
		return S_OK;

	}

	sprintf(pclMessage, "HDLC_OK,0,Request submited");
	olData =  pclMessage;
	*data = olData;
	//free(pclMessage);
	delete[] pclMessage;
	//printf("\n==> Request submited.\n\n");	

	return S_OK;
}


//-----------------------------------------------------------------------------------
// der anfang in databuf:
// 1) "POLL_MSG:"    ist eine Meldung und kein fehler
// 2) "POLL_ERROR:"  grosses scheisse ist passiert
// ansonsten wird ret_code und ino_gef�llt, kannst dir den code ja mal anschauen
//-----------------------------------------------------------------------------------
STDMETHODIMP CHdlc::PollNCB(long *info, long *ret_code, long *length, BSTR *databuf)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	_bstr_t olData( *databuf, false );
	olData = *databuf;

	DWORD	dwRC;			// Return code
	ECNCB	*pNCB = NULL;	// NCB pointer
	
	
		
	// Poll for completed NCBs
	pNCB = EcWaitForNcbCompletionEx(hEcSession, TO_IMM, FALSE);

	// Check result of polling
	if ( pNCB == NULL )		// EcWaitForNcbCompletionEx failed
	{
		dwRC = EcGetLastError();
		switch (dwRC)
		{
			case ECNCB_ERR_WAIT_TIMEOUT:	// No NCB has completed yet.
				{
					olData = "POLL_MSG,0,No NCB has completed yet.";
				}
			break;

			default:
				{
					olData = "POLL_ERROR,1,EcWaitForNcbCompletionEx failed";
					//*ret_code = (long)dwRC;
				}
			break;
		} // end switch dwRC

		*ret_code = (long)dwRC;
		*databuf = olData;
		return S_OK;
	}

	// Polling is successful, an NCB has completed
	switch (pNCB->command)	// Check completed NCB
	{
		case NCB_CONNECT:	// Connect request completed
			{
				// Check if the connection is successful.
				// If yes, save LSN returned by the card to be used in 
				// subsequent requests.
				olData = "POLL_MSG,1,Connect NCB completed";
				*info = (long)pNCB->info;
				if ( pNCB->retcode == ECNCB_SUCCESS )
				{
					bLSN = pNCB->lsn;
					*info = 1;
				}
				*ret_code = (long)pNCB->retcode;
			}
		break;

		case NCB_DISCONNECT:	// Disconnect request completed
			{
			//printf("\n==>Disconnect NCB completed, RetCode = 0x%02X\n\n",
			//							pNCB->retcode);
				olData = "POLL_MSG,5,Disconnect NCB completed";
				*info = (long)pNCB->info;
				if ( pNCB->retcode == ECNCB_SUCCESS )
				{
					*info = 5;
				}
				*ret_code = (long)pNCB->retcode;
			}
		break;

		case NCB_SEND:		// Send request completed
			{
//				printf("\n==>Send NCB completed, RetCode = 0x%02X\n\n",
//											pNCB->retcode);
				// Free the memory allocated previously (in SubmitNCB)
				// for the data buffer to Send
				olData = "POLL_MSG,2,Send NCB completed";
				*info = (long)pNCB->info;
				if ( pNCB->retcode == ECNCB_SUCCESS )
				{
					*info = 2;
				}
				*ret_code = (long)pNCB->retcode;
				free(pNCB->buf);
			}
		break;

		case NCB_RECEIVE:	// Receive request completed
//			printf("\n==>Receive NCB completed, Info = 0x%02X, RetCode = 0x%02X\n",
//										pNCB->info, pNCB->retcode);
			//INFO und return code braucht berni
			*ret_code = (long)pNCB->retcode;
			*info = (long)pNCB->info;
			if ( pNCB->retcode == ECNCB_SUCCESS )
			{
				*length = (long)pNCB->len;
				*info = 3;
				olData = (char*)pNCB->buf;
				//printf("Data received (%d bytes): %s\n\n", pNCB->len, pNCB->buf);
			}
			// Free the memory allocated previously (in SubmitNCB)
			// for the data buffer to Receive
			free(pNCB->buf);
		break;

		default:			// Invalid request completed
			olData = "POLL_MSG,9,Invalid NCB completed";
			*ret_code = (long)pNCB->retcode;
//			printf("\n==>Invalid NCB completed, RetCode = 0x%02X\n\n",
//										pNCB->retcode);
		break;
	} // end switch (bCmd)

	// Free the memory allocated previously (in SubmitNCB) for the NCB
	free(pNCB);
	*databuf = olData;
/*	char pclDataBuf[512] = "Das w�re der Test";

	//CComBSTR str("Hallo");
	//*databuf = BSTR(str);
	_bstr_t olBstr( *databuf, false );
	olBstr = pclDataBuf;
	*databuf = olBstr;
	*info = 1;
	*ret_code = 2;
	*length = 3;
*/
	return S_OK;
}

STDMETHODIMP CHdlc::CloseSession()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if ( EcCloseNcbSession(hEcSession, 0) == FALSE )	// Could not close session
	{
		//printf("\n==>CloseSession: EcCloseNcbSession failed with error 0x%04X\n\n", EcGetLastError());
		;
	}

	return S_OK;
}
