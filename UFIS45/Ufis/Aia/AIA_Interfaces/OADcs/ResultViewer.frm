VERSION 5.00
Begin VB.Form ResultViewer 
   Caption         =   "Result Data"
   ClientHeight    =   5505
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   9480
   LinkTopic       =   "Form1"
   ScaleHeight     =   5505
   ScaleWidth      =   9480
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton Command2 
      Caption         =   "Close"
      Height          =   255
      Left            =   4800
      TabIndex        =   2
      Top             =   5160
      Width           =   1095
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Reset"
      Height          =   255
      Left            =   3600
      TabIndex        =   1
      Top             =   5160
      Width           =   1095
   End
   Begin VB.ListBox ResultData 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   161
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   4890
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   9255
   End
   Begin VB.Label CurrentLine 
      Alignment       =   1  'Right Justify
      Height          =   255
      Left            =   7320
      TabIndex        =   4
      Top             =   5160
      Width           =   855
   End
   Begin VB.Label TotalLines 
      Alignment       =   1  'Right Justify
      Height          =   255
      Left            =   8400
      TabIndex        =   3
      Top             =   5160
      Width           =   855
   End
End
Attribute VB_Name = "ResultViewer"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Command1_Click()
    ResultData.Clear
    TotalLines.Caption = ""
    CurrentLine.Caption = ""
End Sub

Private Sub Command2_Click()
    ResultData.Clear
    TotalLines.Caption = ""
    CurrentLine.Caption = ""
    ResultViewer.Hide
End Sub

Private Sub ResultData_Click()
    CurrentLine.Caption = ResultData.ListIndex + 1
End Sub
