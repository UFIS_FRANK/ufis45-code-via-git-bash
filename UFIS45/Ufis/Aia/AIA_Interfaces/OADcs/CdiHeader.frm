VERSION 5.00
Begin VB.Form CdiHeader 
   Caption         =   "CDI Message Header Configuration"
   ClientHeight    =   7455
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   9705
   LinkTopic       =   "Form1"
   ScaleHeight     =   7455
   ScaleWidth      =   9705
   StartUpPosition =   3  'Windows Default
   Begin VB.Frame CfgSection 
      BackColor       =   &H00C0C0C0&
      Caption         =   "From OA DCS (Incoming Messages)"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   161
         Weight          =   700
         Underline       =   -1  'True
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FF0000&
      Height          =   3585
      Index           =   1
      Left            =   60
      TabIndex        =   79
      Top             =   3780
      Width           =   9585
      Begin VB.CommandButton Command2 
         Caption         =   "Close"
         Height          =   255
         Index           =   1
         Left            =   8460
         TabIndex        =   163
         Top             =   3210
         Width           =   975
      End
      Begin VB.CheckBox TestRecv 
         Caption         =   "Test"
         Height          =   255
         Left            =   7410
         Style           =   1  'Graphical
         TabIndex        =   161
         Top             =   3210
         Width           =   975
      End
      Begin VB.Frame Frame3 
         Caption         =   "Future Use"
         Height          =   1605
         Index           =   1
         Left            =   6660
         TabIndex        =   159
         Top             =   1470
         Width           =   2805
      End
      Begin VB.Frame Frame2 
         Caption         =   "Number Circles"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   3045
         Index           =   1
         Left            =   3510
         TabIndex        =   112
         Top             =   420
         Width           =   3045
         Begin VB.TextBox SeqNbrCmd 
            Alignment       =   2  'Center
            Height          =   285
            Index           =   15
            Left            =   120
            TabIndex        =   152
            Text            =   "SYS"
            Top             =   2640
            Width           =   495
         End
         Begin VB.TextBox SeqNbrBgn 
            Alignment       =   1  'Right Justify
            Height          =   285
            Index           =   15
            Left            =   660
            TabIndex        =   151
            Top             =   2640
            Width           =   615
         End
         Begin VB.TextBox SeqNbrEnd 
            Alignment       =   1  'Right Justify
            Height          =   285
            Index           =   15
            Left            =   1920
            TabIndex        =   150
            Top             =   2640
            Width           =   615
         End
         Begin VB.TextBox SeqNbrAct 
            Alignment       =   1  'Right Justify
            Height          =   285
            Index           =   15
            Left            =   1290
            TabIndex        =   149
            Top             =   2640
            Width           =   615
         End
         Begin VB.TextBox SeqNbrCmd 
            Alignment       =   2  'Center
            Height          =   285
            Index           =   14
            Left            =   120
            TabIndex        =   148
            Top             =   2340
            Width           =   495
         End
         Begin VB.TextBox SeqNbrBgn 
            Alignment       =   1  'Right Justify
            Height          =   285
            Index           =   14
            Left            =   660
            TabIndex        =   147
            Top             =   2340
            Width           =   615
         End
         Begin VB.TextBox SeqNbrEnd 
            Alignment       =   1  'Right Justify
            Height          =   285
            Index           =   14
            Left            =   1920
            TabIndex        =   146
            Top             =   2340
            Width           =   615
         End
         Begin VB.TextBox SeqNbrAct 
            Alignment       =   1  'Right Justify
            Height          =   285
            Index           =   14
            Left            =   1290
            TabIndex        =   145
            Top             =   2340
            Width           =   615
         End
         Begin VB.TextBox SeqNbrAct 
            Alignment       =   1  'Right Justify
            Height          =   285
            Index           =   13
            Left            =   1290
            TabIndex        =   144
            Top             =   2040
            Width           =   615
         End
         Begin VB.TextBox SeqNbrAct 
            Alignment       =   1  'Right Justify
            Height          =   285
            Index           =   12
            Left            =   1290
            TabIndex        =   143
            Top             =   1740
            Width           =   615
         End
         Begin VB.TextBox SeqNbrEnd 
            Alignment       =   1  'Right Justify
            Height          =   285
            Index           =   13
            Left            =   1920
            TabIndex        =   142
            Top             =   2040
            Width           =   615
         End
         Begin VB.TextBox SeqNbrEnd 
            Alignment       =   1  'Right Justify
            Height          =   285
            Index           =   12
            Left            =   1920
            TabIndex        =   141
            Top             =   1740
            Width           =   615
         End
         Begin VB.TextBox SeqNbrBgn 
            Alignment       =   1  'Right Justify
            Height          =   285
            Index           =   13
            Left            =   660
            TabIndex        =   140
            Top             =   2040
            Width           =   615
         End
         Begin VB.TextBox SeqNbrBgn 
            Alignment       =   1  'Right Justify
            Height          =   285
            Index           =   12
            Left            =   660
            TabIndex        =   139
            Top             =   1740
            Width           =   615
         End
         Begin VB.TextBox SeqNbrCmd 
            Alignment       =   2  'Center
            Height          =   285
            Index           =   13
            Left            =   120
            TabIndex        =   138
            Top             =   2040
            Width           =   495
         End
         Begin VB.TextBox SeqNbrCmd 
            Alignment       =   2  'Center
            Height          =   285
            Index           =   12
            Left            =   120
            TabIndex        =   137
            Top             =   1740
            Width           =   495
         End
         Begin VB.TextBox SeqNbrAct 
            Alignment       =   1  'Right Justify
            Height          =   285
            Index           =   11
            Left            =   1290
            TabIndex        =   136
            Text            =   "0"
            Top             =   1440
            Width           =   615
         End
         Begin VB.TextBox SeqNbrEnd 
            Alignment       =   1  'Right Justify
            Height          =   285
            Index           =   11
            Left            =   1920
            TabIndex        =   135
            Text            =   "1"
            Top             =   1440
            Width           =   615
         End
         Begin VB.TextBox SeqNbrBgn 
            Alignment       =   1  'Right Justify
            Height          =   285
            Index           =   11
            Left            =   660
            TabIndex        =   134
            Text            =   "1"
            Top             =   1440
            Width           =   615
         End
         Begin VB.TextBox SeqNbrCmd 
            Alignment       =   2  'Center
            Height          =   285
            Index           =   11
            Left            =   120
            TabIndex        =   133
            Text            =   "ERR"
            Top             =   1440
            Width           =   495
         End
         Begin VB.TextBox CmdColor 
            Alignment       =   2  'Center
            BackColor       =   &H0000FFFF&
            ForeColor       =   &H00000000&
            Height          =   285
            Index           =   15
            Left            =   2580
            TabIndex        =   132
            Text            =   "TT"
            Top             =   2640
            Width           =   345
         End
         Begin VB.TextBox CmdColor 
            Alignment       =   2  'Center
            Height          =   285
            Index           =   14
            Left            =   2580
            TabIndex        =   131
            Text            =   "TT"
            Top             =   2340
            Width           =   345
         End
         Begin VB.TextBox CmdColor 
            Alignment       =   2  'Center
            BackColor       =   &H00FFFFFF&
            Height          =   285
            Index           =   13
            Left            =   2580
            TabIndex        =   130
            Text            =   "TT"
            Top             =   2040
            Width           =   345
         End
         Begin VB.TextBox CmdColor 
            Alignment       =   2  'Center
            BackColor       =   &H00FFFFFF&
            ForeColor       =   &H00000000&
            Height          =   285
            Index           =   12
            Left            =   2580
            TabIndex        =   129
            Text            =   "TT"
            Top             =   1740
            Width           =   345
         End
         Begin VB.TextBox CmdColor 
            Alignment       =   2  'Center
            BackColor       =   &H000000FF&
            ForeColor       =   &H00FFFFFF&
            Height          =   285
            Index           =   11
            Left            =   2580
            TabIndex        =   128
            Text            =   "TT"
            Top             =   1440
            Width           =   345
         End
         Begin VB.TextBox SeqNbrAct 
            Alignment       =   1  'Right Justify
            Height          =   285
            Index           =   10
            Left            =   1290
            TabIndex        =   127
            Text            =   "0"
            Top             =   1140
            Width           =   615
         End
         Begin VB.TextBox SeqNbrEnd 
            Alignment       =   1  'Right Justify
            Height          =   285
            Index           =   10
            Left            =   1920
            TabIndex        =   126
            Text            =   "1"
            Top             =   1140
            Width           =   615
         End
         Begin VB.TextBox SeqNbrBgn 
            Alignment       =   1  'Right Justify
            Height          =   285
            Index           =   10
            Left            =   660
            TabIndex        =   125
            Text            =   "1"
            Top             =   1140
            Width           =   615
         End
         Begin VB.TextBox SeqNbrCmd 
            Alignment       =   2  'Center
            Height          =   285
            Index           =   10
            Left            =   120
            TabIndex        =   124
            Text            =   "CCO"
            Top             =   1140
            Width           =   495
         End
         Begin VB.TextBox CmdColor 
            Alignment       =   2  'Center
            BackColor       =   &H00FFFF00&
            Height          =   285
            Index           =   10
            Left            =   2580
            TabIndex        =   123
            Text            =   "TT"
            Top             =   1140
            Width           =   345
         End
         Begin VB.TextBox SeqNbrAct 
            Alignment       =   1  'Right Justify
            Height          =   285
            Index           =   9
            Left            =   1290
            TabIndex        =   122
            Text            =   "0"
            Top             =   840
            Width           =   615
         End
         Begin VB.TextBox SeqNbrEnd 
            Alignment       =   1  'Right Justify
            Height          =   285
            Index           =   9
            Left            =   1920
            TabIndex        =   121
            Text            =   "99999"
            Top             =   840
            Width           =   615
         End
         Begin VB.TextBox SeqNbrBgn 
            Alignment       =   1  'Right Justify
            Height          =   285
            Index           =   9
            Left            =   660
            TabIndex        =   120
            Text            =   "1"
            Top             =   840
            Width           =   615
         End
         Begin VB.TextBox SeqNbrCmd 
            Alignment       =   2  'Center
            Height          =   285
            Index           =   9
            Left            =   120
            TabIndex        =   119
            Text            =   "PFD"
            Top             =   840
            Width           =   495
         End
         Begin VB.TextBox CmdColor 
            Alignment       =   2  'Center
            Height          =   285
            Index           =   9
            Left            =   2580
            TabIndex        =   118
            Text            =   "TT"
            Top             =   840
            Width           =   345
         End
         Begin VB.TextBox SeqNbrAct 
            Alignment       =   1  'Right Justify
            Height          =   285
            Index           =   8
            Left            =   1290
            TabIndex        =   117
            Text            =   "0"
            Top             =   540
            Width           =   615
         End
         Begin VB.TextBox SeqNbrEnd 
            Alignment       =   1  'Right Justify
            Height          =   285
            Index           =   8
            Left            =   1920
            TabIndex        =   116
            Text            =   "99999"
            Top             =   540
            Width           =   615
         End
         Begin VB.TextBox SeqNbrBgn 
            Alignment       =   1  'Right Justify
            Height          =   285
            Index           =   8
            Left            =   660
            TabIndex        =   115
            Text            =   "1"
            Top             =   540
            Width           =   615
         End
         Begin VB.TextBox SeqNbrCmd 
            Alignment       =   2  'Center
            Height          =   285
            Index           =   8
            Left            =   120
            TabIndex        =   114
            Text            =   "RFD"
            Top             =   540
            Width           =   495
         End
         Begin VB.TextBox CmdColor 
            Alignment       =   2  'Center
            BackColor       =   &H00000000&
            ForeColor       =   &H0000FF00&
            Height          =   285
            Index           =   8
            Left            =   2580
            TabIndex        =   113
            Text            =   "TT"
            Top             =   540
            Width           =   345
         End
         Begin VB.Label Label5 
            Caption         =   "Actual"
            Height          =   255
            Index           =   1
            Left            =   1380
            TabIndex        =   157
            Top             =   270
            Width           =   495
         End
         Begin VB.Label Label6 
            Caption         =   "End"
            Height          =   255
            Index           =   1
            Left            =   2010
            TabIndex        =   156
            Top             =   270
            Width           =   375
         End
         Begin VB.Label Label4 
            Caption         =   "Begin"
            Height          =   255
            Index           =   1
            Left            =   750
            TabIndex        =   155
            Top             =   270
            Width           =   495
         End
         Begin VB.Label Label3 
            Caption         =   "Type"
            Height          =   255
            Index           =   1
            Left            =   150
            TabIndex        =   154
            Top             =   270
            Width           =   495
         End
         Begin VB.Label Label7 
            Caption         =   "Log"
            Height          =   255
            Index           =   1
            Left            =   2580
            TabIndex        =   153
            Top             =   270
            Width           =   375
         End
      End
      Begin VB.Frame Frame1 
         Caption         =   "Time Format"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   945
         Index           =   1
         Left            =   6660
         TabIndex        =   107
         Top             =   420
         Width           =   2805
         Begin VB.TextBox TISTFormat 
            Alignment       =   2  'Center
            Height          =   285
            Index           =   1
            Left            =   750
            TabIndex        =   109
            Text            =   "yyyymmddhhmmss"
            Top             =   540
            Width           =   1935
         End
         Begin VB.TextBox Text2 
            Alignment       =   2  'Center
            Enabled         =   0   'False
            Height          =   285
            Index           =   1
            Left            =   120
            TabIndex        =   108
            Text            =   "UTC"
            Top             =   540
            Width           =   615
         End
         Begin VB.Label Label2 
            Caption         =   "Format"
            Height          =   255
            Index           =   1
            Left            =   1350
            TabIndex        =   111
            Top             =   270
            Width           =   615
         End
         Begin VB.Label Label1 
            Caption         =   "Type"
            Height          =   255
            Index           =   1
            Left            =   120
            TabIndex        =   110
            Top             =   270
            Width           =   495
         End
      End
      Begin VB.Frame CdiStruct 
         Caption         =   "Header Structure"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   3045
         Index           =   1
         Left            =   120
         TabIndex        =   80
         Top             =   420
         Width           =   3285
         Begin VB.TextBox CdiHeadDatLen 
            Alignment       =   1  'Right Justify
            Height          =   285
            Index           =   15
            Left            =   2790
            TabIndex        =   96
            Text            =   "5"
            Top             =   2640
            Width           =   375
         End
         Begin VB.TextBox CdiHeadDatLen 
            Alignment       =   1  'Right Justify
            Height          =   285
            Index           =   14
            Left            =   2790
            TabIndex        =   95
            Text            =   "5"
            Top             =   2340
            Width           =   375
         End
         Begin VB.TextBox CdiHeadDatLen 
            Alignment       =   1  'Right Justify
            Height          =   285
            Index           =   13
            Left            =   2790
            TabIndex        =   94
            Text            =   "14"
            Top             =   2040
            Width           =   375
         End
         Begin VB.TextBox CdiHeadDatLen 
            Alignment       =   1  'Right Justify
            Height          =   285
            Index           =   12
            Left            =   2790
            TabIndex        =   93
            Text            =   "3"
            Top             =   1740
            Width           =   375
         End
         Begin VB.TextBox CdiHeadDatLen 
            Alignment       =   1  'Right Justify
            Height          =   285
            Index           =   11
            Left            =   2790
            TabIndex        =   92
            Text            =   "10"
            Top             =   1440
            Width           =   375
         End
         Begin VB.TextBox CdiHeadDatLen 
            Alignment       =   1  'Right Justify
            Height          =   285
            Index           =   10
            Left            =   2790
            TabIndex        =   91
            Text            =   "10"
            Top             =   1140
            Width           =   375
         End
         Begin VB.TextBox CdiHeadDatLen 
            Alignment       =   1  'Right Justify
            Height          =   285
            Index           =   9
            Left            =   2790
            TabIndex        =   90
            Text            =   "3"
            Top             =   840
            Width           =   375
         End
         Begin VB.TextBox CdiHeadDatLen 
            Alignment       =   1  'Right Justify
            Height          =   285
            Index           =   8
            Left            =   2790
            TabIndex        =   89
            Text            =   "0"
            Top             =   540
            Width           =   375
         End
         Begin VB.TextBox CdiHeadCfgDat 
            Height          =   285
            Index           =   15
            Left            =   1320
            TabIndex        =   88
            Text            =   "0"
            Top             =   2640
            Width           =   1455
         End
         Begin VB.TextBox CdiHeadCfgDat 
            Height          =   285
            Index           =   14
            Left            =   1320
            TabIndex        =   87
            Top             =   2340
            Width           =   1455
         End
         Begin VB.TextBox CdiHeadCfgDat 
            Height          =   285
            Index           =   13
            Left            =   1320
            TabIndex        =   86
            Text            =   "19991231235959"
            Top             =   2040
            Width           =   1455
         End
         Begin VB.TextBox CdiHeadCfgDat 
            Height          =   285
            Index           =   12
            Left            =   1320
            TabIndex        =   85
            Text            =   "002"
            Top             =   1740
            Width           =   1455
         End
         Begin VB.TextBox CdiHeadCfgDat 
            Height          =   285
            Index           =   11
            Left            =   1320
            TabIndex        =   84
            Text            =   "UFISFIDS"
            Top             =   1440
            Width           =   1455
         End
         Begin VB.TextBox CdiHeadCfgDat 
            Height          =   285
            Index           =   10
            Left            =   1320
            TabIndex        =   83
            Text            =   "OADCS"
            Top             =   1140
            Width           =   1455
         End
         Begin VB.TextBox CdiHeadCfgDat 
            Height          =   285
            Index           =   9
            Left            =   1320
            TabIndex        =   82
            Text            =   "FID"
            Top             =   840
            Width           =   1455
         End
         Begin VB.TextBox CdiHeadCfgDat 
            Height          =   285
            Index           =   8
            Left            =   1320
            TabIndex        =   81
            Top             =   540
            Width           =   1455
         End
         Begin VB.Label MsgLen 
            Caption         =   "Data Length"
            Height          =   255
            Index           =   1
            Left            =   120
            TabIndex        =   106
            Top             =   2670
            Width           =   1200
         End
         Begin VB.Label MsgNbr 
            Caption         =   "Seq. Number"
            Height          =   255
            Index           =   1
            Left            =   120
            TabIndex        =   105
            Top             =   2370
            Width           =   1200
         End
         Begin VB.Label TIST 
            Caption         =   "Timestamp"
            Height          =   255
            Index           =   1
            Left            =   120
            TabIndex        =   104
            Top             =   2070
            Width           =   1200
         End
         Begin VB.Label CdiVersion 
            Caption         =   "CDI Version"
            Height          =   255
            Index           =   1
            Left            =   120
            TabIndex        =   103
            Top             =   1770
            Width           =   1200
         End
         Begin VB.Label RcvName 
            Caption         =   "Receiver Name"
            Height          =   255
            Index           =   1
            Left            =   120
            TabIndex        =   102
            Top             =   1470
            Width           =   1200
         End
         Begin VB.Label SndName 
            Caption         =   "Sender Name"
            Height          =   255
            Index           =   1
            Left            =   120
            TabIndex        =   101
            Top             =   1200
            Width           =   1200
         End
         Begin VB.Label ServerSystem 
            Caption         =   "Server System"
            Height          =   255
            Index           =   1
            Left            =   120
            TabIndex        =   100
            Top             =   870
            Width           =   1200
         End
         Begin VB.Label CdiPrefix 
            Caption         =   "CDI Prefix (Len)"
            Height          =   255
            Index           =   1
            Left            =   120
            TabIndex        =   99
            Top             =   570
            Width           =   1200
         End
         Begin VB.Label CdiVal 
            Caption         =   "Value (Default)"
            Height          =   255
            Index           =   1
            Left            =   1380
            TabIndex        =   98
            Top             =   270
            Width           =   1155
         End
         Begin VB.Label CdiLen 
            Caption         =   "Len"
            Height          =   255
            Index           =   1
            Left            =   2820
            TabIndex        =   97
            Top             =   270
            Width           =   315
         End
      End
   End
   Begin VB.Frame CfgSection 
      BackColor       =   &H00C0C0C0&
      Caption         =   "To OA DCS (Outgoing Messages)"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   161
         Weight          =   700
         Underline       =   -1  'True
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FF0000&
      Height          =   3585
      Index           =   0
      Left            =   60
      TabIndex        =   0
      Top             =   60
      Width           =   9585
      Begin VB.CommandButton Command2 
         Caption         =   "Close"
         Height          =   255
         Index           =   0
         Left            =   8460
         TabIndex        =   162
         Top             =   3210
         Width           =   975
      End
      Begin VB.CheckBox TestSend 
         Caption         =   "Test"
         Height          =   255
         Left            =   7410
         Style           =   1  'Graphical
         TabIndex        =   160
         Top             =   3210
         Width           =   975
      End
      Begin VB.Frame Frame3 
         Caption         =   "Future Use"
         Height          =   1605
         Index           =   0
         Left            =   6660
         TabIndex        =   158
         Top             =   1470
         Width           =   2805
      End
      Begin VB.Frame CdiStruct 
         Caption         =   "Header Structure"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   3045
         Index           =   0
         Left            =   120
         TabIndex        =   27
         Top             =   420
         Width           =   3285
         Begin VB.TextBox CdiHeadCfgDat 
            Height          =   285
            Index           =   0
            Left            =   1320
            TabIndex        =   43
            Top             =   540
            Width           =   1455
         End
         Begin VB.TextBox CdiHeadCfgDat 
            Height          =   285
            Index           =   1
            Left            =   1320
            TabIndex        =   42
            Text            =   "FID"
            Top             =   840
            Width           =   1455
         End
         Begin VB.TextBox CdiHeadCfgDat 
            Height          =   285
            Index           =   2
            Left            =   1320
            TabIndex        =   41
            Text            =   "UFISFIDS"
            Top             =   1140
            Width           =   1455
         End
         Begin VB.TextBox CdiHeadCfgDat 
            Height          =   285
            Index           =   3
            Left            =   1320
            TabIndex        =   40
            Text            =   "OADCS"
            Top             =   1440
            Width           =   1455
         End
         Begin VB.TextBox CdiHeadCfgDat 
            Height          =   285
            Index           =   4
            Left            =   1320
            TabIndex        =   39
            Text            =   "002"
            Top             =   1740
            Width           =   1455
         End
         Begin VB.TextBox CdiHeadCfgDat 
            Height          =   285
            Index           =   5
            Left            =   1320
            TabIndex        =   38
            Text            =   "19991231235959"
            Top             =   2040
            Width           =   1455
         End
         Begin VB.TextBox CdiHeadCfgDat 
            Height          =   285
            Index           =   6
            Left            =   1320
            TabIndex        =   37
            Top             =   2340
            Width           =   1455
         End
         Begin VB.TextBox CdiHeadCfgDat 
            Height          =   285
            Index           =   7
            Left            =   1320
            TabIndex        =   36
            Text            =   "0"
            Top             =   2640
            Width           =   1455
         End
         Begin VB.TextBox CdiHeadDatLen 
            Alignment       =   1  'Right Justify
            Height          =   285
            Index           =   0
            Left            =   2790
            TabIndex        =   35
            Text            =   "0"
            Top             =   540
            Width           =   375
         End
         Begin VB.TextBox CdiHeadDatLen 
            Alignment       =   1  'Right Justify
            Height          =   285
            Index           =   1
            Left            =   2790
            TabIndex        =   34
            Text            =   "3"
            Top             =   840
            Width           =   375
         End
         Begin VB.TextBox CdiHeadDatLen 
            Alignment       =   1  'Right Justify
            Height          =   285
            Index           =   2
            Left            =   2790
            TabIndex        =   33
            Text            =   "10"
            Top             =   1140
            Width           =   375
         End
         Begin VB.TextBox CdiHeadDatLen 
            Alignment       =   1  'Right Justify
            Height          =   285
            Index           =   3
            Left            =   2790
            TabIndex        =   32
            Text            =   "10"
            Top             =   1440
            Width           =   375
         End
         Begin VB.TextBox CdiHeadDatLen 
            Alignment       =   1  'Right Justify
            Height          =   285
            Index           =   4
            Left            =   2790
            TabIndex        =   31
            Text            =   "3"
            Top             =   1740
            Width           =   375
         End
         Begin VB.TextBox CdiHeadDatLen 
            Alignment       =   1  'Right Justify
            Height          =   285
            Index           =   5
            Left            =   2790
            TabIndex        =   30
            Text            =   "14"
            Top             =   2040
            Width           =   375
         End
         Begin VB.TextBox CdiHeadDatLen 
            Alignment       =   1  'Right Justify
            Height          =   285
            Index           =   6
            Left            =   2790
            TabIndex        =   29
            Text            =   "5"
            Top             =   2340
            Width           =   375
         End
         Begin VB.TextBox CdiHeadDatLen 
            Alignment       =   1  'Right Justify
            Height          =   285
            Index           =   7
            Left            =   2790
            TabIndex        =   28
            Text            =   "5"
            Top             =   2640
            Width           =   375
         End
         Begin VB.Label CdiLen 
            Caption         =   "Len"
            Height          =   255
            Index           =   0
            Left            =   2820
            TabIndex        =   57
            Top             =   270
            Width           =   315
         End
         Begin VB.Label CdiVal 
            Caption         =   "Value (Default)"
            Height          =   255
            Index           =   0
            Left            =   1380
            TabIndex        =   56
            Top             =   270
            Width           =   1155
         End
         Begin VB.Label CdiPrefix 
            Caption         =   "CDI Prefix (Len)"
            Height          =   255
            Index           =   0
            Left            =   120
            TabIndex        =   51
            Top             =   570
            Width           =   1200
         End
         Begin VB.Label ServerSystem 
            Caption         =   "Server System"
            Height          =   255
            Index           =   0
            Left            =   120
            TabIndex        =   50
            Top             =   870
            Width           =   1200
         End
         Begin VB.Label SndName 
            Caption         =   "Sender Name"
            Height          =   255
            Index           =   0
            Left            =   120
            TabIndex        =   49
            Top             =   1200
            Width           =   1200
         End
         Begin VB.Label RcvName 
            Caption         =   "Receiver Name"
            Height          =   255
            Index           =   0
            Left            =   120
            TabIndex        =   48
            Top             =   1470
            Width           =   1200
         End
         Begin VB.Label CdiVersion 
            Caption         =   "CDI Version"
            Height          =   255
            Index           =   0
            Left            =   120
            TabIndex        =   47
            Top             =   1770
            Width           =   1200
         End
         Begin VB.Label TIST 
            Caption         =   "Timestamp"
            Height          =   255
            Index           =   0
            Left            =   120
            TabIndex        =   46
            Top             =   2070
            Width           =   1200
         End
         Begin VB.Label MsgNbr 
            Caption         =   "Seq. Number"
            Height          =   255
            Index           =   0
            Left            =   120
            TabIndex        =   45
            Top             =   2370
            Width           =   1200
         End
         Begin VB.Label MsgLen 
            Caption         =   "Data Length"
            Height          =   255
            Index           =   0
            Left            =   120
            TabIndex        =   44
            Top             =   2670
            Width           =   1200
         End
      End
      Begin VB.Frame Frame1 
         Caption         =   "Time Format"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   945
         Index           =   0
         Left            =   6660
         TabIndex        =   22
         Top             =   420
         Width           =   2805
         Begin VB.TextBox Text2 
            Alignment       =   2  'Center
            Enabled         =   0   'False
            Height          =   285
            Index           =   0
            Left            =   120
            TabIndex        =   24
            Text            =   "UTC"
            Top             =   540
            Width           =   615
         End
         Begin VB.TextBox TISTFormat 
            Alignment       =   2  'Center
            Height          =   285
            Index           =   0
            Left            =   750
            TabIndex        =   23
            Text            =   "yyyymmddhhmmss"
            Top             =   540
            Width           =   1935
         End
         Begin VB.Label Label1 
            Caption         =   "Type"
            Height          =   255
            Index           =   0
            Left            =   120
            TabIndex        =   26
            Top             =   270
            Width           =   495
         End
         Begin VB.Label Label2 
            Caption         =   "Format"
            Height          =   255
            Index           =   0
            Left            =   1350
            TabIndex        =   25
            Top             =   270
            Width           =   615
         End
      End
      Begin VB.Frame Frame2 
         Caption         =   "Number Circles"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   3045
         Index           =   0
         Left            =   3510
         TabIndex        =   1
         Top             =   420
         Width           =   3045
         Begin VB.TextBox CmdColor 
            Alignment       =   2  'Center
            BackColor       =   &H0000FFFF&
            Height          =   285
            Index           =   7
            Left            =   2580
            TabIndex        =   77
            Text            =   "TT"
            Top             =   2640
            Width           =   345
         End
         Begin VB.TextBox SeqNbrCmd 
            Alignment       =   2  'Center
            Height          =   285
            Index           =   7
            Left            =   120
            TabIndex        =   76
            Text            =   "SYS"
            Top             =   2640
            Width           =   495
         End
         Begin VB.TextBox SeqNbrBgn 
            Alignment       =   1  'Right Justify
            Height          =   285
            Index           =   7
            Left            =   660
            TabIndex        =   75
            Top             =   2640
            Width           =   615
         End
         Begin VB.TextBox SeqNbrEnd 
            Alignment       =   1  'Right Justify
            Height          =   285
            Index           =   7
            Left            =   1920
            TabIndex        =   74
            Top             =   2640
            Width           =   615
         End
         Begin VB.TextBox SeqNbrAct 
            Alignment       =   1  'Right Justify
            Height          =   285
            Index           =   7
            Left            =   1290
            TabIndex        =   73
            Top             =   2640
            Width           =   615
         End
         Begin VB.TextBox CmdColor 
            Alignment       =   2  'Center
            Height          =   285
            Index           =   6
            Left            =   2580
            TabIndex        =   72
            Text            =   "TT"
            Top             =   2340
            Width           =   345
         End
         Begin VB.TextBox SeqNbrCmd 
            Alignment       =   2  'Center
            Height          =   285
            Index           =   6
            Left            =   120
            TabIndex        =   71
            Top             =   2340
            Width           =   495
         End
         Begin VB.TextBox SeqNbrBgn 
            Alignment       =   1  'Right Justify
            Height          =   285
            Index           =   6
            Left            =   660
            TabIndex        =   70
            Top             =   2340
            Width           =   615
         End
         Begin VB.TextBox SeqNbrEnd 
            Alignment       =   1  'Right Justify
            Height          =   285
            Index           =   6
            Left            =   1920
            TabIndex        =   69
            Top             =   2340
            Width           =   615
         End
         Begin VB.TextBox SeqNbrAct 
            Alignment       =   1  'Right Justify
            Height          =   285
            Index           =   6
            Left            =   1290
            TabIndex        =   68
            Top             =   2340
            Width           =   615
         End
         Begin VB.TextBox CmdColor 
            Alignment       =   2  'Center
            Height          =   285
            Index           =   5
            Left            =   2580
            TabIndex        =   67
            Text            =   "TT"
            Top             =   2040
            Width           =   345
         End
         Begin VB.TextBox SeqNbrCmd 
            Alignment       =   2  'Center
            Height          =   285
            Index           =   5
            Left            =   120
            TabIndex        =   66
            Top             =   2040
            Width           =   495
         End
         Begin VB.TextBox SeqNbrBgn 
            Alignment       =   1  'Right Justify
            Height          =   285
            Index           =   5
            Left            =   660
            TabIndex        =   65
            Top             =   2040
            Width           =   615
         End
         Begin VB.TextBox SeqNbrEnd 
            Alignment       =   1  'Right Justify
            Height          =   285
            Index           =   5
            Left            =   1920
            TabIndex        =   64
            Top             =   2040
            Width           =   615
         End
         Begin VB.TextBox SeqNbrAct 
            Alignment       =   1  'Right Justify
            Height          =   285
            Index           =   5
            Left            =   1290
            TabIndex        =   63
            Top             =   2040
            Width           =   615
         End
         Begin VB.TextBox CmdColor 
            Alignment       =   2  'Center
            Height          =   285
            Index           =   4
            Left            =   2580
            TabIndex        =   62
            Text            =   "TT"
            Top             =   1740
            Width           =   345
         End
         Begin VB.TextBox CmdColor 
            Alignment       =   2  'Center
            BackColor       =   &H000000FF&
            ForeColor       =   &H00FFFFFF&
            Height          =   285
            Index           =   3
            Left            =   2580
            TabIndex        =   61
            Text            =   "TT"
            Top             =   1440
            Width           =   345
         End
         Begin VB.TextBox CmdColor 
            Alignment       =   2  'Center
            BackColor       =   &H00FFFF00&
            Height          =   285
            Index           =   2
            Left            =   2580
            TabIndex        =   60
            Text            =   "TT"
            Top             =   1140
            Width           =   345
         End
         Begin VB.TextBox CmdColor 
            Alignment       =   2  'Center
            Height          =   285
            Index           =   1
            Left            =   2580
            TabIndex        =   59
            Text            =   "TT"
            Top             =   840
            Width           =   345
         End
         Begin VB.TextBox CmdColor 
            Alignment       =   2  'Center
            BackColor       =   &H00000000&
            ForeColor       =   &H0000FF00&
            Height          =   285
            Index           =   0
            Left            =   2580
            TabIndex        =   58
            Text            =   "TT"
            Top             =   540
            Width           =   345
         End
         Begin VB.TextBox SeqNbrCmd 
            Alignment       =   2  'Center
            Height          =   285
            Index           =   4
            Left            =   120
            TabIndex        =   55
            Top             =   1740
            Width           =   495
         End
         Begin VB.TextBox SeqNbrBgn 
            Alignment       =   1  'Right Justify
            Height          =   285
            Index           =   4
            Left            =   660
            TabIndex        =   54
            Top             =   1740
            Width           =   615
         End
         Begin VB.TextBox SeqNbrEnd 
            Alignment       =   1  'Right Justify
            Height          =   285
            Index           =   4
            Left            =   1920
            TabIndex        =   53
            Top             =   1740
            Width           =   615
         End
         Begin VB.TextBox SeqNbrAct 
            Alignment       =   1  'Right Justify
            Height          =   285
            Index           =   4
            Left            =   1290
            TabIndex        =   52
            Top             =   1740
            Width           =   615
         End
         Begin VB.TextBox SeqNbrCmd 
            Alignment       =   2  'Center
            Height          =   285
            Index           =   0
            Left            =   120
            TabIndex        =   17
            Text            =   "RFD"
            Top             =   540
            Width           =   495
         End
         Begin VB.TextBox SeqNbrCmd 
            Alignment       =   2  'Center
            Height          =   285
            Index           =   1
            Left            =   120
            TabIndex        =   16
            Text            =   "UFD"
            Top             =   840
            Width           =   495
         End
         Begin VB.TextBox SeqNbrBgn 
            Alignment       =   1  'Right Justify
            Height          =   285
            Index           =   0
            Left            =   660
            TabIndex        =   15
            Text            =   "1"
            Top             =   540
            Width           =   615
         End
         Begin VB.TextBox SeqNbrBgn 
            Alignment       =   1  'Right Justify
            Height          =   285
            Index           =   1
            Left            =   660
            TabIndex        =   14
            Text            =   "1"
            Top             =   840
            Width           =   615
         End
         Begin VB.TextBox SeqNbrEnd 
            Alignment       =   1  'Right Justify
            Height          =   285
            Index           =   0
            Left            =   1920
            TabIndex        =   13
            Text            =   "99999"
            Top             =   540
            Width           =   615
         End
         Begin VB.TextBox SeqNbrEnd 
            Alignment       =   1  'Right Justify
            Height          =   285
            Index           =   1
            Left            =   1920
            TabIndex        =   12
            Text            =   "99999"
            Top             =   840
            Width           =   615
         End
         Begin VB.TextBox SeqNbrAct 
            Alignment       =   1  'Right Justify
            Height          =   285
            Index           =   0
            Left            =   1290
            TabIndex        =   11
            Text            =   "0"
            Top             =   540
            Width           =   615
         End
         Begin VB.TextBox SeqNbrAct 
            Alignment       =   1  'Right Justify
            Height          =   285
            Index           =   1
            Left            =   1290
            TabIndex        =   10
            Text            =   "0"
            Top             =   840
            Width           =   615
         End
         Begin VB.TextBox SeqNbrAct 
            Alignment       =   1  'Right Justify
            Height          =   285
            Index           =   2
            Left            =   1290
            TabIndex        =   9
            Text            =   "0"
            Top             =   1140
            Width           =   615
         End
         Begin VB.TextBox SeqNbrEnd 
            Alignment       =   1  'Right Justify
            Height          =   285
            Index           =   2
            Left            =   1920
            TabIndex        =   8
            Text            =   "1"
            Top             =   1140
            Width           =   615
         End
         Begin VB.TextBox SeqNbrBgn 
            Alignment       =   1  'Right Justify
            Height          =   285
            Index           =   2
            Left            =   660
            TabIndex        =   7
            Text            =   "1"
            Top             =   1140
            Width           =   615
         End
         Begin VB.TextBox SeqNbrCmd 
            Alignment       =   2  'Center
            Height          =   285
            Index           =   2
            Left            =   120
            TabIndex        =   6
            Text            =   "CCO"
            Top             =   1140
            Width           =   495
         End
         Begin VB.TextBox SeqNbrAct 
            Alignment       =   1  'Right Justify
            Height          =   285
            Index           =   3
            Left            =   1290
            TabIndex        =   5
            Text            =   "0"
            Top             =   1440
            Width           =   615
         End
         Begin VB.TextBox SeqNbrEnd 
            Alignment       =   1  'Right Justify
            Height          =   285
            Index           =   3
            Left            =   1920
            TabIndex        =   4
            Text            =   "1"
            Top             =   1440
            Width           =   615
         End
         Begin VB.TextBox SeqNbrBgn 
            Alignment       =   1  'Right Justify
            Height          =   285
            Index           =   3
            Left            =   660
            TabIndex        =   3
            Text            =   "1"
            Top             =   1440
            Width           =   615
         End
         Begin VB.TextBox SeqNbrCmd 
            Alignment       =   2  'Center
            Height          =   285
            Index           =   3
            Left            =   120
            TabIndex        =   2
            Text            =   "ERR"
            Top             =   1440
            Width           =   495
         End
         Begin VB.Label Label7 
            Caption         =   "Log"
            Height          =   255
            Index           =   0
            Left            =   2580
            TabIndex        =   78
            Top             =   270
            Width           =   375
         End
         Begin VB.Label Label3 
            Caption         =   "Type"
            Height          =   255
            Index           =   0
            Left            =   150
            TabIndex        =   21
            Top             =   270
            Width           =   495
         End
         Begin VB.Label Label4 
            Caption         =   "Begin"
            Height          =   255
            Index           =   0
            Left            =   750
            TabIndex        =   20
            Top             =   270
            Width           =   495
         End
         Begin VB.Label Label6 
            Caption         =   "End"
            Height          =   255
            Index           =   0
            Left            =   2010
            TabIndex        =   19
            Top             =   270
            Width           =   375
         End
         Begin VB.Label Label5 
            Caption         =   "Actual"
            Height          =   255
            Index           =   0
            Left            =   1380
            TabIndex        =   18
            Top             =   270
            Width           =   495
         End
      End
   End
End
Attribute VB_Name = "CdiHeader"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public Sub BuildMessageHeader(ChkSeqIdx As Integer, ActCmd As String, DatLen As Integer, MessageHeader As String)
    Dim ActIdx As Integer
    Dim SeqIdx As Integer
    Dim idxOff As Integer
    Dim IdxFrom As Integer
    Dim IdxTo As Integer
    Dim CfgSec As Integer
    Dim tmpText As String
    Dim CdiMessageHeader As String
    CdiMessageHeader = ""
    SeqIdx = ChkSeqIdx
    Select Case SeqIdx
        Case FOR_SEND
            IdxFrom = 0
            IdxTo = 7
        Case FOR_RECV
            IdxFrom = 8
            IdxTo = 15
        Case Else
            'dedicated index
    End Select
    If SeqIdx < 0 Then
        If ActCmd <> "" Then
            For ActIdx = IdxFrom To IdxTo
                If ActCmd = SeqNbrCmd(ActIdx).Text Then
                    SeqIdx = ActIdx
                    Exit For
                End If
            Next ActIdx
        End If
    End If
    If SeqIdx < 8 Then
        idxOff = 0
        CfgSec = 0
    Else
        idxOff = 8
        CfgSec = 1
    End If
    If SeqIdx >= 0 Then
        CdiHeadCfgDat(idxOff + 5).Text = Format(Now, TISTFormat(CfgSec).Text)
        CdiHeadCfgDat(idxOff + 7).Text = DatLen
        SeqNbrAct(SeqIdx).Text = Val(SeqNbrAct(SeqIdx).Text) + 1
        If Val(SeqNbrAct(SeqIdx).Text) > Val(SeqNbrEnd(SeqIdx).Text) Then
            SeqNbrAct(SeqIdx).Text = SeqNbrBgn(SeqIdx).Text
        End If
        CdiHeadCfgDat(idxOff + 6).Text = SeqNbrAct(SeqIdx).Text
        For ActIdx = IdxFrom To IdxTo
            If CdiHeadDatLen(ActIdx).Text > 0 Then
                tmpText = String(CdiHeadDatLen(ActIdx).Text, " ")
                Mid(tmpText, 1) = Trim(CdiHeadCfgDat(ActIdx).Text)
                CdiMessageHeader = CdiMessageHeader & tmpText
            End If
        Next ActIdx
        MessageHeader = CdiMessageHeader
    End If
End Sub

Public Function CheckRcvMsg(RcvMsg As String, ActCmd As String, FieldList As String, DataList As String) As Boolean
Dim ActIdx As Integer
Dim actPos As Integer
Dim ilLen As Integer
Dim datIdx As Integer
Dim tmpData(8) As String
Dim RetVal As Boolean
Dim tmpVal As Integer
Dim MsgDat As String
Dim FldNam As String
Dim ItmNbr As Integer
Dim ItmFrom As Integer
Dim ItmTo As Integer
Dim ItmOff As Integer
Dim tmpBuf As String
Dim CedaCmd As String
Dim CustRetVal As Boolean
    RetVal = RC_SUCCESS
    ActCmd = ""
    FieldList = ""
    DataList = ""
    If CDI.SendMode(0).Value = True Then
        ItmFrom = 8
        ItmTo = 15
        ItmOff = 8
    Else
        ItmFrom = 0
        ItmTo = 7
        ItmOff = 0
    End If
    'Strip Header KeyWords from Message Text
    actPos = 1
    datIdx = 0
    For ActIdx = ItmFrom To ItmTo
        ilLen = Val(CdiHeadDatLen(ActIdx))
        If ilLen > 0 Then
            tmpData(datIdx) = Mid(RcvMsg, actPos, ilLen)
            actPos = actPos + ilLen
        End If
        datIdx = datIdx + 1
    Next
    'actPos now points to the begin of Data
    'it will be used later on. So don't change it.
    datIdx = 0
    'Check Header KeyWords
    For ActIdx = ItmFrom To ItmTo - 3
        If Trim(tmpData(datIdx)) <> Trim(CdiHeadCfgDat(ActIdx)) Then
            FieldList = FieldList & str(datIdx) & ","
            DataList = DataList & Trim(tmpData(datIdx)) & ","
            RetVal = RC_FAIL
        End If
        datIdx = datIdx + 1
    Next
    'Check SeqNumber - depending on Command
    ActCmd = Trim(Mid(RcvMsg, actPos, 3))
    CdiHeadCfgDat(ItmOff + 6) = tmpData(6)
    tmpVal = CheckRcvSeqNbr(ActCmd, tmpData(6))
    If tmpVal < 0 Then
        FieldList = FieldList & "6,"
        DataList = DataList & tmpVal & ";" & Trim(tmpData(6)) & ","
        RetVal = False
    End If
    'Timestamp -- not checked
    CdiHeadCfgDat(ItmOff + 5) = tmpData(5)
    'Check Data Length
    CdiHeadCfgDat(ItmOff + 7) = tmpData(7)
    tmpVal = Len(RcvMsg) - actPos + 1
    If tmpVal <> Val(tmpData(7)) Then
        FieldList = FieldList & "7,"
        DataList = DataList & Trim(tmpData(7)) & ","
        RetVal = False
    End If
    If RetVal = RC_SUCCESS Then
        'ALL OK, build FieldList and DataList
        'First Check For Cmd RFD
        actPos = actPos + 3
        Select Case ActCmd
            Case "RFD"
                'Get Command Type: "R" or "A"
                DataList = Mid(RcvMsg, actPos, 1)
            Case "PFD", "UFD"
                MsgDat = Mid(RcvMsg, actPos)
                CedaCmd = GetItem(MsgDat, 1, "|")
                FieldList = GetItem(MsgDat, 2, "|")
                DataList = GetItem(MsgDat, 3, "|")
                If CDI.SendMode(0).Value = True Then
                    PrepareCedaEvent FieldList, DataList
                Else
                    tmpBuf = CedaCmd & "|" & FieldList & "|" & DataList
                    tmpBuf = Replace(tmpBuf, ",", ";", 1, -1, vbBinaryCompare)
                    CDI.DisplayMessage -3, tmpBuf, ActCmd
                    'CONNECT HERE TO CUSTOMER FORM
                    CustRetVal = Customer.DataReceived(CedaCmd, FieldList, DataList)
                End If
            Case "CCO"
            Case "ERR"
            Case Else
        End Select
    End If
    CheckRcvMsg = RetVal
End Function

Private Function CheckRcvSeqNbr(ActCmd As String, tmpNbr As String) As Integer
Dim fndIdx As Integer
Dim ActIdx As Integer
Dim RetVal As Integer
    RetVal = Val(tmpNbr)
    fndIdx = -1
    If ActCmd <> "" Then
        'Received Numbers are in Index 8 to 15
        For ActIdx = 8 To 15
            If ActCmd = SeqNbrCmd(ActIdx).Text Then
                fndIdx = ActIdx
                Exit For
            End If
        Next ActIdx
    End If
    If fndIdx >= 0 Then
        'Increase Current SeqNumber
        SeqNbrAct(fndIdx).Text = Val(SeqNbrAct(fndIdx).Text) + 1
        If Val(SeqNbrAct(fndIdx).Text) > Val(SeqNbrEnd(fndIdx).Text) Then
            SeqNbrAct(fndIdx).Text = SeqNbrBgn(fndIdx).Text
        End If
        If Trim(tmpNbr) <> Trim(SeqNbrAct(fndIdx).Text) Then
            If Val(Trim(tmpNbr)) = 1 Then
                'Implicit Reset Of Sequence
                SeqNbrAct(fndIdx).Text = Trim(tmpNbr)
            Else
                RetVal = -2
            End If
        End If
    Else
        RetVal = -1
    End If
    'Activate to accept any SeqNumber
    RetVal = Val(tmpNbr)
    CheckRcvSeqNbr = RetVal
End Function

Private Sub PrepareCedaEvent(AtcFieldList As String, AtcDataList As String)
Dim ActFldLst As String
Dim OldFldLst As String
Dim ActDatLst As String
Dim OldDatLst As String
Dim retStr As String
Dim tmpFldNam As String
Dim tmpFldDat As String
Dim tmpAdid As String
Dim ItmNbr As Integer
Dim tmpLen As Integer
Dim CedaFields As String
Dim CedaData As String

    'OA DCS: ARCID,ADEP,ADEPOLD,ADES,ADESOLD,ETD ,ETDOLD ,ETA ,ATD ,ATA ,RWYID ,ARCTYP,STND
    'UFIS:   CSGN ,ORG4,ORG4old,DES4,DES4old,ETDA,ETDAold,ETAA,AIRA,LNDA,RWYA/D,------,----
    ActFldLst = ""
    OldFldLst = ""
    ActDatLst = ""
    OldDatLst = ""
    ItmNbr = 0
    Do
        ItmNbr = ItmNbr + 1
        tmpFldNam = GetItem(AtcFieldList, ItmNbr, ",")
        If tmpFldNam <> "" Then
            tmpFldDat = GetItem(AtcDataList, ItmNbr, ",")
            Select Case tmpFldNam
                Case "ARCID"
                    ActFldLst = ActFldLst & "CSGN,"
                    ActDatLst = ActDatLst & tmpFldDat & ","
                Case "ADEP"
                    ActFldLst = ActFldLst & "ORG4,"
                    If tmpFldDat = "LGAV" Then tmpFldDat = "LGAT"
                    ActDatLst = ActDatLst & tmpFldDat & ","
                Case "ADEPOLD"
                    OldFldLst = OldFldLst & "ORG4,"
                    If tmpFldDat = "LGAV" Then tmpFldDat = "LGAT"
                    OldDatLst = OldDatLst & tmpFldDat & ","
                Case "ADES"
                    ActFldLst = ActFldLst & "DES4,"
                    If tmpFldDat = "LGAV" Then tmpFldDat = "LGAT"
                    ActDatLst = ActDatLst & tmpFldDat & ","
                Case "ADESOLD"
                    OldFldLst = OldFldLst & "DES4,"
                    If tmpFldDat = "LGAV" Then tmpFldDat = "LGAT"
                    OldDatLst = OldDatLst & tmpFldDat & ","
                Case "ETD"
                    ActFldLst = ActFldLst & "ETDA,"
                    ActDatLst = ActDatLst & tmpFldDat & ","
                Case "ETDOLD"
                    OldFldLst = OldFldLst & "ETDA,"
                    OldDatLst = OldDatLst & tmpFldDat & ","
                Case "ETA"
                    ActFldLst = ActFldLst & "ETAA,"
                    ActDatLst = ActDatLst & tmpFldDat & ","
                Case "ATA"
                    ActFldLst = ActFldLst & "LNDA,"
                    ActDatLst = ActDatLst & tmpFldDat & ","
                Case "RWYID"
                    ActFldLst = ActFldLst & "RWYA,"
                    ActDatLst = ActDatLst & tmpFldDat & ","
                Case "ARCTYP"
                    ActFldLst = ActFldLst & "ACT5,"
                    ActDatLst = ActDatLst & tmpFldDat & ","
                Case Else
            End Select
        End If
    Loop While tmpFldNam <> ""
    tmpLen = Len(ActFldLst) - 1
    If tmpLen > 0 Then ActFldLst = Left(ActFldLst, tmpLen)
    tmpLen = Len(OldFldLst) - 1
    If tmpLen > 0 Then OldFldLst = Left(OldFldLst, tmpLen)
    tmpLen = Len(ActDatLst) - 1
    If tmpLen > 0 Then ActDatLst = Left(ActDatLst, tmpLen)
    tmpLen = Len(OldDatLst) - 1
    If tmpLen > 0 Then OldDatLst = Left(OldDatLst, tmpLen)
    CedaFields = ActFldLst & vbLf & OldFldLst
    CedaData = ActDatLst & vbLf & OldDatLst
    'MsgBox CedaFields & vbNewLine & CedaData
    retStr = CDI.aUfis.Ufis("EXCO", "AFTTAB", CedaFields, "ATC", CedaData)
End Sub

Public Sub CdiHeaderReset(SeqIdx As Integer, ActCmd As String, ForWhat As String)
Dim ActIdx As Integer
Dim fndIdx As Integer
Dim idxOff As Integer
    If ForWhat = "SND" Then idxOff = 0 Else idxOff = 8
    If (SeqIdx >= 0) Or (ActCmd <> "") Then
        'dedicated reset
        fndIdx = SeqIdx
        If SeqIdx < 0 Then
            For ActIdx = idxOff + 0 To idxOff + 7
                If ActCmd = SeqNbrCmd(ActIdx).Text Then
                    fndIdx = ActIdx
                    Exit For
                End If
            Next ActIdx
        End If
        If fndIdx >= 0 Then
            SeqNbrAct(fndIdx).Text = Val(SeqNbrBgn(fndIdx).Text) - 1
            CdiHeadCfgDat(idxOff + 5).Text = "Reset of " & SeqNbrCmd(fndIdx).Text
        End If
    Else
        'Full reset
        For ActIdx = idxOff + 0 To idxOff + 7
            If Trim(SeqNbrCmd(ActIdx).Text) <> "" Then
                If Trim(SeqNbrBgn(ActIdx).Text) <> "" Then
                    SeqNbrAct(ActIdx).Text = Val(SeqNbrBgn(ActIdx).Text) - 1
                End If
            End If
        Next ActIdx
        CdiHeadCfgDat(idxOff + 5).Text = "Complete Reset"
    End If
    CdiHeadCfgDat(idxOff + 6).Text = 0
    CdiHeadCfgDat(idxOff + 7).Text = 0
End Sub

Public Sub DecreaseSequence(ChkSeqIdx As Integer, ActCmd As String)
Dim SeqIdx As Integer
Dim IdxFrom As Integer
Dim IdxTo As Integer
Dim ActIdx As Integer
    SeqIdx = ChkSeqIdx
    Select Case SeqIdx
        Case FOR_SEND
            IdxFrom = 0
            IdxTo = 7
        Case FOR_RECV
            IdxFrom = 8
            IdxTo = 15
        Case Else
            'dedicated index
    End Select
    If SeqIdx < 0 Then
        If ActCmd <> "" Then
            For ActIdx = IdxFrom To IdxTo
                If ActCmd = SeqNbrCmd(ActIdx).Text Then
                    SeqIdx = ActIdx
                    Exit For
                End If
            Next ActIdx
        End If
    End If
    If SeqIdx >= 0 Then
        SeqNbrAct(SeqIdx).Text = Val(SeqNbrAct(SeqIdx).Text) - 1
        If Val(SeqNbrAct(SeqIdx).Text) < Val(SeqNbrBgn(SeqIdx).Text) Then
            SeqNbrAct(SeqIdx).Text = SeqNbrEnd(SeqIdx).Text
        End If
    End If
End Sub
Private Sub Command2_Click(Index As Integer)
    Me.Hide
End Sub

Private Sub Form_Activate()
    If CDI.SendMode(0).Value = True Then
        Caption = "CDI Header Configuration: UFIS/AODB to OA/DCS"
        CfgSection(0).Caption = "To OA/DCS (Outgoing Messages)"
        CfgSection(1).Caption = "From OA/DCS (Incoming Messages)"
    Else
        Caption = "CDI Header Configuration: OA/DCS to UFIS/AODB (Simulator)"
        CfgSection(1).Caption = "To UFIS/AODB (Outgoing Messages)"
        CfgSection(0).Caption = "From UFIS/AODB (Incoming Messages)"
    End If
End Sub

Private Sub TestRecv_Click()
    If TestRecv.Value = 1 Then
        Unload HeaderTest
        Load HeaderTest
        HeaderTest.InitHeaderTest FOR_RECV
        HeaderTest.Show , Me
    End If
End Sub

Private Sub TestSend_Click()
    If TestSend.Value = 1 Then
        Unload HeaderTest
        Load HeaderTest
        HeaderTest.InitHeaderTest FOR_SEND
        HeaderTest.Show , Me
    End If
End Sub
