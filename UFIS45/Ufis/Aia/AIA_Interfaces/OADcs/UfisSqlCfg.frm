VERSION 5.00
Begin VB.Form UfisSqlCfg 
   Caption         =   "SQL Configuration"
   ClientHeight    =   4170
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   9510
   LinkTopic       =   "Form1"
   ScaleHeight     =   4170
   ScaleWidth      =   9510
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton Command3 
      Caption         =   "Close"
      Height          =   255
      Left            =   3990
      TabIndex        =   14
      Top             =   3840
      Width           =   1095
   End
   Begin VB.Frame Frame2 
      Caption         =   "Departure Flights"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1455
      Left            =   120
      TabIndex        =   8
      Top             =   2280
      Width           =   9255
      Begin VB.TextBox DepSqlKey 
         Height          =   285
         Left            =   840
         TabIndex        =   11
         Text            =   "TIFD BETWEEN 'TimeFrameBgn' AND 'TimeFrameEnd' AND ORG3='ATH' AND ALC3='OAL'"
         Top             =   720
         Width           =   8175
      End
      Begin VB.CommandButton Command2 
         Caption         =   "Test"
         Height          =   255
         Left            =   840
         TabIndex        =   10
         Top             =   1080
         Width           =   1095
      End
      Begin VB.TextBox DepFldLst 
         Height          =   285
         Left            =   840
         TabIndex        =   9
         Text            =   "FLNO,ORG3,VIA3,DES3,TIFA,TIFD,PSTA,PSTD,GTD1,GTD2,REGN,LAND,OFBL"
         Top             =   360
         Width           =   8175
      End
      Begin VB.Label Label5 
         Caption         =   "WHERE"
         Height          =   255
         Left            =   120
         TabIndex        =   13
         Top             =   720
         Width           =   615
      End
      Begin VB.Label Label4 
         Caption         =   "Fieldlist"
         Height          =   255
         Left            =   120
         TabIndex        =   12
         Top             =   360
         Width           =   615
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Arrival Flights"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1455
      Left            =   120
      TabIndex        =   2
      Top             =   600
      Width           =   9255
      Begin VB.TextBox ArrFldLst 
         Height          =   285
         Left            =   840
         TabIndex        =   6
         Text            =   "FLNO,ORG3,VIA3,DES3,TIFA,TIFD,PSTA,PSTD,GTD1,GTD2,REGN,LAND,OFBL"
         Top             =   360
         Width           =   8175
      End
      Begin VB.CommandButton Command1 
         Caption         =   "Test"
         Height          =   255
         Left            =   840
         TabIndex        =   5
         Top             =   1080
         Width           =   1095
      End
      Begin VB.TextBox ArrSqlKey 
         Height          =   285
         Left            =   840
         TabIndex        =   3
         Text            =   "TIFD BETWEEN 'TimeFrameBgn' AND 'TimeFrameEnd' AND DES3='ATH' AND ALC3='OAL'"
         Top             =   720
         Width           =   8175
      End
      Begin VB.Label Label3 
         Caption         =   "Fieldlist"
         Height          =   255
         Left            =   120
         TabIndex        =   7
         Top             =   360
         Width           =   615
      End
      Begin VB.Label Label2 
         Caption         =   "WHERE"
         Height          =   255
         Left            =   120
         TabIndex        =   4
         Top             =   720
         Width           =   615
      End
   End
   Begin VB.TextBox TableName 
      Alignment       =   2  'Center
      Height          =   285
      Left            =   600
      TabIndex        =   0
      Text            =   "AFTTAB"
      Top             =   120
      Width           =   1065
   End
   Begin VB.Label Label1 
      Caption         =   "Table"
      Height          =   255
      Left            =   120
      TabIndex        =   1
      Top             =   120
      Width           =   495
   End
End
Attribute VB_Name = "UfisSqlCfg"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private Sub Command1_Click()
    ResultViewer.Show
    Open "c:\tmp\atc_arr_dat.txt" For Output As #2
    SendArrivalData
    Close #2
End Sub
Public Sub SendArrivalData()
Dim tmpCmd As String
Dim tmpSqlKey As String
Dim tmpTable As String
Dim tmpFields As String
    tmpSqlKey = "WHERE " & ArrSqlKey.Text
    tmpSqlKey = PatchTimeFrame(tmpSqlKey, -120, 360)
    tmpTable = TableName.Text
    tmpCmd = "RTA"
    tmpFields = ArrFldLst.Text
    CDI.RequestCedaData tmpCmd, tmpTable, tmpFields, tmpSqlKey, " ", True
End Sub
Private Function PatchTimeFrame(tmpSqlKey As String, MinBegin As Integer, MinEnd As Integer) As String
Dim tmpResult As String
Dim tmpTifrBgn As String
Dim tmpTifrEnd As String
    tmpResult = tmpSqlKey
    tmpTifrBgn = GetTimeStamp(MinBegin)
    tmpTifrEnd = GetTimeStamp(MinEnd)
    tmpResult = Replace(tmpResult, "TimeFrameBgn", tmpTifrBgn, 1, -1, vbBinaryCompare)
    tmpResult = Replace(tmpResult, "TimeFrameEnd", tmpTifrEnd, 1, -1, vbBinaryCompare)
    PatchTimeFrame = tmpResult
End Function
Public Function GetTimeStamp(MinuteOffset As Integer) As String
Dim tmpTime 'as variant
    tmpTime = DateAdd("n", MinuteOffset, Now)
    GetTimeStamp = Format(tmpTime, "yyyymmddhhmmss")
End Function
Private Sub Command2_Click()
    ResultViewer.Show
    Open "c:\tmp\atc_dep_dat.txt" For Output As #2
    SendDepartureData
    Close #2
End Sub
Public Sub SendDepartureData()
Dim tmpCmd As String
Dim tmpSqlKey As String
Dim tmpTable As String
Dim tmpFields As String
    tmpSqlKey = "WHERE " & DepSqlKey.Text
    tmpSqlKey = PatchTimeFrame(tmpSqlKey, -120, 360)
    tmpTable = TableName.Text
    tmpCmd = "RTA"
    tmpFields = DepFldLst.Text
    CDI.RequestCedaData tmpCmd, tmpTable, tmpFields, tmpSqlKey, " ", True
End Sub

Private Sub Command3_Click()
    UfisSqlCfg.Hide
End Sub


