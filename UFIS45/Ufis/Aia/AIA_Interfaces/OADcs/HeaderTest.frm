VERSION 5.00
Begin VB.Form HeaderTest 
   Caption         =   "Test of Message Header"
   ClientHeight    =   2040
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   7440
   LinkTopic       =   "Form1"
   ScaleHeight     =   2040
   ScaleWidth      =   7440
   StartUpPosition =   3  'Windows Default
   Begin VB.Frame Frame3 
      Caption         =   "Test Conditions"
      Height          =   1815
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   7215
      Begin VB.CommandButton FillHeader 
         Caption         =   "Fill Header"
         Height          =   255
         Left            =   1260
         TabIndex        =   8
         Top             =   1440
         Width           =   1095
      End
      Begin VB.CommandButton Command1 
         Caption         =   "Close"
         Height          =   255
         Left            =   3600
         TabIndex        =   7
         Top             =   1440
         Width           =   1095
      End
      Begin VB.TextBox ActHeader 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   161
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   240
         TabIndex        =   2
         Top             =   1080
         Width           =   6855
      End
      Begin VB.TextBox Text3 
         Appearance      =   0  'Flat
         BackColor       =   &H8000000F&
         BorderStyle     =   0  'None
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   161
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   240
         TabIndex        =   5
         Text            =   "        10        20        30        40        50        60"
         Top             =   600
         Width           =   6855
      End
      Begin VB.TextBox Text1 
         Appearance      =   0  'Flat
         BackColor       =   &H8000000F&
         BorderStyle     =   0  'None
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   161
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   240
         TabIndex        =   4
         Text            =   "1234567890123456789012345678901234567890123456789012345678901234"
         Top             =   840
         Width           =   6855
      End
      Begin VB.CommandButton BuildMsgHead 
         Caption         =   "Test"
         Height          =   255
         Left            =   2400
         TabIndex        =   3
         Top             =   1440
         Width           =   1095
      End
      Begin VB.TextBox ActCmd 
         Alignment       =   2  'Center
         Height          =   285
         Left            =   600
         TabIndex        =   1
         Top             =   240
         Width           =   495
      End
      Begin VB.Label Label7 
         Caption         =   "Type"
         Height          =   255
         Left            =   120
         TabIndex        =   6
         Top             =   240
         Width           =   495
      End
   End
End
Attribute VB_Name = "HeaderTest"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private TestType As Integer
Public Sub InitHeaderTest(HeaderType As Integer)
    TestType = HeaderType
    Select Case HeaderType
        Case FOR_SEND
            If CDI.SendMode(0).Value = True Then
                Caption = "Test of Outgoing Message Header"
            Else
                Caption = "Test of Incoming Message Header"
            End If
            FillHeader.Visible = False
            Top = CdiHeader.Top + 270 * Screen.TwipsPerPixelY
            Left = CdiHeader.Left + 100 * Screen.TwipsPerPixelX
        Case FOR_RECV
            If CDI.SendMode(0).Value = True Then
                Caption = "Test of Incoming Message Header"
            Else
                Caption = "Test of Outgoing Message Header"
            End If
            FillHeader.Visible = True
            Top = CdiHeader.Top + 100 * Screen.TwipsPerPixelY
            Left = CdiHeader.Left + 100 * Screen.TwipsPerPixelX
    End Select
End Sub
Private Sub ActCmd_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
End Sub

Private Sub BuildMsgHead_Click()
Dim MessageHeader As String
Dim tmpCmd As String
Dim FieldList As String
Dim DataList As String
Dim RetVal As Boolean
    Select Case TestType
        Case FOR_SEND
            CdiHeader.BuildMessageHeader TestType, ActCmd.Text, 3, MessageHeader
            ActHeader.Text = MessageHeader & ActCmd.Text
        Case FOR_RECV
            RetVal = CdiHeader.CheckRcvMsg(ActHeader.Text, tmpCmd, FieldList, DataList)
            'MsgBox RetVal & vbNewLine & FieldList & vbNewLine & DataList
            If RetVal = RC_SUCCESS Then
            Else
            End If
        Case Else
    End Select
End Sub

Private Sub Command1_Click()
    Unload Me
End Sub

Private Sub FillHeader_Click()
Dim MessageHeader As String
    CdiHeader.BuildMessageHeader TestType, ActCmd.Text, 14, MessageHeader
    ActHeader.Text = MessageHeader & ActCmd.Text & "#FLD1#VAL1#"
    CdiHeader.DecreaseSequence FOR_RECV, ActCmd.Text
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Select Case TestType
        Case FOR_SEND
            CdiHeader.TestSend.Value = 0
        Case FOR_RECV
            CdiHeader.TestRecv.Value = 0
        Case Else
    End Select
End Sub
