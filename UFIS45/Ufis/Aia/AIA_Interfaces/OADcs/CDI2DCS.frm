VERSION 5.00
Object = "{7DA325C2-D4A4-11D3-9B36-020128B0A0FF}#1.0#0"; "UfisBroadcasts.ocx"
Object = "{A2F31E92-C74F-11D3-A251-00500437F607}#1.0#0"; "ufiscom.ocx"
Object = "{248DD890-BB45-11CF-9ABC-0080C7E7B78D}#1.0#0"; "MSWINSCK.OCX"
Object = "{64E8E382-05E2-11D2-9B1D-9B0630BC8F12}#1.0#0"; "tab.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomctl.ocx"
Begin VB.Form CDI 
   Caption         =   "UFIS To OA DCS"
   ClientHeight    =   8040
   ClientLeft      =   2640
   ClientTop       =   1440
   ClientWidth     =   10260
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   FillStyle       =   0  'Solid
   Icon            =   "CDI2DCS.frx":0000
   LinkTopic       =   "Form1"
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   8040
   ScaleWidth      =   10260
   Begin TABLib.TAB ErrList 
      Height          =   945
      Left            =   7230
      TabIndex        =   61
      Top             =   1980
      Width           =   2985
      _Version        =   65536
      _ExtentX        =   5265
      _ExtentY        =   1667
      _StockProps     =   0
   End
   Begin MSComctlLib.StatusBar myStatusBar 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   53
      Top             =   7755
      Width           =   10260
      _ExtentX        =   18098
      _ExtentY        =   503
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   3
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   2222
            MinWidth        =   2222
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   2928
            MinWidth        =   2928
         EndProperty
         BeginProperty Panel3 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
         EndProperty
      EndProperty
   End
   Begin VB.TextBox ConnectStatus 
      Alignment       =   2  'Center
      BackColor       =   &H00008000&
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   315
      Left            =   6030
      TabIndex        =   52
      Text            =   "Connecting"
      Top             =   630
      Visible         =   0   'False
      Width           =   1155
   End
   Begin VB.TextBox HangUp 
      Alignment       =   2  'Center
      BackColor       =   &H000000FF&
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   315
      Left            =   6030
      TabIndex        =   51
      Text            =   "Hanging Up"
      Top             =   630
      Visible         =   0   'False
      Width           =   1155
   End
   Begin TABLib.TAB tabDataList 
      Height          =   1155
      Index           =   0
      Left            =   3360
      TabIndex        =   39
      Top             =   2190
      Width           =   2355
      _Version        =   65536
      _ExtentX        =   4154
      _ExtentY        =   2037
      _StockProps     =   0
   End
   Begin VB.TextBox DataListFrame 
      Height          =   1395
      Index           =   0
      Left            =   30
      TabIndex        =   40
      Top             =   1950
      Width           =   3195
   End
   Begin VB.Frame Frame4 
      Caption         =   "Don't touch"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1815
      Left            =   8880
      TabIndex        =   33
      Top             =   30
      Width           =   1335
      Begin VB.CommandButton btnCustomer 
         Cancel          =   -1  'True
         Caption         =   "Customer"
         Height          =   255
         Left            =   120
         TabIndex        =   63
         Top             =   450
         Width           =   1095
      End
      Begin VB.CommandButton ResetCmd 
         Caption         =   "Reset"
         Height          =   255
         Left            =   120
         TabIndex        =   62
         Top             =   780
         Width           =   1095
      End
      Begin VB.Frame Frame6 
         BorderStyle     =   0  'None
         Caption         =   "Frame6"
         Height          =   315
         Left            =   120
         TabIndex        =   57
         Top             =   1110
         Width           =   1125
         Begin VB.OptionButton SendMode 
            Caption         =   "UFIS"
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   0
            Left            =   0
            Style           =   1  'Graphical
            TabIndex        =   59
            Top             =   0
            Value           =   -1  'True
            Width           =   525
         End
         Begin VB.OptionButton SendMode 
            Caption         =   "DCS"
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   1
            Left            =   570
            Style           =   1  'Graphical
            TabIndex        =   58
            Top             =   0
            Width           =   525
         End
      End
      Begin VB.CommandButton ExitCmd 
         Caption         =   "E&xit"
         Height          =   255
         Left            =   120
         TabIndex        =   34
         Top             =   1440
         Width           =   1095
      End
   End
   Begin VB.Frame Frame2 
      Caption         =   "System Check"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1815
      Left            =   1650
      TabIndex        =   14
      Top             =   30
      Width           =   1575
      Begin VB.CommandButton Command3 
         Caption         =   "Test / Log's"
         Height          =   255
         Left            =   120
         TabIndex        =   19
         Top             =   1440
         Width           =   1095
      End
      Begin VB.CommandButton Command1 
         Caption         =   "SPL Config"
         Height          =   255
         Left            =   120
         TabIndex        =   28
         Top             =   1200
         Width           =   1095
      End
      Begin VB.CommandButton Command6 
         Caption         =   "SQL Config"
         Height          =   255
         Left            =   120
         TabIndex        =   16
         Top             =   960
         Width           =   1095
      End
      Begin VB.CommandButton Command7 
         Caption         =   "FLD Config"
         Height          =   255
         Left            =   120
         TabIndex        =   17
         Top             =   720
         Width           =   1095
      End
      Begin VB.CommandButton Command8 
         Caption         =   "CCO Config"
         Height          =   255
         Left            =   120
         TabIndex        =   18
         Top             =   480
         Width           =   1095
      End
      Begin VB.CommandButton Command5 
         Caption         =   "CDI Config"
         Height          =   255
         Left            =   120
         TabIndex        =   15
         Top             =   240
         Width           =   1095
      End
      Begin VB.Shape shCcoCfg 
         BackStyle       =   1  'Opaque
         FillColor       =   &H0000FF00&
         FillStyle       =   0  'Solid
         Height          =   135
         Left            =   1320
         Shape           =   3  'Circle
         Top             =   540
         Width           =   135
      End
      Begin VB.Shape shCdiCfg 
         BackStyle       =   1  'Opaque
         FillColor       =   &H0000FF00&
         FillStyle       =   0  'Solid
         Height          =   135
         Left            =   1320
         Shape           =   3  'Circle
         Top             =   300
         Width           =   135
      End
      Begin VB.Shape Shape9 
         BackStyle       =   1  'Opaque
         FillColor       =   &H0000FF00&
         FillStyle       =   0  'Solid
         Height          =   135
         Left            =   1320
         Shape           =   3  'Circle
         Top             =   1500
         Width           =   135
      End
      Begin VB.Shape Shape8 
         BackStyle       =   1  'Opaque
         FillColor       =   &H0000FF00&
         FillStyle       =   0  'Solid
         Height          =   135
         Left            =   1320
         Shape           =   3  'Circle
         Top             =   1260
         Width           =   135
      End
      Begin VB.Shape Shape7 
         BackStyle       =   1  'Opaque
         FillColor       =   &H0000FF00&
         FillStyle       =   0  'Solid
         Height          =   135
         Left            =   1320
         Shape           =   3  'Circle
         Top             =   1020
         Width           =   135
      End
      Begin VB.Shape Shape6 
         BackStyle       =   1  'Opaque
         FillColor       =   &H0000FF00&
         FillStyle       =   0  'Solid
         Height          =   135
         Left            =   1320
         Shape           =   3  'Circle
         Top             =   780
         Width           =   135
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "System Status"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1815
      Left            =   30
      TabIndex        =   11
      Top             =   30
      Width           =   1515
      Begin VB.Timer ExtChkTimer 
         Enabled         =   0   'False
         Interval        =   30000
         Left            =   1020
         Top             =   1290
      End
      Begin VB.Timer CedaChkTimer 
         Enabled         =   0   'False
         Interval        =   30000
         Left            =   1020
         Top             =   240
      End
      Begin VB.Timer CedaConTimer 
         Enabled         =   0   'False
         Interval        =   10000
         Left            =   60
         Top             =   210
      End
      Begin VB.Timer ExtCcoTimer 
         Enabled         =   0   'False
         Interval        =   60000
         Left            =   60
         Top             =   1290
      End
      Begin VB.Shape shSvrFlowLed 
         BackStyle       =   1  'Opaque
         FillColor       =   &H000080FF&
         FillStyle       =   0  'Solid
         Height          =   135
         Index           =   3
         Left            =   810
         Shape           =   3  'Circle
         Top             =   810
         Width           =   135
      End
      Begin VB.Shape shSvrFlowLed 
         BackStyle       =   1  'Opaque
         FillColor       =   &H000080FF&
         FillStyle       =   0  'Solid
         Height          =   135
         Index           =   2
         Left            =   810
         Shape           =   3  'Circle
         Top             =   570
         Width           =   135
      End
      Begin VB.Shape shSvrFlowLed 
         BackStyle       =   1  'Opaque
         FillColor       =   &H000080FF&
         FillStyle       =   0  'Solid
         Height          =   135
         Index           =   1
         Left            =   570
         Shape           =   3  'Circle
         Top             =   570
         Width           =   135
      End
      Begin VB.Shape shSvrFlowLed 
         BackStyle       =   1  'Opaque
         FillColor       =   &H000080FF&
         FillStyle       =   0  'Solid
         Height          =   135
         Index           =   0
         Left            =   570
         Shape           =   3  'Circle
         Top             =   810
         Width           =   135
      End
      Begin VB.Shape shExtFlowLed 
         BackStyle       =   1  'Opaque
         FillColor       =   &H000080FF&
         FillStyle       =   0  'Solid
         Height          =   135
         Index           =   3
         Left            =   570
         Shape           =   3  'Circle
         Top             =   990
         Width           =   135
      End
      Begin VB.Shape shExtFlowLed 
         BackStyle       =   1  'Opaque
         FillColor       =   &H000080FF&
         FillStyle       =   0  'Solid
         Height          =   135
         Index           =   2
         Left            =   570
         Shape           =   3  'Circle
         Top             =   1230
         Width           =   135
      End
      Begin VB.Shape shSvrSpool 
         BackStyle       =   1  'Opaque
         FillColor       =   &H0000FF00&
         FillStyle       =   0  'Solid
         Height          =   315
         Left            =   1080
         Shape           =   3  'Circle
         Top             =   810
         Visible         =   0   'False
         Width           =   315
      End
      Begin VB.Shape shExtSpool 
         BackStyle       =   1  'Opaque
         FillColor       =   &H0000FF00&
         FillStyle       =   0  'Solid
         Height          =   315
         Left            =   120
         Shape           =   3  'Circle
         Top             =   810
         Visible         =   0   'False
         Width           =   315
      End
      Begin VB.Shape shExtFlowLed 
         BackStyle       =   1  'Opaque
         FillColor       =   &H000080FF&
         FillStyle       =   0  'Solid
         Height          =   135
         Index           =   1
         Left            =   810
         Shape           =   3  'Circle
         Top             =   1230
         Width           =   135
      End
      Begin VB.Shape shExtFlowLed 
         BackStyle       =   1  'Opaque
         FillColor       =   &H000080FF&
         FillStyle       =   0  'Solid
         Height          =   135
         Index           =   0
         Left            =   810
         Shape           =   3  'Circle
         Top             =   990
         Width           =   135
      End
      Begin VB.Shape shCdiLed 
         BackStyle       =   1  'Opaque
         FillColor       =   &H0000FF00&
         FillStyle       =   0  'Solid
         Height          =   255
         Index           =   1
         Left            =   750
         Shape           =   3  'Circle
         Top             =   840
         Width           =   255
      End
      Begin VB.Shape shCdiLed 
         BackStyle       =   1  'Opaque
         FillColor       =   &H0000FF00&
         FillStyle       =   0  'Solid
         Height          =   255
         Index           =   0
         Left            =   510
         Shape           =   3  'Circle
         Top             =   840
         Width           =   255
      End
      Begin VB.Label Label1 
         BackStyle       =   0  'Transparent
         Caption         =   " UFIS"
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   161
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   135
         Left            =   570
         TabIndex        =   13
         Top             =   360
         Width           =   375
      End
      Begin VB.Label Label2 
         BackStyle       =   0  'Transparent
         Caption         =   " DCS"
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   165
         Left            =   530
         TabIndex        =   12
         Top             =   1410
         Width           =   435
      End
      Begin VB.Shape shSvrConLed 
         BackStyle       =   1  'Opaque
         FillColor       =   &H00008000&
         FillStyle       =   0  'Solid
         Height          =   345
         Index           =   0
         Left            =   570
         Top             =   630
         Width           =   135
      End
      Begin VB.Shape shSvrConLed 
         BackStyle       =   1  'Opaque
         FillColor       =   &H00008000&
         FillStyle       =   0  'Solid
         Height          =   375
         Index           =   1
         Left            =   810
         Top             =   600
         Width           =   135
      End
      Begin VB.Shape shExtConLed 
         BackStyle       =   1  'Opaque
         FillColor       =   &H00008000&
         FillStyle       =   0  'Solid
         Height          =   315
         Index           =   1
         Left            =   570
         Top             =   960
         Width           =   135
      End
      Begin VB.Shape shExtConLed 
         BackStyle       =   1  'Opaque
         FillColor       =   &H00008000&
         FillStyle       =   0  'Solid
         Height          =   315
         Index           =   0
         Left            =   810
         Top             =   960
         Width           =   135
      End
      Begin VB.Shape shExtConLed 
         BackStyle       =   1  'Opaque
         FillColor       =   &H0000FF00&
         FillStyle       =   0  'Solid
         Height          =   135
         Index           =   2
         Left            =   300
         Top             =   960
         Visible         =   0   'False
         Width           =   285
      End
      Begin VB.Shape shSvrConLed 
         BackStyle       =   1  'Opaque
         FillColor       =   &H00008000&
         FillStyle       =   0  'Solid
         Height          =   135
         Index           =   2
         Left            =   870
         Top             =   840
         Visible         =   0   'False
         Width           =   345
      End
      Begin VB.Shape shServerLed 
         BackStyle       =   1  'Opaque
         FillColor       =   &H000080FF&
         FillStyle       =   0  'Solid
         Height          =   435
         Left            =   540
         Shape           =   4  'Rounded Rectangle
         Top             =   240
         Width           =   435
      End
      Begin VB.Shape shExtSysLed 
         BackStyle       =   1  'Opaque
         FillColor       =   &H000080FF&
         FillStyle       =   0  'Solid
         Height          =   435
         Left            =   540
         Shape           =   4  'Rounded Rectangle
         Top             =   1260
         Width           =   435
      End
      Begin VB.Shape shSvrConLed 
         BackStyle       =   1  'Opaque
         FillColor       =   &H0000FF00&
         FillStyle       =   0  'Solid
         Height          =   135
         Index           =   3
         Left            =   870
         Top             =   960
         Visible         =   0   'False
         Width           =   345
      End
      Begin VB.Shape shExtConLed 
         BackStyle       =   1  'Opaque
         FillColor       =   &H00008000&
         FillStyle       =   0  'Solid
         Height          =   135
         Index           =   3
         Left            =   300
         Top             =   840
         Visible         =   0   'False
         Width           =   285
      End
   End
   Begin VB.Frame ConnectionUfis 
      Caption         =   "UFIS AODB"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1815
      Left            =   3330
      TabIndex        =   1
      Top             =   30
      Width           =   2535
      Begin VB.TextBox Text3 
         Alignment       =   1  'Right Justify
         Enabled         =   0   'False
         Height          =   285
         Left            =   2040
         TabIndex        =   21
         ToolTipText     =   "Last received sequence nbr."
         Top             =   600
         Width           =   375
      End
      Begin VB.ComboBox BcConnex 
         Enabled         =   0   'False
         Height          =   315
         ItemData        =   "CDI2DCS.frx":0442
         Left            =   1320
         List            =   "CDI2DCS.frx":0449
         TabIndex        =   20
         Text            =   "UfisBc32"
         Top             =   240
         Width           =   1095
      End
      Begin VB.CommandButton btnDisConCeda 
         Caption         =   "Disconnect"
         Enabled         =   0   'False
         Height          =   255
         Left            =   120
         TabIndex        =   10
         Top             =   1440
         Width           =   1095
      End
      Begin VB.CommandButton btnDisConBC 
         Caption         =   "Disconnect"
         Enabled         =   0   'False
         Height          =   255
         Left            =   1320
         TabIndex        =   9
         Top             =   1440
         Width           =   1095
      End
      Begin VB.CommandButton btnConnectBC 
         Caption         =   "Connect"
         Height          =   255
         Left            =   1320
         TabIndex        =   8
         Top             =   1200
         Width           =   1095
      End
      Begin VB.TextBox txtBcNum 
         Alignment       =   1  'Right Justify
         Height          =   285
         Left            =   1320
         TabIndex        =   7
         ToolTipText     =   "Last received sequence nbr."
         Top             =   600
         Width           =   705
      End
      Begin VB.CommandButton btnConnectCeda 
         Caption         =   "Connect"
         Height          =   255
         Left            =   120
         TabIndex        =   6
         Top             =   1200
         Width           =   1095
      End
      Begin VB.TextBox txtTableExt 
         Alignment       =   2  'Center
         Height          =   285
         Left            =   720
         TabIndex        =   5
         Text            =   "TAB"
         Top             =   600
         Width           =   495
      End
      Begin VB.TextBox txtHopo 
         Alignment       =   2  'Center
         Height          =   285
         Left            =   120
         TabIndex        =   4
         Text            =   "ATH"
         Top             =   600
         Width           =   495
      End
      Begin VB.ComboBox cbServer 
         Height          =   315
         ItemData        =   "CDI2DCS.frx":0457
         Left            =   120
         List            =   "CDI2DCS.frx":0461
         TabIndex        =   2
         Text            =   "ufislh"
         Top             =   240
         Width           =   1095
      End
      Begin UFISCOMLib.UfisCom aUfis 
         Left            =   0
         Top             =   1560
         _Version        =   65536
         _ExtentX        =   1085
         _ExtentY        =   661
         _StockProps     =   0
      End
      Begin UFISBROADCASTSLib.UfisBroadcasts bcComm 
         Left            =   1800
         Top             =   1560
         _Version        =   65536
         _ExtentX        =   1296
         _ExtentY        =   661
         _StockProps     =   0
      End
      Begin VB.ComboBox cbConnectType 
         Height          =   315
         ItemData        =   "CDI2DCS.frx":0475
         Left            =   120
         List            =   "CDI2DCS.frx":0477
         TabIndex        =   3
         Text            =   "CEDA"
         Top             =   240
         Width           =   1095
      End
   End
   Begin VB.Frame TcpConnectionFrame 
      Caption         =   "Connect as"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1815
      Left            =   7230
      TabIndex        =   0
      Top             =   30
      Width           =   1575
      Begin VB.Frame Frame5 
         BorderStyle     =   0  'None
         Caption         =   "Frame5"
         Height          =   465
         Left            =   120
         TabIndex        =   54
         Top             =   240
         Width           =   945
         Begin VB.OptionButton MakeCallOpt 
            Caption         =   "Caller"
            Height          =   255
            Left            =   0
            TabIndex        =   56
            Top             =   0
            Width           =   735
         End
         Begin VB.OptionButton WaitCallOpt 
            Caption         =   "Listener"
            Height          =   255
            Left            =   0
            TabIndex        =   55
            Top             =   240
            Value           =   -1  'True
            Width           =   855
         End
      End
      Begin MSWinsockLib.Winsock TcpConnect 
         Index           =   0
         Left            =   1110
         Top             =   1410
         _ExtentX        =   741
         _ExtentY        =   741
         _Version        =   393216
      End
      Begin VB.TextBox txtRemoteHost 
         Height          =   285
         Left            =   120
         TabIndex        =   30
         Top             =   1110
         Width           =   1335
      End
      Begin VB.TextBox txtRemotePort 
         Height          =   285
         Left            =   120
         TabIndex        =   29
         Text            =   "4398"
         Top             =   1440
         Width           =   1335
      End
   End
   Begin VB.Frame Frame3 
      Caption         =   "External"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1815
      Left            =   5940
      TabIndex        =   22
      Top             =   30
      Width           =   1335
      Begin VB.TextBox ExtRcvCounter 
         Alignment       =   1  'Right Justify
         Height          =   285
         Left            =   120
         TabIndex        =   27
         ToolTipText     =   "Last received sequence nbr."
         Top             =   600
         Width           =   615
      End
      Begin VB.ComboBox ExtConnexType 
         Height          =   315
         ItemData        =   "CDI2DCS.frx":0479
         Left            =   90
         List            =   "CDI2DCS.frx":0483
         TabIndex        =   26
         Text            =   "TCP/IP"
         Top             =   240
         Width           =   1155
      End
      Begin VB.TextBox Text4 
         Alignment       =   1  'Right Justify
         Enabled         =   0   'False
         Height          =   285
         Left            =   840
         TabIndex        =   25
         ToolTipText     =   "Last received sequence nbr."
         Top             =   600
         Width           =   375
      End
      Begin VB.CommandButton btnDisConExt 
         Caption         =   "Disconnect"
         Enabled         =   0   'False
         Height          =   255
         Left            =   120
         TabIndex        =   24
         Top             =   1440
         Width           =   1095
      End
      Begin VB.CommandButton btnConnectExt 
         Caption         =   "Connect"
         Height          =   255
         Left            =   120
         TabIndex        =   23
         Top             =   1200
         Width           =   1095
      End
   End
   Begin TABLib.TAB tabDataList 
      Height          =   1155
      Index           =   1
      Left            =   3360
      TabIndex        =   41
      Top             =   3630
      Width           =   2355
      _Version        =   65536
      _ExtentX        =   4154
      _ExtentY        =   2037
      _StockProps     =   0
   End
   Begin VB.TextBox DataListFrame 
      Height          =   1395
      Index           =   1
      Left            =   30
      TabIndex        =   42
      Top             =   3390
      Width           =   3195
   End
   Begin TABLib.TAB tabDataList 
      Height          =   1125
      Index           =   2
      Left            =   3360
      TabIndex        =   43
      Top             =   5070
      Width           =   2355
      _Version        =   65536
      _ExtentX        =   4154
      _ExtentY        =   1984
      _StockProps     =   0
   End
   Begin VB.TextBox DataListFrame 
      Height          =   1395
      Index           =   2
      Left            =   30
      TabIndex        =   44
      Top             =   4830
      Width           =   3195
   End
   Begin TABLib.TAB tabDataList 
      Height          =   1125
      Index           =   3
      Left            =   3360
      TabIndex        =   45
      Top             =   6480
      Width           =   2355
      _Version        =   65536
      _ExtentX        =   4154
      _ExtentY        =   1984
      _StockProps     =   0
   End
   Begin VB.TextBox DataListFrame 
      Height          =   1395
      Index           =   3
      Left            =   30
      TabIndex        =   46
      Top             =   6270
      Width           =   3195
   End
   Begin VB.Frame Splitter 
      BackColor       =   &H000000FF&
      BorderStyle     =   0  'None
      Height          =   225
      Index           =   0
      Left            =   3360
      TabIndex        =   47
      Top             =   1950
      Visible         =   0   'False
      Width           =   2355
   End
   Begin VB.Frame Splitter 
      BackColor       =   &H00C0C0C0&
      BorderStyle     =   0  'None
      Height          =   225
      Index           =   2
      Left            =   3360
      MousePointer    =   7  'Size N S
      TabIndex        =   49
      Top             =   4830
      Width           =   2355
   End
   Begin VB.Frame Splitter 
      BackColor       =   &H00C0C0C0&
      BorderStyle     =   0  'None
      Height          =   225
      Index           =   3
      Left            =   3360
      MousePointer    =   7  'Size N S
      TabIndex        =   50
      Top             =   6240
      Width           =   2355
   End
   Begin VB.Frame Splitter 
      BackColor       =   &H00C0C0C0&
      BorderStyle     =   0  'None
      Height          =   225
      Index           =   1
      Left            =   3360
      MousePointer    =   7  'Size N S
      TabIndex        =   48
      Top             =   3390
      Width           =   2355
   End
   Begin VB.TextBox txtDummy 
      Height          =   315
      Left            =   9120
      TabIndex        =   60
      Text            =   "Text1"
      Top             =   570
      Width           =   585
   End
   Begin VB.Frame LapbConnectionFrame 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1815
      Left            =   7230
      TabIndex        =   31
      Top             =   30
      Visible         =   0   'False
      Width           =   1575
      Begin VB.TextBox ExtPollTakt 
         Alignment       =   2  'Center
         Height          =   285
         Left            =   1020
         TabIndex        =   38
         Text            =   "25"
         Top             =   1050
         Width           =   405
      End
      Begin VB.TextBox ExtCcoTakt 
         Alignment       =   2  'Center
         Enabled         =   0   'False
         Height          =   285
         Left            =   570
         TabIndex        =   37
         Text            =   "60"
         Top             =   1380
         Width           =   405
      End
      Begin VB.TextBox ExtCcoCheck 
         Alignment       =   2  'Center
         Enabled         =   0   'False
         Height          =   285
         Left            =   1020
         TabIndex        =   36
         Text            =   "180"
         Top             =   1380
         Width           =   405
      End
      Begin VB.TextBox CmdRcvSet 
         Alignment       =   2  'Center
         Enabled         =   0   'False
         Height          =   285
         Left            =   570
         TabIndex        =   35
         Text            =   "0"
         Top             =   1050
         Width           =   405
      End
      Begin VB.Timer LapbPollTimer 
         Enabled         =   0   'False
         Interval        =   1000
         Left            =   120
         Top             =   570
      End
      Begin VB.TextBox Text2 
         Enabled         =   0   'False
         Height          =   285
         Left            =   90
         TabIndex        =   32
         Top             =   240
         Visible         =   0   'False
         Width           =   1335
      End
   End
End
Attribute VB_Name = "CDI"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
'Dim X25 As New X25HDLCLib.Hdlc

'Flag to indicate when a listen is pending
Dim ListenPending As Boolean
'Flag to indicate when a recv is pending
Dim RecvPending As Boolean
'Flag to indicate a stopped connection
Dim DisConnectSubmitted As Boolean

Const RECV_BUFFER_SIZE = 255
Const CARRIAGE_RETURN = vbCr
Const LINEFEED = vbCrLf
Const BACKSPACE = vbBack

Const ConnectCmd = 1
Const SendCmd = 2
Const ReceiveCmd = 3
Const DisconnectCmd = 4

Const LAPB = 0
Const TCPIP = 1

Public bCedaIsConnected As Boolean
Public bBcIsConnected As Boolean
Public isread As Boolean
Public ColCount As Integer
Public i As Integer
Public j As Integer
Public k As Integer
Public CdiType As String
Public extCommConnex As Integer
Public ConnexType As Integer

Private Sub InitListFields()
    Dim i As Integer
    For i = 0 To 3
        tabDataList(i).Left = DataListFrame(i).Left + 1 * Screen.TwipsPerPixelX
        tabDataList(i).Top = DataListFrame(i).Top + 1 * Screen.TwipsPerPixelY
        tabDataList(i).ResetContent
        tabDataList(i).FontName = "Courier"
        tabDataList(i).HeaderFontSize = 14
        tabDataList(i).FontSize = 14
        tabDataList(i).LineHeight = 15
        tabDataList(i).Height = 6 * 15 * Screen.TwipsPerPixelY
        tabDataList(i).HeaderLengthString = "38,1000,80,40"
        DataListFrame(i).Height = tabDataList(i).Height + 2 * Screen.TwipsPerPixelY
        Splitter(i).Left = DataListFrame(i).Left
        Splitter(i).Top = DataListFrame(i).Top - 6 * Screen.TwipsPerPixelY
        Splitter(i).Tag = -1
    Next
    tabDataList(0).HeaderString = "Count,Received from OA DCS,URNO,Index"
    tabDataList(1).HeaderString = "Count,Sent to UFIS AODB,URNO,Index"
    tabDataList(2).HeaderString = "Count,Received from UFIS AODB,URNO,Index"
    tabDataList(3).HeaderString = "Count,Sent to OA DCS,URNO,Index"
    ErrList.Top = Frame4.Top + 6 * Screen.TwipsPerPixelY
    ErrList.Height = Frame4.Height - 6 * Screen.TwipsPerPixelY
    ErrList.Left = Frame4.Left + Frame4.Width + 6 * Screen.TwipsPerPixelX
    ErrList.ResetContent
    ErrList.FontName = "Courier"
    ErrList.HeaderFontSize = 14
    ErrList.FontSize = 14
    ErrList.LineHeight = 15
    ErrList.HeaderLengthString = "38,150,200,40"
    ErrList.HeaderString = "Line,ErrMsg,Remark,Idx"
End Sub

Public Sub ResetListFields()
Dim i As Integer
    For i = 0 To 3
        tabDataList(i).ResetContent
    Next
    ErrList.ResetContent
    CdiHeader.CdiHeaderReset -1, "", "SND"
    CdiHeader.CdiHeaderReset -1, "", "RCV"
End Sub

Private Sub bcComm_SendBroadcast(ByVal BcNum As String, ByVal DestName As String, ByVal RecvName As String, ByVal Command As String, ByVal ObjName As String, ByVal Selection As String, ByVal Fields As String, ByVal Data As String)
    Dim bc_str As String
    Dim tmpRaco As String
    Dim tmpMsgData As String
    If btnDisConBC.Enabled = True Then
        shSvrFlowLed(2).FillColor = vbYellow
        shSvrFlowLed(3).FillColor = vbYellow
        bc_str = Command & "|" & ObjName & "|" & Fields & "|" & Data
        bc_str = BcNum & "," & Replace(bc_str, ",", ";", 1, -1, vbBinaryCompare)
        txtBcNum.Text = BcNum
        shServerLed.FillColor = vbGreen
        shSvrConLed(1).FillColor = vbGreen
        Select Case ObjName
            Case "AFTTAB"
                tabDataList(2).InsertTextLineAt 0, bc_str, True
                'tmpRaco = GetFieldValue("RACO", Data, Fields)
                'If tmpRaco <> "" Then
                    CDI.BuildMessageData Fields, Data, tmpMsgData
                    tmpMsgData = Command & "|" & tmpMsgData
                    CDI.SendMessageData FOR_SEND, "UFD", tmpMsgData
                'End If
            Case "CCOCHK"
                tabDataList(2).InsertTextLineAt 0, bc_str, True
                shSvrConLed(0).FillColor = vbGreen
                CedaChkTimer.Enabled = False
        End Select
    End If
    shSvrFlowLed(2).FillColor = vbGreen
    shSvrFlowLed(3).FillColor = vbGreen
End Sub

Private Sub btnConnectBC_Click()
    shSvrConLed(1).FillColor = vbYellow
    If bBcIsConnected <> True Then
        bcComm.InitComm
    End If
    txtBcNum.Text = "0"
    bBcIsConnected = True
    btnDisConBC.Enabled = True
    btnConnectBC.Enabled = False
    SendCcoToCeda
End Sub

Private Sub btnConnectCeda_Click()
    Dim sServer As String
    Dim sHopo As String
    Dim sTableExt As String
    Dim sConnectType As String
    Dim ret As Integer
    MousePointer = 11
    isread = False
    shExtConLed(2).Visible = False
    shExtSpool.Visible = False
    shSvrSpool.Visible = False
    shSvrConLed(3).Visible = False
    sServer = cbServer.Text
    If sServer <> "LOCAL" Then
        sHopo = txtHopo.Text
        sTableExt = txtTableExt.Text
        sConnectType = cbConnectType.Text
        aUfis.CleanupCom
        ret = aUfis.SetCedaPerameters("CDI2ATC", sHopo, sTableExt)
        ret = aUfis.InitCom(sServer, sConnectType)
        If ret = 0 Then 'Error: not connected
            bCedaIsConnected = False
            shSvrConLed(0).FillColor = vbRed
            shServerLed.FillColor = vbRed
        Else
            bCedaIsConnected = True
            btnConnectCeda.Enabled = False
            btnDisConCeda.Enabled = True
            shSvrConLed(0).FillColor = vbGreen
            shServerLed.FillColor = vbGreen
            CedaConTimer.Enabled = True
        End If
    Else
        shExtSpool.Visible = True
        shExtConLed(2).Visible = True
        shSvrSpool.Visible = True
        shSvrConLed(3).Visible = True
        btnConnectCeda.Enabled = False
        btnDisConCeda.Enabled = True
    End If
    CdiHeader.CdiHeaderReset -1, "", "SND"
    CdiHeader.CdiHeaderReset -1, "", "RCV"
    MousePointer = 0
End Sub

Private Sub btnConnectExt_Click()
    Dim info As Long
    Dim ret As Long
    Dim length As Long
    Dim str As String
    Dim bl As Boolean
    Dim Data As String
    Dim cmd As Integer
    Dim tmpdat As String
    ConnectStatus.Visible = True
    shExtFlowLed(0).FillColor = vbYellow
    shExtFlowLed(3).FillColor = vbYellow
    If InStr(ExtConnexType.Text, "LAPB") > 0 Then
        MsgBox "There is no X25 facility"
        ConnectStatus.Visible = False
    ElseIf InStr(ExtConnexType.Text, "TCP/IP") > 0 Then
        ConnexType = TCPIP
        If WaitCallOpt.Value = True Then
            CdiType = "Listener"
            TcpConnect(0).LocalPort = txtRemotePort.Text
            TcpConnect(0).Listen
            extCommConnex = 0
            btnDisConExt.Enabled = True
            btnConnectExt.Enabled = False
            ConnectStatus.Text = "Listening"
            'tmpDat = DisplayMessage(3, "-------------(ready)-----------------", "SYS")
        End If
        If MakeCallOpt.Value = True Then
            CdiType = "Caller"
            TcpConnect(0).RemoteHost = txtRemoteHost.Text
            TcpConnect(0).RemotePort = txtRemotePort.Text
            TcpConnect(0).Connect txtRemoteHost.Text, txtRemotePort.Text
            ExtCcoTimer.Enabled = True
            btnDisConExt.Enabled = True
            btnConnectExt.Enabled = False
            ConnectStatus.Text = "Calling"
        End If
    End If
End Sub
Public Sub ResetDataLists(ListType As Integer)
    If ListType <= 1 Then
        tabDataList(0).ResetContent
        tabDataList(3).ResetContent
    End If
    If ListType >= 1 Then
        tabDataList(1).ResetContent
        tabDataList(2).ResetContent
    End If
End Sub

Private Sub FinishLapbConnex(Index As Integer)
        ExtCcoTimer.Enabled = True
        btnDisConExt.Enabled = True
        shExtFlowLed(0).FillColor = vbGreen
        shExtFlowLed(3).FillColor = vbGreen
        SetRcvCommands 1
        ConnectStatus.Visible = False
End Sub

Private Sub btnCustomer_Click()
    Customer.Show , Me
End Sub

Private Sub btnDisConBC_Click()
    btnDisConBC.Enabled = False
    btnConnectBC.Enabled = True
    tabDataList(2).InsertTextLineAt 0, ",Connection closed", True
    shSvrConLed(1).FillColor = vbMagenta
End Sub

Private Sub btnDisConCeda_Click()
    If cbServer.Text <> "LOCAL" Then
        Close #1
        aUfis.CleanupCom
        shSvrConLed(0).FillColor = vbMagenta
        CedaConTimer.Enabled = False
    Else
        shExtConLed(2).Visible = False
        shExtSpool.Visible = False
        shSvrSpool.Visible = False
        shSvrConLed(3).Visible = False
    End If
    btnDisConCeda.Enabled = False
    btnConnectCeda.Enabled = True
End Sub

Private Sub btnDisConExt_Click()
    Dim tmpdat As String
    ConnectStatus.Visible = False
    HangUp.Visible = True
    tmpdat = DisplayMessage(0, "--------------(hanging up)------------------", "SYS")
    If InStr(ExtConnexType.Text, "LAPB") > 0 Then
        'SubmitCloseLapbConnex 0
    ElseIf InStr(ExtConnexType.Text, "TCP/IP") > 0 Then
        CloseTcpConnex 0
    End If
End Sub

Public Sub RequestCedaData(spCmd As String, spTable As String, spFields As String, spSqlKey As String, spData As String, bpTest As Boolean)
    Dim CedaCmd As String
    Dim table As String
    Dim FieldList As String
    Dim CedaData As String
    Dim where As String
    Dim Line As String
    Dim retStr As String
    Dim ret As Integer
    Dim count As Integer
    Dim ItemCount As Integer
    Dim strArr() As String
    Dim tmpData As String
    Dim tmpMsgData As String
    count = 0
    
    'MsgBox "CMD:" & spCmd
    'MsgBox "TBL:" & spTable
    'MsgBox "FLD:" & spFields
    'MsgBox "TBL:" & spSqlKey
    
    table = spTable
    FieldList = spFields
    where = spSqlKey
    CedaCmd = spCmd
    CedaData = spData
    If isread = False Then
        retStr = aUfis.Ufis(CedaCmd, table, FieldList, where, CedaData)
    End If
    count = aUfis.GetBufferCount
    
    If ResultViewer.ResultData.Visible = True Then
        ResultViewer.ResultData.Clear
        ResultViewer.TotalLines.Caption = count
    End If
    count = count + 1
    For i = 0 To count
        Line = aUfis.GetBufferLine(i)
        If (Trim(Line) <> "") Then
            tmpData = Line
            If ResultViewer.ResultData.Visible = True Then
                ResultViewer.ResultData.AddItem Line
                Print #2, Line
            End If
            BuildMessageData FieldList, tmpData, tmpMsgData
            tmpMsgData = "UFD|" & tmpMsgData
            SendMessageData FOR_SEND, "UFD", tmpMsgData
        End If
    Next i
End Sub
Public Sub BuildMessageData(tmpFields As String, tmpData As String, tmpMsgData As String)
    tmpMsgData = tmpFields & "|" & tmpData
End Sub

Public Sub SendMessageData(ChkCmdIdx As Integer, tmpCmd As String, tmpMsgData As String)
    Dim MessageBuffer() As String
    Dim MessageHeader As String
    Dim MsgText As String
    Dim outText As String
    Dim sndText As String
    Dim firstMsg As Integer, lastMsg As Integer, msgIdx As Integer, MsgLen As Integer
    Dim totLen As Long
    Dim tmpLen As String
    Dim CmdNbr As Integer
    Dim tmpdat As String
    MessageBuffer = Split(tmpMsgData, vbLf)
    firstMsg = LBound(MessageBuffer)
    lastMsg = UBound(MessageBuffer)
    For msgIdx = firstMsg To lastMsg
        MsgText = Trim(tmpCmd & MessageBuffer(msgIdx))
        If MsgText <> "" Then
            MsgLen = Len(MsgText)
            CdiHeader.BuildMessageHeader ChkCmdIdx, tmpCmd, MsgLen, MessageHeader
            outText = MessageHeader & MsgText
            sndText = outText
            totLen = Len(outText)
            If ConnexType = LAPB Then
                'MsgBox
            ElseIf ConnexType = TCPIP Then
                If extCommConnex >= 0 Then
                    tmpLen = Trim(str(totLen))
                    tmpLen = Left((tmpLen & "    "), 4)
                    TcpConnect(extCommConnex).SendData tmpLen & outText
                    tmpdat = DisplayMessage(3, sndText, tmpCmd)
                    TraceOutData "SND", tmpCmd, sndText
                End If
            End If
        End If
    Next msgIdx
End Sub
Public Sub TraceOutData(tmpType As String, tmpCmd As String, tmpData As String)
    Dim TraceStatus As Boolean
    If TestProcs.chkLogFile.Value = 1 Then
        TraceStatus = False
        Select Case tmpType
            Case "RCV"
                If TestProcs.TraceRcv.Value = 1 Then
                    Select Case tmpCmd
                        Case "CCO"
                            If TestProcs.TraceCco.Value = 1 Then
                                TraceStatus = True
                            End If
                        Case "ERR"
                            If TestProcs.TraceErr.Value = 1 Then
                                TraceStatus = True
                            End If
                        Case "UFD"
                            If TestProcs.TraceUfd.Value = 1 Then
                                TraceStatus = True
                            End If
                        Case "PFD"
                            If TestProcs.TraceUfd.Value = 1 Then
                                TraceStatus = True
                            End If
                        Case "RFDR"
                            If TestProcs.TraceRfd.Value = 1 Then
                                TraceStatus = True
                            End If
                        Case "RFDA"
                            If TestProcs.TraceRfd.Value = 1 Then
                                TraceStatus = True
                            End If
                        Case Else
                    End Select
                End If
            Case "SND"
                If TestProcs.TraceSnd.Value = 1 Then
                    Select Case tmpCmd
                        Case "CCO"
                            If TestProcs.TraceCco.Value = 1 Then
                                TraceStatus = True
                            End If
                        Case "ERR"
                            If TestProcs.TraceErr.Value = 1 Then
                                TraceStatus = True
                            End If
                        Case "UFD"
                            If TestProcs.TraceUfd.Value = 1 Then
                                TraceStatus = True
                            End If
                        Case "PFD"
                            If TestProcs.TraceUfd.Value = 1 Then
                                TraceStatus = True
                            End If
                        Case "RFDR"
                            If TestProcs.TraceRfd.Value = 1 Then
                                TraceStatus = True
                            End If
                        Case "RFDA"
                            If TestProcs.TraceRfd.Value = 1 Then
                                TraceStatus = True
                            End If
                        Case Else
                    End Select
                End If
            Case Else
        End Select
        If TraceStatus = True Then
            Print #1, tmpData
        End If
    End If
End Sub

Private Sub CedaChkTimer_Timer()
    shSvrConLed(1).FillColor = vbRed
End Sub

Private Sub CedaConTimer_Timer()
    SendCcoToCeda
End Sub
Private Sub SendCcoToCeda()
    Dim CedaCmd As String, table As String, FieldList As String, CedaData As String, where As String, Line As String, retStr As String
    Dim tmpdat As String
    CedaCmd = "SBC"
    table = "CCOCHK"
    FieldList = "FIELDS"
    where = "SELECTION"
    CedaData = "DATA"
    shSvrFlowLed(0).FillColor = vbYellow
    shSvrConLed(0).FillColor = vbYellow
    shSvrFlowLed(1).FillColor = vbYellow
    shSvrFlowLed(2).FillColor = vbYellow
    If shSvrConLed(1).FillColor <> vbRed Then shSvrConLed(1).FillColor = vbYellow
    shSvrFlowLed(3).FillColor = vbYellow
    retStr = aUfis.Ufis(CedaCmd, table, FieldList, where, CedaData)
    tmpdat = DisplayMessage(1, (CedaCmd & "|" & table & "|" & FieldList & "|" & where & "|" & CedaData), CedaCmd)
    CedaChkTimer.Enabled = True
    shSvrFlowLed(0).FillColor = vbGreen
    shSvrConLed(0).FillColor = vbGreen
    shSvrFlowLed(1).FillColor = vbGreen
End Sub

Private Sub Command3_Click()
    TestProcs.Show , Me
End Sub

Private Sub Command5_Click()
    CdiHeader.Show , Me
End Sub
Private Sub Command6_Click()
    UfisSqlCfg.Show , Me
End Sub
Private Sub ExitCmd_Click()
    End
End Sub
Private Sub ExtCcoCheck_LostFocus()
'    Dim oldState As Boolean
'    oldState = ExtChkTimer.Enabled
'    ExtChkTimer.Enabled = False
'    ExtChkTimer.Interval = Val(ExtCcoCheck.Text) * 1000
'    ExtChkTimer.Enabled = oldState
End Sub
Private Sub ExtCcoTakt_LostFocus()
'    Dim oldState As Boolean
'    oldState = ExtCcoTimer.Enabled
'    ExtCcoTimer.Enabled = False
'    ExtCcoTimer.Interval = Val(ExtCcoTakt.Text) * 1000
'    ExtCcoTimer.Enabled = oldState
End Sub
Private Sub ExtChkTimer_Timer()
Static CcoCounter As Integer
    CcoCounter = CcoCounter + 1
    If CcoCounter >= 3 Then
        shExtConLed(1).FillColor = vbRed
        CcoCounter = 0
    End If
End Sub
Private Sub ExtConnexType_Click()
    TcpConnectionFrame.Visible = False
    LapbConnectionFrame.Visible = False
    Select Case ExtConnexType.Text
        Case "TCP/IP"
            TcpConnectionFrame.Visible = True
        Case "X25 LAPB"
            'LapbConnectionFrame.Visible = True
        Case Else
            '
    End Select
End Sub
Private Sub ExtCcoTimer_Timer()
Static CcoCounter As Integer
Dim i As Integer
    CcoCounter = CcoCounter + 1
    If CcoCounter >= 1 Then
        For i = 0 To 1
            If shExtConLed(i).FillColor <> vbRed Then shExtConLed(i).FillColor = vbYellow
        Next
        If CDI.SendMode(0).Value = True Then
            SendMessageData FOR_SEND, "CCO", " "
        Else
            SendMessageData FOR_RECV, "CCO", " "
        End If
        ExtChkTimer.Enabled = True
        CcoCounter = 0
    End If
End Sub
Private Sub ExtPollTakt_LostFocus()
    'Dim oldState As Boolean
    'oldState = LapbPollTimer.Enabled
    'LapbPollTimer.Enabled = False
    'LapbPollTimer.Interval = Val(ExtPollTakt.Text)
    'LapbPollTimer.Enabled = oldState
End Sub
Private Sub Form_Load()
Dim tmpData As String
    isread = False
    cbServer.Clear
    tmpData = GetIniEntry("c:\ufis\system\ceda.ini", "GLOBAL", "", "HOSTNAME", "")
    'MsgBox tmpData
    cbServer.AddItem tmpData
    cbServer.Text = tmpData
    cbConnectType.AddItem ("CEDA")
    cbConnectType.AddItem ("ODBC")
    extCommConnex = -1
    ConnexType = -1
    Height = 2565
    Width = 1680
    InitListFields
    txtRemoteHost.Text = TcpConnect(0).LocalIP
 End Sub
Private Sub Form_Resize()
    Dim newHeight As Long
    Dim newWidth As Long
    Dim i As Integer
    For i = 0 To 3
        tabDataList(i).Width = CDI.ScaleWidth - 6 * Screen.TwipsPerPixelX
        DataListFrame(i).Width = CDI.ScaleWidth - 4 * Screen.TwipsPerPixelX
        DataListFrame(i).Refresh
        Splitter(i).Width = CDI.ScaleWidth - 6 * Screen.TwipsPerPixelX
    Next
    newHeight = CDI.ScaleHeight - tabDataList(3).Top - myStatusBar.Height - 4 * Screen.TwipsPerPixelY
    If newHeight > 30 * Screen.TwipsPerPixelY Then
        tabDataList(3).Height = newHeight
        DataListFrame(3).Height = CDI.ScaleHeight - DataListFrame(3).Top - myStatusBar.Height - 3 * Screen.TwipsPerPixelY
        DataListFrame(3).Refresh
    End If
    newWidth = CDI.ScaleWidth - 3 * Screen.TwipsPerPixelX - ErrList.Left
    If newWidth > 800 Then ErrList.Width = newWidth
End Sub
Private Sub Form_Unload(Cancel As Integer)
    aUfis.CleanupCom
End Sub
Private Sub LapbPollTimer_Timer()
    LapbPollTimer.Enabled = False
End Sub
Private Sub SetRcvCommands(ipNbr As Integer)
    'Dim cmd As Integer
    'Dim count As Integer
    'Dim Data As String
    'Dim length As Long
    'count = Val(CmdRcvSet.Text) + ipNbr
    'If count < 0 Then count = 0
    'If ipNbr > 0 Then
    '    cmd = ReceiveCmd
    '    Data = ""
    '    length = Len(Data)
    '    X25.SubmitNCB cmd, Data, length
    'ElseIf ipNbr = 0 Then
    '    count = 0
    'End If
    'CmdRcvSet.Text = Trim(str(count))
End Sub
Private Sub lbRcvExtData_Click()
    MessageDetails.Show
    MessageDetails.Text1.Text = tabDataList(0).Text
End Sub
Private Sub lbSndExtData_DblClick()
    MessageDetails.Show
    MessageDetails.Text1.Text = tabDataList(3).Text
End Sub
Private Sub lbSndUfisData_Click()
    MessageDetails.Show
    MessageDetails.Text1.Text = tabDataList(1).Text
End Sub

Private Sub MakeCallOpt_Click()
    SendMode(1).Value = True
End Sub

Private Sub ResetCmd_Click()
    ResetListFields
    txtDummy.SetFocus
End Sub

Private Sub SendMode_Click(Index As Integer)
    If Index = 0 And SendMode(Index).Value = True Then
        Caption = "UFIS To DCS"
        ConnectionUfis.Enabled = True
    Else
        Caption = "DCS To UFIS (Integrated Simulator)"
        ConnectionUfis.Enabled = False
    End If
    txtDummy.SetFocus
End Sub
Private Sub Splitter_MouseDown(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
    Splitter(Index).Tag = Y
End Sub
Private Sub Splitter_MouseMove(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
    Dim tmpDiff As Integer
    Dim idx0 As Integer
    Dim newHeight0 As Integer
    Dim newHeight1 As Integer
    If (Splitter(Index).Tag > 0) And (Splitter(Index).Tag <> Y) Then
        tmpDiff = Y - Splitter(Index).Tag
        idx0 = Index - 1
        newHeight0 = DataListFrame(idx0).Height + tmpDiff
        newHeight1 = DataListFrame(Index).Height - tmpDiff
        If newHeight0 > 465 And newHeight1 > 465 Then
            Splitter(Index).Top = Splitter(Index).Top + tmpDiff
            tabDataList(idx0).Height = tabDataList(idx0).Height + tmpDiff
            DataListFrame(idx0).Height = DataListFrame(idx0).Height + tmpDiff
            DataListFrame(idx0).Refresh
            tabDataList(Index).Height = tabDataList(Index).Height - tmpDiff
            tabDataList(Index).Top = tabDataList(Index).Top + tmpDiff
            DataListFrame(Index).Height = DataListFrame(Index).Height - tmpDiff
            DataListFrame(Index).Top = DataListFrame(Index).Top + tmpDiff
            DataListFrame(Index).Refresh
        End If
    End If
End Sub
Private Sub Splitter_MouseUp(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
    Splitter(Index).Tag = -1
End Sub
Private Sub tabDataList_SendLButtonDblClick(Index As Integer, ByVal Line As Long, ByVal Col As Long)
    Dim tmpData As String
    Dim tmpNbr As String
    tmpData = tabDataList(Index).GetColumnValue(Line, 1)
    tmpNbr = tabDataList(Index).GetColumnValue(Line, 0)
    If tmpData <> "" Then
        MessageDetails.Text1.Text = tmpData
        MessageDetails.Label2.Caption = tmpNbr
        MessageDetails.Caption = GetItem(tabDataList(Index).GetLineValues(-1), 2, ",")
        MessageDetails.Show , Me
    End If
End Sub
Private Sub TcpConnect_Close(Index As Integer)
    'MsgBox CdiType & "Close: " & Index
    CloseTcpConnex Index
End Sub
Private Sub CloseTcpConnex(Index As Integer)
    Dim tmpdat As String
    extCommConnex = -1
    ConnectStatus.Visible = False
    ExtCcoTimer.Enabled = False
    If TcpConnect(Index).State <> sckClosed Then TcpConnect(Index).Close
    If TcpConnect(0).State <> sckClosed Then TcpConnect(0).Close
    If extCommConnex > 0 Then
        If TcpConnect(extCommConnex).State <> sckClosed Then TcpConnect(extCommConnex).Close
        Unload TcpConnect(extCommConnex)
    End If
    btnDisConExt.Enabled = False
    btnConnectExt.Enabled = True
    tmpdat = DisplayMessage(0, "-------------(disconnected)-----------------", "SYS")
    If WaitCallOpt.Value = True Then
        tmpdat = DisplayMessage(3, "-------------(waiting for call)-----------------", "SYS")
        btnConnectExt.Value = True
    End If
End Sub

Private Sub SubmitCloseLapbConnex(Index As Integer)
    'Dim info As Long
    'Dim ret As Long
    'Dim length As Long
    'Dim str As String
    'Dim bl As Boolean
    'Dim Data As String
    'Dim cmd As Integer
    'ExtCcoTimer.Enabled = False
    'btnDisConExt.Enabled = False
    'shExtFlowLed(0).FillColor = vbMagenta
    'shExtFlowLed(3).FillColor = vbMagenta
    'cmd = DisconnectCmd
    'Data = ""
    'length = Len(Data)
    'X25.SubmitNCB cmd, Data, length
    'MsgBox Data
End Sub
Private Sub FinishCloseLapbConnex(Index As Integer)
    'Dim tmpdat As String
    'LapbPollTimer.Enabled = False
    'X25.CloseSession
    'btnConnectExt.Enabled = True
    'shExtFlowLed(0).FillColor = vbRed
    'shExtFlowLed(3).FillColor = vbRed
    'HangUp.Visible = False
    'tmpdat = DisplayMessage(0, "-------------(disconnected)-----------------", "SYS")
End Sub

Private Sub TcpConnect_Connect(Index As Integer)
    ConnectStatus.Text = "Connected"
    extCommConnex = 0
End Sub

Private Sub TcpConnect_ConnectionRequest(Index As Integer, ByVal requestID As Long)
    If TcpConnect(0).State <> sckClosed Then TcpConnect(0).Close
    extCommConnex = 1
    Load TcpConnect(extCommConnex)
    TcpConnect(extCommConnex).Accept requestID
    ExtCcoTimer.Enabled = True
End Sub

Private Sub TcpConnect_DataArrival(Index As Integer, ByVal bytesTotal As Long)
    Static msgRest As String
    Dim RcvData As String
    Dim rcvText As String
    Dim RcvMsg As String
    Dim tmpLen As String
    Dim MsgLen As Integer
    Dim txtLen As Integer
    Dim msgPos As Integer
    Dim gotMsg As Boolean
    TcpConnect(Index).GetData RcvData, vbString
    rcvText = msgRest & RcvData
    msgRest = ""
    txtLen = Len(rcvText)
    gotMsg = True
    msgPos = 1
    While (msgPos < txtLen) And (gotMsg = True)
        gotMsg = False
        If (msgPos + 4) < txtLen Then
            tmpLen = Mid(rcvText, msgPos, 4)
            MsgLen = Val(tmpLen)
        End If
        If (msgPos + 3 + MsgLen) <= txtLen Then
            msgPos = msgPos + 4
            RcvMsg = Mid(rcvText, msgPos, MsgLen)
            msgPos = msgPos + MsgLen
            ProcessRcvMessage RcvMsg
            gotMsg = True
        End If
    Wend
    If gotMsg = False Then
        msgRest = Mid(rcvText, msgPos)
    End If
End Sub

Private Sub TcpConnect_Error(Index As Integer, ByVal Number As Integer, Description As String, ByVal Scode As Long, ByVal Source As String, ByVal HelpFile As String, ByVal HelpContext As Long, CancelDisplay As Boolean)
    CloseTcpConnex Index
    MsgBox CdiType & "Error: " & Index & "ErrNo: " & Number & vbLf & Description
End Sub

Private Sub TcpConnect_SendComplete(Index As Integer)
    'MsgBox "SendComplete: " & Index
End Sub

Private Sub TcpConnect_SendProgress(Index As Integer, ByVal bytesSent As Long, ByVal bytesRemaining As Long)
    'MsgBox "SendProgress: " & Index & vbLf & "Sent=" & bytesSent & "Rest=" & bytesRemaining
End Sub

Private Sub ProcessRcvMessage(RcvMsg As String)
    Dim RetVal As Boolean
    Dim tmpCmd As String
    Dim FieldList As String
    Dim DataList As String
    Dim tmpErrText As String
    Dim tmpLinNbr As String
    Dim tmpBuf As String
    shExtSysLed.FillColor = vbGreen
    shExtConLed(0).FillColor = vbGreen
    shExtConLed(1).FillColor = vbGreen
    RetVal = CdiHeader.CheckRcvMsg(RcvMsg, tmpCmd, FieldList, DataList)
    tmpBuf = Replace(RcvMsg, ",", ";", 1, -1, vbBinaryCompare)
    tmpLinNbr = DisplayMessage(0, tmpBuf, tmpCmd)
    If RetVal = RC_SUCCESS Then
        Select Case tmpCmd
            Case "RFD"
                If DataList = "R" Then
                    TraceOutData "RCV", "RFDR", RcvMsg
                    CDI.SendMessageData FOR_SEND, "RFD", "A"
                    SendFlightPlan
                End If
                If DataList = "A" Then
                    TraceOutData "RCV", "RFDA", RcvMsg
                End If
            Case "CCO"
                TraceOutData "RCV", tmpCmd, RcvMsg
                ExtChkTimer.Enabled = False
            Case "ERR"
                TraceOutData "RCV", tmpCmd, RcvMsg
            Case "PFD"
                TraceOutData "RCV", tmpCmd, RcvMsg
            Case "UFD"
                TraceOutData "RCV", tmpCmd, RcvMsg
            Case Else
                'All other Commands
                'MsgBox RcvMsg
        End Select
    Else
        'MsgBox "Invalid Message Received!" & vbNewLine & tmpCmd & vbNewLine & FieldList & vbNewLine & DataList
        tmpErrText = tmpLinNbr & "," & DataList & "," & tmpCmd & ": " & FieldList
        ErrList.InsertTextLineAt 0, tmpErrText, True
    End If
End Sub

Private Sub SendFlightPlan()
    CdiHeader.CdiHeaderReset -1, "UFD", "SND"
    UfisSqlCfg.SendArrivalData
    UfisSqlCfg.SendDepartureData
End Sub

Public Function DisplayMessage(UseIndex As Integer, MsgText As String, ActCmd As String) As String
Dim Index As Integer
    Dim count As Long
    Dim DisplayLine As String
    Dim MsgNbr As String
    Dim ForeColor As Long
    Dim BackColor As Long
    Static CcoCount(4) As Integer
    Index = UseIndex
    If SendMode(1).Value = True Then
        Select Case UseIndex
            Case 0
                Index = 2
            Case 3
                Index = 1
            Case Else
                Index = Abs(UseIndex)
        End Select
    End If
    count = Val(tabDataList(Index).Tag) + 1
    If count > 99999 Then count = 1
    MsgNbr = Right("      " & str(count), 5)
    DisplayLine = MsgNbr & "," & MsgText
    tabDataList(Index).Tag = MsgNbr
    If ActCmd = "CCO" Then
        If CcoCount(Index) = 2 Then
            tabDataList(Index).UpdateTextLine 0, DisplayLine, True
        Else
            tabDataList(Index).InsertTextLineAt 0, DisplayLine, True
            CcoCount(Index) = CcoCount(Index) + 1
        End If
    Else
        tabDataList(Index).InsertTextLineAt 0, DisplayLine, True
        CcoCount(Index) = 0
    End If
    GetCmdColors ActCmd, ForeColor, BackColor
    tabDataList(Index).SetLineColor 0, ForeColor, BackColor
    DisplayMessage = MsgNbr
End Function
Private Sub GetCmdColors(ActCmd As String, ForeColor As Long, BackColor As Long)
    Select Case ActCmd
        Case "CCO"
            ForeColor = vbBlack
            BackColor = vbCyan
        'Case "UFD"
        Case "RFD"
            ForeColor = vbGreen
            BackColor = vbBlack
        Case "ERR"
            ForeColor = vbWhite
            BackColor = vbRed
        Case "SYS"
            ForeColor = vbBlack
            BackColor = vbYellow
        Case Else
            ForeColor = vbBlack
            BackColor = vbWhite
    End Select
End Sub

Private Sub WaitCallOpt_Click()
    SendMode(0).Value = True
End Sub
