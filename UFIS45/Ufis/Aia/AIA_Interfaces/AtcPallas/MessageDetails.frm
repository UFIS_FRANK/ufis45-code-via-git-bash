VERSION 5.00
Begin VB.Form MessageDetails 
   Caption         =   "Form1"
   ClientHeight    =   915
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   9615
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   ScaleHeight     =   915
   ScaleWidth      =   9615
   StartUpPosition =   3  'Windows Default
   Begin VB.TextBox Text1 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9.75
         Charset         =   161
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   555
      Left            =   30
      MultiLine       =   -1  'True
      ScrollBars      =   1  'Horizontal
      TabIndex        =   1
      Top             =   330
      Width           =   9375
   End
   Begin VB.CommandButton btnClose 
      Caption         =   "Close"
      Height          =   255
      Left            =   8520
      TabIndex        =   0
      Top             =   30
      Width           =   1095
   End
   Begin VB.Label Label2 
      Caption         =   "12345"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   225
      Left            =   1590
      TabIndex        =   3
      Top             =   60
      Width           =   615
   End
   Begin VB.Label Label1 
      Caption         =   "List Line Number:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   225
      Left            =   60
      TabIndex        =   2
      Top             =   60
      Width           =   1605
   End
End
Attribute VB_Name = "MessageDetails"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private Sub btnClose_Click()
    Me.Hide
End Sub

Private Sub Form_Resize()
    Text1.Width = Me.ScaleWidth - 4 * Screen.TwipsPerPixelX
    btnClose.Left = Me.ScaleWidth - btnClose.Width - 2 * Screen.TwipsPerPixelX
End Sub
