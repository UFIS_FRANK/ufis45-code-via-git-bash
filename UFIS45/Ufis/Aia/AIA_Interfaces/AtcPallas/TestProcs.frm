VERSION 5.00
Begin VB.Form TestProcs 
   Caption         =   "Testprocedures and Logging (EXCO)"
   ClientHeight    =   3570
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   7125
   LinkTopic       =   "Form1"
   ScaleHeight     =   3570
   ScaleWidth      =   7125
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton Command17 
      Cancel          =   -1  'True
      Caption         =   "Close"
      Default         =   -1  'True
      Height          =   255
      Left            =   5790
      TabIndex        =   28
      Top             =   3240
      Width           =   1095
   End
   Begin VB.Frame Frame5 
      Caption         =   "Msg Data"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1815
      Left            =   5640
      TabIndex        =   26
      Top             =   1320
      Width           =   1335
      Begin VB.ComboBox ErrorCode 
         Height          =   315
         ItemData        =   "TestProcs.frx":0000
         Left            =   120
         List            =   "TestProcs.frx":002B
         TabIndex        =   27
         Text            =   "FM"
         Top             =   240
         Width           =   1095
      End
   End
   Begin VB.Frame Frame4 
      Caption         =   "Future Use"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2055
      Left            =   2880
      TabIndex        =   17
      Top             =   1320
      Width           =   2535
      Begin VB.CommandButton Command16 
         Height          =   255
         Left            =   120
         TabIndex        =   25
         Top             =   960
         Width           =   1095
      End
      Begin VB.CommandButton Command15 
         Height          =   255
         Left            =   120
         TabIndex        =   24
         Top             =   1320
         Width           =   1095
      End
      Begin VB.CommandButton Command14 
         Height          =   255
         Left            =   120
         TabIndex        =   23
         Top             =   1680
         Width           =   1095
      End
      Begin VB.CommandButton Command13 
         Height          =   255
         Left            =   1320
         TabIndex        =   22
         Top             =   600
         Width           =   1095
      End
      Begin VB.CommandButton Command12 
         Height          =   255
         Left            =   1320
         TabIndex        =   21
         Top             =   960
         Width           =   1095
      End
      Begin VB.CommandButton Command11 
         Height          =   255
         Left            =   1320
         TabIndex        =   20
         Top             =   1680
         Width           =   1095
      End
      Begin VB.CommandButton Command10 
         Height          =   255
         Left            =   1320
         TabIndex        =   19
         Top             =   1320
         Width           =   1095
      End
      Begin VB.CommandButton Command9 
         Height          =   255
         Left            =   120
         TabIndex        =   18
         Top             =   600
         Width           =   1095
      End
   End
   Begin VB.Frame Frame3 
      Caption         =   "CCO Timer"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1095
      Left            =   5640
      TabIndex        =   8
      Top             =   120
      Width           =   1335
      Begin VB.OptionButton CcoMode 
         Caption         =   "Disabled"
         Height          =   255
         Index           =   1
         Left            =   120
         TabIndex        =   10
         Top             =   480
         Width           =   975
      End
      Begin VB.OptionButton CcoMode 
         Caption         =   "Enabled"
         Height          =   255
         Index           =   0
         Left            =   120
         TabIndex        =   9
         Top             =   240
         Value           =   -1  'True
         Width           =   975
      End
   End
   Begin VB.Frame Frame2 
      Caption         =   "Send Message as"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2055
      Left            =   120
      TabIndex        =   2
      Top             =   1320
      Width           =   2535
      Begin VB.OptionButton SendMode 
         Caption         =   "PALLAS"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   225
         Index           =   1
         Left            =   1260
         TabIndex        =   35
         Top             =   300
         Width           =   1065
      End
      Begin VB.OptionButton SendMode 
         Caption         =   "UFIS"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   225
         Index           =   0
         Left            =   360
         TabIndex        =   1
         Top             =   300
         Value           =   -1  'True
         Width           =   795
      End
      Begin VB.CommandButton Command8 
         Caption         =   "RFD-R"
         Height          =   255
         Left            =   120
         TabIndex        =   16
         Top             =   600
         Width           =   1095
      End
      Begin VB.CommandButton Command7 
         Caption         =   "FlightPlan A"
         Height          =   255
         Left            =   1320
         TabIndex        =   15
         Top             =   1320
         Width           =   1095
      End
      Begin VB.CommandButton Command6 
         Caption         =   "FlightPlan D"
         Height          =   255
         Left            =   1320
         TabIndex        =   14
         Top             =   1680
         Width           =   1095
      End
      Begin VB.CommandButton SendDep 
         Caption         =   "UFD DEP"
         Height          =   255
         Left            =   1320
         TabIndex        =   7
         Top             =   960
         Width           =   1095
      End
      Begin VB.CommandButton SendArr 
         Caption         =   "UFD ARR"
         Height          =   255
         Left            =   1320
         TabIndex        =   6
         Top             =   600
         Width           =   1095
      End
      Begin VB.CommandButton Command3 
         Caption         =   "ERR"
         Height          =   255
         Left            =   120
         TabIndex        =   5
         Top             =   1680
         Width           =   1095
      End
      Begin VB.CommandButton Command2 
         Caption         =   "CCO"
         Height          =   255
         Left            =   120
         TabIndex        =   4
         Top             =   1320
         Width           =   1095
      End
      Begin VB.CommandButton Command1 
         Caption         =   "RFD-A"
         Height          =   255
         Left            =   120
         TabIndex        =   3
         Top             =   960
         Width           =   1095
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Event Logging"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1095
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   5295
      Begin VB.CheckBox TraceSnd 
         Caption         =   "Sender"
         Height          =   195
         Left            =   2880
         TabIndex        =   34
         Top             =   480
         Value           =   1  'Checked
         Width           =   975
      End
      Begin VB.CheckBox TraceRcv 
         Caption         =   "Receiver"
         Height          =   195
         Left            =   2880
         TabIndex        =   33
         Top             =   240
         Value           =   1  'Checked
         Width           =   1095
      End
      Begin VB.CheckBox TraceCco 
         Caption         =   "CCO"
         Height          =   195
         Left            =   2880
         TabIndex        =   32
         Top             =   720
         Value           =   1  'Checked
         Width           =   735
      End
      Begin VB.CheckBox TraceErr 
         Caption         =   "ERR"
         Height          =   195
         Left            =   4080
         TabIndex        =   31
         Top             =   720
         Value           =   1  'Checked
         Width           =   975
      End
      Begin VB.CheckBox TraceRfd 
         Caption         =   "RFD-R/A"
         Height          =   195
         Left            =   4080
         TabIndex        =   30
         Top             =   480
         Value           =   1  'Checked
         Width           =   975
      End
      Begin VB.CheckBox TraceUfd 
         Caption         =   "UFD/PFD"
         Height          =   195
         Left            =   4080
         TabIndex        =   29
         Top             =   240
         Value           =   1  'Checked
         Width           =   1095
      End
      Begin VB.CheckBox chkLogFile 
         Caption         =   "Logging activated for events of:"
         Height          =   255
         Left            =   120
         TabIndex        =   13
         Top             =   240
         Width           =   2655
      End
      Begin VB.TextBox LogFileName 
         Height          =   285
         Left            =   840
         TabIndex        =   11
         Text            =   "CDI2ATC.LOG"
         Top             =   600
         Width           =   1695
      End
      Begin VB.Label Label1 
         Caption         =   "Filename"
         Height          =   255
         Left            =   120
         TabIndex        =   12
         Top             =   600
         Width           =   735
      End
   End
End
Attribute VB_Name = "TestProcs"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub CcoMode_Click(Index As Integer)
    If Index = 0 Then
        CDI.ExtCcoTimer.Enabled = True
    Else
        CDI.ExtCcoTimer.Enabled = False
    End If
End Sub

Private Sub chkLogFile_Click()
    If chkLogFile.Value = 1 Then
        Open ("c:\tmp\" & LogFileName.Text) For Output As #1
    Else
        Close #1
    End If
End Sub

Private Sub Command1_Click()
    If SendMode(0).Value = True Then
        CDI.SendMessageData FOR_SEND, "RFD", "", "A"
    Else
        CDI.SendMessageData FOR_RECV, "RFD", "", "A"
    End If
End Sub

Private Sub Command17_Click()
    TestProcs.Hide
End Sub

Private Sub Command2_Click()
    If SendMode(0).Value = True Then
        CDI.SendMessageData FOR_SEND, "CCO", "", " "
    Else
        CDI.SendMessageData FOR_RECV, "CCO", "", " "
    End If
End Sub

Private Sub Command3_Click()
    Dim MsgDat As String
    If CurRcvSeqNbr = "" Then CurRcvSeqNbr = "0"
    MsgDat = CurRcvSeqNbr & ErrorCode.Text
    If SendMode(0).Value = True Then
        CDI.SendMessageData FOR_SEND, "ERR", "", MsgDat
    Else
        CDI.SendMessageData FOR_RECV, "ERR", "", MsgDat
    End If
End Sub

Private Sub Command6_Click()
    Dim FltRec As String
    Dim tmpMsgData As String
    Dim FieldList As String
    Open "c:\ufis\testdata\interfaces\cdi2atc\atc_dep_dat.txt" For Input As #2
    While Not EOF(2)
        Line Input #2, FltRec
        FieldList = UfisSqlCfg.DepFldLst.Text
        CDI.BuildMessageData FieldList, FltRec, tmpMsgData
        If SendMode(0).Value = True Then
            CDI.SendMessageData FOR_SEND, "UFD", "", tmpMsgData
        Else
            CDI.SendMessageData FOR_RECV, "PFD", "", tmpMsgData
        End If
    Wend
    Close #2
End Sub

Private Sub Command7_Click()
    Dim FltRec As String
    Dim tmpMsgData As String
    Dim FieldList As String
    Open "c:\ufis\testdata\interfaces\cdi2atc\atc_arr_dat.txt" For Input As #2
    While Not EOF(2)
        Line Input #2, FltRec
        FieldList = UfisSqlCfg.DepFldLst.Text
        CDI.BuildMessageData FieldList, FltRec, tmpMsgData
        If SendMode(0).Value = True Then
            CDI.SendMessageData FOR_SEND, "UFD", "", tmpMsgData
        Else
            CDI.SendMessageData FOR_RECV, "PFD", "", tmpMsgData
        End If
    Wend
    Close #2
End Sub

Private Sub Command8_Click()
    If SendMode(0).Value = True Then
        CDI.SendMessageData FOR_SEND, "RFD", "", "R"
    Else
        CDI.SendMessageData FOR_RECV, "RFD", "", "R"
    End If
End Sub

Private Sub Form_Activate()
    If CDI.ExtCcoTimer.Enabled = True Then
        CcoMode(0).Value = True
        CcoMode(1).Value = False
    Else
        CcoMode(0).Value = False
        CcoMode(1).Value = True
    End If
    If CDI.SendMode(0).Value = True Then
        SendMode(0) = True
    Else
        SendMode(1) = True
    End If
End Sub

Public Sub SendArr_Click()
    Dim FltRec As String
    Dim tmpMsgData As String
    Dim FieldList As String
    Open "c:\ufis\testdata\interfaces\cdi2atc\atc_arr_dat.txt" For Input As #2
    Line Input #2, FltRec
    Close #2
    FieldList = UfisSqlCfg.ArrFldLst.Text
    CDI.BuildMessageData FieldList, FltRec, tmpMsgData
    If SendMode(0).Value = True Then
        CDI.SendMessageData FOR_SEND, "UFD", "", tmpMsgData
    Else
        CDI.SendMessageData FOR_RECV, "PFD", "", tmpMsgData
    End If
End Sub

Private Sub SendDep_Click()
    Dim FltRec As String
    Dim tmpMsgData As String
    Dim FieldList As String
    Open "c:\ufis\testdata\interfaces\cdi2atc\atc_dep_dat.txt" For Input As #2
    Line Input #2, FltRec
    Close #2
    FieldList = UfisSqlCfg.DepFldLst.Text
    CDI.BuildMessageData FieldList, FltRec, tmpMsgData
    If SendMode(0).Value = True Then
        CDI.SendMessageData FOR_SEND, "UFD", "", tmpMsgData
    Else
        CDI.SendMessageData FOR_RECV, "PFD", "", tmpMsgData
    End If
End Sub

Private Sub SendMode_Click(Index As Integer)
    If Index = 0 Then
        SendArr.Caption = "UFD ARR"
        SendDep.Caption = "UFD DEP"
    Else
        SendArr.Caption = "PFD ARR"
        SendDep.Caption = "PFD DEP"
    End If
End Sub
