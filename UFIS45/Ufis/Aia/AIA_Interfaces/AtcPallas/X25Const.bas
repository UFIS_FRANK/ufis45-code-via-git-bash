Attribute VB_Name = "X.25 Constants"
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' X25COMM.BAS
'
' X.25 OCX Development Tools 2.0
'
' This module defines global constants for the X25COMM control.
' To use the module add it to your Visual Basic project.
'
' (c) Eicon Technology Corporation, 1998
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'
' Communications properties constants.
'
' These constants can be used to refer to a particular communications property,
' typically for X25CMD in the X25EVENT
'
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

Global Const X25CALL = 0
Global Const X25LISTEN = 1
Global Const X25ACCEPT = 2
Global Const X25SEND = 3
Global Const X25SENDCONF = 4

Global Const X25RECV = 7
Global Const X25HANGUP = 8
Global Const X25HANGUPCONF = 9
Global Const X25RESET = 10
Global Const X25RESETCONF = 11
Global Const X25FLOWCTL = 12

Global Const X25INIT = 16
Global Const X25END = 19
Global Const X25CANCEL = 20

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'
' Error codes returned returned in X25ERROR of the X25EVENT.
'
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

' Errors which start with ENET are returned by the EiconCard driver, the
' EiconCard, or by ECLAN (if applicable).

Global Const ENETNOERROR = 0                    ' No error
Global Const ENETBUFLEN = 1                     ' Invalid buffer length
Global Const ENETICMD = 3                       ' Invalid command
Global Const ENETCMDTO = 5                      ' Command timeout
Global Const ENETIMSG = 6                       ' Incomplete message
Global Const ENETILSN = 8                       ' Invalid local session number
Global Const ENETNOCKT = 9                      ' No circuit available
Global Const ENETCALLCLR = 10                   ' Call cleared
Global Const ENETCMDCANC = 11                   ' Command cancelled
Global Const ENETPUEXIST = 13                   ' PU name exists
Global Const ENETPUFULL = 14                    ' PU name table full
Global Const ENETNOSESS = 17                    ' Session table full
Global Const ENETUCALL = 18                     ' Unsuccessful call
Global Const ENETNAMENF = 20                    ' Protocol name not found
Global Const ENETINUSE = 22                     ' Circuit in use
Global Const ENETNOPU = 23                      ' PU name not found
Global Const ENETSRESET = 24                    ' Session reset
Global Const ENETNOTRACE = 25                   ' Trace not started
Global Const ENETNOMEM = 26                     ' Insufficient memory
Global Const ENET2MANYC = 34                    ' Too many outstanding command
Global Const ENETBADPORT = 35                   ' Bad port number
Global Const ENETNONUM = 37                     ' No number to dial
Global Const ENETNOTONE = 64                    ' No dial tone
Global Const ENETMDFAILED = 80                  ' Modem test failed
Global Const ENET_CLIENT_NOT_READY = 112        ' ECLAN Client not ready
Global Const ENET_SERVER_NOT_READY = 113        ' ECLAN Server not ready
Global Const ENET_SERVER_NOT_FOUND = 114        ' ECLAN Server not found
Global Const ENET_ECLAN_BUSY = 130              ' ECLAN Client not ready
Global Const ENETMDNR = 144                     ' Modem not ready
Global Const ENETMDDSR = 145                    ' Modem not ready (no DSR)
Global Const ENETMDCTS = 146                    ' Modem not ready (no CTS)
Global Const ENETMDCLK = 147                    ' Modem not ready (no clock)
Global Const ENETBUSY = 148                     ' Remote line busy
Global Const ENETNOANSWER = 149                 ' Remote not answering
Global Const ENETNOLINE = 150                   ' No line available
Global Const ENETMDUSED = 153                   ' Modem in use
Global Const ENETMDSYNTAX = 158                 ' Bad phone number syntax
Global Const ENETMDINIT = 159                   ' Modem initialized
Global Const ENETLINKNR = 160                   ' Link level not ready
Global Const ENETPKTLNR = 176                   ' Packet level not ready
Global Const ENETRESTART = 178                  ' Restart indication received
Global Const ENETXPTCCLRR = 192                 ' Transport connection cleared remotely
Global Const ENETXPTCCLRL = 193                 ' Transport connection cleared locally
Global Const ENETXPTOUTCRDR = 194               ' Timeout occurred on CR or DR
Global Const ENETXPXNNETCLR = 195               ' X.25 network connection cleared
Global Const ENETXPXNETRESET = 196              ' X.25 network reset
Global Const ENETXPTCREFR = 197                 ' transport connection refused by remote
Global Const ENETXPCRRNA = 198                  ' CR sent by remote site not accceptable
Global Const ENETTXPNOMEM = 207                 ' Not enough memory to start transport layer
Global Const ENETAGAIN = 248                    ' Submit request again
Global Const ENETSNAP = 249                     ' EiconCard snapshot
Global Const ENETSNARK = 250                    ' EiconCard snarked
Global Const ENETCARD = 251                     ' EiconCard error
Global Const ENETLOAD = 252                     ' EiconCard not loaded
Global Const ENETHALT = 253                     ' EiconCard halted
Global Const ENETNODRV = 254                    ' EiconCard driver not installed
Global Const ENETPEND = 255                     ' Command not completed

' Errors which start with EX25 are returned by the EX25.DLL used by X25COMM.OCX

Global Const EX25NOTINIT = 256                  ' Library not initialized
Global Const EX25LIB = 257                      ' Library error
Global Const EX25MALLOC = 258                   ' No more memory available
Global Const EX25DTEADDR = 259                  ' Bad DTE address
Global Const EX25INTR = 260                     ' Interrupted operation
Global Const EX25BUFUSED = 261                  ' Buffer in use
Global Const EX25NOCONN = 262                   ' Connection not established
Global Const EX25FACLEN = 263                   ' Bad facility length
Global Const EX25DONETO = 264                   ' Done command timed-out
Global Const EX25ICONN = 265                    ' Invalid connection number
Global Const EX25NOPEND = 266                   ' No pending request
Global Const EX25INVINIT = 267                  ' Invalid toolkit initialization
Global Const EX25ISRVNAME = 268                 ' Invalid gateway name
Global Const EX25NULLPTR = 269                  ' Invalid NULL pointer address

' Errors which start with EOCX are returned by the OCX Development Tools
' control X25COMM.OCX.

Global Const EOCXBUSY = 1000                    ' Control already connected or listening
Global Const EOCXNOINIT = 10001                 ' OCX linitialization error
Global Const EOCXRRESETIND = 1010               ' Remote reset indication, confirm generated
Global Const EOCXACKRRESETIND = 1011            ' Remote reset indication, confirm expected
Global Const EOCXLRESETIND = 1012               ' Local reset indication generated
Global Const EOCXRCLRIND = 1013                 ' Remote clear indication, confirme generated
Global Const EOCXACKRCLRIND = 1014              ' Remote clear indication, confirm expected
Global Const EOCXLCLRIND = 1015                 ' Local clear indication generated
Global Const EOCXAPPLCLRIND = 1016              ' Application generated clear request


'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'
' Cause codes returned by X25CAUSE property.
'
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

' Connection clearing causes

Global Const EX25DTEORG = 0                     ' DTE originated
Global Const EX25NUMBUSY = 1                    ' Number busy
Global Const EX25IFREQ = 3                      ' Invalid facility request
Global Const EX25NETCONG = 5                    ' Network congestion
Global Const EX25OUTORDER = 9                   ' Out of order
Global Const EX25ABARRED = 11                   ' Accessed barred
Global Const EX25NOTOBT = 13                    ' Not obtainable
Global Const EX25REMPROC = 17                   ' Remote procedure error
Global Const EX25LOCPROC = 19                   ' Local procedure error
Global Const EX25RPOAOOR = 21                   ' RPOA out of order
Global Const EX25REVCHRGNS = 25                 ' Reverse charging acceptance not subscribed
Global Const EX25INCDEST = 33                   ' Incompatible destinations
Global Const EX25FASTLELNS = 41                 ' Fast select acceptance not subscribed to
Global Const EX25SHIPABS = 57                   ' Ship absent (for mobile maritime service)

' Connection resetting causes

Global Const EX25RDTEORG = 0                    ' DTE originated call
Global Const EX25ROUTORDER = 1                  ' Out of order (PVC only)
Global Const EX25RREMPROC = 3                   ' Remote procedure error
Global Const EX25RLOCPROC = 5                   ' Local procedure error
Global Const EX25RNETCONG = 7                   ' Network congestion
Global Const EX25RREMDTEOP = 9                  ' Remote DTE operational (PVC only)
Global Const EX25RNETOP = 15                    ' Network operational (PVC only)
Global Const EX25RINCDEST = 17                  ' Incompatible destination
Global Const EX25RNETOOR = 29                   ' Network out of order (PVC only)

' Connection restarting causes

Global Const EX25RSTDTEORG = 0                  ' DTE originated call
Global Const EX25RSTLOCPROC = 1                 ' Local procedure error
Global Const EX25RSTNETOPER = 215               ' Network operational


'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'
' Diagnostic codes returned by X25DIAG property.
'
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

Global Const EX25NOINFO = 0                     ' No additional information
Global Const EX25INVPS = 1                      ' Invalid P(S)
Global Const EX25INVPR = 2                      ' Invalid P(R)
Global Const EX25PKTINV = 16                    ' Packet type invalid
Global Const EX25PKTIR1 = 17                    ' For state r1
Global Const EX25PKTIR2 = 18                    ' For state r2
Global Const EX25PKTIR3 = 19                    ' For state r3
Global Const EX25PKTIP1 = 20                    ' For state p1
Global Const EX25PKTIP2 = 21                    ' For state p2
Global Const EX25PKTIP3 = 22                    ' For state p3
Global Const EX25PKTIP4 = 23                    ' For state p4
Global Const EX25PKTIP5 = 24                    ' For state p5
Global Const EX25PKTIP6 = 25                    ' For state p6
Global Const EX25PKTIP7 = 26                    ' For state p7
Global Const EX25PKTID1 = 27                    ' For state d1
Global Const EX25PKTID2 = 28                    ' For state d2
Global Const EX25PKTID3 = 29                    ' For state d3
Global Const EX25PKTNA = 32                     ' Packet not allowed
Global Const EX25UPKT = 33                      ' Unidentifiable packet
Global Const EX25COWLC = 34                     ' Call on one way logical channel
Global Const EX25IPKT = 35                      ' Invalid packet type on a permanent virtual circuit
Global Const EX25PKTULC = 36                    ' Packet on unassigned logical channel
Global Const EX25REJNST = 37                    ' Reject not subscribed to
Global Const EX25PKT2S = 38                     ' Packet too short
Global Const EX25PKT2L = 39                     ' Packet too long
Global Const EX25IGFI = 40                      ' Invalid general format identifier
Global Const EX25RN0GFI = 41                    ' Restart with non-zero general format identifier
Global Const EX25PKTNCF = 42                    ' Packet type not compatible with  facility
Global Const EX25UINTRC = 43                    ' Unauthorized interrupt confirmation
Global Const EX25UINTR = 44                     ' Unauthorized interrupt
Global Const EX25UREJ = 45                      ' Unauthorized reject
Global Const EX25TIMEXP = 48                    ' Timer expired
Global Const EX25TEIC = 49                      '     for incoming call
Global Const EX25TECI = 50                      '     for clear indication
Global Const EX25TERI = 51                      '     for reset indication
Global Const EX25TERAI = 52                     '     for restart indication
Global Const EX25CSUP = 64                      ' Call set-up problem
Global Const EX25FCNA = 65                      ' Facility code not allowed
Global Const EX25FPNA = 66                      ' Facility parameter not allowed
Global Const EX25ICDA = 67                      ' Invalid called address
Global Const EX25ICGA = 68                      ' Invalid calling address
Global Const EX25IFRLEN = 69                    ' Invalid facility/registration len
Global Const EX25ICBARRED = 70                  ' Incoming call barred
Global Const EX25NLCAVAIL = 71                  ' No logical channel available
Global Const EX25CALLCOLL = 72                  ' Call collision
Global Const EX25DUPFACREQ = 73                 ' Duplicate facility requested
Global Const EX25N0ADDRLEN = 74                 ' Non-zero address length
Global Const EX25N0FACLEN = 75                  ' Non-zero facility length
Global Const EX25FNOTPROV = 76                  ' Facility not provided when expected
Global Const EX25ICCITTF = 77                   ' Invalid CCITT-specified DTE facility
Global Const EX25IMPCCODE = 81                  ' Improper cause code from DTE
Global Const EX25NOTALIGN = 82                  ' Non-aligned byte (octet)
Global Const EX25IQBITSET = 83                  ' Inconsistent Q-bit setting
Global Const EX25INTLPROB = 112                 ' International problem
Global Const EX25RNETPROB = 113                 ' Remote network problem
Global Const EX25INTLPPROB = 114                ' International protocol problem
Global Const EX25INTLLOOR = 115                 ' International link out of order
Global Const EX25INTLLBUSY = 116                ' International link busy
Global Const EX25TNETFPROB = 117                ' Transit network facility problem
Global Const EX25RNETFPROB = 118                ' Remote network facility problem
Global Const EX25INTLRPROB = 119                ' International routing problem
Global Const EX25TEMPRPROB = 120                ' Temporary routine problem
Global Const EX25UCDNIC = 121                   ' Unknown called DNIC
Global Const EX25MAINTACT = 122                 ' Maintenance action


'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'
' Some additional useful constants.
'
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

Global Const FLOWCTRLON = 1
Global Const FLOWCTRLOFF = 0

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'
' THE CONSANTS BELOW ARE FOR BACKWARDS COMPATABILITY ONLY!
'
' These constants were used in the original X.25 VBX control for VB3.0
' If your application uses these constants, it is recommended to convert to the
' current definitions defined above.
'
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

' Error codes returned by X25Error in X25Event.

Global Const X25ENOERROR = 0            'No errors
Global Const X25EINVBAFFERLEN = 1       'Invalid buffer length
Global Const X25EICMD = 3               'Invalid command
Global Const X25ECOMMANDTO = 5          'Command timeout
Global Const X25EMORESET = 6            'Incomplete message
Global Const X25EILSN = 8               'Invalid local session number
Global Const X25ENOVCAVAIL = 9          'No circuit available
Global Const X25ECALLCLEAR = 10         'Call cleared
Global Const X25ECMDCANCEL = 11         'Command canceled
Global Const X25ENOSESS = 17            'Session table full
Global Const X25ECALLFAILED = 18        'Unsuccessful call
Global Const X25ENAMENF = 20            'Protocol name not found
Global Const X25EINUSE = 22             'Circuit in use
Global Const X25ERESET = 24             'Session reset
Global Const X25EINSUFMEM = 26          'Insufficient memory
Global Const X25ETOOMANYCMD = 34        'Too many outstanding commands
Global Const X25BADPORT = 35            'Bad port number
Global Const X25ESNARK = 250            'EiconCard snarked
Global Const X25ECARD = 251             'EiconCard error
Global Const X25ELOAD = 252             'EiconCard not loaded
Global Const X25EHALT = 253             'EiconCard halted
Global Const X25ENODRV = 254            'EiconCard driver not installed
Global Const X25ELIBNOTINIT = 256       'Library not initialized
Global Const X25ELIBERROR = 257         'Library error
Global Const X25NOMOREMEM = 258         'No more memory available
Global Const X25BADDTEADDR = 259        'Bad DTE address
Global Const X25EINTR = 260             'Interrupted operation
Global Const X25EBUFUSED = 261          'Buffer in use
Global Const X25ENOCONN = 262           'Connection not established
Global Const X25EFACLEN = 263           'Bad facility length
Global Const X25EDONETO = 264           'Done command timed out
Global Const X25EICONN = 265            'Invalid connection number
Global Const X25ENOPEND = 266           'No pending request
Global Const X25EINVINIT = 267          'Invalid toolkit initialization
Global Const X25EISRVNAME = 268         'Invalid gateway name
Global Const X25ENULLPTR = 269          'Invalid NULL pointer address
Global Const X25INVOPT = 270            'Invalid Toolkit User option

'Clearing Cause Codes

Global Const X25EDTEORG = 0             'DTE originated
Global Const X25ENUMBUSY = 1            'Number busy
Global Const X25EIFREQ = 3              'Invalid facility request
Global Const X25ENETCONG = 5            'Network congestion
Global Const X25EOUTORDER = 9           'Out of order
Global Const X25EABARRED = 11           'Accessed barred
Global Const X25ENOTOBT = 13            'Not obtainable
Global Const X25EREMPROC = 17           'Remote procedure error
Global Const X25ELOCPROC = 19           'Local procedure error
Global Const X25ERPOAOOR = 21           'RPOA out of order
Global Const X25EREVCHRGNS = 25         'Reverse charging acceptance not subscribed

'Resetting Cause Codes

Global Const X25EREMDTEOP = 9           'Remote DTE operational (PVC only)
Global Const X25ENETOP = 5              'Network operational (PVC only)
Global Const X25EINCDEST = 17           'Incompatible destination
Global Const X25ENETOOR = 29            'Network out of order (PVC only)

'Restarting Cause Codes

Global Const X25ESTDTEORG = 0           'DTE originated call
Global Const X25ESTLOCPROC = 1          'Local procedure error
Global Const X25ESTNETOPER = 7          'Network operational

