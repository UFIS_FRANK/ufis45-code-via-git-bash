rem Copies all generated files from the SCM-Structue into the Install-Shield directory.


rem set drive to J: (IS 6.2) or K: (IS 8.0) 
set drive=k:

rem Copy ini- and config-files
copy c:\ufis_bin\*.ini %drive%\blr_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\*.cfg %drive%\blr_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\*.bmp %drive%\blr_build\ufis_client_appl\system\*.*

rem copy applications
copy c:\ufis_bin\debug\bdps_sec.exe %drive%\blr_build\ufis_client_appl\applications\*.*
copy c:\ufis_bin\debug\bdpspass.exe %drive%\blr_build\ufis_client_appl\applications\*.*
copy c:\ufis_bin\debug\bdpsuif.exe %drive%\blr_build\ufis_client_appl\applications\*.*
copy c:\ufis_bin\debug\fips.exe %drive%\blr_build\ufis_client_appl\applications\*.*
copy c:\ufis_bin\debug\rules.exe %drive%\blr_build\ufis_client_appl\applications\*.*
copy c:\ufis_bin\release\alerter.exe %drive%\blr_build\ufis_client_appl\applications\*.*
copy c:\ufis_bin\release\fidas.exe %drive%\blr_build\ufis_client_appl\applications\*.*
copy c:\ufis_bin\release\loadtabviewer.exe %drive%\blr_build\ufis_client_appl\applications\*.*
copy c:\ufis_bin\release\telexpool.exe %drive%\blr_build\ufis_client_appl\applications\*.*
copy c:\ufis_bin\release\importflights.exe %drive%\blr_build\ufis_client_appl\applications\*.*
copy c:\ufis_bin\release\flightdatagenerator.exe %drive%\blr_build\ufis_client_appl\applications\*.*
copy c:\ufis_bin\release\fips_reports.exe %drive%\blr_build\ufis_client_appl\applications\*.*
copy c:\ufis_bin\release\data_changes.exe %drive%\blr_build\ufis_client_appl\applications\*.*

REM copy system-files
copy c:\ufis_bin\release\bcproxy.exe %drive%\blr_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\bcproxy.tlb %drive%\blr_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\bcserv32.exe %drive%\blr_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\ufis32.dll %drive%\blr_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\ufisappmng.exe %drive%\blr_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\ufisappmng.tlb %drive%\blr_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\tab.ocx %drive%\blr_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\tab.tlb %drive%\blr_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\ufiscom.ocx %drive%\blr_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\ufiscom.tlb %drive%\blr_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\ucom.ocx %drive%\blr_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\ucom.tlb %drive%\blr_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\ugantt.ocx %drive%\blr_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\ugantt.tlb %drive%\blr_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\aatlogin.ocx %drive%\blr_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\aatlogin.tlb %drive%\blr_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\trafficlight.ocx %drive%\blr_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\trafficlight.tlb %drive%\blr_build\ufis_client_appl\system\*.*

rem copy interop dll's
copy c:\ufis_bin\release\*lib.dll %drive%\blr_build\ufis_client_appl\applications\interopdll\*.*
copy c:\ufis_bin\release\ufis.utils.dll %drive%\blr_build\ufis_client_appl\applications\interopdll\*.*
copy c:\ufis_bin\release\usercontrols.dll %drive%\blr_build\ufis_client_appl\applications\interopdll\*.*
copy c:\ufis_bin\release\ZedGraph.dll %drive%\blr_build\ufis_client_appl\applications\interopdll\*.*

rem Copy Help-Files
rem copy c:\ufis_bin\Help\*.chm %drive%\blr_build\ufis_client_appl\help\*.*