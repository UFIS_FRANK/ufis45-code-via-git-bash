#ifndef _DEF_mks_version
  #define _DEF_mks_version
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version[] = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Tools/rep_sapdf.c 1.0 2011/08/29 15:39:11SGT ble Exp  $";
#endif /* _DEF_mks_version */
/******************************************************************************/
/*                                                                            */
/* ABB ACE/FC Program Skeleton                                                */
/*                                                                            */
/* Author         :  Mei, Lee Bee Suan                                        */
/* Date           :  29-AUG-2011                                              */
/* Description    :  To populate SAPDF report for BLR                         */
/* Notes          :                                                           */
/*                                                                            */
/* Update history :                                                           */
/******************************************************************************/
/*                                                                            */
/* source-code-control-system version string                                  */

static char sccs_version[] ="@(#) UFIS45 (c) ABB AAT/I skeleton.c 45.1.0 / 11.12.2002 HEB";

/* be carefule with strftime or similar functions !!!                         */
/*                                                                            */
/******************************************************************************/
/* This program is a MIKE main program */

#define U_MAIN
#define UGCCS_PRG
#define STH_USE
#include <stdio.h>
#include <malloc.h>
#include <string.h>
#include <errno.h>
#include <signal.h>
#include <unistd.h>
#include "ugccsma.h"
#include "glbdef.h"
#include "uevent.h"
#include "db_if.h"
#include <time.h>
#include <cedatime.h>
#include <stdarg.h>

typedef struct 
{
    short SqlCursor;
    short SqlFunc;
    char  SqlBuf[2048];
    char  SqlData[4096];
} SQLREC;

typedef struct 
{
    char  DSSN[4];
    char  STYP[4];
    char  SSST[4];
    char  VALU[8];
} LOAREC;

int igDebug = 0;
int igNumFields;
char cg_FCLASS_DOM = 0x01;
char cg_FCLASS_INT = 0x02;
char cg_BCLASS_DOM = 0x04;
char cg_BCLASS_INT = 0x08;
char cg_ECLASS_DOM = 0x10;
char cg_ECLASS_INT = 0x20;
char cg_CLASS_FULL = 0x3F;
char pcgRepFname[128];
char cgRepDelimiter = '#';
char pcgDssnPriority[3][4]= { "USR", "LDM", "MVT" };
char pcgArrFields[] = "URNO,STOA,ONBL,ONBL,ALC3,ADID,RTYP,FTYP,ALC3,TTYP,PAXT,PAXT,"
                      "FLNO,DES4,VIA4,REGN,RKEY,AURN,STOA,STOD,LAND,LAND,AIRB,AIRB,"
                      "ONBL,ONBL,OFBL,OFBL,PSTA,ORG4,VIA4,EXT1,GTD1,MAIL,CGOT,FLNO,"
                      "TTYP,PSTD,BLT1,BLT2,BAGW,OFLC,OFLM,RWYA,RWYD,PAID,STOA,STOD,"
                      "DCD1,DTD1,DCD2,DTD2,B1BA,B1BA,B1EA,B1EA,B1BS,B1BS,B1ES,B1ES,"
                      "GD1X,GD1X,GD1Y,GD1Y,BOAO,BOAO,FCAL,FCAL";
char pcgDepFields[] = "URNO,STOD,OFBL,OFBL,ALC3,ADID,RTYP,FTYP,ALC3,TTYP,PAXT,PAXT,"
                      "FLNO,DES4,VIA4,REGN,RKEY,AURN,STOA,STOD,LAND,LAND,AIRB,AIRB,"
                      "ONBL,ONBL,OFBL,OFBL,PSTA,ORG4,VIA4,EXT1,GTD1,MAIL,CGOT,FLNO,"
                      "TTYP,PSTD,BLT1,BLT2,BAGW,OFLC,OFLM,RWYA,RWYD,PAID,STOA,STOD,"
                      "DCD1,DTD1,DCD2,DTD2,B1BA,B1BA,B1EA,B1EA,B1BS,B1BS,B1ES,B1ES,"
                      "GD1X,GD1X,GD1Y,GD1Y,BOAO,BOAO,FCAL,FCAL";

extern int debug_level = 0;
void dbglog(char *fmt, ...);
int GetPAXT( char *pcpUaft );
int TrimSpace( char *pcpInStr );

/******************************************************************************/
/*                                                                            */
/* The MAIN program                                                           */
/*                                                                            */
/******************************************************************************/
MAIN
{
    FILE *fptr;
    SQLREC rlSQLAFT;
    int ilRc;
    int ilField;
    int ili;
    int ilTries;
    int ilTotalPax;
    int ilUrno;
    char pclTmpStr[2048];
    char *p;

    igDebug = 1;
    if( argc < 4 )
    {
        dbglog( "Usage: rep_sapdf report_filename from_day to_day" );
        dbglog( "Eg.: rep_sapdf /ceda/exco/FILES/AFTTAB_20110829.txt 20110825 20110826" );
        exit( -1 );
    }

    igDebug = 0;
    if( argc > 4 && argv[4][0] == 'D' )
        igDebug = 1;
        
    do
    {
        ilRc = init_db();
        if( ilRc != DB_SUCCESS )
        {
            dbglog( "Cannot connect to UFIS database, try again\n" );
            sleep( 60 );
            ilTries++;
        }
    } while( ilTries <= 10 && ilRc != DB_SUCCESS );

    if( ilTries > 10 && ilRc != DB_SUCCESS )
    {
        dbglog( "Fail to connect to UFIS DB" );
        exit( -1 );
    }

    dbglog( "UFIS DB connected" );
    sprintf( pcgRepFname, "%s", argv[1] );
    dbglog( "Report File Name <%s>", pcgRepFname );

    fptr = fopen( pcgRepFname, "w" );
    if( fptr == NULL )
    {
        dbglog( "Cannot open file for writing <%s> error <%s><%d>", pcgRepFname, errno, strerror(errno) );
        exit( -1 );
    }

    /*** Work on ARRIVAL FLIGHTS first ***/
    /* Print fields */
    igNumFields = get_no_of_items(pcgArrFields);
    dbglog( "Number of Arrival Fields = %d", igNumFields  );
    memset( pclTmpStr, 0, sizeof(pclTmpStr) );
    strcpy( pclTmpStr, pcgArrFields );
    p = strchr( pclTmpStr, ',' );
    while( p != NULL )
    {
        *p = cgRepDelimiter;
        p = strchr( pclTmpStr, ',' );
    }
    fprintf( fptr, "%s%cLOA.PAXT%c\n", pclTmpStr, cgRepDelimiter, cgRepDelimiter );

    
    /* Get AFT Fields */
    memset( &rlSQLAFT, 0, sizeof(rlSQLAFT) );
    ilUrno = get_item_no( pcgArrFields, "URNO", 5 );
    sprintf( pclTmpStr, "(STOA BETWEEN '%s183000' AND '%s182900' "
                        "AND (FLNO NOT LIKE 'ZZ%%') AND (DES3 = 'BLR')) ORDER BY TIFA ", argv[2], argv[3] );
    sprintf( rlSQLAFT.SqlBuf, "SELECT %s FROM AFTTAB WHERE %s", pcgArrFields, pclTmpStr );
    rlSQLAFT.SqlCursor = 0;
    rlSQLAFT.SqlFunc = START;
    dbglog( "ARRIVAL flight query <%s>", rlSQLAFT.SqlBuf );
    ili=0;
    while( (sql_if( rlSQLAFT.SqlFunc, &rlSQLAFT.SqlCursor, rlSQLAFT.SqlBuf, rlSQLAFT.SqlData )) == DB_SUCCESS )
    {
        dbglog( "%d ARRIVAL flight", ili++ );
        rlSQLAFT.SqlFunc = NEXT;
        for( ilField = 0; ilField < igNumFields; ilField++ )
        {
            get_fld(rlSQLAFT.SqlData,ilField,STR,1024,pclTmpStr); 
            TrimSpace(pclTmpStr);
            if( ilField == ilUrno )
            {
                /* Calculate PAXT from LOATAB */
                ilTotalPax = GetPAXT( pclTmpStr );
            }
            fprintf( fptr, "%s%c", pclTmpStr, cgRepDelimiter );
        }
        fprintf( fptr, "%d%c\n", ilTotalPax, cgRepDelimiter );
    }
    dbglog( "Complete ARRIVAL flight" );
    close_my_cursor( &rlSQLAFT.SqlCursor );

    dbglog( "*******************************************************" );

    /*** Work on DEPARTURE FLIGHTS first ***/
    /* Print fields */
    igNumFields = get_no_of_items(pcgDepFields);
    dbglog( "Number of Departure Fields = %d", igNumFields  );
    memset( pclTmpStr, 0, sizeof(pclTmpStr) );
    strcpy( pclTmpStr, pcgDepFields );
    p = strchr( pclTmpStr, ',' );
    while( p != NULL )
    {
        *p = cgRepDelimiter;
        p = strchr( pclTmpStr, ',' );
    }
    fprintf( fptr, "%s%cLOA.PAXT%c\n", pclTmpStr, cgRepDelimiter, cgRepDelimiter );

    
    /* Get AFT Fields */
    memset( &rlSQLAFT, 0, sizeof(rlSQLAFT) );
    ilUrno = get_item_no( pcgDepFields, "URNO", 5 );
    sprintf( pclTmpStr, "(STOD BETWEEN '%s183000' AND '%s182900' "
                        "AND (FLNO NOT LIKE 'ZZ%%') AND (ORG3 = 'BLR') "
                        "AND (FTYP != 'T' and FTYP != 'G')) ORDER BY TIFD", argv[2], argv[3] );
    sprintf( rlSQLAFT.SqlBuf, "SELECT %s FROM AFTTAB WHERE %s", pcgDepFields, pclTmpStr );
    dbglog( "DEPARTURE flight query <%s>", rlSQLAFT.SqlBuf );
    rlSQLAFT.SqlCursor = 0;
    rlSQLAFT.SqlFunc = START;
    ili=0;

    while( (sql_if( rlSQLAFT.SqlFunc, &rlSQLAFT.SqlCursor, rlSQLAFT.SqlBuf, rlSQLAFT.SqlData )) == DB_SUCCESS )
    {
        dbglog( "%d DEPARTURE flight", ili++ );
        rlSQLAFT.SqlFunc = NEXT;
        for( ilField = 0; ilField < igNumFields; ilField++ )
        {
            get_fld(rlSQLAFT.SqlData,ilField,STR,1024,pclTmpStr); 
            TrimSpace(pclTmpStr);
            if( ilField == ilUrno )
            {
                /* Calculate PAXT from LOATAB */
                ilTotalPax = GetPAXT( pclTmpStr );
            }
            fprintf( fptr, "%s%c", pclTmpStr, cgRepDelimiter );
        }
        fprintf( fptr, "%d%c\n", ilTotalPax, cgRepDelimiter );
    }
    close_my_cursor( &rlSQLAFT.SqlCursor );
    dbglog( "Complete DEPARTURE flight" );

    
    fclose( fptr );
    dbglog( "File <%s> successfully created", pcgRepFname );
}

void dbglog(char *fmt, ...)
{
    va_list args;
    struct tm *tmptr;
    char pclTmStr[24];
    time_t tlNow;
    char pclPrint[1024];

    if( !igDebug )
        return;
    tlNow = time(0);
    tmptr = localtime( (time_t *)&tlNow );
    sprintf( pclTmStr, "%2.2d-%2.2d-%2.2d %2.2d:%2.2d:%2.2d ", 
                       tmptr->tm_mday, tmptr->tm_mon+1, tmptr->tm_year%100,
                       tmptr->tm_hour, tmptr->tm_min, tmptr->tm_sec );

     va_start(args, fmt);
     sprintf( pclPrint, "%s", pclTmStr );
     strcat( pclPrint, fmt );
     strcat( pclPrint, "\n" );
     vprintf( pclPrint, args );
     va_end( args );
}

int GetPAXT( char *pcpUaft )
{
    SQLREC rlSQLLOA;
    LOAREC rlLOA;
    int ilRc;
    int ilField;
    int ili;
    int ilPrio;
    int ilTotalPax;
    char clStatus, clOldStatus;
    char pclTmpStr[128];

    dbglog( "Enter GetPAXT UAFT <%s>", pcpUaft );
    clStatus = 0x00;
    ilTotalPax = 0;

    for( ilPrio = 0; ilPrio < 3; ilPrio++ )
    {
        if( clStatus == cg_CLASS_FULL )
            break;
        memset( &rlSQLLOA, 0, sizeof(SQLREC) );
        sprintf( rlSQLLOA.SqlBuf, "SELECT TRIM(VALU),TRIM(STYP),TRIM(SSST) FROM LOATAB " 
                                  "WHERE FLNU = %s AND TYPE = 'PAX' AND DSSN = '%s' AND APC3 = ' '"
                                  "AND SSTP = ' ' AND STYP IN ('F','B','E') AND SSST IN ('DOM','INT')", 
                                  pcpUaft, pcgDssnPriority[ilPrio] );
        dbglog( "LOA Query <%s>", rlSQLLOA.SqlBuf );
        rlSQLLOA.SqlCursor = 0;
        rlSQLLOA.SqlFunc = START;
        while( (sql_if( rlSQLLOA.SqlFunc, &rlSQLLOA.SqlCursor, rlSQLLOA.SqlBuf, rlSQLLOA.SqlData )) == DB_SUCCESS )
        {
            memset( &rlLOA, 0, sizeof(LOAREC) );
            rlSQLLOA.SqlFunc = NEXT;
            clOldStatus = clStatus;
            get_fld(rlSQLLOA.SqlData,FIELD_1,STR,8,rlLOA.VALU); 
            get_fld(rlSQLLOA.SqlData,FIELD_2,STR,8,rlLOA.STYP); 
            get_fld(rlSQLLOA.SqlData,FIELD_3,STR,8,rlLOA.SSST); 
            dbglog( "STYP <%s> SSST <%s> VALU <%s>", rlLOA.STYP, rlLOA.SSST, rlLOA.VALU );
            if( rlLOA.STYP[0] == 'F' )
            {
                dbglog( "FCLASS" );
                if( !strcmp( rlLOA.SSST, "DOM" ) )
                {
                    dbglog( "DOM Status <0x%x>", clStatus );
                    if( !(clStatus & cg_FCLASS_DOM) )
                        clStatus |= cg_FCLASS_DOM;
                }
                else
                {
                    dbglog( "INT Status <0x%x>", clStatus );
                    if( !(clStatus & cg_FCLASS_INT) )
                        clStatus |= cg_FCLASS_INT;
                }
            }
            else if( rlLOA.STYP[0] == 'B' )
            {
                dbglog( "BCLASS" );
                if( !strcmp( rlLOA.SSST, "DOM" ) )
                {
                    dbglog( "DOM Status <0x%x> DOM <0x%x> and <0x%x>", clStatus, cg_BCLASS_DOM, clStatus&cg_BCLASS_DOM );
                    if( !(clStatus & cg_BCLASS_DOM) )
                    {
                        dbglog( "Set BCLASS DOMESTIC" );
                        clStatus |= cg_BCLASS_DOM;
                    }
                }
                else
                {
                    dbglog( "INT Status <0x%x>", clStatus );
                    if( !(clStatus & cg_BCLASS_INT) )
                        clStatus |= cg_BCLASS_INT;
                }
            }
            else if( rlLOA.STYP[0] == 'E' )
            {
                dbglog( "ECLASS" );
                if( !strcmp( rlLOA.SSST, "DOM" ) )
                {
                    dbglog( "DOM Status <0x%x>", clStatus  );
                    if( !(clStatus & cg_ECLASS_DOM) )
                        clStatus |= cg_ECLASS_DOM;
                }
                else
                {
                    dbglog( "INT Status <0x%x>", clStatus );
                    if( !(clStatus & cg_ECLASS_INT) )
                        clStatus |= cg_ECLASS_INT;
                }
            }
            if( clStatus != clOldStatus )
                ilTotalPax += atoi(rlLOA.VALU);
        } /* while loop */
        close_my_cursor( &rlSQLLOA.SqlCursor );
    }
    dbglog( "GetPAXT clStatus = <0x%x> ilTotalPax <%d>", clStatus, ilTotalPax );
    return ilTotalPax;
}

/* trim front and back space only */
int TrimSpace( char *pcpInStr )
{
    int ils =0, ile = 0;
    int ilLen;
    char *pclTmpStr;
    char *pclFunc = "TrimSpace";

    if( pcpInStr == NULL || (ilLen=strlen(pcpInStr)) <= 0 )
        return;
    for( ils = 0; ils < ilLen; ils++ )
        if( pcpInStr[ils] != ' ' )
            break;
    if( ils == ilLen )
    {
        pcpInStr[0] = '\0';
        return;
    }
    for( ile = ilLen-1; ile >= 0; ile-- )
        if( pcpInStr[ile] != ' ' )
            break;
    if( ils == 0 && ile >= ils )
    {
        pcpInStr[ile+1] = '\0';
        return;
    }
    pclTmpStr = (char *)malloc( ilLen + 10 );
    memcpy( pclTmpStr, &pcpInStr[ils], ile-ils+1 );
    memset( pcpInStr, 0, ilLen );
    strcpy( pcpInStr, pclTmpStr );
    free( pclTmpStr );
}
