#ifndef _DEF_mks_version
  #define _DEF_mks_version
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version[] = "@(#) "UFIS_VERSION" $Id: Ufis/DEL/DEL_Server/Base/Server/Fids/remhdl.c 1.2 2008/04/03 14:58:59SGT akl Exp  $";
#endif /* _DEF_mks_version */
/******************************************************************************/
/*                                                                            */
/* ABB AAT/I REMHDL.C                                                         */
/*                                                                            */
/* Author         : Jochen Hiller                               */
/* Date           : 19. November 2001                                         */
/* Description    : Remark handler trigger for all remarks in AFTTAB          */
/*                                                                            */
/* Update history :                                                           */
/*                                                                            */
/*                                                                            */
/*                                                                            */
/*                                                                            */
/*                                                                            */
/*                                                                            */
/******************************************************************************/
/*                                                                            */
/* source-code-control-system version string                                  */
static char sccs_remhdl[]="%Z% UFIS 4.4 (c) ABB AAT/I %M% %I% / %E% %U% / JHI";
/* be carefule with strftime or similar functions !!!                         */
/*                                                                            */
/******************************************************************************/
/* This program is a MIKE main program */
#define U_MAIN
#define UGCCS_PRG
#define STH_USE

#include <stdio.h>
#include <malloc.h>
#include <errno.h>
#include <signal.h>
#include <netdb.h>
#include <time.h>
#include "debugrec.h"
#include "hsbsub.h"
#include "db_if.h" 
#include "tools.h"
#include "helpful.h"
#include "timdef.h"
 

#define XS_BUFF  128
#define S_BUFF   512
#define M_BUFF   1024
#define L_BUFF   2048
#define XL_BUFF  4096
#define XXL_BUFF 8192 
#define FIELDS "URNO,REMP,STOA,ETOA,ETAI,TIFA,STOD,ETOD,ETDI,TIFD,FTYP,GD1X,GD1Y,AIRB,LAND,OFBL,ONBL,TMOA,FLNO,B1BA,B1EA"



/******************************************************************************/
/* External variables                                                         */
/******************************************************************************/
FILE *outp       = NULL;
int  debug_level = TRACE;
/******************************************************************************/
/* External functions                                                         */
/******************************************************************************/
extern int SetSignals(void (*HandleSignal)(int));
extern int DebugPrintItem(int,ITEM *);
extern int DebugPrintEvent(int,EVENT *);
extern int init_db(void);
extern int  ResetDBCounter(void);
extern void HandleRemoteDB(EVENT*);
extern int  sql_if(short ,short* ,char* ,char* ); 
extern int close_my_cursor(short *cursor); 
extern void snap(char*,int,FILE*);
extern int GetDataItem(char *pcpResult, char *pcpInput, int ipNum, char cpDel,char *pcpDef,char *pcpTrim);
extern int BuildItemBuffer( char *pcpData, char *pcpFieldList, int ipNoOfFields, char *pcpSepChr);
extern int get_item_no(char *s, char *f, short elem_len);
extern void  GetServerTimeStamp(char*,int,long,char*);
static void GetConfig(char* pcpFile,char* pcpSection,char* pcpTag,char* pcpTarget,char* pcpDefault);
static void GetConfigValue(char* pcpFile,char* pcpSection,char* pcpTag,int* piTarget,int piDefault);
/******************************************************************************/
/* Global variables                                                           */
/******************************************************************************/
static ITEM  *prgItem      = NULL;        /* The queue item pointer  */
static EVENT *prgEvent     = NULL;        /* The event pointer       */
static int   igItemLen     = 0;           /* length of incoming item */
static int   igInitOK      = FALSE;      /* Flag for init */
/*   static CFG   *prgCfg; */                     /* structure of the cfg-file */
static char  pcgHomeAp[XS_BUFF];      /* buffer for home airport */
static char  pcgTabEnd[XS_BUFF];      /* buffer for TABEND */
static char  pcgTwEnd[XS_BUFF];          /* buffer for TABEND */
static char  pcgConfFile[S_BUFF];      /* buffer for config-file name */
static int   igUseHopo = FALSE;          /* flag for use of HOPO-field */
static int   igModID_Router  = 1200;      /* MOD-ID of Router  */
static char  pcgCLOSED[XS_BUFF];
static char  pcgCLOSING[XS_BUFF];
static char  pcgOPEN[XS_BUFF];
static char  pcgDELAYED[XS_BUFF];
static int   igUTCDIFF;
static char  pcgEKAirlines[XS_BUFF];
static char  pcgSpecialRemarks[XS_BUFF];
static int   igRemarkBeforeStdForEK;
static int   igRemarkBeforeStdForOther;
static int   igOpenBeforeStdForEK;
static int   igOpenBeforeStdForOther;
static int   igClosingBeforeEtdForEK;
static int   igClosingBeforeEtdForOther;
static int   igClosedBeforeEtdForEK;
static int   igClosedBeforeEtdForOther;
static int   igDelayTimeDiffDep;
static int   igHOMEAIRLINE;
static int   igOTHERAIRLINE;
static int   igDiffUtcToLocal;
static char  pcgHostName[XS_BUFF];
static char  pcgHostIp[XS_BUFF];
/*entry's from configfile*/
static char     pcgARRIVALFIELDS[L_BUFF];
static char     pcgDEPARTUREFIELDS[L_BUFF];
static int    igQueCounter=0;
static int   lgArrCount  = 4;          /* Number of internal array's */
/* for trigger action */
static EVENT *prgOutEvent = NULL;

static int    Init_remhdl();
static int    Reset(void);                        /* Reset program          */
static void   Terminate(int);                     /* Terminate program      */
static void   HandleSignal(int);                  /* Handles signals        */
static void   HandleErr(int);                     /* Handles general errors */
static void   HandleQueErr(int);                  /* Handles queuing errors */
static int    HandleInternalData(void);           /* Handles event data     */
static void   HandleQueues(void);                 /* Waiting for Sts.-switch*/
/******************************************************************************/
/* Function prototypes by JWE&JHI                                                    */
/******************************************************************************/
/* Init-functions  */

static int GetQueues();
static int SendEvent(char *pcpCmd,int ipModID,int ipPriority,char *pcpTable,
                     char *pcpTwStart,char* pcpTwEnd,
		     char *pcpSelection,char *pcpFields,char *pcpData,
                     char *pcpAddStruct,int ipAddstructSize); 
static int GetRecord(char* pcpUrno,char* pcpAdid);
static int TriggerRemarks(char*  pcpFields,char* pcpRemp,char* pcpUrno,char* pcpData,char* pcpAdid,char* pcpResult);
static int TriggerCheckin(void);
static int TriggerCheckinNew(void);
static long GetSecondsFromCEDATime(char *pcpDateTime);
/******************************************************************************/
/*                                                                            */
/* The MAIN program                                                           */
/*                                                                            */
/******************************************************************************/
MAIN
{
  int    ilRc = RC_SUCCESS;            /* Return code            */
  int    ilCnt = 0;
  int   ilItemFlag=TRUE; 
  time_t now = 0;
  INITIALIZE;            /* General initialization    */

  /* signal handling of SIGPIPE,SIGCHLD,SIGALRM,SIGTERM */
  SetSignals(HandleSignal);

  dbg(TRACE,"------------------------------------------");
  dbg(TRACE,"MAIN: version <%s>",sccs_remhdl);

  /* Attach to the MIKE queues */
  do{
    ilRc = init_que();
    if(ilRc != RC_SUCCESS)
      {
	dbg(TRACE,"MAIN: init_que() failed! waiting 6 sec ...");
	sleep(6);
	ilCnt++;
      }/* end of if */
  }while((ilCnt < 10) && (ilRc != RC_SUCCESS));
  if(ilRc != RC_SUCCESS)
    {
      dbg(TRACE,"MAIN: init_que() failed! waiting 60 sec ...");
      sleep(60);
      exit(1);
    }else{
      dbg(TRACE,"MAIN: init_que() OK!");
      dbg(TRACE,"MAIN: mod_id   <%d>",mod_id);
      dbg(TRACE,"MAIN: mod_name <%s>",mod_name);
    }/* end of if */
  do
    {
      ilRc = init_db();
      if (ilRc != RC_SUCCESS)
	{
	  dbg(TRACE,"MAIN: init_db() failed! waiting 6 sec ...");
	  sleep(6);
	  ilCnt++;
	} /* end of if */
    } while((ilCnt < 10) && (ilRc != RC_SUCCESS));
  if(ilRc != RC_SUCCESS)
    {
      dbg(TRACE,"MAIN: init_db() failed! waiting 60 sec ...");
      sleep(60);
      exit(2);
    }else{
      dbg(TRACE,"MAIN: init_db()  OK!");
    } /* end of if */

  /* logon to DB is ok, but do NOT use DB while ctrl_sta == HSB_COMING_UP !!! */
  *pcgConfFile = 0x00;
  sprintf(pcgConfFile,"%s/%s",getenv("BIN_PATH"),mod_name);
  ilRc = TransferFile(pcgConfFile);
  if(ilRc != RC_SUCCESS)
    {
      dbg(TRACE,"MAIN: TransferFile(%s) failed!",pcgConfFile);
    } /* end of if */
  dbg(TRACE,"MAIN: Binary-file = <%s>",pcgConfFile);
  ilRc = SendRemoteShutdown(mod_id);
  if(ilRc != RC_SUCCESS)
    {
      dbg(TRACE,"MAIN: SendRemoteShutdown(%d) failed!",mod_id);
    }
  sprintf(pcgConfFile,"%s/fids/%s.cfg",getenv("CFG_PATH"),mod_name);
  
  dbg(TRACE,"MAIN: Config-file = <%s>",pcgConfFile);     






  if((ctrl_sta != HSB_STANDALONE) && (ctrl_sta != HSB_ACTIVE) && (ctrl_sta != HSB_ACT_TO_SBY))
    {
      dbg(TRACE,"MAIN: waiting for status switch ...");
      HandleQueues();
      dbg(TRACE,"MAIN: now running ...");
    }/* end of if */
  if((ctrl_sta == HSB_STANDALONE) || (ctrl_sta == HSB_ACTIVE) || (ctrl_sta == HSB_ACT_TO_SBY))
    {
      dbg(TRACE,"MAIN: initializing ...");
      dbg(TRACE,"------------------------------------------");
      if(igInitOK == FALSE)
	{
	  ilRc = Init_remhdl();
	  if(ilRc == RC_SUCCESS)
	    {
	      dbg(TRACE,"");
	      dbg(TRACE,"------------------------------------------");
	      dbg(TRACE,"MAIN: initializing OK");
	      igInitOK = TRUE;
	    } 
	}
    }else{
      Terminate(1);
    }
  dbg(TRACE,"------------------------------------------");
    
  if (igInitOK == TRUE)
    {
      now = time(NULL);
      while(TRUE)
	{
	  memset(prgItem,0x00,igItemLen);
	  ilRc = que(QUE_GETBIG,0,mod_id,PRIORITY_3,igItemLen,(char *)&prgItem);
	  dbg(TRACE,"QUE Counter %d",++igQueCounter);
	  /* depending on the size of the received item  */
	  /* a realloc could be made by the que function */
	  /* so do never forget to set event pointer !!! */
	  prgEvent = (EVENT *) prgItem->text;
	  if( ilRc == RC_SUCCESS )
	    {
	      /* Acknowledge the item */
	      ilRc = que(QUE_ACK,0,mod_id,0,0,NULL);
	      if( ilRc != RC_SUCCESS ) 
		{
		  /* handle que_ack error */
		  HandleQueErr(ilRc);
		} /* fi */
	      switch( prgEvent->command )
		{
		case    HSB_STANDBY    :
		  ctrl_sta = prgEvent->command;
		  HandleQueues();
		  break;    
		case    HSB_COMING_UP    :
		  ctrl_sta = prgEvent->command;
		  HandleQueues();
		  break;    
		case    HSB_ACTIVE    :
		  ctrl_sta = prgEvent->command;
		  break;    
		case    HSB_ACT_TO_SBY    :
		  ctrl_sta = prgEvent->command;
		  /* CloseConnection(); */
		  HandleQueues();
		  break;    
		case    HSB_DOWN    :
		  /* whole system shutdown - do not further use que(), send_message() or timsch() ! */
		  ctrl_sta = prgEvent->command;
		  Terminate(1);
		  break;    
		case    HSB_STANDALONE    :
		  ctrl_sta = prgEvent->command;
		  ResetDBCounter();
		  break;    
		case    REMOTE_DB :
		  /* ctrl_sta is checked inside */
		  HandleRemoteDB(prgEvent);
		  break;
		case    SHUTDOWN    :
		  /* process shutdown - maybe from uutil */
		  Terminate(1);
		  break;
		case    RESET        :
		  ilRc = Reset();
		  break;
		case    EVENT_DATA    :
		  if((ctrl_sta == HSB_STANDALONE) ||
		     (ctrl_sta == HSB_ACTIVE) ||
		     (ctrl_sta == HSB_ACT_TO_SBY))
		    {
		      ilItemFlag=TRUE;
		      ilRc = HandleInternalData();
		      if(ilRc != RC_SUCCESS)
			{
			  HandleErr(ilRc);
			}/* end of if */
		    }
		  else
		    {
		      dbg(TRACE,"MAIN: wrong HSB-status <%d>",ctrl_sta);
		      DebugPrintItem(TRACE,prgItem);
		      DebugPrintEvent(TRACE,prgEvent);
		    }/* end of if */
		  break; 
		case    TRACE_ON :
		  dbg_handle_debug(prgEvent->command);
		  break;
		case    TRACE_OFF :
		  dbg_handle_debug(prgEvent->command);
		  break;
          
		  /*-----------------------testroutine by jhi ----------------------*/
		  break;
		default            :
		  dbg(TRACE,"MAIN: unknown event");
		  DebugPrintItem(TRACE,prgItem);
		  DebugPrintEvent(TRACE,prgEvent);
		  break;
		} /* end switch */
	    }else{
	      /* Handle queuing errors */
	      HandleQueErr(ilRc);
	    } /* end else */



	  /**************************************************************/
	  /* time parameter for cyclic actions                          */
	  /**************************************************************/
    
	  now = time(NULL);

	} /* end while */
    }else{
      dbg(TRACE,"MAIN: Init_remhdl() failed with <%d> Sleeping 30 sec.! Then terminating ...",ilRc);
      sleep(30);
    }
  exit(0);
  return 0;
} /* end of MAIN */

/******************************************************************************/
/* The initialization routine                                                 */
/******************************************************************************/
static int Init_remhdl()
{
  int    ilRc = RC_SUCCESS;            /* Return code */
  GetQueues();
  /* reading default home-airport from sgs.tab */
  memset(pcgHomeAp,0x00,sizeof(pcgHomeAp));
  ilRc = tool_search_exco_data("SYS","HOMEAP",pcgHomeAp);
  if (ilRc != RC_SUCCESS)
    {
      dbg(TRACE,"Init_remhdl : No HOMEAP entry in sgs.tab: EXTAB! Please add!");
      return RC_FAIL;
    }
  else
    {
      dbg(TRACE,"Init_remhdl : HOMEAP = <%s>",pcgHomeAp);
    }
  /* reading default table-extension from sgs.tab */
  memset(pcgTabEnd,0x00,sizeof(pcgTabEnd));
  ilRc = tool_search_exco_data("ALL","TABEND",pcgTabEnd);
  if (ilRc != RC_SUCCESS)
    {
      dbg(TRACE,"Init_remhdl : No TABEND entry in sgs.tab: EXTAB! Please add!");
      return RC_FAIL;
    }
  else
    {
      dbg(TRACE,"Init_remhdl : TABEND = <%s>",pcgTabEnd);
      memset(pcgTwEnd,0x00,XS_BUFF);
      sprintf(pcgTwEnd,"%s,%s,%s",pcgHomeAp,pcgTabEnd,mod_name);
      dbg(TRACE,"Init_remhdl : TW_END = <%s>",pcgTwEnd);
      
      if (strcmp(pcgTabEnd,"TAB") == 0)
	{
	  igUseHopo = TRUE;
	  dbg(TRACE,"Init_remhdl: use HOPO-field!");
	}
    }
  CheckCfg();
  return(ilRc);
} /* end of initialize */
/*********************************************************************
Function : GetQueues()
Paramter :
Return Code: RC_SUCCESS,RC_FAIL
Result:
Description: Gets all necessary queue-ID's for CEDA-internal
             communication!
*********************************************************************/
static int GetQueues()
{
  int ilRc = RC_FAIL;

  /* get mod-id of router */
  if ((igModID_Router = tool_get_q_id("router")) == RC_NOT_FOUND ||
      igModID_Router == RC_FAIL || igModID_Router == 0)
    {
      dbg(TRACE,"GetQueues   : tool_get_q_id(router) returns: <%d>",igModID_Router);
      ilRc = RC_FAIL;
    }else{
      dbg(TRACE,"GetQueues   : <router> mod_id <%d>",igModID_Router);
      ilRc = RC_SUCCESS;
    } 
  return ilRc;
}

static int CheckCfg(void)
{
  int ilRc = RC_SUCCESS;
  GetConfigValue(pcgConfFile,"MAIN","OFFSET_TO_UTC_IN_MIN",&igUTCDIFF,240);
  GetConfigValue(pcgConfFile,"MAIN","DELAY_TIME_DIFF_DEP",&igDelayTimeDiffDep,15);
  GetConfig(pcgConfFile,"MAIN","EK_AIRLINES",pcgEKAirlines,"EK");
  GetConfigValue(pcgConfFile,"MAIN","TRIGGER_REM_EK",&igRemarkBeforeStdForEK,180);
  GetConfigValue(pcgConfFile,"MAIN","TRIGGER_REM_OTHER",&igRemarkBeforeStdForOther,180);
  GetConfigValue(pcgConfFile,"MAIN","TRIGGER_OPEN_EK",&igOpenBeforeStdForEK,180);
  GetConfigValue(pcgConfFile,"MAIN","TRIGGER_OPEN_OTHER",&igOpenBeforeStdForOther,180);
  GetConfigValue(pcgConfFile,"MAIN","TRIGGER_CLOSING_EK",&igClosingBeforeEtdForEK,60);
  GetConfigValue(pcgConfFile,"MAIN","TRIGGER_CLOSING_OTHER",&igClosingBeforeEtdForOther,90);
  GetConfigValue(pcgConfFile,"MAIN","TRIGGER_CLOSED_EK",&igClosedBeforeEtdForEK,30);
  GetConfigValue(pcgConfFile,"MAIN","TRIGGER_CLOSED_OTHER",&igClosedBeforeEtdForOther,45);
  GetConfig(pcgConfFile,"MAIN","CLOSED",pcgCLOSED,"CCC");
  GetConfig(pcgConfFile,"MAIN","CLOSING",pcgCLOSING,"CCL");
  GetConfig(pcgConfFile,"MAIN","OPEN",pcgOPEN,"CCO");
  GetConfig(pcgConfFile,"MAIN","DELAYED",pcgDELAYED,"CCD");
  GetConfig(pcgConfFile,"MAIN","SPECIAL_REMARKS",pcgSpecialRemarks,"RSK");
  igHOMEAIRLINE = igClosingBeforeEtdForEK;
  igOTHERAIRLINE = igClosingBeforeEtdForOther;


  return ilRc;
}

static void GetConfig(char* pcpFile,char* pcpSection,char* pcpTag,char* pcpTarget,char* pcpDefault)
{
  int ilRc = RC_FAIL;
  char pclTmp[L_BUFF];
  memset(pclTmp,0x00,L_BUFF);
  if ((ilRc = iGetConfigEntry(pcpFile,pcpSection,pcpTag,CFG_STRING,pclTmp)) == RC_SUCCESS)
    {
      strcpy(pcpTarget,pclTmp);
      dbg(TRACE,"GetCfgEntry %s \tis <%s>.",pcpTag,pclTmp);
    }else{
      strcpy(pcpTarget,pcpDefault);
      dbg(TRACE,"GetCfgEntry set %s \tto default<%s>.",pcpTag,pcpTarget);
    }
}

static void GetConfigValue(char* pcpFile,char* pcpSection,char* pcpTag,int* piTarget,int piDefault)
{
  int ilRc = RC_FAIL;
  char pclTmp[L_BUFF];

  memset(pclTmp,0x00,L_BUFF);
  if ((ilRc = iGetConfigEntry(pcgConfFile,pcpSection,pcpTag,CFG_STRING,pclTmp)) == RC_SUCCESS)
    {
      *piTarget = (time_t)atoi(pclTmp);
      dbg(TRACE,"GetCfgEntry %s \t\tis <%d>",pcpTag,*piTarget);
    }else{
      *piTarget = piDefault;
      if(strlen(pcpTag)>10)
	{
	  dbg(TRACE,"GetCfgEntry set %s \tto default<%d>",pcpTag,*piTarget);
	}else{
	  dbg(TRACE,"GetCfgEntry set %s \t\tto default<%d>",pcpTag,*piTarget);
	}
      
    }
}



/******************************************************************************/
/* The Reset routine                                                          */
/******************************************************************************/
static int Reset()
{

  int    ilRc = RC_SUCCESS;    /* Return code */
    
  dbg(TRACE,"Reset: now reseting ...");

  return ilRc;
    
} /* end of Reset */
/******************************************************************************/
/* The termination routine                                                    */
/******************************************************************************/
static void Terminate(int ipSleep)
{
  dbg(TRACE,"Terminate: now leaving ...");
 
  sleep(ipSleep);
  
  exit(0);
    
} /* end of Terminate */
/******************************************************************************/
/* The handle signals routine                                                 */
/******************************************************************************/
static void HandleSignal(int pipSig)
{
  switch(pipSig)
    {
    case SIGALRM:
      break;
    case SIGPIPE:
      break;
    case SIGCHLD:
      break;
    case SIGTERM:
      Terminate(1);
      break;
    default    :
      Terminate(10);
      break;
    } /* end of switch */
} /* end of HandleSignal */
/******************************************************************************/
/* The handle general error routine                                           */
/******************************************************************************/
static void HandleErr(int pipErr)
{
  /*    int    ilRc = RC_SUCCESS; */
    return;
} /* end of HandleErr */
/******************************************************************************/
/* The handle queuing error routine                                           */
/******************************************************************************/
static void HandleQueErr(int pipErr)
{
    int    ilRc = RC_SUCCESS;
    
    switch(pipErr) {
    case    QUE_E_FUNC    :    /* Unknown function */
        dbg(TRACE,"<%d> : unknown function",pipErr);
        break;
    case    QUE_E_MEMORY    :    /* Malloc reports no memory */
        dbg(TRACE,"<%d> : malloc failed",pipErr);
        break;
    case    QUE_E_SEND    :    /* Error using msgsnd */
            dbg(TRACE,"<%d> : msgsnd failed",pipErr);
            break;
    case    QUE_E_GET    :    /* Error using msgrcv */
            if(pipErr != 4)        
             dbg(DEBUG,"<%d> : msgrcv failed",pipErr);
        break;
    case    QUE_E_EXISTS    :
        dbg(TRACE,"<%d> : route/queue already exists ",pipErr);
        break;
    case    QUE_E_NOFIND    :
        dbg(TRACE,"<%d> : route not found ",pipErr);
        break;
    case    QUE_E_ACKUNEX    : 
        dbg(TRACE,"<%d> : unexpected ack received ",pipErr);
        break;
    case    QUE_E_STATUS    :
        dbg(TRACE,"<%d> :   unknown queue status ",pipErr);
        break;
    case    QUE_E_INACTIVE    :
        dbg(TRACE,"<%d> : queue is inaktive ",pipErr);
        break;
    case    QUE_E_MISACK    :
        dbg(TRACE,"<%d> : missing ack ",pipErr);
        break;
    case    QUE_E_NOQUEUES    :
        dbg(TRACE,"<%d> : queue does not exist",pipErr);
        break;
    case    QUE_E_RESP    :    /* No response on CREATE */
        dbg(TRACE,"<%d> : no response on create",pipErr);
        break;
    case    QUE_E_FULL    :
        dbg(TRACE,"<%d> : too many route destinations",pipErr);
        break;
    case    QUE_E_NOMSG    :    /* No message on queue */
        /*dbg(TRACE,"<%d> : no messages on queue",pipErr);*/
        break;
    case    QUE_E_INVORG    :    /* Mod id by que call is 0 */
        dbg(TRACE,"<%d> : invalid originator=0",pipErr);
        break;
    case    QUE_E_NOINIT    :    /* Queues is not initialized*/
        dbg(TRACE,"<%d> : queues are not initialized",pipErr);
        break;
    case    QUE_E_ITOBIG    :
        dbg(TRACE,"<%d> : requestet itemsize to big ",pipErr);
        break;
    case    QUE_E_BUFSIZ    :
        dbg(TRACE,"<%d> : receive buffer to small ",pipErr);
        break;
    case    QUE_E_PRIORITY    :
        dbg(TRACE,"<%d> : wrong priority was send ",pipErr);
        break;
    default            :    /* Unknown queue error */
        dbg(TRACE,"<%d> : unknown error",pipErr);
        break;
    } /* end switch */
         
    return;
} /* end of HandleQueErr */
/******************************************************************************/
/* The handle queues routine                                                  */
/******************************************************************************/
static void HandleQueues()
{
  int    ilRc = RC_SUCCESS;            /* Return code */
  int    ilBreakOut = FALSE;
    
  do{
    memset(prgItem,0x00,igItemLen);
    ilRc = que(QUE_GETBIG,0,mod_id,PRIORITY_3,igItemLen,(char *)&prgItem);
    /* depending on the size of the received item  */
    /* a realloc could be made by the que function */
    /* so do never forget to set event pointer !!! */
    prgEvent = (EVENT *) prgItem->text;    
    if( ilRc == RC_SUCCESS )
      {
    /* Acknowledge the item */
    ilRc = que(QUE_ACK,0,mod_id,0,0,NULL);
    if( ilRc != RC_SUCCESS ) 
      {
        /* handle que_ack error */
        HandleQueErr(ilRc);
      } /* fi */
        
    switch( prgEvent->command )
      {
      case    HSB_STANDBY    :
        ctrl_sta = prgEvent->command;
        break;    
      case    HSB_COMING_UP    :
        ctrl_sta = prgEvent->command;
        break;    
      case    HSB_ACTIVE    :
        ctrl_sta = prgEvent->command;
        ilBreakOut = TRUE;
        break;    
      case    HSB_ACT_TO_SBY    :
        ctrl_sta = prgEvent->command;
        break;    
      case    HSB_DOWN    :
        /* whole system shutdown - do not further use que(), send_message() or timsch() ! */
        ctrl_sta = prgEvent->command;
        Terminate(10);
        break;    
      case    HSB_STANDALONE    :
        ctrl_sta = prgEvent->command;
        ResetDBCounter();
        ilBreakOut = TRUE;
        break;    
      case    REMOTE_DB :
        /* ctrl_sta is checked inside */
        HandleRemoteDB(prgEvent);
        break;
      case    SHUTDOWN    :
        Terminate(1);
        break;
      case    RESET        :
        ilRc = Reset();
        break;
      case    EVENT_DATA    :
        dbg(TRACE,"HandleQueues: wrong hsb status <%d>",ctrl_sta);
        DebugPrintItem(TRACE,prgItem);
        DebugPrintEvent(TRACE,prgEvent);
        break;
      case    TRACE_ON :
        dbg_handle_debug(prgEvent->command);
        break;
      case    TRACE_OFF :
        dbg_handle_debug(prgEvent->command);
        break;
      default            :
        dbg(TRACE,"HandleQueues: unknown event");
        DebugPrintItem(TRACE,prgItem);

        DebugPrintEvent(TRACE,prgEvent);

        break;
      } /* end switch */
      }else{
    /* Handle queuing errors */
    HandleQueErr(ilRc);
      } /* end else */
  } while (ilBreakOut == FALSE);
  if(igInitOK == FALSE)
    {
      ilRc = Init_remhdl();
      if(ilRc == RC_SUCCESS)
    {
      dbg(TRACE,"HandleQueues: Init_remhdl() OK!");
      igInitOK = TRUE;
    }else{ /* end of if */
      dbg(TRACE,"HandleQueues: Init_remhdl() failed!");
      igInitOK = FALSE;
    } /* end of if */
    }/* end of if */
  /* OpenConnection(); */
} /* end of HandleQueues */
/******************************************************************************/
/* The handle data routine                                                    */
/******************************************************************************/
static int HandleInternalData()
{
  int  ilRc = RC_SUCCESS;      /* Return code */
  char *pclSelection = NULL;
  char *pclFields = NULL;
  char *pclData = NULL;
  char pclCuteFields[L_BUFF];
  char pclNewData[L_BUFF];
  char pclOldData[L_BUFF];
  char pclUrno[50];
  char pclADID[5];
  char pclRemp[XS_BUFF];
  char pclResult[XS_BUFF];
  char *pclTmpPtr=NULL;
  BC_HEAD *bchd = NULL;          /* Broadcast header*/
  CMDBLK  *cmdblk = NULL; 
  bchd  = (BC_HEAD *) ((char *)prgEvent + sizeof(EVENT));
  cmdblk= (CMDBLK  *) ((char *)bchd->data);

  /***********************************/
  /*    DebugPrintItem(DEBUG,prgItem);   */
  /*    DebugPrintEvent(DEBUG,prgEvent); */
  /***********************************/
  memset(pclOldData,0x00,L_BUFF);
  pclSelection = cmdblk->data;
  pclFields = (char *)pclSelection + strlen(pclSelection) + 1;
  pclData = (char*)pclFields + strlen(pclFields) + 1;

  pclTmpPtr=NULL;
  strcpy(pclNewData,pclData);
  pclTmpPtr=strstr(pclNewData,"\n");
  if(pclTmpPtr !=NULL)
    {
      strcpy(pclOldData,pclTmpPtr);
      *pclTmpPtr='\0';
    }
    if (!strcmp(cmdblk->command,"CHECK"))
    { 
      /* TriggerCheckin(); */
      TriggerCheckinNew();
    }
  
  /* dbg(DEBUG,"\nFields <%s> \nNewData <%s>\nOldData <%s>\nSelection <%s>\n",pclFields,pclNewData,pclOldData,pclSelection); */
  if (!strcmp(cmdblk->command,"REMP"))
    { 
      pclUrno[0]='\0';
      pclADID[0]='\0';
      
      GetDataItem(pclUrno,pclNewData,1,','," ","\0 ");
      GetDataItem(pclADID,pclNewData,2,','," ","\0 ");

      if(strchr(pclADID,'A')!=NULL||strchr(pclADID,'D')!=NULL)
	{
	  GetRecord(pclUrno,pclADID);
	}
    }
  return ilRc;
} /* end of HandleInternalData */
/*********************************************************************
Function : SendEvent()
Paramter : IN: pcpCmd = command for cmdblk->command
           IN: ipModID = process-ID where the event is send to
       IN: ipPriority = priority for sending ( 1- 5, usuallay 3)
       IN: pcpTable = Name (3 letters) of cmdblk->obj_name (if
           necessary), will be expanded with "pcgTabEnd".
       IN: pcpTwStart = cmdblk->twstart
       IN: pcpTwEnd = cmdblk->twend (always HOMEAP,TABEND,processname)
       IN: pcpSelection = selection for event (cmdblk->data)
       IN: pcpFields = fieldlist (corresponding to pcpdata)
       IN: pcpData = datalist (comma separated, corresponding to 
                                   pcpFields)
       IN: pcpAddStruct = additional structure to be transmitted
       IN: ipAddStructSize = size of the additional structure
Return Code: RC_SUCCESS, RC_FAIL
Result:
Description: Sends an event to another CEDA-process using que(QUE_PUT).
             Sends the event in standard CEDA-format (BCHEAD,CMDBLK,
         selection,fieldlist,datalist) or sends a different
         data structure (special) at CMDBLK->data. !! Sends always
         only one type, standard OR special, with one event !!
*********************************************************************/
static int SendEvent(char *pcpCmd,int ipModID,int ipPriority,char *pcpTable,
             char *pcpTwStart, char *pcpTwEnd,
             char *pcpSelection,char *pcpFields,char *pcpData,
             char *pcpAddStruct,int ipAddStructSize)
{
  int     ilRc             = RC_FAIL;
  int     ilLen            = 0;
  EVENT   *prlOutEvent  = NULL;
  BC_HEAD *prlOutBCHead = NULL;
  CMDBLK  *prlOutCmdblk = NULL;

  if (pcpAddStruct == NULL)
    ipAddStructSize = 0;
  if (ipAddStructSize == 0)
    pcpAddStruct = NULL;

  /* size-calculation for prlOutEvent */
  ilLen = sizeof(EVENT) + sizeof(BC_HEAD) + sizeof(CMDBLK) + 
    strlen(pcpSelection) + strlen(pcpFields) + strlen(pcpData) + 
    ipAddStructSize + 128;

  /* memory for prlOutEvent */
  if ((prlOutEvent = (EVENT*)malloc((size_t)ilLen)) == NULL)
    {
      dbg(TRACE,"SendEvent: cannot malloc <%d>-bytes for outgoing event!",ilLen);
      prlOutEvent = NULL;
    }else{
      /* clear whole outgoing event */
      memset((void*)prlOutEvent, 0x00, ilLen);

      /* set event structure... */
      prlOutEvent->type         = SYS_EVENT;
      prlOutEvent->command    = EVENT_DATA;
      prlOutEvent->originator   = (short)mod_id;
      prlOutEvent->retry_count  = 0;
      prlOutEvent->data_offset  = sizeof(EVENT);
      prlOutEvent->data_length  = ilLen - sizeof(EVENT); 

      /* BC_HEAD-Structure... */
      prlOutBCHead = (BC_HEAD*)((char*)prlOutEvent+sizeof(EVENT));
      /* prlOutBCHead->rc = (short)RC_SUCCESS;*/
      prlOutBCHead->rc = (short)NETOUT_NO_ACK;/*spaeter nur bei disconnect*/
      strncpy(prlOutBCHead->dest_name,mod_name,10);
      strncpy(prlOutBCHead->recv_name, "EXCO",10);
 
      /* Cmdblk-Structure... */
      prlOutCmdblk = (CMDBLK*)((char*)prlOutBCHead->data);
      strcpy(prlOutCmdblk->command,pcpCmd);
      if (pcpTable != NULL)
    {
      strcpy(prlOutCmdblk->obj_name,pcpTable);
      strcat(prlOutCmdblk->obj_name,pcgTabEnd);
    }
        
      /* setting tw_x entries */
      strncpy(prlOutCmdblk->tw_start,pcpTwStart,32);
      strncpy(prlOutCmdblk->tw_end,pcpTwEnd,32);
        
      /* means that no additional structure is used */
      /* STANDARD CEDA-ipcs between CEDA-processes */
      if (pcpAddStruct == NULL)
    {
      /* setting selection inside event */
      strcpy(prlOutCmdblk->data,pcpSelection);
      /* setting field-list inside event */
      strcpy(prlOutCmdblk->data+strlen(pcpSelection)+1,pcpFields);
      /* setting data-list inside event */
      strcpy((prlOutCmdblk->data + (strlen(pcpSelection)+1) + (strlen(pcpFields)+1)),pcpData);
    }else{
      /*an additional structure is used and will be copied to */
      /*cmdblk + sizeof(CMDBLK).!!! No STANDARD CEDA-ipcs is used !!!! */
      memcpy(prlOutCmdblk->data,(char*)pcpAddStruct,ipAddStructSize);    
    }

      /*DebugPrintEvent(DEBUG,prlOutEvent);*/ 
      /*snapit((char*)prlOutEvent,ilLen,outp);*/
      dbg(DEBUG,"SendEvent: sending event to mod_id <%d>",ipModID);

      if (ipModID != 0)
    {
      if ((ilRc = que(QUE_PUT,ipModID,mod_id,ipPriority,ilLen,(char*)prlOutEvent))
          != RC_SUCCESS)
        {
          dbg(TRACE,"SendEvent: QUE_PUT returns: <%d>", ilRc);
          Terminate(1);
        }
    }else{
      dbg(TRACE,"SendEvent: mod_id = <%d>! Can't send!",ipModID);
    }
      /* free memory */
      free((void*)prlOutEvent); 
    }
  return ilRc;
}

static int GetRecord(char* pcpUrno,char* pcpAdid)
{
  int ilRc = RC_SUCCESS;
  short slCursor = 0;
  short slFkt = 0;
  int ilItemNo=0;
 

  char pclSqlBuf[M_BUFF];
  char pclBuildSelectBuf[M_BUFF];
  char pclSqlAnswer[XL_BUFF];
  char pclResult[M_BUFF];
  char pclRemp[XS_BUFF];
  memset(pclSqlAnswer,0x00,XL_BUFF);

  strcpy(pclSqlBuf,"SELECT ");
  strcat(pclSqlBuf,FIELDS);
  sprintf(pclBuildSelectBuf," where urno = %s",pcpUrno);
  /*built up select statement*/
  strcat(pclSqlBuf," from afttab ");
  strcat(pclSqlBuf,pclBuildSelectBuf);
  slFkt = START; 
  slCursor = 0;
  if((ilRc = sql_if(slFkt,&slCursor,pclSqlBuf,pclSqlAnswer))!=RC_SUCCESS)
    {
      dbg(TRACE,"<InitDatabaseFieldInfos> sql_if in Line <%d> failed with <%d>",__LINE__,ilRc);
    }
  dbg(DEBUG,"%s",pclSqlBuf);
  close_my_cursor(&slCursor);
  BuildItemBuffer(pclSqlAnswer,FIELDS,0,",");
  ilItemNo = get_item_no(FIELDS,"REMP",5);
  ilItemNo++;

  GetDataItem(pclRemp,pclSqlAnswer,ilItemNo,',',"","");
  if(NOT_FOUND==TriggerRemarks(FIELDS,pclRemp,pcpUrno,pclSqlAnswer,pcpAdid,pclResult))
    {
      dbg(DEBUG,"Ignore Event");
    }
  return ilRc;
} 
  
static int TriggerRemarks(char*  pcpFields,char* pcpRemp,char* pcpUrno,char* pcpData,char* pcpAdid,char *pcpResult)
{
  int ilRc = RC_SUCCESS;
  int ilFieldCnt = 0;
  int ilItemNo = 0;
  char pclTmpBuf[XS_BUFF];
  char pclTmpBuf2[XS_BUFF];
  char pclTimeBuf1[XS_BUFF];
  char pclTimeBuf2[XS_BUFF];
  char pclDBSel[XS_BUFF];
  char pclDBFields[XS_BUFF];
  char pclDBData[XS_BUFF];
  char pclFlno[XS_BUFF];        
  memset(pclDBSel ,0x00,XS_BUFF);
  memset(pclDBFields ,0x00,XS_BUFF);
  memset(pclDBData ,0x00,XS_BUFF);
  memset(pclTmpBuf,0x00,XS_BUFF);
  memset(pclTmpBuf2,0x00,XS_BUFF);
  memset(pclTimeBuf1,0x00,XS_BUFF);
  memset(pclTimeBuf2,0x00,XS_BUFF);
  memset(pclFlno,0x00,XS_BUFF);

  ilItemNo = get_item_no(pcpFields,"FLNO",5);
  ilItemNo++;
  GetDataItem(pclFlno,pcpData,ilItemNo,',',"","");
  

  /*for arrival*/
  if(strcmp(pcpAdid,"A")==0)
    {
      /*Last bag*/
      ilItemNo = get_item_no(pcpFields,"B1EA",5);
      ilItemNo++;
      GetDataItem(pclTmpBuf,pcpData,ilItemNo,',',"","");
      ilItemNo = get_item_no(pcpFields,"B1BA",5);
      ilItemNo++;
      GetDataItem(pclTmpBuf2,pcpData,ilItemNo,',',"","");
      DeleteCharacterInString(pclTmpBuf,cBLANK);
      if(pclTmpBuf[0]!='\0'&&pclTmpBuf2[0]!='\0')
	{
	  strcpy(pcpResult,"LBG");
	  sprintf(pclDBSel,"WHERE AURN = %s",pcpUrno);
	  strcpy(pclDBFields,"CREC");
	  
	  dbg(TRACE,"SEND URT CREC <%s> URNO <%s> FLNO <%s>", pcpResult, pcpUrno ,pclFlno);
	  if ((ilRc = SendEvent("URT",igModID_Router,PRIORITY_4,"FLD","DEV_DB_UPD",pcgTwEnd,pclDBSel,
				pclDBFields,pcpResult,NULL,0)) != RC_SUCCESS)
	    {
	      dbg(TRACE,"<TriggerRemarks> SendEvent() to <%d> returns <%d>!",igModID_Router,ilRc);
	    }
	  
	  return RC_SUCCESS;
	} 

      /*FBG*/
      ilItemNo = get_item_no(pcpFields,"B1BA",5);
      ilItemNo++;
      GetDataItem(pclTmpBuf,pcpData,ilItemNo,',',"","");
      DeleteCharacterInString(pclTmpBuf,cBLANK);
      if(pclTmpBuf[0]!='\0')
	{
	  strcpy(pcpResult,"FBG");
	  sprintf(pclDBSel,"WHERE AURN = %s",pcpUrno);
	  strcpy(pclDBFields,"CREC");
	  dbg(TRACE,"SEND URT CREC <%s> URNO <%s> FLNO <%s>", pcpResult, pcpUrno ,pclFlno);
	  if ((ilRc = SendEvent("URT",igModID_Router,PRIORITY_4,"FLD","DEV_DB_UPD",pcgTwEnd,pclDBSel,
				pclDBFields,pcpResult,NULL,0)) != RC_SUCCESS)
	    {
	      dbg(TRACE,"<TriggerRemarks> SendEvent() to <%d> returns <%d>!",igModID_Router,ilRc);
	    }
	  return RC_SUCCESS;
	}
 
    }
#if 0
  if(strcmp(pcpAdid,"D")==0)
    {
      /*CANCELLED*/
      ilItemNo = get_item_no(pcpFields,"FTYP",5);
      ilItemNo++;
      GetDataItem(pclTmpBuf,pcpData,ilItemNo,',',"","");
      DeleteCharacterInString(pclTmpBuf,cBLANK);
      if(pclTmpBuf[0]=='X'||pclTmpBuf[0]=='x')
	{
	  strcpy(pcpResult,"CNL");
	  sprintf(pclDBSel,"WHERE URNO = %s",pcpUrno);
	  strcpy(pclDBFields,"REMP");
	  if(strstr(pcpRemp,"CNL")==NULL)
	    {    
	      dbg(TRACE,"SEND UFR REMP <%s> URNO <%s> FLNO <%s>", pcpResult, pcpUrno ,pclFlno);
	      if ((ilRc = SendEvent("UFR",igModID_Router,PRIORITY_4,"AFT","DEV_DB_UPD",pcgTwEnd,pclDBSel,
				    pclDBFields,pcpResult,NULL,0)) != RC_SUCCESS)
		{
		  dbg(TRACE,"<TriggerRemarks> SendEvent() to <%d> returns <%d>!",igModID_Router,ilRc);
		}
	    }
	  return RC_SUCCESS;
	}
    }
#endif
#if 0
  if(strcmp(pcpAdid,"A")==0)
    {
      /*CANCELLED*/
      ilItemNo = get_item_no(pcpFields,"FTYP",5);
      ilItemNo++;
      GetDataItem(pclTmpBuf,pcpData,ilItemNo,',',"","");
      DeleteCharacterInString(pclTmpBuf,cBLANK);
      if(pclTmpBuf[0]=='X'||pclTmpBuf[0]=='x')
	{
	  strcpy(pcpResult,"CNL");
	  sprintf(pclDBSel,"WHERE URNO = %s",pcpUrno);
	  strcpy(pclDBFields,"REMP");
	  if(strstr(pcpRemp,"CNL")==NULL)
	    {    
	      dbg(TRACE,"SEND UFR REMP <%s> URNO <%s> FLNO <%s>", pcpResult, pcpUrno ,pclFlno);
	      if ((ilRc = SendEvent("UFR",igModID_Router,PRIORITY_4,"AFT","DEV_DB_UPD",pcgTwEnd,pclDBSel,
				    pclDBFields,pcpResult,NULL,0)) != RC_SUCCESS)
		{
		  dbg(TRACE,"<TriggerRemarks> SendEvent() to <%d> returns <%d>!",igModID_Router,ilRc);
		}
	    }
	  return RC_SUCCESS;
	}
      /*UNCANCEL FLIGHT*/	
      if(pclTmpBuf[0]=='O'||pclTmpBuf[0]=='o')
	{
	  strcpy(pcpResult," ");
	  sprintf(pclDBSel,"WHERE URNO = %s",pcpUrno);
	  strcpy(pclDBFields,"REMP");
	  if(strstr(pcpRemp,"CNL")!=NULL)
	    {    
	      dbg(TRACE,"SEND UFR REMP <%s> URNO <%s> FLNO <%s>", pcpResult, pcpUrno ,pclFlno);
	      if ((ilRc = SendEvent("UFR",igModID_Router,PRIORITY_4,"AFT","DEV_DB_UPD",pcgTwEnd,pclDBSel,
				    pclDBFields,pcpResult,NULL,0)) != RC_SUCCESS)
		{
		  dbg(TRACE,"<TriggerRemarks> SendEvent() to <%d> returns <%d>!",igModID_Router,ilRc);
		}
	    
	      return RC_SUCCESS;
	    }
	}	
      /*DIVERTED*/
      ilItemNo = get_item_no(pcpFields,"FTYP",5);
      ilItemNo++;
      GetDataItem(pclTmpBuf,pcpData,ilItemNo,',',"","");
      DeleteCharacterInString(pclTmpBuf,cBLANK);
      if(pclTmpBuf[0]=='D'||pclTmpBuf[0]=='d')
	{
	  strcpy(pcpResult,"DIV");
	  sprintf(pclDBSel,"WHERE URNO = %s",pcpUrno);
	  strcpy(pclDBFields,"REMP");
	  if(strstr(pcpRemp,"DIV")==NULL)
	    {    
	      dbg(TRACE,"SEND UFR REMP <%s> URNO <%s> FLNO <%s>", pcpResult, pcpUrno ,pclFlno);
	      if ((ilRc = SendEvent("UFR",igModID_Router,PRIORITY_4,"AFT","DEV_DB_UPD",pcgTwEnd,pclDBSel,
				    pclDBFields,pcpResult,NULL,0)) != RC_SUCCESS)
		{
		  dbg(TRACE,"<TriggerRemarks> SendEvent() to <%d> returns <%d>!",igModID_Router,ilRc);
		}
	    }
	  return RC_SUCCESS;
	}
      /*UNDIVERT FLIGHT*/	
      if(pclTmpBuf[0]=='O'||pclTmpBuf[0]=='o')
	{
	  strcpy(pcpResult," ");
	  sprintf(pclDBSel,"WHERE URNO = %s",pcpUrno);
	  strcpy(pclDBFields,"REMP");
	  if(strstr(pcpRemp,"DIV")!=NULL)
	    {    
	      dbg(TRACE,"SEND UFR REMP <%s> URNO <%s> FLNO <%s>", pcpResult, pcpUrno ,pclFlno);
	      if ((ilRc = SendEvent("UFR",igModID_Router,PRIORITY_4,"AFT","DEV_DB_UPD",pcgTwEnd,pclDBSel,
				    pclDBFields,pcpResult,NULL,0)) != RC_SUCCESS)
		{
		  dbg(TRACE,"<TriggerRemarks> SendEvent() to <%d> returns <%d>!",igModID_Router,ilRc);
		}
	      return RC_SUCCESS;
	    }
	}
      /*ARRIVED*/
      ilItemNo = get_item_no(pcpFields,"ONBL",5);
      ilItemNo++;
      GetDataItem(pclTmpBuf,pcpData,ilItemNo,',',"","");
      DeleteCharacterInString(pclTmpBuf,cBLANK);
      if(pclTmpBuf[0]!='\0')
	{
	  strcpy(pcpResult,"ARR");
	  sprintf(pclDBSel,"WHERE URNO = %s",pcpUrno);
	  strcpy(pclDBFields,"REMP");
	  if(strstr(pcpRemp,"ARR")==NULL)
	    {
	      dbg(TRACE,"SEND UFR REMP <%s> URNO <%s> FLNO <%s>", pcpResult, pcpUrno ,pclFlno);
	      if ((ilRc = SendEvent("UFR",igModID_Router,PRIORITY_4,"AFT","DEV_DB_UPD",pcgTwEnd,pclDBSel,
				    pclDBFields,pcpResult,NULL,0)) != RC_SUCCESS)
		{
		  dbg(TRACE,"<TriggerRemarks> SendEvent() to <%d> returns <%d>!",igModID_Router,ilRc);
		}
	    }
	  return RC_SUCCESS;
	}
      /*LAND*/
      ilItemNo = get_item_no(pcpFields,"LAND",5);
      ilItemNo++;
      GetDataItem(pclTmpBuf,pcpData,ilItemNo,',',"","");
      DeleteCharacterInString(pclTmpBuf,cBLANK);
      if(pclTmpBuf[0]!='\0')
	{
	  strcpy(pcpResult,"LND");
	  sprintf(pclDBSel,"WHERE URNO = %s",pcpUrno);
	  strcpy(pclDBFields,"REMP");
	  if(strstr(pcpRemp,"LND")==NULL)
	    {
	      dbg(TRACE,"SEND UFR REMP <%s> URNO <%s> FLNO <%s>", pcpResult, pcpUrno ,pclFlno);
	      if ((ilRc = SendEvent("UFR",igModID_Router,PRIORITY_4,"AFT","DEV_DB_UPD",pcgTwEnd,pclDBSel,
				    pclDBFields,pcpResult,NULL,0)) != RC_SUCCESS)
		{
		  dbg(TRACE,"<TriggerRemarks> SendEvent() to <%d> returns <%d>!",igModID_Router,ilRc);
		}
	    }
	  return RC_SUCCESS;
	}
      /*DELAYED*/
      ilItemNo = get_item_no(pcpFields,"ETOA",5);
      ilItemNo++;
      GetDataItem(pclTimeBuf1,pcpData,ilItemNo,',',"","");
      DeleteCharacterInString(pclTimeBuf1,cBLANK);
      ilItemNo = get_item_no(pcpFields,"STOA",5);
      ilItemNo++;
      GetDataItem(pclTimeBuf2,pcpData,ilItemNo,',',"","");
      DeleteCharacterInString(pclTimeBuf2,cBLANK);
      if(strcmp(pclTimeBuf1,pclTimeBuf2)>0)
	{
	  strcpy(pcpResult,"DLY");
	  sprintf(pclDBSel,"WHERE URNO = %s",pcpUrno);
	  strcpy(pclDBFields,"REMP");
	  if(strstr(pcpRemp,"DLY")==NULL)
	    {    
	      dbg(TRACE,"SEND UFR REMP <%s> URNO <%s> FLNO <%s>", pcpResult, pcpUrno ,pclFlno);
	      if ((ilRc = SendEvent("UFR",igModID_Router,PRIORITY_4,"AFT","DEV_DB_UPD",pcgTwEnd,pclDBSel,
				    pclDBFields,pcpResult,NULL,0)) != RC_SUCCESS)
		{
		  dbg(TRACE,"<TriggerRemarks> SendEvent() to <%d> returns <%d>!",igModID_Router,ilRc);
		}
	    }
	  return RC_SUCCESS;
	}
      /*EXPECTED*/
      ilItemNo = get_item_no(pcpFields,"ETOA",5);
      ilItemNo++;
      GetDataItem(pclTimeBuf1,pcpData,ilItemNo,',',"","");
      DeleteCharacterInString(pclTimeBuf1,cBLANK);
      ilItemNo = get_item_no(pcpFields,"STOA",5);
      ilItemNo++;
      GetDataItem(pclTimeBuf2,pcpData,ilItemNo,',',"","");
      DeleteCharacterInString(pclTimeBuf2,cBLANK);
      if(strcmp(pclTimeBuf2,pclTimeBuf1)>0&&pclTimeBuf1[0] != '\0')
	{
	  strcpy(pcpResult,"EXP");
	  sprintf(pclDBSel,"WHERE URNO = %s",pcpUrno);
	  strcpy(pclDBFields,"REMP");
	  if(strstr(pcpRemp,"EXP")==NULL)
	    {    
	      dbg(TRACE,"SEND UFR REMP <%s> URNO <%s> FLNO <%s>", pcpResult, pcpUrno ,pclFlno);
	      if ((ilRc = SendEvent("UFR",igModID_Router,PRIORITY_4,"AFT","DEV_DB_UPD",pcgTwEnd,pclDBSel,
				    pclDBFields,pcpResult,NULL,0)) != RC_SUCCESS)
		{
		  dbg(TRACE,"<TriggerRemarks> SendEvent() to <%d> returns <%d>!",igModID_Router,ilRc);
		}
	    }
	  return RC_SUCCESS;
	}
    }/*end arrival*/
  /*for departure*/
  if(strcmp(pcpAdid,"D")==0)
    {
      /*CANCELLED*/
      ilItemNo = get_item_no(pcpFields,"FTYP",5);
      ilItemNo++;
      GetDataItem(pclTmpBuf,pcpData,ilItemNo,',',"","");
      DeleteCharacterInString(pclTmpBuf,cBLANK);
      if(pclTmpBuf[0]=='X'||pclTmpBuf[0]=='x')
	{
	  strcpy(pcpResult,"CNL");
	  sprintf(pclDBSel,"WHERE URNO = %s",pcpUrno);
	  strcpy(pclDBFields,"REMP");
	  if(strstr(pcpRemp,"CNL")==NULL)
	    {    
	      dbg(TRACE,"SEND UFR REMP <%s> URNO <%s> FLNO <%s>", pcpResult, pcpUrno ,pclFlno);
	      if ((ilRc = SendEvent("UFR",igModID_Router,PRIORITY_4,"AFT","DEV_DB_UPD",pcgTwEnd,pclDBSel,
				    pclDBFields,pcpResult,NULL,0)) != RC_SUCCESS)
		{
		  dbg(TRACE,"<TriggerRemarks> SendEvent() to <%d> returns <%d>!",igModID_Router,ilRc);
		}
	    }
	  return RC_SUCCESS;
	}
      /*UNCANCEL FLIGHT*/
      if(pclTmpBuf[0]=='O'||pclTmpBuf[0]=='o')
	{
	  strcpy(pcpResult," ");
	  sprintf(pclDBSel,"WHERE URNO = %s",pcpUrno);
	  strcpy(pclDBFields,"REMP");
	  if(strstr(pcpRemp,"CNL")!=NULL)
	    {    
	      dbg(TRACE,"SEND UFR REMP <%s> URNO <%s> FLNO <%s>", pcpResult, pcpUrno ,pclFlno);
	      if ((ilRc = SendEvent("UFR",igModID_Router,PRIORITY_4,"AFT","DEV_DB_UPD",pcgTwEnd,pclDBSel,
				    pclDBFields,pcpResult,NULL,0)) != RC_SUCCESS)
		{
		  dbg(TRACE,"<TriggerRemarks> SendEvent() to <%d> returns <%d>!",igModID_Router,ilRc);
		}
	      return RC_SUCCESS;
	    }
	}
      /*DEPARTED*/
      ilItemNo = get_item_no(pcpFields,"OFBL",5);
      ilItemNo++;
      GetDataItem(pclTmpBuf,pcpData,ilItemNo,',',"","");
      DeleteCharacterInString(pclTmpBuf,cBLANK);
      if(pclTmpBuf[0]!='\0')
	{
	  strcpy(pcpResult,"DEP");
	  sprintf(pclDBSel,"WHERE URNO = %s",pcpUrno);
	  strcpy(pclDBFields,"REMP");
	  if(strstr(pcpRemp,"DEP")==NULL)
	    {
	      dbg(TRACE,"SEND UFR REMP <%s> URNO <%s> FLNO <%s>", pcpResult, pcpUrno ,pclFlno);
	      if ((ilRc = SendEvent("UFR",igModID_Router,PRIORITY_4,"AFT","DEV_DB_UPD",pcgTwEnd,pclDBSel,
				    pclDBFields,pcpResult,NULL,0)) != RC_SUCCESS)
		{
		  dbg(TRACE,"<TriggerRemarks> SendEvent() to <%d> returns <%d>!",igModID_Router,ilRc);
		}
	    }
	  return RC_SUCCESS;
	}
      pclTimeBuf2[0]='\0';
      pclTimeBuf1[0]='\0';
      ilItemNo = get_item_no(pcpFields,"REMP",5);
      ilItemNo++;
      GetDataItem(pclTimeBuf2,pcpData,ilItemNo,',',"","");
      DeleteCharacterInString(pclTimeBuf2,cBLANK);
      ilItemNo = get_item_no(pcpFields,"GD1Y",5);
      ilItemNo++;
      GetDataItem(pclTimeBuf1,pcpData,ilItemNo,',',"","");
      DeleteCharacterInString(pclTimeBuf1,cBLANK);
      if(pclTimeBuf1[0]!='\0')
	{
	  strcpy(pcpResult,"GCL");
	  sprintf(pclDBSel,"WHERE URNO = %s",pcpUrno);
	  strcpy(pclDBFields,"REMP");
	  if(strstr(pcpRemp,"GCL")==NULL)
	    {    
	      dbg(TRACE,"SEND UFR REMP <%s> URNO <%s> FLNO <%s>", pcpResult, pcpUrno ,pclFlno);
	      if ((ilRc = SendEvent("UFR",igModID_Router,PRIORITY_4,"AFT","DEV_DB_UPD",pcgTwEnd,pclDBSel,
				    pclDBFields,pcpResult,NULL,0)) != RC_SUCCESS)
		{
		  dbg(TRACE,"<TriggerRemarks> SendEvent() to <%d> returns <%d>!",igModID_Router,ilRc);
		}
	    }
	  return RC_SUCCESS; 
	}
      pclTimeBuf2[0]='\0';
      pclTimeBuf1[0]='\0';
      ilItemNo = get_item_no(pcpFields,"REMP",5);
      ilItemNo++;
      GetDataItem(pclTimeBuf2,pcpData,ilItemNo,',',"","");
      DeleteCharacterInString(pclTimeBuf2,cBLANK);
      if(!strcmp(pclTimeBuf2,"BRG")||!strcmp(pclTimeBuf2,"FNL")||!strcmp(pclTimeBuf2,"GCL"))
	{
	  strcpy(pcpResult,pclTimeBuf2);
	  return RC_SUCCESS;
	}
      pclTimeBuf2[0]='\0';
      pclTimeBuf1[0]='\0';
      /*GateOpen*/
      ilItemNo = get_item_no(pcpFields,"GD1X",5);
      ilItemNo++;
      GetDataItem(pclTimeBuf1,pcpData,ilItemNo,',',"","");
      DeleteCharacterInString(pclTimeBuf1,cBLANK);
      ilItemNo = get_item_no(pcpFields,"ETOD",5);
      ilItemNo++;
      GetDataItem(pclTimeBuf2,pcpData,ilItemNo,',',"","");
      DeleteCharacterInString(pclTimeBuf2,cBLANK);
      if(pclTimeBuf1[0]!='\0')
	{
	  if(pclTimeBuf2[0]=='\0')
	    {
	      strcpy(pcpResult,"GTP");
	      sprintf(pclDBSel,"WHERE URNO = %s",pcpUrno);
	      strcpy(pclDBFields,"REMP");
	      if(strstr(pcpRemp,"GTP")==NULL)
		{    
		  dbg(TRACE,"SEND UFR REMP <%s> URNO <%s> FLNO <%s>", pcpResult, pcpUrno ,pclFlno);
		  if ((ilRc = SendEvent("UFR",igModID_Router,PRIORITY_4,"AFT","DEV_DB_UPD",pcgTwEnd,pclDBSel,
					pclDBFields,pcpResult,NULL,0)) != RC_SUCCESS)
		    {
		      dbg(TRACE,"<TriggerRemarks> SendEvent() to <%d> returns <%d>!",igModID_Router,ilRc);
		    }
		}
	      return RC_SUCCESS;
	    }
	  if(pclTimeBuf2[0]!='\0' )
	    {
	      strcpy(pcpResult,"GTP");
	      sprintf(pclDBSel,"WHERE URNO = %s",pcpUrno);
	      strcpy(pclDBFields,"REMP");
	      if(strstr(pcpRemp,"GTP")==NULL)
		{    
		  dbg(TRACE,"SEND UFR REMP <%s> URNO <%s> FLNO <%s>", pcpResult, pcpUrno ,pclFlno);
		  if ((ilRc = SendEvent("UFR",igModID_Router,PRIORITY_4,"AFT","DEV_DB_UPD",pcgTwEnd,pclDBSel,
					pclDBFields,pcpResult,NULL,0)) != RC_SUCCESS)
		    {
		      dbg(TRACE,"<TriggerRemarks> SendEvent() to <%d> returns <%d>!",igModID_Router,ilRc);
		    }
		}
	      return RC_SUCCESS;
	    }
	  return RC_SUCCESS;
	}
      /*DELAYED*/
      ilItemNo = get_item_no(pcpFields,"ETOD",5);
      ilItemNo++;
      GetDataItem(pclTimeBuf1,pcpData,ilItemNo,',',"","");
      DeleteCharacterInString(pclTimeBuf1,cBLANK);
      ilItemNo = get_item_no(pcpFields,"STOD",5);
      ilItemNo++;
      GetDataItem(pclTimeBuf2,pcpData,ilItemNo,',',"","");
      DeleteCharacterInString(pclTimeBuf2,cBLANK);
      if(strcmp(pclTimeBuf1,pclTimeBuf2)>0)
	{
	  strcpy(pcpResult,"DLY");
	  sprintf(pclDBSel,"WHERE URNO = %s",pcpUrno);
	  strcpy(pclDBFields,"REMP");
	  if(strstr(pcpRemp,"DLY")==NULL)
	    {    
	      dbg(TRACE,"SEND UFR REMP <%s> URNO <%s> FLNO <%s>", pcpResult, pcpUrno ,pclFlno);
	      if ((ilRc = SendEvent("UFR",igModID_Router,PRIORITY_4,"AFT","DEV_DB_UPD",pcgTwEnd,pclDBSel,
				    pclDBFields,pcpResult,NULL,0)) != RC_SUCCESS)
		{
		  dbg(TRACE,"<TriggerRemarks> SendEvent() to <%d> returns <%d>!",igModID_Router,ilRc);
		}
	    }
	  return RC_SUCCESS;
	}
    }/*end departure*/
#endif
  return NOT_FOUND;
}
 

static int TriggerCheckin()
{
  int ilRc = RC_SUCCESS;
  int ilFieldCnt = 0;
  int ilItemNo = 0;
  char pclTmpBuf[XS_BUFF];
  char pclTmpBuf2[XS_BUFF];
  char pclTimeBuf1[XS_BUFF];
  char pclTimeBuf2[XS_BUFF];
  char pclDBSel[XS_BUFF];
  char pclDBFields[XS_BUFF];
  char pclDBData[XS_BUFF];
  char pclFlno[XS_BUFF];        
  time_t    now;
  struct tm *_tm;
  char      _tmpc[6]; 
  char pclTimeNow[15];
  char pclTime1Now[15];
  char pclTimeparamDel1[XS_BUFF];
  char pclTimeparamDel2[XS_BUFF];
  memset(pclDBSel ,0x00,XS_BUFF);
  memset(pclDBFields ,0x00,XS_BUFF);
  memset(pclDBData ,0x00,XS_BUFF);
  memset(pclTmpBuf,0x00,XS_BUFF);
  memset(pclTmpBuf2,0x00,XS_BUFF);
  memset(pclTimeBuf1,0x00,XS_BUFF);
  memset(pclTimeBuf2,0x00,XS_BUFF);


  sprintf(pclTimeparamDel1,"%0.6f", (float)(igUTCDIFF-igOTHERAIRLINE)/1440);
  sprintf(pclTimeparamDel2,"%0.6f", (float)(igUTCDIFF-igHOMEAIRLINE)/1440);
  
  strcpy(pclDBData,"");
  sprintf(pclDBSel,"update fddtab set rem2= 'CXX' where ADID='D' and REMP = 'CXX'",pclTimeparamDel1);
  strcpy(pclDBFields,"REM2");
  

  if ((ilRc = SendEvent("SQL",7150,PRIORITY_4,"","DEV_DB_UPD",pcgTwEnd,pclDBSel,
			pclDBFields,pclDBData,NULL,0)) != RC_SUCCESS)
    {
      dbg(TRACE,"<TriggerRemarks> SendEvent() to <%d> returns <%d>!",7150,ilRc);
    }
  
  
  strcpy(pclDBData,"");
  sprintf(pclDBSel,"update fddtab set rem2 = '%s' where ADID='D' and CKIF <> ' ' and ETOF <> ' 'and REMP <> 'CXX'",pcgDELAYED,pclTimeparamDel1);
  strcpy(pclDBFields,"");
  

  if ((ilRc = SendEvent("SQL",7150,PRIORITY_4,"","DEV_DB_UPD",pcgTwEnd,pclDBSel,
			pclDBFields,pclDBData,NULL,0)) != RC_SUCCESS)
    {
      dbg(TRACE,"<TriggerRemarks> SendEvent() to <%d> returns <%d>!",7150,ilRc);
    }
  
  strcpy(pclDBData,"");
  sprintf(pclDBSel,"update fddtab set rem2 = '%s' where ADID='D' and CKIF <> ' ' and ETOF = ' ' and FLNO NOT LIKE 'EK%%' and REMP <> 'CXX' and (STOF <= TO_CHAR(SYSDATE -%s,'YYYYMMDDHH24MISS'))",pcgCLOSING,pclTimeparamDel1);
  strcpy(pclDBFields,"");
  nap(100);

  if ((ilRc = SendEvent("SQL",7150,PRIORITY_4,"","DEV_DB_UPD",pcgTwEnd,pclDBSel,
			pclDBFields,pclDBData,NULL,0)) != RC_SUCCESS)
    {
      dbg(TRACE,"<TriggerRemarks> SendEvent() to <%d> returns <%d>!",7150,ilRc);
    }
  strcpy(pclDBData,"");
  sprintf(pclDBSel,"update fddtab set rem2 = '%s' where ADID='D' and CKIF <> ' ' and ETOF = ' ' and FLNO NOT LIKE 'EK%%' and REMP <> 'CXX' and (STOF > TO_CHAR(SYSDATE -%s,'YYYYMMDDHH24MISS'))",pcgOPEN,pclTimeparamDel1);
  strcpy(pclDBFields,"");
  nap(100);

  if ((ilRc = SendEvent("SQL",7150,PRIORITY_4,"","DEV_DB_UPD",pcgTwEnd,pclDBSel,
			pclDBFields,pclDBData,NULL,0)) != RC_SUCCESS)
    {
      dbg(TRACE,"<TriggerRemarks> SendEvent() to <%d> returns <%d>!",7150,ilRc);
    }
  
  
  strcpy(pclDBData,"");
  sprintf(pclDBSel,"update fddtab set rem2 = '%s' where ADID='D' and CKIF <> ' ' and ETOF = ' ' and FLNO LIKE 'EK%%' and REMP <> 'CXX' and (STOF <= TO_CHAR(SYSDATE -%s,'YYYYMMDDHH24MISS'))",pcgCLOSING,pclTimeparamDel2);
  strcpy(pclDBFields,"");
  nap(100);

  if ((ilRc = SendEvent("SQL",7150,PRIORITY_4,"FDD","DEV_DB_UPD",pcgTwEnd,pclDBSel,
			pclDBFields,pclDBData,NULL,0)) != RC_SUCCESS)
    {
      dbg(TRACE,"<TriggerRemarks> SendEvent() to <%d> returns <%d>!",7150,ilRc);
    }
  strcpy(pclDBData,"");
  sprintf(pclDBSel,"update fddtab set rem2 = '%s' where ADID='D' and CKIF <> ' ' and ETOF = ' ' and FLNO LIKE 'EK%%' and REMP <> 'CXX' and (STOF > TO_CHAR(SYSDATE -%s,'YYYYMMDDHH24MISS'))",pcgOPEN,pclTimeparamDel2);
  strcpy(pclDBFields,"");
  nap(100);

  if ((ilRc = SendEvent("SQL",7150,PRIORITY_4,"FDD","DEV_DB_UPD",pcgTwEnd,pclDBSel,
			pclDBFields,pclDBData,NULL,0)) != RC_SUCCESS)
    {
      dbg(TRACE,"<TriggerRemarks> SendEvent() to <%d> returns <%d>!",7150,ilRc);
    }
  

  return RC_SUCCESS;

}


/*******************************************++++++++++*************************/
/*   GetSecondsFromCEDATime                                                   */
/*                                                                            */
/*   Serviceroutine, rechnet einen Datumseintrag im Cedaformat in Seconds um  */
/*   Input:  char *  Zeiger auf CEDA-Zeitpuffer                               */
/*   Output: long    Zeitwert                                                 */
/******************************************************************************/

long GetSecondsFromCEDATime(char *pcpDateTime)
{
  long rc = 0;
  int year;
  char ch_help[5];
  struct tm *tstr1 = NULL, t1;
  struct tm *CurTime, t2;
  time_t llTime;
  time_t llUtcTime;
  time_t llLocalTime;

  CurTime = &t2;
  llTime = time(NULL);
  CurTime = (struct tm *)gmtime(&llTime);
  CurTime->tm_isdst = 0;
  llUtcTime = mktime(CurTime);
  CurTime = (struct tm *)localtime(&llTime);
  CurTime->tm_isdst = 0;
  llLocalTime = mktime(CurTime);
  igDiffUtcToLocal = llLocalTime - llUtcTime;
  /* dbg(TRACE,"UTC: <%ld> , Local: <%ld> , Diff: <%ld>",llUtcTime,llLocalTime,igDiffUtcToLocal); */

  memset(&t1,0x00,sizeof(struct tm));

  tstr1 = &t1;

  memset(ch_help,0x00,5);
  strncpy(ch_help,pcpDateTime,4);
  year = atoi(ch_help);
  tstr1->tm_year = year - 1900;

  memset(ch_help,0x00,5);
  strncpy(ch_help,&pcpDateTime[4],2);
  tstr1->tm_mon = atoi(ch_help) -1;

  memset(ch_help,0x00,5);
  strncpy(ch_help,&pcpDateTime[6],2);
  tstr1->tm_mday = atoi(ch_help);

  memset(ch_help,0x00,5);
  strncpy(ch_help,&pcpDateTime[8],2);
  tstr1->tm_hour = atoi(ch_help);

  memset(ch_help,0x00,5);
  strncpy(ch_help,&pcpDateTime[10],2);
  tstr1->tm_min = atoi(ch_help);

  memset(ch_help,0x00,5);
  strncpy(ch_help,&pcpDateTime[12],2);
  tstr1->tm_sec = atoi(ch_help);

  tstr1->tm_wday = 0;
  tstr1->tm_yday = 0;
  tstr1->tm_isdst = 0;
  rc = mktime(tstr1);

  rc = rc + igDiffUtcToLocal; /* add difference between local and utc */

  return(rc);
} /* end of GetSecondsFromCEDATime() */
 

static int TriggerCheckinNew()
{
  int ilRC = RC_SUCCESS;
  int ilRCdbRd = DB_SUCCESS;
  short slFktRd;
  short slCursorRd;
  char pclSqlBufRd[L_BUFF];
  char pclSelectBufRd[L_BUFF];
  char pclDataBufRd[XL_BUFF];
  int ilRCdbWr = DB_SUCCESS;
  short slFktWr;
  short slCursorWr;
  char pclSqlBufWr[L_BUFF];
  char pclSelectBufWr[L_BUFF];
  char pclDataBufWr[XL_BUFF];
  int ilTotRecs;
  int ilUpdRecs;
  /* char pclUrno[16] enhanced due to database change MCU 20061103*/
  char pclUrno[24];
  char pclFlno[16];
  char pclStof[32];
  char pclEtof[32];
  char pclCkif[16];
  char pclRemp[16];
  char pclRem2[16];
  /*char pclAurn[16]; enhanced due to database change MCU 20061103*/

  char pclAurn[24];
  char pclTime[32];
  char pclRemarkTime[32];
  char pclOpenTime[32];
  char pclClosingTime[32];
  char pclClosedTime[32];
  char pclAirline[8];
  int ilLen;
  int ilTimeDiffRemark;
  int ilTimeDiffOpen;
  int ilTimeDiffClosing;
  int ilTimeDiffClosed;
  char pclNewRem2[16];
  long llCurTime;
  long llRemarkTime;
  long llOpenTime;
  long llClosingTime;
  long llClosedTime;

  ilTotRecs = 0;
  ilUpdRecs = 0; 
  strcpy(pclSqlBufRd,"SELECT URNO,FLNO,STOF,ETOF,CKIF,REMP,REM2,AURN FROM FDDTAB ");
  sprintf(pclSelectBufRd,"WHERE ADID = 'D' AND DSEQ IN ('0','1') ORDER BY STOF");
  strcat(pclSqlBufRd,pclSelectBufRd);
  slCursorRd = 0;
  slFktRd = START;
  /* dbg(TRACE,"TriggerCheckinNew: SqlBuf = <%s>",pclSqlBufRd); */
  ilRCdbRd = sql_if(slFktRd,&slCursorRd,pclSqlBufRd,pclDataBufRd);
  while (ilRCdbRd == DB_SUCCESS)
  {
     ilTotRecs++;
     BuildItemBuffer(pclDataBufRd,"",8,",");
     ilLen = get_real_item(pclUrno,pclDataBufRd,1);
     ilLen = get_real_item(pclFlno,pclDataBufRd,2);
     ilLen = get_real_item(pclStof,pclDataBufRd,3);
     ilLen = get_real_item(pclEtof,pclDataBufRd,4);
     ilLen = get_real_item(pclCkif,pclDataBufRd,5);
     ilLen = get_real_item(pclRemp,pclDataBufRd,6);
     ilLen = get_real_item(pclRem2,pclDataBufRd,7);
     if (ilLen == 0)
        strcpy(pclRem2," ");
     ilLen = get_real_item(pclAurn,pclDataBufRd,8);
     if (ilLen == 0)
        strcpy(pclRem2," ");
     strncpy(pclAirline,pclFlno,3);
     if (pclAirline[2] == ' ')
        pclAirline[2] = '\0';
     else
        pclAirline[3] = '\0';
     if (strlen(pclAirline) > 0 && strlen(pcgEKAirlines) > 0 &&
         strstr(pcgEKAirlines,pclAirline) != NULL)
     {
        ilTimeDiffRemark = igRemarkBeforeStdForEK;
        ilTimeDiffOpen = igOpenBeforeStdForEK;
        ilTimeDiffClosing = igClosingBeforeEtdForEK;
        ilTimeDiffClosed = igClosedBeforeEtdForEK;
     }
     else
     {
        ilTimeDiffRemark = igRemarkBeforeStdForOther;
        ilTimeDiffOpen = igOpenBeforeStdForOther;
        ilTimeDiffClosing = igClosingBeforeEtdForOther;
        ilTimeDiffClosed = igClosedBeforeEtdForOther;
     }
     strcpy(pclTime,pclStof);
     if (strlen(pclEtof) > 0)
     {
        if (strcmp(pclEtof,pclStof) > 0)
        {
           strcpy(pclTime,pclEtof);
        }
     }
     llCurTime = time(NULL);
     strcpy(pclRemarkTime,pclStof);
     ilRC = AddSecondsToCEDATime(pclRemarkTime,ilTimeDiffRemark*(-60),1);
     llRemarkTime = GetSecondsFromCEDATime(pclRemarkTime);
     strcpy(pclOpenTime,pclStof);
     ilRC = AddSecondsToCEDATime(pclOpenTime,ilTimeDiffOpen*(-60),1);
     llOpenTime = GetSecondsFromCEDATime(pclOpenTime);
     strcpy(pclClosingTime,pclTime);
     ilRC = AddSecondsToCEDATime(pclClosingTime,ilTimeDiffClosing*(-60),1);
     llClosingTime = GetSecondsFromCEDATime(pclClosingTime);
     strcpy(pclClosedTime,pclTime);
     ilRC = AddSecondsToCEDATime(pclClosedTime,ilTimeDiffClosed*(-60),1);
     llClosedTime = GetSecondsFromCEDATime(pclClosedTime);
     strcpy(pclNewRem2," ");
     if (strlen(pclRemp) > 0 && strlen(pcgSpecialRemarks) > 0 && 
         strstr(pcgSpecialRemarks,pclRemp) != NULL)
     {
        strcpy(pclNewRem2,pclRemp);
     }
     else
     {
        if (llCurTime >= llRemarkTime || ilTimeDiffRemark == 0)
        {
           if (strlen(pclCkif) > 0)
           {
              if (llCurTime >= llClosedTime)
                 strcpy(pclNewRem2,pcgCLOSED);
              else if (llCurTime >= llClosingTime)
                 strcpy(pclNewRem2,pcgCLOSING);
              else if (llCurTime >= llOpenTime || ilTimeDiffOpen == 0)
                 strcpy(pclNewRem2,pcgOPEN);
              else
                 strcpy(pclNewRem2,pcgCLOSED);
           }
           else
           {
              if (llCurTime >= llClosingTime)
                 strcpy(pclNewRem2,pcgCLOSED);
              else if (strlen(pclEtof) > 0)
              {
                 ilRC = AddSecondsToCEDATime(pclEtof,igDelayTimeDiffDep*(-60),1);
                 if (strcmp(pclEtof,pclStof) > 0)
                    strcpy(pclNewRem2,pcgDELAYED);
              }
              else
              {
                 strcpy(pclNewRem2,pcgCLOSED);
              }
           }
        }
     }
     if (strcmp(pclNewRem2,pclRem2) != 0)
     {
        sprintf(pclSqlBufWr,"UPDATE FDDTAB SET REM2 = '%s' ",pclNewRem2);
        /*sprintf(pclSelectBufWr,"WHERE URNO = %s",pclUrno);*/
        sprintf(pclSelectBufWr,"WHERE AURN = %s",pclAurn);
        strcat(pclSqlBufWr,pclSelectBufWr);
        slCursorWr = 0;
        slFktWr = START;
        dbg(TRACE,"TriggerCheckinNew: SqlBuf = <%s>",pclSqlBufWr);
        ilRCdbWr = sql_if(slFktWr,&slCursorWr,pclSqlBufWr,pclDataBufWr);
        commit_work();
        close_my_cursor(&slCursorWr);
        ilUpdRecs++;
     }
     slFktRd = NEXT;
     ilRCdbRd = sql_if(slFktRd,&slCursorRd,pclSqlBufRd,pclDataBufRd);
  }
  close_my_cursor(&slCursorRd);
  /*dbg(TRACE,"There were %d / %d records of FDDTAB read / updated",ilTotRecs,ilUpdRecs);*/
  dbg(TRACE,"There were %d / %d main records of FDDTAB read / updated as well as their code-shares!",ilTotRecs,ilUpdRecs);

  return ilRC;
} /* End of TriggerCheckinNew */

