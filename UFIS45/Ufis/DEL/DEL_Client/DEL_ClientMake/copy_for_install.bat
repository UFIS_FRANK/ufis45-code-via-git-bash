rem Copies all generated files from the SCM-Structue into the Install-Shield directory.
rem 

rem set drive to J: (IS 6.2) or K: (IS 8.0) 
set drive=C:\InstallShield

rem Copy ini- and config-files
copy c:\ufis_bin\*.ini %drive%\DEL_Build\ufis_client_appl\system\*.*
copy c:\ufis_bin\*.bmp %drive%\DEL_Build\ufis_client_appl\system\*.*
copy c:\ufis_bin\*.cfg %drive%\DEL_Build\ufis_client_appl\system\*.*
copy c:\ufis_bin\rosteringprint.ulf %drive%\DEL_Build\ufis_client_appl\system\*.*

rem copy applications
copy c:\ufis_bin\debug\bdps_sec.exe %drive%\DEL_Build\ufis_client_appl\applications\*.*
copy c:\ufis_bin\debug\bdpspass.exe %drive%\DEL_Build\ufis_client_appl\applications\*.*
copy c:\ufis_bin\debug\bdpsuif.exe %drive%\DEL_Build\ufis_client_appl\applications\*.*
copy c:\ufis_bin\debug\ucdialog.exe %drive%\DEL_build\ufis_client_appl\applications\*.*
copy c:\ufis_bin\debug\fips.exe %drive%\DEL_Build\ufis_client_appl\applications\*.*
copy c:\ufis_bin\debug\rules.exe %drive%\DEL_Build\ufis_client_appl\applications\*.*
copy c:\ufis_bin\debug\coverage.exe %drive%\DEL_Build\ufis_client_appl\applications\*.*
copy c:\ufis_bin\debug\grp.exe %drive%\DEL_Build\ufis_client_appl\applications\*.*
copy c:\ufis_bin\debug\opsspm.exe %drive%\DEL_Build\ufis_client_appl\applications\*.*
copy c:\ufis_bin\debug\regelwerk.exe %drive%\DEL_Build\ufis_client_appl\applications\*.*
copy c:\ufis_bin\debug\servicecatalog.exe %drive%\DEL_Build\ufis_client_appl\applications\*.*
copy c:\ufis_bin\release\fidas.exe %drive%\DEL_build\ufis_client_appl\applications\*.*
copy c:\ufis_bin\release\loadtabviewer.exe %drive%\DEL_Build\ufis_client_appl\applications\*.*
copy c:\ufis_bin\release\telexpool.exe %drive%\DEL_Build\ufis_client_appl\applications\*.*
copy c:\ufis_bin\release\importflights.exe %drive%\DEL_build\ufis_client_appl\applications\*.*
copy c:\ufis_bin\release\rostering.exe %drive%\DEL_Build\ufis_client_appl\applications\*.*
copy c:\ufis_bin\release\rosteringprint.exe %drive%\DEL_Build\ufis_client_appl\applications\*.*
copy c:\ufis_bin\release\fips_reports.exe %drive%\DEL_Build\ufis_client_appl\applications\*.*
copy c:\ufis_bin\release\Data_Changes.exe %drive%\DEL_Build\ufis_client_appl\applications\*.*


rem copy System Components
copy c:\ufis_bin\release\aatlogin.ocx %drive%\DEL_Build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\aatlogin.tlb %drive%\DEL_Build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\bcproxy.exe %drive%\DEL_Build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\bcproxy.tlb %drive%\DEL_Build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\bcserv32.exe %drive%\DEL_Build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\tab.ocx %drive%\DEL_Build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\tab.tlb %drive%\DEL_Build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\ucom.ocx %drive%\DEL_Build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\ucom.tlb %drive%\DEL_Build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\ufis32.dll %drive%\DEL_Build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\ufisappmng.exe %drive%\DEL_Build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\ufisappmng.tlb %drive%\DEL_Build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\ufisapplmgr.exe %drive%\DEL_Build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\ufisapplmgr.tlb %drive%\DEL_Build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\ufiscom.ocx %drive%\DEL_Build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\ufiscom.tlb %drive%\DEL_Build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\ugantt.ocx %drive%\DEL_Build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\ugantt.tlb %drive%\DEL_Build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\ucedit.ocx %drive%\DEL_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\ucedit.tlb %drive%\DEL_build\ufis_client_appl\system\*.*

rem copy interop dll's
copy c:\ufis_bin\release\*lib.dll %drive%\del_build\ufis_client_appl\applications\interopdll\*.*
copy c:\ufis_bin\release\ufis.utils.dll %drive%\del_build\ufis_client_appl\applications\interopdll\*.*
copy c:\ufis_bin\release\usercontrols.dll %drive%\del_build\ufis_client_appl\applications\interopdll\*.*
copy c:\ufis_bin\release\zedgraph.dll %drive%\del_build\ufis_client_appl\applications\interopdll\*.*




rem Copy Help-Files
rem copy c:\ufis_bin\Help\*.chm %drive%\DEL_Build\ufis_client_appl\help\*.*

